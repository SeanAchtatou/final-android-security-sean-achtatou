package com.andframework.config;

import android.util.Log;
import com.andframework.zmfile.ZMFile;
import com.andframework.zmfile.ZMFilePath;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.util.HashMap;

public class LocalData {
    private static LocalData inst = null;
    private static HashMap<String, String> localData = new HashMap<>();
    public static final String localDataKey_updatetips = "localDataKey_updatetips";
    private ZMFilePath path = new ZMFilePath((byte) 0, true);

    private LocalData() {
        initLocalData();
    }

    private void initLocalData() {
        this.path.addFileName("localdata.dat");
        String localPath = this.path.toString();
        if (ZMFile.fileExist(localPath)) {
            ByteArrayInputStream dd = new ByteArrayInputStream(ZMFile.readAll(localPath));
            DataInputStream dis = new DataInputStream(dd);
            boolean rett = true;
            while (rett) {
                try {
                    localData.put(dis.readUTF(), dis.readUTF());
                } catch (EOFException e) {
                    Log.i("LocalData>initLocalData>", "EOFException");
                    rett = false;
                } catch (IOException e2) {
                    Log.i("LocalData>initLocalData>", "IOException");
                    rett = false;
                }
                if (!rett) {
                    try {
                        dis.close();
                        dd.close();
                    } catch (IOException e3) {
                        e3.printStackTrace();
                    }
                }
            }
            return;
        }
        Log.i("LocalData>initLocalData>", "file not exists");
    }

    public static LocalData getInst() {
        if (inst == null) {
            inst = new LocalData();
        }
        return inst;
    }

    public String getLocalValue(String key) {
        if (key == null || key.length() <= 0) {
            return null;
        }
        return localData.get(key);
    }

    public String getLocalDataPath() {
        return this.path.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException} */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0085 A[SYNTHETIC, Splitter:B:27:0x0085] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x008a A[Catch:{ IOException -> 0x008e }] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0094  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0096 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x009f A[SYNTHETIC, Splitter:B:40:0x009f] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00a4 A[Catch:{ IOException -> 0x00a8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00ae  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00b0 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00b9 A[SYNTHETIC, Splitter:B:53:0x00b9] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00be A[Catch:{ IOException -> 0x00c2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00c8  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00ca A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00cf A[SYNTHETIC, Splitter:B:63:0x00cf] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x00d4 A[Catch:{ IOException -> 0x00dc }] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x00d8  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x00da  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean setLocalValue(java.lang.String r17, java.lang.String r18) {
        /*
            r16 = this;
            java.util.HashMap<java.lang.String, java.lang.String> r14 = com.andframework.config.LocalData.localData
            r0 = r17
            r1 = r18
            r14.put(r0, r1)
            r0 = r16
            com.andframework.zmfile.ZMFilePath r14 = r0.path
            java.lang.String r14 = r14.toString()
            com.andframework.zmfile.ZMFile.delFile(r14)
            java.util.HashMap<java.lang.String, java.lang.String> r14 = com.andframework.config.LocalData.localData
            java.util.Set r14 = r14.entrySet()
            java.util.Iterator r9 = r14.iterator()
            r7 = 0
            r2 = 0
            r12 = 1
            java.io.File r6 = new java.io.File     // Catch:{ FileNotFoundException -> 0x0108, UTFDataFormatException -> 0x0098, IOException -> 0x00b2 }
            r0 = r16
            com.andframework.zmfile.ZMFilePath r14 = r0.path     // Catch:{ FileNotFoundException -> 0x0108, UTFDataFormatException -> 0x0098, IOException -> 0x00b2 }
            java.lang.String r14 = r14.toString()     // Catch:{ FileNotFoundException -> 0x0108, UTFDataFormatException -> 0x0098, IOException -> 0x00b2 }
            r6.<init>(r14)     // Catch:{ FileNotFoundException -> 0x0108, UTFDataFormatException -> 0x0098, IOException -> 0x00b2 }
            java.io.File r11 = r6.getParentFile()     // Catch:{ FileNotFoundException -> 0x0108, UTFDataFormatException -> 0x0098, IOException -> 0x00b2 }
            if (r11 == 0) goto L_0x003d
            boolean r14 = r11.exists()     // Catch:{ FileNotFoundException -> 0x0108, UTFDataFormatException -> 0x0098, IOException -> 0x00b2 }
            if (r14 != 0) goto L_0x003d
            r11.mkdirs()     // Catch:{ FileNotFoundException -> 0x0108, UTFDataFormatException -> 0x0098, IOException -> 0x00b2 }
        L_0x003d:
            java.io.FileOutputStream r8 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0108, UTFDataFormatException -> 0x0098, IOException -> 0x00b2 }
            r0 = r16
            com.andframework.zmfile.ZMFilePath r14 = r0.path     // Catch:{ FileNotFoundException -> 0x0108, UTFDataFormatException -> 0x0098, IOException -> 0x00b2 }
            java.lang.String r14 = r14.toString()     // Catch:{ FileNotFoundException -> 0x0108, UTFDataFormatException -> 0x0098, IOException -> 0x00b2 }
            r15 = 1
            r8.<init>(r14, r15)     // Catch:{ FileNotFoundException -> 0x0108, UTFDataFormatException -> 0x0098, IOException -> 0x00b2 }
            java.io.DataOutputStream r3 = new java.io.DataOutputStream     // Catch:{ FileNotFoundException -> 0x010b, UTFDataFormatException -> 0x0101, IOException -> 0x00fa, all -> 0x00f3 }
            r3.<init>(r8)     // Catch:{ FileNotFoundException -> 0x010b, UTFDataFormatException -> 0x0101, IOException -> 0x00fa, all -> 0x00f3 }
        L_0x0050:
            boolean r14 = r9.hasNext()     // Catch:{ FileNotFoundException -> 0x007c, UTFDataFormatException -> 0x0104, IOException -> 0x00fd, all -> 0x00f6 }
            if (r14 != 0) goto L_0x0063
            if (r3 == 0) goto L_0x00e2
            r3.close()     // Catch:{ IOException -> 0x00ea }
        L_0x005b:
            if (r8 == 0) goto L_0x00e5
            r8.close()     // Catch:{ IOException -> 0x00ea }
            r2 = r3
            r7 = r8
        L_0x0062:
            return r12
        L_0x0063:
            java.lang.Object r5 = r9.next()     // Catch:{ FileNotFoundException -> 0x007c, UTFDataFormatException -> 0x0104, IOException -> 0x00fd, all -> 0x00f6 }
            java.util.Map$Entry r5 = (java.util.Map.Entry) r5     // Catch:{ FileNotFoundException -> 0x007c, UTFDataFormatException -> 0x0104, IOException -> 0x00fd, all -> 0x00f6 }
            java.lang.Object r10 = r5.getKey()     // Catch:{ FileNotFoundException -> 0x007c, UTFDataFormatException -> 0x0104, IOException -> 0x00fd, all -> 0x00f6 }
            java.lang.String r10 = (java.lang.String) r10     // Catch:{ FileNotFoundException -> 0x007c, UTFDataFormatException -> 0x0104, IOException -> 0x00fd, all -> 0x00f6 }
            java.lang.Object r13 = r5.getValue()     // Catch:{ FileNotFoundException -> 0x007c, UTFDataFormatException -> 0x0104, IOException -> 0x00fd, all -> 0x00f6 }
            java.lang.String r13 = (java.lang.String) r13     // Catch:{ FileNotFoundException -> 0x007c, UTFDataFormatException -> 0x0104, IOException -> 0x00fd, all -> 0x00f6 }
            r3.writeUTF(r10)     // Catch:{ FileNotFoundException -> 0x007c, UTFDataFormatException -> 0x0104, IOException -> 0x00fd, all -> 0x00f6 }
            r3.writeUTF(r13)     // Catch:{ FileNotFoundException -> 0x007c, UTFDataFormatException -> 0x0104, IOException -> 0x00fd, all -> 0x00f6 }
            goto L_0x0050
        L_0x007c:
            r4 = move-exception
            r2 = r3
            r7 = r8
        L_0x007f:
            r12 = 0
            r4.printStackTrace()     // Catch:{ all -> 0x00cc }
            if (r2 == 0) goto L_0x0094
            r2.close()     // Catch:{ IOException -> 0x008e }
        L_0x0088:
            if (r7 == 0) goto L_0x0096
            r7.close()     // Catch:{ IOException -> 0x008e }
            goto L_0x0062
        L_0x008e:
            r4 = move-exception
            r12 = 0
            r4.printStackTrace()
            goto L_0x0062
        L_0x0094:
            r12 = 0
            goto L_0x0088
        L_0x0096:
            r12 = 0
            goto L_0x0062
        L_0x0098:
            r4 = move-exception
        L_0x0099:
            r12 = 0
            r4.printStackTrace()     // Catch:{ all -> 0x00cc }
            if (r2 == 0) goto L_0x00ae
            r2.close()     // Catch:{ IOException -> 0x00a8 }
        L_0x00a2:
            if (r7 == 0) goto L_0x00b0
            r7.close()     // Catch:{ IOException -> 0x00a8 }
            goto L_0x0062
        L_0x00a8:
            r4 = move-exception
            r12 = 0
            r4.printStackTrace()
            goto L_0x0062
        L_0x00ae:
            r12 = 0
            goto L_0x00a2
        L_0x00b0:
            r12 = 0
            goto L_0x0062
        L_0x00b2:
            r4 = move-exception
        L_0x00b3:
            r12 = 0
            r4.printStackTrace()     // Catch:{ all -> 0x00cc }
            if (r2 == 0) goto L_0x00c8
            r2.close()     // Catch:{ IOException -> 0x00c2 }
        L_0x00bc:
            if (r7 == 0) goto L_0x00ca
            r7.close()     // Catch:{ IOException -> 0x00c2 }
            goto L_0x0062
        L_0x00c2:
            r4 = move-exception
            r12 = 0
            r4.printStackTrace()
            goto L_0x0062
        L_0x00c8:
            r12 = 0
            goto L_0x00bc
        L_0x00ca:
            r12 = 0
            goto L_0x0062
        L_0x00cc:
            r14 = move-exception
        L_0x00cd:
            if (r2 == 0) goto L_0x00d8
            r2.close()     // Catch:{ IOException -> 0x00dc }
        L_0x00d2:
            if (r7 == 0) goto L_0x00da
            r7.close()     // Catch:{ IOException -> 0x00dc }
        L_0x00d7:
            throw r14
        L_0x00d8:
            r12 = 0
            goto L_0x00d2
        L_0x00da:
            r12 = 0
            goto L_0x00d7
        L_0x00dc:
            r4 = move-exception
            r12 = 0
            r4.printStackTrace()
            goto L_0x00d7
        L_0x00e2:
            r12 = 0
            goto L_0x005b
        L_0x00e5:
            r12 = 0
            r2 = r3
            r7 = r8
            goto L_0x0062
        L_0x00ea:
            r4 = move-exception
            r12 = 0
            r4.printStackTrace()
            r2 = r3
            r7 = r8
            goto L_0x0062
        L_0x00f3:
            r14 = move-exception
            r7 = r8
            goto L_0x00cd
        L_0x00f6:
            r14 = move-exception
            r2 = r3
            r7 = r8
            goto L_0x00cd
        L_0x00fa:
            r4 = move-exception
            r7 = r8
            goto L_0x00b3
        L_0x00fd:
            r4 = move-exception
            r2 = r3
            r7 = r8
            goto L_0x00b3
        L_0x0101:
            r4 = move-exception
            r7 = r8
            goto L_0x0099
        L_0x0104:
            r4 = move-exception
            r2 = r3
            r7 = r8
            goto L_0x0099
        L_0x0108:
            r4 = move-exception
            goto L_0x007f
        L_0x010b:
            r4 = move-exception
            r7 = r8
            goto L_0x007f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.andframework.config.LocalData.setLocalValue(java.lang.String, java.lang.String):boolean");
    }
}
