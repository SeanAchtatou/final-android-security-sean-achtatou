package com.andframework.config;

import android.content.Context;
import android.content.res.XmlResourceParser;
import android.util.Log;
import com.andframework.common.KeyValue;
import com.jianq.misc.StringEx;
import java.io.IOException;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParserException;

public class FrameworkConfig {
    private static String DOMAIN = StringEx.Empty;
    private static String LOGNAME = StringEx.Empty;
    private static String LOGPATH = StringEx.Empty;
    private static ArrayList<KeyValue> configValue = new ArrayList<>();
    private static Context context;
    private static FrameworkConfig fConfig;

    private FrameworkConfig(Context c, int R_xml_config) {
        context = c;
        buildConfigData(context, R_xml_config);
    }

    public static FrameworkConfig getInst() {
        if (fConfig == null) {
            fConfig = new FrameworkConfig(null, -1);
        }
        return fConfig;
    }

    public static FrameworkConfig getInst(Context context2, int R_raw_zmconfig) {
        if (fConfig == null) {
            fConfig = new FrameworkConfig(context2, R_raw_zmconfig);
        }
        return fConfig;
    }

    public String getLogPath() {
        return LOGPATH;
    }

    public String getLogName() {
        return LOGNAME;
    }

    public String getDomain() {
        return DOMAIN;
    }

    public String getConfigValue(String key) {
        if (key == null || key.equals(StringEx.Empty)) {
            return null;
        }
        int size = configValue.size();
        for (int i = 0; i < size; i++) {
            KeyValue kv = configValue.get(i);
            if (key.equals(kv.getKey())) {
                return kv.getValue();
            }
        }
        return null;
    }

    private void buildConfigData(Context c, int R_xml_config) {
        XmlResourceParser xrp = c.getResources().getXml(R_xml_config);
        try {
            xrp.next();
            boolean startTag = false;
            String tagname = null;
            for (int eventType = xrp.getEventType(); eventType != 1; eventType = xrp.next()) {
                if (eventType != 0) {
                    if (eventType == 2) {
                        startTag = true;
                        tagname = xrp.getName();
                    } else if (eventType != 3 && eventType == 4 && startTag) {
                        String value = xrp.getText();
                        Log.i(tagname, new StringBuilder(String.valueOf(value)).toString());
                        configValue.add(new KeyValue(tagname, value));
                        startTag = false;
                        if (tagname.equals("domain")) {
                            DOMAIN = value;
                        }
                        if (tagname.equals("logpath")) {
                            LOGPATH = value;
                        }
                        if (tagname.equals("logname")) {
                            LOGNAME = value;
                        }
                    }
                }
            }
        } catch (XmlPullParserException e1) {
            e1.printStackTrace();
        } catch (IOException e12) {
            e12.printStackTrace();
        } finally {
            xrp.close();
        }
    }
}
