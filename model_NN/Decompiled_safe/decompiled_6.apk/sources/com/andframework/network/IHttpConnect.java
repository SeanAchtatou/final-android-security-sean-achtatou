package com.andframework.network;

import android.app.Activity;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import android.util.Log;
import cn.com.jit.pnxclient.constant.PNXConfigConstant;
import com.andframework.common.KeyValue;
import com.jianq.misc.StringEx;
import com.jianq.net.JQBasicNetwork;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import org.apache.http.client.ClientProtocolException;

public class IHttpConnect {
    private static String ENCODING = JQBasicNetwork.UTF_8;
    public static Activity mainActivity;
    private static String proxyHost = null;
    private static int proxyPort = 0;
    private static String session = StringEx.Empty;
    private String boundary = ("----" + System.currentTimeMillis());
    private int connectTimeout = 20000;
    private HttpURLConnection connection;
    private int httpResponseCode = -1;
    private Map<String, UploadFileVO> map_file;
    private Map<String, String> map_normal;
    private int readTimeout = 20000;
    private ArrayList<KeyValue> reqHeadPropertys = new ArrayList<>();

    static {
        System.setProperty("http.keepAlive", "false");
    }

    public void setConnectTimeout(int connectTimeout2) {
        this.connectTimeout = connectTimeout2;
    }

    public void setReadTimeout(int readTimeout2) {
        this.readTimeout = readTimeout2;
    }

    public void setFormData(Map<String, String> map_normal2) {
        this.map_normal = map_normal2;
    }

    public void setUploadFileData(Map<String, UploadFileVO> map_file2) {
        this.map_file = map_file2;
    }

    public void pushHeadPropertys(KeyValue kv) {
        if (kv.getKey() != null && !kv.getKey().equals(StringEx.Empty) && kv.getValue() != null && !kv.getValue().equals(StringEx.Empty)) {
            this.reqHeadPropertys.add(kv);
        }
    }

    public void disConnect() {
        if (this.connection != null) {
            this.connection.disconnect();
            this.connection = null;
        }
    }

    public static void setProxy(String iproxyHost, int iproxyPort) {
        proxyHost = iproxyHost;
        proxyPort = iproxyPort;
    }

    public static int getCurrentAPNType() {
        ConnectivityManager connectivityManager;
        NetworkInfo info;
        if (mainActivity == null || (connectivityManager = (ConnectivityManager) mainActivity.getSystemService("connectivity")) == null || (info = connectivityManager.getActiveNetworkInfo()) == null || !info.isAvailable()) {
            return -1;
        }
        return info.getType();
    }

    public boolean sendData(byte[] buffer, String url) {
        if (mainActivity != null && proxyHost == null) {
            ConnectivityManager connectivityManager = (ConnectivityManager) mainActivity.getSystemService("connectivity");
            if (connectivityManager == null) {
                return false;
            }
            NetworkInfo info = connectivityManager.getActiveNetworkInfo();
            if (info == null || !info.isAvailable()) {
                return false;
            }
            if (info.getType() != 1) {
                proxyHost = Proxy.getDefaultHost();
                proxyPort = Proxy.getDefaultPort();
            }
            if ((proxyHost == null || proxyHost.length() <= 0) && ("cmwap".equalsIgnoreCase(info.getExtraInfo()) || "3gwap".equalsIgnoreCase(info.getExtraInfo()) || "uniwap".equalsIgnoreCase(info.getExtraInfo()))) {
                proxyHost = "10.0.0.172";
                proxyPort = 80;
            }
            if ("cmnet".equalsIgnoreCase(info.getExtraInfo()) || "3gnet".equalsIgnoreCase(info.getExtraInfo()) || "uninet".equalsIgnoreCase(info.getExtraInfo())) {
                proxyHost = null;
                proxyPort = 0;
            }
        }
        try {
            URL postUrl = new URL(url);
            if (proxyHost == null || proxyHost.trim().length() <= 0) {
                this.connection = (HttpURLConnection) postUrl.openConnection();
            } else {
                this.connection = (HttpURLConnection) postUrl.openConnection(new java.net.Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost, proxyPort)));
            }
            this.connection.setConnectTimeout(this.connectTimeout);
            this.connection.setReadTimeout(this.readTimeout);
            this.connection.setUseCaches(false);
            this.connection.setDoOutput(true);
            this.connection.setDoInput(true);
            this.connection.setRequestProperty("Charset", ENCODING);
            this.connection.setRequestProperty("Accept-Encoding", "gzip, deflate");
            if (session != null && !session.equals(StringEx.Empty)) {
                this.connection.setRequestProperty("Cookie", session);
            }
            if (this.reqHeadPropertys.size() > 0) {
                int reqPs = this.reqHeadPropertys.size();
                for (int i = 0; i < reqPs; i++) {
                    KeyValue kv = this.reqHeadPropertys.get(i);
                    this.connection.setRequestProperty(kv.getKey(), kv.getValue());
                }
            }
            if (buffer != null) {
                this.connection.setRequestMethod("POST");
                this.connection.connect();
                this.connection.getOutputStream().write(buffer);
                this.connection.getOutputStream().flush();
                this.connection.getOutputStream().close();
            } else if (this.map_file != null && this.map_file.size() > 0) {
                this.connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + this.boundary);
                this.connection.setRequestMethod("POST");
                this.connection.connect();
                postByMultipart(this.connection.getOutputStream(), this.map_normal, this.map_file);
            } else if (this.map_normal == null || this.map_normal.size() <= 0) {
                this.connection.setRequestMethod("GET");
                this.connection.connect();
            } else {
                this.connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                this.connection.setRequestMethod("POST");
                this.connection.connect();
                postByFormUrlencoded(this.connection.getOutputStream(), this.map_normal);
            }
            return true;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            this.connection = null;
            return false;
        } catch (IOException e2) {
            e2.printStackTrace();
            this.connection = null;
            return false;
        }
    }

    public static void resetSession() {
        session = null;
    }

    public static String getSession() {
        return session;
    }

    public int httpResponseCode() {
        return this.httpResponseCode;
    }

    public byte[] receiveData() {
        BufferedInputStream inputStream;
        try {
            if (this.connection == null) {
                return null;
            }
            int rc = this.connection.getResponseCode();
            this.httpResponseCode = rc;
            if (rc == 200) {
                if (session == null || session.equals(StringEx.Empty)) {
                    String tmpsession = StringEx.Empty;
                    Map<String, List<String>> m = this.connection.getHeaderFields();
                    List<String> list = m.get("Set-Cookie");
                    if (list != null) {
                        for (String s : list) {
                            if (s.endsWith(";")) {
                                tmpsession = String.valueOf(tmpsession) + s;
                            } else {
                                tmpsession = String.valueOf(tmpsession) + s + ";";
                            }
                        }
                    }
                    if (!tmpsession.equals(StringEx.Empty)) {
                        session = tmpsession;
                    }
                    String tmpsession2 = StringEx.Empty;
                    List<String> list2 = m.get("set-cookie");
                    if (list2 != null) {
                        for (String s2 : list2) {
                            if (s2.endsWith(";")) {
                                tmpsession2 = String.valueOf(tmpsession2) + s2;
                            } else {
                                tmpsession2 = String.valueOf(tmpsession2) + s2 + ";";
                            }
                        }
                    }
                    if (!tmpsession2.equals(StringEx.Empty)) {
                        session = String.valueOf(session) + tmpsession2;
                    }
                }
                InputStream is = this.connection.getInputStream();
                String encoding = this.connection.getContentEncoding();
                if (encoding == null || !encoding.equals("gzip")) {
                    inputStream = new BufferedInputStream(is, 8192);
                } else {
                    inputStream = new BufferedInputStream(new GZIPInputStream(is), 8192);
                }
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                byte[] buffer = new byte[8192];
                for (int bufsize = inputStream.read(buffer); bufsize != -1; bufsize = inputStream.read(buffer)) {
                    bos.write(buffer, 0, bufsize);
                }
                byte[] buffer2 = bos.toByteArray();
                bos.close();
                inputStream.close();
                if (is != null) {
                    is.close();
                }
                this.connection.disconnect();
                this.connection = null;
                return buffer2;
            }
            Log.i("NetSender>receiveData>ResponseCode：", new StringBuilder(String.valueOf(rc)).toString());
            return null;
        } catch (IOException e) {
            Log.i("NetSender>receiveData> IOException:", "message:" + e.getMessage());
        }
    }

    private void postByFormUrlencoded(OutputStream out, Map<String, String> map_normal2) throws IOException {
        StringBuffer sb = new StringBuffer();
        for (String key : map_normal2.keySet()) {
            sb.append("&");
            sb.append(key);
            sb.append("=");
            sb.append(URLEncoder.encode(map_normal2.get(key).toString(), ENCODING));
        }
        byte[] b = sb.substring(1).getBytes();
        out.write(b, 0, b.length);
        out.flush();
        out.close();
    }

    private void postByMultipart(OutputStream out, Map<String, String> map_normal2, Map<String, UploadFileVO> map_file2) throws UnsupportedEncodingException, IOException {
        if (map_normal2 != null && map_normal2.size() > 0) {
            StringBuilder sb = new StringBuilder();
            for (String key : map_normal2.keySet()) {
                sb.append("--");
                sb.append(this.boundary);
                sb.append("\r\nContent-Disposition: form-data; name=\"");
                sb.append(key);
                sb.append("\"\r\n\r\n");
                sb.append(map_normal2.get(key));
                sb.append(PNXConfigConstant.RESP_SPLIT_2);
            }
            out.write(sb.toString().getBytes(ENCODING));
            out.flush();
        }
        byte[] buffer = new byte[1024];
        for (String key2 : map_file2.keySet()) {
            UploadFileVO vo = map_file2.get(key2);
            out.write(("--" + this.boundary + "\r\nContent-Disposition: form-data; name=\"" + key2 + "\"; filename=\"" + vo.getFileName() + "\"\r\nContent-Type: " + vo.getContentType() + "\r\n\r\n").getBytes(ENCODING));
            InputStream inputStream = vo.getInputStream();
            if (inputStream != null) {
                while (true) {
                    int len = inputStream.read(buffer);
                    if (len == -1) {
                        break;
                    }
                    out.write(buffer, 0, len);
                }
                inputStream.close();
            }
            out.write(PNXConfigConstant.RESP_SPLIT_2.getBytes(ENCODING));
            out.flush();
        }
        out.write(("--" + this.boundary + "--\r\n").getBytes(ENCODING));
        out.flush();
        out.close();
    }
}
