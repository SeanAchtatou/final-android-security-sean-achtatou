package com.andframework.network;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class UploadFileVO {
    private String contentType;
    private String fileName;
    private InputStream inputStream;

    public UploadFileVO() {
        this.contentType = "application/octet-stream";
    }

    public UploadFileVO(String filePath) {
        this(new File(filePath));
    }

    public UploadFileVO(File file) {
        this.contentType = "application/octet-stream";
        if (file != null && file.exists()) {
            try {
                this.inputStream = new FileInputStream(file);
                this.fileName = file.getName();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public UploadFileVO(String fileName2, InputStream inputStream2) {
        this.contentType = "application/octet-stream";
        this.fileName = fileName2;
        this.inputStream = inputStream2;
    }

    public UploadFileVO(String fileName2, String contentType2, InputStream inputStream2) {
        this.contentType = "application/octet-stream";
        this.fileName = fileName2;
        this.contentType = contentType2;
        this.inputStream = inputStream2;
    }

    public String getFileName() {
        return this.fileName;
    }

    public void setFileName(String fileName2) {
        this.fileName = fileName2;
    }

    public String getContentType() {
        return this.contentType;
    }

    public void setContentType(String contentType2) {
        this.contentType = contentType2;
    }

    public InputStream getInputStream() {
        return this.inputStream;
    }

    public void setInputStream(InputStream inputStream2) {
        this.inputStream = inputStream2;
    }
}
