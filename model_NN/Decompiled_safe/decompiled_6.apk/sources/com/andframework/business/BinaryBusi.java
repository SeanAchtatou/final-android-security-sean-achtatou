package com.andframework.business;

import com.andframework.myinterface.UiCallBack;
import com.andframework.parse.BinaryParse;

public class BinaryBusi extends BaseBusi {
    public BinaryBusi(UiCallBack bCallBack) {
        super(bCallBack, BinaryParse.class);
    }

    public void setReqUrl(String url) {
        this.domain = url;
    }

    /* access modifiers changed from: protected */
    public void prepare() {
    }
}
