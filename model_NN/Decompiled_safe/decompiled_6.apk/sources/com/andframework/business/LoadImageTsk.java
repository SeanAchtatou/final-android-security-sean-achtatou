package com.andframework.business;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;
import com.andframework.common.ImageDownLoadListener;
import com.andframework.network.IHttpConnect;
import com.andframework.ui.CustomImageView;
import com.andframework.util.Util;
import com.andframework.zmfile.ZMFile;
import com.andframework.zmfile.ZMFilePath;
import com.jianq.misc.StringEx;

public class LoadImageTsk extends AsyncTask<String, String, Bitmap> {
    private ImageDownLoadListener bitmaplistener;
    private ImageView civ;
    private FinishedListner flistner;
    private IHttpConnect iCon;
    public String picUrl;
    public long timestmp;

    public void setImageView(CustomImageView civ2) {
        this.civ = civ2;
    }

    public void setImageDownLoadListener(ImageDownLoadListener bitmaplistener2) {
        this.bitmaplistener = bitmaplistener2;
    }

    public void iExcute() {
        if (this.picUrl != null) {
            execute(this.picUrl);
        }
    }

    public void setFinishedListner(FinishedListner fl) {
        this.flistner = fl;
    }

    /* access modifiers changed from: protected */
    public Bitmap doInBackground(String... params) {
        String url = params[0];
        Log.i("doInBackground >", String.valueOf(url) + "'");
        String picname = Util.urlToFileName(url);
        ZMFilePath fp = new ZMFilePath((byte) 0, true);
        fp.pushPathNode("tmppic");
        fp.addFileName(picname);
        byte[] tmpPic = ZMFile.readAll(fp.toString());
        if (tmpPic == null || tmpPic.length <= 0) {
            this.iCon = new IHttpConnect();
            this.iCon.sendData(null, url);
            byte[] result = this.iCon.receiveData();
            Bitmap bp = null;
            if (result != null && result.length > 0) {
                Log.i("file>", fp.toString());
                ZMFile.delFile(fp.toString());
                ZMFile.write(fp.toString(), result, 0, result.length, 0);
                BitmapFactory.Options opts = new BitmapFactory.Options();
                opts.inScaled = false;
                bp = BitmapFactory.decodeByteArray(result, 0, result.length, opts);
            }
            return bp;
        }
        Log.i("file exit>", String.valueOf(tmpPic.length) + "'");
        BitmapFactory.Options opts2 = new BitmapFactory.Options();
        opts2.inScaled = false;
        Bitmap bp2 = BitmapFactory.decodeByteArray(tmpPic, 0, tmpPic.length, opts2);
        if (bp2 != null) {
            Log.i("LoadImageTsk>doInBackground>local w-h", String.valueOf(bp2.getWidth()) + "-" + bp2.getHeight());
        }
        return bp2;
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        if (this.iCon != null) {
            this.iCon.disConnect();
        }
        super.onCancelled();
        this.flistner.onCancle(this);
        Log.i("onCancelled>", StringEx.Empty);
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Bitmap object) {
        if (this.bitmaplistener != null) {
            this.bitmaplistener.OnImageDownFinish(object, this.picUrl);
        }
        if (object != null) {
            Log.i("onPostExecute >", "pic loadOk");
            if (this.civ != null) {
                this.civ.setImageBitmap(object);
            }
        }
        if (this.flistner != null) {
            this.flistner.onFinished(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(String... values) {
    }
}
