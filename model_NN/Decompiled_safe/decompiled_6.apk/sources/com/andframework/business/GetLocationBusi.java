package com.andframework.business;

import android.app.Activity;
import android.telephony.CellLocation;
import android.telephony.NeighboringCellInfo;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import com.andframework.myinterface.UiCallBack;
import com.andframework.parse.GetLocaionParse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GetLocationBusi extends BaseBusi {
    Activity act;

    public GetLocationBusi(UiCallBack bCallBack) {
        super(bCallBack, GetLocaionParse.class);
    }

    public void setActivity(Activity act2) {
        this.act = act2;
    }

    public boolean isCanExecute() {
        if (this.act == null) {
            Log.i("iExecute", "activity is null");
            return false;
        }
        TelephonyManager telMan = (TelephonyManager) this.act.getSystemService("phone");
        String operator = telMan.getNetworkOperator();
        CellLocation cl = telMan.getCellLocation();
        if (cl instanceof GsmCellLocation) {
            int cid = ((GsmCellLocation) cl).getCid();
            int lac = ((GsmCellLocation) cl).getLac();
            String substring = operator.substring(0, 3);
            String substring2 = operator.substring(3);
        } else if (cl instanceof CdmaCellLocation) {
            int baseStationId = ((CdmaCellLocation) cl).getBaseStationId();
            int networkId = ((CdmaCellLocation) cl).getNetworkId();
            String substring3 = operator.substring(0, 3);
            String valueOf = String.valueOf(((CdmaCellLocation) cl).getSystemId());
        } else {
            Log.i("iExecute", "CellLocation is unknow type");
            return false;
        }
        return true;
    }

    public void iExecute() {
        int cid;
        int lac;
        String mcc;
        String mnc;
        JSONObject object;
        try {
            if (this.act == null) {
                Log.i("iExecute", "activity is null");
                return;
            }
            TelephonyManager telMan = (TelephonyManager) this.act.getSystemService("phone");
            String operator = telMan.getNetworkOperator();
            boolean isCdma = false;
            CellLocation cl = telMan.getCellLocation();
            if (cl instanceof GsmCellLocation) {
                cid = ((GsmCellLocation) cl).getCid();
                lac = ((GsmCellLocation) cl).getLac();
                mcc = operator.substring(0, 3);
                mnc = operator.substring(3);
            } else if (cl instanceof CdmaCellLocation) {
                cid = ((CdmaCellLocation) cl).getBaseStationId();
                lac = ((CdmaCellLocation) cl).getNetworkId();
                mcc = operator.substring(0, 3);
                mnc = String.valueOf(((CdmaCellLocation) cl).getSystemId());
                isCdma = true;
            } else {
                Log.i("iExecute", "CellLocation is unknow type");
                return;
            }
            JSONObject tower = new JSONObject();
            try {
                tower.put("cell_id", cid);
                tower.put("location_area_code", lac);
                tower.put("mobile_country_code", mcc);
                tower.put("mobile_network_code", mnc);
            } catch (JSONException e) {
                Log.e("requestNetworkLocation", "JSONObject put failed", e);
            }
            JSONArray jarray = new JSONArray();
            jarray.put(tower);
            for (NeighboringCellInfo nci : telMan.getNeighboringCellInfo()) {
                JSONObject tmpTower = new JSONObject();
                try {
                    tmpTower.put("cell_id", nci.getCid());
                    tmpTower.put("location_area_code", nci.getLac());
                    tmpTower.put("mobile_country_code", mcc);
                    tmpTower.put("mobile_network_code", mnc);
                } catch (JSONException e2) {
                    Log.e("JSONException", "JSONObject put failed");
                }
                jarray.put(tmpTower);
            }
            if (!isCdma) {
                object = createJSONObject("cell_towers", jarray);
            } else {
                object = createCDMAJSONObject("cell_towers", jarray, mcc, mnc);
            }
            this.buffer = object.toString().getBytes();
            this.domain = "http://www.google.com/loc/json";
            super.iExecute();
        } catch (Exception e3) {
            Log.d("Exception", "request GsmCellLocation failed,reason:" + e3.getMessage());
        }
    }

    private JSONObject createJSONObject(String arrayName, JSONArray array) {
        JSONObject object = new JSONObject();
        try {
            object.put("version", "1.1.0");
            object.put("host", "maps.google.com");
            object.put(arrayName, array);
        } catch (JSONException e) {
            Log.e("JSONException", "JSONObject put failed");
        }
        return object;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    private JSONObject createCDMAJSONObject(String arrayName, JSONArray array, String mcc, String mnc) {
        JSONObject object = new JSONObject();
        try {
            object.put("version", "1.1.0");
            object.put("host", "maps.google.com");
            object.put("home_mobile_country_code", mcc);
            object.put("home_mobile_network_code", mnc);
            object.put("radio_type", "cdma");
            object.put("request_address", true);
            if ("460".equals(mcc)) {
                object.put("address_language", "zh_CN");
            } else {
                object.put("address_language", "en_US");
            }
            object.put(arrayName, array);
        } catch (JSONException e) {
            Log.e("createCDMAJSONObject>JSONException", "JSONObject put failed");
        }
        return object;
    }

    /* access modifiers changed from: protected */
    public void prepare() {
    }
}
