package com.andframework.business;

import com.andframework.myinterface.UiCallBack;
import com.andframework.parse.VersionParse;

public class VersionBusi extends BaseBusi {
    private String tail = "login/detectversion.action?";

    public VersionBusi(UiCallBack bCallBack) {
        super(bCallBack, VersionParse.class);
    }

    /* access modifiers changed from: protected */
    public void prepare() {
        this.reqParam = this.tail;
    }
}
