package com.andframework.business;

import com.andframework.network.IHttpConnect;
import java.util.Vector;

public class PicDownTaskManager extends Thread implements FinishedListner {
    private static PicDownTaskManager pdtm;
    private boolean isRunning = true;
    private Vector<LoadImageTsk> readyQueue = new Vector<>();
    private Vector<LoadImageTsk> runningQueue = new Vector<>();

    private PicDownTaskManager() {
    }

    public static PicDownTaskManager getInst() {
        if (pdtm == null) {
            pdtm = new PicDownTaskManager();
            pdtm.start();
        }
        return pdtm;
    }

    public void destroyPicDownTask() {
        pdtm = null;
        this.isRunning = false;
        this.runningQueue.removeAllElements();
        this.readyQueue.removeAllElements();
    }

    public void postTask(LoadImageTsk msg) {
        synchronized (this) {
            msg.timestmp = System.currentTimeMillis();
            msg.setFinishedListner(this);
            this.runningQueue.add(msg);
            if (this.readyQueue.size() == 0) {
                notify();
            }
        }
    }

    private int calcIndex() {
        int size = this.runningQueue.size();
        long temtimestamp = 0;
        int blank = 0;
        int returnindex = -1;
        for (int i = size - 1; i >= 0; i--) {
            LoadImageTsk lt = this.runningQueue.get(i);
            returnindex = i;
            if (i == size - 1) {
                temtimestamp = lt.timestmp;
            } else if (blank == 0) {
                blank = (int) (temtimestamp - lt.timestmp);
                temtimestamp = lt.timestmp;
                if (blank > 200) {
                    return returnindex + 1;
                }
            } else if (((long) (blank + 200)) < temtimestamp - lt.timestmp) {
                return returnindex + 1;
            } else {
                temtimestamp = lt.timestmp;
            }
        }
        return returnindex;
    }

    public void run() {
        while (this.isRunning) {
            synchronized (this) {
                int tsknum = 2;
                if (IHttpConnect.getCurrentAPNType() == 1) {
                    tsknum = 4;
                }
                if (this.readyQueue.size() > tsknum) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    int downindex = calcIndex();
                    if (downindex == -1) {
                        try {
                            wait();
                        } catch (InterruptedException e2) {
                            e2.printStackTrace();
                        }
                    } else {
                        LoadImageTsk lit = this.runningQueue.remove(downindex);
                        this.readyQueue.add(lit);
                        lit.iExcute();
                    }
                }
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException e3) {
                e3.printStackTrace();
            }
        }
    }

    public void onFinished(LoadImageTsk lt) {
        synchronized (this) {
            this.readyQueue.remove(lt);
            notify();
        }
    }

    public void onCancle(LoadImageTsk lt) {
        synchronized (this) {
            this.readyQueue.remove(lt);
            notify();
        }
    }
}
