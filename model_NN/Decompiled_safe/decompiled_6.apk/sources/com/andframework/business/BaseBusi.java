package com.andframework.business;

import android.os.AsyncTask;
import android.util.Log;
import com.andframework.common.KeyValue;
import com.andframework.config.FrameworkConfig;
import com.andframework.myinterface.UiCallBack;
import com.andframework.network.IHttpConnect;
import com.andframework.network.UploadFileVO;
import com.andframework.parse.BaseParse;
import com.jianq.misc.StringEx;
import java.util.ArrayList;
import java.util.Map;

public abstract class BaseBusi extends AsyncTask<String, String, BaseBusi> {
    protected BaseParse baseParse;
    protected UiCallBack bcb;
    public byte[] buffer;
    private int connectTimeout = 20000;
    protected String domain = FrameworkConfig.getInst().getDomain();
    private IHttpConnect iCon;
    private Map<String, UploadFileVO> map_file;
    private Map<String, String> map_normal;
    private int readTimeout = 20000;
    private ArrayList<KeyValue> reqHeadPropertys = new ArrayList<>();
    protected String reqParam = StringEx.Empty;

    /* access modifiers changed from: protected */
    public abstract void prepare();

    public BaseBusi(UiCallBack bCallBack, Class<?> cls) {
        this.bcb = bCallBack;
        if (cls == null) {
            Log.e("BaseBusi", "Class<?> is null");
            return;
        }
        try {
            Object obj = cls.newInstance();
            if (obj instanceof BaseParse) {
                this.baseParse = (BaseParse) obj;
            } else {
                Log.e("BaseBusi", "newInstance not extends BaseParse ");
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e2) {
            e2.printStackTrace();
        }
    }

    public void setConnectTimeout(int connectTimeout2) {
        this.connectTimeout = connectTimeout2;
    }

    public void setReadTimeout(int readTimeout2) {
        this.readTimeout = readTimeout2;
    }

    public void setFormData(Map<String, String> map_normal2) {
        this.map_normal = map_normal2;
    }

    public void setUploadFileData(Map<String, UploadFileVO> map_file2) {
        this.map_file = map_file2;
    }

    public void setHeadPropertys(ArrayList<KeyValue> kvs) {
        if (kvs != null && kvs.size() > 0) {
            this.reqHeadPropertys = kvs;
        }
    }

    public void pushHeadPropertys(KeyValue kv) {
        if (kv.getKey() != null && !kv.getKey().equals(StringEx.Empty) && kv.getValue() != null && !kv.getValue().equals(StringEx.Empty)) {
            this.reqHeadPropertys.add(kv);
        }
    }

    public BaseParse getBaseStruct() {
        return this.baseParse;
    }

    public void iCancle() {
        cancel(true);
        if (this.iCon != null) {
            this.iCon.disConnect();
        }
    }

    public void iExecute() {
        prepare();
        execute(String.valueOf(this.domain) + this.reqParam);
    }

    /* access modifiers changed from: protected */
    public BaseBusi doInBackground(String... params) {
        String url = params[0];
        Log.i("busi Req url=", url);
        this.iCon = new IHttpConnect();
        this.iCon.setFormData(this.map_normal);
        this.iCon.setUploadFileData(this.map_file);
        this.iCon.setConnectTimeout(this.connectTimeout);
        this.iCon.setReadTimeout(this.readTimeout);
        if (this.reqHeadPropertys.size() >= 0) {
            for (int i = 0; i < this.reqHeadPropertys.size(); i++) {
                this.iCon.pushHeadPropertys(this.reqHeadPropertys.get(i));
            }
        }
        this.iCon.sendData(this.buffer, url);
        byte[] result = this.iCon.receiveData();
        if (result != null) {
            Log.i(String.valueOf(url) + " busi rsp =", new String(result));
        } else {
            Log.i(String.valueOf(url) + " busi rsp =", "null");
        }
        if (this.baseParse != null) {
            this.baseParse.parse(result);
            this.baseParse.httpResponseCode = this.iCon.httpResponseCode();
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        super.onCancelled();
        Log.i("onCancelled>", StringEx.Empty);
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(BaseBusi object) {
        this.bcb.uiCallBack(object);
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(String... values) {
    }
}
