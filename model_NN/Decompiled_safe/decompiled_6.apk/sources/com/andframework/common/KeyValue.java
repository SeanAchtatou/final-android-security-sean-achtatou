package com.andframework.common;

public class KeyValue {
    private String key;
    private String value;

    public KeyValue() {
    }

    public KeyValue(String k, String v) {
        this.key = k;
        this.value = v;
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(String key2) {
        this.key = key2;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value2) {
        this.value = value2;
    }
}
