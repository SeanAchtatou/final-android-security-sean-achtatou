package com.andframework.common;

import android.graphics.Bitmap;

public interface ImageDownLoadListener {
    void OnImageDownFinish(Bitmap bitmap, String str);
}
