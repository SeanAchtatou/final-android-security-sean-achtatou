package com.andframework.common;

import java.util.HashMap;

public class ObjectStores {
    private static ObjectStores inst;
    private HashMap<String, Object> objectStores = new HashMap<>();

    private ObjectStores() {
    }

    public static ObjectStores getInst() {
        if (inst == null) {
            inst = new ObjectStores();
        }
        return inst;
    }

    public void putObject(String key, Object object) {
        this.objectStores.put(key, object);
    }

    public Object getObject(String key) {
        return this.objectStores.get(key);
    }

    public void removeObject(String key) {
        this.objectStores.remove(key);
    }
}
