package com.baosteelOnline.dl;

import com.andframework.parse.BaseParse;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class FileParse extends BaseParse {
    public String commonData;
    public String fileName;
    public String msg;

    public boolean parse(byte[] data) {
        this.commonData = new String(data);
        try {
            JSONObject json = (JSONObject) new JSONTokener(this.commonData).nextValue();
            this.msg = json.getString("msg");
            this.fileName = json.getString("fileName");
            return false;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean parse2(String data) {
        this.commonData = data;
        try {
            JSONObject json = (JSONObject) new JSONTokener(this.commonData).nextValue();
            this.msg = json.getString("msg");
            this.fileName = json.getString("fileName");
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
