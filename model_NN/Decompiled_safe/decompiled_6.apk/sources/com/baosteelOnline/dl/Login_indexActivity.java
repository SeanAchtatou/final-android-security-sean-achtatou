package com.baosteelOnline.dl;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.andframework.business.BaseBusi;
import com.andframework.common.ObjectStores;
import com.andframework.myinterface.UiCallBack;
import com.andframework.ui.CustomMessageShow;
import com.baosteelOnline.R;
import com.baosteelOnline.common.DataBaseFactory;
import com.baosteelOnline.common.DataXml;
import com.baosteelOnline.common.SysConfig;
import com.baosteelOnline.common.SysOsInfo;
import com.baosteelOnline.common.UploadUtil;
import com.baosteelOnline.common.ZipUtils;
import com.baosteelOnline.data.LoginBusi;
import com.baosteelOnline.data.LoginBusi_getCus;
import com.baosteelOnline.data.Msg_registerEquipmentInfoBusi;
import com.baosteelOnline.data.RequestBidData;
import com.baosteelOnline.data_parse.ContractParse;
import com.baosteelOnline.data_parse.LoginParse;
import com.baosteelOnline.data_parse.NoticeParse;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.model.sysConfigModel;
import com.baosteelOnline.model.sysOsInfoModel;
import com.baosteelOnline.service.MsgService;
import com.baosteelOnline.sql.DBHelper;
import com.baosteelOnline.xhjy.ChooseActivity;
import com.baosteelOnline.xhjy.ContractListActivity;
import com.baosteelOnline.xhjy.CyclicGoodsBidActivity;
import com.baosteelOnline.xhjy.CyclicGoodsBidHallPageActivity;
import com.baosteelOnline.xhjy.CyclicGoodsBidPageActivity;
import com.baosteelOnline.xhjy.CyclicGoodsBidSession;
import com.baosteelOnline.xhjy.CyclicGoodsBidSessionSelect;
import com.baosteelOnline.xhjy.CyclicGoodsConfirmArrivalActivity;
import com.baosteelOnline.xhjy.CyclicGoodsIndexActivity;
import com.baosteelOnline.xhjy.CyclicGoodsJingJiaJieGuo;
import com.baosteelOnline.xhjy.GCJYBidPageActivity;
import com.baosteelOnline.xhjy.SearchItemInfoActivity;
import com.baosteelOnline.xhjy.ShopActivity;
import com.baosteelOnline.xhjy.Xhjy_indexActivity;
import com.baosteelOnline.xhjy.Xhjy_more;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Login_indexActivity extends Activity implements UiCallBack, RadioGroup.OnCheckedChangeListener {
    /* access modifiers changed from: private */
    public String BSOL = StringEx.Empty;
    private String BSP_companyCode = StringEx.Empty;
    private String G_User = StringEx.Empty;
    private String GfullName = StringEx.Empty;
    /* access modifiers changed from: private */
    public String XH = StringEx.Empty;
    private String XH_msg = StringEx.Empty;
    private String XH_reStr = StringEx.Empty;
    private RelativeLayout bottom_nva1;
    private RelativeLayout bottom_nva2;
    /* access modifiers changed from: private */
    public String bspCompanyCode = StringEx.Empty;
    CheckBox checkUsername;
    /* access modifiers changed from: private */
    public String chineseFullname = StringEx.Empty;
    /* access modifiers changed from: private */
    public String clientBeginTimeMillis = StringEx.Empty;
    /* access modifiers changed from: private */
    public String clientEndTimeMillis = StringEx.Empty;
    private String companyCode = StringEx.Empty;
    private String companyFullName = StringEx.Empty;
    public ContractParse contractParse;
    /* access modifiers changed from: private */
    public String czy = StringEx.Empty;
    /* access modifiers changed from: private */
    public String czyname = StringEx.Empty;
    /* access modifiers changed from: private */
    public String czyrealname = StringEx.Empty;
    private DataBaseFactory db;
    private DBHelper dbHelper;
    private SharedPreferences.Editor editor;
    /* access modifiers changed from: private */
    public String errorCode = StringEx.Empty;
    /* access modifiers changed from: private */
    public String exceptionClass = StringEx.Empty;
    /* access modifiers changed from: private */
    public String exceptionMessage = StringEx.Empty;
    private File file_login;
    private File file_operation;
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    new FileUploadLoginTask().execute(new Integer[0]);
                    return;
                default:
                    return;
            }
        }
    };
    private JQUICallBack httpCallBack = new JQUICallBack() {
        public void callBack(ResultData result) {
            Login_indexActivity.this.progressDialog.dismiss();
            Log.d("获取的数据：", result.getStringData());
            String dataString = result.getStringData();
            if (!dataString.equals(null) && !dataString.equals(StringEx.Empty)) {
                Login_indexActivity.this.contractParse = ContractParse.parse(result.getStringData());
                if (ContractParse.getDataString().equals(null) || ContractParse.getDataString().equals(StringEx.Empty)) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(Login_indexActivity.this);
                    dialog.setCancelable(false);
                    dialog.setMessage("登录失败！").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
                    return;
                }
                try {
                    Log.d("获取的ROWs数据：", ContractParse.jjo.toString());
                } catch (Exception e) {
                }
                if (ContractParse.getDataString().equals(null) || ContractParse.getDataString().equals(StringEx.Empty)) {
                    AlertDialog.Builder dialog2 = new AlertDialog.Builder(Login_indexActivity.this);
                    dialog2.setCancelable(false);
                    dialog2.setMessage("登录失败！").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
                    return;
                }
                for (int i = 0; i < ContractParse.jjo.length(); i++) {
                    try {
                        JSONArray row = new JSONArray(ContractParse.jjo.getString(i));
                        Login_indexActivity.this.czy = row.getString(0);
                        Login_indexActivity.this.czyname = row.getString(1);
                        Login_indexActivity.this.czyrealname = row.getString(2);
                        Login_indexActivity.this.hy = row.getString(3);
                        Login_indexActivity.this.hyname = row.getString(4);
                        Log.d("ROW数据：", "czy = " + Login_indexActivity.this.czy + "  czyname = " + Login_indexActivity.this.czyname + " czyrealname = " + Login_indexActivity.this.czyrealname + " hy = " + Login_indexActivity.this.hy + " hyname = " + Login_indexActivity.this.hyname);
                    } catch (JSONException e2) {
                        e2.printStackTrace();
                    }
                    Login_indexActivity.this.setGlobalAll();
                    Login_indexActivity.this.run.run();
                    String uesrName = Login_indexActivity.this.czy.toLowerCase();
                    String kaiqituisong = Login_indexActivity.this.mMessageServerSharedPreferences.getString("kaiqituisong", "NO");
                    if (!uesrName.equals(Login_indexActivity.this.mMessageServerSharedPreferences.getString("UserName", StringEx.Empty))) {
                        Login_indexActivity.this.mMessageServerEditor.putString("UserName", uesrName);
                        Log.d("登录保存的用户名：", uesrName);
                        Login_indexActivity.this.mMessageServerEditor.commit();
                        Login_indexActivity.this.showPushDialog();
                    } else {
                        if (kaiqituisong.equals("YES")) {
                            Login_indexActivity.this.registerDevice();
                        } else {
                            Login_indexActivity.this.stopService(new Intent(Login_indexActivity.this, MsgService.class));
                        }
                        Login_indexActivity.this.jumpto();
                    }
                }
            }
        }
    };
    private JQUICallBack httpCallBackForLogin1 = new JQUICallBack() {
        public void callBack(ResultData result) {
            Login_indexActivity.this.progressDialog.dismiss();
            String dataString = result.getStringData();
            if (!dataString.equals(null) && !dataString.equals(StringEx.Empty)) {
                Login_indexActivity.this.contractParse = ContractParse.parse(result.getStringData());
                Log.d("解密的数据：", ContractParse.getDecryptData());
                if (ContractParse.getDataString().equals(null) || ContractParse.getDataString().equals(StringEx.Empty)) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(Login_indexActivity.this);
                    dialog.setCancelable(false);
                    dialog.setMessage("登录失败！").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
                    return;
                }
                try {
                    JSONObject jsonObject = new JSONObject(ContractParse.getDataString());
                    if (jsonObject.has("bspCompanyCode")) {
                        Login_indexActivity.this.bspCompanyCode = jsonObject.getString("bspCompanyCode");
                    }
                    if (jsonObject.has("chineseFullname")) {
                        Login_indexActivity.this.chineseFullname = jsonObject.getString("chineseFullname");
                    }
                    if (jsonObject.has("isSuccess")) {
                        Login_indexActivity.this.isSuccess = jsonObject.getString("isSuccess");
                    }
                    if (jsonObject.has("loginNickName")) {
                        Login_indexActivity.this.loginNickName = jsonObject.getString("loginNickName");
                    }
                    if (jsonObject.has("resultDesc")) {
                        Login_indexActivity.this.resultDesc = jsonObject.getString("resultDesc");
                        Log.d("登录提示：", "resultDesc:" + Login_indexActivity.this.resultDesc);
                    }
                    if (jsonObject.has("srPageModel")) {
                        Login_indexActivity.this.srPageModel = jsonObject.getString("srPageModel");
                    }
                    if (jsonObject.has("srSysProcessModel")) {
                        Login_indexActivity.this.srSysProcessModel = jsonObject.getString("srSysProcessModel");
                    }
                    if (jsonObject.has("openAppClusters")) {
                        Login_indexActivity.this.openAppClusters = jsonObject.getString("openAppClusters");
                    }
                    JSONObject jsonObjectopenAppClusters = new JSONObject(Login_indexActivity.this.openAppClusters);
                    if (jsonObjectopenAppClusters.has("XH")) {
                        Login_indexActivity.this.XH = new StringBuilder(String.valueOf(jsonObjectopenAppClusters.getInt("XH"))).toString();
                    }
                    if (jsonObjectopenAppClusters.has("BSOL")) {
                        Login_indexActivity.this.BSOL = new StringBuilder(String.valueOf(jsonObjectopenAppClusters.getInt("BSOL"))).toString();
                        if (Login_indexActivity.this.BSOL.equals(null) || Login_indexActivity.this.BSOL.equals(StringEx.Empty)) {
                            Login_indexActivity.this.BSOL = StringEx.Empty;
                        } else {
                            Login_indexActivity.this.BSOL = "1";
                            Login_indexActivity.this.reStr = "1";
                        }
                    }
                    JSONObject jsonObjectsrSysProcessModel = new JSONObject(Login_indexActivity.this.srSysProcessModel);
                    if (jsonObjectsrSysProcessModel.has("clientBeginTimeMillis")) {
                        Login_indexActivity.this.clientBeginTimeMillis = jsonObjectsrSysProcessModel.getString("clientBeginTimeMillis");
                    }
                    if (jsonObjectsrSysProcessModel.has("clientEndTimeMillis")) {
                        Login_indexActivity.this.clientEndTimeMillis = jsonObjectsrSysProcessModel.getString("clientEndTimeMillis");
                    }
                    if (jsonObjectsrSysProcessModel.has("methodName")) {
                        Login_indexActivity.this.methodName = jsonObjectsrSysProcessModel.getString("methodName");
                    }
                    if (jsonObjectsrSysProcessModel.has("serverBeginTimeMillis")) {
                        Login_indexActivity.this.serverBeginTimeMillis = jsonObjectsrSysProcessModel.getString("serverBeginTimeMillis");
                    }
                    if (jsonObjectsrSysProcessModel.has("serverEndTimeMillis")) {
                        Login_indexActivity.this.serverEndTimeMillis = jsonObjectsrSysProcessModel.getString("serverEndTimeMillis");
                    }
                    if (jsonObjectsrSysProcessModel.has("serviceClassName")) {
                        Login_indexActivity.this.serviceClassName = jsonObjectsrSysProcessModel.getString("serviceClassName");
                    }
                    if (jsonObjectsrSysProcessModel.has("serviceName")) {
                        Login_indexActivity.this.serviceName = jsonObjectsrSysProcessModel.getString("serviceName");
                    }
                    if (jsonObjectsrSysProcessModel.has("serviceProcessId")) {
                        Login_indexActivity.this.serviceProcessId = jsonObjectsrSysProcessModel.getString("serviceProcessId");
                    }
                    if (jsonObject.has("srUserProcessModel")) {
                        Login_indexActivity.this.srUserProcessModel = jsonObject.getString("srUserProcessModel");
                    }
                    JSONObject jsonObjectsrUserProcessModel = new JSONObject(Login_indexActivity.this.srUserProcessModel);
                    if (jsonObjectsrUserProcessModel.has("errorCode")) {
                        Login_indexActivity.this.errorCode = jsonObjectsrUserProcessModel.getString("errorCode");
                    }
                    if (jsonObjectsrUserProcessModel.has("exceptionClass")) {
                        Login_indexActivity.this.exceptionClass = jsonObjectsrUserProcessModel.getString("exceptionClass");
                    }
                    if (jsonObjectsrUserProcessModel.has("exceptionMessage")) {
                        Login_indexActivity.this.exceptionMessage = jsonObjectsrUserProcessModel.getString("exceptionMessage");
                    }
                    if (jsonObjectsrUserProcessModel.has("resultChineseDesc")) {
                        Login_indexActivity.this.resultChineseDesc = jsonObjectsrUserProcessModel.getString("resultChineseDesc");
                    }
                    if (jsonObjectsrUserProcessModel.has("resultCode")) {
                        Login_indexActivity.this.resultCode = jsonObjectsrUserProcessModel.getString("resultCode");
                    }
                    if (jsonObjectsrUserProcessModel.has("resultDesc")) {
                        Login_indexActivity.this.resultDescofsrUserProcessModel = jsonObjectsrUserProcessModel.getString("resultDesc");
                    }
                    if (jsonObjectsrUserProcessModel.has("resultDescBundle")) {
                        Login_indexActivity.this.resultDescBundle = jsonObjectsrUserProcessModel.getString("resultDescBundle");
                    }
                    if (jsonObjectsrUserProcessModel.has("userBeginTimeMillis")) {
                        Login_indexActivity.this.userBeginTimeMillis = jsonObjectsrUserProcessModel.getString("userBeginTimeMillis");
                    }
                    if (jsonObjectsrUserProcessModel.has("userEndTimeMillis")) {
                        Login_indexActivity.this.userEndTimeMillis = jsonObjectsrUserProcessModel.getString("userEndTimeMillis");
                    }
                    if (jsonObjectsrUserProcessModel.has("userTimeSepType")) {
                        Login_indexActivity.this.userTimeSepType = jsonObjectsrUserProcessModel.getString("userTimeSepType");
                    }
                    if (jsonObject.has("umcLoginNo")) {
                        Login_indexActivity.this.umcLoginNo = jsonObject.getString("umcLoginNo");
                    }
                    if (jsonObject.has("userName")) {
                        Login_indexActivity.this.userName = jsonObject.getString("userName");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (!Login_indexActivity.this.resultDesc.equals("登陆成功!")) {
                    if (Login_indexActivity.this.resultDesc.equals(StringEx.Empty) || Login_indexActivity.this.resultDesc.equals("null") || Login_indexActivity.this.resultDesc.equals(null)) {
                        Login_indexActivity.this.resultDesc = "登录失败！";
                    }
                    AlertDialog.Builder dialog2 = new AlertDialog.Builder(Login_indexActivity.this);
                    dialog2.setCancelable(false);
                    dialog2.setMessage(Login_indexActivity.this.resultDesc).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
                } else if (Login_indexActivity.this.BSOL.equals(StringEx.Empty) || Login_indexActivity.this.BSOL.equals(null)) {
                    AlertDialog.Builder dialog3 = new AlertDialog.Builder(Login_indexActivity.this);
                    dialog3.setCancelable(false);
                    dialog3.setMessage("您正使用的并非本系统账号，请确认！").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
                } else {
                    Login_indexActivity.this.getLoginData();
                }
            }
        }
    };
    private JQUICallBack httpCallBackRes = new JQUICallBack() {
        public void callBack(ResultData result) {
            Login_indexActivity.this.progressDialog.dismiss();
            String dataString = result.getStringData();
            if (!dataString.equals(null) && !dataString.equals(StringEx.Empty)) {
                Login_indexActivity.this.contractParse = ContractParse.parse(result.getStringData());
                if (!ContractParse.getDataString().equals(null) && !ContractParse.getDataString().equals(StringEx.Empty) && !ContractParse.commonData.msg.isEmpty()) {
                    Log.d("获取的注册推送数据：", ContractParse.getDataString());
                    Log.d("获取的注册推送信息：", ContractParse.commonData.msg);
                    String pushMessage = ContractParse.commonData.msg;
                    if (pushMessage.contains("成功") || pushMessage.equals("该设备已有相关信息存在了")) {
                        Login_indexActivity.this.startService(new Intent(Login_indexActivity.this, MsgService.class));
                    }
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public String hy = StringEx.Empty;
    /* access modifiers changed from: private */
    public String hyname = StringEx.Empty;
    private String index_more = StringEx.Empty;
    private String inputPassword = StringEx.Empty;
    private String inputUserName = StringEx.Empty;
    private boolean isServerRunning = false;
    /* access modifiers changed from: private */
    public String isSuccess = StringEx.Empty;
    /* access modifiers changed from: private */
    public String loginNickName = StringEx.Empty;
    private RadioButton mIndexNavigation1;
    private RadioButton mIndexNavigation2;
    private RadioButton mIndexNavigation3;
    private RadioButton mIndexNavigation4;
    private RadioButton mIndexNavigation5;
    /* access modifiers changed from: private */
    public SharedPreferences.Editor mMessageServerEditor;
    /* access modifiers changed from: private */
    public SharedPreferences mMessageServerSharedPreferences;
    private RadioGroup mRadioderGroup;
    /* access modifiers changed from: private */
    public String mUserName = StringEx.Empty;
    /* access modifiers changed from: private */
    public String methodName = StringEx.Empty;
    /* access modifiers changed from: private */
    public String openAppClusters = StringEx.Empty;
    private String parameter_userid = StringEx.Empty;
    private String parameter_usertokenid = StringEx.Empty;
    private String part = StringEx.Empty;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    EditText pwdText;
    private RadioGroup radioderGroup;
    /* access modifiers changed from: private */
    public String reStr = StringEx.Empty;
    /* access modifiers changed from: private */
    public String result;
    /* access modifiers changed from: private */
    public String resultChineseDesc = StringEx.Empty;
    /* access modifiers changed from: private */
    public String resultCode = StringEx.Empty;
    /* access modifiers changed from: private */
    public String resultDesc = StringEx.Empty;
    private String resultDescArgs = StringEx.Empty;
    /* access modifiers changed from: private */
    public String resultDescBundle = StringEx.Empty;
    /* access modifiers changed from: private */
    public String resultDescofsrUserProcessModel = StringEx.Empty;
    /* access modifiers changed from: private */
    public Runnable run;
    /* access modifiers changed from: private */
    public String serverBeginTimeMillis = StringEx.Empty;
    /* access modifiers changed from: private */
    public String serverEndTimeMillis = StringEx.Empty;
    /* access modifiers changed from: private */
    public String serviceClassName = StringEx.Empty;
    /* access modifiers changed from: private */
    public String serviceName = StringEx.Empty;
    /* access modifiers changed from: private */
    public String serviceProcessId = StringEx.Empty;
    private SharedPreferences sp;
    /* access modifiers changed from: private */
    public String srPageModel = StringEx.Empty;
    /* access modifiers changed from: private */
    public String srSysProcessModel = StringEx.Empty;
    /* access modifiers changed from: private */
    public String srUserProcessModel = StringEx.Empty;
    /* access modifiers changed from: private */
    public String umcLoginNo = StringEx.Empty;
    /* access modifiers changed from: private */
    public String userBeginTimeMillis = StringEx.Empty;
    /* access modifiers changed from: private */
    public String userEndTimeMillis = StringEx.Empty;
    /* access modifiers changed from: private */
    public String userName = StringEx.Empty;
    EditText userNameText;
    /* access modifiers changed from: private */
    public String userTimeSepType = StringEx.Empty;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        ExitApplication.getInstance().addActivity(this);
        setContentView((int) R.layout.login);
        this.dbHelper = DataBaseFactory.getInstance(this);
        this.mMessageServerSharedPreferences = getSharedPreferences("MessageServer", 0);
        this.mMessageServerEditor = this.mMessageServerSharedPreferences.edit();
        this.isServerRunning = isServiceRunning(this, MsgService.class.getName());
        this.bottom_nva1 = (RelativeLayout) findViewById(R.id.bottom_nva1);
        this.bottom_nva2 = (RelativeLayout) findViewById(R.id.bottom_nva2);
        this.sp = getSharedPreferences("BaoIndex", 0);
        this.editor = this.sp.edit();
        this.mRadioderGroup = (RadioGroup) findViewById(R.id.cyclic_goods_index_RGroup);
        this.mIndexNavigation1 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item1);
        this.mIndexNavigation2 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item2);
        this.mIndexNavigation3 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item3);
        this.mIndexNavigation4 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item4);
        this.mIndexNavigation5 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item5);
        this.mRadioderGroup.setOnCheckedChangeListener(this);
        if (this.sp.getString("bIndex", StringEx.Empty).equals("xhwz")) {
            this.bottom_nva1.setVisibility(8);
            this.bottom_nva2.setVisibility(0);
            this.mIndexNavigation1.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    ExitApplication.getInstance().startActivity(Login_indexActivity.this, CyclicGoodsIndexActivity.class);
                    Login_indexActivity.this.finish();
                }
            });
        } else {
            this.bottom_nva2.setVisibility(8);
            this.bottom_nva1.setVisibility(0);
        }
        ((TextView) findViewById(R.id.titletextview)).setText((int) R.string.logintitle);
        this.userNameText = (EditText) findViewById(R.id.bankingYourNameEditText);
        this.pwdText = (EditText) findViewById(R.id.bankingContactTelEditText);
        this.checkUsername = (CheckBox) findViewById(R.id.checkbox);
        ((RadioButton) findViewById(R.id.radio_home3)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ExitApplication.getInstance().startActivity(Login_indexActivity.this, Xhjy_indexActivity.class);
                Login_indexActivity.this.finish();
            }
        });
        this.radioderGroup = (RadioGroup) findViewById(R.id.xhjy_index_RGroup);
        this.radioderGroup.setOnCheckedChangeListener(this);
        SharedPreferences sp2 = getSharedPreferences("login_username", 2);
        System.out.println("username--------->" + sp2.getString("userName", StringEx.Empty));
        this.userNameText.setText(sp2.getString("userName", StringEx.Empty));
        this.run = new Runnable() {
            public void run() {
                Message message = new Message();
                message.what = 1;
                Login_indexActivity.this.handler.sendMessage(message);
            }
        };
    }

    public void backButtonAction(View v) {
        jumpto();
        ExitApplication.getInstance().back(this);
    }

    public void loginButtonAction(View v) throws InstantiationException, IllegalAccessException {
        String userName2 = this.userNameText.getText().toString();
        String password = this.pwdText.getText().toString();
        InputMethodManager imm = (InputMethodManager) getSystemService("input_method");
        imm.hideSoftInputFromWindow(this.pwdText.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(this.userNameText.getWindowToken(), 0);
        if (StringEx.Empty.equals(userName2)) {
            new AlertDialog.Builder(this).setMessage("用户名或密码为空").setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
        } else if (StringEx.Empty.equals(password)) {
            new AlertDialog.Builder(this).setMessage("用户名或密码为空").setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
        } else {
            ObjectStores.getInst().putObject("inputUserName", userName2);
            ObjectStores.getInst().putObject("inputPassword", password);
            testBusi();
        }
    }

    public void testBusi() {
        SharedPreferences sharedPreferences = getSharedPreferences("login_username", 2);
        RequestBidData requestBidData = new RequestBidData(this);
        requestBidData.setRequestData("登陆1");
        Log.d("请求的指令：", requestBidData.infoDate());
        requestBidData.setHttpCallBack(this.httpCallBackForLogin1);
        requestBidData.iExecuteForLogin1();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    /* access modifiers changed from: private */
    public void getLoginData() {
        ObjectStores.getInst().putObject("G_User", this.umcLoginNo);
        ObjectStores.getInst().putObject("inputUserName", this.inputUserName);
        RequestBidData requestBidData = new RequestBidData(this);
        requestBidData.setRequestData("登陆2");
        requestBidData.setId(ObjectStores.getInst().getObject("inputUserName").toString());
        Log.d("请求的指令：", requestBidData.infoDate());
        requestBidData.setHttpCallBack(this.httpCallBack);
        requestBidData.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    public void testBusi_getCus() {
        LoginBusi_getCus loginBusi_getCus = new LoginBusi_getCus(this);
        loginBusi_getCus.BSP_companyCode = this.BSP_companyCode;
        loginBusi_getCus.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    public void uiCallBack(BaseBusi obj) {
        if (obj instanceof LoginBusi) {
            getData((LoginParse) ((LoginBusi) obj).getBaseStruct());
        }
        if (obj instanceof LoginBusi_getCus) {
            setGlobal((NoticeParse) ((LoginBusi_getCus) obj).getBaseStruct());
        }
    }

    public void getData(LoginParse loginparse) {
        Log.d("=========getData(LoginParse loginparse)========", "==================");
        if (loginparse.commonData != null) {
            this.XH_msg = loginparse.commonData.msg;
            if (this.XH_msg.equals("Fail...")) {
                CustomMessageShow.getInst().showLongToast(this, this.XH_msg);
                return;
            }
            this.GfullName = loginparse.commonData.attr.parameter_username;
            this.parameter_usertokenid = loginparse.commonData.attr.parameter_usertokenid;
            String extattr = loginparse.commonData.attr.extattr;
            System.out.println("extattr:" + extattr);
            String[] string = extattr.split(",");
            for (int i = 0; i < string.length; i++) {
                System.out.println("aaaaa:" + string[i].split("=")[0]);
                System.out.println("前面的值----：" + string[i].split("=")[0]);
                if (string[i].split("=")[0].indexOf("BSP_COMPANY_CODE") != -1) {
                    this.companyCode = string[i].split("=")[1];
                    this.parameter_userid = this.companyCode;
                    System.out.println("parameter_userid:" + this.parameter_userid);
                }
                if (string[i].split("=")[0].indexOf("CHINESE_FULLNAME") != -1) {
                    this.companyFullName = string[i].split("=")[1].substring(0, string[i].split("=")[1].length() - 1);
                    System.out.println("companyFullName:" + this.companyFullName);
                }
                System.out.println("bbbbbbbb" + string[i].split("=")[0].indexOf("BSOL"));
                if (extattr.indexOf("BSOL") != -1) {
                    this.XH_reStr = String.valueOf(extattr.indexOf("BSOL"));
                    System.out.println("XH_reStr:" + this.XH_reStr);
                }
                if (string[i].split("=")[0].indexOf("UMC_LOGIN_NO") != -1) {
                    this.G_User = string[i].split("=")[1];
                    this.BSP_companyCode = string[i].split("=")[1];
                    System.out.println("G_User:" + this.G_User);
                    System.out.println("BSP_companyCode:BSP_companyCode=" + this.BSP_companyCode);
                }
            }
            if (!StringEx.Empty.equals(this.XH_reStr)) {
                this.XH_reStr = "1";
                this.reStr = "1";
            }
            System.out.println("BSOL==>" + this.BSOL);
            System.out.println("reStr==>" + this.reStr);
            System.out.println("XH_reStr==>" + this.XH_reStr);
            if ("1".equals(this.XH_reStr)) {
                getLoginData();
            } else {
                setGlobal(new NoticeParse());
            }
        }
    }

    public void setGlobal(NoticeParse noticeParse) {
        if (!(noticeParse.commonData == null || noticeParse.commonData.blocks == null || noticeParse.commonData.blocks.r0 == null || noticeParse.commonData.blocks.r0.mar == null || noticeParse.commonData.blocks.r0.mar.rows == null || noticeParse.commonData.blocks.r0.mar.rows.rows == null || noticeParse.commonData.blocks.r0.mar.rows.rows.size() <= 0)) {
            System.out.println("返回的rows---:" + noticeParse.commonData.blocks.r0.mar.rows.rows);
            Log.d("返回的rows", noticeParse.commonData.blocks.r0.mar.rows.rows.toString());
            System.out.println("返回的rows-大小--:" + noticeParse.commonData.blocks.r0.mar.rows.rows.size());
            int rowsCount = noticeParse.commonData.blocks.r0.mar.rows.rows.size();
            for (int i = 0; i < rowsCount; i++) {
                ArrayList<String> rowdata = noticeParse.commonData.blocks.r0.mar.rows.rows.get(i);
                if (rowdata != null) {
                    for (int k = 0; k < rowdata.size(); k++) {
                        if (k == 0) {
                            this.czy = (String) rowdata.get(k);
                        }
                        if (k == 1) {
                            this.czyname = (String) rowdata.get(k);
                        }
                        if (k == 2) {
                            this.czyrealname = (String) rowdata.get(k);
                        }
                        if (k == 3) {
                            this.hy = (String) rowdata.get(k);
                        }
                        if (k == 4) {
                            this.hyname = (String) rowdata.get(k);
                        }
                    }
                }
            }
        }
        if ("1".equals(this.reStr)) {
            setGlobalAll();
            showPushDialog();
            return;
        }
        new AlertDialog.Builder(this).setMessage(this.XH_msg).setPositiveButton("Ok", (DialogInterface.OnClickListener) null).show();
    }

    private void writeFileData() {
        SharedPreferences.Editor editor2 = getSharedPreferences("login_username", 2).edit();
        if (this.checkUsername.isChecked()) {
            editor2.putString("userName", this.userNameText.getText().toString().toLowerCase());
        } else {
            editor2.putString("userName", StringEx.Empty);
        }
        sysOsInfoModel soim = new SysOsInfo().sysOsInfo(this);
        sysConfigModel syss = new SysConfig().setSysconfigbs();
        editor2.putString("appcode", soim.getAppcode());
        editor2.putString("network", soim.getNetwork());
        editor2.putString("os", soim.getOs());
        editor2.putString("osVersion", soim.getOsVersion());
        editor2.putString("resolution1", soim.getResolution1());
        editor2.putString("resolution2", soim.getResolution2());
        editor2.putString("deviceid", soim.getDeviceid());
        editor2.putString("BSP_companyCode", this.umcLoginNo);
        editor2.putString("projectName", syss.getProjectName());
        editor2.commit();
    }

    /* access modifiers changed from: private */
    public void showPushDialog() {
        new AlertDialog.Builder(this).setMessage("是否开通推送消息功能？").setNegativeButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Login_indexActivity.this.registerDevice();
                Login_indexActivity.this.mUserName = Login_indexActivity.this.userNameText.getText().toString().toLowerCase();
                Login_indexActivity.this.mMessageServerEditor.putString("UserName", Login_indexActivity.this.mUserName);
                Login_indexActivity.this.mMessageServerEditor.putString("kaiqituisong", "YES");
                Login_indexActivity.this.mMessageServerEditor.commit();
                Login_indexActivity.this.jumpto();
            }
        }).setPositiveButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Login_indexActivity.this.mUserName = Login_indexActivity.this.userNameText.getText().toString().toLowerCase();
                Login_indexActivity.this.mMessageServerEditor.putString("UserName", Login_indexActivity.this.mUserName);
                Login_indexActivity.this.mMessageServerEditor.putString("kaiqituisong", "NO");
                Login_indexActivity.this.mMessageServerEditor.commit();
                Login_indexActivity.this.stopService(new Intent(Login_indexActivity.this, MsgService.class));
                dialog.dismiss();
                Login_indexActivity.this.jumpto();
            }
        }).create().show();
    }

    /* access modifiers changed from: private */
    public void registerDevice() {
        stopService(new Intent(this, MsgService.class));
        registerServer();
    }

    private void registerServer() {
        Msg_registerEquipmentInfoBusi res = new Msg_registerEquipmentInfoBusi(this);
        res.setHttpCallBack(this.httpCallBackRes);
        res.iExecute();
    }

    public static boolean isServiceRunning(Context mContext, String className) {
        Log.d("推送服务的名称：", className);
        boolean isRunning = false;
        List<ActivityManager.RunningServiceInfo> serviceList = ((ActivityManager) mContext.getSystemService("activity")).getRunningServices(30);
        if (serviceList.size() <= 0) {
            return false;
        }
        int i = 0;
        while (true) {
            if (i >= serviceList.size()) {
                break;
            }
            Log.d("现有后台服务：", serviceList.get(i).service.getClassName());
            if (serviceList.get(i).service.getClassName().equals(className)) {
                isRunning = true;
                break;
            }
            i++;
        }
        return isRunning;
    }

    /* access modifiers changed from: private */
    public void jumpto() {
        SharedPreferences sp2 = getSharedPreferences("index_mark", 2);
        if (sp2.getString("index", StringEx.Empty).equals("ContractList")) {
            ExitApplication.getInstance().startActivity(this, ContractListActivity.class, null, 1);
        }
        if (sp2.getString("index", StringEx.Empty).equals("Shopcar")) {
            this.dbHelper.insertOperation("循环物资", "钢材交易首页", "登录");
            ExitApplication.getInstance().startActivity(this, ShopActivity.class, null, 1);
        }
        if (sp2.getString("index", StringEx.Empty).equals("竞价场次筛选页面")) {
            ExitApplication.getInstance().startActivity(this, CyclicGoodsBidSessionSelect.class, null, 1);
        }
        if (sp2.getString("index", StringEx.Empty).equals("竞价场次页面")) {
            this.dbHelper.insertOperation("循环物资", "循环资源首页", "登录");
            int sessionCode = getIntent().getExtras().getInt("sessionCode");
            String startDate = getIntent().getExtras().getString("startDate");
            String endDate = getIntent().getExtras().getString("endDate");
            HashMap<String, Object> map = new HashMap<>();
            map.put("sessionCode", Integer.valueOf(sessionCode));
            map.put("startDate", startDate);
            map.put("endDate", endDate);
            ExitApplication.getInstance().startActivity(this, CyclicGoodsBidSession.class, map, 1);
        }
        if (sp2.getString("index", StringEx.Empty).equals("注销")) {
            if (ObjectStores.getInst().getObject("formX") == "formX") {
                ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class, null, 1);
            } else {
                ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class, null, 1);
            }
        }
        if (sp2.getString("index", StringEx.Empty).equals("更多")) {
            ExitApplication.getInstance().startActivity(this, Xhjy_more.class, null, 1);
        }
        if (sp2.getString("index", StringEx.Empty).equals("竞价大厅")) {
            startActivity(new Intent(this, CyclicGoodsBidHallPageActivity.class));
        }
        if (sp2.getString("index", StringEx.Empty).equals("到货确认列表")) {
            this.dbHelper.insertOperation("循环物资", "循环资源首页", "登录");
            ExitApplication.getInstance().startActivity(this, CyclicGoodsConfirmArrivalActivity.class, null, 1);
        }
        if (sp2.getString("index", StringEx.Empty).equals("竞价结果列表")) {
            ExitApplication.getInstance().startActivity(this, CyclicGoodsJingJiaJieGuo.class, null, 1);
        }
        if (sp2.getString("index", StringEx.Empty).equals("竞价公告的详情页")) {
            writeFileData();
            Intent intent = new Intent(this, CyclicGoodsBidPageActivity.class);
            intent.putExtra("id", getIntent().getExtras().getString("id"));
            intent.putExtra("activity", getIntent().getExtras().getString("activity"));
            Log.d("Login_indexActivity收到竞价公告的详情页的id：", String.valueOf(getIntent().getExtras().getString("id")) + "收到的activity：" + getIntent().getExtras().getString("activity"));
            startActivity(intent);
            finish();
        }
        if (sp2.getString("index", StringEx.Empty).equals("竞价公告的详情页2")) {
            writeFileData();
            Intent intent2 = new Intent(this, GCJYBidPageActivity.class);
            intent2.putExtra("id", getIntent().getExtras().getString("id"));
            intent2.putExtra("activity", getIntent().getExtras().getString("activity"));
            String sign = getIntent().getStringExtra("sign");
            String numOfBtn = getIntent().getExtras().getString("numOfBtn");
            intent2.putExtra("sign", sign);
            intent2.putExtra("numOfBtn", numOfBtn);
            startActivity(intent2);
            finish();
        }
        if (sp2.getString("index", StringEx.Empty).equals("SearchItemInfo12")) {
            HashMap<String, String> hasmap = new HashMap<>();
            hasmap.put("productname2", getIntent().getStringExtra("productname2"));
            hasmap.put("company2", getIntent().getStringExtra("company2"));
            hasmap.put("mertial2", getIntent().getStringExtra("mertial2"));
            hasmap.put("format2", getIntent().getStringExtra("format2"));
            hasmap.put("price2", getIntent().getStringExtra("price2"));
            hasmap.put("desunit2", getIntent().getStringExtra("desunit2"));
            hasmap.put("productaddr2", getIntent().getStringExtra("productaddr2"));
            hasmap.put("gpls", getIntent().getStringExtra("gpls"));
            ExitApplication.getInstance().startActivity(this, SearchItemInfoActivity.class, hasmap);
            finish();
        }
        writeFileData();
        finish();
    }

    public void setGlobalAll() {
        ObjectStores.getInst().putObject("inputUserName", this.inputUserName);
        ObjectStores.getInst().putObject("inputPassword", this.inputPassword);
        ObjectStores.getInst().putObject("XH_msg", this.resultDesc);
        ObjectStores.getInst().putObject("GfullName", this.userName);
        ObjectStores.getInst().putObject("parameter_usertokenid", this.bspCompanyCode);
        ObjectStores.getInst().putObject("parameter_userid", this.bspCompanyCode);
        ObjectStores.getInst().putObject("companyCode", this.bspCompanyCode);
        ObjectStores.getInst().putObject("companyFullName", this.chineseFullname);
        ObjectStores.getInst().putObject("BSP_companyCode", this.umcLoginNo);
        ObjectStores.getInst().putObject("G_User", this.umcLoginNo);
        ObjectStores.getInst().putObject("XH_reStr", this.BSOL);
        String sb = new StringBuilder(String.valueOf(this.BSOL)).toString();
        ObjectStores.getInst().putObject("reStr", this.reStr);
        ObjectStores.getInst().putObject("parameter_czy", this.czy);
        ObjectStores.getInst().putObject("parameter_czyname", this.czyname);
        ObjectStores.getInst().putObject("parameter_czyrealname", this.czyrealname);
        ObjectStores.getInst().putObject("parameter_hy", this.hy);
        ObjectStores.getInst().putObject("parameter_hyname", this.hyname);
        System.out.println("czy:=====" + this.czy);
        this.dbHelper.insertLogin((String) ObjectStores.getInst().getObject("companyFullName"), (String) ObjectStores.getInst().getObject("BSP_companyCode"));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.bspCompanyCode = StringEx.Empty;
        this.chineseFullname = StringEx.Empty;
        this.isSuccess = StringEx.Empty;
        this.loginNickName = StringEx.Empty;
        this.openAppClusters = StringEx.Empty;
        this.XH = StringEx.Empty;
        this.BSOL = StringEx.Empty;
        this.resultDesc = StringEx.Empty;
        this.srPageModel = StringEx.Empty;
        this.srSysProcessModel = StringEx.Empty;
        this.clientBeginTimeMillis = StringEx.Empty;
        this.clientEndTimeMillis = StringEx.Empty;
        this.methodName = StringEx.Empty;
        this.serverBeginTimeMillis = StringEx.Empty;
        this.serverEndTimeMillis = StringEx.Empty;
        this.serviceClassName = StringEx.Empty;
        this.serviceName = StringEx.Empty;
        this.serviceProcessId = StringEx.Empty;
        this.srUserProcessModel = StringEx.Empty;
        this.errorCode = StringEx.Empty;
        this.exceptionClass = StringEx.Empty;
        this.exceptionMessage = StringEx.Empty;
        this.resultChineseDesc = StringEx.Empty;
        this.resultCode = StringEx.Empty;
        this.resultDescofsrUserProcessModel = StringEx.Empty;
        this.resultDescArgs = StringEx.Empty;
        this.resultDescBundle = StringEx.Empty;
        this.userBeginTimeMillis = StringEx.Empty;
        this.userEndTimeMillis = StringEx.Empty;
        this.userTimeSepType = StringEx.Empty;
        this.umcLoginNo = StringEx.Empty;
        this.userName = StringEx.Empty;
        this.userNameText = null;
        this.pwdText = null;
        this.checkUsername = null;
        this.inputUserName = StringEx.Empty;
        this.inputPassword = StringEx.Empty;
        this.parameter_userid = StringEx.Empty;
        this.XH_msg = StringEx.Empty;
        this.GfullName = StringEx.Empty;
        this.parameter_usertokenid = StringEx.Empty;
        this.companyCode = StringEx.Empty;
        this.companyFullName = StringEx.Empty;
        this.BSP_companyCode = StringEx.Empty;
        this.G_User = StringEx.Empty;
        this.XH_reStr = StringEx.Empty;
        this.reStr = StringEx.Empty;
        this.czy = StringEx.Empty;
        this.czyname = StringEx.Empty;
        this.czyrealname = StringEx.Empty;
        this.hy = StringEx.Empty;
        this.hyname = StringEx.Empty;
        super.onDestroy();
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.cyclicgoods_index_navigation_item2:
                this.mIndexNavigation2.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item3:
                this.mIndexNavigation3.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidSessionSelect.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item1:
                this.mIndexNavigation1.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item4:
                this.mIndexNavigation4.setChecked(true);
                startActivity(new Intent(this, CyclicGoodsBidHallPageActivity.class));
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item5:
                this.mIndexNavigation5.setChecked(true);
                Intent intent = new Intent(this, Xhjy_more.class);
                intent.putExtra("part", "循环物资");
                startActivity(intent);
                finish();
                return;
            case R.id.radio_search3:
                ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
                return;
            case R.id.radio_shopcar3:
                ((RadioButton) findViewById(R.id.radio_shopcar3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ShopActivity.class);
                return;
            case R.id.radio_home3:
                ((RadioButton) findViewById(R.id.radio_home3)).setChecked(true);
                this.index_more = "1";
                HashMap hasmap = new HashMap();
                hasmap.put("index_more", this.index_more);
                ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class, hasmap);
                return;
            case R.id.radio_contract3:
                ((RadioButton) findViewById(R.id.radio_contract3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ContractListActivity.class);
                return;
            case R.id.radio_notice3:
                ((RadioButton) findViewById(R.id.radio_notice3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, Xhjy_more.class);
                return;
            default:
                return;
        }
    }

    public void onBackPressed() {
        jumpto();
        super.onBackPressed();
    }

    class FileUploadLoginTask extends AsyncTask<Integer, Integer, String> {
        FileUploadLoginTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Integer... params) {
            Login_indexActivity.this.FileToZip();
            Login_indexActivity.this.result = new UploadUtil().uploadFile(Environment.getExternalStorageDirectory() + "/Baosteel/YDlogin" + new SimpleDateFormat("yyyyMMdd").format(new Date()).toString() + ZipUtils.EXT, "http://10.31.108.247:8080/clientlog/log/" + new SysConfig().setMethod().getMethodName());
            System.out.println("result111==>" + Login_indexActivity.this.result);
            return Login_indexActivity.this.result;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            FileParse fileParse = new FileParse();
            if (result == null) {
                result = StringEx.Empty;
            }
            fileParse.parse2(result);
            Login_indexActivity.this.getInfo(fileParse);
            new FileUploadYdotTask().execute(new Integer[0]);
            super.onPostExecute((Object) result);
        }
    }

    class FileUploadYdotTask extends AsyncTask<Integer, Integer, String> {
        FileUploadYdotTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Integer... params) {
            Login_indexActivity.this.FileToZip();
            Login_indexActivity.this.result = new UploadUtil().uploadFile(Environment.getExternalStorageDirectory() + "/Baosteel/YDopt" + new SimpleDateFormat("yyyyMMdd").format(new Date()).toString() + ZipUtils.EXT, "http://10.31.108.247:8080/clientlog/log/" + new SysConfig().setMethod().getMethodName());
            System.out.println("result==>" + Login_indexActivity.this.result);
            return Login_indexActivity.this.result;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            FileParse fileParse = new FileParse();
            if (result == null) {
                result = StringEx.Empty;
            }
            fileParse.parse2(result);
            Login_indexActivity.this.getInfo(fileParse);
            super.onPostExecute((Object) result);
        }
    }

    /* access modifiers changed from: private */
    public void FileToZip() {
        DataXml dx = new DataXml();
        this.file_operation = dx.DataOpeartionToTxt(this);
        this.file_login = dx.DataLoginToTxt(this);
        new ZipUtils();
        try {
            ZipUtils.compress(this.file_operation);
            ZipUtils.compress(this.file_login);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void getInfo(FileParse fs) {
        String message = fs.msg;
        if (message == null) {
            message = "11111";
        }
        String file = fs.fileName;
        String time = new SimpleDateFormat("yyyyMMdd").format(new Date()).toString();
        File file_login_zip = new File(Environment.getExternalStorageDirectory() + "/Baosteel/YDopt" + time + ZipUtils.EXT);
        File file_operation_zip = new File(Environment.getExternalStorageDirectory() + "/Baosteel/YDlogin" + time + ZipUtils.EXT);
        this.file_operation.delete();
        file_operation_zip.delete();
        this.file_login.delete();
        file_login_zip.delete();
        if (message.equals("上传日志成功！")) {
            if (file.equals("YDopt" + time + ZipUtils.EXT)) {
                this.dbHelper.DropOpeartionTable();
            }
            if (file.equals("YDlogin" + time + ZipUtils.EXT)) {
                this.dbHelper.DropLoginTable();
            }
        }
    }
}
