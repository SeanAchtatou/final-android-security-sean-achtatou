package com.baosteelOnline.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.common.SysOsInfo;
import com.baosteelOnline.common.jqhttp.Utils;
import com.baosteelOnline.model.sysOsInfoModel;
import com.jianq.misc.StringEx;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DBHelper extends SQLiteOpenHelper {
    public static final String APPNAME = "东方钢铁在线";
    public static final String BSP_COMPANYCODE = "bsp_companycode";
    private static final String DATABASE_NAME = "log_db";
    private static final int DATABASE_VERSION = 4;
    public static final String FIRSTMODULE = "first_module";
    public static final String LOGINDATE = "LoginDate";
    public static final String LOG_LOGIN_ID = "_id";
    public static final String LOG_OPERATION_ID = "_id";
    public static final String MOBAPPID = "mob_app_id";
    public static final String MOBEQUIPMENTID = "MobEquipmentId";
    public static final String MOBLOCALVERSION = "MobLocalVersion";
    public static final String MOBOSVERSION = "MobOsVersion";
    public static final String OPERATETIME = "operate_time";
    public static final String POSITIONID = "positionId";
    public static final String SECONDMODULE = "second_module";
    private static final String TABLE_NAME = "log_operation_tbl";
    private static final String TABLE_NAMES = "log_login_tbl";
    public static final String THIRDMODULE = "third_module";
    public static final String USERNAME = "username";
    private static String version = StringEx.Empty;
    private Context context;

    public DBHelper(Context context2) {
        super(context2, DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 4);
        this.context = context2;
        try {
            version = Utils.getVersionName(context2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("Create table log_operation_tbl(_id integer primary key autoincrement,username text, bsp_companycode text, mob_app_id text, first_module text, second_module text, third_module text, operate_time text, positionId text, MobOsVersion text, MobEquipmentId text, MobLocalVersion text );");
        db.execSQL("Create table log_login_tbl(_id integer primary key autoincrement,username text, bsp_companycode text, mob_app_id text, LoginDate text, MobOsVersion text, MobEquipmentId text, MobLocalVersion text);");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        System.out.println("删除表开始");
        db.execSQL(" DROP TABLE IF EXISTS log_operation_tbl");
        db.execSQL(" DROP TABLE IF EXISTS log_login_tbl");
        System.out.println("==============");
        onCreate(db);
    }

    public long insertOperation(String firstModule, String secondModule, String thirdModule) {
        sysOsInfoModel soim = new SysOsInfo().sysOsInfo(this.context);
        String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()).toString();
        String XH_reStr = (String) ObjectStores.getInst().getObject("XH_reStr");
        String username = StringEx.Empty;
        String bsp_companyCode = StringEx.Empty;
        if (XH_reStr != null && XH_reStr.equals("1")) {
            username = (String) ObjectStores.getInst().getObject("companyFullName");
            bsp_companyCode = (String) ObjectStores.getInst().getObject("BSP_companyCode");
        }
        String positionId = (String) ObjectStores.getInst().getObject("positionName");
        if (positionId == null) {
            positionId = StringEx.Empty;
        }
        System.out.println("positionId==>" + positionId);
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("username", username);
        cv.put("bsp_companycode", bsp_companyCode);
        cv.put("mob_app_id", APPNAME);
        cv.put("first_module", firstModule);
        cv.put("second_module", secondModule);
        cv.put("third_module", thirdModule);
        cv.put("operate_time", time);
        cv.put("positionId", positionId);
        cv.put("MobOsVersion", soim.getOsVersion());
        cv.put("MobEquipmentId", soim.getDeviceid());
        cv.put("MobLocalVersion", version);
        return db.insert(TABLE_NAME, null, cv);
    }

    public long insertLogin(String username, String bsp_companyCode) {
        sysOsInfoModel soim = new SysOsInfo().sysOsInfo(this.context);
        String loginDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()).toString();
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("username", username);
        cv.put("bsp_companycode", bsp_companyCode);
        cv.put("mob_app_id", APPNAME);
        cv.put("LoginDate", loginDate);
        cv.put("MobOsVersion", soim.getOsVersion());
        cv.put("MobEquipmentId", soim.getDeviceid());
        cv.put("MobLocalVersion", version);
        return db.insert(TABLE_NAMES, null, cv);
    }

    public Cursor selectOperation() {
        return getReadableDatabase().query(TABLE_NAME, null, null, null, null, null, " _id desc");
    }

    public Cursor selectLogin() {
        return getReadableDatabase().query(TABLE_NAMES, null, null, null, null, null, " _id desc");
    }

    public void DropOpeartionTable() {
        getWritableDatabase().execSQL("DELETE FROM log_operation_tbl;");
    }

    public void DropLoginTable() {
        getWritableDatabase().execSQL("DELETE FROM log_login_tbl;");
    }

    public void DropDatabase(Context ctx) {
        try {
            ctx.deleteDatabase(DATABASE_NAME);
        } catch (Exception ex) {
            Log.i("异常", ex.toString());
        }
    }
}
