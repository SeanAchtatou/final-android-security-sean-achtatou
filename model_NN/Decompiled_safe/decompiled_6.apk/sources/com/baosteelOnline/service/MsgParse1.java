package com.baosteelOnline.service;

import android.util.Log;
import com.andframework.parse.BaseParse;
import com.jianq.misc.StringEx;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MsgParse1 extends BaseParse {
    public CommonData commonData;
    private String dataString = StringEx.Empty;
    private String dataString1 = StringEx.Empty;
    private String message = StringEx.Empty;

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message2) {
        this.message = message2;
    }

    public String getDataString1() {
        return this.dataString1;
    }

    public void setDataString1(String dataString12) {
        this.dataString1 = dataString12;
    }

    public String getDataString() {
        return this.dataString;
    }

    public void setDataString(String dataString2) {
        this.dataString = dataString2;
    }

    public boolean parse(byte[] data) {
        System.out.println("NoticeParse----" + new String(data));
        setDataString1(new String(data));
        if (super.parse(data) && this.parseType == 0) {
            Log.i("MsgParse", new String(data));
            try {
                this.commonData = new CommonData();
                if (this.jsonObject.has("msg")) {
                    this.commonData.msg = this.jsonObject.getString("msg");
                    setMessage(this.jsonObject.getString("msg"));
                }
                if (this.jsonObject.has("msgKey")) {
                    this.commonData.msgKey = this.jsonObject.getString("msgKey");
                }
                if (this.jsonObject.has("detailMsg")) {
                    this.commonData.detailMsg = this.jsonObject.getString("detailMsg");
                }
                if (this.jsonObject.has("status")) {
                    this.commonData.status = this.jsonObject.getString("status");
                }
                if (this.jsonObject.has("attr")) {
                    this.commonData.attr = new NoticeAttribute();
                    JSONObject jo = this.jsonObject.getJSONObject("attr");
                    if (jo.has("serviceName")) {
                        this.commonData.attr.serviceName = jo.getString("serviceName");
                    }
                    if (jo.has("projectName")) {
                        this.commonData.attr.projectName = jo.getString("projectName");
                    }
                    if (jo.has("methodName")) {
                        this.commonData.attr.methodName = jo.getString("methodName");
                    }
                    if (jo.has("operateNo")) {
                        this.commonData.attr.operateNo = jo.getString("operateNo");
                    }
                    if (jo.has("parameter_userid")) {
                        this.commonData.attr.parameter_userid = jo.getString("parameter_userid");
                    }
                    if (jo.has("parameter_username")) {
                        this.commonData.attr.parameter_username = jo.getString("parameter_username");
                    }
                }
                if (this.jsonObject.has("blocks")) {
                    this.commonData.blocks = new NoticeBlocks();
                    JSONObject jo2 = this.jsonObject.getJSONObject("blocks");
                    if (jo2.has("r0")) {
                        this.commonData.blocks.r0 = new R0();
                        JSONObject jo22 = jo2.getJSONObject("r0");
                        this.commonData.blocks.r0.mar = parseMetaAndRow(jo22);
                    }
                    if (jo2.has("i0")) {
                        this.commonData.blocks.i0 = new I0();
                        JSONObject jo3 = jo2.getJSONObject("i0");
                        this.commonData.blocks.i0.mar = parseMetaAndRow(jo3);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public boolean parse(String data) {
        System.out.println("NoticeParse----" + new String(data));
        if (this.parseType != 0) {
            return false;
        }
        Log.d("推送获取MsgParse", new String(data));
        setDataString(data);
        try {
            this.jsonObject = new JSONObject(data);
            this.commonData = new CommonData();
            if (this.jsonObject.has("msg")) {
                this.commonData.msg = this.jsonObject.getString("msg");
            }
            if (this.jsonObject.has("msgKey")) {
                this.commonData.msgKey = this.jsonObject.getString("msgKey");
            }
            if (this.jsonObject.has("detailMsg")) {
                this.commonData.detailMsg = this.jsonObject.getString("detailMsg");
            }
            if (this.jsonObject.has("status")) {
                this.commonData.status = this.jsonObject.getString("status");
            }
            if (this.jsonObject.has("attr")) {
                this.commonData.attr = new NoticeAttribute();
                JSONObject jo = this.jsonObject.getJSONObject("attr");
                if (jo.has("serviceName")) {
                    this.commonData.attr.serviceName = jo.getString("serviceName");
                }
                if (jo.has("projectName")) {
                    this.commonData.attr.projectName = jo.getString("projectName");
                }
                if (jo.has("methodName")) {
                    this.commonData.attr.methodName = jo.getString("methodName");
                }
                if (jo.has("operateNo")) {
                    this.commonData.attr.operateNo = jo.getString("operateNo");
                }
                if (jo.has("parameter_userid")) {
                    this.commonData.attr.parameter_userid = jo.getString("parameter_userid");
                }
                if (jo.has("parameter_username")) {
                    this.commonData.attr.parameter_username = jo.getString("parameter_username");
                }
            }
            if (!this.jsonObject.has("blocks")) {
                return false;
            }
            this.commonData.blocks = new NoticeBlocks();
            JSONObject jo2 = this.jsonObject.getJSONObject("blocks");
            if (jo2.has("r0")) {
                this.commonData.blocks.r0 = new R0();
                JSONObject jo22 = jo2.getJSONObject("r0");
                this.commonData.blocks.r0.mar = parseMetaAndRow(jo22);
            }
            if (!jo2.has("i0")) {
                return false;
            }
            this.commonData.blocks.i0 = new I0();
            JSONObject jo3 = jo2.getJSONObject("i0");
            this.commonData.blocks.i0.mar = parseMetaAndRow(jo3);
            return false;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    private MetaAndRow parseMetaAndRow(JSONObject jo) throws JSONException {
        MetaAndRow rar = new MetaAndRow();
        if (jo.has("meta")) {
            rar.meta = new Meta();
            JSONObject jo1 = jo.getJSONObject("meta");
            if (jo1.has("columns")) {
                JSONArray js = jo1.getJSONArray("columns");
                rar.meta.columns = new ArrayList<>();
                for (int k = 0; k < js.length(); k++) {
                    ColumnsData cd = new ColumnsData();
                    JSONObject mc = js.getJSONObject(k);
                    if (mc.has("pos")) {
                        cd.pos = mc.getString("pos");
                    }
                    if (mc.has("name")) {
                        cd.name = mc.getString("name");
                    }
                    if (mc.has("descName")) {
                        cd.descName = mc.getString("descName");
                    }
                    rar.meta.columns.add(cd);
                }
            }
        }
        if (jo.has("rows")) {
            rar.rows = new Rows();
            JSONArray jjo = jo.getJSONArray("rows");
            rar.rows.rows = new ArrayList<>();
            for (int m = 0; m < jjo.length(); m++) {
                JSONArray mm = jjo.getJSONArray(m);
                ArrayList<String> tmpa = new ArrayList<>();
                for (int l = 0; l < mm.length(); l++) {
                    tmpa.add(mm.getString(l));
                }
                rar.rows.rows.add(tmpa);
            }
        }
        return rar;
    }

    public class CommonData {
        public NoticeAttribute attr;
        public NoticeBlocks blocks;
        public String detailMsg = StringEx.Empty;
        public String msg = StringEx.Empty;
        public String msgKey = StringEx.Empty;
        public String status = StringEx.Empty;

        public CommonData() {
        }
    }

    public class NoticeAttribute {
        public String methodName = StringEx.Empty;
        public String operateNo = StringEx.Empty;
        public String parameter_userid = StringEx.Empty;
        public String parameter_username = StringEx.Empty;
        public String projectName = StringEx.Empty;
        public String serviceName = StringEx.Empty;

        public NoticeAttribute() {
        }
    }

    public class NoticeBlocks {
        public I0 i0;
        public R0 r0;

        public NoticeBlocks() {
        }
    }

    public class R0 {
        public MetaAndRow mar;

        public R0() {
        }
    }

    public class I0 {
        public MetaAndRow mar;

        public I0() {
        }
    }

    public class MetaAndRow {
        public Meta meta;
        public Rows rows;

        public MetaAndRow() {
        }
    }

    public class Meta {
        public ArrayList<ColumnsData> columns;

        public Meta() {
        }
    }

    public class Rows {
        public ArrayList<ArrayList<String>> rows;

        public Rows() {
        }
    }

    public class ColumnsData {
        public String descName;
        public String name;
        public String pos;

        public ColumnsData() {
        }
    }
}
