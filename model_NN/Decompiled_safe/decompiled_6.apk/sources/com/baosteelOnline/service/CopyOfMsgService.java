package com.baosteelOnline.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import com.andframework.ui.CustomMessageShow;
import com.baosteelOnline.R;
import com.baosteelOnline.data.Msg_queryPushInfoBusi;
import com.baosteelOnline.data_parse.MsgParse;
import com.baosteelOnline.xhjy.Xhjy_indexActivity;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import java.util.ArrayList;

public class CopyOfMsgService extends Service {
    /* access modifiers changed from: private */
    public Handler handler = new Handler();
    private JQUICallBack httpCallBack = new JQUICallBack() {
        public void callBack(ResultData result) {
        }
    };
    private Intent intent = null;
    private Notification notification = null;
    private NotificationManager notificationManager = null;
    private PendingIntent pendingIntent = null;
    Runnable updateThread = new Runnable() {
        public void run() {
            CopyOfMsgService.this.startConnect();
            CopyOfMsgService.this.handler.postDelayed(CopyOfMsgService.this.updateThread, 10000);
        }
    };

    public IBinder onBind(Intent arg0) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        this.notificationManager = (NotificationManager) getSystemService("notification");
        this.handler.post(this.updateThread);
    }

    /* access modifiers changed from: private */
    public void startConnect() {
        Msg_queryPushInfoBusi res = new Msg_queryPushInfoBusi(this);
        res.setHttpCallBack(this.httpCallBack);
        res.iExecute();
    }

    private void queryPushInfoBack(MsgParse ds) {
        if (MsgParse.commonData == null) {
            CustomMessageShow.getInst().showLongToast(this, "服务器无数据返回！");
        } else if (MsgParse.commonData.blocks.r0.mar.rows.rows.size() == 0) {
            CustomMessageShow.getInst().showLongToast(this, "没有数据！");
        } else if (MsgParse.commonData != null && MsgParse.commonData.blocks != null && MsgParse.commonData.blocks.r0 != null && MsgParse.commonData.blocks.r0.mar != null && MsgParse.commonData.blocks.r0.mar.rows != null && MsgParse.commonData.blocks.r0.mar.rows.rows.size() >= 1) {
            for (int i = 0; i < MsgParse.commonData.blocks.r0.mar.rows.rows.size(); i++) {
                ArrayList<String> rowdata_r0 = MsgParse.commonData.blocks.r0.mar.rows.rows.get(i);
                ListRow sr = new ListRow();
                if (rowdata_r0 != null) {
                    for (int k = 0; k < rowdata_r0.size(); k++) {
                        if (k == 7) {
                            sr.seqId = (String) rowdata_r0.get(k);
                        }
                        if (k == 11) {
                            sr.messageInfo = (String) rowdata_r0.get(k);
                        }
                    }
                    showNotification(Integer.valueOf(sr.seqId).intValue(), R.drawable.icon, "您有新的信息，请查看!", null, sr.messageInfo);
                }
            }
        }
    }

    private void registerBack(MsgParse ds) {
        if (MsgParse.commonData != null && MsgParse.commonData != null && MsgParse.commonData.attr != null && MsgParse.commonData.msg != null && MsgParse.commonData.msg.indexOf("成功") != -1) {
            Log.v("MsgService", "消息中心注册成功！");
        }
    }

    public void onStart(Intent intent2, int startId) {
        super.onStart(intent2, startId);
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void showNotification(int notification_id, int icon, String tickertext, String title, String content) {
        this.notification = new Notification(icon, tickertext, System.currentTimeMillis());
        this.notification.flags = 16;
        this.notification.defaults = -1;
        this.intent = new Intent(getApplicationContext(), Xhjy_indexActivity.class);
        this.pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, this.intent, 0);
        this.notification.setLatestEventInfo(getApplicationContext(), title, content, this.pendingIntent);
        this.notificationManager.notify(notification_id, this.notification);
    }

    class ListRow {
        public String createDate = StringEx.Empty;
        public String customerId = StringEx.Empty;
        public String customerName = StringEx.Empty;
        public String fromSystem = StringEx.Empty;
        public String messageInfo = StringEx.Empty;
        public String messageStatus = StringEx.Empty;
        public String mobAppId = StringEx.Empty;
        public String mobEquipmentId = StringEx.Empty;
        public String mobOs = StringEx.Empty;
        public String mobOsVersion = StringEx.Empty;
        public String mobPhoneNumber = StringEx.Empty;
        public String segNo = StringEx.Empty;
        public String sendDate = StringEx.Empty;
        public String seqId = StringEx.Empty;
        public String userLoginNo = StringEx.Empty;

        ListRow() {
        }
    }
}
