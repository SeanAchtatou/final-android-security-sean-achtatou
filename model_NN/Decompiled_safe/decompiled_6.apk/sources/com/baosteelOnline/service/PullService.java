package com.baosteelOnline.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.util.Log;
import com.jianq.misc.StringEx;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public class PullService extends Service {
    public static final String ACTION_START = "action_start";
    public static final String ACTION_STOP = "action_stop";
    public static final String TAG = "PullService";
    public static final int TYPE_DEFAULT = 0;
    public static boolean isAvailable = true;
    /* access modifiers changed from: private */
    public long interval = 10000;
    private NotificationManager mManager;
    private NotifiThread mNotifiThread;
    private Notification mNotification;
    private SharedPreferences mPreferences;

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        Log.v(TAG, "onCreate");
        initNotifiManager();
    }

    private void initPreferences() {
        this.interval = 10000;
        System.out.println("interval----:" + this.interval);
    }

    private void initNotifiManager() {
        this.mManager = (NotificationManager) getSystemService("notification");
        this.mNotification = new Notification();
        this.mNotification.icon = 17301630;
        this.mNotification.tickerText = "tickerText";
        this.mNotification.defaults = 1;
        this.mNotification.flags = 16;
    }

    private void launchNotification() {
        this.mNotification.when = System.currentTimeMillis();
        this.mNotification.setLatestEventInfo(this, "测试标题", "测试内容", PendingIntent.getActivity(this, 0, null, 268435456));
        this.mManager.notify(0, this.mNotification);
    }

    public static void startAction(Context ctx) {
        Intent i = new Intent();
        i.setClass(ctx, PullService.class);
        i.setAction(ACTION_START);
        Log.v(TAG, "startAction");
        ctx.startService(i);
    }

    public static void stopAction(Context ctx) {
        Intent i = new Intent();
        i.setClass(ctx, PullService.class);
        i.setAction(ACTION_STOP);
        Log.v(TAG, "stopAction");
        ctx.startService(i);
    }

    public void onStart(Intent intent, int startId) {
        String action = intent.getAction();
        Log.v(TAG, "onStart");
        if (ACTION_START.equals(action)) {
            if (this.mNotifiThread != null && this.mNotifiThread.isAlive()) {
                isAvailable = false;
                this.mNotifiThread.interrupt();
                this.mNotifiThread = null;
            }
            isAvailable = true;
            this.mNotifiThread = new NotifiThread();
            this.mNotifiThread.start();
        } else if (ACTION_STOP.equals(action) && this.mNotifiThread != null && this.mNotifiThread.isAlive()) {
            isAvailable = false;
            this.mNotifiThread.interrupt();
            this.mNotifiThread = null;
        }
    }

    class NotifiThread extends Thread {
        NotifiThread() {
        }

        public void run() {
            while (PullService.isAvailable) {
                try {
                    System.out.println("updateThread执行了-------");
                    sleep(PullService.this.interval);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public JSONObject checkNotificationFromServer() {
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(StringEx.Empty);
        try {
            List values = new ArrayList();
            values.add(new BasicNameValuePair("id", "admin"));
            values.add(new BasicNameValuePair("pwd", "admin"));
            post.setEntity(new UrlEncodedFormEntity(values, "utf-8"));
            BufferedReader reader = new BufferedReader(new InputStreamReader(client.execute(post).getEntity().getContent()));
            StringBuilder builder = new StringBuilder();
            try {
                for (String s = reader.readLine(); s != null; s = reader.readLine()) {
                    builder.append(s);
                }
                System.out.println("返回结果-Service---:" + builder.toString());
                StringBuilder sb = builder;
                return new JSONObject(builder.toString());
            } catch (UnsupportedEncodingException e) {
                e = e;
                e.printStackTrace();
                return null;
            } catch (ClientProtocolException e2) {
                e = e2;
                e.printStackTrace();
                return null;
            } catch (IOException e3) {
                e = e3;
                e.printStackTrace();
                return null;
            } catch (JSONException e4) {
                e = e4;
                e.printStackTrace();
                return null;
            }
        } catch (UnsupportedEncodingException e5) {
            e = e5;
            e.printStackTrace();
            return null;
        } catch (ClientProtocolException e6) {
            e = e6;
            e.printStackTrace();
            return null;
        } catch (IOException e7) {
            e = e7;
            e.printStackTrace();
            return null;
        } catch (JSONException e8) {
            e = e8;
            e.printStackTrace();
            return null;
        }
    }
}
