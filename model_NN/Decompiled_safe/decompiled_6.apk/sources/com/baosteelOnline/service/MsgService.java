package com.baosteelOnline.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.text.format.Time;
import android.util.Log;
import cn.com.jit.pnxclient.constant.PNXConfigConstant;
import com.andframework.ui.CustomMessageShow;
import com.baosteelOnline.R;
import com.baosteelOnline.data_parse.MsgParse;
import com.baosteelOnline.xhjy.CyclicGoodsIndexActivity;
import com.jianq.misc.StringEx;
import com.jianq.net.JQBasicNetwork;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

public class MsgService extends Service {
    public static String TIME_OUT = "操作超时";
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            new DownloadTask().execute(new Integer[0]);
            super.handleMessage(msg);
        }
    };
    private Intent intent = null;
    private Notification notification = null;
    private NotificationManager notificationManager = null;
    private PendingIntent pendingIntent = null;
    public String projectName = StringEx.Empty;
    public String result = StringEx.Empty;
    private int sss = 1;
    TimerTask task = new TimerTask() {
        public void run() {
            Message message = new Message();
            message.what = 1;
            MsgService.this.handler.sendMessage(message);
        }
    };
    private final Timer timer = new Timer();
    public String userLoginNo = StringEx.Empty;

    public IBinder onBind(Intent arg0) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        this.notificationManager = (NotificationManager) getSystemService("notification");
        Log.d("发推送请求：", "5分钟一次");
        this.timer.schedule(this.task, 0, 300000);
    }

    class DownloadTask extends AsyncTask<Integer, Integer, String> {
        DownloadTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Integer... params) {
            try {
                Thread.sleep(10);
                MsgService.this.result = MsgService.this.startConnect();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            return MsgService.this.result;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            Log.d("onPostExecute推送获取的数据", "数据为：" + result);
            MsgParse1 msgparse = new MsgParse1();
            msgparse.parse(result);
            MsgService.this.queryPushInfoBack(msgparse);
            super.onPostExecute((Object) result);
        }
    }

    /* access modifiers changed from: private */
    public String startConnect() throws Exception {
        System.out.println("startConnect------------");
        String parameter_postdata = infoData();
        List<NameValuePair> params = new ArrayList<>();
        SharedPreferences sp = getSharedPreferences("login_username", 2);
        params.add(new BasicNameValuePair("appcode", sp.getString("appcode", StringEx.Empty)));
        params.add(new BasicNameValuePair("parameter_encryptdata", "false"));
        params.add(new BasicNameValuePair("parameter_compressdata", "false"));
        params.add(new BasicNameValuePair("parameter_postdata", parameter_postdata));
        params.add(new BasicNameValuePair("network", sp.getString("network", StringEx.Empty)));
        params.add(new BasicNameValuePair("os", sp.getString("os", StringEx.Empty)));
        params.add(new BasicNameValuePair("osVersion", sp.getString("osVersion", StringEx.Empty)));
        params.add(new BasicNameValuePair("resolution1", sp.getString("resolution1", StringEx.Empty)));
        params.add(new BasicNameValuePair("resolution2", sp.getString("resolution2", StringEx.Empty)));
        params.add(new BasicNameValuePair("deviceid", sp.getString("deviceid", StringEx.Empty)));
        this.result = doPost(params, "http://m.baosteel.net.cn/iPlatMBS/AgentService");
        return this.result;
    }

    private String infoData() {
        SharedPreferences sp = getSharedPreferences("login_username", 2);
        this.projectName = sp.getString("projectName", StringEx.Empty);
        String mobEquipmentId = sp.getString("deviceid", StringEx.Empty);
        this.userLoginNo = sp.getString("BSP_companyCode", StringEx.Empty);
        String jsonStr = "{\"msg\":\"\",\"msgKey\":\"\",\"detailMsg\":\"\",\"status\":0,\"attr\":{\"projectName\":\"" + this.projectName + "\"" + ",\"messageStatus\":\"" + 10 + "\"" + ",\"mobAppId\":\"" + "BSOL" + "\"" + ",\"mobOs\":\"" + "AnroidPhone" + "\"" + ",\"methodName\":\"" + "queryPushInfo" + "\"" + ",\"serviceName\":\"" + "D1MB0102" + "\"" + ",\"cmd\":\"" + "queryPushInfo" + "\"" + ",\"mobPhoneNumber\":\"" + mobEquipmentId + "\"" + ",\"parameter_userid\":\"\"" + ",\"parameter_username\":\"\"" + ",\"mobEquipmentId\":\"" + mobEquipmentId + "\"" + ",\"userLoginNo\":\"" + this.userLoginNo + "\"" + ",\"customerId\":\"" + this.userLoginNo + "\"" + "},\"blocks\":{}}";
        System.out.println("jsonStr---:" + jsonStr);
        return jsonStr;
    }

    /* access modifiers changed from: private */
    public void queryPushInfoBack(MsgParse1 ds) {
        Log.d("获取的推送数据", ds.getDataString());
        if (ds.commonData == null) {
            CustomMessageShow.getInst().showLongToast(this, "服务器无数据返回！");
        } else if ((ds.commonData == null || ds.commonData.blocks == null || ds.commonData.blocks.r0 == null || ds.commonData.blocks.r0.mar == null || ds.commonData.blocks.r0.mar.rows == null || ds.commonData.blocks.r0.mar.rows.rows == null || ds.commonData.blocks.r0.mar.rows.rows.size() != 0) && ds.commonData != null && ds.commonData.blocks != null && ds.commonData.blocks.r0 != null && ds.commonData.blocks.r0.mar != null && ds.commonData.blocks.r0.mar.rows != null && ds.commonData.blocks.r0.mar.rows.rows.size() >= 1) {
            for (int i = 0; i < ds.commonData.blocks.r0.mar.rows.rows.size(); i++) {
                ArrayList<String> rowdata_r0 = ds.commonData.blocks.r0.mar.rows.rows.get(i);
                ListRow sr = new ListRow();
                if (rowdata_r0 != null) {
                    for (int k = 0; k < rowdata_r0.size(); k++) {
                        if (k == 7) {
                            sr.seqId = (String) rowdata_r0.get(k);
                        }
                        if (k == 11) {
                            sr.messageInfo = (String) rowdata_r0.get(k);
                        }
                        if (k == 0) {
                            sr.customerId = (String) rowdata_r0.get(k);
                        }
                    }
                    if (!sr.messageInfo.isEmpty()) {
                        Log.d("传递的消息", String.valueOf(sr.messageInfo) + " ID=" + Integer.parseInt(sr.seqId));
                        showNotification(Integer.valueOf(sr.seqId).intValue(), R.drawable.icon, "您有新的信息，请查看!", null, sr.messageInfo, sr);
                    }
                }
            }
        }
    }

    private void registerBack(MsgParse ds) {
        if (MsgParse.commonData != null && MsgParse.commonData != null && MsgParse.commonData.attr != null && MsgParse.commonData.msg != null && MsgParse.commonData.msg.indexOf("成功") != -1) {
            Log.v("MsgService", "消息中心注册成功！");
        }
    }

    public void onStart(Intent intent2, int startId) {
        super.onStart(intent2, startId);
    }

    public void onDestroy() {
        this.timer.cancel();
        super.onDestroy();
    }

    public void showNotification(int notification_id, int icon, String tickertext, String title, String content, ListRow sr) {
        SharedPreferences messageHistory = getSharedPreferences(sr.customerId.toLowerCase(), 0);
        int nowNum = messageHistory.getInt("nowNum", 0);
        SharedPreferences.Editor editor = messageHistory.edit();
        editor.putString(new StringBuilder(String.valueOf(nowNum + 1)).toString(), content);
        Time t = new Time();
        t.setToNow();
        int year = t.year;
        int month = t.month + 1;
        int date = t.monthDay;
        int hour = t.hour;
        int minute = t.minute;
        editor.putString(String.valueOf(nowNum + 1) + "date", String.valueOf(year) + "-" + month + "-" + date + " " + hour + PNXConfigConstant.RESP_SPLIT_3 + minute + PNXConfigConstant.RESP_SPLIT_3 + t.second);
        editor.putString(String.valueOf(nowNum + 1) + "id", new StringBuilder(String.valueOf(notification_id)).toString());
        editor.putInt("nowNum", nowNum + 1);
        editor.commit();
        Notification notification2 = new Notification(icon, tickertext, System.currentTimeMillis());
        notification2.flags = 16;
        notification2.defaults = -1;
        Intent intent2 = new Intent(getApplicationContext(), CyclicGoodsIndexActivity.class);
        intent2.putExtra("pushMessage", content);
        intent2.putExtra("contactid", sr.customerId);
        intent2.putExtra("id", new StringBuilder(String.valueOf(notification_id)).toString());
        notification2.setLatestEventInfo(getApplicationContext(), title, content, PendingIntent.getActivity(getApplicationContext(), notification_id, intent2, 134217728));
        ((NotificationManager) getSystemService("notification")).notify(notification_id, notification2);
    }

    public static String doPost(List<NameValuePair> params, String url) throws Exception {
        HttpPost httpPost = new HttpPost(url);
        httpPost.setEntity(new UrlEncodedFormEntity(params, JQBasicNetwork.UTF_8));
        DefaultHttpClient httpClient = new DefaultHttpClient();
        httpClient.getParams().setParameter("http.connection.timeout", 30000);
        httpClient.getParams().setParameter("http.socket.timeout", 30000);
        HttpResponse httpResp = httpClient.execute(httpPost);
        if (httpResp.getStatusLine().getStatusCode() == 200) {
            String result2 = EntityUtils.toString(httpResp.getEntity(), JQBasicNetwork.UTF_8);
            Log.i("HttpPost", "HttpPost方式请求成功，返回数据如下：");
            Log.i("result", result2);
            return result2;
        }
        Log.i("HttpPost", "HttpPost方式请求失败");
        return null;
    }

    class ListRow {
        public String createDate = StringEx.Empty;
        public String customerId = StringEx.Empty;
        public String customerName = StringEx.Empty;
        public String fromSystem = StringEx.Empty;
        public String messageInfo = StringEx.Empty;
        public String messageStatus = StringEx.Empty;
        public String mobAppId = StringEx.Empty;
        public String mobEquipmentId = StringEx.Empty;
        public String mobOs = StringEx.Empty;
        public String mobOsVersion = StringEx.Empty;
        public String mobPhoneNumber = StringEx.Empty;
        public String segNo = StringEx.Empty;
        public String sendDate = StringEx.Empty;
        public String seqId = StringEx.Empty;
        public String userLoginNo = StringEx.Empty;

        ListRow() {
        }
    }
}
