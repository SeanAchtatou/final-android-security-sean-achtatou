package com.baosteelOnline.data_parse;

import com.baosteelOnline.common.JQEncryptUtil;
import com.jianq.misc.StringEx;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class WelcomeParse {
    public static CommonData commonData;
    public static WelcomeParse contractParse;

    public static class ColumnsData {
        public String descName;
        public String name;
        public String pos;
    }

    public static class CommonData {
        public NoticeAttribute attr;
        public NoticeBlocks blocks;
        public String detailMsg = StringEx.Empty;
        public String msg = StringEx.Empty;
        public String msgKey = StringEx.Empty;
        public String status = StringEx.Empty;
    }

    public static class I0 {
        public MetaAndRow mar;
    }

    public static class Meta {
        public ArrayList<ColumnsData> columns;
    }

    public static class MetaAndRow {
        public Meta meta;
        public Rows rows;
    }

    public static class NoticeAttribute {
        public String methodName = StringEx.Empty;
        public String operateNo = StringEx.Empty;
        public String parameter_userid = StringEx.Empty;
        public String parameter_username = StringEx.Empty;
        public String projectName = StringEx.Empty;
        public String serviceName = StringEx.Empty;
    }

    public static class NoticeBlocks {
        public I0 i0;
        public R0 r0;
    }

    public static class R0 {
        public MetaAndRow mar;
    }

    public static class Rows {
        public ArrayList<ArrayList<String>> rows;
    }

    public static WelcomeParse parse(String data) {
        String decryptData = data;
        try {
            decryptData = JQEncryptUtil.decrypt(data);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        try {
            contractParse = new WelcomeParse();
            commonData = new CommonData();
            JSONObject jsonObjectAll = new JSONObject(decryptData);
            if (jsonObjectAll.has("dataString")) {
                JSONObject jsonObject = new JSONObject((String) jsonObjectAll.get("dataString"));
                if (jsonObject.has("msg")) {
                    System.out.println("Search_Data: " + jsonObject.toString());
                    commonData.msg = jsonObject.getString("msg");
                }
                if (jsonObject.has("msgKey")) {
                    commonData.msgKey = jsonObject.getString("msgKey");
                }
                if (jsonObject.has("detailMsg")) {
                    commonData.detailMsg = jsonObject.getString("detailMsg");
                }
                if (jsonObject.has("status")) {
                    commonData.status = jsonObject.getString("status");
                }
                if (jsonObject.has("attr")) {
                    commonData.attr = new NoticeAttribute();
                    JSONObject jo = jsonObject.getJSONObject("attr");
                    if (jo.has("serviceName")) {
                        commonData.attr.serviceName = jo.getString("serviceName");
                    }
                    if (jo.has("projectName")) {
                        commonData.attr.projectName = jo.getString("projectName");
                    }
                    if (jo.has("serviceMethodName")) {
                        commonData.attr.methodName = jo.getString("serviceMethodName");
                    }
                }
                if (jsonObject.has("blocks")) {
                    commonData.blocks = new NoticeBlocks();
                    JSONObject jo2 = jsonObject.getJSONObject("blocks");
                    if (jo2.has("r0")) {
                        commonData.blocks.r0 = new R0();
                        JSONObject jo22 = jo2.getJSONObject("r0");
                        commonData.blocks.r0.mar = parseMetaAndRow(jo22);
                    }
                    if (jo2.has("i0")) {
                        commonData.blocks.i0 = new I0();
                        JSONObject jo3 = jo2.getJSONObject("i0");
                        commonData.blocks.i0.mar = parseMetaAndRow(jo3);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return contractParse;
    }

    private static MetaAndRow parseMetaAndRow(JSONObject jo) throws JSONException {
        MetaAndRow rar = new MetaAndRow();
        if (jo.has("meta")) {
            rar.meta = new Meta();
            JSONObject jo1 = jo.getJSONObject("meta");
            if (jo1.has("columns")) {
                JSONArray js = jo1.getJSONArray("columns");
                rar.meta.columns = new ArrayList<>();
                for (int k = 0; k < js.length(); k++) {
                    ColumnsData cd = new ColumnsData();
                    JSONObject mc = js.getJSONObject(k);
                    if (mc.has("pos")) {
                        cd.pos = mc.getString("pos");
                    }
                    if (mc.has("name")) {
                        cd.name = mc.getString("name");
                    }
                    if (mc.has("descName")) {
                        cd.descName = mc.getString("descName");
                    }
                    rar.meta.columns.add(cd);
                }
            }
        }
        if (jo.has("rows")) {
            rar.rows = new Rows();
            JSONArray jjo = jo.getJSONArray("rows");
            rar.rows.rows = new ArrayList<>();
            for (int m = 0; m < jjo.length(); m++) {
                JSONArray mm = jjo.getJSONArray(m);
                ArrayList<String> tmpa = new ArrayList<>();
                for (int l = 0; l < mm.length(); l++) {
                    String tmp = mm.getString(l);
                    System.out.println("qstmp[" + l + "]:" + tmp);
                    tmpa.add(tmp);
                }
                rar.rows.rows.add(tmpa);
            }
        }
        return rar;
    }
}
