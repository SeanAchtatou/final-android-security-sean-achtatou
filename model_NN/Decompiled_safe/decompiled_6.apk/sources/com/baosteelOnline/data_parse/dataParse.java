package com.baosteelOnline.data_parse;

import com.baosteelOnline.common.JQEncryptUtil;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class dataParse {
    public static CommonData commonData;
    public static dataParse getNumParse;

    public static class CommonData {
        public ArrayList<DataList> dataList;
    }

    public static class DataList {
        public String amt;
        public String bz;
        public String date;
        public String details;
        public String extbill;
        public String extype;
        public String lockdate;
        public String paysavesz;
        public String receivingside;
        public String retcode;
        public String retmsg;
        public String shoppingmall;
        public String sort;
        public String state;
        public String type;
        public String unlockdate;
    }

    public static dataParse parse(String data) {
        if (data != null) {
            try {
                getNumParse = new dataParse();
                String jiemi = JQEncryptUtil.decrypt(data);
                System.out.println("jiemi==>" + jiemi);
                commonData = new CommonData();
                JSONObject jsonObject = new JSONObject(jiemi);
                if (jsonObject.has("dataString")) {
                    JSONArray js = new JSONArray(jsonObject.getString("dataString"));
                    commonData.dataList = new ArrayList<>();
                    for (int i = 0; i < js.length(); i++) {
                        DataList cl = new DataList();
                        JSONObject mc = js.getJSONObject(i);
                        if (mc.has("amt")) {
                            cl.amt = mc.getString("amt");
                        }
                        if (mc.has("bz")) {
                            cl.bz = mc.getString("bz");
                        }
                        if (mc.has("date")) {
                            cl.date = mc.getString("date");
                        }
                        if (mc.has("details")) {
                            cl.details = mc.getString("details");
                        }
                        if (mc.has("extbill")) {
                            cl.extbill = mc.getString("extbill");
                        }
                        if (mc.has("extype")) {
                            cl.extype = mc.getString("extype");
                        }
                        if (mc.has("lockdate")) {
                            cl.lockdate = mc.getString("lockdate");
                        }
                        if (mc.has("paysavesz")) {
                            cl.paysavesz = mc.getString("paysavesz");
                        }
                        if (mc.has("receivingside")) {
                            cl.receivingside = mc.getString("receivingside");
                        }
                        if (mc.has("retcode")) {
                            cl.retcode = mc.getString("retcode");
                        }
                        if (mc.has("retmsg")) {
                            cl.retmsg = mc.getString("retmsg");
                        }
                        if (mc.has("shoppingmall")) {
                            cl.shoppingmall = mc.getString("shoppingmall");
                        }
                        if (mc.has("sort")) {
                            cl.sort = mc.getString("sort");
                        }
                        if (mc.has("state")) {
                            cl.state = mc.getString("state");
                        }
                        if (mc.has("type")) {
                            cl.type = mc.getString("type");
                        }
                        if (mc.has("unlockdate")) {
                            cl.unlockdate = mc.getString("unlockdate");
                        }
                        commonData.dataList.add(cl);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return getNumParse;
    }
}
