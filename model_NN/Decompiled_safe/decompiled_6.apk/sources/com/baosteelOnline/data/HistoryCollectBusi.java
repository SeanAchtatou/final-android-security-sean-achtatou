package com.baosteelOnline.data;

import android.content.Context;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.common.SysConfig;
import com.baosteelOnline.http.HRParameter;
import com.jianq.common.JQFrameworkConfig;
import com.jianq.helper.JQNetworkHelper;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import com.jianq.util.JQUtils;
import org.json.JSONException;

public class HistoryCollectBusi {
    public String appcode = "com.baosight.ets";
    public String buyerid = StringEx.Empty;
    public String companyCode = StringEx.Empty;
    public String contactName = StringEx.Empty;
    private Context context;
    public String deviceid = StringEx.Empty;
    public String flag = StringEx.Empty;
    private JQUICallBack httpCallBack = null;
    private boolean isNetwork;
    public String logid = StringEx.Empty;
    private String method = "query";
    public String network = StringEx.Empty;
    public String operateNo = StringEx.Empty;
    public String orderby = StringEx.Empty;
    public String os = StringEx.Empty;
    public String osVersion = StringEx.Empty;
    public String parameter_userid = StringEx.Empty;
    public String parameter_usertokenid = StringEx.Empty;
    public String pricemax = StringEx.Empty;
    public String pricemin = StringEx.Empty;
    public String projectName = StringEx.Empty;
    public String resolution1 = StringEx.Empty;
    public String resolution2 = StringEx.Empty;
    public String sellername = StringEx.Empty;
    public String shopsign = StringEx.Empty;
    private String tail = "iPlatMBS/AgentService";
    public String thickmax = StringEx.Empty;
    public String thickmin = StringEx.Empty;
    public String type4 = StringEx.Empty;

    public void setHttpCallBack(JQUICallBack httpCallBack2) {
        this.httpCallBack = httpCallBack2;
    }

    public HistoryCollectBusi(Context context2) {
        this.context = context2;
    }

    public void iExecute() {
        String parameter_postdata = infoData();
        this.isNetwork = JQUtils.isNetworkAvailable(this.context);
        if (this.isNetwork) {
            JQNetworkHelper networkHelper = new JQNetworkHelper(this.context);
            networkHelper.progress = false;
            HRParameter parameter = null;
            try {
                HRParameter parameter2 = new HRParameter();
                try {
                    parameter2.setMethod(this.method);
                    parameter2.setBizObj(parameter_postdata);
                    parameter = parameter2;
                } catch (JSONException e) {
                    parameter = parameter2;
                }
            } catch (JSONException e2) {
            }
            networkHelper.open("post", String.valueOf(JQFrameworkConfig.getInst().getDomain()) + this.method);
            networkHelper.setCallBack(this.httpCallBack);
            System.out.println("parameter==>" + parameter.toString());
            networkHelper.send(parameter.toString());
            return;
        }
        System.out.println("============");
    }

    private String infoData() {
        if (ObjectStores.getInst().getObject("buyerid") == null) {
            this.buyerid = StringEx.Empty;
        } else {
            this.buyerid = (String) ObjectStores.getInst().getObject("buyerid");
        }
        if (ObjectStores.getInst().getObject("companyCode") == null) {
            this.contactName = StringEx.Empty;
        } else {
            this.contactName = (String) ObjectStores.getInst().getObject("companyCode");
        }
        this.projectName = new SysConfig().setSysconfigbsol().getProjectName();
        String parameter_postdata = "{'msgKey':'','attr':{'projectName':'" + this.projectName + "'" + ",'operateNo ':'A1','methodName':'newsavemySql','serviceName':'M1TE01'}," + "'blocks':{'i0':{'meta':{'columns':[";
        if (!StringEx.Empty.equals(this.buyerid)) {
            parameter_postdata = String.valueOf(parameter_postdata) + "{'descName':' ','name':'customerId','pos':0},";
        }
        if (!StringEx.Empty.equals(this.contactName)) {
            parameter_postdata = String.valueOf(parameter_postdata) + "{'descName':' ','name':'contactName','pos':0},";
        }
        if (!StringEx.Empty.equals(this.sellername)) {
            parameter_postdata = String.valueOf(parameter_postdata) + "{'descName':' ','name':'sellercustomerid','pos':0},";
        }
        if (!StringEx.Empty.equals(this.type4)) {
            parameter_postdata = String.valueOf(parameter_postdata) + "{'descName':' ','name':'producttypeid','pos':0},";
        }
        if (!StringEx.Empty.equals(this.shopsign)) {
            parameter_postdata = String.valueOf(parameter_postdata) + "{'descName':' ','name':'shopsign','pos':0},";
        }
        if (!StringEx.Empty.equals(this.orderby)) {
            parameter_postdata = String.valueOf(parameter_postdata) + "{'descName':' ','name':'orderby','pos':0},";
        }
        if (!StringEx.Empty.equals(this.thickmin)) {
            parameter_postdata = String.valueOf(parameter_postdata) + "{'descName':' ','name':'thickmin','pos':0},";
        }
        if (!StringEx.Empty.equals(this.thickmax)) {
            parameter_postdata = String.valueOf(parameter_postdata) + "{'descName':' ','name':'thickmax','pos':0},";
        }
        if (!StringEx.Empty.equals(this.pricemin)) {
            parameter_postdata = String.valueOf(parameter_postdata) + "{'descName':' ','name':'pricemin','pos':0},";
        }
        if (!StringEx.Empty.equals(this.pricemax)) {
            parameter_postdata = String.valueOf(parameter_postdata) + "{'descName':' ','name':'pricemax','pos':0},";
        }
        if (!StringEx.Empty.equals(this.flag)) {
            parameter_postdata = String.valueOf(parameter_postdata) + "{'descName':' ','name':'flag','pos':0},";
        }
        if (!StringEx.Empty.equals(this.logid)) {
            parameter_postdata = String.valueOf(parameter_postdata) + "{'descName':' ','name':'logid','pos':0}";
        }
        String parameter_postdata2 = String.valueOf(parameter_postdata) + "]},'rows':[['";
        if (!StringEx.Empty.equals(this.buyerid)) {
            parameter_postdata2 = String.valueOf(parameter_postdata2) + this.buyerid + "','";
        }
        if (!StringEx.Empty.equals(this.contactName)) {
            parameter_postdata2 = String.valueOf(parameter_postdata2) + this.contactName + "','";
        }
        if (!StringEx.Empty.equals(this.sellername)) {
            parameter_postdata2 = String.valueOf(parameter_postdata2) + this.sellername + "','";
        }
        if (!StringEx.Empty.equals(this.type4)) {
            parameter_postdata2 = String.valueOf(parameter_postdata2) + this.type4 + "','";
        }
        if (!StringEx.Empty.equals(this.shopsign)) {
            parameter_postdata2 = String.valueOf(parameter_postdata2) + this.shopsign + "','";
        }
        if (!StringEx.Empty.equals(this.orderby)) {
            parameter_postdata2 = String.valueOf(parameter_postdata2) + this.orderby + "','";
        }
        if (!StringEx.Empty.equals(this.thickmin)) {
            parameter_postdata2 = String.valueOf(parameter_postdata2) + this.thickmin + "','";
        }
        if (!StringEx.Empty.equals(this.thickmax)) {
            parameter_postdata2 = String.valueOf(parameter_postdata2) + this.thickmax + "','";
        }
        if (!StringEx.Empty.equals(this.pricemin)) {
            parameter_postdata2 = String.valueOf(parameter_postdata2) + this.pricemin + "','";
        }
        if (!StringEx.Empty.equals(this.pricemax)) {
            parameter_postdata2 = String.valueOf(parameter_postdata2) + this.pricemax + "','";
        }
        if (!StringEx.Empty.equals(this.flag)) {
            parameter_postdata2 = String.valueOf(parameter_postdata2) + this.flag + "','";
        }
        if (!StringEx.Empty.equals(this.logid)) {
            parameter_postdata2 = String.valueOf(parameter_postdata2) + this.logid + "' ";
        }
        return String.valueOf(parameter_postdata2) + "]]}}}";
    }
}
