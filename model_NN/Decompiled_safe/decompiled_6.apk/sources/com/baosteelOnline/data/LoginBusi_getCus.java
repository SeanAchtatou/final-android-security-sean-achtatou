package com.baosteelOnline.data;

import android.util.Log;
import com.andframework.business.BaseBusi;
import com.andframework.myinterface.UiCallBack;
import com.baosteelOnline.common.JQEncryptUtil;
import com.baosteelOnline.common.SysConfig;
import com.baosteelOnline.data_parse.NoticeParse;
import com.jianq.misc.StringEx;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class LoginBusi_getCus extends BaseBusi {
    public String BSP_companyCode = StringEx.Empty;
    public String appcode = "com.baosight.ets";
    public String deviceid = StringEx.Empty;
    public String network = StringEx.Empty;
    public String operateNo = StringEx.Empty;
    public String os = StringEx.Empty;
    public String osVersion = StringEx.Empty;
    public String parameter_postdata = StringEx.Empty;
    public String parameter_usertokenid = StringEx.Empty;
    public String projectName = StringEx.Empty;
    public String resolution1 = StringEx.Empty;
    public String resolution2 = StringEx.Empty;
    private String tail = "iPlatMBS/AgentService";

    public LoginBusi_getCus(UiCallBack bCallBack) {
        super(bCallBack, NoticeParse.class);
    }

    /* access modifiers changed from: protected */
    public void prepare() {
        this.reqParam = this.tail;
        Map<String, String> mapnormal = new HashMap<>();
        this.parameter_postdata = infoData();
        mapnormal.put("appcode", this.appcode);
        mapnormal.put("parameter_encryptdata", "false");
        mapnormal.put("parameter_compressdata", "false");
        mapnormal.put("parameter_postdata", this.parameter_postdata);
        mapnormal.put("network", this.network);
        mapnormal.put("os", this.os);
        mapnormal.put("osVersion", this.osVersion);
        mapnormal.put("resolution1", this.resolution1);
        mapnormal.put("resolution2", this.resolution2);
        mapnormal.put("deviceid", this.deviceid);
        mapnormal.put("parameter_usertokenid", this.parameter_usertokenid);
        mapnormal.put("operateNo", this.operateNo);
        mapnormal.put("BSP_companyCode", StringEx.Empty);
        setFormData(mapnormal);
    }

    private String infoData() {
        this.projectName = new SysConfig().setSysconfigbsol().getProjectName();
        String jsonStr = "{'msgKey':'','attr':{'projectName':'" + this.projectName + "','methodName':'doDefault'," + "'serviceMethodName':'bspToBsteelUserService','showcar':'1'," + "'parameter_czy':'" + this.BSP_companyCode + "','parameter_hy':''," + "'serviceName':'M1TE01'},'blocks':{'i0':{'meta':{'columns':[]},'rows':[[]]}}}";
        Log.d("login", jsonStr);
        String encryptString = jsonStr;
        try {
            return URLEncoder.encode(JQEncryptUtil.encrypt(jsonStr));
        } catch (Exception e) {
            e.printStackTrace();
            return encryptString;
        }
    }
}
