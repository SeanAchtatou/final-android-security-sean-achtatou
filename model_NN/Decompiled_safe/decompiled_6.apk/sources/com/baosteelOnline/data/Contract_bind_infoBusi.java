package com.baosteelOnline.data;

import android.content.Context;
import android.util.Log;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.common.JQEncryptUtil;
import com.baosteelOnline.common.SysConfig;
import com.baosteelOnline.http.HRParameter;
import com.baosteelOnline.model.sysConfigModel;
import com.jianq.common.JQFrameworkConfig;
import com.jianq.helper.JQNetworkHelper;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import com.jianq.util.JQUtils;
import java.net.URLEncoder;
import org.json.JSONException;

public class Contract_bind_infoBusi {
    public String appcode = "com.baosight.ets";
    private Context context;
    public String czy = StringEx.Empty;
    public String deviceid = StringEx.Empty;
    public String hth = StringEx.Empty;
    private JQUICallBack httpCallBack = null;
    public String hy = StringEx.Empty;
    private boolean isNetwork;
    private String method = "query";
    public String network = StringEx.Empty;
    public String operateNo = StringEx.Empty;
    public String os = StringEx.Empty;
    public String osVersion = StringEx.Empty;
    public String parameter_userid = StringEx.Empty;
    public String parameter_usertokenid = StringEx.Empty;
    public String projectName = StringEx.Empty;
    public String resolution1 = StringEx.Empty;
    public String resolution2 = StringEx.Empty;
    private String tail = "iPlatMBS/AgentService";
    public String xh = StringEx.Empty;

    public Contract_bind_infoBusi(Context context2) {
        this.context = context2;
    }

    public void setHttpCallBack(JQUICallBack httpCallBack2) {
        this.httpCallBack = httpCallBack2;
    }

    public void iExecute() {
        String parameter_postdata = infoDate();
        this.isNetwork = JQUtils.isNetworkAvailable(this.context);
        if (this.isNetwork) {
            JQNetworkHelper networkHelper = new JQNetworkHelper(this.context);
            networkHelper.progress = false;
            HRParameter parameter = null;
            try {
                HRParameter parameter2 = new HRParameter();
                try {
                    parameter2.setMethod(this.method);
                    parameter2.setBizObj(parameter_postdata);
                    parameter = parameter2;
                } catch (JSONException e) {
                    parameter = parameter2;
                }
            } catch (JSONException e2) {
            }
            networkHelper.open("post", String.valueOf(JQFrameworkConfig.getInst().getDomain()) + this.method);
            networkHelper.setCallBack(this.httpCallBack);
            System.out.println("parameter==>" + parameter.toString());
            networkHelper.send(parameter.toString());
            return;
        }
        System.out.println("============");
    }

    private String infoDate() {
        sysConfigModel scf = new SysConfig().setSysconfigbsol();
        this.czy = (String) ObjectStores.getInst().getObject("parameter_czy");
        this.hy = (String) ObjectStores.getInst().getObject("parameter_hy");
        this.projectName = scf.getProjectName();
        String parameter_postdata = "{'msgKey':'','attr':{'projectName':'" + this.projectName + "','operateNo ':'A17'," + "'methodName':'doDefault','serviceMethodName':'pactViewPackService'," + "'showcar':'1','parameter_czy':'" + this.czy + "','parameter_hy':'" + this.hy + "','serviceName':'M1TE01'}," + "'blocks':{'i0':{'meta':{'columns':[" + "{'descName':' ','name':'hth','pos':0},{'descName':' ','name':'xh','pos':0}]}," + "'rows':[['" + this.hth + "','" + this.xh + "']]}}}";
        Log.d("parameter_postdata====>", parameter_postdata);
        String encryptString = parameter_postdata;
        try {
            return URLEncoder.encode(JQEncryptUtil.encrypt(parameter_postdata));
        } catch (Exception e) {
            e.printStackTrace();
            return encryptString;
        }
    }
}
