package com.baosteelOnline.data;

import android.content.Context;
import com.baosteelOnline.common.JQEncryptUtil;
import com.baosteelOnline.common.SysConfig;
import com.baosteelOnline.http.HRParameter;
import com.jianq.common.JQFrameworkConfig;
import com.jianq.helper.JQNetworkHelper;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import com.jianq.util.JQUtils;
import java.net.URLEncoder;
import org.json.JSONException;

public class SearchBusi {
    public String appcode = "com.baosight.ets";
    public String cd = StringEx.Empty;
    private Context context;
    public String cz = StringEx.Empty;
    public String deviceid = StringEx.Empty;
    public String dj = StringEx.Empty;
    public String djg1 = StringEx.Empty;
    public String djg2 = StringEx.Empty;
    public String gpls = StringEx.Empty;
    public String hd1 = StringEx.Empty;
    public String hd2 = StringEx.Empty;
    private JQUICallBack httpCallBack = null;
    private boolean isNetwork;
    public String kd1 = StringEx.Empty;
    public String kd2 = StringEx.Empty;
    public String key = StringEx.Empty;
    public String limit = "10";
    private String method = "query";
    public String mj = StringEx.Empty;
    public String network = StringEx.Empty;
    public String offset = "1";
    public String operateNo = StringEx.Empty;
    public String orderGg = StringEx.Empty;
    public String orderJg = StringEx.Empty;
    public String os = StringEx.Empty;
    public String osVersion = StringEx.Empty;
    public String parameter_userid = StringEx.Empty;
    public String parameter_usertokenid = StringEx.Empty;
    public String pm = StringEx.Empty;
    public String projectName = StringEx.Empty;
    public String resolution1 = StringEx.Empty;
    public String resolution2 = StringEx.Empty;

    public void setHttpCallBack(JQUICallBack httpCallBack2) {
        this.httpCallBack = httpCallBack2;
    }

    public SearchBusi(Context context2) {
        this.context = context2;
    }

    public void iExecute() {
        String parameter_postdata = infoData();
        this.isNetwork = JQUtils.isNetworkAvailable(this.context);
        if (this.isNetwork) {
            JQNetworkHelper networkHelper = new JQNetworkHelper(this.context);
            networkHelper.progress = false;
            HRParameter parameter = null;
            try {
                HRParameter parameter2 = new HRParameter();
                try {
                    parameter2.setMethod(this.method);
                    parameter2.setBizObj(parameter_postdata);
                    parameter = parameter2;
                } catch (JSONException e) {
                    parameter = parameter2;
                }
            } catch (JSONException e2) {
            }
            networkHelper.open("post", String.valueOf(JQFrameworkConfig.getInst().getDomain()) + this.method);
            networkHelper.setCallBack(this.httpCallBack);
            System.out.println("parameter==>" + parameter.toString());
            networkHelper.send(parameter.toString());
            return;
        }
        System.out.println("============");
    }

    private String infoData() {
        this.projectName = new SysConfig().setSysconfigbsol().getProjectName();
        String jsonStr = "{'msgKey':'','attr':{'projectName':'" + this.projectName + "','operateNo ':'A6'," + "'methodName':'doDefault','serviceMethodName':'resourceService'," + "'showcar':'1','parameter_czy':'','parameter_hy':''," + "'serviceName':'M1TE01'},'blocks':{'i0':{'meta':{'columns':[" + "{'descName':' ','name':'pm','pos':0}," + "{'descName':' ','name':'mj','pos':0}," + "{'descName':' ','name':'cz','pos':0}," + "{'descName':' ','name':'cd','pos':0}," + "{'descName':' ','name':'dj','pos':0}," + "{'descName':' ','name':'gpls','pos':0}," + "{'descName':' ','name':'orderJg','pos':0}," + "{'descName':' ','name':'orderGg','pos':0}," + "{'descName':' ','name':'key','pos':0}," + "{'descName':' ','name':'djg1','pos':0}," + "{'descName':' ','name':'djg2','pos':0}," + "{'descName':' ','name':'hd1','pos':0}," + "{'descName':' ','name':'hd2','pos':0}," + "{'descName':' ','name':'kd1','pos':0}," + "{'descName':' ','name':'kd2','pos':0}," + "{'descName':' ','name':'limit','pos':0}," + "{'descName':' ','name':'offset','pos':0}]},'rows':[[" + "'" + this.pm + "','" + this.mj + "','" + this.cz + "','" + this.cd + "','" + this.dj + "','" + this.gpls + "','" + this.orderJg + "'," + "'" + this.orderGg + "','" + this.key + "','" + this.djg1 + "','" + this.djg2 + "','" + this.hd1 + "','" + this.hd2 + "'," + "'" + this.kd1 + "','" + this.kd2 + "','" + this.limit + "','" + this.offset + "']]}}}";
        String encryptString = jsonStr;
        try {
            return URLEncoder.encode(JQEncryptUtil.encrypt(jsonStr));
        } catch (Exception e) {
            e.printStackTrace();
            return encryptString;
        }
    }
}
