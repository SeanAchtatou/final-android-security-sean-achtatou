package com.baosteelOnline.data;

import android.util.Log;
import com.jianq.misc.StringEx;
import com.jianq.net.JQBasicNetwork;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;

public class GetData {
    private static String JsonStr = "{'msgKey':'','attr':{'projectName':'bgxh','operateNo ':'%E6%9C%AA%E6%9C%89%E7%BC%96%E5%8F%B7','methodName':'queryNotice_XH','serviceName':'M1TE01'},'blocks':{'i0':{'meta':{'columns':[{'descName':' ','name':'limit','pos':0},{'descName':' ','name':'offset','pos':0},{'descName':' ','name':'currentPage','pos':0}]},'rows':[['8','0','1']]}}}";
    public static String appcode = "com.baosight.ets";
    public static String deviceid = StringEx.Empty;
    public static String empId = StringEx.Empty;
    public static HttpClient httpClient;
    private static HttpParams httpParams;
    public static String network = StringEx.Empty;
    public static String operateNo = StringEx.Empty;
    public static String os = StringEx.Empty;
    public static String osVersion = StringEx.Empty;
    public static String parameter_clienidtversion = StringEx.Empty;
    public static String parameter_clienttypeid = StringEx.Empty;
    public static String parameter_deviceid = StringEx.Empty;
    public static String parameter_password = StringEx.Empty;
    public static String parameter_postdata = StringEx.Empty;
    public static String parameter_userid = StringEx.Empty;
    public static String parameter_usertokenid = StringEx.Empty;
    public static String pwd = StringEx.Empty;
    public static String resolution1 = StringEx.Empty;
    public static String resolution2 = StringEx.Empty;
    private static String url = "http://m.baosteel.net.cn/iPlatMBS/AgentService";
    public static String userid = StringEx.Empty;

    public static String getHttpData() {
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("appcode", appcode));
        params.add(new BasicNameValuePair("parameter_encryptdata", "false"));
        params.add(new BasicNameValuePair("parameter_compressdata", "false"));
        params.add(new BasicNameValuePair("parameter_postdata", JsonStr));
        params.add(new BasicNameValuePair("network", network));
        params.add(new BasicNameValuePair("os", os));
        params.add(new BasicNameValuePair("osVersion", osVersion));
        params.add(new BasicNameValuePair("resolution1", resolution1));
        params.add(new BasicNameValuePair("resolution2", resolution2));
        params.add(new BasicNameValuePair("deviceid", deviceid));
        params.add(new BasicNameValuePair("parameter_userid", parameter_userid));
        params.add(new BasicNameValuePair("parameter_usertokenid", parameter_usertokenid));
        getHttpClient();
        String str = doPost("http://m.baosteel.net.cn/iPlatMBS/AgentService", params);
        System.out.println("返回的数据==" + str);
        return str.replaceAll("'", StringEx.Empty);
    }

    public static HttpClient getHttpClient() {
        System.out.println("jsfactory----getHttpClient");
        httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, 2000);
        HttpConnectionParams.setSoTimeout(httpParams, 2000);
        HttpConnectionParams.setSocketBufferSize(httpParams, 8192);
        HttpClientParams.setRedirecting(httpParams, true);
        HttpProtocolParams.setUserAgent(httpParams, "Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN; rv:1.9.2) Gecko/20100115 Firefox/3.6");
        httpClient = new DefaultHttpClient(httpParams);
        return httpClient;
    }

    public static String doPost(String url2, List<NameValuePair> params) {
        String strResult;
        System.out.println("JsFactory---doPost---url==>" + url2);
        HttpPost httpRequest = new HttpPost(url2);
        try {
            httpRequest.setEntity(new UrlEncodedFormEntity(params, JQBasicNetwork.UTF_8));
            System.out.println("123456");
            HttpResponse httpResponse = httpClient.execute(httpRequest);
            System.out.println("状态码: " + httpResponse.getStatusLine().getStatusCode());
            if (httpResponse.getStatusLine().getStatusCode() == 200) {
                strResult = EntityUtils.toString(httpResponse.getEntity());
            } else {
                strResult = "Error Response: " + httpResponse.getStatusLine().toString();
            }
        } catch (ClientProtocolException e) {
            strResult = e.getMessage().toString();
            e.printStackTrace();
        } catch (IOException e2) {
            strResult = e2.getMessage().toString();
            e2.printStackTrace();
        } catch (Exception e3) {
            strResult = e3.getMessage().toString();
            e3.printStackTrace();
        }
        Log.v("strResult", strResult);
        System.out.println("orr: " + strResult);
        return strResult;
    }
}
