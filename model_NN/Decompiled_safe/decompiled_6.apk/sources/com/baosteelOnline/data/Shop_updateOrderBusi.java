package com.baosteelOnline.data;

import android.content.Context;
import com.baosteelOnline.common.JQEncryptUtil;
import com.baosteelOnline.common.SysConfig;
import com.baosteelOnline.http.HRParameter;
import com.jianq.common.JQFrameworkConfig;
import com.jianq.helper.JQNetworkHelper;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import com.jianq.util.JQUtils;
import java.net.URLEncoder;
import org.json.JSONException;

public class Shop_updateOrderBusi {
    public String appcode = "com.baosight.ets";
    private Context context;
    public String deviceid = StringEx.Empty;
    private JQUICallBack httpCallBack = null;
    private boolean isNetwork;
    private String method = "query";
    public String network = StringEx.Empty;
    public String operateNo = StringEx.Empty;
    public String orderid = StringEx.Empty;
    public String os = StringEx.Empty;
    public String osVersion = StringEx.Empty;
    public String outOrderId = StringEx.Empty;
    public String parameter_userid = StringEx.Empty;
    public String parameter_usertokenid = StringEx.Empty;
    public String projectName = StringEx.Empty;
    public String resolution1 = StringEx.Empty;
    public String resolution2 = StringEx.Empty;
    private String tail = "iPlatMBS/AgentService";

    public Shop_updateOrderBusi(Context context2) {
        this.context = context2;
    }

    public void setHttpCallBack(JQUICallBack httpCallBack2) {
        this.httpCallBack = httpCallBack2;
    }

    public void iExecute() {
        String parameter_postdata = infoData();
        this.isNetwork = JQUtils.isNetworkAvailable(this.context);
        if (this.isNetwork) {
            JQNetworkHelper networkHelper = new JQNetworkHelper(this.context);
            networkHelper.progress = false;
            HRParameter parameter = null;
            try {
                HRParameter parameter2 = new HRParameter();
                try {
                    parameter2.setMethod(this.method);
                    parameter2.setBizObj(parameter_postdata);
                    parameter = parameter2;
                } catch (JSONException e) {
                    parameter = parameter2;
                }
            } catch (JSONException e2) {
            }
            networkHelper.open("post", String.valueOf(JQFrameworkConfig.getInst().getDomain()) + this.method);
            networkHelper.setCallBack(this.httpCallBack);
            System.out.println("parameter==>" + parameter.toString());
            networkHelper.send(parameter.toString());
            return;
        }
        System.out.println("============");
    }

    private String infoData() {
        this.projectName = new SysConfig().setSysconfigbsol().getProjectName();
        String parameter_postdata = "{'msgKey':'','attr':{'projectName':'" + this.projectName + "','operateNo ':'a7'," + "'methodName':'updateOrder','serviceName':'M1TE01'}," + "'blocks':{'i0':{'meta':{'columns':[" + "{'descName':' ','name':'orderid','pos':0}]}," + "'rows':[['" + this.outOrderId + "']]}}}";
        String encryptString = parameter_postdata;
        try {
            return URLEncoder.encode(JQEncryptUtil.encrypt(parameter_postdata));
        } catch (Exception e) {
            e.printStackTrace();
            return encryptString;
        }
    }
}
