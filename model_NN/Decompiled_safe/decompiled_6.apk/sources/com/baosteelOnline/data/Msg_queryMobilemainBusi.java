package com.baosteelOnline.data;

import android.content.Context;
import com.baosteelOnline.common.JQEncryptUtil;
import com.baosteelOnline.common.SysConfig;
import com.baosteelOnline.common.SysOsInfo;
import com.baosteelOnline.http.HRParameter;
import com.baosteelOnline.model.sysConfigModel;
import com.baosteelOnline.model.sysOsInfoModel;
import com.jianq.common.JQFrameworkConfig;
import com.jianq.helper.JQNetworkHelper;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import com.jianq.util.JQUtils;
import java.net.URLEncoder;
import org.json.JSONException;

public class Msg_queryMobilemainBusi {
    private Context context;
    private JQUICallBack httpCallBack = null;
    private boolean isNetwork;
    private String method = "query";
    public String parameter_hy = StringEx.Empty;
    private String tail = "iPlatMBS/AgentService";

    public Msg_queryMobilemainBusi(Context context2) {
        this.context = context2;
    }

    public void setHttpCallBack(JQUICallBack httpCallBack2) {
        this.httpCallBack = httpCallBack2;
    }

    public void iExecute() {
        String parameter_postdata = infoData();
        this.isNetwork = JQUtils.isNetworkAvailable(this.context);
        if (this.isNetwork) {
            JQNetworkHelper networkHelper = new JQNetworkHelper(this.context);
            networkHelper.progress = false;
            HRParameter parameter = null;
            try {
                HRParameter parameter2 = new HRParameter();
                try {
                    parameter2.setMethod(this.method);
                    parameter2.setBizObj(parameter_postdata);
                    parameter = parameter2;
                } catch (JSONException e) {
                    parameter = parameter2;
                }
            } catch (JSONException e2) {
            }
            networkHelper.open("post", String.valueOf(JQFrameworkConfig.getInst().getDomain()) + this.method);
            networkHelper.setCallBack(this.httpCallBack);
            System.out.println("parameter==>" + parameter.toString());
            networkHelper.send(parameter.toString());
            return;
        }
        System.out.println("============");
    }

    private String infoData() {
        sysConfigModel sysconfigbs = new SysConfig().setSysconfigbs();
        sysOsInfoModel sysOsInfo = new SysOsInfo().sysOsInfo(this.context);
        String jsonStr = "{'msgKey':'','attr':{'serviceMethodName':'queryMobilemain'},'blocks':{'i0':{'meta':{'columns':[{'descName':'','name':'hydm','pos':0},{'descName':'','name':'zc','pos':0}]},'rows':[['','" + "2" + "']]}}}";
        System.out.println("jsonStr---:" + jsonStr);
        String encryptString = jsonStr;
        try {
            return URLEncoder.encode(JQEncryptUtil.encrypt(jsonStr));
        } catch (Exception e) {
            e.printStackTrace();
            return encryptString;
        }
    }
}
