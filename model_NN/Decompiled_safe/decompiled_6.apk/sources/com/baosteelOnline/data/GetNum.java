package com.baosteelOnline.data;

import android.content.Context;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.common.JQEncryptUtil;
import com.baosteelOnline.http.HttpsCallBack;
import com.baosteelOnline.http.HttpsConnect;
import com.jianq.misc.StringEx;
import java.net.URLEncoder;

public class GetNum {
    private Context context;
    private HttpsCallBack httpsCallBack = null;
    private String tail = "queryType13";

    public void setHttpsCallBack(HttpsCallBack httpsCallBack2) {
        this.httpsCallBack = httpsCallBack2;
    }

    public GetNum(Context context2) {
        this.context = context2;
    }

    public void iExecute() {
        try {
            String post_data = infoData();
            HttpsConnect httpsConnect = new HttpsConnect(this.context);
            httpsConnect.setMethod(this.tail);
            httpsConnect.setData(post_data);
            httpsConnect.setSessionId(ObjectStores.getInst().getObject("sessionId").toString());
            httpsConnect.setHttpsCallBack(this.httpsCallBack);
            httpsConnect.send();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String infoData() throws Exception {
        return URLEncoder.encode("{'biz':\"" + JQEncryptUtil.encrypt(StringEx.Empty) + "\"," + "'sys':{'appcode':'','compressdata':'false','encryptdata':'false'," + "'keycode':'pkznf6585535555660675056396297413','method':'queryType13'}}");
    }
}
