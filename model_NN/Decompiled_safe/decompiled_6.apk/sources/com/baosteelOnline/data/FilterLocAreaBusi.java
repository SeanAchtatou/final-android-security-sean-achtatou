package com.baosteelOnline.data;

import android.content.Context;
import com.baosteelOnline.common.JQEncryptUtil;
import com.baosteelOnline.common.SysConfig;
import com.baosteelOnline.http.HRParameter;
import com.jianq.common.JQFrameworkConfig;
import com.jianq.helper.JQNetworkHelper;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import com.jianq.util.JQUtils;
import java.net.URLEncoder;
import org.json.JSONException;

public class FilterLocAreaBusi {
    public String appcode = "com.baosight.ets";
    public String buyerid = StringEx.Empty;
    public String contactid = StringEx.Empty;
    private Context context;
    public String deviceid = StringEx.Empty;
    private JQUICallBack httpCallBack = null;
    private boolean isNetwork;
    private String method = "query";
    public String methodName = "queryLocArea";
    public String network = StringEx.Empty;
    public String operateNo = StringEx.Empty;
    public String os = StringEx.Empty;
    public String osVersion = StringEx.Empty;
    public String parameter_userid = StringEx.Empty;
    public String parameter_usertokenid = StringEx.Empty;
    public String projectName = StringEx.Empty;
    public String resolution1 = StringEx.Empty;
    public String resolution2 = StringEx.Empty;
    private String tail = "iPlatMBS/AgentService";

    public void setHttpCallBack(JQUICallBack httpCallBack2) {
        this.httpCallBack = httpCallBack2;
    }

    public FilterLocAreaBusi(Context context2) {
        this.context = context2;
    }

    public void iExecute() {
        String parameter_postdata = infoDate();
        this.isNetwork = JQUtils.isNetworkAvailable(this.context);
        if (this.isNetwork) {
            JQNetworkHelper networkHelper = new JQNetworkHelper(this.context);
            networkHelper.progress = false;
            HRParameter parameter = null;
            try {
                HRParameter parameter2 = new HRParameter();
                try {
                    parameter2.setMethod(this.method);
                    parameter2.setBizObj(parameter_postdata);
                    parameter = parameter2;
                } catch (JSONException e) {
                    parameter = parameter2;
                }
            } catch (JSONException e2) {
            }
            networkHelper.open("post", String.valueOf(JQFrameworkConfig.getInst().getDomain()) + this.method);
            networkHelper.setCallBack(this.httpCallBack);
            System.out.println("parameter==>" + parameter.toString());
            networkHelper.send(parameter.toString());
            return;
        }
        System.out.println("============");
    }

    private String infoDate() {
        this.projectName = new SysConfig().setSysconfig().getProjectName();
        String parameter_postdata = "{'msgKey':'','attr':{'projectName':'" + this.projectName + "'," + "'operateNo ':'A18','methodName':'" + this.methodName + "','serviceName':'M1TE01'}," + "'blocks':{'i0':{'meta':{'columns':[" + "{'descName':' ','name':'buyerid','pos':0}," + "{'descName':' ','name':'type3','pos':0}]}," + "'rows':[['" + this.buyerid + "'" + ",'']]}}}";
        String encryptString = parameter_postdata;
        try {
            return URLEncoder.encode(JQEncryptUtil.encrypt(parameter_postdata));
        } catch (Exception e) {
            e.printStackTrace();
            return encryptString;
        }
    }
}
