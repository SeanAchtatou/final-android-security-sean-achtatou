package com.baosteelOnline.data;

import android.content.Context;
import android.util.Log;
import com.baosteelOnline.common.JQEncryptUtil;
import com.baosteelOnline.common.SysConfig;
import com.baosteelOnline.http.HRParameter;
import com.jianq.common.JQFrameworkConfig;
import com.jianq.helper.JQNetworkHelper;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import com.jianq.util.JQUtils;
import java.net.URLEncoder;
import org.json.JSONException;

public class NoticeBusi {
    private Context context;
    private JQUICallBack httpCallBack = null;
    private boolean isNetwork;
    public String market;
    private String method = "query";
    public String numOfBtn;
    public String pageNum;
    private String projectName;
    public String startRow;
    public String type;

    public void setHttpCallBack(JQUICallBack httpCallBack2) {
        this.httpCallBack = httpCallBack2;
    }

    public NoticeBusi(Context context2) {
        this.context = context2;
    }

    public void iExecute() {
        String parameter_postdata = infoData();
        this.isNetwork = JQUtils.isNetworkAvailable(this.context);
        if (this.isNetwork) {
            JQNetworkHelper networkHelper = new JQNetworkHelper(this.context);
            networkHelper.progress = false;
            HRParameter parameter = null;
            try {
                HRParameter parameter2 = new HRParameter();
                try {
                    parameter2.setMethod(this.method);
                    parameter2.setBizObj(parameter_postdata);
                    parameter = parameter2;
                } catch (JSONException e) {
                    parameter = parameter2;
                }
            } catch (JSONException e2) {
            }
            networkHelper.open("post", String.valueOf(JQFrameworkConfig.getInst().getDomain()) + this.method);
            networkHelper.setCallBack(this.httpCallBack);
            System.out.println("parameter==>" + parameter.toString());
            networkHelper.send(parameter.toString());
            return;
        }
        System.out.println("============");
    }

    private String infoData() {
        this.projectName = new SysConfig().setSysconfigbsol().getProjectName();
        String jsonStr = StringEx.Empty;
        if (this.numOfBtn.equals("平台公告按钮")) {
            jsonStr = "{'msgKey':'','attr':{'projectName':'" + this.projectName + "','serviceMethodName':'queryBidPubmsg4Move'," + "'showcar':'1','parameter_czy':'','parameter_hy':''," + "'serviceName':'M1TE01'}," + "'blocks':{'i0':{'meta':{'columns':[" + "{'pos':0,'name':'startRow','descName':'第几页'}," + "{'pos':1,'name':'pageNum','descName':'每页几行'}," + "{'pos':2,'name':'type','descName':'类型'}," + "{'pos':3,'name':'market','descName':'专场'}]}," + "'rows':[['" + this.startRow + "','" + this.pageNum + "','2','" + this.market + "']]}}}";
        } else if (this.numOfBtn.equals("交易预告按钮")) {
            jsonStr = "{'msgKey':'','attr':{'projectName':'" + this.projectName + "','serviceMethodName':'queryBidPubmsg4Move'," + "'showcar':'1','parameter_czy':'','parameter_hy':''," + "'serviceName':'M1TE01'}," + "'blocks':{'i0':{'meta':{'columns':[" + "{'pos':0,'name':'startRow','descName':'第几页'}," + "{'pos':1,'name':'pageNum','descName':'每页几行'}," + "{'pos':2,'name':'type','descName':'类型'}," + "{'pos':3,'name':'market','descName':'专场'}]}," + "'rows':[['" + this.startRow + "','" + this.pageNum + "','0','" + this.market + "']]}}}";
        } else if (this.numOfBtn.equals("竞价公告")) {
            jsonStr = "{'msgKey':'','attr':{'projectName':'" + this.projectName + "','serviceMethodName':'queryBidPubmsg4Move'," + "'showcar':'1','parameter_czy':'','parameter_hy':''," + "'serviceName':'M1TE01'}," + "'blocks':{'i0':{'meta':{'columns':[" + "{'pos':0,'name':'startRow','descName':'第几页'}," + "{'pos':1,'name':'pageNum','descName':'每页几行'}," + "{'pos':2,'name':'type','descName':'类型'}," + "{'pos':3,'name':'market','descName':'专场'}]}," + "'rows':[['" + this.startRow + "','" + this.pageNum + "','1','" + this.market + "']]}}}";
        }
        Log.d("未加密的请求", jsonStr);
        String encryptString = jsonStr;
        try {
            return URLEncoder.encode(JQEncryptUtil.encrypt(jsonStr));
        } catch (Exception e) {
            e.printStackTrace();
            return encryptString;
        }
    }
}
