package com.baosteelOnline.data;

import android.content.Context;
import android.util.Log;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.common.JQEncryptUtil;
import com.baosteelOnline.common.Utils;
import com.jianq.misc.StringEx;
import java.net.URLEncoder;

public class RequestBidData extends RequestDataBase {
    private String ContractId = StringEx.Empty;
    public String czm = StringEx.Empty;
    public String endDate = StringEx.Empty;
    public String hyh = StringEx.Empty;
    public String id = StringEx.Empty;
    public String jsonStr;
    private String mark = "2";
    public String pageNum = "1";
    public String requestData;
    private String sellerName = StringEx.Empty;
    public String sessionCode = StringEx.Empty;
    public String startDate = StringEx.Empty;
    public String startRow = "0";
    public String xh = StringEx.Empty;

    public String getSellerName() {
        return this.sellerName;
    }

    public void setSellerName(String sellerName2) {
        this.sellerName = sellerName2;
    }

    public String getContractId() {
        return this.ContractId;
    }

    public void setContractId(String contractId) {
        this.ContractId = contractId;
    }

    public String getPageNum() {
        return this.pageNum;
    }

    public void setPageNum(String pageNum2) {
        this.pageNum = pageNum2;
    }

    public RequestBidData(Context context) {
        super(context);
    }

    public String infoDate() {
        String project = Utils.getNetConfigProperties().getProperty("project");
        String projectName = StringEx.Empty;
        if (project.equals("official")) {
            projectName = Utils.getNetConfigProperties().getProperty("BsolprojectName");
        }
        if (project.equals("test")) {
            projectName = Utils.getNetConfigProperties().getProperty("BsolprojectNametest");
        }
        if (getRequestData().equals("竞价公告列表")) {
            this.jsonStr = "{'msgKey':'','attr':{'projectName':'" + projectName + "','serviceMethodName':'queryBidPubmsg4Move','showcar':'1','parameter_czy':'" + ObjectStores.getInst().getObject("parameter_czy") + "','parameter_hy':'" + ObjectStores.getInst().getObject("parameter_hy") + "','serviceName':'M1TE01'},'blocks':{'i0':{'meta':{'columns':[{'descName':'','name':'startRow','pos':0},{'descName':'','name':'pageNum','pos':0},{'descName':'','name':'type','pos':0},{'descName':'','name':'market','pos':0}]},'rows':[['" + getPageNum() + "','10','1','" + getMark() + "']]}}}";
        } else if (getRequestData().equals("竞价公告详情")) {
            this.jsonStr = "{'msgKey':'','attr':{'projectName':'" + projectName + "','serviceMethodName':'getBidPubmsgById4Move','showcar':'1','parameter_czy':'" + ObjectStores.getInst().getObject("parameter_czy") + "','parameter_hy':'" + ObjectStores.getInst().getObject("parameter_hy") + "','serviceName':'M1TE01'},'blocks':{'i0':{'meta':{'columns':[{'descName':'','name':'hydm','pos':0},{'descName':'','name':'ggid','pos':0}]},'rows':[['" + ObjectStores.getInst().getObject("parameter_hy") + "','" + getId() + "']]}}}";
        } else if (getRequestData().equals("到货确认列表")) {
            this.jsonStr = "{'msgKey':'','attr':{'projectName':'" + projectName + "','serviceMethodName':'getNotUpdateStatus4Move','showcar':'1','parameter_czy':'" + ObjectStores.getInst().getObject("parameter_czy") + "','parameter_hy':'" + ObjectStores.getInst().getObject("parameter_hy") + "','serviceName':'M1TE01'},'blocks':{'i0':{'meta':{'columns':[{'descName':'','name':'limit','pos':0},{'descName':'','name':'offset','pos':0},{'descName':'','name':'hydm','pos':0}]},'rows':[['10','" + getPageNum() + "','" + ObjectStores.getInst().getObject("parameter_hy") + "']]}}}";
        } else if (getRequestData().equals("卖家协议")) {
            this.jsonStr = "{'msgKey':'','attr':{'projectName':'" + projectName + "','serviceMethodName':'showSellAgreement4Move','showcar':'1','parameter_czy':'" + ObjectStores.getInst().getObject("parameter_czy") + "','parameter_hy':'" + ObjectStores.getInst().getObject("parameter_hy") + "','serviceName':'M1TE01'},'blocks':{'i0':{'meta':{'columns':[{'descName':'','name':'wtid','pos':0}]},'rows':[['" + getId() + "']]}}}";
        } else if (getRequestData().equals("竞价协议")) {
            this.jsonStr = "{'msgKey':'','attr':{'projectName':'" + projectName + "','serviceMethodName':'showAgreement4Move','showcar':'1','parameter_czy':'" + ObjectStores.getInst().getObject("parameter_czy") + "','parameter_hy':'" + ObjectStores.getInst().getObject("parameter_hy") + "','serviceName':'M1TE01'},'blocks':{'i0':{'meta':{'columns':[{'descName':'','name':'wtid','pos':0},{'descName':'','name':'hydm','pos':0},{'descName':'','name':'czy','pos':0}]},'rows':[['" + getId() + "','" + ObjectStores.getInst().getObject("parameter_hy") + "','" + ObjectStores.getInst().getObject("parameter_czy") + "']]}}}";
        } else if (getRequestData().equals("协议条款同意")) {
            this.jsonStr = "{'msgKey':'','attr':{'projectName':'" + projectName + "','serviceMethodName':'joinBuy4Move','showcar':'1','parameter_czy':'" + ObjectStores.getInst().getObject("parameter_czy") + "','parameter_hy':'" + ObjectStores.getInst().getObject("parameter_hy") + "','serviceName':'M1TE01'},'blocks':{'i0':{'meta':{'columns':[{'descName':'','name':'wtid','pos':0},{'descName':'','name':'hydm','pos':0},{'descName':'','name':'czy','pos':0}]},'rows':[['" + getId() + "','" + ObjectStores.getInst().getObject("parameter_hy") + "','" + ObjectStores.getInst().getObject("parameter_czy") + "']]}}}";
        } else if (getRequestData().equals("拼盘信息查询")) {
            this.jsonStr = "{'msgKey':'','attr':{'projectName':'" + projectName + "','serviceMethodName':'queryAuctionPps4Move','showcar':'1','parameter_czy':'" + ObjectStores.getInst().getObject("parameter_czy") + "','parameter_hy':'" + ObjectStores.getInst().getObject("parameter_hy") + "','serviceName':'M1TE01'},'blocks':{'i0':{'meta':{'columns':[{'descName':'','name':'hydm','pos':0},{'descName':'','name':'limit','pos':0},{'descName':'','name':'offset','pos':0},{'descName':'','name':'czy','pos':0}]},'rows':[['" + ObjectStores.getInst().getObject("parameter_hy") + "','10','" + getPageNum() + "','" + ObjectStores.getInst().getObject("parameter_czy") + "']]}}}";
        } else if (getRequestData().equals("竞价场次列表")) {
            this.jsonStr = "{'msgKey':'','attr':{'projectName':'" + projectName + "','serviceMethodName':'queryAuctionWts4Move','showcar':'1','parameter_czy':'" + "','parameter_hy':'" + "','serviceName':'M1TE01'},'blocks':{'i0':{'meta':{'columns':[{'descName':'','name':'hydm','pos':0},{'descName':'','name':'zc','pos':0},{'descName':'','name':'wtzt','pos':0},{'descName':'','name':'rqStart','pos':0},{'descName':'','name':'rqEnd','pos':0}]},'rows':[['" + ObjectStores.getInst().getObject("parameter_hy") + "','" + getMark() + "','" + getSessionCode() + "','" + getStartDate() + "','" + getEndDate() + "']]}}}";
        } else if (getRequestData().equals("合同详情")) {
            this.jsonStr = "{'msgKey':'','attr':{'projectName':'" + projectName + "','serviceMethodName':'pactViewSourceService','showcar':'1','parameter_czy':'" + ObjectStores.getInst().getObject("parameter_czy") + "','parameter_hy':'" + ObjectStores.getInst().getObject("parameter_hy") + "','serviceName':'M1TE01'},'blocks':{'i0':{'meta':{'columns':[{'descName':'','name':'hth','pos':0}]},'rows':[['" + getId() + "']]}}}";
        } else if (getRequestData().equals("捆包信息")) {
            this.jsonStr = "{'msgKey':'','attr':{'projectName':'" + projectName + "','serviceMethodName':'pactViewPackService','showcar':'1','parameter_czy':'" + ObjectStores.getInst().getObject("parameter_czy") + "','parameter_hy':'" + ObjectStores.getInst().getObject("parameter_hy") + "','serviceName':'M1TE01'},'blocks':{'i0':{'meta':{'columns':[{'descName':'','name':'hth','pos':0},{'descName':'','name':'xh','pos':0}]},'rows':[['" + getId() + "','" + getXh() + "']]}}}";
        } else if (getRequestData().equals("竞价大厅列表")) {
            this.jsonStr = "{'msgKey':'','attr':{'projectName':'" + projectName + "','serviceMethodName':'queryAuctionPps4Move','showcar':'1','parameter_czy':'" + ObjectStores.getInst().getObject("parameter_czy") + "','parameter_hy':'" + ObjectStores.getInst().getObject("parameter_hy") + "','serviceName':'M1TE01'},'blocks':{'i0':{'meta':{'columns':[{'descName':'','name':'hydm','pos':0},{'descName':'','name':'limit','pos':0},{'descName':'','name':'offset','pos':0},{'descName':'','name':'czy','pos':0}]},'rows':[['" + ObjectStores.getInst().getObject("parameter_hy") + "','10','" + getPageNum() + "','" + ObjectStores.getInst().getObject("parameter_czy") + "']]}}}";
        } else if (getRequestData().equals("关注拼盘设置")) {
            this.jsonStr = "{'msgKey':'','attr':{'projectName':'" + projectName + "','serviceMethodName':'setGz4Move','showcar':'1','parameter_czy':'" + ObjectStores.getInst().getObject("parameter_czy") + "','parameter_hy':'" + ObjectStores.getInst().getObject("parameter_hy") + "','serviceName':'M1TE01'},'blocks':{'i0':{'meta':{'columns':[{'descName':'','name':'id','pos':0},{'descName':'','name':'hydm','pos':0},{'descName':'','name':'flag','pos':0}]},'rows':[['" + getId() + "','" + ObjectStores.getInst().getObject("parameter_czy") + "','" + getXh() + "']]}}}";
        } else if (getRequestData().equals("到货确认按钮")) {
            this.jsonStr = "{'msgKey':'','attr':{'projectName':'" + projectName + "','serviceMethodName':'updateStatus4Move','showcar':'1','parameter_czy':'" + ObjectStores.getInst().getObject("parameter_czy") + "','parameter_hy':'" + ObjectStores.getInst().getObject("parameter_hy") + "','serviceName':'M1TE01'},'blocks':{'i0':{'meta':{'columns':[{'descName':'','name':'fphm','pos':0},{'descName':'','name':'dhczy','pos':0}]},'rows':[['" + getId() + "','" + ObjectStores.getInst().getObject("parameter_hy") + "']]}}}";
        } else if (getRequestData().equals("关注拼盘信息查询")) {
            this.jsonStr = "{'msgKey':'','attr':{'projectName':'" + projectName + "','serviceMethodName':'queryGzAuctionPps4Move','showcar':'1','parameter_czy':'" + ObjectStores.getInst().getObject("parameter_czy") + "','parameter_hy':'" + ObjectStores.getInst().getObject("parameter_hy") + "','serviceName':'M1TE01'},'blocks':{'i0':{'meta':{'columns':[{'descName':'','name':'hydm','pos':0},{'descName':'','name':'czy','pos':0}]},'rows':[['" + ObjectStores.getInst().getObject("parameter_hy") + "','" + ObjectStores.getInst().getObject("parameter_czy") + "']]}}}";
        } else if (getRequestData().equals("竞价出价")) {
            this.jsonStr = "{'msgKey':'','attr':{'projectName':'" + projectName + "','serviceMethodName':'saveAuction4Move','showcar':'1','parameter_czy':'" + ObjectStores.getInst().getObject("parameter_czy") + "','parameter_hy':'" + ObjectStores.getInst().getObject("parameter_hy") + "','serviceName':'M1TE01'},'blocks':{'i0':{'meta':{'columns':[{'descName':'','name':'bjlb','pos':0},{'descName':'','name':'price','pos':0},{'descName':'','name':'id','pos':0},{'descName':'','name':'userid','pos':0},{'descName':'','name':'memberId','pos':0}]},'rows':[['" + getXh() + "','" + getEndDate() + "','" + getId() + "','" + ObjectStores.getInst().getObject("parameter_czy") + "','" + ObjectStores.getInst().getObject("parameter_hy") + "']]}}}";
        } else if (getRequestData().equals("退出竞价")) {
            this.jsonStr = "{'msgKey':'','attr':{'projectName':'" + projectName + "','serviceMethodName':'quitWt4Move','showcar':'1','parameter_czy':'" + ObjectStores.getInst().getObject("parameter_czy") + "','parameter_hy':'" + ObjectStores.getInst().getObject("parameter_hy") + "','serviceName':'M1TE01'},'blocks':{'i0':{'meta':{'columns':[{'descName':'','name':'wtid','pos':0},{'descName':'','name':'hydm','pos':0},{'descName':'','name':'czy','pos':0}]},'rows':[['" + getId() + "','" + ObjectStores.getInst().getObject("parameter_hy") + "','" + ObjectStores.getInst().getObject("parameter_czy") + "']]}}}";
        } else if (getRequestData().equals("登陆2")) {
            this.jsonStr = "{'msgKey':'','attr':{'projectName':'" + projectName + "','methodName':'doDefault','serviceMethodName':'bspToBsteelUserService','showcar':'1','parameter_czy':'" + ObjectStores.getInst().getObject("G_User") + "','parameter_hy':'','serviceName':'M1TE01'},'blocks':{'i0':{'meta':{'columns':[]},'rows':[[]]}}}";
        } else if (getRequestData().equals("登陆1")) {
            this.jsonStr = "{'loginPassword':'" + ObjectStores.getInst().getObject("inputPassword") + "','umcLoginName':'" + ObjectStores.getInst().getObject("inputUserName") + "'}";
        } else if (getRequestData().equals("推送已读")) {
            this.jsonStr = "{'msgKey':'','attr':{'projectName':'" + projectName + "'," + "'methodName':'updateReadMessages'," + "'serviceName':'D1MB0102'}," + "'blocks':{'i0':{'meta':{'columns':[" + "{'descName':' ','name':'customerId','pos':0}]}," + "'rows':[[" + "'" + getContractId() + "'" + "]]}}}";
        } else if (getRequestData().equals("竞价结果列表")) {
            this.jsonStr = "{'msgKey':'','attr':{'serviceName':'M1TE01','projectName':'" + projectName + "','serviceMethodName':'queryMyJoin4Move','showcar':'1','parameter_czy':'" + ObjectStores.getInst().getObject("parameter_czy") + "','parameter_hy':'" + ObjectStores.getInst().getObject("parameter_hy") + "'},'blocks':{'i0':{'meta':{'columns':[{'descName':'','name':'hydm','pos':0},{'descName':'','name':'limit','pos':0},{'descName':'','name':'offset','pos':0},{'descName':'','name':'hz','pos':0},{'descName':'','name':'rqStart','pos':0},{'descName':'','name':'rqEnd','pos':0}]},'rows':[['" + ObjectStores.getInst().getObject("parameter_hy") + "','10','" + getPageNum() + "','" + getSellerName() + "','" + getStartDate() + "','" + getEndDate() + "']]}}}";
        } else if (getRequestData().equals("竞价结果列表2")) {
            this.jsonStr = "{'msgKey':'','attr':{'serviceName':'M1TE01','projectName':'" + projectName + "','serviceMethodName':'queryWtResult4Move'" + "},'blocks':{'i0':{'meta':{'columns':[{'descName':'','name':'wtid','pos':0},{'descName':'','name':'limit','pos':0},{'descName':'','name':'offset','pos':0}]},'rows':[['" + getId() + "','10','" + getPageNum() + "']]}}}";
        }
        String encryptString = this.jsonStr;
        try {
            encryptString = URLEncoder.encode(JQEncryptUtil.encrypt(this.jsonStr));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d("请求的指令,未加密：", this.jsonStr);
        return encryptString;
    }

    public String getRequestData() {
        return this.requestData;
    }

    public void setRequestData(String requestData2) {
        this.requestData = requestData2;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getSessionCode() {
        return this.sessionCode;
    }

    public void setSessionCode(String sessionCode2) {
        this.sessionCode = sessionCode2;
    }

    public String getStartDate() {
        return this.startDate;
    }

    public void setStartDate(String startDate2) {
        this.startDate = startDate2;
    }

    public String getEndDate() {
        return this.endDate;
    }

    public void setEndDate(String endDate2) {
        this.endDate = endDate2;
    }

    public String getXh() {
        return this.xh;
    }

    public void setXh(String xh2) {
        this.xh = xh2;
    }

    public String getStartRow() {
        return this.startRow;
    }

    public void setStartRow(String startRow2) {
        this.startRow = startRow2;
    }

    public String getMark() {
        if (ObjectStores.getInst().getObject("formX") == "formX") {
            this.mark = "1";
        } else {
            this.mark = "2";
        }
        return this.mark;
    }
}
