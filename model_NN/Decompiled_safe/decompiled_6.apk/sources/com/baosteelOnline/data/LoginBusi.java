package com.baosteelOnline.data;

import com.andframework.business.BaseBusi;
import com.andframework.common.ObjectStores;
import com.andframework.myinterface.UiCallBack;
import com.baosteelOnline.common.SysConfig;
import com.baosteelOnline.data_parse.LoginParse;
import com.jianq.misc.StringEx;
import java.util.HashMap;
import java.util.Map;

public class LoginBusi extends BaseBusi {
    public String appcode = StringEx.Empty;
    public String network = StringEx.Empty;
    public String operateNo = StringEx.Empty;
    public String os = StringEx.Empty;
    public String osVersion = StringEx.Empty;
    public String parameter_clienidtversion = StringEx.Empty;
    public String parameter_clienttypeid = StringEx.Empty;
    public String parameter_deviceid = StringEx.Empty;
    public String parameter_postdata = StringEx.Empty;
    public String parameter_usertokenid = StringEx.Empty;
    public String resolution1 = StringEx.Empty;
    public String resolution2 = StringEx.Empty;
    private String tail = "iPlatMBS/LoginService";

    public LoginBusi(UiCallBack bCallBack) {
        super(bCallBack, LoginParse.class);
    }

    /* access modifiers changed from: protected */
    public void prepare() {
        this.reqParam = this.tail;
        this.appcode = new SysConfig().setAppCode().getProjectName();
        Map<String, String> mapnormal = new HashMap<>();
        mapnormal.put("appcode", this.appcode);
        mapnormal.put("parameter_encryptdata", "false");
        mapnormal.put("parameter_compressdata", "false");
        mapnormal.put("parameter_postdata", this.parameter_postdata);
        mapnormal.put("parameter_userid", (String) ObjectStores.getInst().getObject("inputUserName"));
        mapnormal.put("parameter_password", (String) ObjectStores.getInst().getObject("inputPassword"));
        mapnormal.put("network", this.network);
        mapnormal.put("os", this.os);
        mapnormal.put("osVersion", this.osVersion);
        mapnormal.put("resolution1", this.resolution1);
        mapnormal.put("resolution2", this.resolution2);
        mapnormal.put("parameter_deviceid", this.parameter_deviceid);
        mapnormal.put("parameter_clienttypeid", this.parameter_clienttypeid);
        mapnormal.put("parameter_usertokenid", this.parameter_usertokenid);
        mapnormal.put("operateNo", this.operateNo);
        mapnormal.put("parameter_clienidtversion", this.parameter_clienidtversion);
        setFormData(mapnormal);
    }
}
