package com.baosteelOnline.data;

import android.content.Context;
import com.baosteelOnline.common.JQEncryptUtil;
import com.baosteelOnline.common.SysConfig;
import com.baosteelOnline.http.HRParameter;
import com.jianq.common.JQFrameworkConfig;
import com.jianq.helper.JQNetworkHelper;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import com.jianq.util.JQUtils;
import java.net.URLEncoder;
import org.json.JSONException;

public class Shop_queryShoppingPacksBusi {
    public static String appcode = "com.baosight.ets";
    public static String deviceid = StringEx.Empty;
    public static String network = StringEx.Empty;
    public static String operateNo = StringEx.Empty;
    public static String os = StringEx.Empty;
    public static String osVersion = StringEx.Empty;
    public static String parameter_userid = StringEx.Empty;
    public static String parameter_usertokenid = StringEx.Empty;
    public static String productId = StringEx.Empty;
    private static String projectName = StringEx.Empty;
    public static String resolution1 = StringEx.Empty;
    public static String resolution2 = StringEx.Empty;
    public static String shoppingId = StringEx.Empty;
    private Context context;
    private JQUICallBack httpCallBack = null;
    private boolean isNetwork;
    private String method = "query";
    private String tail = "iPlatMBS/AgentService";

    public Shop_queryShoppingPacksBusi(Context context2) {
        this.context = context2;
    }

    public void setHttpCallBack(JQUICallBack httpCallBack2) {
        this.httpCallBack = httpCallBack2;
    }

    public void iExecute() {
        String parameter_postdata = infoData();
        this.isNetwork = JQUtils.isNetworkAvailable(this.context);
        if (this.isNetwork) {
            JQNetworkHelper networkHelper = new JQNetworkHelper(this.context);
            networkHelper.progress = false;
            HRParameter parameter = null;
            try {
                HRParameter parameter2 = new HRParameter();
                try {
                    parameter2.setMethod(this.method);
                    parameter2.setBizObj(parameter_postdata);
                    parameter = parameter2;
                } catch (JSONException e) {
                    parameter = parameter2;
                }
            } catch (JSONException e2) {
            }
            networkHelper.open("post", String.valueOf(JQFrameworkConfig.getInst().getDomain()) + this.method);
            networkHelper.setCallBack(this.httpCallBack);
            System.out.println("parameter==>" + parameter.toString());
            networkHelper.send(parameter.toString());
            return;
        }
        System.out.println("============");
    }

    private String infoData() {
        projectName = new SysConfig().setSysconfigbsol().getProjectName();
        String parameter_postdata = String.valueOf(String.valueOf(String.valueOf(String.valueOf("{'msgKey':'','attr':{'projectName':'" + projectName + "'" + ",'operateNo ':'A1','methodName':'queryShoppingPacks','serviceName':'M1TE01'}," + "'blocks':{'i0':{'meta':{'columns':[") + "{'descName':' ','name':'shoppingId','pos':0},") + "{'descName':' ','name':'productId','pos':0}") + "]},'rows':[['" + shoppingId + "','" + productId + "'") + "]]}}}";
        System.out.println("parameter_postdata:  " + parameter_postdata);
        String encryptString = parameter_postdata;
        try {
            return URLEncoder.encode(JQEncryptUtil.encrypt(parameter_postdata));
        } catch (Exception e) {
            e.printStackTrace();
            return encryptString;
        }
    }
}
