package com.baosteelOnline.data;

import android.content.Context;
import com.baosteelOnline.common.JQEncryptUtil;
import com.baosteelOnline.common.SysConfig;
import com.baosteelOnline.http.HRParameter;
import com.jianq.common.JQFrameworkConfig;
import com.jianq.helper.JQNetworkHelper;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import com.jianq.util.JQUtils;
import java.net.URLEncoder;
import org.json.JSONException;

public class RequestDataBase {
    public String appcode = "com.baosight.ets";
    public Context context;
    public String deviceid = StringEx.Empty;
    public JQUICallBack httpCallBack = null;
    public boolean isNetwork;
    public String method = "query";
    public String methodForLogin = "login";
    public String network = StringEx.Empty;
    public String operateNo = StringEx.Empty;
    public String os = StringEx.Empty;
    public String osVersion = StringEx.Empty;
    public String parameter_postdata = StringEx.Empty;
    public String parameter_userid = StringEx.Empty;
    public String parameter_usertokenid = StringEx.Empty;
    public String projectName = StringEx.Empty;
    public String resolution1 = StringEx.Empty;
    public String resolution2 = StringEx.Empty;
    private String tail = "iPlatMBS/AgentService";

    public RequestDataBase(Context context2) {
        this.context = context2;
    }

    public void setHttpCallBack(JQUICallBack httpCallBack2) {
        this.httpCallBack = httpCallBack2;
    }

    public void iExecute() {
        String parameter_postdata2 = infoDate();
        this.isNetwork = JQUtils.isNetworkAvailable(this.context);
        if (this.isNetwork) {
            JQNetworkHelper networkHelper = new JQNetworkHelper(this.context);
            networkHelper.progress = false;
            HRParameter parameter = null;
            try {
                HRParameter parameter2 = new HRParameter();
                try {
                    parameter2.setMethod(this.method);
                    parameter2.setBizObj(parameter_postdata2);
                    parameter = parameter2;
                } catch (JSONException e) {
                    parameter = parameter2;
                }
            } catch (JSONException e2) {
            }
            networkHelper.open("post", String.valueOf(JQFrameworkConfig.getInst().getDomain()) + this.method);
            networkHelper.setCallBack(this.httpCallBack);
            System.out.println("parameter==>" + parameter.toString());
            networkHelper.send(parameter.toString());
            return;
        }
        System.out.println("============");
    }

    public void iExecuteForLogin1() {
        String parameter_postdata2 = infoDate();
        this.isNetwork = JQUtils.isNetworkAvailable(this.context);
        if (this.isNetwork) {
            JQNetworkHelper networkHelper = new JQNetworkHelper(this.context);
            networkHelper.progress = false;
            HRParameter parameter = null;
            try {
                HRParameter parameter2 = new HRParameter();
                try {
                    parameter2.setMethod(this.methodForLogin);
                    parameter2.setBizObj(parameter_postdata2);
                    parameter = parameter2;
                } catch (JSONException e) {
                    parameter = parameter2;
                }
            } catch (JSONException e2) {
            }
            networkHelper.open("post", String.valueOf(JQFrameworkConfig.getInst().getDomain()) + this.methodForLogin);
            networkHelper.setCallBack(this.httpCallBack);
            System.out.println("parameter==>" + parameter.toString());
            networkHelper.send(parameter.toString());
            return;
        }
        System.out.println("============");
    }

    public String infoDate() {
        this.projectName = new SysConfig().setSysconfigbsol().getProjectName();
        String jsonStr = "{'msgKey':'','attr':{'projectName':'" + this.projectName + "','operateNo ':'A23'," + "'methodName':'doDefault','serviceMethodName':'breedNameService'," + "'serviceName':'M1TE01'},'blocks':{'i0':{'meta':{'columns':[]}," + "'rows':[[]]}}}";
        String encryptString = jsonStr;
        try {
            return URLEncoder.encode(JQEncryptUtil.encrypt(jsonStr));
        } catch (Exception e) {
            e.printStackTrace();
            return encryptString;
        }
    }
}
