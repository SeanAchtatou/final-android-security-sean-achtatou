package com.baosteelOnline.data;

import android.content.Context;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.common.JQEncryptUtil;
import com.baosteelOnline.common.SysConfig;
import com.baosteelOnline.common.SysOsInfo;
import com.baosteelOnline.http.HRParameter;
import com.baosteelOnline.model.sysConfigModel;
import com.baosteelOnline.model.sysOsInfoModel;
import com.jianq.common.JQFrameworkConfig;
import com.jianq.helper.JQNetworkHelper;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import com.jianq.util.JQUtils;
import java.net.URLEncoder;
import org.json.JSONException;

public class Msg_queryPushInfoBusi {
    private Context context;
    public String factoryProductId = StringEx.Empty;
    private JQUICallBack httpCallBack = null;
    private boolean isNetwork;
    private String method = "query";
    public String projectName = StringEx.Empty;
    private String tail = "iPlatMBS/AgentService";
    String userLoginNo = StringEx.Empty;

    public Msg_queryPushInfoBusi(Context context2) {
        this.context = context2;
    }

    public void setHttpCallBack(JQUICallBack httpCallBack2) {
        this.httpCallBack = httpCallBack2;
    }

    public void iExecute() {
        String parameter_postdata = infoData();
        this.isNetwork = JQUtils.isNetworkAvailable(this.context);
        if (this.isNetwork) {
            JQNetworkHelper networkHelper = new JQNetworkHelper(this.context);
            networkHelper.progress = false;
            HRParameter parameter = null;
            try {
                HRParameter parameter2 = new HRParameter();
                try {
                    parameter2.setMethod(this.method);
                    parameter2.setBizObj(parameter_postdata);
                    parameter = parameter2;
                } catch (JSONException e) {
                    parameter = parameter2;
                }
            } catch (JSONException e2) {
            }
            networkHelper.open("post", String.valueOf(JQFrameworkConfig.getInst().getDomain()) + this.method);
            networkHelper.setCallBack(this.httpCallBack);
            System.out.println("parameter==>" + parameter.toString());
            networkHelper.send(parameter.toString());
            return;
        }
        System.out.println("============");
    }

    private String infoData() {
        sysConfigModel syss = new SysConfig().setSysconfigbs();
        sysOsInfoModel sysinfo = new SysOsInfo().sysOsInfo(this.context);
        this.projectName = syss.getProjectName();
        String mobEquipmentId = sysinfo.getDeviceid();
        String os = sysinfo.getOs();
        String osVersion = sysinfo.getOsVersion();
        this.userLoginNo = (String) ObjectStores.getInst().getObject("BSP_companyCode");
        String jsonStr = "{\"msg\":\"\",\"msgKey\":\"\",\"detailMsg\":\"\",\"status\":0,\"attr\":{\"projectName\":\"" + this.projectName + "\"" + ",\"methodName\":\"" + "queryPushInfo" + "\"" + ",\"serviceName\":\"" + "D1MB0102" + "\"" + ",\"cmd\":\"" + "queryPushInfo" + "\"" + ",\"mobPhoneNumber\":\"" + mobEquipmentId + "\"" + ",\"parameter_userid\":\"\"" + ",\"parameter_username\":\"\"" + ",\"mobEquipmentId\":\"" + mobEquipmentId + "\"" + ",\"userLoginNo\":\"" + this.userLoginNo + "\"" + ",\"customerId\":\"" + this.userLoginNo + "\"" + "},\"blocks\":{}}";
        System.out.println("jsonStr---:" + jsonStr);
        String encryptString = jsonStr;
        try {
            return URLEncoder.encode(JQEncryptUtil.encrypt(jsonStr));
        } catch (Exception e) {
            e.printStackTrace();
            return encryptString;
        }
    }
}
