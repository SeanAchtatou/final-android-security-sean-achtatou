package com.baosteelOnline.model;

import android.content.Context;
import android.widget.TextView;

public class SmallNumUtil {
    public static void GWCNum(Context context, TextView gwc_dhqrnum) {
        int gcnum = context.getSharedPreferences("smallnum", 2).getInt("gwcnum", 0);
        gwc_dhqrnum.setGravity(17);
        gwc_dhqrnum.setText(new StringBuilder(String.valueOf(gcnum)).toString());
        if (gcnum != 0) {
            gwc_dhqrnum.setVisibility(0);
        } else {
            gwc_dhqrnum.setVisibility(8);
        }
    }

    public static void JJNum(Context context, TextView dhqrnum) {
        int num = context.getSharedPreferences("smallnum", 2).getInt("jjnum", 0);
        System.out.println("num==>" + num);
        dhqrnum.setGravity(17);
        dhqrnum.setText(new StringBuilder(String.valueOf(num)).toString());
        if (num != 0) {
            dhqrnum.setVisibility(0);
        } else {
            dhqrnum.setVisibility(8);
        }
    }
}
