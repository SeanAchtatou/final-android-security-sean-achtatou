package com.baosteelOnline.model;

public class sysConfigModel {
    private String projectName;

    public sysConfigModel() {
    }

    public sysConfigModel(String projectName2) {
        this.projectName = projectName2;
    }

    public String getProjectName() {
        return this.projectName;
    }

    public void setProjectName(String projectName2) {
        this.projectName = projectName2;
    }
}
