package com.baosteelOnline.model;

public class sysOsInfoModel {
    public String appcode;
    public String deviceid;
    public String ip;
    public String network;
    public String os;
    public String osVersion;
    public String resolution1;
    public String resolution2;

    public sysOsInfoModel() {
    }

    public sysOsInfoModel(String appcode2, String deviceid2, String network2, String os2, String osVersion2, String resolution12, String resolution22, String ip2) {
        this.appcode = appcode2;
        this.deviceid = deviceid2;
        this.network = network2;
        this.os = os2;
        this.osVersion = osVersion2;
        this.resolution1 = resolution12;
        this.resolution2 = resolution22;
        this.ip = ip2;
    }

    public String getAppcode() {
        return this.appcode;
    }

    public void setAppcode(String appcode2) {
        this.appcode = appcode2;
    }

    public String getDeviceid() {
        return this.deviceid;
    }

    public void setDeviceid(String deviceid2) {
        this.deviceid = deviceid2;
    }

    public String getNetwork() {
        return this.network;
    }

    public void setNetwork(String network2) {
        this.network = network2;
    }

    public String getOs() {
        return this.os;
    }

    public void setOs(String os2) {
        this.os = os2;
    }

    public String getOsVersion() {
        return this.osVersion;
    }

    public void setOsVersion(String osVersion2) {
        this.osVersion = osVersion2;
    }

    public String getResolution1() {
        return this.resolution1;
    }

    public void setResolution1(String resolution12) {
        this.resolution1 = resolution12;
    }

    public String getResolution2() {
        return this.resolution2;
    }

    public void setResolution2(String resolution22) {
        this.resolution2 = resolution22;
    }

    public String getIp() {
        return this.ip;
    }

    public void setIp(String ip2) {
        this.ip = ip2;
    }
}
