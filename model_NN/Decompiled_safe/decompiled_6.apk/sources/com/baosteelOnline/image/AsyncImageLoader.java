package com.baosteelOnline.image;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Map;

public class AsyncImageLoader {
    public Map<String, SoftReference<Drawable>> imageCache = new HashMap();

    public interface ImageCallBack {
        void imageLoaded(Drawable drawable);
    }

    public Drawable loaDrawable(final String imageUrl, final ImageCallBack imageCallBack) {
        Drawable drawable;
        Bitmap bmpFromSD = FileCache.getInstance().getBmp(imageUrl);
        if (bmpFromSD != null) {
            return new BitmapDrawable(bmpFromSD);
        }
        if (this.imageCache.containsKey(imageUrl) && (drawable = (Drawable) this.imageCache.get(imageUrl).get()) != null) {
            return drawable;
        }
        final Handler handler = new Handler() {
            public void handleMessage(Message message) {
                imageCallBack.imageLoaded((Drawable) message.obj);
            }
        };
        new Thread() {
            public void run() {
                Drawable drawable = AsyncImageLoader.this.loadImageFromUrl(imageUrl);
                AsyncImageLoader.this.imageCache.put(imageUrl, new SoftReference(drawable));
                handler.sendMessage(handler.obtainMessage(0, drawable));
            }
        }.start();
        return null;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v2, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: java.io.InputStream} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.drawable.Drawable loadImageFromUrl(java.lang.String r9) {
        /*
            r8 = this;
            r4 = 0
            r1 = 0
            java.net.URL r5 = new java.net.URL     // Catch:{ Exception -> 0x0016 }
            r5.<init>(r9)     // Catch:{ Exception -> 0x0016 }
            java.lang.Object r6 = r5.getContent()     // Catch:{ Exception -> 0x0016 }
            r0 = r6
            java.io.InputStream r0 = (java.io.InputStream) r0     // Catch:{ Exception -> 0x0016 }
            r4 = r0
        L_0x000f:
            java.lang.String r6 = "src"
            android.graphics.drawable.Drawable r1 = android.graphics.drawable.Drawable.createFromStream(r4, r6)     // Catch:{ OutOfMemoryError -> 0x001b }
        L_0x0015:
            return r1
        L_0x0016:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x000f
        L_0x001b:
            r3 = move-exception
            java.io.PrintStream r6 = java.lang.System.out
            java.lang.String r7 = "�ڴ����..."
            r6.println(r7)
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baosteelOnline.image.AsyncImageLoader.loadImageFromUrl(java.lang.String):android.graphics.drawable.Drawable");
    }
}
