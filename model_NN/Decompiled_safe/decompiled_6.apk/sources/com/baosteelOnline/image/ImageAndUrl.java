package com.baosteelOnline.image;

public class ImageAndUrl {
    private String imageurl;
    private String url;

    public ImageAndUrl(String imageurl2, String url2) {
        this.imageurl = imageurl2;
        this.url = url2;
    }

    public String getImageurl() {
        return this.imageurl;
    }

    public void setImageurl(String imageurl2) {
        this.imageurl = imageurl2;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public ImageAndUrl() {
    }

    public String toString() {
        return "ImageAndUrl [imageurl=" + this.imageurl + ", url=" + this.url + "]";
    }
}
