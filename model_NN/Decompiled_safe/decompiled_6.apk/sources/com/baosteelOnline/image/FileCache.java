package com.baosteelOnline.image;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v4.view.MotionEventCompat;
import android.util.Log;
import com.jianq.misc.StringEx;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileCache {
    private static FileCache fileCache;
    private String strImgPath;
    private String strJsonPath;

    private FileCache() {
        String strPathHead;
        if ("mounted".equals(Environment.getExternalStorageState())) {
            strPathHead = Environment.getExternalStorageDirectory().toString();
        } else {
            strPathHead = "/data/data/com.teffy.viewpager";
        }
        this.strImgPath = String.valueOf(strPathHead) + "/vp/images/";
        this.strJsonPath = String.valueOf(strPathHead) + "/vp/json/";
    }

    public static FileCache getInstance() {
        if (fileCache == null) {
            fileCache = new FileCache();
        }
        return fileCache;
    }

    public boolean saveData(String strApiUrl, String dataJson, String imgurl, Bitmap bmp) {
        String fileName = toHexString(strApiUrl);
        String imgName = imgurl.substring(imgurl.lastIndexOf(47) + 2, imgurl.length());
        File jsonFile = new File(this.strJsonPath);
        File imgFile = new File(this.strImgPath);
        if (!jsonFile.exists()) {
            jsonFile.mkdirs();
        }
        if (!imgFile.exists()) {
            imgFile.mkdirs();
        }
        File fTXT = new File(String.valueOf(this.strJsonPath) + fileName + ".txt");
        File fImg = new File(String.valueOf(this.strImgPath) + imgName);
        writeToFile(dataJson, fTXT);
        writeToFile(bmp, fImg);
        return true;
    }

    public boolean savaJsonData(String strApiUrl, String dataJson) {
        String fileName = toHexString(strApiUrl);
        File jsonFile = new File(this.strJsonPath);
        if (!jsonFile.exists()) {
            jsonFile.mkdirs();
        }
        File fTXT = new File(String.valueOf(this.strJsonPath) + fileName + ".txt");
        if (fTXT.exists()) {
            fTXT.delete();
        }
        writeToFile(dataJson, fTXT);
        return true;
    }

    public boolean savaBmpData(String imgurl, Bitmap bmp) {
        String imgName = imgurl.substring(imgurl.lastIndexOf(47) + 2, imgurl.length());
        File imgFileDirs = new File(this.strImgPath);
        if (!imgFileDirs.exists()) {
            imgFileDirs.mkdirs();
        }
        File fImg = new File(String.valueOf(this.strImgPath) + imgName);
        if (fImg.exists()) {
            fImg.delete();
        }
        writeToFile(bmp, fImg);
        return true;
    }

    public boolean saveBmpDataByName(String bmpName, Bitmap bmp) {
        File imgFileDirs = new File(this.strImgPath);
        if (!imgFileDirs.exists()) {
            imgFileDirs.mkdirs();
        }
        File fImg = new File(String.valueOf(this.strImgPath) + bmpName);
        if (fImg.exists()) {
            fImg.delete();
        }
        writeToFile(bmp, fImg);
        return true;
    }

    public String getJson(String strApiUrl) {
        File file = new File(String.valueOf(this.strJsonPath) + toHexString(strApiUrl) + ".txt");
        StringBuffer sb = new StringBuffer();
        if (file.exists()) {
            try {
                try {
                    BufferedReader br = new BufferedReader(new FileReader(file));
                    while (true) {
                        String str = br.readLine();
                        if (str == null) {
                            return sb.toString();
                        }
                        sb.append(str);
                    }
                } catch (FileNotFoundException e) {
                    e = e;
                    e.printStackTrace();
                    return null;
                } catch (IOException e2) {
                    e = e2;
                    e.printStackTrace();
                    return null;
                }
            } catch (FileNotFoundException e3) {
                e = e3;
                e.printStackTrace();
                return null;
            } catch (IOException e4) {
                e = e4;
                e.printStackTrace();
                return null;
            }
        }
        return null;
    }

    public Bitmap getBmp(String imgurl) {
        File imgFile = new File(String.valueOf(this.strImgPath) + imgurl.substring(imgurl.lastIndexOf(47) + 2, imgurl.length()));
        if (imgFile.exists()) {
            try {
                return BitmapFactory.decodeStream(new FileInputStream(imgFile));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            Log.v("����", "Ҫ�����ͼƬ�ļ�������");
            return null;
        }
    }

    public Bitmap getBmpByName(String bmpName) {
        File imgFile = new File(String.valueOf(this.strImgPath) + bmpName);
        if (imgFile.exists()) {
            try {
                return BitmapFactory.decodeStream(new FileInputStream(imgFile));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            Log.v("����", "Ҫ�����ͼƬ�ļ�������");
            return null;
        }
    }

    public File getImgFile(String imgurl) {
        return new File(String.valueOf(this.strImgPath) + imgurl.substring(imgurl.lastIndexOf(47) + 2, imgurl.length()));
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0034 A[SYNTHETIC, Splitter:B:18:0x0034] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0044 A[SYNTHETIC, Splitter:B:27:0x0044] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0050 A[SYNTHETIC, Splitter:B:33:0x0050] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:15:0x002f=Splitter:B:15:0x002f, B:24:0x003f=Splitter:B:24:0x003f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean writeToFile(java.lang.String r6, java.io.File r7) {
        /*
            r5 = this;
            boolean r4 = r7.exists()
            if (r4 == 0) goto L_0x0009
            r7.delete()
        L_0x0009:
            r2 = 0
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x002e, IOException -> 0x003e }
            r3.<init>(r7)     // Catch:{ FileNotFoundException -> 0x002e, IOException -> 0x003e }
            java.io.BufferedOutputStream r0 = new java.io.BufferedOutputStream     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x005c, all -> 0x0059 }
            r0.<init>(r3)     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x005c, all -> 0x0059 }
            byte[] r4 = r6.getBytes()     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x005c, all -> 0x0059 }
            r0.write(r4)     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x005c, all -> 0x0059 }
            r0.flush()     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x005c, all -> 0x0059 }
            r0.close()     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x005c, all -> 0x0059 }
            if (r3 == 0) goto L_0x0026
            r3.close()     // Catch:{ IOException -> 0x0029 }
        L_0x0026:
            r4 = 1
            r2 = r3
        L_0x0028:
            return r4
        L_0x0029:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0026
        L_0x002e:
            r1 = move-exception
        L_0x002f:
            r1.printStackTrace()     // Catch:{ all -> 0x004d }
            if (r2 == 0) goto L_0x0037
            r2.close()     // Catch:{ IOException -> 0x0039 }
        L_0x0037:
            r4 = 0
            goto L_0x0028
        L_0x0039:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0037
        L_0x003e:
            r1 = move-exception
        L_0x003f:
            r1.printStackTrace()     // Catch:{ all -> 0x004d }
            if (r2 == 0) goto L_0x0037
            r2.close()     // Catch:{ IOException -> 0x0048 }
            goto L_0x0037
        L_0x0048:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0037
        L_0x004d:
            r4 = move-exception
        L_0x004e:
            if (r2 == 0) goto L_0x0053
            r2.close()     // Catch:{ IOException -> 0x0054 }
        L_0x0053:
            throw r4
        L_0x0054:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0053
        L_0x0059:
            r4 = move-exception
            r2 = r3
            goto L_0x004e
        L_0x005c:
            r1 = move-exception
            r2 = r3
            goto L_0x003f
        L_0x005f:
            r1 = move-exception
            r2 = r3
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baosteelOnline.image.FileCache.writeToFile(java.lang.String, java.io.File):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x006a A[SYNTHETIC, Splitter:B:28:0x006a] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0097 A[SYNTHETIC, Splitter:B:45:0x0097] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00aa A[SYNTHETIC, Splitter:B:51:0x00aa] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:42:0x0092=Splitter:B:42:0x0092, B:25:0x0065=Splitter:B:25:0x0065} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean writeToFile(android.graphics.Bitmap r10, java.io.File r11) {
        /*
            r9 = this;
            boolean r6 = r11.exists()
            if (r6 == 0) goto L_0x0009
            r11.delete()
        L_0x0009:
            java.lang.String r5 = r11.getName()
            r6 = 46
            int r6 = r5.lastIndexOf(r6)
            int r7 = r5.length()
            java.lang.String r4 = r5.substring(r6, r7)
            r2 = 0
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x00cd, IOException -> 0x0091 }
            r3.<init>(r11)     // Catch:{ FileNotFoundException -> 0x00cd, IOException -> 0x0091 }
            java.io.BufferedOutputStream r0 = new java.io.BufferedOutputStream     // Catch:{ FileNotFoundException -> 0x0063, IOException -> 0x00ca, all -> 0x00c7 }
            r0.<init>(r3)     // Catch:{ FileNotFoundException -> 0x0063, IOException -> 0x00ca, all -> 0x00c7 }
            if (r10 == 0) goto L_0x007b
            java.lang.String r6 = ".JPEG"
            boolean r6 = r6.equalsIgnoreCase(r4)     // Catch:{ FileNotFoundException -> 0x0063, IOException -> 0x00ca, all -> 0x00c7 }
            if (r6 != 0) goto L_0x0038
            java.lang.String r6 = ".JPG"
            boolean r6 = r6.equalsIgnoreCase(r4)     // Catch:{ FileNotFoundException -> 0x0063, IOException -> 0x00ca, all -> 0x00c7 }
            if (r6 == 0) goto L_0x004d
        L_0x0038:
            android.graphics.Bitmap$CompressFormat r6 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ FileNotFoundException -> 0x0063, IOException -> 0x00ca, all -> 0x00c7 }
            r7 = 100
            r10.compress(r6, r7, r0)     // Catch:{ FileNotFoundException -> 0x0063, IOException -> 0x00ca, all -> 0x00c7 }
            r0.flush()     // Catch:{ FileNotFoundException -> 0x0063, IOException -> 0x00ca, all -> 0x00c7 }
            r0.close()     // Catch:{ FileNotFoundException -> 0x0063, IOException -> 0x00ca, all -> 0x00c7 }
        L_0x0045:
            if (r3 == 0) goto L_0x004a
            r3.close()     // Catch:{ IOException -> 0x006f }
        L_0x004a:
            r6 = 1
            r2 = r3
        L_0x004c:
            return r6
        L_0x004d:
            java.lang.String r6 = ".PNG"
            boolean r6 = r6.equalsIgnoreCase(r4)     // Catch:{ FileNotFoundException -> 0x0063, IOException -> 0x00ca, all -> 0x00c7 }
            if (r6 == 0) goto L_0x0045
            android.graphics.Bitmap$CompressFormat r6 = android.graphics.Bitmap.CompressFormat.PNG     // Catch:{ FileNotFoundException -> 0x0063, IOException -> 0x00ca, all -> 0x00c7 }
            r7 = 100
            r10.compress(r6, r7, r0)     // Catch:{ FileNotFoundException -> 0x0063, IOException -> 0x00ca, all -> 0x00c7 }
            r0.flush()     // Catch:{ FileNotFoundException -> 0x0063, IOException -> 0x00ca, all -> 0x00c7 }
            r0.close()     // Catch:{ FileNotFoundException -> 0x0063, IOException -> 0x00ca, all -> 0x00c7 }
            goto L_0x0045
        L_0x0063:
            r1 = move-exception
            r2 = r3
        L_0x0065:
            r1.printStackTrace()     // Catch:{ all -> 0x00a7 }
            if (r2 == 0) goto L_0x006d
            r2.close()     // Catch:{ IOException -> 0x0085 }
        L_0x006d:
            r6 = 0
            goto L_0x004c
        L_0x006f:
            r1 = move-exception
            r1.printStackTrace()
            java.lang.String r6 = "����"
            java.lang.String r7 = "ͼƬд�뻺���ļ�����"
            android.util.Log.v(r6, r7)
            goto L_0x004a
        L_0x007b:
            r0.close()     // Catch:{ FileNotFoundException -> 0x0063, IOException -> 0x00ca, all -> 0x00c7 }
            if (r3 == 0) goto L_0x00c5
            r3.close()     // Catch:{ IOException -> 0x00ba }
            r2 = r3
            goto L_0x006d
        L_0x0085:
            r1 = move-exception
            r1.printStackTrace()
            java.lang.String r6 = "����"
            java.lang.String r7 = "ͼƬд�뻺���ļ�����"
            android.util.Log.v(r6, r7)
            goto L_0x006d
        L_0x0091:
            r1 = move-exception
        L_0x0092:
            r1.printStackTrace()     // Catch:{ all -> 0x00a7 }
            if (r2 == 0) goto L_0x006d
            r2.close()     // Catch:{ IOException -> 0x009b }
            goto L_0x006d
        L_0x009b:
            r1 = move-exception
            r1.printStackTrace()
            java.lang.String r6 = "����"
            java.lang.String r7 = "ͼƬд�뻺���ļ�����"
            android.util.Log.v(r6, r7)
            goto L_0x006d
        L_0x00a7:
            r6 = move-exception
        L_0x00a8:
            if (r2 == 0) goto L_0x00ad
            r2.close()     // Catch:{ IOException -> 0x00ae }
        L_0x00ad:
            throw r6
        L_0x00ae:
            r1 = move-exception
            r1.printStackTrace()
            java.lang.String r7 = "����"
            java.lang.String r8 = "ͼƬд�뻺���ļ�����"
            android.util.Log.v(r7, r8)
            goto L_0x00ad
        L_0x00ba:
            r1 = move-exception
            r1.printStackTrace()
            java.lang.String r6 = "����"
            java.lang.String r7 = "ͼƬд�뻺���ļ�����"
            android.util.Log.v(r6, r7)
        L_0x00c5:
            r2 = r3
            goto L_0x006d
        L_0x00c7:
            r6 = move-exception
            r2 = r3
            goto L_0x00a8
        L_0x00ca:
            r1 = move-exception
            r2 = r3
            goto L_0x0092
        L_0x00cd:
            r1 = move-exception
            goto L_0x0065
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baosteelOnline.image.FileCache.writeToFile(android.graphics.Bitmap, java.io.File):boolean");
    }

    public boolean clearImgByImgUrl(String imgurl) {
        File imgFile = getImgFile(imgurl);
        if (!imgFile.exists()) {
            return false;
        }
        imgFile.delete();
        return true;
    }

    public int clearAllData() {
        File imgDir = new File(this.strImgPath);
        File txtDir = new File(this.strJsonPath);
        File[] imgFiles = imgDir.listFiles();
        File[] txtFiles = txtDir.listFiles();
        int m = imgFiles.length;
        int x = txtFiles.length;
        int g = 0;
        int t = 0;
        for (int i = 0; i < m; i++) {
            if (!imgFiles[i].exists()) {
                g++;
            } else if (imgFiles[i].delete()) {
                g++;
            }
        }
        for (int i2 = 0; i2 < x; i2++) {
            if (!txtFiles[i2].exists()) {
                t++;
            } else if (txtFiles[i2].delete()) {
                t++;
            }
        }
        if (g == m && t == x) {
            return 1;
        }
        return 0;
    }

    private String toHexString(String s) {
        String str = StringEx.Empty;
        for (int i = 0; i < s.length(); i++) {
            str = String.valueOf(str) + Integer.toHexString(s.charAt(i));
        }
        return "0x" + str;
    }

    private String toStringHex(String s) {
        if ("0x".equals(s.substring(0, 2))) {
            s = s.substring(2);
        }
        byte[] baKeyword = new byte[(s.length() / 2)];
        for (int i = 0; i < baKeyword.length; i++) {
            try {
                baKeyword[i] = (byte) (Integer.parseInt(s.substring(i * 2, (i * 2) + 2), 16) & MotionEventCompat.ACTION_MASK);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            return new String(baKeyword, "utf-8");
        } catch (Exception e1) {
            e1.printStackTrace();
            return s;
        }
    }
}
