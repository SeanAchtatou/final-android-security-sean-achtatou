package com.baosteelOnline.xhjy;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.andframework.common.ObjectStores;
import com.andframework.ui.CustomListView;
import com.andframework.ui.CustomMessageShow;
import com.baosteelOnline.R;
import com.baosteelOnline.common.DataBaseFactory;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.data.ContractListBusi;
import com.baosteelOnline.data_parse.ContractParse;
import com.baosteelOnline.dl.Login_indexActivity;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.model.SmallNumUtil;
import com.baosteelOnline.sql.DBHelper;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import java.util.ArrayList;
import java.util.HashMap;

public class ContractListActivity extends JQActivity implements CustomListView.OnGetMoreListener, RadioGroup.OnCheckedChangeListener {
    /* access modifiers changed from: private */
    public ArrayList<SearchRow> LisItems = new ArrayList<>();
    public String allcount = StringEx.Empty;
    public String allpage = StringEx.Empty;
    private String begin_time = StringEx.Empty;
    private String contract_num = StringEx.Empty;
    private String contract_stats = StringEx.Empty;
    CustomListView contractslist;
    public int countNo = 0;
    private DataBaseFactory db;
    private DBHelper dbHelper;
    private String end_time = StringEx.Empty;
    View footview;
    private JQUICallBack httpCallBack = new JQUICallBack() {
        public void callBack(ResultData result) {
            ContractListActivity.this.progressDialog.dismiss();
            System.out.println("result.getStringData" + result.getStringData());
            ContractListActivity.this.updateProductList(ContractParse.parse(result.getStringData()));
        }
    };
    private String limit = "10";
    /* access modifiers changed from: private */
    public String offset = "1";
    public String offsets = StringEx.Empty;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    private RadioGroup radioderGroup;
    private String sellername = StringEx.Empty;
    TextView textcont;
    private View view;
    private View view2;
    private View view3;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ExitApplication.getInstance().addActivity(this);
        this.dbHelper = DataBaseFactory.getInstance(this);
        this.dbHelper.insertOperation("钢材交易", "买家中心", "合同查询列表");
        requestWindowFeature(1);
        setContentView((int) R.layout.xhjy_contractlist);
        this.footview = View.inflate(this, R.layout.xhjy_list_bottom_button, null);
        this.textcont = (TextView) this.footview.findViewById(R.id.xhjy_list_bottom_text);
        ((RadioButton) findViewById(R.id.radio_contract3)).setChecked(true);
        SmallNumUtil.GWCNum(this, (TextView) findViewById(R.id.gwc_dhqrnum));
        this.radioderGroup = (RadioGroup) findViewById(R.id.xhjy_index_RGroup);
        this.radioderGroup.setOnCheckedChangeListener(this);
        if (ObjectStores.getInst().getObject("reStr") == "1" || ObjectStores.getInst().getObject("czy") == "1") {
            this.contractslist = (CustomListView) findViewById(R.id.contractslist);
            this.contractslist.setonGetMoreListener(this);
            this.contractslist.setClickable(true);
            this.contractslist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View v, int arg2, long arg3) {
                    if (Integer.valueOf(ContractListActivity.this.allpage).intValue() != Integer.valueOf(ContractListActivity.this.offsets).intValue() || arg2 - 1 != ContractListActivity.this.LisItems.size()) {
                        if (arg2 - 1 == ContractListActivity.this.LisItems.size()) {
                            ContractListActivity.this.offset = String.valueOf(Integer.valueOf(ContractListActivity.this.offset).intValue() + 1);
                            new ContractListBusi(ContractListActivity.this).offset = ContractListActivity.this.offset;
                            ContractListActivity.this.testBusi();
                            return;
                        }
                        int num = arg2 - 1;
                        String hth = ((SearchRow) ContractListActivity.this.LisItems.get(num)).contractid.toString();
                        String company2 = ((SearchRow) ContractListActivity.this.LisItems.get(num)).company.toString();
                        String piece2 = ((SearchRow) ContractListActivity.this.LisItems.get(num)).piece.toString();
                        String mertial2 = ((SearchRow) ContractListActivity.this.LisItems.get(num)).mertial.toString();
                        String price = ((SearchRow) ContractListActivity.this.LisItems.get(num)).Allprice.toString();
                        HashMap hasmap = new HashMap();
                        hasmap.put("contractid", hth);
                        hasmap.put("company", company2);
                        hasmap.put("piece", piece2);
                        hasmap.put("mertial", mertial2);
                        hasmap.put("price", price);
                        ExitApplication.getInstance().startActivity(ContractListActivity.this, ContractActivity.class, hasmap);
                    }
                }
            });
            testBusi();
            return;
        }
        new AlertDialog.Builder(this).setMessage("您还没有登录,请登录！").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                SharedPreferences.Editor editor = ContractListActivity.this.getSharedPreferences("index_mark", 2).edit();
                editor.putString("index", "ContractList");
                editor.commit();
                ExitApplication.getInstance().startActivity(ContractListActivity.this, Login_indexActivity.class, null, 1);
            }
        }).show();
    }

    public void testBusi() {
        String beginTime;
        String endTime;
        String hth = (String) ObjectStores.getInst().getObject("contract_num");
        String mj = (String) ObjectStores.getInst().getObject("sellername");
        String zt = (String) ObjectStores.getInst().getObject("contract_stats");
        String beginTime2 = (String) ObjectStores.getInst().getObject("begin_time");
        String endTime2 = (String) ObjectStores.getInst().getObject("end_time");
        if (mj == null || mj.equals("所有卖家")) {
            mj = StringEx.Empty;
        }
        if (zt == null) {
            zt = StringEx.Empty;
        }
        if (beginTime2 == null || beginTime2 == StringEx.Empty) {
            beginTime = StringEx.Empty;
        } else {
            beginTime = String.valueOf(beginTime2.split("-")[0]) + beginTime2.split("-")[1] + beginTime2.split("-")[2];
        }
        if (endTime2 == null || endTime2 == StringEx.Empty) {
            endTime = StringEx.Empty;
        } else {
            endTime = String.valueOf(endTime2.split("-")[0]) + endTime2.split("-")[1] + endTime2.split("-")[2];
        }
        ContractListBusi contractlistBusi = new ContractListBusi(this);
        contractlistBusi.hth = hth;
        contractlistBusi.mj = mj;
        contractlistBusi.ok = zt;
        contractlistBusi.rqStart = beginTime;
        contractlistBusi.rqEnd = endTime;
        contractlistBusi.offset = this.offset;
        contractlistBusi.limit = this.limit;
        contractlistBusi.setHttpCallBack(this.httpCallBack);
        contractlistBusi.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    /* access modifiers changed from: package-private */
    public void updateProductList(ContractParse contractParse) {
        if (ContractParse.commonData == null || ContractParse.commonData.blocks == null || ContractParse.commonData.blocks.r0 == null || ContractParse.commonData.blocks.r0.mar == null || ContractParse.commonData.blocks.r0.mar.rows == null) {
            CustomMessageShow.getInst().showLongToast(this, "服务器无数据返回！");
            return;
        }
        if (ContractParse.commonData.blocks.r0.mar.rows.rows.size() == 0) {
            new AlertDialog.Builder(this).setMessage("对不起，没有数据!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    CustomMessageShow.getInst().cancleProgressDialog();
                    ContractListActivity.this.clearGlobe();
                    ExitApplication.getInstance().back(0);
                }
            }).show().setCanceledOnTouchOutside(false);
        } else if (!(ContractParse.commonData == null || ContractParse.commonData.blocks == null || ContractParse.commonData.blocks.r0 == null || ContractParse.commonData.blocks.r0.mar == null || ContractParse.commonData.blocks.r0.mar.rows == null || ContractParse.commonData.blocks.r0.mar.rows.rows == null)) {
            int rowsCount = ContractParse.commonData.blocks.r0.mar.rows.rows.size();
            this.allpage = (String) ContractParse.commonData.blocks.i0.mar.rows.rows.get(0).get(0);
            this.offsets = (String) ContractParse.commonData.blocks.i0.mar.rows.rows.get(0).get(1);
            this.allcount = (String) ContractParse.commonData.blocks.i0.mar.rows.rows.get(0).get(2);
            for (int i = 0; i < rowsCount; i++) {
                ArrayList<String> rowdata = ContractParse.commonData.blocks.r0.mar.rows.rows.get(i);
                SearchRow sr = new SearchRow();
                if (rowdata != null) {
                    for (int k = 0; k < rowdata.size(); k++) {
                        if (k == 0) {
                            sr.number = String.valueOf(String.valueOf(this.countNo + 1)) + ".合同号:";
                            sr.contractid = (String) rowdata.get(k);
                        }
                        if (k == 1) {
                            sr.company = (String) rowdata.get(k);
                        } else if (k == 2) {
                            sr.piece = (String) rowdata.get(k);
                        } else if (k == 4) {
                            sr.mertial = (String) rowdata.get(k);
                        } else if (k == 5) {
                            sr.Allprice = (String) rowdata.get(k);
                        } else if (k == 6) {
                            sr.time = (String) rowdata.get(k);
                            if (sr.time.length() > 10) {
                                sr.time = sr.time.substring(0, 10);
                            }
                        } else if (k == 7) {
                            sr.desunit = (String) rowdata.get(k);
                        } else if (k == 8) {
                            sr.desunitName = (String) rowdata.get(k);
                        }
                    }
                    this.countNo++;
                    this.LisItems.add(sr);
                    this.view3 = buildRowView(sr);
                    this.contractslist.addViewToLast(this.view3);
                    rowdata.removeAll(rowdata);
                }
            }
            if (this.countNo <= 10) {
                if (this.countNo == Integer.valueOf(this.allcount).intValue()) {
                    this.textcont.setText("这是最后一页");
                } else {
                    this.textcont.setText("下10条...");
                }
                this.contractslist.addFooterView(this.footview);
            } else if (this.countNo == Integer.valueOf(this.allcount).intValue()) {
                this.textcont.setText("这是最后一页");
            } else {
                this.textcont.setText("下10条...");
            }
            this.contractslist.onRefreshComplete();
        }
        CustomMessageShow.getInst().cancleProgressDialog();
    }

    /* access modifiers changed from: private */
    public void clearGlobe() {
        ObjectStores.getInst().putObject("sellername", StringEx.Empty);
        ObjectStores.getInst().putObject("contract_stats", StringEx.Empty);
        ObjectStores.getInst().putObject("contract_status", StringEx.Empty);
        ObjectStores.getInst().putObject("contract_num", StringEx.Empty);
        ObjectStores.getInst().putObject("begin_time", StringEx.Empty);
        ObjectStores.getInst().putObject("end_time", StringEx.Empty);
    }

    private View buildRowView(SearchRow sr) {
        this.view = View.inflate(this, R.layout.xhjy_contractslist_row, null);
        TextView TV_desunit = (TextView) this.view.findViewById(R.id.TV_desunit_clist);
        ((TextView) this.view.findViewById(R.id.TV_number_clist)).setText(sr.number);
        ((TextView) this.view.findViewById(R.id.TV_contractid_clist)).setText(sr.contractid);
        ((TextView) this.view.findViewById(R.id.TV_company_clist)).setText("卖家:" + sr.company);
        ((TextView) this.view.findViewById(R.id.TV_mertial_clist)).setText("数量/重量:" + sr.piece + "件/" + sr.mertial + "吨");
        ((TextView) this.view.findViewById(R.id.TV_format_clist)).setText("有效日期:" + sr.time);
        ((TextView) this.view.findViewById(R.id.TV_price_clist)).setText(sr.Allprice);
        if (sr.desunitName != "null") {
            TV_desunit.setText("合同状态:" + sr.desunitName);
        }
        return this.view;
    }

    class SearchRow {
        public String Allprice = StringEx.Empty;
        public String company = StringEx.Empty;
        public String contractid = StringEx.Empty;
        public String desunit = StringEx.Empty;
        public String desunitName = StringEx.Empty;
        public String mertial = StringEx.Empty;
        public String number = StringEx.Empty;
        public String piece = StringEx.Empty;
        public String time = StringEx.Empty;

        SearchRow() {
        }
    }

    public void contract_list_backButtonAction(View v) {
        ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class);
    }

    public void contract_list_fiterButtonAction(View v) {
        ExitApplication.getInstance().startActivity(this, Contract_siltActivity.class);
    }

    public void onBackPressed() {
        super.onBackPressed();
        ExitApplication.getInstance().back(0);
    }

    public void setGlobal() {
        ObjectStores.getInst().putObject("limit", this.limit);
        ObjectStores.getInst().putObject("offset", this.offset);
        ObjectStores.getInst().putObject("sellername", this.sellername);
        ObjectStores.getInst().putObject("contract_stats", this.contract_stats);
        ObjectStores.getInst().putObject("contract_num", this.contract_num);
        ObjectStores.getInst().putObject("begin_time", this.begin_time);
        ObjectStores.getInst().putObject("end_time", this.end_time);
    }

    public void onGetmore() {
        int listNum = (this.contractslist.getListNum() / 20) + 1;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.contractslist = null;
        this.view = null;
        this.view2 = null;
        this.view3 = null;
        this.LisItems = null;
        this.limit = null;
        this.offset = null;
        this.sellername = null;
        this.contract_stats = null;
        this.contract_num = null;
        this.begin_time = null;
        this.end_time = null;
        this.countNo = 0;
        super.onDestroy();
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.radio_search3:
                ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
                return;
            case R.id.radio_shopcar3:
                ((RadioButton) findViewById(R.id.radio_shopcar3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ShopActivity.class);
                return;
            case R.id.radio_home3:
                ((RadioButton) findViewById(R.id.radio_home3)).setChecked(true);
                HashMap hasmap = new HashMap();
                hasmap.put("index_more", "1");
                ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class, hasmap);
                return;
            case R.id.radio_contract3:
                ((RadioButton) findViewById(R.id.radio_contract3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ContractListActivity.class);
                return;
            case R.id.radio_notice3:
                ((RadioButton) findViewById(R.id.radio_notice3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, Xhjy_more.class);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }
}
