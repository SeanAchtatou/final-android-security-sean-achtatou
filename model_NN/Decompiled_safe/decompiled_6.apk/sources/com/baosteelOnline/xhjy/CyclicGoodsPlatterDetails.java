package com.baosteelOnline.xhjy;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.R;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.data.RequestBidData;
import com.baosteelOnline.data_parse.ContractParse;
import com.baosteelOnline.main.ExitApplication;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;

public class CyclicGoodsPlatterDetails extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    public ContractParse contractParse;
    private JQUICallBack httpCallBack = new JQUICallBack() {
        public void callBack(ResultData result) {
            CyclicGoodsPlatterDetails.this.progressDialog.dismiss();
            Log.d("获取的数据：", result.getStringData());
            String dataString = result.getStringData();
            if (!dataString.equals(null) && !dataString.equals(StringEx.Empty)) {
                CyclicGoodsPlatterDetails.this.contractParse = ContractParse.parse(result.getStringData());
                Log.d("获取的ROWs数据：", ContractParse.jjo.toString());
                if (ContractParse.jjo.toString().equals(null) || ContractParse.jjo.toString().equals("[[]]") || ContractParse.jjo.toString().equals(StringEx.Empty) || ContractParse.jjo.toString().equals("[]")) {
                    new AlertDialog.Builder(CyclicGoodsPlatterDetails.this).setMessage("数据为空！").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
                } else {
                    for (int i = 0; i < ContractParse.jjo.length(); i++) {
                        try {
                            new JSONArray(ContractParse.jjo.getString(i));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            CyclicGoodsPlatterDetails.this.mSimpleAdater.notifyDataSetChanged();
        }
    };
    private RadioButton mIndexNavigation1;
    private RadioButton mIndexNavigation2;
    private RadioButton mIndexNavigation3;
    private RadioButton mIndexNavigation4;
    private RadioButton mIndexNavigation5;
    private ArrayList<HashMap<String, Object>> mItemArrayList;
    public TextView mListTitleTextview;
    private ListView mListView;
    private RadioGroup mRadioderGroup;
    /* access modifiers changed from: private */
    public SimpleAdapter mSimpleAdater;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.activity_cyclic_goods_platter_details);
        ExitApplication.getInstance().addActivity(this);
        this.mListView = (ListView) findViewById(R.id.cyclicgoods_platter_details_listview);
        this.mItemArrayList = new ArrayList<>();
        this.mRadioderGroup = (RadioGroup) findViewById(R.id.cyclic_goods_index_RGroup);
        this.mIndexNavigation1 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item1);
        this.mIndexNavigation2 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item2);
        this.mIndexNavigation3 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item3);
        this.mIndexNavigation4 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item4);
        this.mIndexNavigation5 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item5);
        this.mListTitleTextview = (TextView) findViewById(R.id.cyclicgoods_list_title_textview);
        this.mListTitleTextview.setText("拼盘详情");
        this.mIndexNavigation3.setChecked(true);
        this.mRadioderGroup.setOnCheckedChangeListener(this);
        setData();
        this.mSimpleAdater = new SimpleAdapter(this, this.mItemArrayList, R.layout.activity_cyclic_goods_platter_details_item, new String[]{"title", "title1", "name", "material", "standard", "weight", "field", "storage"}, new int[]{R.id.cyclicgoods_platter_details_item_textview_title, R.id.cyclicgoods_platter_details_item_textview_title1, R.id.cyclicgoods_platter_details_item_textview_name, R.id.cyclicgoods_platter_details_item_textview_material, R.id.cyclicgoods_platter_details_item_textview_standard, R.id.cyclicgoods_platter_details_item_textview_weight, R.id.cyclicgoods_platter_details_item_textview_field, R.id.cyclicgoods_platter_details_item_textview_storage});
        this.mListView.setAdapter((ListAdapter) this.mSimpleAdater);
        this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                int i = arg2 + 1;
            }
        });
        getData();
    }

    private void getData() {
        RequestBidData requestBidData = new RequestBidData(this);
        requestBidData.setRequestData("拼盘信息查询");
        Log.d("请求的指令：", requestBidData.infoDate());
        requestBidData.setHttpCallBack(this.httpCallBack);
        requestBidData.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    private void setData() {
        for (int i = 0; i < 20; i++) {
            HashMap<String, Object> item = new HashMap<>();
            item.put("title", String.valueOf(i + 1) + ".捆绑号：");
            item.put("title1", "JJ405971");
            item.put("name", "品名：热轧板");
            item.put("material", "材质：APHC");
            item.put("standard", "规格：2.00*1250*C");
            item.put("weight", "重量：102.324545");
            item.put("field", "产地：宝钢");
            item.put("storage", "仓库：宝新1号库");
            this.mItemArrayList.add(item);
        }
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.cyclicgoods_index_navigation_item2:
                this.mIndexNavigation2.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item3:
                this.mIndexNavigation3.setChecked(true);
                return;
            case R.id.cyclicgoods_index_navigation_item1:
                if (ObjectStores.getInst().getObject("formX") == "formX") {
                    ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class);
                    finish();
                    return;
                }
                this.mIndexNavigation1.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item4:
                this.mIndexNavigation4.setChecked(true);
                startActivity(new Intent(this, CyclicGoodsBidHallPageActivity.class));
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item5:
                this.mIndexNavigation5.setChecked(true);
                Intent intent = new Intent(this, Xhjy_more.class);
                intent.putExtra("part", "循环物资");
                startActivity(intent);
                finish();
                return;
            default:
                return;
        }
    }

    public void backAaction(View v) {
        ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
        finish();
    }

    public void onBackPressed() {
        ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
        finish();
    }
}
