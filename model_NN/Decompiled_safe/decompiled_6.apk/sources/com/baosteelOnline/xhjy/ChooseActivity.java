package com.baosteelOnline.xhjy;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.R;
import com.baosteelOnline.common.DataBaseFactory;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.model.SmallNumUtil;
import com.baosteelOnline.sql.DBHelper;
import com.jianq.misc.StringEx;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ChooseActivity extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    private TextView TV_choose_sift_price;
    private TextView TV_choose_sift_thickness;
    private TextView TV_choose_sift_width;
    /* access modifiers changed from: private */
    public TextView choose_sellername;
    private TextView choose_sift_allname;
    /* access modifiers changed from: private */
    public TextView choose_sift_paixu;
    private DataBaseFactory db;
    private DBHelper dbHelper;
    /* access modifiers changed from: private */
    public String index_orderby_sign = StringEx.Empty;
    final String[] itemDate = {"价格从低到高", "价格从高到低", "规格从低到高", "规格从高到低"};
    public String length = StringEx.Empty;
    public String length_sing;
    private List<String> list = new ArrayList();
    public String number = StringEx.Empty;
    public String orderBy = StringEx.Empty;
    public String pname = StringEx.Empty;
    public String price = StringEx.Empty;
    public String price_order;
    public String price_sign;
    public String product_sign;
    private RadioGroup radioderGroup;
    final String[] seller = {"所有卖家", "宝钢新事业", "宝山公司", "上海不锈"};
    public String sellername = StringEx.Empty;
    public String shopsign = StringEx.Empty;
    public String thickness = StringEx.Empty;
    public String thickness_sign;
    public String width = StringEx.Empty;
    public String width_sign;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        ExitApplication.getInstance().addActivity(this);
        this.dbHelper = DataBaseFactory.getInstance(this);
        this.dbHelper.insertOperation("钢材交易", "搜索", "搜索资源筛选");
        setContentView((int) R.layout.xhjy_choose);
        SmallNumUtil.GWCNum(this, (TextView) findViewById(R.id.gwc_dhqrnum));
        init();
    }

    /* access modifiers changed from: package-private */
    public void init() {
        this.price_sign = (String) ObjectStores.getInst().getObject("price_sign");
        this.thickness_sign = (String) ObjectStores.getInst().getObject("thickness_sign");
        this.width_sign = (String) ObjectStores.getInst().getObject("width_sign");
        this.product_sign = (String) ObjectStores.getInst().getObject("product_sign");
        if (this.price_sign == null) {
            this.price_sign = "00";
        }
        if (this.thickness_sign == null) {
            this.thickness_sign = "00";
        }
        if (this.width_sign == null) {
            this.width_sign = "00";
        }
        if (this.product_sign == null) {
            this.product_sign = "所有品种";
        }
        ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
        this.radioderGroup = (RadioGroup) findViewById(R.id.xhjy_index_RGroup);
        this.radioderGroup.setOnCheckedChangeListener(this);
        this.choose_sift_allname = (TextView) findViewById(R.id.choose_sift_allname);
        this.choose_sift_paixu = (TextView) findViewById(R.id.choose_sift_paixu);
        this.TV_choose_sift_price = (TextView) findViewById(R.id.TV_choose_sift_price);
        this.TV_choose_sift_thickness = (TextView) findViewById(R.id.TV_choose_sift_thickness);
        this.TV_choose_sift_width = (TextView) findViewById(R.id.TV_choose_sift_width);
        this.choose_sellername = (TextView) findViewById(R.id.choose_sift_maijia);
        this.choose_sift_allname.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                ExitApplication.getInstance().startActivity(ChooseActivity.this, Choose_productActivity.class);
            }
        });
        this.choose_sift_allname.setText(this.product_sign);
        if (this.product_sign.equals("所有品种")) {
            this.product_sign = StringEx.Empty;
        }
        ObjectStores.getInst().putObject("pm", this.product_sign);
        if (this.thickness_sign.equals("00")) {
            this.TV_choose_sift_thickness.setText("所有厚度");
            ObjectStores.getInst().putObject("thickmin", StringEx.Empty);
            ObjectStores.getInst().putObject("thickmax", StringEx.Empty);
        } else if (this.thickness_sign.equals("01")) {
            this.TV_choose_sift_thickness.setText("0-1.99mm");
            ObjectStores.getInst().putObject("thickmin", "0");
            ObjectStores.getInst().putObject("thickmax", "1.99");
        } else if (this.thickness_sign.equals("02")) {
            this.TV_choose_sift_thickness.setText("2-2.49mm");
            ObjectStores.getInst().putObject("thickmin", "2");
            ObjectStores.getInst().putObject("thickmax", "2.49");
        } else if (this.thickness_sign.equals("03")) {
            this.TV_choose_sift_thickness.setText("2.5-3.49mm");
            ObjectStores.getInst().putObject("thickmin", "2.5");
            ObjectStores.getInst().putObject("thickmax", "3.49");
        } else if (this.thickness_sign.equals("04")) {
            this.TV_choose_sift_thickness.setText("3.5-7.99mm");
            ObjectStores.getInst().putObject("thickmin", "3.5");
            ObjectStores.getInst().putObject("thickmax", "7.99");
        } else if (this.thickness_sign.equals("05")) {
            this.TV_choose_sift_thickness.setText("8mm以上");
            ObjectStores.getInst().putObject("thickmin", "8");
            ObjectStores.getInst().putObject("thickmax", StringEx.Empty);
        } else if (this.thickness_sign.equals("11")) {
            String tmin = getIntent().getStringExtra("thickmin");
            String tmax = getIntent().getStringExtra("thickmax");
            this.TV_choose_sift_thickness.setText(String.valueOf(tmin) + " mm-" + tmax + " mm");
            ObjectStores.getInst().putObject("thickmin", tmin);
            ObjectStores.getInst().putObject("thickmax", tmax);
        }
        if (this.width_sign.equals("00")) {
            this.TV_choose_sift_width.setText("所有宽度");
            ObjectStores.getInst().putObject("widemin", StringEx.Empty);
            ObjectStores.getInst().putObject("widemax", StringEx.Empty);
        } else if (this.width_sign.equals("01")) {
            this.TV_choose_sift_width.setText("0-700mm");
            ObjectStores.getInst().putObject("widemin", "0");
            ObjectStores.getInst().putObject("widemax", "700");
        } else if (this.width_sign.equals("02")) {
            this.TV_choose_sift_width.setText("700-1000mm");
            ObjectStores.getInst().putObject("widemin", "700");
            ObjectStores.getInst().putObject("widemax", "1000");
        } else if (this.width_sign.equals("03")) {
            this.TV_choose_sift_width.setText("1000-1300mm");
            ObjectStores.getInst().putObject("widemin", "1000");
            ObjectStores.getInst().putObject("widemax", "1300");
        } else if (this.width_sign.equals("04")) {
            this.TV_choose_sift_width.setText("1300-1500mm");
            ObjectStores.getInst().putObject("widemin", "1300");
            ObjectStores.getInst().putObject("widemax", "1500");
        } else if (this.width_sign.equals("05")) {
            this.TV_choose_sift_width.setText("1500mm以上");
            ObjectStores.getInst().putObject("widemin", "1500");
            ObjectStores.getInst().putObject("widemax", StringEx.Empty);
        } else if (this.width_sign.equals("11")) {
            String wmin = getIntent().getStringExtra("widemin");
            String wmax = getIntent().getStringExtra("widemax");
            this.TV_choose_sift_width.setText(String.valueOf(wmin) + " mm-" + wmax + " mm");
            ObjectStores.getInst().putObject("widemin", wmin);
            ObjectStores.getInst().putObject("widemax", wmax);
        }
        if (this.price_sign.equals("00")) {
            this.TV_choose_sift_price.setText("所有价格");
            ObjectStores.getInst().putObject("pricemin", StringEx.Empty);
            ObjectStores.getInst().putObject("pricemax", StringEx.Empty);
        } else if (this.price_sign.equals("01")) {
            this.TV_choose_sift_price.setText("1000-4000元");
            ObjectStores.getInst().putObject("pricemin", "1000");
            ObjectStores.getInst().putObject("pricemax", "4000");
        } else if (this.price_sign.equals("02")) {
            this.TV_choose_sift_price.setText("4000-6000元");
            ObjectStores.getInst().putObject("pricemin", "4000");
            ObjectStores.getInst().putObject("pricemax", "6000");
        } else if (this.price_sign.equals("03")) {
            this.TV_choose_sift_price.setText("6000-8000元");
            ObjectStores.getInst().putObject("pricemin", "6000");
            ObjectStores.getInst().putObject("pricemax", "8000");
        } else if (this.price_sign.equals("04")) {
            this.TV_choose_sift_price.setText("8000元以上");
            ObjectStores.getInst().putObject("pricemin", "8000");
            ObjectStores.getInst().putObject("pricemax", StringEx.Empty);
        } else if (this.price_sign.equals("11")) {
            String pmin = getIntent().getStringExtra("pricemin");
            String pmax = getIntent().getStringExtra("pricemax");
            this.TV_choose_sift_price.setText(String.valueOf(pmin) + " 元-" + pmax + " 元");
            ObjectStores.getInst().putObject("pricemin", pmin);
            ObjectStores.getInst().putObject("pricemax", pmax);
        }
    }

    public void xhjy_choose_back_ButtonAction(View v) {
        startActivity(new Intent(this, Xhjy_indexActivity.class));
        finish();
    }

    public void onBackPressed() {
        ExitApplication.getInstance().back(0);
        super.onBackPressed();
    }

    public void xhjy_choose_finish_ButtonAction(View v) {
        if (this.index_orderby_sign == "jg") {
            ObjectStores.getInst().putObject("orderJg", this.orderBy);
        } else if (this.index_orderby_sign == "gg") {
            ObjectStores.getInst().putObject("orderGg", this.orderBy);
        } else {
            ObjectStores.getInst().putObject("orderJg", "asc");
        }
        ObjectStores.getInst().putObject("searchbody", "ChooseActivity");
        if (this.sellername.equals("所有卖家")) {
            this.sellername = StringEx.Empty;
        }
        ObjectStores.getInst().putObject("sellername", this.sellername);
        ExitApplication.getInstance().startActivity(this, SearchActivity.class);
    }

    public void xhjy_choose_products_Action(View v) {
        ExitApplication.getInstance().startActivity(this, Choose_productActivity.class);
    }

    public void PriceOrderByAction(View v) {
        this.dbHelper.insertOperation("钢材交易", "搜索", "搜索排序规则页面");
        new AlertDialog.Builder(this).setTitle("请选择交货期排序").setItems(this.itemDate, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (ChooseActivity.this.itemDate[which].equals("价格从低到高")) {
                    ChooseActivity.this.orderBy = "asc";
                    ChooseActivity.this.index_orderby_sign = "jg";
                    ChooseActivity.this.choose_sift_paixu.setText("价格从低到高");
                } else if (ChooseActivity.this.itemDate[which].equals("价格从高到低")) {
                    ChooseActivity.this.orderBy = "desc";
                    ChooseActivity.this.index_orderby_sign = "jg";
                    ChooseActivity.this.choose_sift_paixu.setText("价格从高到低");
                } else if (ChooseActivity.this.itemDate[which].equals("规格从低到高")) {
                    ChooseActivity.this.orderBy = "asc";
                    ChooseActivity.this.index_orderby_sign = "gg";
                    ChooseActivity.this.choose_sift_paixu.setText("规格从低到高");
                } else if (ChooseActivity.this.itemDate[which].equals("规格从高到低")) {
                    ChooseActivity.this.orderBy = "desc";
                    ChooseActivity.this.index_orderby_sign = "gg";
                    ChooseActivity.this.choose_sift_paixu.setText("规格从高到低");
                }
            }
        }).create().show();
    }

    public void sellerAction(View v) {
        this.dbHelper.insertOperation("钢材交易", "搜索", "搜索卖家页面");
        new AlertDialog.Builder(this).setTitle("请选择卖家").setItems(this.seller, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (ChooseActivity.this.seller[which].equals("所有卖家")) {
                    ChooseActivity.this.sellername = "所有卖家";
                    ChooseActivity.this.choose_sellername.setText("所有卖家");
                } else if (ChooseActivity.this.seller[which].equals("宝钢新事业")) {
                    ChooseActivity.this.sellername = "宝钢新事业";
                    ChooseActivity.this.choose_sellername.setText("宝钢新事业");
                } else if (ChooseActivity.this.seller[which].equals("宝山公司")) {
                    ChooseActivity.this.sellername = "宝山公司";
                    ChooseActivity.this.choose_sellername.setText("宝山公司");
                } else if (ChooseActivity.this.seller[which].equals("上海不锈")) {
                    ChooseActivity.this.sellername = "上海不锈";
                    ChooseActivity.this.choose_sellername.setText("上海不锈");
                }
            }
        }).create().show();
    }

    public void choose_sift_priceDataAction(View v) {
        this.dbHelper.insertOperation("钢材交易", "搜索", "搜索价格区间页面");
        ExitApplication.getInstance().startActivity(this, Choose_filter_price.class);
    }

    public void choose_sift_thicknessAction(View v) {
        this.dbHelper.insertOperation("钢材交易", "搜索", "搜索厚度区间页面");
        ExitApplication.getInstance().startActivity(this, Choose_filter_thickness.class);
    }

    public void choose_sift_widthAction(View v) {
        this.dbHelper.insertOperation("钢材交易", "搜索", "搜索宽度价格区间页面");
        ExitApplication.getInstance().startActivity(this, Choose_filter_width.class);
    }

    /* access modifiers changed from: package-private */
    public void Golbe() {
        ObjectStores.getInst().putObject("length_sign", "0");
        ObjectStores.getInst().putObject("price_sign", "0");
        ObjectStores.getInst().putObject("thickness_sign", "0");
        ObjectStores.getInst().putObject("width_sign", "0");
        ObjectStores.getInst().putObject("product_sign", "0");
        ObjectStores.getInst().putObject("thickmin", StringEx.Empty);
        ObjectStores.getInst().putObject("thickmax", StringEx.Empty);
        ObjectStores.getInst().putObject("widemin", StringEx.Empty);
        ObjectStores.getInst().putObject("widemax", StringEx.Empty);
        ObjectStores.getInst().putObject("lengthmin", StringEx.Empty);
        ObjectStores.getInst().putObject("lengthmax", StringEx.Empty);
        ObjectStores.getInst().putObject("pricemin", StringEx.Empty);
        ObjectStores.getInst().putObject("pricemax", StringEx.Empty);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.choose_sift_paixu = null;
        this.choose_sift_allname = null;
        this.TV_choose_sift_price = null;
        this.TV_choose_sift_thickness = null;
        this.TV_choose_sift_width = null;
        this.list.removeAll(this.list);
        this.list = null;
        this.sellername = null;
        this.price = null;
        this.thickness = null;
        this.width = null;
        this.length = null;
        this.number = null;
        this.pname = null;
        this.length_sing = null;
        this.price_sign = null;
        this.thickness_sign = null;
        this.width_sign = null;
        this.product_sign = null;
        this.price_order = null;
        this.shopsign = null;
        super.onDestroy();
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.radio_search3:
                ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
                return;
            case R.id.radio_shopcar3:
                ((RadioButton) findViewById(R.id.radio_shopcar3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ShopActivity.class);
                return;
            case R.id.radio_home3:
                ((RadioButton) findViewById(R.id.radio_home3)).setChecked(true);
                HashMap hasmap = new HashMap();
                hasmap.put("index_more", "1");
                ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class, hasmap);
                return;
            case R.id.radio_contract3:
                ((RadioButton) findViewById(R.id.radio_contract3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ContractListActivity.class);
                return;
            case R.id.radio_notice3:
                ((RadioButton) findViewById(R.id.radio_notice3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, Xhjy_more.class);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
        this.radioderGroup = (RadioGroup) findViewById(R.id.xhjy_index_RGroup);
        this.radioderGroup.setOnCheckedChangeListener(this);
        super.onResume();
    }
}
