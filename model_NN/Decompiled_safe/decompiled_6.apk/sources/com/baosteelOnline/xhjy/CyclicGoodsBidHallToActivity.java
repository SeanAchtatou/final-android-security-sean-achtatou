package com.baosteelOnline.xhjy;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.R;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.data.RequestBidData;
import com.baosteelOnline.data_parse.ContractParse;
import com.baosteelOnline.main.ExitApplication;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;

public class CyclicGoodsBidHallToActivity extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    public ContractParse contractParse;
    private JQUICallBack httpCallBack = new JQUICallBack() {
        public void callBack(ResultData result) {
            CyclicGoodsBidHallToActivity.this.progressDialog.dismiss();
            Log.d("result.getStringData获取的数据：", result.getStringData());
            if (!result.getStringData().equals(null) && !result.getStringData().equals(StringEx.Empty)) {
                CyclicGoodsBidHallToActivity.this.contractParse = ContractParse.parse(result.getStringData());
                if (ContractParse.getDataString().equals(null) || ContractParse.getDataString().equals(StringEx.Empty)) {
                    new AlertDialog.Builder(CyclicGoodsBidHallToActivity.this).setMessage("获取数据失败").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
                }
            }
        }
    };
    private String mHtmlData = StringEx.Empty;
    public RadioButton mIndexNavigation1;
    public RadioButton mIndexNavigation2;
    public RadioButton mIndexNavigation3;
    public RadioButton mIndexNavigation4;
    public RadioButton mIndexNavigation5;
    public TextView mListTitleTextview;
    public RadioGroup mRadioderGroup;
    private WebView mWebView;
    private String ppid = StringEx.Empty;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    private String url = StringEx.Empty;
    private String wtid = StringEx.Empty;
    private String wtidShort = StringEx.Empty;

    /* access modifiers changed from: protected */
    @SuppressLint({"SetJavaScriptEnabled"})
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.activity_cyclic_goods_bid_hall_to);
        ExitApplication.getInstance().addActivity(this);
        this.ppid = getIntent().getExtras().getString("ppid");
        this.wtid = getIntent().getExtras().getString("wtid");
        this.wtidShort = this.wtid.substring(0, this.wtid.length() - 2);
        this.url = "http://10.60.1.25:8081/newexchange/b-1-a/competitiveBidding.do?method=showPPzy&ppid=" + this.ppid + "&wtid=" + this.wtidShort;
        this.mWebView = (WebView) findViewById(R.id.cyclicgoods_bid_hall_to_webview);
        this.mWebView.getSettings().setBuiltInZoomControls(true);
        this.mWebView.setBackgroundColor(Color.parseColor("#ffffff"));
        this.mWebView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        this.mWebView.getSettings().setUseWideViewPort(true);
        this.mWebView.getSettings().setLoadWithOverviewMode(true);
        this.mWebView.setFocusable(true);
        this.mWebView.requestFocus();
        this.mWebView.clearCache(true);
        this.mWebView.getSettings().setJavaScriptEnabled(true);
        this.mWebView.setWebViewClient(new HelloWebViewClient(this, null));
        this.mRadioderGroup = (RadioGroup) findViewById(R.id.cyclic_goods_index_RGroup);
        this.mIndexNavigation1 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item1);
        this.mIndexNavigation2 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item2);
        this.mIndexNavigation3 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item3);
        this.mIndexNavigation4 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item4);
        this.mIndexNavigation5 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item5);
        this.mRadioderGroup.setOnCheckedChangeListener(this);
        this.mIndexNavigation4.setChecked(true);
        this.mListTitleTextview = (TextView) findViewById(R.id.cyclicgoods_list_title_textview);
        this.mListTitleTextview.setText("资源详情");
        this.mWebView.loadUrl(this.url);
    }

    private class HelloWebViewClient extends WebViewClient {
        private HelloWebViewClient() {
        }

        /* synthetic */ HelloWebViewClient(CyclicGoodsBidHallToActivity cyclicGoodsBidHallToActivity, HelloWebViewClient helloWebViewClient) {
            this();
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    public void getDate() {
        RequestBidData requestBidData = new RequestBidData(this);
        requestBidData.setRequestData("竞价大厅的资源页");
        Log.d("请求的指令：", requestBidData.infoDate());
        requestBidData.setHttpCallBack(this.httpCallBack);
        requestBidData.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.cyclicgoods_index_navigation_item2:
                this.mIndexNavigation2.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item3:
                this.mIndexNavigation3.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidSessionSelect.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item1:
                if (ObjectStores.getInst().getObject("formX") == "formX") {
                    ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class);
                    finish();
                    return;
                }
                this.mIndexNavigation1.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item4:
                this.mIndexNavigation4.setChecked(true);
                return;
            case R.id.cyclicgoods_index_navigation_item5:
                this.mIndexNavigation5.setChecked(true);
                Intent intent = new Intent(this, Xhjy_more.class);
                intent.putExtra("part", "循环物资");
                startActivity(intent);
                finish();
                return;
            default:
                return;
        }
    }

    public void backAaction(View v) {
        startActivity(new Intent(this, CyclicGoodsBidHallPageActivity.class));
        finish();
    }

    public void onBackPressed() {
        startActivity(new Intent(this, CyclicGoodsBidHallPageActivity.class));
        finish();
    }
}
