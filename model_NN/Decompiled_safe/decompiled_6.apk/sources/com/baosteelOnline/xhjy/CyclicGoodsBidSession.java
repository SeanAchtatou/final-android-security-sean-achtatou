package com.baosteelOnline.xhjy;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.MotionEventCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.R;
import com.baosteelOnline.common.DataBaseFactory;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.data.RequestBidData;
import com.baosteelOnline.data_parse.ContractParse;
import com.baosteelOnline.dl.Login_indexActivity;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.model.SmallNumUtil;
import com.baosteelOnline.sql.DBHelper;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;

public class CyclicGoodsBidSession extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    public ContractParse contractParse;
    private DataBaseFactory db;
    private DBHelper dbHelper;
    /* access modifiers changed from: private */
    public String dfyj = StringEx.Empty;
    /* access modifiers changed from: private */
    public String endDate = StringEx.Empty;
    /* access modifiers changed from: private */
    public String ggid = StringEx.Empty;
    /* access modifiers changed from: private */
    public String ggmc = StringEx.Empty;
    private JQUICallBack httpCallBack = new JQUICallBack() {
        public void callBack(ResultData result) {
            CyclicGoodsBidSession.this.progressDialog.dismiss();
            Log.d("获取的数据：", result.getStringData());
            String dataString = result.getStringData();
            if (!dataString.equals(null) && !dataString.equals(StringEx.Empty)) {
                CyclicGoodsBidSession.this.contractParse = ContractParse.parse(result.getStringData());
                if (ContractParse.getDataString().equals(null) || ContractParse.getDataString().equals(StringEx.Empty)) {
                    new AlertDialog.Builder(CyclicGoodsBidSession.this).setMessage("获取数据失败").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
                } else if (ContractParse.getDataString().equals(null) || ContractParse.getDataString().equals(StringEx.Empty) || ContractParse.jjo.toString().equals("[]") || ContractParse.jjo.toString().equals("[[]]")) {
                    new AlertDialog.Builder(CyclicGoodsBidSession.this).setMessage("数据为空！").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
                } else {
                    for (int i = 0; i < ContractParse.jjo.length(); i++) {
                        try {
                            JSONArray row = new JSONArray(ContractParse.jjo.getString(i));
                            HashMap<String, Object> item = new HashMap<>();
                            CyclicGoodsBidSession.this.wtid = row.getString(0);
                            CyclicGoodsBidSession.this.ggmc = row.getString(1);
                            CyclicGoodsBidSession.this.ggid = row.getString(2);
                            CyclicGoodsBidSession.this.joinStatus = row.getString(3);
                            CyclicGoodsBidSession.this.hzjc = row.getString(4);
                            CyclicGoodsBidSession.this.dfyj = row.getString(5);
                            CyclicGoodsBidSession.this.kssj = row.getString(6);
                            CyclicGoodsBidSession.this.jssj = row.getString(7);
                            CyclicGoodsBidSession.this.isCanJoin = row.getString(8);
                            CyclicGoodsBidSession.this.ssdl = row.getString(9);
                            item.put("wtid", CyclicGoodsBidSession.this.wtid);
                            item.put("ggmc", CyclicGoodsBidSession.this.ggmc);
                            item.put("ggid", CyclicGoodsBidSession.this.ggid);
                            item.put("joinStatus", CyclicGoodsBidSession.this.joinStatus);
                            item.put("hzjc", CyclicGoodsBidSession.this.hzjc);
                            item.put("dfyj", CyclicGoodsBidSession.this.dfyj);
                            item.put("kssj", CyclicGoodsBidSession.this.kssj);
                            item.put("jssj", CyclicGoodsBidSession.this.jssj);
                            item.put("isCanJoin", CyclicGoodsBidSession.this.isCanJoin);
                            item.put("ssdl", CyclicGoodsBidSession.this.ssdl);
                            Log.d("ROW数据：", "  wtid = " + CyclicGoodsBidSession.this.wtid + "  ggmc = " + CyclicGoodsBidSession.this.ggmc + "  ggid = " + CyclicGoodsBidSession.this.ggid + "  joinStatus = " + CyclicGoodsBidSession.this.joinStatus + "  hzjc = " + CyclicGoodsBidSession.this.hzjc + "  dfyj = " + CyclicGoodsBidSession.this.dfyj + "  kssj = " + CyclicGoodsBidSession.this.kssj + "  jssj = " + CyclicGoodsBidSession.this.jssj + "  isCanJoin = " + CyclicGoodsBidSession.this.isCanJoin + " ssdl = " + CyclicGoodsBidSession.this.ssdl);
                            CyclicGoodsBidSession.this.mItemArrayList.add(item);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    CyclicGoodsBidSession.this.myAdapter.notifyDataSetChanged();
                }
            }
        }
    };
    private JQUICallBack httpCallBackOutBid = new JQUICallBack() {
        public void callBack(ResultData result) {
            Log.d("获取的数据：", result.getStringData());
            String dataString = result.getStringData();
            if (!dataString.equals(null) && !dataString.equals(StringEx.Empty)) {
                CyclicGoodsBidSession.this.contractParse = ContractParse.parse(result.getStringData());
                Log.d("获取的竞价大厅列表数据，已解密，未解析：", ContractParse.getDecryptData());
                if (ContractParse.getDataString().equals(null) || ContractParse.getDataString().equals(StringEx.Empty)) {
                    new AlertDialog.Builder(CyclicGoodsBidSession.this).setMessage("获取数据失败").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
                    return;
                }
                Log.d("获取的ROWs数据：", ContractParse.jjo.toString());
                if (ContractParse.jjo.toString().equals(null) || ContractParse.jjo.toString().equals("[[]]") || ContractParse.jjo.toString().equals(StringEx.Empty) || ContractParse.jjo.toString().equals("[]")) {
                    new AlertDialog.Builder(CyclicGoodsBidSession.this).setMessage("数据为空！").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
                    return;
                }
                int i = 0;
                while (i < ContractParse.jjo.length()) {
                    try {
                        CyclicGoodsBidSession.this.setOutBidResult = new JSONArray(ContractParse.jjo.getString(i)).getString(0);
                        if (CyclicGoodsBidSession.this.setOutBidResult.equals(null) || CyclicGoodsBidSession.this.setOutBidResult.equals(StringEx.Empty) || !CyclicGoodsBidSession.this.setOutBidResult.equals("0")) {
                            if (!CyclicGoodsBidSession.this.setOutBidResult.equals(null) && !CyclicGoodsBidSession.this.setOutBidResult.equals(StringEx.Empty) && CyclicGoodsBidSession.this.setOutBidResult.equals("1")) {
                                new AlertDialog.Builder(CyclicGoodsBidSession.this).setMessage("退出成功!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        CyclicGoodsBidSession.this.mItemArrayList.clear();
                                        CyclicGoodsBidSession.this.getData();
                                        dialog.cancel();
                                    }
                                }).show();
                            }
                            i++;
                        } else {
                            new AlertDialog.Builder(CyclicGoodsBidSession.this).setMessage("退出失败!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            }).show();
                            i++;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public String hzjc = StringEx.Empty;
    private LayoutInflater inflater;
    /* access modifiers changed from: private */
    public String isCanJoin = StringEx.Empty;
    /* access modifiers changed from: private */
    public String joinStatus = StringEx.Empty;
    /* access modifiers changed from: private */
    public String jssj = StringEx.Empty;
    /* access modifiers changed from: private */
    public String kssj = StringEx.Empty;
    private RadioButton mIndexNavigation1;
    private RadioButton mIndexNavigation2;
    private RadioButton mIndexNavigation3;
    private RadioButton mIndexNavigation4;
    private RadioButton mIndexNavigation5;
    /* access modifiers changed from: private */
    public ArrayList<HashMap<String, Object>> mItemArrayList;
    public TextView mListTitleTextview;
    private ListView mListView;
    private ImageButton mOut;
    private ImageButton mPrice;
    private RadioGroup mRadioderGroup;
    private SimpleAdapter mSimpleAdater;
    /* access modifiers changed from: private */
    public MyAdapter myAdapter;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    /* access modifiers changed from: private */
    public int sessionCode = 30;
    /* access modifiers changed from: private */
    public String setOutBidResult = StringEx.Empty;
    /* access modifiers changed from: private */
    public String ssdl = StringEx.Empty;
    /* access modifiers changed from: private */
    public String startDate = StringEx.Empty;
    /* access modifiers changed from: private */
    public String wtid = StringEx.Empty;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.activity_cyclic_goods_bid_session);
        ExitApplication.getInstance().addActivity(this);
        this.dbHelper = DataBaseFactory.getInstance(this);
        this.dbHelper.insertOperation("循环物资", "竞价场次", "竞价场次搜索");
        this.myAdapter = new MyAdapter(this);
        this.sessionCode = getIntent().getExtras().getInt("sessionCode");
        this.startDate = getIntent().getExtras().getString("startDate");
        this.endDate = getIntent().getExtras().getString("endDate");
        this.inflater = getLayoutInflater();
        View inflate = this.inflater.inflate((int) R.layout.activity_cyclic_goods_bid_session_item, (ViewGroup) null);
        Log.d("获取的数据：", "sessionCode = " + this.sessionCode + "  startDate=" + this.startDate + " endDate=" + this.endDate);
        this.mListView = (ListView) findViewById(R.id.cyclicgoods_bid_session_listview);
        this.mItemArrayList = new ArrayList<>();
        this.mRadioderGroup = (RadioGroup) findViewById(R.id.cyclic_goods_index_RGroup);
        this.mIndexNavigation1 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item1);
        this.mIndexNavigation2 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item2);
        this.mIndexNavigation3 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item3);
        this.mIndexNavigation4 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item4);
        this.mIndexNavigation5 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item5);
        this.mListTitleTextview = (TextView) findViewById(R.id.cyclicgoods_list_title_textview);
        this.mListTitleTextview.setText("竞价场次");
        SmallNumUtil.JJNum(this, (TextView) findViewById(R.id.jj_dhqrnum));
        this.mIndexNavigation3.setChecked(true);
        this.mRadioderGroup.setOnCheckedChangeListener(this);
        this.mListView.setAdapter((ListAdapter) this.myAdapter);
        this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                int i = arg2 + 1;
                Intent intent = new Intent(CyclicGoodsBidSession.this, CyclicGoodsBidPageActivity.class);
                String id = ((HashMap) CyclicGoodsBidSession.this.mItemArrayList.get(arg2)).get("ggid").toString();
                Log.d("竞价场次列表CyclicGoodsBidSession传过去的ggid：", id);
                if (id.equals(null) || id.equals(StringEx.Empty) || id.equals("null")) {
                    new AlertDialog.Builder(CyclicGoodsBidSession.this).setMessage("该场次暂无竞价公告").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
                    return;
                }
                intent.putExtra("id", id);
                intent.putExtra("activity", "竞价场次");
                intent.putExtra("sessionCode", CyclicGoodsBidSession.this.sessionCode);
                intent.putExtra("startDate", CyclicGoodsBidSession.this.startDate);
                intent.putExtra("endDate", CyclicGoodsBidSession.this.endDate);
                CyclicGoodsBidSession.this.startActivity(intent);
                CyclicGoodsBidSession.this.finish();
            }
        });
        getData();
    }

    /* access modifiers changed from: private */
    public void outBid(String wtid2) {
        RequestBidData requestBidData = new RequestBidData(this);
        requestBidData.setRequestData("退出竞价");
        requestBidData.setId(wtid2);
        Log.d("请求的指令,加密后：", requestBidData.infoDate());
        requestBidData.setHttpCallBack(this.httpCallBackOutBid);
        requestBidData.iExecute();
    }

    /* access modifiers changed from: private */
    public void getData() {
        RequestBidData requestBidData = new RequestBidData(this);
        requestBidData.setRequestData("竞价场次列表");
        requestBidData.setSessionCode(new StringBuilder(String.valueOf(this.sessionCode)).toString());
        requestBidData.setStartDate(this.startDate);
        requestBidData.setEndDate(this.endDate);
        Log.d("请求的指令：", requestBidData.infoDate());
        requestBidData.setHttpCallBack(this.httpCallBack);
        requestBidData.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.cyclicgoods_index_navigation_item2:
                this.mIndexNavigation2.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item3:
                this.mIndexNavigation3.setChecked(true);
                return;
            case R.id.cyclicgoods_index_navigation_item1:
                if (ObjectStores.getInst().getObject("formX") == "formX") {
                    ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class);
                    finish();
                    return;
                }
                this.mIndexNavigation1.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item4:
                this.mIndexNavigation4.setChecked(true);
                startActivity(new Intent(this, CyclicGoodsBidHallPageActivity.class));
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item5:
                this.mIndexNavigation5.setChecked(true);
                Intent intent = new Intent(this, Xhjy_more.class);
                intent.putExtra("part", "循环物资");
                startActivity(intent);
                finish();
                return;
            default:
                return;
        }
    }

    public void backAaction(View v) {
        startActivity(new Intent(this, CyclicGoodsBidSessionSelect.class));
        finish();
    }

    public void selectAction(View v) {
        startActivity(new Intent(this, CyclicGoodsBidSessionSelect.class));
        finish();
    }

    public void onBackPressed() {
        startActivity(new Intent(this, CyclicGoodsBidSessionSelect.class));
        finish();
    }

    public void priceAaction(View v) {
        startActivity(new Intent(this, CyclicGoodsBidHallPageActivity.class));
        finish();
    }

    public class ViewHolder {
        public ImageView goodImage;
        public TextView guaranteeTextView;
        public ImageButton joinOrOutImageButton;
        public TextView joinStatusTextView;
        public ImageButton priceImageButton;
        public TextView sellerTextView;
        public TextView timeTextView;
        public TextView titleTextView;

        public ViewHolder() {
        }
    }

    public class MyAdapter extends BaseAdapter {
        private LayoutInflater mMyAdapterInflater;

        public MyAdapter(Context context) {
            this.mMyAdapterInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return CyclicGoodsBidSession.this.mItemArrayList.size();
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            int i = position + 1;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = this.mMyAdapterInflater.inflate((int) R.layout.activity_cyclic_goods_bid_session_item, (ViewGroup) null);
                holder.titleTextView = (TextView) convertView.findViewById(R.id.cyclicgoods_bid_session_item_textview_title);
                holder.joinStatusTextView = (TextView) convertView.findViewById(R.id.cyclicgoods_bid_session_item_textview_join_state1);
                holder.sellerTextView = (TextView) convertView.findViewById(R.id.cyclicgoods_bid_session_item_textview_seller);
                holder.guaranteeTextView = (TextView) convertView.findViewById(R.id.cyclicgoods_bid_session_item_textview_guarantee);
                holder.timeTextView = (TextView) convertView.findViewById(R.id.cyclicgoods_bid_session_item_textview_bid_time);
                holder.goodImage = (ImageView) convertView.findViewById(R.id.cyclic_goods_imageview);
                holder.priceImageButton = (ImageButton) convertView.findViewById(R.id.cyclicgoods_bid_session_item_price_imbt);
                holder.joinOrOutImageButton = (ImageButton) convertView.findViewById(R.id.cyclicgoods_bid_session_item_out_imbt);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            String dfyj = (String) ((HashMap) CyclicGoodsBidSession.this.mItemArrayList.get(position)).get("dfyj");
            String kssj = (String) ((HashMap) CyclicGoodsBidSession.this.mItemArrayList.get(position)).get("kssj");
            String jssj = (String) ((HashMap) CyclicGoodsBidSession.this.mItemArrayList.get(position)).get("jssj");
            String joinStatus = (String) ((HashMap) CyclicGoodsBidSession.this.mItemArrayList.get(position)).get("joinStatus");
            String str = (String) ((HashMap) CyclicGoodsBidSession.this.mItemArrayList.get(position)).get("isCanJoin");
            String ssdl = (String) ((HashMap) CyclicGoodsBidSession.this.mItemArrayList.get(position)).get("ssdl");
            String wtid = (String) ((HashMap) CyclicGoodsBidSession.this.mItemArrayList.get(position)).get("wtid");
            String str2 = (String) ((HashMap) CyclicGoodsBidSession.this.mItemArrayList.get(position)).get("ggid");
            String dfyjString = new DecimalFormat(",###.0").format(Double.parseDouble(dfyj));
            String cutkssj = kssj.replace(".0", StringEx.Empty);
            String cutjssj = jssj.replace(".0", StringEx.Empty);
            holder.titleTextView.setText((String) ((HashMap) CyclicGoodsBidSession.this.mItemArrayList.get(position)).get("ggmc"));
            holder.sellerTextView.setText("卖方：" + ((String) ((HashMap) CyclicGoodsBidSession.this.mItemArrayList.get(position)).get("hzjc")));
            if (dfyj.equals("0.0") || dfyj.equals("0.00")) {
                holder.guaranteeTextView.setText("保证金:" + dfyj + "元");
            } else {
                holder.guaranteeTextView.setText("保证金:" + dfyjString + "元");
            }
            holder.timeTextView.setText("竞买时间：" + cutkssj + "-" + cutjssj);
            String nowTimeFormat = String.valueOf(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())) + "0";
            String kssjTime = kssj.replaceAll("\\D", StringEx.Empty);
            String jssjTime = jssj.replaceAll("\\D", StringEx.Empty);
            Log.d("当前时间转换格式为", String.valueOf(nowTimeFormat) + " 结束时间=" + jssjTime);
            long nowTimeLong = Long.parseLong(nowTimeFormat);
            long jssjTimeLong = Long.parseLong(jssjTime);
            long kssjTimeLong = Long.parseLong(kssjTime);
            Log.d("当前时间转换格式为Long", String.valueOf(nowTimeLong) + " 结束时间=" + jssjTimeLong);
            final boolean canJoinByTime = nowTimeLong < jssjTimeLong;
            boolean canJoinBykssjTime = nowTimeLong < kssjTimeLong;
            Log.d("当前canJoinByTime", new StringBuilder(String.valueOf(canJoinByTime)).toString());
            if (joinStatus.equals("1")) {
                holder.joinStatusTextView.setText("已参加");
                holder.joinStatusTextView.setTextColor(Color.rgb((int) MotionEventCompat.ACTION_MASK, 82, 47));
                if (canJoinByTime) {
                    holder.joinOrOutImageButton.setVisibility(0);
                    holder.joinOrOutImageButton.setBackgroundResource(R.drawable.cyclicgoods_bid_session_item_out_selector);
                    if (canJoinBykssjTime) {
                        holder.priceImageButton.setVisibility(8);
                    } else {
                        holder.priceImageButton.setVisibility(0);
                    }
                } else {
                    holder.joinOrOutImageButton.setVisibility(8);
                    holder.priceImageButton.setVisibility(8);
                }
            } else if (joinStatus.equals("0")) {
                holder.joinStatusTextView.setTextColor(-16777216);
                holder.joinStatusTextView.setText("未参加");
                if (canJoinByTime) {
                    holder.joinOrOutImageButton.setBackgroundResource(R.drawable.cyclicgoods_bid_session_item_join_selector);
                    holder.joinOrOutImageButton.setVisibility(0);
                    holder.priceImageButton.setVisibility(8);
                } else {
                    holder.joinOrOutImageButton.setVisibility(8);
                    holder.priceImageButton.setVisibility(8);
                }
            }
            if (ssdl.equals("中厚板")) {
                holder.goodImage.setBackgroundResource(R.drawable.zhb);
            } else if (ssdl.equals("棒钢")) {
                holder.goodImage.setBackgroundResource(R.drawable.banggang);
            } else if (ssdl.equals("不锈钢")) {
                holder.goodImage.setBackgroundResource(R.drawable.buxiugang);
            } else if (ssdl.equals("不锈钢盘条")) {
                holder.goodImage.setBackgroundResource(R.drawable.buxiugangpantiao);
            } else if (ssdl.equals("电镀锌")) {
                holder.goodImage.setBackgroundResource(R.drawable.dianduxin);
            } else if (ssdl.equals("电工钢")) {
                holder.goodImage.setBackgroundResource(R.drawable.diangonggang);
            } else if (ssdl.equals("镀铬")) {
                holder.goodImage.setBackgroundResource(R.drawable.duge);
            } else if (ssdl.equals("镀铝锌")) {
                holder.goodImage.setBackgroundResource(R.drawable.dulvxin);
            } else if (ssdl.equals("镀锡")) {
                holder.goodImage.setBackgroundResource(R.drawable.duxi);
            } else if (ssdl.equals("钢管")) {
                holder.goodImage.setBackgroundResource(R.drawable.gangguan);
            } else if (ssdl.equals("钢坯")) {
                holder.goodImage.setBackgroundResource(R.drawable.gangpei);
            } else if (ssdl.equals("冷轧不锈钢板")) {
                holder.goodImage.setBackgroundResource(R.drawable.lengzhabuxiugangguan);
            } else if (ssdl.equals("普冷")) {
                holder.goodImage.setBackgroundResource(R.drawable.puleng);
            } else if (ssdl.equals("热镀锌")) {
                holder.goodImage.setBackgroundResource(R.drawable.reduxin);
            } else if (ssdl.equals("热轧")) {
                holder.goodImage.setBackgroundResource(R.drawable.rezha);
            } else if (ssdl.equals("热轧不锈钢板")) {
                holder.goodImage.setBackgroundResource(R.drawable.rezhabuxiugangguan);
            } else if (ssdl.equals("酸洗")) {
                holder.goodImage.setBackgroundResource(R.drawable.suanxi);
            } else if (ssdl.equals("特殊钢")) {
                holder.goodImage.setBackgroundResource(R.drawable.teshugang);
            } else if (ssdl.equals("线材")) {
                holder.goodImage.setBackgroundResource(R.drawable.xiancai);
            } else if (ssdl.equals("型钢")) {
                holder.goodImage.setBackgroundResource(R.drawable.xinggang);
            } else if (ssdl.equals("油脂类")) {
                holder.goodImage.setBackgroundResource(R.drawable.youzhi);
            } else if (ssdl.equals("耐材类")) {
                holder.goodImage.setBackgroundResource(R.drawable.naicai);
            } else if (ssdl.equals("电器")) {
                holder.goodImage.setBackgroundResource(R.drawable.dianqi);
            } else if (ssdl.equals("建材")) {
                holder.goodImage.setBackgroundResource(R.drawable.jiancai);
            } else if (ssdl.equals("五金")) {
                holder.goodImage.setBackgroundResource(R.drawable.wujin);
            } else if (ssdl.equals("橡塑")) {
                holder.goodImage.setBackgroundResource(R.drawable.xiangsu);
            } else if (ssdl.equals("轧辊")) {
                holder.goodImage.setBackgroundResource(R.drawable.gagun);
            } else if (ssdl.equals("构筑物")) {
                holder.goodImage.setBackgroundResource(R.drawable.gozhuwu);
            } else if (ssdl.equals("汽配件")) {
                holder.goodImage.setBackgroundResource(R.drawable.qipeijian);
            } else if (ssdl.equals("包装废料")) {
                holder.goodImage.setBackgroundResource(R.drawable.baozhuangfeiliao);
            } else if (ssdl.equals("电气设备")) {
                holder.goodImage.setBackgroundResource(R.drawable.dianqishebei);
            } else if (ssdl.equals("动力设备")) {
                holder.goodImage.setBackgroundResource(R.drawable.dlsb);
            } else if (ssdl.equals("管网设备")) {
                holder.goodImage.setBackgroundResource(R.drawable.gwsb);
            } else if (ssdl.equals("焊机设备")) {
                holder.goodImage.setBackgroundResource(R.drawable.hjsb);
            } else if (ssdl.equals("合金钢类")) {
                holder.goodImage.setBackgroundResource(R.drawable.hjgl);
            } else if (ssdl.equals("黑色金属")) {
                holder.goodImage.setBackgroundResource(R.drawable.hsjs);
            } else if (ssdl.equals("机加设备")) {
                holder.goodImage.setBackgroundResource(R.drawable.jjsb);
            } else if (ssdl.equals("空调设备")) {
                holder.goodImage.setBackgroundResource(R.drawable.ktsb);
            } else if (ssdl.equals("矿化设备")) {
                holder.goodImage.setBackgroundResource(R.drawable.khsb);
            } else if (ssdl.equals("其他设备")) {
                holder.goodImage.setBackgroundResource(R.drawable.qtsb);
            } else if (ssdl.equals("其他资材")) {
                holder.goodImage.setBackgroundResource(R.drawable.qtzc);
            } else if (ssdl.equals("起重设备")) {
                holder.goodImage.setBackgroundResource(R.drawable.qzsb);
            } else if (ssdl.equals("生产废料")) {
                holder.goodImage.setBackgroundResource(R.drawable.scfl);
            } else if (ssdl.equals("稀有金属")) {
                holder.goodImage.setBackgroundResource(R.drawable.xyjs);
            } else if (ssdl.equals("液压设备")) {
                holder.goodImage.setBackgroundResource(R.drawable.yysb);
            } else if (ssdl.equals("有色金属")) {
                holder.goodImage.setBackgroundResource(R.drawable.ysjs);
            } else if (ssdl.equals("整体产线")) {
                holder.goodImage.setBackgroundResource(R.drawable.ztcx);
            } else if (ssdl.equals("办公类设备")) {
                holder.goodImage.setBackgroundResource(R.drawable.bglsb);
            } else if (ssdl.equals("工程机械设备")) {
                holder.goodImage.setBackgroundResource(R.drawable.gcj);
            } else if (ssdl.equals("仪器仪表设备")) {
                holder.goodImage.setBackgroundResource(R.drawable.yqyb);
            } else if (ssdl.equals("渣泥类")) {
                holder.goodImage.setBackgroundResource(R.drawable.znl);
            } else if (ssdl.equals("运输设备")) {
                holder.goodImage.setBackgroundResource(R.drawable.yssb);
            } else if (ssdl.equals("捆带")) {
                holder.goodImage.setBackgroundResource(R.drawable.fcc);
            } else if (ssdl.equals("棒钢废次材")) {
                holder.goodImage.setBackgroundResource(R.drawable.banggang);
            } else if (ssdl.equals("彩涂废次材")) {
                holder.goodImage.setBackgroundResource(R.drawable.fcc);
            } else if (ssdl.equals("镀铬废次材")) {
                holder.goodImage.setBackgroundResource(R.drawable.duge);
            } else if (ssdl.equals("镀铝废次材")) {
                holder.goodImage.setBackgroundResource(R.drawable.fcc);
            } else if (ssdl.equals("镀铅废次材")) {
                holder.goodImage.setBackgroundResource(R.drawable.fcc);
            } else if (ssdl.equals("镀锡废次材")) {
                holder.goodImage.setBackgroundResource(R.drawable.duxi);
            } else if (ssdl.equals("钢管废次材")) {
                holder.goodImage.setBackgroundResource(R.drawable.gangguan);
            } else if (ssdl.equals("钢筋废次材")) {
                holder.goodImage.setBackgroundResource(R.drawable.fcc);
            } else if (ssdl.equals("钢坯废次材")) {
                holder.goodImage.setBackgroundResource(R.drawable.gangpei);
            } else if (ssdl.equals("捆带废次材")) {
                holder.goodImage.setBackgroundResource(R.drawable.fcc);
            } else if (ssdl.equals("普冷废次材")) {
                holder.goodImage.setBackgroundResource(R.drawable.puleng);
            } else if (ssdl.equals("其它废次材")) {
                holder.goodImage.setBackgroundResource(R.drawable.fcc);
            } else if (ssdl.equals("热轧废次材")) {
                holder.goodImage.setBackgroundResource(R.drawable.rezha);
            } else if (ssdl.equals("酸洗废次材")) {
                holder.goodImage.setBackgroundResource(R.drawable.suanxi);
            } else if (ssdl.equals("线材废次材")) {
                holder.goodImage.setBackgroundResource(R.drawable.xiancai);
            } else if (ssdl.equals("型钢废次材")) {
                holder.goodImage.setBackgroundResource(R.drawable.xinggang);
            } else if (ssdl.equals("轧硬废次材")) {
                holder.goodImage.setBackgroundResource(R.drawable.fcc);
            } else if (ssdl.equals("不锈钢废次材")) {
                holder.goodImage.setBackgroundResource(R.drawable.buxiugang);
            } else if (ssdl.equals("电镀锌废次材")) {
                holder.goodImage.setBackgroundResource(R.drawable.dianduxin);
            } else if (ssdl.equals("电工钢废次材")) {
                holder.goodImage.setBackgroundResource(R.drawable.diangonggang);
            } else if (ssdl.equals("镀铝铅废次材")) {
                holder.goodImage.setBackgroundResource(R.drawable.fcc);
            } else if (ssdl.equals("镀铝锌废次材")) {
                holder.goodImage.setBackgroundResource(R.drawable.dulvxin);
            } else if (ssdl.equals("镀锡锌废次材")) {
                holder.goodImage.setBackgroundResource(R.drawable.fcc);
            } else if (ssdl.equals("复合板废次材")) {
                holder.goodImage.setBackgroundResource(R.drawable.fcc);
            } else if (ssdl.equals("热镀锌废次材")) {
                holder.goodImage.setBackgroundResource(R.drawable.reduxin);
            } else if (ssdl.equals("特殊钢废次材")) {
                holder.goodImage.setBackgroundResource(R.drawable.teshugang);
            } else if (ssdl.equals("中厚板废次材")) {
                holder.goodImage.setBackgroundResource(R.drawable.zhb);
            } else {
                holder.goodImage.setBackgroundResource(R.drawable.nophotosmall);
            }
            holder.priceImageButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    CyclicGoodsBidSession.this.startActivity(new Intent(CyclicGoodsBidSession.this, CyclicGoodsBidHallPageActivity.class));
                    CyclicGoodsBidSession.this.finish();
                }
            });
            final String str3 = joinStatus;
            final String str4 = wtid;
            holder.joinOrOutImageButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (str3.equals("1")) {
                        Log.d("wtid==", str4);
                        CyclicGoodsBidSession.this.outBid(str4);
                    } else if (str3.equals("0") && canJoinByTime) {
                        if (ObjectStores.getInst().getObject("reStr") != "1") {
                            AlertDialog.Builder dialog = new AlertDialog.Builder(CyclicGoodsBidSession.this);
                            dialog.setCancelable(false);
                            dialog.setMessage("您还没有登录，请登录！").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    SharedPreferences.Editor editor = CyclicGoodsBidSession.this.getSharedPreferences("index_mark", 2).edit();
                                    editor.putString("index", "竞价场次页面");
                                    editor.commit();
                                    Intent intent = new Intent(CyclicGoodsBidSession.this, Login_indexActivity.class);
                                    intent.putExtra("sessionCode", CyclicGoodsBidSession.this.sessionCode);
                                    intent.putExtra("startDate", CyclicGoodsBidSession.this.startDate);
                                    intent.putExtra("endDate", CyclicGoodsBidSession.this.endDate);
                                    CyclicGoodsBidSession.this.startActivity(intent);
                                    CyclicGoodsBidSession.this.finish();
                                }
                            }).show();
                            return;
                        }
                        Intent intent = new Intent(CyclicGoodsBidSession.this, CyclicGoodsBidProtocolPageActivity.class);
                        intent.putExtra("wtid", str4);
                        intent.putExtra("sessionCode", CyclicGoodsBidSession.this.sessionCode);
                        intent.putExtra("startDate", CyclicGoodsBidSession.this.startDate);
                        intent.putExtra("endDate", CyclicGoodsBidSession.this.endDate);
                        intent.putExtra("activity", "竞价场次");
                        CyclicGoodsBidSession.this.startActivity(intent);
                        CyclicGoodsBidSession.this.finish();
                    }
                }
            });
            return convertView;
        }
    }
}
