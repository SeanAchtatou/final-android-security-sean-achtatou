package com.baosteelOnline.xhjy;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.TextView;

public class LineText extends TextView {
    public LineText(Context context) {
        super(context);
    }

    public LineText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LineText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint Paint = new Paint();
        Paint.setColor(-65536);
        Paint.setStrokeWidth(4.0f);
        canvas.drawLine(0.0f, (float) (getHeight() - 1), (float) getWidth(), (float) (getHeight() - 1), Paint);
    }
}
