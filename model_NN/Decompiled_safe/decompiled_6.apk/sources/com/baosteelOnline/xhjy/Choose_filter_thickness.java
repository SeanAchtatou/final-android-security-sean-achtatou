package com.baosteelOnline.xhjy;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.R;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.model.SmallNumUtil;
import com.jianq.misc.StringEx;
import java.util.HashMap;

public class Choose_filter_thickness extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    private Button btn_ok;
    /* access modifiers changed from: private */
    public int longmax = 0;
    /* access modifiers changed from: private */
    public int longmin = 0;
    private RadioGroup radioderGroup;
    /* access modifiers changed from: private */
    public EditText thickMax;
    /* access modifiers changed from: private */
    public EditText thickMin;
    /* access modifiers changed from: private */
    public String thicknessMax;
    /* access modifiers changed from: private */
    public String thicknessMin;
    private TextView titlleText;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        ExitApplication.getInstance().addActivity(this);
        setContentView((int) R.layout.xhjy_choose_filter_thickness);
        this.titlleText = (TextView) findViewById(R.id.TV_titletext_history_length);
        this.titlleText.setText("搜索厚度");
        this.thickMin = (EditText) findViewById(R.id.choose_filter_thcknessMin);
        this.thickMax = (EditText) findViewById(R.id.choose_filter_thckness8Max);
        ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
        SmallNumUtil.GWCNum(this, (TextView) findViewById(R.id.gwc_dhqrnum));
        this.radioderGroup = (RadioGroup) findViewById(R.id.xhjy_index_RGroup);
        this.radioderGroup.setOnCheckedChangeListener(this);
        this.btn_ok = (Button) findViewById(R.id.choose_filter_thickness_ok);
        this.btn_ok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Choose_filter_thickness.this.thicknessMin = Choose_filter_thickness.this.thickMin.getText().toString();
                Choose_filter_thickness.this.thicknessMax = Choose_filter_thickness.this.thickMax.getText().toString();
                try {
                    if (!Choose_filter_thickness.this.thicknessMin.equals(StringEx.Empty)) {
                        Choose_filter_thickness.this.longmin = Integer.valueOf(Choose_filter_thickness.this.thicknessMin).intValue();
                    }
                    if (!Choose_filter_thickness.this.thicknessMax.equals(StringEx.Empty)) {
                        Choose_filter_thickness.this.longmax = Integer.valueOf(Choose_filter_thickness.this.thicknessMax).intValue();
                    }
                } catch (Exception e) {
                }
                if (Choose_filter_thickness.this.thicknessMin.equals(StringEx.Empty) && Choose_filter_thickness.this.thicknessMax.equals(StringEx.Empty)) {
                    new AlertDialog.Builder(Choose_filter_thickness.this).setMessage("输入的最小值必须比最大值小！").setPositiveButton("确认", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                        }
                    }).show();
                } else if (Choose_filter_thickness.this.longmin > Choose_filter_thickness.this.longmax) {
                    new AlertDialog.Builder(Choose_filter_thickness.this).setMessage("输入的最小值必须比最大值小！").setPositiveButton("确认", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                        }
                    }).show();
                } else {
                    ObjectStores.getInst().putObject("thickness_sign", "11");
                    ObjectStores.getInst().putObject("thickmin", Choose_filter_thickness.this.thicknessMin);
                    ObjectStores.getInst().putObject("thickmax", Choose_filter_thickness.this.thicknessMax);
                    ExitApplication.getInstance().startActivity(Choose_filter_thickness.this, ChooseActivity.class);
                }
            }
        });
    }

    public void xhjy_choose_back_ButtonAction(View v) {
        ExitApplication.getInstance().back(this);
    }

    public void onBackPressed() {
        ExitApplication.getInstance().back(0);
        super.onBackPressed();
    }

    public void choose_filter_all_thicknessAction(View v) {
        ObjectStores.getInst().putObject("thickness_sign", "00");
        ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
    }

    public void choose_filter_all_thicknessActions(View v) {
        ObjectStores.getInst().putObject("thickness_sign", "01");
        ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
    }

    public void choose_filter_all_thickness14Action(View v) {
        ObjectStores.getInst().putObject("thickness_sign", "02");
        ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
    }

    public void choose_filter_all_thickness46Action(View v) {
        ObjectStores.getInst().putObject("thickness_sign", "03");
        ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
    }

    public void choose_filter_all_thickness68Action(View v) {
        ObjectStores.getInst().putObject("thickness_sign", "04");
        ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
    }

    public void choose_filter_all_thickness8Action(View v) {
        ObjectStores.getInst().putObject("thickness_sign", "05");
        ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.thickMin = null;
        this.thickMax = null;
        this.btn_ok = null;
        this.thicknessMin = null;
        this.thicknessMax = null;
        this.longmin = 0;
        this.longmax = 0;
        super.onDestroy();
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.radio_search3:
                ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
                return;
            case R.id.radio_shopcar3:
                ((RadioButton) findViewById(R.id.radio_shopcar3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ShopActivity.class);
                return;
            case R.id.radio_home3:
                ((RadioButton) findViewById(R.id.radio_home3)).setChecked(true);
                HashMap hasmap = new HashMap();
                hasmap.put("index_more", "1");
                ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class, hasmap);
                return;
            case R.id.radio_contract3:
                ((RadioButton) findViewById(R.id.radio_contract3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ContractListActivity.class);
                return;
            case R.id.radio_notice3:
                ((RadioButton) findViewById(R.id.radio_notice3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, Xhjy_more.class);
                return;
            default:
                return;
        }
    }
}
