package com.baosteelOnline.xhjy;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.andframework.common.ObjectStores;
import com.andframework.parse.BaseParse;
import com.andframework.ui.CustomListView;
import com.andframework.ui.CustomMessageShow;
import com.baosteelOnline.R;
import com.baosteelOnline.common.DataBaseFactory;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.data.SearchItemInfoBusi;
import com.baosteelOnline.data.Shop_addShoppingBusi;
import com.baosteelOnline.data_parse.SearchParse;
import com.baosteelOnline.dl.Login_indexActivity;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.model.SmallNumUtil;
import com.baosteelOnline.sql.DBHelper;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import java.util.ArrayList;
import java.util.HashMap;

public class SearchItemInfoActivity extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    /* access modifiers changed from: private */
    public ImageView IV_choose;
    /* access modifiers changed from: private */
    public ArrayList<SearchRow> LisItems = new ArrayList<>();
    TextView TV_company;
    TextView TV_desunit;
    TextView TV_format;
    TextView TV_mertial;
    TextView TV_price;
    TextView TV_productaddr;
    TextView TV_productname;
    /* access modifiers changed from: private */
    public String company2;
    private DataBaseFactory db;
    private DBHelper dbHelper;
    /* access modifiers changed from: private */
    public String desunit2;
    /* access modifiers changed from: private */
    public String format2;
    /* access modifiers changed from: private */
    public String gpls;
    private JQUICallBack httpCallBack1 = new JQUICallBack() {
        public void callBack(ResultData result) {
            SearchItemInfoActivity.this.progressDialog.dismiss();
            SearchItemInfoActivity.this.updateNoticeList(SearchParse.parse(result.getStringData()));
        }
    };
    private JQUICallBack httpCallBack2 = new JQUICallBack() {
        public void callBack(ResultData result) {
            SearchItemInfoActivity.this.progressDialog.dismiss();
            SearchItemInfoActivity.this.updateNotice(SearchParse.parse(result.getStringData()));
        }
    };
    private int list_num = 0;
    private Boolean list_stats = false;
    /* access modifiers changed from: private */
    public String mertial2;
    CustomListView myiteminfo_list;
    Drawable opt_empty_bg;
    Drawable opt_selected_bg;
    /* access modifiers changed from: private */
    public String price2;
    /* access modifiers changed from: private */
    public String productaddr2;
    /* access modifiers changed from: private */
    public String productname2;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    private RadioGroup radioderGroup;
    private View view;
    private View view2;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.xhjy_searchiteminfo);
        ExitApplication.getInstance().addActivity(this);
        this.myiteminfo_list = (CustomListView) findViewById(R.id.xhjy_iteminfo_list);
        this.myiteminfo_list.setPageSize(50);
        this.dbHelper = DataBaseFactory.getInstance(this);
        this.TV_productname = (TextView) findViewById(R.id.TV_productname_iteminfo);
        this.TV_company = (TextView) findViewById(R.id.TV_company_iteminfo);
        this.TV_mertial = (TextView) findViewById(R.id.TV_mertial_iteminfo);
        this.TV_format = (TextView) findViewById(R.id.TV_format_iteminfo);
        this.TV_price = (TextView) findViewById(R.id.TV_price_iteminfo);
        this.TV_desunit = (TextView) findViewById(R.id.TV_desunit_iteminfo);
        this.TV_productaddr = (TextView) findViewById(R.id.TV_productaddr_iteminfo);
        this.opt_selected_bg = getResources().getDrawable(R.drawable.opt_selected_bg);
        this.opt_empty_bg = getResources().getDrawable(R.drawable.opt_empty_bg);
        ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
        this.radioderGroup = (RadioGroup) findViewById(R.id.xhjy_index_RGroup);
        this.radioderGroup.setOnCheckedChangeListener(this);
        SmallNumUtil.GWCNum(this, (TextView) findViewById(R.id.gwc_dhqrnum));
        initData();
        this.myiteminfo_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View v, int num1, long arg3) {
                String stats = ((SearchRow) SearchItemInfoActivity.this.LisItems.get(num1 - 1)).stats;
                System.out.println("you choose : " + num1);
                System.out.println("改变之前状态-----:" + ((SearchRow) SearchItemInfoActivity.this.LisItems.get(num1 - 1)).stats);
                SearchItemInfoActivity.this.IV_choose = (ImageView) v.findViewById(R.id.checkbox_iteminfo_list2);
                if ("0".equals(stats)) {
                    ((SearchRow) SearchItemInfoActivity.this.LisItems.get(num1 - 1)).stats = "1";
                    SearchItemInfoActivity.this.IV_choose.setImageDrawable(SearchItemInfoActivity.this.opt_selected_bg);
                } else if ("1".equals(stats)) {
                    v.setTag("0");
                    ((SearchRow) SearchItemInfoActivity.this.LisItems.get(num1 - 1)).stats = "0";
                    SearchItemInfoActivity.this.IV_choose.setImageDrawable(SearchItemInfoActivity.this.opt_empty_bg);
                }
                System.out.println("改变后的状态值----:" + ((SearchRow) SearchItemInfoActivity.this.LisItems.get(num1 - 1)).stats);
            }
        });
        testBusi();
    }

    private void initData() {
        this.productname2 = getIntent().getStringExtra("productname2");
        this.company2 = getIntent().getStringExtra("company2");
        this.mertial2 = getIntent().getStringExtra("mertial2");
        this.format2 = getIntent().getStringExtra("format2");
        this.price2 = getIntent().getStringExtra("price2");
        this.desunit2 = getIntent().getStringExtra("desunit2");
        this.productaddr2 = getIntent().getStringExtra("productaddr2");
        this.TV_productname.setText(this.productname2);
        this.TV_company.setText(this.company2);
        this.TV_mertial.setText(this.mertial2);
        this.TV_format.setText(this.format2);
        this.TV_price.setText(this.price2);
        this.TV_desunit.setText(this.desunit2);
        this.TV_productaddr.setText(this.productaddr2);
        this.gpls = getIntent().getStringExtra("gpls");
    }

    public void testBusi() {
        SearchItemInfoBusi infoBusi = new SearchItemInfoBusi(this);
        infoBusi.gpls = this.gpls;
        infoBusi.setHttpCallBack(this.httpCallBack1);
        infoBusi.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    /* access modifiers changed from: package-private */
    public void updateNotice(SearchParse searchParse) {
        if (SearchParse.commonData == null || SearchParse.commonData.msg == null) {
            CustomMessageShow.getInst().showLongToast(this, "服务器无数据返回！");
            return;
        }
        String msg = SearchParse.commonData.msg;
        System.out.println("msg: " + msg);
        if (msg.indexOf("成功") != -1) {
            new AlertDialog.Builder(this).setMessage("已经成功加入购物车！\n是否继续购物").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    ExitApplication.getInstance().startActivity(SearchItemInfoActivity.this, SearchActivity.class);
                }
            }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    ExitApplication.getInstance().startActivity(SearchItemInfoActivity.this, ShopActivity.class);
                }
            }).show().setCanceledOnTouchOutside(false);
        } else {
            new AlertDialog.Builder(this).setMessage(msg).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int arg1) {
                    dialog.cancel();
                }
            }).show().setCanceledOnTouchOutside(false);
        }
    }

    /* access modifiers changed from: package-private */
    public void updateNoticeList(SearchParse searchParse) {
        if (SearchParse.commonData == null || SearchParse.commonData.blocks == null || SearchParse.commonData.blocks.r0 == null || SearchParse.commonData.blocks.r0.mar == null || SearchParse.commonData.blocks.r0.mar.rows == null) {
            new AlertDialog.Builder(this).setMessage("对不起，网络数据异常!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    ExitApplication.getInstance().back(0);
                }
            }).show().setCanceledOnTouchOutside(false);
        } else if (SearchParse.commonData.blocks.r0.mar.rows.rows.size() == 0) {
            new AlertDialog.Builder(this).setMessage("对不起，没有数据!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    ExitApplication.getInstance().back(0);
                }
            }).show().setCanceledOnTouchOutside(false);
        } else if (SearchParse.commonData != null && SearchParse.commonData.blocks != null && SearchParse.commonData.blocks.r0 != null && SearchParse.commonData.blocks.r0.mar != null && SearchParse.commonData.blocks.r0.mar.rows != null && SearchParse.commonData.blocks.r0.mar.rows.rows != null) {
            int j = SearchParse.commonData.blocks.r0.mar.rows.rows.size();
            System.out.println("ItemInfo---size: " + j);
            for (int i = 0; i < j; i++) {
                ArrayList<String> rowdata = SearchParse.commonData.blocks.r0.mar.rows.rows.get(i);
                SearchRow sr = new SearchRow();
                if (rowdata != null) {
                    for (int k = 0; k < rowdata.size(); k++) {
                        if (k == 0) {
                            sr.packId = (String) rowdata.get(k);
                            sr.number = String.valueOf(i + 1);
                            sr.stats = String.valueOf(0);
                            sr.bz = (String) rowdata.get(k);
                            if (sr.bz.equals("null")) {
                                sr.bz = StringEx.Empty;
                            } else {
                                sr.bz = "备注:" + ((String) rowdata.get(k));
                                if (sr.bz.length() > 46) {
                                    sr.bz = sr.bz.substring(0, 46);
                                }
                            }
                        } else if (k == 8) {
                            sr.ck = "仓库:" + ((String) rowdata.get(k));
                        } else if (k == 9) {
                            sr.khw = "库位号:" + ((String) rowdata.get(k));
                        } else if (k == 2) {
                            sr.weight = "重量:" + ((String) rowdata.get(k)) + "吨";
                        }
                    }
                    this.LisItems.add(sr);
                    this.view2 = buildRowView(sr);
                    this.myiteminfo_list.addViewToLast(this.view2);
                    rowdata.retainAll(rowdata);
                }
            }
            this.myiteminfo_list.onRefreshComplete();
        }
    }

    private View buildRowView(SearchRow sr) {
        this.view = View.inflate(this, R.layout.xhjy_searchiteminfo_row, null);
        TextView TV_khw = (TextView) this.view.findViewById(R.id.khw_iteminfo_list);
        TextView TV_ck = (TextView) this.view.findViewById(R.id.ck_iteminfo_list);
        TextView TV_bei = (TextView) this.view.findViewById(R.id.bei_iteminfo_list);
        this.view.setTag(sr.stats);
        ((TextView) this.view.findViewById(R.id.TV_num_iteminfo_list)).setText(sr.number);
        ((TextView) this.view.findViewById(R.id.weight_iteminfo_list)).setText(sr.weight);
        if (sr.khw.equals("库位号:")) {
            TV_khw.setVisibility(8);
        } else {
            TV_khw.setVisibility(0);
            TV_khw.setText(sr.khw);
        }
        TV_ck.setText(sr.ck);
        TV_bei.setText(sr.bz);
        return this.view;
    }

    class SearchRow {
        public String bz;
        public String ck;
        public String khw;
        public String number;
        public String packId;
        public String productid3;
        public String sellerid3;
        public String stats = "0";
        public String weight;
        public String wproviderId3;

        SearchRow() {
        }
    }

    public void xhjy_iteminfo_back_ButtonAction(View v) {
        ExitApplication.getInstance().back(0);
    }

    public void onBackPressed() {
        ExitApplication.getInstance().back(0);
        super.onBackPressed();
    }

    public void xhjy_iteminfo_finish_ButtonAction(View v) {
        String packids = StringEx.Empty;
        new BaseParse();
        this.dbHelper.insertOperation("钢材交易", "搜索", "捆包明细页面");
        for (int i = 0; i < this.LisItems.size(); i++) {
            if ("1".equals(this.LisItems.get(i).stats)) {
                packids = String.valueOf(packids) + this.LisItems.get(i).packId.toString() + ",";
            }
            getSystemService("connectivity");
        }
        if (ObjectStores.getInst().getObject("reStr") != "1") {
            new AlertDialog.Builder(this).setMessage("您还没有登录,请登录！").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    SharedPreferences.Editor editor = SearchItemInfoActivity.this.getSharedPreferences("index_mark", 2).edit();
                    editor.putString("index", "SearchItemInfo12");
                    editor.commit();
                    HashMap hasmap = new HashMap();
                    hasmap.put("productname2", SearchItemInfoActivity.this.productname2);
                    hasmap.put("company2", SearchItemInfoActivity.this.company2);
                    hasmap.put("mertial2", SearchItemInfoActivity.this.mertial2);
                    hasmap.put("format2", SearchItemInfoActivity.this.format2);
                    hasmap.put("price2", SearchItemInfoActivity.this.price2);
                    hasmap.put("desunit2", SearchItemInfoActivity.this.desunit2);
                    hasmap.put("productaddr2", SearchItemInfoActivity.this.productaddr2);
                    hasmap.put("gpls", SearchItemInfoActivity.this.gpls);
                    ExitApplication.getInstance().startActivity(SearchItemInfoActivity.this, Login_indexActivity.class, hasmap);
                }
            }).show();
        } else if (ObjectStores.getInst().getObject("XH_reStr").toString() != "1") {
            new AlertDialog.Builder(this).setMessage("现货频道未开启，请联系管理员！").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    ExitApplication.getInstance().startActivity(SearchItemInfoActivity.this, Xhjy_indexActivity.class);
                }
            }).show();
        } else if (StringEx.Empty.equals(packids)) {
            new AlertDialog.Builder(this).setMessage("请选择捆包").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                }
            }).show();
        } else {
            String packids2 = packids.substring(0, packids.length() - 1);
            Shop_addShoppingBusi addBusi = new Shop_addShoppingBusi(this);
            addBusi.kbh = packids2;
            addBusi.gpls = this.gpls;
            addBusi.setHttpCallBack(this.httpCallBack2);
            addBusi.iExecute();
            this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.myiteminfo_list = null;
        this.view = null;
        this.view2 = null;
        this.TV_productname = null;
        this.TV_company = null;
        this.TV_mertial = null;
        this.TV_format = null;
        this.TV_price = null;
        this.TV_desunit = null;
        this.TV_productaddr = null;
        this.productname2 = null;
        this.company2 = null;
        this.mertial2 = null;
        this.format2 = null;
        this.price2 = null;
        this.desunit2 = null;
        this.productaddr2 = null;
        this.IV_choose = null;
        this.list_num = 0;
        this.LisItems = null;
        super.onDestroy();
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.radio_search3:
                ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
                return;
            case R.id.radio_shopcar3:
                ((RadioButton) findViewById(R.id.radio_shopcar3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ShopActivity.class);
                return;
            case R.id.radio_home3:
                ((RadioButton) findViewById(R.id.radio_home3)).setChecked(true);
                HashMap hasmap = new HashMap();
                hasmap.put("index_more", "1");
                ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class, hasmap);
                return;
            case R.id.radio_contract3:
                ((RadioButton) findViewById(R.id.radio_contract3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ContractListActivity.class);
                return;
            case R.id.radio_notice3:
                ((RadioButton) findViewById(R.id.radio_notice3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, Xhjy_more.class);
                return;
            default:
                return;
        }
    }
}
