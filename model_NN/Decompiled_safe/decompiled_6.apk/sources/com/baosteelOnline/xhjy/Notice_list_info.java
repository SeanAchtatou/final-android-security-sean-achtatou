package com.baosteelOnline.xhjy;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.baosteelOnline.R;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.main.ExitApplication;
import com.jianq.misc.StringEx;
import java.util.HashMap;

public class Notice_list_info extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    private static int gWebviewZoom = 100;
    private WebView aView = null;
    private RelativeLayout bottom_nva1;
    private RelativeLayout bottom_nva2;
    private String ggid = StringEx.Empty;
    private RadioButton mIndexNavigation1;
    private RadioButton mIndexNavigation2;
    private RadioButton mIndexNavigation3;
    private RadioButton mIndexNavigation4;
    private RadioButton mIndexNavigation5;
    private RadioGroup mRadioderGroup;
    private String part = StringEx.Empty;
    private RadioButton radio_notice;
    private RadioGroup radioderGroup;
    private String sign = StringEx.Empty;
    private SharedPreferences sp;
    private TextView title;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ExitApplication.getInstance().addActivity(this);
        requestWindowFeature(1);
        setContentView((int) R.layout.notice_list_info);
        this.bottom_nva1 = (RelativeLayout) findViewById(R.id.bottom_nva1);
        this.bottom_nva2 = (RelativeLayout) findViewById(R.id.bottom_nva2);
        this.sp = getSharedPreferences("BaoIndex", 2);
        if (this.sp.getString("bIndex", StringEx.Empty).equals("xhwz")) {
            this.bottom_nva1.setVisibility(8);
            this.bottom_nva2.setVisibility(0);
        } else {
            this.bottom_nva2.setVisibility(8);
            this.bottom_nva1.setVisibility(0);
        }
        this.mRadioderGroup = (RadioGroup) findViewById(R.id.cyclic_goods_index_RGroup);
        this.mIndexNavigation1 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item1);
        this.mIndexNavigation2 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item2);
        this.mIndexNavigation3 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item3);
        this.mIndexNavigation4 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item4);
        this.mIndexNavigation5 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item5);
        this.mRadioderGroup.setOnCheckedChangeListener(this);
        this.title = (TextView) findViewById(R.id.title_text);
        this.radio_notice = (RadioButton) findViewById(R.id.radio_home3);
        this.radio_notice.setChecked(true);
        this.radioderGroup = (RadioGroup) findViewById(R.id.xhjy_index_RGroup);
        this.radioderGroup.setOnCheckedChangeListener(this);
        this.aView = (WebView) findViewById(R.id.weblist_noticeInfo);
        this.aView.getSettings().setJavaScriptEnabled(true);
        this.ggid = getIntent().getStringExtra("ggid");
        this.sign = getIntent().getStringExtra("sign");
        if (this.sign.equals("gcjjyg") || this.sign.equals("wzjjyg")) {
            this.title.setText("交易预告");
        }
        if (this.sign.equals("gcjjgg") || this.sign.equals("wzjjgg")) {
            this.title.setText("竞价公告");
        }
        this.aView.clearView();
        this.aView.setBackgroundColor(0);
        if (gWebviewZoom > 0) {
            this.aView.setInitialScale(gWebviewZoom);
        } else {
            this.aView.getSettings().setSupportZoom(false);
        }
        int screenDensity = getResources().getDisplayMetrics().densityDpi;
        WebSettings.ZoomDensity zoomDensity = WebSettings.ZoomDensity.MEDIUM;
        switch (screenDensity) {
            case 120:
                zoomDensity = WebSettings.ZoomDensity.CLOSE;
                break;
            case 160:
                zoomDensity = WebSettings.ZoomDensity.MEDIUM;
                break;
            case 240:
                zoomDensity = WebSettings.ZoomDensity.FAR;
                break;
        }
        this.aView.getSettings().setDefaultZoom(zoomDensity);
        this.aView.requestFocus();
        this.aView.getSettings().setSupportZoom(true);
        this.aView.getSettings().setBuiltInZoomControls(true);
        this.aView.loadUrl("http://testex.bsteel.com/newexchange/b-0/gcbIndex.do?method=doQueryMakeNoticeDetailGcbM&ggid=" + this.ggid);
        this.aView.setWebViewClient(new HelloWebViewClient(this, null));
    }

    private class HelloWebViewClient extends WebViewClient {
        private HelloWebViewClient() {
        }

        /* synthetic */ HelloWebViewClient(Notice_list_info notice_list_info, HelloWebViewClient helloWebViewClient) {
            this();
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    public void back_button(View v) {
        ExitApplication.getInstance().back(this);
    }

    public void onBackPressed() {
        ExitApplication.getInstance().back(0);
        super.onBackPressed();
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.cyclicgoods_index_navigation_item2:
                this.mIndexNavigation2.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item3:
                this.mIndexNavigation3.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidSessionSelect.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item1:
                this.mIndexNavigation1.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item4:
                this.mIndexNavigation4.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidHallPageActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item5:
                this.mIndexNavigation5.setChecked(true);
                Intent intent = new Intent(this, Xhjy_more.class);
                intent.putExtra("part", "循环物资");
                startActivity(intent);
                finish();
                return;
            case R.id.radio_search3:
                ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
                return;
            case R.id.radio_shopcar3:
                ((RadioButton) findViewById(R.id.radio_shopcar3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ShopActivity.class);
                return;
            case R.id.radio_home3:
                ((RadioButton) findViewById(R.id.radio_home3)).setChecked(true);
                HashMap hasmap = new HashMap();
                hasmap.put("index_more", "1");
                ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class, hasmap);
                return;
            case R.id.radio_contract3:
                ((RadioButton) findViewById(R.id.radio_contract3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ContractListActivity.class);
                return;
            case R.id.radio_notice3:
                ((RadioButton) findViewById(R.id.radio_notice3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, Xhjy_more.class);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.aView = null;
        super.onDestroy();
    }
}
