package com.baosteelOnline.xhjy;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.R;
import com.baosteelOnline.common.DataBaseFactory;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.data.RequestBidData;
import com.baosteelOnline.data_parse.ContractParse;
import com.baosteelOnline.dl.Login_indexActivity;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.model.SmallNumUtil;
import com.baosteelOnline.sql.DBHelper;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;

public class CyclicGoodsConfirmArrivalActivity extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    /* access modifiers changed from: private */
    public String confirmResult;
    /* access modifiers changed from: private */
    public String contract1 = StringEx.Empty;
    public ContractParse contractParse;
    /* access modifiers changed from: private */
    public String date = StringEx.Empty;
    private DataBaseFactory db;
    private DBHelper dbHelper;
    private JQUICallBack httpCallBack = new JQUICallBack() {
        public void callBack(ResultData result) {
            CyclicGoodsConfirmArrivalActivity.this.progressDialog.dismiss();
            Log.d("获取的数据：", result.getStringData());
            if (!result.getStringData().equals(null) && !result.getStringData().equals(StringEx.Empty)) {
                CyclicGoodsConfirmArrivalActivity.this.contractParse = ContractParse.parse(result.getStringData());
                CyclicGoodsConfirmArrivalActivity.this.numOfPageGet = Integer.parseInt(ContractParse.pageNumTal);
                Log.d("总页数：", String.valueOf(CyclicGoodsConfirmArrivalActivity.this.numOfPageGet) + "页");
                Log.d("获取的ROWs数据：", String.valueOf(ContractParse.getDataString()) + "前面没数据就是空");
                if (ContractParse.getDataString().equals(null) || ContractParse.getDataString().equals(StringEx.Empty)) {
                    new AlertDialog.Builder(CyclicGoodsConfirmArrivalActivity.this).setMessage("获取数据失败").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
                } else {
                    Log.d("获取的ROWs数据：", ContractParse.jjo.toString());
                    if (ContractParse.jjo.toString().equals(null) || ContractParse.jjo.toString().equals("[[]]") || ContractParse.jjo.toString().equals(StringEx.Empty) || ContractParse.jjo.toString().equals("[]")) {
                        new AlertDialog.Builder(CyclicGoodsConfirmArrivalActivity.this).setMessage("无未做到货确认的合同").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
                    } else {
                        for (int i = 0; i < ContractParse.jjo.length(); i++) {
                            try {
                                JSONArray row = new JSONArray(ContractParse.jjo.getString(i));
                                HashMap<String, Object> item = new HashMap<>();
                                CyclicGoodsConfirmArrivalActivity.this.contract1 = row.getString(0);
                                CyclicGoodsConfirmArrivalActivity.this.seller = row.getString(1);
                                CyclicGoodsConfirmArrivalActivity.this.date = row.getString(6);
                                CyclicGoodsConfirmArrivalActivity.this.number = row.getString(2);
                                CyclicGoodsConfirmArrivalActivity.this.weight = row.getString(4);
                                CyclicGoodsConfirmArrivalActivity.this.sum = row.getString(5);
                                CyclicGoodsConfirmArrivalActivity.this.state = row.getString(7);
                                CyclicGoodsConfirmArrivalActivity.this.unit = row.getString(3);
                                CyclicGoodsConfirmArrivalActivity.this.stateName = row.getString(8);
                                if (CyclicGoodsConfirmArrivalActivity.this.markImage) {
                                    item.put("background_image", Integer.valueOf((int) R.drawable.list_item_background1));
                                    CyclicGoodsConfirmArrivalActivity.this.markImage = false;
                                } else {
                                    CyclicGoodsConfirmArrivalActivity.this.markImage = true;
                                    item.put("background_image", Integer.valueOf((int) R.drawable.list_item_background1));
                                }
                                item.put("contract", String.valueOf(CyclicGoodsConfirmArrivalActivity.this.numOfItem + 1) + ".合同号：");
                                item.put("weight", CyclicGoodsConfirmArrivalActivity.this.weight);
                                item.put("contract1", CyclicGoodsConfirmArrivalActivity.this.contract1);
                                item.put("seller", CyclicGoodsConfirmArrivalActivity.this.seller);
                                item.put("date", CyclicGoodsConfirmArrivalActivity.this.date);
                                item.put("number", CyclicGoodsConfirmArrivalActivity.this.number);
                                item.put("sum", CyclicGoodsConfirmArrivalActivity.this.sum);
                                item.put("state", CyclicGoodsConfirmArrivalActivity.this.state);
                                if (CyclicGoodsConfirmArrivalActivity.this.unit.equals(null) || CyclicGoodsConfirmArrivalActivity.this.unit.equals(StringEx.Empty) || CyclicGoodsConfirmArrivalActivity.this.unit.equals("null")) {
                                    item.put("unit", "件");
                                    item.put("number1", String.valueOf(CyclicGoodsConfirmArrivalActivity.this.number) + "件/" + CyclicGoodsConfirmArrivalActivity.this.weight + "吨");
                                } else {
                                    item.put("number1", String.valueOf(CyclicGoodsConfirmArrivalActivity.this.number) + CyclicGoodsConfirmArrivalActivity.this.unit + "/" + CyclicGoodsConfirmArrivalActivity.this.weight + "吨");
                                    item.put("unit", CyclicGoodsConfirmArrivalActivity.this.unit);
                                }
                                item.put("stateName", String.valueOf(CyclicGoodsConfirmArrivalActivity.this.stateName) + "效");
                                item.put("background", Integer.valueOf((int) R.drawable.more_back_1));
                                item.put("lastten", StringEx.Empty);
                                CyclicGoodsConfirmArrivalActivity cyclicGoodsConfirmArrivalActivity = CyclicGoodsConfirmArrivalActivity.this;
                                cyclicGoodsConfirmArrivalActivity.numOfItem = cyclicGoodsConfirmArrivalActivity.numOfItem + 1;
                                CyclicGoodsConfirmArrivalActivity.this.mItemArrayList.add(item);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        HashMap<String, Object> item2 = new HashMap<>();
                        item2.put("contract", StringEx.Empty);
                        item2.put("unit", StringEx.Empty);
                        item2.put("weight", StringEx.Empty);
                        item2.put("contract1", StringEx.Empty);
                        item2.put("seller", StringEx.Empty);
                        item2.put("date", StringEx.Empty);
                        item2.put("number1", StringEx.Empty);
                        item2.put("number", StringEx.Empty);
                        item2.put("sum", StringEx.Empty);
                        item2.put("state", StringEx.Empty);
                        item2.put("stateName", StringEx.Empty);
                        item2.put("background_image", Integer.valueOf((int) R.drawable.more_back_white));
                        if (CyclicGoodsConfirmArrivalActivity.this.numOfPageGet > CyclicGoodsConfirmArrivalActivity.this.numOfPage) {
                            item2.put("lastten", "下10条...");
                        } else {
                            CyclicGoodsConfirmArrivalActivity.this.isLastPage = true;
                            item2.put("lastten", "已经是最后一页");
                        }
                        item2.put("background", Integer.valueOf((int) R.drawable.bg_next));
                        CyclicGoodsConfirmArrivalActivity.this.mItemArrayList.add(item2);
                    }
                }
            }
            CyclicGoodsConfirmArrivalActivity.this.myAdapter.notifyDataSetChanged();
        }
    };
    private JQUICallBack httpCallBackGz = new JQUICallBack() {
        public void callBack(ResultData result) {
            CyclicGoodsConfirmArrivalActivity.this.progressDialog.dismiss();
            Log.d("获取的数据：", result.getStringData());
            String dataString = result.getStringData();
            if (!dataString.equals(null) && !dataString.equals(StringEx.Empty)) {
                CyclicGoodsConfirmArrivalActivity.this.contractParse = ContractParse.parse(result.getStringData());
                if (ContractParse.getDataString().equals(null) || ContractParse.getDataString().equals(StringEx.Empty)) {
                    new AlertDialog.Builder(CyclicGoodsConfirmArrivalActivity.this).setMessage("获取数据失败").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
                    return;
                }
                Log.d("获取的ROWs数据：", ContractParse.jjo.toString());
                int i = 0;
                while (i < ContractParse.jjo.length()) {
                    try {
                        CyclicGoodsConfirmArrivalActivity.this.confirmResult = new JSONArray(ContractParse.jjo.getString(i)).getString(1);
                        if (CyclicGoodsConfirmArrivalActivity.this.confirmResult.equals(null) || CyclicGoodsConfirmArrivalActivity.this.confirmResult.equals(StringEx.Empty) || !CyclicGoodsConfirmArrivalActivity.this.confirmResult.equals("0")) {
                            if (!CyclicGoodsConfirmArrivalActivity.this.confirmResult.equals(null) && !CyclicGoodsConfirmArrivalActivity.this.confirmResult.equals(StringEx.Empty) && CyclicGoodsConfirmArrivalActivity.this.confirmResult.equals("1")) {
                                new AlertDialog.Builder(CyclicGoodsConfirmArrivalActivity.this).setMessage("确认到货成功!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        CyclicGoodsConfirmArrivalActivity.this.mItemArrayList.clear();
                                        CyclicGoodsConfirmArrivalActivity.this.getListData();
                                        dialog.cancel();
                                    }
                                }).show();
                            }
                            i++;
                        } else {
                            new AlertDialog.Builder(CyclicGoodsConfirmArrivalActivity.this).setMessage("确认到货失败!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            }).show();
                            i++;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    };
    private LayoutInflater inflater;
    /* access modifiers changed from: private */
    public boolean isLastPage = false;
    private int itemHeight;
    private View itemView;
    private RadioButton mIndexNavigation1;
    private RadioButton mIndexNavigation2;
    private RadioButton mIndexNavigation3;
    private RadioButton mIndexNavigation4;
    private RadioButton mIndexNavigation5;
    /* access modifiers changed from: private */
    public ArrayList<HashMap<String, Object>> mItemArrayList;
    private ListView mListView;
    private RadioGroup mRadioderGroup;
    private SimpleAdapter mSimpleAdater;
    /* access modifiers changed from: private */
    public boolean markImage = true;
    /* access modifiers changed from: private */
    public MyAdapter myAdapter;
    /* access modifiers changed from: private */
    public int numOfItem = 0;
    /* access modifiers changed from: private */
    public int numOfPage = 1;
    /* access modifiers changed from: private */
    public int numOfPageGet = 1;
    /* access modifiers changed from: private */
    public String number = StringEx.Empty;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    /* access modifiers changed from: private */
    public String seller = StringEx.Empty;
    private SharedPreferences sp;
    /* access modifiers changed from: private */
    public String state = StringEx.Empty;
    /* access modifiers changed from: private */
    public String stateName = StringEx.Empty;
    /* access modifiers changed from: private */
    public String sum = StringEx.Empty;
    /* access modifiers changed from: private */
    public String unit = StringEx.Empty;
    /* access modifiers changed from: private */
    public String weight = StringEx.Empty;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.activity_cyclic_goods_confirm_arrival_activity);
        ExitApplication.getInstance().addActivity(this);
        this.dbHelper = DataBaseFactory.getInstance(this);
        this.dbHelper.insertOperation("循环物资", "循环资源首页", "到货确认列表");
        this.mListView = (ListView) findViewById(R.id.cyclicgoods_confirm_arrival_listview);
        this.mItemArrayList = new ArrayList<>();
        this.sp = getSharedPreferences("BaoIndex", 2);
        ((ImageButton) findViewById(R.id.cyclicgoods_list_title_imbt)).setBackgroundResource(R.drawable.button_home_selector);
        this.inflater = getLayoutInflater();
        this.itemView = this.inflater.inflate((int) R.layout.activity_cyclic_goods_confirm_arrival_activity_item, (ViewGroup) null);
        this.itemView.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        this.itemView.measure(0, 0);
        this.itemHeight = this.itemView.getMeasuredHeight();
        Log.d("Item高度:", "高度为：" + this.itemHeight);
        if (ObjectStores.getInst().getObject("reStr") != "1") {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setCancelable(false);
            dialog.setMessage("您还没有登录，请登录！").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    SharedPreferences.Editor editor = CyclicGoodsConfirmArrivalActivity.this.getSharedPreferences("index_mark", 2).edit();
                    editor.putString("index", "到货确认列表");
                    editor.commit();
                    CyclicGoodsConfirmArrivalActivity.this.startActivity(new Intent(CyclicGoodsConfirmArrivalActivity.this, Login_indexActivity.class));
                    CyclicGoodsConfirmArrivalActivity.this.finish();
                }
            }).show();
            return;
        }
        this.myAdapter = new MyAdapter(this);
        this.mRadioderGroup = (RadioGroup) findViewById(R.id.cyclic_goods_index_RGroup);
        this.mIndexNavigation1 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item1);
        this.mIndexNavigation2 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item2);
        this.mIndexNavigation3 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item3);
        this.mIndexNavigation4 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item4);
        this.mIndexNavigation5 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item5);
        this.mIndexNavigation1.setChecked(true);
        SmallNumUtil.JJNum(this, (TextView) findViewById(R.id.jj_dhqrnum));
        this.mIndexNavigation1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ExitApplication.getInstance().startActivity(CyclicGoodsConfirmArrivalActivity.this, CyclicGoodsIndexActivity.class);
                CyclicGoodsConfirmArrivalActivity.this.finish();
            }
        });
        this.mRadioderGroup.setOnCheckedChangeListener(this);
        this.mListView.setAdapter((ListAdapter) this.myAdapter);
        this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                if (arg2 + 1 != CyclicGoodsConfirmArrivalActivity.this.mItemArrayList.size()) {
                    Intent intent = new Intent(CyclicGoodsConfirmArrivalActivity.this, CyclicGoodsContractDetails.class);
                    String contract1 = ((HashMap) CyclicGoodsConfirmArrivalActivity.this.mItemArrayList.get(arg2)).get("contract1").toString();
                    String seller = ((HashMap) CyclicGoodsConfirmArrivalActivity.this.mItemArrayList.get(arg2)).get("seller").toString();
                    String number = ((HashMap) CyclicGoodsConfirmArrivalActivity.this.mItemArrayList.get(arg2)).get("number").toString();
                    String weight = ((HashMap) CyclicGoodsConfirmArrivalActivity.this.mItemArrayList.get(arg2)).get("weight").toString();
                    String sum = ((HashMap) CyclicGoodsConfirmArrivalActivity.this.mItemArrayList.get(arg2)).get("sum").toString();
                    String state = ((HashMap) CyclicGoodsConfirmArrivalActivity.this.mItemArrayList.get(arg2)).get("state").toString();
                    ObjectStores.getInst().putObject("positionName", contract1);
                    intent.putExtra("contract1", contract1);
                    intent.putExtra("seller", seller);
                    intent.putExtra("number", number);
                    intent.putExtra("weight", weight);
                    intent.putExtra("sum", sum);
                    intent.putExtra("state", state);
                    CyclicGoodsConfirmArrivalActivity.this.startActivity(intent);
                } else if (!CyclicGoodsConfirmArrivalActivity.this.isLastPage) {
                    CyclicGoodsConfirmArrivalActivity.this.mItemArrayList.remove(arg2);
                    CyclicGoodsConfirmArrivalActivity cyclicGoodsConfirmArrivalActivity = CyclicGoodsConfirmArrivalActivity.this;
                    cyclicGoodsConfirmArrivalActivity.numOfPage = cyclicGoodsConfirmArrivalActivity.numOfPage + 1;
                    CyclicGoodsConfirmArrivalActivity.this.getListData();
                }
            }
        });
        getListData();
    }

    /* access modifiers changed from: private */
    public void getListData() {
        RequestBidData requestBidData = new RequestBidData(this);
        requestBidData.setRequestData("到货确认列表");
        requestBidData.setPageNum(new StringBuilder(String.valueOf(this.numOfPage)).toString());
        Log.d("请求的指令：", requestBidData.infoDate());
        requestBidData.setHttpCallBack(this.httpCallBack);
        requestBidData.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.cyclicgoods_index_navigation_item2:
                this.mIndexNavigation2.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item3:
                this.mIndexNavigation3.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidSessionSelect.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item1:
                if (ObjectStores.getInst().getObject("formX") == "formX") {
                    ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class);
                } else {
                    this.mIndexNavigation1.setChecked(true);
                    ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
                }
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item4:
                this.mIndexNavigation4.setChecked(true);
                startActivity(new Intent(this, CyclicGoodsBidHallPageActivity.class));
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item5:
                this.mIndexNavigation5.setChecked(true);
                Intent intent = new Intent(this, Xhjy_more.class);
                intent.putExtra("part", "循环物资");
                startActivity(intent);
                finish();
                return;
            default:
                return;
        }
    }

    public void backAaction(View v) {
        if (ObjectStores.getInst().getObject("formX") != "formX") {
            ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
        } else {
            ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class);
        }
        finish();
    }

    public void onBackPressed() {
        if (ObjectStores.getInst().getObject("formX") != "formX") {
            ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
        } else {
            ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class);
        }
        finish();
    }

    /* access modifiers changed from: private */
    public void getGzData(String id2) {
        RequestBidData requestBidData = new RequestBidData(this);
        requestBidData.setRequestData("到货确认按钮");
        requestBidData.setId(id2);
        Log.d("请求的指令：", requestBidData.infoDate());
        requestBidData.setHttpCallBack(this.httpCallBackGz);
        requestBidData.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    public class ViewHolder {
        public ImageView background_image;
        public ImageButton confirmArriva;
        public TextView contract;
        public TextView contract1;
        public TextView date;
        public TextView hetongzhuangtai;
        public TextView lastTen;
        public TextView money;
        public ImageView moreBackground;
        public TextView numTextView;
        public TextView sellerTextView;
        public TextView state;
        public TextView youxiaoriqi;

        public ViewHolder() {
        }
    }

    public class MyAdapter extends BaseAdapter {
        private LayoutInflater mMyAdapterInflater;

        public MyAdapter(Context context) {
            this.mMyAdapterInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return CyclicGoodsConfirmArrivalActivity.this.mItemArrayList.size();
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            DecimalFormat df1;
            int pos = position;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = this.mMyAdapterInflater.inflate((int) R.layout.activity_cyclic_goods_confirm_arrival_activity_item, (ViewGroup) null);
                holder.contract = (TextView) convertView.findViewById(R.id.confirm_arrival_list_item_contract_textview);
                holder.contract1 = (TextView) convertView.findViewById(R.id.confirm_arrival_list_item_contract_textview1);
                holder.sellerTextView = (TextView) convertView.findViewById(R.id.confirm_arrival_list_item_seller_textview1);
                holder.numTextView = (TextView) convertView.findViewById(R.id.confirm_arrival_list_item_number_textview1);
                holder.money = (TextView) convertView.findViewById(R.id.confirm_arrival_list_item_sum_textview1);
                holder.date = (TextView) convertView.findViewById(R.id.confirm_arrival_list_item_date_textview1);
                holder.state = (TextView) convertView.findViewById(R.id.confirm_arrival_list_item_state_textview1);
                holder.moreBackground = (ImageView) convertView.findViewById(R.id.bid_confirm_list_item_background_more);
                holder.background_image = (ImageView) convertView.findViewById(R.id.list_item_background_image);
                holder.confirmArriva = (ImageButton) convertView.findViewById(R.id.list_item_confirm_arrival_image);
                holder.youxiaoriqi = (TextView) convertView.findViewById(R.id.confirm_arrival_list_item_date_textview);
                holder.hetongzhuangtai = (TextView) convertView.findViewById(R.id.confirm_arrival_list_item_state_textview);
                holder.lastTen = (TextView) convertView.findViewById(R.id.bid_confirm__list_next_ten_item_or_end);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            final String contract1 = (String) ((HashMap) CyclicGoodsConfirmArrivalActivity.this.mItemArrayList.get(position)).get("contract1");
            String seller = (String) ((HashMap) CyclicGoodsConfirmArrivalActivity.this.mItemArrayList.get(position)).get("seller");
            String date = (String) ((HashMap) CyclicGoodsConfirmArrivalActivity.this.mItemArrayList.get(position)).get("date");
            String number = (String) ((HashMap) CyclicGoodsConfirmArrivalActivity.this.mItemArrayList.get(position)).get("number");
            String weight = (String) ((HashMap) CyclicGoodsConfirmArrivalActivity.this.mItemArrayList.get(position)).get("weight");
            String sum = (String) ((HashMap) CyclicGoodsConfirmArrivalActivity.this.mItemArrayList.get(position)).get("sum");
            String unit = (String) ((HashMap) CyclicGoodsConfirmArrivalActivity.this.mItemArrayList.get(position)).get("unit");
            String stateName = (String) ((HashMap) CyclicGoodsConfirmArrivalActivity.this.mItemArrayList.get(position)).get("stateName");
            int background = ((Integer) ((HashMap) CyclicGoodsConfirmArrivalActivity.this.mItemArrayList.get(position)).get("background")).intValue();
            int background_image = ((Integer) ((HashMap) CyclicGoodsConfirmArrivalActivity.this.mItemArrayList.get(position)).get("background_image")).intValue();
            final String lastten = (String) ((HashMap) CyclicGoodsConfirmArrivalActivity.this.mItemArrayList.get(position)).get("lastten");
            Log.d("获取的已解析数据", String.valueOf(contract1) + " " + seller + " " + date + " " + number + " " + weight + " " + sum + " " + ((String) ((HashMap) CyclicGoodsConfirmArrivalActivity.this.mItemArrayList.get(position)).get("state")) + " " + unit + " " + stateName);
            if (!lastten.equals(StringEx.Empty)) {
                holder.contract.setVisibility(8);
                holder.contract1.setVisibility(8);
                holder.sellerTextView.setVisibility(8);
                holder.numTextView.setVisibility(8);
                holder.money.setVisibility(8);
                holder.date.setVisibility(8);
                holder.state.setVisibility(8);
                holder.youxiaoriqi.setVisibility(8);
                holder.hetongzhuangtai.setVisibility(8);
                holder.background_image.setVisibility(8);
                holder.confirmArriva.setVisibility(8);
            } else {
                holder.contract.setVisibility(0);
                holder.contract1.setVisibility(0);
                holder.sellerTextView.setVisibility(0);
                holder.numTextView.setVisibility(0);
                holder.money.setVisibility(0);
                holder.date.setVisibility(0);
                holder.state.setVisibility(0);
                holder.youxiaoriqi.setVisibility(0);
                holder.hetongzhuangtai.setVisibility(0);
                holder.background_image.setVisibility(0);
                holder.confirmArriva.setVisibility(0);
            }
            holder.background_image.setBackgroundResource(background_image);
            holder.lastTen.setText(lastten);
            holder.contract.setText(String.valueOf(pos + 1) + ".合同号：");
            holder.contract1.setText(contract1);
            holder.sellerTextView.setText(seller);
            if (unit.equals(null) || unit.equals(StringEx.Empty) || unit.equals("null")) {
                holder.numTextView.setText(String.valueOf(number) + "件/" + weight + "吨");
            } else {
                holder.numTextView.setText(String.valueOf(number) + unit + "/" + weight + "吨");
            }
            if (sum.equals(StringEx.Empty) || sum.equals("null") || sum.equals(null)) {
                holder.money.setText(StringEx.Empty);
            } else {
                Log.d("sum", String.valueOf(sum) + "是否有小数点" + sum.contains("."));
                double value = Double.parseDouble(sum);
                if (sum.contains(".")) {
                    df1 = new DecimalFormat(",###.0");
                } else {
                    df1 = new DecimalFormat(",###");
                }
                String dfyjString = df1.format(value);
                if (sum.equals("0.0") || sum.equals("0.00")) {
                    holder.money.setText(sum);
                } else {
                    holder.money.setText(dfyjString);
                }
            }
            holder.date.setText(date);
            holder.state.setText(stateName);
            holder.moreBackground.setBackgroundResource(background);
            if (position + 1 != CyclicGoodsConfirmArrivalActivity.this.mItemArrayList.size()) {
                holder.confirmArriva.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        if (lastten.equals(StringEx.Empty)) {
                            AlertDialog.Builder message = new AlertDialog.Builder(CyclicGoodsConfirmArrivalActivity.this).setMessage("是否到货确认？");
                            final String str = contract1;
                            message.setNegativeButton("确定", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    CyclicGoodsConfirmArrivalActivity.this.getGzData(str);
                                }
                            }).setPositiveButton("取消", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            }).show();
                        }
                    }
                });
            }
            return convertView;
        }
    }
}
