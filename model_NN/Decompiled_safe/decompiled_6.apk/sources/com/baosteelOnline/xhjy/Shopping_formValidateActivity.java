package com.baosteelOnline.xhjy;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.andframework.ui.CustomListView;
import com.andframework.ui.CustomMessageShow;
import com.baosteelOnline.R;
import com.baosteelOnline.common.DataBaseFactory;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.data.Shop_toContractRealBusi;
import com.baosteelOnline.data.Shopping_formValidateBusi;
import com.baosteelOnline.data.Shopping_jsfsBusi;
import com.baosteelOnline.data_parse.ContractParse;
import com.baosteelOnline.data_parse.SearchParse;
import com.baosteelOnline.data_parse.Shop_toContractRealParse;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.sql.DBHelper;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Shopping_formValidateActivity extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    /* access modifiers changed from: private */
    public ImageView IV_state1;
    private ArrayList<SearchRow> LisItems = new ArrayList<>();
    private TextView TV_negotiate_book;
    /* access modifiers changed from: private */
    public ArrayAdapter<String> adapter2_jsfs;
    /* access modifiers changed from: private */
    public ArrayAdapter<String> adapter_fkfs;
    /* access modifiers changed from: private */
    public String amount = StringEx.Empty;
    private ImageButton btn_rigth;
    private DataBaseFactory db;
    private DBHelper dbHelper;
    /* access modifiers changed from: private */
    public String fkfs;
    private Spinner fkfs_Spinner;
    private String flag_indent = StringEx.Empty;
    private String gpls_indent = StringEx.Empty;
    private JQUICallBack httpCallBack1 = new JQUICallBack() {
        public void callBack(ResultData result) {
            Shopping_formValidateActivity.this.progressDialog.dismiss();
            System.out.println("result=StringData---->" + result.getStringData());
            Shopping_formValidateActivity.this.updateNoticeList(SearchParse.parse(result.getStringData()));
        }
    };
    private JQUICallBack httpCallBack2 = new JQUICallBack() {
        public void callBack(ResultData result) {
            Shopping_formValidateActivity.this.progressDialog.dismiss();
            System.out.println("result=StringData---->" + result.getStringData());
            Shopping_formValidateActivity.this.updatejsfs(ContractParse.parse(result.getStringData()));
        }
    };
    private JQUICallBack httpCallBack3 = new JQUICallBack() {
        public void callBack(ResultData result) {
            Shopping_formValidateActivity.this.progressDialog.dismiss();
            System.out.println("result=StringData---->" + result.getStringData());
            Shopping_formValidateActivity.this.update_msg(Shop_toContractRealParse.parse(result.getStringData()));
        }
    };
    private CustomListView indents_list;
    /* access modifiers changed from: private */
    public String jsfs = "5";
    /* access modifiers changed from: private */
    public Spinner jsfs_Spinner;
    private List<String> list = new ArrayList();
    /* access modifiers changed from: private */
    public List<String> list2 = new ArrayList();
    private String msg = StringEx.Empty;
    private TextView piece_indent;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    private RadioGroup radioderGroup;
    /* access modifiers changed from: private */
    public String rzgx = StringEx.Empty;
    private TextView seller_indent;
    /* access modifiers changed from: private */
    public Boolean state = true;
    private String type = StringEx.Empty;
    private View view;
    private View view2;
    private String who;
    private String wpName;
    private String wzly;
    private String yjdh_indent = StringEx.Empty;
    private String ziyuan = StringEx.Empty;
    private TextView zongdun_indent;
    private TextView zongjine_indent;
    private String zylx;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ExitApplication.getInstance().addActivity(this);
        requestWindowFeature(1);
        setContentView((int) R.layout.xhjy_shopping_formvalidate);
        this.dbHelper = DataBaseFactory.getInstance(this);
        this.dbHelper.insertOperation("钢材交易", "购物车", "订单确认页面");
        this.indents_list = (CustomListView) findViewById(R.id.indents_list);
        this.indents_list.setPageSize(100);
        this.seller_indent = (TextView) findViewById(R.id.seller_indent);
        this.piece_indent = (TextView) findViewById(R.id.piece_indent);
        this.zongdun_indent = (TextView) findViewById(R.id.zongdun_indent);
        this.zongjine_indent = (TextView) findViewById(R.id.zongjine_indent);
        this.TV_negotiate_book = (TextView) findViewById(R.id.TV_negotiate_book);
        this.fkfs_Spinner = (Spinner) findViewById(R.id.xhjy_indent_fkfs);
        this.jsfs_Spinner = (Spinner) findViewById(R.id.xhjy_indent_jsfs);
        this.IV_state1 = (ImageView) findViewById(R.id.IV_indent_choose);
        this.btn_rigth = (ImageButton) findViewById(R.id.btn_fiter_indent);
        this.list.add("请选择");
        this.list2.add("请选择");
        ((RadioButton) findViewById(R.id.radio_shopcar3)).setChecked(true);
        this.radioderGroup = (RadioGroup) findViewById(R.id.xhjy_index_RGroup);
        this.radioderGroup.setOnCheckedChangeListener(this);
        this.indents_list.setClickable(true);
        this.IV_state1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (Shopping_formValidateActivity.this.state.booleanValue()) {
                    Shopping_formValidateActivity.this.state = false;
                    Shopping_formValidateActivity.this.IV_state1.setBackgroundResource(R.drawable.opt_empty_bg);
                } else if (!Shopping_formValidateActivity.this.state.booleanValue()) {
                    Shopping_formValidateActivity.this.state = true;
                    Shopping_formValidateActivity.this.IV_state1.setBackgroundResource(R.drawable.opt_selected_bg);
                }
            }
        });
        this.TV_negotiate_book.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                System.out.println("2345");
                HashMap hash = new HashMap();
                hash.put("rzgx", Shopping_formValidateActivity.this.rzgx);
                hash.put("jsfs", Shopping_formValidateActivity.this.jsfs);
                hash.put("zje", Shopping_formValidateActivity.this.amount);
                ExitApplication.getInstance().startActivity(Shopping_formValidateActivity.this, Negotiate_BookActivity.class, hash);
            }
        });
        Bundle bundle = getIntent().getExtras();
        try {
            this.seller_indent.setText(bundle.getString("sellerName"));
            this.piece_indent.setText(bundle.getString("piece"));
            this.zongdun_indent.setText(bundle.getString("weight"));
            this.zongjine_indent.setText(bundle.getString("amount"));
            this.amount = bundle.getString("amount");
            this.gpls_indent = bundle.getString("shop_gpls");
            this.flag_indent = bundle.getString("shop_flag");
            this.type = bundle.getString("type");
            this.yjdh_indent = bundle.getString("shop_yjdh");
        } catch (Exception e) {
        }
        testBusi();
    }

    public void init_spinner() {
        System.out.println("zylx===" + this.zylx);
        System.out.println("wzly===" + this.wzly);
        if (this.zylx.equals("0")) {
            this.ziyuan = "0";
        } else if (this.zylx.equals("3")) {
            this.ziyuan = "1";
        }
        if (this.wzly.equals("3")) {
            this.ziyuan = "2";
        }
        System.out.println("ziyuan------------>" + this.ziyuan);
        this.adapter_fkfs = new ArrayAdapter<>(this, 17367048, this.list);
        this.adapter_fkfs.setDropDownViewResource(17367049);
        this.fkfs_Spinner.setAdapter((SpinnerAdapter) this.adapter_fkfs);
        if (this.list.size() > 1) {
            this.fkfs_Spinner.setSelection(1, true);
        }
        if (this.ziyuan == "0") {
            this.fkfs_Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                    Shopping_formValidateActivity.this.fkfs = (String) Shopping_formValidateActivity.this.adapter_fkfs.getItem(arg2);
                    if ("全额".equals(Shopping_formValidateActivity.this.fkfs)) {
                        Shopping_formValidateActivity.this.fkfs = "0";
                        Shopping_formValidateActivity.this.list2.clear();
                        Shopping_formValidateActivity.this.list2.add("请选择");
                        Shopping_formValidateActivity.this.list2.add("场外结算");
                        Shopping_formValidateActivity.this.list2.add("场外结算");
                        Shopping_formValidateActivity.this.jsfs_Spinner.setSelection(1, true);
                    } else if ("定金".equals(Shopping_formValidateActivity.this.fkfs)) {
                        Shopping_formValidateActivity.this.fkfs = "2";
                        Shopping_formValidateActivity.this.list2.clear();
                        Shopping_formValidateActivity.this.list2.add("请选择");
                        Shopping_formValidateActivity.this.list2.add("自由付款方式");
                        Shopping_formValidateActivity.this.jsfs_Spinner.setSelection(1, true);
                    } else if ("融资".equals(Shopping_formValidateActivity.this.fkfs)) {
                        Shopping_formValidateActivity.this.fkfs = "3";
                        Shopping_formValidateActivity.this.list2.clear();
                        Shopping_formValidateActivity.this.list2.add("请选择");
                        Shopping_formValidateActivity.this.list2.add("自由付款方式");
                        Shopping_formValidateActivity.this.jsfs_Spinner.setSelection(1, true);
                    } else if ("请选择".equals(Shopping_formValidateActivity.this.fkfs)) {
                        Shopping_formValidateActivity.this.fkfs = StringEx.Empty;
                        Shopping_formValidateActivity.this.list2.clear();
                        Shopping_formValidateActivity.this.list2.add("请选择");
                        Shopping_formValidateActivity.this.jsfs_Spinner.setSelection(0, true);
                    }
                    arg0.setVisibility(0);
                }

                public void onNothingSelected(AdapterView<?> arg0) {
                    arg0.setVisibility(0);
                }
            });
        } else if (this.ziyuan == "1") {
            this.fkfs_Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                    Shopping_formValidateActivity.this.fkfs = (String) Shopping_formValidateActivity.this.adapter_fkfs.getItem(arg2);
                    if ("全额".equals(Shopping_formValidateActivity.this.fkfs)) {
                        Shopping_formValidateActivity.this.fkfs = "0";
                        Shopping_formValidateActivity.this.list2.clear();
                        Shopping_formValidateActivity.this.list2.add("请选择");
                        Shopping_formValidateActivity.this.list2.add("场内结算");
                        Shopping_formValidateActivity.this.jsfs_Spinner.setSelection(1, true);
                    } else if ("定金".equals(Shopping_formValidateActivity.this.fkfs)) {
                        Shopping_formValidateActivity.this.fkfs = "2";
                        Shopping_formValidateActivity.this.list2.clear();
                        Shopping_formValidateActivity.this.list2.add("请选择");
                        Shopping_formValidateActivity.this.list2.add("场内结算");
                        Shopping_formValidateActivity.this.jsfs_Spinner.setSelection(1, true);
                    } else if ("融资".equals(Shopping_formValidateActivity.this.fkfs)) {
                        Shopping_formValidateActivity.this.fkfs = "3";
                        Shopping_formValidateActivity.this.list2.clear();
                        Shopping_formValidateActivity.this.list2.add("请选择");
                        Shopping_formValidateActivity.this.list2.add("自由付款方式");
                        Shopping_formValidateActivity.this.jsfs_Spinner.setSelection(1, true);
                    } else if ("请选择".equals(Shopping_formValidateActivity.this.fkfs)) {
                        Shopping_formValidateActivity.this.fkfs = StringEx.Empty;
                        Shopping_formValidateActivity.this.list2.clear();
                        Shopping_formValidateActivity.this.list2.add("请选择");
                        Shopping_formValidateActivity.this.jsfs_Spinner.setSelection(0, true);
                    }
                    arg0.setVisibility(0);
                }

                public void onNothingSelected(AdapterView<?> arg0) {
                    arg0.setVisibility(0);
                }
            });
        }
        if (this.ziyuan == "2") {
            this.fkfs_Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                    Shopping_formValidateActivity.this.fkfs = (String) Shopping_formValidateActivity.this.adapter_fkfs.getItem(arg2);
                    if ("全额".equals(Shopping_formValidateActivity.this.fkfs)) {
                        Shopping_formValidateActivity.this.fkfs = "0";
                        Shopping_formValidateActivity.this.list2.clear();
                        Shopping_formValidateActivity.this.list2.add("请选择");
                        Shopping_formValidateActivity.this.list2.add("场外结算");
                        Shopping_formValidateActivity.this.jsfs_Spinner.setSelection(1, true);
                    } else if ("定金".equals(Shopping_formValidateActivity.this.fkfs)) {
                        Shopping_formValidateActivity.this.fkfs = "2";
                        Shopping_formValidateActivity.this.list2.clear();
                        Shopping_formValidateActivity.this.list2.add("请选择");
                        Shopping_formValidateActivity.this.list2.add("场外结算");
                        Shopping_formValidateActivity.this.jsfs_Spinner.setSelection(1, true);
                    } else if ("融资".equals(Shopping_formValidateActivity.this.fkfs)) {
                        Shopping_formValidateActivity.this.fkfs = "3";
                        Shopping_formValidateActivity.this.list2.clear();
                        Shopping_formValidateActivity.this.list2.add("请选择");
                        Shopping_formValidateActivity.this.list2.add("自由付款方式");
                        Shopping_formValidateActivity.this.jsfs_Spinner.setSelection(1, true);
                    } else if ("请选择".equals(Shopping_formValidateActivity.this.fkfs)) {
                        Shopping_formValidateActivity.this.fkfs = StringEx.Empty;
                        Shopping_formValidateActivity.this.list2.clear();
                        Shopping_formValidateActivity.this.list2.add("请选择");
                        Shopping_formValidateActivity.this.jsfs_Spinner.setSelection(0, true);
                    }
                    arg0.setVisibility(0);
                }

                public void onNothingSelected(AdapterView<?> arg0) {
                    arg0.setVisibility(0);
                }
            });
        }
        this.fkfs_Spinner.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                v.setVisibility(0);
            }
        });
        this.adapter2_jsfs = new ArrayAdapter<>(this, 17367048, this.list2);
        this.adapter2_jsfs.setDropDownViewResource(17367049);
        this.jsfs_Spinner.setAdapter((SpinnerAdapter) this.adapter2_jsfs);
        if (this.list2.size() > 2) {
            this.jsfs_Spinner.setSelection(2, true);
        } else if (this.list2.size() > 1) {
            this.jsfs_Spinner.setSelection(1, true);
        }
        this.jsfs_Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView arg0, View arg1, int arg2, long arg3) {
                Shopping_formValidateActivity.this.jsfs = (String) Shopping_formValidateActivity.this.adapter2_jsfs.getItem(arg2);
                if ("场内结算".equals(Shopping_formValidateActivity.this.jsfs)) {
                    Shopping_formValidateActivity.this.jsfs = "4";
                } else if ("场外结算".equals(Shopping_formValidateActivity.this.jsfs)) {
                    Shopping_formValidateActivity.this.jsfs = "5";
                } else if ("自由付款方式".equals(Shopping_formValidateActivity.this.jsfs)) {
                    Shopping_formValidateActivity.this.jsfs = "6";
                } else if ("请选择".equals(Shopping_formValidateActivity.this.jsfs)) {
                    Shopping_formValidateActivity.this.jsfs = StringEx.Empty;
                }
                arg0.setVisibility(0);
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.jsfs_Spinner.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                v.setVisibility(0);
            }
        });
    }

    public void testBusi() {
        Shopping_formValidateBusi sfvBusi = new Shopping_formValidateBusi(this);
        Shopping_formValidateBusi.gpls = this.gpls_indent;
        Shopping_formValidateBusi.yjdh = this.yjdh_indent;
        sfvBusi.setHttpCallBack(this.httpCallBack1);
        sfvBusi.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    public void testBusi2() {
        Shopping_jsfsBusi jsfsBusi = new Shopping_jsfsBusi(this);
        Shopping_jsfsBusi.gpls = this.gpls_indent;
        Shopping_jsfsBusi.yjdh = this.yjdh_indent;
        jsfsBusi.setHttpCallBack(this.httpCallBack2);
        jsfsBusi.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    /* access modifiers changed from: package-private */
    public void update_msg(Shop_toContractRealParse ds) {
        if (Shop_toContractRealParse.commonData == null) {
            CustomMessageShow.getInst().showLongToast(this, "服务器无数据返回！");
        } else if (Shop_toContractRealParse.commonData.msg.toString().indexOf("成功") != -1) {
            new AlertDialog.Builder(this).setMessage("生成合同成功").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                }
            }).create().show();
            Dialog alertDialog2 = new AlertDialog.Builder(this).setMessage("请上东方钢铁在线官网完成支付、配送、在线等业务").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    ExitApplication.getInstance().startActivity(Shopping_formValidateActivity.this, ContractListActivity.class);
                }
            }).create();
            alertDialog2.setCanceledOnTouchOutside(false);
            alertDialog2.show();
        } else {
            Dialog alertDialog22 = new AlertDialog.Builder(this).setMessage("生成合同失败").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    ExitApplication.getInstance().startActivity(Shopping_formValidateActivity.this, ShopActivity.class);
                }
            }).create();
            alertDialog22.setCanceledOnTouchOutside(false);
            alertDialog22.show();
        }
    }

    /* access modifiers changed from: package-private */
    public void updatejsfs(ContractParse shParse) {
        if (ContractParse.commonData != null && ContractParse.commonData.blocks != null && ContractParse.commonData.blocks.r0 != null && ContractParse.commonData.blocks.r0.mar != null && ContractParse.commonData.blocks.r0.mar.rows != null) {
            if (!(ContractParse.commonData == null || ContractParse.commonData.blocks == null || ContractParse.commonData.blocks.r0 == null || ContractParse.commonData.blocks.r0.mar == null || ContractParse.commonData.blocks.r0.mar.rows == null || ContractParse.commonData.blocks.r0.mar.rows.rows == null)) {
                for (int i = 0; i < ContractParse.commonData.blocks.r0.mar.rows.rows.size(); i++) {
                    ArrayList<String> rowdata = ContractParse.commonData.blocks.r0.mar.rows.rows.get(i);
                    if (rowdata != null) {
                        for (int k = 0; k < rowdata.size(); k++) {
                            if (k == 0) {
                                System.out.println("0---" + ((String) rowdata.get(k)));
                                if (((String) rowdata.get(k)).indexOf("0") != -1) {
                                    this.list.add("全额");
                                }
                                if (((String) rowdata.get(k)).indexOf("2") != -1) {
                                    this.list.add("定金");
                                }
                                if (((String) rowdata.get(k)).indexOf("3") != -1) {
                                    this.list.add("融资");
                                }
                            }
                            if (k == 1) {
                                System.out.println("1---" + ((String) rowdata.get(k)));
                                if (((String) rowdata.get(k)).indexOf("4") != -1) {
                                    this.list2.add("场内结算");
                                }
                                if (((String) rowdata.get(k)).indexOf("5") != -1) {
                                    this.list2.add("场外结算");
                                }
                                if (((String) rowdata.get(k)).indexOf("6") != -1) {
                                    this.list2.add("自由付款方式");
                                }
                            }
                        }
                        rowdata.clear();
                    }
                }
            }
            System.out.println("--list.size-:" + this.list.size());
            System.out.println("--list2.size:" + this.list2.size());
            for (int i2 = 0; i2 < this.list.size(); i2++) {
                System.out.println("list--v :" + this.list.get(i2).toString());
            }
            for (int i3 = 0; i3 < this.list2.size(); i3++) {
                System.out.println("list2--v :" + this.list2.get(i3).toString());
            }
            init_spinner();
        }
    }

    /* access modifiers changed from: package-private */
    public void updateNoticeList(SearchParse searchParse) {
        if (SearchParse.commonData == null || SearchParse.commonData.blocks == null || SearchParse.commonData.blocks.r0 == null || SearchParse.commonData.blocks.r0.mar == null || SearchParse.commonData.blocks.r0.mar.rows == null) {
            CustomMessageShow.getInst().showLongToast(this, "服务器无数据返回！");
            return;
        }
        if (SearchParse.commonData.blocks.r0.mar.rows.rows.size() == 0) {
            new AlertDialog.Builder(this).setMessage("对不起，没有数据!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    ExitApplication.getInstance().back(0);
                }
            }).show();
        } else if (!(SearchParse.commonData == null || SearchParse.commonData.blocks == null || SearchParse.commonData.blocks.r0 == null || SearchParse.commonData.blocks.r0.mar == null || SearchParse.commonData.blocks.r0.mar.rows == null || SearchParse.commonData.blocks.r0.mar.rows.rows == null)) {
            for (int i = 0; i < SearchParse.commonData.blocks.r0.mar.rows.rows.size(); i++) {
                ArrayList<String> rowdata = SearchParse.commonData.blocks.r0.mar.rows.rows.get(i);
                SearchRow sr = new SearchRow();
                if (rowdata != null) {
                    for (int k = 0; k < rowdata.size(); k++) {
                        if (k == 0) {
                            sr.number = String.valueOf(String.valueOf(i + 1)) + ".";
                            sr.mj = (String) rowdata.get(k);
                        }
                        if (k == 1) {
                            sr.productname = (String) rowdata.get(k);
                        } else if (k == 2) {
                            sr.mertial = (String) rowdata.get(k);
                        } else if (k == 3) {
                            sr.format = (String) rowdata.get(k);
                        } else if (k == 4) {
                            sr.piece = (String) rowdata.get(k);
                        } else if (k == 5) {
                            sr.weigh = (String) rowdata.get(k);
                        } else if (k == 6) {
                            sr.price = (String) rowdata.get(k);
                        } else if (k == 7) {
                            sr.ck = (String) rowdata.get(k);
                        } else if (k == 8) {
                            sr.zsl = (String) rowdata.get(k);
                        } else if (k == 9) {
                            sr.zzl = (String) rowdata.get(k);
                        } else if (k == 10) {
                            sr.zje = (String) rowdata.get(k);
                        } else if (k == 11) {
                            sr.gpls = (String) rowdata.get(k);
                        } else if (k == 12) {
                            sr.wzly = (String) rowdata.get(k);
                            this.wzly = (String) rowdata.get(k);
                            System.out.println("wzly++++" + this.wzly);
                        } else if (k == 13) {
                            sr.zylx = (String) rowdata.get(k);
                            this.zylx = (String) rowdata.get(k);
                            System.out.println("zylx++++" + this.zylx);
                        } else if (k == 14) {
                            sr.isdj = (String) rowdata.get(k);
                        } else if (k == 15) {
                            sr.rzgx = (String) rowdata.get(k);
                            this.rzgx = sr.rzgx;
                        } else if (k == 16) {
                            sr.cd = (String) rowdata.get(k);
                        }
                    }
                    this.LisItems.add(sr);
                    this.view2 = buildRowView(sr);
                    this.indents_list.addViewToLast(this.view2);
                    rowdata.removeAll(rowdata);
                }
            }
            this.indents_list.onRefreshComplete();
        }
        testBusi2();
    }

    private View buildRowView(SearchRow sr) {
        this.view = View.inflate(this, R.layout.xhjy_shopping_formvalidate_row, null);
        ((TextView) this.view.findViewById(R.id.productname_indent_info)).setText(String.valueOf(sr.number) + sr.productname);
        ((TextView) this.view.findViewById(R.id.material_indent_info)).setText("材质:" + sr.mertial);
        ((TextView) this.view.findViewById(R.id.formatname_indent_info)).setText("规格:" + sr.format);
        ((TextView) this.view.findViewById(R.id.ck_indent_info)).setText("仓库:" + sr.ck);
        ((TextView) this.view.findViewById(R.id.price_indent_info)).setText(sr.price);
        ((TextView) this.view.findViewById(R.id.numberunit_indent_info)).setText(String.valueOf(sr.piece) + "件/" + sr.weigh + "吨");
        ((TextView) this.view.findViewById(R.id.addressname_indent_info)).setText("产地:" + sr.cd);
        return this.view;
    }

    class SearchRow {
        public String cd;
        public String ck;
        public String format;
        public String gpls;
        public String isdj;
        public String mertial;
        public String mj;
        public String number;
        public String piece;
        public String price;
        public String productname;
        public String rzgx;
        public String weigh;
        public String wzly;
        public String zje;
        public String zsl;
        public String zylx;
        public String zzl;

        SearchRow() {
        }
    }

    public void indent_fiterButtonAction(View v) {
        if (this.state.booleanValue()) {
            if (StringEx.Empty.equals(this.fkfs)) {
                new AlertDialog.Builder(this).setMessage("请选择付款方式").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                }).create().show();
            } else if (StringEx.Empty.equals(this.jsfs)) {
                new AlertDialog.Builder(this).setMessage("请选择结算方式").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                }).create().show();
            } else {
                Shop_toContractRealBusi toContractBusi = new Shop_toContractRealBusi(this);
                toContractBusi.gpls = this.gpls_indent;
                toContractBusi.fkfs = this.fkfs;
                toContractBusi.jsfs = this.jsfs;
                toContractBusi.zje = this.amount;
                toContractBusi.yjdh = this.yjdh_indent;
                toContractBusi.setHttpCallBack(this.httpCallBack3);
                toContractBusi.iExecute();
                this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
            }
        } else if (!this.state.booleanValue()) {
            new AlertDialog.Builder(this).setMessage("请勾选同意《东方钢铁在线议价协议》").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                }
            }).create().show();
        }
    }

    public void indent_backButtonAction(View v) {
        ExitApplication.getInstance().back(0);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.indents_list = null;
        this.who = null;
        this.msg = null;
        this.wpName = null;
        this.type = null;
        this.LisItems = null;
        this.IV_state1 = null;
        this.seller_indent = null;
        this.piece_indent = null;
        this.zongdun_indent = null;
        this.zongjine_indent = null;
        this.btn_rigth = null;
        this.view = null;
        this.view2 = null;
        super.onDestroy();
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.radio_search3:
                ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
                return;
            case R.id.radio_shopcar3:
                ((RadioButton) findViewById(R.id.radio_shopcar3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ShopActivity.class);
                return;
            case R.id.radio_home3:
                ((RadioButton) findViewById(R.id.radio_home3)).setChecked(true);
                HashMap hasmap = new HashMap();
                hasmap.put("index_more", "1");
                ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class, hasmap);
                return;
            case R.id.radio_contract3:
                ((RadioButton) findViewById(R.id.radio_contract3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ContractListActivity.class);
                return;
            case R.id.radio_notice3:
                ((RadioButton) findViewById(R.id.radio_notice3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, Xhjy_more.class);
                return;
            default:
                return;
        }
    }
}
