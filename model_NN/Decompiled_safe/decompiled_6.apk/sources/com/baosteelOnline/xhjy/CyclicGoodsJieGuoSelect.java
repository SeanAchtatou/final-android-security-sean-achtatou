package com.baosteelOnline.xhjy;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.R;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.model.SmallNumUtil;
import com.jianq.misc.StringEx;
import java.util.Calendar;

public class CyclicGoodsJieGuoSelect extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    /* access modifiers changed from: private */
    public long beginTime = 20130601;
    /* access modifiers changed from: private */
    public TextView date1;
    /* access modifiers changed from: private */
    public TextView date2;
    DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
            int mon = month + 1;
            String monString = new StringBuilder(String.valueOf(mon)).toString();
            if (mon < 10) {
                monString = "0" + monString;
            }
            if (dayOfMonth > 9) {
                CyclicGoodsJieGuoSelect.this.dateString = " " + year + "-" + monString + "-" + dayOfMonth + " ";
            } else {
                CyclicGoodsJieGuoSelect.this.dateString = " " + year + "-" + monString + "-" + "0" + dayOfMonth + " ";
            }
            String ss = CyclicGoodsJieGuoSelect.this.dateString.replaceAll("\\D", StringEx.Empty);
            Log.d("dateString=", CyclicGoodsJieGuoSelect.this.dateString);
            if (CyclicGoodsJieGuoSelect.this.isDate1) {
                CyclicGoodsJieGuoSelect.this.beginTime = Long.parseLong(ss);
                CyclicGoodsJieGuoSelect.this.date1.setText(CyclicGoodsJieGuoSelect.this.dateString);
                return;
            }
            CyclicGoodsJieGuoSelect.this.endTime = Long.parseLong(ss);
            CyclicGoodsJieGuoSelect.this.date2.setText(CyclicGoodsJieGuoSelect.this.dateString);
        }
    };
    /* access modifiers changed from: private */
    public String dateString = StringEx.Empty;
    /* access modifiers changed from: private */
    public long endTime = 20130808;
    /* access modifiers changed from: private */
    public boolean isDate1 = true;
    private Calendar mCalendar;
    /* access modifiers changed from: private */
    public DatePickerDialog mDatePickerDialog;
    /* access modifiers changed from: private */
    public DatePickerDialog mDatePickerDialog2;
    public RadioButton mIndexNavigation1;
    public RadioButton mIndexNavigation2;
    public RadioButton mIndexNavigation3;
    public RadioButton mIndexNavigation4;
    public RadioButton mIndexNavigation5;
    public TextView mListTitleTextview;
    public RadioGroup mRadioderGroup;
    private EditText mSpinnerTextView;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        String defaultYearOfDate1;
        String defaultYearOfDate2;
        String defaultMonthOfDate1;
        String defaultMonthOfDate2;
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.activity_cyclic_goods_jie_guo_select);
        ExitApplication.getInstance().addActivity(this);
        this.mSpinnerTextView = (EditText) findViewById(R.id.select_spinner);
        this.date1 = (TextView) findViewById(R.id.cyclicgoods_bid_session_select_date_edit1);
        this.date2 = (TextView) findViewById(R.id.cyclicgoods_bid_session_select_date_edit2);
        this.mCalendar = Calendar.getInstance();
        if (this.mCalendar.get(2) + 1 == 12) {
            defaultYearOfDate1 = new StringBuilder(String.valueOf(this.mCalendar.get(1))).toString();
            defaultYearOfDate2 = new StringBuilder(String.valueOf(this.mCalendar.get(1) + 1)).toString();
            defaultMonthOfDate1 = "11";
            defaultMonthOfDate2 = "01";
        } else if (this.mCalendar.get(2) == 0) {
            defaultYearOfDate1 = new StringBuilder(String.valueOf(this.mCalendar.get(1) - 1)).toString();
            defaultYearOfDate2 = new StringBuilder(String.valueOf(this.mCalendar.get(1))).toString();
            defaultMonthOfDate1 = "12";
            defaultMonthOfDate2 = "02";
        } else {
            defaultYearOfDate1 = new StringBuilder(String.valueOf(this.mCalendar.get(1))).toString();
            defaultYearOfDate2 = defaultYearOfDate1;
            int mon = this.mCalendar.get(2) + 1;
            if (mon < 11) {
                defaultMonthOfDate1 = "0" + this.mCalendar.get(2);
            } else {
                defaultMonthOfDate1 = new StringBuilder(String.valueOf(this.mCalendar.get(2))).toString();
            }
            if (mon < 9) {
                defaultMonthOfDate2 = "0" + (mon + 1);
            } else {
                defaultMonthOfDate2 = new StringBuilder(String.valueOf(mon + 1)).toString();
            }
        }
        String begin = " " + defaultYearOfDate1 + "-" + defaultMonthOfDate1 + "-" + "01" + " ";
        String end = " " + defaultYearOfDate2 + "-" + defaultMonthOfDate2 + "-" + "01" + " ";
        this.date1.setText(begin);
        this.date2.setText(end);
        this.beginTime = Long.parseLong(begin.replaceAll("\\D", StringEx.Empty));
        this.endTime = Long.parseLong(end.replaceAll("\\D", StringEx.Empty));
        this.mListTitleTextview = (TextView) findViewById(R.id.cyclicgoods_list_title_textview);
        this.mListTitleTextview.setText("竞价结果");
        this.mRadioderGroup = (RadioGroup) findViewById(R.id.cyclic_goods_index_RGroup);
        this.mIndexNavigation1 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item1);
        this.mIndexNavigation2 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item2);
        this.mIndexNavigation3 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item3);
        this.mIndexNavigation4 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item4);
        this.mIndexNavigation5 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item5);
        SmallNumUtil.JJNum(this, (TextView) findViewById(R.id.jj_dhqrnum));
        this.mRadioderGroup.setOnCheckedChangeListener(this);
        this.mIndexNavigation1.setChecked(true);
        this.mIndexNavigation1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ExitApplication.getInstance().startActivity(CyclicGoodsJieGuoSelect.this, CyclicGoodsIndexActivity.class);
            }
        });
        this.date1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CyclicGoodsJieGuoSelect.this.isDate1 = true;
                String[] ss = CyclicGoodsJieGuoSelect.this.date1.getText().toString().replaceAll(" ", StringEx.Empty).split("-");
                CyclicGoodsJieGuoSelect.this.mDatePickerDialog = new DatePickerDialog(CyclicGoodsJieGuoSelect.this, CyclicGoodsJieGuoSelect.this.dateListener, Integer.parseInt(ss[0]), Integer.parseInt(ss[1]) - 1, Integer.parseInt(ss[2]));
                CyclicGoodsJieGuoSelect.this.mDatePickerDialog.show();
            }
        });
        this.date2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CyclicGoodsJieGuoSelect.this.isDate1 = false;
                String[] ss = CyclicGoodsJieGuoSelect.this.date2.getText().toString().replaceAll(" ", StringEx.Empty).split("-");
                CyclicGoodsJieGuoSelect.this.mDatePickerDialog2 = new DatePickerDialog(CyclicGoodsJieGuoSelect.this, CyclicGoodsJieGuoSelect.this.dateListener, Integer.parseInt(ss[0]), Integer.parseInt(ss[1]) - 1, Integer.parseInt(ss[2]));
                CyclicGoodsJieGuoSelect.this.mDatePickerDialog2.show();
            }
        });
    }

    public void doneAction(View v) {
        Intent intent = new Intent(this, CyclicGoodsJingJiaJieGuo.class);
        String maiFanDanwei = this.mSpinnerTextView.getText().toString();
        if (maiFanDanwei.equals(StringEx.Empty) || maiFanDanwei.equals("null") || maiFanDanwei.equals(null)) {
            intent.putExtra("maiFanDanwei", StringEx.Empty);
        } else {
            intent.putExtra("maiFanDanwei", maiFanDanwei);
        }
        intent.putExtra("beginDate", this.date1.getText().toString().replaceAll(" ", StringEx.Empty).replaceAll("-", StringEx.Empty));
        intent.putExtra("endDate", this.date2.getText().toString().replaceAll(" ", StringEx.Empty).replaceAll("-", StringEx.Empty));
        Log.d("传递的数据", "maiFanDanwei" + maiFanDanwei + " beginDate=" + this.date1.getText().toString().replaceAll(" ", StringEx.Empty).replaceAll("-", StringEx.Empty) + " endDate=" + this.date2.getText().toString().replaceAll(" ", StringEx.Empty).replaceAll("-", StringEx.Empty));
        startActivity(intent);
        finish();
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.cyclicgoods_index_navigation_item2:
                this.mIndexNavigation2.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item3:
                this.mIndexNavigation3.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidSessionSelect.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item1:
                if (ObjectStores.getInst().getObject("formX") == "formX") {
                    ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class);
                    finish();
                    return;
                }
                this.mIndexNavigation1.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item4:
                this.mIndexNavigation4.setChecked(true);
                startActivity(new Intent(this, CyclicGoodsBidHallPageActivity.class));
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item5:
                this.mIndexNavigation5.setChecked(true);
                Intent intent = new Intent(this, Xhjy_more.class);
                intent.putExtra("part", "循环物资");
                startActivity(intent);
                finish();
                return;
            default:
                return;
        }
    }

    public void backAaction(View v) {
        if (ObjectStores.getInst().getObject("formX") == "formX") {
            ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class);
        } else {
            ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
        }
        finish();
    }

    public void onBackPressed() {
        if (ObjectStores.getInst().getObject("formX") == "formX") {
            ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class);
            finish();
            return;
        }
        ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
        finish();
    }

    public void dateAction(View v) {
        switch (v.getId()) {
            case R.id.cyclicgoods_bid_session_select_date_edit1:
                this.isDate1 = true;
                this.mDatePickerDialog.show();
                return;
            case R.id.cyclicgoods_bid_session_select_line:
            default:
                return;
            case R.id.cyclicgoods_bid_session_select_date_edit2:
                this.isDate1 = false;
                this.mDatePickerDialog2.show();
                return;
        }
    }
}
