package com.baosteelOnline.xhjy;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.R;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.data.Msg_registerEquipmentInfoBusi;
import com.baosteelOnline.data_parse.ContractParse;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.model.SmallNumUtil;
import com.baosteelOnline.service.MsgService;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import java.util.HashMap;

public class MoreMessageSetting extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    private RelativeLayout bottom_nva1;
    private RelativeLayout bottom_nva2;
    public ContractParse contractParse;
    private JQUICallBack httpCallBackRes = new JQUICallBack() {
        public void callBack(ResultData result) {
            String dataString = result.getStringData();
            if (!dataString.equals(null) && !dataString.equals(StringEx.Empty)) {
                MoreMessageSetting.this.contractParse = ContractParse.parse(result.getStringData());
                if (!ContractParse.getDataString().equals(null) && !ContractParse.getDataString().equals(StringEx.Empty) && !ContractParse.commonData.msg.isEmpty()) {
                    Log.d("获取的注册推送数据：", ContractParse.getDataString());
                    Log.d("获取的注册推送信息：", ContractParse.commonData.msg);
                    String pushMessage = ContractParse.commonData.msg;
                    if (pushMessage.contains("成功") || pushMessage.equals("该设备已有相关信息存在了")) {
                        MoreMessageSetting.this.startService(new Intent(MoreMessageSetting.this, MsgService.class));
                    }
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public boolean isCheck = true;
    private String kaiqituisong = StringEx.Empty;
    public RadioButton mIndexNavigation1;
    public RadioButton mIndexNavigation2;
    public RadioButton mIndexNavigation3;
    public RadioButton mIndexNavigation4;
    public RadioButton mIndexNavigation5;
    /* access modifiers changed from: private */
    public SharedPreferences.Editor mMessageServerEditor;
    private SharedPreferences mMessageServerSharedPreferences;
    public RadioGroup mRadioderGroup;
    private RadioGroup radioderGroup;
    private SharedPreferences sp;
    /* access modifiers changed from: private */
    public TextView xiaoxi_tongzhi;
    /* access modifiers changed from: private */
    public RadioButton xiaoxi_tongzhi_rb;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.activity_more_message_setting);
        ExitApplication.getInstance().addActivity(this);
        this.mRadioderGroup = (RadioGroup) findViewById(R.id.cyclic_goods_index_RGroup);
        this.mIndexNavigation1 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item1);
        this.mIndexNavigation2 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item2);
        this.mIndexNavigation3 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item3);
        this.mIndexNavigation4 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item4);
        this.mIndexNavigation5 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item5);
        SmallNumUtil.JJNum(this, (TextView) findViewById(R.id.jj_dhqrnum));
        this.mRadioderGroup.setOnCheckedChangeListener(this);
        this.bottom_nva1 = (RelativeLayout) findViewById(R.id.bottom_nva1);
        this.bottom_nva2 = (RelativeLayout) findViewById(R.id.bottom_nva2);
        RadioButton radio_notice = (RadioButton) findViewById(R.id.radio_notice3);
        SmallNumUtil.GWCNum(this, (TextView) findViewById(R.id.gwc_dhqrnum));
        this.radioderGroup = (RadioGroup) findViewById(R.id.xhjy_index_RGroup);
        this.radioderGroup.setOnCheckedChangeListener(this);
        this.sp = getSharedPreferences("BaoIndex", 0);
        if (this.sp.getString("bIndex", StringEx.Empty).equals("xhwz")) {
            this.bottom_nva1.setVisibility(8);
            this.bottom_nva2.setVisibility(0);
            this.mIndexNavigation5.setChecked(true);
        } else {
            radio_notice.setChecked(true);
            this.bottom_nva2.setVisibility(8);
            this.bottom_nva1.setVisibility(0);
        }
        this.mMessageServerSharedPreferences = getSharedPreferences("MessageServer", 0);
        this.mMessageServerEditor = this.mMessageServerSharedPreferences.edit();
        this.kaiqituisong = this.mMessageServerSharedPreferences.getString("kaiqituisong", StringEx.Empty);
        this.xiaoxi_tongzhi = (TextView) findViewById(R.id.xiaoxi_tongzhi);
        this.xiaoxi_tongzhi_rb = (RadioButton) findViewById(R.id.xiaoxi_tongzhi_rb);
        if (this.kaiqituisong.equals("YES")) {
            this.xiaoxi_tongzhi_rb.setChecked(true);
            this.xiaoxi_tongzhi.setText("开启消息通知");
            this.isCheck = true;
        } else if (this.kaiqituisong.equals("NO")) {
            this.xiaoxi_tongzhi_rb.setChecked(false);
            this.xiaoxi_tongzhi.setText("关闭消息通知");
            this.isCheck = false;
        }
        this.xiaoxi_tongzhi_rb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (MoreMessageSetting.this.isCheck) {
                    MoreMessageSetting.this.xiaoxi_tongzhi_rb.setChecked(false);
                    MoreMessageSetting.this.mMessageServerEditor.putString("kaiqituisong", "NO");
                    MoreMessageSetting.this.mMessageServerEditor.commit();
                    MoreMessageSetting.this.xiaoxi_tongzhi.setText("关闭消息通知");
                    MoreMessageSetting.this.stopService(new Intent(MoreMessageSetting.this, MsgService.class));
                    MoreMessageSetting.this.isCheck = false;
                    return;
                }
                MoreMessageSetting.this.xiaoxi_tongzhi_rb.setChecked(true);
                MoreMessageSetting.this.mMessageServerEditor.putString("kaiqituisong", "YES");
                MoreMessageSetting.this.mMessageServerEditor.commit();
                MoreMessageSetting.this.xiaoxi_tongzhi.setText("开启消息通知");
                MoreMessageSetting.this.stopService(new Intent(MoreMessageSetting.this, MsgService.class));
                MoreMessageSetting.this.registerServer();
                MoreMessageSetting.this.isCheck = true;
            }
        });
    }

    /* access modifiers changed from: private */
    public void registerServer() {
        Msg_registerEquipmentInfoBusi res = new Msg_registerEquipmentInfoBusi(this);
        res.setHttpCallBack(this.httpCallBackRes);
        res.iExecute();
    }

    public void messageHistoryAction(View v) {
        Intent intent = new Intent(this, MessageHistory.class);
        intent.putExtra("part", getIntent().getExtras().getString("part"));
        startActivity(intent);
        finish();
    }

    public void backButtonAction(View v) {
        Intent intent = new Intent(this, Xhjy_more.class);
        intent.putExtra("part", getIntent().getExtras().getString("part"));
        startActivity(intent);
        finish();
    }

    public void onBackPressed() {
        Intent intent = new Intent(this, Xhjy_more.class);
        intent.putExtra("part", getIntent().getExtras().getString("part"));
        startActivity(intent);
        finish();
        super.onBackPressed();
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.cyclicgoods_index_navigation_item2:
                this.mIndexNavigation2.setChecked(true);
                startActivity(new Intent(this, CyclicGoodsBidActivity.class));
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item3:
                this.mIndexNavigation3.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidSessionSelect.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item1:
                if (ObjectStores.getInst().getObject("formX") == "formX") {
                    ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class);
                    finish();
                    return;
                }
                this.mIndexNavigation1.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item4:
                this.mIndexNavigation4.setChecked(true);
                startActivity(new Intent(this, CyclicGoodsBidHallPageActivity.class));
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item5:
                this.mIndexNavigation5.setChecked(true);
                return;
            case R.id.radio_search3:
                ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
                return;
            case R.id.radio_shopcar3:
                ((RadioButton) findViewById(R.id.radio_shopcar3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ShopActivity.class);
                return;
            case R.id.radio_home3:
                ((RadioButton) findViewById(R.id.radio_home3)).setChecked(true);
                HashMap hasmap = new HashMap();
                hasmap.put("index_more", "1");
                ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class, hasmap);
                return;
            case R.id.radio_contract3:
                ((RadioButton) findViewById(R.id.radio_contract3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ContractListActivity.class);
                return;
            case R.id.radio_notice3:
                ((RadioButton) findViewById(R.id.radio_notice3)).setChecked(true);
                return;
            default:
                return;
        }
    }
}
