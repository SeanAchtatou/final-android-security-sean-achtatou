package com.baosteelOnline.xhjy;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.baosteelOnline.R;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.data.RequestBidData;
import com.baosteelOnline.data_parse.ContractParse;
import com.baosteelOnline.main.ExitApplication;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.net.JQBasicNetwork;
import com.jianq.ui.JQUICallBack;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;

public class GCJYBidProtocolPageActivity extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    private String activityName;
    public ContractParse contractParse;
    private String dfyj = StringEx.Empty;
    /* access modifiers changed from: private */
    public Double dfyjDouble = Double.valueOf(0.0d);
    /* access modifiers changed from: private */
    public String getDataName = StringEx.Empty;
    private JQUICallBack httpCallBack = new JQUICallBack() {
        public void callBack(ResultData result) {
            GCJYBidProtocolPageActivity.this.progressDialog.dismiss();
            Log.d("CyclicGoodsBidProtocolPageActivity获取的数据", result.getStringData());
            if (!result.getStringData().equals(null) && !result.getStringData().equals(StringEx.Empty)) {
                GCJYBidProtocolPageActivity.this.contractParse = ContractParse.parse(result.getStringData());
                if (ContractParse.getDataString().equals(null) || ContractParse.getDataString().equals(StringEx.Empty)) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(GCJYBidProtocolPageActivity.this);
                    dialog.setCancelable(false);
                    dialog.setMessage("获取数据失败").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            GCJYBidProtocolPageActivity.this.outActivity();
                        }
                    }).show();
                    return;
                }
                Log.d("获取的ROWs数据：", ContractParse.jjo.toString());
                Log.d("获取的DataString数据：", ContractParse.getDataString());
                System.out.println(ContractParse.getDataString());
                if (!ContractParse.jjo.toString().equals(null) && !ContractParse.jjo.toString().equals("[[]]") && !ContractParse.jjo.toString().equals(StringEx.Empty) && !ContractParse.jjo.toString().equals("[]")) {
                    for (int i = 0; i < ContractParse.jjo.length(); i++) {
                        String mHtmlData = StringEx.Empty;
                        String resultString = StringEx.Empty;
                        try {
                            JSONArray row = new JSONArray(ContractParse.jjo.getString(i));
                            if (GCJYBidProtocolPageActivity.this.getDataName.equals("协议条款同意")) {
                                resultString = row.getString(0);
                                String wtzt = row.getString(1);
                            } else if (GCJYBidProtocolPageActivity.this.getDataName.equals("卖家协议")) {
                                mHtmlData = row.getString(0);
                            } else {
                                mHtmlData = row.getString(0);
                                GCJYBidProtocolPageActivity.this.dfyjDouble = Double.valueOf(row.getDouble(1));
                                GCJYBidProtocolPageActivity.this.kyyeDouble = Double.valueOf(row.getDouble(2));
                            }
                            Log.d("CyclicGoodsBidPageActivity获取的ROWs数据HtmlData：", mHtmlData);
                            if (GCJYBidProtocolPageActivity.this.getDataName.equals("卖家协议")) {
                                if (mHtmlData.equals(StringEx.Empty) || mHtmlData.equals(null)) {
                                    GCJYBidProtocolPageActivity.this.ismHtmlData = false;
                                    GCJYBidProtocolPageActivity.this.mListTitleTextview.setText("竞价协议");
                                    GCJYBidProtocolPageActivity.this.getDate("竞价协议");
                                } else {
                                    GCJYBidProtocolPageActivity.this.ismHtmlData = true;
                                    GCJYBidProtocolPageActivity.this.mListTitleTextview.setText("卖家协议");
                                    GCJYBidProtocolPageActivity.this.mWebView.loadDataWithBaseURL(null, mHtmlData, "text/html", JQBasicNetwork.UTF_8, null);
                                }
                            } else if (GCJYBidProtocolPageActivity.this.getDataName.equals("竞价协议")) {
                                GCJYBidProtocolPageActivity.this.mWebView.getSettings().setUseWideViewPort(true);
                                GCJYBidProtocolPageActivity.this.mWebView.getSettings().setLoadWithOverviewMode(true);
                                GCJYBidProtocolPageActivity.this.mListTitleTextview.setText("竞价协议");
                                GCJYBidProtocolPageActivity.this.ismHtmlData = false;
                                if (!mHtmlData.equals("null") && !mHtmlData.equals(StringEx.Empty) && !mHtmlData.equals(null)) {
                                    GCJYBidProtocolPageActivity.this.mWebView.loadDataWithBaseURL(null, mHtmlData, "text/html", JQBasicNetwork.UTF_8, null);
                                    new CountDownTimer(10000, 1000) {
                                        public void onTick(long millisUntilFinished) {
                                            GCJYBidProtocolPageActivity.this.mAgreeButton.setTextColor(-7829368);
                                            GCJYBidProtocolPageActivity.this.mAgreeButton.setText("同意协议（ " + (millisUntilFinished / 1000) + "）");
                                        }

                                        public void onFinish() {
                                            GCJYBidProtocolPageActivity.this.mAgreeButton.setText("同意协议");
                                            GCJYBidProtocolPageActivity.this.mAgreeButton.setTextColor(-16777216);
                                            GCJYBidProtocolPageActivity.this.timeOfAgree = true;
                                        }
                                    }.start();
                                }
                            } else if (GCJYBidProtocolPageActivity.this.getDataName.equals("协议条款同意")) {
                                if (resultString.equals("1")) {
                                    if (GCJYBidProtocolPageActivity.this.wtid.startsWith("J")) {
                                        new AlertDialog.Builder(GCJYBidProtocolPageActivity.this).setMessage("您已成功参加竞价！该场次为集合议价，无法在移动客户端进行出价！").setPositiveButton("参加其他场次", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                ExitApplication.getInstance().startActivity(GCJYBidProtocolPageActivity.this, CyclicGoodsBidSessionSelect.class);
                                                GCJYBidProtocolPageActivity.this.finish();
                                            }
                                        }).show();
                                    } else {
                                        new AlertDialog.Builder(GCJYBidProtocolPageActivity.this).setMessage("您已成功参加竞价！").setNegativeButton("开始出价", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                GCJYBidProtocolPageActivity.this.startActivity(new Intent(GCJYBidProtocolPageActivity.this, CyclicGoodsBidHallPageActivity.class));
                                                GCJYBidProtocolPageActivity.this.finish();
                                            }
                                        }).setPositiveButton("其他场次", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                ExitApplication.getInstance().startActivity(GCJYBidProtocolPageActivity.this, CyclicGoodsBidSessionSelect.class);
                                                GCJYBidProtocolPageActivity.this.finish();
                                            }
                                        }).show();
                                    }
                                } else if (resultString.equals("0")) {
                                    new AlertDialog.Builder(GCJYBidProtocolPageActivity.this).setMessage("参加竞价失败！").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    }).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } else if (GCJYBidProtocolPageActivity.this.getDataName.equals("卖家协议")) {
                    GCJYBidProtocolPageActivity.this.ismHtmlData = false;
                    GCJYBidProtocolPageActivity.this.getDate("竞价协议");
                } else {
                    AlertDialog.Builder dialog2 = new AlertDialog.Builder(GCJYBidProtocolPageActivity.this);
                    dialog2.setCancelable(false);
                    dialog2.setMessage("数据为空！").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            GCJYBidProtocolPageActivity.this.outActivity();
                        }
                    }).show();
                }
            }
        }
    };
    private String isJoin = StringEx.Empty;
    /* access modifiers changed from: private */
    public boolean ismHtmlData = true;
    private String kyye = StringEx.Empty;
    /* access modifiers changed from: private */
    public Double kyyeDouble = Double.valueOf(0.0d);
    /* access modifiers changed from: private */
    public Button mAgreeButton;
    public TextView mListTitleTextview;
    /* access modifiers changed from: private */
    public WebView mWebView;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    private RadioGroup radioderGroup;
    /* access modifiers changed from: private */
    public boolean timeOfAgree = false;
    /* access modifiers changed from: private */
    public String wtid = StringEx.Empty;

    /* access modifiers changed from: protected */
    @SuppressLint({"SetJavaScriptEnabled"})
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.activity_gcjybid_protocol_page);
        this.mAgreeButton = (Button) findViewById(R.id.gcjy_btn_agree);
        this.radioderGroup = (RadioGroup) findViewById(R.id.xhjy_index_RGroup);
        this.radioderGroup.setOnCheckedChangeListener(this);
        ((RadioButton) findViewById(R.id.radio_home3)).setChecked(true);
        ExitApplication.getInstance().addActivity(this);
        this.wtid = getIntent().getExtras().getString("wtid");
        this.activityName = getIntent().getExtras().getString("activity");
        Log.d("获取的ID", this.wtid);
        this.mWebView = (WebView) findViewById(R.id.gcjy_webview_protocol);
        this.mWebView.getSettings().setBuiltInZoomControls(true);
        this.mWebView.setBackgroundColor(Color.parseColor("#ffffff"));
        this.mWebView.setFocusable(true);
        this.mWebView.requestFocus();
        this.mWebView.clearCache(true);
        this.mWebView.getSettings().setJavaScriptEnabled(true);
        this.mWebView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        this.mWebView.setWebViewClient(new HelloWebViewClient(this, null));
        this.mListTitleTextview = (TextView) findViewById(R.id.cyclicgoods_list_title_textview);
        this.mListTitleTextview.setText("卖家协议");
        getDate("卖家协议");
    }

    private class HelloWebViewClient extends WebViewClient {
        private HelloWebViewClient() {
        }

        /* synthetic */ HelloWebViewClient(GCJYBidProtocolPageActivity gCJYBidProtocolPageActivity, HelloWebViewClient helloWebViewClient) {
            this();
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    public void getDate(String name) {
        RequestBidData requestBidData = new RequestBidData(this);
        requestBidData.setRequestData(name);
        this.getDataName = name;
        requestBidData.setId(this.wtid);
        Log.d("请求的指令：", requestBidData.infoDate());
        requestBidData.setHttpCallBack(this.httpCallBack);
        requestBidData.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    public void onCheckedChanged(RadioGroup arg0, int arg1) {
        switch (arg1) {
            case R.id.radio_search3:
                ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
                return;
            case R.id.radio_shopcar3:
                ((RadioButton) findViewById(R.id.radio_shopcar3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ShopActivity.class);
                return;
            case R.id.radio_home3:
                ((RadioButton) findViewById(R.id.radio_home3)).setChecked(true);
                HashMap hasmap = new HashMap();
                hasmap.put("index_more", "1");
                ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class, hasmap);
                return;
            case R.id.radio_contract3:
                ((RadioButton) findViewById(R.id.radio_contract3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ContractListActivity.class);
                return;
            case R.id.radio_notice3:
                ((RadioButton) findViewById(R.id.radio_notice3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, Xhjy_more.class);
                return;
            default:
                return;
        }
    }

    public void agreeAction(View v) {
        if (this.ismHtmlData) {
            getDate("竞价协议");
        } else if (!this.timeOfAgree) {
        } else {
            if (this.dfyjDouble.doubleValue() == 0.0d || this.dfyjDouble.doubleValue() == 0.0d) {
                getDate("协议条款同意");
            } else if (this.dfyjDouble.doubleValue() > this.kyyeDouble.doubleValue()) {
                new AlertDialog.Builder(this).setMessage("余额不足！").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).show();
            } else {
                getDate("协议条款同意");
            }
        }
    }

    public void backAaction(View v) {
        outActivity();
    }

    /* access modifiers changed from: private */
    public void outActivity() {
        Intent intent;
        if (this.activityName.equals("竞价场次")) {
            try {
                Intent intent2 = new Intent(this, CyclicGoodsBidSession.class);
                try {
                    int sessionCode = getIntent().getExtras().getInt("sessionCode");
                    String startDate = getIntent().getExtras().getString("startDate");
                    String endDate = getIntent().getExtras().getString("endDate");
                    intent2.putExtra("sessionCode", sessionCode);
                    intent2.putExtra("startDate", startDate);
                    intent2.putExtra("endDate", endDate);
                    intent = intent2;
                } catch (Exception e) {
                    intent = new Intent(this, CyclicGoodsBidSessionSelect.class);
                    startActivity(intent);
                    finish();
                }
            } catch (Exception e2) {
                intent = new Intent(this, CyclicGoodsBidSessionSelect.class);
                startActivity(intent);
                finish();
            }
        } else {
            intent = new Intent(this, GCJYBidPageActivity.class);
            String activity2 = getIntent().getExtras().getString("activity2");
            intent.putExtra("id", getIntent().getExtras().getString("id"));
            intent.putExtra("activity", activity2);
            if (activity2.equals("竞价场次")) {
                int sessionCode2 = getIntent().getExtras().getInt("sessionCode");
                String startDate2 = getIntent().getExtras().getString("startDate");
                String endDate2 = getIntent().getExtras().getString("endDate");
                intent.putExtra("sessionCode", sessionCode2);
                intent.putExtra("startDate", startDate2);
                intent.putExtra("endDate", endDate2);
            }
            Log.d("CyclicGoodsBidProtocolPageActivity收到竞价公告的详情页的id：", String.valueOf(getIntent().getExtras().getString("id")) + "收到的activity：" + getIntent().getExtras().getString("activity"));
        }
        startActivity(intent);
        finish();
    }

    public void onBackPressed() {
        outActivity();
    }
}
