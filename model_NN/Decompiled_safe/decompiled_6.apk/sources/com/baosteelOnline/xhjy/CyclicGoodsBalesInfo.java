package com.baosteelOnline.xhjy;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.R;
import com.baosteelOnline.common.DataBaseFactory;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.data.RequestBidData;
import com.baosteelOnline.data_parse.ContractParse;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.model.SmallNumUtil;
import com.baosteelOnline.sql.DBHelper;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;

public class CyclicGoodsBalesInfo extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    private String cd = StringEx.Empty;
    private TextView cdTextView;
    public ContractParse contractParse;
    private String cz = StringEx.Empty;
    private TextView czTextView;
    private DataBaseFactory db;
    private DBHelper dbHelper;
    private String dj = StringEx.Empty;
    private TextView djTextView;
    private String gg = StringEx.Empty;
    private TextView ggTextView;
    private String hth = StringEx.Empty;
    private JQUICallBack httpCallBack = new JQUICallBack() {
        public void callBack(ResultData result) {
            CyclicGoodsBalesInfo.this.progressDialog.dismiss();
            Log.d("获取的数据：", result.getStringData());
            if (!result.getStringData().equals(null) && !result.getStringData().equals(StringEx.Empty)) {
                CyclicGoodsBalesInfo.this.contractParse = ContractParse.parse(result.getStringData());
                CyclicGoodsBalesInfo.this.numOfPageGet = Integer.parseInt(ContractParse.pageNumTal);
                if (ContractParse.getDataString().equals(null) || ContractParse.getDataString().equals(StringEx.Empty)) {
                    new AlertDialog.Builder(CyclicGoodsBalesInfo.this).setMessage("获取数据失败。").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
                } else {
                    Log.d("获取的ROWs数据：", ContractParse.jjo.toString());
                    if (ContractParse.jjo.toString().equals(null) || ContractParse.jjo.toString().equals("[[]]") || ContractParse.jjo.toString().equals(StringEx.Empty) || ContractParse.jjo.toString().equals("[]")) {
                        new AlertDialog.Builder(CyclicGoodsBalesInfo.this).setMessage("数据为空！").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
                    } else {
                        for (int i = 0; i < ContractParse.jjo.length(); i++) {
                            try {
                                JSONArray row = new JSONArray(ContractParse.jjo.getString(i));
                                HashMap<String, Object> item = new HashMap<>();
                                CyclicGoodsBalesInfo.this.kba = row.getString(0);
                                CyclicGoodsBalesInfo.this.weight = row.getString(1);
                                CyclicGoodsBalesInfo.this.info = row.getString(5);
                                CyclicGoodsBalesInfo.this.location = row.getString(4);
                                CyclicGoodsBalesInfo.this.jlfs = row.getString(3);
                                CyclicGoodsBalesInfo.this.num = row.getString(2);
                                item.put("num", new StringBuilder(String.valueOf(i + 1)).toString());
                                item.put("material", "重量：" + CyclicGoodsBalesInfo.this.weight + "吨");
                                if (CyclicGoodsBalesInfo.this.location == null) {
                                    item.put("location", StringEx.Empty);
                                } else {
                                    item.put("location", "库位：" + CyclicGoodsBalesInfo.this.location);
                                }
                                item.put("standard", "备注：" + CyclicGoodsBalesInfo.this.info);
                                CyclicGoodsBalesInfo.this.mItemArrayList.add(item);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
            CyclicGoodsBalesInfo.this.mSimpleAdater.notifyDataSetChanged();
        }
    };
    /* access modifiers changed from: private */
    public String info = StringEx.Empty;
    private boolean isLastPage = false;
    /* access modifiers changed from: private */
    public String jlfs = StringEx.Empty;
    /* access modifiers changed from: private */
    public String kba = StringEx.Empty;
    /* access modifiers changed from: private */
    public String location = StringEx.Empty;
    private RadioButton mIndexNavigation1;
    private RadioButton mIndexNavigation2;
    private RadioButton mIndexNavigation3;
    private RadioButton mIndexNavigation4;
    private RadioButton mIndexNavigation5;
    /* access modifiers changed from: private */
    public ArrayList<HashMap<String, Object>> mItemArrayList;
    public TextView mListTitleTextview;
    private ListView mListView;
    private RadioGroup mRadioderGroup;
    /* access modifiers changed from: private */
    public SimpleAdapter mSimpleAdater;
    /* access modifiers changed from: private */
    public String num = StringEx.Empty;
    private int numOfItem = 0;
    private int numOfPage = 1;
    /* access modifiers changed from: private */
    public int numOfPageGet = 1;
    private String pm = StringEx.Empty;
    private TextView pmTextView;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    private String seller = StringEx.Empty;
    private TextView sellerTextView;
    private String sl = StringEx.Empty;
    private TextView slTextView;
    /* access modifiers changed from: private */
    public String weight = StringEx.Empty;
    private String xh = StringEx.Empty;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.activity_cyclic_goods_bales_info);
        ExitApplication.getInstance().addActivity(this);
        Intent intent = getIntent();
        this.dbHelper = DataBaseFactory.getInstance(this);
        this.dbHelper.insertOperation("循环物资", "循环资源首页", "捆包信息");
        this.dj = intent.getExtras().getString("dj");
        this.seller = intent.getExtras().getString("seller");
        this.pm = intent.getExtras().getString("pm");
        this.gg = intent.getExtras().getString("gg");
        this.hth = intent.getExtras().getString("hth");
        this.xh = intent.getExtras().getString("xh");
        this.cd = intent.getExtras().getString("cd");
        this.cz = intent.getExtras().getString("cz");
        this.sl = intent.getExtras().getString("sl");
        this.djTextView = (TextView) findViewById(R.id.cyclicgoods_bales_info_textview_price1);
        this.sellerTextView = (TextView) findViewById(R.id.cyclicgoods_bales_info_textview_seller1);
        this.pmTextView = (TextView) findViewById(R.id.cyclicgoods_bales_info_textview_seller_title);
        this.ggTextView = (TextView) findViewById(R.id.cyclicgoods_bales_info_textview_standard1);
        this.cdTextView = (TextView) findViewById(R.id.cyclicgoods_bales_info_textview_field1);
        this.czTextView = (TextView) findViewById(R.id.cyclicgoods_bales_info_textview_cz);
        this.slTextView = (TextView) findViewById(R.id.cyclicgoods_bales_info_textview_sl);
        this.djTextView.setText(this.dj);
        this.sellerTextView.setText(this.seller);
        this.pmTextView.setText(this.pm);
        this.ggTextView.setText(this.gg);
        this.cdTextView.setText(this.cd);
        this.czTextView.setText("材质：" + this.cz);
        this.slTextView.setText(this.sl);
        this.mListView = (ListView) findViewById(R.id.cyclicgoods_confirm_arrival_listview_bales_info);
        this.mItemArrayList = new ArrayList<>();
        this.mRadioderGroup = (RadioGroup) findViewById(R.id.cyclic_goods_index_RGroup);
        this.mIndexNavigation1 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item1);
        this.mIndexNavigation2 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item2);
        this.mIndexNavigation3 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item3);
        this.mIndexNavigation4 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item4);
        this.mIndexNavigation5 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item5);
        this.mListTitleTextview = (TextView) findViewById(R.id.cyclicgoods_list_title_textview);
        this.mListTitleTextview.setText("捆包信息");
        SmallNumUtil.JJNum(this, (TextView) findViewById(R.id.jj_dhqrnum));
        this.mIndexNavigation1.setChecked(true);
        this.mRadioderGroup.setOnCheckedChangeListener(this);
        this.mIndexNavigation1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ExitApplication.getInstance().startActivity(CyclicGoodsBalesInfo.this, CyclicGoodsIndexActivity.class);
            }
        });
        this.mSimpleAdater = new SimpleAdapter(this, this.mItemArrayList, R.layout.activity_cyclic_goods_bales_info_item, new String[]{"num", "material", "standard", "location"}, new int[]{R.id.cyclic_goods_bals_info_num_textview, R.id.cyclic_goods_bals_info_material_textview, R.id.cyclic_goods_bals_info_standard_textview, R.id.cyclic_goods_bals_info_location_textview});
        this.mListView.setAdapter((ListAdapter) this.mSimpleAdater);
        this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
            }
        });
        getData();
    }

    private void getData() {
        RequestBidData requestBidData = new RequestBidData(this);
        requestBidData.setRequestData("捆包信息");
        requestBidData.setId(this.hth);
        requestBidData.setXh(this.xh);
        requestBidData.setPageNum(new StringBuilder(String.valueOf(this.numOfPage)).toString());
        Log.d("请求的指令：", requestBidData.infoDate());
        requestBidData.setHttpCallBack(this.httpCallBack);
        requestBidData.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.cyclicgoods_index_navigation_item2:
                this.mIndexNavigation2.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item3:
                this.mIndexNavigation3.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidSessionSelect.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item1:
                if (ObjectStores.getInst().getObject("formX") == "formX") {
                    ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class);
                    finish();
                    return;
                }
                this.mIndexNavigation1.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item4:
                this.mIndexNavigation4.setChecked(true);
                startActivity(new Intent(this, CyclicGoodsBidHallPageActivity.class));
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item5:
                this.mIndexNavigation5.setChecked(true);
                Intent intent = new Intent(this, Xhjy_more.class);
                intent.putExtra("part", "循环物资");
                startActivity(intent);
                finish();
                return;
            default:
                return;
        }
    }

    public void backAaction(View v) {
        Intent intent = new Intent(this, CyclicGoodsContractDetails.class);
        String contract1 = getIntent().getExtras().getString("contract1");
        String seller2 = getIntent().getExtras().getString("seller");
        String number = getIntent().getExtras().getString("number");
        String weight2 = getIntent().getExtras().getString("weight");
        String sum = getIntent().getExtras().getString("sum");
        String state = getIntent().getExtras().getString("state");
        intent.putExtra("contract1", contract1);
        intent.putExtra("seller", seller2);
        intent.putExtra("number", number);
        intent.putExtra("weight", weight2);
        intent.putExtra("sum", sum);
        intent.putExtra("state", state);
        startActivity(intent);
        finish();
    }

    public void onBackPressed() {
        Intent intent = new Intent(this, CyclicGoodsContractDetails.class);
        String contract1 = getIntent().getExtras().getString("contract1");
        String seller2 = getIntent().getExtras().getString("seller");
        String number = getIntent().getExtras().getString("number");
        String weight2 = getIntent().getExtras().getString("weight");
        String sum = getIntent().getExtras().getString("sum");
        String state = getIntent().getExtras().getString("state");
        intent.putExtra("contract1", contract1);
        intent.putExtra("seller", seller2);
        intent.putExtra("number", number);
        intent.putExtra("weight", weight2);
        intent.putExtra("sum", sum);
        intent.putExtra("state", state);
        startActivity(intent);
        finish();
    }
}
