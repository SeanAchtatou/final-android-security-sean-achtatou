package com.baosteelOnline.xhjy;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.R;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.main.ExitApplication;
import com.jianq.misc.StringEx;
import java.util.ArrayList;
import java.util.HashMap;

public class MessageHistory extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    private ListView MessageHistory_listview;
    private RelativeLayout bottom_nva1;
    private RelativeLayout bottom_nva2;
    /* access modifiers changed from: private */
    public SharedPreferences.Editor edit;
    private SharedPreferences loginname;
    private ArrayList<HashMap<String, Object>> mArrayList;
    public RadioButton mIndexNavigation1;
    public RadioButton mIndexNavigation2;
    public RadioButton mIndexNavigation3;
    public RadioButton mIndexNavigation4;
    public RadioButton mIndexNavigation5;
    public RadioGroup mRadioderGroup;
    private SimpleAdapter mSimpleAdapter;
    private SharedPreferences messageHistory;
    private RadioGroup radioderGroup;
    private SharedPreferences sp;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.activity_message_history);
        ExitApplication.getInstance().addActivity(this);
        this.mRadioderGroup = (RadioGroup) findViewById(R.id.cyclic_goods_index_RGroup);
        this.mIndexNavigation1 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item1);
        this.mIndexNavigation2 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item2);
        this.mIndexNavigation3 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item3);
        this.mIndexNavigation4 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item4);
        this.mIndexNavigation5 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item5);
        this.mRadioderGroup.setOnCheckedChangeListener(this);
        this.bottom_nva1 = (RelativeLayout) findViewById(R.id.bottom_nva1);
        this.bottom_nva2 = (RelativeLayout) findViewById(R.id.bottom_nva2);
        RadioButton radio_notice = (RadioButton) findViewById(R.id.radio_notice3);
        this.radioderGroup = (RadioGroup) findViewById(R.id.xhjy_index_RGroup);
        this.radioderGroup.setOnCheckedChangeListener(this);
        this.sp = getSharedPreferences("BaoIndex", 0);
        if (this.sp.getString("bIndex", StringEx.Empty).equals("xhwz")) {
            this.bottom_nva1.setVisibility(8);
            this.bottom_nva2.setVisibility(0);
            this.mIndexNavigation5.setChecked(true);
        } else {
            radio_notice.setChecked(true);
            this.bottom_nva2.setVisibility(8);
            this.bottom_nva1.setVisibility(0);
        }
        this.MessageHistory_listview = (ListView) findViewById(R.id.MessageHistory_listview);
        this.mArrayList = new ArrayList<>();
        this.mSimpleAdapter = new SimpleAdapter(this, this.mArrayList, R.layout.message_history_item, new String[]{"num", "message", "date"}, new int[]{R.id.message_history_item_num, R.id.message_history_item_message, R.id.message_history_item_date});
        this.MessageHistory_listview.setAdapter((ListAdapter) this.mSimpleAdapter);
        setData();
    }

    private void setData() {
        this.loginname = getSharedPreferences("MessageServer", 0);
        this.messageHistory = getSharedPreferences(this.loginname.getString("UserName", StringEx.Empty), 0);
        this.edit = this.messageHistory.edit();
        int nowNum = this.messageHistory.getInt("nowNum", 0);
        for (int i = nowNum; i > 0; i--) {
            HashMap<String, Object> item = new HashMap<>();
            item.put("num", new StringBuilder(String.valueOf((nowNum - i) + 1)).toString());
            item.put("message", "消息内容：" + this.messageHistory.getString(new StringBuilder(String.valueOf(i)).toString(), StringEx.Empty));
            item.put("date", "时间：" + this.messageHistory.getString(String.valueOf(i) + "date", StringEx.Empty));
            this.mArrayList.add(item);
        }
        this.mSimpleAdapter.notifyDataSetChanged();
    }

    public void clearButtonAction(View v) {
        AlertDialog.Builder myBuilder = new AlertDialog.Builder(this);
        myBuilder.setCancelable(false);
        myBuilder.setMessage("确定清空历史消息？");
        myBuilder.setNegativeButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                MessageHistory.this.edit.clear();
                MessageHistory.this.edit.commit();
                Intent intent = new Intent(MessageHistory.this, MoreMessageSetting.class);
                intent.putExtra("part", MessageHistory.this.getIntent().getExtras().getString("part"));
                MessageHistory.this.startActivity(intent);
                MessageHistory.this.finish();
            }
        });
        myBuilder.setPositiveButton("取消", (DialogInterface.OnClickListener) null);
        myBuilder.create().show();
    }

    public void backButtonAction(View v) {
        Intent intent = new Intent(this, MoreMessageSetting.class);
        intent.putExtra("part", getIntent().getExtras().getString("part"));
        startActivity(intent);
        finish();
    }

    public void onBackPressed() {
        Intent intent = new Intent(this, MoreMessageSetting.class);
        intent.putExtra("part", getIntent().getExtras().getString("part"));
        startActivity(intent);
        finish();
        super.onBackPressed();
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.cyclicgoods_index_navigation_item2:
                this.mIndexNavigation2.setChecked(true);
                startActivity(new Intent(this, CyclicGoodsBidActivity.class));
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item3:
                this.mIndexNavigation3.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidSessionSelect.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item1:
                if (ObjectStores.getInst().getObject("formX") == "formX") {
                    ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class);
                    finish();
                    return;
                }
                this.mIndexNavigation1.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item4:
                this.mIndexNavigation4.setChecked(true);
                startActivity(new Intent(this, CyclicGoodsBidHallPageActivity.class));
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item5:
                this.mIndexNavigation5.setChecked(true);
                return;
            case R.id.radio_search3:
                ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
                return;
            case R.id.radio_shopcar3:
                ((RadioButton) findViewById(R.id.radio_shopcar3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ShopActivity.class);
                return;
            case R.id.radio_home3:
                ((RadioButton) findViewById(R.id.radio_home3)).setChecked(true);
                HashMap hasmap = new HashMap();
                hasmap.put("index_more", "1");
                ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class, hasmap);
                return;
            case R.id.radio_contract3:
                ((RadioButton) findViewById(R.id.radio_contract3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ContractListActivity.class);
                return;
            case R.id.radio_notice3:
                ((RadioButton) findViewById(R.id.radio_notice3)).setChecked(true);
                return;
            default:
                return;
        }
    }
}
