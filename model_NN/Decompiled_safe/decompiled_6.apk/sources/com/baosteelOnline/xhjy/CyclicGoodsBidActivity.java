package com.baosteelOnline.xhjy;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.R;
import com.baosteelOnline.common.DataBaseFactory;
import com.baosteelOnline.data.RequestBidData;
import com.baosteelOnline.data_parse.ContractParse;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.sql.DBHelper;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;

public class CyclicGoodsBidActivity extends Activity implements RadioGroup.OnCheckedChangeListener {
    public ContractParse contractParse;
    /* access modifiers changed from: private */
    public String date = StringEx.Empty;
    private DataBaseFactory db;
    private DBHelper dbHelper;
    private JQUICallBack httpCallBack = new JQUICallBack() {
        public void callBack(ResultData result) {
            CyclicGoodsBidActivity.this.progressDialog.dismiss();
            Log.d("获取的数据：", result.getStringData());
            String dataString = result.getStringData();
            if (!dataString.equals(null) && !dataString.equals(StringEx.Empty)) {
                CyclicGoodsBidActivity.this.contractParse = ContractParse.parse(result.getStringData());
                if (ContractParse.pageNumTal.equals(null) || ContractParse.pageNumTal.equals(StringEx.Empty)) {
                    CyclicGoodsBidActivity.this.numOfPageGet = 1;
                } else {
                    CyclicGoodsBidActivity.this.numOfPageGet = Integer.parseInt(ContractParse.pageNumTal);
                }
                Log.d("总页数：", String.valueOf(CyclicGoodsBidActivity.this.numOfPageGet) + "页");
                if (ContractParse.getDataString().equals(null) || ContractParse.getDataString().equals(StringEx.Empty)) {
                    new AlertDialog.Builder(CyclicGoodsBidActivity.this).setMessage("获取数据失败").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            CyclicGoodsBidActivity.this.startActivity(new Intent(CyclicGoodsBidActivity.this, CyclicGoodsIndexActivity.class));
                            CyclicGoodsBidActivity.this.finish();
                        }
                    }).show();
                } else if (ContractParse.jjo.toString().equals(null) || ContractParse.jjo.toString().equals("[[]]") || ContractParse.jjo.toString().equals(StringEx.Empty) || ContractParse.jjo.toString().equals("[]")) {
                    new AlertDialog.Builder(CyclicGoodsBidActivity.this).setMessage("数据为空！").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
                } else {
                    for (int i = 0; i < ContractParse.jjo.length(); i++) {
                        try {
                            JSONArray row = new JSONArray(ContractParse.jjo.getString(i));
                            HashMap<String, Object> item = new HashMap<>();
                            CyclicGoodsBidActivity.this.title = row.getString(0);
                            CyclicGoodsBidActivity.this.id = row.getString(1);
                            CyclicGoodsBidActivity.this.date = row.getString(2);
                            item.put("num", new StringBuilder(String.valueOf(CyclicGoodsBidActivity.this.numOfItem + 1)).toString());
                            item.put("title", CyclicGoodsBidActivity.this.title);
                            item.put("date", CyclicGoodsBidActivity.this.date);
                            item.put("id", CyclicGoodsBidActivity.this.id);
                            item.put("next_ten", StringEx.Empty);
                            CyclicGoodsBidActivity cyclicGoodsBidActivity = CyclicGoodsBidActivity.this;
                            cyclicGoodsBidActivity.numOfItem = cyclicGoodsBidActivity.numOfItem + 1;
                            Log.d("ROW数据：", "title = " + CyclicGoodsBidActivity.this.title + "  id = " + CyclicGoodsBidActivity.this.id + " date = " + CyclicGoodsBidActivity.this.date);
                            CyclicGoodsBidActivity.this.mItemArrayList.add(item);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        String index_id = "0";
                        if (CyclicGoodsBidActivity.this.mItemArrayList.size() > 0) {
                            index_id = ((HashMap) CyclicGoodsBidActivity.this.mItemArrayList.get(0)).get("id").toString();
                        }
                        SharedPreferences.Editor editor = CyclicGoodsBidActivity.this.getSharedPreferences("welcome", 2).edit();
                        editor.putInt("index_jjgg", Integer.valueOf(index_id).intValue());
                        editor.commit();
                    }
                    HashMap<String, Object> item2 = new HashMap<>();
                    if (CyclicGoodsBidActivity.this.numOfPageGet > CyclicGoodsBidActivity.this.numOfPage) {
                        item2.put("next_ten", "下10条...");
                    } else {
                        CyclicGoodsBidActivity.this.isLastPage = true;
                        item2.put("next_ten", "已经是最后一页");
                    }
                    item2.put("background", Integer.valueOf((int) R.drawable.bg_next));
                    item2.put("num", StringEx.Empty);
                    item2.put("title", StringEx.Empty);
                    item2.put("date", StringEx.Empty);
                    item2.put("id", StringEx.Empty);
                    CyclicGoodsBidActivity.this.mItemArrayList.add(item2);
                }
            }
            CyclicGoodsBidActivity.this.mSimpleAdater.notifyDataSetChanged();
        }
    };
    /* access modifiers changed from: private */
    public String id = StringEx.Empty;
    private LayoutInflater inflater;
    /* access modifiers changed from: private */
    public boolean isLastPage = false;
    LinearLayout linearlayout;
    public RadioButton mIndexNavigation1;
    public RadioButton mIndexNavigation2;
    public RadioButton mIndexNavigation3;
    public RadioButton mIndexNavigation4;
    public RadioButton mIndexNavigation5;
    /* access modifiers changed from: private */
    public ArrayList<HashMap<String, Object>> mItemArrayList;
    public TextView mListTitleTextview;
    private ListView mListView;
    public RadioGroup mRadioderGroup;
    /* access modifiers changed from: private */
    public SimpleAdapter mSimpleAdater;
    private TextView nextView;
    /* access modifiers changed from: private */
    public int numOfItem = 0;
    /* access modifiers changed from: private */
    public int numOfPage = 1;
    /* access modifiers changed from: private */
    public int numOfPageGet = 1;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    /* access modifiers changed from: private */
    public String title = StringEx.Empty;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ExitApplication.getInstance().addActivity(this);
        requestWindowFeature(1);
        setContentView((int) R.layout.activity_cyclic_goods_bid);
        this.dbHelper = DataBaseFactory.getInstance(this);
        this.dbHelper.insertOperation("循环物资", "竞价公告", "竞价公告");
        TextView dhqrnum = (TextView) findViewById(R.id.jj_dhqrnum);
        int num = getSharedPreferences("smallnum", 2).getInt("jjnum", 0);
        dhqrnum.setGravity(17);
        dhqrnum.setText(new StringBuilder(String.valueOf(num)).toString());
        if (num != 0) {
            dhqrnum.setVisibility(0);
        } else {
            dhqrnum.setVisibility(8);
        }
        ((ImageButton) findViewById(R.id.cyclicgoods_list_title_imbt)).setBackgroundResource(R.drawable.button_home_selector);
        this.mListView = (ListView) findViewById(R.id.cyclicgoods_confirm_arrival_listview_goodsbid);
        this.mListTitleTextview = (TextView) findViewById(R.id.cyclicgoods_list_title_textview);
        this.mListTitleTextview.setText(getString(R.string.cyclic_goods_index_imbt3).toString());
        this.mRadioderGroup = (RadioGroup) findViewById(R.id.cyclic_goods_index_RGroup);
        this.mIndexNavigation1 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item1);
        this.mIndexNavigation2 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item2);
        this.mIndexNavigation3 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item3);
        this.mIndexNavigation4 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item4);
        this.mIndexNavigation5 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item5);
        this.mRadioderGroup.setOnCheckedChangeListener(this);
        this.mIndexNavigation2.setChecked(true);
        this.mItemArrayList = new ArrayList<>();
        getListData();
        this.mSimpleAdater = new SimpleAdapter(this, this.mItemArrayList, R.layout.activity_cyclic_goods_bid_item, new String[]{"num", "title", "date", "background", "next_ten"}, new int[]{R.id.bid_num_textview, R.id.bid_title_textview, R.id.bid_date_textview, R.id.bid_list_item_background_more, R.id.bid_list_next_ten_item_or_end});
        this.mListView.setAdapter((ListAdapter) this.mSimpleAdater);
        this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                if (arg2 + 1 != CyclicGoodsBidActivity.this.mItemArrayList.size()) {
                    Intent intent = new Intent(CyclicGoodsBidActivity.this, CyclicGoodsBidPageActivity.class);
                    String id = ((HashMap) CyclicGoodsBidActivity.this.mItemArrayList.get(arg2)).get("id").toString();
                    intent.putExtra("id", id);
                    intent.putExtra("activity", "竞价公告");
                    Log.d("CyclicGoodsBidActivity传过去的id：", id);
                    CyclicGoodsBidActivity.this.startActivity(intent);
                    CyclicGoodsBidActivity.this.finish();
                } else if (!CyclicGoodsBidActivity.this.isLastPage) {
                    CyclicGoodsBidActivity.this.mItemArrayList.remove(arg2);
                    CyclicGoodsBidActivity cyclicGoodsBidActivity = CyclicGoodsBidActivity.this;
                    cyclicGoodsBidActivity.numOfPage = cyclicGoodsBidActivity.numOfPage + 1;
                    CyclicGoodsBidActivity.this.getListData();
                }
            }
        });
    }

    public void getListData() {
        RequestBidData requestBidData = new RequestBidData(this);
        requestBidData.setRequestData("竞价公告列表");
        requestBidData.setPageNum(new StringBuilder(String.valueOf(this.numOfPage)).toString());
        Log.d("请求的指令：", requestBidData.infoDate());
        requestBidData.setHttpCallBack(this.httpCallBack);
        requestBidData.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.cyclicgoods_index_navigation_item2:
            default:
                return;
            case R.id.cyclicgoods_index_navigation_item3:
                this.mIndexNavigation3.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidSessionSelect.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item1:
                if (ObjectStores.getInst().getObject("formX") == "formX") {
                    ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class);
                    finish();
                    return;
                }
                this.mIndexNavigation1.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item4:
                this.mIndexNavigation4.setChecked(true);
                startActivity(new Intent(this, CyclicGoodsBidHallPageActivity.class));
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item5:
                this.mIndexNavigation5.setChecked(true);
                Intent intent = new Intent(this, Xhjy_more.class);
                intent.putExtra("part", "循环物资");
                startActivity(intent);
                finish();
                return;
        }
    }

    public void backAaction(View v) {
        if (ObjectStores.getInst().getObject("formX") != "formX") {
            ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
        } else {
            ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class);
        }
        finish();
    }

    public void onBackPressed() {
        if (ObjectStores.getInst().getObject("formX") == "formX") {
            ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class);
            finish();
            return;
        }
        ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
        finish();
    }
}
