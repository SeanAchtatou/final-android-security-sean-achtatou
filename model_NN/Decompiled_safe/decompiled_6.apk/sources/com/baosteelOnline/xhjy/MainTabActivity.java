package com.baosteelOnline.xhjy;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TabHost;
import com.baosteelOnline.R;

public class MainTabActivity extends TabActivity implements RadioGroup.OnCheckedChangeListener {
    private TabHost mHost;
    private RadioGroup radioderGroup;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.main_tabs);
        this.mHost = getTabHost();
        Bundle bundle = getIntent().getExtras();
        String str = bundle.getString("str");
        String sellername = bundle.getString("sellername");
        Intent intent_search = new Intent(this, SearchActivity.class);
        intent_search.putExtra("sellername", sellername);
        this.mHost.addTab(this.mHost.newTabSpec("ONE").setIndicator("ONE").setContent(new Intent(this, Xhjy_indexActivity.class)));
        this.mHost.addTab(this.mHost.newTabSpec("TWO").setIndicator("TWO").setContent(new Intent(this, Xhjy_indexActivity.class)));
        this.mHost.addTab(this.mHost.newTabSpec("THREE").setIndicator("THREE").setContent(new Intent(intent_search).addFlags(67108864)));
        this.mHost.addTab(this.mHost.newTabSpec("FOUR").setIndicator("FOUR").setContent(new Intent(this, ShopActivity.class)));
        this.mHost.addTab(this.mHost.newTabSpec("FIVE").setIndicator("FIVE").setContent(new Intent(this, ContractListActivity.class)));
        this.radioderGroup = (RadioGroup) findViewById(R.id.main_radio);
        this.radioderGroup.setOnCheckedChangeListener(this);
        this.mHost.setCurrentTabByTag(str);
        RadioButton radioButton2 = (RadioButton) findViewById(R.id.radio_notice);
        RadioButton radioButton3 = (RadioButton) findViewById(R.id.radio_search);
        RadioButton radioButton4 = (RadioButton) findViewById(R.id.radio_shopcar);
        RadioButton radioButton5 = (RadioButton) findViewById(R.id.radio_contract);
        if (str.equals("TWO") || str == "TWO") {
            radioButton2.setChecked(true);
        }
        if (str.equals("THREE") || str == "THREE") {
            radioButton3.setChecked(true);
        }
        if (str.equals("FOUR") || str == "FOUR") {
            radioButton4.setChecked(true);
        }
        if (str.equals("FIVE") || str == "FIVE") {
            radioButton5.setChecked(true);
        }
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.radio_home:
                startActivity(new Intent(this, Xhjy_indexActivity.class));
                return;
            case R.id.radio_notice:
                this.mHost.setCurrentTabByTag("TWO");
                return;
            case R.id.radio_search:
                this.mHost.setCurrentTabByTag("THREE");
                return;
            case R.id.radio_shopcar:
                this.mHost.setCurrentTabByTag("FOUR");
                return;
            case R.id.radio_contract:
                this.mHost.setCurrentTabByTag("FIVE");
                return;
            default:
                return;
        }
    }
}
