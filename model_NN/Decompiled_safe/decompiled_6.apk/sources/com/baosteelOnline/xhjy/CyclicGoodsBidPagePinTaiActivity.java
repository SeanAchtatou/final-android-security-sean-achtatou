package com.baosteelOnline.xhjy;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.R;
import com.baosteelOnline.common.DataBaseFactory;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.data.RequestBidData;
import com.baosteelOnline.data_parse.ContractParse;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.sql.DBHelper;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.net.JQBasicNetwork;
import com.jianq.ui.JQUICallBack;
import org.json.JSONArray;
import org.json.JSONException;

public class CyclicGoodsBidPagePinTaiActivity extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    private String activity = StringEx.Empty;
    public ContractParse contractParse;
    private DataBaseFactory db;
    private DBHelper dbHelper;
    private JQUICallBack httpCallBack = new JQUICallBack() {
        public void callBack(ResultData result) {
            CyclicGoodsBidPagePinTaiActivity.this.progressDialog.dismiss();
            Log.d("result.getStringData获取的数据：", result.getStringData());
            if (!result.getStringData().equals(null) && !result.getStringData().equals(StringEx.Empty)) {
                CyclicGoodsBidPagePinTaiActivity.this.contractParse = ContractParse.parse(result.getStringData());
                if (ContractParse.getDataString().equals(null) || ContractParse.getDataString().equals(StringEx.Empty)) {
                    new AlertDialog.Builder(CyclicGoodsBidPagePinTaiActivity.this).setMessage("获取数据失败").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
                    return;
                }
                Log.d("获取的ROWs数据：", ContractParse.jjo.toString());
                if (ContractParse.jjo.toString().equals(null) || ContractParse.jjo.toString().equals("[[]]") || ContractParse.jjo.toString().equals(StringEx.Empty) || ContractParse.jjo.toString().equals("[]")) {
                    new AlertDialog.Builder(CyclicGoodsBidPagePinTaiActivity.this).setMessage("数据为空！").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
                    return;
                }
                for (int i = 0; i < ContractParse.jjo.length(); i++) {
                    try {
                        JSONArray row = new JSONArray(ContractParse.jjo.getString(i));
                        CyclicGoodsBidPagePinTaiActivity.this.mHtmlData = row.getString(1).replaceAll("(LINE-HEIGHT: .*?);", StringEx.Empty);
                        CyclicGoodsBidPagePinTaiActivity.this.wtid = row.getString(2);
                        Log.d("获取的html数据：", CyclicGoodsBidPagePinTaiActivity.this.mHtmlData);
                        CyclicGoodsBidPagePinTaiActivity.this.mWebView.loadDataWithBaseURL(null, CyclicGoodsBidPagePinTaiActivity.this.mHtmlData, "text/html", JQBasicNetwork.UTF_8, null);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    };
    private String id = StringEx.Empty;
    /* access modifiers changed from: private */
    public String mHtmlData = StringEx.Empty;
    public RadioButton mIndexNavigation1;
    public RadioButton mIndexNavigation2;
    public RadioButton mIndexNavigation3;
    public RadioButton mIndexNavigation4;
    public RadioButton mIndexNavigation5;
    public TextView mListTitleTextview;
    public RadioGroup mRadioderGroup;
    /* access modifiers changed from: private */
    public WebView mWebView;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    /* access modifiers changed from: private */
    public String wtid = StringEx.Empty;

    /* access modifiers changed from: protected */
    @SuppressLint({"SetJavaScriptEnabled"})
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.activity_cyclic_goods_bid_page_pin_tai);
        ExitApplication.getInstance().addActivity(this);
        this.dbHelper = DataBaseFactory.getInstance(this);
        Intent intent = getIntent();
        this.wtid = intent.getExtras().getString("id");
        this.activity = intent.getExtras().getString("activity");
        Log.d("CyclicGoodsBidPageActivity收到的id：", this.id);
        Log.d("CyclicGoodsBidPageActivity收到的activity：", this.activity);
        this.mWebView = (WebView) findViewById(R.id.cyclicgoods_bid_page_webview_pin_tai);
        this.mWebView.getSettings().setBuiltInZoomControls(true);
        this.mWebView.setBackgroundColor(Color.parseColor("#ffffff"));
        this.mWebView.setFocusable(true);
        this.mWebView.requestFocus();
        this.mWebView.clearCache(true);
        this.mWebView.getSettings().setJavaScriptEnabled(true);
        this.mWebView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        this.mWebView.setWebViewClient(new HelloWebViewClient(this, null));
        this.mRadioderGroup = (RadioGroup) findViewById(R.id.cyclic_goods_index_RGroup);
        this.mIndexNavigation1 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item1);
        this.mIndexNavigation2 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item2);
        this.mIndexNavigation3 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item3);
        this.mIndexNavigation4 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item4);
        this.mIndexNavigation5 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item5);
        this.mRadioderGroup.setOnCheckedChangeListener(this);
        this.mIndexNavigation1.setChecked(true);
        this.mListTitleTextview = (TextView) findViewById(R.id.cyclicgoods_list_title_textview);
        this.mIndexNavigation1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ExitApplication.getInstance().startActivity(CyclicGoodsBidPagePinTaiActivity.this, CyclicGoodsIndexActivity.class);
                CyclicGoodsBidPagePinTaiActivity.this.finish();
            }
        });
        if (this.activity.equals("平台公告")) {
            this.dbHelper.insertOperation("循环物资", "循环资源首页", "平台公告详情");
            this.mListTitleTextview.setText("平台公告");
            this.mWebView.getSettings().setTextSize(WebSettings.TextSize.NORMAL);
        } else if (this.activity.equals("交易预告")) {
            ObjectStores.getInst().putObject("positionName", this.wtid);
            this.dbHelper.insertOperation("循环物资", "循环资源首页", "交易预告详情");
            this.mListTitleTextview.setText("交易预告");
        }
        getDate();
    }

    private class HelloWebViewClient extends WebViewClient {
        private HelloWebViewClient() {
        }

        /* synthetic */ HelloWebViewClient(CyclicGoodsBidPagePinTaiActivity cyclicGoodsBidPagePinTaiActivity, HelloWebViewClient helloWebViewClient) {
            this();
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    public void getDate() {
        RequestBidData requestBidData = new RequestBidData(this);
        requestBidData.setRequestData("竞价公告详情");
        requestBidData.setId(this.wtid);
        Log.d("请求的指令：", requestBidData.infoDate());
        requestBidData.setHttpCallBack(this.httpCallBack);
        requestBidData.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    public void onBackPressed() {
        Intent intent = new Intent(this, Xhjy_url_list.class);
        String sign = getIntent().getStringExtra("sign");
        String numOfBtn = getIntent().getExtras().getString("numOfBtn");
        intent.putExtra("sign", sign);
        intent.putExtra("numOfBtn", numOfBtn);
        startActivity(intent);
        finish();
    }

    public void backAaction(View v) {
        Intent intent = new Intent(this, Xhjy_url_list.class);
        String sign = getIntent().getStringExtra("sign");
        String numOfBtn = getIntent().getExtras().getString("numOfBtn");
        intent.putExtra("sign", sign);
        intent.putExtra("numOfBtn", numOfBtn);
        startActivity(intent);
        finish();
    }

    public void onCheckedChanged(RadioGroup arg0, int arg1) {
        switch (arg1) {
            case R.id.cyclicgoods_index_navigation_item2:
                this.mIndexNavigation2.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item3:
                this.mIndexNavigation3.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidSessionSelect.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item1:
                this.mIndexNavigation1.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item4:
                this.mIndexNavigation4.setChecked(true);
                startActivity(new Intent(this, CyclicGoodsBidHallPageActivity.class));
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item5:
                this.mIndexNavigation5.setChecked(true);
                Intent intent = new Intent(this, Xhjy_more.class);
                intent.putExtra("part", "循环物资");
                startActivity(intent);
                finish();
                return;
            default:
                return;
        }
    }
}
