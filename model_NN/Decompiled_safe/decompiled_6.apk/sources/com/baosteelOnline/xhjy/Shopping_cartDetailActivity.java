package com.baosteelOnline.xhjy;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.andframework.ui.CustomListView;
import com.andframework.ui.CustomMessageShow;
import com.baosteelOnline.R;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.data.Shop_deleteShoppingBusi;
import com.baosteelOnline.data.Shopping_cartDetailBusi;
import com.baosteelOnline.data_parse.ContractParse;
import com.baosteelOnline.data_parse.SearchParse;
import com.baosteelOnline.main.ExitApplication;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import java.util.ArrayList;
import java.util.HashMap;

public class Shopping_cartDetailActivity extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    public ArrayList<SearchRow> LisItems = new ArrayList<>();
    public ArrayList<SearchRow> LisItems2 = new ArrayList<>();
    /* access modifiers changed from: private */
    public String M_amount;
    private String M_discusstype;
    /* access modifiers changed from: private */
    public String M_piece;
    /* access modifiers changed from: private */
    public String M_sellerName;
    /* access modifiers changed from: private */
    public String M_weight;
    private TextView TV_amount;
    private TextView TV_piece;
    private TextView TV_sellerName;
    private TextView TV_weight;
    private TextView TV_wpName;
    private Button btn_delete;
    /* access modifiers changed from: private */
    public String flag;
    /* access modifiers changed from: private */
    public String gpls;
    private JQUICallBack httpCallBack1 = new JQUICallBack() {
        public void callBack(ResultData result) {
            Shopping_cartDetailActivity.this.progressDialog.dismiss();
            Shopping_cartDetailActivity.this.updateNoticeList(ContractParse.parse(result.getStringData()));
        }
    };
    /* access modifiers changed from: private */
    public JQUICallBack httpCallBack2 = new JQUICallBack() {
        public void callBack(ResultData result) {
            Shopping_cartDetailActivity.this.progressDialog.dismiss();
            Shopping_cartDetailActivity.this.updateList(SearchParse.parse(result.getStringData()));
        }
    };
    private String msg;
    CustomListView order_info_list;
    private String packIdAll = StringEx.Empty;
    /* access modifiers changed from: private */
    public int proId_num = 0;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    private RadioGroup radioderGroup;
    /* access modifiers changed from: private */
    public String status;
    public View view;
    public View view2;
    /* access modifiers changed from: private */
    public String yjdh;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        ExitApplication.getInstance().addActivity(this);
        setContentView((int) R.layout.shopping_cartdetail);
        this.order_info_list = (CustomListView) findViewById(R.id.order_info_list);
        this.TV_sellerName = (TextView) findViewById(R.id.seller_order_info);
        this.TV_piece = (TextView) findViewById(R.id.piece_order_info);
        this.TV_weight = (TextView) findViewById(R.id.zongdun_order_info);
        this.TV_amount = (TextView) findViewById(R.id.zongjine_order_info);
        ((RadioButton) findViewById(R.id.radio_shopcar3)).setChecked(true);
        this.radioderGroup = (RadioGroup) findViewById(R.id.xhjy_index_RGroup);
        this.radioderGroup.setOnCheckedChangeListener(this);
        Bundle bundle = getIntent().getExtras();
        try {
            this.flag = bundle.getString("flag");
            this.M_sellerName = bundle.getString("sellerName");
            this.status = bundle.getString("status");
            this.M_piece = bundle.getString("piece");
            this.M_weight = bundle.getString("weight");
            this.M_amount = bundle.getString("amount");
            this.gpls = bundle.getString("gpls");
            this.yjdh = bundle.getString("yjdh");
            if ("null".equals(this.yjdh)) {
                this.yjdh = StringEx.Empty;
            }
        } catch (Exception e) {
        }
        this.TV_sellerName.setText(this.M_sellerName);
        this.TV_piece.setText(this.M_piece);
        this.TV_weight.setText(this.M_weight);
        this.TV_amount.setText(this.M_amount);
        this.order_info_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                final int num = arg2 - 1;
                if (Shopping_cartDetailActivity.this.status.equals("0") || Shopping_cartDetailActivity.this.status.equals("1")) {
                    Dialog alertDialog1 = new AlertDialog.Builder(Shopping_cartDetailActivity.this).setMessage("您确定要删除此捆包吗？").setPositiveButton("删除", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            Shopping_cartDetailActivity access$0 = Shopping_cartDetailActivity.this;
                            access$0.proId_num = access$0.proId_num - 1;
                            Shop_deleteShoppingBusi deBusi = new Shop_deleteShoppingBusi(Shopping_cartDetailActivity.this);
                            deBusi.gpls = Shopping_cartDetailActivity.this.LisItems.get(num).gpls.toString();
                            deBusi.htfphm = Shopping_cartDetailActivity.this.LisItems.get(num).htfphm.toString();
                            deBusi.yjdh = Shopping_cartDetailActivity.this.yjdh;
                            deBusi.setHttpCallBack(Shopping_cartDetailActivity.this.httpCallBack2);
                            deBusi.iExecute();
                            Shopping_cartDetailActivity.this.progressDialog = ProgressDialog.show(Shopping_cartDetailActivity.this, null, "数据加载中", true, false);
                        }
                    }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).create();
                    alertDialog1.setCanceledOnTouchOutside(false);
                    alertDialog1.show();
                }
            }
        });
        testBusi();
    }

    public void testBusi() {
        Shopping_cartDetailBusi cdBusi = new Shopping_cartDetailBusi(this);
        cdBusi.gpls = this.gpls;
        cdBusi.yjdh = this.yjdh;
        cdBusi.setHttpCallBack(this.httpCallBack1);
        cdBusi.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    /* access modifiers changed from: package-private */
    public void updateList(SearchParse sp) {
        System.out.println("updateList--");
        System.out.println("proId_num:" + this.proId_num);
        this.msg = SearchParse.commonData.msg;
        System.out.println("msg--" + this.msg);
        if (this.msg.indexOf("成功") > -1 && this.proId_num == 0) {
            System.out.println("msg--2345");
            new AlertDialog.Builder(this).setMessage(" 删除成功 ").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    ExitApplication.getInstance().startActivity(Shopping_cartDetailActivity.this, ShopActivity.class, null, 1);
                }
            }).show();
        } else if (this.msg.indexOf("成功") > -1 && this.proId_num > 0) {
            new AlertDialog.Builder(this).setMessage(" 删除成功 ").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    Shopping_cartDetailActivity.this.order_info_list.removeAllView();
                    Shopping_cartDetailActivity.this.LisItems.removeAll(Shopping_cartDetailActivity.this.LisItems);
                    HashMap hash = new HashMap();
                    hash.put("sellerName", Shopping_cartDetailActivity.this.M_sellerName);
                    hash.put("piece", Shopping_cartDetailActivity.this.M_piece);
                    hash.put("weight", Shopping_cartDetailActivity.this.M_weight);
                    hash.put("amount", Shopping_cartDetailActivity.this.M_amount);
                    hash.put("gpls", Shopping_cartDetailActivity.this.gpls);
                    hash.put("yjdh", Shopping_cartDetailActivity.this.yjdh);
                    hash.put("flag", Shopping_cartDetailActivity.this.flag);
                    hash.put("status", Shopping_cartDetailActivity.this.status);
                    ExitApplication.getInstance().startActivity(Shopping_cartDetailActivity.this, Shopping_cartDetailActivity.class, hash, 1);
                }
            }).show();
        }
    }

    /* access modifiers changed from: package-private */
    public void updateNoticeList(ContractParse contractParse) {
        if (ContractParse.commonData == null || ContractParse.commonData.blocks == null || ContractParse.commonData.blocks.r0 == null || ContractParse.commonData.blocks.r0.mar == null || ContractParse.commonData.blocks.r0.mar.rows == null) {
            CustomMessageShow.getInst().showLongToast(this, "服务器无数据返回！");
        } else if (ContractParse.commonData != null && ContractParse.commonData.blocks != null && ContractParse.commonData.blocks.r0 != null && ContractParse.commonData.blocks.r0.mar != null && ContractParse.commonData.blocks.r0.mar.rows != null && ContractParse.commonData.blocks.r0.mar.rows.rows != null) {
            this.proId_num = ContractParse.commonData.blocks.r0.mar.rows.rows.size();
            for (int i = 0; i < ContractParse.commonData.blocks.r0.mar.rows.rows.size(); i++) {
                ArrayList<String> rowdata = ContractParse.commonData.blocks.r0.mar.rows.rows.get(i);
                SearchRow sr = new SearchRow();
                if (rowdata != null) {
                    for (int k = 0; k < rowdata.size(); k++) {
                        if (k == 0) {
                            sr.kbh = String.valueOf(String.valueOf(i + 1)) + ".捆包号:" + ((String) rowdata.get(k));
                            sr.kbh2 = (String) rowdata.get(k);
                        } else if (k == 1) {
                            sr.piece = (String) rowdata.get(k);
                        } else if (k == 2) {
                            sr.weiht = (String) rowdata.get(k);
                        } else if (k == 3) {
                            sr.jlfs = (String) rowdata.get(k);
                        } else if (k == 4) {
                            sr.gpls = (String) rowdata.get(k);
                        } else if (k == 5) {
                            sr.htfphm = (String) rowdata.get(k);
                        } else if (k == 6) {
                            sr.wprovider = (String) rowdata.get(k);
                        } else if (k == 7) {
                            sr.isct = (String) rowdata.get(k);
                        } else if (k == 8) {
                            sr.bz = (String) rowdata.get(k);
                            if ("null".equals(sr.bz)) {
                                sr.bz = StringEx.Empty;
                            }
                        }
                    }
                    this.LisItems.add(sr);
                    this.view2 = buildRowView(sr);
                    this.order_info_list.addViewToLast(this.view2);
                    rowdata.clear();
                }
            }
            this.order_info_list.onRefreshComplete();
        }
    }

    private View buildRowView(SearchRow sr) {
        this.view = View.inflate(this, R.layout.shopping_cartdetail_row, null);
        ((TextView) this.view.findViewById(R.id.order_list_kbh)).setText(sr.kbh);
        ((TextView) this.view.findViewById(R.id.order_list_numberunit)).setText(String.valueOf(sr.piece) + "件/" + sr.weiht + "吨");
        ((TextView) this.view.findViewById(R.id.order_list_ck)).setText("仓库:" + sr.wprovider);
        ((TextView) this.view.findViewById(R.id.order_list_bz)).setText("备注:" + sr.bz);
        ((TextView) this.view.findViewById(R.id.order_list_jlfs)).setText("计量方式:" + sr.jlfs);
        ImageView IV_del = (ImageView) this.view.findViewById(R.id.order_list_delete);
        if (!this.status.equals("0") && !this.status.equals("1")) {
            IV_del.setVisibility(8);
        }
        return this.view;
    }

    class SearchRow {
        public String bz;
        public String gpls;
        public String htfphm;
        public String isct;
        public String jlfs;
        public String kbh;
        public String kbh2;
        public String piece;
        public String weiht;
        public String wprovider;

        SearchRow() {
        }
    }

    public void order_info_backAction(View v) {
        ExitApplication.getInstance().back(0);
    }

    public void onBackPressed() {
        super.onBackPressed();
        ExitApplication.getInstance().back(0);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.view = null;
        this.view2 = null;
        this.order_info_list = null;
        this.LisItems = null;
        this.LisItems2 = null;
        this.TV_sellerName = null;
        this.TV_wpName = null;
        this.TV_amount = null;
        this.TV_weight = null;
        this.TV_piece = null;
        this.packIdAll = null;
        this.msg = null;
        this.M_sellerName = null;
        this.M_piece = null;
        this.M_weight = null;
        this.M_amount = null;
        this.M_discusstype = null;
        this.btn_delete = null;
        super.onDestroy();
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.radio_search3:
                ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
                return;
            case R.id.radio_shopcar3:
                ((RadioButton) findViewById(R.id.radio_shopcar3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ShopActivity.class);
                return;
            case R.id.radio_home3:
                ((RadioButton) findViewById(R.id.radio_home3)).setChecked(true);
                HashMap hasmap = new HashMap();
                hasmap.put("index_more", "1");
                ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class, hasmap);
                return;
            case R.id.radio_contract3:
                ((RadioButton) findViewById(R.id.radio_contract3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ContractListActivity.class);
                return;
            case R.id.radio_notice3:
                ((RadioButton) findViewById(R.id.radio_notice3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, Xhjy_more.class);
                return;
            default:
                return;
        }
    }
}
