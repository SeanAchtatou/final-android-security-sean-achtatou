package com.baosteelOnline.xhjy;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.andframework.ui.CustomListView;
import com.andframework.ui.CustomMessageShow;
import com.baosteelOnline.R;
import com.baosteelOnline.common.DataBaseFactory;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.data.ContractBusi;
import com.baosteelOnline.data_parse.ContractParse;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.model.SmallNumUtil;
import com.baosteelOnline.sql.DBHelper;
import com.jianq.common.ResultData;
import com.jianq.ui.JQUICallBack;
import java.util.ArrayList;
import java.util.HashMap;

public class ContractActivity extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    /* access modifiers changed from: private */
    public ArrayList<SearchRow> LisItems = new ArrayList<>();
    private TextView TV_company;
    private TextView TV_mertial;
    private TextView TV_piece;
    private TextView TV_price;
    private TextView TV_wproviderName;
    private String contractid;
    CustomListView contractrowslist;
    private DataBaseFactory db;
    private DBHelper dbHelper;
    private JQUICallBack httpCallBack = new JQUICallBack() {
        public void callBack(ResultData result) {
            ContractActivity.this.progressDialog.dismiss();
            System.out.println("result.getStringData()====>" + result.getStringData());
            ContractActivity.this.updateNoticeList(ContractParse.parse(result.getStringData()));
        }
    };
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    private RadioGroup radioderGroup;
    /* access modifiers changed from: private */
    public String sellername;
    private Boolean stats = false;
    private View view;
    private View view2;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ExitApplication.getInstance().addActivity(this);
        requestWindowFeature(1);
        setContentView((int) R.layout.xhjy_contract);
        this.dbHelper = DataBaseFactory.getInstance(this);
        this.dbHelper.insertOperation("钢材交易", "买家中心", "合同详情列表");
        this.contractrowslist = (CustomListView) findViewById(R.id.contractrowslist);
        this.TV_company = (TextView) findViewById(R.id.seller_contract);
        this.TV_wproviderName = (TextView) findViewById(R.id.store_contract);
        this.TV_piece = (TextView) findViewById(R.id.piece_contract);
        this.TV_mertial = (TextView) findViewById(R.id.zongdun_contract);
        this.TV_price = (TextView) findViewById(R.id.zongjine_contract);
        ((RadioButton) findViewById(R.id.radio_contract3)).setChecked(true);
        SmallNumUtil.GWCNum(this, (TextView) findViewById(R.id.gwc_dhqrnum));
        this.radioderGroup = (RadioGroup) findViewById(R.id.xhjy_index_RGroup);
        this.radioderGroup.setOnCheckedChangeListener(this);
        Bundle bundle = getIntent().getExtras();
        try {
            this.sellername = bundle.getString("company");
            this.contractid = bundle.getString("contractid");
            this.TV_company.setText("卖家:" + this.sellername);
            this.TV_piece.setText(bundle.getString("piece"));
            this.TV_mertial.setText(bundle.getString("mertial"));
            this.TV_price.setText(bundle.getString("price"));
        } catch (Exception e) {
        }
        this.contractrowslist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                int num = arg2 - 1;
                String productname = ((SearchRow) ContractActivity.this.LisItems.get(num)).productname2.toString();
                String mertial = ((SearchRow) ContractActivity.this.LisItems.get(num)).mertial.toString();
                String format = ((SearchRow) ContractActivity.this.LisItems.get(num)).format.toString();
                String price = ((SearchRow) ContractActivity.this.LisItems.get(num)).price.toString();
                String piece = ((SearchRow) ContractActivity.this.LisItems.get(num)).piece.toString();
                String desunit = ((SearchRow) ContractActivity.this.LisItems.get(num)).dun.toString();
                String productaddr = ((SearchRow) ContractActivity.this.LisItems.get(num)).productaddr.toString();
                String hth = ((SearchRow) ContractActivity.this.LisItems.get(num)).contractid.toString();
                String xh = ((SearchRow) ContractActivity.this.LisItems.get(num)).xh.toString();
                String wproviderId = ((SearchRow) ContractActivity.this.LisItems.get(num)).wproviderId.toString();
                System.out.println("productname:" + productname);
                System.out.println("mertial:" + mertial);
                System.out.println("format:" + format);
                System.out.println("price:" + price);
                System.out.println("piece:" + piece);
                System.out.println("desunit:" + desunit);
                System.out.println("productaddr:" + productaddr);
                System.out.println("hth:" + hth);
                System.out.println("xh:" + xh);
                System.out.println("wproviderId:" + wproviderId);
                System.out.println("sellername:" + ContractActivity.this.sellername);
                HashMap hasmap = new HashMap();
                hasmap.put("sellername", ContractActivity.this.sellername);
                hasmap.put("productname", productname);
                hasmap.put("mertial", mertial);
                hasmap.put("format", format);
                hasmap.put("price", price);
                hasmap.put("piece", piece);
                hasmap.put("desunit", desunit);
                hasmap.put("productaddr", productaddr);
                hasmap.put("wproviderId", wproviderId);
                hasmap.put("hth", hth);
                hasmap.put("xh", xh);
                ExitApplication.getInstance().startActivity(ContractActivity.this, Contract_bind_infoActivity.class, hasmap);
            }
        });
        testBusi();
    }

    public void testBusi() {
        ContractBusi contractBusi = new ContractBusi(this);
        contractBusi.contractid = this.contractid;
        contractBusi.setHttpCallBack(this.httpCallBack);
        contractBusi.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    /* access modifiers changed from: package-private */
    public void updateNoticeList(ContractParse contractParse) {
        if (ContractParse.commonData == null) {
            CustomMessageShow.getInst().showLongToast(this, "服务器无数据返回！");
        } else if (ContractParse.commonData.blocks.r0.mar.rows.rows.size() == 0) {
            new AlertDialog.Builder(this).setMessage("对不起，没有数据!").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    Intent intent = new Intent();
                    intent.setClass(ContractActivity.this.getApplicationContext(), ContractListActivity.class);
                    ContractActivity.this.startActivity(intent);
                }
            }).show().setCanceledOnTouchOutside(false);
        } else if (ContractParse.commonData != null && ContractParse.commonData.blocks != null && ContractParse.commonData.blocks.r0 != null && ContractParse.commonData.blocks.r0.mar != null && ContractParse.commonData.blocks.r0.mar.rows != null && ContractParse.commonData.blocks.r0.mar.rows.rows != null) {
            for (int i = 0; i < ContractParse.commonData.blocks.r0.mar.rows.rows.size(); i++) {
                ArrayList<String> rowdata = ContractParse.commonData.blocks.r0.mar.rows.rows.get(i);
                SearchRow sr = new SearchRow();
                if (rowdata != null) {
                    System.out.println("rowdata.size:" + rowdata.size());
                    for (int k = 0; k < rowdata.size(); k++) {
                        if (k == 0) {
                            sr.productname = String.valueOf(String.valueOf(i + 1)) + "." + ((String) rowdata.get(k));
                            sr.productname2 = (String) rowdata.get(k);
                        } else if (k == 1) {
                            sr.mertial = (String) rowdata.get(k);
                        } else if (k == 2) {
                            sr.format = (String) rowdata.get(k);
                        } else if (k == 3) {
                            sr.piece = (String) rowdata.get(k);
                        } else if (k == 4) {
                            sr.dun = (String) rowdata.get(k);
                        } else if (k == 5) {
                            sr.contractid = (String) rowdata.get(k);
                        } else if (k == 6) {
                            sr.xh = (String) rowdata.get(k);
                        } else if (k == 7) {
                            sr.price = (String) rowdata.get(k);
                        } else if (k == 8) {
                            sr.wproviderId = (String) rowdata.get(k);
                        } else if (k == 9) {
                            sr.productaddr = (String) rowdata.get(k);
                        }
                    }
                    this.LisItems.add(sr);
                    this.view2 = buildRowView(sr);
                    this.contractrowslist.addViewToLast(this.view2);
                    rowdata.removeAll(rowdata);
                }
            }
            if (this.LisItems.size() > 0) {
                this.TV_wproviderName.setText("仓库:" + this.LisItems.get(0).wproviderId);
            }
            this.contractrowslist.onRefreshComplete();
        }
    }

    private View buildRowView(SearchRow sr) {
        this.view = View.inflate(this, R.layout.xhjy_contract_row, null);
        ((TextView) this.view.findViewById(R.id.productname)).setText(sr.productname);
        ((TextView) this.view.findViewById(R.id.material)).setText("材质:" + sr.mertial);
        ((TextView) this.view.findViewById(R.id.formatname)).setText("规格:" + sr.format);
        ((TextView) this.view.findViewById(R.id.price)).setText(sr.price);
        ((TextView) this.view.findViewById(R.id.numberunit)).setText(String.valueOf(sr.piece) + "件/" + sr.dun + "吨");
        ((TextView) this.view.findViewById(R.id.addressname)).setText("产地:" + sr.productaddr);
        return this.view;
    }

    class SearchRow {
        public String contractid;
        public String dun;
        public String format;
        public String mertial;
        public String piece;
        public String price;
        public String productaddr;
        public String productname;
        public String productname2;
        public String wproviderId;
        public String xh;

        SearchRow() {
        }
    }

    public void backButtonAction(View v) {
        ExitApplication.getInstance().back(0);
    }

    public void onBackPressed() {
        super.onBackPressed();
        ExitApplication.getInstance().back(0);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.contractrowslist = null;
        this.view = null;
        this.view2 = null;
        this.LisItems = null;
        this.TV_company = null;
        this.TV_wproviderName = null;
        this.TV_piece = null;
        this.TV_mertial = null;
        this.TV_price = null;
        this.contractid = null;
        this.sellername = null;
        super.onDestroy();
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.radio_search3:
                ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
                return;
            case R.id.radio_shopcar3:
                ((RadioButton) findViewById(R.id.radio_shopcar3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ShopActivity.class);
                return;
            case R.id.radio_home3:
                ((RadioButton) findViewById(R.id.radio_home3)).setChecked(true);
                HashMap hasmap = new HashMap();
                hasmap.put("index_more", "1");
                ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class, hasmap);
                return;
            case R.id.radio_contract3:
                ((RadioButton) findViewById(R.id.radio_contract3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ContractListActivity.class);
                return;
            case R.id.radio_notice3:
                ((RadioButton) findViewById(R.id.radio_notice3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, Xhjy_more.class);
                return;
            default:
                return;
        }
    }
}
