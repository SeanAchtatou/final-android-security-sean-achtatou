package com.baosteelOnline.xhjy;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.andframework.common.ObjectStores;
import com.andframework.ui.CustomListView;
import com.andframework.ui.CustomMessageShow;
import com.baosteelOnline.R;
import com.baosteelOnline.common.DataBaseFactory;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.data.Choose_productsBusi;
import com.baosteelOnline.data_parse.ContractParse;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.sql.DBHelper;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Choose_product_twoActivity extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    private DataBaseFactory db;
    private DBHelper dbHelper;
    private JQUICallBack httpCallBack = new JQUICallBack() {
        public void callBack(ResultData result) {
            Choose_product_twoActivity.this.progressDialog.dismiss();
            System.out.println("result.getStringData()" + result.getStringData());
            Choose_product_twoActivity.this.updateNoticeList(ContractParse.parse(result.getStringData()));
        }
    };
    List<String> list = new ArrayList();
    CustomListView product_twolist;
    private String productname;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    private RadioGroup radioderGroup;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        ExitApplication.getInstance().addActivity(this);
        setContentView((int) R.layout.xhjy_choose_product_two);
        this.dbHelper = DataBaseFactory.getInstance(this);
        this.dbHelper.insertOperation("钢材交易", "搜索", "二级品种搜索页面");
        this.product_twolist = (CustomListView) findViewById(R.id.product_twolist);
        ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
        this.radioderGroup = (RadioGroup) findViewById(R.id.xhjy_index_RGroup);
        this.radioderGroup.setOnCheckedChangeListener(this);
        this.productname = getIntent().getStringExtra("productname");
        this.product_twolist.setPageSize(100);
        this.product_twolist.setClickable(true);
        this.product_twolist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View v, int arg2, long arg3) {
                ObjectStores.getInst().putObject("product_sign", Choose_product_twoActivity.this.list.get(arg2 - 1).toString());
                ExitApplication.getInstance().startActivity(Choose_product_twoActivity.this, ChooseActivity.class);
            }
        });
        testBusi();
    }

    public void testBusi() {
        Choose_productsBusi cmBusi = new Choose_productsBusi(this);
        cmBusi.pm = this.productname;
        cmBusi.setHttpCallBack(this.httpCallBack);
        cmBusi.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    /* access modifiers changed from: package-private */
    public void updateNoticeList(ContractParse shParse) {
        if (ContractParse.commonData == null) {
            CustomMessageShow.getInst().showLongToast(this, "服务器无数据返回！");
            return;
        }
        if (ContractParse.commonData.blocks.r0.mar.rows.rows.size() == 0) {
            new AlertDialog.Builder(this).setMessage("对不起，没有数据!").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    ExitApplication.getInstance().back(0);
                }
            }).show().setCanceledOnTouchOutside(false);
        } else if (!(ContractParse.commonData == null || ContractParse.commonData.blocks == null || ContractParse.commonData.blocks.r0 == null || ContractParse.commonData.blocks.r0.mar == null || ContractParse.commonData.blocks.r0.mar.rows == null || ContractParse.commonData.blocks.r0.mar.rows.rows == null)) {
            for (int i = 0; i < ContractParse.commonData.blocks.r0.mar.rows.rows.size(); i++) {
                ArrayList<String> rowdata = ContractParse.commonData.blocks.r0.mar.rows.rows.get(i);
                if (rowdata != null) {
                    for (int k = 0; k < rowdata.size(); k++) {
                        if (k == 0) {
                            this.list.add((String) rowdata.get(k));
                        }
                    }
                }
            }
        }
        for (int i2 = 0; i2 < this.list.size(); i2++) {
            String str1 = this.list.get(i2);
            int j = i2 + 1;
            while (j < this.list.size()) {
                if (str1.equals(this.list.get(j))) {
                    this.list.remove(j);
                    j--;
                }
                j++;
            }
        }
        showView();
    }

    /* access modifiers changed from: package-private */
    public void showView() {
        int len = this.list.size();
        for (int i = 0; i < len; i++) {
            this.product_twolist.addViewToLast(buildRowView(this.list.get(i)));
        }
        this.product_twolist.onRefreshComplete();
    }

    private View buildRowView(String sr) {
        View view = View.inflate(this, R.layout.xhjy_choose_product_two_row, null);
        ((TextView) view.findViewById(R.id.product_two_name)).setText(sr);
        return view;
    }

    public void xhjy_choose_back_ButtonAction(View v) {
        ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class);
    }

    public void onBackPressed() {
        ExitApplication.getInstance().back(0);
        super.onBackPressed();
    }

    public void xhjy_choose_finish_ButtonAction(View v) {
        ExitApplication.getInstance().startActivity(this, SearchActivity.class);
    }

    /* access modifiers changed from: package-private */
    public void Golbe() {
        ObjectStores.getInst().putObject("length_sign", "0");
        ObjectStores.getInst().putObject("price_sign", "0");
        ObjectStores.getInst().putObject("thickness_sign", "0");
        ObjectStores.getInst().putObject("width_sign", "0");
        ObjectStores.getInst().putObject("thickmin", StringEx.Empty);
        ObjectStores.getInst().putObject("thickmax", StringEx.Empty);
        ObjectStores.getInst().putObject("widemin", StringEx.Empty);
        ObjectStores.getInst().putObject("widemax", StringEx.Empty);
        ObjectStores.getInst().putObject("lengthmin", StringEx.Empty);
        ObjectStores.getInst().putObject("lengthmax", StringEx.Empty);
        ObjectStores.getInst().putObject("pricemin", StringEx.Empty);
        ObjectStores.getInst().putObject("pricemax", StringEx.Empty);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.productname = null;
        this.list = null;
        this.product_twolist = null;
        super.onDestroy();
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.radio_search3:
                ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
                return;
            case R.id.radio_shopcar3:
                ((RadioButton) findViewById(R.id.radio_shopcar3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ShopActivity.class);
                return;
            case R.id.radio_home3:
                ((RadioButton) findViewById(R.id.radio_home3)).setChecked(true);
                HashMap hasmap = new HashMap();
                hasmap.put("index_more", "1");
                ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class, hasmap);
                return;
            case R.id.radio_contract3:
                ((RadioButton) findViewById(R.id.radio_contract3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ContractListActivity.class);
                return;
            case R.id.radio_notice3:
                ((RadioButton) findViewById(R.id.radio_notice3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, Xhjy_more.class);
                return;
            default:
                return;
        }
    }
}
