package com.baosteelOnline.xhjy;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.R;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.model.SmallNumUtil;
import com.jianq.misc.StringEx;
import java.util.HashMap;

public class Choose_filter_width extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    private Button btn_ok;
    /* access modifiers changed from: private */
    public int longmax = 0;
    /* access modifiers changed from: private */
    public int longmin = 0;
    private RadioGroup radioderGroup;
    private TextView titlleText;
    /* access modifiers changed from: private */
    public EditText wMax;
    /* access modifiers changed from: private */
    public EditText wMin;
    /* access modifiers changed from: private */
    public String widthMax;
    /* access modifiers changed from: private */
    public String widthMin;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        ExitApplication.getInstance().addActivity(this);
        setContentView((int) R.layout.xhjy_choose_filter_width);
        this.titlleText = (TextView) findViewById(R.id.TV_titletext_history_length);
        this.titlleText.setText("搜索宽度");
        this.wMin = (EditText) findViewById(R.id.choose_filter_widthMin);
        this.wMax = (EditText) findViewById(R.id.choose_filter_width8Max);
        ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
        SmallNumUtil.GWCNum(this, (TextView) findViewById(R.id.gwc_dhqrnum));
        this.radioderGroup = (RadioGroup) findViewById(R.id.xhjy_index_RGroup);
        this.radioderGroup.setOnCheckedChangeListener(this);
        this.btn_ok = (Button) findViewById(R.id.choose_filter_width_ok);
        this.btn_ok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Choose_filter_width.this.widthMin = Choose_filter_width.this.wMin.getText().toString();
                Choose_filter_width.this.widthMax = Choose_filter_width.this.wMax.getText().toString();
                try {
                    if (!Choose_filter_width.this.widthMin.equals(StringEx.Empty)) {
                        Choose_filter_width.this.longmin = Integer.valueOf(Choose_filter_width.this.widthMin).intValue();
                    }
                    if (!Choose_filter_width.this.widthMax.equals(StringEx.Empty)) {
                        Choose_filter_width.this.longmax = Integer.valueOf(Choose_filter_width.this.widthMax).intValue();
                    }
                } catch (Exception e) {
                }
                if (Choose_filter_width.this.widthMin.equals(StringEx.Empty) && Choose_filter_width.this.widthMax.equals(StringEx.Empty)) {
                    new AlertDialog.Builder(Choose_filter_width.this).setMessage("输入的最小值必须比最大值小！").setPositiveButton("确认", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                        }
                    }).show();
                } else if (Choose_filter_width.this.longmin > Choose_filter_width.this.longmax) {
                    new AlertDialog.Builder(Choose_filter_width.this).setMessage("输入的最小值必须比最大值小！").setPositiveButton("确认", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                        }
                    }).show();
                } else {
                    ObjectStores.getInst().putObject("width_sign", "11");
                    ObjectStores.getInst().putObject("widemin", Choose_filter_width.this.widthMin);
                    ObjectStores.getInst().putObject("widemax", Choose_filter_width.this.widthMax);
                    ExitApplication.getInstance().startActivity(Choose_filter_width.this, ChooseActivity.class);
                }
            }
        });
    }

    public void xhjy_choose_back_ButtonAction(View v) {
        ExitApplication.getInstance().back(0);
    }

    public void onBackPressed() {
        ExitApplication.getInstance().back(0);
        super.onBackPressed();
    }

    public void choose_filter_all_widthAction(View v) {
        ObjectStores.getInst().putObject("width_sign", "00");
        ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
    }

    public void choose_filter_all_width14Action(View v) {
        ObjectStores.getInst().putObject("width_sign", "01");
        ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
    }

    public void choose_filter_all_width46Action(View v) {
        ObjectStores.getInst().putObject("width_sign", "02");
        ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
    }

    public void choose_filter_all_width68Action(View v) {
        ObjectStores.getInst().putObject("width_sign", "03");
        ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
    }

    public void choose_filter_all_width8Action(View v) {
        ObjectStores.getInst().putObject("width_sign", "04");
        ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
    }

    public void choose_filter_all_width9Action(View v) {
        ObjectStores.getInst().putObject("width_sign", "05");
        ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.wMin = null;
        this.wMax = null;
        this.btn_ok = null;
        this.widthMin = null;
        this.widthMax = null;
        this.longmin = 0;
        this.longmax = 0;
        super.onDestroy();
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.radio_search3:
                ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
                return;
            case R.id.radio_shopcar3:
                ((RadioButton) findViewById(R.id.radio_shopcar3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ShopActivity.class);
                return;
            case R.id.radio_home3:
                ((RadioButton) findViewById(R.id.radio_home3)).setChecked(true);
                HashMap hasmap = new HashMap();
                hasmap.put("index_more", "1");
                ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class, hasmap);
                return;
            case R.id.radio_contract3:
                ((RadioButton) findViewById(R.id.radio_contract3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ContractListActivity.class);
                return;
            case R.id.radio_notice3:
                ((RadioButton) findViewById(R.id.radio_notice3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, Xhjy_more.class);
                return;
            default:
                return;
        }
    }
}
