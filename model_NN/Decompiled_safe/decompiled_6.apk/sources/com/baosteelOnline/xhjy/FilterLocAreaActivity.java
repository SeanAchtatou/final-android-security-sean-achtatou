package com.baosteelOnline.xhjy;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RadioButton;
import android.widget.TextView;
import com.andframework.common.ObjectStores;
import com.andframework.ui.CustomListView;
import com.andframework.ui.CustomMessageShow;
import com.baosteelOnline.R;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.data.FilterLocAreaBusi;
import com.baosteelOnline.data_parse.SearchParse;
import com.baosteelOnline.main.ExitApplication;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import java.util.ArrayList;

public class FilterLocAreaActivity extends JQActivity {
    /* access modifiers changed from: private */
    public String Area = StringEx.Empty;
    /* access modifiers changed from: private */
    public ArrayList<SearchRow> LisItems = new ArrayList<>();
    private JQUICallBack httpCallBack = new JQUICallBack() {
        public void callBack(ResultData result) {
            FilterLocAreaActivity.this.progressDialog.dismiss();
            try {
                System.out.println("result=StringData13=>" + result.getStringData());
                FilterLocAreaActivity.this.updateNoticeList(SearchParse.parse(result.getStringData()));
            } catch (Exception e) {
            }
        }
    };
    private String methodName = StringEx.Empty;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    CustomListView rowslist_area;
    private View view;
    private View view2;
    private String who = StringEx.Empty;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.xhjy_filterlocarea);
        ExitApplication.getInstance().addActivity(this);
        initializeView();
        testBusi();
    }

    public void testBusi() {
        FilterLocAreaBusi areaBusi = new FilterLocAreaBusi(this);
        areaBusi.methodName = this.methodName;
        areaBusi.iExecute();
        areaBusi.setHttpCallBack(this.httpCallBack);
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    /* access modifiers changed from: package-private */
    public void initializeView() {
        this.rowslist_area = (CustomListView) findViewById(R.id.rowslist_area);
        this.rowslist_area.setClickable(true);
        this.rowslist_area.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View v, int arg2, long arg3) {
                FilterLocAreaActivity.this.Area = ((SearchRow) FilterLocAreaActivity.this.LisItems.get(arg2 - 1)).areaname.toString();
                Intent intent = new Intent(FilterLocAreaActivity.this.getApplicationContext(), SearchActivity.class);
                if (ObjectStores.getInst().getObject("who_for_me").toString() == "所在地") {
                    ObjectStores.getInst().putObject("locArea", FilterLocAreaActivity.this.Area);
                    ObjectStores.getInst().putObject("searchbody", "Area");
                } else if (ObjectStores.getInst().getObject("who_for_me").toString() == "质量等级") {
                    ObjectStores.getInst().putObject("qualityGrade", FilterLocAreaActivity.this.Area);
                    ObjectStores.getInst().putObject("searchbody", "qualityGrade");
                }
                FilterLocAreaActivity.this.startActivity(intent);
                FilterLocAreaActivity.this.finish();
            }
        });
        ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
        try {
            this.methodName = getIntent().getExtras().getString("methodName");
        } catch (Exception e) {
        }
    }

    public void xhjy_area_filterButtonAction(View v) {
        Intent intent = new Intent(this, ChooseActivity.class);
        intent.putExtra("who", "search");
        startActivity(intent);
        finish();
    }

    public void xhjy_area_levelButtonAction(View v) {
        ObjectStores.getInst().putObject("who_for_me", "质量等级");
        Intent intent = new Intent(this, FilterLocAreaActivity.class);
        intent.putExtra("methodName", "queryQualityGrade");
        startActivity(intent);
        finish();
    }

    public void xhjy_area_addressButtonAction(View v) {
        ObjectStores.getInst().putObject("who_for_me", "所在地");
        Intent intent = new Intent(this, FilterLocAreaActivity.class);
        intent.putExtra("methodName", "queryLocArea");
        startActivity(intent);
        finish();
    }

    public void xhjy_area_backButtonAction(View v) {
        finish();
    }

    /* access modifiers changed from: package-private */
    public void updateNoticeList(SearchParse searchParse) {
        if (SearchParse.commonData == null || SearchParse.commonData.blocks == null || SearchParse.commonData.blocks.r0 == null || SearchParse.commonData.blocks.r0.mar == null || SearchParse.commonData.blocks.r0.mar.rows == null) {
            CustomMessageShow.getInst().showLongToast(this, "服务器无数据返回！");
        } else if (SearchParse.commonData.blocks.r0.mar.rows.rows.size() == 0) {
            new AlertDialog.Builder(this).setMessage("对不起，没有数据!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    Intent intent = new Intent();
                    intent.setClass(FilterLocAreaActivity.this.getApplicationContext(), SearchActivity.class);
                    FilterLocAreaActivity.this.startActivity(intent);
                    FilterLocAreaActivity.this.finish();
                }
            }).show();
        } else if (SearchParse.commonData != null && SearchParse.commonData.blocks != null && SearchParse.commonData.blocks.r0 != null && SearchParse.commonData.blocks.r0.mar != null && SearchParse.commonData.blocks.r0.mar.rows != null && SearchParse.commonData.blocks.r0.mar.rows.rows != null) {
            for (int i = 0; i < SearchParse.commonData.blocks.r0.mar.rows.rows.size(); i++) {
                ArrayList<String> rowdata = SearchParse.commonData.blocks.r0.mar.rows.rows.get(i);
                SearchRow sr = new SearchRow();
                if (rowdata != null) {
                    for (int k = 0; k < rowdata.size(); k++) {
                        if (k == 0) {
                            sr.areaname = (String) rowdata.get(k);
                        }
                    }
                    this.LisItems.add(sr);
                    this.view2 = buildRowView(sr);
                    this.rowslist_area.addViewToLast(this.view2);
                    rowdata.removeAll(rowdata);
                }
            }
            this.rowslist_area.onRefreshComplete();
        }
    }

    private View buildRowView(SearchRow sr) {
        this.view = View.inflate(this, R.layout.xhjy_filterlocarea_row, null);
        ((TextView) this.view.findViewById(R.id.TV_area_xhjy)).setText(sr.areaname);
        return this.view;
    }

    class SearchRow {
        public String areaname;

        SearchRow() {
        }
    }

    public void main_home_buttonaction(View v) {
        startActivity(new Intent(this, Xhjy_indexActivity.class));
        finish();
    }

    public void xhjy_home_buttonaction(View v) {
        startActivity(new Intent(this, Xhjy_indexActivity.class));
        finish();
    }

    public void xhjy_search_buttonaction(View v) {
        startActivity(new Intent(this, SearchHistoryActivity.class));
        finish();
    }

    public void xhjy_shop_buttonaction(View v) {
        startActivity(new Intent(this, ShopActivity.class));
        finish();
    }

    public void xhjy_contract_buttonaction(View v) {
        startActivity(new Intent(this, ContractListActivity.class));
        finish();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.rowslist_area = null;
        this.view = null;
        this.view2 = null;
        this.LisItems = null;
        this.Area = null;
        this.who = null;
        this.methodName = null;
        super.onDestroy();
    }
}
