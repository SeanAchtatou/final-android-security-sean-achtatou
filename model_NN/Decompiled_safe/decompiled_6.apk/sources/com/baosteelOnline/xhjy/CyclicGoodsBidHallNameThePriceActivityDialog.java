package com.baosteelOnline.xhjy;

import android.os.Build;
import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import com.baosteelOnline.R;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.main.ExitApplication;

public class CyclicGoodsBidHallNameThePriceActivityDialog extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    private RadioButton mIndexNavigation1;
    private RadioButton mIndexNavigation2;
    private RadioButton mIndexNavigation3;
    private RelativeLayout mManPrice;
    private RelativeLayout mOnePrice;
    private RadioGroup mRadioderGroup;
    private RelativeLayout mSysPrice;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.activity_cyclic_goods_bid_hall_name_the_price_activity_dialog);
        ExitApplication.getInstance().addActivity(this);
        if (Build.VERSION.SDK_INT >= 11) {
            setFinishOnTouchOutside(false);
        }
        this.mManPrice = (RelativeLayout) findViewById(R.id.cyclicgoods_bid_man_name_the_price_rl);
        this.mSysPrice = (RelativeLayout) findViewById(R.id.cyclicgoods_bid_system_name_the_price_rl);
        this.mOnePrice = (RelativeLayout) findViewById(R.id.cyclicgoods_bid_the_price_rl);
        this.mRadioderGroup = (RadioGroup) findViewById(R.id.cyclic_goods_bid_dialog);
        this.mIndexNavigation1 = (RadioButton) findViewById(R.id.cyclicgoods_bid_man_name_the_price);
        this.mIndexNavigation2 = (RadioButton) findViewById(R.id.cyclicgoods_bid_system_name_the_price);
        this.mIndexNavigation3 = (RadioButton) findViewById(R.id.cyclicgoods_bid_the_price);
        this.mIndexNavigation1.setChecked(true);
        this.mRadioderGroup.setOnCheckedChangeListener(this);
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.cyclicgoods_bid_man_name_the_price:
                this.mIndexNavigation1.setChecked(true);
                this.mManPrice.setVisibility(0);
                this.mSysPrice.setVisibility(8);
                this.mOnePrice.setVisibility(8);
                return;
            case R.id.cyclicgoods_bid_system_name_the_price:
                this.mIndexNavigation2.setChecked(true);
                this.mManPrice.setVisibility(8);
                this.mSysPrice.setVisibility(0);
                this.mOnePrice.setVisibility(8);
                return;
            case R.id.cyclicgoods_bid_the_price:
                this.mIndexNavigation3.setChecked(true);
                this.mManPrice.setVisibility(8);
                this.mSysPrice.setVisibility(8);
                this.mOnePrice.setVisibility(0);
                return;
            default:
                return;
        }
    }

    public void onBackPressed() {
        finish();
    }
}
