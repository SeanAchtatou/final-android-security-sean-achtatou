package com.baosteelOnline.xhjy;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.R;
import com.baosteelOnline.common.DataBaseFactory;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.model.SmallNumUtil;
import com.baosteelOnline.sql.DBHelper;
import com.jianq.misc.StringEx;
import java.util.Calendar;

public class CyclicGoodsBidSessionSelect extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    /* access modifiers changed from: private */
    public long beginTime = 20130601;
    /* access modifiers changed from: private */
    public TextView date1;
    /* access modifiers changed from: private */
    public TextView date2;
    DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
            int mon = month + 1;
            String monString = new StringBuilder(String.valueOf(mon)).toString();
            if (mon < 10) {
                monString = "0" + monString;
            }
            if (dayOfMonth > 9) {
                CyclicGoodsBidSessionSelect.this.dateString = " " + year + "-" + monString + "-" + dayOfMonth + " ";
            } else {
                CyclicGoodsBidSessionSelect.this.dateString = " " + year + "-" + monString + "-" + "0" + dayOfMonth + " ";
            }
            String ss = CyclicGoodsBidSessionSelect.this.dateString.replaceAll("\\D", StringEx.Empty);
            Log.d("dateString=", CyclicGoodsBidSessionSelect.this.dateString);
            if (CyclicGoodsBidSessionSelect.this.isDate1) {
                CyclicGoodsBidSessionSelect.this.beginTime = Long.parseLong(ss);
                CyclicGoodsBidSessionSelect.this.date1.setText(CyclicGoodsBidSessionSelect.this.dateString);
                return;
            }
            CyclicGoodsBidSessionSelect.this.endTime = Long.parseLong(ss);
            CyclicGoodsBidSessionSelect.this.date2.setText(CyclicGoodsBidSessionSelect.this.dateString);
        }
    };
    /* access modifiers changed from: private */
    public String dateString = StringEx.Empty;
    private DataBaseFactory db;
    private DBHelper dbHelper;
    private SharedPreferences.Editor editor;
    /* access modifiers changed from: private */
    public long endTime = 20130808;
    private LayoutInflater inflater;
    /* access modifiers changed from: private */
    public boolean isDate1 = true;
    private Calendar mCalendar;
    /* access modifiers changed from: private */
    public DatePickerDialog mDatePickerDialog;
    /* access modifiers changed from: private */
    public DatePickerDialog mDatePickerDialog2;
    public RadioButton mIndexNavigation1;
    public RadioButton mIndexNavigation2;
    public RadioButton mIndexNavigation3;
    public RadioButton mIndexNavigation4;
    public RadioButton mIndexNavigation5;
    public TextView mListTitleTextview;
    public RadioGroup mRadioderGroup;
    /* access modifiers changed from: private */
    public TextView mSpinnerTextView;
    /* access modifiers changed from: private */
    public int sessionCode = 3;
    private SharedPreferences sp;
    private SharedPreferences sp2;

    /* access modifiers changed from: protected */
    @SuppressLint({"InlinedApi"})
    public void onCreate(Bundle savedInstanceState) {
        String defaultYearOfDate1;
        String defaultYearOfDate2;
        String defaultMonthOfDate1;
        String defaultMonthOfDate2;
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.activity_cyclic_goods_bid_session_select);
        ExitApplication.getInstance().addActivity(this);
        this.dbHelper = DataBaseFactory.getInstance(this);
        this.dbHelper.insertOperation("循环物资", "场次搜索", "竞价场次列表");
        this.mSpinnerTextView = (TextView) findViewById(R.id.cyclicgoods_bid_session_select_spinner);
        this.sp = getSharedPreferences("BaoIndex", 2);
        this.sp2 = getSharedPreferences("setDate", 2);
        this.editor = this.sp2.edit();
        ((ImageButton) findViewById(R.id.cyclicgoods_list_title_imbt)).setBackgroundResource(R.drawable.button_home_selector);
        this.date1 = (TextView) findViewById(R.id.cyclicgoods_bid_session_select_date_edit1);
        this.date2 = (TextView) findViewById(R.id.cyclicgoods_bid_session_select_date_edit2);
        SmallNumUtil.JJNum(this, (TextView) findViewById(R.id.jj_dhqrnum));
        this.mCalendar = Calendar.getInstance();
        if (this.sp2.getString("startDate", StringEx.Empty).equals(null) || this.sp2.getString("startDate", StringEx.Empty).equals(StringEx.Empty)) {
            if (this.mCalendar.get(2) + 1 == 12) {
                defaultYearOfDate1 = new StringBuilder(String.valueOf(this.mCalendar.get(1))).toString();
                defaultYearOfDate2 = new StringBuilder(String.valueOf(this.mCalendar.get(1) + 1)).toString();
                defaultMonthOfDate1 = "11";
                defaultMonthOfDate2 = "01";
            } else if (this.mCalendar.get(2) == 0) {
                defaultYearOfDate1 = new StringBuilder(String.valueOf(this.mCalendar.get(1) - 1)).toString();
                defaultYearOfDate2 = new StringBuilder(String.valueOf(this.mCalendar.get(1))).toString();
                defaultMonthOfDate1 = "12";
                defaultMonthOfDate2 = "02";
            } else {
                defaultYearOfDate1 = new StringBuilder(String.valueOf(this.mCalendar.get(1))).toString();
                defaultYearOfDate2 = defaultYearOfDate1;
                int mon = this.mCalendar.get(2) + 1;
                if (mon < 11) {
                    defaultMonthOfDate1 = "0" + this.mCalendar.get(2);
                } else {
                    defaultMonthOfDate1 = new StringBuilder(String.valueOf(this.mCalendar.get(2))).toString();
                }
                if (mon < 9) {
                    defaultMonthOfDate2 = "0" + (mon + 1);
                } else {
                    defaultMonthOfDate2 = new StringBuilder(String.valueOf(mon + 1)).toString();
                }
            }
            String begin = " " + defaultYearOfDate1 + "-" + defaultMonthOfDate1 + "-" + "01" + " ";
            String end = " " + defaultYearOfDate2 + "-" + defaultMonthOfDate2 + "-" + "01" + " ";
            this.date1.setText(begin);
            this.date2.setText(end);
            this.beginTime = Long.parseLong(begin.replaceAll("\\D", StringEx.Empty));
            this.endTime = Long.parseLong(end.replaceAll("\\D", StringEx.Empty));
            this.mSpinnerTextView.setText("  可参加场次");
        } else {
            this.date1.setText(this.sp2.getString("startDate", StringEx.Empty));
            this.date2.setText(this.sp2.getString("endDate", StringEx.Empty));
            this.mSpinnerTextView.setText(this.sp2.getString("session", StringEx.Empty));
            this.sessionCode = this.sp2.getInt("sessionCode", 3);
            this.beginTime = Long.parseLong(this.sp2.getString("startDate", StringEx.Empty).replaceAll("\\D", StringEx.Empty));
            this.endTime = Long.parseLong(this.sp2.getString("endDate", StringEx.Empty).replaceAll("\\D", StringEx.Empty));
        }
        getWindowManager().getDefaultDisplay().getMetrics(new DisplayMetrics());
        this.inflater = getLayoutInflater();
        this.mListTitleTextview = (TextView) findViewById(R.id.cyclicgoods_list_title_textview);
        this.mListTitleTextview.setText("竞价场次");
        this.mRadioderGroup = (RadioGroup) findViewById(R.id.cyclic_goods_index_RGroup);
        this.mIndexNavigation1 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item1);
        this.mIndexNavigation2 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item2);
        this.mIndexNavigation3 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item3);
        this.mIndexNavigation4 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item4);
        this.mIndexNavigation5 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item5);
        final AlertDialog.Builder myBuilder = new AlertDialog.Builder(this);
        this.mRadioderGroup.setOnCheckedChangeListener(this);
        this.mIndexNavigation3.setChecked(true);
        this.mRadioderGroup.setOnCheckedChangeListener(this);
        this.mSpinnerTextView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                myBuilder.setTitle("请选择");
                myBuilder.setItems(new String[]{"全部场次", "可参加场次", "历史场次"}, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                CyclicGoodsBidSessionSelect.this.mSpinnerTextView.setText("  全部场次");
                                CyclicGoodsBidSessionSelect.this.sessionCode = 20;
                                return;
                            case 1:
                                CyclicGoodsBidSessionSelect.this.mSpinnerTextView.setText("  可参加场次");
                                CyclicGoodsBidSessionSelect.this.sessionCode = 3;
                                return;
                            case 2:
                                CyclicGoodsBidSessionSelect.this.mSpinnerTextView.setText("  历史场次");
                                CyclicGoodsBidSessionSelect.this.sessionCode = 30;
                                return;
                            default:
                                return;
                        }
                    }
                });
                myBuilder.create().show();
            }
        });
        this.date1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CyclicGoodsBidSessionSelect.this.isDate1 = true;
                String[] ss = CyclicGoodsBidSessionSelect.this.date1.getText().toString().replaceAll(" ", StringEx.Empty).split("-");
                CyclicGoodsBidSessionSelect.this.mDatePickerDialog = new DatePickerDialog(CyclicGoodsBidSessionSelect.this, CyclicGoodsBidSessionSelect.this.dateListener, Integer.parseInt(ss[0]), Integer.parseInt(ss[1]) - 1, Integer.parseInt(ss[2]));
                CyclicGoodsBidSessionSelect.this.mDatePickerDialog.show();
            }
        });
        this.date2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CyclicGoodsBidSessionSelect.this.isDate1 = false;
                String[] ss = CyclicGoodsBidSessionSelect.this.date2.getText().toString().replaceAll(" ", StringEx.Empty).split("-");
                CyclicGoodsBidSessionSelect.this.mDatePickerDialog2 = new DatePickerDialog(CyclicGoodsBidSessionSelect.this, CyclicGoodsBidSessionSelect.this.dateListener, Integer.parseInt(ss[0]), Integer.parseInt(ss[1]) - 1, Integer.parseInt(ss[2]));
                CyclicGoodsBidSessionSelect.this.mDatePickerDialog2.show();
            }
        });
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.cyclicgoods_index_navigation_item2:
                this.mIndexNavigation2.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item3:
                this.mIndexNavigation3.setChecked(true);
                return;
            case R.id.cyclicgoods_index_navigation_item1:
                if (ObjectStores.getInst().getObject("formX") == "formX") {
                    ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class);
                    finish();
                    return;
                }
                this.mIndexNavigation1.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item4:
                this.mIndexNavigation4.setChecked(true);
                startActivity(new Intent(this, CyclicGoodsBidHallPageActivity.class));
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item5:
                this.mIndexNavigation5.setChecked(true);
                Intent intent = new Intent(this, Xhjy_more.class);
                intent.putExtra("part", "循环物资");
                startActivity(intent);
                finish();
                return;
            default:
                return;
        }
    }

    public void doneAction(View v) {
        if (this.endTime > this.beginTime) {
            Intent intent = new Intent(this, CyclicGoodsBidSession.class);
            intent.putExtra("sessionCode", this.sessionCode);
            intent.putExtra("session", this.mSpinnerTextView.getText().toString());
            intent.putExtra("startDate", this.date1.getText().toString().replaceAll(" ", StringEx.Empty));
            intent.putExtra("endDate", this.date2.getText().toString().replaceAll(" ", StringEx.Empty));
            this.editor.putInt("sessionCode", this.sessionCode);
            this.editor.putString("startDate", this.date1.getText().toString());
            this.editor.putString("endDate", this.date2.getText().toString());
            this.editor.putString("session", this.mSpinnerTextView.getText().toString());
            this.editor.commit();
            Log.d("传递过去的数据：", "session=" + this.mSpinnerTextView.getText().toString() + "sessionCode = " + this.sessionCode + "  startDate=" + ((Object) this.date1.getText()) + " endDate=" + ((Object) this.date2.getText()));
            startActivity(intent);
            finish();
            return;
        }
        new AlertDialog.Builder(this).setMessage("起始时间不能晚于结束时间！").setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        }).show();
    }

    public void backAaction(View v) {
        if (ObjectStores.getInst().getObject("formX") == "formX") {
            ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class);
        } else {
            ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
        }
        finish();
    }

    public void onBackPressed() {
        if (ObjectStores.getInst().getObject("formX") == "formX") {
            ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class);
            finish();
            return;
        }
        ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
        finish();
    }

    public void selectSessionAction(View v) {
    }

    public void allSession(View v) {
    }

    public void cancelAction(View v) {
    }

    public void dateAction(View v) {
        switch (v.getId()) {
            case R.id.cyclicgoods_bid_session_select_date_edit1:
                this.isDate1 = true;
                this.mDatePickerDialog.show();
                return;
            case R.id.cyclicgoods_bid_session_select_line:
            default:
                return;
            case R.id.cyclicgoods_bid_session_select_date_edit2:
                this.isDate1 = false;
                this.mDatePickerDialog2.show();
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }
}
