package com.baosteelOnline.xhjy;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.R;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.main.ExitApplication;
import java.util.ArrayList;
import java.util.HashMap;

public class CyclicGoodsResourcesDetails extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    private RadioButton mIndexNavigation1;
    private RadioButton mIndexNavigation2;
    private RadioButton mIndexNavigation3;
    private RadioButton mIndexNavigation4;
    private RadioButton mIndexNavigation5;
    private ArrayList<HashMap<String, Object>> mItemArrayList;
    public TextView mListTitleTextview;
    private ListView mListView;
    private RadioGroup mRadioderGroup;
    private SimpleAdapter mSimpleAdater;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.activity_cyclic_goods_resources_details);
        ExitApplication.getInstance().addActivity(this);
        this.mListView = (ListView) findViewById(R.id.cyclicgoods_confirm_arrival_listview_resources_details);
        this.mItemArrayList = new ArrayList<>();
        this.mRadioderGroup = (RadioGroup) findViewById(R.id.cyclic_goods_index_RGroup);
        this.mIndexNavigation1 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item1);
        this.mIndexNavigation2 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item2);
        this.mIndexNavigation3 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item3);
        this.mIndexNavigation4 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item4);
        this.mIndexNavigation5 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item5);
        this.mListTitleTextview = (TextView) findViewById(R.id.cyclicgoods_list_title_textview);
        this.mListTitleTextview.setText("资源详情");
        this.mIndexNavigation3.setChecked(true);
        this.mRadioderGroup.setOnCheckedChangeListener(this);
        setData();
        this.mSimpleAdater = new SimpleAdapter(this, this.mItemArrayList, R.layout.activity_cyclic_goods_resources_details_item, new String[]{"title", "title1", "model", "bidprice", "way", "sum", "price", "state"}, new int[]{R.id.cyclicgoods_resources_details_item_textview_title, R.id.cyclicgoods_resources_details_item_textview_title1, R.id.cyclicgoods_resources_details_item_textview_bid_model, R.id.cyclicgoods_resources_details_item_textview_starting_price, R.id.cyclicgoods_resources_details_item_textview_offer_way, R.id.cyclicgoods_resources_details_item_textview_sum_num, R.id.cyclicgoods_resources_details_item_textview_price, R.id.cyclicgoods_resources_details_item_textview_bid_state1});
        this.mListView.setAdapter((ListAdapter) this.mSimpleAdater);
        this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                int i = arg2 + 1;
            }
        });
    }

    private void setData() {
        for (int i = 0; i < 20; i++) {
            HashMap<String, Object> item = new HashMap<>();
            item.put("title", String.valueOf(i + 1) + "拼盘号：");
            item.put("title1", "P121114002001");
            item.put("model", "竞价模式：公开增加");
            item.put("bidprice", "起拍价：1000.00");
            item.put("way", "报盘方式：单价");
            item.put("sum", "总量：316.7993545");
            item.put("price", "一口价：3000.00");
            item.put("state", "竞价中");
            this.mItemArrayList.add(item);
        }
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.cyclicgoods_index_navigation_item2:
                this.mIndexNavigation2.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item3:
                this.mIndexNavigation3.setChecked(true);
                return;
            case R.id.cyclicgoods_index_navigation_item1:
                if (ObjectStores.getInst().getObject("formX") == "formX") {
                    ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class);
                    finish();
                    return;
                }
                this.mIndexNavigation1.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item4:
                this.mIndexNavigation4.setChecked(true);
                startActivity(new Intent(this, CyclicGoodsBidHallPageActivity.class));
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item5:
                this.mIndexNavigation5.setChecked(true);
                Intent intent = new Intent(this, Xhjy_more.class);
                intent.putExtra("part", "循环物资");
                startActivity(intent);
                finish();
                return;
            default:
                return;
        }
    }

    public void backAaction(View v) {
        ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
        finish();
    }

    public void onBackPressed() {
        ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
        finish();
    }
}
