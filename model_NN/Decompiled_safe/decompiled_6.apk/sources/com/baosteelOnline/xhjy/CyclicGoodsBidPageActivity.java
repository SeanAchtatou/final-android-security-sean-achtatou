package com.baosteelOnline.xhjy;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.com.jit.pnxclient.constant.PNXConfigConstant;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.R;
import com.baosteelOnline.common.DataBaseFactory;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.data.RequestBidData;
import com.baosteelOnline.data_parse.ContractParse;
import com.baosteelOnline.dl.Login_indexActivity;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.model.SmallNumUtil;
import com.baosteelOnline.sql.DBHelper;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.net.JQBasicNetwork;
import com.jianq.ui.JQUICallBack;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;

public class CyclicGoodsBidPageActivity extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    /* access modifiers changed from: private */
    public String activity = StringEx.Empty;
    /* access modifiers changed from: private */
    public ImageButton backButton;
    private RelativeLayout bottom_nva1;
    private RelativeLayout bottom_nva2;
    public ContractParse contractParse;
    private boolean cyclic = false;
    private DataBaseFactory db;
    private DBHelper dbHelper;
    private JQUICallBack httpCallBack = new JQUICallBack() {
        public void callBack(ResultData result) {
            CyclicGoodsBidPageActivity.this.progressDialog.dismiss();
            Log.d("result.getStringData获取的数据：", result.getStringData());
            if (!result.getStringData().equals(null) && !result.getStringData().equals(StringEx.Empty)) {
                CyclicGoodsBidPageActivity.this.contractParse = ContractParse.parse(result.getStringData());
                if (ContractParse.getDataString().equals(null) || ContractParse.getDataString().equals(StringEx.Empty)) {
                    new AlertDialog.Builder(CyclicGoodsBidPageActivity.this).setMessage("获取数据失败。").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
                    return;
                }
                Log.d("获取的ROWs数据：", ContractParse.jjo.toString());
                if (ContractParse.jjo.toString().equals(null) || ContractParse.jjo.toString().equals("[[]]") || ContractParse.jjo.toString().equals(StringEx.Empty) || ContractParse.jjo.toString().equals("[]")) {
                    new AlertDialog.Builder(CyclicGoodsBidPageActivity.this).setMessage("数据为空！").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
                    return;
                }
                for (int i = 0; i < ContractParse.jjo.length(); i++) {
                    try {
                        JSONArray row = new JSONArray(ContractParse.jjo.getString(i));
                        CyclicGoodsBidPageActivity.this.mHtmlData = row.getString(1);
                        CyclicGoodsBidPageActivity.this.wtid = row.getString(2);
                        CyclicGoodsBidPageActivity.this.isJoin = row.getString(3);
                        Log.d("CyclicGoodsBidPageActivity获取的ROWs数据HtmlData：", CyclicGoodsBidPageActivity.this.mHtmlData);
                        System.out.println(CyclicGoodsBidPageActivity.this.mHtmlData);
                        String ss = CyclicGoodsBidPageActivity.this.mHtmlData.replace(PNXConfigConstant.RESP_SPLIT_2, StringEx.Empty);
                        Log.d("ss：", ss);
                        System.out.println(ss);
                        Log.d("CyclicGoodsBidPageActivity获取的ROWs数据wtid：", CyclicGoodsBidPageActivity.this.wtid);
                        Log.d("CyclicGoodsBidPageActivity获取的ROWs数据canJoin：", CyclicGoodsBidPageActivity.this.isJoin);
                        ObjectStores.getInst().putObject("positionName", CyclicGoodsBidPageActivity.this.wtid);
                        if (ObjectStores.getInst().getObject("reStr") != "1") {
                            if (CyclicGoodsBidPageActivity.this.activity.equals("竞价场次")) {
                                CyclicGoodsBidPageActivity.this.joinImbtn.setVisibility(8);
                            } else if (CyclicGoodsBidPageActivity.this.activity.equals("竞价公告")) {
                                CyclicGoodsBidPageActivity.this.joinImbtn.setVisibility(0);
                            }
                        } else if (!CyclicGoodsBidPageActivity.this.isJoin.equals(null) && CyclicGoodsBidPageActivity.this.isJoin.equals("1")) {
                            CyclicGoodsBidPageActivity.this.joinImbtn.setVisibility(0);
                        } else if (!CyclicGoodsBidPageActivity.this.isJoin.equals(null) && CyclicGoodsBidPageActivity.this.isJoin.equals("0")) {
                            CyclicGoodsBidPageActivity.this.joinImbtn.setVisibility(8);
                        }
                        CyclicGoodsBidPageActivity.this.mWebView.loadDataWithBaseURL(null, CyclicGoodsBidPageActivity.this.mHtmlData, "text/html", JQBasicNetwork.UTF_8, null);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public int i = 0;
    /* access modifiers changed from: private */
    public String id = StringEx.Empty;
    private String index_more = StringEx.Empty;
    private SharedPreferences indextz;
    /* access modifiers changed from: private */
    public String isJoin = StringEx.Empty;
    /* access modifiers changed from: private */
    public ImageButton joinImbtn;
    /* access modifiers changed from: private */
    public String mHtmlData = StringEx.Empty;
    public RadioButton mIndexNavigation1;
    public RadioButton mIndexNavigation2;
    public RadioButton mIndexNavigation3;
    public RadioButton mIndexNavigation4;
    public RadioButton mIndexNavigation5;
    public TextView mListTitleTextview;
    public RadioGroup mRadioderGroup;
    /* access modifiers changed from: private */
    public WebView mWebView;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    private RadioGroup radioderGroup;
    private SharedPreferences sp;
    /* access modifiers changed from: private */
    public String wtid = StringEx.Empty;

    /* access modifiers changed from: protected */
    @SuppressLint({"SetJavaScriptEnabled"})
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.activity_cyclic_goods_bid_page);
        ExitApplication.getInstance().addActivity(this);
        this.backButton = (ImageButton) findViewById(R.id.cyclicgoods_list_title_imbt);
        this.dbHelper = DataBaseFactory.getInstance(this);
        this.dbHelper.insertOperation("循环物资", "竞价公告", "竞价公告详情");
        this.mRadioderGroup = (RadioGroup) findViewById(R.id.cyclic_goods_index_RGroup);
        this.mIndexNavigation1 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item1);
        this.mIndexNavigation2 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item2);
        this.mIndexNavigation3 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item3);
        this.mIndexNavigation4 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item4);
        this.mIndexNavigation5 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item5);
        this.mRadioderGroup.setOnCheckedChangeListener(this);
        SmallNumUtil.JJNum(this, (TextView) findViewById(R.id.jj_dhqrnum));
        this.bottom_nva1 = (RelativeLayout) findViewById(R.id.bottom_nva1);
        this.bottom_nva2 = (RelativeLayout) findViewById(R.id.bottom_nva2);
        this.indextz = getSharedPreferences("indextz", 2);
        this.cyclic = this.indextz.getBoolean("cyclic", false);
        Intent intent = getIntent();
        this.id = intent.getExtras().getString("id");
        this.activity = intent.getExtras().getString("activity");
        Log.d("CyclicGoodsBidPageActivity收到的id：", String.valueOf(this.id) + "收到的activity：" + this.activity + "收到的cyclic:" + this.cyclic);
        RadioButton radio_notice = (RadioButton) findViewById(R.id.radio_home3);
        this.radioderGroup = (RadioGroup) findViewById(R.id.xhjy_index_RGroup);
        this.radioderGroup.setOnCheckedChangeListener(this);
        SmallNumUtil.GWCNum(this, (TextView) findViewById(R.id.gwc_dhqrnum));
        this.sp = getSharedPreferences("BaoIndex", 0);
        Log.d("部分bIndex = ", this.sp.getString("bIndex", StringEx.Empty));
        if (this.sp.getString("bIndex", StringEx.Empty).equals("xhwz")) {
            this.bottom_nva1.setVisibility(8);
            this.bottom_nva2.setVisibility(0);
            this.mIndexNavigation2.setChecked(true);
        } else {
            radio_notice.setChecked(true);
            this.bottom_nva2.setVisibility(8);
            this.bottom_nva1.setVisibility(0);
        }
        this.mWebView = (WebView) findViewById(R.id.cyclicgoods_bid_page_webview);
        this.mWebView.getSettings().setBuiltInZoomControls(true);
        this.mWebView.setBackgroundColor(Color.parseColor("#ffffff"));
        this.mWebView.setFocusable(true);
        this.mWebView.requestFocus();
        this.mWebView.getSettings().setJavaScriptEnabled(true);
        this.mWebView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        this.mWebView.setWebViewClient(new HelloWebViewClient(this, null));
        this.mWebView.getSettings().setTextSize(WebSettings.TextSize.NORMAL);
        this.mWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int newProgress) {
                if (CyclicGoodsBidPageActivity.this.mWebView.canGoBack()) {
                    CyclicGoodsBidPageActivity.this.backButton.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            CyclicGoodsBidPageActivity access$0 = CyclicGoodsBidPageActivity.this;
                            access$0.i = access$0.i - 1;
                            Log.d("i======>竞价公告", new StringBuilder(String.valueOf(CyclicGoodsBidPageActivity.this.i)).toString());
                            if (CyclicGoodsBidPageActivity.this.i == 0) {
                                CyclicGoodsBidPageActivity.this.mWebView.loadDataWithBaseURL(null, CyclicGoodsBidPageActivity.this.mHtmlData, "text/html", JQBasicNetwork.UTF_8, null);
                            } else if (CyclicGoodsBidPageActivity.this.i == -1) {
                                CyclicGoodsBidPageActivity.this.backAction();
                            } else {
                                CyclicGoodsBidPageActivity.this.mWebView.goBack();
                            }
                        }
                    });
                } else {
                    CyclicGoodsBidPageActivity.this.backButton.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            CyclicGoodsBidPageActivity.this.backAction();
                        }
                    });
                }
            }
        });
        this.joinImbtn = (ImageButton) findViewById(R.id.bid_join_imbt);
        this.joinImbtn.setVisibility(8);
        this.mListTitleTextview = (TextView) findViewById(R.id.cyclicgoods_list_title_textview);
        this.mListTitleTextview.setText(getString(R.string.cyclic_goods_index_imbt3).toString());
        getDate();
    }

    private class HelloWebViewClient extends WebViewClient {
        private HelloWebViewClient() {
        }

        /* synthetic */ HelloWebViewClient(CyclicGoodsBidPageActivity cyclicGoodsBidPageActivity, HelloWebViewClient helloWebViewClient) {
            this();
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            CyclicGoodsBidPageActivity cyclicGoodsBidPageActivity = CyclicGoodsBidPageActivity.this;
            cyclicGoodsBidPageActivity.i = cyclicGoodsBidPageActivity.i + 1;
            Log.d("i======>竞价公告", new StringBuilder(String.valueOf(CyclicGoodsBidPageActivity.this.i)).toString());
            view.loadUrl(url);
            return true;
        }
    }

    public void getDate() {
        RequestBidData requestBidData = new RequestBidData(this);
        requestBidData.setRequestData("竞价公告详情");
        requestBidData.setId(this.id);
        Log.d("请求的指令：", requestBidData.infoDate());
        requestBidData.setHttpCallBack(this.httpCallBack);
        requestBidData.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    public void onBackPressed() {
        backAction();
    }

    public void joinAction(View v) {
        if (ObjectStores.getInst().getObject("reStr") != "1") {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setCancelable(false);
            dialog.setMessage("您还没有登录，请登录！").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    SharedPreferences.Editor editor = CyclicGoodsBidPageActivity.this.getSharedPreferences("index_mark", 2).edit();
                    editor.putString("index", "竞价公告的详情页");
                    editor.commit();
                    Intent intent = new Intent(CyclicGoodsBidPageActivity.this, Login_indexActivity.class);
                    intent.putExtra("id", CyclicGoodsBidPageActivity.this.id);
                    intent.putExtra("activity", CyclicGoodsBidPageActivity.this.activity);
                    CyclicGoodsBidPageActivity.this.startActivity(intent);
                    CyclicGoodsBidPageActivity.this.finish();
                }
            }).show();
            return;
        }
        Intent intent = new Intent(this, CyclicGoodsBidProtocolPageActivity.class);
        intent.putExtra("wtid", this.wtid);
        intent.putExtra("id", this.id);
        intent.putExtra("activity", "竞价公告");
        if (this.activity.equals("竞价场次")) {
            intent.putExtra("activity2", "竞价场次");
        } else {
            intent.putExtra("activity2", "竞价公告");
        }
        try {
            intent.putExtra("sessionCode", getIntent().getExtras().getInt("sessionCode"));
            intent.putExtra("startDate", getIntent().getExtras().getString("startDate"));
            intent.putExtra("endDate", getIntent().getExtras().getString("endDate"));
        } catch (Exception e) {
        }
        startActivity(intent);
        finish();
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.cyclicgoods_index_navigation_item2:
                this.mIndexNavigation2.setChecked(true);
                return;
            case R.id.cyclicgoods_index_navigation_item3:
                this.mIndexNavigation3.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidSessionSelect.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item1:
                if (ObjectStores.getInst().getObject("formX") == "formX") {
                    ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class);
                    finish();
                    return;
                }
                this.mIndexNavigation1.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item4:
                this.mIndexNavigation4.setChecked(true);
                startActivity(new Intent(this, CyclicGoodsBidHallPageActivity.class));
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item5:
                this.mIndexNavigation5.setChecked(true);
                Intent intent = new Intent(this, Xhjy_more.class);
                intent.putExtra("part", "循环物资");
                startActivity(intent);
                finish();
                return;
            case R.id.radio_search3:
                ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
                return;
            case R.id.radio_shopcar3:
                ((RadioButton) findViewById(R.id.radio_shopcar3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ShopActivity.class);
                return;
            case R.id.radio_home3:
                ((RadioButton) findViewById(R.id.radio_home3)).setChecked(true);
                this.index_more = "1";
                HashMap hasmap = new HashMap();
                hasmap.put("index_more", this.index_more);
                ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class, hasmap);
                return;
            case R.id.radio_contract3:
                ((RadioButton) findViewById(R.id.radio_contract3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ContractListActivity.class);
                return;
            case R.id.radio_notice3:
                ((RadioButton) findViewById(R.id.radio_notice3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, Xhjy_more.class);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void backAction() {
        Intent intent = null;
        if (this.cyclic) {
            ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
            finish();
            this.indextz.edit().putBoolean("cyclic", false).commit();
        }
        if (this.activity.equals("竞价场次")) {
            intent = new Intent(this, CyclicGoodsBidSession.class);
            int sessionCode = getIntent().getExtras().getInt("sessionCode");
            String startDate = getIntent().getExtras().getString("startDate");
            String endDate = getIntent().getExtras().getString("endDate");
            intent.putExtra("sessionCode", sessionCode);
            intent.putExtra("startDate", startDate);
            intent.putExtra("endDate", endDate);
        } else if (this.activity.equals("竞价公告")) {
            intent = new Intent(this, CyclicGoodsBidActivity.class);
        }
        intent.putExtra("id", getIntent().getExtras().getString("id"));
        intent.putExtra("activity", getIntent().getExtras().getString("activity"));
        Log.d("CyclicGoodsBidActivity收到竞价公告的id：", String.valueOf(getIntent().getExtras().getString("id")) + "收到的activity：" + getIntent().getExtras().getString("activity"));
        startActivity(intent);
        finish();
    }

    public void backAaction(View v) {
        backAction();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (ObjectStores.getInst().getObject("reStr") != "1") {
            if (this.activity.equals("竞价场次")) {
                this.joinImbtn.setVisibility(8);
            } else if (this.activity.equals("竞价公告")) {
                this.joinImbtn.setVisibility(0);
            }
        } else if (!this.isJoin.equals(null) && this.isJoin.equals("1")) {
            this.joinImbtn.setVisibility(0);
        } else if (!this.isJoin.equals(null) && this.isJoin.equals("0")) {
            this.joinImbtn.setVisibility(8);
        }
        super.onResume();
    }
}
