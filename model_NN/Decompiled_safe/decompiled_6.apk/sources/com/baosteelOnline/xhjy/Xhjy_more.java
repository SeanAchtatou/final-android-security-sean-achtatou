package com.baosteelOnline.xhjy;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.R;
import com.baosteelOnline.common.DataBaseFactory;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.common.jqhttp.CustomDialog;
import com.baosteelOnline.common.jqhttp.UpdateHelper;
import com.baosteelOnline.common.jqhttp.Utils;
import com.baosteelOnline.dl.Login_indexActivity;
import com.baosteelOnline.gd.More_stock_help;
import com.baosteelOnline.gd.More_system;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.model.SmallNumUtil;
import com.baosteelOnline.service.MsgService;
import com.baosteelOnline.sql.DBHelper;
import com.jianq.misc.StringEx;
import java.util.HashMap;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

public class Xhjy_more extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    String XH_reStr = StringEx.Empty;
    String bIndex = StringEx.Empty;
    private RelativeLayout bottom_nva1;
    private RelativeLayout bottom_nva2;
    LinearLayout cancel;
    private DataBaseFactory db;
    private DBHelper dbHelper;
    /* access modifiers changed from: private */
    public SharedPreferences.Editor editor;
    String filePath = StringEx.Empty;
    UpdateHelper helper;
    String index = StringEx.Empty;
    String index_more = StringEx.Empty;
    LinearLayout linear_message;
    private Handler mHandler;
    private RadioButton mIndexNavigation1;
    private RadioButton mIndexNavigation2;
    private RadioButton mIndexNavigation3;
    private RadioButton mIndexNavigation4;
    private RadioButton mIndexNavigation5;
    private Message mMsg;
    private RadioGroup mRadioderGroup;
    private String part = StringEx.Empty;
    private RadioGroup radioderGroup;
    private SharedPreferences sp;
    private TextView texdulu;
    /* access modifiers changed from: private */
    public DialogInterface.OnClickListener updateCancelExitListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            Xhjy_more.this.clearAll();
            ExitApplication.getInstance().exit();
        }
    };
    /* access modifiers changed from: private */
    public DialogInterface.OnClickListener updateCancelListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
        }
    };
    String updateUrl = StringEx.Empty;
    /* access modifiers changed from: private */
    public String updateflag = StringEx.Empty;
    String updatename = StringEx.Empty;
    String updateurl = StringEx.Empty;

    /* access modifiers changed from: protected */
    @SuppressLint({"HandlerLeak"})
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.more);
        ExitApplication.getInstance().addActivity(this);
        this.dbHelper = DataBaseFactory.getInstance(this);
        this.dbHelper.insertOperation("循环资源/钢材交易", "更多", "更多页面");
        this.texdulu = (TextView) findViewById(R.id.texthelp);
        this.bottom_nva1 = (RelativeLayout) findViewById(R.id.bottom_nva1);
        this.bottom_nva2 = (RelativeLayout) findViewById(R.id.bottom_nva2);
        this.mMsg = new Message();
        this.mMsg.what = 1;
        this.mHandler = new Handler() {
            public void handleMessage(Message msg) {
                if (msg.what == 1) {
                    new AlertDialog.Builder(Xhjy_more.this).setMessage("已经是最新版本！").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).create().show();
                } else if (msg.what == 2) {
                    Xhjy_more.this.helper.update(Xhjy_more.this.updateUrl, Xhjy_more.this.filePath, Xhjy_more.this.updateCancelListener, Xhjy_more.this.updateflag);
                } else if (msg.what == 4) {
                    Xhjy_more.this.helper.updateMandatory(Xhjy_more.this.updateUrl, Xhjy_more.this.filePath, Xhjy_more.this.updateCancelExitListener, Xhjy_more.this.updateflag);
                }
                super.handleMessage(msg);
            }
        };
        try {
            this.part = getIntent().getExtras().getString("part");
        } catch (Exception e) {
            this.part = StringEx.Empty;
        }
        if (this.part.equals("循环物资")) {
            this.bottom_nva1.setVisibility(8);
            this.bottom_nva2.setVisibility(0);
        } else {
            this.bottom_nva2.setVisibility(8);
            this.bottom_nva1.setVisibility(0);
        }
        this.mRadioderGroup = (RadioGroup) findViewById(R.id.cyclic_goods_index_RGroup);
        this.mIndexNavigation1 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item1);
        this.mIndexNavigation2 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item2);
        this.mIndexNavigation3 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item3);
        this.mIndexNavigation4 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item4);
        this.mIndexNavigation5 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item5);
        this.mIndexNavigation5.setChecked(true);
        SmallNumUtil.JJNum(this, (TextView) findViewById(R.id.jj_dhqrnum));
        this.mRadioderGroup.setOnCheckedChangeListener(this);
        ExitApplication.getInstance().addActivity(this);
        this.cancel = (LinearLayout) findViewById(R.id.linear4);
        this.linear_message = (LinearLayout) findViewById(R.id.linear_message);
        this.XH_reStr = (String) ObjectStores.getInst().getObject("XH_reStr");
        if (this.XH_reStr == "1") {
            this.texdulu.setText("注销帐号");
            this.cancel.setVisibility(0);
            this.linear_message.setVisibility(0);
        }
        ((RadioButton) findViewById(R.id.radio_notice3)).setChecked(true);
        SmallNumUtil.GWCNum(this, (TextView) findViewById(R.id.gwc_dhqrnum));
        this.radioderGroup = (RadioGroup) findViewById(R.id.xhjy_index_RGroup);
        this.radioderGroup.setOnCheckedChangeListener(this);
    }

    public void versionInfoButtonAction(View v) {
        Intent intent = new Intent(this, More_system.class);
        intent.putExtra("part", this.part);
        startActivity(intent);
        if (this.part.equals("循环物资")) {
            finish();
        }
    }

    public void helpButtonAction(View v) {
        Intent intent = new Intent(this, More_stock_help.class);
        intent.putExtra("part", this.part);
        startActivity(intent);
        if (this.part.equals("循环物资")) {
            finish();
        }
    }

    public void zyfwButtonAction(View v) {
        this.sp = getSharedPreferences("BaoIndex", 2);
        this.editor = this.sp.edit();
        new AlertDialog.Builder(this).setMessage("请选择主营范围:").setNegativeButton("钢材交易", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Xhjy_more.this.index = "gcjy";
                Xhjy_more.this.index_more = "1";
                HashMap hasmap = new HashMap();
                hasmap.put("bIndex", Xhjy_more.this.bIndex);
                Xhjy_more.this.editor.putString("bIndex", Xhjy_more.this.index);
                Xhjy_more.this.editor.commit();
                hasmap.put("index_more", Xhjy_more.this.index_more);
                ObjectStores.getInst().putObject("formX", "formX");
                ExitApplication.getInstance().startActivity(Xhjy_more.this, Xhjy_indexActivity.class, hasmap);
                Xhjy_more.this.finish();
            }
        }).setPositiveButton("循环物资", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Xhjy_more.this.index_more = "1";
                Xhjy_more.this.bIndex = "1";
                Xhjy_more.this.index = "xhwz";
                HashMap hasmap = new HashMap();
                hasmap.put("bIndex", Xhjy_more.this.bIndex);
                Xhjy_more.this.editor.putString("bIndex", Xhjy_more.this.index);
                Xhjy_more.this.editor.commit();
                hasmap.put("index_more", Xhjy_more.this.index_more);
                ObjectStores.getInst().putObject("formX", "formY");
                ExitApplication.getInstance().startActivity(Xhjy_more.this, CyclicGoodsIndexActivity.class, hasmap);
                Xhjy_more.this.finish();
            }
        }).create().show();
    }

    public void zhuxiaoButtonAction(View v) {
        if (ObjectStores.getInst().getObject("reStr") != "1") {
            SharedPreferences.Editor editor2 = getSharedPreferences("index_mark", 2).edit();
            editor2.putString("index", "更多");
            editor2.commit();
            ExitApplication.getInstance().startActivity(this, Login_indexActivity.class);
            return;
        }
        new AlertDialog.Builder(this).setMessage("确定要注销吗？").setNegativeButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Xhjy_more.this.clearGlobe();
                SharedPreferences.Editor jjed = Xhjy_more.this.getSharedPreferences("smallnum", 2).edit();
                jjed.putInt("jjnum", 0);
                jjed.putInt("gwcnum", 0);
                jjed.commit();
                SharedPreferences.Editor editor = Xhjy_more.this.getSharedPreferences("index_mark", 2).edit();
                editor.putString("index", "注销");
                editor.commit();
                ExitApplication.getInstance().startActivity(Xhjy_more.this, Login_indexActivity.class);
                Xhjy_more.this.stopService(new Intent(Xhjy_more.this, MsgService.class));
                Log.d("关闭推送服务：", "关闭推送服务");
                Xhjy_more.this.finish();
            }
        }).setPositiveButton("取消", (DialogInterface.OnClickListener) null).show();
    }

    public void messageButtonAction(View v) {
        if (ObjectStores.getInst().getObject("reStr") != "1") {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setCancelable(false);
            dialog.setMessage("您还没有登录，请登录！").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    SharedPreferences.Editor editor = Xhjy_more.this.getSharedPreferences("index_mark", 2).edit();
                    editor.putString("index", "更多");
                    editor.commit();
                    Xhjy_more.this.startActivity(new Intent(Xhjy_more.this, Login_indexActivity.class));
                    Xhjy_more.this.finish();
                }
            }).show();
            return;
        }
        Intent intent = new Intent(this, MoreMessageSetting.class);
        intent.putExtra("part", this.part);
        startActivity(intent);
        if (this.part.equals("循环物资")) {
            finish();
        }
    }

    public void backButtonAction(View v) {
        if (ObjectStores.getInst().getObject("formX") == "formX") {
            ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class);
        } else {
            ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
        }
        finish();
    }

    public void onBackPressed() {
        if (ObjectStores.getInst().getObject("formX") == "formX") {
            ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class);
        } else {
            ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
        }
        finish();
        super.onBackPressed();
    }

    /* access modifiers changed from: private */
    public void clearGlobe() {
        ObjectStores.getInst().putObject("inputUserName", StringEx.Empty);
        ObjectStores.getInst().putObject("inputPassword", StringEx.Empty);
        ObjectStores.getInst().putObject("XH_msg", StringEx.Empty);
        ObjectStores.getInst().putObject("GfullName", StringEx.Empty);
        ObjectStores.getInst().putObject("parameter_usertokenid", StringEx.Empty);
        ObjectStores.getInst().putObject("parameter_userid", StringEx.Empty);
        ObjectStores.getInst().putObject("companyCode", StringEx.Empty);
        ObjectStores.getInst().putObject("companyFullName", StringEx.Empty);
        ObjectStores.getInst().putObject("BSP_companyCode", StringEx.Empty);
        ObjectStores.getInst().putObject("G_User", StringEx.Empty);
        ObjectStores.getInst().putObject("XH_reStr", StringEx.Empty);
        ObjectStores.getInst().putObject("reStr", StringEx.Empty);
        ObjectStores.getInst().putObject("parameter_czy", StringEx.Empty);
        ObjectStores.getInst().putObject("parameter_czyname", StringEx.Empty);
        ObjectStores.getInst().putObject("parameter_czyrealname", StringEx.Empty);
        ObjectStores.getInst().putObject("parameter_hy", StringEx.Empty);
        ObjectStores.getInst().putObject("parameter_hyname", StringEx.Empty);
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.cyclicgoods_index_navigation_item2:
                this.mIndexNavigation2.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item3:
                this.mIndexNavigation3.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidSessionSelect.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item1:
                if (ObjectStores.getInst().getObject("formX") == "formX") {
                    ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class);
                    finish();
                    return;
                }
                ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item4:
                this.mIndexNavigation4.setChecked(true);
                startActivity(new Intent(this, CyclicGoodsBidHallPageActivity.class));
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item5:
                this.mIndexNavigation5.setChecked(true);
                Intent intent = new Intent(this, Xhjy_more.class);
                intent.putExtra("part", "循环物资");
                startActivity(intent);
                finish();
                return;
            case R.id.radio_search3:
                ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
                return;
            case R.id.radio_shopcar3:
                ((RadioButton) findViewById(R.id.radio_shopcar3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ShopActivity.class);
                return;
            case R.id.radio_home3:
                ((RadioButton) findViewById(R.id.radio_home3)).setChecked(true);
                this.index_more = "1";
                HashMap hasmap = new HashMap();
                hasmap.put("index_more", this.index_more);
                ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class, hasmap);
                return;
            case R.id.radio_contract3:
                ((RadioButton) findViewById(R.id.radio_contract3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ContractListActivity.class);
                return;
            case R.id.radio_notice3:
                ((RadioButton) findViewById(R.id.radio_notice3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, Xhjy_more.class);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        ((RadioButton) findViewById(R.id.radio_notice3)).setChecked(true);
        this.radioderGroup = (RadioGroup) findViewById(R.id.xhjy_index_RGroup);
        this.radioderGroup.setOnCheckedChangeListener(this);
        super.onResume();
    }

    public void updateButtonAction(View v) {
        new Thread(new Runnable() {
            public void run() {
                Xhjy_more.this.checkforupdate();
            }
        }).start();
    }

    private String getversion() {
        try {
            HttpResponse httpResponse = new DefaultHttpClient().execute(new HttpGet("http://m.bsteel.net/update/update_bsteelonline_Android.txt"));
            if (httpResponse.getStatusLine().getStatusCode() == 200) {
                String result = EntityUtils.toString(httpResponse.getEntity());
                System.out.println("result-----:" + result);
                JSONObject json = new JSONObject(result);
                String version = json.getString("latestversion");
                this.updateurl = json.getString("updateurl");
                this.updatename = json.getString("updatename");
                this.updateflag = json.getString("updateflag");
                System.out.println("取得到的version----:" + version);
                return version;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return StringEx.Empty;
    }

    /* access modifiers changed from: private */
    public void checkforupdate() {
        String serverVersion = getversion();
        System.out.println("程序包路径----:" + getApplicationContext().getFilesDir().getAbsolutePath());
        this.filePath = "/mnt/sdcard/bsteel/temp/" + this.updatename;
        this.updateUrl = this.updateurl;
        try {
            String clientVersion = Utils.getVersionName(this);
            this.helper = new UpdateHelper(this);
            String str = this.updateUrl;
            String str2 = this.filePath;
            Log.d("版本：", "serverVersion:" + serverVersion + "clientVersion" + clientVersion);
            Log.d("路径：", "filePath:" + this.filePath + "updateUrl" + this.updateUrl);
            if (!this.helper.checkUpdate(serverVersion, clientVersion)) {
                this.mHandler.sendEmptyMessage(1);
            } else if (this.updateflag.equals("0")) {
                this.mHandler.sendEmptyMessage(2);
            } else if (this.updateflag.equals("1")) {
                this.mHandler.sendEmptyMessage(4);
            }
        } catch (PackageManager.NameNotFoundException e) {
            CustomDialog.getInst().showAlertDialog(this, null, "没找到versionName", null, 0, null);
        }
    }

    private void cancelUpdate() {
    }

    /* access modifiers changed from: private */
    public void clearAll() {
        ObjectStores.getInst().putObject("inputUserName", StringEx.Empty);
        ObjectStores.getInst().putObject("inputPassword", StringEx.Empty);
        ObjectStores.getInst().putObject("XH_msg", StringEx.Empty);
        ObjectStores.getInst().putObject("GfullName", StringEx.Empty);
        ObjectStores.getInst().putObject("parameter_usertokenid", StringEx.Empty);
        ObjectStores.getInst().putObject("parameter_userid", StringEx.Empty);
        ObjectStores.getInst().putObject("companyCode", StringEx.Empty);
        ObjectStores.getInst().putObject("companyFullName", StringEx.Empty);
        ObjectStores.getInst().putObject("BSP_companyCode", StringEx.Empty);
        ObjectStores.getInst().putObject("G_User", StringEx.Empty);
        ObjectStores.getInst().putObject("XH_reStr", StringEx.Empty);
        ObjectStores.getInst().putObject("reStr", StringEx.Empty);
        ObjectStores.getInst().putObject("parameter_czy", StringEx.Empty);
        ObjectStores.getInst().putObject("parameter_czyname", StringEx.Empty);
        ObjectStores.getInst().putObject("parameter_czyrealname", StringEx.Empty);
        ObjectStores.getInst().putObject("parameter_hy", StringEx.Empty);
        ObjectStores.getInst().putObject("parameter_hyname", StringEx.Empty);
        ObjectStores.getInst().putObject("deviceid", StringEx.Empty);
        this.editor.clear();
        this.editor.commit();
    }
}
