package com.baosteelOnline.xhjy;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.andframework.common.ObjectStores;
import com.andframework.ui.CustomListView;
import com.andframework.ui.CustomMessageShow;
import com.baosteelOnline.R;
import com.baosteelOnline.common.DataBaseFactory;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.data.NoticeBusi;
import com.baosteelOnline.data_parse.ContractParse;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.model.SmallNumUtil;
import com.baosteelOnline.sql.DBHelper;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import java.util.ArrayList;
import java.util.HashMap;

public class Xhjy_url_list extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    private RelativeLayout bottom_nva1;
    private RelativeLayout bottom_nva2;
    private int count = 0;
    public int countNo = 0;
    private DataBaseFactory db;
    private DBHelper dbHelper;
    private ImageButton homeBtn;
    private JQUICallBack httpCallBack = new JQUICallBack() {
        public void callBack(ResultData result) {
            Xhjy_url_list.this.progressDialog.dismiss();
            try {
                Log.d("交易预告获取的数据：", result.getStringData());
                System.out.println("result=StringData11=>" + result.getStringData());
                Xhjy_url_list.this.updateProductList(ContractParse.parse(result.getStringData()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    private String index_more = StringEx.Empty;
    /* access modifiers changed from: private */
    public ArrayList<NoticeRow> lisItems = new ArrayList<>();
    /* access modifiers changed from: private */
    public CustomListView listView;
    private RadioButton mIndexNavigation1;
    private RadioButton mIndexNavigation2;
    private RadioButton mIndexNavigation3;
    private RadioButton mIndexNavigation4;
    private RadioButton mIndexNavigation5;
    private RadioGroup mRadioderGroup;
    private String market;
    /* access modifiers changed from: private */
    public String numOfBtn = StringEx.Empty;
    private int numOfItem = 1;
    public String pageNum = "10";
    private String pageNumTal;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    private RadioGroup radioderGroup;
    /* access modifiers changed from: private */
    public int rowsCount = 0;
    /* access modifiers changed from: private */
    public String sign = StringEx.Empty;
    /* access modifiers changed from: private */
    public SharedPreferences sp;
    /* access modifiers changed from: private */
    public String startRow = "1";
    /* access modifiers changed from: private */
    public TextView textcont;
    private TextView title;
    private String type;
    private View view;
    private View view1;
    private View view2;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.all_url_list);
        ExitApplication.getInstance().addActivity(this);
        this.dbHelper = DataBaseFactory.getInstance(this);
        this.homeBtn = (ImageButton) findViewById(R.id.homebutton);
        this.listView = (CustomListView) findViewById(R.id.contractslist);
        this.title = (TextView) findViewById(R.id.titletextview);
        this.numOfBtn = getIntent().getExtras().getString("numOfBtn");
        this.sp = getSharedPreferences("BaoIndex", 2);
        this.bottom_nva1 = (RelativeLayout) findViewById(R.id.bottom_nva1);
        this.bottom_nva2 = (RelativeLayout) findViewById(R.id.bottom_nva2);
        this.view2 = View.inflate(this, R.layout.next_ten_bottom_button, null);
        this.textcont = (TextView) this.view2.findViewById(R.id.khfw_list_bottom_text);
        ((RadioButton) findViewById(R.id.radio_home3)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ExitApplication.getInstance().startActivity(Xhjy_url_list.this, Xhjy_indexActivity.class);
                Xhjy_url_list.this.finish();
            }
        });
        this.radioderGroup = (RadioGroup) findViewById(R.id.xhjy_index_RGroup);
        this.radioderGroup.setOnCheckedChangeListener(this);
        SmallNumUtil.GWCNum(this, (TextView) findViewById(R.id.gwc_dhqrnum));
        if (this.sp.getString("bIndex", StringEx.Empty).equals("xhwz")) {
            this.bottom_nva1.setVisibility(8);
            this.bottom_nva2.setVisibility(0);
        } else {
            this.bottom_nva2.setVisibility(8);
            this.bottom_nva1.setVisibility(0);
        }
        this.mRadioderGroup = (RadioGroup) findViewById(R.id.cyclic_goods_index_RGroup);
        this.mIndexNavigation1 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item1);
        this.mIndexNavigation2 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item2);
        this.mIndexNavigation3 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item3);
        this.mIndexNavigation4 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item4);
        this.mIndexNavigation5 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item5);
        this.mRadioderGroup.setOnCheckedChangeListener(this);
        this.mIndexNavigation1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ExitApplication.getInstance().startActivity(Xhjy_url_list.this, CyclicGoodsIndexActivity.class);
                Xhjy_url_list.this.finish();
            }
        });
        SmallNumUtil.JJNum(this, (TextView) findViewById(R.id.jj_dhqrnum));
        init();
        load();
        this.homeBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Xhjy_url_list.this.sp.getString("bIndex", StringEx.Empty).equals("xhwz")) {
                    Xhjy_url_list.this.startActivity(new Intent(Xhjy_url_list.this, CyclicGoodsIndexActivity.class));
                } else {
                    Xhjy_url_list.this.startActivity(new Intent(Xhjy_url_list.this, Xhjy_indexActivity.class));
                }
                Xhjy_url_list.this.finish();
            }
        });
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent;
                System.out.println("rowsCount==>" + Xhjy_url_list.this.rowsCount);
                System.out.println("position==>" + position);
                System.out.println("lisItems.size()==>" + Xhjy_url_list.this.lisItems.size());
                if (position - 1 == Xhjy_url_list.this.lisItems.size()) {
                    Xhjy_url_list.this.startRow = new StringBuilder(String.valueOf(Integer.parseInt(Xhjy_url_list.this.startRow) + 1)).toString();
                    if (Xhjy_url_list.this.textcont.getText().toString().equals("下10条...")) {
                        Xhjy_url_list.this.listView.removeFooterView(view);
                        Xhjy_url_list.this.load();
                        return;
                    }
                    return;
                }
                HashMap<String, String> hasmap = new HashMap<>();
                hasmap.put("ggid", ((NoticeRow) Xhjy_url_list.this.lisItems.get(position - 1)).ggid.toString());
                hasmap.put("sign", Xhjy_url_list.this.sign);
                Log.d("ggid", ((NoticeRow) Xhjy_url_list.this.lisItems.get(position - 1)).ggid.toString());
                if (ObjectStores.getInst().getObject("formX") != "formX") {
                    intent = new Intent(Xhjy_url_list.this, CyclicGoodsBidPagePinTaiActivity.class);
                } else if (Xhjy_url_list.this.numOfBtn.equals("竞价公告")) {
                    intent = new Intent(Xhjy_url_list.this, GCJYBidPageActivity.class);
                } else {
                    intent = new Intent(Xhjy_url_list.this, Gcjy_pageinfo.class);
                }
                intent.putExtra("id", ((NoticeRow) Xhjy_url_list.this.lisItems.get(position - 1)).ggid.toString());
                intent.putExtra("numOfBtn", Xhjy_url_list.this.numOfBtn);
                intent.putExtra("sign", Xhjy_url_list.this.sign);
                intent.putExtra("ggid", ((NoticeRow) Xhjy_url_list.this.lisItems.get(position - 1)).ggid.toString());
                if (Xhjy_url_list.this.numOfBtn.equals("平台公告按钮")) {
                    intent.putExtra("activity", "平台公告");
                } else if (Xhjy_url_list.this.numOfBtn.equals("交易预告按钮")) {
                    intent.putExtra("activity", "交易预告");
                } else if (Xhjy_url_list.this.numOfBtn.equals("竞价公告")) {
                    intent.putExtra("activity", "竞价公告");
                }
                Xhjy_url_list.this.startActivity(intent);
                Xhjy_url_list.this.finish();
            }
        });
    }

    /* access modifiers changed from: private */
    public void load() {
        NoticeBusi noticeInfo = new NoticeBusi(this);
        noticeInfo.startRow = this.startRow;
        noticeInfo.pageNum = this.pageNum;
        noticeInfo.type = this.type;
        noticeInfo.numOfBtn = this.numOfBtn;
        noticeInfo.market = this.market;
        noticeInfo.setHttpCallBack(this.httpCallBack);
        noticeInfo.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    private void init() {
        this.sign = getIntent().getStringExtra("sign");
        if (this.sign.equals("gcjjyg")) {
            this.title.setText("交易预告");
            this.type = "0";
            this.market = "1";
        }
        if (this.sign.equals("wzjjyg")) {
            this.title.setText("交易预告");
            this.type = "0";
            this.market = "2";
        }
        if (this.sign.equals("gcjjgg")) {
            this.title.setText("竞价公告");
            this.type = "1";
            this.market = "1";
        }
        if (this.sign.equals("wzjjgg")) {
            this.title.setText("竞价公告");
            this.type = "1";
            this.market = "2";
        }
        this.numOfBtn = getIntent().getExtras().getString("numOfBtn");
        if (this.numOfBtn.equals("平台公告按钮")) {
            this.title.setText("平台公告");
            this.type = "2";
        } else if (this.numOfBtn.equals("交易预告按钮")) {
            this.title.setText("交易预告");
            this.type = "0";
        } else if (this.numOfBtn.equals("竞价公告")) {
            this.title.setText("竞价公告");
            this.type = "1";
            this.market = "1";
        }
    }

    /* access modifiers changed from: private */
    public void updateProductList(ContractParse contractParse) {
        if (ContractParse.commonData == null || ContractParse.commonData.blocks == null || ContractParse.commonData.blocks.r0 == null || ContractParse.commonData.blocks.r0.mar == null || ContractParse.commonData.blocks.r0.mar.rows == null) {
            CustomMessageShow.getInst().showLongToast(this, "服务器无数据返回！");
        } else if (ContractParse.commonData.blocks.r0.mar.rows.rows == null || ContractParse.commonData.blocks.r0.mar.rows.rows.size() == 0) {
            new AlertDialog.Builder(this).setMessage("对不起，没有数据!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    ExitApplication.getInstance().back(0);
                }
            }).show().setCanceledOnTouchOutside(false);
        } else if (ContractParse.commonData != null && ContractParse.commonData.blocks != null && ContractParse.commonData.blocks.r0 != null && ContractParse.commonData.blocks.r0.mar != null && ContractParse.commonData.blocks.r0.mar.rows != null && ContractParse.commonData.blocks.r0.mar.rows.rows != null) {
            this.rowsCount = ContractParse.commonData.blocks.r0.mar.rows.rows.size();
            this.pageNumTal = contractParse.getPageNumTal();
            Log.d("contractParse.getDataString()", ContractParse.getDataString());
            for (int i = 0; i < this.rowsCount; i++) {
                ArrayList<String> rowdata = ContractParse.commonData.blocks.r0.mar.rows.rows.get(i);
                NoticeRow sr = new NoticeRow();
                if (rowdata != null) {
                    for (int k = 0; k < rowdata.size(); k++) {
                        if (k == 0) {
                            sr.number = new StringBuilder(String.valueOf(this.numOfItem)).toString();
                            sr.ggmc = (String) rowdata.get(k);
                        }
                        if (k == 1) {
                            sr.ggid = (String) rowdata.get(k);
                        }
                        if (k == 2) {
                            sr.ggrq = (String) rowdata.get(k);
                        }
                    }
                    this.numOfItem++;
                    this.lisItems.add(sr);
                    this.view = buildRowView(sr);
                    this.listView.addViewToLast(this.view);
                    rowdata.removeAll(rowdata);
                }
            }
            String index_id = "0";
            if (this.lisItems.size() > 0) {
                index_id = this.lisItems.get(0).ggid.toString();
            }
            System.out.println(String.valueOf(index_id) + "===================url");
            if (this.sign.equals("gcjjyg")) {
                SharedPreferences.Editor editor = getSharedPreferences("gcwelcome", 2).edit();
                if (this.type.equals("0")) {
                    this.dbHelper.insertOperation("钢材交易", "钢材交易首页", "交易预告");
                    editor.putInt("gcindex_jyyg", Integer.valueOf(index_id).intValue());
                    Log.d("钢材交易============>", "交易预告");
                } else if (this.type.equals("1")) {
                    editor.putInt("gcindex_jjgg", Integer.valueOf(index_id).intValue());
                    Log.d("钢材交易============>", "竞价公告");
                }
                editor.commit();
            }
            if (this.numOfBtn.equals("竞价公告")) {
                SharedPreferences.Editor editor2 = getSharedPreferences("gcwelcome", 2).edit();
                editor2.putInt("gcindex_jjgg", Integer.valueOf(index_id).intValue());
                Log.d("钢材交易============>", "竞价公告");
                editor2.commit();
            }
            if (this.sign.equals("wzjjyg")) {
                SharedPreferences.Editor editor3 = getSharedPreferences("welcome", 2).edit();
                if (this.type.equals("0")) {
                    this.dbHelper.insertOperation("循环物资", "交易预告", StringEx.Empty);
                    editor3.putInt("index_jyyg", Integer.valueOf(index_id).intValue());
                    Log.d("循环物质============>", "交易预告");
                } else if (this.type.equals("2")) {
                    this.dbHelper.insertOperation("循环物资", "平台公告", StringEx.Empty);
                    editor3.putInt("index_ptgg", Integer.valueOf(index_id).intValue());
                    Log.d("循环物质============>", "平台公告");
                }
                editor3.commit();
            }
            if (Integer.valueOf(this.startRow).intValue() < Integer.valueOf(this.pageNumTal).intValue()) {
                this.textcont.setText("下10条...");
                this.listView.addFooterView(this.view2);
            } else {
                this.textcont.setText("已经是最后一页");
                this.listView.addFooterView(this.view2);
            }
            this.listView.onRefreshComplete();
        }
    }

    private View buildRowView(NoticeRow sr) {
        this.view1 = View.inflate(this, R.layout.all_url_list_row, null);
        ((TextView) this.view1.findViewById(R.id.gg_number)).setText(sr.number);
        ((TextView) this.view1.findViewById(R.id.gg_context)).setText(sr.ggmc);
        ((TextView) this.view1.findViewById(R.id.gg_date)).setText(sr.ggrq);
        return this.view1;
    }

    class NoticeRow {
        public String ggid;
        public String ggmc;
        public String ggrq;
        public String number;

        NoticeRow() {
        }
    }

    public void onBackPressed() {
        if (this.sp.getString("bIndex", StringEx.Empty).equals("xhwz")) {
            startActivity(new Intent(this, CyclicGoodsIndexActivity.class));
        } else {
            startActivity(new Intent(this, Xhjy_indexActivity.class));
        }
        finish();
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.cyclicgoods_index_navigation_item2:
                this.mIndexNavigation2.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item3:
                this.mIndexNavigation3.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidSessionSelect.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item1:
                this.mIndexNavigation1.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item4:
                this.mIndexNavigation4.setChecked(true);
                startActivity(new Intent(this, CyclicGoodsBidHallPageActivity.class));
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item5:
                this.mIndexNavigation5.setChecked(true);
                Intent intent = new Intent(this, Xhjy_more.class);
                intent.putExtra("part", "循环物资");
                startActivity(intent);
                finish();
                return;
            case R.id.radio_search3:
                ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
                return;
            case R.id.radio_shopcar3:
                ((RadioButton) findViewById(R.id.radio_shopcar3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ShopActivity.class);
                return;
            case R.id.radio_home3:
                ((RadioButton) findViewById(R.id.radio_home3)).setChecked(true);
                this.index_more = "1";
                HashMap hasmap = new HashMap();
                hasmap.put("index_more", this.index_more);
                ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class, hasmap);
                return;
            case R.id.radio_contract3:
                ((RadioButton) findViewById(R.id.radio_contract3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ContractListActivity.class);
                return;
            case R.id.radio_notice3:
                ((RadioButton) findViewById(R.id.radio_notice3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, Xhjy_more.class);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.sign = StringEx.Empty;
        this.index_more = StringEx.Empty;
        this.listView = null;
        this.view = null;
        this.view1 = null;
        this.view2 = null;
        this.type = StringEx.Empty;
        this.market = StringEx.Empty;
        this.countNo = 0;
        this.startRow = StringEx.Empty;
        this.pageNum = StringEx.Empty;
        this.rowsCount = 0;
        this.count = 0;
        this.lisItems = null;
        super.onDestroy();
    }
}
