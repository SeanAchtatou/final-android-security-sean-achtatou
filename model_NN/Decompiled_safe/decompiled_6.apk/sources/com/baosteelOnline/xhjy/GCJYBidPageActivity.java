package com.baosteelOnline.xhjy;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.R;
import com.baosteelOnline.common.DataBaseFactory;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.data.RequestBidData;
import com.baosteelOnline.data_parse.ContractParse;
import com.baosteelOnline.dl.Login_indexActivity;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.model.SmallNumUtil;
import com.baosteelOnline.sql.DBHelper;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.net.JQBasicNetwork;
import com.jianq.ui.JQUICallBack;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;

public class GCJYBidPageActivity extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    /* access modifiers changed from: private */
    public String activity = StringEx.Empty;
    /* access modifiers changed from: private */
    public ImageButton backButton;
    public ContractParse contractParse;
    private DataBaseFactory db;
    private DBHelper dbHelper;
    private JQUICallBack httpCallBack = new JQUICallBack() {
        public void callBack(ResultData result) {
            GCJYBidPageActivity.this.progressDialog.dismiss();
            Log.d("result.getStringData获取的数据：", result.getStringData());
            if (!result.getStringData().equals(null) && !result.getStringData().equals(StringEx.Empty)) {
                GCJYBidPageActivity.this.contractParse = ContractParse.parse(result.getStringData());
                if (ContractParse.getDataString().equals(null) || ContractParse.getDataString().equals(StringEx.Empty)) {
                    new AlertDialog.Builder(GCJYBidPageActivity.this).setMessage("获取数据失败。").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
                    return;
                }
                Log.d("获取的ROWs数据：", ContractParse.jjo.toString());
                if (ContractParse.jjo.toString().equals(null) || ContractParse.jjo.toString().equals("[[]]") || ContractParse.jjo.toString().equals(StringEx.Empty) || ContractParse.jjo.toString().equals("[]")) {
                    new AlertDialog.Builder(GCJYBidPageActivity.this).setMessage("数据为空！").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
                    return;
                }
                for (int i = 0; i < ContractParse.jjo.length(); i++) {
                    try {
                        JSONArray row = new JSONArray(ContractParse.jjo.getString(i));
                        GCJYBidPageActivity.this.mHtmlData = row.getString(1);
                        GCJYBidPageActivity.this.wtid = row.getString(2);
                        GCJYBidPageActivity.this.isJoin = row.getString(3);
                        if (ObjectStores.getInst().getObject("reStr") != "1") {
                            if (GCJYBidPageActivity.this.activity.equals("竞价场次")) {
                                GCJYBidPageActivity.this.joinImbtn.setVisibility(8);
                            } else if (GCJYBidPageActivity.this.activity.equals("竞价公告")) {
                                GCJYBidPageActivity.this.joinImbtn.setVisibility(0);
                            }
                        } else if (!GCJYBidPageActivity.this.isJoin.equals(null) && GCJYBidPageActivity.this.isJoin.equals("1")) {
                            GCJYBidPageActivity.this.joinImbtn.setVisibility(0);
                        } else if (!GCJYBidPageActivity.this.isJoin.equals(null) && GCJYBidPageActivity.this.isJoin.equals("0")) {
                            GCJYBidPageActivity.this.joinImbtn.setVisibility(8);
                        }
                        GCJYBidPageActivity.this.mWebView.loadDataWithBaseURL(null, GCJYBidPageActivity.this.mHtmlData, "text/html", JQBasicNetwork.UTF_8, null);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public int i = 0;
    /* access modifiers changed from: private */
    public String id = StringEx.Empty;
    private String index_more = StringEx.Empty;
    /* access modifiers changed from: private */
    public String isJoin = StringEx.Empty;
    /* access modifiers changed from: private */
    public ImageButton joinImbtn;
    /* access modifiers changed from: private */
    public String mHtmlData = StringEx.Empty;
    public TextView mListTitleTextview;
    /* access modifiers changed from: private */
    public WebView mWebView;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    private RadioGroup radioderGroup;
    private SharedPreferences sp;
    /* access modifiers changed from: private */
    public String wtid = StringEx.Empty;
    private boolean xhjygc = false;

    /* access modifiers changed from: protected */
    @SuppressLint({"SetJavaScriptEnabled"})
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.activity_gcjybid_page);
        ExitApplication.getInstance().addActivity(this);
        this.dbHelper = DataBaseFactory.getInstance(this);
        this.backButton = (ImageButton) findViewById(R.id.cyclicgoods_list_title_imbt);
        ((RadioButton) findViewById(R.id.radio_home3)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ExitApplication.getInstance().startActivity(GCJYBidPageActivity.this, Xhjy_indexActivity.class);
                GCJYBidPageActivity.this.finish();
            }
        });
        SmallNumUtil.GWCNum(this, (TextView) findViewById(R.id.gwc_dhqrnum));
        final SharedPreferences preferences = getSharedPreferences("indextz", 2);
        this.xhjygc = preferences.getBoolean("xhjygc", false);
        Intent intent = getIntent();
        this.id = intent.getExtras().getString("id");
        this.activity = intent.getExtras().getString("activity");
        this.radioderGroup = (RadioGroup) findViewById(R.id.xhjy_index_RGroup);
        this.radioderGroup.setOnCheckedChangeListener(this);
        if (this.xhjygc) {
            this.backButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    ExitApplication.getInstance().startActivity(GCJYBidPageActivity.this, Xhjy_indexActivity.class);
                    GCJYBidPageActivity.this.finish();
                    preferences.edit().putBoolean("xhjygc", false);
                }
            });
        }
        this.sp = getSharedPreferences("BaoIndex", 2);
        this.mWebView = (WebView) findViewById(R.id.gcjy_bid_page_webview);
        this.mWebView.getSettings().setBuiltInZoomControls(true);
        this.mWebView.setBackgroundColor(Color.parseColor("#ffffff"));
        this.mWebView.setFocusable(true);
        this.mWebView.requestFocus();
        this.mWebView.clearCache(true);
        this.mWebView.getSettings().setJavaScriptEnabled(true);
        this.mWebView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        this.mWebView.setWebViewClient(new HelloWebViewClient(this, null));
        this.mWebView.getSettings().setTextSize(WebSettings.TextSize.NORMAL);
        this.joinImbtn = (ImageButton) findViewById(R.id.bid_join_imbt);
        this.joinImbtn.setVisibility(8);
        this.mWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int newProgress) {
                if (GCJYBidPageActivity.this.mWebView.canGoBack()) {
                    GCJYBidPageActivity.this.backButton.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            GCJYBidPageActivity access$0 = GCJYBidPageActivity.this;
                            access$0.i = access$0.i - 1;
                            Log.d("i======>竞价公告", new StringBuilder(String.valueOf(GCJYBidPageActivity.this.i)).toString());
                            if (GCJYBidPageActivity.this.i == 0) {
                                GCJYBidPageActivity.this.mWebView.loadDataWithBaseURL(null, GCJYBidPageActivity.this.mHtmlData, "text/html", JQBasicNetwork.UTF_8, null);
                            } else if (GCJYBidPageActivity.this.i == -1) {
                                GCJYBidPageActivity.this.backAction();
                            } else {
                                GCJYBidPageActivity.this.mWebView.goBack();
                            }
                        }
                    });
                } else {
                    GCJYBidPageActivity.this.backButton.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            GCJYBidPageActivity.this.backAction();
                        }
                    });
                }
            }
        });
        this.mListTitleTextview = (TextView) findViewById(R.id.cyclicgoods_list_title_textview);
        this.mListTitleTextview.setText(getString(R.string.cyclic_goods_index_imbt3).toString());
        getDate();
    }

    private class HelloWebViewClient extends WebViewClient {
        private HelloWebViewClient() {
        }

        /* synthetic */ HelloWebViewClient(GCJYBidPageActivity gCJYBidPageActivity, HelloWebViewClient helloWebViewClient) {
            this();
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            GCJYBidPageActivity gCJYBidPageActivity = GCJYBidPageActivity.this;
            gCJYBidPageActivity.i = gCJYBidPageActivity.i + 1;
            view.loadUrl(url);
            return true;
        }
    }

    public void getDate() {
        RequestBidData requestBidData = new RequestBidData(this);
        requestBidData.setRequestData("竞价公告详情");
        requestBidData.setId(this.id);
        Log.d("请求的指令：", requestBidData.infoDate());
        requestBidData.setHttpCallBack(this.httpCallBack);
        requestBidData.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    /* access modifiers changed from: private */
    public void backAction() {
        Intent intent = null;
        if (this.activity.equals("竞价场次")) {
            intent = new Intent(this, CyclicGoodsBidSession.class);
            int sessionCode = getIntent().getExtras().getInt("sessionCode");
            String startDate = getIntent().getExtras().getString("startDate");
            String endDate = getIntent().getExtras().getString("endDate");
            intent.putExtra("sessionCode", sessionCode);
            intent.putExtra("startDate", startDate);
            intent.putExtra("endDate", endDate);
            intent.putExtra("id", getIntent().getExtras().getString("id"));
            intent.putExtra("activity", getIntent().getExtras().getString("activity"));
        } else if (this.activity.equals("竞价公告")) {
            intent = new Intent(this, Xhjy_url_list.class);
            String sign = getIntent().getStringExtra("sign");
            String numOfBtn = getIntent().getExtras().getString("numOfBtn");
            intent.putExtra("sign", sign);
            intent.putExtra("numOfBtn", numOfBtn);
            Log.d("sign=", String.valueOf(sign) + "numOfBtn=" + numOfBtn);
        }
        startActivity(intent);
        finish();
    }

    public void onBackPressed() {
        backAction();
    }

    public void joinAction(View v) {
        if (ObjectStores.getInst().getObject("reStr") != "1") {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setCancelable(false);
            dialog.setMessage("您还没有登录，请登录！").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    SharedPreferences.Editor editor = GCJYBidPageActivity.this.getSharedPreferences("index_mark", 2).edit();
                    editor.putString("index", "竞价公告的详情页2");
                    editor.commit();
                    Intent intent = new Intent(GCJYBidPageActivity.this, Login_indexActivity.class);
                    intent.putExtra("id", GCJYBidPageActivity.this.id);
                    intent.putExtra("activity", GCJYBidPageActivity.this.activity);
                    String sign = GCJYBidPageActivity.this.getIntent().getStringExtra("sign");
                    String numOfBtn = GCJYBidPageActivity.this.getIntent().getExtras().getString("numOfBtn");
                    intent.putExtra("sign", sign);
                    intent.putExtra("numOfBtn", numOfBtn);
                    GCJYBidPageActivity.this.startActivity(intent);
                    GCJYBidPageActivity.this.finish();
                }
            }).show();
            return;
        }
        Intent intent = new Intent(this, GCJYBidProtocolPageActivity.class);
        intent.putExtra("wtid", this.wtid);
        intent.putExtra("id", this.id);
        intent.putExtra("activity", "竞价公告");
        if (this.activity.equals("竞价场次")) {
            intent.putExtra("activity2", "竞价场次");
        } else {
            intent.putExtra("activity2", "竞价公告");
        }
        try {
            intent.putExtra("sessionCode", getIntent().getExtras().getInt("sessionCode"));
            intent.putExtra("startDate", getIntent().getExtras().getString("startDate"));
            intent.putExtra("endDate", getIntent().getExtras().getString("endDate"));
        } catch (Exception e) {
        }
        startActivity(intent);
        finish();
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.radio_search3:
                ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
                return;
            case R.id.radio_shopcar3:
                ((RadioButton) findViewById(R.id.radio_shopcar3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ShopActivity.class);
                return;
            case R.id.radio_home3:
                ((RadioButton) findViewById(R.id.radio_home3)).setChecked(true);
                this.index_more = "1";
                HashMap hasmap = new HashMap();
                hasmap.put("index_more", this.index_more);
                ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class, hasmap);
                return;
            case R.id.radio_contract3:
                ((RadioButton) findViewById(R.id.radio_contract3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ContractListActivity.class);
                return;
            case R.id.radio_notice3:
                ((RadioButton) findViewById(R.id.radio_notice3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, Xhjy_more.class);
                return;
            default:
                return;
        }
    }

    public void backAaction(View v) {
        Log.d("xhjgc========>我的内容是什么？===", String.valueOf(this.xhjygc) + ",1234");
        backAction();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (ObjectStores.getInst().getObject("reStr") != "1") {
            if (this.activity.equals("竞价场次")) {
                this.joinImbtn.setVisibility(8);
            } else if (this.activity.equals("竞价公告")) {
                this.joinImbtn.setVisibility(0);
            }
        } else if (!this.isJoin.equals(null) && this.isJoin.equals("1")) {
            this.joinImbtn.setVisibility(0);
        } else if (!this.isJoin.equals(null) && this.isJoin.equals("0")) {
            this.joinImbtn.setVisibility(8);
        }
        super.onResume();
    }
}
