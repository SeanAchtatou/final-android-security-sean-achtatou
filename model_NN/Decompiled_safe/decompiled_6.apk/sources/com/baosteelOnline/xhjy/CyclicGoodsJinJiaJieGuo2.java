package com.baosteelOnline.xhjy;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.com.jit.pnxclient.constant.PNXConfigConstant;
import com.baosteelOnline.R;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.data.RequestBidData;
import com.baosteelOnline.data_parse.ContractParse;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.model.SmallNumUtil;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;

public class CyclicGoodsJinJiaJieGuo2 extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    private RelativeLayout CyclicGoodsJinJiaJieGuo2_first_item;
    private ListView CyclicGoodsJingJiaJieGuo2_listview;
    private String allResultString = StringEx.Empty;
    /* access modifiers changed from: private */
    public String bpfs = StringEx.Empty;
    private String chengjiaojia = "成交价:卖方不公开;";
    private String chengjiaojieguo = "成交结果:卖方不公开;";
    /* access modifiers changed from: private */
    public String cjj = StringEx.Empty;
    public ContractParse contractParse;
    private TextView first_item_text;
    private TextView first_item_text_jieguo;
    /* access modifiers changed from: private */
    public boolean hasNextTen = false;
    private JQUICallBack httpCallBack = new JQUICallBack() {
        public void callBack(ResultData result) {
            String nextString;
            CyclicGoodsJinJiaJieGuo2.this.progressDialog.dismiss();
            Log.d("获取的数据：", result.getStringData());
            String dataString = result.getStringData();
            if (!dataString.equals(null) && !dataString.equals(StringEx.Empty)) {
                CyclicGoodsJinJiaJieGuo2.this.contractParse = ContractParse.parse(result.getStringData());
                if (ContractParse.pageNumTal.equals(null) || ContractParse.pageNumTal.equals(StringEx.Empty)) {
                    CyclicGoodsJinJiaJieGuo2.this.numOfPageGet = 1;
                } else {
                    CyclicGoodsJinJiaJieGuo2.this.numOfPageGet = Integer.parseInt(ContractParse.pageNumTal);
                }
                if (CyclicGoodsJinJiaJieGuo2.this.nowNum < CyclicGoodsJinJiaJieGuo2.this.numOfPageGet) {
                    CyclicGoodsJinJiaJieGuo2.this.hasNextTen = true;
                } else {
                    CyclicGoodsJinJiaJieGuo2.this.hasNextTen = false;
                }
                Log.d("收到的数据：", ContractParse.decryptData);
                if (ContractParse.getDataString().equals(null) || ContractParse.getDataString().equals(StringEx.Empty)) {
                    new AlertDialog.Builder(CyclicGoodsJinJiaJieGuo2.this).setMessage("获取数据失败").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            CyclicGoodsJinJiaJieGuo2.this.finish();
                        }
                    }).show();
                } else {
                    Log.d("获取的ROWs数据：", ContractParse.jjo.toString());
                    if (ContractParse.jjo.toString().equals(null) || ContractParse.jjo.toString().equals("[[]]") || ContractParse.jjo.toString().equals(StringEx.Empty) || ContractParse.jjo.toString().equals("[]")) {
                        new AlertDialog.Builder(CyclicGoodsJinJiaJieGuo2.this).setMessage("数据为空！").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
                    } else {
                        for (int i = 0; i < ContractParse.jjo.length(); i++) {
                            try {
                                JSONArray row = new JSONArray(ContractParse.jjo.getString(i));
                                CyclicGoodsJinJiaJieGuo2.this.ppid = row.getString(0).toString();
                                CyclicGoodsJinJiaJieGuo2.this.ppzt = row.getString(1).toString();
                                CyclicGoodsJinJiaJieGuo2.this.jjms = row.getString(2).toString();
                                CyclicGoodsJinJiaJieGuo2.this.bpfs = row.getString(3).toString();
                                CyclicGoodsJinJiaJieGuo2.this.cjj = row.getString(4).toString();
                                CyclicGoodsJinJiaJieGuo2.this.zbhy = row.getString(5).toString();
                                CyclicGoodsJinJiaJieGuo2.this.kssj = row.getString(6).toString();
                                CyclicGoodsJinJiaJieGuo2.this.zj = row.getString(7).toString();
                                HashMap<String, Object> item = new HashMap<>();
                                item.put("ppid", CyclicGoodsJinJiaJieGuo2.this.ppid);
                                item.put("ppzt", CyclicGoodsJinJiaJieGuo2.this.ppzt);
                                item.put("jjms", CyclicGoodsJinJiaJieGuo2.this.jjms);
                                item.put("bpfs", CyclicGoodsJinJiaJieGuo2.this.bpfs);
                                item.put("cjj", CyclicGoodsJinJiaJieGuo2.this.cjj);
                                item.put("zbhy", CyclicGoodsJinJiaJieGuo2.this.zbhy);
                                item.put("kssj", CyclicGoodsJinJiaJieGuo2.this.kssj);
                                item.put("zj", CyclicGoodsJinJiaJieGuo2.this.zj);
                                CyclicGoodsJinJiaJieGuo2.this.mItemArrayList.add(item);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        HashMap<String, Object> item2 = new HashMap<>();
                        if (CyclicGoodsJinJiaJieGuo2.this.hasNextTen) {
                            nextString = "下10条...";
                        } else {
                            nextString = "已经是最后一页";
                        }
                        item2.put("nextString", nextString);
                        CyclicGoodsJinJiaJieGuo2.this.mItemArrayList.add(item2);
                    }
                }
            }
            CyclicGoodsJinJiaJieGuo2.this.mMyAdapter.notifyDataSetChanged();
        }
    };
    /* access modifiers changed from: private */
    public String isShowDealMember = StringEx.Empty;
    /* access modifiers changed from: private */
    public String isShowDealPrice = StringEx.Empty;
    /* access modifiers changed from: private */
    public String isShowDealResult = StringEx.Empty;
    /* access modifiers changed from: private */
    public String jjms = StringEx.Empty;
    /* access modifiers changed from: private */
    public String kssj = StringEx.Empty;
    private RadioButton mIndexNavigation1;
    private RadioButton mIndexNavigation2;
    private RadioButton mIndexNavigation3;
    private RadioButton mIndexNavigation4;
    private RadioButton mIndexNavigation5;
    /* access modifiers changed from: private */
    public ArrayList<HashMap<String, Object>> mItemArrayList;
    /* access modifiers changed from: private */
    public MyAdapter mMyAdapter;
    private RadioGroup mRadioderGroup;
    /* access modifiers changed from: private */
    public int nowNum = 1;
    /* access modifiers changed from: private */
    public int numOfPageGet = 1;
    /* access modifiers changed from: private */
    public String ppid = StringEx.Empty;
    private String ppidGet = StringEx.Empty;
    /* access modifiers changed from: private */
    public String ppzt = StringEx.Empty;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    /* access modifiers changed from: private */
    public String zbhy = StringEx.Empty;
    private String zhongbiaodanwei = "中标单位:卖方不公开;";
    /* access modifiers changed from: private */
    public String zj = StringEx.Empty;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.activity_cyclic_goods_jin_jia_jie_guo2);
        ExitApplication.getInstance().addActivity(this);
        Intent intent = getIntent();
        this.ppidGet = intent.getExtras().getString("ppid");
        this.isShowDealResult = intent.getExtras().getString("isShowDealResult");
        this.isShowDealMember = intent.getExtras().getString("isShowDealMember");
        this.isShowDealPrice = intent.getExtras().getString("isShowDealPrice");
        this.CyclicGoodsJinJiaJieGuo2_first_item = (RelativeLayout) findViewById(R.id.CyclicGoodsJinJiaJieGuo2_first_item);
        this.first_item_text = (TextView) findViewById(R.id.first_item_text);
        this.first_item_text_jieguo = (TextView) findViewById(R.id.first_item_text_jieguo);
        if (this.isShowDealResult.equals("0")) {
            this.chengjiaojieguo = StringEx.Empty;
        } else if (this.isShowDealResult.equals("1")) {
            this.chengjiaojieguo = "成交结果:卖方不公开;";
        }
        if (this.isShowDealMember.equals("0")) {
            this.zhongbiaodanwei = StringEx.Empty;
        } else if (this.isShowDealMember.equals("1")) {
            this.zhongbiaodanwei = "中标单位:卖方不公开;";
        }
        if (this.isShowDealPrice.equals("0")) {
            this.chengjiaojia = StringEx.Empty;
        } else if (this.isShowDealPrice.equals("1")) {
            this.chengjiaojia = "成交价:卖方不公开;";
        }
        this.allResultString = String.valueOf(this.chengjiaojieguo) + this.chengjiaojia + this.zhongbiaodanwei;
        if (!this.isShowDealResult.equals("0") || !this.isShowDealMember.equals("0") || !this.isShowDealPrice.equals("0")) {
            this.CyclicGoodsJinJiaJieGuo2_first_item.setVisibility(0);
            this.CyclicGoodsJinJiaJieGuo2_first_item.setBackgroundColor(Color.parseColor("#f1f1f1"));
            this.first_item_text.setPadding(30, 10, 0, 0);
            this.first_item_text.setText("本场竞价:");
            this.first_item_text_jieguo.setPadding(0, 10, 0, 20);
            this.first_item_text_jieguo.setText(this.allResultString);
        } else {
            this.first_item_text.setText(StringEx.Empty);
            this.first_item_text_jieguo.setText(StringEx.Empty);
            this.first_item_text.setHeight(0);
            this.first_item_text.setWidth(0);
            this.first_item_text_jieguo.setWidth(0);
            this.first_item_text_jieguo.setHeight(0);
            this.CyclicGoodsJinJiaJieGuo2_first_item.setVisibility(4);
        }
        this.CyclicGoodsJingJiaJieGuo2_listview = (ListView) findViewById(R.id.CyclicGoodsJingJiaJieGuo2_listview);
        this.mItemArrayList = new ArrayList<>();
        this.mMyAdapter = new MyAdapter(this);
        this.CyclicGoodsJingJiaJieGuo2_listview.setAdapter((ListAdapter) this.mMyAdapter);
        this.CyclicGoodsJingJiaJieGuo2_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                if (arg2 == CyclicGoodsJinJiaJieGuo2.this.mItemArrayList.size() - 1 && CyclicGoodsJinJiaJieGuo2.this.hasNextTen) {
                    CyclicGoodsJinJiaJieGuo2.this.mItemArrayList.remove(arg2);
                    CyclicGoodsJinJiaJieGuo2.this.getNextTen();
                }
            }
        });
        this.mRadioderGroup = (RadioGroup) findViewById(R.id.cyclic_goods_index_RGroup);
        this.mIndexNavigation1 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item1);
        this.mIndexNavigation2 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item2);
        this.mIndexNavigation3 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item3);
        this.mIndexNavigation4 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item4);
        this.mIndexNavigation5 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item5);
        this.mIndexNavigation1.setChecked(true);
        this.mRadioderGroup.setOnCheckedChangeListener(this);
        this.mIndexNavigation1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ExitApplication.getInstance().startActivity(CyclicGoodsJinJiaJieGuo2.this, CyclicGoodsIndexActivity.class);
            }
        });
        SmallNumUtil.JJNum(this, (TextView) findViewById(R.id.jj_dhqrnum));
        getListData(1);
    }

    public void backButtonAction(View v) {
        finish();
    }

    public void onBackPressed() {
        finish();
    }

    public void getListData(int num) {
        RequestBidData requestBidData = new RequestBidData(this);
        requestBidData.setRequestData("竞价结果列表2");
        requestBidData.setPageNum(new StringBuilder(String.valueOf(num)).toString());
        requestBidData.setId(this.ppidGet);
        this.nowNum = num;
        Log.d("请求的指令：", requestBidData.infoDate());
        requestBidData.setHttpCallBack(this.httpCallBack);
        requestBidData.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    /* access modifiers changed from: private */
    public void getNextTen() {
        this.nowNum++;
        getListData(this.nowNum);
    }

    public void onCheckedChanged(RadioGroup arg0, int arg1) {
        switch (arg1) {
            case R.id.cyclicgoods_index_navigation_item2:
                this.mIndexNavigation2.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item3:
                this.mIndexNavigation3.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidSessionSelect.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item1:
                this.mIndexNavigation1.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item4:
                this.mIndexNavigation4.setChecked(true);
                startActivity(new Intent(this, CyclicGoodsBidHallPageActivity.class));
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item5:
                this.mIndexNavigation5.setChecked(true);
                Intent intent = new Intent(this, Xhjy_more.class);
                intent.putExtra("part", "循环物资");
                startActivity(intent);
                finish();
                return;
            default:
                return;
        }
    }

    public class ViewHolder {
        public TextView jingjia_jieguo_chengjiaojia2;
        public TextView jingjia_jieguo_danwei2;
        public TextView jingjia_jieguo_jeiguo2;
        public TextView jingjia_jieguo_jine2;
        public TextView jingjia_jieguo_pinpanhao2;
        public TextView jingjia_jieguo_riqi2;
        public TextView jingjia_jieguo_zengjia;
        public TextView jingjia_jieguo_zengjia2;
        public TextView nextTen;

        public ViewHolder() {
        }
    }

    public class MyAdapter extends BaseAdapter {
        private LayoutInflater mMyAdapterInflater;

        public MyAdapter(Context context) {
            this.mMyAdapterInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return CyclicGoodsJinJiaJieGuo2.this.mItemArrayList.size();
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (position == CyclicGoodsJinJiaJieGuo2.this.mItemArrayList.size() - 1) {
                ViewHolder holder1 = new ViewHolder();
                View convertView2 = this.mMyAdapterInflater.inflate((int) R.layout.next_ten, (ViewGroup) null);
                holder1.nextTen = (TextView) convertView2.findViewById(R.id.next_ten_text);
                convertView2.setTag(holder1);
                holder1.nextTen.setText((String) ((HashMap) CyclicGoodsJinJiaJieGuo2.this.mItemArrayList.get(position)).get("nextString"));
                return convertView2;
            }
            ViewHolder holder = new ViewHolder();
            View convertView3 = this.mMyAdapterInflater.inflate((int) R.layout.jingjia_jieguo2_item, (ViewGroup) null);
            holder.jingjia_jieguo_pinpanhao2 = (TextView) convertView3.findViewById(R.id.jingjia_jieguo_pinpanhao2);
            holder.jingjia_jieguo_jeiguo2 = (TextView) convertView3.findViewById(R.id.jingjia_jieguo_jeiguo2);
            holder.jingjia_jieguo_zengjia = (TextView) convertView3.findViewById(R.id.jingjia_jieguo_zengjia);
            holder.jingjia_jieguo_zengjia2 = (TextView) convertView3.findViewById(R.id.jingjia_jieguo_zengjia2);
            holder.jingjia_jieguo_chengjiaojia2 = (TextView) convertView3.findViewById(R.id.jingjia_jieguo_chengjiaojia2);
            holder.jingjia_jieguo_jine2 = (TextView) convertView3.findViewById(R.id.jingjia_jieguo_jine2);
            holder.jingjia_jieguo_riqi2 = (TextView) convertView3.findViewById(R.id.jingjia_jieguo_riqi2);
            holder.jingjia_jieguo_danwei2 = (TextView) convertView3.findViewById(R.id.jingjia_jieguo_danwei2);
            String ppid = (String) ((HashMap) CyclicGoodsJinJiaJieGuo2.this.mItemArrayList.get(position)).get("ppid");
            String ppzt = (String) ((HashMap) CyclicGoodsJinJiaJieGuo2.this.mItemArrayList.get(position)).get("ppzt");
            String jjms = (String) ((HashMap) CyclicGoodsJinJiaJieGuo2.this.mItemArrayList.get(position)).get("jjms");
            String bpfs = (String) ((HashMap) CyclicGoodsJinJiaJieGuo2.this.mItemArrayList.get(position)).get("bpfs");
            String cjj = (String) ((HashMap) CyclicGoodsJinJiaJieGuo2.this.mItemArrayList.get(position)).get("cjj");
            String zbhy = (String) ((HashMap) CyclicGoodsJinJiaJieGuo2.this.mItemArrayList.get(position)).get("zbhy");
            String kssj = (String) ((HashMap) CyclicGoodsJinJiaJieGuo2.this.mItemArrayList.get(position)).get("kssj");
            String zj = (String) ((HashMap) CyclicGoodsJinJiaJieGuo2.this.mItemArrayList.get(position)).get("zj");
            if (ppid.equals(null) || ppid.equals("null") || ppid.equals(StringEx.Empty)) {
                holder.jingjia_jieguo_pinpanhao2.setText(StringEx.Empty);
            } else {
                holder.jingjia_jieguo_pinpanhao2.setText(ppid);
            }
            if (CyclicGoodsJinJiaJieGuo2.this.isShowDealResult.equals("1")) {
                holder.jingjia_jieguo_jeiguo2.setText("--");
            } else if (CyclicGoodsJinJiaJieGuo2.this.isShowDealResult.equals("0")) {
                if (ppzt.equals(null) || ppzt.equals("null") || ppzt.equals(StringEx.Empty)) {
                    holder.jingjia_jieguo_jeiguo2.setText(StringEx.Empty);
                } else {
                    holder.jingjia_jieguo_jeiguo2.setText(ppzt);
                }
            }
            if (jjms.equals(null) || jjms.equals("null") || jjms.equals(StringEx.Empty)) {
                holder.jingjia_jieguo_zengjia.setText(StringEx.Empty);
            } else {
                holder.jingjia_jieguo_zengjia.setText(String.valueOf(jjms) + PNXConfigConstant.RESP_SPLIT_3);
            }
            if (bpfs.equals(null) || bpfs.equals("null") || bpfs.equals(StringEx.Empty)) {
                holder.jingjia_jieguo_zengjia2.setText(StringEx.Empty);
            } else {
                holder.jingjia_jieguo_zengjia2.setText(bpfs);
            }
            if (CyclicGoodsJinJiaJieGuo2.this.isShowDealPrice.equals("1")) {
                holder.jingjia_jieguo_chengjiaojia2.setText("--");
                holder.jingjia_jieguo_jine2.setText("--");
            } else if (CyclicGoodsJinJiaJieGuo2.this.isShowDealPrice.equals("0")) {
                if (cjj.equals(null) || cjj.equals("null") || cjj.equals(StringEx.Empty)) {
                    holder.jingjia_jieguo_chengjiaojia2.setText(StringEx.Empty);
                } else {
                    holder.jingjia_jieguo_chengjiaojia2.setText(cjj);
                }
                if (zj.equals(null) || zj.equals("null") || zj.equals(StringEx.Empty)) {
                    holder.jingjia_jieguo_jine2.setText(StringEx.Empty);
                } else {
                    holder.jingjia_jieguo_jine2.setText(zj);
                }
            }
            if (kssj.equals(null) || kssj.equals("null") || kssj.equals(StringEx.Empty)) {
                holder.jingjia_jieguo_riqi2.setText(StringEx.Empty);
            } else {
                holder.jingjia_jieguo_riqi2.setText(kssj);
            }
            if (CyclicGoodsJinJiaJieGuo2.this.isShowDealMember.equals("1")) {
                holder.jingjia_jieguo_danwei2.setText("--");
            } else if (CyclicGoodsJinJiaJieGuo2.this.isShowDealMember.equals("0")) {
                if (zbhy.equals(null) || zbhy.equals("null") || zbhy.equals(StringEx.Empty)) {
                    holder.jingjia_jieguo_danwei2.setText(StringEx.Empty);
                } else {
                    holder.jingjia_jieguo_danwei2.setText(zbhy);
                }
            }
            convertView3.setTag(holder);
            return convertView3;
        }
    }
}
