package com.baosteelOnline.xhjy;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.R;
import com.baosteelOnline.common.DataBaseFactory;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.common.jqhttp.CustomDialog;
import com.baosteelOnline.common.jqhttp.UpdateHelper;
import com.baosteelOnline.common.jqhttp.Utils;
import com.baosteelOnline.data.Msg_queryHYinfoBusi;
import com.baosteelOnline.data.Msg_queryMobilemainBusi;
import com.baosteelOnline.data.RequestBidData;
import com.baosteelOnline.data_parse.WelcomeParse;
import com.baosteelOnline.image.AsyncImageLoader;
import com.baosteelOnline.image.FileCache;
import com.baosteelOnline.image.ImageAndUrl;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.sql.DBHelper;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

public class CyclicGoodsIndexActivity extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    private ViewPagerAdapter adapter;
    AsyncImageLoader asyncImageLoader;
    private String bIndex = StringEx.Empty;
    private ImageButton closeBtn;
    /* access modifiers changed from: private */
    public int currentItem;
    /* access modifiers changed from: private */
    public ImageView cyclic_small1;
    /* access modifiers changed from: private */
    public ImageView cyclic_small2;
    /* access modifiers changed from: private */
    public ImageView cyclic_small3;
    private String czm = StringEx.Empty;
    private DataBaseFactory db;
    /* access modifiers changed from: private */
    public DBHelper dbHelper;
    /* access modifiers changed from: private */
    public TextView dhqrnum;
    /* access modifiers changed from: private */
    public ArrayList<View> dots = new ArrayList<>();
    private SharedPreferences.Editor editor;
    /* access modifiers changed from: private */
    public SharedPreferences.Editor editor2;
    /* access modifiers changed from: private */
    public SharedPreferences.Editor editorfirstSharedPreferences;
    String filePath = StringEx.Empty;
    /* access modifiers changed from: private */
    public SharedPreferences firstSharedPreferences;
    private TextView gsTextview;
    UpdateHelper helper;
    private JQUICallBack httpCallBack = new JQUICallBack() {
        public void callBack(ResultData result) {
            Log.d("获取的数据：", result.getStringData());
        }
    };
    private String hyh = StringEx.Empty;
    List<ImageAndUrl> imageInfosShow = new ArrayList();
    List<ImageView> images;
    List<String> imagesur2 = new ArrayList();
    List<String> imagesurl = new ArrayList();
    private String index = StringEx.Empty;
    private JQUICallBack index_httpCallBack = new JQUICallBack() {
        public void callBack(ResultData result) {
            CyclicGoodsIndexActivity.this.getDate(WelcomeParse.parse(result.getStringData()));
        }
    };
    private String index_more = StringEx.Empty;
    private View indexview;
    /* access modifiers changed from: private */
    public LayoutInflater inflater;
    private boolean isLogin = false;
    public RelativeLayout layout1;
    /* access modifiers changed from: private */
    public Handler mHandler;
    private RadioButton mIndexNavigation1;
    private RadioButton mIndexNavigation2;
    private RadioButton mIndexNavigation3;
    private RadioButton mIndexNavigation4;
    private RadioButton mIndexNavigation5;
    private Message mMsg;
    private RadioGroup mRadioderGroup;
    private Runnable mRunnable = new Runnable() {
        public void run() {
            if (CyclicGoodsIndexActivity.this.firstSharedPreferences.getString("firstTimeEver", "YES").equals("YES") || CyclicGoodsIndexActivity.this.firstSharedPreferences.getString("firstTimeEver", "YES").equals(StringEx.Empty) || CyclicGoodsIndexActivity.this.firstSharedPreferences.getString("firstTimeEver", "YES").equals(null)) {
                CyclicGoodsIndexActivity.this.yindao.showAsDropDown(CyclicGoodsIndexActivity.this.textView);
                CyclicGoodsIndexActivity.this.editorfirstSharedPreferences.putString("firstTimeEver", "NO");
                CyclicGoodsIndexActivity.this.editorfirstSharedPreferences.commit();
            }
        }
    };
    private JQUICallBack mob_httpCallBack = new JQUICallBack() {
        public void callBack(ResultData result) {
            CyclicGoodsIndexActivity.this.getMobDate(WelcomeParse.parse(result.getStringData()));
        }
    };
    /* access modifiers changed from: private */
    public int oldPosition = 0;
    private String pushMessage = StringEx.Empty;
    private ScheduledExecutorService scheduledExecutor;
    private ScheduledExecutorService scheduledExecutorService;
    private SharedPreferences sp;
    private SharedPreferences sp2;
    /* access modifiers changed from: private */
    public TextView textView;
    /* access modifiers changed from: private */
    public DialogInterface.OnClickListener updateCancelExitListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            CyclicGoodsIndexActivity.this.clearAll();
            ExitApplication.getInstance().exit();
            CyclicGoodsIndexActivity.this.finish();
        }
    };
    /* access modifiers changed from: private */
    public DialogInterface.OnClickListener updateCancelListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
        }
    };
    String updateUrl = StringEx.Empty;
    /* access modifiers changed from: private */
    public String updateflag = StringEx.Empty;
    String updatename = StringEx.Empty;
    String updateurl = StringEx.Empty;
    /* access modifiers changed from: private */
    public ViewPager viewPager;
    /* access modifiers changed from: private */
    public PopupWindow warmingInfo;
    private TextView warmingInfoTextView;
    private View warmingInfoView;
    private TextView welcomText1;
    int[] welcomeDate;
    /* access modifiers changed from: private */
    public TextView welcomeText;
    private ImageView ydImage;
    /* access modifiers changed from: private */
    public PopupWindow yindao;
    private View yingDaoView;

    /* access modifiers changed from: protected */
    @SuppressLint({"SetJavaScriptEnabled"})
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.activity_cyclic_goods_index);
        ExitApplication.getInstance().addActivity(this);
        this.dbHelper = DataBaseFactory.getInstance(this);
        this.dbHelper.insertOperation("循环物资", "循环资源首页", "循环资源首页");
        this.layout1 = (RelativeLayout) findViewById(R.id.cycic_pop_view);
        this.asyncImageLoader = new AsyncImageLoader();
        this.viewPager = (ViewPager) findViewById(R.id.vp);
        this.viewPager.setOnPageChangeListener(new MyOnPageChangeListener(this, null));
        this.sp2 = getSharedPreferences("setDate", 2);
        this.editor2 = this.sp2.edit();
        this.inflater = getLayoutInflater();
        ObjectStores.getInst().putObject("formX", "formY");
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/fzjl.ttf");
        this.gsTextview = (TextView) findViewById(R.id.index_gs_text);
        this.welcomeText = (TextView) findViewById(R.id.index_welcome_text);
        this.welcomText1 = (TextView) findViewById(R.id.index_welcome_text1);
        if (ObjectStores.getInst().getObject("companyFullName") == null || ObjectStores.getInst().getObject("companyFullName").equals(StringEx.Empty)) {
            this.welcomText1.setVisibility(0);
            this.welcomText1.setText("亲，欢迎您！");
            this.welcomeText.setVisibility(8);
            this.gsTextview.setVisibility(8);
        } else {
            this.welcomText1.setVisibility(8);
            this.gsTextview.setVisibility(0);
            this.welcomeText.setVisibility(0);
            this.welcomeText.setText(getString(R.string.index_welcome));
            this.gsTextview.setText(ObjectStores.getInst().getObject("companyFullName").toString());
        }
        this.welcomText1.setTypeface(face);
        this.gsTextview.setTypeface(face);
        this.welcomeText.setTypeface(face);
        this.textView = (TextView) findViewById(R.id.yingdao_text);
        this.cyclic_small1 = (ImageView) findViewById(R.id.cyclic_small1);
        this.cyclic_small2 = (ImageView) findViewById(R.id.cyclic_small2);
        this.cyclic_small3 = (ImageView) findViewById(R.id.cyclic_small3);
        this.mRadioderGroup = (RadioGroup) findViewById(R.id.cyclic_goods_index_RGroup);
        this.mIndexNavigation1 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item1);
        this.mIndexNavigation2 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item2);
        this.mIndexNavigation3 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item3);
        this.mIndexNavigation4 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item4);
        this.mIndexNavigation5 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item5);
        mobBusi();
        testBusi();
        this.firstSharedPreferences = getSharedPreferences("firstSharedPreferences", 2);
        this.editorfirstSharedPreferences = this.firstSharedPreferences.edit();
        this.yingDaoView = this.inflater.inflate((int) R.layout.cyclicgoods_index_yingdao, (ViewGroup) null);
        this.ydImage = (ImageView) this.yingDaoView.findViewById(R.id.close_imageview);
        this.warmingInfoView = this.inflater.inflate((int) R.layout.warming_nfo, (ViewGroup) null);
        this.warmingInfoTextView = (TextView) this.warmingInfoView.findViewById(R.id.close_textview);
        this.closeBtn = (ImageButton) this.warmingInfoView.findViewById(R.id.btn_close);
        this.warmingInfoTextView.setText(Html.fromHtml("<font color='#ffffff'>&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;东方钢铁在线手机版客户端风险提示<br><br> 尊敬的客户:<br>&nbsp;&nbsp;&nbsp;&nbsp;请您在使用手机版东方钢铁在线客户端之前，仔细阅读以下有关东方钢铁在线手机版客户端风险提示:<br>&nbsp;&nbsp;&nbsp;&nbsp;1、手机客户端主要通过wifi无线网络、移动互联网等公共网络进行数据传输，基于移动互联网的自身特点，存在网络故障、通讯问题以及安全问题等风险。<br>&nbsp;&nbsp;&nbsp;&nbsp;2、通过本手机客户端发起报价、进行交易，客户应注意智能手机操作方式不同于PC端的操作方式，避免在使用时出现操作失误。<br>&nbsp;&nbsp;&nbsp;&nbsp;3、提醒各位客户，东方钢铁在线对于本手机客户端因客户手机的网络故障、通信延迟以及客户操作错误而导致的损失恕不承担责，客户可及时联系东方钢铁在线予以协助处理。<br></font> <font color='#EE3B3B'>&nbsp;&nbsp;&nbsp;&nbsp;以上手机版客户端风险提示，本单位（本人）已阅读，并完全理解，本人是经慎重考虑过后自愿下载使用，由此引起的风险由本人自行负责。</font>"));
        this.warmingInfo = new PopupWindow(this.warmingInfoView, -1, -1);
        this.yindao = new PopupWindow(this.yingDaoView, -1, -1);
        this.ydImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (CyclicGoodsIndexActivity.this.yindao.isShowing()) {
                    CyclicGoodsIndexActivity.this.yindao.dismiss();
                    CyclicGoodsIndexActivity.this.warmingInfo.showAtLocation(CyclicGoodsIndexActivity.this.welcomeText, 1, 0, 0);
                }
            }
        });
        this.closeBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (CyclicGoodsIndexActivity.this.warmingInfo.isShowing()) {
                    CyclicGoodsIndexActivity.this.warmingInfo.dismiss();
                }
            }
        });
        try {
            Log.d("firstUseOrNot", "firstUseOrNot=" + ObjectStores.getInst().getObject("firstUseOrNot").toString());
            if (ObjectStores.getInst().getObject("firstUseOrNot") == "YES") {
                ObjectStores.getInst().putObject("firstUseOrNot", "NO");
            }
        } catch (Exception e) {
            ObjectStores.getInst().putObject("firstUseOrNot", "NO");
        }
        this.mHandler = new Handler() {
            public void handleMessage(Message msg) {
                if (msg.what == 2) {
                    CyclicGoodsIndexActivity.this.helper.update(CyclicGoodsIndexActivity.this.updateUrl, CyclicGoodsIndexActivity.this.filePath, CyclicGoodsIndexActivity.this.updateCancelListener, CyclicGoodsIndexActivity.this.updateflag);
                } else if (msg.what == 3) {
                    CyclicGoodsIndexActivity.this.warmingInfo.showAtLocation(CyclicGoodsIndexActivity.this.welcomeText, 1, 0, 0);
                } else if (msg.what == 4) {
                    CyclicGoodsIndexActivity.this.helper.updateMandatory(CyclicGoodsIndexActivity.this.updateUrl, CyclicGoodsIndexActivity.this.filePath, CyclicGoodsIndexActivity.this.updateCancelExitListener, CyclicGoodsIndexActivity.this.updateflag);
                } else if (msg.what == 5) {
                    SharedPreferences wsp = CyclicGoodsIndexActivity.this.getSharedPreferences("welcome", 2);
                    SharedPreferences.Editor editor = wsp.edit();
                    if (!wsp.getBoolean("isClick", false)) {
                        CyclicGoodsIndexActivity.this.cyclic_small1.setVisibility(0);
                        CyclicGoodsIndexActivity.this.cyclic_small2.setVisibility(0);
                        CyclicGoodsIndexActivity.this.cyclic_small3.setVisibility(0);
                    } else if (wsp.getInt("index_jjgg", -1) == -1) {
                        editor.putInt("index_jjgg", CyclicGoodsIndexActivity.this.welcomeDate[1]);
                        editor.putInt("index_jyyg", CyclicGoodsIndexActivity.this.welcomeDate[2]);
                        editor.putInt("index_ptgg", CyclicGoodsIndexActivity.this.welcomeDate[0]);
                        editor.commit();
                        CyclicGoodsIndexActivity.this.cyclic_small1.setVisibility(0);
                        CyclicGoodsIndexActivity.this.cyclic_small2.setVisibility(0);
                        CyclicGoodsIndexActivity.this.cyclic_small3.setVisibility(0);
                    } else {
                        if (CyclicGoodsIndexActivity.this.welcomeDate[1] > wsp.getInt("index_jjgg", -1)) {
                            CyclicGoodsIndexActivity.this.cyclic_small1.setVisibility(0);
                        } else {
                            CyclicGoodsIndexActivity.this.cyclic_small1.setVisibility(8);
                        }
                        if (CyclicGoodsIndexActivity.this.welcomeDate[2] > wsp.getInt("index_jyyg", -1)) {
                            CyclicGoodsIndexActivity.this.cyclic_small2.setVisibility(0);
                        } else {
                            CyclicGoodsIndexActivity.this.cyclic_small2.setVisibility(8);
                        }
                        if (CyclicGoodsIndexActivity.this.welcomeDate[0] > wsp.getInt("index_ptgg", -1)) {
                            CyclicGoodsIndexActivity.this.cyclic_small3.setVisibility(0);
                        } else {
                            CyclicGoodsIndexActivity.this.cyclic_small3.setVisibility(8);
                        }
                    }
                    int num = CyclicGoodsIndexActivity.this.welcomeDate[3];
                    SharedPreferences.Editor jjed = CyclicGoodsIndexActivity.this.getSharedPreferences("smallnum", 2).edit();
                    jjed.putInt("jjnum", num);
                    jjed.commit();
                    System.out.println("num==>" + num);
                    CyclicGoodsIndexActivity.this.dhqrnum = (TextView) CyclicGoodsIndexActivity.this.findViewById(R.id.jj_dhqrnum);
                    CyclicGoodsIndexActivity.this.dhqrnum.setGravity(17);
                    CyclicGoodsIndexActivity.this.dhqrnum.setText(new StringBuilder(String.valueOf(num)).toString());
                    if (num != 0) {
                        CyclicGoodsIndexActivity.this.dhqrnum.setVisibility(0);
                    } else {
                        CyclicGoodsIndexActivity.this.dhqrnum.setVisibility(8);
                    }
                } else if (msg.what == 6) {
                    CyclicGoodsIndexActivity.this.getDatas();
                } else if (msg.what == 1) {
                    Animation loadAnimation = AnimationUtils.loadAnimation(CyclicGoodsIndexActivity.this, R.anim.enter);
                    CyclicGoodsIndexActivity.this.viewPager.setCurrentItem(CyclicGoodsIndexActivity.this.currentItem);
                }
                super.handleMessage(msg);
            }
        };
        this.sp = getSharedPreferences("BaoIndex", 2);
        this.editor = this.sp.edit();
        setData();
        this.mIndexNavigation1.setChecked(true);
        this.mRadioderGroup.setOnCheckedChangeListener(this);
        this.mHandler.postDelayed(this.mRunnable, 500);
    }

    /* access modifiers changed from: private */
    public void getDatas() {
        for (int i = 0; i < this.imagesurl.size(); i++) {
            ImageAndUrl item = new ImageAndUrl();
            item.setImageurl(this.imagesurl.get(i));
            item.setUrl("item-->" + i);
            this.imageInfosShow.add(item);
        }
        this.images = new ArrayList();
        for (int i2 = 0; i2 < this.imageInfosShow.size(); i2++) {
            this.images.add(new ImageView(this));
        }
        initDots();
        this.adapter = new ViewPagerAdapter(this, null);
        this.viewPager.setAdapter(this.adapter);
        vpChangeTask();
    }

    private void initDots() {
        LinearLayout ll_vp_point = (LinearLayout) findViewById(R.id.ll_vp_point);
        for (int i = 0; i < this.imageInfosShow.size(); i++) {
            ImageView iv_image = (ImageView) this.inflater.inflate((int) R.layout.item_dots, (ViewGroup) null);
            if (i == 0) {
                iv_image.setBackgroundResource(R.drawable.dxt_point_selected);
            }
            iv_image.setTag(this.imageInfosShow.get(i));
            ll_vp_point.addView(iv_image);
            this.dots.add(iv_image);
        }
    }

    private void vpChangeTask() {
        this.scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        this.scheduledExecutorService.scheduleWithFixedDelay(new ViewPagerTask(this, null), 5, 5, TimeUnit.SECONDS);
    }

    private class ViewPagerTask implements Runnable {
        private ViewPagerTask() {
        }

        /* synthetic */ ViewPagerTask(CyclicGoodsIndexActivity cyclicGoodsIndexActivity, ViewPagerTask viewPagerTask) {
            this();
        }

        public void run() {
            CyclicGoodsIndexActivity.this.currentItem = (CyclicGoodsIndexActivity.this.currentItem + 1) % CyclicGoodsIndexActivity.this.imageInfosShow.size();
            Message message = new Message();
            message.what = 1;
            CyclicGoodsIndexActivity.this.mHandler.sendMessage(message);
        }
    }

    private class ViewPagerAdapter extends PagerAdapter {
        private ViewPagerAdapter() {
        }

        /* synthetic */ ViewPagerAdapter(CyclicGoodsIndexActivity cyclicGoodsIndexActivity, ViewPagerAdapter viewPagerAdapter) {
            this();
        }

        public void destroyItem(View container, int position, Object object) {
            ((ViewPager) container).removeView(CyclicGoodsIndexActivity.this.images.get(position));
        }

        public int getCount() {
            return CyclicGoodsIndexActivity.this.imageInfosShow.size();
        }

        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }

        public Object instantiateItem(View view, int position) {
            View imageLayout = CyclicGoodsIndexActivity.this.inflater.inflate((int) R.layout.item_pager_image, (ViewGroup) null);
            final ImageView imageView = (ImageView) imageLayout.findViewById(R.id.image);
            final String imgUrl = CyclicGoodsIndexActivity.this.imageInfosShow.get(position).getImageurl();
            final String ggid = CyclicGoodsIndexActivity.this.imagesur2.get(position);
            imageView.setTag(imgUrl);
            imageView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent intent = new Intent(CyclicGoodsIndexActivity.this, CyclicGoodsBidPageActivity.class);
                    SharedPreferences.Editor editor = CyclicGoodsIndexActivity.this.getSharedPreferences("indextz", 2).edit();
                    editor.putBoolean("cyclic", true);
                    editor.commit();
                    intent.putExtra("id", ggid);
                    intent.putExtra("activity", "竞价公告");
                    CyclicGoodsIndexActivity.this.startActivity(intent);
                }

                private SharedPreferences getPreferences(String string, int modeWorldWriteable) {
                    return null;
                }
            });
            if (CyclicGoodsIndexActivity.this.checkConnection()) {
                Bitmap bmpFromSD = FileCache.getInstance().getBmp(imgUrl);
                if (bmpFromSD != null) {
                    imageView.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
                    imageView.setImageBitmap(bmpFromSD);
                } else {
                    Drawable cachedImage = CyclicGoodsIndexActivity.this.asyncImageLoader.loaDrawable(imgUrl, new AsyncImageLoader.ImageCallBack() {
                        public void imageLoaded(Drawable imageDrawable) {
                            Bitmap bitmap = CyclicGoodsIndexActivity.this.drawToBmp(imageDrawable);
                            FileCache.getInstance().savaBmpData(imgUrl, bitmap);
                            ImageView imageViewByTag = null;
                            if (bitmap != null) {
                                imageViewByTag = (ImageView) imageView.findViewWithTag(imgUrl);
                                imageViewByTag.setLayoutParams(new LinearLayout.LayoutParams(-2, bitmap.getHeight()));
                            }
                            if (imageViewByTag == null) {
                                return;
                            }
                            if (CyclicGoodsIndexActivity.this.isWifi(CyclicGoodsIndexActivity.this)) {
                                imageViewByTag.setImageBitmap(bitmap);
                            } else if (bitmap != null) {
                                imageViewByTag.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                                imageViewByTag.setImageBitmap(bitmap);
                                imageViewByTag.setScaleType(ImageView.ScaleType.MATRIX);
                            }
                        }
                    });
                    if (cachedImage == null) {
                        imageView.setImageResource(R.drawable.index_image_bg);
                    } else if (CyclicGoodsIndexActivity.this.isWifi(CyclicGoodsIndexActivity.this)) {
                        Bitmap bitmap = CyclicGoodsIndexActivity.this.drawToBmp(cachedImage);
                        imageView.setLayoutParams(new LinearLayout.LayoutParams(-2, bitmap.getHeight()));
                        imageView.setImageBitmap(bitmap);
                    } else {
                        imageView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                        imageView.setImageBitmap(CyclicGoodsIndexActivity.this.drawToBmp(cachedImage));
                    }
                }
            } else {
                Bitmap bmpFromSD2 = FileCache.getInstance().getBmp(imgUrl);
                if (bmpFromSD2 != null) {
                    ImageView imageViewByTag = (ImageView) imageView.findViewWithTag(imgUrl);
                    imageViewByTag.setLayoutParams(new LinearLayout.LayoutParams(-2, bmpFromSD2.getHeight()));
                    imageViewByTag.setImageBitmap(bmpFromSD2);
                } else {
                    imageView.setImageResource(R.drawable.index_image_bg);
                }
            }
            ((ViewPager) view).addView(imageLayout, 0);
            return imageLayout;
        }
    }

    public Bitmap drawToBmp(Drawable d) {
        if (d != null) {
            return ((BitmapDrawable) d).getBitmap();
        }
        return null;
    }

    public boolean checkConnection() {
        NetworkInfo networkInfo = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
        if (networkInfo != null) {
            return networkInfo.isAvailable();
        }
        return false;
    }

    public boolean isWifi(Context mContext) {
        NetworkInfo activeNetInfo = ((ConnectivityManager) mContext.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetInfo == null || !activeNetInfo.getTypeName().equals("WIFI")) {
            return false;
        }
        return true;
    }

    private class MyOnPageChangeListener implements ViewPager.OnPageChangeListener {
        private MyOnPageChangeListener() {
        }

        /* synthetic */ MyOnPageChangeListener(CyclicGoodsIndexActivity cyclicGoodsIndexActivity, MyOnPageChangeListener myOnPageChangeListener) {
            this();
        }

        public void onPageScrollStateChanged(int arg0) {
        }

        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        public void onPageSelected(int position) {
            ((View) CyclicGoodsIndexActivity.this.dots.get(CyclicGoodsIndexActivity.this.oldPosition)).setBackgroundResource(R.drawable.dxt_point_nomral);
            ((View) CyclicGoodsIndexActivity.this.dots.get(position)).setBackgroundResource(R.drawable.dxt_point_selected);
            CyclicGoodsIndexActivity.this.oldPosition = position;
            CyclicGoodsIndexActivity.this.currentItem = position;
        }
    }

    public void mobBusi() {
        Msg_queryMobilemainBusi infoBusi = new Msg_queryMobilemainBusi(this);
        infoBusi.setHttpCallBack(this.mob_httpCallBack);
        infoBusi.iExecute();
    }

    /* access modifiers changed from: private */
    public void getMobDate(WelcomeParse searchParse) {
        if (WelcomeParse.commonData != null && WelcomeParse.commonData.blocks != null && WelcomeParse.commonData.blocks.r0 != null && WelcomeParse.commonData.blocks.r0.mar != null && WelcomeParse.commonData.blocks.r0.mar.rows != null && WelcomeParse.commonData.blocks.r0.mar.rows.rows.size() != 0 && WelcomeParse.commonData != null && WelcomeParse.commonData.blocks != null && WelcomeParse.commonData.blocks.r0 != null && WelcomeParse.commonData.blocks.r0.mar != null && WelcomeParse.commonData.blocks.r0.mar.rows != null && WelcomeParse.commonData.blocks.r0.mar.rows.rows != null) {
            int j = WelcomeParse.commonData.blocks.r0.mar.rows.rows.size();
            System.out.println("ItemInfo---size: " + j);
            for (int i = 0; i < j; i++) {
                ArrayList<String> rowdata = WelcomeParse.commonData.blocks.r0.mar.rows.rows.get(i);
                if (rowdata != null) {
                    for (int k = 0; k < rowdata.size(); k++) {
                        Log.i("welcome==============>WebImage" + k, (String) rowdata.get(k));
                        if (k == 0) {
                            this.imagesurl.add((String) rowdata.get(0));
                        } else if (k == 1) {
                            this.imagesur2.add((String) rowdata.get(1));
                        }
                    }
                }
            }
            Message message = new Message();
            message.what = 6;
            this.mHandler.sendMessage(message);
        }
    }

    public void testBusi() {
        Msg_queryHYinfoBusi infoBusi = new Msg_queryHYinfoBusi(this);
        if (ObjectStores.getInst().getObject("parameter_hy") == null) {
            infoBusi.parameter_hy = StringEx.Empty;
        } else {
            infoBusi.parameter_hy = ObjectStores.getInst().getObject("parameter_hy").toString();
        }
        infoBusi.zc = "2";
        infoBusi.setHttpCallBack(this.index_httpCallBack);
        infoBusi.iExecute();
    }

    /* access modifiers changed from: private */
    public void getDate(WelcomeParse searchParse) {
        if (WelcomeParse.commonData != null && WelcomeParse.commonData.blocks != null && WelcomeParse.commonData.blocks.r0 != null && WelcomeParse.commonData.blocks.r0.mar != null && WelcomeParse.commonData.blocks.r0.mar.rows != null && WelcomeParse.commonData.blocks.r0.mar.rows.rows.size() != 0 && WelcomeParse.commonData != null && WelcomeParse.commonData.blocks != null && WelcomeParse.commonData.blocks.r0 != null && WelcomeParse.commonData.blocks.r0.mar != null && WelcomeParse.commonData.blocks.r0.mar.rows != null && WelcomeParse.commonData.blocks.r0.mar.rows.rows != null) {
            int j = WelcomeParse.commonData.blocks.r0.mar.rows.rows.size();
            System.out.println("ItemInfo---size: " + j);
            this.welcomeDate = new int[5];
            for (int i = 0; i < j; i++) {
                ArrayList<String> rowdata = WelcomeParse.commonData.blocks.r0.mar.rows.rows.get(i);
                if (rowdata != null) {
                    for (int k = 0; k < rowdata.size(); k++) {
                        Log.i("welcome==============>" + k, (String) rowdata.get(k));
                        this.welcomeDate[k] = Integer.valueOf((String) rowdata.get(k)).intValue();
                    }
                }
            }
            Message message = new Message();
            message.what = 5;
            this.mHandler.sendMessage(message);
        }
    }

    public int getStatusBarHeight(Context context) {
        try {
            Class<?> c = Class.forName("com.android.internal.R$dimen");
            return context.getResources().getDimensionPixelSize(Integer.parseInt(c.getField("status_bar_height").get(c.newInstance()).toString()));
        } catch (Exception e1) {
            e1.printStackTrace();
            return 0;
        }
    }

    private void checkUpdate() {
        new Thread(new Runnable() {
            public void run() {
                CyclicGoodsIndexActivity.this.checkforupdate();
            }
        }).start();
    }

    public void onBackPressed() {
        new AlertDialog.Builder(this).setTitle((int) R.string.app_name).setIcon((int) R.drawable.icon).setMessage("您确认要退出吗？").setNegativeButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                CyclicGoodsIndexActivity.this.dbHelper.insertOperation("退出", "退出", "退出");
                CyclicGoodsIndexActivity.this.clearAll();
                CyclicGoodsIndexActivity.this.editor2.clear();
                CyclicGoodsIndexActivity.this.editor2.commit();
                ExitApplication.getInstance().exit();
                CyclicGoodsIndexActivity.this.finish();
            }
        }).setPositiveButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        }).show();
    }

    public void indexImtb1Action(View v) {
        SharedPreferences.Editor editor3 = getSharedPreferences("welcome", 2).edit();
        editor3.putBoolean("isClick", true);
        editor3.commit();
        HashMap hasmap = new HashMap();
        String index2 = getSharedPreferences("BaoIndex", 2).getString("bIndex", StringEx.Empty);
        hasmap.put("part", "循环物资");
        hasmap.put("numOfBtn", "平台公告按钮");
        System.out.println("index===" + index2);
        if (index2.equals("gcjy")) {
            hasmap.put("sign", "gcjjyg");
        }
        if (index2.equals("xhwz")) {
            hasmap.put("sign", "wzjjyg");
        }
        this.dbHelper.insertOperation("循环物资", "循环资源首页", "平台公告");
        ExitApplication.getInstance().startActivity(this, Xhjy_url_list.class, hasmap);
        finish();
    }

    public void indexImtb2Action(View v) {
        SharedPreferences.Editor editor3 = getSharedPreferences("welcome", 2).edit();
        editor3.putBoolean("isClick", true);
        editor3.commit();
        HashMap hasmap = new HashMap();
        String index2 = getSharedPreferences("BaoIndex", 2).getString("bIndex", StringEx.Empty);
        hasmap.put("part", "循环物资");
        hasmap.put("numOfBtn", "交易预告按钮");
        System.out.println("index===" + index2);
        if (index2.equals("gcjy")) {
            hasmap.put("sign", "gcjjyg");
        }
        if (index2.equals("xhwz")) {
            hasmap.put("sign", "wzjjyg");
        }
        this.dbHelper.insertOperation("循环物资", "循环资源首页", "交易预告");
        ExitApplication.getInstance().startActivity(this, Xhjy_url_list.class, hasmap);
        finish();
    }

    public void mjchangAction(View v) {
        this.index = "gcjy";
        this.index_more = "1";
        HashMap hasmap = new HashMap();
        hasmap.put("bIndex", this.bIndex);
        this.editor.putString("bIndex", this.index);
        this.editor.commit();
        hasmap.put("index_more", this.index_more);
        ObjectStores.getInst().putObject("formX", "formX");
        ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class, hasmap);
        overridePendingTransition(R.anim.danrdanchu, R.anim.danchudanr);
        finish();
    }

    public void indexImtb3Action(View v) {
        SharedPreferences.Editor editor3 = getSharedPreferences("welcome", 2).edit();
        editor3.putBoolean("isClick", true);
        editor3.commit();
        ExitApplication.getInstance().startActivity(this, CyclicGoodsBidActivity.class);
        finish();
    }

    public void indexImtb4Action(View v) {
        ExitApplication.getInstance().startActivity(this, CyclicGoodsConfirmArrivalActivity.class);
        finish();
    }

    public void jingjiajieguoImtb1Action(View v) {
        this.dbHelper.insertOperation("循环物资", "循环物资--首页", "竞价结果");
        ExitApplication.getInstance().startActivity(this, CyclicGoodsJingJiaJieGuo.class);
        finish();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.mIndexNavigation1 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item1);
        this.mIndexNavigation1.setChecked(true);
        try {
            Intent intent = getIntent();
            String pushMessage2 = intent.getExtras().getString("pushMessage");
            String string = intent.getExtras().getString("id");
            String lowerCase = getIntent().getExtras().getString("contactid").toString().toLowerCase();
            if (!pushMessage2.isEmpty()) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                dialog.setMessage(pushMessage2);
                dialog.setNegativeButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).show();
            }
        } catch (Exception e) {
        }
        try {
            Log.d("mustUpdate", "mustUpdate=" + ObjectStores.getInst().getObject("mustUpdate").toString());
            if (ObjectStores.getInst().getObject("mustUpdate") == "YES") {
                try {
                    if (ObjectStores.getInst().getObject("downloadDialog") != "isShowing" && ObjectStores.getInst().getObject("downloadDialog") == "Dismiss") {
                        checkUpdate();
                    }
                } catch (Exception e2) {
                    checkUpdate();
                }
            }
        } catch (Exception e3) {
            checkUpdate();
        }
        super.onResume();
    }

    private void setRead(String contractId) {
        RequestBidData requestBidData = new RequestBidData(this);
        requestBidData.setRequestData("推送已读");
        requestBidData.setContractId(contractId);
        Log.d("请求的指令：", requestBidData.infoDate());
        requestBidData.setHttpCallBack(this.httpCallBack);
        requestBidData.iExecute();
    }

    public void onCheckedChanged(RadioGroup arg0, int arg1) {
        switch (arg1) {
            case R.id.cyclicgoods_index_navigation_item2:
                this.mIndexNavigation2.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item3:
                this.mIndexNavigation3.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidSessionSelect.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item1:
                this.mIndexNavigation1.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item4:
                this.mIndexNavigation4.setChecked(true);
                startActivity(new Intent(this, CyclicGoodsBidHallPageActivity.class));
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item5:
                this.mIndexNavigation5.setChecked(true);
                Intent intent = new Intent(this, Xhjy_more.class);
                intent.putExtra("part", "循环物资");
                startActivity(intent);
                finish();
                return;
            default:
                return;
        }
    }

    public boolean isLogin() {
        return this.isLogin;
    }

    public void setLogin(boolean isLogin2) {
        this.isLogin = isLogin2;
    }

    public String getHyh() {
        return this.hyh;
    }

    public void setHyh(String hyh2) {
        this.hyh = hyh2;
    }

    public String getCzm() {
        return this.czm;
    }

    public void setCzm(String czm2) {
        this.czm = czm2;
    }

    /* access modifiers changed from: private */
    public void clearAll() {
        ObjectStores.getInst().putObject("inputUserName", StringEx.Empty);
        ObjectStores.getInst().putObject("inputPassword", StringEx.Empty);
        ObjectStores.getInst().putObject("XH_msg", StringEx.Empty);
        ObjectStores.getInst().putObject("GfullName", StringEx.Empty);
        ObjectStores.getInst().putObject("parameter_usertokenid", StringEx.Empty);
        ObjectStores.getInst().putObject("parameter_userid", StringEx.Empty);
        ObjectStores.getInst().putObject("companyCode", StringEx.Empty);
        ObjectStores.getInst().putObject("companyFullName", StringEx.Empty);
        ObjectStores.getInst().putObject("BSP_companyCode", StringEx.Empty);
        ObjectStores.getInst().putObject("G_User", StringEx.Empty);
        ObjectStores.getInst().putObject("XH_reStr", StringEx.Empty);
        ObjectStores.getInst().putObject("reStr", StringEx.Empty);
        ObjectStores.getInst().putObject("parameter_czy", StringEx.Empty);
        ObjectStores.getInst().putObject("parameter_czyname", StringEx.Empty);
        ObjectStores.getInst().putObject("parameter_czyrealname", StringEx.Empty);
        ObjectStores.getInst().putObject("parameter_hy", StringEx.Empty);
        ObjectStores.getInst().putObject("parameter_hyname", StringEx.Empty);
        ObjectStores.getInst().putObject("deviceid", StringEx.Empty);
        this.editor.clear();
        this.editor.commit();
    }

    private void setData() {
        this.editor.putString("bIndex", "xhwz");
        this.editor.commit();
        ObjectStores.getInst().putObject("formX", "formY");
    }

    private String getversion() {
        try {
            HttpResponse httpResponse = new DefaultHttpClient().execute(new HttpGet("http://m.bsteel.net/update/update_bsteelonline_Android.txt"));
            if (httpResponse.getStatusLine().getStatusCode() == 200) {
                String result = EntityUtils.toString(httpResponse.getEntity());
                System.out.println("result-----:" + result);
                JSONObject json = new JSONObject(result);
                String version = json.getString("latestversion");
                this.updateurl = json.getString("updateurl");
                this.updatename = json.getString("updatename");
                this.updateflag = json.getString("updateflag");
                Log.d("更新信息", "version" + version + "updateurl=" + this.updateurl + "updatename" + this.updatename + "updateflag" + this.updateflag);
                return version;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return StringEx.Empty;
    }

    /* access modifiers changed from: private */
    public void checkforupdate() {
        String serverVersion = getversion();
        System.out.println("程序包路径----:" + getApplicationContext().getFilesDir().getAbsolutePath());
        this.filePath = "/mnt/sdcard/bsteel/temp/" + this.updatename;
        this.updateUrl = this.updateurl;
        try {
            String clientVersion = Utils.getVersionName(this);
            this.helper = new UpdateHelper(this);
            if (!this.helper.checkUpdate(serverVersion, clientVersion)) {
                return;
            }
            if (this.updateflag.equals("0")) {
                ObjectStores.getInst().putObject("mustUpdate", "NO");
                this.mHandler.sendEmptyMessage(2);
            } else if (this.updateflag.equals("1")) {
                ObjectStores.getInst().putObject("mustUpdate", "YES");
                this.mHandler.sendEmptyMessage(4);
            }
        } catch (PackageManager.NameNotFoundException e) {
            CustomDialog.getInst().showAlertDialog(this, null, "没找到versionName", null, 0, null);
        }
    }

    private void cancelUpdate() {
    }
}
