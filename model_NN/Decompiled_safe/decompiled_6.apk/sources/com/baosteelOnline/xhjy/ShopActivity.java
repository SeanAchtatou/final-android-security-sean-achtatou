package com.baosteelOnline.xhjy;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.andframework.common.ObjectStores;
import com.andframework.ui.CustomListView;
import com.andframework.ui.CustomMessageShow;
import com.baosteelOnline.R;
import com.baosteelOnline.common.DataBaseFactory;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.data.ShopBusi;
import com.baosteelOnline.data.Shop_clearShoppingBusi;
import com.baosteelOnline.data_parse.SearchParse;
import com.baosteelOnline.dl.Login_indexActivity;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.model.SmallNumUtil;
import com.baosteelOnline.sql.DBHelper;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONObject;

public class ShopActivity extends JQActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    /* access modifiers changed from: private */
    public ArrayList<SearchRow> LisItems = new ArrayList<>();
    private TextView TV_right_btn;
    private DataBaseFactory db;
    /* access modifiers changed from: private */
    public DBHelper dbHelper;
    /* access modifiers changed from: private */
    public JQUICallBack httpCallBack = new JQUICallBack() {
        public void callBack(ResultData result) {
            ShopActivity.this.progressDialog.dismiss();
            System.out.println("result.getStringData======>" + result.getStringData());
            ShopActivity.this.updateNoticeList(SearchParse.parse(result.getStringData()));
        }
    };
    private LinearLayout layoutAction;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    private RadioGroup radioderGroup;
    private ShopBusi shopBusi;
    /* access modifiers changed from: private */
    public String shop_amount;
    /* access modifiers changed from: private */
    public String shop_flag;
    /* access modifiers changed from: private */
    public String shop_gpls;
    /* access modifiers changed from: private */
    public String shop_piece;
    /* access modifiers changed from: private */
    public String shop_sellerName;
    /* access modifiers changed from: private */
    public String shop_status;
    /* access modifiers changed from: private */
    public String shop_weight;
    /* access modifiers changed from: private */
    public String shop_yjdh;
    CustomListView shoppingcartlist;
    private ImageView state1;
    private View view;
    private View view2;
    private String who;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ExitApplication.getInstance().addActivity(this);
        this.dbHelper = DataBaseFactory.getInstance(this);
        this.dbHelper.insertOperation("钢材交易", "购物车", "购物车");
        requestWindowFeature(1);
        setContentView((int) R.layout.xhjy_shoplist);
        ((RadioButton) findViewById(R.id.radio_shopcar3)).setChecked(true);
        this.radioderGroup = (RadioGroup) findViewById(R.id.xhjy_index_RGroup);
        this.radioderGroup.setOnCheckedChangeListener(this);
        SmallNumUtil.GWCNum(this, (TextView) findViewById(R.id.gwc_dhqrnum));
        if (ObjectStores.getInst().getObject("reStr") != "1") {
            new AlertDialog.Builder(this).setMessage("您还没有登录,请登录！").setPositiveButton("确认", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    SharedPreferences.Editor editor = ShopActivity.this.getSharedPreferences("index_mark", 2).edit();
                    editor.putString("index", "Shopcar");
                    editor.commit();
                    ExitApplication.getInstance().startActivity(ShopActivity.this, Login_indexActivity.class, null, 1);
                }
            }).show();
        } else if (ObjectStores.getInst().getObject("reStr").toString() == "1") {
            this.shoppingcartlist = (CustomListView) findViewById(R.id.shoppingcartlist);
            this.shoppingcartlist.setClickable(true);
            testBusi();
        }
    }

    public void testBusi() {
        this.shopBusi = new ShopBusi(this);
        this.shopBusi.setHttpCallBack(this.httpCallBack);
        this.shopBusi.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    /* access modifiers changed from: package-private */
    public void updateNoticeList(SearchParse searchParse) {
        if (SearchParse.commonData == null) {
            CustomMessageShow.getInst().showLongToast(this, "服务器无数据返回！");
            ExitApplication.getInstance().back(0);
        } else if (SearchParse.commonData.blocks == null) {
            CustomMessageShow.getInst().showLongToast(this, "服务器无数据返回！");
            ExitApplication.getInstance().back(0);
        } else if (SearchParse.commonData.blocks.r0 == null) {
            CustomMessageShow.getInst().showLongToast(this, "服务器无数据返回！");
            ExitApplication.getInstance().back(0);
        } else if (SearchParse.commonData.blocks.r0.mar.rows.rows == null) {
            CustomMessageShow.getInst().showLongToast(this, "服务器无数据返回！");
            ExitApplication.getInstance().back(0);
        } else if (SearchParse.commonData.blocks.r0.mar.rows.rows.size() == 0) {
            new AlertDialog.Builder(this).setMessage("对不起，没有数据!").setPositiveButton("确认", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    ExitApplication.getInstance().back(0);
                }
            }).show();
        } else if (SearchParse.commonData != null && SearchParse.commonData.blocks != null && SearchParse.commonData.blocks.r0 != null && SearchParse.commonData.blocks.r0.mar != null && SearchParse.commonData.blocks.r0.mar.rows != null && SearchParse.commonData.blocks.r0.mar.rows.rows != null) {
            for (int i = 0; i < SearchParse.commonData.blocks.r0.mar.rows.rows.size(); i++) {
                ArrayList<String> rowdata = SearchParse.commonData.blocks.r0.mar.rows.rows.get(i);
                SearchRow sr = new SearchRow();
                if (rowdata != null) {
                    String data = String.valueOf(StringEx.Empty) + "{";
                    for (int k = 0; k < rowdata.size(); k++) {
                        if (k == 0) {
                            sr.sellerName = (String) rowdata.get(k);
                            data = String.valueOf(data) + "'sellerName':'" + sr.sellerName + "',";
                        }
                        if (k == 1) {
                            sr.piece = (String) rowdata.get(k);
                            data = String.valueOf(data) + "'piece':'" + sr.piece + "',";
                        } else if (k == 2) {
                            sr.weight = (String) rowdata.get(k);
                            data = String.valueOf(data) + "'weight':'" + sr.weight + "',";
                        } else if (k == 3) {
                            sr.amount = (String) rowdata.get(k);
                            data = String.valueOf(data) + "'amount':'" + sr.amount + "',";
                        } else if (k == 4) {
                            sr.gpls = (String) rowdata.get(k);
                            data = String.valueOf(data) + "'gpls':'" + sr.gpls + "',";
                        } else if (k == 5) {
                            sr.flag = (String) rowdata.get(k);
                            data = String.valueOf(data) + "'flag':'" + sr.flag + "',";
                        } else if (k == 6) {
                            sr.status = (String) rowdata.get(k);
                            data = String.valueOf(data) + "'status':'" + sr.status + "',";
                        } else if (k == 7) {
                            sr.yjdh = (String) rowdata.get(k);
                            data = String.valueOf(data) + "'yjdh':'" + sr.yjdh + "'";
                        }
                    }
                    this.LisItems.add(sr);
                    this.view2 = buildRowView(sr, String.valueOf(data) + "}");
                    this.shoppingcartlist.addViewToLast(this.view2);
                    rowdata.clear();
                }
            }
            this.shoppingcartlist.onRefreshComplete();
        }
    }

    private View buildRowView(SearchRow sr, String data) {
        this.view = View.inflate(this, R.layout.xhjy_shoplist_row, null);
        TextView TV_sellerName = (TextView) this.view.findViewById(R.id.TV_sellername_shop);
        this.layoutAction = (LinearLayout) this.view.findViewById(R.id.shoplist_left_LLout);
        this.layoutAction.setTag(data);
        this.layoutAction.setOnClickListener(this);
        TextView TV_piece = (TextView) this.view.findViewById(R.id.TV_piece_shop);
        TextView TV_weight = (TextView) this.view.findViewById(R.id.TV_tonne_shop);
        TextView TV_amount = (TextView) this.view.findViewById(R.id.TV_price_shop);
        this.state1 = (ImageView) this.view.findViewById(R.id.IV_materia_shopcar);
        this.state1.setTag(data);
        this.state1.setOnClickListener(this);
        this.TV_right_btn = (TextView) this.view.findViewById(R.id.TV_right_shopcar);
        this.TV_right_btn.setTag(data);
        this.TV_right_btn.setOnClickListener(this);
        if (sr.status.equals("2")) {
            this.state1.setBackgroundResource(R.drawable.btn_bg_withgt_ct);
        } else if (sr.status.equals("3")) {
            this.state1.setBackgroundResource(R.drawable.btn_bg_withgt_re);
        }
        TV_sellerName.setText("卖家:" + sr.sellerName);
        TV_piece.setText(sr.piece);
        TV_weight.setText(sr.weight);
        TV_amount.setText(sr.amount);
        return this.view;
    }

    class SearchRow {
        public String amount;
        public String flag;
        public String gpls;
        public String piece;
        public String sellerName;
        public String status;
        public String weight;
        public String yjdh;

        SearchRow() {
        }
    }

    public void onBackPressed() {
        super.onBackPressed();
        ExitApplication.getInstance().back(0);
    }

    public void shoppingcart_clearAction(View v) {
        new AlertDialog.Builder(this).setMessage("是否确认清空购物车？").setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                ShopActivity.this.shoppingcartlist.removeAllView();
                ShopActivity.this.LisItems.removeAll(ShopActivity.this.LisItems);
                ShopActivity.this.dbHelper.insertOperation("钢材交易", "购物车", "清空按钮");
                Shop_clearShoppingBusi cBusi = new Shop_clearShoppingBusi(ShopActivity.this);
                cBusi.setHttpCallBack(ShopActivity.this.httpCallBack);
                cBusi.iExecute();
            }
        }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        }).create().show();
    }

    public void shoppingcart_backAction(View v) {
        ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
    }

    public void onClick(View v) {
        if (v instanceof ImageView) {
            try {
                JSONObject item = new JSONObject(v.getTag().toString());
                try {
                    this.shop_sellerName = item.getString("sellerName");
                    this.shop_piece = item.getString("piece");
                    this.shop_weight = item.getString("weight");
                    this.shop_amount = item.getString("amount");
                    this.shop_gpls = item.getString("gpls");
                    this.shop_flag = item.getString("flag");
                    this.shop_status = item.getString("status");
                    this.shop_yjdh = item.getString("yjdh");
                } catch (Exception e) {
                }
            } catch (Exception e2) {
            }
            if ("0".equals(this.shop_status)) {
                HashMap hasmap = new HashMap();
                hasmap.put("sellerName", this.shop_sellerName);
                hasmap.put("piece", this.shop_piece);
                hasmap.put("weight", this.shop_weight);
                hasmap.put("amount", this.shop_amount);
                hasmap.put("shop_gpls", this.shop_gpls);
                hasmap.put("shop_flag", this.shop_flag);
                hasmap.put("type", this.shop_status);
                hasmap.put("shop_yjdh", this.shop_yjdh);
                ExitApplication.getInstance().startActivity(this, Shopping_formValidateActivity.class, hasmap);
            } else if ("1".equals(this.shop_status)) {
                Dialog Dialog1 = new AlertDialog.Builder(this).setMessage("所选订单可以议价，是否议价？").setPositiveButton("不议价", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        HashMap hasmap = new HashMap();
                        hasmap.put("sellerName", ShopActivity.this.shop_sellerName);
                        hasmap.put("piece", ShopActivity.this.shop_piece);
                        hasmap.put("weight", ShopActivity.this.shop_weight);
                        hasmap.put("amount", ShopActivity.this.shop_amount);
                        hasmap.put("shop_gpls", ShopActivity.this.shop_gpls);
                        hasmap.put("shop_flag", ShopActivity.this.shop_flag);
                        hasmap.put("type", ShopActivity.this.shop_status);
                        hasmap.put("shop_yjdh", ShopActivity.this.shop_yjdh);
                        ExitApplication.getInstance().startActivity(ShopActivity.this, Shopping_formValidateActivity.class, hasmap);
                    }
                }).setNegativeButton("议价", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        HashMap hasmap = new HashMap();
                        hasmap.put("sellerName", ShopActivity.this.shop_sellerName);
                        hasmap.put("piece", ShopActivity.this.shop_piece);
                        hasmap.put("weight", ShopActivity.this.shop_weight);
                        hasmap.put("amount", ShopActivity.this.shop_amount);
                        hasmap.put("shop_gpls", ShopActivity.this.shop_gpls);
                        hasmap.put("shop_flag", ShopActivity.this.shop_flag);
                        hasmap.put("type", "0");
                        hasmap.put("shop_yjdh", ShopActivity.this.shop_yjdh);
                        ExitApplication.getInstance().startActivity(ShopActivity.this, NegotiateActivity.class, hasmap);
                    }
                }).create();
                Dialog1.setCanceledOnTouchOutside(false);
                Dialog1.show();
            } else if ("2".equals(this.shop_status)) {
                HashMap hasmap2 = new HashMap();
                hasmap2.put("type", this.shop_status);
                hasmap2.put("sellerName", this.shop_sellerName);
                hasmap2.put("piece", this.shop_piece);
                hasmap2.put("weight", this.shop_weight);
                hasmap2.put("amount", this.shop_amount);
                hasmap2.put("shop_gpls", this.shop_gpls);
                hasmap2.put("shop_flag", this.shop_flag);
                hasmap2.put("shop_yjdh", this.shop_yjdh);
                ExitApplication.getInstance().startActivity(this, NegotiateActivity.class, hasmap2);
            } else if ("3".equals(this.shop_status)) {
                HashMap hasmap3 = new HashMap();
                hasmap3.put("sellerName", this.shop_sellerName);
                hasmap3.put("piece", this.shop_piece);
                hasmap3.put("weight", this.shop_weight);
                hasmap3.put("amount", this.shop_amount);
                hasmap3.put("shop_gpls", this.shop_gpls);
                hasmap3.put("shop_flag", this.shop_flag);
                hasmap3.put("type", this.shop_status);
                hasmap3.put("shop_yjdh", this.shop_yjdh);
                ExitApplication.getInstance().startActivity(this, Shopping_formValidateActivity.class, hasmap3);
            }
        } else if (v instanceof LinearLayout) {
            String obj = v.getTag().toString();
            try {
                HashMap hasmap4 = new HashMap();
                JSONObject item2 = new JSONObject(v.getTag().toString());
                try {
                    hasmap4.put("sellerName", item2.getString("sellerName"));
                    hasmap4.put("piece", item2.getString("piece"));
                    hasmap4.put("weight", item2.getString("weight"));
                    hasmap4.put("amount", item2.getString("amount"));
                    hasmap4.put("flag", item2.getString("flag"));
                    hasmap4.put("status", item2.getString("status"));
                    hasmap4.put("gpls", item2.getString("gpls"));
                    hasmap4.put("yjdh", item2.getString("yjdh"));
                    ExitApplication.getInstance().startActivity(this, Shopping_cartDetailActivity.class, hasmap4);
                } catch (Exception e3) {
                }
            } catch (Exception e4) {
            }
        } else if (v instanceof TextView) {
            try {
                HashMap hasmap5 = new HashMap();
                JSONObject item3 = new JSONObject(v.getTag().toString());
                try {
                    hasmap5.put("sellerName", item3.getString("sellerName"));
                    hasmap5.put("piece", item3.getString("piece"));
                    hasmap5.put("weight", item3.getString("weight"));
                    hasmap5.put("amount", item3.getString("amount"));
                    hasmap5.put("flag", item3.getString("flag"));
                    hasmap5.put("status", item3.getString("status"));
                    hasmap5.put("gpls", item3.getString("gpls"));
                    hasmap5.put("yjdh", item3.getString("yjdh"));
                    ExitApplication.getInstance().startActivity(this, Shopping_cartDetailActivity.class, hasmap5);
                } catch (Exception e5) {
                }
            } catch (Exception e6) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.shoppingcartlist = null;
        this.view = null;
        this.view2 = null;
        this.who = null;
        this.shop_status = null;
        this.shop_sellerName = null;
        this.shop_piece = null;
        this.shop_weight = null;
        this.shop_amount = null;
        this.LisItems = null;
        this.state1 = null;
        this.layoutAction = null;
        ObjectStores.getInst().putObject("index_shop", "n");
        super.onDestroy();
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.radio_search3:
                ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
                ObjectStores.getInst().putObject("index_shop", "n");
                ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
                return;
            case R.id.radio_shopcar3:
                ((RadioButton) findViewById(R.id.radio_shopcar3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ShopActivity.class);
                return;
            case R.id.radio_home3:
                ((RadioButton) findViewById(R.id.radio_home3)).setChecked(true);
                ObjectStores.getInst().putObject("index_shop", "n");
                HashMap hasmap = new HashMap();
                hasmap.put("index_more", "1");
                ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class, hasmap);
                return;
            case R.id.radio_contract3:
                ((RadioButton) findViewById(R.id.radio_contract3)).setChecked(true);
                ObjectStores.getInst().putObject("index_shop", "n");
                ExitApplication.getInstance().startActivity(this, ContractListActivity.class);
                return;
            case R.id.radio_notice3:
                ((RadioButton) findViewById(R.id.radio_notice3)).setChecked(true);
                ObjectStores.getInst().putObject("index_shop", "n");
                ExitApplication.getInstance().startActivity(this, Xhjy_more.class);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (ObjectStores.getInst().getObject("index_shop") != null && ObjectStores.getInst().getObject("index_shop").equals("y")) {
            this.shoppingcartlist.removeAllView();
            this.LisItems.clear();
            testBusi();
        }
        ((RadioButton) findViewById(R.id.radio_shopcar3)).setChecked(true);
        this.radioderGroup = (RadioGroup) findViewById(R.id.xhjy_index_RGroup);
        this.radioderGroup.setOnCheckedChangeListener(this);
        super.onResume();
    }
}
