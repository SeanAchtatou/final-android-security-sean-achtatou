package com.baosteelOnline.xhjy;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.andframework.ui.CustomListView;
import com.baosteelOnline.R;
import com.baosteelOnline.common.DataBaseFactory;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.data.NegotiateHistoryBusi;
import com.baosteelOnline.data.Shop_sendNegoRealBusi;
import com.baosteelOnline.data_parse.ContractParse;
import com.baosteelOnline.data_parse.SearchParse;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.sql.DBHelper;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class NegotiateActivity extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    private EditText ET_price;
    public ArrayList<infoRow> LisItems = new ArrayList<>();
    public ArrayList<SearchRow> LisItems2 = new ArrayList<>();
    private LinearLayout TV_LLout02;
    private TextView TV_cishu;
    private TextView TV_fenzh;
    private TextView TV_miao;
    /* access modifiers changed from: private */
    public TextView TV_num;
    private TextView TV_shengyu;
    private TextView TV_shengyu02;
    /* access modifiers changed from: private */
    public ArrayAdapter<String> adapter2_jsfs;
    /* access modifiers changed from: private */
    public ArrayAdapter<String> adapter_fkfs;
    /* access modifiers changed from: private */
    public String amount_neg;
    private ImageButton btn_ok;
    private DataBaseFactory db;
    private DBHelper dbHelper;
    private long endT;
    /* access modifiers changed from: private */
    public String fkfs = "0";
    private Spinner fkfs_Spinner;
    private String flag_neg;
    /* access modifiers changed from: private */
    public String gpls_neg;
    private JQUICallBack httpCallBack1 = new JQUICallBack() {
        public void callBack(ResultData result) {
            NegotiateActivity.this.progressDialog.dismiss();
            try {
                NegotiateActivity.this.updateNoticeList22(SearchParse.parse(result.getStringData()));
            } catch (Exception e) {
            }
        }
    };
    /* access modifiers changed from: private */
    public JQUICallBack httpCallBack2 = new JQUICallBack() {
        public void callBack(ResultData result) {
            NegotiateActivity.this.progressDialog.dismiss();
            try {
                NegotiateActivity.this.updateShop_send3(SearchParse.parse(result.getStringData()));
            } catch (Exception e) {
            }
        }
    };
    private JQUICallBack httpCallBack3 = new JQUICallBack() {
        public void callBack(ResultData result) {
            NegotiateActivity.this.progressDialog.dismiss();
            try {
                NegotiateActivity.this.updateOrderList4(SearchParse.parse(result.getStringData()));
            } catch (Exception e) {
            }
        }
    };
    /* access modifiers changed from: private */
    public String jsfs = "5";
    /* access modifiers changed from: private */
    public Spinner jsfs_Spinner;
    private List<String> list = new ArrayList();
    /* access modifiers changed from: private */
    public List<String> list2 = new ArrayList();
    private RelativeLayout listview;
    /* access modifiers changed from: private */
    public MyCount mc;
    private LinearLayout negotiate_book_LLout;
    private LinearLayout negotiate_fkfs_LLout;
    CustomListView negotiate_list;
    private int num = 3;
    private String outOrderId = StringEx.Empty;
    private String piece_neg;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    private RadioGroup radioderGroup;
    private String sellerName_neg;
    /* access modifiers changed from: private */
    public String sendprice = StringEx.Empty;
    private long startT;
    private String success_stats = StringEx.Empty;
    /* access modifiers changed from: private */
    public TextView tv;
    /* access modifiers changed from: private */
    public TextView tv2;
    private String type_neg;
    private View view;
    private View view2;
    private String weight_neg;
    public String wzly;
    /* access modifiers changed from: private */
    public String yjdh_neg;
    public String ziyuan;
    public String zylx;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ExitApplication.getInstance().addActivity(this);
        requestWindowFeature(1);
        setContentView((int) R.layout.xhjy_negotiate);
        this.dbHelper = DataBaseFactory.getInstance(this);
        this.dbHelper.insertOperation("钢材交易", "购物车", "议价协议页面");
        ((RadioButton) findViewById(R.id.radio_shopcar3)).setChecked(true);
        this.radioderGroup = (RadioGroup) findViewById(R.id.xhjy_index_RGroup);
        this.radioderGroup.setOnCheckedChangeListener(this);
        this.tv = (TextView) findViewById(R.id.TV_seconds);
        this.tv2 = (TextView) findViewById(R.id.TV_minutes);
        this.TV_num = (TextView) findViewById(R.id.TV_negotiate_num);
        this.TV_shengyu = (TextView) findViewById(R.id.TV_negotiate_shengyu);
        this.TV_fenzh = (TextView) findViewById(R.id.TV_negotiate_fenzh);
        this.TV_miao = (TextView) findViewById(R.id.TV_negotiate_miao);
        this.TV_shengyu02 = (TextView) findViewById(R.id.TV_negotiate_shengyu02);
        this.TV_cishu = (TextView) findViewById(R.id.TV_negotiate_cishu);
        this.TV_LLout02 = (LinearLayout) findViewById(R.id.TV_negotiate_LLout02);
        this.btn_ok = (ImageButton) findViewById(R.id.btn_ok_negotiate_);
        this.negotiate_book_LLout = (LinearLayout) findViewById(R.id.negotiate_book_LLout);
        this.negotiate_fkfs_LLout = (LinearLayout) findViewById(R.id.negotiate_fkfs_LLout);
        this.listview = (RelativeLayout) findViewById(R.id.contently);
        this.fkfs_Spinner = (Spinner) findViewById(R.id.xhjy_negotiate_fkfs);
        this.jsfs_Spinner = (Spinner) findViewById(R.id.xhjy_negotiate_jsfs);
        this.list.add("请选择");
        this.list2.add("请选择");
        this.negotiate_list = (CustomListView) findViewById(R.id.negotiatelist);
        this.ET_price = (EditText) findViewById(R.id.ET_price_negotiate);
        Bundle bundle = getIntent().getExtras();
        try {
            this.sellerName_neg = bundle.getString("sellerName");
            this.piece_neg = bundle.getString("piece");
            this.weight_neg = bundle.getString("weight");
            this.amount_neg = bundle.getString("amount");
            this.gpls_neg = bundle.getString("shop_gpls");
            this.flag_neg = bundle.getString("shop_flag");
            this.type_neg = bundle.getString("type");
            this.yjdh_neg = bundle.getString("shop_yjdh");
        } catch (Exception e) {
        }
        getHistory();
    }

    public void init_spinner() {
        if (this.zylx.equals("0")) {
            this.ziyuan = "0";
        } else if (this.zylx.equals("3")) {
            this.ziyuan = "1";
        }
        if (this.wzly.equals("3")) {
            this.ziyuan = "2";
        }
        System.out.println("ziyuan------------>" + this.ziyuan);
        this.adapter_fkfs = new ArrayAdapter<>(this, 17367048, this.list);
        this.adapter_fkfs.setDropDownViewResource(17367049);
        this.fkfs_Spinner.setAdapter((SpinnerAdapter) this.adapter_fkfs);
        if (this.list.size() > 1) {
            this.fkfs_Spinner.setSelection(1, true);
        }
        if (this.ziyuan == "0") {
            this.fkfs_Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                    NegotiateActivity.this.fkfs = (String) NegotiateActivity.this.adapter_fkfs.getItem(arg2);
                    if ("全额".equals(NegotiateActivity.this.fkfs)) {
                        NegotiateActivity.this.fkfs = "0";
                        NegotiateActivity.this.list2.clear();
                        NegotiateActivity.this.list2.add("请选择");
                        NegotiateActivity.this.list2.add("场外结算");
                        NegotiateActivity.this.list2.add("场外结算");
                        NegotiateActivity.this.jsfs_Spinner.setSelection(1, true);
                    } else if ("定金".equals(NegotiateActivity.this.fkfs)) {
                        NegotiateActivity.this.fkfs = "2";
                        NegotiateActivity.this.list2.clear();
                        NegotiateActivity.this.list2.add("请选择");
                        NegotiateActivity.this.list2.add("自由付款方式");
                        NegotiateActivity.this.jsfs_Spinner.setSelection(1, true);
                    } else if ("融资".equals(NegotiateActivity.this.fkfs)) {
                        NegotiateActivity.this.fkfs = "3";
                        NegotiateActivity.this.list2.clear();
                        NegotiateActivity.this.list2.add("请选择");
                        NegotiateActivity.this.list2.add("自由付款方式");
                        NegotiateActivity.this.jsfs_Spinner.setSelection(1, true);
                    } else if ("请选择".equals(NegotiateActivity.this.fkfs)) {
                        NegotiateActivity.this.fkfs = StringEx.Empty;
                        NegotiateActivity.this.list2.clear();
                        NegotiateActivity.this.list2.add("请选择");
                        NegotiateActivity.this.jsfs_Spinner.setSelection(0, true);
                    }
                    arg0.setVisibility(0);
                }

                public void onNothingSelected(AdapterView<?> arg0) {
                    arg0.setVisibility(0);
                }
            });
        } else if (this.ziyuan == "1") {
            this.fkfs_Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                    NegotiateActivity.this.fkfs = (String) NegotiateActivity.this.adapter_fkfs.getItem(arg2);
                    if ("全额".equals(NegotiateActivity.this.fkfs)) {
                        NegotiateActivity.this.fkfs = "0";
                        NegotiateActivity.this.list2.clear();
                        NegotiateActivity.this.list2.add("请选择");
                        NegotiateActivity.this.list2.add("场内结算");
                        NegotiateActivity.this.jsfs_Spinner.setSelection(1, true);
                    } else if ("定金".equals(NegotiateActivity.this.fkfs)) {
                        NegotiateActivity.this.fkfs = "2";
                        NegotiateActivity.this.list2.clear();
                        NegotiateActivity.this.list2.add("请选择");
                        NegotiateActivity.this.list2.add("场内结算");
                        NegotiateActivity.this.jsfs_Spinner.setSelection(1, true);
                    } else if ("融资".equals(NegotiateActivity.this.fkfs)) {
                        NegotiateActivity.this.fkfs = "3";
                        NegotiateActivity.this.list2.clear();
                        NegotiateActivity.this.list2.add("请选择");
                        NegotiateActivity.this.list2.add("自由付款方式");
                        NegotiateActivity.this.jsfs_Spinner.setSelection(1, true);
                    } else if ("请选择".equals(NegotiateActivity.this.fkfs)) {
                        NegotiateActivity.this.fkfs = StringEx.Empty;
                        NegotiateActivity.this.list2.clear();
                        NegotiateActivity.this.list2.add("请选择");
                        NegotiateActivity.this.jsfs_Spinner.setSelection(0, true);
                    }
                    arg0.setVisibility(0);
                }

                public void onNothingSelected(AdapterView<?> arg0) {
                    arg0.setVisibility(0);
                }
            });
        }
        if (this.ziyuan == "2") {
            this.fkfs_Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                    NegotiateActivity.this.fkfs = (String) NegotiateActivity.this.adapter_fkfs.getItem(arg2);
                    if ("全额".equals(NegotiateActivity.this.fkfs)) {
                        NegotiateActivity.this.fkfs = "0";
                        NegotiateActivity.this.list2.clear();
                        NegotiateActivity.this.list2.add("请选择");
                        NegotiateActivity.this.list2.add("场外结算");
                        NegotiateActivity.this.jsfs_Spinner.setSelection(1, true);
                    } else if ("定金".equals(NegotiateActivity.this.fkfs)) {
                        NegotiateActivity.this.fkfs = "2";
                        NegotiateActivity.this.list2.clear();
                        NegotiateActivity.this.list2.add("请选择");
                        NegotiateActivity.this.list2.add("场外结算");
                        NegotiateActivity.this.jsfs_Spinner.setSelection(1, true);
                    } else if ("融资".equals(NegotiateActivity.this.fkfs)) {
                        NegotiateActivity.this.fkfs = "3";
                        NegotiateActivity.this.list2.clear();
                        NegotiateActivity.this.list2.add("请选择");
                        NegotiateActivity.this.list2.add("自由付款方式");
                        NegotiateActivity.this.jsfs_Spinner.setSelection(1, true);
                    } else if ("请选择".equals(NegotiateActivity.this.fkfs)) {
                        NegotiateActivity.this.fkfs = StringEx.Empty;
                        NegotiateActivity.this.list2.clear();
                        NegotiateActivity.this.list2.add("请选择");
                        NegotiateActivity.this.jsfs_Spinner.setSelection(0, true);
                    }
                    arg0.setVisibility(0);
                }

                public void onNothingSelected(AdapterView<?> arg0) {
                    arg0.setVisibility(0);
                }
            });
        }
        this.fkfs_Spinner.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                v.setVisibility(0);
            }
        });
        this.adapter2_jsfs = new ArrayAdapter<>(this, 17367048, this.list2);
        this.adapter2_jsfs.setDropDownViewResource(17367049);
        this.jsfs_Spinner.setAdapter((SpinnerAdapter) this.adapter2_jsfs);
        if (this.list2.size() > 2) {
            this.jsfs_Spinner.setSelection(2, true);
        } else if (this.list2.size() > 1) {
            this.jsfs_Spinner.setSelection(1, true);
        }
        this.jsfs_Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView arg0, View arg1, int arg2, long arg3) {
                NegotiateActivity.this.jsfs = (String) NegotiateActivity.this.adapter2_jsfs.getItem(arg2);
                if ("场内结算".equals(NegotiateActivity.this.jsfs)) {
                    NegotiateActivity.this.jsfs = "4";
                } else if ("场外结算".equals(NegotiateActivity.this.jsfs)) {
                    NegotiateActivity.this.jsfs = "5";
                } else if ("自由付款方式".equals(NegotiateActivity.this.jsfs)) {
                    NegotiateActivity.this.jsfs = "6";
                } else if ("请选择".equals(NegotiateActivity.this.jsfs)) {
                    NegotiateActivity.this.jsfs = StringEx.Empty;
                }
                arg0.setVisibility(0);
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.jsfs_Spinner.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                v.setVisibility(0);
            }
        });
    }

    public void getHistory() {
        NegotiateHistoryBusi hBusi = new NegotiateHistoryBusi(this);
        hBusi.gpls = this.gpls_neg;
        hBusi.yjdh = this.yjdh_neg;
        hBusi.setHttpCallBack(this.httpCallBack1);
        hBusi.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    /* access modifiers changed from: package-private */
    public void updatejsfs(ContractParse shParse) {
        if (ContractParse.commonData != null && ContractParse.commonData.blocks != null && ContractParse.commonData.blocks.r0 != null && ContractParse.commonData.blocks.r0.mar != null && ContractParse.commonData.blocks.r0.mar.rows != null) {
            if (!(ContractParse.commonData == null || ContractParse.commonData.blocks == null || ContractParse.commonData.blocks.r0 == null || ContractParse.commonData.blocks.r0.mar == null || ContractParse.commonData.blocks.r0.mar.rows == null || ContractParse.commonData.blocks.r0.mar.rows.rows == null)) {
                for (int i = 0; i < ContractParse.commonData.blocks.r0.mar.rows.rows.size(); i++) {
                    ArrayList<String> rowdata = ContractParse.commonData.blocks.r0.mar.rows.rows.get(i);
                    if (rowdata != null) {
                        for (int k = 0; k < rowdata.size(); k++) {
                            if (k == 0) {
                                System.out.println("0---" + ((String) rowdata.get(k)));
                                if (((String) rowdata.get(k)).indexOf("0") != -1) {
                                    this.list.add("全额");
                                }
                                if (((String) rowdata.get(k)).indexOf("2") != -1) {
                                    this.list.add("定金");
                                }
                                if (((String) rowdata.get(k)).indexOf("3") != -1) {
                                    this.list.add("融资");
                                }
                            }
                            if (k == 1) {
                                System.out.println("1---" + ((String) rowdata.get(k)));
                                if (((String) rowdata.get(k)).indexOf("4") != -1) {
                                    this.list2.add("场内结算");
                                }
                                if (((String) rowdata.get(k)).indexOf("5") != -1) {
                                    this.list2.add("场外结算");
                                }
                                if (((String) rowdata.get(k)).indexOf("6") != -1) {
                                    this.list2.add("自由付款方式");
                                }
                            }
                        }
                        rowdata.clear();
                    }
                }
            }
            System.out.println("--list.size-:" + this.list.size());
            System.out.println("--list2.size:" + this.list2.size());
            for (int i2 = 0; i2 < this.list.size(); i2++) {
                System.out.println("list--v :" + this.list.get(i2).toString());
            }
            for (int i3 = 0; i3 < this.list2.size(); i3++) {
                System.out.println("list2--v :" + this.list2.get(i3).toString());
            }
            init_spinner();
        }
    }

    /* access modifiers changed from: package-private */
    public void updateOrderList4(SearchParse searchParse) {
    }

    /* access modifiers changed from: package-private */
    public void updateShop_send3(SearchParse searchParse) {
        System.out.println("updateShop_send3");
        if (SearchParse.commonData.blocks.r0.mar.rows.rows.size() == 0) {
            new AlertDialog.Builder(this).setMessage(SearchParse.commonData.msg.toString()).setPositiveButton("确认", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    NegotiateActivity.this.mc.cancel();
                    ExitApplication.getInstance().back(0);
                }
            }).show();
        } else if (!(SearchParse.commonData == null || SearchParse.commonData.blocks == null || SearchParse.commonData.blocks.r0 == null || SearchParse.commonData.blocks.r0.mar == null || SearchParse.commonData.blocks.r0.mar.rows == null || SearchParse.commonData.blocks.r0.mar.rows.rows == null)) {
            this.yjdh_neg = (String) SearchParse.commonData.blocks.r0.mar.rows.rows.get(0).get(0);
            System.out.println("yjdh_neg------:" + this.yjdh_neg);
        }
        getHistory();
    }

    /* access modifiers changed from: package-private */
    public void updateNoticeList22(SearchParse searchParse) {
        if (SearchParse.commonData == null || SearchParse.commonData.blocks == null || SearchParse.commonData.blocks.r0 == null || SearchParse.commonData.blocks.r0.mar == null || SearchParse.commonData.blocks.r0.mar.rows == null) {
            new AlertDialog.Builder(this).setMessage("对不起，网络数据异常!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    ExitApplication.getInstance().back(0);
                }
            }).show();
        } else if (SearchParse.commonData.blocks.r0.mar.rows.rows.size() == 0) {
            new AlertDialog.Builder(this).setMessage(SearchParse.commonData.msg.toString()).setPositiveButton("确认", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    ExitApplication.getInstance().back(0);
                }
            }).show();
        } else if (!(SearchParse.commonData == null || SearchParse.commonData.blocks == null || SearchParse.commonData.blocks.r0 == null || SearchParse.commonData.blocks.r0.mar == null || SearchParse.commonData.blocks.r0.mar.rows == null || SearchParse.commonData.blocks.r0.mar.rows.rows == null)) {
            int j = SearchParse.commonData.blocks.r0.mar.rows.rows.size();
            this.wzly = (String) SearchParse.commonData.blocks.r0.mar.rows.rows.get(0).get(4);
            this.zylx = (String) SearchParse.commonData.blocks.r0.mar.rows.rows.get(0).get(5);
            System.out.println("wzly====" + this.wzly);
            System.out.println("zylx====" + this.zylx);
            if (((String) SearchParse.commonData.blocks.r0.mar.rows.rows.get(0).get(0)).length() > 5) {
                System.out.println("-----1-------");
                SearchRow sr2 = new SearchRow();
                sr2.cutSdate = (String) SearchParse.commonData.blocks.r0.mar.rows.rows.get(0).get(2);
                sr2.cutEdate = (String) SearchParse.commonData.blocks.r0.mar.rows.rows.get(0).get(3);
                for (int i = 0; i < j; i++) {
                    SearchRow sr = new SearchRow();
                    sr.yjxx = (String) SearchParse.commonData.blocks.r0.mar.rows.rows.get(i).get(0);
                    sr.listState = (String) SearchParse.commonData.blocks.r0.mar.rows.rows.get(i).get(8);
                    if (sr.listState.equals("6")) {
                        sr.listState = "接受";
                        this.success_stats = "6";
                    } else if (sr.listState.equals("5")) {
                        sr.listState = "拒绝";
                    } else if (sr.listState.equals("0")) {
                        sr.listState = "等待";
                    }
                    this.LisItems2.add(sr);
                    this.view2 = buildRowView(sr, i);
                    this.negotiate_list.addViewToLast(this.view2);
                }
                hidd();
                setTime(sr2);
            }
        }
        this.negotiate_list.onRefreshComplete();
    }

    /* access modifiers changed from: package-private */
    public void setTime(SearchRow sr2) {
        String startTime = sr2.cutSdate.toString();
        String endTime = sr2.cutEdate.toString();
        if (startTime != StringEx.Empty) {
            String starth = StringEx.Empty;
            String startmin = StringEx.Empty;
            String startsec = StringEx.Empty;
            String endh = StringEx.Empty;
            String endmin = StringEx.Empty;
            String endsec = StringEx.Empty;
            try {
                starth = startTime.substring(11, 13);
                startmin = startTime.substring(14, 16);
                startsec = startTime.substring(17, 19);
                endh = endTime.substring(11, 13);
                endmin = endTime.substring(14, 16);
                endsec = endTime.substring(17, 19);
            } catch (Exception e) {
            }
            try {
                this.mc.cancel();
            } catch (Exception e2) {
            }
            try {
                int time_starth = Integer.valueOf(starth).intValue();
                int time_startm = Integer.valueOf(startmin).intValue();
                int time_starts = Integer.valueOf(startsec).intValue();
                int time_endh = Integer.valueOf(endh).intValue();
                int time_endmin = Integer.valueOf(endmin).intValue();
                int time_s = (((((time_endh * 3600) + (time_endmin * 60)) + Integer.valueOf(endsec).intValue()) - (time_starth * 3600)) - (time_startm * 60)) - time_starts;
                System.out.println("-------time_s:" + time_s);
                this.mc = new MyCount((long) (time_s * 1000), 1000);
                this.mc.start();
            } catch (Exception e3) {
            }
            this.negotiate_book_LLout.setVisibility(8);
            this.negotiate_fkfs_LLout.setVisibility(8);
            return;
        }
        this.listview.setVisibility(8);
    }

    /* access modifiers changed from: package-private */
    public void hidd() {
        if (this.success_stats.equals("6")) {
            this.TV_shengyu.setText("订单议");
            this.tv2.setVisibility(8);
            this.TV_fenzh.setText("价已");
            this.tv.setVisibility(8);
            this.TV_miao.setText("功成");
            this.TV_LLout02.setVisibility(8);
            this.TV_shengyu02.setText(StringEx.Empty);
            this.TV_num.setText(StringEx.Empty);
            this.TV_cishu.setText(StringEx.Empty);
        }
    }

    private View buildRowView(SearchRow sr, int i) {
        System.out.println("yjxx" + i + "=:" + sr.yjxx);
        this.view = View.inflate(this, R.layout.xhjy_negotiate_row, null);
        ((LinearLayout) this.view.findViewById(R.id.negotiate_row_LLout22)).setVisibility(8);
        TextView TV_time = (TextView) this.view.findViewById(R.id.negotiate_row_time);
        TextView textView = (TextView) this.view.findViewById(R.id.negotiate_row_price);
        TextView textView2 = (TextView) this.view.findViewById(R.id.negotiate_row_stats);
        TextView textView3 = (TextView) this.view.findViewById(R.id.negotiate_row_msg);
        ImageView IV_num = (ImageView) this.view.findViewById(R.id.IV_negotiate_row1);
        if (i == 0) {
            IV_num.setBackgroundResource(R.drawable.btn_num1);
        }
        if (i == 1) {
            IV_num.setBackgroundResource(R.drawable.btn_num2);
        }
        if (i == 2) {
            IV_num.setBackgroundResource(R.drawable.btn_num3);
        }
        if (i == 3) {
            IV_num.setBackgroundResource(R.drawable.btn_num4);
        }
        if (i == 4) {
            IV_num.setBackgroundResource(R.drawable.btn_num5);
        }
        if (i == 5) {
            IV_num.setBackgroundResource(R.drawable.btn_num6);
        }
        TV_time.setText(String.valueOf(sr.yjxx) + "   ");
        return this.view;
    }

    class SearchRow {
        public String baojiaCount;
        public String cutEdate;
        public String cutSdate;
        public String fkfs;
        public String isdj;
        public String jsfs;
        public String listState;
        public String listStateStr;
        public String rzgx;
        public String yjxx;
        public String yjxx2;

        SearchRow() {
        }
    }

    class infoRow {
        public String minutes;
        public String numder;
        public String orderid;
        public String seconds;

        infoRow() {
        }
    }

    public void negotiate_fiterButtonAction(View v) {
        int len = this.LisItems2.size();
        if (len == 0) {
            this.sendprice = this.ET_price.getText().toString();
            if (StringEx.Empty.equals(this.sendprice)) {
                new AlertDialog.Builder(this).setMessage("请输入价格").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                }).show().setCanceledOnTouchOutside(false);
            } else if (StringEx.Empty.equals(this.fkfs)) {
                new AlertDialog.Builder(this).setMessage("请选择付款方式").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                }).show().setCanceledOnTouchOutside(false);
            } else if (StringEx.Empty.equals(this.jsfs)) {
                new AlertDialog.Builder(this).setMessage("请选择结算方式").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                }).show().setCanceledOnTouchOutside(false);
            } else {
                Dialog Dialog1 = new AlertDialog.Builder(this).setMessage("是否确定要优惠元" + this.sendprice + "吗？ ").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        Shop_sendNegoRealBusi srBusi = new Shop_sendNegoRealBusi(NegotiateActivity.this);
                        srBusi.gpls = NegotiateActivity.this.gpls_neg;
                        srBusi.fkfs = NegotiateActivity.this.fkfs;
                        srBusi.jsfs = NegotiateActivity.this.jsfs;
                        srBusi.sendPrize = NegotiateActivity.this.sendprice;
                        srBusi.yjdh = NegotiateActivity.this.yjdh_neg;
                        srBusi.zje = NegotiateActivity.this.amount_neg;
                        srBusi.setHttpCallBack(NegotiateActivity.this.httpCallBack2);
                        srBusi.iExecute();
                        NegotiateActivity.this.progressDialog = ProgressDialog.show(NegotiateActivity.this, null, "数据加载中", true, false);
                        NegotiateActivity.this.TV_num.setText("2");
                        NegotiateActivity.this.mc = new MyCount(900000, 1000);
                        NegotiateActivity.this.mc.start();
                    }
                }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).create();
                Dialog1.setCanceledOnTouchOutside(false);
                Dialog1.show();
            }
            this.ET_price.setText(StringEx.Empty);
        } else if (len == 1) {
            if ("等待".equals(this.LisItems2.get(0).listState.toString())) {
                new AlertDialog.Builder(this).setMessage("请耐心等待买家回复！").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                }).show();
            }
        } else if (len <= 1) {
        } else {
            if ("等待".equals(this.LisItems2.get(1).listState.toString())) {
                new AlertDialog.Builder(this).setMessage("请耐心等待买家回复！").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                }).show();
                return;
            }
            int n = 0;
            while (0 < len) {
                if ("拒绝".equals(this.LisItems2.get(0).listState.toString())) {
                    n++;
                }
            }
            if (this.num - n > -1) {
                this.sendprice = this.ET_price.getText().toString();
                if (StringEx.Empty.equals(this.sendprice)) {
                    new AlertDialog.Builder(this).setMessage("请输入价格").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                        }
                    }).show().setCanceledOnTouchOutside(false);
                } else if (StringEx.Empty.equals(this.fkfs)) {
                    new AlertDialog.Builder(this).setMessage("请选择付款方式").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                        }
                    }).show().setCanceledOnTouchOutside(false);
                } else if (StringEx.Empty.equals(this.jsfs)) {
                    new AlertDialog.Builder(this).setMessage("请选择结算方式").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                        }
                    }).show().setCanceledOnTouchOutside(false);
                } else {
                    Dialog dialog = new AlertDialog.Builder(this).setMessage("是否确定要优惠元" + this.sendprice + "吗？").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            NegotiateActivity.this.negotiate_list.removeAllView();
                            Shop_sendNegoRealBusi srBusi = new Shop_sendNegoRealBusi(NegotiateActivity.this);
                            srBusi.gpls = NegotiateActivity.this.gpls_neg;
                            srBusi.fkfs = NegotiateActivity.this.fkfs;
                            srBusi.jsfs = NegotiateActivity.this.jsfs;
                            srBusi.sendPrize = NegotiateActivity.this.sendprice;
                            srBusi.yjdh = NegotiateActivity.this.yjdh_neg;
                            srBusi.zje = NegotiateActivity.this.amount_neg;
                            srBusi.setHttpCallBack(NegotiateActivity.this.httpCallBack2);
                            srBusi.iExecute();
                        }
                    }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    }).create();
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();
                }
                this.ET_price.setText(StringEx.Empty);
                return;
            }
            new AlertDialog.Builder(this).setMessage("您已经议价3次了").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                }
            }).show().setCanceledOnTouchOutside(false);
        }
    }

    public void negotiatet_backButtonAction(View v) {
        ExitApplication.getInstance().back(0);
    }

    public void onBackPressed() {
        ExitApplication.getInstance().back(0);
        super.onBackPressed();
    }

    public void negotiate_refreshAtion(View v) {
        if (this.LisItems2.size() > 0) {
            try {
                this.negotiate_list.removeAllView();
            } catch (Exception e) {
            }
            try {
                this.LisItems2.clear();
            } catch (Exception e2) {
            }
            try {
                this.mc.cancel();
            } catch (Exception e3) {
            }
            getHistory();
        }
    }

    class MyCount extends CountDownTimer {
        public MyCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        public void onFinish() {
            NegotiateActivity.this.tv.setText("0");
            NegotiateActivity.this.tv2.setText("0");
        }

        public void onTick(long mss) {
            NegotiateActivity.this.tv.setText(new StringBuilder().append((mss % 60000) / 1000).toString());
            NegotiateActivity.this.tv2.setText(new StringBuilder().append((mss % 3600000) / 60000).toString());
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.tv = null;
        this.tv2 = null;
        this.TV_num = null;
        this.TV_shengyu = null;
        this.TV_fenzh = null;
        this.TV_miao = null;
        this.TV_shengyu02 = null;
        this.TV_cishu = null;
        this.TV_LLout02 = null;
        this.ET_price = null;
        this.btn_ok = null;
        this.mc = null;
        this.negotiate_list = null;
        this.LisItems = null;
        this.LisItems2 = null;
        this.outOrderId = null;
        this.sendprice = null;
        this.success_stats = null;
        this.view = null;
        this.view2 = null;
        super.onDestroy();
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.radio_search3:
                ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
                return;
            case R.id.radio_shopcar3:
                ((RadioButton) findViewById(R.id.radio_shopcar3)).setChecked(true);
                try {
                    this.mc.cancel();
                } catch (Exception e) {
                }
                ExitApplication.getInstance().startActivity(this, ShopActivity.class);
                return;
            case R.id.radio_home3:
                ((RadioButton) findViewById(R.id.radio_home3)).setChecked(true);
                HashMap hasmap = new HashMap();
                hasmap.put("index_more", "1");
                ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class, hasmap);
                return;
            case R.id.radio_contract3:
                ((RadioButton) findViewById(R.id.radio_contract3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ContractListActivity.class);
                return;
            case R.id.radio_notice3:
                ((RadioButton) findViewById(R.id.radio_notice3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, Xhjy_more.class);
                return;
            default:
                return;
        }
    }
}
