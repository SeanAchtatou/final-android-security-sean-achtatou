package com.baosteelOnline.xhjy;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.RadioButton;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.R;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.common.Utils;
import com.baosteelOnline.main.ExitApplication;
import com.jianq.misc.StringEx;

public class Negotiate_BookActivity extends JQActivity {
    static int gWebviewZoom = 100;
    private WebView aView = null;
    private ImageButton backBtn;
    private String hy = "000027";
    private String jsfs = "5";
    public String project = Utils.getNetConfigProperties().getProperty("project");
    RadioButton radio_notice;
    private String rzgx = StringEx.Empty;
    public String sId = StringEx.Empty;
    private String sUrl_official_dfgt = "http://www.bsteel.com.cn/exchange/";
    private String sUrl_test_dfgt = "http://testex.bsteel.com/newexchange/";
    private String seller = StringEx.Empty;
    private String zje = StringEx.Empty;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        ExitApplication.getInstance().addActivity(this);
        setContentView((int) R.layout.negotiate_book);
        this.backBtn = (ImageButton) findViewById(R.id.homebutton_Negotiate_Book);
        this.radio_notice = (RadioButton) findViewById(R.id.radio_shopcar3);
        this.radio_notice.setChecked(true);
        this.aView = (WebView) findViewById(R.id.weblist_Negotiate_Book);
        this.aView.getSettings().setJavaScriptEnabled(true);
        Bundle bundle = getIntent().getExtras();
        try {
            this.rzgx = bundle.getString("rzgx");
            this.jsfs = bundle.getString("jsfs");
            this.seller = bundle.getString("sellerName");
            this.zje = bundle.getString("zje");
        } catch (Exception e) {
        }
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ExitApplication.getInstance().back(0);
            }
        });
        this.aView.clearView();
        this.aView.setBackgroundColor(0);
        if (gWebviewZoom > 0) {
            this.aView.setInitialScale(gWebviewZoom);
        } else {
            this.aView.getSettings().setSupportZoom(false);
        }
        int screenDensity = getResources().getDisplayMetrics().densityDpi;
        WebSettings.ZoomDensity zoomDensity = WebSettings.ZoomDensity.MEDIUM;
        switch (screenDensity) {
            case 120:
                zoomDensity = WebSettings.ZoomDensity.CLOSE;
                break;
            case 160:
                zoomDensity = WebSettings.ZoomDensity.MEDIUM;
                break;
            case 240:
                zoomDensity = WebSettings.ZoomDensity.FAR;
                break;
        }
        this.aView.getSettings().setDefaultZoom(zoomDensity);
        this.aView.requestFocus();
        this.aView.getSettings().setSupportZoom(true);
        this.aView.getSettings().setBuiltInZoomControls(true);
        if (this.project.equals("test")) {
            this.aView.loadUrl(String.valueOf(this.sUrl_test_dfgt) + "b-1-s/shoppingCartIndex.do?method=toyjxy&buyer=" + ObjectStores.getInst().getObject("parameter_czy") + "&seller=" + this.seller + "&jsfs=" + this.jsfs + "&rzFlag=" + this.rzgx + "&jsje=" + this.zje);
        } else if (this.project.equals("official")) {
            this.aView.loadUrl(String.valueOf(this.sUrl_official_dfgt) + "b-1-s/shoppingCartIndex.do?method=toyjxy&buyer=" + ObjectStores.getInst().getObject("parameter_czy") + "&seller=" + this.seller + "&jsfs=" + this.jsfs + "&rzFlag=" + this.rzgx + "&jsje=" + this.zje);
        }
        this.aView.setWebViewClient(new HelloWebViewClient(this, null));
    }

    private class HelloWebViewClient extends WebViewClient {
        private HelloWebViewClient() {
        }

        /* synthetic */ HelloWebViewClient(Negotiate_BookActivity negotiate_BookActivity, HelloWebViewClient helloWebViewClient) {
            this();
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    public void main_home_buttonaction(View v) {
        ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class);
    }

    public void xhjy_search_buttonaction(View v) {
        ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
    }

    public void xhjy_shop_buttonaction(View v) {
    }

    public void xhjy_home_buttonaction(View v) {
        ExitApplication.getInstance().startActivity(this, ContractListActivity.class);
    }

    public void xhjy_more_buttonaction(View v) {
        ExitApplication.getInstance().startActivity(this, Xhjy_more.class);
    }

    public void onBackPressed() {
        super.onBackPressed();
        ExitApplication.getInstance().back(0);
    }
}
