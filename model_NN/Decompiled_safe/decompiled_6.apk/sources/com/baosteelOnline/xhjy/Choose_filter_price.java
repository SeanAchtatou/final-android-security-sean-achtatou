package com.baosteelOnline.xhjy;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.R;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.model.SmallNumUtil;
import com.jianq.misc.StringEx;
import java.util.HashMap;

public class Choose_filter_price extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    private Button btn_ok;
    /* access modifiers changed from: private */
    public int longmax = 0;
    /* access modifiers changed from: private */
    public int longmin = 0;
    /* access modifiers changed from: private */
    public String pMax;
    /* access modifiers changed from: private */
    public String pMin;
    /* access modifiers changed from: private */
    public EditText priceMax;
    /* access modifiers changed from: private */
    public EditText priceMin;
    private RadioGroup radioderGroup;
    private TextView titletext;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        ExitApplication.getInstance().addActivity(this);
        setContentView((int) R.layout.xhjy_choose_filter_price);
        this.titletext = (TextView) findViewById(R.id.TV_titletext_history_length);
        this.titletext.setText("搜索价格");
        this.priceMin = (EditText) findViewById(R.id.choose_filter_priceMin);
        this.priceMax = (EditText) findViewById(R.id.choose_filter_price8Max);
        ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
        SmallNumUtil.GWCNum(this, (TextView) findViewById(R.id.gwc_dhqrnum));
        this.radioderGroup = (RadioGroup) findViewById(R.id.xhjy_index_RGroup);
        this.radioderGroup.setOnCheckedChangeListener(this);
        this.btn_ok = (Button) findViewById(R.id.choose_filter_price_ok);
        this.btn_ok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Choose_filter_price.this.pMin = Choose_filter_price.this.priceMin.getText().toString();
                Choose_filter_price.this.pMax = Choose_filter_price.this.priceMax.getText().toString();
                try {
                    if (!Choose_filter_price.this.pMin.equals(StringEx.Empty)) {
                        Choose_filter_price.this.longmin = Integer.valueOf(Choose_filter_price.this.pMin).intValue();
                    }
                    if (!Choose_filter_price.this.pMax.equals(StringEx.Empty)) {
                        Choose_filter_price.this.longmax = Integer.valueOf(Choose_filter_price.this.pMax).intValue();
                    }
                } catch (Exception e) {
                }
                if (Choose_filter_price.this.pMin.equals(StringEx.Empty) && Choose_filter_price.this.pMax.equals(StringEx.Empty)) {
                    new AlertDialog.Builder(Choose_filter_price.this).setMessage("输入的最小值必须比最大值小！").setPositiveButton("确认", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                        }
                    }).show();
                } else if (Choose_filter_price.this.longmin > Choose_filter_price.this.longmax) {
                    new AlertDialog.Builder(Choose_filter_price.this).setMessage("输入的最小值必须比最大值小！").setPositiveButton("确认", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                        }
                    }).show();
                } else {
                    ObjectStores.getInst().putObject("price_sign", "11");
                    ObjectStores.getInst().putObject("pricemin", Choose_filter_price.this.pMin);
                    ObjectStores.getInst().putObject("pricemax", Choose_filter_price.this.pMax);
                    ExitApplication.getInstance().startActivity(Choose_filter_price.this, ChooseActivity.class);
                }
            }
        });
    }

    public void xhjy_choose_back_ButtonAction(View v) {
        ExitApplication.getInstance().back(0);
    }

    public void onBackPressed() {
        ExitApplication.getInstance().back(0);
        super.onBackPressed();
    }

    public void choose_filter_all_priceAction(View v) {
        ObjectStores.getInst().putObject("price_sign", "00");
        ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
    }

    public void choose_filter_all_price14Action(View v) {
        ObjectStores.getInst().putObject("price_sign", "01");
        ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
    }

    public void choose_filter_all_price46Action(View v) {
        ObjectStores.getInst().putObject("price_sign", "02");
        ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
    }

    public void choose_filter_all_price68Action(View v) {
        ObjectStores.getInst().putObject("price_sign", "03");
        ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
    }

    public void choose_filter_all_price8Action(View v) {
        ObjectStores.getInst().putObject("price_sign", "04");
        ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.priceMin = null;
        this.priceMax = null;
        this.btn_ok = null;
        this.pMin = null;
        this.pMax = null;
        this.longmin = 0;
        this.longmax = 0;
        super.onDestroy();
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.radio_search3:
                ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
                return;
            case R.id.radio_shopcar3:
                ((RadioButton) findViewById(R.id.radio_shopcar3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ShopActivity.class);
                return;
            case R.id.radio_home3:
                ((RadioButton) findViewById(R.id.radio_home3)).setChecked(true);
                HashMap hasmap = new HashMap();
                hasmap.put("index_more", "1");
                ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class, hasmap);
                return;
            case R.id.radio_contract3:
                ((RadioButton) findViewById(R.id.radio_contract3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ContractListActivity.class);
                return;
            case R.id.radio_notice3:
                ((RadioButton) findViewById(R.id.radio_notice3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, Xhjy_more.class);
                return;
            default:
                return;
        }
    }
}
