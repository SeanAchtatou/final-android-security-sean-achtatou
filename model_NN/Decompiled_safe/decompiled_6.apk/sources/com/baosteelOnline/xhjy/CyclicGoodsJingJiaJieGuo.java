package com.baosteelOnline.xhjy;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.R;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.data.RequestBidData;
import com.baosteelOnline.data_parse.ContractParse;
import com.baosteelOnline.dl.Login_indexActivity;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.model.SmallNumUtil;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;

public class CyclicGoodsJingJiaJieGuo extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    private String beginDate = StringEx.Empty;
    /* access modifiers changed from: private */
    public String changciNum;
    public ContractParse contractParse;
    private String endDate = StringEx.Empty;
    /* access modifiers changed from: private */
    public boolean hasNextTen = false;
    private JQUICallBack httpCallBack = new JQUICallBack() {
        public void callBack(ResultData result) {
            String nextString;
            CyclicGoodsJingJiaJieGuo.this.progressDialog.dismiss();
            Log.d("获取的数据：", result.getStringData());
            String dataString = result.getStringData();
            if (!dataString.equals(null) && !dataString.equals(StringEx.Empty)) {
                CyclicGoodsJingJiaJieGuo.this.contractParse = ContractParse.parse(result.getStringData());
                if (ContractParse.pageNumTal.equals(null) || ContractParse.pageNumTal.equals(StringEx.Empty)) {
                    CyclicGoodsJingJiaJieGuo.this.numOfPageGet = 1;
                } else {
                    CyclicGoodsJingJiaJieGuo.this.numOfPageGet = Integer.parseInt(ContractParse.pageNumTal);
                }
                if (CyclicGoodsJingJiaJieGuo.this.nowNum < CyclicGoodsJingJiaJieGuo.this.numOfPageGet) {
                    CyclicGoodsJingJiaJieGuo.this.hasNextTen = true;
                } else {
                    CyclicGoodsJingJiaJieGuo.this.hasNextTen = false;
                }
                Log.d("收到的数据：", ContractParse.decryptData);
                if (ContractParse.getDataString().equals(null) || ContractParse.getDataString().equals(StringEx.Empty)) {
                    new AlertDialog.Builder(CyclicGoodsJingJiaJieGuo.this).setMessage("获取数据失败").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
                } else {
                    Log.d("获取的ROWs数据：", ContractParse.jjo.toString());
                    if (ContractParse.getDataString().equals(null) || ContractParse.getDataString().equals(StringEx.Empty) || ContractParse.jjo.toString().equals("[]") || ContractParse.jjo.toString().equals("[[]]")) {
                        new AlertDialog.Builder(CyclicGoodsJingJiaJieGuo.this).setMessage("数据为空！").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
                    } else {
                        for (int i = 0; i < ContractParse.jjo.length(); i++) {
                            try {
                                JSONArray row = new JSONArray(ContractParse.jjo.getString(i));
                                HashMap<String, Object> item = new HashMap<>();
                                CyclicGoodsJingJiaJieGuo.this.changciNum = row.getString(0);
                                CyclicGoodsJingJiaJieGuo.this.seller = row.getString(1);
                                CyclicGoodsJingJiaJieGuo.this.times = row.getString(2);
                                CyclicGoodsJingJiaJieGuo.this.isShowDealResult = row.getString(3);
                                CyclicGoodsJingJiaJieGuo.this.isShowDealMember = row.getString(4);
                                CyclicGoodsJingJiaJieGuo.this.isShowDealPrice = row.getString(5);
                                item.put("changcinum", CyclicGoodsJingJiaJieGuo.this.changciNum);
                                item.put("seller", CyclicGoodsJingJiaJieGuo.this.seller);
                                item.put("times", CyclicGoodsJingJiaJieGuo.this.times);
                                item.put("isShowDealResult", CyclicGoodsJingJiaJieGuo.this.isShowDealResult);
                                item.put("isShowDealMember", CyclicGoodsJingJiaJieGuo.this.isShowDealMember);
                                item.put("isShowDealPrice", CyclicGoodsJingJiaJieGuo.this.isShowDealPrice);
                                CyclicGoodsJingJiaJieGuo.this.mItemArrayList.add(item);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        HashMap<String, Object> item2 = new HashMap<>();
                        if (CyclicGoodsJingJiaJieGuo.this.hasNextTen) {
                            nextString = "下10条...";
                        } else {
                            nextString = "已经是最后一页";
                        }
                        item2.put("nextString", nextString);
                        CyclicGoodsJingJiaJieGuo.this.mItemArrayList.add(item2);
                    }
                }
            }
            CyclicGoodsJingJiaJieGuo.this.myAdapter.notifyDataSetChanged();
        }
    };
    private boolean isLastPage = false;
    /* access modifiers changed from: private */
    public String isShowDealMember = StringEx.Empty;
    /* access modifiers changed from: private */
    public String isShowDealPrice = StringEx.Empty;
    /* access modifiers changed from: private */
    public String isShowDealResult = StringEx.Empty;
    private RadioButton mIndexNavigation1;
    private RadioButton mIndexNavigation2;
    private RadioButton mIndexNavigation3;
    private RadioButton mIndexNavigation4;
    private RadioButton mIndexNavigation5;
    /* access modifiers changed from: private */
    public ArrayList<HashMap<String, Object>> mItemArrayList;
    public TextView mListTitleTextview;
    private ListView mListView;
    private RadioGroup mRadioderGroup;
    private String maiFanDanwei = StringEx.Empty;
    private boolean markImage = true;
    /* access modifiers changed from: private */
    public MyAdapter myAdapter;
    /* access modifiers changed from: private */
    public int nowNum = 1;
    private int numOfPage = 1;
    /* access modifiers changed from: private */
    public int numOfPageGet = 1;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    /* access modifiers changed from: private */
    public String seller;
    /* access modifiers changed from: private */
    public String times;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.activity_cyclic_goods_jing_jia_jie_guo);
        ExitApplication.getInstance().addActivity(this);
        try {
            this.maiFanDanwei = getIntent().getExtras().getString("maiFanDanwei");
            this.beginDate = getIntent().getExtras().getString("beginDate");
            this.endDate = getIntent().getExtras().getString("endDate");
        } catch (Exception e) {
            this.maiFanDanwei = StringEx.Empty;
            this.beginDate = StringEx.Empty;
            this.endDate = StringEx.Empty;
        }
        this.mListView = (ListView) findViewById(R.id.CyclicGoodsJingJiaJieGuo_listview);
        this.mItemArrayList = new ArrayList<>();
        this.mListTitleTextview = (TextView) findViewById(R.id.cyclicgoods_list_title_textview);
        this.mListTitleTextview.setText("竞价结果");
        if (ObjectStores.getInst().getObject("reStr") != "1") {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setCancelable(false);
            dialog.setMessage("您还没有登录，请登录！").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    SharedPreferences.Editor editor = CyclicGoodsJingJiaJieGuo.this.getSharedPreferences("index_mark", 2).edit();
                    editor.putString("index", "竞价结果列表");
                    editor.commit();
                    CyclicGoodsJingJiaJieGuo.this.startActivity(new Intent(CyclicGoodsJingJiaJieGuo.this, Login_indexActivity.class));
                    CyclicGoodsJingJiaJieGuo.this.finish();
                }
            }).show();
            return;
        }
        this.myAdapter = new MyAdapter(this);
        this.mRadioderGroup = (RadioGroup) findViewById(R.id.cyclic_goods_index_RGroup);
        this.mIndexNavigation1 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item1);
        this.mIndexNavigation2 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item2);
        this.mIndexNavigation3 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item3);
        this.mIndexNavigation4 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item4);
        this.mIndexNavigation5 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item5);
        SmallNumUtil.JJNum(this, (TextView) findViewById(R.id.jj_dhqrnum));
        this.mIndexNavigation1.setChecked(true);
        this.mRadioderGroup.setOnCheckedChangeListener(this);
        this.mIndexNavigation1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ExitApplication.getInstance().startActivity(CyclicGoodsJingJiaJieGuo.this, CyclicGoodsIndexActivity.class);
                CyclicGoodsJingJiaJieGuo.this.finish();
            }
        });
        this.mListView.setAdapter((ListAdapter) this.myAdapter);
        this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                if (arg2 != CyclicGoodsJingJiaJieGuo.this.mItemArrayList.size() - 1) {
                    Intent intent = new Intent(CyclicGoodsJingJiaJieGuo.this, CyclicGoodsJinJiaJieGuo2.class);
                    String changciNum = ((HashMap) CyclicGoodsJingJiaJieGuo.this.mItemArrayList.get(arg2)).get("changcinum").toString();
                    String isShowDealResult = ((HashMap) CyclicGoodsJingJiaJieGuo.this.mItemArrayList.get(arg2)).get("isShowDealResult").toString();
                    String isShowDealMember = ((HashMap) CyclicGoodsJingJiaJieGuo.this.mItemArrayList.get(arg2)).get("isShowDealMember").toString();
                    String isShowDealPrice = ((HashMap) CyclicGoodsJingJiaJieGuo.this.mItemArrayList.get(arg2)).get("isShowDealPrice").toString();
                    intent.putExtra("ppid", changciNum);
                    intent.putExtra("isShowDealResult", isShowDealResult);
                    intent.putExtra("isShowDealMember", isShowDealMember);
                    intent.putExtra("isShowDealPrice", isShowDealPrice);
                    CyclicGoodsJingJiaJieGuo.this.startActivity(intent);
                } else if (CyclicGoodsJingJiaJieGuo.this.hasNextTen) {
                    CyclicGoodsJingJiaJieGuo.this.mItemArrayList.remove(arg2);
                    CyclicGoodsJingJiaJieGuo.this.getNextTen();
                }
            }
        });
        getData(1);
    }

    private void getData(int num) {
        RequestBidData requestBidData = new RequestBidData(this);
        requestBidData.setRequestData("竞价结果列表");
        requestBidData.setPageNum(new StringBuilder(String.valueOf(num)).toString());
        requestBidData.setSellerName(this.maiFanDanwei);
        requestBidData.setStartDate(this.beginDate);
        requestBidData.setEndDate(this.endDate);
        this.nowNum = num;
        Log.d("请求的指令：", requestBidData.infoDate());
        requestBidData.setHttpCallBack(this.httpCallBack);
        requestBidData.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    /* access modifiers changed from: private */
    public void getNextTen() {
        this.nowNum++;
        getData(this.nowNum);
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.cyclicgoods_index_navigation_item2:
                this.mIndexNavigation2.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item3:
                this.mIndexNavigation3.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidSessionSelect.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item1:
                if (ObjectStores.getInst().getObject("formX") == "formX") {
                    ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class);
                } else {
                    this.mIndexNavigation1.setChecked(true);
                    ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
                }
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item4:
                this.mIndexNavigation4.setChecked(true);
                startActivity(new Intent(this, CyclicGoodsBidHallPageActivity.class));
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item5:
                this.mIndexNavigation5.setChecked(true);
                Intent intent = new Intent(this, Xhjy_more.class);
                intent.putExtra("part", "循环物资");
                startActivity(intent);
                finish();
                return;
            default:
                return;
        }
    }

    public void selectAction(View v) {
        startActivity(new Intent(this, CyclicGoodsJieGuoSelect.class));
        finish();
    }

    public void backAaction(View v) {
        if (ObjectStores.getInst().getObject("formX") != "formX") {
            ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
        } else {
            ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class);
        }
        finish();
    }

    public void onBackPressed() {
        if (ObjectStores.getInst().getObject("formX") != "formX") {
            ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
        } else {
            ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class);
        }
        finish();
    }

    public class ViewHolder {
        public TextView changciNum;
        public TextView changciNums;
        public TextView lastTen;
        public TextView nextTen;
        public TextView seller;
        public TextView sellers;
        public TextView times;
        public TextView timess;

        public ViewHolder() {
        }
    }

    public class MyAdapter extends BaseAdapter {
        private LayoutInflater mMyAdapterInflater;

        public MyAdapter(Context context) {
            this.mMyAdapterInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return CyclicGoodsJingJiaJieGuo.this.mItemArrayList.size();
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (position == CyclicGoodsJingJiaJieGuo.this.mItemArrayList.size() - 1) {
                ViewHolder holder1 = new ViewHolder();
                View convertView2 = this.mMyAdapterInflater.inflate((int) R.layout.next_ten, (ViewGroup) null);
                holder1.nextTen = (TextView) convertView2.findViewById(R.id.next_ten_text);
                convertView2.setTag(holder1);
                holder1.nextTen.setText((String) ((HashMap) CyclicGoodsJingJiaJieGuo.this.mItemArrayList.get(position)).get("nextString"));
                return convertView2;
            }
            ViewHolder holder = new ViewHolder();
            View convertView3 = this.mMyAdapterInflater.inflate((int) R.layout.bidding_result_index_row, (ViewGroup) null);
            holder.changciNum = (TextView) convertView3.findViewById(R.id.bidding_result_index_item_num);
            holder.seller = (TextView) convertView3.findViewById(R.id.bidding_result_index_item_sell_unit_name);
            holder.times = (TextView) convertView3.findViewById(R.id.bidding_result_index_item_sell_month);
            holder.changciNums = (TextView) convertView3.findViewById(R.id.bidding_result_index_item_num_name);
            holder.sellers = (TextView) convertView3.findViewById(R.id.bidding_result_index_item_sell_unit);
            holder.timess = (TextView) convertView3.findViewById(R.id.bidding_result_index_item_sell_time);
            holder.changciNum.setText((String) ((HashMap) CyclicGoodsJingJiaJieGuo.this.mItemArrayList.get(position)).get("changcinum"));
            holder.seller.setText((String) ((HashMap) CyclicGoodsJingJiaJieGuo.this.mItemArrayList.get(position)).get("seller"));
            holder.times.setText((String) ((HashMap) CyclicGoodsJingJiaJieGuo.this.mItemArrayList.get(position)).get("times"));
            convertView3.setTag(holder);
            return convertView3;
        }
    }
}
