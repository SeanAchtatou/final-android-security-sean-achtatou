package com.baosteelOnline.xhjy;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.R;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.data.RequestBidData;
import com.baosteelOnline.data_parse.ContractParse;
import com.baosteelOnline.main.ExitApplication;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.net.JQBasicNetwork;
import com.jianq.ui.JQUICallBack;
import org.json.JSONArray;
import org.json.JSONException;

public class CyclicGoodsBidProtocolPageActivity extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    private String activityName;
    public ContractParse contractParse;
    private String dfyj = StringEx.Empty;
    /* access modifiers changed from: private */
    public Double dfyjDouble = Double.valueOf(0.0d);
    /* access modifiers changed from: private */
    public String getDataName = StringEx.Empty;
    private JQUICallBack httpCallBack = new JQUICallBack() {
        public void callBack(ResultData result) {
            String msgString;
            CyclicGoodsBidProtocolPageActivity.this.progressDialog.dismiss();
            Log.d("CyclicGoodsBidProtocolPageActivity获取的数据", result.getStringData());
            if (!result.getStringData().equals(null) && !result.getStringData().equals(StringEx.Empty)) {
                CyclicGoodsBidProtocolPageActivity.this.contractParse = ContractParse.parse(result.getStringData());
                if (ContractParse.getDataString().equals(null) || ContractParse.getDataString().equals(StringEx.Empty)) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(CyclicGoodsBidProtocolPageActivity.this);
                    dialog.setCancelable(false);
                    dialog.setMessage("获取数据失败").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            CyclicGoodsBidProtocolPageActivity.this.outActivity();
                        }
                    }).show();
                    return;
                }
                Log.d("获取的ROWs数据：", ContractParse.jjo.toString());
                Log.d("获取的DataString数据：", ContractParse.getDataString());
                System.out.println(ContractParse.getDataString());
                if (!ContractParse.jjo.toString().equals(null) && !ContractParse.jjo.toString().equals("[[]]") && !ContractParse.jjo.toString().equals(StringEx.Empty) && !ContractParse.jjo.toString().equals("[]")) {
                    for (int i = 0; i < ContractParse.jjo.length(); i++) {
                        String mHtmlData = StringEx.Empty;
                        String resultString = StringEx.Empty;
                        try {
                            JSONArray row = new JSONArray(ContractParse.jjo.getString(i));
                            if (CyclicGoodsBidProtocolPageActivity.this.getDataName.equals("协议条款同意")) {
                                resultString = row.getString(0);
                                String wtzt = row.getString(1);
                            } else if (CyclicGoodsBidProtocolPageActivity.this.getDataName.equals("卖家协议")) {
                                mHtmlData = row.getString(0);
                            } else {
                                mHtmlData = row.getString(0);
                                CyclicGoodsBidProtocolPageActivity.this.dfyjDouble = Double.valueOf(row.getDouble(1));
                                CyclicGoodsBidProtocolPageActivity.this.kyyeDouble = Double.valueOf(row.getDouble(2));
                            }
                            Log.d("CyclicGoodsBidPageActivity获取的ROWs数据HtmlData：", mHtmlData);
                            if (CyclicGoodsBidProtocolPageActivity.this.getDataName.equals("卖家协议")) {
                                if (mHtmlData.equals(StringEx.Empty) || mHtmlData.equals(null)) {
                                    CyclicGoodsBidProtocolPageActivity.this.ismHtmlData = false;
                                    CyclicGoodsBidProtocolPageActivity.this.mListTitleTextview.setText("竞价协议");
                                    CyclicGoodsBidProtocolPageActivity.this.getDate("竞价协议");
                                } else {
                                    CyclicGoodsBidProtocolPageActivity.this.ismHtmlData = true;
                                    CyclicGoodsBidProtocolPageActivity.this.mListTitleTextview.setText("卖家协议");
                                    Log.d("卖家协议mHtmlData", "===" + mHtmlData);
                                    CyclicGoodsBidProtocolPageActivity.this.mWebView.loadDataWithBaseURL(null, mHtmlData, "text/html", JQBasicNetwork.UTF_8, null);
                                }
                            } else if (CyclicGoodsBidProtocolPageActivity.this.getDataName.equals("竞价协议")) {
                                CyclicGoodsBidProtocolPageActivity.this.mListTitleTextview.setText("竞价协议");
                                CyclicGoodsBidProtocolPageActivity.this.mWebView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
                                CyclicGoodsBidProtocolPageActivity.this.mWebView.getSettings().setUseWideViewPort(true);
                                CyclicGoodsBidProtocolPageActivity.this.mWebView.getSettings().setLoadWithOverviewMode(true);
                                CyclicGoodsBidProtocolPageActivity.this.ismHtmlData = false;
                                if (!mHtmlData.equals("null") && !mHtmlData.equals(StringEx.Empty) && !mHtmlData.equals(null)) {
                                    CyclicGoodsBidProtocolPageActivity.this.mWebView.loadDataWithBaseURL(null, mHtmlData, "text/html", JQBasicNetwork.UTF_8, null);
                                    new CountDownTimer(10000, 1000) {
                                        public void onTick(long millisUntilFinished) {
                                            CyclicGoodsBidProtocolPageActivity.this.mAgreeButton.setTextColor(-7829368);
                                            CyclicGoodsBidProtocolPageActivity.this.mAgreeButton.setText("同意协议（ " + (millisUntilFinished / 1000) + "）");
                                        }

                                        public void onFinish() {
                                            CyclicGoodsBidProtocolPageActivity.this.mAgreeButton.setText("同意协议");
                                            CyclicGoodsBidProtocolPageActivity.this.mAgreeButton.setTextColor(-16777216);
                                            CyclicGoodsBidProtocolPageActivity.this.timeOfAgree = true;
                                        }
                                    }.start();
                                }
                            } else if (CyclicGoodsBidProtocolPageActivity.this.getDataName.equals("协议条款同意")) {
                                if (resultString.equals("1")) {
                                    if (CyclicGoodsBidProtocolPageActivity.this.wtid.startsWith("J")) {
                                        new AlertDialog.Builder(CyclicGoodsBidProtocolPageActivity.this).setMessage("您已成功参加竞价！该场次为集合议价，无法在移动客户端进行出价！").setPositiveButton("参加其他场次", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                ExitApplication.getInstance().startActivity(CyclicGoodsBidProtocolPageActivity.this, CyclicGoodsBidSessionSelect.class);
                                                CyclicGoodsBidProtocolPageActivity.this.finish();
                                            }
                                        }).show();
                                    } else {
                                        new AlertDialog.Builder(CyclicGoodsBidProtocolPageActivity.this).setMessage("您已成功参加竞价！").setNegativeButton("开始出价", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                CyclicGoodsBidProtocolPageActivity.this.startActivity(new Intent(CyclicGoodsBidProtocolPageActivity.this, CyclicGoodsBidHallPageActivity.class));
                                                CyclicGoodsBidProtocolPageActivity.this.finish();
                                            }
                                        }).setPositiveButton("其他场次", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                ExitApplication.getInstance().startActivity(CyclicGoodsBidProtocolPageActivity.this, CyclicGoodsBidSessionSelect.class);
                                                CyclicGoodsBidProtocolPageActivity.this.finish();
                                            }
                                        }).show();
                                    }
                                } else if (resultString.equals("0")) {
                                    new AlertDialog.Builder(CyclicGoodsBidProtocolPageActivity.this).setMessage("参加竞价失败！").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    }).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } else if (CyclicGoodsBidProtocolPageActivity.this.getDataName.equals("卖家协议")) {
                    CyclicGoodsBidProtocolPageActivity.this.ismHtmlData = false;
                    CyclicGoodsBidProtocolPageActivity.this.getDate("竞价协议");
                } else {
                    if (ContractParse.msgString.equals(StringEx.Empty) || ContractParse.msgString.equals("null") || ContractParse.msgString.equals(null)) {
                        msgString = "资质不符合，不能参加该场次";
                    } else {
                        msgString = ContractParse.msgString;
                    }
                    AlertDialog.Builder dialog2 = new AlertDialog.Builder(CyclicGoodsBidProtocolPageActivity.this);
                    dialog2.setCancelable(false);
                    dialog2.setMessage(msgString).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            CyclicGoodsBidProtocolPageActivity.this.outActivity();
                        }
                    }).show();
                }
            }
        }
    };
    private String isJoin = StringEx.Empty;
    /* access modifiers changed from: private */
    public boolean ismHtmlData = true;
    private String kyye = StringEx.Empty;
    /* access modifiers changed from: private */
    public Double kyyeDouble = Double.valueOf(0.0d);
    /* access modifiers changed from: private */
    public Button mAgreeButton;
    private RadioButton mIndexNavigation1;
    private RadioButton mIndexNavigation2;
    private RadioButton mIndexNavigation3;
    private RadioButton mIndexNavigation4;
    private RadioButton mIndexNavigation5;
    public TextView mListTitleTextview;
    private RadioGroup mRadioderGroup;
    /* access modifiers changed from: private */
    public WebView mWebView;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    /* access modifiers changed from: private */
    public boolean timeOfAgree = false;
    /* access modifiers changed from: private */
    public String wtid = StringEx.Empty;

    /* access modifiers changed from: protected */
    @SuppressLint({"SetJavaScriptEnabled"})
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.activity_cyclic_goods_bid_protocol_page);
        this.mAgreeButton = (Button) findViewById(R.id.cyclicgoods_btn_agree);
        ExitApplication.getInstance().addActivity(this);
        this.wtid = getIntent().getExtras().getString("wtid");
        this.activityName = getIntent().getExtras().getString("activity");
        Log.d("获取的ID", this.wtid);
        this.mWebView = (WebView) findViewById(R.id.cyclicgoods_webview_protocol);
        this.mWebView.getSettings().setBuiltInZoomControls(true);
        this.mWebView.setBackgroundColor(Color.parseColor("#ffffff"));
        this.mWebView.setFocusable(true);
        this.mWebView.requestFocus();
        this.mWebView.clearCache(true);
        this.mWebView.getSettings().setJavaScriptEnabled(true);
        this.mWebView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        this.mWebView.setWebViewClient(new HelloWebViewClient(this, null));
        this.mRadioderGroup = (RadioGroup) findViewById(R.id.cyclic_goods_index_RGroup);
        this.mIndexNavigation1 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item1);
        this.mIndexNavigation2 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item2);
        this.mIndexNavigation3 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item3);
        this.mIndexNavigation4 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item4);
        this.mIndexNavigation5 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item5);
        this.mListTitleTextview = (TextView) findViewById(R.id.cyclicgoods_list_title_textview);
        this.mListTitleTextview.setText("卖家协议");
        this.mIndexNavigation2.setChecked(true);
        this.mRadioderGroup.setOnCheckedChangeListener(this);
        getDate("卖家协议");
    }

    private class HelloWebViewClient extends WebViewClient {
        private HelloWebViewClient() {
        }

        /* synthetic */ HelloWebViewClient(CyclicGoodsBidProtocolPageActivity cyclicGoodsBidProtocolPageActivity, HelloWebViewClient helloWebViewClient) {
            this();
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    public void getDate(String name) {
        RequestBidData requestBidData = new RequestBidData(this);
        requestBidData.setRequestData(name);
        this.getDataName = name;
        requestBidData.setId(this.wtid);
        Log.d("请求的指令：", requestBidData.infoDate());
        requestBidData.setHttpCallBack(this.httpCallBack);
        requestBidData.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    public void onCheckedChanged(RadioGroup arg0, int arg1) {
        switch (arg1) {
            case R.id.cyclicgoods_index_navigation_item2:
                this.mIndexNavigation2.setChecked(true);
                return;
            case R.id.cyclicgoods_index_navigation_item3:
                this.mIndexNavigation3.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidSessionSelect.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item1:
                if (ObjectStores.getInst().getObject("formX") == "formX") {
                    ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class);
                    finish();
                    return;
                }
                this.mIndexNavigation1.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item4:
                this.mIndexNavigation4.setChecked(true);
                startActivity(new Intent(this, CyclicGoodsBidHallPageActivity.class));
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item5:
                this.mIndexNavigation5.setChecked(true);
                Intent intent = new Intent(this, Xhjy_more.class);
                intent.putExtra("part", "循环物资");
                startActivity(intent);
                finish();
                return;
            default:
                return;
        }
    }

    public void agreeAction(View v) {
        if (this.ismHtmlData) {
            getDate("竞价协议");
        } else if (!this.timeOfAgree) {
        } else {
            if (this.dfyjDouble.doubleValue() == 0.0d || this.dfyjDouble.doubleValue() == 0.0d) {
                getDate("协议条款同意");
            } else if (this.dfyjDouble.doubleValue() > this.kyyeDouble.doubleValue()) {
                new AlertDialog.Builder(this).setMessage("余额不足！").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).show();
            } else {
                getDate("协议条款同意");
            }
        }
    }

    public void backAaction(View v) {
        outActivity();
    }

    /* access modifiers changed from: private */
    public void outActivity() {
        Intent intent;
        if (this.activityName.equals("竞价场次")) {
            try {
                Intent intent2 = new Intent(this, CyclicGoodsBidSession.class);
                try {
                    int sessionCode = getIntent().getExtras().getInt("sessionCode");
                    String startDate = getIntent().getExtras().getString("startDate");
                    String endDate = getIntent().getExtras().getString("endDate");
                    intent2.putExtra("sessionCode", sessionCode);
                    intent2.putExtra("startDate", startDate);
                    intent2.putExtra("endDate", endDate);
                    intent = intent2;
                } catch (Exception e) {
                    intent = new Intent(this, CyclicGoodsBidSessionSelect.class);
                    startActivity(intent);
                    finish();
                }
            } catch (Exception e2) {
                intent = new Intent(this, CyclicGoodsBidSessionSelect.class);
                startActivity(intent);
                finish();
            }
        } else {
            intent = new Intent(this, CyclicGoodsBidPageActivity.class);
            String activity2 = getIntent().getExtras().getString("activity2");
            intent.putExtra("id", getIntent().getExtras().getString("id"));
            intent.putExtra("activity", activity2);
            if (activity2.equals("竞价场次")) {
                int sessionCode2 = getIntent().getExtras().getInt("sessionCode");
                String startDate2 = getIntent().getExtras().getString("startDate");
                String endDate2 = getIntent().getExtras().getString("endDate");
                intent.putExtra("sessionCode", sessionCode2);
                intent.putExtra("startDate", startDate2);
                intent.putExtra("endDate", endDate2);
            }
            Log.d("CyclicGoodsBidProtocolPageActivity收到竞价公告的详情页的id：", String.valueOf(getIntent().getExtras().getString("id")) + "收到的activity：" + getIntent().getExtras().getString("activity"));
        }
        startActivity(intent);
        finish();
    }

    public void onBackPressed() {
        outActivity();
    }
}
