package com.baosteelOnline.xhjy;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.ViewFlipper;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.R;
import com.baosteelOnline.common.DataBaseFactory;
import com.baosteelOnline.common.Dataofdate;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.common.MyDigitalClock;
import com.baosteelOnline.common.ViewPagerAdapter;
import com.baosteelOnline.data.RequestBidData;
import com.baosteelOnline.data_parse.ContractParse;
import com.baosteelOnline.dl.Login_indexActivity;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.sql.DBHelper;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;

public class CyclicGoodsBidHallPageActivity extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    /* access modifiers changed from: private */
    public int GZ_num = 0;
    private ColorDrawable backgroundColorDrawable;
    /* access modifiers changed from: private */
    public View bidItem;
    /* access modifiers changed from: private */
    public String bjtd;
    /* access modifiers changed from: private */
    public String bpfs;
    /* access modifiers changed from: private */
    public TextView cyclicgoods_page_num;
    private DataBaseFactory db;
    private DBHelper dbHelper;
    /* access modifiers changed from: private */
    public String dqj;
    /* access modifiers changed from: private */
    public boolean firstTimeGetData = true;
    /* access modifiers changed from: private */
    public EditText freePriceEditText;
    /* access modifiers changed from: private */
    public ImageButton freePriceImbt;
    private ListView guanZhu;
    /* access modifiers changed from: private */
    public PopupWindow guanZhuPop;
    /* access modifiers changed from: private */
    public View guanZhuPopView;
    /* access modifiers changed from: private */
    public String gz_bjtd;
    private String gz_bpfs;
    /* access modifiers changed from: private */
    public String gz_dqj;
    /* access modifiers changed from: private */
    public String gz_id;
    /* access modifiers changed from: private */
    public String gz_jjms;
    /* access modifiers changed from: private */
    public String gz_myPrice;
    /* access modifiers changed from: private */
    public String gz_wtmc;
    /* access modifiers changed from: private */
    public String gz_wtprice;
    private JQUICallBack httpCallBack = new JQUICallBack() {
        public void callBack(ResultData result) {
            CyclicGoodsBidHallPageActivity.this.progressDialog.dismiss();
            Log.d("获取的竞价大厅列表数据,未解密，未解析：：", result.getStringData());
            String dataString = result.getStringData();
            if (dataString.equals(null) || dataString.equals(StringEx.Empty)) {
                CyclicGoodsBidHallPageActivity.this.cyclicgoods_page_num.setVisibility(8);
                AlertDialog.Builder dialog = new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this);
                dialog.setCancelable(false);
                dialog.setMessage("获取数据失败").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        CyclicGoodsBidHallPageActivity.this.startActivity(new Intent(CyclicGoodsBidHallPageActivity.this, CyclicGoodsIndexActivity.class));
                        CyclicGoodsBidHallPageActivity.this.finish();
                    }
                }).show();
                return;
            }
            ContractParse parse = ContractParse.parse(result.getStringData());
            Log.d("获取的竞价大厅列表数据，已解密，未解析：", ContractParse.getDecryptData());
            int nowPage = CyclicGoodsBidHallPageActivity.this.mViewPager.getCurrentItem() + 1;
            if (ContractParse.getDataString().equals(null) || ContractParse.getDataString().equals(StringEx.Empty)) {
                CyclicGoodsBidHallPageActivity.this.cyclicgoods_page_num.setVisibility(8);
                new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this).setMessage("当前无参加的竞价场次").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        CyclicGoodsBidHallPageActivity.this.startActivity(new Intent(CyclicGoodsBidHallPageActivity.this, CyclicGoodsIndexActivity.class));
                        CyclicGoodsBidHallPageActivity.this.finish();
                    }
                }).show();
                return;
            }
            if (ContractParse.pageNumTal.equals(null) || ContractParse.pageNumTal.equals(StringEx.Empty)) {
                CyclicGoodsBidHallPageActivity.this.numOfPageGet = 1;
                CyclicGoodsBidHallPageActivity.this.rightImbt.setBackgroundResource(R.drawable.right_gray);
            } else {
                CyclicGoodsBidHallPageActivity.this.numOfPageGet = Integer.parseInt(ContractParse.pageNumTal);
                CyclicGoodsBidHallPageActivity.this.numOfPageGet1 = Integer.parseInt(ContractParse.pageNumTal);
                Log.d("总页数contractParse.pageNumTal=：", "(" + CyclicGoodsBidHallPageActivity.this.numOfPageGet + ")");
                CyclicGoodsBidHallPageActivity.this.showPageNum(CyclicGoodsBidHallPageActivity.this.mViewPager.getCurrentItem() + 1, CyclicGoodsBidHallPageActivity.this.numOfPageGet);
                if (CyclicGoodsBidHallPageActivity.this.numOfPageGet > nowPage) {
                    CyclicGoodsBidHallPageActivity.this.rightImbt.setBackgroundResource(R.drawable.right_btn1);
                } else if (CyclicGoodsBidHallPageActivity.this.numOfPageGet == nowPage) {
                    CyclicGoodsBidHallPageActivity.this.rightImbt.setBackgroundResource(R.drawable.right_gray);
                } else if (CyclicGoodsBidHallPageActivity.this.numOfPageGet < nowPage) {
                    CyclicGoodsBidHallPageActivity.this.refresh();
                    return;
                }
            }
            if (ContractParse.systemTime.equals(null) || ContractParse.systemTime.equals(StringEx.Empty)) {
                CyclicGoodsBidHallPageActivity.this.mClock.removeAllViews();
                CyclicGoodsBidHallPageActivity.this.mClockParent.setVisibility(8);
                CyclicGoodsBidHallPageActivity.this.sysTimeTextView.setVisibility(8);
            } else {
                CyclicGoodsBidHallPageActivity.this.mClock.removeAllViews();
                CyclicGoodsBidHallPageActivity.this.systemTime = ContractParse.systemTime;
                Dataofdate.setDate(CyclicGoodsBidHallPageActivity.this.systemTime);
                LinearLayout layout = (LinearLayout) CyclicGoodsBidHallPageActivity.this.inflater.inflate((int) R.layout.timelayout, (ViewGroup) null);
                MyDigitalClock myClock = (MyDigitalClock) layout.findViewById(R.id.timeofmy);
                layout.removeView(myClock);
                CyclicGoodsBidHallPageActivity.this.mMyDigitalClock = myClock;
                CyclicGoodsBidHallPageActivity.this.mClock.addView(myClock);
                CyclicGoodsBidHallPageActivity.this.mClockParent.setVisibility(0);
                CyclicGoodsBidHallPageActivity.this.sysTimeTextView.setVisibility(0);
            }
            CyclicGoodsBidHallPageActivity.this.viewPagerList.clear();
            for (int i = 0; i < CyclicGoodsBidHallPageActivity.this.numOfPageGet; i++) {
                RelativeLayout relativeLayout = (RelativeLayout) CyclicGoodsBidHallPageActivity.this.inflater.inflate((int) R.layout.activity_cyclic_goods_bid_hall_page_listview, (ViewGroup) null);
                ListView mListView = (ListView) relativeLayout.findViewById(R.id.cyclicgoods_bid_hall_page_listview);
                relativeLayout.removeView(mListView);
                mListView.setAdapter((ListAdapter) CyclicGoodsBidHallPageActivity.this.myAdapter);
                mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
                    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    }

                    public void onScrollStateChanged(AbsListView view, int scrollState) {
                        CyclicGoodsBidHallPageActivity.this.showPageNum(CyclicGoodsBidHallPageActivity.this.mViewPager.getCurrentItem() + 1, CyclicGoodsBidHallPageActivity.this.numOfPageGet);
                    }
                });
                mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                        CyclicGoodsBidHallPageActivity.this.bidItem = arg1;
                        CyclicGoodsBidHallPageActivity.this.bidItem.setBackgroundColor(-16720385);
                        String str = (String) ((HashMap) CyclicGoodsBidHallPageActivity.this.mItemArrayList.get(arg2)).get("wtmc");
                        String str2 = (String) ((HashMap) CyclicGoodsBidHallPageActivity.this.mItemArrayList.get(arg2)).get("zys");
                        final String qpj = (String) ((HashMap) CyclicGoodsBidHallPageActivity.this.mItemArrayList.get(arg2)).get("qpj");
                        final String dqj = (String) ((HashMap) CyclicGoodsBidHallPageActivity.this.mItemArrayList.get(arg2)).get("dqj");
                        String jssj = (String) ((HashMap) CyclicGoodsBidHallPageActivity.this.mItemArrayList.get(arg2)).get("jssj");
                        String jjms = (String) ((HashMap) CyclicGoodsBidHallPageActivity.this.mItemArrayList.get(arg2)).get("jjms");
                        String str3 = (String) ((HashMap) CyclicGoodsBidHallPageActivity.this.mItemArrayList.get(arg2)).get("bpfs");
                        String str4 = (String) ((HashMap) CyclicGoodsBidHallPageActivity.this.mItemArrayList.get(arg2)).get("isGz");
                        final String id = (String) ((HashMap) CyclicGoodsBidHallPageActivity.this.mItemArrayList.get(arg2)).get("id");
                        final String bjtd = (String) ((HashMap) CyclicGoodsBidHallPageActivity.this.mItemArrayList.get(arg2)).get("bjtd");
                        String ykj = (String) ((HashMap) CyclicGoodsBidHallPageActivity.this.mItemArrayList.get(arg2)).get("ykj");
                        String wtprice = (String) ((HashMap) CyclicGoodsBidHallPageActivity.this.mItemArrayList.get(arg2)).get("wtprice");
                        String kssj = (String) ((HashMap) CyclicGoodsBidHallPageActivity.this.mItemArrayList.get(arg2)).get("kssj");
                        final String jjzt = (String) ((HashMap) CyclicGoodsBidHallPageActivity.this.mItemArrayList.get(arg2)).get("jjzt");
                        String myprice = (String) ((HashMap) CyclicGoodsBidHallPageActivity.this.mItemArrayList.get(arg2)).get("myprice");
                        final String pm = (String) ((HashMap) CyclicGoodsBidHallPageActivity.this.mItemArrayList.get(arg2)).get("pm");
                        final int qpjInt = Integer.parseInt(qpj);
                        final int dqjInt = Integer.parseInt(dqj);
                        int parseInt = Integer.parseInt(ykj);
                        if (ykj.equals("0")) {
                            CyclicGoodsBidHallPageActivity.this.thePriceImbt.setBackgroundResource(R.drawable.price_total2_yikoujia2);
                            CyclicGoodsBidHallPageActivity.this.ykjAction = false;
                        } else {
                            CyclicGoodsBidHallPageActivity.this.ykjAction = true;
                            CyclicGoodsBidHallPageActivity.this.thePriceImbt.setBackgroundResource(R.drawable.cyclicgoods_yikoujia_selector);
                        }
                        new Date();
                        new Date();
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        try {
                            Date dat = simpleDateFormat.parse(kssj);
                            Date dat2 = simpleDateFormat.parse(jssj);
                            CyclicGoodsBidHallPageActivity.this.kssjLong = dat.getTime();
                            CyclicGoodsBidHallPageActivity.this.jssjLong = dat2.getTime();
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        CyclicGoodsBidHallPageActivity.this.sysTimeLong = CyclicGoodsBidHallPageActivity.this.mMyDigitalClock.getDateTime();
                        Log.d("时间比较", "开始时间毫秒：" + CyclicGoodsBidHallPageActivity.this.kssjLong + "系统时间毫秒：" + CyclicGoodsBidHallPageActivity.this.sysTimeLong);
                        if (CyclicGoodsBidHallPageActivity.this.kssjLong > CyclicGoodsBidHallPageActivity.this.sysTimeLong) {
                            CyclicGoodsBidHallPageActivity.this.kssjOverSysTime = true;
                        } else {
                            CyclicGoodsBidHallPageActivity.this.kssjOverSysTime = false;
                        }
                        CyclicGoodsBidHallPageActivity.this.manGrads.setText("梯度，每个梯度" + bjtd + "元");
                        CyclicGoodsBidHallPageActivity.this.sysGrads.setText("系统自动帮我加上一个梯度（" + bjtd + "）出价");
                        if (wtprice.equals(null) || wtprice.equals(StringEx.Empty)) {
                            CyclicGoodsBidHallPageActivity.this.sysNowPrice.setText("0");
                        } else {
                            CyclicGoodsBidHallPageActivity.this.sysNowPrice.setText(wtprice);
                        }
                        if (ykj.equals(null) || ykj.equals(StringEx.Empty)) {
                            CyclicGoodsBidHallPageActivity.this.thePrice.setText("0");
                        } else {
                            CyclicGoodsBidHallPageActivity.this.thePrice.setText(ykj);
                        }
                        if (myprice.equals(null) || myprice.equals(StringEx.Empty) || myprice.equals("0")) {
                            CyclicGoodsBidHallPageActivity.this.manAddPriceEditText.setText("0");
                            CyclicGoodsBidHallPageActivity.this.manAmount.setText(new StringBuilder(String.valueOf(Integer.parseInt(qpj))).toString());
                        } else {
                            CyclicGoodsBidHallPageActivity.this.manAddPriceEditText.setText("1");
                            CyclicGoodsBidHallPageActivity.this.manAmount.setText(new StringBuilder(String.valueOf(Integer.parseInt(bjtd) + Integer.parseInt(dqj))).toString());
                        }
                        if (dqj.equals(null) || dqj.equals(StringEx.Empty) || dqj.equals("0")) {
                            CyclicGoodsBidHallPageActivity.this.sysPriceEditText.setText("0");
                            CyclicGoodsBidHallPageActivity.this.sysnotOver.setText(new StringBuilder(String.valueOf(Integer.parseInt(qpj))).toString());
                        } else {
                            CyclicGoodsBidHallPageActivity.this.sysPriceEditText.setText("1");
                            CyclicGoodsBidHallPageActivity.this.sysnotOver.setText(new StringBuilder(String.valueOf(Integer.parseInt(bjtd) + Integer.parseInt(dqj))).toString());
                        }
                        CyclicGoodsBidHallPageActivity.this.manAddPriceEditText.addTextChangedListener(new TextWatcher() {
                            public void afterTextChanged(Editable s) {
                            }

                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                            }

                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                String inputString = CyclicGoodsBidHallPageActivity.this.manAddPriceEditText.getText().toString();
                                long manAmountText = 0;
                                if (!inputString.equals(null) && !inputString.equals(StringEx.Empty) && !bjtd.equals(null) && !bjtd.equals(StringEx.Empty) && !dqj.equals(null) && !dqj.equals(StringEx.Empty) && !dqj.equals("0")) {
                                    manAmountText = (Long.parseLong(inputString) * Long.parseLong(bjtd)) + Long.parseLong(dqj);
                                } else if (!inputString.equals(null) && !inputString.equals(StringEx.Empty) && !bjtd.equals(null) && !bjtd.equals(StringEx.Empty) && dqj.equals("0")) {
                                    manAmountText = (Long.parseLong(inputString) * Long.parseLong(bjtd)) + ((long) qpjInt);
                                } else if (!inputString.equals(null) && !inputString.equals(StringEx.Empty) && ((bjtd.equals(StringEx.Empty) || bjtd.equals("0")) && !dqj.equals(null) && !dqj.equals(StringEx.Empty))) {
                                    manAmountText = dqj.equals("0") ? Long.parseLong(inputString) + Long.parseLong(qpj) : Long.parseLong(inputString) + Long.parseLong(dqj);
                                }
                                if (manAmountText != 0) {
                                    CyclicGoodsBidHallPageActivity.this.manAmount.setText(new StringBuilder(String.valueOf(manAmountText)).toString());
                                } else {
                                    CyclicGoodsBidHallPageActivity.this.manAmount.setText(StringEx.Empty);
                                }
                            }
                        });
                        CyclicGoodsBidHallPageActivity.this.sysPriceEditText.addTextChangedListener(new TextWatcher() {
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                String inputString = CyclicGoodsBidHallPageActivity.this.sysPriceEditText.getText().toString();
                                long manAmountText = 0;
                                if (!inputString.equals(null) && !inputString.equals(StringEx.Empty) && !bjtd.equals(null) && !bjtd.equals(StringEx.Empty) && !dqj.equals(null) && !dqj.equals(StringEx.Empty) && !dqj.equals("0")) {
                                    manAmountText = (Long.parseLong(inputString) * Long.parseLong(bjtd)) + Long.parseLong(dqj);
                                } else if (!inputString.equals(null) && !inputString.equals(StringEx.Empty) && !bjtd.equals(null) && !bjtd.equals(StringEx.Empty) && dqj.equals("0")) {
                                    manAmountText = (Long.parseLong(inputString) * Long.parseLong(bjtd)) + ((long) qpjInt);
                                } else if (!inputString.equals(null) && !inputString.equals(StringEx.Empty) && ((bjtd.equals(StringEx.Empty) || bjtd.equals("0")) && !dqj.equals(null) && !dqj.equals(StringEx.Empty))) {
                                    manAmountText = dqj.equals("0") ? Long.parseLong(inputString) + Long.parseLong(qpj) : Long.parseLong(inputString) + Long.parseLong(dqj);
                                }
                                if (manAmountText != 0) {
                                    CyclicGoodsBidHallPageActivity.this.sysnotOver.setText(new StringBuilder(String.valueOf(manAmountText)).toString());
                                } else {
                                    CyclicGoodsBidHallPageActivity.this.sysnotOver.setText(StringEx.Empty);
                                }
                            }

                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                            }

                            public void afterTextChanged(Editable s) {
                            }
                        });
                        final int i = qpjInt;
                        final String str5 = dqj;
                        CyclicGoodsBidHallPageActivity.this.manPriceImbt.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                if (!jjzt.equals("3")) {
                                    AlertDialog.Builder dialog = new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this);
                                    dialog.setCancelable(false);
                                    dialog.setMessage("该拼盘还未开始竞价!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (CyclicGoodsBidHallPageActivity.this.bidItem != null) {
                                                CyclicGoodsBidHallPageActivity.this.bidItem.setBackgroundColor(0);
                                            }
                                            dialog.cancel();
                                        }
                                    }).show();
                                } else if (CyclicGoodsBidHallPageActivity.this.manAmount.getText().toString().equals(null) || CyclicGoodsBidHallPageActivity.this.manAmount.getText().toString().equals(StringEx.Empty)) {
                                    new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this).setMessage("价格不能为空!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    }).show();
                                } else {
                                    long lastprice = Long.parseLong(CyclicGoodsBidHallPageActivity.this.manAmount.getText().toString());
                                    if (lastprice < ((long) i)) {
                                        new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this).setMessage("出价金额不得低于起拍价!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        }).show();
                                    } else if (!str5.equals("0") && lastprice < ((long) dqjInt)) {
                                        new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this).setMessage("出价不能小于当前价!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        }).show();
                                    } else if (!str5.equals("0") || lastprice < ((long) i)) {
                                        if (!str5.equals("0") && lastprice >= ((long) i)) {
                                            if (((float) lastprice) / ((float) i) >= 2.0f) {
                                                if (((double) (((float) lastprice) / ((float) dqjInt))) >= 1.5d) {
                                                    new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this).setMessage("您的出价为" + lastprice + "，已经高出当前价的50%，不允许出价！").setNegativeButton("确定", new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            dialog.cancel();
                                                        }
                                                    }).show();
                                                    return;
                                                }
                                                AlertDialog.Builder negativeButton = new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this).setMessage("您的出价为" + lastprice + "，已经高出起拍价的100%").setNegativeButton("取消", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.cancel();
                                                    }
                                                });
                                                final String str = id;
                                                negativeButton.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        if (CyclicGoodsBidHallPageActivity.this.jssjLong > CyclicGoodsBidHallPageActivity.this.mMyDigitalClock.getDateTime()) {
                                                            CyclicGoodsBidHallPageActivity.this.getManData("3", CyclicGoodsBidHallPageActivity.this.manAmount.getText().toString(), str);
                                                            dialog.cancel();
                                                            return;
                                                        }
                                                        AlertDialog.Builder dialog1 = new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this);
                                                        dialog1.setCancelable(false);
                                                        dialog1.setMessage("该拼盘已经结束!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int which) {
                                                                if (CyclicGoodsBidHallPageActivity.this.bidItem != null) {
                                                                    CyclicGoodsBidHallPageActivity.this.bidItem.setBackgroundColor(0);
                                                                }
                                                                dialog.cancel();
                                                                CyclicGoodsBidHallPageActivity.this.refresh();
                                                            }
                                                        }).show();
                                                    }
                                                }).show();
                                            } else if (((double) (((float) lastprice) / ((float) i))) >= 1.5d) {
                                                AlertDialog.Builder negativeButton2 = new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this).setMessage("您的出价为" + lastprice + "，已经高出起拍价的50%").setNegativeButton("取消", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.cancel();
                                                    }
                                                });
                                                final String str2 = id;
                                                negativeButton2.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        if (CyclicGoodsBidHallPageActivity.this.jssjLong > CyclicGoodsBidHallPageActivity.this.mMyDigitalClock.getDateTime()) {
                                                            CyclicGoodsBidHallPageActivity.this.getManData("3", CyclicGoodsBidHallPageActivity.this.manAmount.getText().toString(), str2);
                                                            dialog.cancel();
                                                            return;
                                                        }
                                                        AlertDialog.Builder dialog1 = new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this);
                                                        dialog1.setCancelable(false);
                                                        dialog1.setMessage("该拼盘已经结束!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int which) {
                                                                if (CyclicGoodsBidHallPageActivity.this.bidItem != null) {
                                                                    CyclicGoodsBidHallPageActivity.this.bidItem.setBackgroundColor(0);
                                                                }
                                                                dialog.cancel();
                                                                CyclicGoodsBidHallPageActivity.this.refresh();
                                                            }
                                                        }).show();
                                                    }
                                                }).show();
                                            } else {
                                                AlertDialog.Builder message = new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this).setTitle("品名" + pm).setMessage("您确认要出价" + CyclicGoodsBidHallPageActivity.this.manAmount.getText().toString() + "元吗？");
                                                final String str3 = id;
                                                message.setNegativeButton("确定", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        if (CyclicGoodsBidHallPageActivity.this.jssjLong > CyclicGoodsBidHallPageActivity.this.mMyDigitalClock.getDateTime()) {
                                                            CyclicGoodsBidHallPageActivity.this.getManData("3", CyclicGoodsBidHallPageActivity.this.manAmount.getText().toString(), str3);
                                                            dialog.cancel();
                                                            return;
                                                        }
                                                        AlertDialog.Builder dialog1 = new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this);
                                                        dialog1.setCancelable(false);
                                                        dialog1.setMessage("该拼盘已经结束!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int which) {
                                                                if (CyclicGoodsBidHallPageActivity.this.bidItem != null) {
                                                                    CyclicGoodsBidHallPageActivity.this.bidItem.setBackgroundColor(0);
                                                                }
                                                                dialog.cancel();
                                                                CyclicGoodsBidHallPageActivity.this.refresh();
                                                            }
                                                        }).show();
                                                    }
                                                }).setPositiveButton("取消", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.cancel();
                                                    }
                                                }).show();
                                            }
                                        }
                                    } else if (((float) lastprice) / ((float) i) >= 2.0f) {
                                        AlertDialog.Builder negativeButton3 = new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this).setMessage("您的出价为" + lastprice + "，已经高出起拍价的100%").setNegativeButton("取消", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        });
                                        final String str4 = id;
                                        negativeButton3.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (CyclicGoodsBidHallPageActivity.this.jssjLong > CyclicGoodsBidHallPageActivity.this.mMyDigitalClock.getDateTime()) {
                                                    CyclicGoodsBidHallPageActivity.this.getManData("3", CyclicGoodsBidHallPageActivity.this.manAmount.getText().toString(), str4);
                                                    dialog.cancel();
                                                    return;
                                                }
                                                AlertDialog.Builder dialog1 = new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this);
                                                dialog1.setCancelable(false);
                                                dialog1.setMessage("该拼盘已经结束!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        if (CyclicGoodsBidHallPageActivity.this.bidItem != null) {
                                                            CyclicGoodsBidHallPageActivity.this.bidItem.setBackgroundColor(0);
                                                        }
                                                        dialog.cancel();
                                                        CyclicGoodsBidHallPageActivity.this.refresh();
                                                    }
                                                }).show();
                                            }
                                        }).show();
                                    } else if (((double) (((float) lastprice) / ((float) i))) >= 1.5d) {
                                        AlertDialog.Builder negativeButton4 = new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this).setMessage("您的出价为" + lastprice + "，已经高出起拍价的50%").setNegativeButton("取消", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        });
                                        final String str5 = id;
                                        negativeButton4.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (CyclicGoodsBidHallPageActivity.this.jssjLong > CyclicGoodsBidHallPageActivity.this.mMyDigitalClock.getDateTime()) {
                                                    CyclicGoodsBidHallPageActivity.this.getManData("3", CyclicGoodsBidHallPageActivity.this.manAmount.getText().toString(), str5);
                                                    dialog.cancel();
                                                    return;
                                                }
                                                AlertDialog.Builder dialog1 = new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this);
                                                dialog1.setCancelable(false);
                                                dialog1.setMessage("该拼盘已经结束!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        if (CyclicGoodsBidHallPageActivity.this.bidItem != null) {
                                                            CyclicGoodsBidHallPageActivity.this.bidItem.setBackgroundColor(0);
                                                        }
                                                        dialog.cancel();
                                                        CyclicGoodsBidHallPageActivity.this.refresh();
                                                    }
                                                }).show();
                                            }
                                        }).show();
                                    } else {
                                        AlertDialog.Builder message2 = new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this).setTitle("品名" + pm).setMessage("您确认要出价" + CyclicGoodsBidHallPageActivity.this.manAmount.getText().toString() + "元吗？");
                                        final String str6 = id;
                                        message2.setNegativeButton("确定", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (CyclicGoodsBidHallPageActivity.this.jssjLong > CyclicGoodsBidHallPageActivity.this.mMyDigitalClock.getDateTime()) {
                                                    CyclicGoodsBidHallPageActivity.this.getManData("3", CyclicGoodsBidHallPageActivity.this.manAmount.getText().toString(), str6);
                                                    dialog.cancel();
                                                    return;
                                                }
                                                AlertDialog.Builder dialog1 = new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this);
                                                dialog1.setCancelable(false);
                                                dialog1.setMessage("该拼盘已经结束!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        if (CyclicGoodsBidHallPageActivity.this.bidItem != null) {
                                                            CyclicGoodsBidHallPageActivity.this.bidItem.setBackgroundColor(0);
                                                        }
                                                        dialog.cancel();
                                                        CyclicGoodsBidHallPageActivity.this.refresh();
                                                    }
                                                }).show();
                                            }
                                        }).setPositiveButton("取消", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        }).show();
                                    }
                                }
                            }
                        });
                        final String str6 = dqj;
                        final int i2 = qpjInt;
                        final int i3 = dqjInt;
                        final String str7 = pm;
                        final String str8 = id;
                        CyclicGoodsBidHallPageActivity.this.sysPriceImbt.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                if (CyclicGoodsBidHallPageActivity.this.sysnotOver.getText().toString().equals(null) || CyclicGoodsBidHallPageActivity.this.sysnotOver.getText().toString().equals(StringEx.Empty)) {
                                    new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this).setMessage("价格不能为空!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    }).show();
                                    return;
                                }
                                Long lastprice = Long.valueOf(Long.parseLong(CyclicGoodsBidHallPageActivity.this.sysnotOver.getText().toString()));
                                if (str6.equals("0") && lastprice.longValue() < ((long) i2)) {
                                    new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this).setMessage("出价金额不得低于起拍价!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    }).show();
                                } else if (str6.equals("0") || lastprice.longValue() >= ((long) i3)) {
                                    AlertDialog.Builder message = new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this).setTitle("品名" + str7).setMessage("您确认要出价" + CyclicGoodsBidHallPageActivity.this.sysnotOver.getText().toString() + "元吗？");
                                    final String str = str8;
                                    message.setNegativeButton("确定", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (CyclicGoodsBidHallPageActivity.this.jssjLong > CyclicGoodsBidHallPageActivity.this.mMyDigitalClock.getDateTime()) {
                                                CyclicGoodsBidHallPageActivity.this.getManData("0", CyclicGoodsBidHallPageActivity.this.sysnotOver.getText().toString(), str);
                                                dialog.cancel();
                                                return;
                                            }
                                            AlertDialog.Builder dialog1 = new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this);
                                            dialog1.setCancelable(false);
                                            dialog1.setMessage("该拼盘已经结束!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    if (CyclicGoodsBidHallPageActivity.this.bidItem != null) {
                                                        CyclicGoodsBidHallPageActivity.this.bidItem.setBackgroundColor(0);
                                                    }
                                                    CyclicGoodsBidHallPageActivity.this.refresh();
                                                }
                                            }).show();
                                        }
                                    }).setPositiveButton("取消", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    }).show();
                                } else {
                                    new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this).setMessage("出价不能小于当前价!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    }).show();
                                }
                            }
                        });
                        if (CyclicGoodsBidHallPageActivity.this.ykjAction) {
                            final String str9 = ykj;
                            CyclicGoodsBidHallPageActivity.this.thePriceImbt.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View v) {
                                    if (str9.equals(null) || str9.equals(StringEx.Empty)) {
                                        new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this).setMessage("价格不能为空!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        }).show();
                                        return;
                                    }
                                    AlertDialog.Builder message = new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this).setTitle("品名" + pm).setMessage("您确认要出价" + str9 + "元吗？");
                                    final String str = str9;
                                    final String str2 = id;
                                    message.setNegativeButton("确定", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (CyclicGoodsBidHallPageActivity.this.jssjLong > CyclicGoodsBidHallPageActivity.this.mMyDigitalClock.getDateTime()) {
                                                CyclicGoodsBidHallPageActivity.this.getManData("1", str, str2);
                                                return;
                                            }
                                            AlertDialog.Builder dialog1 = new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this);
                                            dialog1.setCancelable(false);
                                            dialog1.setMessage("该拼盘已经结束!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    if (CyclicGoodsBidHallPageActivity.this.bidItem != null) {
                                                        CyclicGoodsBidHallPageActivity.this.bidItem.setBackgroundColor(0);
                                                    }
                                                    CyclicGoodsBidHallPageActivity.this.refresh();
                                                }
                                            }).show();
                                        }
                                    }).setPositiveButton("取消", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    }).show();
                                }
                            });
                        }
                        final String str10 = dqj;
                        final int i4 = dqjInt;
                        final int i5 = qpjInt;
                        final String str11 = pm;
                        final String str12 = id;
                        CyclicGoodsBidHallPageActivity.this.freePriceImbt.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                Log.d("自由竞价价格：", "自由竞价价格：" + CyclicGoodsBidHallPageActivity.this.freePriceEditText.getText().toString());
                                if (CyclicGoodsBidHallPageActivity.this.freePriceEditText.getText().toString().equals(null) || CyclicGoodsBidHallPageActivity.this.freePriceEditText.getText().toString().equals(StringEx.Empty)) {
                                    new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this).setMessage("价格不能为空!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    }).show();
                                } else if (!str10.equals("0") && Long.parseLong(CyclicGoodsBidHallPageActivity.this.freePriceEditText.getText().toString()) < ((long) i4)) {
                                    new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this).setMessage("出价不能小于当前价!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    }).show();
                                } else if (!str10.equals("0") || Long.parseLong(CyclicGoodsBidHallPageActivity.this.freePriceEditText.getText().toString()) >= ((long) i5)) {
                                    AlertDialog.Builder message = new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this).setTitle("品名" + str11).setMessage("您确认要出价" + CyclicGoodsBidHallPageActivity.this.freePriceEditText.getText().toString() + "元吗？");
                                    final String str = str12;
                                    message.setNegativeButton("确定", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (CyclicGoodsBidHallPageActivity.this.jssjLong > CyclicGoodsBidHallPageActivity.this.mMyDigitalClock.getDateTime()) {
                                                CyclicGoodsBidHallPageActivity.this.getManData("2", CyclicGoodsBidHallPageActivity.this.freePriceEditText.getText().toString(), str);
                                                return;
                                            }
                                            AlertDialog.Builder dialog1 = new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this);
                                            dialog1.setCancelable(false);
                                            dialog1.setMessage("该拼盘已经结束!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    if (CyclicGoodsBidHallPageActivity.this.bidItem != null) {
                                                        CyclicGoodsBidHallPageActivity.this.bidItem.setBackgroundColor(0);
                                                    }
                                                    CyclicGoodsBidHallPageActivity.this.refresh();
                                                }
                                            }).show();
                                        }
                                    }).setPositiveButton("取消", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    }).show();
                                } else {
                                    new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this).setMessage("出价金额不得低于起拍价!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    }).show();
                                }
                            }
                        });
                        if (jjms.equals("1")) {
                            if (CyclicGoodsBidHallPageActivity.this.mPopupWindowPriceFree != null && CyclicGoodsBidHallPageActivity.this.mPopupWindowPriceFree.isShowing()) {
                                CyclicGoodsBidHallPageActivity.this.mPopupWindowPriceFree.dismiss();
                            }
                            if (CyclicGoodsBidHallPageActivity.this.mPopupWindowPrice != null && CyclicGoodsBidHallPageActivity.this.mPopupWindowPrice.isShowing()) {
                                CyclicGoodsBidHallPageActivity.this.mPopupWindowPrice.dismiss();
                                CyclicGoodsBidHallPageActivity.this.manAddPriceEditText.setText(StringEx.Empty);
                                CyclicGoodsBidHallPageActivity.this.sysPriceEditText.setText(StringEx.Empty);
                            } else if (CyclicGoodsBidHallPageActivity.this.kssjOverSysTime) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this);
                                builder.setCancelable(false);
                                builder.setMessage("该拼盘还未开始竞价!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (CyclicGoodsBidHallPageActivity.this.bidItem != null) {
                                            CyclicGoodsBidHallPageActivity.this.bidItem.setBackgroundColor(0);
                                        }
                                        dialog.dismiss();
                                    }
                                }).show();
                            } else {
                                CyclicGoodsBidHallPageActivity.this.mPopupWindowPrice.showAtLocation(arg1, 17, 0, 30);
                            }
                        } else if (jjms.equals("2")) {
                            if (CyclicGoodsBidHallPageActivity.this.mPopupWindowPrice != null && CyclicGoodsBidHallPageActivity.this.mPopupWindowPrice.isShowing()) {
                                CyclicGoodsBidHallPageActivity.this.mPopupWindowPrice.dismiss();
                            }
                            if (CyclicGoodsBidHallPageActivity.this.mPopupWindowPriceFree != null && CyclicGoodsBidHallPageActivity.this.mPopupWindowPriceFree.isShowing()) {
                                CyclicGoodsBidHallPageActivity.this.mPopupWindowPriceFree.dismiss();
                                CyclicGoodsBidHallPageActivity.this.freePriceEditText.setText(StringEx.Empty);
                            } else if (CyclicGoodsBidHallPageActivity.this.kssjOverSysTime) {
                                AlertDialog.Builder builder2 = new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this);
                                builder2.setCancelable(false);
                                builder2.setMessage("该拼盘还未开始竞价!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (CyclicGoodsBidHallPageActivity.this.bidItem != null) {
                                            CyclicGoodsBidHallPageActivity.this.bidItem.setBackgroundColor(0);
                                        }
                                        dialog.dismiss();
                                    }
                                }).show();
                            } else {
                                CyclicGoodsBidHallPageActivity.this.mPopupWindowPriceFree.showAtLocation(arg1, 17, 0, 30);
                            }
                        }
                        if (!(CyclicGoodsBidHallPageActivity.this.mPopupWindowPrice == null || !CyclicGoodsBidHallPageActivity.this.mPopupWindowPrice.isShowing() || CyclicGoodsBidHallPageActivity.this.mManPrice.getVisibility() == 0)) {
                            CyclicGoodsBidHallPageActivity.this.mSysPrice.getVisibility();
                        }
                        if (CyclicGoodsBidHallPageActivity.this.mPopupWindowPriceFree != null) {
                            CyclicGoodsBidHallPageActivity.this.mPopupWindowPriceFree.isShowing();
                        }
                    }
                });
                CyclicGoodsBidHallPageActivity.this.viewPagerList.add(mListView);
            }
            Log.d("获取的ROWs数据：", ContractParse.jjo.toString());
            if (ContractParse.jjo.toString().equals(null) || ContractParse.jjo.toString().equals("[[]]") || ContractParse.jjo.toString().equals(StringEx.Empty) || ContractParse.jjo.toString().equals("[]")) {
                CyclicGoodsBidHallPageActivity.this.leftImbt.setVisibility(8);
                CyclicGoodsBidHallPageActivity.this.rightImbt.setVisibility(8);
                CyclicGoodsBidHallPageActivity.this.cyclicgoods_page_num.setVisibility(8);
                new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this).setMessage("数据为空！").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
                return;
            }
            for (int i2 = 0; i2 < ContractParse.jjo.length(); i2++) {
                try {
                    JSONArray row = new JSONArray(ContractParse.jjo.getString(i2));
                    HashMap<String, Object> item = new HashMap<>();
                    CyclicGoodsBidHallPageActivity.this.wtmc = row.getString(0);
                    CyclicGoodsBidHallPageActivity.this.zys = row.getString(1);
                    CyclicGoodsBidHallPageActivity.this.qpj = row.getString(2);
                    CyclicGoodsBidHallPageActivity.this.dqj = row.getString(3);
                    CyclicGoodsBidHallPageActivity.this.jssj = row.getString(4);
                    CyclicGoodsBidHallPageActivity.this.jjms = row.getString(5);
                    CyclicGoodsBidHallPageActivity.this.bpfs = row.getString(6);
                    CyclicGoodsBidHallPageActivity.this.isGz = row.getString(7);
                    CyclicGoodsBidHallPageActivity.this.id = row.getString(8);
                    CyclicGoodsBidHallPageActivity.this.bjtd = row.getString(9);
                    CyclicGoodsBidHallPageActivity.this.ykj = row.getString(11);
                    CyclicGoodsBidHallPageActivity.this.wtprice = row.getString(10);
                    CyclicGoodsBidHallPageActivity.this.kssj = row.getString(12);
                    CyclicGoodsBidHallPageActivity.this.jjzt = row.getString(13);
                    CyclicGoodsBidHallPageActivity.this.myprice = row.getString(14);
                    CyclicGoodsBidHallPageActivity.this.pm = row.getString(15);
                    item.put("wtmc", CyclicGoodsBidHallPageActivity.this.wtmc);
                    item.put("zys", CyclicGoodsBidHallPageActivity.this.zys);
                    item.put("qpj", CyclicGoodsBidHallPageActivity.this.qpj);
                    item.put("dqj", CyclicGoodsBidHallPageActivity.this.dqj);
                    item.put("jssj", CyclicGoodsBidHallPageActivity.this.jssj);
                    item.put("jjms", CyclicGoodsBidHallPageActivity.this.jjms);
                    item.put("bpfs", CyclicGoodsBidHallPageActivity.this.bpfs);
                    item.put("isGz", CyclicGoodsBidHallPageActivity.this.isGz);
                    item.put("id", CyclicGoodsBidHallPageActivity.this.id);
                    item.put("bjtd", CyclicGoodsBidHallPageActivity.this.bjtd);
                    item.put("ykj", CyclicGoodsBidHallPageActivity.this.ykj);
                    item.put("wtprice", CyclicGoodsBidHallPageActivity.this.wtprice);
                    item.put("kssj", CyclicGoodsBidHallPageActivity.this.kssj);
                    item.put("jjzt", CyclicGoodsBidHallPageActivity.this.jjzt);
                    item.put("myprice", CyclicGoodsBidHallPageActivity.this.myprice);
                    item.put("pm", CyclicGoodsBidHallPageActivity.this.pm);
                    CyclicGoodsBidHallPageActivity.this.mItemArrayList.add(item);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            CyclicGoodsBidHallPageActivity.this.leftImbt.setVisibility(0);
            CyclicGoodsBidHallPageActivity.this.rightImbt.setVisibility(0);
            CyclicGoodsBidHallPageActivity.this.myAdapter.notifyDataSetChanged();
            if (CyclicGoodsBidHallPageActivity.this.firstTimeGetData) {
                CyclicGoodsBidHallPageActivity.this.getGuangzhu();
                CyclicGoodsBidHallPageActivity.this.firstTimeGetData = false;
            }
        }
    };
    private JQUICallBack httpCallBackGuanzhu = new JQUICallBack() {
        public void callBack(ResultData result1) {
            CyclicGoodsBidHallPageActivity.this.progressDialog.dismiss();
            Log.d("获取的关注数据，未解密，未解析：", result1.getStringData());
            String dataString = result1.getStringData();
            if (dataString.equals(null) || dataString.equals(StringEx.Empty)) {
                new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this).setMessage("获取数据失败").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        CyclicGoodsBidHallPageActivity.this.startActivity(new Intent(CyclicGoodsBidHallPageActivity.this, CyclicGoodsIndexActivity.class));
                        CyclicGoodsBidHallPageActivity.this.finish();
                    }
                }).show();
            } else {
                ContractParse parse = ContractParse.parse(result1.getStringData());
                Log.d("获取的关注数据，已解密，未解析：", ContractParse.getDecryptData());
                if (ContractParse.getDataString().equals(null) || ContractParse.getDataString().equals(StringEx.Empty)) {
                    CyclicGoodsBidHallPageActivity.this.mFlipper.setVisibility(8);
                    new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this).setMessage("获取数据失败").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            CyclicGoodsBidHallPageActivity.this.startActivity(new Intent(CyclicGoodsBidHallPageActivity.this, CyclicGoodsIndexActivity.class));
                            CyclicGoodsBidHallPageActivity.this.finish();
                        }
                    }).show();
                } else {
                    Log.d("关注获取的ROWs数据：", ContractParse.jjo.toString());
                    CyclicGoodsBidHallPageActivity.this.mGuznZhuArrayList.clear();
                    if (ContractParse.jjo.length() < 6) {
                        CyclicGoodsBidHallPageActivity.this.guanZhuPop = new PopupWindow(CyclicGoodsBidHallPageActivity.this.guanZhuPopView, -1, (CyclicGoodsBidHallPageActivity.this.itemHeight * ContractParse.jjo.length()) + (ContractParse.jjo.length() - 1));
                    } else {
                        CyclicGoodsBidHallPageActivity.this.guanZhuPop = new PopupWindow(CyclicGoodsBidHallPageActivity.this.guanZhuPopView, -1, (CyclicGoodsBidHallPageActivity.this.itemHeight * 5) + (ContractParse.jjo.length() - 1));
                    }
                    for (int i = 0; i < ContractParse.jjo.length(); i++) {
                        try {
                            JSONArray row = new JSONArray(ContractParse.jjo.getString(i));
                            HashMap<String, Object> guanZhuItem = new HashMap<>();
                            CyclicGoodsBidHallPageActivity.this.GZ_num = i + 1;
                            CyclicGoodsBidHallPageActivity.this.gz_wtmc = row.getString(0);
                            CyclicGoodsBidHallPageActivity.this.gz_dqj = row.getString(2);
                            CyclicGoodsBidHallPageActivity.this.gz_jjms = row.getString(3);
                            CyclicGoodsBidHallPageActivity.this.gz_id = row.getString(4);
                            CyclicGoodsBidHallPageActivity.this.gz_bjtd = row.getString(5);
                            CyclicGoodsBidHallPageActivity.this.gz_wtprice = row.getString(6);
                            CyclicGoodsBidHallPageActivity.this.gz_myPrice = row.getString(7);
                            View item = CyclicGoodsBidHallPageActivity.this.inflater.inflate((int) R.layout.activity_cyclic_goods_bid_hall_guanzhu, (ViewGroup) null);
                            TextView price = (TextView) item.findViewById(R.id.cyclic_goods_guangzhu_price);
                            TextView myPrice = (TextView) item.findViewById(R.id.cyclic_goods_guangzhu_my_price2);
                            TextView myPriceGreen = (TextView) item.findViewById(R.id.cyclic_goods_guangzhu_my_price2_green);
                            ((TextView) item.findViewById(R.id.cyclic_goods_guangzhu_title)).setText(CyclicGoodsBidHallPageActivity.this.gz_wtmc);
                            ((TextView) item.findViewById(R.id.cyclic_goods_guangzhu_title_num)).setText("拼盘号：" + CyclicGoodsBidHallPageActivity.this.gz_id);
                            guanZhuItem.put("title", CyclicGoodsBidHallPageActivity.this.gz_wtmc);
                            guanZhuItem.put("title_num", "拼盘号：" + CyclicGoodsBidHallPageActivity.this.gz_id);
                            if (CyclicGoodsBidHallPageActivity.this.gz_jjms.equals("2")) {
                                guanZhuItem.put("price", "自由报价");
                                guanZhuItem.put("myprice", CyclicGoodsBidHallPageActivity.this.gz_myPrice);
                                guanZhuItem.put("green", StringEx.Empty);
                                price.setText("自由报价");
                                myPriceGreen.setText(StringEx.Empty);
                                myPrice.setText(new StringBuilder(String.valueOf(CyclicGoodsBidHallPageActivity.this.gz_myPrice)).toString());
                            } else {
                                guanZhuItem.put("price", "当前价：" + CyclicGoodsBidHallPageActivity.this.gz_dqj);
                                price.setText("当前价：" + CyclicGoodsBidHallPageActivity.this.gz_dqj);
                                if (Integer.parseInt(CyclicGoodsBidHallPageActivity.this.gz_myPrice) >= Integer.parseInt(CyclicGoodsBidHallPageActivity.this.gz_dqj)) {
                                    myPrice.setText(String.valueOf(CyclicGoodsBidHallPageActivity.this.gz_myPrice) + "（领先）");
                                    myPriceGreen.setText(StringEx.Empty);
                                    guanZhuItem.put("myprice", String.valueOf(CyclicGoodsBidHallPageActivity.this.gz_myPrice) + "（领先）");
                                    guanZhuItem.put("green", StringEx.Empty);
                                } else {
                                    myPrice.setText(StringEx.Empty);
                                    myPriceGreen.setText(String.valueOf(CyclicGoodsBidHallPageActivity.this.gz_myPrice) + "（落后）");
                                    guanZhuItem.put("myprice", StringEx.Empty);
                                    guanZhuItem.put("green", String.valueOf(CyclicGoodsBidHallPageActivity.this.gz_myPrice) + "（落后）");
                                }
                            }
                            Log.d("关注解析的数据", "gz_wtmc=" + CyclicGoodsBidHallPageActivity.this.gz_wtmc + "gz_dqj=" + CyclicGoodsBidHallPageActivity.this.gz_dqj + "gz_jjms=" + CyclicGoodsBidHallPageActivity.this.gz_jjms + "gz_id=" + CyclicGoodsBidHallPageActivity.this.gz_id + "gz_bjtd=" + CyclicGoodsBidHallPageActivity.this.gz_bjtd + "gz_wtprice=" + CyclicGoodsBidHallPageActivity.this.gz_wtprice + "gz_myPrice=" + CyclicGoodsBidHallPageActivity.this.gz_myPrice);
                            CyclicGoodsBidHallPageActivity.this.mGuznZhuArrayList.add(guanZhuItem);
                            if (ContractParse.jjo.length() > 1) {
                                item.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View v) {
                                        CyclicGoodsBidHallPageActivity.this.guanZhuPop.showAtLocation(CyclicGoodsBidHallPageActivity.this.mRadioderGroup, 81, 0, CyclicGoodsBidHallPageActivity.this.mRadioderGroup.getHeight() - 20);
                                        CyclicGoodsBidHallPageActivity.this.mFlipper.setVisibility(8);
                                        CyclicGoodsBidHallPageActivity.this.popupBackground.setVisibility(0);
                                    }
                                });
                            }
                            CyclicGoodsBidHallPageActivity.this.mFlipper.addView(item);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    CyclicGoodsBidHallPageActivity.this.mFlipper.setVisibility(0);
                    if (CyclicGoodsBidHallPageActivity.this.GZ_num >= 2) {
                        CyclicGoodsBidHallPageActivity.this.mFlipper.startFlipping();
                    } else {
                        CyclicGoodsBidHallPageActivity.this.mFlipper.stopFlipping();
                    }
                }
            }
            CyclicGoodsBidHallPageActivity.this.mSimpleAdater.notifyDataSetChanged();
        }
    };
    private JQUICallBack httpCallBackGz = new JQUICallBack() {
        public void callBack(ResultData result) {
            CyclicGoodsBidHallPageActivity.this.progressDialog.dismiss();
            Log.d("获取的数据：", result.getStringData());
            String dataString = result.getStringData();
            if (!dataString.equals(null) && !dataString.equals(StringEx.Empty)) {
                ContractParse parse = ContractParse.parse(result.getStringData());
                if (ContractParse.getDataString().equals(null) || ContractParse.getDataString().equals(StringEx.Empty)) {
                    new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this).setMessage("获取数据失败").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            CyclicGoodsBidHallPageActivity.this.startActivity(new Intent(CyclicGoodsBidHallPageActivity.this, CyclicGoodsIndexActivity.class));
                            CyclicGoodsBidHallPageActivity.this.finish();
                        }
                    }).show();
                    return;
                }
                Log.d("获取的ROWs数据：", ContractParse.jjo.toString());
                int i = 0;
                while (i < ContractParse.jjo.length()) {
                    try {
                        CyclicGoodsBidHallPageActivity.this.setGzResult = new JSONArray(ContractParse.jjo.getString(i)).getString(0);
                        if (CyclicGoodsBidHallPageActivity.this.setGzResult.equals(null) || CyclicGoodsBidHallPageActivity.this.setGzResult.equals(StringEx.Empty) || !CyclicGoodsBidHallPageActivity.this.setGzResult.equals("0")) {
                            if (!CyclicGoodsBidHallPageActivity.this.setGzResult.equals(null) && !CyclicGoodsBidHallPageActivity.this.setGzResult.equals(StringEx.Empty) && CyclicGoodsBidHallPageActivity.this.setGzResult.equals("1")) {
                                CyclicGoodsBidHallPageActivity.this.mItemArrayList.clear();
                                if (CyclicGoodsBidHallPageActivity.this.mFlipper.getChildCount() > 0) {
                                    CyclicGoodsBidHallPageActivity.this.mFlipper.removeAllViews();
                                }
                                CyclicGoodsBidHallPageActivity.this.getData(1);
                            }
                            i++;
                        } else {
                            AlertDialog.Builder dialog = new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this);
                            dialog.setCancelable(false);
                            dialog.setMessage("设置关注失败!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            }).show();
                            i++;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    };
    private JQUICallBack httpCallBackMan = new JQUICallBack() {
        public void callBack(ResultData result) {
            CyclicGoodsBidHallPageActivity.this.progressDialog.dismiss();
            Log.d("获取的数据：", result.getStringData());
            String dataString = result.getStringData();
            if (dataString.equals(null) || dataString.equals(StringEx.Empty)) {
                new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this).setMessage("获取数据失败").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        CyclicGoodsBidHallPageActivity.this.startActivity(new Intent(CyclicGoodsBidHallPageActivity.this, CyclicGoodsIndexActivity.class));
                        CyclicGoodsBidHallPageActivity.this.finish();
                    }
                }).show();
                return;
            }
            ContractParse parse = ContractParse.parse(result.getStringData());
            if (ContractParse.getDataString().equals(null) || ContractParse.getDataString().equals(StringEx.Empty)) {
                new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this).setMessage("获取数据失败").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        CyclicGoodsBidHallPageActivity.this.startActivity(new Intent(CyclicGoodsBidHallPageActivity.this, CyclicGoodsIndexActivity.class));
                        CyclicGoodsBidHallPageActivity.this.finish();
                    }
                }).show();
                return;
            }
            Log.d("获取的ROWs数据：", ContractParse.jjo.toString());
            int i = 0;
            while (i < ContractParse.jjo.length()) {
                try {
                    CyclicGoodsBidHallPageActivity.this.setGzResult = new JSONArray(ContractParse.jjo.getString(i)).getString(0);
                    if (CyclicGoodsBidHallPageActivity.this.setGzResult.equals(null) || CyclicGoodsBidHallPageActivity.this.setGzResult.equals(StringEx.Empty) || !CyclicGoodsBidHallPageActivity.this.setGzResult.equals("0")) {
                        if (!CyclicGoodsBidHallPageActivity.this.setGzResult.equals(null) && !CyclicGoodsBidHallPageActivity.this.setGzResult.equals(StringEx.Empty) && CyclicGoodsBidHallPageActivity.this.setGzResult.equals("1")) {
                            AlertDialog.Builder dialog = new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this);
                            dialog.setCancelable(false);
                            dialog.setMessage("出价成功!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    if (CyclicGoodsBidHallPageActivity.this.mPopupWindowPrice != null && CyclicGoodsBidHallPageActivity.this.mPopupWindowPrice.isShowing()) {
                                        CyclicGoodsBidHallPageActivity.this.mPopupWindowPrice.dismiss();
                                        CyclicGoodsBidHallPageActivity.this.manAddPriceEditText.setText(StringEx.Empty);
                                        CyclicGoodsBidHallPageActivity.this.sysPriceEditText.setText(StringEx.Empty);
                                    }
                                    if (CyclicGoodsBidHallPageActivity.this.mPopupWindowPriceFree != null && CyclicGoodsBidHallPageActivity.this.mPopupWindowPriceFree.isShowing()) {
                                        CyclicGoodsBidHallPageActivity.this.mPopupWindowPriceFree.dismiss();
                                        CyclicGoodsBidHallPageActivity.this.freePriceEditText.setText(StringEx.Empty);
                                    }
                                    CyclicGoodsBidHallPageActivity.this.mItemArrayList.clear();
                                    if (CyclicGoodsBidHallPageActivity.this.mFlipper.getChildCount() > 0) {
                                        CyclicGoodsBidHallPageActivity.this.mFlipper.removeAllViews();
                                    }
                                    CyclicGoodsBidHallPageActivity.this.getData(1);
                                    dialog.cancel();
                                }
                            }).show();
                        }
                        i++;
                    } else {
                        new AlertDialog.Builder(CyclicGoodsBidHallPageActivity.this).setMessage("出价失败!").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
                        i++;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public String id;
    /* access modifiers changed from: private */
    public LayoutInflater inflater;
    private boolean isFirstTime = true;
    private boolean isGetNextPage = false;
    /* access modifiers changed from: private */
    public String isGz;
    /* access modifiers changed from: private */
    public int itemHeight;
    private View itemHeightView;
    /* access modifiers changed from: private */
    public String jjms;
    /* access modifiers changed from: private */
    public String jjzt = StringEx.Empty;
    /* access modifiers changed from: private */
    public String jssj;
    /* access modifiers changed from: private */
    public long jssjLong;
    private boolean jssjOverSysTime = false;
    /* access modifiers changed from: private */
    public String kssj = StringEx.Empty;
    /* access modifiers changed from: private */
    public long kssjLong;
    /* access modifiers changed from: private */
    public boolean kssjOverSysTime = false;
    /* access modifiers changed from: private */
    public ImageButton leftImbt;
    /* access modifiers changed from: private */
    public LinearLayout mClock;
    /* access modifiers changed from: private */
    public LinearLayout mClockParent;
    /* access modifiers changed from: private */
    public ViewFlipper mFlipper;
    /* access modifiers changed from: private */
    public ArrayList<HashMap<String, Object>> mGuznZhuArrayList;
    public RadioButton mIndexNavigation1;
    public RadioButton mIndexNavigation2;
    public RadioButton mIndexNavigation3;
    public RadioButton mIndexNavigation4;
    public RadioButton mIndexNavigation5;
    /* access modifiers changed from: private */
    public ArrayList<HashMap<String, Object>> mItemArrayList;
    public TextView mListTitleTextview;
    private ListView mListView;
    /* access modifiers changed from: private */
    public RelativeLayout mManPrice;
    /* access modifiers changed from: private */
    public MyDigitalClock mMyDigitalClock;
    private RelativeLayout mOnePrice;
    /* access modifiers changed from: private */
    public PopupWindow mPopupWindowPrice;
    /* access modifiers changed from: private */
    public PopupWindow mPopupWindowPriceFree;
    private RadioButton mPriceNavigation1;
    private RadioButton mPriceNavigation2;
    private RadioButton mPriceNavigation3;
    private RadioGroup mPriceRadioderGroup;
    public RadioGroup mRadioderGroup;
    private View mRadioderGroupView;
    /* access modifiers changed from: private */
    public SimpleAdapter mSimpleAdater;
    public RadioButton mStart;
    /* access modifiers changed from: private */
    public RelativeLayout mSysPrice;
    /* access modifiers changed from: private */
    public ViewPager mViewPager;
    private ViewPagerAdapter mViewPagerPagerAdapter;
    /* access modifiers changed from: private */
    public EditText manAddPriceEditText;
    /* access modifiers changed from: private */
    public TextView manAmount;
    /* access modifiers changed from: private */
    public TextView manGrads;
    /* access modifiers changed from: private */
    public ImageButton manPriceImbt;
    /* access modifiers changed from: private */
    public MyAdapter myAdapter;
    /* access modifiers changed from: private */
    public String myprice = StringEx.Empty;
    /* access modifiers changed from: private */
    public int nextPageNum = 1;
    /* access modifiers changed from: private */
    public int numOfPageGet = 1;
    /* access modifiers changed from: private */
    public int numOfPageGet1 = 0;
    /* access modifiers changed from: private */
    public String pm = StringEx.Empty;
    /* access modifiers changed from: private */
    public ImageView popupBackground;
    /* access modifiers changed from: private */
    public int positionGz = 0;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    /* access modifiers changed from: private */
    public String qpj;
    /* access modifiers changed from: private */
    public ImageButton rightImbt;
    private int screenHeight;
    private int screenWidth;
    /* access modifiers changed from: private */
    public String setGzResult;
    /* access modifiers changed from: private */
    public TextView sysGrads;
    /* access modifiers changed from: private */
    public TextView sysNowPrice;
    /* access modifiers changed from: private */
    public EditText sysPriceEditText;
    /* access modifiers changed from: private */
    public ImageButton sysPriceImbt;
    /* access modifiers changed from: private */
    public long sysTimeLong;
    private long sysTimeLong2;
    /* access modifiers changed from: private */
    public TextView sysTimeTextView;
    /* access modifiers changed from: private */
    public TextView sysnotOver;
    /* access modifiers changed from: private */
    public String systemTime = StringEx.Empty;
    /* access modifiers changed from: private */
    public TextView thePrice;
    /* access modifiers changed from: private */
    public ImageButton thePriceImbt;
    /* access modifiers changed from: private */
    public ArrayList<View> viewPagerList;
    /* access modifiers changed from: private */
    public String wtmc;
    /* access modifiers changed from: private */
    public String wtprice;
    /* access modifiers changed from: private */
    public String ykj;
    /* access modifiers changed from: private */
    public boolean ykjAction = true;
    /* access modifiers changed from: private */
    public String zys;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void}
     arg types: [android.view.View, int, int, int]
     candidates:
      ClspMth{android.widget.PopupWindow.<init>(android.content.Context, android.util.AttributeSet, int, int):void}
      ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void} */
    /* access modifiers changed from: protected */
    @SuppressLint({"InlinedApi"})
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        ExitApplication.getInstance().addActivity(this);
        setContentView((int) R.layout.activity_cyclic_goods_bid_hall_page);
        this.dbHelper = DataBaseFactory.getInstance(this);
        this.dbHelper.insertOperation("循环物资", "我的竞价", "竞价大厅");
        ((ImageButton) findViewById(R.id.cyclicgoods_list_title_imbt)).setBackgroundResource(R.drawable.button_home_selector);
        this.mClockParent = (LinearLayout) findViewById(R.id.time_layout2);
        this.sysTimeTextView = (TextView) findViewById(R.id.system_time_text_view);
        this.sysTimeTextView.setVisibility(8);
        this.mClockParent.setVisibility(8);
        this.leftImbt = (ImageButton) findViewById(R.id.imbt_left);
        this.rightImbt = (ImageButton) findViewById(R.id.imbt_right);
        this.mFlipper = (ViewFlipper) findViewById(R.id.flipper);
        this.leftImbt.setVisibility(8);
        this.rightImbt.setVisibility(8);
        this.mListTitleTextview = (TextView) findViewById(R.id.cyclicgoods_list_title_textview);
        this.mListTitleTextview.setText("竞价大厅");
        this.mRadioderGroup = (RadioGroup) findViewById(R.id.cyclic_goods_index_RGroup);
        this.mIndexNavigation1 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item1);
        this.mIndexNavigation2 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item2);
        this.mIndexNavigation3 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item3);
        this.mIndexNavigation4 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item4);
        this.mIndexNavigation5 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item5);
        this.mViewPager = (ViewPager) findViewById(R.id.cyclicgoods_viewpager_bid_hall_page);
        if (ObjectStores.getInst().getObject("reStr") != "1") {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setCancelable(false);
            dialog.setMessage("您还没有登录，请登录！").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    SharedPreferences.Editor editor = CyclicGoodsBidHallPageActivity.this.getSharedPreferences("index_mark", 2).edit();
                    editor.putString("index", "竞价大厅");
                    editor.commit();
                    CyclicGoodsBidHallPageActivity.this.startActivity(new Intent(CyclicGoodsBidHallPageActivity.this, Login_indexActivity.class));
                    CyclicGoodsBidHallPageActivity.this.finish();
                }
            }).show();
            return;
        }
        this.inflater = getLayoutInflater();
        this.itemHeightView = this.inflater.inflate((int) R.layout.cyclic_goods_guanzhu_popup_item, (ViewGroup) null);
        ImageView messure_imageview = (ImageView) this.itemHeightView.findViewById(R.id.messure_imageview);
        messure_imageview.measure(0, 0);
        this.itemHeight = messure_imageview.getMeasuredHeight();
        Log.d("itemHeight高度", "高度为：" + this.itemHeight);
        this.cyclicgoods_page_num = (TextView) findViewById(R.id.cyclicgoods_page_num);
        this.popupBackground = (ImageView) findViewById(R.id.popup_background);
        this.mClock = (LinearLayout) findViewById(R.id.time_layout_clock);
        this.mFlipper.setInAnimation(AnimationUtils.loadAnimation(this, R.anim.push_up_in));
        this.mFlipper.setOutAnimation(AnimationUtils.loadAnimation(this, R.anim.push_up_out));
        this.guanZhuPopView = this.inflater.inflate((int) R.layout.cyclic_goods_guanzhu_popup, (ViewGroup) null);
        this.guanZhu = (ListView) this.guanZhuPopView.findViewById(R.id.cyclic_goods_guanzhu_popup);
        this.mGuznZhuArrayList = new ArrayList<>();
        this.mSimpleAdater = new SimpleAdapter(this, this.mGuznZhuArrayList, R.layout.cyclic_goods_guanzhu_popup_item, new String[]{"title", "title_num", "price", "myprice", "green"}, new int[]{R.id.cyclic_goods_guangzhu_item_title, R.id.cyclic_goods_guangzhu_item_title_num, R.id.cyclic_goods_guangzhu_price, R.id.cyclic_goods_guangzhu_item_my_price2, R.id.cyclic_goods_guangzhu_item_my_price_green});
        this.guanZhu.setAdapter((ListAdapter) this.mSimpleAdater);
        this.guanZhuPop = new PopupWindow(this.guanZhuPopView, -1, 400);
        this.guanZhuPop.setClippingEnabled(false);
        this.guanZhuPop.setOutsideTouchable(true);
        this.guanZhuPop.setAnimationStyle(R.style.AnimBottom);
        new ColorDrawable(-1342177280);
        this.backgroundColorDrawable = new ColorDrawable(Color.argb(0, (int) MotionEventCompat.ACTION_MASK, (int) MotionEventCompat.ACTION_MASK, (int) MotionEventCompat.ACTION_MASK));
        this.popupBackground.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (CyclicGoodsBidHallPageActivity.this.guanZhuPop.isShowing()) {
                    CyclicGoodsBidHallPageActivity.this.guanZhuPop.dismiss();
                }
                CyclicGoodsBidHallPageActivity.this.popupBackground.setVisibility(8);
                CyclicGoodsBidHallPageActivity.this.mFlipper.setVisibility(0);
            }
        });
        View popupWindowView = this.inflater.inflate((int) R.layout.activity_cyclic_goods_bid_hall_name_the_price_activity_dialog, (ViewGroup) null);
        this.manAddPriceEditText = (EditText) popupWindowView.findViewById(R.id.cyclicgoods_bid_dialog_add_price_edittext);
        this.manGrads = (TextView) popupWindowView.findViewById(R.id.cyclicgoods_bid_dialog_grads_yuan);
        this.manAmount = (TextView) popupWindowView.findViewById(R.id.cyclicgoods_bid_dialog_as_edittext);
        this.manPriceImbt = (ImageButton) popupWindowView.findViewById(R.id.cyclicgoods_bid_dialog_price_btn);
        this.sysPriceEditText = (EditText) popupWindowView.findViewById(R.id.cyclicgoods_bid_dialog_add_on1);
        this.sysGrads = (TextView) popupWindowView.findViewById(R.id.cyclicgoods_bid_dialog_sys_add);
        this.sysNowPrice = (TextView) popupWindowView.findViewById(R.id.cyclicgoods_bid_dialog_now_price1);
        this.sysnotOver = (TextView) popupWindowView.findViewById(R.id.cyclicgoods_bid_dialog_other1);
        this.sysPriceImbt = (ImageButton) popupWindowView.findViewById(R.id.cyclicgoods_bid_dialog_authorize_price_btn);
        this.thePrice = (TextView) popupWindowView.findViewById(R.id.cyclicgoods_bid_dialog_i_do1);
        this.thePriceImbt = (ImageButton) popupWindowView.findViewById(R.id.cyclicgoods_bid_dialog_authorize_price_btn1);
        this.mPopupWindowPrice = new PopupWindow(popupWindowView, -2, -2, true);
        this.mPopupWindowPrice.setBackgroundDrawable(this.backgroundColorDrawable);
        this.mPopupWindowPrice.setOutsideTouchable(true);
        this.mPopupWindowPrice.setOnDismissListener(new PopupWindow.OnDismissListener() {
            public void onDismiss() {
                CyclicGoodsBidHallPageActivity.this.sysPriceEditText.setText(StringEx.Empty);
                CyclicGoodsBidHallPageActivity.this.manAddPriceEditText.setText(StringEx.Empty);
                if (CyclicGoodsBidHallPageActivity.this.bidItem != null) {
                    CyclicGoodsBidHallPageActivity.this.bidItem.setBackgroundColor(0);
                }
            }
        });
        View popupWindowViewFree = this.inflater.inflate((int) R.layout.cyclic_goods_free_price, (ViewGroup) null);
        this.freePriceEditText = (EditText) popupWindowViewFree.findViewById(R.id.cyclicgoods_bid_dialog_free_price_edittext);
        this.freePriceImbt = (ImageButton) popupWindowViewFree.findViewById(R.id.cyclicgoods_bid_dialog_price_btn_free);
        this.mPopupWindowPriceFree = new PopupWindow(popupWindowViewFree, -2, -2, true);
        this.mPopupWindowPriceFree.setBackgroundDrawable(this.backgroundColorDrawable);
        this.mPopupWindowPriceFree.setOutsideTouchable(true);
        this.mPopupWindowPriceFree.setOnDismissListener(new PopupWindow.OnDismissListener() {
            public void onDismiss() {
                CyclicGoodsBidHallPageActivity.this.freePriceEditText.setText(StringEx.Empty);
                if (CyclicGoodsBidHallPageActivity.this.bidItem != null) {
                    CyclicGoodsBidHallPageActivity.this.bidItem.setBackgroundColor(0);
                }
            }
        });
        this.mRadioderGroupView = this.inflater.inflate((int) R.layout.cyclic_goods_navigation, (ViewGroup) null);
        this.mIndexNavigation4.setChecked(true);
        this.mManPrice = (RelativeLayout) popupWindowView.findViewById(R.id.cyclicgoods_bid_man_name_the_price_rl);
        this.mSysPrice = (RelativeLayout) popupWindowView.findViewById(R.id.cyclicgoods_bid_system_name_the_price_rl);
        this.mOnePrice = (RelativeLayout) popupWindowView.findViewById(R.id.cyclicgoods_bid_the_price_rl);
        this.mPriceRadioderGroup = (RadioGroup) popupWindowView.findViewById(R.id.cyclic_goods_bid_dialog);
        this.mPriceNavigation1 = (RadioButton) popupWindowView.findViewById(R.id.cyclicgoods_bid_man_name_the_price);
        this.mPriceNavigation2 = (RadioButton) popupWindowView.findViewById(R.id.cyclicgoods_bid_system_name_the_price);
        this.mPriceNavigation3 = (RadioButton) popupWindowView.findViewById(R.id.cyclicgoods_bid_the_price);
        this.mPriceNavigation1.setChecked(true);
        this.mPriceRadioderGroup.setOnCheckedChangeListener(this);
        this.mRadioderGroup.setOnCheckedChangeListener(this);
        this.myAdapter = new MyAdapter(this);
        this.viewPagerList = new ArrayList<>();
        this.mItemArrayList = new ArrayList<>();
        this.mViewPagerPagerAdapter = new ViewPagerAdapter(this.viewPagerList);
        this.mViewPager.setAdapter(this.mViewPagerPagerAdapter);
        this.mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageSelected(int arg0) {
                CyclicGoodsBidHallPageActivity.this.nextPageNum = arg0 + 1;
                CyclicGoodsBidHallPageActivity.this.mItemArrayList.clear();
                CyclicGoodsBidHallPageActivity.this.myAdapter.notifyDataSetInvalidated();
                CyclicGoodsBidHallPageActivity.this.showPageNum(CyclicGoodsBidHallPageActivity.this.nextPageNum, CyclicGoodsBidHallPageActivity.this.numOfPageGet);
                CyclicGoodsBidHallPageActivity.this.getData(CyclicGoodsBidHallPageActivity.this.nextPageNum);
                if (CyclicGoodsBidHallPageActivity.this.numOfPageGet > CyclicGoodsBidHallPageActivity.this.nextPageNum) {
                    CyclicGoodsBidHallPageActivity.this.rightImbt.setBackgroundResource(R.drawable.right_gray);
                } else {
                    CyclicGoodsBidHallPageActivity.this.rightImbt.setBackgroundResource(R.drawable.right_btn1);
                }
                if (CyclicGoodsBidHallPageActivity.this.nextPageNum > 1) {
                    CyclicGoodsBidHallPageActivity.this.leftImbt.setBackgroundResource(R.drawable.left_btn1);
                } else {
                    CyclicGoodsBidHallPageActivity.this.leftImbt.setBackgroundResource(R.drawable.left_gray);
                }
            }

            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            public void onPageScrollStateChanged(int arg0) {
            }
        });
        getData(1);
    }

    /* access modifiers changed from: private */
    public void showPageNum(int nowPage, int allPage) {
        this.cyclicgoods_page_num.setText(String.valueOf(nowPage) + "/" + allPage);
        this.cyclicgoods_page_num.setVisibility(0);
        this.rightImbt.setVisibility(0);
        this.leftImbt.setVisibility(0);
        new CountDownTimer(3000, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                CyclicGoodsBidHallPageActivity.this.cyclicgoods_page_num.setVisibility(8);
                CyclicGoodsBidHallPageActivity.this.rightImbt.setVisibility(8);
                CyclicGoodsBidHallPageActivity.this.leftImbt.setVisibility(8);
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public void getGuangzhu() {
        RequestBidData requestBidData = new RequestBidData(this);
        requestBidData.setRequestData("关注拼盘信息查询");
        Log.d("请求的指令：", requestBidData.infoDate());
        requestBidData.setHttpCallBack(this.httpCallBackGuanzhu);
        requestBidData.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    public void dissPopupWindowAction(View v) {
        if (!this.guanZhuPop.equals(null) && this.guanZhuPop.isShowing()) {
            this.guanZhuPop.dismiss();
            this.mFlipper.setVisibility(0);
        }
        this.popupBackground.setVisibility(8);
    }

    public void toLeftPageAction(View v) {
        int nowPage = this.mViewPager.getCurrentItem();
        if (nowPage > 0) {
            this.mItemArrayList.clear();
            this.myAdapter.notifyDataSetChanged();
            this.mViewPager.setCurrentItem(nowPage - 1);
        }
    }

    public void toRightPageAction(View v) {
        int nowPage = this.mViewPager.getCurrentItem();
        if (nowPage < this.numOfPageGet - 1) {
            this.mItemArrayList.clear();
            this.myAdapter.notifyDataSetChanged();
            this.mViewPager.setCurrentItem(nowPage + 1);
        }
    }

    /* access modifiers changed from: private */
    public void getManData(String bjlb, String price, String id2) {
        RequestBidData requestBidData = new RequestBidData(this);
        requestBidData.setRequestData("竞价出价");
        requestBidData.setId(id2);
        requestBidData.setXh(bjlb);
        requestBidData.setEndDate(price);
        Log.d("请求的指令：", requestBidData.infoDate());
        requestBidData.setHttpCallBack(this.httpCallBackMan);
        requestBidData.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    /* access modifiers changed from: private */
    public void getData(int pageNum) {
        RequestBidData requestBidData = new RequestBidData(this);
        requestBidData.setRequestData("竞价大厅列表");
        requestBidData.setPageNum(new StringBuilder(String.valueOf(pageNum)).toString());
        if (pageNum == 1) {
            this.mViewPager.setCurrentItem(0);
            this.firstTimeGetData = true;
        }
        Log.d("请求的指令：", requestBidData.infoDate());
        requestBidData.setHttpCallBack(this.httpCallBack);
        requestBidData.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.cyclicgoods_bid_man_name_the_price:
                this.mPriceNavigation1.setChecked(true);
                this.mManPrice.setVisibility(0);
                this.mSysPrice.setVisibility(8);
                this.mOnePrice.setVisibility(8);
                return;
            case R.id.cyclicgoods_bid_system_name_the_price:
                this.mPriceNavigation2.setChecked(true);
                this.mManPrice.setVisibility(8);
                this.mSysPrice.setVisibility(0);
                this.mOnePrice.setVisibility(8);
                return;
            case R.id.cyclicgoods_bid_the_price:
                this.mPriceNavigation3.setChecked(true);
                this.mManPrice.setVisibility(8);
                this.mSysPrice.setVisibility(8);
                this.mOnePrice.setVisibility(0);
                return;
            case R.id.cyclicgoods_index_navigation_item2:
                this.mIndexNavigation2.setChecked(true);
                startActivity(new Intent(this, CyclicGoodsBidActivity.class));
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item3:
                this.mIndexNavigation3.setChecked(true);
                startActivity(new Intent(this, CyclicGoodsBidSessionSelect.class));
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item1:
                if (ObjectStores.getInst().getObject("formX") == "formX") {
                    startActivity(new Intent(this, Xhjy_indexActivity.class));
                    finish();
                    return;
                }
                this.mIndexNavigation1.setChecked(true);
                startActivity(new Intent(this, CyclicGoodsIndexActivity.class));
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item4:
                this.mIndexNavigation4.setChecked(true);
                return;
            case R.id.cyclicgoods_index_navigation_item5:
                this.mIndexNavigation5.setChecked(true);
                Intent intent2 = new Intent(this, Xhjy_more.class);
                intent2.putExtra("part", "循环物资");
                startActivity(intent2);
                finish();
                return;
            default:
                return;
        }
    }

    public void backAaction(View v) {
        this.mIndexNavigation1.setChecked(true);
        if (ObjectStores.getInst().getObject("formX") == "formX") {
            ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class);
        } else {
            ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
            this.mIndexNavigation1.setChecked(true);
        }
        finish();
    }

    public void refreshAction(View v) {
        refresh();
    }

    public void refresh() {
        if (this.mPopupWindowPrice != null && this.mPopupWindowPrice.isShowing()) {
            this.mPopupWindowPrice.dismiss();
            this.manAddPriceEditText.setText(StringEx.Empty);
            this.sysPriceEditText.setText(StringEx.Empty);
        }
        if (this.mPopupWindowPriceFree != null && this.mPopupWindowPriceFree.isShowing()) {
            this.mPopupWindowPriceFree.dismiss();
            this.freePriceEditText.setText(StringEx.Empty);
        }
        this.mGuznZhuArrayList.clear();
        this.mItemArrayList.clear();
        if (this.mFlipper.getChildCount() > 0) {
            this.mFlipper.removeAllViews();
        }
        getData(1);
    }

    public void onBackPressed() {
        try {
            if (this.bidItem != null) {
                this.bidItem.setBackgroundColor(0);
            }
            if (this.guanZhuPop != null && this.guanZhuPop.isShowing()) {
                this.popupBackground.setVisibility(8);
                this.guanZhuPop.dismiss();
                this.mFlipper.setVisibility(0);
            } else if (this.mPopupWindowPrice != null && this.mPopupWindowPrice.isShowing()) {
                this.mPopupWindowPrice.dismiss();
                this.manAddPriceEditText.setText(StringEx.Empty);
                this.sysPriceEditText.setText(StringEx.Empty);
            } else if (this.mPopupWindowPriceFree == null || !this.mPopupWindowPriceFree.isShowing()) {
                if (this.mFlipper.getChildCount() > 0) {
                    this.mFlipper.removeAllViews();
                }
                if (ObjectStores.getInst().getObject("formX") == "formX") {
                    ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class);
                    finish();
                    return;
                }
                this.mIndexNavigation1.setChecked(true);
                startActivity(new Intent(this, CyclicGoodsIndexActivity.class));
                finish();
            } else {
                this.mPopupWindowPriceFree.dismiss();
                this.freePriceEditText.setText(StringEx.Empty);
            }
        } catch (Exception e) {
        }
    }

    public class ViewHolder {
        public TextView add_price;
        public TextView end_time;
        public TextView myprice;
        public TextView myprice1;
        public TextView now_price;
        public TextView now_price1;
        public TextView pinpanhao;
        public LineText resources;
        public LineText resourcesBegin;
        public LineText resourcesEnd;
        public RadioButton star;
        public TextView start_price;
        public TextView title;

        public ViewHolder() {
        }
    }

    /* access modifiers changed from: private */
    public void getGzData(String id2, String isGz2) {
        RequestBidData requestBidData = new RequestBidData(this);
        requestBidData.setRequestData("关注拼盘设置");
        requestBidData.setId(id2);
        requestBidData.setXh(isGz2);
        Log.d("请求的指令：", requestBidData.infoDate());
        requestBidData.setHttpCallBack(this.httpCallBackGz);
        requestBidData.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    public class MyAdapter extends BaseAdapter {
        private LayoutInflater mMyAdapterInflater;

        public MyAdapter(Context context) {
            this.mMyAdapterInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return CyclicGoodsBidHallPageActivity.this.mItemArrayList.size();
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            int pos = position;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = this.mMyAdapterInflater.inflate((int) R.layout.activity_cyclic_goods_bid_hall_page_item, (ViewGroup) null);
                holder.title = (TextView) convertView.findViewById(R.id.cyclicgoods_bid_hall_page_item_title);
                holder.resources = (LineText) convertView.findViewById(R.id.cyclicgoods_bid_hall_page_item_resources2);
                holder.start_price = (TextView) convertView.findViewById(R.id.cyclicgoods_bid_hall_page_item_start_price);
                holder.now_price1 = (TextView) convertView.findViewById(R.id.cyclicgoods_bid_hall_page_item_now_price);
                holder.now_price = (TextView) convertView.findViewById(R.id.cyclicgoods_bid_hall_page_item_now_price2);
                holder.end_time = (TextView) convertView.findViewById(R.id.cyclicgoods_bid_hall_page_item_end_time);
                holder.add_price = (TextView) convertView.findViewById(R.id.cyclicgoods_bid_hall_page_item_add_price);
                holder.pinpanhao = (TextView) convertView.findViewById(R.id.cyclicgoods_bid_hall_page_item_pinpanhao);
                holder.resourcesBegin = (LineText) convertView.findViewById(R.id.cyclicgoods_bid_hall_page_item_resources);
                holder.resourcesEnd = (LineText) convertView.findViewById(R.id.cyclicgoods_bid_hall_page_item_resources3);
                holder.star = (RadioButton) convertView.findViewById(R.id.cyclicgoods_star);
                holder.myprice1 = (TextView) convertView.findViewById(R.id.cyclicgoods_bid_hall_page_item_my_new_price2);
                holder.myprice = (TextView) convertView.findViewById(R.id.cyclicgoods_bid_hall_page_item_my_new_price);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            String wtmc = (String) ((HashMap) CyclicGoodsBidHallPageActivity.this.mItemArrayList.get(position)).get("wtmc");
            String zys = (String) ((HashMap) CyclicGoodsBidHallPageActivity.this.mItemArrayList.get(position)).get("zys");
            String qpj = (String) ((HashMap) CyclicGoodsBidHallPageActivity.this.mItemArrayList.get(position)).get("qpj");
            String dqj = (String) ((HashMap) CyclicGoodsBidHallPageActivity.this.mItemArrayList.get(position)).get("dqj");
            String jssj = (String) ((HashMap) CyclicGoodsBidHallPageActivity.this.mItemArrayList.get(position)).get("jssj");
            String jjms = (String) ((HashMap) CyclicGoodsBidHallPageActivity.this.mItemArrayList.get(position)).get("jjms");
            String bpfs = (String) ((HashMap) CyclicGoodsBidHallPageActivity.this.mItemArrayList.get(position)).get("bpfs");
            final String isGz = (String) ((HashMap) CyclicGoodsBidHallPageActivity.this.mItemArrayList.get(position)).get("isGz");
            final String id = (String) ((HashMap) CyclicGoodsBidHallPageActivity.this.mItemArrayList.get(position)).get("id");
            String str = (String) ((HashMap) CyclicGoodsBidHallPageActivity.this.mItemArrayList.get(position)).get("bjtd");
            String str2 = (String) ((HashMap) CyclicGoodsBidHallPageActivity.this.mItemArrayList.get(position)).get("ykj");
            String str3 = (String) ((HashMap) CyclicGoodsBidHallPageActivity.this.mItemArrayList.get(position)).get("wtprice");
            String kssj = (String) ((HashMap) CyclicGoodsBidHallPageActivity.this.mItemArrayList.get(position)).get("kssj");
            String str4 = (String) ((HashMap) CyclicGoodsBidHallPageActivity.this.mItemArrayList.get(position)).get("jjzt");
            String myprice = (String) ((HashMap) CyclicGoodsBidHallPageActivity.this.mItemArrayList.get(position)).get("myprice");
            String str5 = (String) ((HashMap) CyclicGoodsBidHallPageActivity.this.mItemArrayList.get(position)).get("pm");
            String systemTimeString = CyclicGoodsBidHallPageActivity.this.systemTime.replaceAll("\\D", StringEx.Empty);
            String kssjString = kssj.replaceAll("\\D", StringEx.Empty);
            long systemTimeLong = Long.parseLong(systemTimeString);
            long kssjStringLong = Long.parseLong(kssjString);
            int dqjInt = Integer.parseInt(dqj);
            int mypriceInt = Integer.parseInt(myprice);
            if (myprice.equals("0") || myprice.equals(StringEx.Empty) || myprice.equals(null)) {
                holder.myprice1.setTextColor(-16777216);
                holder.myprice1.setText("未出价");
            } else if (jjms.equals("2")) {
                holder.myprice1.setTextColor(-65536);
                holder.myprice1.setText(new StringBuilder(String.valueOf(mypriceInt)).toString());
            } else if (mypriceInt >= dqjInt) {
                holder.myprice1.setTextColor(-65536);
                holder.myprice1.setText(String.valueOf(mypriceInt) + "（领先）");
            } else {
                holder.myprice1.setTextColor(Color.rgb(0, 133, 0));
                holder.myprice1.setText(String.valueOf(mypriceInt) + "（落后）");
            }
            holder.title.setText(wtmc);
            holder.resources.setText(zys);
            holder.start_price.setText("起拍价：" + qpj);
            holder.now_price.setText(dqj);
            if (kssjStringLong > systemTimeLong) {
                holder.end_time.setText("开始时间：" + kssj);
            } else {
                holder.end_time.setText("结束时间：" + jssj);
            }
            holder.pinpanhao.setText("拼盘号：" + id);
            if (!jjms.equals(null) && !jjms.equals(StringEx.Empty) && jjms.equals("1")) {
                if (!bpfs.equals(null) && !bpfs.equals(StringEx.Empty) && bpfs.equals("1")) {
                    holder.add_price.setText("公开增价:单价");
                } else if (!bpfs.equals(null) && !bpfs.equals(StringEx.Empty) && bpfs.equals("2")) {
                    holder.add_price.setText("公开增价:总价");
                }
                holder.now_price1.setVisibility(0);
                holder.now_price.setVisibility(0);
            } else if (!jjms.equals(null) && !jjms.equals(StringEx.Empty) && jjms.equals("2")) {
                if (!bpfs.equals(null) && !bpfs.equals(StringEx.Empty) && bpfs.equals("1")) {
                    holder.add_price.setText("自由报价:单价");
                } else if (!bpfs.equals(null) && !bpfs.equals(StringEx.Empty) && bpfs.equals("2")) {
                    holder.add_price.setText("自由报价:总价");
                }
                holder.now_price1.setVisibility(8);
                holder.now_price.setVisibility(8);
            }
            if (!isGz.equals(null) && !isGz.equals(StringEx.Empty) && isGz.equals("0")) {
                holder.star.setChecked(true);
            } else if (!isGz.equals(null) && !isGz.equals(StringEx.Empty) && isGz.equals("1")) {
                holder.star.setChecked(false);
            }
            CyclicGoodsBidHallPageActivity.this.setTextVeiwClick(holder.resourcesBegin, id, wtmc);
            CyclicGoodsBidHallPageActivity.this.setTextVeiwClick(holder.resources, id, wtmc);
            CyclicGoodsBidHallPageActivity.this.setTextVeiwClick(holder.resourcesEnd, id, wtmc);
            final int i = pos;
            holder.star.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    String gz = StringEx.Empty;
                    if (isGz.equals("0")) {
                        gz = "1";
                    } else if (isGz.equals("1")) {
                        gz = "0";
                    }
                    CyclicGoodsBidHallPageActivity.this.positionGz = i;
                    CyclicGoodsBidHallPageActivity.this.getGzData(id, gz);
                }
            });
            return convertView;
        }
    }

    /* access modifiers changed from: private */
    public void setTextVeiwClick(LineText lk, final String ppid, final String wtid) {
        lk.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(CyclicGoodsBidHallPageActivity.this, CyclicGoodsBidHallToActivity.class);
                intent.putExtra("ppid", ppid);
                intent.putExtra("wtid", wtid);
                if (CyclicGoodsBidHallPageActivity.this.mFlipper.getChildCount() > 0) {
                    CyclicGoodsBidHallPageActivity.this.mFlipper.removeAllViews();
                }
                CyclicGoodsBidHallPageActivity.this.startActivity(intent);
                CyclicGoodsBidHallPageActivity.this.finish();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        try {
            if (this.isFirstTime) {
                this.isFirstTime = false;
            } else {
                if (this.mPopupWindowPrice != null && this.mPopupWindowPrice.isShowing()) {
                    this.mPopupWindowPrice.dismiss();
                    this.manAddPriceEditText.setText(StringEx.Empty);
                    this.sysPriceEditText.setText(StringEx.Empty);
                }
                if (this.mPopupWindowPriceFree != null && this.mPopupWindowPriceFree.isShowing()) {
                    this.mPopupWindowPriceFree.dismiss();
                    this.freePriceEditText.setText(StringEx.Empty);
                }
                try {
                    this.mItemArrayList.clear();
                } catch (Exception e) {
                }
                if (this.mFlipper.getChildCount() > 0) {
                    this.mFlipper.removeAllViews();
                }
                getData(1);
            }
        } catch (Exception e2) {
        }
        super.onResume();
    }
}
