package com.baosteelOnline.xhjy;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.baosteelOnline.R;
import com.baosteelOnline.common.DataBaseFactory;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.model.SmallNumUtil;
import com.baosteelOnline.sql.DBHelper;
import com.jianq.misc.StringEx;
import java.util.HashMap;

public class Jg_url_list extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    private RelativeLayout bottom_nva1;
    private RelativeLayout bottom_nva2;
    private DataBaseFactory db;
    /* access modifiers changed from: private */
    public DBHelper dbHelper;
    /* access modifiers changed from: private */
    public ImageButton homeBtn;
    private String index_more;
    private ImageButton listBtn;
    private RadioButton mIndexNavigation1;
    private RadioButton mIndexNavigation2;
    private RadioButton mIndexNavigation3;
    private RadioButton mIndexNavigation4;
    private RadioButton mIndexNavigation5;
    private RadioGroup mRadioderGroup;
    /* access modifiers changed from: private */
    public WebView mWebView = null;
    private String part = StringEx.Empty;
    private RadioGroup radioderGroup;
    private String sign;
    private SharedPreferences sp;
    private TextView title;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.jg_list);
        ExitApplication.getInstance().addActivity(this);
        this.dbHelper = DataBaseFactory.getInstance(this);
        this.homeBtn = (ImageButton) findViewById(R.id.homebutton);
        this.listBtn = (ImageButton) findViewById(R.id.listbutton);
        this.listBtn.setVisibility(8);
        this.title = (TextView) findViewById(R.id.titletextview);
        this.bottom_nva1 = (RelativeLayout) findViewById(R.id.bottom_nva1);
        this.bottom_nva2 = (RelativeLayout) findViewById(R.id.bottom_nva2);
        this.mWebView = (WebView) findViewById(R.id.weblist);
        this.mWebView.getSettings().setJavaScriptEnabled(true);
        this.sign = getIntent().getStringExtra("sign");
        this.part = getIntent().getExtras().getString("part");
        this.sp = getSharedPreferences("BaoIndex", 2);
        if (this.sp.getString("bIndex", StringEx.Empty).equals("xhwz")) {
            this.bottom_nva1.setVisibility(8);
            this.bottom_nva2.setVisibility(0);
        } else {
            this.bottom_nva2.setVisibility(8);
            this.bottom_nva1.setVisibility(0);
        }
        this.mRadioderGroup = (RadioGroup) findViewById(R.id.cyclic_goods_index_RGroup);
        this.mIndexNavigation1 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item1);
        this.mIndexNavigation2 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item2);
        this.mIndexNavigation3 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item3);
        this.mIndexNavigation4 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item4);
        this.mIndexNavigation5 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item5);
        this.mRadioderGroup.setOnCheckedChangeListener(this);
        this.mIndexNavigation1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ExitApplication.getInstance().startActivity(Jg_url_list.this, CyclicGoodsIndexActivity.class);
                Jg_url_list.this.finish();
            }
        });
        load();
        ((RadioButton) findViewById(R.id.radio_home3)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ExitApplication.getInstance().startActivity(Jg_url_list.this, Xhjy_indexActivity.class);
                Jg_url_list.this.finish();
            }
        });
        SmallNumUtil.GWCNum(this, (TextView) findViewById(R.id.gwc_dhqrnum));
        this.radioderGroup = (RadioGroup) findViewById(R.id.xhjy_index_RGroup);
        this.radioderGroup.setOnCheckedChangeListener(this);
        this.homeBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Jg_url_list.this.mWebView.canGoBack()) {
                    Jg_url_list.this.mWebView.goBack();
                    Jg_url_list.this.homeBtn.setBackgroundResource(R.drawable.button_home_selector);
                    return;
                }
                ExitApplication.getInstance().back(Jg_url_list.this);
            }
        });
        this.listBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Jg_url_list.this.dbHelper.insertOperation("钢材交易", "钢材交易首页", "价格政策列表");
                Jg_url_list.this.load();
            }
        });
    }

    /* access modifiers changed from: private */
    public void load() {
        if (this.sign.equals("ptgg")) {
            this.title.setText("平台公告");
            this.mWebView.loadUrl("http://testex.bsteel.com/newexchange/proclamation2-0-indexMoveNewMarketNotice.html");
        }
        if (this.sign.equals("jgzc")) {
            this.title.setText("价格政策");
            this.mWebView.loadUrl("http://www.bsteel.com.cn/exchange/jsp/navigation/common/createFle/mbgtj/index.html");
        }
        this.mWebView.setWebViewClient(new HelloWebViewClient(this, null));
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || !this.mWebView.canGoBack()) {
            return super.onKeyDown(keyCode, event);
        }
        this.mWebView.goBack();
        return true;
    }

    private class HelloWebViewClient extends WebViewClient {
        private HelloWebViewClient() {
        }

        /* synthetic */ HelloWebViewClient(Jg_url_list jg_url_list, HelloWebViewClient helloWebViewClient) {
            this();
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Jg_url_list.this.homeBtn.setBackgroundResource(R.drawable.button_back_selector);
            Jg_url_list.this.dbHelper.insertOperation("钢材交易", "钢材交易首页", "价格政策详情");
            view.loadUrl(url);
            return true;
        }
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.cyclicgoods_index_navigation_item2:
                this.mIndexNavigation2.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item3:
                this.mIndexNavigation3.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidSessionSelect.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item1:
                this.mIndexNavigation1.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item4:
                this.mIndexNavigation4.setChecked(true);
                startActivity(new Intent(this, CyclicGoodsBidHallPageActivity.class));
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item5:
                this.mIndexNavigation5.setChecked(true);
                Intent intent = new Intent(this, Xhjy_more.class);
                intent.putExtra("part", "循环物资");
                startActivity(intent);
                finish();
                return;
            case R.id.radio_search3:
                ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
                return;
            case R.id.radio_shopcar3:
                ((RadioButton) findViewById(R.id.radio_shopcar3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ShopActivity.class);
                return;
            case R.id.radio_home3:
                ((RadioButton) findViewById(R.id.radio_home3)).setChecked(true);
                this.index_more = "1";
                HashMap hasmap = new HashMap();
                hasmap.put("index_more", this.index_more);
                ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class, hasmap);
                return;
            case R.id.radio_contract3:
                ((RadioButton) findViewById(R.id.radio_contract3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ContractListActivity.class);
                return;
            case R.id.radio_notice3:
                ((RadioButton) findViewById(R.id.radio_notice3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, Xhjy_more.class);
                return;
            default:
                return;
        }
    }
}
