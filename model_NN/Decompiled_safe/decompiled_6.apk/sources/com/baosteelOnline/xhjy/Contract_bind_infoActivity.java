package com.baosteelOnline.xhjy;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.andframework.ui.CustomListView;
import com.andframework.ui.CustomMessageShow;
import com.baosteelOnline.R;
import com.baosteelOnline.common.DataBaseFactory;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.data.Contract_bind_infoBusi;
import com.baosteelOnline.data_parse.SearchParse;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.model.SmallNumUtil;
import com.baosteelOnline.sql.DBHelper;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import java.util.ArrayList;
import java.util.HashMap;

public class Contract_bind_infoActivity extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    private TextView TV_addressname;
    private TextView TV_formatname;
    private TextView TV_mertial;
    private TextView TV_numberunit;
    private TextView TV_price;
    private TextView TV_productname;
    private TextView TV_sellername;
    CustomListView contract_bind_list;
    private DataBaseFactory db;
    private DBHelper dbHelper;
    private String hth = StringEx.Empty;
    private JQUICallBack httCallBack = new JQUICallBack() {
        public void callBack(ResultData result) {
            Contract_bind_infoActivity.this.progressDialog.dismiss();
            try {
                System.out.println("result=StringData11=>" + result.getStringData());
                Contract_bind_infoActivity.this.updateNoticeList(SearchParse.parse(result.getStringData()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    private RadioGroup radioderGroup;
    private View view;
    private View view2;
    private String xh = StringEx.Empty;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ExitApplication.getInstance().addActivity(this);
        requestWindowFeature(1);
        setContentView((int) R.layout.xhjy_contract_bind_info);
        this.dbHelper = DataBaseFactory.getInstance(this);
        this.dbHelper.insertOperation("钢材交易", "买家中心", "捆包信息列表");
        this.contract_bind_list = (CustomListView) findViewById(R.id.contract_bind_list);
        this.TV_productname = (TextView) findViewById(R.id.productname_contract_bind);
        this.TV_sellername = (TextView) findViewById(R.id.sellername_contract_bind);
        this.TV_mertial = (TextView) findViewById(R.id.material_contract_bind);
        this.TV_formatname = (TextView) findViewById(R.id.formatname_contract_bind);
        this.TV_price = (TextView) findViewById(R.id.price_contract_bind);
        this.TV_numberunit = (TextView) findViewById(R.id.numberunit_contract_bind);
        this.TV_addressname = (TextView) findViewById(R.id.addressname_contract_bind);
        ((RadioButton) findViewById(R.id.radio_contract3)).setChecked(true);
        SmallNumUtil.GWCNum(this, (TextView) findViewById(R.id.gwc_dhqrnum));
        this.radioderGroup = (RadioGroup) findViewById(R.id.xhjy_index_RGroup);
        this.radioderGroup.setOnCheckedChangeListener(this);
        this.xh = getIntent().getStringExtra("xh");
        this.hth = getIntent().getStringExtra("hth");
        this.TV_productname.setText(getIntent().getStringExtra("productname"));
        this.TV_sellername.setText("卖家:" + getIntent().getStringExtra("sellername"));
        this.TV_mertial.setText("材质:" + getIntent().getStringExtra("mertial"));
        this.TV_formatname.setText("规格:" + getIntent().getStringExtra("format"));
        this.TV_price.setText(getIntent().getStringExtra("price"));
        this.TV_numberunit.setText(String.valueOf(getIntent().getStringExtra("piece")) + "件/" + getIntent().getStringExtra("desunit") + "吨");
        this.TV_addressname.setText("产地:" + getIntent().getStringExtra("productaddr"));
        testBusi();
    }

    public void testBusi() {
        Contract_bind_infoBusi infoBusi = new Contract_bind_infoBusi(this);
        infoBusi.xh = this.xh;
        infoBusi.hth = this.hth;
        infoBusi.setHttpCallBack(this.httCallBack);
        infoBusi.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    /* access modifiers changed from: package-private */
    public void updateNoticeList(SearchParse searchParse) {
        if (SearchParse.commonData == null) {
            CustomMessageShow.getInst().showLongToast(this, "服务器无数据返回！");
            return;
        }
        if (SearchParse.commonData.blocks.r0.mar.rows.rows.size() == 0) {
            new AlertDialog.Builder(this).setMessage("对不起，没有数据!").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    CustomMessageShow.getInst().cancleProgressDialog();
                    ExitApplication.getInstance().back(0);
                }
            }).show().setCanceledOnTouchOutside(false);
        } else if (!(SearchParse.commonData == null || SearchParse.commonData.blocks == null || SearchParse.commonData.blocks.r0 == null || SearchParse.commonData.blocks.r0.mar == null || SearchParse.commonData.blocks.r0.mar.rows == null || SearchParse.commonData.blocks.r0.mar.rows.rows == null)) {
            for (int i = 0; i < SearchParse.commonData.blocks.r0.mar.rows.rows.size(); i++) {
                ArrayList<String> rowdata = SearchParse.commonData.blocks.r0.mar.rows.rows.get(i);
                SearchRow sr = new SearchRow();
                if (rowdata != null) {
                    for (int k = 0; k < rowdata.size(); k++) {
                        if (k == 0) {
                            sr.number = String.valueOf(i + 1);
                        }
                        if (k == 1) {
                            sr.weight = "重量:" + ((String) rowdata.get(k)) + "吨";
                        }
                        if (k == 4) {
                            sr.khw = (String) rowdata.get(k);
                        }
                        if (k == 5) {
                            sr.bz = "备注:" + ((String) rowdata.get(k));
                        }
                    }
                    this.view2 = buildRowView(sr);
                    this.contract_bind_list.addViewToLast(this.view2);
                    rowdata.removeAll(rowdata);
                }
            }
            this.contract_bind_list.onRefreshComplete();
        }
        CustomMessageShow.getInst().cancleProgressDialog();
    }

    private View buildRowView(SearchRow sr) {
        this.view = View.inflate(this, R.layout.xhjy_contract_bind_info_row, null);
        TextView TV_khw = (TextView) this.view.findViewById(R.id.khw_contract_bind_row);
        TextView TV_bei = (TextView) this.view.findViewById(R.id.bz_contract_bind_row);
        ((TextView) this.view.findViewById(R.id.TV_num_contract_bind_row)).setText(sr.number);
        ((TextView) this.view.findViewById(R.id.weight_contract_bind_row)).setText(sr.weight);
        if (sr.khw.equals(StringEx.Empty)) {
            TV_khw.setVisibility(8);
        } else {
            TV_khw.setText("库位:" + sr.khw);
        }
        TV_bei.setText(sr.bz);
        return this.view;
    }

    class SearchRow {
        public String bz;
        public String ck;
        public String khw;
        public String number;
        public String weight;

        SearchRow() {
        }
    }

    public void contract_bind_backButtonAction(View v) {
        ExitApplication.getInstance().back(0);
    }

    public void onBackPressed() {
        ExitApplication.getInstance().back(0);
        super.onBackPressed();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.TV_sellername = null;
        this.TV_productname = null;
        this.TV_addressname = null;
        this.TV_mertial = null;
        this.TV_formatname = null;
        this.TV_numberunit = null;
        this.TV_price = null;
        this.hth = null;
        this.xh = null;
        this.contract_bind_list = null;
        this.view = null;
        this.view2 = null;
        super.onDestroy();
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.radio_search3:
                ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
                return;
            case R.id.radio_shopcar3:
                ((RadioButton) findViewById(R.id.radio_shopcar3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ShopActivity.class);
                return;
            case R.id.radio_home3:
                ((RadioButton) findViewById(R.id.radio_home3)).setChecked(true);
                HashMap hasmap = new HashMap();
                hasmap.put("index_more", "1");
                ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class, hasmap);
                return;
            case R.id.radio_contract3:
                ((RadioButton) findViewById(R.id.radio_contract3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ContractListActivity.class);
                return;
            case R.id.radio_notice3:
                ((RadioButton) findViewById(R.id.radio_notice3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, Xhjy_more.class);
                return;
            default:
                return;
        }
    }
}
