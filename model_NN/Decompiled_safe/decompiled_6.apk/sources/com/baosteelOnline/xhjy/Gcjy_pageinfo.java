package com.baosteelOnline.xhjy;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.R;
import com.baosteelOnline.common.DataBaseFactory;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.data.RequestBidData;
import com.baosteelOnline.data_parse.ContractParse;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.model.SmallNumUtil;
import com.baosteelOnline.sql.DBHelper;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.net.JQBasicNetwork;
import com.jianq.ui.JQUICallBack;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;

public class Gcjy_pageinfo extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    private String activity = StringEx.Empty;
    /* access modifiers changed from: private */
    public ImageButton backButton;
    public ContractParse contractParse;
    private DataBaseFactory db;
    private DBHelper dbHelper;
    private JQUICallBack httpCallBack = new JQUICallBack() {
        public void callBack(ResultData result) {
            Gcjy_pageinfo.this.progressDialog.dismiss();
            Log.d("result.getStringData获取的数据：", result.getStringData());
            if (!result.getStringData().equals(null) && !result.getStringData().equals(StringEx.Empty)) {
                Gcjy_pageinfo.this.contractParse = ContractParse.parse(result.getStringData());
                if (ContractParse.getDataString().equals(null) || ContractParse.getDataString().equals(StringEx.Empty)) {
                    new AlertDialog.Builder(Gcjy_pageinfo.this).setMessage("获取数据失败。").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
                    return;
                }
                Log.d("获取的ROWs数据：", ContractParse.jjo.toString());
                for (int i = 0; i < ContractParse.jjo.length(); i++) {
                    try {
                        JSONArray row = new JSONArray(ContractParse.jjo.getString(i));
                        Gcjy_pageinfo.this.mHtmlData = row.getString(1);
                        Gcjy_pageinfo.this.wtid = row.getString(2);
                        Gcjy_pageinfo.this.mWebView.loadDataWithBaseURL(null, Gcjy_pageinfo.this.mHtmlData, "text/html", JQBasicNetwork.UTF_8, null);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public int i = 0;
    /* access modifiers changed from: private */
    public String mHtmlData = StringEx.Empty;
    public TextView mListTitleTextview;
    /* access modifiers changed from: private */
    public WebView mWebView;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    private RadioGroup radioderGroup;
    /* access modifiers changed from: private */
    public String wtid = StringEx.Empty;

    /* access modifiers changed from: protected */
    @SuppressLint({"SetJavaScriptEnabled"})
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.activity_gcjy_pageinfo);
        ExitApplication.getInstance().addActivity(this);
        this.dbHelper = DataBaseFactory.getInstance(this);
        this.backButton = (ImageButton) findViewById(R.id.cyclicgoods_list_title_imbt);
        this.radioderGroup = (RadioGroup) findViewById(R.id.xhjy_index_RGroup);
        RadioButton radio_notice = (RadioButton) findViewById(R.id.radio_home3);
        radio_notice.setChecked(true);
        radio_notice.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ExitApplication.getInstance().startActivity(Gcjy_pageinfo.this, Xhjy_indexActivity.class);
                Gcjy_pageinfo.this.finish();
            }
        });
        SmallNumUtil.GWCNum(this, (TextView) findViewById(R.id.gwc_dhqrnum));
        Intent intent = getIntent();
        this.wtid = intent.getExtras().getString("id");
        this.activity = intent.getExtras().getString("activity");
        this.radioderGroup.setOnCheckedChangeListener(this);
        this.mWebView = (WebView) findViewById(R.id.Gcjy__bid_page_webview_pin_tai);
        this.mWebView.getSettings().setBuiltInZoomControls(true);
        this.mWebView.setBackgroundColor(Color.parseColor("#ffffff"));
        this.mWebView.setFocusable(true);
        this.mWebView.requestFocus();
        this.mWebView.clearCache(true);
        this.mWebView.getSettings().setJavaScriptEnabled(true);
        this.mWebView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        this.mWebView.setWebViewClient(new HelloWebViewClient(this, null));
        this.mWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int newProgress) {
                if (Gcjy_pageinfo.this.mWebView.canGoBack()) {
                    Gcjy_pageinfo.this.backButton.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            Gcjy_pageinfo access$0 = Gcjy_pageinfo.this;
                            access$0.i = access$0.i - 1;
                            Log.d("i======>竞价公告", new StringBuilder(String.valueOf(Gcjy_pageinfo.this.i)).toString());
                            if (Gcjy_pageinfo.this.i == 0) {
                                Gcjy_pageinfo.this.mWebView.loadDataWithBaseURL(null, Gcjy_pageinfo.this.mHtmlData, "text/html", JQBasicNetwork.UTF_8, null);
                            } else if (Gcjy_pageinfo.this.i == -1) {
                                Gcjy_pageinfo.this.backAction();
                            } else {
                                Gcjy_pageinfo.this.mWebView.goBack();
                            }
                        }
                    });
                } else {
                    Gcjy_pageinfo.this.backButton.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            Gcjy_pageinfo.this.backAction();
                        }
                    });
                }
            }
        });
        this.mListTitleTextview = (TextView) findViewById(R.id.cyclicgoods_list_title_textview);
        getDate();
        if (this.activity.equals("平台公告")) {
            this.dbHelper.insertOperation("钢材交易", "钢材交易首页", "平台公告详情");
            this.mListTitleTextview.setText("平台公告");
            this.mWebView.getSettings().setTextSize(WebSettings.TextSize.NORMAL);
        } else if (this.activity.equals("交易预告")) {
            this.dbHelper.insertOperation("钢材交易", "钢材交易首页", "交易预告详情");
            ObjectStores.getInst().putObject("positionName", this.wtid);
            this.mListTitleTextview.setText("交易预告");
        } else if (this.activity.equals("竞价公告")) {
            this.mWebView.getSettings().setTextSize(WebSettings.TextSize.NORMAL);
            this.mListTitleTextview.setText("竞价公告");
        }
    }

    private class HelloWebViewClient extends WebViewClient {
        private HelloWebViewClient() {
        }

        /* synthetic */ HelloWebViewClient(Gcjy_pageinfo gcjy_pageinfo, HelloWebViewClient helloWebViewClient) {
            this();
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Gcjy_pageinfo gcjy_pageinfo = Gcjy_pageinfo.this;
            gcjy_pageinfo.i = gcjy_pageinfo.i + 1;
            view.loadUrl(url);
            return true;
        }
    }

    public void getDate() {
        RequestBidData requestBidData = new RequestBidData(this);
        requestBidData.setRequestData("竞价公告详情");
        requestBidData.setId(this.wtid);
        Log.d("请求的指令：", requestBidData.infoDate());
        requestBidData.setHttpCallBack(this.httpCallBack);
        requestBidData.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    public void onBackPressed() {
        backAction();
    }

    /* access modifiers changed from: private */
    public void backAction() {
        Intent intent = new Intent(this, Xhjy_url_list.class);
        String sign = getIntent().getStringExtra("sign");
        String numOfBtn = getIntent().getExtras().getString("numOfBtn");
        intent.putExtra("sign", sign);
        intent.putExtra("numOfBtn", numOfBtn);
        Log.d("sign=", String.valueOf(sign) + "numOfBtn=" + numOfBtn);
        startActivity(intent);
        finish();
    }

    public void backAaction(View v) {
        backAction();
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.radio_search3:
                ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
                return;
            case R.id.radio_shopcar3:
                ((RadioButton) findViewById(R.id.radio_shopcar3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ShopActivity.class);
                return;
            case R.id.radio_home3:
                ((RadioButton) findViewById(R.id.radio_home3)).setChecked(true);
                HashMap hasmap = new HashMap();
                hasmap.put("index_more", "1");
                ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class, hasmap);
                return;
            case R.id.radio_contract3:
                ((RadioButton) findViewById(R.id.radio_contract3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ContractListActivity.class);
                return;
            case R.id.radio_notice3:
                ((RadioButton) findViewById(R.id.radio_notice3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, Xhjy_more.class);
                return;
            default:
                return;
        }
    }
}
