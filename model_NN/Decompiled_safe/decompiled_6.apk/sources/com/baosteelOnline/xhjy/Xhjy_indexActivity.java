package com.baosteelOnline.xhjy;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.R;
import com.baosteelOnline.common.CodeFilter;
import com.baosteelOnline.common.DataBaseFactory;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.common.jqhttp.CustomDialog;
import com.baosteelOnline.common.jqhttp.UpdateHelper;
import com.baosteelOnline.common.jqhttp.Utils;
import com.baosteelOnline.data.Msg_queryHYinfoBusi;
import com.baosteelOnline.data.Msg_queryMobilemainBusi;
import com.baosteelOnline.data_parse.WelcomeParse;
import com.baosteelOnline.image.AsyncImageLoader;
import com.baosteelOnline.image.FileCache;
import com.baosteelOnline.image.ImageAndUrl;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.sql.DBHelper;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

public class Xhjy_indexActivity extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    private ViewPagerAdapter adapter;
    AsyncImageLoader asyncImageLoader;
    private String bIndex = StringEx.Empty;
    /* access modifiers changed from: private */
    public int currentItem;
    private DataBaseFactory db;
    /* access modifiers changed from: private */
    public DBHelper dbHelper;
    /* access modifiers changed from: private */
    public ArrayList<View> dots = new ArrayList<>();
    private SharedPreferences.Editor editor;
    /* access modifiers changed from: private */
    public SharedPreferences.Editor editor2;
    String filePath = StringEx.Empty;
    /* access modifiers changed from: private */
    public ViewPager gcviewPager;
    private TextView gsTextview;
    /* access modifiers changed from: private */
    public TextView gwc_dhqrnum;
    UpdateHelper helper;
    List<ImageAndUrl> imageInfosShow = new ArrayList();
    List<ImageView> images;
    List<String> imagesur2 = new ArrayList();
    List<String> imagesurl = new ArrayList();
    private String index = StringEx.Empty;
    private JQUICallBack index_httpCallBack = new JQUICallBack() {
        public void callBack(ResultData result) {
            Xhjy_indexActivity.this.getDate(WelcomeParse.parse(result.getStringData()));
        }
    };
    private String index_more = StringEx.Empty;
    public RelativeLayout layout2;
    /* access modifiers changed from: private */
    public Handler mHandler;
    private Message mMsg;
    private JQUICallBack mob_httpCallBack = new JQUICallBack() {
        public void callBack(ResultData result) {
            Xhjy_indexActivity.this.getMobDate(WelcomeParse.parse(result.getStringData()));
        }
    };
    /* access modifiers changed from: private */
    public int oldPosition = 0;
    ProgressDialog progressDialog;
    private RadioGroup radioderGroup;
    private ScheduledExecutorService scheduledExecutorService;
    private SharedPreferences sp;
    private SharedPreferences sp2;
    /* access modifiers changed from: private */
    public DialogInterface.OnClickListener updateCancelListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
        }
    };
    String updateUrl = StringEx.Empty;
    /* access modifiers changed from: private */
    public String updateflag = StringEx.Empty;
    String updatename = StringEx.Empty;
    String updateurl = StringEx.Empty;
    /* access modifiers changed from: private */
    public int[] welcomeDate;
    private TextView welcomeText;
    private TextView welcomeText1;
    /* access modifiers changed from: private */
    public ImageView xhjy_small1;
    /* access modifiers changed from: private */
    public ImageView xhjy_small2;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.xhjy_index);
        ExitApplication.getInstance().addActivity(this);
        this.dbHelper = DataBaseFactory.getInstance(this);
        this.dbHelper.insertOperation("钢材交易", "钢材交易首页", "钢材交易首页");
        this.asyncImageLoader = new AsyncImageLoader();
        this.gcviewPager = (ViewPager) findViewById(R.id.vp_gc);
        this.gcviewPager.setOnPageChangeListener(new MyOnPageChangeListener(this, null));
        this.layout2 = (RelativeLayout) findViewById(R.id.layout2);
        this.sp2 = getSharedPreferences("setDate", 2);
        this.editor2 = this.sp2.edit();
        this.sp = getSharedPreferences("BaoIndex", 2);
        this.editor = this.sp.edit();
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/fzjl.ttf");
        this.gsTextview = (TextView) findViewById(R.id.gcindex_gs_text);
        this.welcomeText = (TextView) findViewById(R.id.gcindex_welcome_text);
        this.welcomeText1 = (TextView) findViewById(R.id.gcindex_welcome_text1);
        if (ObjectStores.getInst().getObject("companyFullName") == null) {
            this.welcomeText1.setVisibility(0);
            this.welcomeText1.setText("亲，欢迎您！");
            this.welcomeText.setVisibility(8);
            this.gsTextview.setVisibility(8);
        } else {
            this.welcomeText1.setVisibility(8);
            this.welcomeText.setVisibility(0);
            this.gsTextview.setVisibility(0);
            this.welcomeText.setText(getString(R.string.index_welcome));
            this.gsTextview.setText(ObjectStores.getInst().getObject("companyFullName").toString());
        }
        this.welcomeText1.setTypeface(face);
        this.gsTextview.setTypeface(face);
        this.welcomeText.setTypeface(face);
        ObjectStores.getInst().putObject("formX", "formX");
        this.xhjy_small1 = (ImageView) findViewById(R.id.xhjy_small1);
        this.xhjy_small2 = (ImageView) findViewById(R.id.xhjy_small2);
        mobBusi();
        testBusi();
        this.mHandler = new Handler() {
            public void handleMessage(Message msg) {
                if (msg.what == 2) {
                    Xhjy_indexActivity.this.helper.update(Xhjy_indexActivity.this.updateUrl, Xhjy_indexActivity.this.filePath, Xhjy_indexActivity.this.updateCancelListener, Xhjy_indexActivity.this.updateflag);
                } else if (msg.what == 4) {
                    Xhjy_indexActivity.this.helper.updateMandatory(Xhjy_indexActivity.this.updateUrl, Xhjy_indexActivity.this.filePath, Xhjy_indexActivity.this.updateCancelListener, Xhjy_indexActivity.this.updateflag);
                } else if (msg.what == 5) {
                    SharedPreferences wsp = Xhjy_indexActivity.this.getSharedPreferences("gcwelcome", 2);
                    SharedPreferences.Editor editor = wsp.edit();
                    if (!wsp.getBoolean("isClick", false)) {
                        Xhjy_indexActivity.this.xhjy_small1.setVisibility(0);
                        Xhjy_indexActivity.this.xhjy_small2.setVisibility(0);
                    } else if (wsp.getInt("gcindex_jjgg", -1) == -1) {
                        editor.putInt("gcindex_jjgg", Xhjy_indexActivity.this.welcomeDate[1]);
                        editor.putInt("gcindex_jyyg", Xhjy_indexActivity.this.welcomeDate[2]);
                        editor.commit();
                        Xhjy_indexActivity.this.xhjy_small1.setVisibility(0);
                        Xhjy_indexActivity.this.xhjy_small2.setVisibility(0);
                    } else {
                        if (Xhjy_indexActivity.this.welcomeDate[1] > wsp.getInt("gcindex_jjgg", -1)) {
                            Xhjy_indexActivity.this.xhjy_small1.setVisibility(0);
                        } else {
                            Xhjy_indexActivity.this.xhjy_small1.setVisibility(8);
                        }
                        if (Xhjy_indexActivity.this.welcomeDate[2] > wsp.getInt("gcindex_jyyg", -1)) {
                            Xhjy_indexActivity.this.xhjy_small2.setVisibility(0);
                        } else {
                            Xhjy_indexActivity.this.xhjy_small2.setVisibility(8);
                        }
                    }
                    int num = Xhjy_indexActivity.this.welcomeDate[4];
                    System.out.println("num==>" + num);
                    SharedPreferences.Editor jjed = Xhjy_indexActivity.this.getSharedPreferences("smallnum", 2).edit();
                    jjed.putInt("gwcnum", num);
                    jjed.commit();
                    Xhjy_indexActivity.this.gwc_dhqrnum = (TextView) Xhjy_indexActivity.this.findViewById(R.id.gwc_dhqrnum);
                    Xhjy_indexActivity.this.gwc_dhqrnum.setGravity(17);
                    Xhjy_indexActivity.this.gwc_dhqrnum.setText(new StringBuilder(String.valueOf(num)).toString());
                    if (num != 0) {
                        Xhjy_indexActivity.this.gwc_dhqrnum.setVisibility(0);
                    } else {
                        Xhjy_indexActivity.this.gwc_dhqrnum.setVisibility(8);
                    }
                } else if (msg.what == 6) {
                    Xhjy_indexActivity.this.getDatas();
                } else if (msg.what == 1) {
                    Animation loadAnimation = AnimationUtils.loadAnimation(Xhjy_indexActivity.this, R.anim.enter);
                    Xhjy_indexActivity.this.gcviewPager.setCurrentItem(Xhjy_indexActivity.this.currentItem);
                }
                super.handleMessage(msg);
            }
        };
        ObjectStores.getInst().putObject("isFirst", "true");
        ((RadioButton) findViewById(R.id.radio_home3)).setChecked(true);
        this.radioderGroup = (RadioGroup) findViewById(R.id.xhjy_index_RGroup);
        this.radioderGroup.setOnCheckedChangeListener(this);
    }

    private class MyOnPageChangeListener implements ViewPager.OnPageChangeListener {
        private MyOnPageChangeListener() {
        }

        /* synthetic */ MyOnPageChangeListener(Xhjy_indexActivity xhjy_indexActivity, MyOnPageChangeListener myOnPageChangeListener) {
            this();
        }

        public void onPageScrollStateChanged(int arg0) {
        }

        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        public void onPageSelected(int position) {
            ((View) Xhjy_indexActivity.this.dots.get(Xhjy_indexActivity.this.oldPosition)).setBackgroundResource(R.drawable.dxt_point_nomral);
            ((View) Xhjy_indexActivity.this.dots.get(position)).setBackgroundResource(R.drawable.dxt_point_selected);
            Xhjy_indexActivity.this.oldPosition = position;
            Xhjy_indexActivity.this.currentItem = position;
        }
    }

    /* access modifiers changed from: private */
    public void getDatas() {
        for (int i = 0; i < this.imagesurl.size(); i++) {
            ImageAndUrl item = new ImageAndUrl();
            item.setImageurl(this.imagesurl.get(i));
            item.setUrl("item-->" + i);
            this.imageInfosShow.add(item);
        }
        this.images = new ArrayList();
        for (int i2 = 0; i2 < this.imageInfosShow.size(); i2++) {
            this.images.add(new ImageView(this));
        }
        initDots();
        this.adapter = new ViewPagerAdapter(this, null);
        this.gcviewPager.setAdapter(this.adapter);
        vpChangeTask();
    }

    private void initDots() {
        LinearLayout ll_vp_point = (LinearLayout) findViewById(R.id.gc_vp_point);
        for (int i = 0; i < this.imageInfosShow.size(); i++) {
            ImageView iv_image = (ImageView) getLayoutInflater().inflate((int) R.layout.item_dots, (ViewGroup) null);
            if (i == 0) {
                iv_image.setBackgroundResource(R.drawable.dxt_point_selected);
            }
            iv_image.setTag(this.imageInfosShow.get(i));
            ll_vp_point.addView(iv_image);
            this.dots.add(iv_image);
        }
    }

    private void vpChangeTask() {
        this.scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        this.scheduledExecutorService.scheduleWithFixedDelay(new ViewPagerTask(this, null), 5, 5, TimeUnit.SECONDS);
    }

    private class ViewPagerTask implements Runnable {
        private ViewPagerTask() {
        }

        /* synthetic */ ViewPagerTask(Xhjy_indexActivity xhjy_indexActivity, ViewPagerTask viewPagerTask) {
            this();
        }

        public void run() {
            Xhjy_indexActivity.this.currentItem = (Xhjy_indexActivity.this.currentItem + 1) % Xhjy_indexActivity.this.imageInfosShow.size();
            Message message = new Message();
            message.what = 1;
            Xhjy_indexActivity.this.mHandler.sendMessage(message);
        }
    }

    private class ViewPagerAdapter extends PagerAdapter {
        private ViewPagerAdapter() {
        }

        /* synthetic */ ViewPagerAdapter(Xhjy_indexActivity xhjy_indexActivity, ViewPagerAdapter viewPagerAdapter) {
            this();
        }

        public void destroyItem(View container, int position, Object object) {
            ((ViewPager) container).removeView(Xhjy_indexActivity.this.images.get(position));
        }

        public int getCount() {
            return Xhjy_indexActivity.this.imageInfosShow.size();
        }

        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }

        public Object instantiateItem(View view, int position) {
            View imageLayout = Xhjy_indexActivity.this.getLayoutInflater().inflate((int) R.layout.item_pager_image, (ViewGroup) null);
            final ImageView imageView = (ImageView) imageLayout.findViewById(R.id.image);
            final String imgUrl = Xhjy_indexActivity.this.imageInfosShow.get(position).getImageurl();
            final String ggid = Xhjy_indexActivity.this.imagesur2.get(position);
            imageView.setTag(imgUrl);
            imageView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    SharedPreferences.Editor editor = Xhjy_indexActivity.this.getSharedPreferences("indextz", 2).edit();
                    editor.putBoolean("xhjygc", true);
                    editor.commit();
                    Intent intent = new Intent(Xhjy_indexActivity.this, GCJYBidPageActivity.class);
                    intent.putExtra("id", ggid);
                    intent.putExtra("xhjygc", "yes");
                    intent.putExtra("activity", "竞价公告");
                    intent.putExtra("numOfBtn", "竞价公告");
                    intent.putExtra("sign", "gcjjgg");
                    Xhjy_indexActivity.this.startActivity(intent);
                }
            });
            if (Xhjy_indexActivity.this.checkConnection()) {
                Bitmap bmpFromSD = FileCache.getInstance().getBmp(imgUrl);
                if (bmpFromSD != null) {
                    imageView.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
                    imageView.setImageBitmap(bmpFromSD);
                } else {
                    Drawable cachedImage = Xhjy_indexActivity.this.asyncImageLoader.loaDrawable(imgUrl, new AsyncImageLoader.ImageCallBack() {
                        public void imageLoaded(Drawable imageDrawable) {
                            Bitmap bitmap = Xhjy_indexActivity.this.drawToBmp(imageDrawable);
                            FileCache.getInstance().savaBmpData(imgUrl, bitmap);
                            ImageView imageViewByTag = null;
                            if (bitmap != null) {
                                imageViewByTag = (ImageView) imageView.findViewWithTag(imgUrl);
                                imageViewByTag.setLayoutParams(new LinearLayout.LayoutParams(-2, bitmap.getHeight()));
                            }
                            if (imageViewByTag == null) {
                                return;
                            }
                            if (Xhjy_indexActivity.this.isWifi(Xhjy_indexActivity.this)) {
                                imageViewByTag.setImageBitmap(bitmap);
                            } else if (bitmap != null) {
                                imageViewByTag.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                                imageViewByTag.setImageBitmap(bitmap);
                                imageViewByTag.setScaleType(ImageView.ScaleType.MATRIX);
                            }
                        }
                    });
                    if (cachedImage == null) {
                        imageView.setImageResource(R.drawable.index_image_bg);
                    } else if (Xhjy_indexActivity.this.isWifi(Xhjy_indexActivity.this)) {
                        Bitmap bitmap = Xhjy_indexActivity.this.drawToBmp(cachedImage);
                        imageView.setLayoutParams(new LinearLayout.LayoutParams(-2, bitmap.getHeight()));
                        imageView.setImageBitmap(bitmap);
                    } else {
                        imageView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                        imageView.setImageBitmap(Xhjy_indexActivity.this.drawToBmp(cachedImage));
                    }
                }
            } else {
                Bitmap bmpFromSD2 = FileCache.getInstance().getBmp(imgUrl);
                if (bmpFromSD2 != null) {
                    ImageView imageViewByTag = (ImageView) imageView.findViewWithTag(imgUrl);
                    imageViewByTag.setLayoutParams(new LinearLayout.LayoutParams(-2, bmpFromSD2.getHeight()));
                    imageViewByTag.setImageBitmap(bmpFromSD2);
                } else {
                    imageView.setImageResource(R.drawable.index_image_bg);
                }
            }
            ((ViewPager) view).addView(imageLayout, 0);
            return imageLayout;
        }
    }

    public Bitmap drawToBmp(Drawable d) {
        if (d != null) {
            return ((BitmapDrawable) d).getBitmap();
        }
        return null;
    }

    public boolean checkConnection() {
        NetworkInfo networkInfo = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
        if (networkInfo != null) {
            return networkInfo.isAvailable();
        }
        return false;
    }

    public boolean isWifi(Context mContext) {
        NetworkInfo activeNetInfo = ((ConnectivityManager) mContext.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetInfo == null || !activeNetInfo.getTypeName().equals("WIFI")) {
            return false;
        }
        return true;
    }

    public void mobBusi() {
        Msg_queryMobilemainBusi infoBusi = new Msg_queryMobilemainBusi(this);
        infoBusi.setHttpCallBack(this.mob_httpCallBack);
        infoBusi.iExecute();
    }

    /* access modifiers changed from: private */
    public void getMobDate(WelcomeParse searchParse) {
        if (WelcomeParse.commonData != null && WelcomeParse.commonData.blocks != null && WelcomeParse.commonData.blocks.r0 != null && WelcomeParse.commonData.blocks.r0.mar != null && WelcomeParse.commonData.blocks.r0.mar.rows != null && WelcomeParse.commonData.blocks.r0.mar.rows.rows.size() != 0 && WelcomeParse.commonData != null && WelcomeParse.commonData.blocks != null && WelcomeParse.commonData.blocks.r0 != null && WelcomeParse.commonData.blocks.r0.mar != null && WelcomeParse.commonData.blocks.r0.mar.rows != null && WelcomeParse.commonData.blocks.r0.mar.rows.rows != null) {
            int j = WelcomeParse.commonData.blocks.r0.mar.rows.rows.size();
            System.out.println("ItemInfo---size: " + j);
            for (int i = 0; i < j; i++) {
                ArrayList<String> rowdata = WelcomeParse.commonData.blocks.r0.mar.rows.rows.get(i);
                if (rowdata != null) {
                    for (int k = 0; k < rowdata.size(); k++) {
                        Log.i("welcome==============>WebImage" + k, (String) rowdata.get(k));
                        if (k == 0) {
                            this.imagesurl.add((String) rowdata.get(0));
                        } else if (k == 1) {
                            this.imagesur2.add((String) rowdata.get(1));
                        }
                    }
                }
            }
            Message message = new Message();
            message.what = 6;
            this.mHandler.sendMessage(message);
        }
    }

    public void testBusi() {
        Msg_queryHYinfoBusi infoBusi = new Msg_queryHYinfoBusi(this);
        if (ObjectStores.getInst().getObject("parameter_hy") == null) {
            infoBusi.parameter_hy = StringEx.Empty;
        } else {
            infoBusi.parameter_hy = ObjectStores.getInst().getObject("parameter_hy").toString();
        }
        infoBusi.zc = "1";
        infoBusi.setHttpCallBack(this.index_httpCallBack);
        infoBusi.iExecute();
    }

    /* access modifiers changed from: private */
    public void getDate(WelcomeParse searchParse) {
        if (WelcomeParse.commonData != null && WelcomeParse.commonData.blocks != null && WelcomeParse.commonData.blocks.r0 != null && WelcomeParse.commonData.blocks.r0.mar != null && WelcomeParse.commonData.blocks.r0.mar.rows != null && WelcomeParse.commonData.blocks.r0.mar.rows.rows.size() != 0 && WelcomeParse.commonData != null && WelcomeParse.commonData.blocks != null && WelcomeParse.commonData.blocks.r0 != null && WelcomeParse.commonData.blocks.r0.mar != null && WelcomeParse.commonData.blocks.r0.mar.rows != null && WelcomeParse.commonData.blocks.r0.mar.rows.rows != null) {
            int j = WelcomeParse.commonData.blocks.r0.mar.rows.rows.size();
            System.out.println("ItemInfo---size: " + j);
            this.welcomeDate = new int[5];
            for (int i = 0; i < j; i++) {
                ArrayList<String> rowdata = WelcomeParse.commonData.blocks.r0.mar.rows.rows.get(i);
                if (rowdata != null) {
                    for (int k = 0; k < rowdata.size(); k++) {
                        Log.i("welcome==============>" + k, (String) rowdata.get(k));
                        this.welcomeDate[k] = Integer.valueOf((String) rowdata.get(k)).intValue();
                    }
                }
            }
            Message message = new Message();
            message.what = 5;
            this.mHandler.sendMessage(message);
        }
    }

    private void checkUpdate() {
        new Thread(new Runnable() {
            public void run() {
                Xhjy_indexActivity.this.checkforupdate();
            }
        }).start();
    }

    public void jumpToFirst() {
        this.index_more = "1";
        this.bIndex = "1";
        this.index = "xhwz";
        HashMap hasmap = new HashMap();
        hasmap.put("bIndex", this.bIndex);
        this.editor.putString("bIndex", this.index);
        this.editor.putString("second", "Second");
        this.editor.commit();
        hasmap.put("index_more", this.index_more);
        ObjectStores.getInst().putObject("formX", "formY");
        ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class, hasmap);
        finish();
    }

    public void changAction(View v) {
        this.index_more = "1";
        this.bIndex = "1";
        this.index = "xhwz";
        HashMap hasmap = new HashMap();
        hasmap.put("bIndex", this.bIndex);
        this.editor.putString("bIndex", this.index);
        this.editor.commit();
        hasmap.put("index_more", this.index_more);
        ObjectStores.getInst().putObject("formX", "formY");
        ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class, hasmap);
        overridePendingTransition(R.anim.danrdanchu, R.anim.danchudanr);
        finish();
    }

    public void ptgg_ImageViewAction(View v) {
        HashMap hasmap = new HashMap();
        this.index = getSharedPreferences("BaoIndex", 2).getString("bIndex", StringEx.Empty);
        hasmap.put("part", "钢材交易");
        hasmap.put("numOfBtn", "平台公告按钮");
        System.out.println("index===" + this.index);
        if (this.index.equals("gcjy")) {
            hasmap.put("sign", "gcjjyg");
        }
        if (this.index.equals("xhwz")) {
            hasmap.put("sign", "wzjjyg");
        }
        this.dbHelper.insertOperation("钢材交易", "钢材交易首页", "平台公告");
        ExitApplication.getInstance().startActivity(this, Xhjy_url_list.class, hasmap);
    }

    public void jyyg_ImageViewAction(View v) {
        SharedPreferences.Editor editor3 = getSharedPreferences("gcwelcome", 2).edit();
        editor3.putBoolean("isClick", true);
        editor3.commit();
        HashMap hasmap = new HashMap();
        this.index = getSharedPreferences("BaoIndex", 2).getString("bIndex", StringEx.Empty);
        hasmap.put("part", "钢材交易");
        hasmap.put("numOfBtn", "交易预告按钮");
        System.out.println("index===" + this.index);
        if (this.index.equals("gcjy")) {
            hasmap.put("sign", "gcjjyg");
        }
        if (this.index.equals("xhwz")) {
            hasmap.put("sign", "wzjjyg");
        }
        ExitApplication.getInstance().startActivity(this, Xhjy_url_list.class, hasmap);
    }

    public void jjgg_ImageViewAction(View v) {
        SharedPreferences.Editor editor3 = getSharedPreferences("gcwelcome", 2).edit();
        editor3.putBoolean("isClick", true);
        editor3.commit();
        HashMap hasmap = new HashMap();
        this.index = getSharedPreferences("BaoIndex", 2).getString("bIndex", StringEx.Empty);
        hasmap.put("numOfBtn", "竞价公告");
        if (this.index.equals("gcjy")) {
            hasmap.put("sign", "gcjjgg");
        }
        if (this.index.equals("xhwz")) {
            hasmap.put("sign", "wzjjgg");
        }
        ExitApplication.getInstance().startActivity(this, Xhjy_url_list.class, hasmap);
    }

    public void jgzc_ImageViewAction(View v) {
        HashMap hasmap = new HashMap();
        hasmap.put("sign", "jgzc");
        ExitApplication.getInstance().startActivity(this, Jg_url_list.class, hasmap);
    }

    public void bggj_ImageViewAction(View v) {
        HashMap hasmap = new HashMap();
        hasmap.put("cd", new CodeFilter().sToUtf("%E5%AE%9D%E9%92%A2"));
        ExitApplication.getInstance().startActivity(this, SearchActivity.class, hasmap);
    }

    public void xej_ImageViewAction(View v) {
        HashMap hasmap = new HashMap();
        String mj = new CodeFilter().sToUtf("%E5%AE%9D%E9%92%A2%E6%96%B0%E4%BA%8B%E4%B8%9A");
        hasmap.put("xhjy_marks", "1");
        hasmap.put("mj", mj);
        ExitApplication.getInstance().startActivity(this, SearchActivity.class, hasmap);
    }

    public void zhbzc_ImageViewAction(View v) {
        HashMap hasmap = new HashMap();
        String pm = new CodeFilter().sToUtf("%E4%B8%AD%E5%8E%9A%E6%9D%BF");
        hasmap.put("xhjy_marks", "1");
        hasmap.put("pm", pm);
        ExitApplication.getInstance().startActivity(this, SearchActivity.class, hasmap);
    }

    public void zhbzc_ImageViewAction1(View v) {
        ObjectStores.getInst().putObject("formX", "formX");
        startActivity(new Intent(this, CyclicGoodsBidHallPageActivity.class));
        finish();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        ((RadioButton) findViewById(R.id.radio_home3)).setChecked(true);
        super.onResume();
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.radio_search3:
                ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
                return;
            case R.id.radio_shopcar3:
                ((RadioButton) findViewById(R.id.radio_shopcar3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ShopActivity.class);
                return;
            case R.id.radio_home3:
                ((RadioButton) findViewById(R.id.radio_home3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class);
                return;
            case R.id.radio_contract3:
                ((RadioButton) findViewById(R.id.radio_contract3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ContractListActivity.class);
                return;
            case R.id.radio_notice3:
                ((RadioButton) findViewById(R.id.radio_notice3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, Xhjy_more.class);
                return;
            default:
                return;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return false;
        }
        new AlertDialog.Builder(this).setTitle((int) R.string.app_name).setIcon((int) R.drawable.icon).setMessage("您确认要退出吗？").setNegativeButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Xhjy_indexActivity.this.dbHelper.insertOperation("退出", "退出", "退出");
                Xhjy_indexActivity.this.clearAll();
                Xhjy_indexActivity.this.editor2.clear();
                Xhjy_indexActivity.this.editor2.commit();
                ExitApplication.getInstance().exit();
            }
        }).setPositiveButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        }).create().show();
        return true;
    }

    /* access modifiers changed from: private */
    public void clearAll() {
        ObjectStores.getInst().putObject("inputUserName", StringEx.Empty);
        ObjectStores.getInst().putObject("inputPassword", StringEx.Empty);
        ObjectStores.getInst().putObject("XH_msg", StringEx.Empty);
        ObjectStores.getInst().putObject("GfullName", StringEx.Empty);
        ObjectStores.getInst().putObject("parameter_usertokenid", StringEx.Empty);
        ObjectStores.getInst().putObject("parameter_userid", StringEx.Empty);
        ObjectStores.getInst().putObject("companyCode", StringEx.Empty);
        ObjectStores.getInst().putObject("companyFullName", StringEx.Empty);
        ObjectStores.getInst().putObject("BSP_companyCode", StringEx.Empty);
        ObjectStores.getInst().putObject("G_User", StringEx.Empty);
        ObjectStores.getInst().putObject("XH_reStr", StringEx.Empty);
        ObjectStores.getInst().putObject("reStr", StringEx.Empty);
        ObjectStores.getInst().putObject("parameter_czy", StringEx.Empty);
        ObjectStores.getInst().putObject("parameter_czyname", StringEx.Empty);
        ObjectStores.getInst().putObject("parameter_czyrealname", StringEx.Empty);
        ObjectStores.getInst().putObject("parameter_hy", StringEx.Empty);
        ObjectStores.getInst().putObject("parameter_hyname", StringEx.Empty);
        ObjectStores.getInst().putObject("deviceid", StringEx.Empty);
        this.editor.clear();
        this.editor.commit();
    }

    private String getversion() {
        try {
            HttpResponse httpResponse = new DefaultHttpClient().execute(new HttpGet("http://m.bsteel.net/update/update_bsteelonline_Android.txt"));
            if (httpResponse.getStatusLine().getStatusCode() == 200) {
                String result = EntityUtils.toString(httpResponse.getEntity());
                System.out.println("result-----:" + result);
                JSONObject json = new JSONObject(result);
                String version = json.getString("latestversion");
                this.updateurl = json.getString("updateurl");
                this.updatename = json.getString("updatename");
                this.updateflag = json.getString("updateflag");
                System.out.println("取得到的version----:" + version);
                return version;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return StringEx.Empty;
    }

    /* access modifiers changed from: private */
    public void checkforupdate() {
        String serverVersion = getversion();
        System.out.println("程序包路径----:" + getApplicationContext().getFilesDir().getAbsolutePath());
        this.filePath = "/mnt/sdcard/bsteel/temp/" + this.updatename;
        this.updateUrl = this.updateurl;
        try {
            String clientVersion = Utils.getVersionName(this);
            this.helper = new UpdateHelper(this);
            if (!this.helper.checkUpdate(serverVersion, clientVersion)) {
                return;
            }
            if (this.updateflag.equals("0")) {
                this.mHandler.sendEmptyMessage(2);
            } else if (this.updateflag.equals("1")) {
                this.mHandler.sendEmptyMessage(4);
            }
        } catch (PackageManager.NameNotFoundException e) {
            CustomDialog.getInst().showAlertDialog(this, null, "没找到versionName", null, 0, null);
        }
    }

    private void cancelUpdate() {
    }

    private void setDialog() {
        this.sp = getSharedPreferences("BaoIndex", 2);
        this.editor = this.sp.edit();
        this.bIndex = getIntent().getStringExtra("bIndex");
        if (this.bIndex == null) {
            this.bIndex = StringEx.Empty;
        }
        this.index_more = getIntent().getStringExtra("index_more");
        if (this.index_more == null) {
            this.index_more = StringEx.Empty;
        }
        if (ObjectStores.getInst().getObject("isFirst") != "true") {
            ObjectStores.getInst().putObject("isFirst", "false");
        } else {
            this.bIndex.equals("1");
        }
    }
}
