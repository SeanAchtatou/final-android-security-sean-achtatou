package com.baosteelOnline.xhjy;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.R;
import com.baosteelOnline.common.DataBaseFactory;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.model.SmallNumUtil;
import com.baosteelOnline.sql.DBHelper;
import com.jianq.misc.StringEx;
import java.util.Calendar;
import java.util.HashMap;

public class Contract_siltActivity extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    private static final int DATE_DIALOG_ID = 1;
    private static final int SHOW_DATAPICK = 0;
    private static final int SHOW_TIMEPICK = 2;
    private static final int TIME_DIALOG_ID = 3;
    public EditText ET_beginTime;
    public EditText ET_endTime;
    public String begin_time = StringEx.Empty;
    final String[] contractStatus = {"所有状态", "作废", "待支付", "生效", "已付款", "已提货", "已完成", "违约结算中", "违约终止", "待收款", "待仓库过户确认", "待做市商确认"};
    public String contract_num = StringEx.Empty;
    public String contract_stats = StringEx.Empty;
    public String contract_status = StringEx.Empty;
    public EditText contractnum;
    Handler dateandtimeHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    Contract_siltActivity.this.showDialog(1);
                    return;
                case 1:
                default:
                    return;
                case 2:
                    Contract_siltActivity.this.showDialog(3);
                    return;
            }
        }
    };
    private DataBaseFactory db;
    private DBHelper dbHelper;
    public String end_time = StringEx.Empty;
    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Contract_siltActivity.this.mYear = year;
            Contract_siltActivity.this.mMonth = monthOfYear;
            Contract_siltActivity.this.mDay = dayOfMonth;
            Contract_siltActivity.this.updateDateDisplay();
        }
    };
    private DatePickerDialog.OnDateSetListener mDateSetListeners = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Contract_siltActivity.this.mYear = year;
            Contract_siltActivity.this.mMonth = monthOfYear;
            Contract_siltActivity.this.mDay = dayOfMonth;
            Contract_siltActivity.this.updateTimeDisplay();
        }
    };
    /* access modifiers changed from: private */
    public int mDay;
    /* access modifiers changed from: private */
    public int mMonth;
    /* access modifiers changed from: private */
    public int mYear;
    private RadioGroup radioderGroup;
    final String[] sellerName = {"所有卖家", "宝钢新事业", "宝山公司", "上海不锈"};
    public String sellername = StringEx.Empty;
    public TextView textContractStatus;
    public TextView textSellerName;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ExitApplication.getInstance().addActivity(this);
        requestWindowFeature(1);
        setContentView((int) R.layout.xhjy_contract_sift);
        this.dbHelper = DataBaseFactory.getInstance(this);
        this.dbHelper.insertOperation("钢材交易", "买家中心", "合同筛选页面");
        Calendar c = Calendar.getInstance();
        this.mYear = c.get(1);
        this.mMonth = c.get(2);
        this.mDay = c.get(5);
        System.out.println("mYear:" + this.mYear);
        System.out.println("mMonth:" + this.mMonth);
        System.out.println("mDay:" + this.mDay);
        SmallNumUtil.GWCNum(this, (TextView) findViewById(R.id.gwc_dhqrnum));
        initializeViews();
    }

    public void contarct_siftt_backButtonAction(View v) {
        ExitApplication.getInstance().back(this);
    }

    public void onBackPressed() {
        ExitApplication.getInstance().back(0);
        super.onBackPressed();
    }

    public void contarct_sift_fiterButtonAction(View v) {
        setGlobal();
        ExitApplication.getInstance().startActivity(this, ContractListActivity.class);
    }

    public void setGlobal() {
        this.contract_num = this.contractnum.getText().toString();
        this.begin_time = this.ET_beginTime.getText().toString();
        this.end_time = this.ET_endTime.getText().toString();
        ObjectStores.getInst().putObject("sellername", this.sellername);
        ObjectStores.getInst().putObject("contract_stats", this.contract_stats);
        ObjectStores.getInst().putObject("contract_status", this.contract_status);
        ObjectStores.getInst().putObject("contract_num", this.contract_num);
        ObjectStores.getInst().putObject("begin_time", this.begin_time);
        ObjectStores.getInst().putObject("end_time", this.end_time);
    }

    private void initializeViews() {
        this.contractnum = (EditText) findViewById(R.id.contarct_sift_contractnum);
        this.textSellerName = (TextView) findViewById(R.id.contarct_sift_allsellers);
        this.textContractStatus = (TextView) findViewById(R.id.contarct_sift_allcontracts);
        this.sellername = (String) ObjectStores.getInst().getObject("sellername");
        this.contract_status = (String) ObjectStores.getInst().getObject("contract_status");
        if (this.sellername == null || this.sellername.equals(StringEx.Empty)) {
            this.sellername = "所有卖家";
        }
        if (this.contract_status == null || this.contract_status.equals(StringEx.Empty)) {
            this.contract_status = "所有状态";
        }
        this.textSellerName.setText(this.sellername);
        this.textContractStatus.setText(this.contract_status);
        ((RadioButton) findViewById(R.id.radio_contract3)).setChecked(true);
        this.radioderGroup = (RadioGroup) findViewById(R.id.xhjy_index_RGroup);
        this.radioderGroup.setOnCheckedChangeListener(this);
        this.ET_beginTime = (EditText) findViewById(R.id.begintime_contract_sift);
        this.ET_endTime = (EditText) findViewById(R.id.endtime_contract_sift);
        int month = this.mMonth + 1;
        if (month < 10) {
            this.ET_beginTime.setText(String.valueOf(this.mYear) + "-0" + month + "-01");
            if (this.mDay < 10) {
                this.ET_endTime.setText(String.valueOf(this.mYear) + "-0" + month + "-0" + this.mDay);
            } else {
                this.ET_endTime.setText(String.valueOf(this.mYear) + "-0" + month + "-" + this.mDay);
            }
        } else {
            this.ET_beginTime.setText(String.valueOf(this.mYear) + "-" + month + "-01");
            this.ET_endTime.setText(String.valueOf(this.mYear) + "-" + month + "-" + this.mDay);
        }
        this.ET_beginTime.setInputType(0);
        this.ET_endTime.setInputType(0);
        this.ET_beginTime.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Message msg = new Message();
                if (Contract_siltActivity.this.ET_beginTime.equals((EditText) v)) {
                    msg.what = 0;
                }
                Contract_siltActivity.this.dateandtimeHandler.sendMessage(msg);
            }
        });
        this.ET_endTime.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Message msg = new Message();
                if (Contract_siltActivity.this.ET_endTime.equals((EditText) v)) {
                    msg.what = 2;
                }
                Contract_siltActivity.this.dateandtimeHandler.sendMessage(msg);
            }
        });
    }

    public void contract_statusButtonAction(View v) {
        this.dbHelper.insertOperation("钢材交易", "买家中心", "合同_筛选状态页面");
        new AlertDialog.Builder(this).setTitle("请选择合同状态").setItems(this.contractStatus, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (Contract_siltActivity.this.contractStatus[which].equals("所有状态")) {
                    Contract_siltActivity.this.contract_stats = StringEx.Empty;
                    Contract_siltActivity.this.contract_status = "所有状态";
                    Contract_siltActivity.this.textContractStatus.setText(Contract_siltActivity.this.contract_status);
                } else if (Contract_siltActivity.this.contractStatus[which].equals("作废")) {
                    Contract_siltActivity.this.contract_stats = "0";
                    Contract_siltActivity.this.contract_status = "作废";
                    Contract_siltActivity.this.textContractStatus.setText(Contract_siltActivity.this.contract_status);
                } else if (Contract_siltActivity.this.contractStatus[which].equals("待支付")) {
                    Contract_siltActivity.this.contract_stats = "1";
                    Contract_siltActivity.this.contract_status = "待支付";
                    Contract_siltActivity.this.textContractStatus.setText(Contract_siltActivity.this.contract_status);
                } else if (Contract_siltActivity.this.contractStatus[which].equals("生效")) {
                    Contract_siltActivity.this.contract_stats = "2";
                    Contract_siltActivity.this.contract_status = "生效";
                    Contract_siltActivity.this.textContractStatus.setText(Contract_siltActivity.this.contract_status);
                } else if (Contract_siltActivity.this.contractStatus[which].equals("已付款")) {
                    Contract_siltActivity.this.contract_stats = "3";
                    Contract_siltActivity.this.contract_status = "已付款";
                    Contract_siltActivity.this.textContractStatus.setText(Contract_siltActivity.this.contract_status);
                } else if (Contract_siltActivity.this.contractStatus[which].equals("已提货")) {
                    Contract_siltActivity.this.contract_stats = "4";
                    Contract_siltActivity.this.contract_status = "已提货";
                    Contract_siltActivity.this.textContractStatus.setText(Contract_siltActivity.this.contract_status);
                } else if (Contract_siltActivity.this.contractStatus[which].equals("已完成")) {
                    Contract_siltActivity.this.contract_stats = "6";
                    Contract_siltActivity.this.contract_status = "已完成";
                    Contract_siltActivity.this.textContractStatus.setText(Contract_siltActivity.this.contract_status);
                } else if (Contract_siltActivity.this.contractStatus[which].equals("违约结算中")) {
                    Contract_siltActivity.this.contract_stats = "D";
                    Contract_siltActivity.this.contract_status = "违约结算中";
                    Contract_siltActivity.this.textContractStatus.setText(Contract_siltActivity.this.contract_status);
                } else if (Contract_siltActivity.this.contractStatus[which].equals("违约终止")) {
                    Contract_siltActivity.this.contract_stats = "F";
                    Contract_siltActivity.this.contract_status = "违约终止";
                    Contract_siltActivity.this.textContractStatus.setText(Contract_siltActivity.this.contract_status);
                } else if (Contract_siltActivity.this.contractStatus[which].equals("待收款")) {
                    Contract_siltActivity.this.contract_stats = "S";
                    Contract_siltActivity.this.contract_status = "待收款";
                    Contract_siltActivity.this.textContractStatus.setText(Contract_siltActivity.this.contract_status);
                } else if (Contract_siltActivity.this.contractStatus[which].equals("待仓库过户确认")) {
                    Contract_siltActivity.this.contract_stats = "13";
                    Contract_siltActivity.this.contract_status = "待仓库过户确认";
                    Contract_siltActivity.this.textContractStatus.setText(Contract_siltActivity.this.contract_status);
                } else if (Contract_siltActivity.this.contractStatus[which].equals("待做市商确认")) {
                    Contract_siltActivity.this.contract_stats = "14";
                    Contract_siltActivity.this.contract_status = "待做市商确认";
                    Contract_siltActivity.this.textContractStatus.setText(Contract_siltActivity.this.contract_status);
                }
            }
        }).create().show();
    }

    public void sellerButtonAction(View v) {
        this.dbHelper.insertOperation("钢材交易", "买家中心", "合同_筛选卖家页面");
        new AlertDialog.Builder(this).setTitle("请选择卖家").setItems(this.sellerName, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (Contract_siltActivity.this.sellerName[which].equals("所有卖家")) {
                    Contract_siltActivity.this.sellername = "所有卖家";
                    Contract_siltActivity.this.textSellerName.setText("所有卖家");
                } else if (Contract_siltActivity.this.sellerName[which].equals("宝钢新事业")) {
                    Contract_siltActivity.this.sellername = "宝钢新事业";
                    Contract_siltActivity.this.textSellerName.setText("宝钢新事业");
                } else if (Contract_siltActivity.this.sellerName[which].equals("宝山公司")) {
                    Contract_siltActivity.this.sellername = "宝山公司";
                    Contract_siltActivity.this.textSellerName.setText("宝山公司");
                } else if (Contract_siltActivity.this.sellerName[which].equals("上海不锈")) {
                    Contract_siltActivity.this.sellername = "上海不锈";
                    Contract_siltActivity.this.textSellerName.setText("上海不锈");
                }
            }
        }).create().show();
    }

    /* access modifiers changed from: private */
    public void updateDateDisplay() {
        this.ET_beginTime.setText(this.mYear + "-" + (this.mMonth + 1 < 10 ? "0" + (this.mMonth + 1) : Integer.valueOf(this.mMonth + 1)) + "-" + (this.mDay < 10 ? "0" + this.mDay : Integer.valueOf(this.mDay)));
    }

    /* access modifiers changed from: private */
    public void updateTimeDisplay() {
        this.ET_endTime.setText(this.mYear + "-" + (this.mMonth + 1 < 10 ? "0" + (this.mMonth + 1) : Integer.valueOf(this.mMonth + 1)) + "-" + (this.mDay < 10 ? "0" + this.mDay : Integer.valueOf(this.mDay)));
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return new DatePickerDialog(this, this.mDateSetListener, this.mYear, this.mMonth, this.mDay);
            case 2:
            default:
                return null;
            case 3:
                return new DatePickerDialog(this, this.mDateSetListeners, this.mYear, this.mMonth, this.mDay);
        }
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int id, Dialog dialog) {
        switch (id) {
            case 1:
                ((DatePickerDialog) dialog).updateDate(this.mYear, this.mMonth, this.mDay);
                return;
            case 2:
            default:
                return;
            case 3:
                ((DatePickerDialog) dialog).updateDate(this.mYear, this.mMonth, this.mDay);
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        System.out.println("finish当前页面");
        super.onDestroy();
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.radio_search3:
                ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
                return;
            case R.id.radio_shopcar3:
                ((RadioButton) findViewById(R.id.radio_shopcar3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ShopActivity.class);
                return;
            case R.id.radio_home3:
                ((RadioButton) findViewById(R.id.radio_home3)).setChecked(true);
                HashMap hasmap = new HashMap();
                hasmap.put("index_more", "1");
                ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class, hasmap);
                return;
            case R.id.radio_contract3:
                ((RadioButton) findViewById(R.id.radio_contract3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ContractListActivity.class);
                return;
            case R.id.radio_notice3:
                ((RadioButton) findViewById(R.id.radio_notice3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, Xhjy_more.class);
                return;
            default:
                return;
        }
    }
}
