package com.baosteelOnline.xhjy;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.andframework.common.ObjectStores;
import com.andframework.ui.CustomListView;
import com.andframework.ui.CustomMessageShow;
import com.baosteelOnline.R;
import com.baosteelOnline.common.DataBaseFactory;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.data.SearchBusi;
import com.baosteelOnline.data_parse.SearchParse;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.model.SmallNumUtil;
import com.baosteelOnline.sql.DBHelper;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import java.util.ArrayList;
import java.util.HashMap;

public class SearchActivity extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    public ArrayList<SearchRow> LisItems = new ArrayList<>();
    public String allcount = StringEx.Empty;
    public String allpage = StringEx.Empty;
    public String cd = StringEx.Empty;
    public int countNo = 0;
    public String cz = StringEx.Empty;
    private DataBaseFactory db;
    private DBHelper dbHelper;
    public String dj = StringEx.Empty;
    public String djg1 = StringEx.Empty;
    public String djg2 = StringEx.Empty;
    View footview;
    public String gpls = StringEx.Empty;
    public String hd1 = StringEx.Empty;
    public String hd2 = StringEx.Empty;
    private JQUICallBack httCallBack = new JQUICallBack() {
        public void callBack(ResultData result) {
            SearchActivity.this.progressDialog.dismiss();
            try {
                System.out.println("result=StringData12=>" + result.getStringData());
                SearchActivity.this.updateNoticeList(SearchParse.parse(result.getStringData()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    public String kd1 = StringEx.Empty;
    public String kd2 = StringEx.Empty;
    public String key = StringEx.Empty;
    public String limit = "10";
    public String mj = StringEx.Empty;
    public String offset = "1";
    public String offsets = StringEx.Empty;
    public String orderGg = StringEx.Empty;
    public String orderJg = StringEx.Empty;
    public String pm = StringEx.Empty;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    private RadioGroup radioderGroup;
    CustomListView rowslist;
    public SearchBusi searchBusi;
    public SearchParse searchParse;
    TextView textcont;
    private View view;
    private View view2;
    public String who = StringEx.Empty;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ExitApplication.getInstance().addActivity(this);
        this.dbHelper = DataBaseFactory.getInstance(this);
        this.dbHelper.insertOperation("钢材交易", "搜索", "搜索资源列表");
        requestWindowFeature(1);
        setContentView((int) R.layout.xhjy_search);
        this.rowslist = (CustomListView) findViewById(R.id.rowslist);
        this.footview = View.inflate(this, R.layout.xhjy_list_bottom_button, null);
        this.textcont = (TextView) this.footview.findViewById(R.id.xhjy_list_bottom_text);
        ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
        this.radioderGroup = (RadioGroup) findViewById(R.id.xhjy_index_RGroup);
        this.radioderGroup.setOnCheckedChangeListener(this);
        SmallNumUtil.GWCNum(this, (TextView) findViewById(R.id.gwc_dhqrnum));
        initData();
        testBusi();
        this.rowslist.setClickable(true);
        this.rowslist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View v, int arg2, long arg3) {
                if (Integer.valueOf(SearchActivity.this.allpage).intValue() != Integer.valueOf(SearchActivity.this.offsets).intValue() || arg2 - 1 != SearchActivity.this.LisItems.size()) {
                    if (arg2 - 1 == SearchActivity.this.LisItems.size()) {
                        SearchActivity.this.offset = String.valueOf(Integer.valueOf(SearchActivity.this.offset).intValue() + 1);
                        SearchActivity.this.searchBusi = new SearchBusi(SearchActivity.this);
                        SearchActivity.this.searchBusi.offset = SearchActivity.this.offset;
                        SearchActivity.this.testBusi();
                        return;
                    }
                    int num = arg2 - 1;
                    String productname2 = SearchActivity.this.LisItems.get(num).productname.toString();
                    String company2 = SearchActivity.this.LisItems.get(num).company.toString();
                    String mertial2 = SearchActivity.this.LisItems.get(num).mertial.toString();
                    String format2 = SearchActivity.this.LisItems.get(num).format.toString();
                    String price2 = SearchActivity.this.LisItems.get(num).price.toString();
                    String desunit2 = SearchActivity.this.LisItems.get(num).desunit.toString();
                    String productaddr2 = SearchActivity.this.LisItems.get(num).productaddr.toString();
                    String gpls = SearchActivity.this.LisItems.get(num).gpls.toString();
                    System.out.println("productname2:" + productname2);
                    System.out.println("company2:" + company2);
                    System.out.println("mertial2:" + mertial2);
                    System.out.println("format2:" + format2);
                    System.out.println("price2:" + price2);
                    System.out.println("desunit2:" + desunit2);
                    System.out.println("productaddr2:" + productaddr2);
                    System.out.println("gpls:" + gpls);
                    HashMap hasmap = new HashMap();
                    hasmap.put("productname2", productname2);
                    hasmap.put("company2", company2);
                    hasmap.put("mertial2", mertial2);
                    hasmap.put("format2", format2);
                    hasmap.put("price2", price2);
                    hasmap.put("desunit2", desunit2);
                    hasmap.put("productaddr2", productaddr2);
                    hasmap.put("gpls", gpls);
                    ExitApplication.getInstance().startActivity(SearchActivity.this, SearchItemInfoActivity.class, hasmap);
                }
            }
        });
    }

    private void initData() {
        if (getIntent().getStringExtra("xhjy_marks") == null || !getIntent().getStringExtra("xhjy_marks").equals("1")) {
            this.pm = (String) ObjectStores.getInst().getObject("pm");
            this.mj = (String) ObjectStores.getInst().getObject("sellername");
        } else {
            this.pm = getIntent().getStringExtra("pm");
            this.mj = getIntent().getStringExtra("mj");
        }
        this.cz = getIntent().getStringExtra("cz");
        this.cd = getIntent().getStringExtra("cd");
        this.dj = getIntent().getStringExtra("dj");
        this.gpls = getIntent().getStringExtra("gpls");
        this.key = getIntent().getStringExtra("key");
        this.orderJg = (String) ObjectStores.getInst().getObject("orderJg");
        this.orderGg = (String) ObjectStores.getInst().getObject("orderGg");
        this.djg1 = (String) ObjectStores.getInst().getObject("pricemin");
        this.djg2 = (String) ObjectStores.getInst().getObject("pricemax");
        this.hd1 = (String) ObjectStores.getInst().getObject("thickmin");
        this.hd2 = (String) ObjectStores.getInst().getObject("thickmax");
        this.kd1 = (String) ObjectStores.getInst().getObject("widemin");
        this.kd2 = (String) ObjectStores.getInst().getObject("widemax");
        if (this.pm == null) {
            this.pm = StringEx.Empty;
        }
        if (this.mj == null) {
            this.mj = StringEx.Empty;
        }
        if (this.cz == null) {
            this.cz = StringEx.Empty;
        }
        if (this.cd == null) {
            this.cd = StringEx.Empty;
        }
        if (this.dj == null) {
            this.dj = StringEx.Empty;
        }
        if (this.gpls == null) {
            this.gpls = StringEx.Empty;
        }
        if (this.orderJg == null) {
            this.orderJg = "asc";
        }
        if (this.orderGg == null) {
            this.orderGg = StringEx.Empty;
        }
        if (this.key == null) {
            this.key = StringEx.Empty;
        }
        if (this.djg1 == null) {
            this.djg1 = StringEx.Empty;
        }
        if (this.djg2 == null) {
            this.djg2 = StringEx.Empty;
        }
        if (this.hd1 == null) {
            this.hd1 = StringEx.Empty;
        }
        if (this.hd2 == null) {
            this.hd2 = StringEx.Empty;
        }
        if (this.kd1 == null) {
            this.kd1 = StringEx.Empty;
        }
        if (this.kd2 == null) {
            this.kd2 = StringEx.Empty;
        }
    }

    public void testBusi() {
        this.searchBusi = new SearchBusi(this);
        this.searchBusi.pm = this.pm;
        this.searchBusi.mj = this.mj;
        this.searchBusi.cz = this.cz;
        this.searchBusi.cd = this.cd;
        this.searchBusi.dj = this.dj;
        this.searchBusi.gpls = this.gpls;
        this.searchBusi.orderJg = this.orderJg;
        this.searchBusi.orderGg = this.orderGg;
        this.searchBusi.key = this.key;
        this.searchBusi.djg1 = this.djg1;
        this.searchBusi.djg2 = this.djg2;
        this.searchBusi.hd1 = this.hd1;
        this.searchBusi.hd2 = this.hd2;
        this.searchBusi.kd1 = this.kd1;
        this.searchBusi.kd2 = this.kd2;
        this.searchBusi.offset = this.offset;
        this.searchBusi.limit = this.limit;
        this.searchBusi.setHttpCallBack(this.httCallBack);
        this.searchBusi.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    public void xhjy_search_filterButtonAction(View v) {
        ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
    }

    public void xhjy_search_backButtonAction(View v) {
        ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
    }

    public void onBackPressed() {
        ExitApplication.getInstance().back(0);
        super.onBackPressed();
    }

    public void priceRadioAction(View v) {
        this.rowslist.removeAllView();
        this.LisItems.removeAll(this.LisItems);
        this.rowslist.removeFooterView(this.footview);
        this.orderJg = "asc";
        this.orderGg = StringEx.Empty;
        this.offset = "1";
        this.countNo = 0;
        testBusi();
    }

    public void thicknesstohRadioAction(View v) {
        this.rowslist.removeAllView();
        this.rowslist.removeFooterView(this.footview);
        this.LisItems.removeAll(this.LisItems);
        this.orderGg = "asc";
        this.orderJg = StringEx.Empty;
        this.offset = "1";
        this.countNo = 0;
        testBusi();
    }

    public void thicknesstolRadioAction(View v) {
        this.rowslist.removeAllView();
        this.LisItems.removeAll(this.LisItems);
        this.rowslist.removeFooterView(this.footview);
        this.orderGg = "desc";
        this.orderJg = StringEx.Empty;
        this.offset = "1";
        this.countNo = 0;
        testBusi();
    }

    /* access modifiers changed from: package-private */
    public void updateNoticeList(SearchParse searchParse2) {
        if (SearchParse.commonData == null || SearchParse.commonData.blocks == null || SearchParse.commonData.blocks.r0 == null || SearchParse.commonData.blocks.r0.mar == null || SearchParse.commonData.blocks.r0.mar.rows == null) {
            CustomMessageShow.getInst().showLongToast(this, "服务器无数据返回!");
        } else if (SearchParse.commonData.blocks.r0.mar.rows.rows.size() == 0 || SearchParse.commonData.blocks.r0.mar.rows.rows == null) {
            new AlertDialog.Builder(this).setMessage("对不起，暂时没有搜索到您要的资源。请更换条件，或者稍后再试试。").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    ExitApplication.getInstance().back(0);
                }
            }).show().setCanceledOnTouchOutside(false);
        } else if (SearchParse.commonData != null && SearchParse.commonData.blocks != null && SearchParse.commonData.blocks.r0 != null && SearchParse.commonData.blocks.r0.mar != null && SearchParse.commonData.blocks.r0.mar.rows != null && SearchParse.commonData.blocks.r0.mar.rows.rows != null) {
            this.allpage = (String) SearchParse.commonData.blocks.i0.mar.rows.rows.get(0).get(0);
            this.offsets = (String) SearchParse.commonData.blocks.i0.mar.rows.rows.get(0).get(1);
            this.allcount = (String) SearchParse.commonData.blocks.i0.mar.rows.rows.get(0).get(2);
            int rowsCount = SearchParse.commonData.blocks.r0.mar.rows.rows.size();
            for (int i = 0; i < rowsCount; i++) {
                ArrayList<String> rowdata = SearchParse.commonData.blocks.r0.mar.rows.rows.get(i);
                SearchRow sr = new SearchRow();
                if (rowdata != null) {
                    for (int k = 0; k < rowdata.size(); k++) {
                        if (k == 0) {
                            sr.number = String.valueOf(((Integer.valueOf(this.offset).intValue() - 1) * 10) + i + 1) + ".";
                            sr.company = "卖家:" + ((String) rowdata.get(k));
                        }
                        if (k == 1) {
                            sr.productname = (String) rowdata.get(k);
                        }
                        if (k == 2) {
                            sr.mertial = "材质:" + ((String) rowdata.get(k));
                        }
                        if (k == 4) {
                            sr.productaddr = "产地:" + ((String) rowdata.get(k));
                        }
                        if (k == 5) {
                            sr.price = (String) rowdata.get(k);
                        }
                        if (k == 6) {
                            sr.desunit = String.valueOf((String) rowdata.get(k + 1)) + "件 / " + ((String) rowdata.get(k)) + "吨";
                        }
                        if (k == 8) {
                            sr.format = "规格:" + ((String) rowdata.get(k));
                        }
                        if (k == 9) {
                            sr.gpls = (String) rowdata.get(k);
                        }
                    }
                    this.countNo++;
                    this.LisItems.add(sr);
                    this.view2 = buildRowView(sr);
                    this.rowslist.addViewToLast(this.view2);
                }
            }
            if (this.countNo <= 10) {
                if (this.countNo == Integer.valueOf(this.allcount).intValue()) {
                    this.textcont.setText("这是最后一页");
                } else {
                    this.textcont.setText("下10条...");
                }
                this.rowslist.addFooterView(this.footview);
            } else if (this.countNo == Integer.valueOf(this.allcount).intValue()) {
                this.textcont.setText("这是最后一页");
            } else {
                this.textcont.setText("下10条...");
            }
            this.rowslist.onRefreshComplete();
        }
    }

    private View buildRowView(SearchRow sr) {
        this.view = View.inflate(this, R.layout.xhjy_search_row, null);
        ((TextView) this.view.findViewById(R.id.TV_number)).setText(sr.number);
        ((TextView) this.view.findViewById(R.id.TV_productname)).setText(sr.productname);
        ((TextView) this.view.findViewById(R.id.TV_company)).setText(sr.company);
        ((TextView) this.view.findViewById(R.id.TV_mertial)).setText(sr.mertial);
        ((TextView) this.view.findViewById(R.id.TV_format)).setText(sr.format);
        ((TextView) this.view.findViewById(R.id.TV_price)).setText(sr.price);
        ((TextView) this.view.findViewById(R.id.TV_desunit)).setText(sr.desunit);
        ((TextView) this.view.findViewById(R.id.TV_productaddr)).setText(sr.productaddr);
        return this.view;
    }

    class SearchRow {
        public String company;
        public String desunit;
        public String format;
        public String gpls;
        public String mertial;
        public String number;
        public String price;
        public String productaddr;
        public String productname;

        SearchRow() {
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.rowslist = null;
        this.footview = null;
        this.textcont = null;
        this.view = null;
        this.view2 = null;
        this.who = null;
        this.LisItems = null;
        super.onDestroy();
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.radio_search3:
                ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ChooseActivity.class);
                return;
            case R.id.radio_shopcar3:
                ((RadioButton) findViewById(R.id.radio_shopcar3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ShopActivity.class);
                return;
            case R.id.radio_home3:
                ((RadioButton) findViewById(R.id.radio_home3)).setChecked(true);
                HashMap hasmap = new HashMap();
                hasmap.put("index_more", "1");
                ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class, hasmap);
                return;
            case R.id.radio_contract3:
                ((RadioButton) findViewById(R.id.radio_contract3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, ContractListActivity.class);
                return;
            case R.id.radio_notice3:
                ((RadioButton) findViewById(R.id.radio_notice3)).setChecked(true);
                ExitApplication.getInstance().startActivity(this, Xhjy_more.class);
                return;
            default:
                return;
        }
    }
}
