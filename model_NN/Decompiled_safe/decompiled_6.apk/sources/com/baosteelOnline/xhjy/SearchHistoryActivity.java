package com.baosteelOnline.xhjy;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.andframework.common.ObjectStores;
import com.andframework.ui.CustomListView;
import com.andframework.ui.CustomMessageShow;
import com.baosteelOnline.R;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.data.HistoryCollectBusi;
import com.baosteelOnline.data.HistoryDeleteBusi;
import com.baosteelOnline.data.SearchHistoryBusi;
import com.baosteelOnline.data_parse.SearchHistoryParse;
import com.baosteelOnline.data_parse.SearchParse;
import com.baosteelOnline.dl.Login_indexActivity;
import com.baosteelOnline.main.ExitApplication;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

public class SearchHistoryActivity extends JQActivity implements RadioGroup.OnCheckedChangeListener, View.OnClickListener {
    ArrayList<String> datalist;
    private JQUICallBack httpCallBack1 = new JQUICallBack() {
        public void callBack(ResultData result) {
            SearchHistoryActivity.this.progressDialog.dismiss();
            SearchHistoryActivity.this.updateNoticeList(SearchHistoryParse.parse(result.getStringData()));
        }
    };
    private JQUICallBack httpCallBack2 = new JQUICallBack() {
        public void callBack(ResultData result) {
            SearchHistoryActivity.this.progressDialog.dismiss();
            SearchHistoryActivity.this.updateNotice(SearchParse.parse(result.getStringData()));
        }
    };
    private JQUICallBack httpCallBack3 = new JQUICallBack() {
        public void callBack(ResultData result) {
            SearchHistoryActivity.this.progressDialog.dismiss();
            SearchHistoryActivity.this.updateNotice(SearchParse.parse(result.getStringData()));
        }
    };
    private String info = StringEx.Empty;
    private String lengthmax = StringEx.Empty;
    private String lengthmin = StringEx.Empty;
    private String limit = "11";
    private LinearLayout lin2;
    private LinearLayout linLayout;
    List<String> list = new ArrayList();
    List<String> list2 = new ArrayList();
    private String locArea = StringEx.Empty;
    private String orderby = StringEx.Empty;
    private String pages = "0";
    private String pricemax = StringEx.Empty;
    private String pricemin = StringEx.Empty;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    private String qualityGrade = StringEx.Empty;
    CustomListView rowslist;
    private String sellername = StringEx.Empty;
    private String shopsign = StringEx.Empty;
    private String thickmax = StringEx.Empty;
    private String thickmin = StringEx.Empty;
    private String type4 = StringEx.Empty;
    private View view;
    private String widemax = StringEx.Empty;
    private String widemin = StringEx.Empty;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        ExitApplication.getInstance().addActivity(this);
        setContentView((int) R.layout.xhjy_searchhistory);
        this.rowslist = (CustomListView) findViewById(R.id.rowslist_history);
        ((RadioButton) findViewById(R.id.radio_search3)).setChecked(true);
        this.rowslist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View v, int arg2, long arg3) {
            }
        });
        testBusi();
    }

    public void testBusi() {
        SearchHistoryBusi shBusi = new SearchHistoryBusi(this);
        shBusi.setHttpCallBack(this.httpCallBack1);
        shBusi.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    /* access modifiers changed from: package-private */
    public void updateNotice(SearchParse shParse) {
        CustomMessageShow.getInst().cancleProgressDialog();
        this.rowslist.removeAllView();
        this.list.removeAll(this.list);
        this.list2.removeAll(this.list2);
        startActivity(new Intent(this, SearchHistoryActivity.class));
        finish();
    }

    /* access modifiers changed from: package-private */
    public void updateNoticeList(SearchHistoryParse shParse) {
        if (SearchHistoryParse.commonData == null || SearchHistoryParse.commonData.blocks == null || SearchHistoryParse.commonData.blocks.r0 == null || SearchHistoryParse.commonData.blocks.r0.mar == null || SearchHistoryParse.commonData.blocks.r0.mar.rows == null) {
            CustomMessageShow.getInst().showLongToast(this, "服务器无数据返回！");
            return;
        }
        if (SearchHistoryParse.commonData.blocks.r0.mar.rows.rows.size() == 0) {
            new AlertDialog.Builder(this).setMessage("对不起，没有数据!").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    Intent intent = new Intent();
                    intent.setClass(SearchHistoryActivity.this.getApplicationContext(), Xhjy_indexActivity.class);
                    SearchHistoryActivity.this.startActivity(intent);
                    SearchHistoryActivity.this.finish();
                }
            }).show();
        } else if (!(SearchHistoryParse.commonData == null || SearchHistoryParse.commonData.blocks == null || SearchHistoryParse.commonData.blocks.r0 == null || SearchHistoryParse.commonData.blocks.r0.mar == null || SearchHistoryParse.commonData.blocks.r0.mar.rows == null || SearchHistoryParse.commonData.blocks.r0.mar.rows.rows == null)) {
            for (int i = 0; i < SearchHistoryParse.commonData.blocks.r0.mar.rows.rows.size(); i++) {
                ArrayList<String> rowdata = SearchHistoryParse.commonData.blocks.r0.mar.rows.rows.get(i);
                SearchRow sr = new SearchRow();
                String data = String.valueOf(StringEx.Empty) + "{";
                if (rowdata != null) {
                    for (int k = 0; k < rowdata.size(); k++) {
                        if (k == 0) {
                            sr.number = String.valueOf(String.valueOf(i + 1)) + ".";
                            sr.type4 = (String) rowdata.get(k);
                            if (sr.type4 == "null" || sr.type4.equals("\"null\"")) {
                                sr.type4 = StringEx.Empty;
                            }
                            data = String.valueOf(data) + "'type4':'" + sr.type4 + "',";
                        }
                        if (k == 1) {
                            sr.sellername = (String) rowdata.get(k);
                            if (sr.sellername == "null" || sr.sellername.equals("\"null\"")) {
                                sr.sellername = StringEx.Empty;
                            }
                            data = String.valueOf(data) + "'sellername':'" + sr.sellername + "',";
                        } else if (k == 2) {
                            sr.minPrice = (String) rowdata.get(k);
                            if (sr.minPrice == "null" || sr.minPrice.equals("\"null\"")) {
                                sr.minPrice = StringEx.Empty;
                            }
                            data = String.valueOf(data) + "'minPrice':'" + sr.minPrice + "',";
                        } else if (k == 3) {
                            sr.maxPrice = (String) rowdata.get(k);
                            if (sr.maxPrice == "null" || sr.maxPrice.equals("\"null\"")) {
                                sr.maxPrice = StringEx.Empty;
                            }
                            data = String.valueOf(data) + "'maxPrice':'" + sr.maxPrice + "',";
                        } else if (k == 4) {
                            sr.minThick = (String) rowdata.get(k);
                            if (sr.minThick == "null" || sr.minThick.equals("\"null\"")) {
                                sr.minThick = StringEx.Empty;
                            }
                            data = String.valueOf(data) + "'minThick':'" + sr.minThick + "',";
                        } else if (k == 5) {
                            sr.maxThick = (String) rowdata.get(k);
                            if (sr.maxThick == "null" || sr.maxThick.equals("\"null\"")) {
                                sr.maxThick = StringEx.Empty;
                            }
                            data = String.valueOf(data) + "'maxThick':'" + sr.maxThick + "',";
                        } else if (k == 6) {
                            sr.shopsign = (String) rowdata.get(k);
                            if (sr.shopsign == "null" || sr.shopsign.equals("\"null\"")) {
                                sr.shopsign = StringEx.Empty;
                            }
                            data = String.valueOf(data) + "'shopsign':'" + sr.shopsign + "',";
                        } else if (k == 7) {
                            sr.orderBy = (String) rowdata.get(k);
                            if (sr.orderBy == "null" || sr.orderBy.equals("\"null\"")) {
                                sr.orderBy = StringEx.Empty;
                                data = String.valueOf(data) + "'orderBy':'" + sr.orderBy + "',";
                            } else if ("t.price asc".equals(sr.orderBy) || "price asc".equals(sr.orderBy) || "T.SPEC1 asc".equals(sr.orderBy) || "to_number(T.SPEC1) asc".equals(sr.orderBy)) {
                                data = String.valueOf(data) + "'orderBy':'" + sr.orderBy + "',";
                                sr.orderBy = "价格从低到高";
                            } else if ("t.price desc".equals(sr.orderBy) || "price desc".equals(sr.orderBy) || "T.SPEC1 desc".equals(sr.orderBy) || "to_number(T.SPEC1) desc".equals(sr.orderBy)) {
                                data = String.valueOf(data) + "'orderBy':'" + sr.orderBy + "',";
                                sr.orderBy = "价格从高到低";
                            }
                        } else if (k == 8) {
                            sr.flag = "flag" + ((String) rowdata.get(k));
                            if (sr.flag == "null") {
                                sr.flag = "flag0";
                            }
                            data = String.valueOf(data) + "'flag':'" + sr.flag + "',";
                        } else if (k == 9) {
                            sr.logid = (String) rowdata.get(k);
                            if (sr.logid == "null") {
                                sr.logid = StringEx.Empty;
                            }
                            data = String.valueOf(data) + "'logid':'" + sr.logid + "'";
                        }
                    }
                    String data2 = String.valueOf(data) + "}";
                    sr.alldata = StringEx.Empty;
                    if (!StringEx.Empty.equals(sr.sellername)) {
                        sr.alldata = String.valueOf(sr.alldata) + sr.sellername + "+";
                    }
                    if (!StringEx.Empty.equals(sr.type4)) {
                        sr.alldata = String.valueOf(sr.alldata) + sr.type4 + "+";
                    }
                    if (!StringEx.Empty.equals(sr.minPrice)) {
                        sr.alldata = String.valueOf(sr.alldata) + sr.minPrice + "元";
                    }
                    if (!StringEx.Empty.equals(sr.maxPrice)) {
                        sr.alldata = String.valueOf(sr.alldata) + "到" + sr.maxPrice + "+";
                    }
                    if (!StringEx.Empty.equals(sr.shopsign)) {
                        sr.alldata = String.valueOf(sr.alldata) + sr.shopsign + "+";
                    }
                    if (!StringEx.Empty.equals(sr.minThick)) {
                        sr.alldata = String.valueOf(sr.alldata) + "厚度" + sr.minThick + "到";
                    }
                    if (!StringEx.Empty.equals(sr.maxThick)) {
                        sr.alldata = String.valueOf(sr.alldata) + sr.maxThick + "+";
                    }
                    if (!StringEx.Empty.equals(sr.orderBy)) {
                        sr.alldata = String.valueOf(sr.alldata) + sr.orderBy + "+";
                    }
                    this.list.add(sr.alldata);
                    this.list2.add(data2);
                    rowdata.retainAll(rowdata);
                }
            }
        }
        for (int i2 = 0; i2 < this.list.size(); i2++) {
            String str1 = this.list.get(i2);
            int j = i2 + 1;
            while (j < this.list.size()) {
                if (str1.equals(this.list.get(j))) {
                    this.list.remove(j);
                    this.list2.remove(j);
                    j--;
                }
                j++;
            }
        }
        showView();
        setGlobal();
    }

    /* access modifiers changed from: package-private */
    public void showView() {
        int len = this.list.size();
        int j = 1;
        for (int i = 0; i < len; i++) {
            this.rowslist.addViewToLast(buildRowView2(this.list.get(i), j, this.list2.get(i)));
            j++;
        }
        this.rowslist.onRefreshComplete();
    }

    private View buildRowView2(String data_show, int num, String data_intent) {
        this.view = View.inflate(this, R.layout.xhjy_searchhistory_row, null);
        this.linLayout = (LinearLayout) this.view.findViewById(R.id.searchhistory_LLout01);
        this.linLayout.setTag(data_intent);
        this.linLayout.setOnClickListener(this);
        TextView TV_number = (TextView) this.view.findViewById(R.id.TV_number_xhjy_list);
        TextView recode = (TextView) this.view.findViewById(R.id.TV_search_recode_xhjy_list);
        ImageView shoucan = (ImageView) this.view.findViewById(R.id.IB_shoucang_xhjy_hislist1);
        shoucan.setTag(data_intent);
        shoucan.setOnClickListener(this);
        if (data_intent.indexOf("flag1") != -1) {
            shoucan.setBackgroundResource(R.drawable.shoucang);
        }
        if (data_intent.indexOf("flag0") != -1) {
            shoucan.setBackgroundResource(R.drawable.wshoucang);
        }
        TV_number.setText(num + " ");
        recode.setText(data_show.substring(0, data_show.length() - 1));
        return this.view;
    }

    class SearchRow {
        public String alldata;
        public String flag;
        public String lengthmax = StringEx.Empty;
        public String lengthmin = StringEx.Empty;
        public String locArea = StringEx.Empty;
        public String logid;
        public String maxPrice;
        public String maxThick;
        public String minPrice;
        public String minThick;
        public String number;
        public String orderBy;
        public String qualityGrade = StringEx.Empty;
        public String sellername;
        public String shopsign;
        public String type4;
        public String widemax = StringEx.Empty;
        public String widemin = StringEx.Empty;

        SearchRow() {
        }
    }

    public void main_home_buttonaction(View v) {
        startActivity(new Intent(this, Xhjy_indexActivity.class));
        finish();
    }

    public void xhjy_home_buttonaction(View v) {
        startActivity(new Intent(this, Xhjy_indexActivity.class));
        finish();
    }

    public void xhjy_search_buttonaction(View v) {
        startActivity(new Intent(this, SearchHistoryActivity.class));
        finish();
    }

    public void xhjy_shop_buttonaction(View v) {
        startActivity(new Intent(this, ShopActivity.class));
        finish();
    }

    public void xhjy_contract_buttonaction(View v) {
        startActivity(new Intent(this, ContractListActivity.class));
        finish();
    }

    public void xhjy_search_history_ButtonAction(View v) {
        startActivity(new Intent(this, SearchHistoryActivity.class));
        finish();
    }

    public void xhjy_choose_ButtonAction(View v) {
        startActivity(new Intent(this, ChooseActivity.class));
    }

    public void xhjy_history_back_ButtonAction(View v) {
        finish();
    }

    public void onClick(View v) {
        System.out.println("---type:" + v.getClass().getName().toString());
        String type = v.getClass().getName().toString();
        if ("android.widget.ImageView".equals(type)) {
            if (ObjectStores.getInst().getObject("contactid") == null || ObjectStores.getInst().getObject("contactid").toString() == StringEx.Empty) {
                new AlertDialog.Builder(this).setMessage("您还没有登录,请登录！").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        SharedPreferences.Editor editor = SearchHistoryActivity.this.getSharedPreferences("index_mark", 2).edit();
                        editor.putString("index", "SearchHistoryshoucan");
                        editor.commit();
                        Intent intent = new Intent();
                        intent.setClass(SearchHistoryActivity.this.getApplicationContext(), Login_indexActivity.class);
                        SearchHistoryActivity.this.startActivity(intent);
                        SearchHistoryActivity.this.finish();
                    }
                }).show();
                return;
            }
            System.out.println("postdata-=-:" + v.getTag().toString());
            try {
                JSONObject item = new JSONObject(v.getTag().toString());
                try {
                    String type42 = item.getString("type4");
                    String orderby2 = item.getString("orderBy");
                    String shopsign2 = item.getString("shopsign");
                    String thickmin2 = item.getString("minThick");
                    String thickmax2 = item.getString("maxThick");
                    String pricemin2 = item.getString("minPrice");
                    String pricemax2 = item.getString("maxPrice");
                    String sellername2 = item.getString("sellername");
                    String flag = item.getString("flag").substring(4, 5);
                    String logid = item.getString("logid");
                    System.out.println("---flag : " + flag);
                    if ("1".equals(flag)) {
                        HistoryDeleteBusi delBusi = new HistoryDeleteBusi(this);
                        delBusi.logid = logid;
                        delBusi.setHttpCallBack(this.httpCallBack3);
                        delBusi.iExecute();
                        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
                    } else if ("0".equals(flag)) {
                        HistoryCollectBusi hcBusi = new HistoryCollectBusi(this);
                        hcBusi.type4 = type42;
                        hcBusi.orderby = orderby2;
                        hcBusi.shopsign = shopsign2;
                        hcBusi.thickmin = thickmin2;
                        hcBusi.thickmax = thickmax2;
                        hcBusi.pricemin = pricemin2;
                        hcBusi.pricemax = pricemax2;
                        hcBusi.sellername = sellername2;
                        hcBusi.flag = flag;
                        hcBusi.logid = logid;
                        hcBusi.setHttpCallBack(this.httpCallBack2);
                        hcBusi.iExecute();
                        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
                    }
                } catch (Exception e) {
                    JSONObject jSONObject = item;
                }
            } catch (Exception e2) {
            }
        } else if ("android.widget.LinearLayout".equals(type)) {
            System.out.println("postdata-=-:" + v.getTag().toString());
            try {
                JSONObject item2 = new JSONObject(v.getTag().toString());
                try {
                    ObjectStores.getInst().putObject("type4", item2.getString("type4"));
                    ObjectStores.getInst().putObject("orderby", item2.getString("orderBy"));
                    ObjectStores.getInst().putObject("shopsign", item2.getString("shopsign"));
                    ObjectStores.getInst().putObject("thickmin", item2.getString("minThick"));
                    ObjectStores.getInst().putObject("thickmax", item2.getString("maxThick"));
                    ObjectStores.getInst().putObject("pricemin", item2.getString("minPrice"));
                    ObjectStores.getInst().putObject("pricemax", item2.getString("maxPrice"));
                    ObjectStores.getInst().putObject("sellername", item2.getString("sellername"));
                    ObjectStores.getInst().putObject("limit", this.limit);
                    ObjectStores.getInst().putObject("pages", this.pages);
                    ObjectStores.getInst().putObject("searchbody", "History");
                } catch (Exception e3) {
                }
            } catch (Exception e4) {
            }
            Intent in = new Intent(this, SearchActivity.class);
            in.putExtra("who", "searchHistory");
            startActivity(in);
            finish();
        }
    }

    public void setGlobal() {
        ObjectStores.getInst().putObject("type4", this.type4);
        ObjectStores.getInst().putObject("orderby", this.orderby);
        ObjectStores.getInst().putObject("shopsign", this.shopsign);
        ObjectStores.getInst().putObject("thickmin", this.thickmin);
        ObjectStores.getInst().putObject("thickmax", this.thickmax);
        ObjectStores.getInst().putObject("widemin", this.widemin);
        ObjectStores.getInst().putObject("widemax", this.widemax);
        ObjectStores.getInst().putObject("lengthmin", this.lengthmin);
        ObjectStores.getInst().putObject("lengthmax", this.lengthmax);
        ObjectStores.getInst().putObject("pricemin", this.pricemin);
        ObjectStores.getInst().putObject("pricemax", this.pricemax);
        ObjectStores.getInst().putObject("sellername", this.sellername);
        ObjectStores.getInst().putObject("locArea", this.locArea);
        ObjectStores.getInst().putObject("qualityGrade", this.qualityGrade);
        ObjectStores.getInst().putObject("info", this.info);
        ObjectStores.getInst().putObject("limit", this.limit);
        ObjectStores.getInst().putObject("pages", this.pages);
    }

    public void onCheckedChanged(RadioGroup arg0, int arg1) {
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.rowslist = null;
        this.datalist = null;
        this.view = null;
        this.list = null;
        this.list2 = null;
        this.sellername = null;
        this.type4 = null;
        this.orderby = null;
        this.shopsign = null;
        this.thickmin = null;
        this.thickmax = null;
        this.widemin = null;
        this.widemax = null;
        this.lengthmin = null;
        this.lengthmax = null;
        this.pricemin = null;
        this.pricemax = null;
        this.locArea = null;
        this.qualityGrade = null;
        this.info = null;
        this.limit = null;
        this.pages = null;
        this.linLayout = null;
        this.lin2 = null;
    }
}
