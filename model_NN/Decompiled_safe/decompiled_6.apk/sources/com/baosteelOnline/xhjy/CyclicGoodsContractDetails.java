package com.baosteelOnline.xhjy;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.R;
import com.baosteelOnline.common.DataBaseFactory;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.data.RequestBidData;
import com.baosteelOnline.data_parse.ContractParse;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.model.SmallNumUtil;
import com.baosteelOnline.sql.DBHelper;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQUICallBack;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;

public class CyclicGoodsContractDetails extends JQActivity implements RadioGroup.OnCheckedChangeListener {
    /* access modifiers changed from: private */
    public String cd = StringEx.Empty;
    /* access modifiers changed from: private */
    public String ck = StringEx.Empty;
    /* access modifiers changed from: private */
    public String contract1 = StringEx.Empty;
    public ContractParse contractParse;
    /* access modifiers changed from: private */
    public String cz = StringEx.Empty;
    private DataBaseFactory db;
    private DBHelper dbHelper;
    /* access modifiers changed from: private */
    public String dj = StringEx.Empty;
    /* access modifiers changed from: private */
    public String gg = StringEx.Empty;
    Handler handler;
    /* access modifiers changed from: private */
    public String hth = StringEx.Empty;
    private JQUICallBack httpCallBack = new JQUICallBack() {
        public void callBack(ResultData result) {
            CyclicGoodsContractDetails.this.progressDialog.dismiss();
            Log.d("获取的数据：", result.getStringData());
            if (!result.getStringData().equals(null) && !result.getStringData().equals(StringEx.Empty)) {
                CyclicGoodsContractDetails.this.contractParse = ContractParse.parse(result.getStringData());
                CyclicGoodsContractDetails.this.numOfPageGet = Integer.parseInt(ContractParse.pageNumTal);
                if (ContractParse.getDataString().equals(null) || ContractParse.getDataString().equals(StringEx.Empty)) {
                    new AlertDialog.Builder(CyclicGoodsContractDetails.this).setMessage("获取数据失败。").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    }).show();
                } else {
                    Log.d("合同详情获取的ROWs数据：", ContractParse.jjo.toString());
                    if (ContractParse.jjo.toString().equals(null) || ContractParse.jjo.toString().equals("[[]]") || ContractParse.jjo.toString().equals(StringEx.Empty) || ContractParse.jjo.toString().equals("[]")) {
                        new AlertDialog.Builder(CyclicGoodsContractDetails.this).setMessage("数据为空！").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
                    } else {
                        for (int i = 0; i < ContractParse.jjo.length(); i++) {
                            try {
                                JSONArray row = new JSONArray(ContractParse.jjo.getString(i));
                                HashMap<String, Object> item = new HashMap<>();
                                CyclicGoodsContractDetails.this.pm = row.getString(0);
                                CyclicGoodsContractDetails.this.cz = row.getString(1);
                                CyclicGoodsContractDetails.this.gg = row.getString(2);
                                CyclicGoodsContractDetails.this.sl = row.getString(3);
                                CyclicGoodsContractDetails.this.zl = row.getString(4);
                                CyclicGoodsContractDetails.this.hth = row.getString(5);
                                CyclicGoodsContractDetails.this.xh = row.getString(6);
                                CyclicGoodsContractDetails.this.dj = row.getString(7);
                                CyclicGoodsContractDetails.this.ck = row.getString(8);
                                CyclicGoodsContractDetails.this.cd = row.getString(9);
                                item.put("title", String.valueOf(CyclicGoodsContractDetails.this.numOfItem + 1) + "." + CyclicGoodsContractDetails.this.pm);
                                item.put("num", String.valueOf(CyclicGoodsContractDetails.this.sl) + "件/" + CyclicGoodsContractDetails.this.zl + "吨");
                                item.put("material", "材质：" + CyclicGoodsContractDetails.this.cz);
                                item.put("price", String.valueOf(CyclicGoodsContractDetails.this.dj) + "元/吨");
                                item.put("standard", "规格：" + CyclicGoodsContractDetails.this.gg);
                                item.put("field", "产地：" + CyclicGoodsContractDetails.this.cd);
                                item.put("pm", CyclicGoodsContractDetails.this.pm);
                                item.put("cz", CyclicGoodsContractDetails.this.cz);
                                item.put("gg", CyclicGoodsContractDetails.this.gg);
                                item.put("sl", CyclicGoodsContractDetails.this.sl);
                                item.put("zl", CyclicGoodsContractDetails.this.zl);
                                item.put("hth", CyclicGoodsContractDetails.this.hth);
                                item.put("xh", CyclicGoodsContractDetails.this.xh);
                                item.put("dj", CyclicGoodsContractDetails.this.dj);
                                item.put("ck", CyclicGoodsContractDetails.this.ck);
                                item.put("cd", CyclicGoodsContractDetails.this.cd);
                                Log.d("合同详情获取", String.valueOf(CyclicGoodsContractDetails.this.pm) + " " + CyclicGoodsContractDetails.this.pm + " " + CyclicGoodsContractDetails.this.cz + " " + CyclicGoodsContractDetails.this.gg + " " + CyclicGoodsContractDetails.this.sl + " " + CyclicGoodsContractDetails.this.zl + " " + CyclicGoodsContractDetails.this.hth + " " + CyclicGoodsContractDetails.this.xh + " " + CyclicGoodsContractDetails.this.dj + " " + CyclicGoodsContractDetails.this.ck + " " + CyclicGoodsContractDetails.this.cd);
                                CyclicGoodsContractDetails cyclicGoodsContractDetails = CyclicGoodsContractDetails.this;
                                cyclicGoodsContractDetails.numOfItem = cyclicGoodsContractDetails.numOfItem + 1;
                                CyclicGoodsContractDetails.this.mItemArrayList.add(item);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Message message = new Message();
                            message.what = 1;
                            CyclicGoodsContractDetails.this.handler.sendMessage(message);
                        }
                    }
                }
            }
            CyclicGoodsContractDetails.this.mSimpleAdater.notifyDataSetChanged();
        }
    };
    private boolean isLastPage = false;
    private RadioButton mIndexNavigation1;
    private RadioButton mIndexNavigation2;
    private RadioButton mIndexNavigation3;
    private RadioButton mIndexNavigation4;
    private RadioButton mIndexNavigation5;
    /* access modifiers changed from: private */
    public ArrayList<HashMap<String, Object>> mItemArrayList;
    public TextView mListTitleTextview;
    private ListView mListView;
    private RadioGroup mRadioderGroup;
    /* access modifiers changed from: private */
    public SimpleAdapter mSimpleAdater;
    /* access modifiers changed from: private */
    public int numOfItem = 0;
    private int numOfPage = 1;
    /* access modifiers changed from: private */
    public int numOfPageGet = 1;
    /* access modifiers changed from: private */
    public String number = StringEx.Empty;
    private TextView numberTextView;
    /* access modifiers changed from: private */
    public String pm = StringEx.Empty;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    /* access modifiers changed from: private */
    public String seller = StringEx.Empty;
    private TextView sellerTextView;
    /* access modifiers changed from: private */
    public String sl = StringEx.Empty;
    /* access modifiers changed from: private */
    public String state = StringEx.Empty;
    /* access modifiers changed from: private */
    public TextView stateTextView;
    /* access modifiers changed from: private */
    public String sum = StringEx.Empty;
    private TextView sumTextView;
    /* access modifiers changed from: private */
    public String weight = StringEx.Empty;
    private TextView weightTextView;
    /* access modifiers changed from: private */
    public String xh = StringEx.Empty;
    /* access modifiers changed from: private */
    public String zl = StringEx.Empty;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.activity_cyclic_goods_contract_details);
        ExitApplication.getInstance().addActivity(this);
        this.dbHelper = DataBaseFactory.getInstance(this);
        this.dbHelper.insertOperation("循环物资", "循环资源首页", "合同详情");
        Intent intent = getIntent();
        this.contract1 = intent.getExtras().getString("contract1");
        this.seller = intent.getExtras().getString("seller");
        this.number = intent.getExtras().getString("number");
        this.weight = intent.getExtras().getString("weight");
        this.sum = intent.getExtras().getString("sum");
        this.state = intent.getExtras().getString("state");
        getData();
        ObjectStores.getInst().putObject("positionName", this.contract1);
        this.sellerTextView = (TextView) findViewById(R.id.cyclicgoods_contract_details_textview_title);
        this.numberTextView = (TextView) findViewById(R.id.cyclicgoods_contract_details_textview_num);
        this.weightTextView = (TextView) findViewById(R.id.cyclicgoods_contract_details_textview_weight);
        this.sumTextView = (TextView) findViewById(R.id.cyclicgoods_contract_details_textview_summoney1);
        this.stateTextView = (TextView) findViewById(R.id.cyclicgoods_contract_details_textview_storage);
        this.sellerTextView.setText("卖家：" + this.seller);
        this.numberTextView.setText(this.number);
        this.weightTextView.setText(this.weight);
        this.sumTextView.setText(this.sum);
        this.mListView = (ListView) findViewById(R.id.cyclicgoods_listview_contract_details);
        this.mItemArrayList = new ArrayList<>();
        this.mRadioderGroup = (RadioGroup) findViewById(R.id.cyclic_goods_index_RGroup);
        this.mIndexNavigation1 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item1);
        this.mIndexNavigation2 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item2);
        this.mIndexNavigation3 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item3);
        this.mIndexNavigation4 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item4);
        this.mIndexNavigation5 = (RadioButton) findViewById(R.id.cyclicgoods_index_navigation_item5);
        this.mListTitleTextview = (TextView) findViewById(R.id.cyclicgoods_list_title_textview);
        this.mListTitleTextview.setText("合同详情");
        SmallNumUtil.JJNum(this, (TextView) findViewById(R.id.jj_dhqrnum));
        this.mIndexNavigation1.setChecked(true);
        this.mRadioderGroup.setOnCheckedChangeListener(this);
        this.mIndexNavigation1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ExitApplication.getInstance().startActivity(CyclicGoodsContractDetails.this, CyclicGoodsIndexActivity.class);
            }
        });
        this.mSimpleAdater = new SimpleAdapter(this, this.mItemArrayList, R.layout.activity_cyclic_goods_contract_details_item, new String[]{"title", "num", "material", "price", "standard", "field"}, new int[]{R.id.cyclicgoods_contract_details_item_textview_title, R.id.cyclicgoods_contract_details_item_textview_num, R.id.cyclicgoods_contract_details_item_textview_material, R.id.cyclicgoods_contract_details_item_textview_price, R.id.cyclicgoods_contract_details_item_textview_standard, R.id.cyclicgoods_contract_details_item_textview_field});
        this.mListView.setAdapter((ListAdapter) this.mSimpleAdater);
        this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                Intent intent = new Intent(CyclicGoodsContractDetails.this, CyclicGoodsBalesInfo.class);
                String dj = ((HashMap) CyclicGoodsContractDetails.this.mItemArrayList.get(arg2)).get("dj").toString();
                String cd = ((HashMap) CyclicGoodsContractDetails.this.mItemArrayList.get(arg2)).get("cd").toString();
                String gg = ((HashMap) CyclicGoodsContractDetails.this.mItemArrayList.get(arg2)).get("gg").toString();
                String pm = ((HashMap) CyclicGoodsContractDetails.this.mItemArrayList.get(arg2)).get("pm").toString();
                String hth = ((HashMap) CyclicGoodsContractDetails.this.mItemArrayList.get(arg2)).get("hth").toString();
                String xh = ((HashMap) CyclicGoodsContractDetails.this.mItemArrayList.get(arg2)).get("xh").toString();
                String cz = ((HashMap) CyclicGoodsContractDetails.this.mItemArrayList.get(arg2)).get("cz").toString();
                String sl = ((HashMap) CyclicGoodsContractDetails.this.mItemArrayList.get(arg2)).get("num").toString();
                intent.putExtra("cz", cz);
                intent.putExtra("sl", sl);
                intent.putExtra("dj", dj);
                intent.putExtra("cd", cd);
                intent.putExtra("gg", gg);
                intent.putExtra("pm", pm);
                intent.putExtra("hth", hth);
                intent.putExtra("xh", xh);
                intent.putExtra("seller", CyclicGoodsContractDetails.this.seller);
                intent.putExtra("contract1", CyclicGoodsContractDetails.this.contract1);
                intent.putExtra("seller", CyclicGoodsContractDetails.this.seller);
                intent.putExtra("number", CyclicGoodsContractDetails.this.number);
                intent.putExtra("weight", CyclicGoodsContractDetails.this.weight);
                intent.putExtra("sum", CyclicGoodsContractDetails.this.sum);
                intent.putExtra("state", CyclicGoodsContractDetails.this.state);
                CyclicGoodsContractDetails.this.startActivity(intent);
                CyclicGoodsContractDetails.this.finish();
            }
        });
        this.handler = new Handler() {
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 1:
                        CyclicGoodsContractDetails.this.stateTextView.setText("仓库：" + CyclicGoodsContractDetails.this.ck);
                        break;
                }
                super.handleMessage(msg);
            }
        };
    }

    private void getData() {
        RequestBidData requestBidData = new RequestBidData(this);
        requestBidData.setRequestData("合同详情");
        requestBidData.setId(this.contract1);
        requestBidData.setPageNum(new StringBuilder(String.valueOf(this.numOfPage)).toString());
        Log.d("请求的指令：", requestBidData.infoDate());
        requestBidData.setHttpCallBack(this.httpCallBack);
        requestBidData.iExecute();
        this.progressDialog = ProgressDialog.show(this, null, "数据加载中", true, false);
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.cyclicgoods_index_navigation_item2:
                this.mIndexNavigation2.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item3:
                this.mIndexNavigation3.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsBidSessionSelect.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item1:
                if (ObjectStores.getInst().getObject("formX") == "formX") {
                    ExitApplication.getInstance().startActivity(this, Xhjy_indexActivity.class);
                    finish();
                    return;
                }
                this.mIndexNavigation1.setChecked(true);
                ExitApplication.getInstance().startActivity(this, CyclicGoodsIndexActivity.class);
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item4:
                this.mIndexNavigation4.setChecked(true);
                startActivity(new Intent(this, CyclicGoodsBidHallPageActivity.class));
                finish();
                return;
            case R.id.cyclicgoods_index_navigation_item5:
                this.mIndexNavigation5.setChecked(true);
                Intent intent = new Intent(this, Xhjy_more.class);
                intent.putExtra("part", "循环物资");
                startActivity(intent);
                finish();
                return;
            default:
                return;
        }
    }

    public void backAaction(View v) {
        startActivity(new Intent(this, CyclicGoodsConfirmArrivalActivity.class));
        finish();
    }

    public void onBackPressed() {
        startActivity(new Intent(this, CyclicGoodsConfirmArrivalActivity.class));
        finish();
    }
}
