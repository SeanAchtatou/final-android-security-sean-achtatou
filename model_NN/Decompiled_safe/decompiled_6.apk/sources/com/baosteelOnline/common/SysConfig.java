package com.baosteelOnline.common;

import com.baosteelOnline.model.sysConfigModel;
import com.baosteelOnline.model.sysMethodModel;

public class SysConfig {
    public String project = Utils.getNetConfigProperties().getProperty("project");

    public sysConfigModel setSysconfig() {
        sysConfigModel sys = new sysConfigModel();
        if (this.project.equals("official")) {
            sys.setProjectName(Utils.getNetConfigProperties().getProperty("projectName"));
        }
        if (this.project.equals("test")) {
            sys.setProjectName(Utils.getNetConfigProperties().getProperty("projectNametest"));
        }
        return sys;
    }

    public sysConfigModel setSysconfigs() {
        sysConfigModel sys = new sysConfigModel();
        if (this.project.equals("official")) {
            sys.setProjectName(Utils.getNetConfigProperties().getProperty("IECprojectName"));
        }
        if (this.project.equals("test")) {
            sys.setProjectName(Utils.getNetConfigProperties().getProperty("IECprojectNametest"));
        }
        return sys;
    }

    public sysConfigModel setSysconfigbs() {
        sysConfigModel sys = new sysConfigModel();
        if (this.project.equals("official")) {
            sys.setProjectName(Utils.getNetConfigProperties().getProperty("BAOprojectName"));
        }
        if (this.project.equals("test")) {
            sys.setProjectName(Utils.getNetConfigProperties().getProperty("BAOprojectNametest"));
        }
        return sys;
    }

    public sysMethodModel setMethod() {
        sysMethodModel sys = new sysMethodModel();
        if (this.project.equals("official")) {
            sys.setMethodName(Utils.getNetConfigProperties().getProperty("LogMethod"));
        }
        if (this.project.equals("test")) {
            sys.setMethodName(Utils.getNetConfigProperties().getProperty("LogMethodtest"));
        }
        return sys;
    }

    public sysConfigModel setSysconfigbsol() {
        sysConfigModel sys = new sysConfigModel();
        if (this.project.equals("official")) {
            sys.setProjectName(Utils.getNetConfigProperties().getProperty("BsolprojectName"));
        }
        if (this.project.equals("test")) {
            sys.setProjectName(Utils.getNetConfigProperties().getProperty("BsolprojectNametest"));
        }
        return sys;
    }

    public sysConfigModel setAppCode() {
        sysConfigModel sys = new sysConfigModel();
        if (this.project.equals("official")) {
            sys.setProjectName("bspnocluster");
        }
        if (this.project.equals("test")) {
            sys.setProjectName("bsptest1");
        }
        return sys;
    }
}
