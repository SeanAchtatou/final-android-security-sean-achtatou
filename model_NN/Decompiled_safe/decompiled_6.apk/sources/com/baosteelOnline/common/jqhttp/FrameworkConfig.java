package com.baosteelOnline.common.jqhttp;

import android.content.Context;
import android.content.res.XmlResourceParser;
import android.util.Log;
import com.jianq.misc.StringEx;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.xmlpull.v1.XmlPullParserException;

public class FrameworkConfig {
    private static FrameworkConfig frameworkConfig;
    private Map<String, String> configValue = new HashMap();
    private String host;
    private String logName;
    private String logPath;
    private String port;

    public FrameworkConfig(Context context, int xmlConfig) {
        buildConfig(context, xmlConfig);
    }

    public static FrameworkConfig getInst() {
        if (frameworkConfig == null) {
            frameworkConfig = new FrameworkConfig(null, -1);
        }
        return frameworkConfig;
    }

    public static FrameworkConfig getInst(Context ctx, int cfg) {
        if (frameworkConfig == null) {
            frameworkConfig = new FrameworkConfig(ctx, cfg);
        }
        return frameworkConfig;
    }

    public String getHost() {
        return this.host;
    }

    public void setHost(String host2) {
        this.host = host2;
    }

    public String getPort() {
        return this.port;
    }

    public void setPort(String port2) {
        this.port = port2;
    }

    public String getLogPath() {
        return this.logPath;
    }

    public void setLogPath(String logPath2) {
        this.logPath = logPath2;
    }

    public String getLogName() {
        return this.logName;
    }

    public void setLogName(String logName2) {
        this.logName = logName2;
    }

    public String getConfigValue(String key) {
        if (key == null || key.equals(StringEx.Empty) || !this.configValue.containsKey(key.toLowerCase())) {
            return null;
        }
        return this.configValue.get(key.toLowerCase());
    }

    private void buildConfig(Context context, int xmlConfig) {
        XmlResourceParser xrp = context.getResources().getXml(xmlConfig);
        String tn = StringEx.Empty;
        while (xrp.getEventType() != 1) {
            try {
                if (xrp.getEventType() == 2) {
                    tn = xrp.getName();
                }
                if (xrp.getEventType() == 4) {
                    String tv = xrp.getText();
                    this.configValue.put(tn.toLowerCase(), tv);
                    if (tn.toLowerCase().equals("host")) {
                        setHost(tv);
                    }
                    if (tn.toLowerCase().equals("port")) {
                        setPort(tv);
                    }
                    if (tn.toLowerCase().equals("logpath")) {
                        setLogPath(tv);
                    }
                    if (tn.toLowerCase().equals("logname")) {
                        setLogName(tv);
                    }
                }
                xrp.next();
            } catch (XmlPullParserException e) {
                Log.e(toString(), new StringBuilder().append(e.getDetail()).toString());
                e.printStackTrace();
                return;
            } catch (IOException e2) {
                Log.e(toString(), e2.getMessage());
                e2.printStackTrace();
                return;
            } finally {
                xrp.close();
            }
        }
    }
}
