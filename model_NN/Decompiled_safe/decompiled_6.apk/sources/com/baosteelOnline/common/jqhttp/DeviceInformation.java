package com.baosteelOnline.common.jqhttp;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.view.Display;

public class DeviceInformation {
    private static DeviceInformation information;
    private String IMEI;
    private String OS = "android";
    private String SDCardPath;
    private String model;
    private int screenHeight;
    private int screenWidth;
    private boolean sdCardExist;
    private int sdk;
    private String version;

    public DeviceInformation(Context context) {
        if (context != null) {
            TelephonyManager tm = (TelephonyManager) context.getSystemService("phone");
            this.sdk = Build.VERSION.SDK_INT;
            this.IMEI = tm.getDeviceId();
            this.version = tm.getDeviceSoftwareVersion();
            Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
            this.screenHeight = display.getHeight();
            this.screenWidth = display.getWidth();
            this.sdCardExist = Environment.getExternalStorageState().equals("mounted");
            if (this.sdCardExist) {
                this.SDCardPath = Environment.getExternalStorageDirectory() + "//";
            }
            switch (tm.getPhoneType()) {
                case 0:
                    this.model = "PHONE_TYPE_CDMA";
                    return;
                case 1:
                    this.model = "PHONE_TYPE_GSM";
                    return;
                case 2:
                    this.model = "PHONE_TYPE_CDMA";
                    return;
                case 3:
                    this.model = "PHONE_TYPE_SIP";
                    return;
                default:
                    return;
            }
        }
    }

    public static DeviceInformation getInst() {
        if (information == null) {
            information = new DeviceInformation(null);
        }
        return information;
    }

    public static DeviceInformation getInst(Context context) {
        if (information == null) {
            information = new DeviceInformation(context);
        }
        return information;
    }

    public String getIMEI() {
        return this.IMEI;
    }

    public String getOS() {
        return this.OS;
    }

    public int getSdk() {
        return this.sdk;
    }

    public String getVersion() {
        return this.version;
    }

    public String getModel() {
        return this.model;
    }

    public String getSDCardPath() {
        return this.SDCardPath;
    }

    public boolean hasSDCard() {
        return this.sdCardExist;
    }

    public int getScreenHeight() {
        return this.screenHeight;
    }

    public int getScreenWidth() {
        return this.screenWidth;
    }
}
