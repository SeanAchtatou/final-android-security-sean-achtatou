package com.baosteelOnline.common.jqhttp;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;
import cn.com.jit.pnxclient.constant.PNXConfigConstant;
import com.andframework.common.ObjectStores;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

public class HttpDownloader {
    private static final int DOWNLOADED = 2;
    private static final int DOWNLOADING = 1;
    private static final int DOWNLOAD_ERROR = -1;
    private static final int PREV_CHECK = 10;
    private static final int START_DOWNLOAD = 0;
    /* access modifiers changed from: private */
    public Context ctx;
    /* access modifiers changed from: private */
    public ProgressDialog downloadDialog;
    /* access modifiers changed from: private */
    public int downloadedSize = 0;
    /* access modifiers changed from: private */
    public String errorMsg;
    /* access modifiers changed from: private */
    public File file;
    /* access modifiers changed from: private */
    public int fileTotalSize;
    private String fullFilePath;
    /* access modifiers changed from: private */
    public boolean isOpen;
    private Handler mainThreadHandler = new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case -1:
                    if (HttpDownloader.this.downloadDialog.isShowing()) {
                        HttpDownloader.this.downloadDialog.dismiss();
                        ObjectStores.getInst().putObject("downloadDialog", "Dismiss");
                    }
                    Toast.makeText(HttpDownloader.this.ctx, HttpDownloader.this.errorMsg, 0).show();
                    if (HttpDownloader.this.file != null) {
                        FileUtils.delete(HttpDownloader.this.file.toString());
                        break;
                    }
                    break;
                case 0:
                    HttpDownloader.this.downloadDialog.show();
                    HttpDownloader.this.downloadDialog.setMax(HttpDownloader.this.fileTotalSize);
                    break;
                case 1:
                    HttpDownloader.this.downloadDialog.setProgress(HttpDownloader.this.downloadedSize);
                    break;
                case 2:
                    HttpDownloader.this.downloadDialog.dismiss();
                    ObjectStores.getInst().putObject("downloadDialog", "Dismiss");
                    Toast.makeText(HttpDownloader.this.ctx, "文件下载完成", 0).show();
                    if (HttpDownloader.this.isOpen) {
                        new OpenFileHelper(HttpDownloader.this.ctx).openFile(HttpDownloader.this.file);
                        break;
                    }
                    break;
            }
            super.handleMessage(msg);
        }
    };
    private URL url = null;

    public HttpDownloader(Context ctx2) {
        this.ctx = ctx2;
    }

    /* access modifiers changed from: private */
    public void sendMessage(int flag) {
        Message msg = this.mainThreadHandler.obtainMessage();
        msg.what = flag;
        this.mainThreadHandler.sendMessage(msg);
    }

    /* access modifiers changed from: private */
    public void write2SDFromInput(InputStream input) {
        FileOutputStream output = null;
        Log.i("HttpDownloader", this.fullFilePath);
        try {
            FileOutputStream output2 = new FileOutputStream(this.file);
            try {
                byte[] buffer = new byte[4096];
                while (true) {
                    int size = input.read(buffer);
                    if (size == -1) {
                        output2.flush();
                        sendMessage(2);
                        try {
                            output2.close();
                            return;
                        } catch (Exception e) {
                            e.printStackTrace();
                            return;
                        }
                    } else {
                        output2.write(buffer, 0, size);
                        this.downloadedSize += size;
                        sendMessage(1);
                    }
                }
            } catch (Exception e2) {
                e = e2;
                output = output2;
            } catch (Throwable th) {
                th = th;
                output = output2;
                try {
                    output.close();
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
                throw th;
            }
        } catch (Exception e4) {
            e = e4;
            try {
                e.printStackTrace();
                try {
                    output.close();
                } catch (Exception e5) {
                    e5.printStackTrace();
                }
            } catch (Throwable th2) {
                th = th2;
                output.close();
                throw th;
            }
        }
    }

    private void startDownload(final String urlStr, String updateflag) {
        this.downloadDialog = new ProgressDialog(this.ctx);
        if (updateflag.equals("1")) {
            this.downloadDialog.setCancelable(false);
        } else if (updateflag.equals("0")) {
            this.downloadDialog.setCancelable(true);
        }
        this.downloadDialog.setProgressStyle(1);
        new Thread(new Runnable() {
            public void run() {
                InputStream inputStream = null;
                try {
                    inputStream = HttpDownloader.this.getInputStreamFromUrl(urlStr);
                    HttpDownloader.this.sendMessage(0);
                    HttpDownloader.this.write2SDFromInput(inputStream);
                    try {
                        inputStream.close();
                    } catch (Exception e) {
                        HttpDownloader.this.errorMsg = "URL出错，请检查能否正常访问到下载URL";
                        HttpDownloader.this.sendMessage(-1);
                        Log.d("HttpDownloader.URL", urlStr);
                    }
                } catch (Exception e2) {
                    HttpDownloader.this.errorMsg = "URL出错，请检查能否正常访问到下载URL";
                    HttpDownloader.this.sendMessage(-1);
                    Log.d("HttpDownloader.URL", urlStr);
                    try {
                        inputStream.close();
                    } catch (Exception e3) {
                        HttpDownloader.this.errorMsg = "URL出错，请检查能否正常访问到下载URL";
                        HttpDownloader.this.sendMessage(-1);
                        Log.d("HttpDownloader.URL", urlStr);
                    }
                } catch (Throwable th) {
                    try {
                        inputStream.close();
                    } catch (Exception e4) {
                        HttpDownloader.this.errorMsg = "URL出错，请检查能否正常访问到下载URL";
                        HttpDownloader.this.sendMessage(-1);
                        Log.d("HttpDownloader.URL", urlStr);
                    }
                    throw th;
                }
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public InputStream getInputStreamFromUrl(String urlStr) throws MalformedURLException, IOException {
        String fName = urlStr.substring(urlStr.lastIndexOf("/") + 1, urlStr.indexOf("?") == -1 ? urlStr.length() : urlStr.indexOf("?"));
        String urlStr2 = urlStr.replace(fName, URLEncoder.encode(fName));
        Log.i("HttpDownloader", "urlStr-->" + urlStr2);
        this.url = new URL(urlStr2);
        URLConnection urlConn = this.url.openConnection();
        urlConn.connect();
        InputStream inputStream = urlConn.getInputStream();
        this.fileTotalSize = urlConn.getContentLength();
        return inputStream;
    }

    public void downloadFile(String url2, String filePath, boolean isOpen2, String updateflag) {
        this.isOpen = isOpen2;
        this.fullFilePath = filePath;
        File file2 = new File(filePath);
        if (file2.isDirectory()) {
            this.errorMsg = "存储对象必须是文件，请联系相关开发人员";
            sendMessage(-1);
            return;
        }
        if (file2.getParent() != null && !new File(file2.getParent()).exists()) {
            FileUtils.createDir(file2.getParent());
        }
        try {
            this.file = FileUtils.createFile(filePath);
            startDownload(url2, updateflag);
        } catch (IOException e) {
            this.errorMsg = "创建文件失败";
            sendMessage(-1);
        }
    }

    public String downloadText(String urlStr) {
        StringBuffer sb = new StringBuffer();
        BufferedReader buffer = null;
        try {
            this.url = new URL(urlStr);
            HttpURLConnection urlConn = (HttpURLConnection) this.url.openConnection();
            BufferedReader buffer2 = new BufferedReader(new InputStreamReader(urlConn.getInputStream(), "GBK"));
            while (true) {
                try {
                    String line = buffer2.readLine();
                    if (line == null) {
                        break;
                    }
                    sb.append(String.valueOf(line) + PNXConfigConstant.RESP_SPLIT_2);
                } catch (Exception e) {
                    e = e;
                    buffer = buffer2;
                } catch (Throwable th) {
                    th = th;
                    buffer = buffer2;
                    try {
                        buffer.close();
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    throw th;
                }
            }
            urlConn.disconnect();
            try {
                buffer2.close();
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        } catch (Exception e4) {
            e = e4;
            try {
                e.printStackTrace();
                try {
                    buffer.close();
                } catch (Exception e5) {
                    e5.printStackTrace();
                }
                return sb.toString();
            } catch (Throwable th2) {
                th = th2;
                buffer.close();
                throw th;
            }
        }
        return sb.toString();
    }
}
