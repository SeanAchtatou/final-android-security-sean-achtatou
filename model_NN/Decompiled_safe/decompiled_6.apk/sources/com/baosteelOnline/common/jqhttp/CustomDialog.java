package com.baosteelOnline.common.jqhttp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import com.andframework.common.ObjectStores;

public class CustomDialog {
    public static CustomDialog customDialog;
    private ProgressDialog progressDialog;

    public static CustomDialog getInst() {
        if (customDialog == null) {
            customDialog = new CustomDialog();
        }
        return customDialog;
    }

    public void showProgressDialog(Context context) {
        if (this.progressDialog != null) {
            this.progressDialog.dismiss();
        }
        this.progressDialog = new ProgressDialog(context);
        this.progressDialog.setMessage("正在加载中...");
        this.progressDialog.setIndeterminate(true);
        this.progressDialog.setCancelable(true);
        this.progressDialog.show();
    }

    public void showProgressDialog(Context context, String message) {
        if (this.progressDialog != null) {
            this.progressDialog.dismiss();
        }
        this.progressDialog = new ProgressDialog(context);
        this.progressDialog.setMessage(message);
        this.progressDialog.setIndeterminate(true);
        this.progressDialog.setCancelable(true);
        this.progressDialog.show();
    }

    public void cancelProgressDialog() {
        if (this.progressDialog != null) {
            this.progressDialog.dismiss();
        }
    }

    public void showAlertDialog(Context context, String title, String msg, String buttonText, int iconid, DialogInterface.OnClickListener listener) {
        String btnText = "确定";
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("信息");
        if (iconid > 0) {
            builder.setIcon(iconid);
        }
        if (title != null) {
            builder.setTitle(title);
        }
        if (msg != null) {
            builder.setMessage(msg);
        }
        if (buttonText != null) {
            btnText = buttonText;
        }
        builder.setPositiveButton(btnText, listener);
        builder.create().show();
    }

    public void showConfirmDialog(Context context, String title, String msg, String lStr, String rStr, int iconid, DialogInterface.OnClickListener listener) {
        String lBtnStr = "确定";
        String rBtnStr = "取消";
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("提示信息");
        if (iconid > 0) {
            builder.setIcon(iconid);
        }
        if (title != null) {
            builder.setTitle(title);
        }
        if (msg != null) {
            builder.setMessage(msg);
        }
        if (lStr != null && lStr.length() > 0) {
            lBtnStr = lStr;
        }
        if (rStr != null && rStr.length() > 0) {
            rBtnStr = rStr;
        }
        builder.setPositiveButton(rBtnStr, listener);
        builder.setNegativeButton(lBtnStr, listener);
        builder.create().show();
    }

    public void showUpdateConfirmDialog(Context context, DialogInterface.OnClickListener leftListener, DialogInterface.OnClickListener rightListenr) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setTitle("更新提醒");
        builder.setIcon(17301659);
        builder.setMessage("发现新版本，是否升级？");
        builder.setPositiveButton("下次提醒", rightListenr);
        builder.setNegativeButton("现在升级", leftListener);
        builder.create().show();
    }

    public void showUpdateConfirmDialogMandatory(Context context, DialogInterface.OnClickListener rightListenr, DialogInterface.OnClickListener leftListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setTitle("更新提醒");
        builder.setIcon(17301659);
        builder.setMessage("发现新版本，请升级！");
        builder.setNegativeButton("现在升级", rightListenr);
        builder.setPositiveButton("取消", leftListener);
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
        ObjectStores.getInst().putObject("downloadDialog", "isShowing");
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            public void onDismiss(DialogInterface dialog) {
            }
        });
    }
}
