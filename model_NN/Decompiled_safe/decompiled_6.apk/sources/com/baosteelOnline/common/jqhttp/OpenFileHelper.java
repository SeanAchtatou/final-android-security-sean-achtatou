package com.baosteelOnline.common.jqhttp;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;
import com.baosteelOnline.R;
import java.io.File;

public class OpenFileHelper {
    private Context context;

    public OpenFileHelper(Context context2) {
        this.context = context2;
    }

    public void openFile(File currentPath) {
        Log.d("OpenFileHelper", currentPath.toString());
        if (currentPath == null || !currentPath.isFile()) {
            showMessage("对不起，这不是文件！");
            return;
        }
        String fileName = currentPath.toString();
        try {
            if (checkEndsWithInStringArray(fileName, this.context.getResources().getStringArray(R.array.fileEndingImage))) {
                this.context.startActivity(OpenFiles.getImageFileIntent(currentPath));
            } else if (checkEndsWithInStringArray(fileName, this.context.getResources().getStringArray(R.array.fileEndingWebText))) {
                this.context.startActivity(OpenFiles.getHtmlFileIntent(currentPath));
            } else if (checkEndsWithInStringArray(fileName, this.context.getResources().getStringArray(R.array.fileEndingPackage))) {
                this.context.startActivity(OpenFiles.getApkFileIntent(currentPath));
            } else if (checkEndsWithInStringArray(fileName, this.context.getResources().getStringArray(R.array.fileEndingAudio))) {
                this.context.startActivity(OpenFiles.getAudioFileIntent(currentPath));
            } else if (checkEndsWithInStringArray(fileName, this.context.getResources().getStringArray(R.array.fileEndingVideo))) {
                this.context.startActivity(OpenFiles.getVideoFileIntent(currentPath));
            } else if (checkEndsWithInStringArray(fileName, this.context.getResources().getStringArray(R.array.fileEndingText))) {
                this.context.startActivity(OpenFiles.getTextFileIntent(currentPath));
            } else if (checkEndsWithInStringArray(fileName, this.context.getResources().getStringArray(R.array.fileEndingPdf))) {
                this.context.startActivity(OpenFiles.getPdfFileIntent(currentPath));
            } else if (checkEndsWithInStringArray(fileName, this.context.getResources().getStringArray(R.array.fileEndingWord))) {
                this.context.startActivity(OpenFiles.getWordFileIntent(currentPath));
            } else if (checkEndsWithInStringArray(fileName, this.context.getResources().getStringArray(R.array.fileEndingExcel))) {
                this.context.startActivity(OpenFiles.getExcelFileIntent(currentPath));
            } else if (checkEndsWithInStringArray(fileName, this.context.getResources().getStringArray(R.array.fileEndingPPT))) {
                this.context.startActivity(OpenFiles.getPPTFileIntent(currentPath));
            } else {
                showMessage("无法打开，请安装相应的软件！");
            }
        } catch (ActivityNotFoundException e) {
            showMessage("无法打开，请安装相应的软件！");
        }
    }

    private void showMessage(String msg) {
        Toast.makeText(this.context, msg, 1).show();
    }

    private boolean checkEndsWithInStringArray(String checkItsEnd, String[] fileEndings) {
        for (String aEnd : fileEndings) {
            if (checkItsEnd.endsWith(aEnd)) {
                return true;
            }
        }
        return false;
    }
}
