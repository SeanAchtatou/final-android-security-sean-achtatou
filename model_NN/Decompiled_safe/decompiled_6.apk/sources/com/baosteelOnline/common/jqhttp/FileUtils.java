package com.baosteelOnline.common.jqhttp;

import android.util.Log;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class FileUtils {
    public static File createDir(String dirPath) {
        File file = new File(dirPath);
        if (!file.exists() && !file.mkdirs()) {
            return null;
        }
        return file;
    }

    public static File createFile(String filePath) throws IOException {
        File file = new File(filePath);
        if ((!file.exists() || !file.isFile()) && !file.createNewFile()) {
            return null;
        }
        return file;
    }

    public static boolean delete(String filePath) {
        Log.d("DeleteFile", filePath);
        if (!new File(filePath).exists()) {
            return false;
        }
        deleteFileOrDir(filePath);
        return true;
    }

    private static void deleteFileOrDir(String path) {
        File file = new File(path);
        if (file.isFile()) {
            file.delete();
            return;
        }
        for (File f : file.listFiles()) {
            deleteFileOrDir(f.getAbsolutePath());
        }
        file.delete();
    }

    public static boolean isExist(String filePath) {
        return new File(filePath).exists();
    }

    public static String getStringFromInputStream(InputStream iStream, String encoding) {
        return null;
    }
}
