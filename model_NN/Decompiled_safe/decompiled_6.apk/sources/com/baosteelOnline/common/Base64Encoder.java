package com.baosteelOnline.common;

import com.jianq.net.JQBasicNetwork;
import java.io.ByteArrayOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

public class Base64Encoder extends FilterOutputStream {
    private static final char[] chars = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};
    private int carryOver;
    private int charCount;

    public Base64Encoder(OutputStream out) {
        super(out);
    }

    public void write(int b) throws IOException {
        if (b < 0) {
            b += 256;
        }
        if (this.charCount % 3 == 0) {
            this.carryOver = b & 3;
            this.out.write(chars[b >> 2]);
        } else if (this.charCount % 3 == 1) {
            int lookup = ((this.carryOver << 4) + (b >> 4)) & 63;
            this.carryOver = b & 15;
            this.out.write(chars[lookup]);
        } else if (this.charCount % 3 == 2) {
            this.out.write(chars[((this.carryOver << 2) + (b >> 6)) & 63]);
            this.out.write(chars[b & 63]);
            this.carryOver = 0;
        }
        this.charCount++;
        if (this.charCount % 57 == 0) {
            this.out.write(10);
        }
    }

    public void write(byte[] buf, int off, int len) throws IOException {
        for (int i = 0; i < len; i++) {
            write(buf[off + i]);
        }
    }

    public void close() throws IOException {
        if (this.charCount % 3 == 1) {
            this.out.write(chars[(this.carryOver << 4) & 63]);
            this.out.write(61);
            this.out.write(61);
        } else if (this.charCount % 3 == 2) {
            this.out.write(chars[(this.carryOver << 2) & 63]);
            this.out.write(61);
        }
        super.close();
    }

    public static String encode(String unencoded) {
        byte[] bytes = null;
        try {
            bytes = unencoded.getBytes(JQBasicNetwork.UTF_8);
        } catch (UnsupportedEncodingException e) {
        }
        return encode(bytes);
    }

    public static String encode(byte[] bytes) {
        ByteArrayOutputStream out = new ByteArrayOutputStream((int) (((double) bytes.length) * 1.37d));
        Base64Encoder encodedOut = new Base64Encoder(out);
        try {
            encodedOut.write(bytes);
            encodedOut.close();
            return out.toString(JQBasicNetwork.UTF_8);
        } catch (IOException e) {
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0047  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void main(java.lang.String[] r8) throws java.lang.Exception {
        /*
            int r6 = r8.length
            r7 = 1
            if (r6 == r7) goto L_0x000c
            java.io.PrintStream r6 = java.lang.System.err
            java.lang.String r7 = "Usage: java com.oreilly.servlet.Base64Encoder fileToEncode"
            r6.println(r7)
        L_0x000b:
            return
        L_0x000c:
            r2 = 0
            r4 = 0
            com.baosteelOnline.common.Base64Encoder r3 = new com.baosteelOnline.common.Base64Encoder     // Catch:{ all -> 0x004b }
            java.io.PrintStream r6 = java.lang.System.out     // Catch:{ all -> 0x004b }
            r3.<init>(r6)     // Catch:{ all -> 0x004b }
            java.io.BufferedInputStream r5 = new java.io.BufferedInputStream     // Catch:{ all -> 0x004d }
            java.io.FileInputStream r6 = new java.io.FileInputStream     // Catch:{ all -> 0x004d }
            r7 = 0
            r7 = r8[r7]     // Catch:{ all -> 0x004d }
            r6.<init>(r7)     // Catch:{ all -> 0x004d }
            r5.<init>(r6)     // Catch:{ all -> 0x004d }
            r6 = 4096(0x1000, float:5.74E-42)
            byte[] r0 = new byte[r6]     // Catch:{ all -> 0x003d }
        L_0x0026:
            int r1 = r5.read(r0)     // Catch:{ all -> 0x003d }
            r6 = -1
            if (r1 != r6) goto L_0x0038
            if (r5 == 0) goto L_0x0032
            r5.close()
        L_0x0032:
            if (r3 == 0) goto L_0x000b
            r3.close()
            goto L_0x000b
        L_0x0038:
            r6 = 0
            r3.write(r0, r6, r1)     // Catch:{ all -> 0x003d }
            goto L_0x0026
        L_0x003d:
            r6 = move-exception
            r4 = r5
            r2 = r3
        L_0x0040:
            if (r4 == 0) goto L_0x0045
            r4.close()
        L_0x0045:
            if (r2 == 0) goto L_0x004a
            r2.close()
        L_0x004a:
            throw r6
        L_0x004b:
            r6 = move-exception
            goto L_0x0040
        L_0x004d:
            r6 = move-exception
            r2 = r3
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baosteelOnline.common.Base64Encoder.main(java.lang.String[]):void");
    }
}
