package com.baosteelOnline.common;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.widget.DigitalClock;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@SuppressLint({"SimpleDateFormat"})
public class MyDigitalClock extends DigitalClock {
    private static final String mFormat = "yyyy-MM-dd kk:mm:ss";
    /* access modifiers changed from: private */
    public long dateTime = 0;
    Calendar mCalendar;
    private FormatChangeObserver mFormatChangeObserver;
    /* access modifiers changed from: private */
    public Handler mHandler;
    /* access modifiers changed from: private */
    public Runnable mTicker;
    /* access modifiers changed from: private */
    public boolean mTickerStopped = false;

    public MyDigitalClock(Context context) {
        super(context);
        initClock(context);
    }

    public MyDigitalClock(Context context, AttributeSet attrs) {
        super(context, attrs);
        initClock(context);
    }

    private void initClock(Context context) {
        String dateOfG = Dataofdate.getDate();
        new Date();
        try {
            this.dateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateOfG).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Resources resources = context.getResources();
        if (this.mCalendar == null) {
            this.mCalendar = Calendar.getInstance();
        }
        this.mFormatChangeObserver = new FormatChangeObserver();
        getContext().getContentResolver().registerContentObserver(Settings.System.CONTENT_URI, true, this.mFormatChangeObserver);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        this.mTickerStopped = false;
        super.onAttachedToWindow();
        this.mHandler = new Handler();
        this.mTicker = new Runnable() {
            public void run() {
                if (!MyDigitalClock.this.mTickerStopped) {
                    MyDigitalClock.this.mCalendar.setTimeInMillis(MyDigitalClock.this.dateTime);
                    MyDigitalClock.this.setText(DateFormat.format(MyDigitalClock.mFormat, MyDigitalClock.this.mCalendar));
                    MyDigitalClock.this.invalidate();
                    long now = SystemClock.uptimeMillis();
                    MyDigitalClock.this.setDateTime(MyDigitalClock.this.dateTime);
                    MyDigitalClock myDigitalClock = MyDigitalClock.this;
                    myDigitalClock.dateTime = myDigitalClock.dateTime + 1000;
                    MyDigitalClock.this.mHandler.postAtTime(MyDigitalClock.this.mTicker, now + (1000 - (now % 1000)));
                }
            }
        };
        this.mTicker.run();
    }

    private class FormatChangeObserver extends ContentObserver {
        public FormatChangeObserver() {
            super(new Handler());
        }

        public void onChange(boolean selfChange) {
        }
    }

    public long getDateTime() {
        return this.dateTime;
    }

    public void setDateTime(long dateTime2) {
        this.dateTime = dateTime2;
    }
}
