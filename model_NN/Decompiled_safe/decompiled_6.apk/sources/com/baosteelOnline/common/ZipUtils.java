package com.baosteelOnline.common;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.CRC32;
import java.util.zip.CheckedOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipUtils {
    private static final String BASE_DIR = "";
    private static final int BUFFER = 1024;
    public static final String EXT = ".zip";
    private static final String PATH = "/";

    public static String getExtensionName(String filename) {
        int dot;
        if (filename == null || filename.length() <= 0 || (dot = filename.lastIndexOf(46)) <= -1 || dot >= filename.length() - 1) {
            return filename;
        }
        return filename.substring(0, dot);
    }

    public static void compress(File srcFile) throws Exception {
        compress(srcFile, String.valueOf(srcFile.getParent()) + PATH + getExtensionName(srcFile.getName()) + EXT);
    }

    public static void compress(File srcFile, File destFile) throws Exception {
        ZipOutputStream zos = new ZipOutputStream(new CheckedOutputStream(new FileOutputStream(destFile), new CRC32()));
        compress(srcFile, zos, "");
        zos.flush();
        zos.close();
    }

    public static void compress(File srcFile, String destPath) throws Exception {
        compress(srcFile, new File(destPath));
    }

    private static void compress(File srcFile, ZipOutputStream zos, String basePath) throws Exception {
        if (srcFile.isDirectory()) {
            compressDir(srcFile, zos, basePath);
        } else {
            compressFile(srcFile, zos, basePath);
        }
    }

    public static void compress(String srcPath) throws Exception {
        compress(new File(srcPath));
    }

    public static void compress(String srcPath, String destPath) throws Exception {
        compress(new File(srcPath), destPath);
    }

    private static void compressDir(File dir, ZipOutputStream zos, String basePath) throws Exception {
        File[] files = dir.listFiles();
        if (files.length < 1) {
            zos.putNextEntry(new ZipEntry(String.valueOf(basePath) + dir.getName() + PATH));
            zos.closeEntry();
        }
        int length = files.length;
        for (int i = 0; i < length; i++) {
            compress(files[i], zos, String.valueOf(basePath) + dir.getName() + PATH);
        }
    }

    private static void compressFile(File file, ZipOutputStream zos, String dir) throws Exception {
        zos.putNextEntry(new ZipEntry(String.valueOf(dir) + file.getName()));
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
        byte[] data = new byte[1024];
        while (true) {
            int count = bis.read(data, 0, 1024);
            if (count == -1) {
                bis.close();
                zos.closeEntry();
                return;
            }
            zos.write(data, 0, count);
        }
    }
}
