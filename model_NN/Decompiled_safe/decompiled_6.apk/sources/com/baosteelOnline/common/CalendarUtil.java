package com.baosteelOnline.common;

import com.jianq.misc.StringEx;
import java.text.DateFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class CalendarUtil {
    private int MaxDate;
    private int MaxYear;
    private int weeks = 0;

    public static int getYear() {
        return Calendar.getInstance().get(1);
    }

    public static int getMonth() {
        return Calendar.getInstance().get(2) + 1;
    }

    public static int getDayOfYear() {
        return Calendar.getInstance().get(6);
    }

    public static int getDayOfMonth() {
        return Calendar.getInstance().get(5);
    }

    public static int getDayOfWeek() {
        return Calendar.getInstance().get(7);
    }

    public static int getWeekOfMonth() {
        return Calendar.getInstance().get(8);
    }

    public static Date getTimeYearNext() {
        Calendar.getInstance().add(6, 183);
        return Calendar.getInstance().getTime();
    }

    public static String convertDateToString(Date dateTime) {
        return new SimpleDateFormat("yyyy-MM-dd").format(dateTime);
    }

    public static String getTwoDay(String sj1, String sj2) {
        SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return new StringBuilder(String.valueOf((myFormatter.parse(sj1).getTime() - myFormatter.parse(sj2).getTime()) / 86400000)).toString();
        } catch (Exception e) {
            return StringEx.Empty;
        }
    }

    public static String getWeek(String sdate) {
        Date date = strToDate(sdate);
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return new SimpleDateFormat("EEEE").format(c.getTime());
    }

    public static Date strToDate(String strDate) {
        return new SimpleDateFormat("yyyy-MM-dd").parse(strDate, new ParsePosition(0));
    }

    public static long getDays(String date1, String date2) {
        if (date1 == null || date1.equals(StringEx.Empty) || date2 == null || date2.equals(StringEx.Empty)) {
            return 0;
        }
        SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        Date mydate = null;
        try {
            date = myFormatter.parse(date1);
            mydate = myFormatter.parse(date2);
        } catch (Exception e) {
        }
        return (date.getTime() - mydate.getTime()) / 86400000;
    }

    public String getDefaultDay() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar lastDate = Calendar.getInstance();
        lastDate.set(5, 1);
        lastDate.add(2, 1);
        lastDate.add(5, -1);
        return sdf.format(lastDate.getTime());
    }

    public String getPreviousMonthFirst() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar lastDate = Calendar.getInstance();
        lastDate.set(5, 1);
        lastDate.add(2, -1);
        return sdf.format(lastDate.getTime());
    }

    public String getFirstDayOfMonth() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        GregorianCalendar currentDate = new GregorianCalendar();
        currentDate.add(5, -30);
        String str = sdf.format(currentDate.getTime());
        System.out.println("本月的时间:" + str);
        return str;
    }

    public String getCurrentWeekday() {
        this.weeks = 0;
        int mondayPlus = getMondayPlus();
        GregorianCalendar currentDate = new GregorianCalendar();
        currentDate.add(5, mondayPlus + 6);
        return DateFormat.getDateInstance().format(currentDate.getTime());
    }

    public String getNowTime(String dateformat) {
        String hehe = new SimpleDateFormat(dateformat).format(new Date());
        System.out.println("今天的时间：" + hehe);
        return hehe;
    }

    private int getMondayPlus() {
        int dayOfWeek = Calendar.getInstance().get(7) - 1;
        if (dayOfWeek == 1) {
            return 0;
        }
        return 1 - dayOfWeek;
    }

    public String getMondayOFWeek() {
        this.weeks = 0;
        int mondayPlus = getMondayPlus();
        GregorianCalendar currentDate = new GregorianCalendar();
        currentDate.add(5, -7);
        String preMonday = new SimpleDateFormat("yyyy-MM-dd").format(currentDate.getTime());
        System.out.println("preMonday的值:" + preMonday);
        return preMonday;
    }

    public String getSaturday() {
        int mondayPlus = getMondayPlus();
        GregorianCalendar currentDate = new GregorianCalendar();
        currentDate.add(5, (this.weeks * 7) + mondayPlus + 6);
        return DateFormat.getDateInstance().format(currentDate.getTime());
    }

    public String getPreviousWeekSunday() {
        this.weeks = 0;
        this.weeks--;
        int mondayPlus = getMondayPlus();
        GregorianCalendar currentDate = new GregorianCalendar();
        currentDate.add(5, this.weeks + mondayPlus);
        return DateFormat.getDateInstance().format(currentDate.getTime());
    }

    public String getPreviousWeekday() {
        this.weeks--;
        int mondayPlus = getMondayPlus();
        GregorianCalendar currentDate = new GregorianCalendar();
        currentDate.add(5, (this.weeks * 7) + mondayPlus);
        return DateFormat.getDateInstance().format(currentDate.getTime());
    }

    public String getNextMonday() {
        this.weeks++;
        int mondayPlus = getMondayPlus();
        GregorianCalendar currentDate = new GregorianCalendar();
        currentDate.add(5, mondayPlus + 7);
        return DateFormat.getDateInstance().format(currentDate.getTime());
    }

    public String getNextSunday() {
        int mondayPlus = getMondayPlus();
        GregorianCalendar currentDate = new GregorianCalendar();
        currentDate.add(5, mondayPlus + 7 + 6);
        return DateFormat.getDateInstance().format(currentDate.getTime());
    }

    private int getMonthPlus() {
        Calendar cd = Calendar.getInstance();
        int monthOfNumber = cd.get(5);
        cd.set(5, 1);
        cd.roll(5, -1);
        this.MaxDate = cd.get(5);
        if (monthOfNumber == 1) {
            return -this.MaxDate;
        }
        return 1 - monthOfNumber;
    }

    public String getPreviousMonthEnd() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar lastDate = Calendar.getInstance();
        lastDate.add(2, -1);
        lastDate.set(5, 1);
        lastDate.roll(5, -1);
        return sdf.format(lastDate.getTime());
    }

    public String getNextMonthFirst() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar lastDate = Calendar.getInstance();
        lastDate.add(2, 1);
        lastDate.set(5, 1);
        return sdf.format(lastDate.getTime());
    }

    public String getNextMonthEnd() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar lastDate = Calendar.getInstance();
        lastDate.add(2, 1);
        lastDate.set(5, 1);
        lastDate.roll(5, -1);
        return sdf.format(lastDate.getTime());
    }

    public String getNextYearEnd() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar lastDate = Calendar.getInstance();
        lastDate.add(1, 1);
        lastDate.set(6, 1);
        lastDate.roll(6, -1);
        return sdf.format(lastDate.getTime());
    }

    public String getNextYearFirst() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar lastDate = Calendar.getInstance();
        lastDate.add(1, 1);
        lastDate.set(6, 1);
        return sdf.format(lastDate.getTime());
    }

    private int getMaxYear() {
        Calendar cd = Calendar.getInstance();
        cd.set(6, 1);
        cd.roll(6, -1);
        return cd.get(6);
    }

    private int getYearPlus() {
        Calendar cd = Calendar.getInstance();
        int yearOfNumber = cd.get(6);
        cd.set(6, 1);
        cd.roll(6, -1);
        int MaxYear2 = cd.get(6);
        if (yearOfNumber == 1) {
            return -MaxYear2;
        }
        return 1 - yearOfNumber;
    }

    public String getCurrentYearFirst() {
        int yearPlus = getYearPlus();
        GregorianCalendar currentDate = new GregorianCalendar();
        currentDate.add(5, yearPlus);
        return DateFormat.getDateInstance().format(currentDate.getTime());
    }

    public String getCurrentYearEnd() {
        return String.valueOf(new SimpleDateFormat("yyyy").format(new Date())) + "-12-31";
    }

    public String getPreviousYearFirst() {
        return String.valueOf(Integer.parseInt(new SimpleDateFormat("yyyy").format(new Date())) - 1) + "-1-1";
    }

    public String getPreviousYearEnd() {
        this.weeks--;
        int yearPlus = getYearPlus();
        GregorianCalendar currentDate = new GregorianCalendar();
        currentDate.add(5, (this.MaxYear * this.weeks) + yearPlus + (this.MaxYear - 1));
        return DateFormat.getDateInstance().format(currentDate.getTime());
    }

    public String getThisSeasonFirstTime(int month) {
        int[][] array = {new int[]{1, 2, 3}, new int[]{4, 5, 6}, new int[]{7, 8, 9}, new int[]{10, 11, 12}};
        int season = 1;
        if (month >= 1 && month <= 3) {
            season = 1;
        }
        if (month >= 4 && month <= 6) {
            season = 2;
        }
        if (month >= 7 && month <= 9) {
            season = 3;
        }
        if (month >= 10 && month <= 12) {
            season = 4;
        }
        int start_month = array[season - 1][0];
        int end_month = array[season - 1][2];
        int years_value = Integer.parseInt(new SimpleDateFormat("yyyy").format(new Date()));
        int lastDayOfMonth = getLastDayOfMonth(years_value, end_month);
        return String.valueOf(years_value) + "-" + start_month + "-" + 1;
    }

    public String getThisSeasonFinallyTime(int month) {
        int[][] array = {new int[]{1, 2, 3}, new int[]{4, 5, 6}, new int[]{7, 8, 9}, new int[]{10, 11, 12}};
        int season = 1;
        if (month >= 1 && month <= 3) {
            season = 1;
        }
        if (month >= 4 && month <= 6) {
            season = 2;
        }
        if (month >= 7 && month <= 9) {
            season = 3;
        }
        if (month >= 10 && month <= 12) {
            season = 4;
        }
        int i = array[season - 1][0];
        int end_month = array[season - 1][2];
        int years_value = Integer.parseInt(new SimpleDateFormat("yyyy").format(new Date()));
        return String.valueOf(years_value) + "-" + end_month + "-" + getLastDayOfMonth(years_value, end_month);
    }

    private int getLastDayOfMonth(int year, int month) {
        if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
            return 31;
        }
        if (month == 4 || month == 6 || month == 9 || month == 11) {
            return 30;
        }
        if (month != 2) {
            return 0;
        }
        if (isLeapYear(year)) {
            return 29;
        }
        return 28;
    }

    public boolean isLeapYear(int year) {
        return (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
    }

    public boolean isLeapYear2(int year) {
        return new GregorianCalendar().isLeapYear(year);
    }
}
