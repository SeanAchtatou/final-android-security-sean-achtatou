package com.baosteelOnline.common;

import cn.com.jit.ida.util.pki.cipher.Mechanism;
import com.jianq.misc.StringEx;

public class SHA1 {
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000e, code lost:
        if (r8.equals(com.jianq.misc.StringEx.Empty) != false) goto L_0x0010;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String Encrypt(java.lang.String r7, java.lang.String r8) {
        /*
            r6 = this;
            r2 = 0
            r3 = 0
            byte[] r0 = r7.getBytes()
            if (r8 == 0) goto L_0x0010
            java.lang.String r4 = ""
            boolean r4 = r8.equals(r4)     // Catch:{ NoSuchAlgorithmException -> 0x0023 }
            if (r4 == 0) goto L_0x0012
        L_0x0010:
            java.lang.String r8 = "MD5"
        L_0x0012:
            java.security.MessageDigest r2 = java.security.MessageDigest.getInstance(r8)     // Catch:{ NoSuchAlgorithmException -> 0x0023 }
            r2.update(r0)     // Catch:{ NoSuchAlgorithmException -> 0x0023 }
            byte[] r4 = r2.digest()     // Catch:{ NoSuchAlgorithmException -> 0x0023 }
            java.lang.String r3 = r6.bytes2Hex(r4)     // Catch:{ NoSuchAlgorithmException -> 0x0023 }
            r4 = r3
        L_0x0022:
            return r4
        L_0x0023:
            r1 = move-exception
            java.io.PrintStream r4 = java.lang.System.out
            java.lang.String r5 = "Invalid algorithm."
            r4.println(r5)
            r4 = 0
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baosteelOnline.common.SHA1.Encrypt(java.lang.String, java.lang.String):java.lang.String");
    }

    public String bytes2Hex(byte[] bts) {
        String des = StringEx.Empty;
        for (byte b : bts) {
            String tmp = Integer.toHexString(b & 255);
            if (tmp.length() == 1) {
                des = String.valueOf(des) + "0";
            }
            des = String.valueOf(des) + tmp;
        }
        return des;
    }

    public static void main(String[] args) {
        SHA1 sha1 = new SHA1();
        System.out.println("Source String:" + "admin");
        System.out.println("Encrypted String:");
        System.out.println("Use Def:" + sha1.Encrypt("admin", null));
        System.out.println("Use MD5:" + sha1.Encrypt("admin", Mechanism.MD5));
        System.out.println("Use SHA:" + sha1.Encrypt("admin", "SHA-1"));
        System.out.println("Use SHA-256:" + sha1.Encrypt("admin", "SHA-256"));
    }
}
