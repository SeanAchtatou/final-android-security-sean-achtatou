package com.baosteelOnline.common;

import android.app.Activity;
import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import com.andframework.common.ObjectStores;
import com.baosteelOnline.dl.Login_indexActivity;
import com.baosteelOnline.main.ExitApplication;
import com.jianq.misc.StringEx;

public class JQActivity extends Activity {
    private int i = 0;
    public RadioButton mIndexNavigation1;
    public RadioButton mIndexNavigation2;
    public RadioButton mIndexNavigation3;
    public RadioButton mIndexNavigation4;
    public RadioButton mIndexNavigation5;
    public RadioGroup mRadioderGroup;
    long timeTestEnd;
    long timeTestStart;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.timeTestStart = System.currentTimeMillis();
        this.i++;
        System.out.println("开始时间:" + this.timeTestStart);
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (getRequestedOrientation() != 1) {
            setRequestedOrientation(1);
        }
        if (this.i != 0) {
            this.timeTestEnd = System.currentTimeMillis();
            System.out.println("结束时间" + this.timeTestEnd);
            System.out.println("运行时间是" + (this.timeTestEnd - this.timeTestStart));
            if (this.timeTestEnd - this.timeTestStart > 1800000) {
                Toast.makeText(this, "您的登录已过期，请重新登录", 0).show();
                clearGlobe();
                ExitApplication.getInstance().startActivity(this, Login_indexActivity.class);
            }
        }
        super.onResume();
    }

    private void clearGlobe() {
        ObjectStores.getInst().putObject("password", StringEx.Empty);
        ObjectStores.getInst().putObject("userid", StringEx.Empty);
        ObjectStores.getInst().putObject("empId", StringEx.Empty);
        ObjectStores.getInst().putObject("parameter_userid", StringEx.Empty);
        ObjectStores.getInst().putObject("MJ_msg", StringEx.Empty);
        ObjectStores.getInst().putObject("segNo", StringEx.Empty);
        ObjectStores.getInst().putObject("customerId", StringEx.Empty);
        ObjectStores.getInst().putObject("customerName", StringEx.Empty);
        ObjectStores.getInst().putObject("userId", StringEx.Empty);
        ObjectStores.getInst().putObject("userName", StringEx.Empty);
        ObjectStores.getInst().putObject("memberId", StringEx.Empty);
        ObjectStores.getInst().putObject("positionName", StringEx.Empty);
        ObjectStores.getInst().putObject("segNo_zz_login", StringEx.Empty);
        ObjectStores.getInst().putObject("inputUserName", StringEx.Empty);
        ObjectStores.getInst().putObject("inputPassword", StringEx.Empty);
        ObjectStores.getInst().putObject("XH_msg", StringEx.Empty);
        ObjectStores.getInst().putObject("GfullName", StringEx.Empty);
        ObjectStores.getInst().putObject("parameter_usertokenid", StringEx.Empty);
        ObjectStores.getInst().putObject("companyCode", StringEx.Empty);
        ObjectStores.getInst().putObject("companyFullName", StringEx.Empty);
        ObjectStores.getInst().putObject("BSP_companyCode", StringEx.Empty);
        ObjectStores.getInst().putObject("G_User", StringEx.Empty);
        ObjectStores.getInst().putObject("XH_reStr", StringEx.Empty);
        ObjectStores.getInst().putObject("IEC_reStr", StringEx.Empty);
        ObjectStores.getInst().putObject("MJ_reStr", StringEx.Empty);
        ObjectStores.getInst().putObject("BGZX_reStr", StringEx.Empty);
        ObjectStores.getInst().putObject("reStr", "0");
        ObjectStores.getInst().putObject("contactid", StringEx.Empty);
        ObjectStores.getInst().putObject("buyerid", StringEx.Empty);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }
}
