package com.baosteelOnline.common;

import android.content.Context;
import android.database.Cursor;
import android.os.Environment;
import com.baosteelOnline.sql.DBHelper;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DataXml {
    public static final String BSP_COMPANYCODE = "bsp_companycode";
    public static final String FIRSTMODULE = "first_module";
    public static final String LOGINDATE = "LoginDate";
    public static final String LOG_LOGIN_ID = "_id";
    public static final String LOG_OPERATION_ID = "_id";
    public static final String MOBAPPID = "mob_app_id";
    public static final String MOBEQUIPMENTID = "MobEquipmentId";
    public static final String MOBLOCALVERSION = "MobLocalVersion";
    public static final String MOBOSVERSION = "MobOsVersion";
    public static final String OPERATETIME = "operate_time";
    public static final String POSITIONID = "positionId";
    public static final String SECONDMODULE = "second_module";
    private static final String TABLE_NAME = "log_operation_tbl";
    private static final String TABLE_NAMES = "log_login_tbl";
    public static final String THIRDMODULE = "third_module";
    public static final String USERNAME = "username";
    private DataBaseFactory db;
    private DBHelper dbHelper;
    private Cursor myCursor;
    String sql_operation = "Create table log_operation_tbl(_id integer primary key autoincrement,username text, bsp_companycode text, mob_app_id text, first_module text, second_module text, third_module text, operate_time text, positionId text, MobOsVersion text, MobEquipmentId text, MobLocalVersion text );";
    String sqls = "Create table log_login_tbl(_id integer primary key autoincrement,username text, bsp_companycode text, mob_app_id text, LoginDate text, MobOsVersion text, MobEquipmentId text, MobLocalVersion text);";

    public String ExistFileName() {
        String fileName = Environment.getExternalStorageDirectory() + "/Baosteel";
        File file = new File(fileName);
        if (!file.exists()) {
            file.mkdir();
        }
        return fileName;
    }

    public File DataOpeartionToTxt(Context context) {
        File file = new File(String.valueOf(ExistFileName()) + "/" + "YDopt" + new SimpleDateFormat("yyyyMMdd").format(new Date()).toString() + ".txt");
        if (!file.isFile()) {
            try {
                this.dbHelper = DataBaseFactory.getInstance(context);
                file.createNewFile();
                this.myCursor = this.dbHelper.selectOperation();
                FileWriter fw = new FileWriter(file);
                while (this.myCursor != null && this.myCursor.moveToNext()) {
                    fw.write(this.myCursor.getString(this.myCursor.getColumnIndex("username")));
                    fw.write("$#$");
                    fw.write(this.myCursor.getString(this.myCursor.getColumnIndex("bsp_companycode")));
                    fw.write("$#$");
                    fw.write(this.myCursor.getString(this.myCursor.getColumnIndex("mob_app_id")));
                    fw.write("$#$");
                    fw.write(this.myCursor.getString(this.myCursor.getColumnIndex("first_module")));
                    fw.write("$#$");
                    fw.write(this.myCursor.getString(this.myCursor.getColumnIndex("second_module")));
                    fw.write("$#$");
                    fw.write(this.myCursor.getString(this.myCursor.getColumnIndex("third_module")));
                    fw.write("$#$");
                    fw.write(this.myCursor.getString(this.myCursor.getColumnIndex("operate_time")));
                    fw.write("$#$");
                    fw.write(this.myCursor.getString(this.myCursor.getColumnIndex("positionId")));
                    fw.write("$#$");
                    fw.write(this.myCursor.getString(this.myCursor.getColumnIndex("MobOsVersion")));
                    fw.write("$#$");
                    fw.write(this.myCursor.getString(this.myCursor.getColumnIndex("MobEquipmentId")));
                    fw.write("$#$");
                    fw.write(this.myCursor.getString(this.myCursor.getColumnIndex("MobLocalVersion")));
                    fw.write("$#$");
                    fw.write("\n");
                }
                fw.flush();
                fw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            file.delete();
            try {
                this.dbHelper = DataBaseFactory.getInstance(context);
                file.createNewFile();
                this.myCursor = this.dbHelper.selectOperation();
                FileWriter fw2 = new FileWriter(file);
                while (this.myCursor != null && this.myCursor.moveToNext()) {
                    fw2.write(this.myCursor.getString(this.myCursor.getColumnIndex("username")));
                    fw2.write("$#$");
                    fw2.write(this.myCursor.getString(this.myCursor.getColumnIndex("bsp_companycode")));
                    fw2.write("$#$");
                    fw2.write(this.myCursor.getString(this.myCursor.getColumnIndex("mob_app_id")));
                    fw2.write("$#$");
                    fw2.write(this.myCursor.getString(this.myCursor.getColumnIndex("first_module")));
                    fw2.write("$#$");
                    fw2.write(this.myCursor.getString(this.myCursor.getColumnIndex("second_module")));
                    fw2.write("$#$");
                    fw2.write(this.myCursor.getString(this.myCursor.getColumnIndex("third_module")));
                    fw2.write("$#$");
                    fw2.write(this.myCursor.getString(this.myCursor.getColumnIndex("operate_time")));
                    fw2.write("$#$");
                    fw2.write(this.myCursor.getString(this.myCursor.getColumnIndex("positionId")));
                    fw2.write("$#$");
                    fw2.write(this.myCursor.getString(this.myCursor.getColumnIndex("MobOsVersion")));
                    fw2.write("$#$");
                    fw2.write(this.myCursor.getString(this.myCursor.getColumnIndex("MobEquipmentId")));
                    fw2.write("$#$");
                    fw2.write(this.myCursor.getString(this.myCursor.getColumnIndex("MobLocalVersion")));
                    fw2.write("$#$");
                    fw2.write("\n");
                }
                fw2.flush();
                fw2.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        return file;
    }

    public File DataLoginToTxt(Context context) {
        File file = new File(String.valueOf(ExistFileName()) + "/" + "YDlogin" + new SimpleDateFormat("yyyyMMdd").format(new Date()).toString() + ".txt");
        if (!file.isFile()) {
            try {
                this.dbHelper = DataBaseFactory.getInstance(context);
                file.createNewFile();
                this.myCursor = this.dbHelper.selectLogin();
                FileWriter fw = new FileWriter(file);
                while (this.myCursor != null && this.myCursor.moveToNext()) {
                    fw.write(this.myCursor.getString(this.myCursor.getColumnIndex("username")));
                    fw.write("$#$");
                    fw.write(this.myCursor.getString(this.myCursor.getColumnIndex("bsp_companycode")));
                    fw.write("$#$");
                    fw.write(this.myCursor.getString(this.myCursor.getColumnIndex("mob_app_id")));
                    fw.write("$#$");
                    fw.write(this.myCursor.getString(this.myCursor.getColumnIndex("LoginDate")));
                    fw.write("$#$");
                    fw.write(this.myCursor.getString(this.myCursor.getColumnIndex("MobOsVersion")));
                    fw.write("$#$");
                    fw.write(this.myCursor.getString(this.myCursor.getColumnIndex("MobEquipmentId")));
                    fw.write("$#$");
                    fw.write(this.myCursor.getString(this.myCursor.getColumnIndex("MobLocalVersion")));
                    fw.write("$#$");
                    fw.write("\n");
                }
                fw.flush();
                fw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            file.delete();
            try {
                this.dbHelper = DataBaseFactory.getInstance(context);
                file.createNewFile();
                this.myCursor = this.dbHelper.selectLogin();
                FileWriter fw2 = new FileWriter(file);
                while (this.myCursor != null && this.myCursor.moveToNext()) {
                    fw2.write(this.myCursor.getString(this.myCursor.getColumnIndex("username")));
                    fw2.write("$#$");
                    fw2.write(this.myCursor.getString(this.myCursor.getColumnIndex("bsp_companycode")));
                    fw2.write("$#$");
                    fw2.write(this.myCursor.getString(this.myCursor.getColumnIndex("mob_app_id")));
                    fw2.write("$#$");
                    fw2.write(this.myCursor.getString(this.myCursor.getColumnIndex("LoginDate")));
                    fw2.write("$#$");
                    fw2.write(this.myCursor.getString(this.myCursor.getColumnIndex("MobOsVersion")));
                    fw2.write("$#$");
                    fw2.write(this.myCursor.getString(this.myCursor.getColumnIndex("MobEquipmentId")));
                    fw2.write("$#$");
                    fw2.write(this.myCursor.getString(this.myCursor.getColumnIndex("MobLocalVersion")));
                    fw2.write("$#$");
                    fw2.write("\n");
                }
                fw2.flush();
                fw2.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        return file;
    }
}
