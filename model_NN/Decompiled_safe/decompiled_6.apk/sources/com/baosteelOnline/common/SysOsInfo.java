package com.baosteelOnline.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import com.baosteelOnline.model.sysOsInfoModel;
import com.jianq.misc.StringEx;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import org.apache.http.conn.util.InetAddressUtils;

public class SysOsInfo {
    sysOsInfoModel sysOsInfo = new sysOsInfoModel();

    public sysOsInfoModel sysOsInfo(Context context) {
        SharedPreferences resolutionSp = context.getSharedPreferences("Resolution", 0);
        String resolution1 = resolutionSp.getString("resolution1", StringEx.Empty);
        String resolution2 = resolutionSp.getString("resolution2", StringEx.Empty);
        String deviceid = resolutionSp.getString("deviceid", StringEx.Empty);
        String network = resolutionSp.getString("network", StringEx.Empty);
        this.sysOsInfo.setAppcode("com.baosight.ets");
        this.sysOsInfo.setDeviceid(deviceid);
        this.sysOsInfo.setNetwork(network);
        this.sysOsInfo.setOs("AnroidPhone");
        this.sysOsInfo.setOsVersion("Android" + Build.VERSION.RELEASE);
        this.sysOsInfo.setResolution1(resolution1);
        this.sysOsInfo.setResolution2(resolution2);
        this.sysOsInfo.setIp(GetHostIp());
        return this.sysOsInfo;
    }

    public String GetHostIp() {
        try {
            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
            while (en.hasMoreElements()) {
                Enumeration<InetAddress> ipAddr = en.nextElement().getInetAddresses();
                while (true) {
                    if (ipAddr.hasMoreElements()) {
                        InetAddress inetAddress = ipAddr.nextElement();
                        if (!inetAddress.isLoopbackAddress()) {
                            String ipv4 = inetAddress.getHostAddress();
                            if (InetAddressUtils.isIPv4Address(ipv4)) {
                                return ipv4;
                            }
                        }
                    }
                }
            }
        } catch (Exception | SocketException e) {
        }
        return null;
    }
}
