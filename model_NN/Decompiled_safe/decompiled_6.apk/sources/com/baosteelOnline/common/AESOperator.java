package com.baosteelOnline.common;

import com.jianq.net.JQBasicNetwork;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AESOperator {
    private static AESOperator instance = null;
    private String ivParameter = "0123456789abcdef";
    private String sKey = "BsteelPay1.0.0TV";

    private AESOperator() {
    }

    public static AESOperator getInstance() {
        if (instance == null) {
            instance = new AESOperator();
        }
        return instance;
    }

    public String encrypt(String sSrc) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(1, new SecretKeySpec(this.sKey.getBytes(), "AES"), new IvParameterSpec(this.ivParameter.getBytes()));
        return Base64Encoder.encode(cipher.doFinal(sSrc.getBytes("utf-8")));
    }

    public String decrypt(String sSrc) throws Exception {
        try {
            SecretKeySpec skeySpec = new SecretKeySpec(this.sKey.getBytes(JQBasicNetwork.ASCII), "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(2, skeySpec, new IvParameterSpec(this.ivParameter.getBytes()));
            return new String(cipher.doFinal(Base64Decoder.decodeToBytes(sSrc)), "utf-8");
        } catch (Exception e) {
            return null;
        }
    }

    public static void main(String[] args) throws Exception {
        System.out.println("admin");
        long lStart = System.currentTimeMillis();
        String enString = getInstance().encrypt("admin");
        System.out.println("加密后的字串是：" + enString);
        System.out.println("加密耗时：" + (System.currentTimeMillis() - lStart) + "毫秒");
        long lStart2 = System.currentTimeMillis();
        System.out.println("解密后的字串是：" + getInstance().decrypt(enString));
        System.out.println("解密耗时：" + (System.currentTimeMillis() - lStart2) + "毫秒");
    }
}
