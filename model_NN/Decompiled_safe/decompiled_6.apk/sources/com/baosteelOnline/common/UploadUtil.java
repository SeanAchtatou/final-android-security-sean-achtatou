package com.baosteelOnline.common;

import android.util.Log;
import cn.com.jit.pnxclient.constant.PNXConfigConstant;
import com.jianq.net.JQBasicNetwork;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class UploadUtil {
    String result = null;

    public String uploadFile(String srcPath, String uploadUrl) {
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(uploadUrl).openConnection();
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty(JQBasicNetwork.CONN_DIRECTIVE, JQBasicNetwork.CONN_KEEP_ALIVE);
            httpURLConnection.setRequestProperty("Charset", JQBasicNetwork.UTF_8);
            httpURLConnection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + "******");
            httpURLConnection.setRequestProperty("fileName", srcPath.substring(srcPath.lastIndexOf("/") + 1));
            DataOutputStream dos = new DataOutputStream(httpURLConnection.getOutputStream());
            dos.writeBytes(String.valueOf("--") + "******" + PNXConfigConstant.RESP_SPLIT_2);
            dos.writeBytes("Content-Disposition: form-data; name=\"imgFile\"; filename=\"" + srcPath.substring(srcPath.lastIndexOf("/") + 1) + "\"" + PNXConfigConstant.RESP_SPLIT_2);
            dos.writeBytes(PNXConfigConstant.RESP_SPLIT_2);
            FileInputStream fis = new FileInputStream(srcPath);
            byte[] buffer = new byte[8192];
            while (true) {
                int count = fis.read(buffer);
                if (count == -1) {
                    break;
                }
                dos.write(buffer, 0, count);
            }
            Log.e("file", "--" + dos.size() + "--");
            fis.close();
            dos.writeBytes(PNXConfigConstant.RESP_SPLIT_2);
            dos.writeBytes(String.valueOf("--") + "******" + "--" + PNXConfigConstant.RESP_SPLIT_2);
            dos.flush();
            InputStream is = httpURLConnection.getInputStream();
            this.result = new BufferedReader(new InputStreamReader(is, "utf-8")).readLine();
            dos.close();
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("EXCEPTION", e.getMessage());
        }
        return this.result;
    }
}
