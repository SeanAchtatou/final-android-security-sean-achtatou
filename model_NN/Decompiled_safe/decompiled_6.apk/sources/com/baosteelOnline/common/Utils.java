package com.baosteelOnline.common;

import java.io.IOException;
import java.util.Properties;

public class Utils {
    public static Properties getNetConfigProperties() {
        Properties props = new Properties();
        try {
            props.load(Utils.class.getResourceAsStream("/project.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return props;
    }
}
