package com.baosteelOnline.common;

import android.content.Context;
import com.baosteelOnline.sql.DBHelper;

public class DataBaseFactory {
    private static DBHelper databasehelper = null;

    public static DBHelper getInstance(Context context) {
        if (databasehelper == null) {
            databasehelper = new DBHelper(context);
        }
        return databasehelper;
    }

    public static void distroyDataBaseHelper(Context context) {
        databasehelper.DropDatabase(context);
        System.gc();
    }
}
