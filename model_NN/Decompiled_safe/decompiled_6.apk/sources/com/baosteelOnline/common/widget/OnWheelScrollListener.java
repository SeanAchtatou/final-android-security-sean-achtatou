package com.baosteelOnline.common.widget;

public interface OnWheelScrollListener {
    void onScrollingFinished(WheelView wheelView);

    void onScrollingStarted(WheelView wheelView);
}
