package com.baosteelOnline.common.widget;

public interface OnWheelClickedListener {
    void onItemClicked(WheelView wheelView, int i);
}
