package com.baosteelOnline.common.widget;

import android.content.Context;
import android.widget.SimpleAdapter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MySimpleAdapter extends SimpleAdapter {
    private ArrayList<HashMap<String, Object>> mItemArrayList;
    private HashMap<String, Object> mItemHashMap;

    public MySimpleAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) {
        super(context, data, resource, from, to);
        this.mItemArrayList = (ArrayList) data;
    }
}
