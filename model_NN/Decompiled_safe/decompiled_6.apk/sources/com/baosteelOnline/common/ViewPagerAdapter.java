package com.baosteelOnline.common;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

public class ViewPagerAdapter extends PagerAdapter {
    private ArrayList<View> viewList = new ArrayList<>();

    public ViewPagerAdapter(ArrayList<View> viewList2) {
        this.viewList = viewList2;
    }

    public int getCount() {
        return this.viewList.size();
    }

    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == arg1;
    }

    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(this.viewList.get(position));
    }

    public Object instantiateItem(ViewGroup container, int position) {
        container.addView(this.viewList.get(position));
        return this.viewList.get(position);
    }
}
