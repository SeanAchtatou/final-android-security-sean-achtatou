package com.baosteelOnline.main;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import com.andframework.common.ObjectStores;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ExitApplication extends Application {
    private static ExitApplication instance;
    private List<Activity> activityList = new LinkedList();

    private ExitApplication() {
    }

    public static ExitApplication getInstance() {
        if (instance == null) {
            instance = new ExitApplication();
        }
        return instance;
    }

    public void addActivity(Activity activity) {
        this.activityList.add(activity);
        showsAllActivity("add");
    }

    public void deleteActivityFront(int iIndex) {
        int size = this.activityList.size();
        System.out.println("iIndex-----:" + iIndex);
        System.out.println("size-----:" + this.activityList.size());
        showsAllActivity("delete前");
        int j = iIndex;
        while (j < this.activityList.size()) {
            this.activityList.get(this.activityList.size() - 1).finish();
            this.activityList.remove(this.activityList.size() - 1);
        }
        showsAllActivity("delete后");
    }

    public void startActivity(Activity activity, Class<?> cls) {
        startActivity(activity, cls, null);
    }

    public void back(Activity activity) {
        showsAllActivity("back前:");
        activity.finish();
        this.activityList.remove(activity);
        showsAllActivity("back后:");
    }

    public void back(int i) {
        deleteActivityFront((this.activityList.size() + i) - 1);
        showsAllActivity("back后:");
    }

    public void startActivity(Activity activity, Class<?> cls, HashMap hashmap) {
        startActivity(activity, cls, hashmap, 0);
    }

    public void startActivity(Activity activity, Class<?> cls, HashMap hashmap, int type) {
        int existIndex = isExist(cls.getName());
        if (existIndex != 0) {
            deleteActivityFront(existIndex);
        }
        Intent intent = new Intent(activity, cls);
        if (hashmap != null) {
            for (Map.Entry obj : hashmap.entrySet()) {
                intent.putExtra(obj.getKey().toString(), obj.getValue().toString());
            }
        }
        activity.startActivity(intent);
        if (type != 0) {
            back(activity);
        }
        showsAllActivity("start");
    }

    public int isExist(String pointAct) {
        for (int i = 0; i < this.activityList.size(); i++) {
            if (this.activityList.get(i).getClass().getName().equals(pointAct)) {
                return i;
            }
        }
        return 0;
    }

    public void showsAllActivity(String name) {
        int i = 0;
        System.out.print(name);
        for (Activity activity : this.activityList) {
            i++;
            System.out.println(String.valueOf(i) + ":--------:" + activity.getClass().getName());
        }
    }

    public void exit() {
        for (Activity activity : this.activityList) {
            System.out.println("结束Activity的名字-------:" + activity.getClass().getName());
            activity.finish();
        }
        this.activityList = null;
        clearAll();
        try {
            System.exit(0);
        } catch (Exception e) {
            System.exit(1);
        }
    }

    public void clearAll() {
        ObjectStores.getInst().removeObject("inputUserName");
        ObjectStores.getInst().removeObject("inputPassword");
        ObjectStores.getInst().removeObject("XH_msg");
        ObjectStores.getInst().removeObject("GfullName");
        ObjectStores.getInst().removeObject("parameter_usertokenid");
        ObjectStores.getInst().removeObject("companyCode");
        ObjectStores.getInst().removeObject("companyFullName");
        ObjectStores.getInst().removeObject("BSP_companyCode");
        ObjectStores.getInst().removeObject("G_User");
        ObjectStores.getInst().removeObject("XH_reStr");
        ObjectStores.getInst().removeObject("IEC_reStr");
        ObjectStores.getInst().removeObject("BGZX_reStr");
        ObjectStores.getInst().removeObject("reStr");
        ObjectStores.getInst().removeObject("contactid");
        ObjectStores.getInst().removeObject("buyerid");
    }
}
