package com.baosteelOnline.main;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import com.andframework.common.ObjectStores;
import com.andframework.config.FrameworkConfig;
import com.andframework.zmfile.ZMFilePath;
import com.ant.liao.GifView;
import com.baosteelOnline.R;
import com.baosteelOnline.common.DataBaseFactory;
import com.baosteelOnline.common.JQActivity;
import com.baosteelOnline.common.SysConfig;
import com.baosteelOnline.common.SysOsInfo;
import com.baosteelOnline.model.sysConfigModel;
import com.baosteelOnline.model.sysOsInfoModel;
import com.baosteelOnline.sql.DBHelper;
import com.baosteelOnline.xhjy.CyclicGoodsIndexActivity;
import com.jianq.misc.StringEx;
import java.util.Timer;
import java.util.TimerTask;

public class MainHelloActivity extends JQActivity {
    private DataBaseFactory db;
    private DBHelper dbHelper;
    ProgressDialog progressDialog;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ExitApplication.getInstance().addActivity(this);
        this.dbHelper = DataBaseFactory.getInstance(this);
        this.dbHelper.insertOperation("启动", "启动", "启动");
        requestWindowFeature(1);
        FrameworkConfig.getInst(this, R.xml.config);
        ZMFilePath.APPROOT = "jianqapp";
        View view = View.inflate(this, R.layout.main_hello, null);
        GifView gf1 = (GifView) view.findViewById(R.id.gif1);
        gf1.setBackgroundResource(R.drawable.df);
        gf1.setGifImageType(GifView.GifImageType.COVER);
        setContentView(view);
        String imei = ((TelephonyManager) getSystemService("phone")).getDeviceId();
        ObjectStores.getInst().putObject("deviceid", imei);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        ObjectStores.getInst().putObject("resolution1", Integer.valueOf(dm.widthPixels));
        ObjectStores.getInst().putObject("resolution2", Integer.valueOf(dm.heightPixels));
        String network = getAPNType(this);
        ObjectStores.getInst().putObject("network", network);
        SharedPreferences.Editor editorSp = getSharedPreferences("Resolution", 0).edit();
        editorSp.putString("resolution1", new StringBuilder(String.valueOf(dm.widthPixels)).toString());
        editorSp.putString("resolution2", new StringBuilder(String.valueOf(dm.heightPixels)).toString());
        editorSp.putString("deviceid", imei);
        editorSp.putString("network", network);
        editorSp.commit();
        sysOsInfoModel soim = new SysOsInfo().sysOsInfo(this);
        sysConfigModel syss = new SysConfig().setSysconfigbs();
        SharedPreferences.Editor editor = getSharedPreferences("login_username", 2).edit();
        editor.putString("appcode", soim.getAppcode());
        editor.putString("network", soim.getNetwork());
        editor.putString("os", soim.getOs());
        editor.putString("osVersion", soim.getOsVersion());
        editor.putString("resolution1", soim.getResolution1());
        editor.putString("resolution2", soim.getResolution2());
        editor.putString("deviceid", soim.getDeviceid());
        editor.putString("projectName", syss.getProjectName());
        editor.commit();
        final Intent intent = new Intent(this, CyclicGoodsIndexActivity.class);
        new Timer().schedule(new TimerTask() {
            public void run() {
                MainHelloActivity.this.startActivity(intent);
                MainHelloActivity.this.finish();
            }
        }, 2000);
    }

    public String getAPNType(Context context) {
        String netType = StringEx.Empty;
        NetworkInfo networkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (networkInfo == null) {
            return netType;
        }
        int nType = networkInfo.getType();
        if (nType == 0) {
            Log.e("networkInfo.getExtraInfo()", "networkInfo.getExtraInfo() is " + networkInfo.getExtraInfo());
            if (networkInfo.getExtraInfo().toLowerCase().equals("cmnet")) {
                netType = "CMNET";
            } else {
                netType = "CMWAP";
            }
        } else if (nType == 1) {
            netType = "WIFI";
        }
        return netType;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4 && event.getRepeatCount() == 0) {
            System.out.println("按得是返回");
            new AlertDialog.Builder(this).setTitle((int) R.string.app_name).setIcon((int) R.drawable.icon).setMessage("您确认要退出吗？").setNegativeButton("取消", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            }).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    MainHelloActivity.this.clearAll();
                    MainHelloActivity.this.finish();
                }
            }).create().show();
            return true;
        }
        System.out.println("按得不是返回");
        return false;
    }

    public void clearAll() {
        System.out.println("clearAll方法");
        ObjectStores.getInst().putObject("inputUserName", StringEx.Empty);
        ObjectStores.getInst().putObject("inputPassword", StringEx.Empty);
        ObjectStores.getInst().putObject("XH_msg", StringEx.Empty);
        ObjectStores.getInst().putObject("GfullName", StringEx.Empty);
        ObjectStores.getInst().putObject("parameter_usertokenid", StringEx.Empty);
        ObjectStores.getInst().putObject("companyCode", StringEx.Empty);
        ObjectStores.getInst().putObject("companyFullName", StringEx.Empty);
        ObjectStores.getInst().putObject("BSP_companyCode", StringEx.Empty);
        ObjectStores.getInst().putObject("G_User", StringEx.Empty);
        ObjectStores.getInst().putObject("XH_reStr", StringEx.Empty);
        ObjectStores.getInst().putObject("IEC_reStr", StringEx.Empty);
        ObjectStores.getInst().putObject("BGZX_reStr", StringEx.Empty);
        ObjectStores.getInst().putObject("reStr", "0");
        ObjectStores.getInst().putObject("contactid", StringEx.Empty);
        ObjectStores.getInst().putObject("buyerid", StringEx.Empty);
        ObjectStores.getInst().putObject("segNo", StringEx.Empty);
        ObjectStores.getInst().putObject("segNo_zz", StringEx.Empty);
        ObjectStores.getInst().putObject("memberId", StringEx.Empty);
    }
}
