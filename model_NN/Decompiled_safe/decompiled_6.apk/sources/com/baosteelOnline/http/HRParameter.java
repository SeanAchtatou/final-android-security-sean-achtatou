package com.baosteelOnline.http;

import org.json.JSONException;
import org.json.JSONObject;

public class HRParameter {
    private String bizObj;
    private JSONObject sysObj = new JSONObject("{\"appcode\":\"\",\"compressdata\":\"false\",\"encryptdata\":\"false\",\"keycode\":\"pkznf6585535555660675056396297413\"}");

    public void setBizObj(String bizObj2) {
        this.bizObj = bizObj2;
    }

    public void setMethod(String method) throws JSONException {
        this.sysObj.put("method", method);
    }

    public String toString() {
        return "xmas-json={\"biz\":\"" + this.bizObj + "\",\"sys\":" + this.sysObj.toString() + "}";
    }
}
