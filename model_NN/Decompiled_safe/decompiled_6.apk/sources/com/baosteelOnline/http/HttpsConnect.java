package com.baosteelOnline.http;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.baosteelOnline.R;
import com.jianq.net.JQSslNetwork;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class HttpsConnect {
    private static final String CLIENT_TRUST_PASSWORD = "123456";
    private static final String HTTS_URL = "https://m.bsteel.net:8000/";
    private static final String TAG = "HttpsDemo";
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public String data;
    /* access modifiers changed from: private */
    public HttpsCallBack httpsCallBack = null;
    private String method;
    /* access modifiers changed from: private */
    public String result;
    private Runnable run;
    private String sessionId;

    public void setData(String data2) {
        this.data = data2;
    }

    public String getMethod() {
        return this.method;
    }

    public void setMethod(String method2) {
        this.method = method2;
    }

    public void setSessionId(String sessionId2) {
        this.sessionId = sessionId2;
    }

    public String getSessionId() {
        return this.sessionId;
    }

    public void setHttpsCallBack(HttpsCallBack httpsCallBack2) {
        this.httpsCallBack = httpsCallBack2;
    }

    public HttpsConnect(Context context2) {
        this.context = context2;
    }

    public void trySslConnct(String sslUrl) throws Exception {
        JQSslNetwork sslNetwork = new JQSslNetwork(this.context, R.raw.jqserver4android, CLIENT_TRUST_PASSWORD);
        List<NameValuePair> parameters = new ArrayList<>();
        parameters.add(new BasicNameValuePair("xmas-json", this.data));
        HttpResponse resp = sslNetwork.getResponse(sslUrl, parameters);
        int statusCode = resp.getStatusLine().getStatusCode();
        InputStream inputs = resp.getEntity().getContent();
        byte[] buf = new byte[((int) resp.getEntity().getContentLength())];
        inputs.read(buf);
        Log.d(TAG, "res:" + new String(buf));
    }

    public void send() {
        try {
            HandlerThread handlerThread = new HandlerThread("handler_thread");
            handlerThread.start();
            new myHandler(handlerThread.getLooper()).obtainMessage().sendToTarget();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Main", e.getMessage());
        }
    }

    private class ConnectTask extends AsyncTask<Integer, Integer, String> {
        private ConnectTask() {
        }

        /* synthetic */ ConnectTask(HttpsConnect httpsConnect, ConnectTask connectTask) {
            this();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Integer... params) {
            try {
                JQSslNetwork sslNetwork = new JQSslNetwork(HttpsConnect.this.context, R.raw.jqserver4android, HttpsConnect.CLIENT_TRUST_PASSWORD);
                List<NameValuePair> parameters = new ArrayList<>();
                parameters.add(new BasicNameValuePair("xmas-json", HttpsConnect.this.data));
                System.out.println("getSessionId()：" + HttpsConnect.this.getSessionId());
                sslNetwork.setXMasSessionId(HttpsConnect.this.getSessionId());
                HttpResponse resp = sslNetwork.getResponse(HttpsConnect.HTTS_URL + HttpsConnect.this.getMethod(), parameters);
                InputStream inputs = resp.getEntity().getContent();
                byte[] buf = new byte[((int) resp.getEntity().getContentLength())];
                inputs.read(buf);
                HttpsConnect.this.result = new String(buf);
                Log.d(HttpsConnect.TAG, "res:" + HttpsConnect.this.result);
                HttpsConnect.this.httpsCallBack.setCallBack(HttpsConnect.this.result);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return HttpsConnect.this.result;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            super.onPostExecute((Object) result);
        }
    }

    class myHandler extends Handler {
        public myHandler() {
        }

        public myHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            new ConnectTask(HttpsConnect.this, null).execute(new Integer[0]);
        }
    }
}
