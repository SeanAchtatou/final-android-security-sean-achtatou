package com.baosteelOnline.http;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.jianq.common.JQFrameworkConfig;
import com.jianq.helper.JQNetworkHelper;
import com.jianq.ui.JQUICallBack;
import com.jianq.util.JQUtils;
import org.json.JSONException;

public class HttpConnect {
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public String data;
    /* access modifiers changed from: private */
    public JQUICallBack httpCallBack = null;
    /* access modifiers changed from: private */
    public boolean isNetwork;
    /* access modifiers changed from: private */
    public String method;
    /* access modifiers changed from: private */
    public String result;
    private String sessionId;

    public void setData(String data2) {
        this.data = data2;
    }

    public String getMethod() {
        return this.method;
    }

    public void setMethod(String method2) {
        this.method = method2;
    }

    public void setSessionId(String sessionId2) {
        this.sessionId = sessionId2;
    }

    public String getSessionId() {
        return this.sessionId;
    }

    public void setHttpCallBack(JQUICallBack httpCallBack2) {
        this.httpCallBack = httpCallBack2;
    }

    public HttpConnect(Context context2) {
        this.context = context2;
    }

    public void send() {
        try {
            this.isNetwork = JQUtils.isNetworkAvailable(this.context);
            if (this.isNetwork) {
                JQNetworkHelper networkHelper = new JQNetworkHelper(this.context);
                networkHelper.progress = false;
                HRParameter parameter = null;
                try {
                    HRParameter parameter2 = new HRParameter();
                    try {
                        parameter2.setMethod(this.method);
                        parameter2.setBizObj(this.data);
                        parameter = parameter2;
                    } catch (JSONException e) {
                        parameter = parameter2;
                        jsonError("JSON解析失败-login-send");
                        networkHelper.open("get", "http://www.baidu.com");
                        networkHelper.setCallBack(this.httpCallBack);
                        System.out.println("parameter==>" + parameter.toString());
                        networkHelper.send(parameter.toString());
                        return;
                    }
                } catch (JSONException e2) {
                    jsonError("JSON解析失败-login-send");
                    networkHelper.open("get", "http://www.baidu.com");
                    networkHelper.setCallBack(this.httpCallBack);
                    System.out.println("parameter==>" + parameter.toString());
                    networkHelper.send(parameter.toString());
                    return;
                }
                networkHelper.open("get", "http://www.baidu.com");
                networkHelper.setCallBack(this.httpCallBack);
                System.out.println("parameter==>" + parameter.toString());
                networkHelper.send(parameter.toString());
                return;
            }
            System.out.println("============");
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    private class ConnectTask extends AsyncTask<Integer, Integer, String> {
        private ConnectTask() {
        }

        /* synthetic */ ConnectTask(HttpConnect httpConnect, ConnectTask connectTask) {
            this();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Integer... params) {
            try {
                HttpConnect.this.isNetwork = JQUtils.isNetworkAvailable(HttpConnect.this.context);
                if (HttpConnect.this.isNetwork) {
                    JQNetworkHelper networkHelper = new JQNetworkHelper(HttpConnect.this.context);
                    networkHelper.progress = false;
                    HRParameter parameter = null;
                    try {
                        HRParameter parameter2 = new HRParameter();
                        try {
                            parameter2.setMethod(HttpConnect.this.method);
                            parameter2.setBizObj(HttpConnect.this.data);
                            parameter = parameter2;
                        } catch (JSONException e) {
                            parameter = parameter2;
                            HttpConnect.this.jsonError("JSON解析失败-login-send");
                            networkHelper.open("post", String.valueOf(JQFrameworkConfig.getInst().getDomain()) + HttpConnect.this.method);
                            networkHelper.setCallBack(HttpConnect.this.httpCallBack);
                            System.out.println("parameter==>" + parameter.toString());
                            networkHelper.send(parameter.toString());
                            return HttpConnect.this.result;
                        }
                    } catch (JSONException e2) {
                        HttpConnect.this.jsonError("JSON解析失败-login-send");
                        networkHelper.open("post", String.valueOf(JQFrameworkConfig.getInst().getDomain()) + HttpConnect.this.method);
                        networkHelper.setCallBack(HttpConnect.this.httpCallBack);
                        System.out.println("parameter==>" + parameter.toString());
                        networkHelper.send(parameter.toString());
                        return HttpConnect.this.result;
                    }
                    networkHelper.open("post", String.valueOf(JQFrameworkConfig.getInst().getDomain()) + HttpConnect.this.method);
                    networkHelper.setCallBack(HttpConnect.this.httpCallBack);
                    System.out.println("parameter==>" + parameter.toString());
                    networkHelper.send(parameter.toString());
                    return HttpConnect.this.result;
                }
                System.out.println("============");
                return HttpConnect.this.result;
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            super.onPostExecute((Object) result);
        }
    }

    class myHandler extends Handler {
        public myHandler() {
        }

        public myHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            new ConnectTask(HttpConnect.this, null).execute(new Integer[0]);
        }
    }

    /* access modifiers changed from: private */
    public void jsonError(String message) {
        System.out.println("-----------------------------");
    }
}
