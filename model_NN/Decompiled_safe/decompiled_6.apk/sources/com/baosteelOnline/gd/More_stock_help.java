package com.baosteelOnline.gd;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.baosteelOnline.R;
import com.baosteelOnline.common.DataBaseFactory;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.sql.DBHelper;
import com.baosteelOnline.xhjy.Xhjy_more;
import java.util.ArrayList;

public class More_stock_help extends Activity {
    private DataBaseFactory db;
    private DBHelper dbHelper;
    private ImageView imageView;
    private ImageView[] imageViews;
    private View pageView1;
    private View pageView2;
    private View pageView3;
    private View pageView4;
    private View pageView5;
    /* access modifiers changed from: private */
    public ArrayList<View> pageViews;
    private ViewPager viewPager;
    private ViewGroup viewPics;
    private View viewPoints;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        ExitApplication.getInstance().addActivity(this);
        setContentView((int) R.layout.gd_helps);
        this.dbHelper = DataBaseFactory.getInstance(this);
        this.dbHelper.insertOperation("循环资源/钢材交易", "更多", "使用帮助页面");
        LayoutInflater inflater = getLayoutInflater();
        this.pageView1 = inflater.inflate((int) R.layout.page_help01, (ViewGroup) null);
        this.pageView2 = inflater.inflate((int) R.layout.page_help02, (ViewGroup) null);
        this.pageView3 = inflater.inflate((int) R.layout.page_help03, (ViewGroup) null);
        this.pageView4 = inflater.inflate((int) R.layout.page_help04, (ViewGroup) null);
        this.pageView5 = inflater.inflate((int) R.layout.page_help05, (ViewGroup) null);
        this.pageViews = new ArrayList<>();
        this.pageViews.add(this.pageView1);
        this.pageViews.add(this.pageView2);
        this.pageViews.add(this.pageView3);
        this.pageViews.add(this.pageView4);
        this.pageViews.add(this.pageView5);
        this.viewPager = (ViewPager) findViewById(R.id.guidePages);
        this.viewPager.setAdapter(new GuidePageAdapter());
        this.viewPager.setOnPageChangeListener(new GuidePageChangeListener());
    }

    public void exit_button(View v) {
        Intent intent = new Intent(this, Xhjy_more.class);
        intent.putExtra("part", getIntent().getExtras().getString("part"));
        startActivity(intent);
        finish();
    }

    public void start_button(View v) {
        Intent intent = new Intent(this, Xhjy_more.class);
        intent.putExtra("part", getIntent().getExtras().getString("part"));
        startActivity(intent);
        finish();
    }

    class GuidePageAdapter extends PagerAdapter {
        GuidePageAdapter() {
        }

        public void destroyItem(View v, int position, Object arg2) {
            ((ViewPager) v).removeView((View) More_stock_help.this.pageViews.get(position));
        }

        public void finishUpdate(View arg0) {
        }

        public int getCount() {
            return More_stock_help.this.pageViews.size();
        }

        public Object instantiateItem(View v, int position) {
            ((ViewPager) v).addView((View) More_stock_help.this.pageViews.get(position));
            return More_stock_help.this.pageViews.get(position);
        }

        public boolean isViewFromObject(View v, Object arg1) {
            return v == arg1;
        }

        public void startUpdate(View arg0) {
        }

        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }

        public void restoreState(Parcelable arg0, ClassLoader arg1) {
        }

        public Parcelable saveState() {
            return null;
        }
    }

    class GuidePageChangeListener implements ViewPager.OnPageChangeListener {
        GuidePageChangeListener() {
        }

        public void onPageScrollStateChanged(int arg0) {
        }

        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        public void onPageSelected(int position) {
        }
    }

    public void onBackPressed() {
        Intent intent = new Intent(this, Xhjy_more.class);
        intent.putExtra("part", getIntent().getExtras().getString("part"));
        startActivity(intent);
        finish();
        super.onBackPressed();
    }
}
