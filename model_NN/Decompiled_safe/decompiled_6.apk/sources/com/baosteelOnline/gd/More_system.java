package com.baosteelOnline.gd;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import com.baosteelOnline.R;
import com.baosteelOnline.common.DataBaseFactory;
import com.baosteelOnline.main.ExitApplication;
import com.baosteelOnline.sql.DBHelper;
import com.baosteelOnline.xhjy.Xhjy_more;
import com.jianq.misc.StringEx;

public class More_system extends Activity {
    private DataBaseFactory db;
    private DBHelper dbHelper;
    private TextView textsys;
    private TextView textversion;
    public String versionName = StringEx.Empty;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.more_system);
        ExitApplication.getInstance().addActivity(this);
        this.dbHelper = DataBaseFactory.getInstance(this);
        this.dbHelper.insertOperation("循环资源/钢材交易", "更多", "关于页面");
        this.textsys = (TextView) findViewById(R.id.textsys2);
        this.textversion = (TextView) findViewById(R.id.textversion2);
        this.textsys.setText(((TelephonyManager) getSystemService("phone")).getDeviceId());
        this.versionName = getAppVersionName(this);
        this.textversion.setText(this.versionName);
    }

    public String getAppVersionName(Context context) {
        String versionName2 = StringEx.Empty;
        try {
            versionName2 = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            if (versionName2 == null || versionName2.length() <= 0) {
                return StringEx.Empty;
            }
        } catch (Exception e) {
            Log.e("VersionInfo", "Exception", e);
        }
        return versionName2;
    }

    public void backButtonAction(View v) {
        Intent intent = new Intent(this, Xhjy_more.class);
        intent.putExtra("part", getIntent().getExtras().getString("part"));
        startActivity(intent);
        finish();
    }

    public void onBackPressed() {
        Intent intent = new Intent(this, Xhjy_more.class);
        intent.putExtra("part", getIntent().getExtras().getString("part"));
        startActivity(intent);
        finish();
        super.onBackPressed();
    }
}
