package de.innosystec.unrar;

import de.innosystec.unrar.exception.RarException;
import de.innosystec.unrar.rarfile.FileHeader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class MVTest {
    public static void main(String[] args) {
        Archive a = null;
        try {
            a = new Archive(new File("/home/Avenger/testdata/test2.part01.rar"));
        } catch (RarException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        if (a != null) {
            a.getMainHeader().print();
            for (FileHeader fh = a.nextFileHeader(); fh != null; fh = a.nextFileHeader()) {
                try {
                    File out = new File("/home/Avenger/testdata/" + fh.getFileNameString().trim());
                    System.out.println(out.getAbsolutePath());
                    FileOutputStream os = new FileOutputStream(out);
                    a.extractFile(fh, os);
                    os.close();
                } catch (FileNotFoundException e3) {
                    e3.printStackTrace();
                } catch (RarException e4) {
                    e4.printStackTrace();
                } catch (IOException e5) {
                    e5.printStackTrace();
                }
            }
        }
    }
}
