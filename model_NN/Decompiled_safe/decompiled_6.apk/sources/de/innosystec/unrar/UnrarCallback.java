package de.innosystec.unrar;

import java.io.File;

public interface UnrarCallback {
    boolean isNextVolumeReady(File file);

    void volumeProgressChanged(long j, long j2);
}
