package de.innosystec.unrar.exception;

public class RarException extends Exception {
    private RarExceptionType type;

    public enum RarExceptionType {
        notImplementedYet,
        crcError,
        notRarArchive,
        badRarArchive,
        unkownError,
        headerNotInArchive,
        wrongHeaderType,
        ioError,
        rarEncryptedException
    }

    public RarException(Exception e) {
        super(RarExceptionType.unkownError.name(), e);
        this.type = RarExceptionType.unkownError;
    }

    public RarException(RarException e) {
        super(e.getMessage(), e);
        this.type = e.getType();
    }

    public RarException(RarExceptionType type2) {
        super(type2.name());
        this.type = type2;
    }

    public RarExceptionType getType() {
        return this.type;
    }

    public void setType(RarExceptionType type2) {
        this.type = type2;
    }
}
