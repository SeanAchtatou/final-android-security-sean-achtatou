package de.innosystec.unrar.io;

import java.io.IOException;
import java.io.InputStream;

public class ReadOnlyAccessInputStream extends InputStream {
    private long curPos;
    private final long endPos;
    private IReadOnlyAccess file;
    private final long startPos;

    public ReadOnlyAccessInputStream(IReadOnlyAccess file2, long startPos2, long endPos2) throws IOException {
        this.file = file2;
        this.startPos = startPos2;
        this.curPos = startPos2;
        this.endPos = endPos2;
        file2.setPosition(this.curPos);
    }

    public int read() throws IOException {
        if (this.curPos == this.endPos) {
            return -1;
        }
        int read = this.file.read();
        this.curPos++;
        return read;
    }

    public int read(byte[] b, int off, int len) throws IOException {
        if (len == 0) {
            return 0;
        }
        if (this.curPos == this.endPos) {
            return -1;
        }
        int bytesRead = this.file.read(b, off, (int) Math.min((long) len, this.endPos - this.curPos));
        this.curPos += (long) bytesRead;
        return bytesRead;
    }

    public int read(byte[] b) throws IOException {
        return read(b, 0, b.length);
    }
}
