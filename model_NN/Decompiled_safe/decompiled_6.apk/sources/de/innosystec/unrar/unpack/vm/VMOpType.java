package de.innosystec.unrar.unpack.vm;

public enum VMOpType {
    VM_OPREG(0),
    VM_OPINT(1),
    VM_OPREGMEM(2),
    VM_OPNONE(3);
    
    private int opType;

    private VMOpType(int opType2) {
        this.opType = opType2;
    }

    public int getOpType() {
        return this.opType;
    }

    public boolean equals(int opType2) {
        return this.opType == opType2;
    }

    public static VMOpType findOpType(int opType2) {
        if (VM_OPREG.equals(opType2)) {
            return VM_OPREG;
        }
        if (VM_OPINT.equals(opType2)) {
            return VM_OPINT;
        }
        if (VM_OPREGMEM.equals(opType2)) {
            return VM_OPREGMEM;
        }
        if (VM_OPNONE.equals(opType2)) {
            return VM_OPNONE;
        }
        return null;
    }
}
