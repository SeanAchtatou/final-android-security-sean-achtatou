package de.innosystec.unrar.unpack;

import android.support.v4.view.MotionEventCompat;
import de.innosystec.unrar.exception.RarException;
import de.innosystec.unrar.unpack.decode.Compress;
import de.innosystec.unrar.unpack.vm.BitInput;
import java.io.IOException;
import java.util.Arrays;
import org.apache.tools.zip.UnixStat;

public abstract class Unpack15 extends BitInput {
    private static int[] DecHf0 = {32768, 49152, 57344, 61952, 61952, 61952, 61952, 61952, 65535};
    private static int[] DecHf1 = {8192, 49152, 57344, 61440, 61952, 61952, 63456, 65535};
    private static int[] DecHf2 = {4096, 9216, 32768, 49152, 64000, 65535, 65535, 65535};
    private static int[] DecHf3 = {2048, 9216, 60928, 65152, 65535, 65535, 65535};
    private static int[] DecHf4 = {MotionEventCompat.ACTION_POINTER_INDEX_MASK, 65535, 65535, 65535, 65535, 65535};
    private static int[] DecL1 = {32768, UnixStat.LINK_FLAG, 49152, 53248, 57344, 59904, 60928, 61440, 61952, 61952, 65535};
    private static int[] DecL2 = {UnixStat.LINK_FLAG, 49152, 53248, 57344, 59904, 60928, 61440, 61952, 62016, 65535};
    private static int[] PosHf0 = {0, 0, 0, 0, 0, 8, 16, 24, 33, 33, 33, 33, 33};
    private static int[] PosHf1 = {0, 0, 0, 0, 0, 0, 4, 44, 60, 76, 80, 80, 127};
    private static int[] PosHf2 = {0, 0, 0, 0, 0, 0, 2, 7, 53, 117, 233, 0, 0};
    private static int[] PosHf3 = {0, 0, 0, 0, 0, 0, 0, 2, 16, 218, 251, 0, 0};
    private static int[] PosHf4 = {0, 0, 0, 0, 0, 0, 0, 0, 0, MotionEventCompat.ACTION_MASK, 0, 0, 0};
    private static int[] PosL1 = {0, 0, 0, 2, 3, 5, 7, 11, 16, 20, 24, 32, 32};
    private static int[] PosL2 = {0, 0, 0, 0, 5, 7, 9, 13, 18, 22, 26, 34, 36};
    private static final int STARTHF0 = 4;
    private static final int STARTHF1 = 5;
    private static final int STARTHF2 = 5;
    private static final int STARTHF3 = 6;
    private static final int STARTHF4 = 8;
    private static final int STARTL1 = 2;
    private static final int STARTL2 = 3;
    static int[] ShortLen1 = {1, 3, 4, 4, 5, 6, 7, 8, 8, 4, 4, 5, 6, 6, 4, 0};
    static int[] ShortLen2 = {2, 3, 3, 3, 4, 4, 5, 6, 6, 4, 4, 5, 6, 6, 4, 0};
    static int[] ShortXor1 = {0, 160, 208, 224, 240, 248, 252, 254, MotionEventCompat.ACTION_MASK, 192, 128, 144, 152, 156, 176};
    static int[] ShortXor2 = {0, 64, 96, 160, 208, 224, 240, 248, 252, 192, 128, 144, 152, 156, 176};
    protected int AvrLn1;
    protected int AvrLn2;
    protected int AvrLn3;
    protected int AvrPlc;
    protected int AvrPlcB;
    protected int Buf60;
    protected int[] ChSet = new int[256];
    protected int[] ChSetA = new int[256];
    protected int[] ChSetB = new int[256];
    protected int[] ChSetC = new int[256];
    protected int FlagBuf;
    protected int FlagsCnt;
    protected int LCount;
    protected int MaxDist3;
    protected int[] NToPl = new int[256];
    protected int[] NToPlB = new int[256];
    protected int[] NToPlC = new int[256];
    protected int Nhfb;
    protected int Nlzb;
    protected int NumHuf;
    protected int[] Place = new int[256];
    protected int[] PlaceA = new int[256];
    protected int[] PlaceB = new int[256];
    protected int[] PlaceC = new int[256];
    protected int StMode;
    protected long destUnpSize;
    protected int lastDist;
    protected int lastLength;
    protected int[] oldDist = new int[4];
    protected int oldDistPtr;
    protected int readBorder;
    protected int readTop;
    protected boolean suspended;
    protected boolean unpAllBuf;
    protected ComprDataIO unpIO;
    protected int unpPtr;
    protected boolean unpSomeRead;
    protected byte[] window;
    protected int wrPtr;

    /* access modifiers changed from: protected */
    public abstract void unpInitData(boolean z);

    /* access modifiers changed from: protected */
    public void unpack15(boolean solid) throws IOException, RarException {
        if (this.suspended) {
            this.unpPtr = this.wrPtr;
        } else {
            unpInitData(solid);
            oldUnpInitData(solid);
            unpReadBuf();
            if (!solid) {
                initHuff();
                this.unpPtr = 0;
            } else {
                this.unpPtr = this.wrPtr;
            }
            this.destUnpSize--;
        }
        if (this.destUnpSize >= 0) {
            getFlagsBuf();
            this.FlagsCnt = 8;
        }
        while (this.destUnpSize >= 0) {
            this.unpPtr &= Compress.MAXWINMASK;
            if (this.inAddr > this.readTop - 30 && !unpReadBuf()) {
                break;
            }
            if (((this.wrPtr - this.unpPtr) & Compress.MAXWINMASK) < 270 && this.wrPtr != this.unpPtr) {
                oldUnpWriteBuf();
                if (this.suspended) {
                    return;
                }
            }
            if (this.StMode != 0) {
                huffDecode();
            } else {
                int i = this.FlagsCnt - 1;
                this.FlagsCnt = i;
                if (i < 0) {
                    getFlagsBuf();
                    this.FlagsCnt = 7;
                }
                if ((this.FlagBuf & 128) != 0) {
                    this.FlagBuf <<= 1;
                    if (this.Nlzb > this.Nhfb) {
                        longLZ();
                    } else {
                        huffDecode();
                    }
                } else {
                    this.FlagBuf <<= 1;
                    int i2 = this.FlagsCnt - 1;
                    this.FlagsCnt = i2;
                    if (i2 < 0) {
                        getFlagsBuf();
                        this.FlagsCnt = 7;
                    }
                    if ((this.FlagBuf & 128) != 0) {
                        this.FlagBuf <<= 1;
                        if (this.Nlzb > this.Nhfb) {
                            huffDecode();
                        } else {
                            longLZ();
                        }
                    } else {
                        this.FlagBuf <<= 1;
                        shortLZ();
                    }
                }
            }
        }
        oldUnpWriteBuf();
    }

    /* access modifiers changed from: protected */
    public boolean unpReadBuf() throws IOException, RarException {
        int dataSize = this.readTop - this.inAddr;
        if (dataSize < 0) {
            return false;
        }
        if (this.inAddr > 16384) {
            if (dataSize > 0) {
                System.arraycopy(this.inBuf, this.inAddr, this.inBuf, 0, dataSize);
            }
            this.inAddr = 0;
            this.readTop = dataSize;
        } else {
            dataSize = this.readTop;
        }
        int readCode = this.unpIO.unpRead(this.inBuf, dataSize, (32768 - dataSize) & -16);
        if (readCode > 0) {
            this.readTop += readCode;
        }
        this.readBorder = this.readTop - 30;
        if (readCode != -1) {
            return true;
        }
        return false;
    }

    private int getShortLen1(int pos) {
        return pos == 1 ? this.Buf60 + 3 : ShortLen1[pos];
    }

    private int getShortLen2(int pos) {
        return pos == 3 ? this.Buf60 + 3 : ShortLen2[pos];
    }

    /* access modifiers changed from: protected */
    public void shortLZ() {
        int Length;
        this.NumHuf = 0;
        int BitField = fgetbits();
        if (this.LCount == 2) {
            faddbits(1);
            if (BitField >= 32768) {
                oldCopyString(this.lastDist, this.lastLength);
                return;
            } else {
                BitField <<= 1;
                this.LCount = 0;
            }
        }
        int BitField2 = BitField >>> 8;
        if (this.AvrLn1 < 37) {
            Length = 0;
            while (((ShortXor1[Length] ^ BitField2) & ((MotionEventCompat.ACTION_MASK >>> getShortLen1(Length)) ^ -1)) != 0) {
                Length++;
            }
            faddbits(getShortLen1(Length));
        } else {
            int Length2 = 0;
            while (((ShortXor2[Length] ^ BitField2) & ((MotionEventCompat.ACTION_MASK >> getShortLen2(Length)) ^ -1)) != 0) {
                Length2 = Length + 1;
            }
            faddbits(getShortLen2(Length));
        }
        if (Length < 9) {
            this.LCount = 0;
            this.AvrLn1 += Length;
            this.AvrLn1 -= this.AvrLn1 >> 4;
            int DistancePlace = decodeNum(fgetbits(), 5, DecHf2, PosHf2) & MotionEventCompat.ACTION_MASK;
            int Distance = this.ChSetA[DistancePlace];
            int DistancePlace2 = DistancePlace - 1;
            if (DistancePlace2 != -1) {
                int[] iArr = this.PlaceA;
                iArr[Distance] = iArr[Distance] - 1;
                int LastDistance = this.ChSetA[DistancePlace2];
                int[] iArr2 = this.PlaceA;
                iArr2[LastDistance] = iArr2[LastDistance] + 1;
                this.ChSetA[DistancePlace2 + 1] = LastDistance;
                this.ChSetA[DistancePlace2] = Distance;
            }
            int Length3 = Length + 2;
            int[] iArr3 = this.oldDist;
            int i = this.oldDistPtr;
            this.oldDistPtr = i + 1;
            int Distance2 = Distance + 1;
            iArr3[i] = Distance2;
            this.oldDistPtr &= 3;
            this.lastLength = Length3;
            this.lastDist = Distance2;
            oldCopyString(Distance2, Length3);
        } else if (Length == 9) {
            this.LCount++;
            oldCopyString(this.lastDist, this.lastLength);
        } else if (Length == 14) {
            this.LCount = 0;
            int Length4 = decodeNum(fgetbits(), 3, DecL2, PosL2) + 5;
            int Distance3 = (fgetbits() >> 1) | 32768;
            faddbits(15);
            this.lastLength = Length4;
            this.lastDist = Distance3;
            oldCopyString(Distance3, Length4);
        } else {
            this.LCount = 0;
            int SaveLength = Length;
            int Distance4 = this.oldDist[(this.oldDistPtr - (Length - 9)) & 3];
            int Length5 = decodeNum(fgetbits(), 2, DecL1, PosL1) + 2;
            if (Length5 == 257 && SaveLength == 10) {
                this.Buf60 ^= 1;
                return;
            }
            if (Distance4 > 256) {
                Length5++;
            }
            if (Distance4 >= this.MaxDist3) {
                Length5++;
            }
            int[] iArr4 = this.oldDist;
            int i2 = this.oldDistPtr;
            this.oldDistPtr = i2 + 1;
            iArr4[i2] = Distance4;
            this.oldDistPtr &= 3;
            this.lastLength = Length5;
            this.lastDist = Distance4;
            oldCopyString(Distance4, Length5);
        }
    }

    /* access modifiers changed from: protected */
    public void longLZ() {
        int Length;
        int DistancePlace;
        int Distance;
        int NewDistancePlace;
        this.NumHuf = 0;
        this.Nlzb += 16;
        if (this.Nlzb > 255) {
            this.Nlzb = 144;
            this.Nhfb >>>= 1;
        }
        int OldAvr2 = this.AvrLn2;
        int BitField = fgetbits();
        if (this.AvrLn2 >= 122) {
            Length = decodeNum(BitField, 3, DecL2, PosL2);
        } else if (this.AvrLn2 >= 64) {
            Length = decodeNum(BitField, 2, DecL1, PosL1);
        } else if (BitField < 256) {
            Length = BitField;
            faddbits(16);
        } else {
            Length = 0;
            while (((BitField << Length) & 32768) == 0) {
                Length++;
            }
            faddbits(Length + 1);
        }
        this.AvrLn2 += Length;
        this.AvrLn2 -= this.AvrLn2 >>> 5;
        int BitField2 = fgetbits();
        if (this.AvrPlcB > 10495) {
            DistancePlace = decodeNum(BitField2, 5, DecHf2, PosHf2);
        } else if (this.AvrPlcB > 1791) {
            DistancePlace = decodeNum(BitField2, 5, DecHf1, PosHf1);
        } else {
            DistancePlace = decodeNum(BitField2, 4, DecHf0, PosHf0);
        }
        this.AvrPlcB += DistancePlace;
        this.AvrPlcB -= this.AvrPlcB >> 8;
        while (true) {
            int Distance2 = this.ChSetB[DistancePlace & MotionEventCompat.ACTION_MASK];
            int[] iArr = this.NToPlB;
            Distance = Distance2 + 1;
            int i = Distance2 & MotionEventCompat.ACTION_MASK;
            NewDistancePlace = iArr[i];
            iArr[i] = NewDistancePlace + 1;
            if ((Distance & MotionEventCompat.ACTION_MASK) != 0) {
                break;
            }
            corrHuff(this.ChSetB, this.NToPlB);
        }
        this.ChSetB[DistancePlace] = this.ChSetB[NewDistancePlace];
        this.ChSetB[NewDistancePlace] = Distance;
        int Distance3 = ((65280 & Distance) | (fgetbits() >>> 8)) >>> 1;
        faddbits(7);
        int OldAvr3 = this.AvrLn3;
        if (!(Length == 1 || Length == 4)) {
            if (Length == 0 && Distance3 <= this.MaxDist3) {
                this.AvrLn3++;
                this.AvrLn3 -= this.AvrLn3 >> 8;
            } else if (this.AvrLn3 > 0) {
                this.AvrLn3--;
            }
        }
        int Length2 = Length + 3;
        if (Distance3 >= this.MaxDist3) {
            Length2++;
        }
        if (Distance3 <= 256) {
            Length2 += 8;
        }
        if (OldAvr3 > 176 || (this.AvrPlc >= 10752 && OldAvr2 < 64)) {
            this.MaxDist3 = 32512;
        } else {
            this.MaxDist3 = 8193;
        }
        int[] iArr2 = this.oldDist;
        int i2 = this.oldDistPtr;
        this.oldDistPtr = i2 + 1;
        iArr2[i2] = Distance3;
        this.oldDistPtr &= 3;
        this.lastLength = Length2;
        this.lastDist = Distance3;
        oldCopyString(Distance3, Length2);
    }

    /* access modifiers changed from: protected */
    public void huffDecode() {
        int BytePlace;
        int Length = 4;
        int BitField = fgetbits();
        if (this.AvrPlc > 30207) {
            BytePlace = decodeNum(BitField, 8, DecHf4, PosHf4);
        } else if (this.AvrPlc > 24063) {
            BytePlace = decodeNum(BitField, 6, DecHf3, PosHf3);
        } else if (this.AvrPlc > 13823) {
            BytePlace = decodeNum(BitField, 5, DecHf2, PosHf2);
        } else if (this.AvrPlc > 3583) {
            BytePlace = decodeNum(BitField, 5, DecHf1, PosHf1);
        } else {
            BytePlace = decodeNum(BitField, 4, DecHf0, PosHf0);
        }
        int BytePlace2 = BytePlace & MotionEventCompat.ACTION_MASK;
        if (this.StMode != 0) {
            if (BytePlace2 == 0 && BitField > 4095) {
                BytePlace2 = 256;
            }
            BytePlace2--;
            if (BytePlace2 == -1) {
                int BitField2 = fgetbits();
                faddbits(1);
                if ((32768 & BitField2) != 0) {
                    this.StMode = 0;
                    this.NumHuf = 0;
                    return;
                }
                if ((BitField2 & 16384) == 0) {
                    Length = 3;
                }
                faddbits(1);
                faddbits(5);
                oldCopyString((decodeNum(fgetbits(), 5, DecHf2, PosHf2) << 5) | (fgetbits() >>> 11), Length);
                return;
            }
        } else {
            int i = this.NumHuf;
            this.NumHuf = i + 1;
            if (i >= 16 && this.FlagsCnt == 0) {
                this.StMode = 1;
            }
        }
        this.AvrPlc += BytePlace2;
        this.AvrPlc -= this.AvrPlc >>> 8;
        this.Nhfb += 16;
        if (this.Nhfb > 255) {
            this.Nhfb = 144;
            this.Nlzb >>>= 1;
        }
        byte[] bArr = this.window;
        int i2 = this.unpPtr;
        this.unpPtr = i2 + 1;
        bArr[i2] = (byte) (this.ChSet[BytePlace2] >>> 8);
        this.destUnpSize--;
        while (true) {
            int CurByte = this.ChSet[BytePlace2];
            int[] iArr = this.NToPl;
            int CurByte2 = CurByte + 1;
            int i3 = CurByte & MotionEventCompat.ACTION_MASK;
            int NewBytePlace = iArr[i3];
            iArr[i3] = NewBytePlace + 1;
            if ((CurByte2 & MotionEventCompat.ACTION_MASK) > 161) {
                corrHuff(this.ChSet, this.NToPl);
            } else {
                this.ChSet[BytePlace2] = this.ChSet[NewBytePlace];
                this.ChSet[NewBytePlace] = CurByte2;
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void getFlagsBuf() {
        int FlagsPlace = decodeNum(fgetbits(), 5, DecHf2, PosHf2);
        while (true) {
            int Flags = this.ChSetC[FlagsPlace];
            this.FlagBuf = Flags >>> 8;
            int[] iArr = this.NToPlC;
            int Flags2 = Flags + 1;
            int i = Flags & MotionEventCompat.ACTION_MASK;
            int NewFlagsPlace = iArr[i];
            iArr[i] = NewFlagsPlace + 1;
            if ((Flags2 & MotionEventCompat.ACTION_MASK) != 0) {
                this.ChSetC[FlagsPlace] = this.ChSetC[NewFlagsPlace];
                this.ChSetC[NewFlagsPlace] = Flags2;
                return;
            }
            corrHuff(this.ChSetC, this.NToPlC);
        }
    }

    /* access modifiers changed from: protected */
    public void oldUnpInitData(boolean Solid) {
        if (!Solid) {
            this.Buf60 = 0;
            this.NumHuf = 0;
            this.AvrLn3 = 0;
            this.AvrLn2 = 0;
            this.AvrLn1 = 0;
            this.AvrPlcB = 0;
            this.AvrPlc = 13568;
            this.MaxDist3 = 8193;
            this.Nlzb = 128;
            this.Nhfb = 128;
        }
        this.FlagsCnt = 0;
        this.FlagBuf = 0;
        this.StMode = 0;
        this.LCount = 0;
        this.readTop = 0;
    }

    /* access modifiers changed from: protected */
    public void initHuff() {
        for (int I = 0; I < 256; I++) {
            int[] iArr = this.Place;
            int[] iArr2 = this.PlaceA;
            this.PlaceB[I] = I;
            iArr2[I] = I;
            iArr[I] = I;
            this.PlaceC[I] = ((I ^ -1) + 1) & MotionEventCompat.ACTION_MASK;
            int[] iArr3 = this.ChSet;
            int i = I << 8;
            this.ChSetB[I] = i;
            iArr3[I] = i;
            this.ChSetA[I] = I;
            this.ChSetC[I] = (((I ^ -1) + 1) & MotionEventCompat.ACTION_MASK) << 8;
        }
        Arrays.fill(this.NToPl, 0);
        Arrays.fill(this.NToPlB, 0);
        Arrays.fill(this.NToPlC, 0);
        corrHuff(this.ChSetB, this.NToPlB);
    }

    /* access modifiers changed from: protected */
    public void corrHuff(int[] CharSet, int[] NumToPlace) {
        int pos = 0;
        for (int I = 7; I >= 0; I--) {
            int J = 0;
            while (J < 32) {
                CharSet[pos] = (CharSet[pos] & -256) | I;
                J++;
                pos++;
            }
        }
        Arrays.fill(NumToPlace, 0);
        for (int I2 = 6; I2 >= 0; I2--) {
            NumToPlace[I2] = (7 - I2) * 32;
        }
    }

    /* access modifiers changed from: protected */
    public void oldCopyString(int Distance, int Length) {
        this.destUnpSize -= (long) Length;
        while (true) {
            int Length2 = Length;
            Length = Length2 - 1;
            if (Length2 != 0) {
                this.window[this.unpPtr] = this.window[(this.unpPtr - Distance) & Compress.MAXWINMASK];
                this.unpPtr = (this.unpPtr + 1) & Compress.MAXWINMASK;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public int decodeNum(int Num, int StartPos, int[] DecTab, int[] PosTab) {
        int Num2 = Num & 65520;
        int I = 0;
        while (DecTab[I] <= Num2) {
            StartPos++;
            I++;
        }
        faddbits(StartPos);
        return ((Num2 - (I != 0 ? DecTab[I - 1] : 0)) >>> (16 - StartPos)) + PosTab[StartPos];
    }

    /* access modifiers changed from: protected */
    public void oldUnpWriteBuf() throws IOException {
        if (this.unpPtr != this.wrPtr) {
            this.unpSomeRead = true;
        }
        if (this.unpPtr < this.wrPtr) {
            this.unpIO.unpWrite(this.window, this.wrPtr, (-this.wrPtr) & Compress.MAXWINMASK);
            this.unpIO.unpWrite(this.window, 0, this.unpPtr);
            this.unpAllBuf = true;
        } else {
            this.unpIO.unpWrite(this.window, this.wrPtr, this.unpPtr - this.wrPtr);
        }
        this.wrPtr = this.unpPtr;
    }
}
