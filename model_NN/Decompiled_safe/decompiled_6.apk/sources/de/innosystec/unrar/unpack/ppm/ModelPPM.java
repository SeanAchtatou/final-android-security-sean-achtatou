package de.innosystec.unrar.unpack.ppm;

import android.support.v4.view.MotionEventCompat;
import de.innosystec.unrar.exception.RarException;
import de.innosystec.unrar.unpack.Unpack;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Arrays;

public class ModelPPM {
    public static final int BIN_SCALE = 16384;
    public static final int INTERVAL = 128;
    public static final int INT_BITS = 7;
    private static int[] InitBinEsc = {15581, 7999, 22975, 18675, 25761, 23228, 26162, 24657};
    public static final int MAX_FREQ = 124;
    public static final int MAX_O = 64;
    public static final int PERIOD_BITS = 7;
    public static final int TOT_BITS = 14;
    private int[] HB2Flag = new int[256];
    private int[] NS2BSIndx = new int[256];
    private int[] NS2Indx = new int[256];
    private SEE2Context[][] SEE2Cont = ((SEE2Context[][]) Array.newInstance(SEE2Context.class, 25, 16));
    private int[][] binSumm = ((int[][]) Array.newInstance(Integer.TYPE, 128, 64));
    private int[] charMask = new int[256];
    private RangeCoder coder = new RangeCoder();
    private SEE2Context dummySEE2Cont;
    private int escCount;
    private State foundState;
    private int hiBitsFlag;
    private int initEsc;
    private int initRL;
    private PPMContext maxContext = null;
    private int maxOrder;
    private PPMContext medContext = null;
    private PPMContext minContext = null;
    private int numMasked;
    private int orderFall;
    private int prevSuccess;
    private final int[] ps = new int[64];
    private int runLength;
    private SubAllocator subAlloc = new SubAllocator();
    private final PPMContext tempPPMContext1 = new PPMContext(null);
    private final PPMContext tempPPMContext2 = new PPMContext(null);
    private final PPMContext tempPPMContext3 = new PPMContext(null);
    private final PPMContext tempPPMContext4 = new PPMContext(null);
    private final State tempState1 = new State(null);
    private final State tempState2 = new State(null);
    private final State tempState3 = new State(null);
    private final State tempState4 = new State(null);
    private final StateRef tempStateRef1 = new StateRef();
    private final StateRef tempStateRef2 = new StateRef();

    public SubAllocator getSubAlloc() {
        return this.subAlloc;
    }

    private void restartModelRare() {
        int i = 12;
        Arrays.fill(this.charMask, 0);
        this.subAlloc.initSubAllocator();
        if (this.maxOrder < 12) {
            i = this.maxOrder;
        }
        this.initRL = (-i) - 1;
        int addr = this.subAlloc.allocContext();
        this.minContext.setAddress(addr);
        this.maxContext.setAddress(addr);
        this.minContext.setSuffix(0);
        this.orderFall = this.maxOrder;
        this.minContext.setNumStats(256);
        this.minContext.getFreqData().setSummFreq(this.minContext.getNumStats() + 1);
        int addr2 = this.subAlloc.allocUnits(128);
        this.foundState.setAddress(addr2);
        this.minContext.getFreqData().setStats(addr2);
        State state = new State(this.subAlloc.getHeap());
        int addr3 = this.minContext.getFreqData().getStats();
        this.runLength = this.initRL;
        this.prevSuccess = 0;
        for (int i2 = 0; i2 < 256; i2++) {
            state.setAddress((i2 * 6) + addr3);
            state.setSymbol(i2);
            state.setFreq(1);
            state.setSuccessor(0);
        }
        for (int i3 = 0; i3 < 128; i3++) {
            for (int k = 0; k < 8; k++) {
                for (int m = 0; m < 64; m += 8) {
                    this.binSumm[i3][k + m] = 16384 - (InitBinEsc[k] / (i3 + 2));
                }
            }
        }
        for (int i4 = 0; i4 < 25; i4++) {
            for (int k2 = 0; k2 < 16; k2++) {
                this.SEE2Cont[i4][k2].init((i4 * 5) + 10);
            }
        }
    }

    private void startModelRare(int MaxOrder) {
        this.escCount = 1;
        this.maxOrder = MaxOrder;
        restartModelRare();
        this.NS2BSIndx[0] = 0;
        this.NS2BSIndx[1] = 2;
        for (int j = 0; j < 9; j++) {
            this.NS2BSIndx[j + 2] = 4;
        }
        for (int j2 = 0; j2 < 245; j2++) {
            this.NS2BSIndx[j2 + 11] = 6;
        }
        int i = 0;
        while (i < 3) {
            this.NS2Indx[i] = i;
            i++;
        }
        int m = i;
        int k = 1;
        int Step = 1;
        while (i < 256) {
            this.NS2Indx[i] = m;
            k--;
            if (k == 0) {
                Step++;
                k = Step;
                m++;
            }
            i++;
        }
        for (int j3 = 0; j3 < 64; j3++) {
            this.HB2Flag[j3] = 0;
        }
        for (int j4 = 0; j4 < 192; j4++) {
            this.HB2Flag[j4 + 64] = 8;
        }
        this.dummySEE2Cont.setShift(7);
    }

    private void clearMask() {
        this.escCount = 1;
        Arrays.fill(this.charMask, 0);
    }

    public boolean decodeInit(Unpack unpackRead, int escChar) throws IOException, RarException {
        boolean reset;
        boolean z = true;
        int MaxOrder = unpackRead.getChar() & MotionEventCompat.ACTION_MASK;
        if ((MaxOrder & 32) != 0) {
            reset = true;
        } else {
            reset = false;
        }
        int MaxMB = 0;
        if (reset) {
            MaxMB = unpackRead.getChar();
        } else if (this.subAlloc.GetAllocatedMemory() == 0) {
            return false;
        }
        if ((MaxOrder & 64) != 0) {
            unpackRead.setPpmEscChar(unpackRead.getChar());
        }
        this.coder.initDecoder(unpackRead);
        if (reset) {
            int MaxOrder2 = (MaxOrder & 31) + 1;
            if (MaxOrder2 > 16) {
                MaxOrder2 = ((MaxOrder2 - 16) * 3) + 16;
            }
            if (MaxOrder2 == 1) {
                this.subAlloc.stopSubAllocator();
                return false;
            }
            this.subAlloc.startSubAllocator(MaxMB + 1);
            this.minContext = new PPMContext(getHeap());
            this.medContext = new PPMContext(getHeap());
            this.maxContext = new PPMContext(getHeap());
            this.foundState = new State(getHeap());
            this.dummySEE2Cont = new SEE2Context();
            for (int i = 0; i < 25; i++) {
                for (int j = 0; j < 16; j++) {
                    this.SEE2Cont[i][j] = new SEE2Context();
                }
            }
            startModelRare(MaxOrder2);
        }
        if (this.minContext.getAddress() == 0) {
            z = false;
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0051, code lost:
        if (r4.minContext.decodeSymbol1(r4) != false) goto L_0x0053;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00a2, code lost:
        if (r4.minContext.decodeSymbol2(r4) == false) goto L_0x001d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int decodeChar() throws java.io.IOException, de.innosystec.unrar.exception.RarException {
        /*
            r4 = this;
            r0 = -1
            de.innosystec.unrar.unpack.ppm.PPMContext r2 = r4.minContext
            int r2 = r2.getAddress()
            de.innosystec.unrar.unpack.ppm.SubAllocator r3 = r4.subAlloc
            int r3 = r3.getPText()
            if (r2 <= r3) goto L_0x001d
            de.innosystec.unrar.unpack.ppm.PPMContext r2 = r4.minContext
            int r2 = r2.getAddress()
            de.innosystec.unrar.unpack.ppm.SubAllocator r3 = r4.subAlloc
            int r3 = r3.getHeapEnd()
            if (r2 <= r3) goto L_0x001e
        L_0x001d:
            return r0
        L_0x001e:
            de.innosystec.unrar.unpack.ppm.PPMContext r2 = r4.minContext
            int r2 = r2.getNumStats()
            r3 = 1
            if (r2 == r3) goto L_0x00aa
            de.innosystec.unrar.unpack.ppm.PPMContext r2 = r4.minContext
            de.innosystec.unrar.unpack.ppm.FreqData r2 = r2.getFreqData()
            int r2 = r2.getStats()
            de.innosystec.unrar.unpack.ppm.SubAllocator r3 = r4.subAlloc
            int r3 = r3.getPText()
            if (r2 <= r3) goto L_0x001d
            de.innosystec.unrar.unpack.ppm.PPMContext r2 = r4.minContext
            de.innosystec.unrar.unpack.ppm.FreqData r2 = r2.getFreqData()
            int r2 = r2.getStats()
            de.innosystec.unrar.unpack.ppm.SubAllocator r3 = r4.subAlloc
            int r3 = r3.getHeapEnd()
            if (r2 > r3) goto L_0x001d
            de.innosystec.unrar.unpack.ppm.PPMContext r2 = r4.minContext
            boolean r2 = r2.decodeSymbol1(r4)
            if (r2 == 0) goto L_0x001d
        L_0x0053:
            de.innosystec.unrar.unpack.ppm.RangeCoder r2 = r4.coder
            r2.decode()
        L_0x0058:
            de.innosystec.unrar.unpack.ppm.State r2 = r4.foundState
            int r2 = r2.getAddress()
            if (r2 != 0) goto L_0x00b0
            de.innosystec.unrar.unpack.ppm.RangeCoder r2 = r4.coder
            r2.ariDecNormalize()
        L_0x0065:
            int r2 = r4.orderFall
            int r2 = r2 + 1
            r4.orderFall = r2
            de.innosystec.unrar.unpack.ppm.PPMContext r2 = r4.minContext
            de.innosystec.unrar.unpack.ppm.PPMContext r3 = r4.minContext
            int r3 = r3.getSuffix()
            r2.setAddress(r3)
            de.innosystec.unrar.unpack.ppm.PPMContext r2 = r4.minContext
            int r2 = r2.getAddress()
            de.innosystec.unrar.unpack.ppm.SubAllocator r3 = r4.subAlloc
            int r3 = r3.getPText()
            if (r2 <= r3) goto L_0x001d
            de.innosystec.unrar.unpack.ppm.PPMContext r2 = r4.minContext
            int r2 = r2.getAddress()
            de.innosystec.unrar.unpack.ppm.SubAllocator r3 = r4.subAlloc
            int r3 = r3.getHeapEnd()
            if (r2 > r3) goto L_0x001d
            de.innosystec.unrar.unpack.ppm.PPMContext r2 = r4.minContext
            int r2 = r2.getNumStats()
            int r3 = r4.numMasked
            if (r2 == r3) goto L_0x0065
            de.innosystec.unrar.unpack.ppm.PPMContext r2 = r4.minContext
            boolean r2 = r2.decodeSymbol2(r4)
            if (r2 == 0) goto L_0x001d
            de.innosystec.unrar.unpack.ppm.RangeCoder r2 = r4.coder
            r2.decode()
            goto L_0x0058
        L_0x00aa:
            de.innosystec.unrar.unpack.ppm.PPMContext r2 = r4.minContext
            r2.decodeBinSymbol(r4)
            goto L_0x0053
        L_0x00b0:
            de.innosystec.unrar.unpack.ppm.State r2 = r4.foundState
            int r0 = r2.getSymbol()
            int r2 = r4.orderFall
            if (r2 != 0) goto L_0x00df
            de.innosystec.unrar.unpack.ppm.State r2 = r4.foundState
            int r2 = r2.getSuccessor()
            de.innosystec.unrar.unpack.ppm.SubAllocator r3 = r4.subAlloc
            int r3 = r3.getPText()
            if (r2 <= r3) goto L_0x00df
            de.innosystec.unrar.unpack.ppm.State r2 = r4.foundState
            int r1 = r2.getSuccessor()
            de.innosystec.unrar.unpack.ppm.PPMContext r2 = r4.minContext
            r2.setAddress(r1)
            de.innosystec.unrar.unpack.ppm.PPMContext r2 = r4.maxContext
            r2.setAddress(r1)
        L_0x00d8:
            de.innosystec.unrar.unpack.ppm.RangeCoder r2 = r4.coder
            r2.ariDecNormalize()
            goto L_0x001d
        L_0x00df:
            r4.updateModel()
            int r2 = r4.escCount
            if (r2 != 0) goto L_0x00d8
            r4.clearMask()
            goto L_0x00d8
        */
        throw new UnsupportedOperationException("Method not decompiled: de.innosystec.unrar.unpack.ppm.ModelPPM.decodeChar():int");
    }

    public SEE2Context[][] getSEE2Cont() {
        return this.SEE2Cont;
    }

    public SEE2Context getDummySEE2Cont() {
        return this.dummySEE2Cont;
    }

    public int getInitRL() {
        return this.initRL;
    }

    public void setEscCount(int escCount2) {
        this.escCount = escCount2 & MotionEventCompat.ACTION_MASK;
    }

    public int getEscCount() {
        return this.escCount;
    }

    public void incEscCount(int dEscCount) {
        setEscCount(getEscCount() + dEscCount);
    }

    public int[] getCharMask() {
        return this.charMask;
    }

    public int getNumMasked() {
        return this.numMasked;
    }

    public void setNumMasked(int numMasked2) {
        this.numMasked = numMasked2;
    }

    public void setPrevSuccess(int prevSuccess2) {
        this.prevSuccess = prevSuccess2 & MotionEventCompat.ACTION_MASK;
    }

    public int getInitEsc() {
        return this.initEsc;
    }

    public void setInitEsc(int initEsc2) {
        this.initEsc = initEsc2;
    }

    public void setRunLength(int runLength2) {
        this.runLength = runLength2;
    }

    public int getRunLength() {
        return this.runLength;
    }

    public void incRunLength(int dRunLength) {
        setRunLength(getRunLength() + dRunLength);
    }

    public int getPrevSuccess() {
        return this.prevSuccess;
    }

    public int getHiBitsFlag() {
        return this.hiBitsFlag;
    }

    public void setHiBitsFlag(int hiBitsFlag2) {
        this.hiBitsFlag = hiBitsFlag2 & MotionEventCompat.ACTION_MASK;
    }

    public int[][] getBinSumm() {
        return this.binSumm;
    }

    public RangeCoder getCoder() {
        return this.coder;
    }

    public int[] getHB2Flag() {
        return this.HB2Flag;
    }

    public int[] getNS2BSIndx() {
        return this.NS2BSIndx;
    }

    public int[] getNS2Indx() {
        return this.NS2Indx;
    }

    public State getFoundState() {
        return this.foundState;
    }

    public byte[] getHeap() {
        return this.subAlloc.getHeap();
    }

    public int getOrderFall() {
        return this.orderFall;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00d7, code lost:
        if (r4.getSuffix() != 0) goto L_0x00d9;
     */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00ae  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00c9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int createSuccessors(boolean r14, de.innosystec.unrar.unpack.ppm.State r15) {
        /*
            r13 = this;
            de.innosystec.unrar.unpack.ppm.StateRef r10 = r13.tempStateRef2
            de.innosystec.unrar.unpack.ppm.State r11 = r13.tempState1
            byte[] r12 = r13.getHeap()
            de.innosystec.unrar.unpack.ppm.State r8 = r11.init(r12)
            de.innosystec.unrar.unpack.ppm.PPMContext r11 = r13.tempPPMContext1
            byte[] r12 = r13.getHeap()
            de.innosystec.unrar.unpack.ppm.PPMContext r4 = r11.init(r12)
            de.innosystec.unrar.unpack.ppm.PPMContext r11 = r13.minContext
            int r11 = r11.getAddress()
            r4.setAddress(r11)
            de.innosystec.unrar.unpack.ppm.PPMContext r11 = r13.tempPPMContext2
            byte[] r12 = r13.getHeap()
            de.innosystec.unrar.unpack.ppm.PPMContext r9 = r11.init(r12)
            de.innosystec.unrar.unpack.ppm.State r11 = r13.foundState
            int r11 = r11.getSuccessor()
            r9.setAddress(r11)
            de.innosystec.unrar.unpack.ppm.State r11 = r13.tempState2
            byte[] r12 = r13.getHeap()
            de.innosystec.unrar.unpack.ppm.State r3 = r11.init(r12)
            r5 = 0
            r2 = 0
            if (r14 != 0) goto L_0x0054
            int[] r11 = r13.ps
            int r6 = r5 + 1
            de.innosystec.unrar.unpack.ppm.State r12 = r13.foundState
            int r12 = r12.getAddress()
            r11[r5] = r12
            int r11 = r4.getSuffix()
            if (r11 != 0) goto L_0x0185
            r2 = 1
            r5 = r6
        L_0x0054:
            if (r2 != 0) goto L_0x00b6
            r1 = 0
            int r11 = r15.getAddress()
            if (r11 == 0) goto L_0x00d9
            int r11 = r15.getAddress()
            r3.setAddress(r11)
            int r11 = r4.getSuffix()
            r4.setAddress(r11)
            r1 = 1
            r6 = r5
        L_0x006d:
            if (r1 != 0) goto L_0x00a3
            int r11 = r4.getSuffix()
            r4.setAddress(r11)
            int r11 = r4.getNumStats()
            r12 = 1
            if (r11 == r12) goto L_0x00bd
            de.innosystec.unrar.unpack.ppm.FreqData r11 = r4.getFreqData()
            int r11 = r11.getStats()
            r3.setAddress(r11)
            int r11 = r3.getSymbol()
            de.innosystec.unrar.unpack.ppm.State r12 = r13.foundState
            int r12 = r12.getSymbol()
            if (r11 == r12) goto L_0x00a3
        L_0x0094:
            r3.incAddress()
            int r11 = r3.getSymbol()
            de.innosystec.unrar.unpack.ppm.State r12 = r13.foundState
            int r12 = r12.getSymbol()
            if (r11 != r12) goto L_0x0094
        L_0x00a3:
            r1 = 0
            int r11 = r3.getSuccessor()
            int r12 = r9.getAddress()
            if (r11 == r12) goto L_0x00c9
            int r11 = r3.getSuccessor()
            r4.setAddress(r11)
            r5 = r6
        L_0x00b6:
            if (r5 != 0) goto L_0x00db
            int r11 = r4.getAddress()
        L_0x00bc:
            return r11
        L_0x00bd:
            de.innosystec.unrar.unpack.ppm.State r11 = r4.getOneState()
            int r11 = r11.getAddress()
            r3.setAddress(r11)
            goto L_0x00a3
        L_0x00c9:
            int[] r11 = r13.ps
            int r5 = r6 + 1
            int r12 = r3.getAddress()
            r11[r6] = r12
            int r11 = r4.getSuffix()
            if (r11 == 0) goto L_0x00b6
        L_0x00d9:
            r6 = r5
            goto L_0x006d
        L_0x00db:
            byte[] r11 = r13.getHeap()
            int r12 = r9.getAddress()
            byte r11 = r11[r12]
            r10.setSymbol(r11)
            int r11 = r9.getAddress()
            int r11 = r11 + 1
            r10.setSuccessor(r11)
            int r11 = r4.getNumStats()
            r12 = 1
            if (r11 == r12) goto L_0x0171
            int r11 = r4.getAddress()
            de.innosystec.unrar.unpack.ppm.SubAllocator r12 = r13.subAlloc
            int r12 = r12.getPText()
            if (r11 > r12) goto L_0x0106
            r11 = 0
            goto L_0x00bc
        L_0x0106:
            de.innosystec.unrar.unpack.ppm.FreqData r11 = r4.getFreqData()
            int r11 = r11.getStats()
            r3.setAddress(r11)
            int r11 = r3.getSymbol()
            int r12 = r10.getSymbol()
            if (r11 == r12) goto L_0x0128
        L_0x011b:
            r3.incAddress()
            int r11 = r3.getSymbol()
            int r12 = r10.getSymbol()
            if (r11 != r12) goto L_0x011b
        L_0x0128:
            int r11 = r3.getFreq()
            int r0 = r11 + -1
            de.innosystec.unrar.unpack.ppm.FreqData r11 = r4.getFreqData()
            int r11 = r11.getSummFreq()
            int r12 = r4.getNumStats()
            int r11 = r11 - r12
            int r7 = r11 - r0
            int r11 = r0 * 2
            if (r11 > r7) goto L_0x0166
            int r11 = r0 * 5
            if (r11 <= r7) goto L_0x0164
            r11 = 1
        L_0x0146:
            int r11 = r11 + 1
            r10.setFreq(r11)
        L_0x014b:
            int[] r11 = r13.ps
            int r5 = r5 + -1
            r11 = r11[r5]
            r8.setAddress(r11)
            int r11 = r4.createChild(r13, r8, r10)
            r4.setAddress(r11)
            int r11 = r4.getAddress()
            if (r11 != 0) goto L_0x017d
            r11 = 0
            goto L_0x00bc
        L_0x0164:
            r11 = 0
            goto L_0x0146
        L_0x0166:
            int r11 = r0 * 2
            int r12 = r7 * 3
            int r11 = r11 + r12
            int r11 = r11 + -1
            int r12 = r7 * 2
            int r11 = r11 / r12
            goto L_0x0146
        L_0x0171:
            de.innosystec.unrar.unpack.ppm.State r11 = r4.getOneState()
            int r11 = r11.getFreq()
            r10.setFreq(r11)
            goto L_0x014b
        L_0x017d:
            if (r5 != 0) goto L_0x014b
            int r11 = r4.getAddress()
            goto L_0x00bc
        L_0x0185:
            r5 = r6
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: de.innosystec.unrar.unpack.ppm.ModelPPM.createSuccessors(boolean, de.innosystec.unrar.unpack.ppm.State):int");
    }

    private void updateModelRestart() {
        restartModelRare();
        this.escCount = 0;
    }

    private void updateModel() {
        int i;
        int i2;
        int cf;
        StateRef fs = this.tempStateRef1;
        fs.setValues(this.foundState);
        State p = this.tempState3.init(getHeap());
        State tempState = this.tempState4.init(getHeap());
        PPMContext pc = this.tempPPMContext3.init(getHeap());
        PPMContext successor = this.tempPPMContext4.init(getHeap());
        pc.setAddress(this.minContext.getSuffix());
        if (fs.getFreq() < 31 && pc.getAddress() != 0) {
            if (pc.getNumStats() != 1) {
                p.setAddress(pc.getFreqData().getStats());
                if (p.getSymbol() != fs.getSymbol()) {
                    do {
                        p.incAddress();
                    } while (p.getSymbol() != fs.getSymbol());
                    tempState.setAddress(p.getAddress() - 6);
                    if (p.getFreq() >= tempState.getFreq()) {
                        State.ppmdSwap(p, tempState);
                        p.decAddress();
                    }
                }
                if (p.getFreq() < 115) {
                    p.incFreq(2);
                    pc.getFreqData().incSummFreq(2);
                }
            } else {
                p.setAddress(pc.getOneState().getAddress());
                if (p.getFreq() < 32) {
                    p.incFreq(1);
                }
            }
        }
        if (this.orderFall == 0) {
            this.foundState.setSuccessor(createSuccessors(true, p));
            this.minContext.setAddress(this.foundState.getSuccessor());
            this.maxContext.setAddress(this.foundState.getSuccessor());
            if (this.minContext.getAddress() == 0) {
                updateModelRestart();
                return;
            }
            return;
        }
        this.subAlloc.getHeap()[this.subAlloc.getPText()] = (byte) fs.getSymbol();
        this.subAlloc.incPText();
        successor.setAddress(this.subAlloc.getPText());
        if (this.subAlloc.getPText() >= this.subAlloc.getFakeUnitsStart()) {
            updateModelRestart();
            return;
        }
        if (fs.getSuccessor() != 0) {
            if (fs.getSuccessor() <= this.subAlloc.getPText()) {
                fs.setSuccessor(createSuccessors(false, p));
                if (fs.getSuccessor() == 0) {
                    updateModelRestart();
                    return;
                }
            }
            int i3 = this.orderFall - 1;
            this.orderFall = i3;
            if (i3 == 0) {
                successor.setAddress(fs.getSuccessor());
                if (this.maxContext.getAddress() != this.minContext.getAddress()) {
                    this.subAlloc.decPText(1);
                }
            }
        } else {
            this.foundState.setSuccessor(successor.getAddress());
            fs.setSuccessor(this.minContext);
        }
        int ns = this.minContext.getNumStats();
        int s0 = (this.minContext.getFreqData().getSummFreq() - ns) - (fs.getFreq() - 1);
        pc.setAddress(this.maxContext.getAddress());
        while (pc.getAddress() != this.minContext.getAddress()) {
            int ns1 = pc.getNumStats();
            if (ns1 != 1) {
                if ((ns1 & 1) == 0) {
                    pc.getFreqData().setStats(this.subAlloc.expandUnits(pc.getFreqData().getStats(), ns1 >>> 1));
                    if (pc.getFreqData().getStats() == 0) {
                        updateModelRestart();
                        return;
                    }
                }
                pc.getFreqData().incSummFreq((ns1 * 2 < ns ? 1 : 0) + (((ns1 * 4 <= ns ? 1 : 0) & (pc.getFreqData().getSummFreq() <= ns1 * 8 ? 1 : 0)) * 2));
            } else {
                p.setAddress(this.subAlloc.allocUnits(1));
                if (p.getAddress() == 0) {
                    updateModelRestart();
                    return;
                }
                p.setValues(pc.getOneState());
                pc.getFreqData().setStats(p);
                if (p.getFreq() < 30) {
                    p.incFreq(p.getFreq());
                } else {
                    p.setFreq(120);
                }
                pc.getFreqData().setSummFreq((ns > 3 ? 1 : 0) + this.initEsc + p.getFreq());
            }
            int cf2 = fs.getFreq() * 2 * (pc.getFreqData().getSummFreq() + 6);
            int sf = s0 + pc.getFreqData().getSummFreq();
            if (cf2 < sf * 6) {
                cf = (cf2 > sf ? 1 : 0) + 1 + (cf2 >= sf * 4 ? 1 : 0);
                pc.getFreqData().incSummFreq(3);
            } else {
                int i4 = (cf2 >= sf * 9 ? 1 : 0) + 4;
                if (cf2 >= sf * 12) {
                    i = 1;
                } else {
                    i = 0;
                }
                int i5 = i4 + i;
                if (cf2 >= sf * 15) {
                    i2 = 1;
                } else {
                    i2 = 0;
                }
                cf = i5 + i2;
                pc.getFreqData().incSummFreq(cf);
            }
            p.setAddress(pc.getFreqData().getStats() + (ns1 * 6));
            p.setSuccessor(successor);
            p.setSymbol(fs.getSymbol());
            p.setFreq(cf);
            pc.setNumStats(ns1 + 1);
            pc.setAddress(pc.getSuffix());
        }
        int address = fs.getSuccessor();
        this.maxContext.setAddress(address);
        this.minContext.setAddress(address);
    }

    public String toString() {
        return "ModelPPM[" + "\n  numMasked=" + this.numMasked + "\n  initEsc=" + this.initEsc + "\n  orderFall=" + this.orderFall + "\n  maxOrder=" + this.maxOrder + "\n  runLength=" + this.runLength + "\n  initRL=" + this.initRL + "\n  escCount=" + this.escCount + "\n  prevSuccess=" + this.prevSuccess + "\n  foundState=" + this.foundState + "\n  coder=" + this.coder + "\n  subAlloc=" + this.subAlloc + "\n]";
    }
}
