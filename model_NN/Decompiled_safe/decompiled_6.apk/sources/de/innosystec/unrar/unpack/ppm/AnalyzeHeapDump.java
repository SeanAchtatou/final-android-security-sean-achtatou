package de.innosystec.unrar.unpack.ppm;

import de.innosystec.unrar.unpack.vm.RarVM;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class AnalyzeHeapDump {
    public static void main(String[] argv) {
        File cfile = new File("P:\\test\\heapdumpc");
        File jfile = new File("P:\\test\\heapdumpj");
        if (!cfile.exists()) {
            System.err.println("File not found: " + cfile.getAbsolutePath());
        } else if (!jfile.exists()) {
            System.err.println("File not found: " + jfile.getAbsolutePath());
        } else {
            long clen = cfile.length();
            long jlen = jfile.length();
            if (clen != jlen) {
                System.out.println("File size mismatch");
                System.out.println("clen = " + clen);
                System.out.println("jlen = " + jlen);
            }
            long len = Math.min(clen, jlen);
            InputStream cin = null;
            InputStream jin = null;
            try {
                InputStream cin2 = new BufferedInputStream(new FileInputStream(cfile), RarVM.VM_MEMSIZE);
                try {
                    InputStream jin2 = new BufferedInputStream(new FileInputStream(jfile), RarVM.VM_MEMSIZE);
                    boolean matching = true;
                    boolean mismatchFound = false;
                    long startOff = 0;
                    long off = 0;
                    while (off < len) {
                        try {
                            if (cin2.read() != jin2.read()) {
                                if (matching) {
                                    startOff = off;
                                    matching = false;
                                    mismatchFound = true;
                                }
                            } else if (!matching) {
                                printMismatch(startOff, off);
                                matching = true;
                            }
                            off++;
                        } catch (IOException e) {
                            e = e;
                            jin = jin2;
                            cin = cin2;
                            try {
                                e.printStackTrace();
                                try {
                                    cin.close();
                                    jin.close();
                                } catch (IOException e2) {
                                    e2.printStackTrace();
                                    return;
                                }
                            } catch (Throwable th) {
                                th = th;
                                try {
                                    cin.close();
                                    jin.close();
                                } catch (IOException e3) {
                                    e3.printStackTrace();
                                }
                                throw th;
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            jin = jin2;
                            cin = cin2;
                            cin.close();
                            jin.close();
                            throw th;
                        }
                    }
                    if (!matching) {
                        printMismatch(startOff, off);
                    }
                    if (!mismatchFound) {
                        System.out.println("Files are identical");
                    }
                    System.out.println("Done");
                    try {
                        cin2.close();
                        jin2.close();
                    } catch (IOException e4) {
                        e4.printStackTrace();
                    }
                } catch (IOException e5) {
                    e = e5;
                    cin = cin2;
                    e.printStackTrace();
                    cin.close();
                    jin.close();
                } catch (Throwable th3) {
                    th = th3;
                    cin = cin2;
                    cin.close();
                    jin.close();
                    throw th;
                }
            } catch (IOException e6) {
                e = e6;
                e.printStackTrace();
                cin.close();
                jin.close();
            }
        }
    }

    private static void printMismatch(long startOff, long bytesRead) {
        System.out.println("Mismatch: off=" + startOff + "(0x" + Long.toHexString(startOff) + "), len=" + (bytesRead - startOff));
    }
}
