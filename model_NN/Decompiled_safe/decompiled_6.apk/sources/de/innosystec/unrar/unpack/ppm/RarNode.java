package de.innosystec.unrar.unpack.ppm;

import de.innosystec.unrar.io.Raw;

public class RarNode extends Pointer {
    public static final int size = 4;
    private int next;

    public RarNode(byte[] mem) {
        super(mem);
    }

    public int getNext() {
        if (this.mem != null) {
            this.next = Raw.readIntLittleEndian(this.mem, this.pos);
        }
        return this.next;
    }

    public void setNext(RarNode next2) {
        setNext(next2.getAddress());
    }

    public void setNext(int next2) {
        this.next = next2;
        if (this.mem != null) {
            Raw.writeIntLittleEndian(this.mem, this.pos, next2);
        }
    }

    public String toString() {
        return "State[" + "\n  pos=" + this.pos + "\n  size=" + 4 + "\n  next=" + getNext() + "\n]";
    }
}
