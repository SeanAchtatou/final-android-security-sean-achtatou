package de.innosystec.unrar.unpack.vm;

import android.support.v4.view.MotionEventCompat;
import com.jianq.net.JQBasicNetwork;
import de.innosystec.unrar.crc.RarCRC;
import de.innosystec.unrar.io.Raw;
import de.innosystec.unrar.unpack.decode.Compress;
import de.innosystec.unrar.unpack.ppm.ModelPPM;
import de.innosystec.unrar.unpack.ppm.RangeCoder;
import de.innosystec.unrar.unpack.ppm.SubAllocator;
import java.util.List;
import java.util.Vector;

public class RarVM extends BitInput {
    private static final long UINT_MASK = -1;
    public static final int VM_FIXEDGLOBALSIZE = 64;
    public static final int VM_GLOBALMEMADDR = 245760;
    public static final int VM_GLOBALMEMSIZE = 8192;
    public static final int VM_MEMMASK = 262143;
    public static final int VM_MEMSIZE = 262144;
    private static final int regCount = 8;
    private int IP;
    private int[] R = new int[8];
    private int codeSize;
    private int flags;
    private int maxOpCount = 25000000;
    private byte[] mem = null;

    public void init() {
        if (this.mem == null) {
            this.mem = new byte[262148];
        }
    }

    private boolean isVMMem(byte[] mem2) {
        return this.mem == mem2;
    }

    private int getValue(boolean byteMode, byte[] mem2, int offset) {
        if (byteMode) {
            if (isVMMem(mem2)) {
                return mem2[offset];
            }
            return mem2[offset] & 255;
        } else if (isVMMem(mem2)) {
            return Raw.readIntLittleEndian(mem2, offset);
        } else {
            return Raw.readIntBigEndian(mem2, offset);
        }
    }

    private void setValue(boolean byteMode, byte[] mem2, int offset, int value) {
        if (byteMode) {
            if (isVMMem(mem2)) {
                mem2[offset] = (byte) value;
            } else {
                mem2[offset] = (byte) ((mem2[offset] & 0) | ((byte) (value & MotionEventCompat.ACTION_MASK)));
            }
        } else if (isVMMem(mem2)) {
            Raw.writeIntLittleEndian(mem2, offset, value);
        } else {
            Raw.writeIntBigEndian(mem2, offset, value);
        }
    }

    public void setLowEndianValue(byte[] mem2, int offset, int value) {
        Raw.writeIntLittleEndian(mem2, offset, value);
    }

    public void setLowEndianValue(Vector<Byte> mem2, int offset, int value) {
        mem2.set(offset + 0, Byte.valueOf((byte) (value & MotionEventCompat.ACTION_MASK)));
        mem2.set(offset + 1, Byte.valueOf((byte) ((value >>> 8) & MotionEventCompat.ACTION_MASK)));
        mem2.set(offset + 2, Byte.valueOf((byte) ((value >>> 16) & MotionEventCompat.ACTION_MASK)));
        mem2.set(offset + 3, Byte.valueOf((byte) ((value >>> 24) & MotionEventCompat.ACTION_MASK)));
    }

    private int getOperand(VMPreparedOperand cmdOp) {
        if (cmdOp.getType() == VMOpType.VM_OPREGMEM) {
            return Raw.readIntLittleEndian(this.mem, (cmdOp.getOffset() + cmdOp.getBase()) & VM_MEMMASK);
        }
        return Raw.readIntLittleEndian(this.mem, cmdOp.getOffset());
    }

    public void execute(VMPreparedProgram prg) {
        for (int i = 0; i < prg.getInitR().length; i++) {
            this.R[i] = prg.getInitR()[i];
        }
        long globalSize = (long) (Math.min(prg.getGlobalData().size(), 8192) & -1);
        if (globalSize != 0) {
            for (int i2 = 0; ((long) i2) < globalSize; i2++) {
                this.mem[VM_GLOBALMEMADDR + i2] = prg.getGlobalData().get(i2).byteValue();
            }
        }
        long staticSize = Math.min((long) prg.getStaticData().size(), 8192 - globalSize) & UINT_MASK;
        if (staticSize != 0) {
            for (int i3 = 0; ((long) i3) < staticSize; i3++) {
                this.mem[VM_GLOBALMEMADDR + ((int) globalSize) + i3] = prg.getStaticData().get(i3).byteValue();
            }
        }
        this.R[7] = 262144;
        this.flags = 0;
        List<VMPreparedCommand> preparedCode = prg.getAltCmd().size() != 0 ? prg.getAltCmd() : prg.getCmd();
        if (!ExecuteCode(preparedCode, prg.getCmdCount())) {
            preparedCode.get(0).setOpCode(VMCommands.VM_RET);
        }
        int newBlockPos = getValue(false, this.mem, 245792) & VM_MEMMASK;
        int newBlockSize = getValue(false, this.mem, 245788) & VM_MEMMASK;
        if (newBlockPos + newBlockSize >= 262144) {
            newBlockPos = 0;
            newBlockSize = 0;
        }
        prg.setFilteredDataOffset(newBlockPos);
        prg.setFilteredDataSize(newBlockSize);
        prg.getGlobalData().clear();
        int dataSize = Math.min(getValue(false, this.mem, 245808), 8128);
        if (dataSize != 0) {
            prg.getGlobalData().setSize(dataSize + 64);
            for (int i4 = 0; i4 < dataSize + 64; i4++) {
                prg.getGlobalData().set(i4, Byte.valueOf(this.mem[VM_GLOBALMEMADDR + i4]));
            }
        }
    }

    public byte[] getMem() {
        return this.mem;
    }

    private boolean setIP(int ip) {
        if (ip >= this.codeSize) {
            return true;
        }
        int i = this.maxOpCount - 1;
        this.maxOpCount = i;
        if (i <= 0) {
            return false;
        }
        this.IP = ip;
        return true;
    }

    private boolean ExecuteCode(List<VMPreparedCommand> preparedCode, int cmdCount) {
        int i;
        int i2;
        int flag;
        int flag2;
        this.maxOpCount = 25000000;
        this.codeSize = cmdCount;
        this.IP = 0;
        while (true) {
            VMPreparedCommand cmd = preparedCode.get(this.IP);
            int op1 = getOperand(cmd.getOp1());
            int op2 = getOperand(cmd.getOp2());
            switch (AnonymousClass1.$SwitchMap$de$innosystec$unrar$unpack$vm$VMCommands[cmd.getOpCode().ordinal()]) {
                case 1:
                    setValue(cmd.isByteMode(), this.mem, op1, getValue(cmd.isByteMode(), this.mem, op2));
                    break;
                case 2:
                    setValue(true, this.mem, op1, getValue(true, this.mem, op2));
                    break;
                case 3:
                    setValue(false, this.mem, op1, getValue(false, this.mem, op2));
                    break;
                case 4:
                    int value1 = getValue(cmd.isByteMode(), this.mem, op1);
                    int result = value1 - getValue(cmd.isByteMode(), this.mem, op2);
                    if (result != 0) {
                        this.flags = result > value1 ? 1 : (VMFlags.VM_FS.getFlag() & result) | 0;
                        break;
                    } else {
                        this.flags = VMFlags.VM_FZ.getFlag();
                        break;
                    }
                case 5:
                    int value12 = getValue(true, this.mem, op1);
                    int result2 = value12 - getValue(true, this.mem, op2);
                    if (result2 != 0) {
                        this.flags = result2 > value12 ? 1 : (VMFlags.VM_FS.getFlag() & result2) | 0;
                        break;
                    } else {
                        this.flags = VMFlags.VM_FZ.getFlag();
                        break;
                    }
                case 6:
                    int value13 = getValue(false, this.mem, op1);
                    int result3 = value13 - getValue(false, this.mem, op2);
                    if (result3 != 0) {
                        this.flags = result3 > value13 ? 1 : (VMFlags.VM_FS.getFlag() & result3) | 0;
                        break;
                    } else {
                        this.flags = VMFlags.VM_FZ.getFlag();
                        break;
                    }
                case 7:
                    int value14 = getValue(cmd.isByteMode(), this.mem, op1);
                    int result4 = (int) ((((long) value14) + ((long) getValue(cmd.isByteMode(), this.mem, op2))) & UINT_MASK);
                    if (cmd.isByteMode()) {
                        result4 &= MotionEventCompat.ACTION_MASK;
                        if (result4 < value14) {
                            flag2 = 1;
                        } else {
                            flag2 = (result4 == 0 ? VMFlags.VM_FZ.getFlag() : (result4 & 128) != 0 ? VMFlags.VM_FS.getFlag() : 0) | 0;
                        }
                        this.flags = flag2;
                    } else {
                        if (result4 < value14) {
                            flag = 1;
                        } else {
                            flag = (result4 == 0 ? VMFlags.VM_FZ.getFlag() : VMFlags.VM_FS.getFlag() & result4) | 0;
                        }
                        this.flags = flag;
                    }
                    setValue(cmd.isByteMode(), this.mem, op1, result4);
                    break;
                case 8:
                    setValue(true, this.mem, op1, (int) (((long) getValue(true, this.mem, op1)) & (UINT_MASK + ((long) getValue(true, this.mem, op2))) & UINT_MASK));
                    break;
                case 9:
                    setValue(false, this.mem, op1, (int) (((long) getValue(false, this.mem, op1)) & (UINT_MASK + ((long) getValue(false, this.mem, op2))) & UINT_MASK));
                    break;
                case 10:
                    int value15 = getValue(cmd.isByteMode(), this.mem, op1);
                    int result5 = (int) (((long) value15) & (UINT_MASK - ((long) getValue(cmd.isByteMode(), this.mem, op2))) & UINT_MASK);
                    this.flags = result5 == 0 ? VMFlags.VM_FZ.getFlag() : result5 > value15 ? 1 : (VMFlags.VM_FS.getFlag() & result5) | 0;
                    setValue(cmd.isByteMode(), this.mem, op1, result5);
                    break;
                case 11:
                    setValue(true, this.mem, op1, (int) (((long) getValue(true, this.mem, op1)) & (UINT_MASK - ((long) getValue(true, this.mem, op2))) & UINT_MASK));
                    break;
                case 12:
                    setValue(false, this.mem, op1, (int) (((long) getValue(false, this.mem, op1)) & (UINT_MASK - ((long) getValue(false, this.mem, op2))) & UINT_MASK));
                    break;
                case JQBasicNetwork.CR:
                    if ((this.flags & VMFlags.VM_FZ.getFlag()) != 0) {
                        setIP(getValue(false, this.mem, op1));
                        continue;
                    }
                    break;
                case ModelPPM.TOT_BITS:
                    if ((this.flags & VMFlags.VM_FZ.getFlag()) == 0) {
                        setIP(getValue(false, this.mem, op1));
                        continue;
                    }
                    break;
                case 15:
                    int result6 = (int) (((long) getValue(cmd.isByteMode(), this.mem, op1)) & 0);
                    if (cmd.isByteMode()) {
                        result6 &= MotionEventCompat.ACTION_MASK;
                    }
                    setValue(cmd.isByteMode(), this.mem, op1, result6);
                    this.flags = result6 == 0 ? VMFlags.VM_FZ.getFlag() : VMFlags.VM_FS.getFlag() & result6;
                    break;
                case 16:
                    setValue(true, this.mem, op1, (int) (((long) getValue(true, this.mem, op1)) & 0));
                    break;
                case 17:
                    setValue(false, this.mem, op1, (int) (((long) getValue(false, this.mem, op1)) & 0));
                    break;
                case 18:
                    int result7 = (int) (((long) getValue(cmd.isByteMode(), this.mem, op1)) & -2);
                    setValue(cmd.isByteMode(), this.mem, op1, result7);
                    this.flags = result7 == 0 ? VMFlags.VM_FZ.getFlag() : VMFlags.VM_FS.getFlag() & result7;
                    break;
                case 19:
                    setValue(true, this.mem, op1, (int) (((long) getValue(true, this.mem, op1)) & -2));
                    break;
                case 20:
                    setValue(false, this.mem, op1, (int) (((long) getValue(false, this.mem, op1)) & -2));
                    break;
                case 21:
                    setIP(getValue(false, this.mem, op1));
                    continue;
                case 22:
                    int result8 = getValue(cmd.isByteMode(), this.mem, op1) ^ getValue(cmd.isByteMode(), this.mem, op2);
                    this.flags = result8 == 0 ? VMFlags.VM_FZ.getFlag() : VMFlags.VM_FS.getFlag() & result8;
                    setValue(cmd.isByteMode(), this.mem, op1, result8);
                    break;
                case 23:
                    int result9 = getValue(cmd.isByteMode(), this.mem, op1) & getValue(cmd.isByteMode(), this.mem, op2);
                    this.flags = result9 == 0 ? VMFlags.VM_FZ.getFlag() : VMFlags.VM_FS.getFlag() & result9;
                    setValue(cmd.isByteMode(), this.mem, op1, result9);
                    break;
                case 24:
                    int result10 = getValue(cmd.isByteMode(), this.mem, op1) | getValue(cmd.isByteMode(), this.mem, op2);
                    this.flags = result10 == 0 ? VMFlags.VM_FZ.getFlag() : VMFlags.VM_FS.getFlag() & result10;
                    setValue(cmd.isByteMode(), this.mem, op1, result10);
                    break;
                case 25:
                    int result11 = getValue(cmd.isByteMode(), this.mem, op1) & getValue(cmd.isByteMode(), this.mem, op2);
                    this.flags = result11 == 0 ? VMFlags.VM_FZ.getFlag() : VMFlags.VM_FS.getFlag() & result11;
                    break;
                case 26:
                    if ((this.flags & VMFlags.VM_FS.getFlag()) != 0) {
                        setIP(getValue(false, this.mem, op1));
                        continue;
                    }
                    break;
                case 27:
                    if ((this.flags & VMFlags.VM_FS.getFlag()) == 0) {
                        setIP(getValue(false, this.mem, op1));
                        continue;
                    }
                    break;
                case 28:
                    if ((this.flags & VMFlags.VM_FC.getFlag()) != 0) {
                        setIP(getValue(false, this.mem, op1));
                        continue;
                    }
                    break;
                case 29:
                    if ((this.flags & (VMFlags.VM_FC.getFlag() | VMFlags.VM_FZ.getFlag())) != 0) {
                        setIP(getValue(false, this.mem, op1));
                        continue;
                    }
                    break;
                case 30:
                    if ((this.flags & (VMFlags.VM_FC.getFlag() | VMFlags.VM_FZ.getFlag())) == 0) {
                        setIP(getValue(false, this.mem, op1));
                        continue;
                    }
                    break;
                case 31:
                    if ((this.flags & VMFlags.VM_FC.getFlag()) == 0) {
                        setIP(getValue(false, this.mem, op1));
                        continue;
                    }
                    break;
                case 32:
                    int[] iArr = this.R;
                    iArr[7] = iArr[7] - 4;
                    setValue(false, this.mem, this.R[7] & VM_MEMMASK, getValue(false, this.mem, op1));
                    break;
                case 33:
                    setValue(false, this.mem, op1, getValue(false, this.mem, this.R[7] & VM_MEMMASK));
                    int[] iArr2 = this.R;
                    iArr2[7] = iArr2[7] + 4;
                    break;
                case 34:
                    int[] iArr3 = this.R;
                    iArr3[7] = iArr3[7] - 4;
                    setValue(false, this.mem, this.R[7] & VM_MEMMASK, this.IP + 1);
                    setIP(getValue(false, this.mem, op1));
                    continue;
                case 35:
                    setValue(cmd.isByteMode(), this.mem, op1, getValue(cmd.isByteMode(), this.mem, op1) ^ -1);
                    break;
                case 36:
                    int value16 = getValue(cmd.isByteMode(), this.mem, op1);
                    int value2 = getValue(cmd.isByteMode(), this.mem, op2);
                    int result12 = value16 << value2;
                    this.flags = (((value16 << (value2 + -1)) & Integer.MIN_VALUE) != 0 ? VMFlags.VM_FC.getFlag() : 0) | (result12 == 0 ? VMFlags.VM_FZ.getFlag() : VMFlags.VM_FS.getFlag() & result12);
                    setValue(cmd.isByteMode(), this.mem, op1, result12);
                    break;
                case 37:
                    int value17 = getValue(cmd.isByteMode(), this.mem, op1);
                    int value22 = getValue(cmd.isByteMode(), this.mem, op2);
                    int result13 = value17 >>> value22;
                    this.flags = (result13 == 0 ? VMFlags.VM_FZ.getFlag() : VMFlags.VM_FS.getFlag() & result13) | ((value17 >>> (value22 - 1)) & VMFlags.VM_FC.getFlag());
                    setValue(cmd.isByteMode(), this.mem, op1, result13);
                    break;
                case SubAllocator.N_INDEXES:
                    int value18 = getValue(cmd.isByteMode(), this.mem, op1);
                    int value23 = getValue(cmd.isByteMode(), this.mem, op2);
                    int result14 = value18 >> value23;
                    this.flags = (result14 == 0 ? VMFlags.VM_FZ.getFlag() : VMFlags.VM_FS.getFlag() & result14) | ((value18 >> (value23 - 1)) & VMFlags.VM_FC.getFlag());
                    setValue(cmd.isByteMode(), this.mem, op1, result14);
                    break;
                case 39:
                    int result15 = -getValue(cmd.isByteMode(), this.mem, op1);
                    this.flags = result15 == 0 ? VMFlags.VM_FZ.getFlag() : VMFlags.VM_FC.getFlag() | (VMFlags.VM_FS.getFlag() & result15);
                    setValue(cmd.isByteMode(), this.mem, op1, result15);
                    break;
                case 40:
                    setValue(true, this.mem, op1, -getValue(true, this.mem, op1));
                    break;
                case 41:
                    setValue(false, this.mem, op1, -getValue(false, this.mem, op1));
                    break;
                case 42:
                    int i3 = 0;
                    int SP = this.R[7] - 4;
                    while (i3 < 8) {
                        setValue(false, this.mem, 262143 & SP, this.R[i3]);
                        i3++;
                        SP -= 4;
                    }
                    int[] iArr4 = this.R;
                    iArr4[7] = iArr4[7] - 32;
                    break;
                case 43:
                    int i4 = 0;
                    int SP2 = this.R[7];
                    while (i4 < 8) {
                        this.R[7 - i4] = getValue(false, this.mem, 262143 & SP2);
                        i4++;
                        SP2 += 4;
                    }
                    break;
                case 44:
                    int[] iArr5 = this.R;
                    iArr5[7] = iArr5[7] - 4;
                    setValue(false, this.mem, this.R[7] & VM_MEMMASK, this.flags);
                    break;
                case 45:
                    this.flags = getValue(false, this.mem, this.R[7] & VM_MEMMASK);
                    int[] iArr6 = this.R;
                    iArr6[7] = iArr6[7] + 4;
                    break;
                case 46:
                    setValue(false, this.mem, op1, getValue(true, this.mem, op2));
                    break;
                case 47:
                    setValue(false, this.mem, op1, (byte) getValue(true, this.mem, op2));
                    break;
                case Compress.DC20:
                    int value19 = getValue(cmd.isByteMode(), this.mem, op1);
                    setValue(cmd.isByteMode(), this.mem, op1, getValue(cmd.isByteMode(), this.mem, op2));
                    setValue(cmd.isByteMode(), this.mem, op2, value19);
                    break;
                case 49:
                    setValue(cmd.isByteMode(), this.mem, op1, (int) (((long) getValue(cmd.isByteMode(), this.mem, op1)) & (UINT_MASK * ((long) getValue(cmd.isByteMode(), this.mem, op2))) & UINT_MASK & UINT_MASK));
                    break;
                case 50:
                    int divider = getValue(cmd.isByteMode(), this.mem, op2);
                    if (divider != 0) {
                        setValue(cmd.isByteMode(), this.mem, op1, getValue(cmd.isByteMode(), this.mem, op1) / divider);
                        break;
                    }
                    break;
                case 51:
                    int value110 = getValue(cmd.isByteMode(), this.mem, op1);
                    int FC = this.flags & VMFlags.VM_FC.getFlag();
                    int result16 = (int) (((long) value110) & (UINT_MASK + ((long) getValue(cmd.isByteMode(), this.mem, op2))) & (UINT_MASK + ((long) FC)) & UINT_MASK);
                    if (cmd.isByteMode()) {
                        result16 &= MotionEventCompat.ACTION_MASK;
                    }
                    if (result16 < value110 || (result16 == value110 && FC != 0)) {
                        i2 = 1;
                    } else {
                        i2 = (result16 == 0 ? VMFlags.VM_FZ.getFlag() : VMFlags.VM_FS.getFlag() & result16) | 0;
                    }
                    this.flags = i2;
                    setValue(cmd.isByteMode(), this.mem, op1, result16);
                    break;
                case 52:
                    int value111 = getValue(cmd.isByteMode(), this.mem, op1);
                    int FC2 = this.flags & VMFlags.VM_FC.getFlag();
                    int result17 = (int) (((long) value111) & (UINT_MASK - ((long) getValue(cmd.isByteMode(), this.mem, op2))) & (UINT_MASK - ((long) FC2)) & UINT_MASK);
                    if (cmd.isByteMode()) {
                        result17 &= MotionEventCompat.ACTION_MASK;
                    }
                    if (result17 > value111 || (result17 == value111 && FC2 != 0)) {
                        i = 1;
                    } else {
                        i = (result17 == 0 ? VMFlags.VM_FZ.getFlag() : VMFlags.VM_FS.getFlag() & result17) | 0;
                    }
                    this.flags = i;
                    setValue(cmd.isByteMode(), this.mem, op1, result17);
                    break;
                case 53:
                    if (this.R[7] >= 262144) {
                        return true;
                    }
                    setIP(getValue(false, this.mem, this.R[7] & VM_MEMMASK));
                    int[] iArr7 = this.R;
                    iArr7[7] = iArr7[7] + 4;
                    continue;
                case 54:
                    ExecuteStandardFilter(VMStandardFilters.findFilter(cmd.getOp1().getData()));
                    break;
            }
            this.IP = this.IP + 1;
            this.maxOpCount = this.maxOpCount - 1;
        }
    }

    public void prepare(byte[] code, int codeSize2, VMPreparedProgram prg) {
        int distance;
        InitBitInput();
        int cpLength = Math.min(32768, codeSize2);
        for (int i = 0; i < cpLength; i++) {
            byte[] bArr = this.inBuf;
            bArr[i] = (byte) (bArr[i] | code[i]);
        }
        byte xorSum = 0;
        for (int i2 = 1; i2 < codeSize2; i2++) {
            xorSum = (byte) (code[i2] ^ xorSum);
        }
        faddbits(8);
        prg.setCmdCount(0);
        if (xorSum == code[0]) {
            VMStandardFilters filterType = IsStandardFilter(code, codeSize2);
            if (filterType != VMStandardFilters.VMSF_NONE) {
                VMPreparedCommand curCmd = new VMPreparedCommand();
                curCmd.setOpCode(VMCommands.VM_STANDARD);
                curCmd.getOp1().setData(filterType.getFilter());
                curCmd.getOp1().setType(VMOpType.VM_OPNONE);
                curCmd.getOp2().setType(VMOpType.VM_OPNONE);
                codeSize2 = 0;
                prg.getCmd().add(curCmd);
                prg.setCmdCount(prg.getCmdCount() + 1);
            }
            int dataFlag = fgetbits();
            faddbits(1);
            if ((32768 & dataFlag) != 0) {
                long dataSize = ((long) ReadData(this)) & 0;
                int i3 = 0;
                while (this.inAddr < codeSize2 && ((long) i3) < dataSize) {
                    prg.getStaticData().add(Byte.valueOf((byte) (fgetbits() >> 8)));
                    faddbits(8);
                    i3++;
                }
            }
            while (this.inAddr < codeSize2) {
                VMPreparedCommand curCmd2 = new VMPreparedCommand();
                int data = fgetbits();
                if ((32768 & data) == 0) {
                    curCmd2.setOpCode(VMCommands.findVMCommand(data >> 12));
                    faddbits(4);
                } else {
                    curCmd2.setOpCode(VMCommands.findVMCommand((data >> 10) - 24));
                    faddbits(6);
                }
                if ((VMCmdFlags.VM_CmdFlags[curCmd2.getOpCode().getVMCommand()] & 4) != 0) {
                    curCmd2.setByteMode((fgetbits() >> 15) == 1);
                    faddbits(1);
                } else {
                    curCmd2.setByteMode(false);
                }
                curCmd2.getOp1().setType(VMOpType.VM_OPNONE);
                curCmd2.getOp2().setType(VMOpType.VM_OPNONE);
                int opNum = VMCmdFlags.VM_CmdFlags[curCmd2.getOpCode().getVMCommand()] & 3;
                if (opNum > 0) {
                    decodeArg(curCmd2.getOp1(), curCmd2.isByteMode());
                    if (opNum == 2) {
                        decodeArg(curCmd2.getOp2(), curCmd2.isByteMode());
                    } else if (curCmd2.getOp1().getType() == VMOpType.VM_OPINT && (VMCmdFlags.VM_CmdFlags[curCmd2.getOpCode().getVMCommand()] & 24) != 0) {
                        int distance2 = curCmd2.getOp1().getData();
                        if (distance2 >= 256) {
                            distance = distance2 - 256;
                        } else {
                            if (distance2 >= 136) {
                                distance2 -= 264;
                            } else if (distance2 >= 16) {
                                distance2 -= 8;
                            } else if (distance2 >= 8) {
                                distance2 -= 16;
                            }
                            distance = distance2 + prg.getCmdCount();
                        }
                        curCmd2.getOp1().setData(distance);
                    }
                }
                prg.setCmdCount(prg.getCmdCount() + 1);
                prg.getCmd().add(curCmd2);
            }
        }
        VMPreparedCommand curCmd3 = new VMPreparedCommand();
        curCmd3.setOpCode(VMCommands.VM_RET);
        curCmd3.getOp1().setType(VMOpType.VM_OPNONE);
        curCmd3.getOp2().setType(VMOpType.VM_OPNONE);
        prg.getCmd().add(curCmd3);
        prg.setCmdCount(prg.getCmdCount() + 1);
        if (codeSize2 != 0) {
            optimize(prg);
        }
    }

    private void decodeArg(VMPreparedOperand op, boolean byteMode) {
        int data = fgetbits();
        if ((32768 & data) != 0) {
            op.setType(VMOpType.VM_OPREG);
            op.setData((data >> 12) & 7);
            op.setOffset(op.getData());
            faddbits(4);
        } else if ((49152 & data) == 0) {
            op.setType(VMOpType.VM_OPINT);
            if (byteMode) {
                op.setData((data >> 6) & MotionEventCompat.ACTION_MASK);
                faddbits(10);
                return;
            }
            faddbits(2);
            op.setData(ReadData(this));
        } else {
            op.setType(VMOpType.VM_OPREGMEM);
            if ((data & 8192) == 0) {
                op.setData((data >> 10) & 7);
                op.setOffset(op.getData());
                op.setBase(0);
                faddbits(6);
                return;
            }
            if ((data & 4096) == 0) {
                op.setData((data >> 9) & 7);
                op.setOffset(op.getData());
                faddbits(7);
            } else {
                op.setData(0);
                faddbits(4);
            }
            op.setBase(ReadData(this));
        }
    }

    private void optimize(VMPreparedProgram prg) {
        VMCommands vMCommands;
        List<VMPreparedCommand> commands = prg.getCmd();
        for (VMPreparedCommand cmd : commands) {
            switch (cmd.getOpCode()) {
                case VM_MOV:
                    cmd.setOpCode(cmd.isByteMode() ? VMCommands.VM_MOVB : VMCommands.VM_MOVD);
                    break;
                case VM_MOVB:
                case VM_MOVD:
                default:
                    if ((VMCmdFlags.VM_CmdFlags[cmd.getOpCode().getVMCommand()] & VMCmdFlags.VMCF_CHFLAGS) == 0) {
                        break;
                    } else {
                        boolean flagsRequired = false;
                        int i = commands.indexOf(cmd) + 1;
                        while (true) {
                            if (i < commands.size()) {
                                byte b = VMCmdFlags.VM_CmdFlags[commands.get(i).getOpCode().getVMCommand()];
                                if ((b & 56) != 0) {
                                    flagsRequired = true;
                                } else if ((b & VMCmdFlags.VMCF_CHFLAGS) == 0) {
                                    i++;
                                }
                            }
                        }
                        if (flagsRequired) {
                            break;
                        } else {
                            switch (cmd.getOpCode()) {
                                case VM_ADD:
                                    if (cmd.isByteMode()) {
                                        vMCommands = VMCommands.VM_ADDB;
                                    } else {
                                        vMCommands = VMCommands.VM_ADDD;
                                    }
                                    cmd.setOpCode(vMCommands);
                                    continue;
                                case VM_SUB:
                                    cmd.setOpCode(cmd.isByteMode() ? VMCommands.VM_SUBB : VMCommands.VM_SUBD);
                                    continue;
                                case VM_INC:
                                    cmd.setOpCode(cmd.isByteMode() ? VMCommands.VM_INCB : VMCommands.VM_INCD);
                                    continue;
                                case VM_DEC:
                                    cmd.setOpCode(cmd.isByteMode() ? VMCommands.VM_DECB : VMCommands.VM_DECD);
                                    continue;
                                case VM_NEG:
                                    cmd.setOpCode(cmd.isByteMode() ? VMCommands.VM_NEGB : VMCommands.VM_NEGD);
                                    continue;
                            }
                        }
                    }
                case VM_CMP:
                    cmd.setOpCode(cmd.isByteMode() ? VMCommands.VM_CMPB : VMCommands.VM_CMPD);
                    break;
            }
        }
    }

    public static int ReadData(BitInput rarVM) {
        int data;
        int data2 = rarVM.fgetbits();
        switch (49152 & data2) {
            case 0:
                rarVM.faddbits(6);
                return (data2 >> 10) & 15;
            case 16384:
                if ((data2 & 15360) == 0) {
                    data = ((data2 >> 2) & MotionEventCompat.ACTION_MASK) | -256;
                    rarVM.faddbits(14);
                } else {
                    data = (data2 >> 6) & MotionEventCompat.ACTION_MASK;
                    rarVM.faddbits(10);
                }
                return data;
            case 32768:
                rarVM.faddbits(2);
                int data3 = rarVM.fgetbits();
                rarVM.faddbits(16);
                return data3;
            default:
                rarVM.faddbits(2);
                rarVM.faddbits(16);
                rarVM.faddbits(16);
                return (rarVM.fgetbits() << 16) | rarVM.fgetbits();
        }
    }

    private VMStandardFilters IsStandardFilter(byte[] code, int codeSize2) {
        VMStandardFilterSignature[] stdList = {new VMStandardFilterSignature(53, -1386780537, VMStandardFilters.VMSF_E8), new VMStandardFilterSignature(57, 1020781950, VMStandardFilters.VMSF_E8E9), new VMStandardFilterSignature(120, 929663295, VMStandardFilters.VMSF_ITANIUM), new VMStandardFilterSignature(29, 235276157, VMStandardFilters.VMSF_DELTA), new VMStandardFilterSignature(149, 472669640, VMStandardFilters.VMSF_RGB), new VMStandardFilterSignature(216, -1132075263, VMStandardFilters.VMSF_AUDIO), new VMStandardFilterSignature(40, 1186579808, VMStandardFilters.VMSF_UPCASE)};
        int CodeCRC = RarCRC.checkCrc(-1, code, 0, code.length) ^ -1;
        for (int i = 0; i < stdList.length; i++) {
            if (stdList[i].getCRC() == CodeCRC && stdList[i].getLength() == code.length) {
                return stdList[i].getType();
            }
        }
        return VMStandardFilters.VMSF_NONE;
    }

    private void ExecuteStandardFilter(VMStandardFilters filterType) {
        long predicted;
        byte cmdMask;
        switch (filterType) {
            case VMSF_E8:
            case VMSF_E8E9:
                int dataSize = this.R[4];
                long fileOffset = (long) (this.R[6] & -1);
                if (dataSize < 245760) {
                    byte cmpByte2 = (byte) (filterType == VMStandardFilters.VMSF_E8E9 ? 233 : 232);
                    int curPos = 0;
                    while (true) {
                        int curPos2 = curPos;
                        if (curPos2 < dataSize - 4) {
                            curPos = curPos2 + 1;
                            byte curByte = this.mem[curPos2];
                            if (curByte == 232 || curByte == cmpByte2) {
                                long offset = ((long) curPos) + fileOffset;
                                long Addr = (long) getValue(false, this.mem, curPos);
                                if ((-2147483648L & Addr) != 0) {
                                    if (((Addr + offset) & -2147483648L) == 0) {
                                        setValue(false, this.mem, curPos, ((int) Addr) + RangeCoder.TOP);
                                    }
                                } else if (((Addr - ((long) RangeCoder.TOP)) & -2147483648L) != 0) {
                                    setValue(false, this.mem, curPos, (int) (Addr - offset));
                                }
                                curPos += 4;
                            }
                        } else {
                            return;
                        }
                    }
                } else {
                    return;
                }
                break;
            case VMSF_ITANIUM:
                int dataSize2 = this.R[4];
                long fileOffset2 = (long) (this.R[6] & -1);
                if (dataSize2 < 245760) {
                    int curPos3 = 0;
                    byte[] Masks = {4, 4, 6, 6, 0, 0, 7, 7, 4, 4, 0, 0, 4, 4, 0, 0};
                    long fileOffset3 = fileOffset2 >>> 4;
                    while (curPos3 < dataSize2 - 21) {
                        int Byte = (this.mem[curPos3] & 31) - 16;
                        if (Byte >= 0 && (cmdMask = Masks[Byte]) != 0) {
                            for (int i = 0; i <= 2; i++) {
                                if (((1 << i) & cmdMask) != 0) {
                                    int startPos = (i * 41) + 5;
                                    if (filterItanium_GetBits(curPos3, startPos + 37, 4) == 5) {
                                        filterItanium_SetBits(curPos3, ((int) (((long) filterItanium_GetBits(curPos3, startPos + 13, 20)) - fileOffset3)) & 1048575, startPos + 13, 20);
                                    }
                                }
                            }
                        }
                        curPos3 += 16;
                        fileOffset3++;
                    }
                    return;
                }
                return;
            case VMSF_DELTA:
                int dataSize3 = this.R[4] & -1;
                int channels = this.R[0] & -1;
                int srcPos = 0;
                int border = (dataSize3 * 2) & -1;
                setValue(false, this.mem, 245792, dataSize3);
                if (dataSize3 < 122880) {
                    int curChannel = 0;
                    while (curChannel < channels) {
                        byte PrevByte = 0;
                        int destPos = dataSize3 + curChannel;
                        while (true) {
                            int srcPos2 = srcPos;
                            if (destPos < border) {
                                byte[] bArr = this.mem;
                                srcPos = srcPos2 + 1;
                                PrevByte = (byte) (PrevByte - this.mem[srcPos2]);
                                bArr[destPos] = PrevByte;
                                destPos += channels;
                            } else {
                                curChannel++;
                                srcPos = srcPos2;
                            }
                        }
                    }
                    return;
                }
                return;
            case VMSF_RGB:
                int dataSize4 = this.R[4];
                int width = this.R[0] - 3;
                int posR = this.R[1];
                int srcPos3 = 0;
                int destDataPos = dataSize4;
                setValue(false, this.mem, 245792, dataSize4);
                if (dataSize4 < 122880 && posR >= 0) {
                    int curChannel2 = 0;
                    while (curChannel2 < 3) {
                        long prevByte = 0;
                        int i2 = curChannel2;
                        while (true) {
                            int srcPos4 = srcPos3;
                            if (i2 < dataSize4) {
                                int upperPos = i2 - width;
                                if (upperPos >= 3) {
                                    int upperDataPos = destDataPos + upperPos;
                                    int upperByte = this.mem[upperDataPos] & MotionEventCompat.ACTION_MASK;
                                    int upperLeftByte = this.mem[upperDataPos - 3] & MotionEventCompat.ACTION_MASK;
                                    long predicted2 = (((long) upperByte) + prevByte) - ((long) upperLeftByte);
                                    int pa = Math.abs((int) (predicted2 - prevByte));
                                    int pb = Math.abs((int) (predicted2 - ((long) upperByte)));
                                    int pc = Math.abs((int) (predicted2 - ((long) upperLeftByte)));
                                    if (pa <= pb && pa <= pc) {
                                        predicted = prevByte;
                                    } else if (pb <= pc) {
                                        predicted = (long) upperByte;
                                    } else {
                                        predicted = (long) upperLeftByte;
                                    }
                                } else {
                                    predicted = prevByte;
                                }
                                srcPos3 = srcPos4 + 1;
                                prevByte = (predicted - ((long) this.mem[srcPos4])) & 255 & 255;
                                this.mem[destDataPos + i2] = (byte) ((int) (255 & prevByte));
                                i2 += 3;
                            } else {
                                curChannel2++;
                                srcPos3 = srcPos4;
                            }
                        }
                    }
                    int border2 = dataSize4 - 2;
                    for (int i3 = posR; i3 < border2; i3 += 3) {
                        byte G = this.mem[destDataPos + i3 + 1];
                        byte[] bArr2 = this.mem;
                        int i4 = destDataPos + i3;
                        bArr2[i4] = (byte) (bArr2[i4] + G);
                        byte[] bArr3 = this.mem;
                        int i5 = destDataPos + i3 + 2;
                        bArr3[i5] = (byte) (bArr3[i5] + G);
                    }
                    return;
                }
                return;
            case VMSF_AUDIO:
                int dataSize5 = this.R[4];
                int channels2 = this.R[0];
                int srcPos5 = 0;
                int destDataPos2 = dataSize5;
                setValue(false, this.mem, 245792, dataSize5);
                if (dataSize5 < 122880) {
                    int curChannel3 = 0;
                    while (curChannel3 < channels2) {
                        long prevByte2 = 0;
                        long prevDelta = 0;
                        long[] Dif = new long[7];
                        int D1 = 0;
                        int D2 = 0;
                        int K1 = 0;
                        int K2 = 0;
                        int K3 = 0;
                        int i6 = curChannel3;
                        int byteCount = 0;
                        while (true) {
                            int srcPos6 = srcPos5;
                            if (i6 < dataSize5) {
                                int D3 = D2;
                                D2 = ((int) prevDelta) - D1;
                                D1 = (int) prevDelta;
                                srcPos5 = srcPos6 + 1;
                                long curByte2 = (long) (this.mem[srcPos6] & 255);
                                long predicted3 = (((((((8 * prevByte2) + ((long) (K1 * D1))) + ((long) (K2 * D2))) + ((long) (K3 * D3))) >>> 3) & 255) - curByte2) & UINT_MASK;
                                this.mem[destDataPos2 + i6] = (byte) ((int) predicted3);
                                prevDelta = (long) ((byte) ((int) (predicted3 - prevByte2)));
                                prevByte2 = predicted3;
                                int D = ((byte) ((int) curByte2)) << 3;
                                Dif[0] = Dif[0] + ((long) Math.abs(D));
                                Dif[1] = Dif[1] + ((long) Math.abs(D - D1));
                                Dif[2] = Dif[2] + ((long) Math.abs(D + D1));
                                Dif[3] = Dif[3] + ((long) Math.abs(D - D2));
                                Dif[4] = Dif[4] + ((long) Math.abs(D + D2));
                                Dif[5] = Dif[5] + ((long) Math.abs(D - D3));
                                Dif[6] = Dif[6] + ((long) Math.abs(D + D3));
                                if ((byteCount & 31) == 0) {
                                    long minDif = Dif[0];
                                    long numMinDif = 0;
                                    Dif[0] = 0;
                                    for (int j = 1; j < Dif.length; j++) {
                                        if (Dif[j] < minDif) {
                                            minDif = Dif[j];
                                            numMinDif = (long) j;
                                        }
                                        Dif[j] = 0;
                                    }
                                    switch ((int) numMinDif) {
                                        case 1:
                                            if (K1 < -16) {
                                                break;
                                            } else {
                                                K1--;
                                                break;
                                            }
                                        case 2:
                                            if (K1 >= 16) {
                                                break;
                                            } else {
                                                K1++;
                                                break;
                                            }
                                        case 3:
                                            if (K2 < -16) {
                                                break;
                                            } else {
                                                K2--;
                                                break;
                                            }
                                        case 4:
                                            if (K2 >= 16) {
                                                break;
                                            } else {
                                                K2++;
                                                break;
                                            }
                                        case 5:
                                            if (K3 < -16) {
                                                break;
                                            } else {
                                                K3--;
                                                break;
                                            }
                                        case 6:
                                            if (K3 >= 16) {
                                                break;
                                            } else {
                                                K3++;
                                                break;
                                            }
                                    }
                                }
                                i6 += channels2;
                                byteCount++;
                            } else {
                                curChannel3++;
                                srcPos5 = srcPos6;
                            }
                        }
                    }
                    return;
                }
                return;
            case VMSF_UPCASE:
                int dataSize6 = this.R[4];
                int srcPos7 = 0;
                int destPos2 = dataSize6;
                if (dataSize6 < 122880) {
                    while (true) {
                        int destPos3 = destPos2;
                        int srcPos8 = srcPos7;
                        if (srcPos8 < dataSize6) {
                            srcPos7 = srcPos8 + 1;
                            byte curByte3 = this.mem[srcPos8];
                            if (curByte3 == 2) {
                                int srcPos9 = srcPos7 + 1;
                                curByte3 = this.mem[srcPos7];
                                if (curByte3 != 2) {
                                    curByte3 = (byte) (curByte3 - 32);
                                    srcPos7 = srcPos9;
                                } else {
                                    srcPos7 = srcPos9;
                                }
                            }
                            destPos2 = destPos3 + 1;
                            this.mem[destPos3] = curByte3;
                        } else {
                            setValue(false, this.mem, 245788, destPos3 - dataSize6);
                            setValue(false, this.mem, 245792, dataSize6);
                            return;
                        }
                    }
                } else {
                    return;
                }
            default:
                return;
        }
    }

    private void filterItanium_SetBits(int curPos, int bitField, int bitPos, int bitCount) {
        int inAddr = bitPos / 8;
        int inBit = bitPos & 7;
        int andMask = ((-1 >>> (32 - bitCount)) << inBit) ^ -1;
        int bitField2 = bitField << inBit;
        for (int i = 0; i < 4; i++) {
            byte[] bArr = this.mem;
            int i2 = curPos + inAddr + i;
            bArr[i2] = (byte) (bArr[i2] & andMask);
            byte[] bArr2 = this.mem;
            int i3 = curPos + inAddr + i;
            bArr2[i3] = (byte) (bArr2[i3] | bitField2);
            andMask = (andMask >>> 8) | -16777216;
            bitField2 >>>= 8;
        }
    }

    private int filterItanium_GetBits(int curPos, int bitPos, int bitCount) {
        int inAddr = bitPos / 8;
        int inAddr2 = inAddr + 1;
        int inAddr3 = inAddr2 + 1;
        return (-1 >>> (32 - bitCount)) & (((((this.mem[curPos + inAddr] & MotionEventCompat.ACTION_MASK) | ((this.mem[curPos + inAddr2] & 255) << 8)) | ((this.mem[curPos + inAddr3] & 255) << VMCmdFlags.VMCF_PROC)) | ((this.mem[curPos + (inAddr3 + 1)] & 255) << 24)) >>> (bitPos & 7));
    }

    public void setMemory(int pos, byte[] data, int offset, int dataSize) {
        if (pos < 262144) {
            int i = 0;
            while (i < Math.min(data.length - offset, dataSize) && VM_MEMSIZE - pos >= i) {
                this.mem[pos + i] = data[offset + i];
                i++;
            }
        }
    }
}
