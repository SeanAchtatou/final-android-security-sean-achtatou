package de.innosystec.unrar.unpack.vm;

public class VMStandardFilterSignature {
    private int CRC;
    private int length;
    private VMStandardFilters type;

    public VMStandardFilterSignature(int length2, int crc, VMStandardFilters type2) {
        this.length = length2;
        this.CRC = crc;
        this.type = type2;
    }

    public int getCRC() {
        return this.CRC;
    }

    public void setCRC(int crc) {
        this.CRC = crc;
    }

    public int getLength() {
        return this.length;
    }

    public void setLength(int length2) {
        this.length = length2;
    }

    public VMStandardFilters getType() {
        return this.type;
    }

    public void setType(VMStandardFilters type2) {
        this.type = type2;
    }
}
