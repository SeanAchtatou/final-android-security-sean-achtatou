package de.innosystec.unrar.unpack.vm;

public enum VMStandardFilters {
    VMSF_NONE(0),
    VMSF_E8(1),
    VMSF_E8E9(2),
    VMSF_ITANIUM(3),
    VMSF_RGB(4),
    VMSF_AUDIO(5),
    VMSF_DELTA(6),
    VMSF_UPCASE(7);
    
    private int filter;

    private VMStandardFilters(int filter2) {
        this.filter = filter2;
    }

    public int getFilter() {
        return this.filter;
    }

    public boolean equals(int filter2) {
        return this.filter == filter2;
    }

    public static VMStandardFilters findFilter(int filter2) {
        if (VMSF_NONE.equals(filter2)) {
            return VMSF_NONE;
        }
        if (VMSF_E8.equals(filter2)) {
            return VMSF_E8;
        }
        if (VMSF_E8E9.equals(filter2)) {
            return VMSF_E8E9;
        }
        if (VMSF_ITANIUM.equals(filter2)) {
            return VMSF_ITANIUM;
        }
        if (VMSF_RGB.equals(filter2)) {
            return VMSF_RGB;
        }
        if (VMSF_AUDIO.equals(filter2)) {
            return VMSF_AUDIO;
        }
        if (VMSF_DELTA.equals(filter2)) {
            return VMSF_DELTA;
        }
        return null;
    }
}
