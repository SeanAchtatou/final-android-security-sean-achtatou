package de.innosystec.unrar.unpack.decode;

public class AudioVariables {
    int byteCount;
    int d1;
    int d2;
    int d3;
    int d4;
    int[] dif = new int[11];
    int k1;
    int k2;
    int k3;
    int k4;
    int k5;
    int lastChar;
    int lastDelta;

    public int getByteCount() {
        return this.byteCount;
    }

    public void setByteCount(int byteCount2) {
        this.byteCount = byteCount2;
    }

    public int getD1() {
        return this.d1;
    }

    public void setD1(int d12) {
        this.d1 = d12;
    }

    public int getD2() {
        return this.d2;
    }

    public void setD2(int d22) {
        this.d2 = d22;
    }

    public int getD3() {
        return this.d3;
    }

    public void setD3(int d32) {
        this.d3 = d32;
    }

    public int getD4() {
        return this.d4;
    }

    public void setD4(int d42) {
        this.d4 = d42;
    }

    public int[] getDif() {
        return this.dif;
    }

    public void setDif(int[] dif2) {
        this.dif = dif2;
    }

    public int getK1() {
        return this.k1;
    }

    public void setK1(int k12) {
        this.k1 = k12;
    }

    public int getK2() {
        return this.k2;
    }

    public void setK2(int k22) {
        this.k2 = k22;
    }

    public int getK3() {
        return this.k3;
    }

    public void setK3(int k32) {
        this.k3 = k32;
    }

    public int getK4() {
        return this.k4;
    }

    public void setK4(int k42) {
        this.k4 = k42;
    }

    public int getK5() {
        return this.k5;
    }

    public void setK5(int k52) {
        this.k5 = k52;
    }

    public int getLastChar() {
        return this.lastChar;
    }

    public void setLastChar(int lastChar2) {
        this.lastChar = lastChar2;
    }

    public int getLastDelta() {
        return this.lastDelta;
    }

    public void setLastDelta(int lastDelta2) {
        this.lastDelta = lastDelta2;
    }
}
