package de.innosystec.unrar.unpack;

import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import de.innosystec.unrar.exception.RarException;
import de.innosystec.unrar.unpack.decode.Compress;
import de.innosystec.unrar.unpack.ppm.BlockTypes;
import de.innosystec.unrar.unpack.ppm.ModelPPM;
import de.innosystec.unrar.unpack.ppm.SubAllocator;
import de.innosystec.unrar.unpack.vm.BitInput;
import de.innosystec.unrar.unpack.vm.RarVM;
import de.innosystec.unrar.unpack.vm.VMPreparedProgram;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

public final class Unpack extends Unpack20 {
    public static int[] DBitLengthCounts = {4, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 14, 0, 12};
    private boolean externalWindow;
    private boolean fileExtracted;
    private List<UnpackFilter> filters = new ArrayList();
    private int lastFilter;
    private int lowDistRepCount;
    private List<Integer> oldFilterLengths = new ArrayList();
    private final ModelPPM ppm = new ModelPPM();
    private boolean ppmError;
    private int ppmEscChar;
    private int prevLowDist;
    private List<UnpackFilter> prgStack = new ArrayList();
    private RarVM rarVM = new RarVM();
    private boolean tablesRead;
    private BlockTypes unpBlockType;
    private byte[] unpOldTable = new byte[Compress.HUFF_TABLE_SIZE];
    private long writtenFileSize;

    public Unpack(ComprDataIO DataIO) {
        this.unpIO = DataIO;
        this.window = null;
        this.externalWindow = false;
        this.suspended = false;
        this.unpAllBuf = false;
        this.unpSomeRead = false;
    }

    public void init(byte[] window) {
        if (window == null) {
            this.window = new byte[Compress.MAXWINSIZE];
        } else {
            this.window = window;
            this.externalWindow = true;
        }
        this.inAddr = 0;
        unpInitData(false);
    }

    public void doUnpack(int method, boolean solid) throws IOException, RarException {
        if (this.unpIO.getSubHeader().getUnpMethod() == 48) {
            unstoreFile();
        }
        switch (method) {
            case 15:
                unpack15(solid);
                return;
            case 20:
            case 26:
                unpack20(solid);
                return;
            case 29:
            case 36:
                unpack29(solid);
                return;
            default:
                return;
        }
    }

    private void unstoreFile() throws IOException, RarException {
        byte[] buffer = new byte[AccessibilityEventCompat.TYPE_VIEW_ACCESSIBILITY_FOCUS_CLEARED];
        while (true) {
            int code = this.unpIO.unpRead(buffer, 0, (int) Math.min((long) buffer.length, this.destUnpSize));
            if (code != 0 && code != -1) {
                if (((long) code) >= this.destUnpSize) {
                    code = (int) this.destUnpSize;
                }
                this.unpIO.unpWrite(buffer, 0, code);
                if (this.destUnpSize >= 0) {
                    this.destUnpSize -= (long) code;
                }
            } else {
                return;
            }
        }
    }

    private void unpack29(boolean solid) throws IOException, RarException {
        int[] DDecode = new int[60];
        byte[] DBits = new byte[60];
        if (DDecode[1] == 0) {
            int Dist = 0;
            int BitLength = 0;
            int Slot = 0;
            int I = 0;
            while (I < DBitLengthCounts.length) {
                int count = DBitLengthCounts[I];
                int J = 0;
                while (J < count) {
                    DDecode[Slot] = Dist;
                    DBits[Slot] = (byte) BitLength;
                    J++;
                    Slot++;
                    Dist += 1 << BitLength;
                }
                I++;
                BitLength++;
            }
        }
        this.fileExtracted = true;
        if (!this.suspended) {
            unpInitData(solid);
            if (unpReadBuf()) {
                if ((!solid || !this.tablesRead) && !readTables()) {
                    return;
                }
            } else {
                return;
            }
        }
        if (!this.ppmError) {
            while (true) {
                this.unpPtr = this.unpPtr & Compress.MAXWINMASK;
                if (this.inAddr > this.readBorder && !unpReadBuf()) {
                    break;
                }
                if (((this.wrPtr - this.unpPtr) & Compress.MAXWINMASK) < 260 && this.wrPtr != this.unpPtr) {
                    UnpWriteBuf();
                    if (this.writtenFileSize > this.destUnpSize) {
                        return;
                    }
                    if (this.suspended) {
                        this.fileExtracted = false;
                        return;
                    }
                }
                if (this.unpBlockType == BlockTypes.BLOCK_PPM) {
                    int Ch = this.ppm.decodeChar();
                    if (Ch == -1) {
                        this.ppmError = true;
                        break;
                    }
                    if (Ch == this.ppmEscChar) {
                        int NextCh = this.ppm.decodeChar();
                        if (NextCh != 0) {
                            if (NextCh != 2 && NextCh != -1) {
                                if (NextCh != 3) {
                                    if (NextCh != 4) {
                                        if (NextCh == 5) {
                                            int Length = this.ppm.decodeChar();
                                            if (Length == -1) {
                                                break;
                                            }
                                            copyString(Length + 4, 1);
                                        }
                                    } else {
                                        int Distance = 0;
                                        int Length2 = 0;
                                        boolean failed = false;
                                        for (int I2 = 0; I2 < 4 && !failed; I2++) {
                                            int ch = this.ppm.decodeChar();
                                            if (ch == -1) {
                                                failed = true;
                                            } else if (I2 == 3) {
                                                Length2 = ch & MotionEventCompat.ACTION_MASK;
                                            } else {
                                                Distance = (Distance << 8) + (ch & MotionEventCompat.ACTION_MASK);
                                            }
                                        }
                                        if (failed) {
                                            break;
                                        }
                                        copyString(Length2 + 32, Distance + 2);
                                    }
                                } else if (!readVMCodePPM()) {
                                    break;
                                }
                            } else {
                                break;
                            }
                        } else if (!readTables()) {
                            break;
                        }
                    }
                    byte[] bArr = this.window;
                    int i = this.unpPtr;
                    this.unpPtr = i + 1;
                    bArr[i] = (byte) Ch;
                } else {
                    int Number = decodeNumber(this.LD);
                    if (Number < 256) {
                        byte[] bArr2 = this.window;
                        int i2 = this.unpPtr;
                        this.unpPtr = i2 + 1;
                        bArr2[i2] = (byte) Number;
                    } else if (Number >= 271) {
                        int Number2 = Number - 271;
                        int Length3 = LDecode[Number2] + 3;
                        byte b = LBits[Number2];
                        if (b > 0) {
                            Length3 += getbits() >>> (16 - b);
                            addbits(b);
                        }
                        int DistNumber = decodeNumber(this.DD);
                        int Distance2 = DDecode[DistNumber] + 1;
                        byte b2 = DBits[DistNumber];
                        if (b2 > 0) {
                            if (DistNumber > 9) {
                                if (b2 > 4) {
                                    Distance2 += (getbits() >>> (20 - b2)) << 4;
                                    addbits(b2 - 4);
                                }
                                if (this.lowDistRepCount > 0) {
                                    this.lowDistRepCount = this.lowDistRepCount - 1;
                                    Distance2 += this.prevLowDist;
                                } else {
                                    int LowDist = decodeNumber(this.LDD);
                                    if (LowDist == 16) {
                                        this.lowDistRepCount = 15;
                                        Distance2 += this.prevLowDist;
                                    } else {
                                        Distance2 += LowDist;
                                        this.prevLowDist = LowDist;
                                    }
                                }
                            } else {
                                Distance2 += getbits() >>> (16 - b2);
                                addbits(b2);
                            }
                        }
                        if (Distance2 >= 8192) {
                            Length3++;
                            if (((long) Distance2) >= 262144) {
                                Length3++;
                            }
                        }
                        insertOldDist(Distance2);
                        insertLastMatch(Length3, Distance2);
                        copyString(Length3, Distance2);
                    } else if (Number == 256) {
                        if (!readEndOfBlock()) {
                            break;
                        }
                    } else if (Number == 257) {
                        if (!readVMCode()) {
                            break;
                        }
                    } else if (Number == 258) {
                        if (this.lastLength != 0) {
                            copyString(this.lastLength, this.lastDist);
                        }
                    } else if (Number < 263) {
                        int DistNum = Number - 259;
                        int Distance3 = this.oldDist[DistNum];
                        for (int I3 = DistNum; I3 > 0; I3--) {
                            this.oldDist[I3] = this.oldDist[I3 - 1];
                        }
                        this.oldDist[0] = Distance3;
                        int LengthNumber = decodeNumber(this.RD);
                        int Length4 = LDecode[LengthNumber] + 2;
                        byte b3 = LBits[LengthNumber];
                        if (b3 > 0) {
                            Length4 += getbits() >>> (16 - b3);
                            addbits(b3);
                        }
                        insertLastMatch(Length4, Distance3);
                        copyString(Length4, Distance3);
                    } else if (Number < 272) {
                        int Number3 = Number - 263;
                        int Distance4 = SDDecode[Number3] + 1;
                        int Bits = SDBits[Number3];
                        if (Bits > 0) {
                            Distance4 += getbits() >>> (16 - Bits);
                            addbits(Bits);
                        }
                        insertOldDist(Distance4);
                        insertLastMatch(2, Distance4);
                        copyString(2, Distance4);
                    }
                }
            }
            UnpWriteBuf();
        }
    }

    private void UnpWriteBuf() throws IOException {
        UnpackFilter NextFilter;
        int WrittenBorder = this.wrPtr;
        int WriteSize = (this.unpPtr - WrittenBorder) & Compress.MAXWINMASK;
        int I = 0;
        while (I < this.prgStack.size()) {
            UnpackFilter flt = this.prgStack.get(I);
            if (flt != null) {
                if (flt.isNextWindow()) {
                    flt.setNextWindow(false);
                } else {
                    int BlockStart = flt.getBlockStart();
                    int BlockLength = flt.getBlockLength();
                    if (((BlockStart - WrittenBorder) & Compress.MAXWINMASK) >= WriteSize) {
                        continue;
                    } else {
                        if (WrittenBorder != BlockStart) {
                            UnpWriteArea(WrittenBorder, BlockStart);
                            WrittenBorder = BlockStart;
                            WriteSize = (this.unpPtr - WrittenBorder) & Compress.MAXWINMASK;
                        }
                        if (BlockLength <= WriteSize) {
                            int BlockEnd = (BlockStart + BlockLength) & Compress.MAXWINMASK;
                            if (BlockStart < BlockEnd || BlockEnd == 0) {
                                this.rarVM.setMemory(0, this.window, BlockStart, BlockLength);
                            } else {
                                int FirstPartLength = Compress.MAXWINSIZE - BlockStart;
                                this.rarVM.setMemory(0, this.window, BlockStart, FirstPartLength);
                                this.rarVM.setMemory(FirstPartLength, this.window, 0, BlockEnd);
                            }
                            VMPreparedProgram ParentPrg = this.filters.get(flt.getParentFilter()).getPrg();
                            VMPreparedProgram Prg = flt.getPrg();
                            if (ParentPrg.getGlobalData().size() > 64) {
                                Prg.getGlobalData().setSize(ParentPrg.getGlobalData().size());
                                for (int i = 0; i < ParentPrg.getGlobalData().size() - 64; i++) {
                                    Prg.getGlobalData().set(i + 64, ParentPrg.getGlobalData().get(i + 64));
                                }
                            }
                            ExecuteCode(Prg);
                            if (Prg.getGlobalData().size() > 64) {
                                if (ParentPrg.getGlobalData().size() < Prg.getGlobalData().size()) {
                                    ParentPrg.getGlobalData().setSize(Prg.getGlobalData().size());
                                }
                                for (int i2 = 0; i2 < Prg.getGlobalData().size() - 64; i2++) {
                                    ParentPrg.getGlobalData().set(i2 + 64, Prg.getGlobalData().get(i2 + 64));
                                }
                            } else {
                                ParentPrg.getGlobalData().clear();
                            }
                            int FilteredDataOffset = Prg.getFilteredDataOffset();
                            int FilteredDataSize = Prg.getFilteredDataSize();
                            byte[] FilteredData = new byte[FilteredDataSize];
                            for (int i3 = 0; i3 < FilteredDataSize; i3++) {
                                FilteredData[i3] = this.rarVM.getMem()[FilteredDataOffset + i3];
                            }
                            this.prgStack.set(I, null);
                            while (I + 1 < this.prgStack.size() && (NextFilter = this.prgStack.get(I + 1)) != null && NextFilter.getBlockStart() == BlockStart && NextFilter.getBlockLength() == FilteredDataSize && !NextFilter.isNextWindow()) {
                                this.rarVM.setMemory(0, FilteredData, 0, FilteredDataSize);
                                VMPreparedProgram pPrg = this.filters.get(NextFilter.getParentFilter()).getPrg();
                                VMPreparedProgram NextPrg = NextFilter.getPrg();
                                if (pPrg.getGlobalData().size() > 64) {
                                    NextPrg.getGlobalData().setSize(pPrg.getGlobalData().size());
                                    for (int i4 = 0; i4 < pPrg.getGlobalData().size() - 64; i4++) {
                                        NextPrg.getGlobalData().set(i4 + 64, pPrg.getGlobalData().get(i4 + 64));
                                    }
                                }
                                ExecuteCode(NextPrg);
                                if (NextPrg.getGlobalData().size() > 64) {
                                    if (pPrg.getGlobalData().size() < NextPrg.getGlobalData().size()) {
                                        pPrg.getGlobalData().setSize(NextPrg.getGlobalData().size());
                                    }
                                    for (int i5 = 0; i5 < NextPrg.getGlobalData().size() - 64; i5++) {
                                        pPrg.getGlobalData().set(i5 + 64, NextPrg.getGlobalData().get(i5 + 64));
                                    }
                                } else {
                                    pPrg.getGlobalData().clear();
                                }
                                int FilteredDataOffset2 = NextPrg.getFilteredDataOffset();
                                FilteredDataSize = NextPrg.getFilteredDataSize();
                                FilteredData = new byte[FilteredDataSize];
                                for (int i6 = 0; i6 < FilteredDataSize; i6++) {
                                    FilteredData[i6] = NextPrg.getGlobalData().get(FilteredDataOffset2 + i6).byteValue();
                                }
                                I++;
                                this.prgStack.set(I, null);
                            }
                            this.unpIO.unpWrite(FilteredData, 0, FilteredDataSize);
                            this.unpSomeRead = true;
                            this.writtenFileSize = this.writtenFileSize + ((long) FilteredDataSize);
                            WrittenBorder = BlockEnd;
                            WriteSize = (this.unpPtr - WrittenBorder) & Compress.MAXWINMASK;
                        } else {
                            for (int J = I; J < this.prgStack.size(); J++) {
                                UnpackFilter filt = this.prgStack.get(J);
                                if (filt != null && filt.isNextWindow()) {
                                    filt.setNextWindow(false);
                                }
                            }
                            this.wrPtr = WrittenBorder;
                            return;
                        }
                    }
                }
            }
            I++;
        }
        UnpWriteArea(WrittenBorder, this.unpPtr);
        this.wrPtr = this.unpPtr;
    }

    private void UnpWriteArea(int startPtr, int endPtr) throws IOException {
        if (endPtr != startPtr) {
            this.unpSomeRead = true;
        }
        if (endPtr < startPtr) {
            UnpWriteData(this.window, startPtr, (-startPtr) & Compress.MAXWINMASK);
            UnpWriteData(this.window, 0, endPtr);
            this.unpAllBuf = true;
            return;
        }
        UnpWriteData(this.window, startPtr, endPtr - startPtr);
    }

    private void UnpWriteData(byte[] data, int offset, int size) throws IOException {
        if (this.writtenFileSize < this.destUnpSize) {
            int writeSize = size;
            long leftToWrite = this.destUnpSize - this.writtenFileSize;
            if (((long) writeSize) > leftToWrite) {
                writeSize = (int) leftToWrite;
            }
            this.unpIO.unpWrite(data, offset, writeSize);
            this.writtenFileSize += (long) size;
        }
    }

    private void insertOldDist(int distance) {
        this.oldDist[3] = this.oldDist[2];
        this.oldDist[2] = this.oldDist[1];
        this.oldDist[1] = this.oldDist[0];
        this.oldDist[0] = distance;
    }

    private void insertLastMatch(int length, int distance) {
        this.lastDist = distance;
        this.lastLength = length;
    }

    private void copyString(int length, int distance) {
        int destPtr;
        int destPtr2 = this.unpPtr - distance;
        if (destPtr2 >= 0 && destPtr2 < 4194044 && this.unpPtr < 4194044) {
            byte[] bArr = this.window;
            int i = this.unpPtr;
            this.unpPtr = i + 1;
            destPtr = destPtr2 + 1;
            bArr[i] = this.window[destPtr2];
            while (true) {
                length--;
                if (length <= 0) {
                    break;
                }
                byte[] bArr2 = this.window;
                int i2 = this.unpPtr;
                this.unpPtr = i2 + 1;
                bArr2[i2] = this.window[destPtr];
                destPtr++;
            }
        } else {
            while (true) {
                destPtr = destPtr2;
                int length2 = length;
                length = length2 - 1;
                if (length2 == 0) {
                    break;
                }
                destPtr2 = destPtr + 1;
                this.window[this.unpPtr] = this.window[destPtr & Compress.MAXWINMASK];
                this.unpPtr = (this.unpPtr + 1) & Compress.MAXWINMASK;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void unpInitData(boolean solid) {
        if (!solid) {
            this.tablesRead = false;
            Arrays.fill(this.oldDist, 0);
            this.oldDistPtr = 0;
            this.lastDist = 0;
            this.lastLength = 0;
            Arrays.fill(this.unpOldTable, (byte) 0);
            this.unpPtr = 0;
            this.wrPtr = 0;
            this.ppmEscChar = 2;
            initFilters();
        }
        InitBitInput();
        this.ppmError = false;
        this.writtenFileSize = 0;
        this.readTop = 0;
        this.readBorder = 0;
        unpInitData20(solid);
    }

    private void initFilters() {
        this.oldFilterLengths.clear();
        this.lastFilter = 0;
        this.filters.clear();
        this.prgStack.clear();
    }

    private boolean readEndOfBlock() throws IOException, RarException {
        boolean NewTable;
        boolean z;
        int BitField = getbits();
        boolean NewFile = false;
        if ((32768 & BitField) != 0) {
            NewTable = true;
            addbits(1);
        } else {
            NewFile = true;
            if ((BitField & 16384) != 0) {
                NewTable = true;
            } else {
                NewTable = false;
            }
            addbits(2);
        }
        if (!NewTable) {
            z = true;
        } else {
            z = false;
        }
        this.tablesRead = z;
        if (NewFile || (NewTable && !readTables())) {
            return false;
        }
        return true;
    }

    private boolean readTables() throws IOException, RarException {
        int N;
        int i;
        int N2;
        int i2;
        byte[] bitLength = new byte[20];
        byte[] table = new byte[Compress.HUFF_TABLE_SIZE];
        if (this.inAddr > this.readTop - 25 && !unpReadBuf()) {
            return false;
        }
        faddbits((8 - this.inBit) & 7);
        long bitField = (long) (fgetbits() & -1);
        if ((32768 & bitField) != 0) {
            this.unpBlockType = BlockTypes.BLOCK_PPM;
            return this.ppm.decodeInit(this, this.ppmEscChar);
        }
        this.unpBlockType = BlockTypes.BLOCK_LZ;
        this.prevLowDist = 0;
        this.lowDistRepCount = 0;
        if ((16384 & bitField) == 0) {
            Arrays.fill(this.unpOldTable, (byte) 0);
        }
        faddbits(2);
        int i3 = 0;
        while (i3 < 20) {
            int length = (fgetbits() >>> 12) & MotionEventCompat.ACTION_MASK;
            faddbits(4);
            if (length == 15) {
                int zeroCount = (fgetbits() >>> 12) & MotionEventCompat.ACTION_MASK;
                faddbits(4);
                if (zeroCount == 0) {
                    bitLength[i3] = 15;
                } else {
                    int zeroCount2 = zeroCount + 2;
                    while (true) {
                        int zeroCount3 = zeroCount2;
                        i2 = i3;
                        zeroCount2 = zeroCount3 - 1;
                        if (zeroCount3 <= 0 || i2 >= bitLength.length) {
                            i3 = i2 - 1;
                        } else {
                            i3 = i2 + 1;
                            bitLength[i2] = 0;
                        }
                    }
                    i3 = i2 - 1;
                }
            } else {
                bitLength[i3] = (byte) length;
            }
            i3++;
        }
        makeDecodeTables(bitLength, 0, this.BD, 20);
        int i4 = 0;
        while (i4 < 404) {
            if (this.inAddr > this.readTop - 5 && !unpReadBuf()) {
                return false;
            }
            int Number = decodeNumber(this.BD);
            if (Number < 16) {
                table[i4] = (byte) ((this.unpOldTable[i4] + Number) & 15);
                i4++;
            } else if (Number < 18) {
                if (Number == 16) {
                    N2 = (fgetbits() >>> 13) + 3;
                    faddbits(3);
                } else {
                    N2 = (fgetbits() >>> 9) + 11;
                    faddbits(7);
                }
                while (true) {
                    int N3 = N2;
                    N2 = N3 - 1;
                    if (N3 <= 0 || i4 >= 404) {
                        break;
                    }
                    table[i4] = table[i4 - 1];
                    i4++;
                }
            } else {
                if (Number == 18) {
                    N = (fgetbits() >>> 13) + 3;
                    faddbits(3);
                } else {
                    N = (fgetbits() >>> 9) + 11;
                    faddbits(7);
                }
                while (true) {
                    int N4 = N;
                    i = i4;
                    N = N4 - 1;
                    if (N4 <= 0 || i >= 404) {
                        i4 = i;
                    } else {
                        i4 = i + 1;
                        table[i] = 0;
                    }
                }
                i4 = i;
            }
        }
        this.tablesRead = true;
        if (this.inAddr > this.readTop) {
            return false;
        }
        makeDecodeTables(table, 0, this.LD, Compress.NC);
        makeDecodeTables(table, Compress.NC, this.DD, 60);
        makeDecodeTables(table, 359, this.LDD, 17);
        makeDecodeTables(table, 376, this.RD, 28);
        for (int i5 = 0; i5 < this.unpOldTable.length; i5++) {
            this.unpOldTable[i5] = table[i5];
        }
        return true;
    }

    private boolean readVMCode() throws IOException, RarException {
        int FirstByte = getbits() >> 8;
        addbits(8);
        int Length = (FirstByte & 7) + 1;
        if (Length == 7) {
            Length = (getbits() >> 8) + 7;
            addbits(8);
        } else if (Length == 8) {
            Length = getbits();
            addbits(16);
        }
        List<Byte> vmCode = new ArrayList<>();
        for (int I = 0; I < Length; I++) {
            if (this.inAddr >= this.readTop - 1 && !unpReadBuf() && I < Length - 1) {
                return false;
            }
            vmCode.add(Byte.valueOf((byte) (getbits() >> 8)));
            addbits(8);
        }
        return addVMCode(FirstByte, vmCode, Length);
    }

    private boolean readVMCodePPM() throws IOException, RarException {
        int B2;
        int FirstByte = this.ppm.decodeChar();
        if (FirstByte == -1) {
            return false;
        }
        int Length = (FirstByte & 7) + 1;
        if (Length == 7) {
            int B1 = this.ppm.decodeChar();
            if (B1 == -1) {
                return false;
            }
            Length = B1 + 7;
        } else if (Length == 8) {
            int B12 = this.ppm.decodeChar();
            if (B12 == -1 || (B2 = this.ppm.decodeChar()) == -1) {
                return false;
            }
            Length = (B12 * 256) + B2;
        }
        List<Byte> vmCode = new ArrayList<>();
        for (int I = 0; I < Length; I++) {
            int Ch = this.ppm.decodeChar();
            if (Ch == -1) {
                return false;
            }
            vmCode.add(Byte.valueOf((byte) Ch));
        }
        return addVMCode(FirstByte, vmCode, Length);
    }

    private boolean addVMCode(int firstByte, List<Byte> vmCode, int length) {
        int FiltPos;
        UnpackFilter Filter;
        int DataSize;
        BitInput Inp = new BitInput();
        Inp.InitBitInput();
        for (int i = 0; i < Math.min(32768, vmCode.size()); i++) {
            Inp.getInBuf()[i] = vmCode.get(i).byteValue();
        }
        this.rarVM.init();
        if ((firstByte & 128) != 0) {
            FiltPos = RarVM.ReadData(Inp);
            if (FiltPos == 0) {
                initFilters();
            } else {
                FiltPos--;
            }
        } else {
            FiltPos = this.lastFilter;
        }
        if (FiltPos > this.filters.size() || FiltPos > this.oldFilterLengths.size()) {
            return false;
        }
        this.lastFilter = FiltPos;
        boolean NewFilter = FiltPos == this.filters.size();
        UnpackFilter StackFilter = new UnpackFilter();
        if (!NewFilter) {
            Filter = this.filters.get(FiltPos);
            StackFilter.setParentFilter(FiltPos);
            Filter.setExecCount(Filter.getExecCount() + 1);
        } else if (FiltPos > 1024) {
            return false;
        } else {
            Filter = new UnpackFilter();
            this.filters.add(Filter);
            StackFilter.setParentFilter(this.filters.size() - 1);
            this.oldFilterLengths.add(0);
            Filter.setExecCount(0);
        }
        this.prgStack.add(StackFilter);
        StackFilter.setExecCount(Filter.getExecCount());
        int BlockStart = RarVM.ReadData(Inp);
        if ((firstByte & 64) != 0) {
            BlockStart += 258;
        }
        StackFilter.setBlockStart((this.unpPtr + BlockStart) & Compress.MAXWINMASK);
        if ((firstByte & 32) != 0) {
            StackFilter.setBlockLength(RarVM.ReadData(Inp));
        } else {
            StackFilter.setBlockLength(FiltPos < this.oldFilterLengths.size() ? this.oldFilterLengths.get(FiltPos).intValue() : 0);
        }
        StackFilter.setNextWindow(this.wrPtr != this.unpPtr && ((this.wrPtr - this.unpPtr) & Compress.MAXWINMASK) <= BlockStart);
        this.oldFilterLengths.set(FiltPos, Integer.valueOf(StackFilter.getBlockLength()));
        Arrays.fill(StackFilter.getPrg().getInitR(), 0);
        StackFilter.getPrg().getInitR()[3] = 245760;
        StackFilter.getPrg().getInitR()[4] = StackFilter.getBlockLength();
        StackFilter.getPrg().getInitR()[5] = StackFilter.getExecCount();
        if ((firstByte & 16) != 0) {
            int InitMask = Inp.fgetbits() >>> 9;
            Inp.faddbits(7);
            for (int I = 0; I < 7; I++) {
                if (((1 << I) & InitMask) != 0) {
                    StackFilter.getPrg().getInitR()[I] = RarVM.ReadData(Inp);
                }
            }
        }
        if (NewFilter) {
            int VMCodeSize = RarVM.ReadData(Inp);
            if (VMCodeSize >= 65536 || VMCodeSize == 0) {
                return false;
            }
            byte[] VMCode = new byte[VMCodeSize];
            for (int I2 = 0; I2 < VMCodeSize; I2++) {
                if (Inp.Overflow(3)) {
                    return false;
                }
                VMCode[I2] = (byte) (Inp.fgetbits() >> 8);
                Inp.faddbits(8);
            }
            this.rarVM.prepare(VMCode, VMCodeSize, Filter.getPrg());
        }
        StackFilter.getPrg().setAltCmd(Filter.getPrg().getCmd());
        StackFilter.getPrg().setCmdCount(Filter.getPrg().getCmdCount());
        int StaticDataSize = Filter.getPrg().getStaticData().size();
        if (StaticDataSize > 0 && StaticDataSize < 8192) {
            StackFilter.getPrg().setStaticData(Filter.getPrg().getStaticData());
        }
        if (StackFilter.getPrg().getGlobalData().size() < 64) {
            StackFilter.getPrg().getGlobalData().clear();
            StackFilter.getPrg().getGlobalData().setSize(64);
        }
        Vector<Byte> globalData = StackFilter.getPrg().getGlobalData();
        for (int I3 = 0; I3 < 7; I3++) {
            this.rarVM.setLowEndianValue(globalData, I3 * 4, StackFilter.getPrg().getInitR()[I3]);
        }
        this.rarVM.setLowEndianValue(globalData, 28, StackFilter.getBlockLength());
        this.rarVM.setLowEndianValue(globalData, 32, 0);
        this.rarVM.setLowEndianValue(globalData, 36, 0);
        this.rarVM.setLowEndianValue(globalData, 40, 0);
        this.rarVM.setLowEndianValue(globalData, 44, StackFilter.getExecCount());
        for (int i2 = 0; i2 < 16; i2++) {
            globalData.set(i2 + 48, null);
        }
        if ((firstByte & 8) != 0) {
            if (Inp.Overflow(3) || (DataSize = RarVM.ReadData(Inp)) > 8128) {
                return false;
            }
            int CurSize = StackFilter.getPrg().getGlobalData().size();
            if (CurSize < DataSize + 64) {
                StackFilter.getPrg().getGlobalData().setSize((DataSize + 64) - CurSize);
            }
            Vector<Byte> globalData2 = StackFilter.getPrg().getGlobalData();
            for (int I4 = 0; I4 < DataSize; I4++) {
                if (Inp.Overflow(3)) {
                    return false;
                }
                globalData2.set(64 + I4, Byte.valueOf((byte) (Inp.fgetbits() >>> 8)));
                Inp.faddbits(8);
            }
        }
        return true;
    }

    private void ExecuteCode(VMPreparedProgram Prg) {
        if (Prg.getGlobalData().size() > 0) {
            Prg.getInitR()[6] = (int) this.writtenFileSize;
            this.rarVM.setLowEndianValue(Prg.getGlobalData(), 36, (int) this.writtenFileSize);
            this.rarVM.setLowEndianValue(Prg.getGlobalData(), 40, (int) (this.writtenFileSize >>> 32));
            this.rarVM.execute(Prg);
        }
    }

    public boolean isFileExtracted() {
        return this.fileExtracted;
    }

    public void setDestSize(long destSize) {
        this.destUnpSize = destSize;
        this.fileExtracted = false;
    }

    public void setSuspended(boolean suspended) {
        this.suspended = suspended;
    }

    public int getChar() throws IOException, RarException {
        if (this.inAddr > 32738) {
            unpReadBuf();
        }
        byte[] bArr = this.inBuf;
        int i = this.inAddr;
        this.inAddr = i + 1;
        return bArr[i] & 255;
    }

    public int getPpmEscChar() {
        return this.ppmEscChar;
    }

    public void setPpmEscChar(int ppmEscChar2) {
        this.ppmEscChar = ppmEscChar2;
    }

    public void cleanUp() {
        SubAllocator allocator;
        if (this.ppm != null && (allocator = this.ppm.getSubAlloc()) != null) {
            allocator.stopSubAllocator();
        }
    }
}
