package de.innosystec.unrar.unpack;

import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import de.innosystec.unrar.exception.RarException;
import de.innosystec.unrar.unpack.decode.AudioVariables;
import de.innosystec.unrar.unpack.decode.BitDecode;
import de.innosystec.unrar.unpack.decode.Compress;
import de.innosystec.unrar.unpack.decode.Decode;
import de.innosystec.unrar.unpack.decode.DistDecode;
import de.innosystec.unrar.unpack.decode.LitDecode;
import de.innosystec.unrar.unpack.decode.LowDistDecode;
import de.innosystec.unrar.unpack.decode.MultDecode;
import de.innosystec.unrar.unpack.decode.RepDecode;
import de.innosystec.unrar.unpack.vm.RarVM;
import java.io.IOException;
import java.util.Arrays;

public abstract class Unpack20 extends Unpack15 {
    public static final int[] DBits = {0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 14, 14, 15, 15, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16};
    public static final int[] DDecode = {0, 1, 2, 3, 4, 6, 8, 12, 16, 24, 32, 48, 64, 96, 128, 192, 256, 384, 512, 768, 1024, 1536, 2048, 3072, 4096, 6144, 8192, 12288, 16384, 24576, 32768, 49152, AccessibilityEventCompat.TYPE_VIEW_ACCESSIBILITY_FOCUS_CLEARED, 98304, AccessibilityEventCompat.TYPE_VIEW_TEXT_TRAVERSED_AT_MOVEMENT_GRANULARITY, 196608, RarVM.VM_MEMSIZE, 327680, 393216, 458752, 524288, 589824, 655360, 720896, 786432, 851968, 917504, 983040};
    public static final byte[] LBits = {0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5};
    public static final int[] LDecode = {0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 12, 14, 16, 20, 24, 28, 32, 40, 48, 56, 64, 80, 96, 112, 128, 160, 192, 224};
    public static final int[] SDBits = {2, 2, 3, 4, 5, 6, 6, 6};
    public static final int[] SDDecode = {0, 4, 8, 16, 32, 64, 128, 192};
    protected AudioVariables[] AudV = new AudioVariables[4];
    protected BitDecode BD = new BitDecode();
    protected DistDecode DD = new DistDecode();
    protected LitDecode LD = new LitDecode();
    protected LowDistDecode LDD = new LowDistDecode();
    protected MultDecode[] MD = new MultDecode[4];
    protected RepDecode RD = new RepDecode();
    protected int UnpAudioBlock;
    protected int UnpChannelDelta;
    protected int UnpChannels;
    protected int UnpCurChannel;
    protected byte[] UnpOldTable20 = new byte[1028];

    /* access modifiers changed from: protected */
    public void unpack20(boolean solid) throws IOException, RarException {
        if (this.suspended) {
            this.unpPtr = this.wrPtr;
        } else {
            unpInitData(solid);
            if (!unpReadBuf()) {
                return;
            }
            if (solid || ReadTables20()) {
                this.destUnpSize--;
            } else {
                return;
            }
        }
        while (this.destUnpSize >= 0) {
            this.unpPtr &= Compress.MAXWINMASK;
            if (this.inAddr > this.readTop - 30 && !unpReadBuf()) {
                break;
            }
            if (((this.wrPtr - this.unpPtr) & Compress.MAXWINMASK) < 270 && this.wrPtr != this.unpPtr) {
                oldUnpWriteBuf();
                if (this.suspended) {
                    return;
                }
            }
            if (this.UnpAudioBlock != 0) {
                int AudioNumber = decodeNumber(this.MD[this.UnpCurChannel]);
                if (AudioNumber != 256) {
                    byte[] bArr = this.window;
                    int i = this.unpPtr;
                    this.unpPtr = i + 1;
                    bArr[i] = DecodeAudio(AudioNumber);
                    int i2 = this.UnpCurChannel + 1;
                    this.UnpCurChannel = i2;
                    if (i2 == this.UnpChannels) {
                        this.UnpCurChannel = 0;
                    }
                    this.destUnpSize--;
                } else if (!ReadTables20()) {
                    break;
                }
            } else {
                int Number = decodeNumber(this.LD);
                if (Number < 256) {
                    byte[] bArr2 = this.window;
                    int i3 = this.unpPtr;
                    this.unpPtr = i3 + 1;
                    bArr2[i3] = (byte) Number;
                    this.destUnpSize--;
                } else if (Number > 269) {
                    int Number2 = Number - 270;
                    int Length = LDecode[Number2] + 3;
                    byte b = LBits[Number2];
                    if (b > 0) {
                        Length += getbits() >>> (16 - b);
                        addbits(b);
                    }
                    int DistNumber = decodeNumber(this.DD);
                    int Distance = DDecode[DistNumber] + 1;
                    int Bits = DBits[DistNumber];
                    if (Bits > 0) {
                        Distance += getbits() >>> (16 - Bits);
                        addbits(Bits);
                    }
                    if (Distance >= 8192) {
                        Length++;
                        if (((long) Distance) >= 262144) {
                            Length++;
                        }
                    }
                    CopyString20(Length, Distance);
                } else if (Number == 269) {
                    if (!ReadTables20()) {
                        break;
                    }
                } else if (Number == 256) {
                    CopyString20(this.lastLength, this.lastDist);
                } else if (Number < 261) {
                    int Distance2 = this.oldDist[(this.oldDistPtr - (Number - 256)) & 3];
                    int LengthNumber = decodeNumber(this.RD);
                    int Length2 = LDecode[LengthNumber] + 2;
                    byte b2 = LBits[LengthNumber];
                    if (b2 > 0) {
                        Length2 += getbits() >>> (16 - b2);
                        addbits(b2);
                    }
                    if (Distance2 >= 257) {
                        Length2++;
                        if (Distance2 >= 8192) {
                            Length2++;
                            if (Distance2 >= 262144) {
                                Length2++;
                            }
                        }
                    }
                    CopyString20(Length2, Distance2);
                } else if (Number < 270) {
                    int Number3 = Number - 261;
                    int Distance3 = SDDecode[Number3] + 1;
                    int Bits2 = SDBits[Number3];
                    if (Bits2 > 0) {
                        Distance3 += getbits() >>> (16 - Bits2);
                        addbits(Bits2);
                    }
                    CopyString20(2, Distance3);
                }
            }
        }
        ReadLastTables();
        oldUnpWriteBuf();
    }

    /* access modifiers changed from: protected */
    public void CopyString20(int Length, int Distance) {
        int DestPtr;
        int[] iArr = this.oldDist;
        int i = this.oldDistPtr;
        this.oldDistPtr = i + 1;
        iArr[i & 3] = Distance;
        this.lastDist = Distance;
        this.lastLength = Length;
        this.destUnpSize -= (long) Length;
        int DestPtr2 = this.unpPtr - Distance;
        if (DestPtr2 < 4194004 && this.unpPtr < 4194004) {
            byte[] bArr = this.window;
            int i2 = this.unpPtr;
            this.unpPtr = i2 + 1;
            int DestPtr3 = DestPtr2 + 1;
            bArr[i2] = this.window[DestPtr2];
            byte[] bArr2 = this.window;
            int i3 = this.unpPtr;
            this.unpPtr = i3 + 1;
            int DestPtr4 = DestPtr3 + 1;
            bArr2[i3] = this.window[DestPtr3];
            while (true) {
                DestPtr = DestPtr4;
                if (Length <= 2) {
                    break;
                }
                Length--;
                byte[] bArr3 = this.window;
                int i4 = this.unpPtr;
                this.unpPtr = i4 + 1;
                DestPtr4 = DestPtr + 1;
                bArr3[i4] = this.window[DestPtr];
            }
        } else {
            while (true) {
                DestPtr = DestPtr2;
                int Length2 = Length;
                Length = Length2 - 1;
                if (Length2 == 0) {
                    break;
                }
                DestPtr2 = DestPtr + 1;
                this.window[this.unpPtr] = this.window[DestPtr & Compress.MAXWINMASK];
                this.unpPtr = (this.unpPtr + 1) & Compress.MAXWINMASK;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void makeDecodeTables(byte[] lenTab, int offset, Decode dec, int size) {
        int[] lenCount = new int[16];
        int[] tmpPos = new int[16];
        Arrays.fill(lenCount, 0);
        Arrays.fill(dec.getDecodeNum(), 0);
        for (int i = 0; i < size; i++) {
            byte b = lenTab[offset + i] & 15;
            lenCount[b] = lenCount[b] + 1;
        }
        lenCount[0] = 0;
        tmpPos[0] = 0;
        dec.getDecodePos()[0] = 0;
        dec.getDecodeLen()[0] = 0;
        long N = 0;
        for (int i2 = 1; i2 < 16; i2++) {
            N = 2 * (((long) lenCount[i2]) + N);
            long M = N << (15 - i2);
            if (M > 65535) {
                M = 65535;
            }
            dec.getDecodeLen()[i2] = (int) M;
            int[] decodePos = dec.getDecodePos();
            int i3 = dec.getDecodePos()[i2 - 1] + lenCount[i2 - 1];
            decodePos[i2] = i3;
            tmpPos[i2] = i3;
        }
        for (int i4 = 0; i4 < size; i4++) {
            if (lenTab[offset + i4] != 0) {
                int[] decodeNum = dec.getDecodeNum();
                byte b2 = lenTab[offset + i4] & 15;
                int i5 = tmpPos[b2];
                tmpPos[b2] = i5 + 1;
                decodeNum[i5] = i4;
            }
        }
        dec.setMaxNum(size);
    }

    /* access modifiers changed from: protected */
    public int decodeNumber(Decode dec) {
        int bits;
        long bitField = (long) (getbits() & 65534);
        int[] decodeLen = dec.getDecodeLen();
        if (bitField < ((long) decodeLen[8])) {
            if (bitField < ((long) decodeLen[4])) {
                if (bitField < ((long) decodeLen[2])) {
                    if (bitField < ((long) decodeLen[1])) {
                        bits = 1;
                    } else {
                        bits = 2;
                    }
                } else if (bitField < ((long) decodeLen[3])) {
                    bits = 3;
                } else {
                    bits = 4;
                }
            } else if (bitField < ((long) decodeLen[6])) {
                if (bitField < ((long) decodeLen[5])) {
                    bits = 5;
                } else {
                    bits = 6;
                }
            } else if (bitField < ((long) decodeLen[7])) {
                bits = 7;
            } else {
                bits = 8;
            }
        } else if (bitField < ((long) decodeLen[12])) {
            if (bitField < ((long) decodeLen[10])) {
                if (bitField < ((long) decodeLen[9])) {
                    bits = 9;
                } else {
                    bits = 10;
                }
            } else if (bitField < ((long) decodeLen[11])) {
                bits = 11;
            } else {
                bits = 12;
            }
        } else if (bitField >= ((long) decodeLen[14])) {
            bits = 15;
        } else if (bitField < ((long) decodeLen[13])) {
            bits = 13;
        } else {
            bits = 14;
        }
        addbits(bits);
        int N = dec.getDecodePos()[bits] + ((((int) bitField) - decodeLen[bits - 1]) >>> (16 - bits));
        if (N >= dec.getMaxNum()) {
            N = 0;
        }
        return dec.getDecodeNum()[N];
    }

    /* access modifiers changed from: protected */
    public boolean ReadTables20() throws IOException, RarException {
        int TableSize;
        int N;
        int I;
        byte[] BitLength = new byte[19];
        byte[] Table = new byte[1028];
        if (this.inAddr > this.readTop - 25 && !unpReadBuf()) {
            return false;
        }
        int BitField = getbits();
        this.UnpAudioBlock = 32768 & BitField;
        if ((BitField & 16384) == 0) {
            Arrays.fill(this.UnpOldTable20, (byte) 0);
        }
        addbits(2);
        if (this.UnpAudioBlock != 0) {
            this.UnpChannels = ((BitField >>> 12) & 3) + 1;
            if (this.UnpCurChannel >= this.UnpChannels) {
                this.UnpCurChannel = 0;
            }
            addbits(2);
            TableSize = this.UnpChannels * Compress.MC20;
        } else {
            TableSize = 374;
        }
        for (int I2 = 0; I2 < 19; I2++) {
            BitLength[I2] = (byte) (getbits() >>> 12);
            addbits(4);
        }
        makeDecodeTables(BitLength, 0, this.BD, 19);
        int I3 = 0;
        while (I3 < TableSize) {
            if (this.inAddr > this.readTop - 5 && !unpReadBuf()) {
                return false;
            }
            int Number = decodeNumber(this.BD);
            if (Number < 16) {
                Table[I3] = (byte) ((this.UnpOldTable20[I3] + Number) & 15);
                I3++;
            } else if (Number == 16) {
                int N2 = (getbits() >>> 14) + 3;
                addbits(2);
                while (true) {
                    int N3 = N2;
                    N2 = N3 - 1;
                    if (N3 <= 0 || I3 >= TableSize) {
                        break;
                    }
                    Table[I3] = Table[I3 - 1];
                    I3++;
                }
            } else {
                if (Number == 17) {
                    N = (getbits() >>> 13) + 3;
                    addbits(3);
                } else {
                    N = (getbits() >>> 9) + 11;
                    addbits(7);
                }
                while (true) {
                    I = I3;
                    int N4 = N;
                    N = N4 - 1;
                    if (N4 <= 0 || I >= TableSize) {
                        I3 = I;
                    } else {
                        I3 = I + 1;
                        Table[I] = 0;
                    }
                }
                I3 = I;
            }
        }
        if (this.inAddr > this.readTop) {
            return true;
        }
        if (this.UnpAudioBlock != 0) {
            for (int I4 = 0; I4 < this.UnpChannels; I4++) {
                makeDecodeTables(Table, I4 * Compress.MC20, this.MD[I4], Compress.MC20);
            }
        } else {
            makeDecodeTables(Table, 0, this.LD, Compress.NC20);
            makeDecodeTables(Table, Compress.NC20, this.DD, 48);
            makeDecodeTables(Table, 346, this.RD, 28);
        }
        for (int i = 0; i < this.UnpOldTable20.length; i++) {
            this.UnpOldTable20[i] = Table[i];
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void unpInitData20(boolean Solid) {
        if (!Solid) {
            this.UnpCurChannel = 0;
            this.UnpChannelDelta = 0;
            this.UnpChannels = 1;
            Arrays.fill(this.AudV, new AudioVariables());
            Arrays.fill(this.UnpOldTable20, (byte) 0);
        }
    }

    /* access modifiers changed from: protected */
    public void ReadLastTables() throws IOException, RarException {
        if (this.readTop < this.inAddr + 5) {
            return;
        }
        if (this.UnpAudioBlock != 0) {
            if (decodeNumber(this.MD[this.UnpCurChannel]) == 256) {
                ReadTables20();
            }
        } else if (decodeNumber(this.LD) == 269) {
            ReadTables20();
        }
    }

    /* access modifiers changed from: protected */
    public byte DecodeAudio(int Delta) {
        AudioVariables v = this.AudV[this.UnpCurChannel];
        v.setByteCount(v.getByteCount() + 1);
        v.setD4(v.getD3());
        v.setD3(v.getD2());
        v.setD2(v.getLastDelta() - v.getD1());
        v.setD1(v.getLastDelta());
        int Ch = ((((((v.getLastChar() * 8) + (v.getK1() * v.getD1())) + ((v.getK2() * v.getD2()) + (v.getK3() * v.getD3()))) + ((v.getK4() * v.getD4()) + (v.getK5() * this.UnpChannelDelta))) >>> 3) & MotionEventCompat.ACTION_MASK) - Delta;
        int D = ((byte) Delta) << 3;
        int[] dif = v.getDif();
        dif[0] = dif[0] + Math.abs(D);
        int[] dif2 = v.getDif();
        dif2[1] = dif2[1] + Math.abs(D - v.getD1());
        int[] dif3 = v.getDif();
        dif3[2] = dif3[2] + Math.abs(v.getD1() + D);
        int[] dif4 = v.getDif();
        dif4[3] = dif4[3] + Math.abs(D - v.getD2());
        int[] dif5 = v.getDif();
        dif5[4] = dif5[4] + Math.abs(v.getD2() + D);
        int[] dif6 = v.getDif();
        dif6[5] = dif6[5] + Math.abs(D - v.getD3());
        int[] dif7 = v.getDif();
        dif7[6] = dif7[6] + Math.abs(v.getD3() + D);
        int[] dif8 = v.getDif();
        dif8[7] = dif8[7] + Math.abs(D - v.getD4());
        int[] dif9 = v.getDif();
        dif9[8] = dif9[8] + Math.abs(v.getD4() + D);
        int[] dif10 = v.getDif();
        dif10[9] = dif10[9] + Math.abs(D - this.UnpChannelDelta);
        int[] dif11 = v.getDif();
        dif11[10] = dif11[10] + Math.abs(this.UnpChannelDelta + D);
        v.setLastDelta((byte) (Ch - v.getLastChar()));
        this.UnpChannelDelta = v.getLastDelta();
        v.setLastChar(Ch);
        if ((v.getByteCount() & 31) == 0) {
            int MinDif = v.getDif()[0];
            int NumMinDif = 0;
            v.getDif()[0] = 0;
            for (int I = 1; I < v.getDif().length; I++) {
                if (v.getDif()[I] < MinDif) {
                    MinDif = v.getDif()[I];
                    NumMinDif = I;
                }
                v.getDif()[I] = 0;
            }
            switch (NumMinDif) {
                case 1:
                    if (v.getK1() >= -16) {
                        v.setK1(v.getK1() - 1);
                        break;
                    }
                    break;
                case 2:
                    if (v.getK1() < 16) {
                        v.setK1(v.getK1() + 1);
                        break;
                    }
                    break;
                case 3:
                    if (v.getK2() >= -16) {
                        v.setK2(v.getK2() - 1);
                        break;
                    }
                    break;
                case 4:
                    if (v.getK2() < 16) {
                        v.setK2(v.getK2() + 1);
                        break;
                    }
                    break;
                case 5:
                    if (v.getK3() >= -16) {
                        v.setK3(v.getK3() - 1);
                        break;
                    }
                    break;
                case 6:
                    if (v.getK3() < 16) {
                        v.setK3(v.getK3() + 1);
                        break;
                    }
                    break;
                case 7:
                    if (v.getK4() >= -16) {
                        v.setK4(v.getK4() - 1);
                        break;
                    }
                    break;
                case 8:
                    if (v.getK4() < 16) {
                        v.setK4(v.getK4() + 1);
                        break;
                    }
                    break;
                case 9:
                    if (v.getK5() >= -16) {
                        v.setK5(v.getK5() - 1);
                        break;
                    }
                    break;
                case 10:
                    if (v.getK5() < 16) {
                        v.setK5(v.getK5() + 1);
                        break;
                    }
                    break;
            }
        }
        return (byte) Ch;
    }
}
