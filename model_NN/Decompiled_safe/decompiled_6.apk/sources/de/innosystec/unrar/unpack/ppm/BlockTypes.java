package de.innosystec.unrar.unpack.ppm;

public enum BlockTypes {
    BLOCK_LZ(0),
    BLOCK_PPM(1);
    
    private int blockType;

    private BlockTypes(int blockType2) {
        this.blockType = blockType2;
    }

    public int getBlockType() {
        return this.blockType;
    }

    public boolean equals(int blockType2) {
        return this.blockType == blockType2;
    }

    public static BlockTypes findBlockType(int blockType2) {
        if (BLOCK_LZ.equals(blockType2)) {
            return BLOCK_LZ;
        }
        if (BLOCK_PPM.equals(blockType2)) {
            return BLOCK_PPM;
        }
        return null;
    }
}
