package de.innosystec.unrar.unpack.ppm;

import android.support.v4.view.MotionEventCompat;

public class StateRef {
    private int freq;
    private int successor;
    private int symbol;

    public int getSymbol() {
        return this.symbol;
    }

    public void setSymbol(int symbol2) {
        this.symbol = symbol2 & MotionEventCompat.ACTION_MASK;
    }

    public int getFreq() {
        return this.freq;
    }

    public void setFreq(int freq2) {
        this.freq = freq2 & MotionEventCompat.ACTION_MASK;
    }

    public void incFreq(int dFreq) {
        this.freq = (this.freq + dFreq) & MotionEventCompat.ACTION_MASK;
    }

    public void decFreq(int dFreq) {
        this.freq = (this.freq - dFreq) & MotionEventCompat.ACTION_MASK;
    }

    public void setValues(State statePtr) {
        setFreq(statePtr.getFreq());
        setSuccessor(statePtr.getSuccessor());
        setSymbol(statePtr.getSymbol());
    }

    public int getSuccessor() {
        return this.successor;
    }

    public void setSuccessor(PPMContext successor2) {
        setSuccessor(successor2.getAddress());
    }

    public void setSuccessor(int successor2) {
        this.successor = successor2;
    }

    public String toString() {
        return "State[" + "\n  symbol=" + getSymbol() + "\n  freq=" + getFreq() + "\n  successor=" + getSuccessor() + "\n]";
    }
}
