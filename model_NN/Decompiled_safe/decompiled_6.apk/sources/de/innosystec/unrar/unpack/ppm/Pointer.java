package de.innosystec.unrar.unpack.ppm;

public abstract class Pointer {
    static final /* synthetic */ boolean $assertionsDisabled = (!Pointer.class.desiredAssertionStatus());
    protected byte[] mem;
    protected int pos;

    public Pointer(byte[] mem2) {
        this.mem = mem2;
    }

    public int getAddress() {
        if ($assertionsDisabled || this.mem != null) {
            return this.pos;
        }
        throw new AssertionError();
    }

    public void setAddress(int pos2) {
        if (!$assertionsDisabled && this.mem == null) {
            throw new AssertionError();
        } else if ($assertionsDisabled || (pos2 >= 0 && pos2 < this.mem.length)) {
            this.pos = pos2;
        } else {
            throw new AssertionError(pos2);
        }
    }
}
