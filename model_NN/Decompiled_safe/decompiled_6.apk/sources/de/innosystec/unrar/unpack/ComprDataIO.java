package de.innosystec.unrar.unpack;

import de.innosystec.unrar.Archive;
import de.innosystec.unrar.Volume;
import de.innosystec.unrar.crc.RarCRC;
import de.innosystec.unrar.exception.RarException;
import de.innosystec.unrar.io.ReadOnlyAccessInputStream;
import de.innosystec.unrar.rarfile.FileHeader;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ComprDataIO {
    private final Archive archive;
    private long curPackRead;
    private long curPackWrite;
    private long curUnpRead;
    private long curUnpWrite;
    private char currentCommand;
    private int decryption;
    private int encryption;
    private InputStream inputStream;
    private int lastPercent;
    private boolean nextVolumeMissing;
    private OutputStream outputStream;
    private long packFileCRC;
    private boolean packVolume;
    private long packedCRC;
    private long processedArcSize;
    private boolean skipUnpCRC;
    private FileHeader subHead;
    private boolean testMode;
    private long totalArcSize;
    private long totalPackRead;
    private long unpArcSize;
    private long unpFileCRC;
    private long unpPackedSize;
    private boolean unpVolume;

    public ComprDataIO(Archive arc) {
        this.archive = arc;
    }

    public void init(OutputStream outputStream2) {
        this.outputStream = outputStream2;
        this.unpPackedSize = 0;
        this.testMode = false;
        this.skipUnpCRC = false;
        this.packVolume = false;
        this.unpVolume = false;
        this.nextVolumeMissing = false;
        this.encryption = 0;
        this.decryption = 0;
        this.totalPackRead = 0;
        this.curUnpWrite = 0;
        this.curUnpRead = 0;
        this.curPackWrite = 0;
        this.curPackRead = 0;
        this.packedCRC = -1;
        this.unpFileCRC = -1;
        this.packFileCRC = -1;
        this.lastPercent = -1;
        this.subHead = null;
        this.currentCommand = 0;
        this.totalArcSize = 0;
        this.processedArcSize = 0;
    }

    public void init(FileHeader hd) throws IOException {
        long startPos = hd.getPositionInFile() + ((long) hd.getHeaderSize());
        this.unpPackedSize = hd.getFullPackSize();
        this.inputStream = new ReadOnlyAccessInputStream(this.archive.getRof(), startPos, this.unpPackedSize + startPos);
        this.subHead = hd;
        this.curUnpRead = 0;
        this.curPackWrite = 0;
        this.packedCRC = -1;
    }

    public int unpRead(byte[] addr, int offset, int count) throws IOException, RarException {
        int readSize;
        int retCode = 0;
        int totalRead = 0;
        while (count > 0) {
            if (((long) count) > this.unpPackedSize) {
                readSize = (int) this.unpPackedSize;
            } else {
                readSize = count;
            }
            retCode = this.inputStream.read(addr, offset, readSize);
            if (retCode >= 0) {
                if (this.subHead.isSplitAfter()) {
                    this.packedCRC = (long) RarCRC.checkCrc((int) this.packedCRC, addr, offset, retCode);
                }
                this.curUnpRead += (long) retCode;
                totalRead += retCode;
                offset += retCode;
                count -= retCode;
                this.unpPackedSize -= (long) retCode;
                this.archive.bytesReadRead(retCode);
                if (this.unpPackedSize != 0 || !this.subHead.isSplitAfter()) {
                    break;
                } else if (!Volume.mergeArchive(this.archive, this)) {
                    this.nextVolumeMissing = true;
                    return -1;
                }
            } else {
                throw new EOFException();
            }
        }
        if (retCode != -1) {
            retCode = totalRead;
        }
        return retCode;
    }

    public void unpWrite(byte[] addr, int offset, int count) throws IOException {
        if (!this.testMode) {
            this.outputStream.write(addr, offset, count);
        }
        this.curUnpWrite += (long) count;
        if (this.skipUnpCRC) {
            return;
        }
        if (this.archive.isOldFormat()) {
            this.unpFileCRC = (long) RarCRC.checkOldCrc((short) ((int) this.unpFileCRC), addr, count);
        } else {
            this.unpFileCRC = (long) RarCRC.checkCrc((int) this.unpFileCRC, addr, offset, count);
        }
    }

    public void setPackedSizeToRead(long size) {
        this.unpPackedSize = size;
    }

    public void setTestMode(boolean mode) {
        this.testMode = mode;
    }

    public void setSkipUnpCRC(boolean skip) {
        this.skipUnpCRC = skip;
    }

    public void setSubHeader(FileHeader hd) {
        this.subHead = hd;
    }

    public long getCurPackRead() {
        return this.curPackRead;
    }

    public void setCurPackRead(long curPackRead2) {
        this.curPackRead = curPackRead2;
    }

    public long getCurPackWrite() {
        return this.curPackWrite;
    }

    public void setCurPackWrite(long curPackWrite2) {
        this.curPackWrite = curPackWrite2;
    }

    public long getCurUnpRead() {
        return this.curUnpRead;
    }

    public void setCurUnpRead(long curUnpRead2) {
        this.curUnpRead = curUnpRead2;
    }

    public long getCurUnpWrite() {
        return this.curUnpWrite;
    }

    public void setCurUnpWrite(long curUnpWrite2) {
        this.curUnpWrite = curUnpWrite2;
    }

    public int getDecryption() {
        return this.decryption;
    }

    public void setDecryption(int decryption2) {
        this.decryption = decryption2;
    }

    public int getEncryption() {
        return this.encryption;
    }

    public void setEncryption(int encryption2) {
        this.encryption = encryption2;
    }

    public boolean isNextVolumeMissing() {
        return this.nextVolumeMissing;
    }

    public void setNextVolumeMissing(boolean nextVolumeMissing2) {
        this.nextVolumeMissing = nextVolumeMissing2;
    }

    public long getPackedCRC() {
        return this.packedCRC;
    }

    public void setPackedCRC(long packedCRC2) {
        this.packedCRC = packedCRC2;
    }

    public long getPackFileCRC() {
        return this.packFileCRC;
    }

    public void setPackFileCRC(long packFileCRC2) {
        this.packFileCRC = packFileCRC2;
    }

    public boolean isPackVolume() {
        return this.packVolume;
    }

    public void setPackVolume(boolean packVolume2) {
        this.packVolume = packVolume2;
    }

    public long getProcessedArcSize() {
        return this.processedArcSize;
    }

    public void setProcessedArcSize(long processedArcSize2) {
        this.processedArcSize = processedArcSize2;
    }

    public long getTotalArcSize() {
        return this.totalArcSize;
    }

    public void setTotalArcSize(long totalArcSize2) {
        this.totalArcSize = totalArcSize2;
    }

    public long getTotalPackRead() {
        return this.totalPackRead;
    }

    public void setTotalPackRead(long totalPackRead2) {
        this.totalPackRead = totalPackRead2;
    }

    public long getUnpArcSize() {
        return this.unpArcSize;
    }

    public void setUnpArcSize(long unpArcSize2) {
        this.unpArcSize = unpArcSize2;
    }

    public long getUnpFileCRC() {
        return this.unpFileCRC;
    }

    public void setUnpFileCRC(long unpFileCRC2) {
        this.unpFileCRC = unpFileCRC2;
    }

    public boolean isUnpVolume() {
        return this.unpVolume;
    }

    public void setUnpVolume(boolean unpVolume2) {
        this.unpVolume = unpVolume2;
    }

    public FileHeader getSubHeader() {
        return this.subHead;
    }
}
