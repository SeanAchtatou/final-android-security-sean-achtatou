package de.innosystec.unrar.unpack.ppm;

import de.innosystec.unrar.exception.RarException;
import de.innosystec.unrar.unpack.Unpack;
import java.io.IOException;

public class RangeCoder {
    public static final int BOT = 32768;
    public static final int TOP = 16777216;
    private static final long uintMask = 4294967295L;
    private long code;
    private long low;
    private long range;
    private final SubRange subRange = new SubRange();
    private Unpack unpackRead;

    public SubRange getSubRange() {
        return this.subRange;
    }

    public void initDecoder(Unpack unpackRead2) throws IOException, RarException {
        this.unpackRead = unpackRead2;
        this.code = 0;
        this.low = 0;
        this.range = uintMask;
        for (int i = 0; i < 4; i++) {
            this.code = ((this.code << 8) | ((long) getChar())) & uintMask;
        }
    }

    public int getCurrentCount() {
        this.range = (this.range / this.subRange.getScale()) & uintMask;
        return (int) ((this.code - this.low) / this.range);
    }

    public long getCurrentShiftCount(int SHIFT) {
        this.range >>>= SHIFT;
        return ((this.code - this.low) / this.range) & uintMask;
    }

    public void decode() {
        this.low = (this.low + (this.range * this.subRange.getLowCount())) & uintMask;
        this.range = (this.range * (this.subRange.getHighCount() - this.subRange.getLowCount())) & uintMask;
    }

    private int getChar() throws IOException, RarException {
        return this.unpackRead.getChar();
    }

    public void ariDecNormalize() throws IOException, RarException {
        boolean c2 = false;
        while (true) {
            if ((this.low ^ (this.low + this.range)) >= 16777216) {
                c2 = this.range < 32768;
                if (!c2) {
                    return;
                }
            }
            if (c2) {
                this.range = (-this.low) & 32767 & uintMask;
                c2 = false;
            }
            this.code = ((this.code << 8) | ((long) getChar())) & uintMask;
            this.range = (this.range << 8) & uintMask;
            this.low = (this.low << 8) & uintMask;
        }
    }

    public String toString() {
        return "RangeCoder[" + "\n  low=" + this.low + "\n  code=" + this.code + "\n  range=" + this.range + "\n  subrange=" + this.subRange + "]";
    }

    public static class SubRange {
        private long highCount;
        private long lowCount;
        private long scale;

        public long getHighCount() {
            return this.highCount;
        }

        public void setHighCount(long highCount2) {
            this.highCount = RangeCoder.uintMask & highCount2;
        }

        public long getLowCount() {
            return this.lowCount & RangeCoder.uintMask;
        }

        public void setLowCount(long lowCount2) {
            this.lowCount = RangeCoder.uintMask & lowCount2;
        }

        public long getScale() {
            return this.scale;
        }

        public void setScale(long scale2) {
            this.scale = RangeCoder.uintMask & scale2;
        }

        public void incScale(int dScale) {
            setScale(getScale() + ((long) dScale));
        }

        public String toString() {
            return "SubRange[" + "\n  lowCount=" + this.lowCount + "\n  highCount=" + this.highCount + "\n  scale=" + this.scale + "]";
        }
    }
}
