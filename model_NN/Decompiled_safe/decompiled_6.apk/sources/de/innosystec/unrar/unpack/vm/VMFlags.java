package de.innosystec.unrar.unpack.vm;

public enum VMFlags {
    VM_FC(1),
    VM_FZ(2),
    VM_FS(Integer.MIN_VALUE);
    
    private int flag;

    private VMFlags(int flag2) {
        this.flag = flag2;
    }

    public static VMFlags findFlag(int flag2) {
        if (VM_FC.equals(flag2)) {
            return VM_FC;
        }
        if (VM_FS.equals(flag2)) {
            return VM_FS;
        }
        if (VM_FZ.equals(flag2)) {
            return VM_FZ;
        }
        return null;
    }

    public boolean equals(int flag2) {
        return this.flag == flag2;
    }

    public int getFlag() {
        return this.flag;
    }
}
