package de.innosystec.unrar.rarfile;

import de.innosystec.unrar.io.Raw;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class MarkHeader extends BaseBlock {
    private Log logger = LogFactory.getLog(MarkHeader.class.getName());
    private boolean oldFormat = false;

    public MarkHeader(BaseBlock bb) {
        super(bb);
    }

    public boolean isValid() {
        if (getHeadCRC() == 24914 && getHeaderType() == UnrarHeadertype.MarkHeader && getFlags() == 6689 && getHeaderSize() == 7) {
            return true;
        }
        return false;
    }

    public boolean isSignature() {
        byte[] d = new byte[7];
        Raw.writeShortLittleEndian(d, 0, this.headCRC);
        d[2] = this.headerType;
        Raw.writeShortLittleEndian(d, 3, this.flags);
        Raw.writeShortLittleEndian(d, 5, this.headerSize);
        if (d[0] != 82) {
            return false;
        }
        if (d[1] == 69 && d[2] == 126 && d[3] == 94) {
            this.oldFormat = true;
            return true;
        } else if (d[1] != 97 || d[2] != 114 || d[3] != 33 || d[4] != 26 || d[5] != 7 || d[6] != 0) {
            return false;
        } else {
            this.oldFormat = false;
            return true;
        }
    }

    public boolean isOldFormat() {
        return this.oldFormat;
    }

    public void print() {
        super.print();
        this.logger.info("valid: " + isValid());
    }
}
