package de.innosystec.unrar.rarfile;

import de.innosystec.unrar.io.Raw;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class MacInfoHeader extends SubBlockHeader {
    public static final short MacInfoHeaderSize = 8;
    private int fileCreator;
    private int fileType;
    private Log logger = LogFactory.getLog(getClass());

    public MacInfoHeader(SubBlockHeader sb, byte[] macHeader) {
        super(sb);
        this.fileType = Raw.readIntLittleEndian(macHeader, 0);
        this.fileCreator = Raw.readIntLittleEndian(macHeader, 0 + 4);
    }

    public int getFileCreator() {
        return this.fileCreator;
    }

    public void setFileCreator(int fileCreator2) {
        this.fileCreator = fileCreator2;
    }

    public int getFileType() {
        return this.fileType;
    }

    public void setFileType(int fileType2) {
        this.fileType = fileType2;
    }

    public void print() {
        super.print();
        this.logger.info("filetype: " + this.fileType);
        this.logger.info("creator :" + this.fileCreator);
    }
}
