package de.innosystec.unrar.rarfile;

import android.support.v4.view.MotionEventCompat;

public class FileNameDecoder {
    public static int getChar(byte[] name, int pos) {
        return name[pos] & 255;
    }

    public static String decode(byte[] name, int encPos) {
        int encPos2;
        int decPos = 0;
        int flags = 0;
        int flagBits = 0;
        int highByte = getChar(name, encPos);
        StringBuffer buf = new StringBuffer();
        int encPos3 = encPos + 1;
        while (encPos3 < name.length) {
            if (flagBits == 0) {
                encPos2 = encPos3 + 1;
                flags = getChar(name, encPos3);
                flagBits = 8;
            } else {
                encPos2 = encPos3;
            }
            switch (flags >> 6) {
                case 0:
                    encPos3 = encPos2 + 1;
                    buf.append((char) getChar(name, encPos2));
                    decPos++;
                    break;
                case 1:
                    encPos3 = encPos2 + 1;
                    buf.append((char) (getChar(name, encPos2) + (highByte << 8)));
                    decPos++;
                    break;
                case 2:
                    buf.append((char) ((getChar(name, encPos2 + 1) << 8) + getChar(name, encPos2)));
                    decPos++;
                    encPos3 = encPos2 + 2;
                    break;
                case 3:
                    encPos3 = encPos2 + 1;
                    int length = getChar(name, encPos2);
                    if ((length & 128) != 0) {
                        int encPos4 = encPos3 + 1;
                        int correction = getChar(name, encPos3);
                        int length2 = (length & 127) + 2;
                        while (length2 > 0 && decPos < name.length) {
                            buf.append((char) ((highByte << 8) + ((getChar(name, decPos) + correction) & MotionEventCompat.ACTION_MASK)));
                            length2--;
                            decPos++;
                        }
                        encPos3 = encPos4;
                        break;
                    } else {
                        int length3 = length + 2;
                        while (length3 > 0 && decPos < name.length) {
                            buf.append((char) getChar(name, decPos));
                            length3--;
                            decPos++;
                        }
                    }
                    break;
                default:
                    encPos3 = encPos2;
                    break;
            }
            flags = (flags << 2) & MotionEventCompat.ACTION_MASK;
            flagBits -= 2;
        }
        return buf.toString();
    }
}
