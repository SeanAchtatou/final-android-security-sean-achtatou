package de.innosystec.unrar.rarfile;

import de.innosystec.unrar.io.Raw;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class EAHeader extends SubBlockHeader {
    public static final short EAHeaderSize = 10;
    private int EACRC;
    private Log logger = LogFactory.getLog(getClass());
    private byte method;
    private int unpSize;
    private byte unpVer;

    public EAHeader(SubBlockHeader sb, byte[] eahead) {
        super(sb);
        this.unpSize = Raw.readIntLittleEndian(eahead, 0);
        int pos = 0 + 4;
        this.unpVer = (byte) (this.unpVer | (eahead[pos] & 255));
        int pos2 = pos + 1;
        this.method = (byte) (this.method | (eahead[pos2] & 255));
        this.EACRC = Raw.readIntLittleEndian(eahead, pos2 + 1);
    }

    public int getEACRC() {
        return this.EACRC;
    }

    public byte getMethod() {
        return this.method;
    }

    public int getUnpSize() {
        return this.unpSize;
    }

    public byte getUnpVer() {
        return this.unpVer;
    }

    public void print() {
        super.print();
        this.logger.info("unpSize: " + this.unpSize);
        this.logger.info("unpVersion: " + ((int) this.unpVer));
        this.logger.info("method: " + ((int) this.method));
        this.logger.info("EACRC:" + this.EACRC);
    }
}
