package de.innosystec.unrar.rarfile;

import cn.com.jit.ida.util.pki.extension.SubjectDirectoryAttributesExt;
import de.innosystec.unrar.io.Raw;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class UnixOwnersHeader extends SubBlockHeader {
    private String group;
    private int groupNameSize;
    private Log logger = LogFactory.getLog(UnixOwnersHeader.class);
    private String owner;
    private int ownerNameSize;

    public UnixOwnersHeader(SubBlockHeader sb, byte[] uoHeader) {
        super(sb);
        this.ownerNameSize = Raw.readShortLittleEndian(uoHeader, 0) & SubjectDirectoryAttributesExt.NONE;
        int pos = 0 + 2;
        this.groupNameSize = Raw.readShortLittleEndian(uoHeader, pos) & SubjectDirectoryAttributesExt.NONE;
        int pos2 = pos + 2;
        if (this.ownerNameSize + 4 < uoHeader.length) {
            byte[] ownerBuffer = new byte[this.ownerNameSize];
            System.arraycopy(uoHeader, pos2, ownerBuffer, 0, this.ownerNameSize);
            this.owner = new String(ownerBuffer);
        }
        int pos3 = this.ownerNameSize + 4;
        if (this.groupNameSize + pos3 < uoHeader.length) {
            byte[] groupBuffer = new byte[this.groupNameSize];
            System.arraycopy(uoHeader, pos3, groupBuffer, 0, this.groupNameSize);
            this.group = new String(groupBuffer);
        }
    }

    public String getGroup() {
        return this.group;
    }

    public void setGroup(String group2) {
        this.group = group2;
    }

    public int getGroupNameSize() {
        return this.groupNameSize;
    }

    public void setGroupNameSize(int groupNameSize2) {
        this.groupNameSize = groupNameSize2;
    }

    public String getOwner() {
        return this.owner;
    }

    public void setOwner(String owner2) {
        this.owner = owner2;
    }

    public int getOwnerNameSize() {
        return this.ownerNameSize;
    }

    public void setOwnerNameSize(int ownerNameSize2) {
        this.ownerNameSize = ownerNameSize2;
    }

    public void print() {
        super.print();
        this.logger.info("ownerNameSize: " + this.ownerNameSize);
        this.logger.info("owner: " + this.owner);
        this.logger.info("groupNameSize: " + this.groupNameSize);
        this.logger.info("group: " + this.group);
    }
}
