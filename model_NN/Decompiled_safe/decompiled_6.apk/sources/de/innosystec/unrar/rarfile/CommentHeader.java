package de.innosystec.unrar.rarfile;

import de.innosystec.unrar.io.Raw;

public class CommentHeader extends BaseBlock {
    public static final short commentHeaderSize = 6;
    private short commCRC;
    private byte unpMethod;
    private short unpSize;
    private byte unpVersion;

    public CommentHeader(BaseBlock bb, byte[] commentHeader) {
        super(bb);
        this.unpSize = Raw.readShortLittleEndian(commentHeader, 0);
        int pos = 0 + 2;
        this.unpVersion = (byte) (this.unpVersion | (commentHeader[pos] & 255));
        int pos2 = pos + 1;
        this.unpMethod = (byte) (this.unpMethod | (commentHeader[pos2] & 255));
        this.commCRC = Raw.readShortLittleEndian(commentHeader, pos2 + 1);
    }

    public short getCommCRC() {
        return this.commCRC;
    }

    public byte getUnpMethod() {
        return this.unpMethod;
    }

    public short getUnpSize() {
        return this.unpSize;
    }

    public byte getUnpVersion() {
        return this.unpVersion;
    }
}
