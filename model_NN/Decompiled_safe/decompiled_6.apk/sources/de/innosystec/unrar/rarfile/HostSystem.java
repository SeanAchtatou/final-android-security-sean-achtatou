package de.innosystec.unrar.rarfile;

public enum HostSystem {
    msdos((byte) 0),
    os2((byte) 1),
    win32((byte) 2),
    unix((byte) 3),
    macos((byte) 4),
    beos((byte) 5);
    
    private byte hostByte;

    public static HostSystem findHostSystem(byte hostByte2) {
        if (msdos.equals(hostByte2)) {
            return msdos;
        }
        if (os2.equals(hostByte2)) {
            return os2;
        }
        if (win32.equals(hostByte2)) {
            return win32;
        }
        if (unix.equals(hostByte2)) {
            return unix;
        }
        if (macos.equals(hostByte2)) {
            return macos;
        }
        if (beos.equals(hostByte2)) {
            return beos;
        }
        return null;
    }

    private HostSystem(byte hostByte2) {
        this.hostByte = hostByte2;
    }

    public boolean equals(byte hostByte2) {
        return this.hostByte == hostByte2;
    }

    public byte getHostByte() {
        return this.hostByte;
    }
}
