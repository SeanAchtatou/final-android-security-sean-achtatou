package de.innosystec.unrar.rarfile;

import de.innosystec.unrar.io.Raw;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class BaseBlock {
    public static final short BaseBlockSize = 7;
    public static final short EARC_DATACRC = 2;
    public static final short EARC_NEXT_VOLUME = 1;
    public static final short EARC_REVSPACE = 4;
    public static final short EARC_VOLNUMBER = 8;
    public static final short LHD_COMMENT = 8;
    public static final short LHD_DIRECTORY = 224;
    public static final short LHD_EXTFLAGS = 8192;
    public static final short LHD_EXTTIME = 4096;
    public static final short LHD_LARGE = 256;
    public static final short LHD_PASSWORD = 4;
    public static final short LHD_SALT = 1024;
    public static final short LHD_SOLID = 16;
    public static final short LHD_SPLIT_AFTER = 2;
    public static final short LHD_SPLIT_BEFORE = 1;
    public static final short LHD_UNICODE = 512;
    public static final short LHD_VERSION = 2048;
    public static final short LHD_WINDOW1024 = 128;
    public static final short LHD_WINDOW128 = 32;
    public static final short LHD_WINDOW2048 = 160;
    public static final short LHD_WINDOW256 = 64;
    public static final short LHD_WINDOW4096 = 192;
    public static final short LHD_WINDOW512 = 96;
    public static final short LHD_WINDOW64 = 0;
    public static final short LHD_WINDOWMASK = 224;
    public static final short LONG_BLOCK = Short.MIN_VALUE;
    public static final short MHD_AV = 32;
    public static final short MHD_COMMENT = 2;
    public static final short MHD_ENCRYPTVER = 512;
    public static final short MHD_FIRSTVOLUME = 256;
    public static final short MHD_LOCK = 4;
    public static final short MHD_NEWNUMBERING = 16;
    public static final short MHD_PACK_COMMENT = 16;
    public static final short MHD_PASSWORD = 128;
    public static final short MHD_PROTECT = 64;
    public static final short MHD_SOLID = 8;
    public static final short MHD_VOLUME = 1;
    public static final short SKIP_IF_UNKNOWN = 16384;
    protected short flags = 0;
    protected short headCRC = 0;
    protected short headerSize = 0;
    protected byte headerType = 0;
    Log logger = LogFactory.getLog(BaseBlock.class.getName());
    protected long positionInFile;

    public BaseBlock() {
    }

    public BaseBlock(BaseBlock bb) {
        this.flags = bb.getFlags();
        this.headCRC = bb.getHeadCRC();
        this.headerType = bb.getHeaderType().getHeaderByte();
        this.headerSize = bb.getHeaderSize();
        this.positionInFile = bb.getPositionInFile();
    }

    public BaseBlock(byte[] baseBlockHeader) {
        this.headCRC = Raw.readShortLittleEndian(baseBlockHeader, 0);
        int pos = 0 + 2;
        this.headerType = (byte) (this.headerType | (baseBlockHeader[pos] & 255));
        int pos2 = pos + 1;
        this.flags = Raw.readShortLittleEndian(baseBlockHeader, pos2);
        this.headerSize = Raw.readShortLittleEndian(baseBlockHeader, pos2 + 2);
    }

    public boolean hasArchiveDataCRC() {
        return (this.flags & 2) != 0;
    }

    public boolean hasVolumeNumber() {
        return (this.flags & 8) != 0;
    }

    public boolean hasEncryptVersion() {
        return (this.flags & 512) != 0;
    }

    public boolean isSubBlock() {
        if (UnrarHeadertype.SubHeader.equals(this.headerType)) {
            return true;
        }
        if (!UnrarHeadertype.NewSubHeader.equals(this.headerType) || (this.flags & 16) == 0) {
            return false;
        }
        return true;
    }

    public long getPositionInFile() {
        return this.positionInFile;
    }

    public short getFlags() {
        return this.flags;
    }

    public short getHeadCRC() {
        return this.headCRC;
    }

    public short getHeaderSize() {
        return this.headerSize;
    }

    public UnrarHeadertype getHeaderType() {
        return UnrarHeadertype.findType(this.headerType);
    }

    public void setPositionInFile(long positionInFile2) {
        this.positionInFile = positionInFile2;
    }

    public void print() {
        StringBuilder str = new StringBuilder();
        str.append("HeaderType: " + getHeaderType());
        str.append("\nHeadCRC: " + Integer.toHexString(getHeadCRC()));
        str.append("\nFlags: " + Integer.toHexString(getFlags()));
        str.append("\nHeaderSize: " + ((int) getHeaderSize()));
        str.append("\nPosition in file: " + getPositionInFile());
        this.logger.info(str.toString());
    }
}
