package de.innosystec.unrar.rarfile;

import de.innosystec.unrar.io.Raw;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class BlockHeader extends BaseBlock {
    public static final short blockHeaderSize = 4;
    private int dataSize;
    private Log logger = LogFactory.getLog(BlockHeader.class.getName());
    private int packSize;

    public BlockHeader() {
    }

    public BlockHeader(BlockHeader bh) {
        super(bh);
        this.packSize = bh.getDataSize();
        this.dataSize = this.packSize;
        this.positionInFile = bh.getPositionInFile();
    }

    public BlockHeader(BaseBlock bb, byte[] blockHeader) {
        super(bb);
        this.packSize = Raw.readIntLittleEndian(blockHeader, 0);
        this.dataSize = this.packSize;
    }

    public int getDataSize() {
        return this.dataSize;
    }

    public int getPackSize() {
        return this.packSize;
    }

    public void print() {
        super.print();
        this.logger.info("DataSize: " + getDataSize() + " packSize: " + getPackSize());
    }
}
