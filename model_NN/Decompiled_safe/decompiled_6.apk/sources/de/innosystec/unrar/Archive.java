package de.innosystec.unrar;

import de.innosystec.unrar.exception.RarException;
import de.innosystec.unrar.io.IReadOnlyAccess;
import de.innosystec.unrar.io.ReadOnlyAccessFile;
import de.innosystec.unrar.rarfile.AVHeader;
import de.innosystec.unrar.rarfile.BaseBlock;
import de.innosystec.unrar.rarfile.BlockHeader;
import de.innosystec.unrar.rarfile.CommentHeader;
import de.innosystec.unrar.rarfile.EAHeader;
import de.innosystec.unrar.rarfile.EndArcHeader;
import de.innosystec.unrar.rarfile.FileHeader;
import de.innosystec.unrar.rarfile.MacInfoHeader;
import de.innosystec.unrar.rarfile.MainHeader;
import de.innosystec.unrar.rarfile.MarkHeader;
import de.innosystec.unrar.rarfile.ProtectHeader;
import de.innosystec.unrar.rarfile.SignHeader;
import de.innosystec.unrar.rarfile.SubBlockHeader;
import de.innosystec.unrar.rarfile.UnixOwnersHeader;
import de.innosystec.unrar.rarfile.UnrarHeadertype;
import de.innosystec.unrar.unpack.ComprDataIO;
import de.innosystec.unrar.unpack.Unpack;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Archive implements Closeable {
    private static Logger logger = Logger.getLogger(Archive.class.getName());
    private long arcDataCRC;
    private int currentHeaderIndex;
    private final ComprDataIO dataIO;
    private boolean encrypted;
    private EndArcHeader endHeader;
    private File file;
    private final List<BaseBlock> headers;
    private MarkHeader markHead;
    private MainHeader newMhd;
    private IReadOnlyAccess rof;
    private int sfxSize;
    private long totalPackedRead;
    private long totalPackedSize;
    private Unpack unpack;
    private final UnrarCallback unrarCallback;

    public Archive(File file2) throws RarException, IOException {
        this(file2, null);
    }

    public Archive(File file2, UnrarCallback unrarCallback2) throws RarException, IOException {
        this.headers = new ArrayList();
        this.markHead = null;
        this.newMhd = null;
        this.endHeader = null;
        this.arcDataCRC = -1;
        this.encrypted = false;
        this.sfxSize = 0;
        this.totalPackedSize = 0;
        this.totalPackedRead = 0;
        setFile(file2);
        this.unrarCallback = unrarCallback2;
        this.dataIO = new ComprDataIO(this);
    }

    public File getFile() {
        return this.file;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* access modifiers changed from: package-private */
    public void setFile(File file2) throws IOException {
        this.file = file2;
        this.totalPackedSize = 0;
        this.totalPackedRead = 0;
        close();
        this.rof = new ReadOnlyAccessFile(file2);
        try {
            readHeaders();
        } catch (Exception e) {
            logger.log(Level.WARNING, "exception in archive constructor maybe file is encrypted or currupt", (Throwable) e);
        }
        for (BaseBlock block : this.headers) {
            if (block.getHeaderType() == UnrarHeadertype.FileHeader) {
                this.totalPackedSize += ((FileHeader) block).getFullPackSize();
            }
        }
        if (this.unrarCallback != null) {
            this.unrarCallback.volumeProgressChanged(this.totalPackedRead, this.totalPackedSize);
        }
    }

    public void bytesReadRead(int count) {
        if (count > 0) {
            this.totalPackedRead += (long) count;
            if (this.unrarCallback != null) {
                this.unrarCallback.volumeProgressChanged(this.totalPackedRead, this.totalPackedSize);
            }
        }
    }

    public IReadOnlyAccess getRof() {
        return this.rof;
    }

    public List<FileHeader> getFileHeaders() {
        List<FileHeader> list = new ArrayList<>();
        for (BaseBlock block : this.headers) {
            if (block.getHeaderType().equals(UnrarHeadertype.FileHeader)) {
                list.add((FileHeader) block);
            }
        }
        return list;
    }

    public FileHeader nextFileHeader() {
        int n = this.headers.size();
        while (this.currentHeaderIndex < n) {
            List<BaseBlock> list = this.headers;
            int i = this.currentHeaderIndex;
            this.currentHeaderIndex = i + 1;
            BaseBlock block = list.get(i);
            if (block.getHeaderType() == UnrarHeadertype.FileHeader) {
                return (FileHeader) block;
            }
        }
        return null;
    }

    public UnrarCallback getUnrarCallback() {
        return this.unrarCallback;
    }

    public boolean isEncrypted() {
        if (this.newMhd != null) {
            return this.newMhd.isEncrypted();
        }
        throw new NullPointerException("mainheader is null");
    }

    private void readHeaders() throws IOException, RarException {
        EndArcHeader endArcHead;
        this.markHead = null;
        this.newMhd = null;
        this.endHeader = null;
        this.headers.clear();
        this.currentHeaderIndex = 0;
        long fileLength = this.file.length();
        while (true) {
            byte[] baseBlockBuffer = new byte[7];
            long position = this.rof.getPosition();
            if (position < fileLength && this.rof.readFully(baseBlockBuffer, 7) != 0) {
                BaseBlock block = new BaseBlock(baseBlockBuffer);
                block.setPositionInFile(position);
                switch (block.getHeaderType()) {
                    case MarkHeader:
                        this.markHead = new MarkHeader(block);
                        if (this.markHead.isSignature()) {
                            this.headers.add(this.markHead);
                            break;
                        } else {
                            throw new RarException(RarException.RarExceptionType.badRarArchive);
                        }
                    case MainHeader:
                        int toRead = block.hasEncryptVersion() ? 7 : 6;
                        byte[] mainbuff = new byte[toRead];
                        int mainHeaderSize = this.rof.readFully(mainbuff, toRead);
                        MainHeader mainHeader = new MainHeader(block, mainbuff);
                        this.headers.add(mainHeader);
                        this.newMhd = mainHeader;
                        if (!this.newMhd.isEncrypted()) {
                            break;
                        } else {
                            throw new RarException(RarException.RarExceptionType.rarEncryptedException);
                        }
                    case SignHeader:
                        byte[] signBuff = new byte[8];
                        int signHeaderSize = this.rof.readFully(signBuff, 8);
                        this.headers.add(new SignHeader(block, signBuff));
                        break;
                    case AvHeader:
                        byte[] avBuff = new byte[7];
                        int avHeaderSize = this.rof.readFully(avBuff, 7);
                        this.headers.add(new AVHeader(block, avBuff));
                        break;
                    case CommHeader:
                        byte[] commBuff = new byte[6];
                        int commHeaderSize = this.rof.readFully(commBuff, 6);
                        CommentHeader commHead = new CommentHeader(block, commBuff);
                        this.headers.add(commHead);
                        this.rof.setPosition(commHead.getPositionInFile() + ((long) commHead.getHeaderSize()));
                        break;
                    case EndArcHeader:
                        int toRead2 = 0;
                        if (block.hasArchiveDataCRC()) {
                            toRead2 = 0 + 4;
                        }
                        if (block.hasVolumeNumber()) {
                            toRead2 += 2;
                        }
                        if (toRead2 > 0) {
                            byte[] endArchBuff = new byte[toRead2];
                            int endArcHeaderSize = this.rof.readFully(endArchBuff, toRead2);
                            endArcHead = new EndArcHeader(block, endArchBuff);
                        } else {
                            endArcHead = new EndArcHeader(block, null);
                        }
                        this.headers.add(endArcHead);
                        this.endHeader = endArcHead;
                        return;
                    default:
                        byte[] blockHeaderBuffer = new byte[4];
                        int readFully = this.rof.readFully(blockHeaderBuffer, 4);
                        BlockHeader blockHead = new BlockHeader(block, blockHeaderBuffer);
                        switch (blockHead.getHeaderType()) {
                            case NewSubHeader:
                            case FileHeader:
                                int toRead3 = (blockHead.getHeaderSize() - 7) - 4;
                                byte[] fileHeaderBuffer = new byte[toRead3];
                                int readFully2 = this.rof.readFully(fileHeaderBuffer, toRead3);
                                FileHeader fileHeader = new FileHeader(blockHead, fileHeaderBuffer);
                                this.headers.add(fileHeader);
                                this.rof.setPosition(fileHeader.getPositionInFile() + ((long) fileHeader.getHeaderSize()) + fileHeader.getFullPackSize());
                                continue;
                            case ProtectHeader:
                                int toRead4 = (blockHead.getHeaderSize() - 7) - 4;
                                byte[] protectHeaderBuffer = new byte[toRead4];
                                int readFully3 = this.rof.readFully(protectHeaderBuffer, toRead4);
                                ProtectHeader protectHeader = new ProtectHeader(blockHead, protectHeaderBuffer);
                                this.rof.setPosition(protectHeader.getPositionInFile() + ((long) protectHeader.getHeaderSize()) + ((long) protectHeader.getDataSize()));
                                continue;
                            case SubHeader:
                                byte[] subHeadbuffer = new byte[3];
                                int readFully4 = this.rof.readFully(subHeadbuffer, 3);
                                SubBlockHeader subBlockHeader = new SubBlockHeader(blockHead, subHeadbuffer);
                                subBlockHeader.print();
                                switch (subBlockHeader.getSubType()) {
                                    case MAC_HEAD:
                                        byte[] macHeaderbuffer = new byte[8];
                                        int readFully5 = this.rof.readFully(macHeaderbuffer, 8);
                                        MacInfoHeader macInfoHeader = new MacInfoHeader(subBlockHeader, macHeaderbuffer);
                                        macInfoHeader.print();
                                        this.headers.add(macInfoHeader);
                                        continue;
                                    case BEEA_HEAD:
                                    case NTACL_HEAD:
                                    case STREAM_HEAD:
                                    default:
                                        continue;
                                    case EA_HEAD:
                                        byte[] eaHeaderBuffer = new byte[10];
                                        int readFully6 = this.rof.readFully(eaHeaderBuffer, 10);
                                        EAHeader eaHeader = new EAHeader(subBlockHeader, eaHeaderBuffer);
                                        eaHeader.print();
                                        this.headers.add(eaHeader);
                                        continue;
                                    case UO_HEAD:
                                        int toRead5 = ((subBlockHeader.getHeaderSize() - 7) - 4) - 3;
                                        byte[] uoHeaderBuffer = new byte[toRead5];
                                        int readFully7 = this.rof.readFully(uoHeaderBuffer, toRead5);
                                        UnixOwnersHeader unixOwnersHeader = new UnixOwnersHeader(subBlockHeader, uoHeaderBuffer);
                                        unixOwnersHeader.print();
                                        this.headers.add(unixOwnersHeader);
                                        continue;
                                }
                            default:
                                logger.warning("Unknown Header");
                                throw new RarException(RarException.RarExceptionType.notRarArchive);
                        }
                }
            } else {
                return;
            }
        }
    }

    public void extractFile(FileHeader hd, OutputStream os) throws RarException {
        if (!this.headers.contains(hd)) {
            throw new RarException(RarException.RarExceptionType.headerNotInArchive);
        }
        try {
            doExtractFile(hd, os);
        } catch (Exception e) {
            if (e instanceof RarException) {
                throw ((RarException) e);
            }
            throw new RarException(e);
        }
    }

    private void doExtractFile(FileHeader hd, OutputStream os) throws RarException, IOException {
        long actualCRC;
        this.dataIO.init(os);
        this.dataIO.init(hd);
        this.dataIO.setUnpFileCRC(isOldFormat() ? 0 : -1);
        if (this.unpack == null) {
            this.unpack = new Unpack(this.dataIO);
        }
        if (!hd.isSolid()) {
            this.unpack.init(null);
        }
        this.unpack.setDestSize(hd.getFullUnpackSize());
        try {
            this.unpack.doUnpack(hd.getUnpVersion(), hd.isSolid());
            FileHeader hd2 = this.dataIO.getSubHeader();
            if (hd2.isSplitAfter()) {
                actualCRC = this.dataIO.getPackedCRC() ^ -1;
            } else {
                actualCRC = this.dataIO.getUnpFileCRC() ^ -1;
            }
            if (actualCRC != ((long) hd2.getFileCRC())) {
                throw new RarException(RarException.RarExceptionType.crcError);
            }
        } catch (Exception e) {
            this.unpack.cleanUp();
            if (e instanceof RarException) {
                throw ((RarException) e);
            }
            throw new RarException(e);
        }
    }

    public MainHeader getMainHeader() {
        return this.newMhd;
    }

    public boolean isOldFormat() {
        return this.markHead.isOldFormat();
    }

    public void close() throws IOException {
        if (this.rof != null) {
            this.rof.close();
            this.rof = null;
        }
        if (this.unpack != null) {
            this.unpack.cleanUp();
        }
    }
}
