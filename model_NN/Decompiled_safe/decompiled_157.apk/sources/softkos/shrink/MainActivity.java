package softkos.shrink;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import softkos.shrink.Vars;

public class MainActivity extends Activity {
    static MainActivity instance;
    Button closeDlgButton = null;
    GameCanvas gameCanvas = null;
    LinearLayout gameLayout = null;
    Dialog howToPlayDlg = null;
    Vars.GameState lastState;
    MenuCanvas menuCanvas = null;
    SettingsCanvas settingsCanvas = null;
    boolean skipInitNewGame = false;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        instance = this;
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        Vars.getInstance().screenSize(dm.widthPixels, dm.heightPixels);
        ImageLoader.getInstance().LoadImages();
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        SoundManager.getInstance();
        showMenu();
    }

    public void showMenu() {
        Vars.getInstance().gameState = Vars.GameState.Menu;
        if (this.menuCanvas == null) {
            this.menuCanvas = new MenuCanvas(this);
        }
        if (this.gameCanvas != null) {
            this.gameCanvas.showUI(false);
        }
        OtherApp.getInstance().random();
        this.menuCanvas.updateButtonText();
        this.menuCanvas.showUI(true);
        this.menuCanvas.layoutUI();
        setContentView(this.menuCanvas);
    }

    public void showSettings() {
        this.lastState = Vars.getInstance().gameState;
        Vars.getInstance().gameState = Vars.GameState.Settings;
        if (this.settingsCanvas == null) {
            this.settingsCanvas = new SettingsCanvas(this);
        }
        this.settingsCanvas.showUI(true);
        this.settingsCanvas.layoutUI();
        setContentView(this.settingsCanvas);
    }

    public void returnFromSettings() {
        Settings.getInstnace().saveSettings();
        if (this.lastState == Vars.GameState.Game) {
            this.skipInitNewGame = true;
            showGame();
        }
        if (this.lastState == Vars.GameState.Menu) {
            showMenu();
        }
        if (this.lastState == Vars.GameState.HowPlay) {
            showHowPlay();
        }
    }

    public Dialog getHowToPlayDlg() {
        return this.howToPlayDlg;
    }

    public void showHowPlay() {
        this.howToPlayDlg = new Dialog(this);
        this.howToPlayDlg.requestWindowFeature(1);
        this.howToPlayDlg.setContentView((int) R.layout.how_to_play);
        this.howToPlayDlg.show();
        this.closeDlgButton = (Button) this.howToPlayDlg.findViewById(R.id.close_dlg);
        this.closeDlgButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MainActivity.getInstance().getHowToPlayDlg().dismiss();
            }
        });
    }

    public void showHighscores() {
    }

    public void showGame() {
        if (this.menuCanvas != null) {
            this.menuCanvas.showUI(false);
        }
        if (this.gameCanvas == null) {
            this.gameLayout = new LinearLayout(this);
            this.gameLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
            this.gameLayout.setOrientation(1);
            this.gameCanvas = new GameCanvas(this);
            AdView adView = new AdView(this, AdSize.BANNER, "a14dce540b09d00");
            adView.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
            this.gameCanvas.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
            adView.loadAd(new AdRequest());
            this.gameLayout.addView(adView);
            this.gameLayout.addView(this.gameCanvas);
        }
        Vars.getInstance().gameState = Vars.GameState.Game;
        if (!this.skipInitNewGame) {
            Vars.getInstance().newGame();
        }
        this.skipInitNewGame = false;
        this.gameCanvas.showUI(true);
        setContentView(this.gameLayout);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4 && (Vars.getInstance().gameState == Vars.GameState.Game || Vars.getInstance().gameState == Vars.GameState.Highscores || Vars.getInstance().gameState == Vars.GameState.HowPlay || Vars.getInstance().gameState == Vars.GameState.Settings)) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyUp(keyCode, event);
        }
        if (Vars.getInstance().gameState == Vars.GameState.Game || Vars.getInstance().gameState == Vars.GameState.Highscores || Vars.getInstance().gameState == Vars.GameState.HowPlay) {
            showMenu();
        }
        if (Vars.getInstance().gameState == Vars.GameState.Settings) {
            returnFromSettings();
        }
        return true;
    }

    public static MainActivity getInstance() {
        return instance;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem add = menu.add(0, 2000, 0, "Reset level");
        MenuItem add2 = menu.add(0, 2003, 0, "Settings");
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 2000:
                Vars.getInstance().resetLevel();
                return true;
            case 2001:
            case 2002:
            default:
                return false;
            case 2003:
                showSettings();
                return true;
        }
    }
}
