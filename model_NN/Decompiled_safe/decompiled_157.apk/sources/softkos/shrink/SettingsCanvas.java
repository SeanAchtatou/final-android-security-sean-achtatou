package softkos.shrink;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;

public class SettingsCanvas extends View {
    static SettingsCanvas instance = null;
    gButton[] otherAppsBtn;
    Paint paint = new Paint();
    PaintManager paintMgr;
    gButton soundSettingsBtn = null;
    Vars vars;

    public SettingsCanvas(Context c) {
        super(c);
        instance = this;
        this.vars = Vars.getInstance();
        this.paintMgr = PaintManager.getInstance();
        initUI();
    }

    public void showUI(boolean show) {
        for (int i = 0; i < this.vars.getButtonListSize(); i++) {
            this.vars.getButton(i).hide();
        }
        if (show) {
            this.soundSettingsBtn.show();
        }
    }

    public void initUI() {
        int btn_w = 260;
        int btn_h = 50;
        if (this.vars.getScreenWidth() > 400) {
            btn_w = 380;
            btn_h = 70;
        }
        this.soundSettingsBtn = new gButton();
        this.soundSettingsBtn.setSize(btn_w, btn_h);
        this.soundSettingsBtn.setId(Vars.getInstance().SOUND_SETTINGS);
        this.soundSettingsBtn.show();
        this.soundSettingsBtn.setBackImages("240x50off", "240x50on");
        Vars.getInstance().addButton(this.soundSettingsBtn);
        updateButtonText();
    }

    public void updateButtonText() {
        String s;
        if (Settings.getInstnace().getIntSettings(Settings.getInstnace().SOUND_DISABLED) != 0) {
            s = String.valueOf("Sound: ") + "OFF";
        } else {
            s = String.valueOf("Sound: ") + "ON";
        }
        this.soundSettingsBtn.setText(s);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        layoutUI();
    }

    public void layoutUI() {
        int xs = (getWidth() / 2) - (this.soundSettingsBtn.getWidth() / 2);
        int ys = getHeight() / 4;
        int height = (int) (((double) this.soundSettingsBtn.getHeight()) * 1.1d);
        this.soundSettingsBtn.setPosition(xs, ys);
    }

    public void mouseDown(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDown(x, y);
            if (pressed != 0) {
                invalidate();
                return;
            }
        }
    }

    public void mouseUp(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseUp(x, y);
            if (pressed != 0) {
                handleCommand(Vars.getInstance().getButton(i).getId());
                invalidate();
                return;
            }
        }
    }

    public void mouseDrag(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDrag(x, y);
            if (pressed != 0) {
                invalidate();
                return;
            }
        }
    }

    public void handleCommand(int id) {
        if (id == Vars.getInstance().SOUND_SETTINGS) {
            Settings.getInstnace().toggleSettings(Settings.getInstnace().SOUND_DISABLED);
            updateButtonText();
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();
        if (event.getAction() == 0) {
            mouseDown(x, y);
        } else if (2 == event.getAction()) {
            mouseDrag(x, y);
        } else if (1 == event.getAction()) {
            mouseUp(x, y);
        }
        invalidate();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.paint.setAntiAlias(true);
        this.paintMgr.drawImage(canvas, "gameback", 0, 0, getWidth(), getHeight());
        this.paintMgr.setColor(-256);
        this.paintMgr.setTextSize(60);
        this.paintMgr.drawString(canvas, "Settings", 0, 20, getWidth(), 70, PaintManager.STR_CENTER);
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            Vars.getInstance().getButton(i).draw(canvas, this.paint);
        }
    }

    public static SettingsCanvas getInstance() {
        return instance;
    }
}
