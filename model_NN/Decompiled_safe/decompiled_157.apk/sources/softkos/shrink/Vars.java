package softkos.shrink;

import android.graphics.Color;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;

public class Vars {
    static Vars instance = null;
    public int GO_TO_MOBILSOFT;
    public int HOWTOPLAY;
    public int OTHER_APP;
    public int PLAY_NEXT_LEVEL;
    public int RESET_ALL_LEVELS;
    public int SOUND_SETTINGS;
    public int START_GAME;
    public int TWITTER;
    int[][] board;
    int board_h;
    int board_pos_x;
    int board_pos_y;
    int board_w;
    ArrayList<Box> boxList;
    ArrayList<gButton> buttonList;
    boolean dont_init_color;
    public GameState gameState;
    boolean game_over;
    int last_bonus;
    int last_scores;
    int level;
    int[] level_borders;
    int lives;
    public int painting;
    Random random;
    int scoreA;
    int scoreB;
    int scoreColor;
    int scoreG;
    int scoreR;
    int scores;
    int[][] scores_board;
    int screenHeight;
    int screenWidth;
    int shrinking_border;
    int target_area;
    Timer updateTimer;

    public enum GameState {
        Menu,
        Game,
        Highscores,
        HowPlay,
        Settings
    }

    public int getLastScores() {
        return this.last_scores;
    }

    public int getLastBonus() {
        return this.last_bonus;
    }

    public int getScoreColor() {
        this.scoreA -= 2;
        this.scoreColor = Color.argb(this.scoreA, this.scoreR, this.scoreG, this.scoreB);
        if (this.scoreA < 0) {
            this.scoreA = 0;
            this.scoreColor = 0;
        }
        return this.scoreColor;
    }

    public Vars() {
        this.painting = 0;
        this.level = 0;
        this.scores = 0;
        this.target_area = 0;
        this.lives = 0;
        this.last_scores = 0;
        this.last_bonus = 0;
        this.scoreColor = 0;
        this.scoreR = 200;
        this.scoreG = 200;
        this.scoreB = 200;
        this.scoreA = 200;
        this.board_w = 6;
        this.board_h = 6;
        this.buttonList = null;
        this.boxList = null;
        this.dont_init_color = false;
        this.shrinking_border = -1;
        this.game_over = false;
        this.SOUND_SETTINGS = 1001;
        this.RESET_ALL_LEVELS = 1002;
        this.START_GAME = 100;
        this.GO_TO_MOBILSOFT = 101;
        this.HOWTOPLAY = 102;
        this.TWITTER = 103;
        this.OTHER_APP = 104;
        this.PLAY_NEXT_LEVEL = 200;
        this.buttonList = new ArrayList<>();
        this.buttonList.clear();
        this.boxList = new ArrayList<>();
        this.boxList.clear();
        this.random = new Random();
        this.updateTimer = new Timer();
        this.updateTimer.schedule(new UpdateTimer(), 10, 50);
        this.board = (int[][]) Array.newInstance(Integer.TYPE, this.board_w, this.board_h);
        this.level = 0;
    }

    public int getBoardWidth() {
        return this.board_w;
    }

    public int getBoardHeight() {
        return this.board_h;
    }

    public void advanceFrame() {
        if (this.gameState == GameState.Menu || this.gameState == GameState.Game) {
            if (this.gameState == GameState.Game) {
                if (this.lives >= 0) {
                    shrinkBorders();
                    updateBoxes();
                    checkLevelFailed();
                } else {
                    return;
                }
            }
            if (this.gameState == GameState.Menu) {
                for (int i = 0; i < this.buttonList.size(); i++) {
                    getButton(i).updatePosition();
                }
            }
        }
    }

    public void updateBoxes() {
        for (int i = 0; i < this.boxList.size(); i++) {
            int sx = getBox(i).getSpeedX();
            int sy = getBox(i).getSpeedY();
            int px = getBox(i).getPx();
            int py = getBox(i).getPy();
            int w = getBox(i).getWidth();
            int h = getBox(i).getHeight();
            boolean hit = false;
            int x = 0;
            while (x < Math.abs(sx)) {
                if (px <= getLeftBorder()) {
                    sx = -sx;
                    hit = true;
                }
                if (px + w >= getRightBorder()) {
                    sx = -sx;
                    hit = true;
                }
                if (sx < 0) {
                    px--;
                }
                if (sx > 0) {
                    px++;
                }
                getBox(i).SetPos(px, py);
                if (!checkLevelFailed()) {
                    x++;
                } else {
                    return;
                }
            }
            int y = 0;
            while (y < Math.abs(sy)) {
                if (sy < 0) {
                    py--;
                }
                if (sy > 0) {
                    py++;
                }
                if (py <= getTopBorder()) {
                    sy = -sy;
                    hit = true;
                }
                if (py + h >= getBottomBorder()) {
                    sy = -sy;
                    hit = true;
                }
                if (hit) {
                    SoundManager.getInstance().playSound(1);
                }
                getBox(i).SetPos(px, py);
                if (!checkLevelFailed()) {
                    y++;
                } else {
                    return;
                }
            }
            getBox(i).setSpeeds(sx, sy);
        }
    }

    public boolean checkLevelFailed() {
        boolean failed = false;
        int i = 0;
        while (true) {
            if (i >= this.boxList.size()) {
                break;
            }
            int px = getBox(i).getPx();
            int py = getBox(i).getPy();
            int w = getBox(i).getWidth();
            int h = getBox(i).getHeight();
            if (px < getLeftBorder() || px + w > getRightBorder()) {
                failed = true;
            } else if (py < getTopBorder() || py + h > getBottomBorder()) {
                failed = true;
            } else if (this.shrinking_border >= 0 && ((this.shrinking_border == 0 && px < getLeftBorder()) || ((this.shrinking_border == 2 && px + w > getRightBorder()) || ((this.shrinking_border == 1 && py < getTopBorder()) || (this.shrinking_border == 3 && py + h > getBottomBorder()))))) {
                failed = true;
            } else {
                i++;
            }
        }
        failed = true;
        if (failed) {
            this.lives--;
            if (this.lives < 0) {
                gameOver();
            } else {
                this.dont_init_color = true;
                initLevel();
            }
        }
        return failed;
    }

    public void hideAllButtons() {
        for (int i = 0; i < this.buttonList.size(); i++) {
            getButton(i).hide();
        }
    }

    public int getBoardPosY() {
        return this.board_pos_y;
    }

    public int getBoardPosX() {
        return this.board_pos_x;
    }

    public void initLevel() {
        this.shrinking_border = -1;
        this.level_borders = new int[4];
        this.level_borders[0] = 20;
        this.level_borders[1] = this.screenHeight - 70;
        this.level_borders[2] = 20;
        this.level_borders[3] = this.screenWidth - 20;
        this.boxList.clear();
        if (!this.dont_init_color) {
            this.scoreA = 200;
            this.scoreColor = Color.argb(this.scoreA, this.scoreR, this.scoreG, this.scoreB);
        }
        this.dont_init_color = false;
        Levels.getInstance().fillLevelData(this.level);
        int max_area = 0;
        for (int i = 0; i < getBoxList().size(); i++) {
            int area = getBox(i).getWidth() * getBox(i).getHeight();
            if (area > max_area) {
                max_area = area;
            }
        }
        this.target_area = (((int) Math.sqrt(1.8d * ((double) (this.level + 1)))) + 1) * max_area;
    }

    public int getBonus() {
        int bonus = (getTargetArea() - getArea()) / 40;
        if (bonus < 0) {
            return 0;
        }
        return bonus;
    }

    public void nextLevel() {
        this.level++;
        this.scores += 100;
        int bonus = getBonus();
        this.scores += bonus;
        this.last_scores = 100;
        this.last_bonus = bonus;
        initLevel();
    }

    public int getScore() {
        return this.scores;
    }

    public int getTargetArea() {
        return this.target_area;
    }

    public int getArea() {
        return (getRightBorder() - getLeftBorder()) * (getBottomBorder() - getTopBorder());
    }

    public void shrinkBorders() {
        if (this.shrinking_border >= 0) {
            if (this.shrinking_border == 0) {
                int[] iArr = this.level_borders;
                iArr[2] = iArr[2] + 2;
            }
            if (this.shrinking_border == 2) {
                int[] iArr2 = this.level_borders;
                iArr2[3] = iArr2[3] - 2;
            }
            if (this.shrinking_border == 1) {
                int[] iArr3 = this.level_borders;
                iArr3[0] = iArr3[0] + 2;
            }
            if (this.shrinking_border == 3) {
                int[] iArr4 = this.level_borders;
                iArr4[1] = iArr4[1] - 2;
            }
        }
    }

    public int getTopBorder() {
        return this.level_borders[0];
    }

    public int getBottomBorder() {
        return this.level_borders[1];
    }

    public int getLeftBorder() {
        return this.level_borders[2];
    }

    public int getRightBorder() {
        return this.level_borders[3];
    }

    public void shrinkBorder(int b) {
        this.shrinking_border = b;
    }

    public void stopShrinking() {
        this.shrinking_border = -1;
    }

    public double dist(int x1, int y1, int x2, int y2) {
        return Math.sqrt((double) (((x1 - x2) * (x1 - x2)) + ((y1 - y2) * (y1 - y2))));
    }

    public void setLevel(int l) {
        this.level = l;
    }

    public void prevLevel() {
        this.level--;
        if (this.level < 0) {
            this.level = 0;
        }
        initLevel();
    }

    public void resetLevel() {
        initLevel();
    }

    public int getLevel() {
        return this.level;
    }

    public void newGame() {
        this.lives = 9;
        this.scores = 0;
        this.game_over = false;
        this.level = 0;
        this.last_scores = 0;
        this.last_bonus = 0;
        initLevel();
    }

    public void setLives(int l) {
        this.lives = l;
    }

    public int getLives() {
        return this.lives;
    }

    public int getButtonListSize() {
        return this.buttonList.size();
    }

    public void addButton(gButton b) {
        this.buttonList.add(b);
    }

    public ArrayList<gButton> getButtonList() {
        return this.buttonList;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public gButton getButton(int b) {
        if (b < 0 || b >= this.buttonList.size()) {
            return null;
        }
        return this.buttonList.get(b);
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public Box getBox(int i) {
        if (i < 0 || i >= this.boxList.size()) {
            return null;
        }
        return this.boxList.get(i);
    }

    public ArrayList<Box> getBoxList() {
        return this.boxList;
    }

    public int getScreenWidth() {
        return this.screenWidth;
    }

    public int getScreenHeight() {
        return this.screenHeight;
    }

    public void init() {
        this.random = new Random();
    }

    public int GetNextInt(int max) {
        if (max == 0) {
            max = 1;
        }
        return Math.abs(this.random.nextInt() % max);
    }

    public Random getRandom() {
        return this.random;
    }

    public void screenSize(int w, int h) {
        this.screenWidth = w;
        this.screenHeight = h;
    }

    public static Vars getInstance() {
        if (instance == null) {
            instance = new Vars();
            instance.init();
        }
        return instance;
    }

    public int nextInt(int max) {
        if (max < 1) {
            return 0;
        }
        return this.random.nextInt(max);
    }

    public void gameOver() {
        this.boxList.clear();
        this.game_over = true;
    }

    public boolean isGameOver() {
        return this.game_over;
    }
}
