package softkos.shrink;

public class Box extends GameObject {
    int speed_x;
    int speed_y;
    int type;

    public void setSpeeds(int sx, int sy) {
        this.speed_x = sx;
        this.speed_y = sy;
    }

    public int getSpeedX() {
        return this.speed_x;
    }

    public int getSpeedY() {
        return this.speed_y;
    }

    public void setType(int t) {
        this.type = t;
    }

    public int getType() {
        return this.type;
    }
}
