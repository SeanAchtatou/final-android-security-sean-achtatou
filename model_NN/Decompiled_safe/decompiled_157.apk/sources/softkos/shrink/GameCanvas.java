package softkos.shrink;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class GameCanvas extends View {
    static GameCanvas instance = null;
    boolean dialog_opened = false;
    Dialog gameOverDlg = null;
    Button gameOverNo = null;
    Button gameOverYes = null;
    int key_size = 32;
    int mouseDownX = 0;
    int mouseDownY = 0;
    int mouseX = -1;
    int mouseY = -1;
    Paint paint;
    PaintManager paintMgr;
    gButton playNextBtn = null;
    Vars vars;
    int vpx;
    int vpy;

    public GameCanvas(Context context) {
        super(context);
        instance = this;
        this.paint = new Paint();
        this.paintMgr = PaintManager.getInstance();
        this.vars = Vars.getInstance();
        initUI();
    }

    public void initUI() {
        int btn_w = 260;
        int btn_h = 50;
        if (this.vars.getScreenWidth() > 400) {
            btn_w = 380;
            btn_h = 70;
        }
        this.playNextBtn = new gButton();
        this.playNextBtn.setSize(btn_w, btn_h);
        this.playNextBtn.setId(Vars.getInstance().PLAY_NEXT_LEVEL);
        this.playNextBtn.hide();
        this.playNextBtn.setText("Next level");
        this.playNextBtn.setBackImages("240x50off", "240x50on");
        Vars.getInstance().addButton(this.playNextBtn);
    }

    public void showUI(boolean show) {
        this.vars.hideAllButtons();
    }

    public void layoutUI() {
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        layoutUI();
        this.key_size = getWidth() / 4;
        invalidate();
    }

    public void drawGame(Canvas canvas) {
        if (this.vars.isGameOver()) {
            showGameOverDlg();
        }
        this.paintMgr.drawImage(canvas, "gameback", 0, 0, getWidth(), getHeight());
        this.playNextBtn.hide();
        if (this.vars.getArea() < this.vars.getTargetArea()) {
            this.paintMgr.drawImage(canvas, "top_1", this.vars.getLeftBorder(), this.vars.getTopBorder() - 20, this.vars.getRightBorder() - this.vars.getLeftBorder(), 20);
            this.paintMgr.drawImage(canvas, "bottom_1", this.vars.getLeftBorder(), this.vars.getBottomBorder(), this.vars.getRightBorder() - this.vars.getLeftBorder(), 20);
            this.paintMgr.drawImage(canvas, "top_left_1", this.vars.getLeftBorder() - 20, this.vars.getTopBorder() - 20, 20, 20);
            this.paintMgr.drawImage(canvas, "top_right_1", this.vars.getRightBorder(), this.vars.getTopBorder() - 20, 20, 20);
            this.paintMgr.drawImage(canvas, "left_1", this.vars.getLeftBorder() - 23, this.vars.getTopBorder(), 20, this.vars.getBottomBorder() - this.vars.getTopBorder());
            this.paintMgr.drawImage(canvas, "right_1", this.vars.getRightBorder() + 3, this.vars.getTopBorder(), 20, this.vars.getBottomBorder() - this.vars.getTopBorder());
            this.paintMgr.drawImage(canvas, "bottom_left_1", this.vars.getLeftBorder() - 20, this.vars.getBottomBorder(), 20, 20);
            this.paintMgr.drawImage(canvas, "bottom_right_1", this.vars.getRightBorder(), this.vars.getBottomBorder(), 20, 20);
            this.playNextBtn.setPosition((getWidth() / 2) - (this.playNextBtn.getWidth() / 2), (getHeight() - (this.key_size * 2)) - this.playNextBtn.getHeight());
            this.playNextBtn.show();
        } else {
            this.paintMgr.drawImage(canvas, "top_0", this.vars.getLeftBorder(), this.vars.getTopBorder() - 20, this.vars.getRightBorder() - this.vars.getLeftBorder(), 20);
            this.paintMgr.drawImage(canvas, "bottom_0", this.vars.getLeftBorder(), this.vars.getBottomBorder(), this.vars.getRightBorder() - this.vars.getLeftBorder(), 20);
            this.paintMgr.drawImage(canvas, "top_left_0", this.vars.getLeftBorder() - 20, this.vars.getTopBorder() - 20, 20, 20);
            this.paintMgr.drawImage(canvas, "top_right_0", this.vars.getRightBorder(), this.vars.getTopBorder() - 20, 20, 20);
            this.paintMgr.drawImage(canvas, "left_0", this.vars.getLeftBorder() - 23, this.vars.getTopBorder(), 20, this.vars.getBottomBorder() - this.vars.getTopBorder());
            this.paintMgr.drawImage(canvas, "right_0", this.vars.getRightBorder() + 3, this.vars.getTopBorder(), 20, this.vars.getBottomBorder() - this.vars.getTopBorder());
            this.paintMgr.drawImage(canvas, "bottom_left_0", this.vars.getLeftBorder() - 20, this.vars.getBottomBorder(), 20, 20);
            this.paintMgr.drawImage(canvas, "bottom_right_0", this.vars.getRightBorder(), this.vars.getBottomBorder(), 20, 20);
        }
        for (int b = 0; b < this.vars.getBoxList().size(); b++) {
            this.paintMgr.drawImage(canvas, "box_" + this.vars.getBox(b).getType(), this.vars.getBox(b).getPx(), this.vars.getBox(b).getPy(), this.vars.getBox(b).getWidth(), this.vars.getBox(b).getHeight());
        }
        this.paintMgr.setTextSize(20);
        this.paintMgr.setColor(-1);
        Canvas canvas2 = canvas;
        this.paintMgr.drawString(canvas2, "Level: " + (this.vars.getLevel() + 1), 5, 5, getWidth(), 25, PaintManager.STR_LEFT);
        Canvas canvas3 = canvas;
        this.paintMgr.drawString(canvas3, "Target: " + this.vars.getTargetArea(), (getWidth() / 2) - 10, 5, getWidth() / 2, 25, PaintManager.STR_LEFT);
        Canvas canvas4 = canvas;
        this.paintMgr.drawString(canvas4, "Score: " + this.vars.getScore(), 5, 28, getWidth() - 10, 25, PaintManager.STR_LEFT);
        Canvas canvas5 = canvas;
        this.paintMgr.drawString(canvas5, "Area: " + this.vars.getArea(), (getWidth() / 2) - 10, 28, (getWidth() / 2) - 10, 25, PaintManager.STR_LEFT);
        Canvas canvas6 = canvas;
        this.paintMgr.drawString(canvas6, "Bonus: " + this.vars.getBonus(), 5, 51, getWidth() - 10, 25, PaintManager.STR_LEFT);
        this.paintMgr.drawImage(canvas, "lives", getWidth() - 40, 5, 40, 40);
        this.paintMgr.setColor(-1);
        this.paintMgr.drawString(canvas, new StringBuilder().append(this.vars.getLives()).toString(), getWidth() - 40, 12, 40, 40, PaintManager.STR_CENTER);
        int scorecolor = this.vars.getScoreColor();
        if (scorecolor > 0 && this.vars.getLastScores() > 0) {
            this.paintMgr.setTextSize(32);
            this.paintMgr.setColor(scorecolor);
            this.paintMgr.drawString(canvas, "+ " + this.vars.getLastScores(), 0, getHeight() / 3, getWidth(), 40, PaintManager.STR_CENTER);
            this.paintMgr.drawString(canvas, "Bonus + " + this.vars.getLastBonus(), 0, (getHeight() / 3) + 40, getWidth(), 40, PaintManager.STR_CENTER);
        }
        this.paintMgr.drawImage(canvas, "key_right", getWidth() - this.key_size, getHeight() - this.key_size, this.key_size, this.key_size);
        this.paintMgr.drawImage(canvas, "key_down", getWidth() - (this.key_size * 2), getHeight() - this.key_size, this.key_size, this.key_size);
        this.paintMgr.drawImage(canvas, "key_left", getWidth() - (this.key_size * 3), getHeight() - this.key_size, this.key_size, this.key_size);
        this.paintMgr.drawImage(canvas, "key_up", getWidth() - (this.key_size * 2), getHeight() - (this.key_size * 2), this.key_size, this.key_size);
    }

    public void checkKeys(int x, int y) {
        if (y > getHeight() - this.key_size && x > getWidth() - this.key_size) {
            this.vars.shrinkBorder(0);
        } else if (y > getHeight() - this.key_size && x > getWidth() - (this.key_size * 2) && x < getWidth() - this.key_size) {
            this.vars.shrinkBorder(1);
        } else if (y > getHeight() - this.key_size && x > getWidth() - (this.key_size * 3) && x < getWidth() - (this.key_size * 2)) {
            this.vars.shrinkBorder(2);
        } else if (y <= getHeight() - (this.key_size * 2) || y >= getHeight() - this.key_size || x <= getWidth() - (this.key_size * 2) || x >= getWidth() - this.key_size) {
            this.vars.shrinkBorder(-1);
        } else {
            this.vars.shrinkBorder(3);
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.paint == null) {
            this.paint = new Paint();
        }
        if (canvas != null) {
            drawGame(canvas);
            for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
                Vars.getInstance().getButton(i).draw(canvas, this.paint);
            }
        }
    }

    public void mouseDown(int x, int y) {
        this.mouseDownX = x;
        this.mouseDownY = y;
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDown(x, y);
            if (pressed != 0) {
                invalidate();
                return;
            }
        }
        checkKeys(x, y);
    }

    public void mouseDrag(int x, int y) {
        this.mouseX = x;
        this.mouseY = y;
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDrag(x, y);
            if (pressed != 0) {
                invalidate();
                return;
            }
        }
        checkKeys(x, y);
    }

    public void mouseUp(int x, int y) {
        this.mouseX = -1;
        this.mouseY = -1;
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseUp(x, y);
            if (pressed != 0) {
                handleCommand(Vars.getInstance().getButton(i).getId());
                invalidate();
                return;
            }
        }
        this.vars.stopShrinking();
    }

    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();
        if (event.getAction() == 0) {
            mouseDown(x, y);
        } else if (2 == event.getAction()) {
            mouseDrag(x, y);
        } else if (1 == event.getAction()) {
            mouseUp(x, y);
        }
        invalidate();
        return true;
    }

    public void handleCommand(int id) {
        if (id == this.vars.PLAY_NEXT_LEVEL) {
            this.vars.nextLevel();
        }
    }

    public static GameCanvas getInstance() {
        return instance;
    }

    public Dialog getGameOverDialog() {
        return this.gameOverDlg;
    }

    public void closeGameOverDialog() {
        MainActivity.getInstance().showMenu();
        this.dialog_opened = false;
        getGameOverDialog().dismiss();
    }

    public void showGameOverDlg() {
        if (!this.dialog_opened) {
            this.dialog_opened = true;
            this.gameOverDlg = new Dialog(MainActivity.getInstance());
            this.gameOverDlg.requestWindowFeature(1);
            this.gameOverDlg.setContentView((int) R.layout.game_over);
            ((TextView) this.gameOverDlg.findViewById(R.id.gameover_msg)).setText("Your score is:\n\n" + this.vars.getScore() + "\n\nDo you want to submit it online?");
            this.gameOverDlg.show();
            this.gameOverYes = (Button) this.gameOverDlg.findViewById(R.id.gameover_yes);
            this.gameOverYes.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    GameCanvas.getInstance().closeGameOverDialog();
                    OnlineScores.getInstance().enterNameDialog();
                }
            });
            this.gameOverNo = (Button) this.gameOverDlg.findViewById(R.id.gameover_no);
            this.gameOverNo.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    GameCanvas.getInstance().closeGameOverDialog();
                }
            });
        }
    }
}
