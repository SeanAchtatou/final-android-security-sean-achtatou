package com.zestadz.android;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.util.Log;
import android.webkit.WebView;
import java.net.InetAddress;
import java.net.URLEncoder;
import java.util.Locale;

public class AdManager {
    static final String LOG = "ZestADZ SDK";
    static final String SDK_SITE_ID = "14131C047A50414347574B574153415E8B";
    static final String SDK_VERSION_DATE = "20110202";
    private static String adclientId;
    private static InetAddress thisIp;
    private static String ua;
    private static String userAgent;

    public static String getAdclientId(Context context) {
        if (adclientId == null) {
            try {
                ApplicationInfo info = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
                if (info != null) {
                    String id = info.metaData.getString("ZestADZ_Adclient_ID");
                    Log.d(LOG, "AdClient ID read from AndroidManifest.xml is " + id);
                    if (!id.equals(SDK_SITE_ID) || !context.getPackageName().equals("com.zestadz.android")) {
                        setadclientId(id);
                    } else {
                        Log.i(LOG, "This is a default adclient ID. Please get yours from zestadz");
                        adclientId = id;
                    }
                }
            } catch (Exception e) {
                Log.e(LOG, "Could not read ZestADZ_Adclient_ID meta-data from AndroidManifest.xml.", e);
            }
        }
        return adclientId;
    }

    public static void setadclientId(String id) {
        if (id == null) {
            integrationError("SETUP ERROR:  Incorrect ZestADZ adclient ID. Please specify correct AdCleint ID in AndroidManifest.xml file:  " + id);
        }
        if (id.equalsIgnoreCase(SDK_SITE_ID)) {
            integrationError("SETUP ERROR:  Cannot use the sample adclient ID (14131C047A50414347574B574153415E8B).  Yours is available on www.zestadz.com.");
        }
        Log.i(LOG, "Adclient ID set to " + id);
        adclientId = id;
    }

    protected static void integrationError(String message) {
        Log.e(LOG, message);
        throw new IllegalArgumentException(message);
    }

    static String getIPAddress() {
        try {
            thisIp = InetAddress.getLocalHost();
            String thisHostIP = thisIp.getHostAddress();
            Log.v("IPADDRESS", thisHostIP.toString());
            return thisHostIP;
        } catch (Exception e) {
            Log.e("INETADDRESS", e.toString());
            return null;
        }
    }

    static String getUserAgent(Context context) {
        if (userAgent == null) {
            StringBuffer arg = new StringBuffer();
            String version = Build.VERSION.RELEASE;
            if (version.length() > 0) {
                arg.append(version);
            } else {
                arg.append("1.0");
            }
            arg.append("; ");
            Locale l = Locale.getDefault();
            String language = l.getLanguage();
            if (language != null) {
                arg.append(language.toLowerCase());
                String country = l.getCountry();
                if (country != null) {
                    arg.append("-");
                    arg.append(country.toLowerCase());
                }
            } else {
                arg.append("en");
            }
            String model = Build.MODEL;
            if (model.length() > 0) {
                arg.append("; ");
                arg.append(model);
            }
            String id = Build.ID;
            if (id.length() > 0) {
                arg.append(" Build/");
                arg.append(id);
            }
            String userAgent2 = new WebView(context).getSettings().getUserAgentString();
            if (userAgent2.contains("google_sdk")) {
                ua = URLEncoder.encode("Mozilla/5.0 (Linux; U; Android 1.5; fr-; HTC Hero Build/CRB43) AppleWebKit/528.5+ (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1");
            } else {
                ua = URLEncoder.encode(userAgent2);
            }
            if (Log.isLoggable(LOG, 3)) {
                Log.d(LOG, "Phone's user-agent is:  " + userAgent2);
            }
        }
        return ua;
    }
}
