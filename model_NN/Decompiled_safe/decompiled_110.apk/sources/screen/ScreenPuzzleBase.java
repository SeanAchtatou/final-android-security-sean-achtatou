package screen;

import android.app.Activity;
import data.Puzzle;
import game.IGame;

public abstract class ScreenPuzzleBase extends ScreenBase {
    protected final Puzzle m_hPuzzle;

    public ScreenPuzzleBase(int iScreenId, Activity hActivity, IGame hGame) {
        super(iScreenId, hActivity, hGame);
        this.m_hPuzzle = hGame.getPuzzle();
    }
}
