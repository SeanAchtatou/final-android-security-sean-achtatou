package screen;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.widget.LinearLayout;
import android.widget.TextView;
import cfg.Option;
import data.PuzzleImage;
import dialog.DialogScreenRating;
import game.IGame;
import helper.PuzzleConfig;
import res.ResDimens;
import res.ResString;
import view.ButtonContainer;
import view.TitleView;

public final class ScreenSolvedAll extends ScreenPuzzleBase {
    private static final int BUTTON_ID_BACK = 0;
    private static final int BUTTON_ID_NEXT = 1;
    private static final int MAX_SCORE = 3000000;
    private final TextView m_tvTotalMoves;
    private final TextView m_tvTotalScore;
    private final TextView m_tvTotalTime;
    private final LinearLayout m_vgContentContainer;

    public ScreenSolvedAll(Activity hActivity, IGame hGame) {
        super(8, hActivity, hGame);
        DisplayMetrics hMetrics = getResources().getDisplayMetrics();
        TitleView hTitle = new TitleView(hActivity, hMetrics);
        hTitle.setTitle(ResString.screen_solved_all_title);
        addView(hTitle);
        this.m_vgContentContainer = new LinearLayout(hActivity);
        this.m_vgContentContainer.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
        this.m_vgContentContainer.setOrientation(1);
        this.m_vgContentContainer.setGravity(17);
        addView(this.m_vgContentContainer);
        TextView tvText = getTextView(hActivity, hMetrics, 25, 1.0f, 20);
        tvText.setText(ResString.screen_solved_all_text);
        this.m_vgContentContainer.addView(tvText);
        this.m_tvTotalMoves = getTextView(hActivity, hMetrics, 20, 0.5f, 5);
        this.m_vgContentContainer.addView(this.m_tvTotalMoves);
        this.m_tvTotalTime = getTextView(hActivity, hMetrics, 20, 0.5f, 5);
        this.m_vgContentContainer.addView(this.m_tvTotalTime);
        this.m_tvTotalScore = getTextView(hActivity, hMetrics, 20, 0.5f, 5);
        this.m_vgContentContainer.addView(this.m_tvTotalScore);
        ButtonContainer vgButtonContainer = new ButtonContainer(hActivity);
        vgButtonContainer.createButton(0, "btn_img_back", this.m_hOnClick);
        vgButtonContainer.createButton(1, "btn_img_next", this.m_hOnClick);
        addView(vgButtonContainer);
    }

    private TextView getTextView(Context hContext, DisplayMetrics hMetrics, int iTextSize, float fShadowRadius, int iPadding) {
        TextView tvText = new TextView(hContext);
        tvText.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        tvText.setGravity(17);
        tvText.setTextColor(-1);
        tvText.setTextSize(1, (float) iTextSize);
        tvText.setTypeface(Typeface.DEFAULT, 1);
        tvText.setShadowLayer(fShadowRadius, 0.0f, 0.0f, Option.HINT_COLOR_DEFAULT);
        int iDipPadding = ResDimens.getDip(hMetrics, iPadding);
        tvText.setPadding(iDipPadding, iDipPadding, iDipPadding, iDipPadding);
        return tvText;
    }

    public boolean beforeShow(int iComeFromScreenId, boolean bReset, Object hData) {
        super.beforeShow(iComeFromScreenId, bReset, hData);
        int iTotalMoves = 0;
        long lTotalTimeInMs = 0;
        for (PuzzleImage hImageData : this.m_hPuzzle.getPuzzleImage()) {
            int iBestMoves = hImageData.getBestMoves();
            int iTotalMovesNew = iTotalMoves + iBestMoves;
            if (iTotalMovesNew - iBestMoves == iTotalMoves) {
                iTotalMoves = iTotalMovesNew;
            } else {
                iTotalMoves = 2147483646;
            }
            long lBestTimeInMs = hImageData.getBestTimeInMs();
            long lTotalTimeInMsNew = lTotalTimeInMs + lBestTimeInMs;
            if (lTotalTimeInMsNew - lBestTimeInMs == lTotalTimeInMs) {
                lTotalTimeInMs = lTotalTimeInMsNew;
            } else {
                lTotalTimeInMs = 9223372036854775806L;
            }
        }
        this.m_tvTotalMoves.setText("Total Moves: " + iTotalMoves);
        this.m_tvTotalTime.setText(ResString.screen_solved_all_label_total_time + String.format(": %.3f s", Float.valueOf(((float) lTotalTimeInMs) / 1000.0f)));
        this.m_tvTotalScore.setText("Total Score: " + calculateTotalScore(this.m_hPuzzle.getImageCount(), lTotalTimeInMs, iTotalMoves));
        return true;
    }

    public void onShow() {
        super.onShow();
        if (PuzzleConfig.getMarketId(this.m_hActivity).intValue() == 0 && DialogScreenRating.shouldShowRatingDialog(this.m_hActivity)) {
            this.m_hGame.getDialogManager().showRating();
        }
        this.m_vgContentContainer.startAnimation(this.m_hAnimIn);
    }

    private int calculateTotalScore(int iImageCount, long lTotalTimeInMS, int iTotalMoves) {
        if (iImageCount <= 0 || lTotalTimeInMS <= 0 || iTotalMoves <= 0) {
            return 0;
        }
        int iTotalScoreImageCountDependent = (MAX_SCORE - ((int) (lTotalTimeInMS / ((long) iImageCount)))) - iTotalMoves;
        if (iTotalScoreImageCountDependent <= 0) {
            return 0;
        }
        return iTotalScoreImageCountDependent;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: game.IGame.switchScreen(int, boolean):void
     arg types: [int, int]
     candidates:
      game.IGame.switchScreen(int, java.lang.Object):void
      game.IGame.switchScreen(int, boolean):void */
    /* access modifiers changed from: protected */
    public void onClick(int iViewId) {
        super.onClick(iViewId);
        switch (iViewId) {
            case 0:
                this.m_hGame.switchScreen(3);
                return;
            case 1:
                this.m_hPuzzle.setPuzzleImageNext();
                this.m_hGame.switchScreen(4, false);
                return;
            default:
                throw new RuntimeException("Invalid view id");
        }
    }
}
