package screen;

import android.app.Activity;
import data.Puzzle;
import data.PuzzleImage;
import game.IGame;
import screen.ScreenPlayBase;

public final class ScreenPlayPuzzleCasual extends ScreenPlayBase {
    private final Puzzle m_hPuzzle;

    public ScreenPlayPuzzleCasual(Activity hActivity, IGame hGame) {
        super(5, hActivity, hGame);
        this.m_hPuzzle = hGame.getPuzzle();
    }

    /* access modifiers changed from: protected */
    public void startLevel() {
        this.m_hPuzzle.setPuzzleImageRandom();
        super.startLevel();
    }

    /* access modifiers changed from: protected */
    public String getDisplayLevel() {
        return "Casual";
    }

    /* access modifiers changed from: protected */
    public int calculateNewGridSize() {
        return 3;
    }

    /* access modifiers changed from: protected */
    public void loadImage() {
        super.loadImage();
        this.m_hGame.lockScreen();
        setImage(this.m_hPuzzle.getPuzzleImageSelected().getImage(this.m_hActivity));
    }

    /* access modifiers changed from: protected */
    public ScreenPlayBase.ResultData getResultData() {
        PuzzleImage hPuzzleImage = this.m_hPuzzle.getPuzzleImageSelected();
        ScreenPlayBase.ResultData hResultData = new ScreenPlayBase.ResultData(1);
        hResultData.sPuzzleId = this.m_hPuzzle.getId();
        hResultData.iImageIndex = hPuzzleImage.getIndex();
        hResultData.sPuzzleImageInfo = hPuzzleImage.getInfo(this.m_hActivity);
        hResultData.sMovesNameRecord = hPuzzleImage.getBestMovesName();
        hResultData.iMovesRecord = hPuzzleImage.getBestMoves();
        hResultData.sTimeInMsNameRecord = hPuzzleImage.getBestTimeInMsName();
        hResultData.lTimeInMsRecord = hPuzzleImage.getBestTimeInMs();
        return hResultData;
    }

    /* access modifiers changed from: protected */
    public void dialogResultDismiss(ScreenPlayBase.ResultData hResultData) {
        this.m_hDialogManager.dismiss();
        startLevel();
    }

    /* access modifiers changed from: protected */
    public void onBackPress() {
        if (this.m_hPuzzle == null) {
            throw new RuntimeException();
        }
        this.m_hDialogManager.showLeavePuzzle(3);
    }
}
