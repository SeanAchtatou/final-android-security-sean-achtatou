package screen;

import android.app.Activity;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import cfg.Option;
import game.IGame;
import res.ResDimens;
import res.ResString;
import view.ButtonContainer;
import view.TitleView;

public final class ScreenPuzzleNotRegistered extends ScreenPuzzleBase {
    private static final int BUTTON_ID_OK = 0;
    private final TextView m_tvText;

    public ScreenPuzzleNotRegistered(Activity hActivity, IGame hGame) {
        super(13, hActivity, hGame);
        DisplayMetrics hMetrics = getResources().getDisplayMetrics();
        TitleView hTitle = new TitleView(hActivity, hMetrics);
        hTitle.setTitle(ResString.screen_puzzle_not_registered_title);
        addView(hTitle);
        RelativeLayout vgContent = new RelativeLayout(hActivity);
        vgContent.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
        addView(vgContent);
        ScrollView svScrollView = new ScrollView(hActivity);
        RelativeLayout.LayoutParams hScrollViewParams = new RelativeLayout.LayoutParams(-1, -2);
        hScrollViewParams.addRule(13);
        svScrollView.setLayoutParams(hScrollViewParams);
        vgContent.addView(svScrollView);
        this.m_tvText = new TextView(hActivity);
        this.m_tvText.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.m_tvText.setGravity(17);
        this.m_tvText.setTextColor(-1);
        this.m_tvText.setTextSize(1, 20.0f);
        this.m_tvText.setTypeface(Typeface.DEFAULT, 1);
        this.m_tvText.setShadowLayer(0.5f, 0.0f, 0.0f, Option.HINT_COLOR_DEFAULT);
        int iDip20 = ResDimens.getDip(hMetrics, 20);
        this.m_tvText.setPadding(iDip20, iDip20, iDip20, iDip20);
        this.m_tvText.setText(ResString.screen_puzzle_not_registered_text);
        svScrollView.addView(this.m_tvText);
        ButtonContainer vgButtonContainer = new ButtonContainer(hActivity);
        vgButtonContainer.createButton(0, "btn_img_ok", this.m_hOnClick);
        addView(vgButtonContainer);
    }

    public void onShow() {
        super.onShow();
        this.m_tvText.startAnimation(this.m_hAnimIn);
    }

    /* access modifiers changed from: protected */
    public void onClick(int iViewId) {
        super.onClick(iViewId);
        switch (iViewId) {
            case 0:
                break;
            default:
                throw new RuntimeException("Invalid view id");
        }
        this.m_hGame.quit();
    }
}
