package screen;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.widget.LinearLayout;
import android.widget.TextView;
import cfg.Option;
import data.MyPuzzle;
import data.MyPuzzleImage;
import game.IGame;
import helper.BitmapHelper;
import res.ResDimens;
import res.ResString;
import view.ButtonContainer;
import view.TitleView;

public final class ScreenMyPuzzleImageGridSize extends ScreenBase {
    private static final int BUTTON_ID_CANCEL = 0;
    private static final int BUTTON_ID_NEXT_GRID_SIZE = 2;
    private static final int BUTTON_ID_OK = 3;
    private static final int BUTTON_ID_PREV_GRID_SIZE = 1;
    private static final int MAX_GRID_SIZE = 8;
    private static final int MIN_GRID_SIZE = 3;
    /* access modifiers changed from: private */
    public Bitmap m_hImage;
    private MyPuzzle m_hMyPuzzle;
    private int m_iGridSize = 3;
    private final TextView m_tvGridSize;

    public ScreenMyPuzzleImageGridSize(Activity hActivity, IGame hGame) {
        super(12, hActivity, hGame);
        DisplayMetrics hMetrics = getResources().getDisplayMetrics();
        TitleView hTitle = new TitleView(hActivity, hMetrics);
        hTitle.setTitle(ResString.screen_my_puzzle_image_grid_size_title);
        addView(hTitle);
        LinearLayout vgContent = new LinearLayout(hActivity);
        vgContent.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
        vgContent.setOrientation(1);
        vgContent.setGravity(17);
        addView(vgContent);
        TextView tvText = getTextView(hActivity, hMetrics);
        tvText.setText(ResString.screen_my_puzzle_image_grid_size_text);
        vgContent.addView(tvText);
        this.m_tvGridSize = getTextView(hActivity, hMetrics);
        vgContent.addView(this.m_tvGridSize);
        ButtonContainer vgButtonContainer = new ButtonContainer(hActivity);
        vgButtonContainer.createButton(0, "btn_img_cancel", this.m_hOnClick);
        vgButtonContainer.createButton(1, "btn_img_arrow_left", this.m_hOnClick);
        vgButtonContainer.createButton(2, "btn_img_arrow_right", this.m_hOnClick);
        vgButtonContainer.createButton(3, "btn_img_ok", this.m_hOnClick);
        addView(vgButtonContainer);
    }

    private TextView getTextView(Context hContext, DisplayMetrics hMetrics) {
        TextView tvText = new TextView(hContext);
        tvText.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        tvText.setGravity(17);
        tvText.setTextColor(-1);
        tvText.setTextSize(1, 20.0f);
        tvText.setTypeface(Typeface.DEFAULT, 1);
        tvText.setShadowLayer(0.5f, 0.0f, 0.0f, Option.HINT_COLOR_DEFAULT);
        int iDip20 = ResDimens.getDip(hMetrics, 20);
        tvText.setPadding(iDip20, iDip20, iDip20, iDip20);
        return tvText;
    }

    public boolean beforeShow(int iComeFromScreenId, boolean bReset, Object hData) {
        super.beforeShow(iComeFromScreenId, bReset, hData);
        if (hData == null) {
            throw new RuntimeException("No data");
        } else if (!(hData instanceof MyPuzzle)) {
            throw new RuntimeException("Invalid type");
        } else {
            this.m_hMyPuzzle = (MyPuzzle) hData;
            setGridSizeText(3);
            return true;
        }
    }

    public void onShow() {
        super.onShow();
        this.m_hGame.lockScreen();
    }

    public void onActivityResult(int iRequestCode, int iResultCode, Intent hData) {
        super.onActivityResult(iRequestCode, iResultCode, hData);
        if (this.m_bIsCurrentScreen && !createImage(hData)) {
            this.m_hGame.switchScreen(11);
        }
    }

    private boolean createImage(Intent hData) {
        if (hData == null) {
            return false;
        }
        Uri hUri = hData.getData();
        if (hUri == null) {
            return false;
        }
        Cursor hCursor = this.m_hActivity.getContentResolver().query(hUri, new String[]{"_data"}, null, null, null);
        if (hCursor == null) {
            return false;
        }
        if (!hCursor.moveToFirst()) {
            return false;
        }
        final String sImageFilePath = hCursor.getString(0);
        hCursor.close();
        new Thread(new Runnable() {
            public void run() {
                ScreenMyPuzzleImageGridSize.this.m_hImage = BitmapHelper.createBitmapFromImageFile(sImageFilePath, 480);
                ScreenMyPuzzleImageGridSize.this.m_hHandler.post(new Runnable() {
                    public void run() {
                        if (ScreenMyPuzzleImageGridSize.this.m_hImage != null) {
                            ScreenMyPuzzleImageGridSize.this.m_hGame.unlockScreen();
                        } else {
                            ScreenMyPuzzleImageGridSize.this.m_hGame.switchScreen(11);
                        }
                    }
                });
            }
        }).start();
        return true;
    }

    private void setGridSizeText(int iGridSize) {
        this.m_iGridSize = iGridSize;
        this.m_tvGridSize.setText(String.valueOf(iGridSize) + " x " + iGridSize);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: game.IGame.switchScreen(int, boolean):void
     arg types: [int, int]
     candidates:
      game.IGame.switchScreen(int, java.lang.Object):void
      game.IGame.switchScreen(int, boolean):void */
    /* access modifiers changed from: protected */
    public void onClick(int iViewId) {
        int i;
        int i2;
        super.onClick(iViewId);
        switch (iViewId) {
            case 0:
                this.m_hGame.switchScreen(11);
                return;
            case 1:
                if (this.m_iGridSize > 3) {
                    i2 = this.m_iGridSize - 1;
                } else {
                    i2 = 8;
                }
                setGridSizeText(i2);
                this.m_hGame.unlockScreen();
                return;
            case 2:
                if (this.m_iGridSize < 8) {
                    i = this.m_iGridSize + 1;
                } else {
                    i = 3;
                }
                setGridSizeText(i);
                this.m_hGame.unlockScreen();
                return;
            case 3:
                MyPuzzleImage hMyPuzzleImage = MyPuzzleImage.createMyPuzzleImage(this.m_hMyPuzzle.getId(), this.m_hImage, this.m_iGridSize);
                if (hMyPuzzleImage == null) {
                    this.m_hGame.unlockScreen();
                    return;
                }
                hMyPuzzleImage.createThumbnail(this.m_hImage, ResDimens.getDip(getResources().getDisplayMetrics(), 96));
                this.m_hMyPuzzle.add(hMyPuzzleImage);
                this.m_hMyPuzzle.saveToSD();
                this.m_hGame.switchScreen(11, true);
                return;
            default:
                throw new RuntimeException("Invalid view id");
        }
    }
}
