package eula;

import android.app.Activity;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import cfg.Option;
import helper.Global;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import res.Res;
import res.ResDimens;
import res.ResString;
import view.ButtonContainer;
import view.TitleView;

public final class Eula {
    private static final int BUTTON_ID_CANCEL = 0;
    private static final int BUTTON_ID_OK = 1;
    private static final int EULA_VERSION = 1;
    private static final int EULA_VERSION_REFUSED = -1;
    private static final String PREF_FILE_EULA = "pref_file_eula";
    private static final String PREF_KEY_EULA_VERSION_ACCEPTED = "PREF_KEY_EULA_VERSION_ACCEPTED";

    public static void showEula(Activity hActivity) {
        SharedPreferences hPref = hActivity.getSharedPreferences(PREF_FILE_EULA, 0);
        if (1 != hPref.getInt(PREF_KEY_EULA_VERSION_ACCEPTED, -1)) {
            final DialogEula hDialogEula = new DialogEula(hActivity, hPref);
            new Handler().post(new Runnable() {
                public void run() {
                    DialogEula.this.show();
                }
            });
        }
    }

    private static final class DialogEula extends Dialog {
        /* access modifiers changed from: private */
        public final Activity m_hActivity;
        private final View.OnClickListener m_hOnClick = new View.OnClickListener() {
            public void onClick(View v) {
                switch (v.getId()) {
                    case 0:
                        DialogEula.this.m_hPref.edit().putInt(Eula.PREF_KEY_EULA_VERSION_ACCEPTED, -1).commit();
                        DialogEula.this.dismiss();
                        DialogEula.this.m_hActivity.finish();
                        return;
                    case 1:
                        DialogEula.this.m_hPref.edit().putInt(Eula.PREF_KEY_EULA_VERSION_ACCEPTED, 1).commit();
                        DialogEula.this.dismiss();
                        return;
                    default:
                        throw new RuntimeException("Invalid view id");
                }
            }
        };
        /* access modifiers changed from: private */
        public final SharedPreferences m_hPref;

        public DialogEula(Activity hActivity, SharedPreferences hPref) {
            super(hActivity);
            this.m_hActivity = hActivity;
            this.m_hPref = hPref;
            setCancelable(false);
            requestWindowFeature(1);
            RelativeLayout relativeLayout = new RelativeLayout(hActivity);
            relativeLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            ImageView imageView = new ImageView(hActivity);
            imageView.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            imageView.setImageResource(Res.drawable(hActivity, "background"));
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            relativeLayout.addView(imageView);
            LinearLayout linearLayout = new LinearLayout(hActivity);
            linearLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            linearLayout.setOrientation(1);
            linearLayout.setGravity(17);
            relativeLayout.addView(linearLayout);
            DisplayMetrics hMetrics = hActivity.getResources().getDisplayMetrics();
            TitleView titleView = new TitleView(hActivity, hMetrics);
            titleView.setTitle(ResString.eula_title);
            linearLayout.addView(titleView);
            WebView webView = new WebView(hActivity);
            webView.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
            int iDip10 = ResDimens.getDip(hMetrics, 10);
            webView.setPadding(iDip10, iDip10, iDip10, iDip10);
            webView.setBackgroundColor(Option.HINT_COLOR_DEFAULT);
            webView.loadData(getEulaText(), "text/html", "UTF-8");
            linearLayout.addView(webView);
            ButtonContainer buttonContainer = new ButtonContainer(hActivity);
            buttonContainer.createButton(0, "btn_img_cancel", this.m_hOnClick);
            buttonContainer.createButton(1, "btn_img_ok", this.m_hOnClick);
            linearLayout.addView(buttonContainer);
            Window hWindow = getWindow();
            hWindow.setBackgroundDrawable(new ColorDrawable(0));
            WindowManager.LayoutParams hLayoutParams = hWindow.getAttributes();
            hLayoutParams.flags |= 1024;
            hLayoutParams.width = -1;
            hLayoutParams.height = -1;
            hWindow.setAttributes(hLayoutParams);
            hWindow.setContentView(relativeLayout);
        }

        private final String getEulaText() {
            IOException e;
            String sFilePath = "/" + Eula.class.getPackage().getName() + "/eula";
            StringBuffer sbEula = new StringBuffer();
            BufferedReader hReader = null;
            try {
                BufferedReader hReader2 = new BufferedReader(new InputStreamReader(Eula.class.getResourceAsStream(sFilePath)));
                while (true) {
                    try {
                        String sLine = hReader2.readLine();
                        if (sLine == null) {
                            Global.closeReader(hReader2);
                            return sbEula.toString();
                        }
                        sbEula.append(sLine).append("\n");
                    } catch (IOException e2) {
                        e = e2;
                        hReader = hReader2;
                    } catch (Throwable th) {
                        th = th;
                        hReader = hReader2;
                        Global.closeReader(hReader);
                        throw th;
                    }
                }
            } catch (IOException e3) {
                e = e3;
                try {
                    throw new RuntimeException(e);
                } catch (Throwable th2) {
                    th = th2;
                    Global.closeReader(hReader);
                    throw th;
                }
            }
        }
    }
}
