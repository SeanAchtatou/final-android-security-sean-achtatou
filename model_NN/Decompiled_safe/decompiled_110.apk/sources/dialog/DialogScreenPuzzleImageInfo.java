package dialog;

import android.app.Activity;
import android.content.DialogInterface;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import res.ResDimens;
import res.ResString;
import screen.ScreenPlayBase;
import view.ButtonContainer;
import view.TitleView;

public final class DialogScreenPuzzleImageInfo extends DialogScreenBase {
    private static final int BUTTON_ID_OK = 0;
    private final View.OnClickListener m_hOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case 0:
                    DialogScreenPuzzleImageInfo.this.m_hDialogManager.dismiss();
                    if (DialogScreenPuzzleImageInfo.this.m_hResultData != null && DialogScreenPuzzleImageInfo.this.m_hOnDismiss != null) {
                        DialogScreenPuzzleImageInfo.this.m_hHandler.post(new Runnable() {
                            public void run() {
                                DialogScreenPuzzleImageInfo.this.m_hDialogManager.showPuzzleSolved(DialogScreenPuzzleImageInfo.this.m_hResultData, DialogScreenPuzzleImageInfo.this.m_hOnDismiss);
                            }
                        });
                        return;
                    }
                    return;
                default:
                    throw new RuntimeException("Invalid view id");
            }
        }
    };
    /* access modifiers changed from: private */
    public DialogInterface.OnDismissListener m_hOnDismiss;
    /* access modifiers changed from: private */
    public ScreenPlayBase.ResultData m_hResultData;
    private final TextView m_tvInfo;

    public DialogScreenPuzzleImageInfo(Activity hActivity, DialogManager hDialogManager) {
        super(hActivity, hDialogManager);
        DisplayMetrics hMetrics = getResources().getDisplayMetrics();
        TitleView hTitle = new TitleView(hActivity, hMetrics);
        hTitle.setTitle(ResString.dialog_screen_puzzle_image_info_title);
        addView(hTitle);
        RelativeLayout vgContent = new RelativeLayout(hActivity);
        vgContent.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
        addView(vgContent);
        ScrollView svScrollView = new ScrollView(hActivity);
        RelativeLayout.LayoutParams hScrollViewParams = new RelativeLayout.LayoutParams(-1, -2);
        hScrollViewParams.addRule(13);
        svScrollView.setLayoutParams(hScrollViewParams);
        vgContent.addView(svScrollView);
        this.m_tvInfo = new TextView(hActivity);
        this.m_tvInfo.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.m_tvInfo.setGravity(17);
        this.m_tvInfo.setTextColor(-1);
        this.m_tvInfo.setTextSize(1, 18.0f);
        int iDip20 = ResDimens.getDip(hMetrics, 20);
        this.m_tvInfo.setPadding(iDip20, iDip20, iDip20, iDip20);
        this.m_tvInfo.setAutoLinkMask(15);
        svScrollView.addView(this.m_tvInfo);
        ButtonContainer vgButtonContainer = new ButtonContainer(hActivity);
        vgButtonContainer.createButton(0, "btn_img_ok", this.m_hOnClick);
        addView(vgButtonContainer);
    }

    public void onShow(String sImageInfo, ScreenPlayBase.ResultData hResultData, DialogInterface.OnDismissListener hOnDismiss) {
        this.m_hResultData = hResultData;
        this.m_hOnDismiss = hOnDismiss;
        this.m_tvInfo.setText(sImageInfo);
    }

    public boolean unlockScreenOnDismiss() {
        if (this.m_hResultData == null || this.m_hOnDismiss == null) {
            return true;
        }
        return false;
    }

    public void cleanUp() {
        super.cleanUp();
        this.m_tvInfo.setText((CharSequence) null);
    }
}
