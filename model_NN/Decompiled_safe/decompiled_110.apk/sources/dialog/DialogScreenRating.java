package dialog;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cfg.Option;
import res.Res;
import res.ResDimens;
import res.ResString;
import screen.ScreenBase;
import view.ButtonContainer;
import view.TitleView;

public final class DialogScreenRating extends DialogScreenBase {
    private static final int BUTTON_ID_CANCEL = 0;
    private static final int BUTTON_ID_OK = 1;
    private static final String PREF_KEY_SHOW_RATING_DIALOG = "PREF_KEY_SHOW_RATING_DIALOG";
    /* access modifiers changed from: private */
    public final Activity m_hActivity;
    private final View.OnClickListener m_hOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case 0:
                    DialogScreenRating.this.m_hDialogManager.dismiss();
                    return;
                case 1:
                    Intent hMarketIntent = new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + DialogScreenRating.this.m_hActivity.getPackageName()));
                    if (ScreenBase.isIntentAvailable(DialogScreenRating.this.m_hActivity, hMarketIntent)) {
                        DialogScreenRating.this.m_hActivity.startActivity(hMarketIntent);
                        DialogScreenRating.this.m_hDialogManager.dismiss();
                        PreferenceManager.getDefaultSharedPreferences(DialogScreenRating.this.m_hActivity).edit().putBoolean(DialogScreenRating.PREF_KEY_SHOW_RATING_DIALOG, false).commit();
                        return;
                    }
                    return;
                default:
                    throw new RuntimeException("Invalid view id");
            }
        }
    };

    public DialogScreenRating(Activity hActivity, DialogManager hDialogManager) {
        super(hActivity, hDialogManager);
        this.m_hActivity = hActivity;
        DisplayMetrics hMetrics = getResources().getDisplayMetrics();
        TitleView hTitle = new TitleView(hActivity, hMetrics);
        hTitle.setTitle(ResString.dialog_screen_rating_title);
        addView(hTitle);
        RelativeLayout vgTextContainer = new RelativeLayout(hActivity);
        vgTextContainer.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
        addView(vgTextContainer);
        TextView tvText = new TextView(hActivity);
        RelativeLayout.LayoutParams hTextParams = new RelativeLayout.LayoutParams(-1, -2);
        hTextParams.addRule(13);
        tvText.setLayoutParams(hTextParams);
        tvText.setGravity(17);
        tvText.setTextColor(-1);
        tvText.setTextSize(1, 20.0f);
        tvText.setShadowLayer(0.5f, 0.0f, 0.0f, Option.HINT_COLOR_DEFAULT);
        tvText.setTypeface(Typeface.DEFAULT, 1);
        int iDip20 = ResDimens.getDip(hMetrics, 20);
        tvText.setPadding(iDip20, iDip20, iDip20, iDip20);
        tvText.setText(String.format(ResString.dialog_screen_rating_text, hActivity.getString(Res.string(hActivity, "app_name"))));
        vgTextContainer.addView(tvText);
        ButtonContainer vgButtonContainer = new ButtonContainer(hActivity);
        vgButtonContainer.createButton(0, "btn_img_cancel", this.m_hOnClick);
        vgButtonContainer.createButton(1, "btn_img_ok", this.m_hOnClick);
        addView(vgButtonContainer);
    }

    public static synchronized boolean shouldShowRatingDialog(Context hContext) {
        boolean z;
        synchronized (DialogScreenRating.class) {
            z = PreferenceManager.getDefaultSharedPreferences(hContext).getBoolean(PREF_KEY_SHOW_RATING_DIALOG, true);
        }
        return z;
    }
}
