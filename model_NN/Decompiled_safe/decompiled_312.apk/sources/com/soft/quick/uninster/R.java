package com.soft.quick.uninster;

public final class R {

    public static final class array {
        public static final int sort_types = 2130968576;
    }

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771971;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771972;
        public static final int secondaryTextColor = 2130771970;
    }

    public static final class drawable {
        public static final int icon = 2130837504;
        public static final int sd = 2130837505;
        public static final int settings = 2130837506;
        public static final int sort = 2130837507;
    }

    public static final class id {
        public static final int adview = 2131099660;
        public static final int clearBtn = 2131099658;
        public static final int date = 2131099654;
        public static final int entryList = 2131099659;
        public static final int icon = 2131099651;
        public static final int moreBtn = 2131099650;
        public static final int name = 2131099653;
        public static final int rateBtn = 2131099649;
        public static final int sd = 2131099652;
        public static final int searchBar = 2131099656;
        public static final int searchText = 2131099657;
        public static final int size = 2131099655;
        public static final int versionText = 2131099648;
    }

    public static final class layout {
        public static final int about = 2130903040;
        public static final int entry = 2130903041;
        public static final int main_screen = 2130903042;
    }

    public static final class string {
        public static final int about = 2131034115;
        public static final int aboutDesc = 2131034121;
        public static final int app_name = 2131034112;
        public static final int clear = 2131034113;
        public static final int notificationString = 2131034116;
        public static final int preference = 2131034119;
        public static final int preferences = 2131034120;
        public static final int showNotification = 2131034117;
        public static final int showNotificationDesc = 2131034118;
        public static final int sort = 2131034114;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
    }
}
