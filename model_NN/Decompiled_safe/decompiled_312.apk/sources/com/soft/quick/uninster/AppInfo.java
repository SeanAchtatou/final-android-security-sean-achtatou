package com.soft.quick.uninster;

import android.content.pm.ApplicationInfo;
import android.graphics.drawable.Drawable;

public class AppInfo {
    public String date;
    public Drawable drawable;
    public ApplicationInfo info;
    public String labelName;
    public boolean onSDCard = false;
    public String packageName;
    public String size;
    public int versionCode;
    public String versionName;
}
