package com.soft.quick.uninster;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class UninstallerPreference extends PreferenceActivity {
    private static final int DIALOG_ABOUT = 0;
    private NotificationManager notificationManager;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setPreferenceScreen(createPreferenceHierarchy());
        this.notificationManager = (NotificationManager) getSystemService("notification");
    }

    private PreferenceScreen createPreferenceHierarchy() {
        PreferenceScreen root = getPreferenceManager().createPreferenceScreen(this);
        CheckBoxPreference checkBoxPref = new CheckBoxPreference(this);
        checkBoxPref.setKey(Constants.PREF_SHOW_NOTIFICATION);
        checkBoxPref.setTitle((int) R.string.showNotification);
        checkBoxPref.setSummary((int) R.string.showNotificationDesc);
        checkBoxPref.setDefaultValue(false);
        checkBoxPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                if (Boolean.TRUE.equals(newValue)) {
                    UninstallerPreference.this.showNotification();
                    return true;
                }
                UninstallerPreference.this.clearNotification();
                return true;
            }
        });
        root.addPreference(checkBoxPref);
        Preference dlgPref = new Preference(this, null) {
            /* access modifiers changed from: protected */
            public void onClick() {
                UninstallerPreference.this.showDialog(0);
            }
        };
        dlgPref.setTitle((int) R.string.about);
        dlgPref.setSummary((int) R.string.aboutDesc);
        root.addPreference(dlgPref);
        return root;
    }

    /* access modifiers changed from: private */
    public void showNotification() {
        String name = getResources().getString(R.string.app_name);
        String strNotify = getResources().getString(R.string.notificationString);
        Notification notification = new Notification();
        notification.icon = R.drawable.icon;
        notification.flags = 34;
        Intent notifyIntent = new Intent(this, Uninstaller.class);
        notifyIntent.setFlags(270532608);
        notification.setLatestEventInfo(this, name, strNotify, PendingIntent.getActivity(this, 0, notifyIntent, 0));
        this.notificationManager.notify(1, notification);
    }

    /* access modifiers changed from: private */
    public void clearNotification() {
        this.notificationManager.cancel(1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        if (id != 0) {
            return null;
        }
        View about = LayoutInflater.from(this).inflate((int) R.layout.about, (ViewGroup) null, false);
        TextView versionText = (TextView) about.findViewById(R.id.versionText);
        String appName = null;
        try {
            PackageInfo info = getPackageManager().getPackageInfo(Constants.FULL_PACKAGE_NAME, 0);
            appName = getPackageManager().getApplicationLabel(info.applicationInfo).toString();
            versionText.setText("Ver. " + info.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(Constants.TAG, "package not found");
        }
        ((Button) about.findViewById(R.id.rateBtn)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setData(Uri.parse("market://search?q=pname:com.soft.quick.uninster"));
                UninstallerPreference.this.startActivity(intent);
            }
        });
        ((Button) about.findViewById(R.id.moreBtn)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setData(Uri.parse("http://market.android.com/search?q=pub:\"com.soft.quick.uninster\""));
                UninstallerPreference.this.startActivity(intent);
            }
        });
        return new AlertDialog.Builder(this).setIcon(17301659).setTitle("About " + appName).setView(about).create();
    }
}
