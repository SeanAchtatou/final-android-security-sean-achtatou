package com.soft.quick.uninster.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import com.soft.quick.uninster.AppInfo;
import com.soft.quick.uninster.Constants;
import com.soft.quick.uninster.Uninstaller;
import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

public class UninstDAO {
    private SQLiteOpenHelper helper;

    public UninstDAO(SQLiteOpenHelper helper2) {
        this.helper = helper2;
    }

    public Map<String, AppInfo> getAppInfos() {
        Map<String, AppInfo> apps = new HashMap<>();
        Cursor cursor = this.helper.getWritableDatabase().query(Constants.TABLE_APP, null, null, null, null, null, null);
        try {
            if (cursor.getCount() == 0) {
                cursor.close();
            } else {
                if (cursor.moveToFirst()) {
                    do {
                        AppInfo info = createAppInfo(cursor);
                        apps.put(info.packageName, info);
                        byte[] bytes = cursor.getBlob(cursor.getColumnIndex(Constants.APPColumns.ICON));
                        if (bytes != null) {
                            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                            if (bitmap != null) {
                                Uninstaller.iconsMapper.put(info.packageName, new BitmapDrawable(bitmap));
                            }
                        }
                    } while (cursor.moveToNext());
                }
                cursor.close();
            }
        } catch (Throwable th) {
            cursor.close();
            throw th;
        }
        return apps;
    }

    public void updateAppInfo(AppInfo appInfo, Drawable drawable) {
        ContentValues values = new ContentValues();
        values.put(Constants.APPColumns.DATE, appInfo.date);
        values.put(Constants.APPColumns.NAME, appInfo.labelName);
        values.put(Constants.APPColumns.VERSION_CODE, Integer.valueOf(appInfo.versionCode));
        values.put(Constants.APPColumns.SIZE, appInfo.size);
        values.put(Constants.APPColumns.VERSION_NAME, appInfo.versionName);
        if (drawable instanceof BitmapDrawable) {
            saveIcon(values, (BitmapDrawable) drawable);
        }
        this.helper.getWritableDatabase().update(Constants.TABLE_APP, values, "package=\"" + appInfo.packageName + "\"", null);
    }

    public void addAppInfo(AppInfo appInfo, Drawable drawable) {
        ContentValues values = new ContentValues();
        values.put(Constants.APPColumns.PACKAGE, appInfo.packageName);
        values.put(Constants.APPColumns.DATE, appInfo.date);
        values.put(Constants.APPColumns.NAME, appInfo.labelName);
        values.put(Constants.APPColumns.VERSION_CODE, Integer.valueOf(appInfo.versionCode));
        values.put(Constants.APPColumns.SIZE, appInfo.size);
        values.put(Constants.APPColumns.VERSION_NAME, appInfo.versionName);
        if (drawable instanceof BitmapDrawable) {
            saveIcon(values, (BitmapDrawable) drawable);
        }
        this.helper.getWritableDatabase().insert(Constants.TABLE_APP, Constants.APPColumns.PACKAGE, values);
    }

    private void saveIcon(ContentValues values, BitmapDrawable drawable) {
        try {
            values.put(Constants.APPColumns.ICON, getBitmapAsByteArray(drawable.getBitmap()));
        } catch (Throwable th) {
            Log.e(Constants.TAG, "Error saving image: ", th);
        }
    }

    private static byte[] getBitmapAsByteArray(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, outputStream);
        return outputStream.toByteArray();
    }

    public void deleteAppInfo(AppInfo appInfo) {
        this.helper.getWritableDatabase().delete(Constants.TABLE_APP, "package=\"" + appInfo.packageName + "\"", null);
    }

    private AppInfo createAppInfo(Cursor cursor) {
        AppInfo info = new AppInfo();
        info.date = cursor.getString(cursor.getColumnIndex(Constants.APPColumns.DATE));
        info.labelName = cursor.getString(cursor.getColumnIndex(Constants.APPColumns.NAME));
        info.packageName = cursor.getString(cursor.getColumnIndex(Constants.APPColumns.PACKAGE));
        info.size = cursor.getString(cursor.getColumnIndex(Constants.APPColumns.SIZE));
        info.versionCode = cursor.getInt(cursor.getColumnIndex(Constants.APPColumns.VERSION_CODE));
        info.versionName = cursor.getString(cursor.getColumnIndex(Constants.APPColumns.VERSION_NAME));
        return info;
    }
}
