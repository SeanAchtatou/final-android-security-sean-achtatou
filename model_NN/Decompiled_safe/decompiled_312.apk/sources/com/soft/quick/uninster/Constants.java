package com.soft.quick.uninster;

public interface Constants {
    public static final String DATABASE_NAME = "hexise.uninst.db";
    public static final int DATE_ASCEND = 4;
    public static final int DATE_DESCEND = 5;
    public static final String FULL_PACKAGE_NAME = "com.soft.quick.uninster";
    public static final int NAME_ASCEND = 0;
    public static final int NAME_DESCEND = 1;
    public static final int NOTIFY_ID = 1;
    public static final String PREF_FILE = "uninst_pref.xml";
    public static final String PREF_INIT = "initDatabaseV2";
    public static final String PREF_SHOW_NOTIFICATION = "show_notification";
    public static final String PREF_SORT = "sort";
    public static final int SIZE_ASCEND = 2;
    public static final int SIZE_DESCEND = 3;
    public static final String TABLE_APP = "apps";
    public static final String TAG = "hexise.uninst";

    public interface APPColumns {
        public static final String DATE = "date";
        public static final String ICON = "icon";
        public static final String NAME = "name";
        public static final String PACKAGE = "package";
        public static final String SIZE = "size";
        public static final String VERSION_CODE = "versionCode";
        public static final String VERSION_NAME = "versionName";
    }
}
