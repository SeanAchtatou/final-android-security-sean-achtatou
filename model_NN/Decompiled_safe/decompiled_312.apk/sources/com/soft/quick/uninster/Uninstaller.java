package com.soft.quick.uninster;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.admob.android.ads.AdView;
import com.soft.quick.uninster.db.UninstDAO;
import com.soft.quick.uninster.db.UninstDBHelper;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Uninstaller extends Activity {
    private static final int MSG_FINISHED = 0;
    private static final int MSG_INCREASE_PROGRESS = 2;
    private static final int MSG_LOAD_APP = 1;
    /* access modifiers changed from: private */
    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
    public static Map<String, Drawable> iconsMapper = new HashMap();
    /* access modifiers changed from: private */
    public POJOListAdapter<AppInfo> adapter;
    /* access modifiers changed from: private */
    public List<AppInfo> appInfos;
    private Comparator<AppInfo> comparator = new Comparator<AppInfo>() {
        public int compare(AppInfo info1, AppInfo info2) {
            switch (Uninstaller.this.sort_type) {
                case 1:
                    return info2.labelName.toLowerCase().compareTo(info1.labelName.toLowerCase());
                case 2:
                    return Long.decode(info1.size).compareTo(Long.decode(info2.size));
                case 3:
                    return Long.decode(info2.size).compareTo(Long.decode(info1.size));
                case 4:
                case Constants.DATE_DESCEND:
                    try {
                        Date date1 = Uninstaller.dateFormat.parse(info1.date);
                        Date date2 = Uninstaller.dateFormat.parse(info2.date);
                        if (Uninstaller.this.sort_type == 4) {
                            return date1.compareTo(date2);
                        }
                        return date2.compareTo(date1);
                    } catch (Throwable th) {
                        Log.e(Constants.TAG, "Error when compare date: ", th);
                        return 0;
                    }
                default:
                    return info1.labelName.toLowerCase().compareTo(info2.labelName.toLowerCase());
            }
        }
    };
    private UninstDAO dao;
    /* access modifiers changed from: private */
    public List<AppInfo> filteredInfos;
    /* access modifiers changed from: private */
    public Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    try {
                        if (Uninstaller.this.progressDialog != null && Uninstaller.this.progressDialog.isShowing()) {
                            Uninstaller.this.progressDialog.dismiss();
                            Uninstaller.this.progressDialog = null;
                        }
                    } catch (Throwable th) {
                        Log.e(Constants.TAG, "Error when close progress dialog", th);
                    }
                    Uninstaller.this.intializeUI();
                    return;
                case 1:
                    if (Uninstaller.this.progressDialog != null && Uninstaller.this.progressDialog.isShowing()) {
                        Uninstaller.this.progressDialog.setMax(msg.arg1);
                        return;
                    }
                    return;
                case 2:
                    if (Uninstaller.this.progressDialog != null && Uninstaller.this.progressDialog.isShowing()) {
                        Uninstaller.this.progressDialog.incrementProgressBy(1);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public UninstDBHelper helper;
    private boolean initUI = false;
    private ListView listView;
    /* access modifiers changed from: private */
    public PackageManager manager;
    private NotificationManager notificationManager;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    /* access modifiers changed from: private */
    public EditText searchText;
    /* access modifiers changed from: private */
    public int sort_type = 0;
    /* access modifiers changed from: private */
    public SharedPreferences sp;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main_screen);
        this.notificationManager = (NotificationManager) getSystemService("notification");
        showNotification();
        this.sp = getSharedPreferences(Constants.PREF_FILE, 0);
        this.sort_type = this.sp.getInt(Constants.PREF_SORT, 0);
        this.helper = new UninstDBHelper(this);
        this.dao = new UninstDAO(this.helper);
        this.manager = getPackageManager();
        this.searchText = (EditText) findViewById(R.id.searchText);
        this.listView = (ListView) findViewById(R.id.entryList);
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Uninstaller.this.startActivity(new Intent("android.intent.action.DELETE", Uri.parse("package:" + ((AppInfo) Uninstaller.this.filteredInfos.get(position)).packageName)));
            }
        });
        this.searchText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                if (Uninstaller.this.filteredInfos != null) {
                    String value = s.toString();
                    Uninstaller.this.filteredInfos.clear();
                    if (value.length() == 0) {
                        Uninstaller.this.filteredInfos.addAll(Uninstaller.this.appInfos);
                    } else {
                        for (AppInfo info : Uninstaller.this.appInfos) {
                            if (info.labelName.toLowerCase().contains(value.toLowerCase())) {
                                Uninstaller.this.filteredInfos.add(info);
                            }
                        }
                    }
                    Uninstaller.this.adapter.setInput(Uninstaller.this.filteredInfos);
                    Uninstaller.this.adapter.notifyDataSetChanged();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        ((Button) findViewById(R.id.clearBtn)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Uninstaller.this.searchText.setText((CharSequence) null);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.sp.getBoolean(Constants.PREF_INIT, false)) {
            refreshVisuals();
        } else {
            this.progressDialog = new ProgressDialog(this);
            this.progressDialog.setTitle("Application Initialization");
            this.progressDialog.setMessage("Setting up application cache. This dialog only appears once.");
            this.progressDialog.setProgressStyle(1);
            this.progressDialog.setCancelable(false);
            this.progressDialog.show();
            new Thread() {
                public void run() {
                    Uninstaller.this.helper.destoryExistDB(Uninstaller.this.helper.getWritableDatabase());
                    Uninstaller.this.initializeAppInfos(true);
                    Uninstaller.this.sp.edit().putBoolean(Constants.PREF_INIT, true).commit();
                    Message msg = new Message();
                    msg.what = 0;
                    Uninstaller.this.handler.sendMessage(msg);
                }
            }.start();
        }
        ((AdView) findViewById(R.id.adview)).requestFreshAd();
    }

    /* access modifiers changed from: private */
    public void refreshVisuals() {
        initializeAppInfos(false);
        intializeUI();
    }

    /* access modifiers changed from: private */
    public void intializeUI() {
        if (this.initUI) {
            this.adapter.setInput(this.filteredInfos);
            this.adapter.notifyDataSetChanged();
            return;
        }
        this.adapter = new POJOListAdapter<AppInfo>(this, R.layout.entry, this.filteredInfos) {
            private static final String KB = "KB";

            /* renamed from: com.soft.quick.uninster.Uninstaller$7$ViewHolder */
            final class ViewHolder {
                TextView dateText;
                ImageView iconView;
                TextView nameText;
                ImageView sdView;
                TextView sizeText;

                ViewHolder() {
                }
            }

            /* access modifiers changed from: protected */
            public View newView(ViewGroup parent, int position) {
                View view = super.newView(parent, position);
                ViewHolder holder = new ViewHolder();
                holder.nameText = (TextView) view.findViewById(R.id.name);
                holder.iconView = (ImageView) view.findViewById(R.id.icon);
                holder.sizeText = (TextView) view.findViewById(R.id.size);
                holder.dateText = (TextView) view.findViewById(R.id.date);
                holder.sdView = (ImageView) view.findViewById(R.id.sd);
                view.setTag(holder);
                return view;
            }

            /* access modifiers changed from: protected */
            public void bindView(View view, Context context, AppInfo info) {
                ViewHolder holder = (ViewHolder) view.getTag();
                String name = info.labelName;
                if (info.versionName != null && info.versionName.length() > 0) {
                    name = String.valueOf(name) + " " + info.versionName;
                }
                holder.nameText.setText(name);
                Drawable icon = Uninstaller.iconsMapper.get(info.packageName);
                if (icon == null) {
                    icon = Uninstaller.this.manager.getApplicationIcon(info.info);
                    Uninstaller.iconsMapper.put(info.packageName, icon);
                }
                holder.iconView.setImageDrawable(icon);
                holder.sizeText.setText(String.valueOf(info.size) + KB);
                holder.dateText.setText(info.date);
                if (info.onSDCard) {
                    holder.sdView.setVisibility(0);
                } else {
                    holder.sdView.setVisibility(8);
                }
            }
        };
        this.listView.setAdapter((ListAdapter) this.adapter);
        this.initUI = true;
        Toast.makeText(this, "Click Application to Uninstall.", 0).show();
    }

    /* access modifiers changed from: private */
    public void initializeAppInfos(boolean updateProgress) {
        List<PackageInfo> allPackages = this.manager.getInstalledPackages(0);
        List<PackageInfo> appPackages = new ArrayList<>();
        for (PackageInfo info : allPackages) {
            if ((info.applicationInfo.flags & 1) == 0) {
                appPackages.add(info);
            }
        }
        this.appInfos = getAppInfos(appPackages, updateProgress);
        Collections.sort(this.appInfos, this.comparator);
        this.filteredInfos = new ArrayList();
        if (this.searchText == null || this.searchText.getText().toString().length() <= 0) {
            this.filteredInfos.addAll(this.appInfos);
            return;
        }
        String value = this.searchText.getText().toString();
        for (AppInfo info2 : this.appInfos) {
            if (info2.labelName.toLowerCase().contains(value.toLowerCase())) {
                this.filteredInfos.add(info2);
            }
        }
    }

    private List<AppInfo> getAppInfos(List<PackageInfo> appPackages, boolean updateProgress) {
        iconsMapper.clear();
        if (updateProgress) {
            Message msg = new Message();
            msg.what = 1;
            msg.arg1 = appPackages.size();
            this.handler.sendMessage(msg);
        }
        Map<String, AppInfo> dbInfos = this.dao.getAppInfos();
        List<AppInfo> appInfos2 = new ArrayList<>();
        for (PackageInfo appPackage : appPackages) {
            AppInfo info = dbInfos.get(appPackage.packageName);
            if (info != null) {
                info.info = appPackage.applicationInfo;
                if (info.versionCode != appPackage.versionCode) {
                    updateAppInfo(info, appPackage);
                    Drawable drawable = this.manager.getApplicationIcon(info.info);
                    iconsMapper.put(info.packageName, drawable);
                    this.dao.updateAppInfo(info, drawable);
                }
            } else {
                info = new AppInfo();
                info.info = appPackage.applicationInfo;
                updateAppInfo(info, appPackage);
                Drawable drawable2 = this.manager.getApplicationIcon(info.info);
                iconsMapper.put(info.packageName, drawable2);
                this.dao.addAppInfo(info, drawable2);
            }
            if ((appPackage.applicationInfo.flags & 262144) != 0) {
                info.onSDCard = true;
            }
            appInfos2.add(info);
            if (updateProgress) {
                Message msg2 = new Message();
                msg2.what = 2;
                this.handler.sendMessage(msg2);
            }
        }
        Collection<AppInfo> deletedInfos = dbInfos.values();
        deletedInfos.removeAll(appInfos2);
        if (!deletedInfos.isEmpty()) {
            deleteAppInfos(deletedInfos);
        }
        return appInfos2;
    }

    private void deleteAppInfos(Collection<AppInfo> deletedInfos) {
        for (AppInfo info : deletedInfos) {
            this.dao.deleteAppInfo(info);
        }
    }

    private void updateAppInfo(AppInfo info, PackageInfo appPackage) {
        info.packageName = appPackage.packageName;
        info.versionCode = appPackage.versionCode;
        info.labelName = this.manager.getApplicationLabel(appPackage.applicationInfo).toString();
        File file = new File(appPackage.applicationInfo.sourceDir);
        info.size = Long.toString(file.length() / 1024);
        info.date = dateFormat.format(new Date(file.lastModified()));
        info.versionName = appPackage.versionName;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add((int) R.string.sort).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                new AlertDialog.Builder(Uninstaller.this).setTitle((int) R.string.sort).setSingleChoiceItems((int) R.array.sort_types, Uninstaller.this.sort_type, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        if (whichButton != Uninstaller.this.sort_type) {
                            Uninstaller.this.sort_type = whichButton;
                            Uninstaller.this.sp.edit().putInt(Constants.PREF_SORT, Uninstaller.this.sort_type).commit();
                            dialog.dismiss();
                            Uninstaller.this.refreshVisuals();
                            return;
                        }
                        dialog.dismiss();
                    }
                }).create().show();
                return false;
            }
        }).setIcon((int) R.drawable.sort);
        menu.add((int) R.string.preferences).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                Intent intent = new Intent();
                intent.setClass(Uninstaller.this, UninstallerPreference.class);
                Uninstaller.this.startActivity(intent);
                return false;
            }
        }).setIcon((int) R.drawable.settings);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.helper != null) {
            this.helper.close();
        }
    }

    private void showNotification() {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(Constants.PREF_SHOW_NOTIFICATION, false)) {
            String name = getResources().getString(R.string.app_name);
            String strNotify = getResources().getString(R.string.notificationString);
            Notification notification = new Notification();
            notification.icon = R.drawable.icon;
            notification.flags = 34;
            Intent notifyIntent = new Intent(this, Uninstaller.class);
            notifyIntent.setFlags(270532608);
            notification.setLatestEventInfo(this, name, strNotify, PendingIntent.getActivity(this, 0, notifyIntent, 0));
            this.notificationManager.notify(1, notification);
        }
    }
}
