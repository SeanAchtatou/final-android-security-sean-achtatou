package com.soft.quick.uninster.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.soft.quick.uninster.Constants;
import java.text.SimpleDateFormat;

public class UninstDBHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 2;
    private static final SimpleDateFormat newDateFormat = new SimpleDateFormat("MM/dd/yyyy");
    private static final SimpleDateFormat oldDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public UninstDBHelper(Context context) {
        super(context, Constants.DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 2);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS apps (package TEXT PRIMARY KEY,versionCode INTEGER,size TEXT,date TEXT,versionName TEXT,name TEXT,icon BLOB)");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion == 1) {
            try {
                Log.i(Constants.TAG, "Database migration from version 1.");
                Cursor cursor = db.query(Constants.TABLE_APP, new String[]{Constants.APPColumns.PACKAGE, Constants.APPColumns.DATE}, null, null, null, null, null);
                while (cursor.moveToNext()) {
                    String pkg = cursor.getString(cursor.getColumnIndex(Constants.APPColumns.PACKAGE));
                    String date = newDateFormat.format(oldDateFormat.parse(cursor.getString(cursor.getColumnIndex(Constants.APPColumns.DATE))));
                    ContentValues values = new ContentValues();
                    values.put(Constants.APPColumns.DATE, date);
                    db.update(Constants.TABLE_APP, values, "package=\"" + pkg + "\"", null);
                }
                cursor.close();
            } catch (Throwable th) {
                Log.e(Constants.TAG, "Error when migrate database from version 1:", th);
            }
        }
    }

    public void destoryExistDB(SQLiteDatabase db) {
        Log.w(Constants.TAG, "Destroying old data during upgrade.");
        db.execSQL("DROP TABLE IF EXISTS apps");
        onCreate(db);
    }
}
