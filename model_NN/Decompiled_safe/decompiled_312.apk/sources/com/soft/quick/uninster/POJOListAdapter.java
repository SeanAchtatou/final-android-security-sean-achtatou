package com.soft.quick.uninster;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.List;

public abstract class POJOListAdapter<T> extends BaseAdapter {
    protected List<T> items;
    protected Context mContext;
    protected LayoutInflater mInflater;
    protected int mLayout;

    /* access modifiers changed from: protected */
    public abstract void bindView(View view, Context context, T t);

    public POJOListAdapter(Context context, int layout) {
        this(context, layout, null);
    }

    public POJOListAdapter(Context context, int layout, List<T> items2) {
        this.mContext = context;
        this.items = items2;
        this.mLayout = layout;
        this.mInflater = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    public int getCount() {
        return this.items.size();
    }

    public void setInput(List<T> items2) {
        this.items = items2;
    }

    public T getItem(int position) {
        return this.items.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (position >= getCount()) {
            throw new IllegalStateException("list do not have any element at position " + position);
        }
        if (convertView == null) {
            view = newView(parent, position);
        } else {
            view = convertView;
        }
        bindView(view, this.mContext, getItem(position));
        return view;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public View newView(ViewGroup parent, int position) {
        return this.mInflater.inflate(this.mLayout, parent, false);
    }

    public List<T> getItems() {
        return this.items;
    }
}
