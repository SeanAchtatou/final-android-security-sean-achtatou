package cn.domob.android.ads;

final class b {
    b() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0045  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static boolean a(cn.domob.android.ads.DBHelper r7, java.lang.String r8, java.lang.String r9, java.util.Hashtable<java.lang.String, byte[]> r10) {
        /*
            r0 = 0
            r1 = 0
            android.net.Uri r2 = cn.domob.android.ads.DBHelper.b     // Catch:{ Exception -> 0x0070 }
            android.database.Cursor r1 = r7.b(r2, r9)     // Catch:{ Exception -> 0x0070 }
            if (r1 == 0) goto L_0x0049
            int r2 = r1.getCount()     // Catch:{ Exception -> 0x007a }
            if (r2 <= 0) goto L_0x0049
            r1.moveToFirst()     // Catch:{ Exception -> 0x007a }
            java.lang.String r2 = "_image"
            int r2 = r1.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x007a }
            byte[] r2 = r1.getBlob(r2)     // Catch:{ Exception -> 0x007a }
            if (r2 == 0) goto L_0x007f
            java.lang.String r3 = "DomobSDK"
            r4 = 3
            boolean r3 = android.util.Log.isLoggable(r3, r4)     // Catch:{ Exception -> 0x007a }
            if (r3 == 0) goto L_0x003c
            java.lang.String r3 = "DomobSDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x007a }
            java.lang.String r5 = "load image from cache:"
            r4.<init>(r5)     // Catch:{ Exception -> 0x007a }
            java.lang.StringBuilder r4 = r4.append(r9)     // Catch:{ Exception -> 0x007a }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x007a }
            android.util.Log.d(r3, r4)     // Catch:{ Exception -> 0x007a }
        L_0x003c:
            r10.put(r8, r2)     // Catch:{ Exception -> 0x007a }
            r0 = 1
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x0043:
            if (r0 == 0) goto L_0x0048
            r0.close()
        L_0x0048:
            return r1
        L_0x0049:
            java.lang.String r2 = "DomobSDK"
            r3 = 3
            boolean r2 = android.util.Log.isLoggable(r2, r3)     // Catch:{ Exception -> 0x007a }
            if (r2 == 0) goto L_0x007f
            java.lang.String r2 = "DomobSDK"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x007a }
            java.lang.String r4 = "fail to load image:"
            r3.<init>(r4)     // Catch:{ Exception -> 0x007a }
            java.lang.StringBuilder r3 = r3.append(r9)     // Catch:{ Exception -> 0x007a }
            java.lang.String r4 = " from cache"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x007a }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x007a }
            android.util.Log.d(r2, r3)     // Catch:{ Exception -> 0x007a }
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0043
        L_0x0070:
            r2 = move-exception
            r6 = r2
            r2 = r1
            r1 = r6
        L_0x0074:
            r1.printStackTrace()
            r1 = r0
            r0 = r2
            goto L_0x0043
        L_0x007a:
            r2 = move-exception
            r6 = r2
            r2 = r1
            r1 = r6
            goto L_0x0074
        L_0x007f:
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.domob.android.ads.b.a(cn.domob.android.ads.DBHelper, java.lang.String, java.lang.String, java.util.Hashtable):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static void a(cn.domob.android.ads.DBHelper r6, java.lang.String r7, byte[] r8) {
        /*
            r0 = 0
            android.net.Uri r1 = cn.domob.android.ads.DBHelper.b     // Catch:{ Exception -> 0x0089 }
            android.database.Cursor r0 = r6.b(r1, r7)     // Catch:{ Exception -> 0x0089 }
            if (r0 == 0) goto L_0x000f
            int r1 = r0.getCount()     // Catch:{ Exception -> 0x0080 }
            if (r1 != 0) goto L_0x005e
        L_0x000f:
            android.net.Uri r1 = cn.domob.android.ads.DBHelper.b     // Catch:{ Exception -> 0x0080 }
            r6.a(r1)     // Catch:{ Exception -> 0x0080 }
            java.lang.String r1 = "DomobSDK"
            r2 = 3
            boolean r1 = android.util.Log.isLoggable(r1, r2)     // Catch:{ Exception -> 0x0080 }
            if (r1 == 0) goto L_0x0037
            java.lang.String r1 = "DomobSDK"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0080 }
            java.lang.String r3 = "save image:"
            r2.<init>(r3)     // Catch:{ Exception -> 0x0080 }
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ Exception -> 0x0080 }
            java.lang.String r3 = " to cache!"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0080 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0080 }
            android.util.Log.d(r1, r2)     // Catch:{ Exception -> 0x0080 }
        L_0x0037:
            android.content.ContentValues r1 = new android.content.ContentValues     // Catch:{ Exception -> 0x0080 }
            r1.<init>()     // Catch:{ Exception -> 0x0080 }
            java.lang.String r2 = "_name"
            r1.put(r2, r7)     // Catch:{ Exception -> 0x0080 }
            java.lang.String r2 = "_image"
            r1.put(r2, r8)     // Catch:{ Exception -> 0x0080 }
            java.lang.String r2 = "_date"
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0080 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ Exception -> 0x0080 }
            r1.put(r2, r3)     // Catch:{ Exception -> 0x0080 }
            android.net.Uri r2 = cn.domob.android.ads.DBHelper.b     // Catch:{ Exception -> 0x0080 }
            r6.a(r2, r1)     // Catch:{ Exception -> 0x0080 }
        L_0x0058:
            if (r0 == 0) goto L_0x005d
            r0.close()
        L_0x005d:
            return
        L_0x005e:
            java.lang.String r1 = "DomobSDK"
            r2 = 3
            boolean r1 = android.util.Log.isLoggable(r1, r2)     // Catch:{ Exception -> 0x0080 }
            if (r1 == 0) goto L_0x0058
            java.lang.String r1 = "DomobSDK"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0080 }
            java.lang.String r3 = java.lang.String.valueOf(r7)     // Catch:{ Exception -> 0x0080 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0080 }
            java.lang.String r3 = " is already in cache."
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0080 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0080 }
            android.util.Log.d(r1, r2)     // Catch:{ Exception -> 0x0080 }
            goto L_0x0058
        L_0x0080:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0084:
            r0.printStackTrace()
            r0 = r1
            goto L_0x0058
        L_0x0089:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0084
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.domob.android.ads.b.a(cn.domob.android.ads.DBHelper, java.lang.String, byte[]):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0038  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static boolean b(cn.domob.android.ads.DBHelper r10, java.lang.String r11, java.lang.String r12, java.util.Hashtable<java.lang.String, byte[]> r13) {
        /*
            r8 = 1
            r7 = 3
            r0 = 0
            r1 = 0
            android.net.Uri r2 = cn.domob.android.ads.DBHelper.a     // Catch:{ Exception -> 0x0080 }
            android.database.Cursor r1 = r10.b(r2, r12)     // Catch:{ Exception -> 0x0080 }
            if (r1 == 0) goto L_0x003c
            int r2 = r1.getCount()     // Catch:{ Exception -> 0x0080 }
            if (r2 <= 0) goto L_0x003c
            r1.moveToFirst()     // Catch:{ Exception -> 0x0080 }
            java.lang.String r2 = "_image"
            int r2 = r1.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x0080 }
            byte[] r2 = r1.getBlob(r2)     // Catch:{ Exception -> 0x0080 }
            if (r2 == 0) goto L_0x0099
            java.lang.String r3 = "DomobSDK"
            r4 = 3
            boolean r3 = android.util.Log.isLoggable(r3, r4)     // Catch:{ Exception -> 0x0080 }
            if (r3 == 0) goto L_0x0031
            java.lang.String r3 = "DomobSDK"
            java.lang.String r4 = "success to load from resources DB."
            android.util.Log.d(r3, r4)     // Catch:{ Exception -> 0x0080 }
        L_0x0031:
            r13.put(r11, r2)     // Catch:{ Exception -> 0x0080 }
            r0 = r1
            r1 = r8
        L_0x0036:
            if (r0 == 0) goto L_0x003b
            r0.close()
        L_0x003b:
            return r1
        L_0x003c:
            android.content.Context r2 = r10.a()     // Catch:{ Exception -> 0x0080 }
            if (r2 == 0) goto L_0x0099
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0080 }
            r3.<init>()     // Catch:{ Exception -> 0x0080 }
            android.content.res.AssetManager r2 = r2.getAssets()     // Catch:{ Exception -> 0x0080 }
            java.io.InputStream r2 = r2.open(r12)     // Catch:{ Exception -> 0x0080 }
            android.graphics.Bitmap r2 = android.graphics.BitmapFactory.decodeStream(r2)     // Catch:{ Exception -> 0x0080 }
            android.graphics.Bitmap$CompressFormat r4 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ Exception -> 0x0080 }
            r5 = 100
            r2.compress(r4, r5, r3)     // Catch:{ Exception -> 0x0080 }
            byte[] r4 = r3.toByteArray()     // Catch:{ Exception -> 0x0080 }
            if (r4 == 0) goto L_0x0074
            java.lang.String r5 = "DomobSDK"
            r6 = 3
            boolean r5 = android.util.Log.isLoggable(r5, r6)     // Catch:{ Exception -> 0x0080 }
            if (r5 == 0) goto L_0x0070
            java.lang.String r5 = "DomobSDK"
            java.lang.String r6 = "success to load from preload resources."
            android.util.Log.d(r5, r6)     // Catch:{ Exception -> 0x0080 }
        L_0x0070:
            r13.put(r11, r4)     // Catch:{ Exception -> 0x0080 }
            r0 = r8
        L_0x0074:
            r3.close()     // Catch:{ Exception -> 0x0080 }
            if (r2 == 0) goto L_0x0099
            r2.recycle()     // Catch:{ Exception -> 0x0080 }
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0036
        L_0x0080:
            r2 = move-exception
            r9 = r2
            r2 = r0
            r0 = r9
            java.lang.String r3 = "DomobSDK"
            java.lang.String r4 = "cannot load it from res this time."
            android.util.Log.i(r3, r4)
            java.lang.String r3 = "DomobSDK"
            boolean r3 = android.util.Log.isLoggable(r3, r7)
            if (r3 == 0) goto L_0x0096
            r0.printStackTrace()
        L_0x0096:
            r0 = r1
            r1 = r2
            goto L_0x0036
        L_0x0099:
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0036
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.domob.android.ads.b.b(cn.domob.android.ads.DBHelper, java.lang.String, java.lang.String, java.util.Hashtable):boolean");
    }
}
