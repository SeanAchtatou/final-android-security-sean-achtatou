package cn.domob.android.ads;

public final class Constants {
    public static final String LOG = "DomobSDK";
    public static final int LOG_LEVEL = 3;
    public static final String VAL_SDK_VER = "20110701";
}
