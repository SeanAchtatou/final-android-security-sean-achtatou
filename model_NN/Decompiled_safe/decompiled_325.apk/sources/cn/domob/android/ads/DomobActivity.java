package cn.domob.android.ads;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import cn.domob.android.download.AppExchangeDownloader;
import cn.domob.android.download.AppExchangeDownloaderListener;

public class DomobActivity extends Activity {
    public static final String ActivityType = "DomobActivityType";
    public static final int Type_Downloader = 2;
    public static final int Type_InstallReceiver = 1;
    public static final int Type_None = 0;
    private Context a = this;
    /* access modifiers changed from: private */
    public String b = "";

    public void onCreate(Bundle savedInstanceState) {
        switch (getIntent().getIntExtra(ActivityType, 0)) {
            case 1:
                setTheme(16973835);
                super.onCreate(savedInstanceState);
                return;
            case 2:
                super.onCreate(savedInstanceState);
                if (getIntent().getExtras() != null) {
                    Intent intent = getIntent();
                    String stringExtra = intent.getStringExtra(AppExchangeDownloader.App_Name);
                    this.b = intent.getStringExtra(AppExchangeDownloader.App_Id);
                    String stringExtra2 = intent.getStringExtra(AppExchangeDownloader.Act_Type);
                    if (Log.isLoggable(Constants.LOG, 3)) {
                        Log.d(Constants.LOG, String.valueOf(stringExtra2) + " is showing");
                    }
                    if (stringExtra2 != null && stringExtra2.equals(AppExchangeDownloader.Type_Cancel)) {
                        new AlertDialog.Builder(this.a).setTitle("取消").setMessage(String.valueOf(stringExtra) + "正在下载是否取消?").setNegativeButton("取消下载", new DialogInterface.OnClickListener() {
                            public final void onClick(DialogInterface dialogInterface, int i) {
                                AppExchangeDownloader appExchangeDownloader = AppExchangeDownloader.Download_Map.get(DomobActivity.this.b);
                                if (appExchangeDownloader != null) {
                                    appExchangeDownloader.stopDownload();
                                    AppExchangeDownloaderListener downloadListener = appExchangeDownloader.getDownloadListener();
                                    if (downloadListener != null) {
                                        downloadListener.onDownloadCanceled();
                                    }
                                }
                                DomobActivity.this.finish();
                            }
                        }).setPositiveButton("继续下载", new DialogInterface.OnClickListener() {
                            public final void onClick(DialogInterface dialogInterface, int i) {
                                DomobActivity.this.finish();
                            }
                        }).show();
                        return;
                    }
                    return;
                }
                return;
            default:
                finish();
                return;
        }
    }

    public static void initReceiver(Context context) {
        if (!DomobActionReceiver.hasRegReceiver()) {
            DomobActionReceiver.setRegReceiver(true);
            DomobActionReceiver domobActionReceiver = new DomobActionReceiver();
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
            intentFilter.addDataScheme("package");
            context.getApplicationContext().registerReceiver(domobActionReceiver, intentFilter);
            if (Log.isLoggable(Constants.LOG, 3)) {
                Log.i(Constants.LOG, "register receiver done");
            }
        }
    }
}
