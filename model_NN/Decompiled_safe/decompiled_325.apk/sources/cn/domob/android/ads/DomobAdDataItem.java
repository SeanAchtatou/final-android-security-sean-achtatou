package cn.domob.android.ads;

import android.graphics.Bitmap;
import org.json.JSONObject;

public class DomobAdDataItem {
    public static final String IMAGE_TYPE = "image";
    public static final String TEXT_TYPE = "text";
    private String a = "";
    private String b = "nullad";
    private String c = TEXT_TYPE;
    private Bitmap d = null;
    private Bitmap e = null;
    private String f = "";
    private Bitmap g = null;
    private String h = "";
    private JSONObject i = null;

    /* access modifiers changed from: protected */
    public final void a(DomobAdEngine domobAdEngine) {
    }

    /* access modifiers changed from: protected */
    public final void a(String str) {
        this.a = str;
    }

    public String getAd_identifier() {
        return this.b;
    }

    public boolean isNullAd() {
        return this.b.equals("nullad");
    }

    /* access modifiers changed from: protected */
    public final void b(String str) {
        this.b = str;
    }

    public String getAd_type() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public final void c(String str) {
        this.c = str;
    }

    public Bitmap getAd_icon() {
        return this.d;
    }

    /* access modifiers changed from: protected */
    public final void a(Bitmap bitmap) {
        this.d = bitmap;
    }

    public Bitmap getAd_action_icon() {
        return this.e;
    }

    /* access modifiers changed from: protected */
    public final void b(Bitmap bitmap) {
        this.e = bitmap;
    }

    public String getAd_text() {
        return this.f;
    }

    /* access modifiers changed from: protected */
    public final void d(String str) {
        this.f = str;
    }

    public Bitmap getAdImage() {
        return this.g;
    }

    /* access modifiers changed from: protected */
    public final void c(Bitmap bitmap) {
        this.g = bitmap;
    }

    public String getAlt_text() {
        return this.h;
    }

    /* access modifiers changed from: protected */
    public final void e(String str) {
        this.h = str;
    }

    /* access modifiers changed from: protected */
    public final boolean a() {
        if (this.b == null || this.a == null || this.c == null || this.i == null) {
            return false;
        }
        if (this.c.equals(TEXT_TYPE)) {
            if (this.d == null || this.e == null || this.f == null) {
                return false;
            }
        } else if (this.c.equals(IMAGE_TYPE) && this.g == null) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public final void a(JSONObject jSONObject) {
        this.i = jSONObject;
    }
}
