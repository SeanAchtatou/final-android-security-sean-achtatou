package net.yebaihe.sdk;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;
import cn.domob.android.ads.DomobAdManager;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.net.URLEncoder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import net.yebaihe.finditnet.R;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class LilysdkActivity extends Activity {
    /* access modifiers changed from: private */
    public String apkName = "unknown";
    private int autoGiveup;
    /* access modifiers changed from: private */
    public Handler changeToMainHandler = new Handler();
    /* access modifiers changed from: private */
    public Runnable changeToMainRunnable = new Runnable() {
        public void run() {
            try {
                try {
                    LilysdkActivity.this.startActivity(new Intent(LilysdkActivity.this, Class.forName(LilysdkActivity.this.getPackageManager().getActivityInfo(new ComponentName(LilysdkActivity.this, LilysdkActivity.class), 128).metaData.getString("changeto"))));
                } catch (Exception e) {
                    LilysdkActivity.this.startActivity(new Intent(LilysdkActivity.this, Class.forName(LilysdkActivity.this.getString(R.string.LilySdkTurnTo))));
                }
            } catch (ClassNotFoundException e2) {
                e2.printStackTrace();
            }
            LilysdkActivity.this.finish();
        }
    };
    protected Node curNode;
    private ImageView ilogo;
    private LinearLayout lbottom;
    /* access modifiers changed from: private */
    public Button lbtn;
    private FrameLayout lflayout;
    private View lnew;
    /* access modifiers changed from: private */
    public ProgressBar lprogress;
    private LinearLayout ltop;
    private WebView lwebview;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.lilysdkmain);
        this.lbtn = (Button) findViewById(R.id.lbutton);
        this.lnew = findViewById(R.id.inewimage);
        this.lflayout = (FrameLayout) findViewById(R.id.flayout);
        this.lwebview = (WebView) findViewById(R.id.lwebview);
        this.lwebview.getSettings().setJavaScriptEnabled(true);
        this.lwebview.setScrollBarStyle(33554432);
        this.lprogress = (ProgressBar) findViewById(R.id.lprogressBar);
        this.ilogo = (ImageView) findViewById(R.id.ilogo);
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        anim.setDuration(3000);
        this.ilogo.startAnimation(anim);
        this.lbtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                LilysdkActivity.this.changeToMainHandler.removeCallbacks(LilysdkActivity.this.changeToMainRunnable);
                LilysdkActivity.this.lbtn.setVisibility(8);
                LilysdkActivity.this.lprogress.setVisibility(0);
                LilysdkActivity.this.startDownload();
            }
        });
        new Handler().postDelayed(new Runnable() {
            public void run() {
                LilysdkActivity.this.login();
            }
        }, 100);
        changeToMainAfter(5000);
    }

    /* access modifiers changed from: private */
    public String getOutputPath() {
        String basedir = Environment.getExternalStorageDirectory() + "/lilydownload/apks/";
        File f = new File(basedir);
        if (!f.exists()) {
            f.mkdirs();
        }
        return String.valueOf(basedir) + this.apkName + ".apk";
    }

    /* access modifiers changed from: private */
    public void startDownload() {
        new HttpConnection(this, new Handler() {
            public void handleMessage(Message message) {
                switch (message.what) {
                    case 0:
                        Toast.makeText(LilysdkActivity.this, "开始下载 " + LilysdkActivity.this.lbtn.getTag(), 1).show();
                        return;
                    case 1:
                    default:
                        return;
                    case 2:
                        Intent intent = new Intent("android.intent.action.VIEW");
                        intent.setDataAndType(Uri.fromFile(new File(LilysdkActivity.this.getOutputPath())), "application/vnd.android.package-archive");
                        LilysdkActivity.this.startActivity(intent);
                        LilysdkActivity.this.finish();
                        return;
                    case 3:
                        LilysdkActivity.this.lprogress.setProgress(((Integer) message.obj).intValue());
                        return;
                }
            }
        }).file(this.lbtn.getTag().toString(), getOutputPath());
    }

    /* access modifiers changed from: protected */
    public void login() {
        new HttpConnection(this, new Handler() {
            public void handleMessage(Message message) {
                switch (message.what) {
                    case 0:
                    case 1:
                    default:
                        return;
                    case 2:
                        String body = (String) message.obj;
                        Log.d("myown", "response:" + body);
                        try {
                            Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(body)));
                            doc.getDocumentElement().normalize();
                            ConnectionManager.session = doc.getDocumentElement().getAttribute("session");
                            Log.d("myown", "session:" + ConnectionManager.session);
                            NodeList nodeList = doc.getElementsByTagName("AutoUpdate");
                            if (nodeList.getLength() > 0) {
                                LilysdkActivity.this.curNode = nodeList.item(0);
                                String apk = LilysdkActivity.this.curNode.getAttributes().getNamedItem("apk").getNodeValue();
                                String nodeValue = LilysdkActivity.this.curNode.getAttributes().getNamedItem("ver").getNodeValue();
                                LilysdkActivity.this.apkName = LilysdkActivity.this.getPackageName();
                                LilysdkActivity.this.showAutoUp(apk, LilysdkActivity.this.curNode.getAttributes().getNamedItem("hint").getNodeValue(), LilysdkActivity.this.curNode.getAttributes().getNamedItem("force").getNodeValue());
                            }
                            NodeList nodeList2 = doc.getElementsByTagName("Message");
                            if (nodeList2.getLength() > 0) {
                                LilysdkActivity.this.showMessage(nodeList2.item(0).getAttributes().getNamedItem(DomobAdManager.ACTION_URL).getNodeValue());
                            }
                            NodeList nodeList3 = doc.getElementsByTagName("Soft");
                            if (nodeList3.getLength() > 0) {
                                LilysdkActivity.this.curNode = nodeList3.item(0);
                                String url = LilysdkActivity.this.curNode.getAttributes().getNamedItem("apk").getNodeValue();
                                String hint = LilysdkActivity.this.curNode.getAttributes().getNamedItem("hint").getNodeValue();
                                String packagename = LilysdkActivity.this.curNode.getAttributes().getNamedItem("package").getNodeValue();
                                LilysdkActivity.this.apkName = LilysdkActivity.this.curNode.getAttributes().getNamedItem("name").getNodeValue();
                                try {
                                    LilysdkActivity.this.getPackageManager().getPackageInfo(packagename, 0);
                                    return;
                                } catch (PackageManager.NameNotFoundException e) {
                                    LilysdkActivity.this.showApkTuijian(url, hint);
                                    return;
                                }
                            } else {
                                return;
                            }
                        } catch (ParserConfigurationException e2) {
                            e2.printStackTrace();
                            return;
                        } catch (SAXException e3) {
                            e3.printStackTrace();
                            return;
                        } catch (IOException e4) {
                            e4.printStackTrace();
                            return;
                        }
                }
            }
        }).get("http://lily.newim.net/appstore/login.php?key=" + getKey());
    }

    /* access modifiers changed from: protected */
    public void showAutoUp(String apk, String hint, String force) {
        if (force.equals("1")) {
            this.changeToMainHandler.removeCallbacks(this.changeToMainRunnable);
        }
        this.lflayout.setVisibility(0);
        this.lwebview.setVisibility(0);
        this.lbtn.setVisibility(0);
        this.lwebview.loadUrl(hint);
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        this.lwebview.startAnimation(anim);
        this.lbtn.startAnimation(anim);
        this.lbtn.setTag(apk);
    }

    /* access modifiers changed from: protected */
    public void showApkTuijian(String url, String hint) {
        this.lflayout.setVisibility(0);
        this.lnew.setVisibility(0);
        this.changeToMainHandler.removeCallbacks(this.changeToMainRunnable);
        changeToMainAfter(5000);
        this.lwebview.setVisibility(0);
        this.lbtn.setVisibility(0);
        this.lwebview.loadUrl(hint);
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        this.lwebview.startAnimation(anim);
        this.lbtn.startAnimation(anim);
        this.lbtn.setTag(url);
    }

    /* access modifiers changed from: protected */
    public void showMessage(String url) {
        this.lflayout.setVisibility(0);
        this.changeToMainHandler.removeCallbacks(this.changeToMainRunnable);
        changeToMainAfter(5000);
        this.lwebview.setVisibility(0);
        this.lwebview.loadUrl(url);
        this.lwebview.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_in));
    }

    private void changeToMainAfter(int delay) {
        this.changeToMainHandler.postDelayed(this.changeToMainRunnable, (long) delay);
    }

    private String getKey() {
        try {
            return URLEncoder.encode(Des2.encode("lilysdk@", "lilysdk|" + getPackageName() + "|" + getAppVersionName(this) + "|" + getIMEI()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getAppVersionName(Context context) {
        try {
            return new StringBuilder().append(context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode).toString();
        } catch (Exception e) {
            return "";
        }
    }

    private String getIMEI() {
        return ((TelephonyManager) getSystemService("phone")).getDeviceId();
    }
}
