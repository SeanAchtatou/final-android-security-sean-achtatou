package net.yebaihe.sdk;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.entity.BufferedHttpEntity;

public class HttpConnection implements Runnable {
    private static final int BITMAP = 4;
    private static final int DELETE = 3;
    public static final int DID_ERROR = 1;
    public static final int DID_PROGRESS = 3;
    public static final int DID_START = 0;
    public static final int DID_SUCCEED = 2;
    private static final int FILE = 5;
    private static final int GET = 0;
    private static final int POST = 1;
    private static final int PUT = 2;
    private String data;
    private Handler handler;
    private HttpClient httpClient;
    private Context mCtx;
    private int method;
    private String url;

    public HttpConnection(Context ctx, Handler _handler) {
        this.handler = _handler;
        this.mCtx = ctx;
    }

    public void create(int method2, String url2, String data2) {
        this.method = method2;
        this.url = url2;
        this.data = data2;
        ConnectionManager.getInstance().push(this);
    }

    public void get(String url2) {
        create(0, fixSession(url2), null);
    }

    private String fixSession(String url2) {
        String ret = url2;
        if (url2.indexOf("?") >= 0) {
            return String.valueOf(ret) + "&session=" + URLEncoder.encode(ConnectionManager.session);
        }
        return String.valueOf(ret) + "?session=" + URLEncoder.encode(ConnectionManager.session);
    }

    public void post(String url2, String data2) {
        create(1, fixSession(url2), data2);
    }

    public void put(String url2, String data2) {
        create(2, fixSession(url2), data2);
    }

    public void delete(String url2) {
        create(3, fixSession(url2), null);
    }

    public void bitmap(String url2, int inSampleSize) {
        create(4, fixSession(url2), new StringBuilder(String.valueOf(inSampleSize)).toString());
    }

    public void file(String url2, String path) {
        create(5, fixSession(url2), path);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x0029 A[Catch:{ Exception -> 0x0099 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r7 = this;
            android.os.Handler r4 = r7.handler
            android.os.Handler r5 = r7.handler
            r6 = 0
            android.os.Message r5 = android.os.Message.obtain(r5, r6)
            r4.sendMessage(r5)
            org.apache.http.impl.client.DefaultHttpClient r4 = new org.apache.http.impl.client.DefaultHttpClient
            r4.<init>()
            r7.httpClient = r4
            org.apache.http.client.HttpClient r4 = r7.httpClient
            org.apache.http.params.HttpParams r4 = r4.getParams()
            r5 = 10000(0x2710, float:1.4013E-41)
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r4, r5)
            r3 = 0
            int r4 = r7.method     // Catch:{ Exception -> 0x0099 }
            switch(r4) {
                case 0: goto L_0x0038;
                case 1: goto L_0x0046;
                case 2: goto L_0x005e;
                case 3: goto L_0x0076;
                case 4: goto L_0x0084;
                case 5: goto L_0x00a8;
                default: goto L_0x0024;
            }     // Catch:{ Exception -> 0x0099 }
        L_0x0024:
            int r4 = r7.method     // Catch:{ Exception -> 0x0099 }
            r5 = 4
            if (r4 >= r5) goto L_0x0030
            org.apache.http.HttpEntity r4 = r3.getEntity()     // Catch:{ Exception -> 0x0099 }
            r7.processEntity(r4)     // Catch:{ Exception -> 0x0099 }
        L_0x0030:
            net.yebaihe.sdk.ConnectionManager r4 = net.yebaihe.sdk.ConnectionManager.getInstance()
            r4.didComplete(r7)
            return
        L_0x0038:
            org.apache.http.client.HttpClient r4 = r7.httpClient     // Catch:{ Exception -> 0x0099 }
            org.apache.http.client.methods.HttpGet r5 = new org.apache.http.client.methods.HttpGet     // Catch:{ Exception -> 0x0099 }
            java.lang.String r6 = r7.url     // Catch:{ Exception -> 0x0099 }
            r5.<init>(r6)     // Catch:{ Exception -> 0x0099 }
            org.apache.http.HttpResponse r3 = r4.execute(r5)     // Catch:{ Exception -> 0x0099 }
            goto L_0x0024
        L_0x0046:
            org.apache.http.client.methods.HttpPost r1 = new org.apache.http.client.methods.HttpPost     // Catch:{ Exception -> 0x0099 }
            java.lang.String r4 = r7.url     // Catch:{ Exception -> 0x0099 }
            r1.<init>(r4)     // Catch:{ Exception -> 0x0099 }
            org.apache.http.entity.StringEntity r4 = new org.apache.http.entity.StringEntity     // Catch:{ Exception -> 0x0099 }
            java.lang.String r5 = r7.data     // Catch:{ Exception -> 0x0099 }
            r4.<init>(r5)     // Catch:{ Exception -> 0x0099 }
            r1.setEntity(r4)     // Catch:{ Exception -> 0x0099 }
            org.apache.http.client.HttpClient r4 = r7.httpClient     // Catch:{ Exception -> 0x0099 }
            org.apache.http.HttpResponse r3 = r4.execute(r1)     // Catch:{ Exception -> 0x0099 }
            goto L_0x0024
        L_0x005e:
            org.apache.http.client.methods.HttpPut r2 = new org.apache.http.client.methods.HttpPut     // Catch:{ Exception -> 0x0099 }
            java.lang.String r4 = r7.url     // Catch:{ Exception -> 0x0099 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x0099 }
            org.apache.http.entity.StringEntity r4 = new org.apache.http.entity.StringEntity     // Catch:{ Exception -> 0x0099 }
            java.lang.String r5 = r7.data     // Catch:{ Exception -> 0x0099 }
            r4.<init>(r5)     // Catch:{ Exception -> 0x0099 }
            r2.setEntity(r4)     // Catch:{ Exception -> 0x0099 }
            org.apache.http.client.HttpClient r4 = r7.httpClient     // Catch:{ Exception -> 0x0099 }
            org.apache.http.HttpResponse r3 = r4.execute(r2)     // Catch:{ Exception -> 0x0099 }
            goto L_0x0024
        L_0x0076:
            org.apache.http.client.HttpClient r4 = r7.httpClient     // Catch:{ Exception -> 0x0099 }
            org.apache.http.client.methods.HttpDelete r5 = new org.apache.http.client.methods.HttpDelete     // Catch:{ Exception -> 0x0099 }
            java.lang.String r6 = r7.url     // Catch:{ Exception -> 0x0099 }
            r5.<init>(r6)     // Catch:{ Exception -> 0x0099 }
            org.apache.http.HttpResponse r3 = r4.execute(r5)     // Catch:{ Exception -> 0x0099 }
            goto L_0x0024
        L_0x0084:
            org.apache.http.client.HttpClient r4 = r7.httpClient     // Catch:{ Exception -> 0x0099 }
            org.apache.http.client.methods.HttpGet r5 = new org.apache.http.client.methods.HttpGet     // Catch:{ Exception -> 0x0099 }
            java.lang.String r6 = r7.url     // Catch:{ Exception -> 0x0099 }
            r5.<init>(r6)     // Catch:{ Exception -> 0x0099 }
            org.apache.http.HttpResponse r3 = r4.execute(r5)     // Catch:{ Exception -> 0x0099 }
            org.apache.http.HttpEntity r4 = r3.getEntity()     // Catch:{ Exception -> 0x0099 }
            r7.processBitmapEntity(r4)     // Catch:{ Exception -> 0x0099 }
            goto L_0x0024
        L_0x0099:
            r4 = move-exception
            r0 = r4
            android.os.Handler r4 = r7.handler
            android.os.Handler r5 = r7.handler
            r6 = 1
            android.os.Message r5 = android.os.Message.obtain(r5, r6, r0)
            r4.sendMessage(r5)
            goto L_0x0030
        L_0x00a8:
            org.apache.http.client.HttpClient r4 = r7.httpClient     // Catch:{ Exception -> 0x0099 }
            org.apache.http.client.methods.HttpGet r5 = new org.apache.http.client.methods.HttpGet     // Catch:{ Exception -> 0x0099 }
            java.lang.String r6 = r7.url     // Catch:{ Exception -> 0x0099 }
            r5.<init>(r6)     // Catch:{ Exception -> 0x0099 }
            org.apache.http.HttpResponse r3 = r4.execute(r5)     // Catch:{ Exception -> 0x0099 }
            org.apache.http.HttpEntity r4 = r3.getEntity()     // Catch:{ Exception -> 0x0099 }
            r7.processFileEntity(r4)     // Catch:{ Exception -> 0x0099 }
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: net.yebaihe.sdk.HttpConnection.run():void");
    }

    private void processFileEntity(HttpEntity entity) throws IOException {
        FileOutputStream out = new FileOutputStream(this.data);
        InputStream in = entity.getContent();
        byte[] bytes = new byte[1024];
        int downloadedSize = 0;
        long totalSize = entity.getContentLength();
        while (true) {
            int read = in.read(bytes);
            if (read == -1) {
                in.close();
                out.flush();
                out.close();
                this.handler.sendMessage(Message.obtain(this.handler, 2, null));
                return;
            }
            out.write(bytes, 0, read);
            downloadedSize += bytes.length;
            this.handler.sendMessage(Message.obtain(this.handler, 3, Integer.valueOf((int) (((long) (downloadedSize * 100)) / totalSize))));
        }
    }

    private void processEntity(HttpEntity entity) throws IllegalStateException, IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(entity.getContent()));
        String result = "";
        while (true) {
            String line = br.readLine();
            if (line == null) {
                this.handler.sendMessage(Message.obtain(this.handler, 2, result));
                return;
            }
            result = String.valueOf(result) + line + "\n";
        }
    }

    private void processBitmapEntity(HttpEntity entity) throws IOException {
        BufferedHttpEntity bufHttpEntity = new BufferedHttpEntity(entity);
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = Integer.parseInt(this.data);
        this.handler.sendMessage(Message.obtain(this.handler, 2, BitmapFactory.decodeStream(bufHttpEntity.getContent(), null, o2)));
    }
}
