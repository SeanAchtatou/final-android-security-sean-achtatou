package net.yebaihe.finditnet;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MyDbHelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "info.db";
    public static final String TB_NAME = "files";
    public static final int VERSION = 2;

    public MyDbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public void onCreate(SQLiteDatabase arg0) {
        arg0.execSQL("CREATE TABLE IF NOT EXISTS files (_id INTEGER PRIMARY KEY,path text,passed integer)");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS files");
        onCreate(db);
    }
}
