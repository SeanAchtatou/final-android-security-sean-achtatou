package net.yebaihe.finditnet;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import cn.domob.android.ads.DomobAdDataItem;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.yebaihe.sdk.HttpConnection;

public class FindItDownload extends Activity {
    List<Map<String, Object>> catlist = new ArrayList();
    private Handler listHandler;
    /* access modifiers changed from: private */
    public View progressBar;

    class CatBitMapHandler extends Handler {
        private Map<String, Object> mMap;
        private String mUrl;

        public CatBitMapHandler(Map<String, Object> map, String url) {
            this.mMap = map;
            this.mUrl = url;
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case 0:
                case 1:
                default:
                    return;
                case 2:
                    this.mMap.put(DomobAdDataItem.IMAGE_TYPE, (Bitmap) message.obj);
                    FindItDownload.this.refreshCatList();
                    return;
            }
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.download);
        this.progressBar = findViewById(R.id.idprogressbar);
        this.listHandler = new Handler() {
            public void handleMessage(Message message) {
                switch (message.what) {
                    case 0:
                    default:
                        return;
                    case 1:
                        FindItDownload.this.progressBar.setVisibility(8);
                        Log.d("myown", "network error" + message.obj);
                        return;
                    case 2:
                        FindItDownload.this.progressBar.setVisibility(8);
                        String response = (String) message.obj;
                        Log.d("myown", response);
                        String[] ret = response.trim().split("\n");
                        FindItDownload.this.catlist.clear();
                        for (String split : ret) {
                            String[] pvs = split.split("\\|", 5);
                            if (pvs.length == 5) {
                                Map<String, Object> map = new HashMap<>();
                                map.put("id", pvs[0]);
                                map.put("foldname", pvs[1]);
                                map.put("total", pvs[2]);
                                map.put("lastupdate", pvs[3]);
                                map.put("name", pvs[4]);
                                String url = "http://lily.newim.net/appstore/findit_catimg.php?folder=" + pvs[1];
                                new HttpConnection(FindItDownload.this, new CatBitMapHandler(map, url)).bitmap(url, 3);
                                FindItDownload.this.catlist.add(map);
                            }
                        }
                        FindItDownload.this.refreshCatList();
                        return;
                }
            }
        };
        new HttpConnection(this, this.listHandler).get("http://lily.newim.net/appstore/findit_cats.php");
        ((ListView) findViewById(R.id.idcatlist)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                Intent i = new Intent(FindItDownload.this, FindItDownloadDetail.class);
                i.putExtra("folder", FindItDownload.this.catlist.get(arg2).get("foldname").toString());
                i.putExtra("id", FindItDownload.this.catlist.get(arg2).get("id").toString());
                FindItDownload.this.startActivity(i);
            }
        });
    }

    public void onBackPressed() {
        finish();
    }

    /* access modifiers changed from: protected */
    public void refreshCatList() {
        ((ListView) findViewById(R.id.idcatlist)).setAdapter((ListAdapter) new CatListCursorAdapter(this, this.catlist, R.layout.catrow, new String[0], new int[0]));
    }
}
