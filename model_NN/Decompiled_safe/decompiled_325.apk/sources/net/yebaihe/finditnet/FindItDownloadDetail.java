package net.yebaihe.finditnet;

import android.app.Activity;
import android.app.AlertDialog;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.yebaihe.sdk.HttpConnection;

public class FindItDownloadDetail extends Activity {
    /* access modifiers changed from: private */
    public ProgressBar bardate;
    /* access modifiers changed from: private */
    public ProgressBar barpopular;
    Bitmap bdtag;
    private Button btnByDate;
    private Button btnByPopular;
    private Button btnDownloadNow;
    /* access modifiers changed from: private */
    public Button btnSelectAll;
    /* access modifiers changed from: private */
    public Handler byDateHandler;
    /* access modifiers changed from: private */
    public Handler byPopularHandler;
    private View.OnClickListener chooseImageClickListenner = new View.OnClickListener() {
        public void onClick(View arg0) {
            boolean z;
            Map<String, Object> map = (Map) ((ImageView) arg0).getTag();
            if (!FindItDownloadDetail.this.dataExists(map)) {
                if (((Boolean) map.get("selected")).booleanValue()) {
                    z = false;
                } else {
                    z = true;
                }
                map.put("selected", Boolean.valueOf(z));
                FindItDownloadDetail.this.redrawImage(map);
                FindItDownloadDetail.this.btnSelectAll.setText(String.format("全选(%d)", Integer.valueOf(FindItDownloadDetail.this.getSelectedCount())));
            }
        }
    };
    /* access modifiers changed from: private */
    public int curDatePage = 0;
    /* access modifiers changed from: private */
    public int curPopularPage = 0;
    /* access modifiers changed from: private */
    public Map<String, Map<String, Object>> datamap = new HashMap();
    protected boolean dateInUpdateing = false;
    protected List<String> dateList = new ArrayList();
    private TableLayout dateTable;
    /* access modifiers changed from: private */
    public SQLiteDatabase db;
    Bitmap dhtag;
    private int eachRowNum = 2;
    /* access modifiers changed from: private */
    public MyDbHelper m_Helper;
    protected boolean popularInUpdateing = false;
    protected List<String> popularList = new ArrayList();
    /* access modifiers changed from: private */
    public TableLayout popularTable;

    class DownloadDialog extends Handler {
        private Map<String, Object> curmap;
        private List<String> downloadList = new ArrayList();
        private AlertDialog mDialog;
        private ProgressBar mProgress;
        private TextView mText;

        DownloadDialog() {
            View view = LayoutInflater.from(FindItDownloadDetail.this).inflate((int) R.layout.progress, (ViewGroup) null);
            this.mProgress = (ProgressBar) view.findViewById(R.id.progressBar1);
            this.mText = (TextView) view.findViewById(R.id.idProgressTxt);
            this.mText.setText("");
            this.mDialog = new AlertDialog.Builder(FindItDownloadDetail.this).setIcon((int) R.drawable.icon).setView(view).create();
        }

        public void show() {
            this.mProgress.setMax(FindItDownloadDetail.this.getSelectedCount());
            this.mProgress.setProgress(0);
            this.mText.setText(String.format("正在下载:%d/%d", Integer.valueOf(this.mProgress.getProgress()), Integer.valueOf(this.mProgress.getMax())));
            tryDownload();
            this.mDialog.show();
        }

        private void tryDownload() {
            for (Map.Entry<String, Map<String, Object>> entry : FindItDownloadDetail.this.datamap.entrySet()) {
                if (((Boolean) ((Map) entry.getValue()).get("selected")).booleanValue() && this.downloadList.indexOf(((Map) entry.getValue()).get("id").toString()) < 0) {
                    this.curmap = (Map) entry.getValue();
                    this.downloadList.add(((Map) entry.getValue()).get("id").toString());
                    String outputpath = String.valueOf(FindItDownloadDetail.getBaseDir()) + ((Map) entry.getValue()).get("id") + ".dat";
                    String url = "http://lily.newim.net/appstore/findit_data.php?id=" + ((Map) entry.getValue()).get("id");
                    Log.d("myown", "try download " + url + " to " + outputpath);
                    new HttpConnection(FindItDownloadDetail.this, this).file(url, outputpath);
                    return;
                }
            }
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case 0:
                default:
                    return;
                case 1:
                    Log.d("myown", new StringBuilder().append(message.obj).toString());
                    return;
                case 2:
                    this.mProgress.setProgress(this.downloadList.size());
                    this.mText.setText(String.format("正在下载:%d/%d", Integer.valueOf(this.mProgress.getProgress()), Integer.valueOf(this.mProgress.getMax())));
                    FindItDownloadDetail.this.redrawImage(this.curmap);
                    FindItDownloadDetail.this.m_Helper = new MyDbHelper(FindItDownloadDetail.this, MyDbHelper.DB_NAME, null, 2);
                    FindItDownloadDetail.this.db = FindItDownloadDetail.this.m_Helper.getWritableDatabase();
                    FindItDownloadDetail.this.db.delete(MyDbHelper.TB_NAME, "path='" + this.curmap.get("id") + "'", null);
                    FindItDownloadDetail.this.db.execSQL("insert into files (path,passed) values ('" + this.curmap.get("id") + "',0)");
                    FindItDownloadDetail.this.db.close();
                    FindItDownloadDetail.this.m_Helper.close();
                    if (this.downloadList.size() < FindItDownloadDetail.this.getSelectedCount()) {
                        tryDownload();
                        return;
                    }
                    this.mDialog.cancel();
                    Toast.makeText(FindItDownloadDetail.this, "下载完成", 1).show();
                    return;
            }
        }
    }

    class BitMapHandler extends Handler {
        private String mId;
        private Map<String, Object> mMap;
        private String mUrl;

        public BitMapHandler(Map<String, Object> map, String id, String url) {
            this.mMap = map;
            this.mId = id;
            this.mUrl = url;
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case 0:
                case 1:
                default:
                    return;
                case 2:
                    this.mMap.put("bmp", (Bitmap) message.obj);
                    FindItDownloadDetail.this.redrawImage(this.mMap);
                    return;
            }
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.downloaddetail);
        this.btnSelectAll = (Button) findViewById(R.id.btnselall);
        this.btnSelectAll.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!FindItDownloadDetail.this.isAllSelected()) {
                    FindItDownloadDetail.this.selectAll();
                } else {
                    FindItDownloadDetail.this.selectNone();
                }
            }
        });
        this.dhtag = BitmapFactory.decodeResource(getResources(), R.drawable.dh);
        this.bdtag = BitmapFactory.decodeResource(getResources(), R.drawable.bd);
        this.btnDownloadNow = (Button) findViewById(R.id.btndownloadnow);
        this.btnDownloadNow.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FindItDownloadDetail.this.downloadNow();
            }
        });
        final String folder = getIntent().getStringExtra("id");
        Log.d("myown", folder);
        TabHost tabHost = (TabHost) findViewById(R.id.tabhost);
        tabHost.setup();
        tabHost.addTab(tabHost.newTabSpec("bydate").setContent((int) R.id.tab1).setIndicator((RelativeLayout) LayoutInflater.from(this).inflate((int) R.layout.bydate, (ViewGroup) null)));
        tabHost.addTab(tabHost.newTabSpec("bypopular").setContent((int) R.id.tab2).setIndicator((RelativeLayout) LayoutInflater.from(this).inflate((int) R.layout.bypopular, (ViewGroup) null)));
        LinearLayout bydatelist = (LinearLayout) LayoutInflater.from(this).inflate((int) R.layout.bydatelist, (ViewGroup) null);
        this.dateTable = (TableLayout) bydatelist.findViewById(R.id.datetable);
        final ObservableScrollView scrollbox = (ObservableScrollView) bydatelist.findViewById(R.id.scrollbox);
        this.bardate = (ProgressBar) bydatelist.findViewById(R.id.progressBarJustForShow);
        scrollbox.setScrollViewListener(new ScrollViewListener() {
            public void onScrollChanged(ObservableScrollView scrollView, int x, int y, int oldx, int oldy) {
                Rect r = new Rect();
                Point offset = new Point();
                if (FindItDownloadDetail.this.bardate.isShown() && scrollbox.getChildVisibleRect(FindItDownloadDetail.this.bardate, r, offset) && !FindItDownloadDetail.this.dateInUpdateing) {
                    FindItDownloadDetail.this.dateInUpdateing = true;
                    Log.d("myown", "update again!");
                    new HttpConnection(FindItDownloadDetail.this, FindItDownloadDetail.this.byDateHandler).get("http://lily.newim.net/appstore/findit_catdetail.php?page=" + FindItDownloadDetail.this.curDatePage + "&sort=date&fold=" + folder);
                }
            }
        });
        ((LinearLayout) findViewById(R.id.tab1)).addView(bydatelist, new LinearLayout.LayoutParams(-1, -1));
        LinearLayout bypopularlist = (LinearLayout) LayoutInflater.from(this).inflate((int) R.layout.bypopularlist, (ViewGroup) null);
        this.popularTable = (TableLayout) bypopularlist.findViewById(R.id.popularTable);
        ((LinearLayout) findViewById(R.id.tab2)).addView(bypopularlist, new LinearLayout.LayoutParams(-1, -1));
        final ObservableScrollView scrollbox2 = (ObservableScrollView) bypopularlist.findViewById(R.id.scrollbox);
        this.barpopular = (ProgressBar) bypopularlist.findViewById(R.id.progressBarJustForShow);
        scrollbox2.setScrollViewListener(new ScrollViewListener() {
            public void onScrollChanged(ObservableScrollView scrollView, int x, int y, int oldx, int oldy) {
                Rect r = new Rect();
                Point offset = new Point();
                if (FindItDownloadDetail.this.barpopular.isShown() && scrollbox2.getChildVisibleRect(FindItDownloadDetail.this.barpopular, r, offset) && !FindItDownloadDetail.this.popularInUpdateing) {
                    FindItDownloadDetail.this.popularInUpdateing = true;
                    new HttpConnection(FindItDownloadDetail.this, FindItDownloadDetail.this.byPopularHandler).get("http://lily.newim.net/appstore/findit_catdetail.php?page=" + FindItDownloadDetail.this.curPopularPage + "&sort=popular&fold=" + folder);
                }
            }
        });
        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            public void onTabChanged(String tabId) {
                if (tabId.equals("bypopular") && FindItDownloadDetail.this.popularTable.getChildCount() == 0) {
                    new HttpConnection(FindItDownloadDetail.this, FindItDownloadDetail.this.byPopularHandler).get("http://lily.newim.net/appstore/findit_catdetail.php?page=" + FindItDownloadDetail.this.curPopularPage + "&sort=popular&fold=" + folder);
                }
            }
        });
        tabHost.setCurrentTab(0);
        this.byDateHandler = new Handler() {
            public void handleMessage(Message message) {
                switch (message.what) {
                    case 0:
                    case 1:
                    default:
                        return;
                    case 2:
                        FindItDownloadDetail findItDownloadDetail = FindItDownloadDetail.this;
                        findItDownloadDetail.curDatePage = findItDownloadDetail.curDatePage + 1;
                        FindItDownloadDetail.this.dateInUpdateing = false;
                        String response = (String) message.obj;
                        Log.d("myown", response);
                        String[] ret = response.trim().split("\n");
                        int i = 0;
                        while (true) {
                            if (i < ret.length) {
                                if (ret[i].trim().equals("end")) {
                                    FindItDownloadDetail.this.bardate.setVisibility(8);
                                    Log.d("myown", "bardate should dispear");
                                } else {
                                    String[] pvs = ret[i].split("\\|", 6);
                                    if (pvs.length == 6) {
                                        if (!FindItDownloadDetail.this.datamap.containsKey(pvs[0])) {
                                            Map<String, Object> map = new HashMap<>();
                                            map.put("id", pvs[0]);
                                            map.put("catid", pvs[1]);
                                            map.put("path", pvs[2]);
                                            map.put("dateadd", pvs[3]);
                                            map.put("downtimes", pvs[4]);
                                            map.put("passtimes", pvs[5]);
                                            map.put("selected", false);
                                            map.put("images", new ArrayList());
                                            FindItDownloadDetail.this.datamap.put(pvs[0], map);
                                            String url = "http://lily.newim.net/appstore/findit_detailbmp.php?cat=" + pvs[1] + "&path=" + pvs[2];
                                            new HttpConnection(FindItDownloadDetail.this, new BitMapHandler(map, pvs[0], url)).bitmap(url, 3);
                                        }
                                        FindItDownloadDetail.this.dateList.add(pvs[0]);
                                    }
                                    i++;
                                }
                            }
                        }
                        FindItDownloadDetail.this.refreshDateTable();
                        return;
                }
            }
        };
        this.byPopularHandler = new Handler() {
            public void handleMessage(Message message) {
                switch (message.what) {
                    case 0:
                    case 1:
                    default:
                        return;
                    case 2:
                        FindItDownloadDetail findItDownloadDetail = FindItDownloadDetail.this;
                        findItDownloadDetail.curPopularPage = findItDownloadDetail.curPopularPage + 1;
                        FindItDownloadDetail.this.popularInUpdateing = false;
                        String[] ret = ((String) message.obj).trim().split("\n");
                        int i = 0;
                        while (true) {
                            if (i < ret.length) {
                                if (ret[i].trim().equals("end")) {
                                    FindItDownloadDetail.this.barpopular.setVisibility(8);
                                    Log.d("myown", "barpopular should dispear");
                                } else {
                                    String[] pvs = ret[i].split("\\|", 6);
                                    if (pvs.length == 6) {
                                        if (!FindItDownloadDetail.this.datamap.containsKey(pvs[0])) {
                                            Map<String, Object> map = new HashMap<>();
                                            map.put("id", pvs[0]);
                                            map.put("catid", pvs[1]);
                                            map.put("path", pvs[2]);
                                            map.put("dateadd", pvs[3]);
                                            map.put("downtimes", pvs[4]);
                                            map.put("passtimes", pvs[5]);
                                            map.put("selected", false);
                                            map.put("images", new ArrayList());
                                            FindItDownloadDetail.this.datamap.put(pvs[0], map);
                                            String url = "http://lily.newim.net/appstore/findit_detailbmp.php?cat=" + pvs[1] + "&path=" + pvs[2];
                                            new HttpConnection(FindItDownloadDetail.this, new BitMapHandler(map, pvs[0], url)).bitmap(url, 3);
                                        }
                                        FindItDownloadDetail.this.popularList.add(pvs[0]);
                                    }
                                    i++;
                                }
                            }
                        }
                        FindItDownloadDetail.this.refreshPopularTable();
                        return;
                }
            }
        };
        this.curDatePage = 0;
        this.curPopularPage = 0;
        new HttpConnection(this, this.byDateHandler).get("http://lily.newim.net/appstore/findit_catdetail.php?page=" + this.curDatePage + "&sort=date&fold=" + folder);
    }

    /* access modifiers changed from: protected */
    public boolean isAllSelected() {
        for (Map.Entry<String, Map<String, Object>> entry : this.datamap.entrySet()) {
            if (!new File(String.valueOf(getBaseDir()) + ((Map) entry.getValue()).get("id") + ".dat").exists() && !((Boolean) ((Map) entry.getValue()).get("selected")).booleanValue()) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean dataExists(Map<String, Object> map) {
        return new File(String.valueOf(getBaseDir()) + map.get("id") + ".dat").exists();
    }

    public static String getBaseDir() {
        String basedir = Environment.getExternalStorageDirectory() + "/finditnet/dat/";
        File f = new File(basedir);
        if (!f.exists()) {
            f.mkdirs();
        }
        return basedir;
    }

    /* access modifiers changed from: protected */
    public void downloadNow() {
        if (getSelectedCount() <= 0) {
            Toast.makeText(this, "您还没有选择任何一个关卡", 1).show();
        } else {
            new DownloadDialog().show();
        }
    }

    /* access modifiers changed from: protected */
    public void selectNone() {
        for (Map.Entry<String, Map<String, Object>> entry : this.datamap.entrySet()) {
            ((Map) entry.getValue()).put("selected", false);
            redrawImage((Map) entry.getValue());
        }
        this.btnSelectAll.setText("全选(0)");
    }

    /* access modifiers changed from: private */
    public void redrawImage(Map<String, Object> map) {
        if (map.containsKey("bmp")) {
            ArrayList<ImageView> images = (ArrayList) map.get("images");
            this.m_Helper = new MyDbHelper(this, MyDbHelper.DB_NAME, null, 2);
            this.db = this.m_Helper.getWritableDatabase();
            for (int idx = 0; idx < images.size(); idx++) {
                ImageView i = (ImageView) images.get(idx);
                Bitmap src = Bitmap.createScaledBitmap((Bitmap) map.get("bmp"), i.getLayoutParams().width, i.getLayoutParams().height, true);
                if (new File(String.valueOf(getBaseDir()) + map.get("id") + ".dat").exists()) {
                    Bitmap tmp = Bitmap.createBitmap(src.getWidth(), src.getHeight(), Bitmap.Config.ARGB_8888);
                    Canvas c = new Canvas(tmp);
                    c.drawBitmap(src, 0.0f, 0.0f, new Paint());
                    c.drawBitmap(this.bdtag, (float) (src.getWidth() - this.bdtag.getWidth()), (float) (src.getHeight() - this.bdtag.getHeight()), new Paint());
                    i.setImageBitmap(tmp);
                    Log.d("myown", "file exists:" + map.get("id"));
                    Cursor cursor = this.db.rawQuery("select * from files where path='" + map.get("id") + "'", null);
                    if (!cursor.moveToFirst()) {
                        this.db.execSQL("insert into files (path,passed) values ('" + map.get("id") + "',0)");
                    }
                    cursor.close();
                } else if (((Boolean) map.get("selected")).booleanValue()) {
                    Bitmap tmp2 = Bitmap.createBitmap(src.getWidth(), src.getHeight(), Bitmap.Config.ARGB_8888);
                    Canvas c2 = new Canvas(tmp2);
                    c2.drawBitmap(src, 0.0f, 0.0f, new Paint());
                    c2.drawBitmap(this.dhtag, (float) (src.getWidth() - this.dhtag.getWidth()), (float) (src.getHeight() - this.dhtag.getHeight()), new Paint());
                    i.setImageBitmap(tmp2);
                    Log.d("myown", "file selected:" + map.get("id"));
                } else {
                    i.setImageBitmap(src);
                    Log.d("myown", "just file:" + map.get("id"));
                }
            }
            this.db.close();
            this.m_Helper.close();
        }
    }

    /* access modifiers changed from: protected */
    public void selectAll() {
        for (Map.Entry<String, Map<String, Object>> entry : this.datamap.entrySet()) {
            if (!dataExists((Map) entry.getValue())) {
                ((Map) entry.getValue()).put("selected", true);
                redrawImage((Map) entry.getValue());
            }
        }
        this.btnSelectAll.setText(String.format("全选(%d)", Integer.valueOf(getSelectedCount())));
    }

    /* access modifiers changed from: protected */
    public int getSelectedCount() {
        int total = 0;
        for (Map.Entry<String, Map<String, Object>> entry : this.datamap.entrySet()) {
            if (((Boolean) ((Map) entry.getValue()).get("selected")).booleanValue()) {
                total++;
            }
        }
        return total;
    }

    public ImageView getImageView(TableLayout table, int row, int col) {
        return (ImageView) ((LinearLayout) ((TableRow) table.getChildAt(row)).getChildAt(col)).findViewById(R.id.singleimg);
    }

    /* access modifiers changed from: protected */
    public void refreshPopularTable() {
        int totalrows = this.popularList.size() / this.eachRowNum;
        if (this.eachRowNum * totalrows < this.popularList.size()) {
            totalrows++;
        }
        for (int i = this.popularTable.getChildCount(); i < totalrows; i++) {
            TableRow row = new TableRow(this);
            for (int j = 0; j < this.eachRowNum; j++) {
                LinearLayout singleData = (LinearLayout) LayoutInflater.from(this).inflate((int) R.layout.singledata, (ViewGroup) null);
                ImageView img = (ImageView) singleData.findViewById(R.id.singleimg);
                int idx = (this.eachRowNum * i) + j;
                if (idx >= this.popularList.size()) {
                    row.addView(new LinearLayout(this));
                } else {
                    img.setTag(this.datamap.get(this.popularList.get(idx)));
                    img.setOnClickListener(this.chooseImageClickListenner);
                    img.getLayoutParams().width = (this.dateTable.getWidth() / this.eachRowNum) - 5;
                    img.getLayoutParams().height = (img.getLayoutParams().width * 3) / 4;
                    ((ArrayList) this.datamap.get(this.popularList.get((this.eachRowNum * i) + j)).get("images")).add(img);
                    ((TextView) singleData.findViewById(R.id.downtimes)).setText(String.format("下载次数:%s", this.datamap.get(this.popularList.get((this.eachRowNum * i) + j)).get("downtimes")));
                    row.addView(singleData);
                    row.setGravity(1);
                    if (this.datamap.get(this.popularList.get((this.eachRowNum * i) + j)).containsKey("bmp")) {
                        redrawImage(this.datamap.get(this.popularList.get((this.eachRowNum * i) + j)));
                    }
                }
            }
            this.popularTable.addView(row);
        }
    }

    /* access modifiers changed from: protected */
    public void refreshDateTable() {
        int totalrows = this.dateList.size() / this.eachRowNum;
        if (this.eachRowNum * totalrows < this.dateList.size()) {
            totalrows++;
        }
        for (int i = this.dateTable.getChildCount(); i < totalrows; i++) {
            TableRow row = new TableRow(this);
            for (int j = 0; j < this.eachRowNum; j++) {
                LinearLayout singleData = (LinearLayout) LayoutInflater.from(this).inflate((int) R.layout.singledata, (ViewGroup) null);
                ImageView img = (ImageView) singleData.findViewById(R.id.singleimg);
                int idx = (this.eachRowNum * i) + j;
                if (idx >= this.dateList.size()) {
                    row.addView(new LinearLayout(this));
                } else {
                    img.setTag(this.datamap.get(this.dateList.get(idx)));
                    img.setOnClickListener(this.chooseImageClickListenner);
                    img.getLayoutParams().width = (this.dateTable.getWidth() / this.eachRowNum) - 5;
                    img.getLayoutParams().height = (img.getLayoutParams().width * 3) / 4;
                    ((ArrayList) this.datamap.get(this.dateList.get((this.eachRowNum * i) + j)).get("images")).add(img);
                    ((TextView) singleData.findViewById(R.id.downtimes)).setText(String.format("下载次数:%s", this.datamap.get(this.dateList.get((this.eachRowNum * i) + j)).get("downtimes")));
                    row.addView(singleData);
                    if (this.datamap.get(this.dateList.get((this.eachRowNum * i) + j)).containsKey("bmp")) {
                        redrawImage(this.datamap.get(this.dateList.get((this.eachRowNum * i) + j)));
                    }
                }
            }
            this.dateTable.addView(row);
        }
    }

    public void onBackPressed() {
        finish();
    }
}
