package net.yebaihe.finditnet;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;
import android.widget.ViewSwitcher;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import net.yebaihe.sdk.SdkUtils;

public class FindItGame extends Activity implements GestureDetector.OnGestureListener {
    private Bitmap bmpmask;
    /* access modifiers changed from: private */
    public int curPage = 0;
    private GestureDetector gestureDetector;
    private ImageView pagehint;
    private TableLayout tablea;
    private TableLayout tableb;
    private int totalPages;
    /* access modifiers changed from: private */
    public ViewSwitcher vs;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.game);
        this.vs = (ViewSwitcher) findViewById(R.id.viewSwitcher1);
        this.tablea = (TableLayout) findViewById(R.id.tablea);
        this.tableb = (TableLayout) findViewById(R.id.tableb);
        this.pagehint = (ImageView) findViewById(R.id.imageView1);
        this.bmpmask = BitmapFactory.decodeResource(getResources(), R.drawable.p5slt);
        this.gestureDetector = new GestureDetector(this);
        MyDbHelper m_Helper = new MyDbHelper(this, MyDbHelper.DB_NAME, null, 2);
        SQLiteDatabase db = m_Helper.getReadableDatabase();
        Cursor result = db.rawQuery("select count(*) from files", null);
        result.moveToFirst();
        int total = result.getInt(0);
        db.close();
        m_Helper.close();
        result.close();
        if (total > 0) {
            Toast.makeText(this, "正在加载，长按图片删除", 1).show();
        }
        new Handler().postDelayed(new Runnable() {
            public void run() {
                FindItGame.this.prepareTable((TableLayout) FindItGame.this.vs.getCurrentView(), 0);
            }
        }, 1000);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        new Handler().postDelayed(new Runnable() {
            public void run() {
                FindItGame.this.prepareTable((TableLayout) FindItGame.this.vs.getCurrentView(), FindItGame.this.curPage);
            }
        }, 200);
    }

    /* access modifiers changed from: private */
    public void prepareTable(TableLayout table, int pageidx) {
        table.removeAllViews();
        table.setStretchAllColumns(false);
        int width = (Math.min(this.vs.getCurrentView().getWidth(), this.vs.getCurrentView().getHeight()) / 3) - 5;
        int height = (width * 3) / 4;
        int colnums = this.vs.getCurrentView().getWidth() / width;
        int rownums = this.vs.getCurrentView().getHeight() / height;
        if (rownums < 0) {
            rownums = 0;
        }
        MyDbHelper myDbHelper = new MyDbHelper(this, MyDbHelper.DB_NAME, null, 2);
        SQLiteDatabase db = myDbHelper.getReadableDatabase();
        Cursor result = db.rawQuery("select count(*) from files", null);
        result.moveToFirst();
        int total = result.getInt(0);
        result.close();
        if (total == 0) {
            Toast.makeText(this, "没有找到任何关卡，请先下载！", 1).show();
            finish();
            return;
        }
        int pagenum = rownums * colnums;
        Cursor result2 = db.rawQuery("select * from files limit " + (pageidx * pagenum) + "," + pagenum, null);
        boolean valid = result2.moveToFirst();
        int idx = 0;
        TableRow row = null;
        boolean lastLevelOk = false;
        TableRow.LayoutParams params = null;
        ArrayList<Integer> needremoves = new ArrayList<>();
        while (valid) {
            if (!new File(String.valueOf(FindItDownloadDetail.getBaseDir()) + result2.getString(1) + ".dat").exists()) {
                needremoves.add(Integer.valueOf(result2.getInt(0)));
                valid = result2.moveToNext();
            } else {
                if (idx % colnums == 0) {
                    row = new TableRow(this);
                    row.setGravity(17);
                    table.addView(row);
                }
                ImageView imageView = new ImageView(this);
                imageView.setImageBitmap(getHintBmp(result2.getString(1), result2));
                params = new TableRow.LayoutParams(width, height, 1.0f);
                params.setMargins(0, 0, 5, 5);
                row.addView(imageView, params);
                int imgidx = (this.curPage * pagenum) + idx;
                if (result2.getInt(2) == 1 || lastLevelOk || (idx == 0 && pageidx == 0)) {
                    imageView.setTag(Integer.valueOf(imgidx));
                } else {
                    imageView.setTag(Integer.valueOf(imgidx * -1));
                }
                lastLevelOk = result2.getInt(2) == 1;
                valid = result2.moveToNext();
                idx++;
            }
        }
        if (row != null) {
            int totalleft = colnums - row.getChildCount();
            for (int i = 0; i < totalleft; i++) {
                row.addView(new LinearLayout(this), params);
            }
        }
        for (int i2 = 0; i2 < rownums - table.getChildCount(); i2++) {
            table.addView(new TableRow(this));
        }
        for (int i3 = 0; i3 < needremoves.size(); i3++) {
            db.delete(MyDbHelper.TB_NAME, "_id=" + needremoves.get(i3), null);
        }
        db.close();
        myDbHelper.close();
        updatePageHint(colnums * rownums, total);
    }

    public void onBackPressed() {
        finish();
    }

    private Bitmap getHintBmp(String id, Cursor rec) {
        try {
            DataInputStream in = new DataInputStream(new FileInputStream(String.valueOf(FindItDownloadDetail.getBaseDir()) + id + ".dat"));
            byte[] buffer = new byte[9];
            in.read(buffer);
            if (buffer[8] == 116) {
                in.read(new byte[in.readInt()]);
                int len = in.readInt();
                if (len > in.available()) {
                    return null;
                }
                byte[] cnt = new byte[len];
                in.read(cnt);
                BitmapFactory.Options o2 = new BitmapFactory.Options();
                o2.inSampleSize = 3;
                Bitmap bmp = BitmapFactory.decodeByteArray(cnt, 0, len, o2);
                in.close();
                byte[] cnt2 = null;
                if (rec.getInt(2) == 0) {
                    bmp = SdkUtils.toGrayscale(bmp, true);
                }
                Bitmap ret = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(), Bitmap.Config.ARGB_8888);
                new Canvas(ret).drawBitmap(SdkUtils.getRoundedCornerBitmap(bmp, 20), 0.0f, 0.0f, new Paint());
                bmp.recycle();
                System.gc();
                return ret;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return null;
    }

    public boolean onTouchEvent(MotionEvent event) {
        return this.gestureDetector.onTouchEvent(event);
    }

    private void updatePageHint(int pagenum, int total) {
        this.totalPages = total / pagenum;
        if (this.totalPages * pagenum < total) {
            this.totalPages++;
        }
        int deltawidth = this.pagehint.getHeight();
        Bitmap bmp = Bitmap.createBitmap(this.totalPages * deltawidth, this.pagehint.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmp);
        Paint p = new Paint();
        p.setStrokeWidth(3.0f);
        for (int i = 0; i < this.totalPages; i++) {
            if (i == this.curPage) {
                p.setColor(-2134049024);
            } else {
                p.setColor(-2138272628);
            }
            c.drawCircle((float) ((deltawidth * i) + (deltawidth / 2)), (float) (this.pagehint.getHeight() / 2), (float) (this.pagehint.getHeight() / 2), p);
        }
        this.pagehint.getLayoutParams().width = this.totalPages * deltawidth;
        this.pagehint.setImageBitmap(bmp);
        Log.d("myown", String.format("total width:%d pagehint width:%d height:%d", Integer.valueOf(this.tablea.getWidth()), Integer.valueOf(this.pagehint.getLayoutParams().width), Integer.valueOf(this.pagehint.getLayoutParams().height)));
    }

    public boolean onDown(MotionEvent arg0) {
        return false;
    }

    public boolean onFling(MotionEvent e1, MotionEvent e2, float arg2, float arg3) {
        int page;
        int page2 = this.curPage;
        if (e1.getX() < e2.getX()) {
            page = page2 - 1;
            if (page < 0) {
                page = 0;
            }
        } else {
            page = page2 + 1;
            if (page >= this.totalPages) {
                page = this.totalPages - 1;
            }
        }
        if (page == this.curPage) {
            return false;
        }
        TableLayout table = getOtherTable();
        if (page > this.curPage) {
            this.vs.setInAnimation(this, R.anim.left_in);
            this.vs.setOutAnimation(this, R.anim.left_out);
            Toast.makeText(this, "下一页", 0).show();
        } else {
            this.vs.setInAnimation(this, R.anim.right_in);
            this.vs.setOutAnimation(this, R.anim.right_out);
            Toast.makeText(this, "上一页", 0).show();
        }
        this.curPage = page;
        prepareTable(table, page);
        this.vs.showNext();
        return false;
    }

    private TableLayout getOtherTable() {
        if (this.vs.getCurrentView() == this.tablea) {
            return this.tableb;
        }
        return this.tablea;
    }

    public void onLongPress(MotionEvent e) {
        TableLayout table = (TableLayout) this.vs.getCurrentView();
        for (int i = 0; i < table.getChildCount(); i++) {
            TableRow row = (TableRow) table.getChildAt(i);
            int j = 0;
            while (j < row.getChildCount()) {
                ImageView v = (ImageView) row.getChildAt(j);
                Rect r = new Rect();
                if (!v.getGlobalVisibleRect(r) || !r.contains((int) e.getX(), (int) e.getY())) {
                    j++;
                } else {
                    LongPressImageBtn(v);
                    return;
                }
            }
        }
    }

    private void LongPressImageBtn(ImageView v) {
        final Integer recidx = Integer.valueOf(Math.abs(((Integer) v.getTag()).intValue()));
        if (recidx.intValue() >= 0) {
            new AlertDialog.Builder(this).setIcon((int) R.drawable.icon).setTitle("您想要删除这个关卡吗？").setPositiveButton("是的，请删除", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    MyDbHelper m_Helper = new MyDbHelper(FindItGame.this, MyDbHelper.DB_NAME, null, 2);
                    SQLiteDatabase db = m_Helper.getWritableDatabase();
                    Cursor result = db.rawQuery("select * from files limit " + recidx + ",1", null);
                    result.moveToFirst();
                    File f = new File(String.valueOf(FindItDownloadDetail.getBaseDir()) + result.getString(1) + ".dat");
                    int id = result.getInt(0);
                    result.close();
                    if (f.exists()) {
                        f.delete();
                    }
                    db.delete(MyDbHelper.TB_NAME, "_id=" + id, null);
                    db.close();
                    m_Helper.close();
                    FindItGame.this.prepareTable((TableLayout) FindItGame.this.vs.getCurrentView(), FindItGame.this.curPage);
                }
            }).create().show();
        }
    }

    public boolean onScroll(MotionEvent arg0, MotionEvent arg1, float arg2, float arg3) {
        return false;
    }

    public void onShowPress(MotionEvent e) {
    }

    public boolean onSingleTapUp(MotionEvent e) {
        TableLayout table = (TableLayout) this.vs.getCurrentView();
        for (int i = 0; i < table.getChildCount(); i++) {
            TableRow row = (TableRow) table.getChildAt(i);
            for (int j = 0; j < row.getChildCount(); j++) {
                if (row.getChildAt(j) instanceof ImageView) {
                    ImageView v = (ImageView) row.getChildAt(j);
                    Rect r = new Rect();
                    if (v.getGlobalVisibleRect(r) && r.contains((int) e.getX(), (int) e.getY())) {
                        ClickImageBtn(v);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private void ClickImageBtn(ImageView v) {
        if (((Integer) v.getTag()).intValue() >= 0) {
            Intent i = new Intent(this, FindItGamePlay.class);
            i.putExtra("idx", (Integer) v.getTag());
            startActivity(i);
            return;
        }
        Toast.makeText(this, "您需要先过完前面的关卡", 1).show();
    }
}
