package net.yebaihe.finditnet;

public interface ScrollViewListener {
    void onScrollChanged(ObservableScrollView observableScrollView, int i, int i2, int i3, int i4);
}
