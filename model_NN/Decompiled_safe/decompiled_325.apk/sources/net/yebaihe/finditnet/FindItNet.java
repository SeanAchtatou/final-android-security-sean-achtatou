package net.yebaihe.finditnet;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import net.yebaihe.sdk.SdkUtils;

public class FindItNet extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        ((TextView) findViewById(R.id.idversion)).setText("版本:" + SdkUtils.getAppVersionName(this));
        ((Button) findViewById(R.id.btnStart)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FindItNet.this.startActivity(new Intent(FindItNet.this, FindItGame.class));
            }
        });
        ((Button) findViewById(R.id.btnDownload)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FindItNet.this.startActivity(new Intent(FindItNet.this, FindItDownload.class));
            }
        });
    }

    public void onBackPressed() {
        finish();
    }
}
