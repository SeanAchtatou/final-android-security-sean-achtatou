package net.yebaihe.finditnet;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import cn.domob.android.ads.DomobAdManager;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import net.yebaihe.sdk.Base64;
import net.yebaihe.sdk.SdkUtils;

public class FindItGamePlay extends Activity implements View.OnTouchListener {
    /* access modifiers changed from: private */
    public Bitmap bitmapRes1;
    private Bitmap bitmapRes2;
    private Bitmap bitmapegg;
    private ArrayList<ImageView> btns = new ArrayList<>();
    /* access modifiers changed from: private */
    public boolean clickable;
    private String dataid;
    private ImageView djgimg;
    /* access modifiers changed from: private */
    public Handler djsHandler;
    /* access modifiers changed from: private */
    public int djsNum;
    /* access modifiers changed from: private */
    public Runnable djsRunnable;
    /* access modifiers changed from: private */
    public TextView eggText;
    /* access modifiers changed from: private */
    public View egglayout;
    private ImageView findind1;
    private ImageView findind2;
    private ImageView findind3;
    private View finditlayout;
    private TextView findittitle;
    /* access modifiers changed from: private */
    public ImageView img01;
    /* access modifiers changed from: private */
    public ImageView img02;
    private int recidx;
    private SoundPool snd;
    private int sndbad;
    private int sndegg;
    private int sndgood;
    private String startid = "";
    private boolean succInited = false;
    /* access modifiers changed from: private */
    public ArrayList<Rect> tagRects = new ArrayList<>();
    private String thisid = "";
    private ArrayList<Rect> usedRects = new ArrayList<>();

    public void onBackPressed() {
        finish();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.findit);
        try {
            byte[] b = Base64.decode(getString(R.string.eggs).trim());
            this.bitmapegg = BitmapFactory.decodeByteArray(b, 0, b.length);
            this.bitmapegg.setDensity(240);
            ((ImageView) findViewById(R.id.iegg)).setImageBitmap(this.bitmapegg);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.finditlayout = findViewById(R.id.finditlayout);
        this.egglayout = findViewById(R.id.egglayout);
        this.recidx = getIntent().getIntExtra("idx", -1);
        setVolumeControlStream(3);
        this.snd = new SoundPool(2, 3, 0);
        this.sndgood = this.snd.load(this, R.raw.good, 0);
        this.sndbad = this.snd.load(this, R.raw.bad, 0);
        this.djgimg = (ImageView) findViewById(R.id.djsimg);
        this.eggText = (TextView) findViewById(R.id.eggtext);
        ((Button) findViewById(R.id.btntest)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Bitmap bitmap01 = Bitmap.createBitmap(400, 300, Bitmap.Config.ARGB_8888);
                Canvas canvas01 = new Canvas(bitmap01);
                Bitmap bitmap02 = Bitmap.createBitmap(400, 300, Bitmap.Config.ARGB_8888);
                Canvas canvas02 = new Canvas(bitmap02);
                Bitmap tmpbitmap = FindItGamePlay.this.bitmapRes1;
                canvas01.drawBitmap(tmpbitmap, new Rect(0, 0, 400, 300), new Rect(0, 0, 400, 300), new Paint());
                canvas02.drawBitmap(tmpbitmap, new Rect(0, 0, 400, 300), new Rect(0, 0, 400, 300), new Paint());
                Paint p = new Paint();
                p.setFilterBitmap(true);
                p.setStyle(Paint.Style.STROKE);
                p.setColor(-65536);
                p.setStrokeWidth(3.0f);
                for (int i = 0; i < FindItGamePlay.this.tagRects.size(); i++) {
                    canvas01.drawRect((Rect) FindItGamePlay.this.tagRects.get(i), p);
                    canvas02.drawRect((Rect) FindItGamePlay.this.tagRects.get(i), p);
                }
                FindItGamePlay.this.img01.setImageBitmap(bitmap01);
                FindItGamePlay.this.img02.setImageBitmap(bitmap02);
                FindItGamePlay.this.img01.invalidate();
                FindItGamePlay.this.img02.invalidate();
            }
        });
        this.findittitle = (TextView) findViewById(R.id.findittitle);
        this.findind1 = (ImageView) findViewById(R.id.findind1);
        this.findind2 = (ImageView) findViewById(R.id.findind2);
        this.findind3 = (ImageView) findViewById(R.id.findind3);
        this.img01 = (ImageView) findViewById(R.id.img01);
        this.img02 = (ImageView) findViewById(R.id.img02);
        this.img01.setOnTouchListener(this);
        this.img02.setOnTouchListener(this);
        this.djsHandler = new Handler();
        this.djsRunnable = new Runnable() {
            public void run() {
                FindItGamePlay findItGamePlay = FindItGamePlay.this;
                findItGamePlay.djsNum = findItGamePlay.djsNum - 1;
                FindItGamePlay.this.updateDJSImg();
                if (FindItGamePlay.this.djsNum <= 0) {
                    FindItGamePlay.this.clickable = false;
                    AlertDialog.Builder builder = new AlertDialog.Builder(FindItGamePlay.this);
                    builder.setIcon((int) R.drawable.icon);
                    builder.setTitle("呵呵");
                    builder.setMessage("午时三刻已到");
                    builder.setPositiveButton("重新开始", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            FindItGamePlay.this.startFind();
                        }
                    });
                    builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        public void onCancel(DialogInterface dialog) {
                        }
                    });
                    builder.show();
                    return;
                }
                FindItGamePlay.this.djsHandler.postDelayed(FindItGamePlay.this.djsRunnable, 1000);
            }
        };
        if (prepareResource()) {
            startFind();
        }
    }

    /* access modifiers changed from: private */
    public boolean prepareResource() {
        MyDbHelper m_Helper = new MyDbHelper(this, MyDbHelper.DB_NAME, null, 2);
        SQLiteDatabase db = m_Helper.getReadableDatabase();
        Cursor result = db.rawQuery("select * from files limit " + this.recidx + ",1", null);
        this.dataid = "";
        if (result.moveToFirst()) {
            this.dataid = result.getString(1);
            db.close();
            m_Helper.close();
            if (!new File(String.valueOf(FindItDownloadDetail.getBaseDir()) + this.dataid + ".dat").exists()) {
                Toast.makeText(this, "数据文件已被损坏", 1).show();
                return false;
            }
            try {
                DataInputStream in = new DataInputStream(new FileInputStream(String.valueOf(FindItDownloadDetail.getBaseDir()) + this.dataid + ".dat"));
                byte[] buffer = new byte[9];
                in.read(buffer);
                if (buffer[8] == 116) {
                    byte[] cnt = new byte[in.readInt()];
                    in.read(cnt);
                    updateRects(new String(cnt, "UTF-8"));
                    int len = in.readInt();
                    byte[] cnt2 = new byte[len];
                    in.read(cnt2);
                    this.bitmapRes1 = BitmapFactory.decodeByteArray(cnt2, 0, len);
                    int len2 = in.readInt();
                    byte[] cnt3 = new byte[len2];
                    in.read(cnt3);
                    this.bitmapRes2 = BitmapFactory.decodeByteArray(cnt3, 0, len2);
                    in.read(buffer);
                    if (buffer[8] == 116) {
                        this.succInited = true;
                        Log.d("myown", "resource indentied!");
                    }
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            return true;
        }
        Toast.makeText(this, "您已经过了所有关卡，再下载一些吧！", 1).show();
        db.close();
        m_Helper.close();
        return false;
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.djsHandler.removeCallbacks(this.djsRunnable);
    }

    /* access modifiers changed from: protected */
    public void updateDJSImg() {
        Bitmap bitmap = Bitmap.createBitmap(70, 70, Bitmap.Config.ARGB_8888);
        Paint p = new Paint();
        p.setStyle(Paint.Style.FILL);
        p.setColor(-65536);
        p.setStrokeWidth(3.0f);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawArc(new RectF(0.0f, 0.0f, 70.0f, 70.0f), 0.0f, (float) (this.djsNum * 6), true, p);
        Paint p2 = new Paint();
        p2.setTextSize(30.0f);
        canvas.drawText(String.format("%02d", Integer.valueOf(this.djsNum)), 19.0f, 47.0f, p2);
        this.djgimg.setImageBitmap(bitmap);
        this.djgimg.invalidate();
    }

    /* access modifiers changed from: private */
    public void startFind() {
        randomDifference();
        this.djsNum = 60;
        this.djsHandler.removeCallbacks(this.djsRunnable);
        this.djsHandler.postDelayed(this.djsRunnable, 1000);
        this.findind1.setImageResource(R.drawable.mzd);
        this.findind1.setTag(0);
        this.findind2.setImageResource(R.drawable.mzd);
        this.findind2.setTag(0);
        this.findind3.setImageResource(R.drawable.mzd);
        this.findind3.setTag(0);
        redrawImg0102();
        this.clickable = true;
    }

    private void randomDifference() {
        this.usedRects.clear();
        for (int i = 0; i < this.tagRects.size(); i++) {
            this.usedRects.add(this.tagRects.get(i));
        }
        int needRemove = this.tagRects.size() - 3;
        for (int i2 = 0; i2 < needRemove; i2++) {
            this.usedRects.remove(SdkUtils.getRandomInt(0, this.tagRects.size()) % this.usedRects.size());
        }
    }

    private void updateRects(String myText) {
        String[] tags = myText.trim().split("\n");
        this.tagRects.clear();
        for (int i = 0; i < tags.length; i++) {
            if (tags[i].length() > 0) {
                String[] numbers = tags[i].trim().split(" ");
                if (numbers.length == 4) {
                    this.tagRects.add(new Rect(Integer.parseInt(numbers[0].trim()), Integer.parseInt(numbers[1].trim()), Integer.parseInt(numbers[2].trim()) + Integer.parseInt(numbers[0].trim()), Integer.parseInt(numbers[3].trim()) + Integer.parseInt(numbers[1].trim())));
                }
            }
        }
    }

    private void redrawImg0102() {
        Bitmap bitmap01 = Bitmap.createBitmap(400, 300, Bitmap.Config.ARGB_8888);
        Canvas canvas01 = new Canvas(bitmap01);
        Bitmap bitmap02 = Bitmap.createBitmap(400, 300, Bitmap.Config.ARGB_8888);
        Canvas canvas02 = new Canvas(bitmap02);
        Bitmap tmpbitmap = this.bitmapRes1;
        canvas01.drawBitmap(tmpbitmap, new Rect(0, 0, 400, 300), new Rect(0, 0, 400, 300), new Paint());
        canvas02.drawBitmap(tmpbitmap, new Rect(0, 0, 400, 300), new Rect(0, 0, 400, 300), new Paint());
        Bitmap tmpbitmap2 = this.bitmapRes2;
        Paint p = new Paint();
        p.setFilterBitmap(true);
        for (int i = 0; i < this.usedRects.size(); i++) {
            canvas02.drawBitmap(tmpbitmap2, this.usedRects.get(i), this.usedRects.get(i), p);
        }
        p.setStyle(Paint.Style.STROKE);
        p.setColor(-65536);
        p.setStrokeWidth(3.0f);
        if (((Integer) this.findind1.getTag()).intValue() == 1) {
            canvas01.drawRect(this.usedRects.get(0), p);
            canvas02.drawRect(this.usedRects.get(0), p);
        }
        if (((Integer) this.findind2.getTag()).intValue() == 1) {
            canvas01.drawRect(this.usedRects.get(1), p);
            canvas02.drawRect(this.usedRects.get(1), p);
        }
        if (((Integer) this.findind3.getTag()).intValue() == 1) {
            canvas01.drawRect(this.usedRects.get(2), p);
            canvas02.drawRect(this.usedRects.get(2), p);
        }
        this.img01.setImageBitmap(bitmap01);
        this.img02.setImageBitmap(bitmap02);
    }

    public boolean onTouch(View arg0, MotionEvent arg1) {
        if (!this.clickable) {
            return false;
        }
        float x = arg1.getX();
        float y = arg1.getY();
        float xo = (400.0f * x) / ((float) arg0.getWidth());
        float yo = (300.0f * y) / ((float) arg0.getHeight());
        boolean goodPoint = false;
        if (this.usedRects.get(0).contains((int) xo, (int) yo)) {
            if (((Integer) this.findind1.getTag()).intValue() != 1) {
                this.findind1.setImageResource(R.drawable.zd);
                this.findind1.setTag(1);
                redrawImg0102();
                this.img01.invalidate(this.usedRects.get(0));
                this.img02.invalidate(this.usedRects.get(0));
            }
            goodPoint = true;
        }
        if (this.usedRects.get(1).contains((int) xo, (int) yo)) {
            if (((Integer) this.findind2.getTag()).intValue() != 1) {
                this.findind2.setImageResource(R.drawable.zd);
                this.findind2.setTag(1);
                redrawImg0102();
            }
            goodPoint = true;
        }
        if (this.usedRects.get(2).contains((int) xo, (int) yo)) {
            if (((Integer) this.findind3.getTag()).intValue() != 1) {
                this.findind3.setImageResource(R.drawable.zd);
                this.findind3.setTag(1);
                redrawImg0102();
            }
            goodPoint = true;
        }
        float vol = getVolume();
        if (goodPoint) {
            this.snd.play(this.sndgood, vol, vol, 0, 0, 1.0f);
        } else {
            this.snd.play(this.sndbad, vol, vol, 0, 0, 1.0f);
            this.djsNum -= 6;
            if (this.djsNum < 1) {
                this.djsNum = 1;
            }
            updateDJSImg();
        }
        if (((Integer) this.findind1.getTag()).intValue() == 1 && ((Integer) this.findind2.getTag()).intValue() == 1 && ((Integer) this.findind3.getTag()).intValue() == 1) {
            this.clickable = false;
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    FindItGamePlay.this.showNextGraph();
                }
            }, 1000);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void showNextGraph() {
        MyDbHelper m_Helper = new MyDbHelper(this, MyDbHelper.DB_NAME, null, 2);
        SQLiteDatabase db = m_Helper.getWritableDatabase();
        db.execSQL("update files set passed=1 where path= '" + this.dataid + "'");
        db.close();
        m_Helper.close();
        this.djsHandler.removeCallbacks(this.djsRunnable);
        this.recidx++;
        if (this.startid.equals("")) {
            this.startid = this.dataid;
        }
        this.thisid = this.dataid;
        if ((this.startid.equals("21") && this.thisid.equals("1")) || (this.startid.equals("1") && this.thisid.equals("21"))) {
            showEgg();
        } else if (prepareResource()) {
            startFind();
        }
    }

    private void showEgg() {
        this.egglayout.requestLayout();
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        this.egglayout.setVisibility(0);
        this.eggText.setVisibility(4);
        this.egglayout.startAnimation(anim);
        anim.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation arg0) {
                Animation anim = AnimationUtils.loadAnimation(FindItGamePlay.this, R.anim.bottom_in);
                anim.setAnimationListener(new Animation.AnimationListener() {
                    public void onAnimationEnd(Animation animation) {
                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                FindItGamePlay.this.egglayout.setVisibility(8);
                                if (FindItGamePlay.this.prepareResource()) {
                                    FindItGamePlay.this.startFind();
                                }
                            }
                        }, 10000);
                    }

                    public void onAnimationRepeat(Animation animation) {
                    }

                    public void onAnimationStart(Animation animation) {
                    }
                });
                FindItGamePlay.this.eggText.setVisibility(0);
                FindItGamePlay.this.eggText.startAnimation(anim);
            }

            public void onAnimationRepeat(Animation arg0) {
            }

            public void onAnimationStart(Animation arg0) {
            }
        });
    }

    private float getVolume() {
        AudioManager audioManager = (AudioManager) getSystemService(DomobAdManager.ACTION_AUDIO);
        int volume = audioManager.getStreamVolume(3);
        int max = audioManager.getStreamMaxVolume(3);
        if (max > 0) {
            return ((float) volume) / ((float) max);
        }
        return 1.0f;
    }
}
