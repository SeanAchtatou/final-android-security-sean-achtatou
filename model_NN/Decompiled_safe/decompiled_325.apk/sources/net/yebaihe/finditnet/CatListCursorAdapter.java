package net.yebaihe.finditnet;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import cn.domob.android.ads.DomobAdDataItem;
import java.util.List;
import java.util.Map;

public class CatListCursorAdapter extends SimpleAdapter {
    private int layout;
    private Context mCtx;
    private List<Map<String, Object>> mData;
    private LayoutInflater mInflater;

    public CatListCursorAdapter(Context context, List<Map<String, Object>> list, int layout2, String[] from, int[] to) {
        super(context, list, layout2, from, to);
        this.mCtx = context;
        this.mData = list;
        this.layout = layout2;
        this.mInflater = LayoutInflater.from(context);
    }

    public View getView(int position, View view, ViewGroup parent) {
        View view2 = this.mInflater.inflate(this.layout, (ViewGroup) null);
        ((TextView) view2.findViewById(R.id.idtitle)).setText(this.mData.get(position).get("name").toString());
        ((TextView) view2.findViewById(R.id.idtotal)).setText("共" + this.mData.get(position).get("total").toString() + "关卡");
        ((TextView) view2.findViewById(R.id.idlastupdate)).setText("最后更新：" + this.mData.get(position).get("lastupdate").toString());
        ImageView i = (ImageView) view2.findViewById(R.id.imgcat);
        if (this.mData.get(position).containsKey(DomobAdDataItem.IMAGE_TYPE)) {
            i.setImageBitmap((Bitmap) this.mData.get(position).get(DomobAdDataItem.IMAGE_TYPE));
        }
        return view2;
    }
}
