package net.yebaihe.finditnet;

public final class R {

    public static final class anim {
        public static final int bottom_in = 2130968576;
        public static final int fade_in = 2130968577;
        public static final int fade_out = 2130968578;
        public static final int left_in = 2130968579;
        public static final int left_out = 2130968580;
        public static final int right_in = 2130968581;
        public static final int right_out = 2130968582;
    }

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771970;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771972;
        public static final int spots = 2130771971;
    }

    public static final class drawable {
        public static final int bd = 2130837504;
        public static final int bg = 2130837505;
        public static final int bg2a = 2130837506;
        public static final int bg2c = 2130837507;
        public static final int bg5 = 2130837508;
        public static final int btnp2 = 2130837509;
        public static final int btnp21 = 2130837510;
        public static final int bydatebg = 2130837511;
        public static final int bypopularbg = 2130837512;
        public static final int dh = 2130837513;
        public static final int download = 2130837514;
        public static final int dx = 2130837515;
        public static final int ic_pulltorefresh_arrow = 2130837516;
        public static final int icon = 2130837517;
        public static final int mzd = 2130837518;
        public static final int new1 = 2130837519;
        public static final int p1 = 2130837520;
        public static final int p2a = 2130837521;
        public static final int p2b = 2130837522;
        public static final int p2c = 2130837523;
        public static final int p2d = 2130837524;
        public static final int p31 = 2130837525;
        public static final int p32 = 2130837526;
        public static final int p33 = 2130837527;
        public static final int p34 = 2130837528;
        public static final int p3m = 2130837529;
        public static final int p3top = 2130837530;
        public static final int p401 = 2130837531;
        public static final int p4a = 2130837532;
        public static final int p4b = 2130837533;
        public static final int p4top1 = 2130837534;
        public static final int p4top2 = 2130837535;
        public static final int p4top3 = 2130837536;
        public static final int p4top4 = 2130837537;
        public static final int p5slt = 2130837538;
        public static final int p5top = 2130837539;
        public static final int pull_to_refresh_header_background = 2130837540;
        public static final int singlebg = 2130837541;
        public static final int webviewbg = 2130837542;
        public static final int zd = 2130837543;
    }

    public static final class id {
        public static final int btnDownload = 2131165233;
        public static final int btnStart = 2131165232;
        public static final int btndownloadnow = 2131165206;
        public static final int btnnext = 2131165212;
        public static final int btnselall = 2131165207;
        public static final int btntest = 2131165213;
        public static final int datetable = 2131165186;
        public static final int djsimg = 2131165210;
        public static final int downtimes = 2131165242;
        public static final int egglayout = 2131165219;
        public static final int eggtext = 2131165222;
        public static final int findind1 = 2131165214;
        public static final int findind2 = 2131165215;
        public static final int findind3 = 2131165216;
        public static final int finditlayout = 2131165209;
        public static final int findittitle = 2131165211;
        public static final int flayout = 2131165226;
        public static final int frameLayout1 = 2131165200;
        public static final int framelayout = 2131165208;
        public static final int idProgressTxt = 2131165236;
        public static final int idcatlist = 2131165201;
        public static final int idlastupdate = 2131165198;
        public static final int idprogressbar = 2131165202;
        public static final int idtitle = 2131165194;
        public static final int idtotal = 2131165197;
        public static final int idversion = 2131165234;
        public static final int iegg = 2131165220;
        public static final int ilogo = 2131165231;
        public static final int imageView1 = 2131165195;
        public static final int imageView2 = 2131165193;
        public static final int imageView3 = 2131165199;
        public static final int img01 = 2131165217;
        public static final int img02 = 2131165218;
        public static final int imgcat = 2131165190;
        public static final int inewimage = 2131165229;
        public static final int lbutton = 2131165228;
        public static final int linearLayout1 = 2131165185;
        public static final int linearLayout2 = 2131165196;
        public static final int linearLayout3 = 2131165191;
        public static final int linearLayout4 = 2131165192;
        public static final int linearLayout5 = 2131165189;
        public static final int lprogressBar = 2131165230;
        public static final int lwebview = 2131165227;
        public static final int popularTable = 2131165188;
        public static final int progressBar1 = 2131165235;
        public static final int progressBarJustForShow = 2131165187;
        public static final int pull_to_refresh_image = 2131165238;
        public static final int pull_to_refresh_progress = 2131165237;
        public static final int pull_to_refresh_text = 2131165239;
        public static final int pull_to_refresh_updated_at = 2131165240;
        public static final int scrollView1 = 2131165221;
        public static final int scrollbox = 2131165184;
        public static final int singleimg = 2131165241;
        public static final int tab1 = 2131165204;
        public static final int tab2 = 2131165205;
        public static final int tabhost = 2131165203;
        public static final int tablea = 2131165224;
        public static final int tableb = 2131165225;
        public static final int viewSwitcher1 = 2131165223;
    }

    public static final class layout {
        public static final int bydate = 2130903040;
        public static final int bydatelist = 2130903041;
        public static final int bypopular = 2130903042;
        public static final int bypopularlist = 2130903043;
        public static final int catrow = 2130903044;
        public static final int download = 2130903045;
        public static final int downloaddetail = 2130903046;
        public static final int findit = 2130903047;
        public static final int game = 2130903048;
        public static final int lilysdkmain = 2130903049;
        public static final int main = 2130903050;
        public static final int progress = 2130903051;
        public static final int pull_to_refresh_header = 2130903052;
        public static final int singledata = 2130903053;
    }

    public static final class raw {
        public static final int bad = 2131034112;
        public static final int good = 2131034113;
    }

    public static final class string {
        public static final int LilySdkTurnTo = 2131099650;
        public static final int app_name = 2131099649;
        public static final int eggs = 2131099656;
        public static final int hello = 2131099648;
        public static final int mywords = 2131099655;
        public static final int pull_to_refresh_pull_label = 2131099651;
        public static final int pull_to_refresh_refreshing_label = 2131099653;
        public static final int pull_to_refresh_release_label = 2131099652;
        public static final int pull_to_refresh_tap_label = 2131099654;
    }

    public static final class styleable {
        public static final int[] cn_domob_android_ads_DomobAdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.keywords, R.attr.spots, R.attr.refreshInterval};
        public static final int cn_domob_android_ads_DomobAdView_backgroundColor = 0;
        public static final int cn_domob_android_ads_DomobAdView_keywords = 2;
        public static final int cn_domob_android_ads_DomobAdView_primaryTextColor = 1;
        public static final int cn_domob_android_ads_DomobAdView_refreshInterval = 4;
        public static final int cn_domob_android_ads_DomobAdView_spots = 3;
    }
}
