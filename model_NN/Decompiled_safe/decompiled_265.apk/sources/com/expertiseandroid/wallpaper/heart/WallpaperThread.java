package com.expertiseandroid.wallpaper.heart;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import java.util.ArrayList;
import java.util.Iterator;

public class WallpaperThread extends Thread {
    private Bitmap background;
    private Bitmap bigHeart;
    boolean bottomTouched = false;
    int currentPosition = 0;
    private long currentTime;
    int height;
    boolean init = false;
    boolean leftTouched = false;
    ArrayList<HeartObject> mHeartList = new ArrayList<>();
    Paint paint = new Paint();
    float positionX;
    float positionY;
    private long previousTime;
    boolean rightTouched = false;
    private boolean run;
    private Bitmap smallHeart;
    private SurfaceHolder surfaceHolder;
    boolean topTouched = false;
    private boolean wait;
    int width;

    public WallpaperThread(SurfaceHolder surfaceHolder2, Context context) {
        this.surfaceHolder = surfaceHolder2;
        this.wait = true;
        this.background = BitmapFactory.decodeResource(context.getResources(), R.drawable.back);
        this.smallHeart = BitmapFactory.decodeResource(context.getResources(), R.drawable.heart_small);
        this.bigHeart = BitmapFactory.decodeResource(context.getResources(), R.drawable.heart);
    }

    public void pausePainting() {
        this.wait = true;
        synchronized (this) {
            notify();
        }
    }

    public void resumePainting() {
        this.wait = false;
        synchronized (this) {
            notify();
        }
    }

    public void stopPainting() {
        this.run = false;
        synchronized (this) {
            notify();
        }
    }

    public void run() {
        boolean z;
        this.run = true;
        while (this.run) {
            synchronized (this.surfaceHolder) {
                this.currentTime = System.currentTimeMillis();
                if (this.currentTime - this.previousTime > 600) {
                    Canvas c = this.surfaceHolder.lockCanvas(null);
                    HeartObject hr = new HeartObject();
                    hr.bitmap = this.smallHeart;
                    hr.y = (float) (this.surfaceHolder.getSurfaceFrame().bottom - 30);
                    hr.x = ((float) Math.random()) * ((float) this.surfaceHolder.getSurfaceFrame().right);
                    this.mHeartList.add(hr);
                    doDraw(c);
                    ArrayList<HeartObject> tmp = new ArrayList<>();
                    Iterator<HeartObject> it = this.mHeartList.iterator();
                    while (it.hasNext()) {
                        HeartObject obj = it.next();
                        obj.y -= 10.0f;
                        if (obj.left) {
                            obj.x -= 3.0f;
                        } else {
                            obj.x += 3.0f;
                        }
                        if (obj.left) {
                            z = false;
                        } else {
                            z = true;
                        }
                        obj.left = z;
                        if (obj.y > 30.0f) {
                            tmp.add(obj);
                        }
                    }
                    this.mHeartList = tmp;
                    this.previousTime = this.currentTime;
                    this.surfaceHolder.unlockCanvasAndPost(c);
                }
            }
            synchronized (this) {
                if (this.wait) {
                    try {
                        wait();
                    } catch (Exception e) {
                    }
                }
            }
        }
    }

    public void setSurfaceSize(int width2, int height2) {
        this.width = width2;
        this.height = height2;
        synchronized (this) {
            notify();
        }
    }

    public void doTouchEvent(MotionEvent event) {
        this.positionX = event.getX();
        this.positionY = event.getY();
        if (event.getAction() == 0) {
            Iterator<HeartObject> it = this.mHeartList.iterator();
            while (it.hasNext()) {
                HeartObject obj = it.next();
                float minV = obj.x;
                float maxV = minV + ((float) this.smallHeart.getWidth());
                float minH = obj.y;
                float maxH = minH + ((float) this.smallHeart.getHeight());
                if (this.positionX > minV && this.positionX < maxV && this.positionY > minH && this.positionY < maxH) {
                    obj.bitmap = this.bigHeart;
                    if (obj.big) {
                        obj.filter = true;
                    } else {
                        obj.big = true;
                    }
                }
            }
        }
        this.wait = false;
        synchronized (this) {
            notify();
        }
    }

    private void doDraw(Canvas canvas) {
        canvas.save();
        canvas.drawColor(-16777216);
        Paint p = new Paint();
        canvas.drawBitmap(this.background, 0.0f, 25.0f, p);
        Iterator<HeartObject> it = this.mHeartList.iterator();
        while (it.hasNext()) {
            HeartObject obj = it.next();
            if (!obj.filter) {
                canvas.drawBitmap(obj.bitmap, obj.x, obj.y, p);
            }
        }
        canvas.restore();
    }

    private void updatePhysics() {
    }

    class HeartObject {
        public boolean big = false;
        public Bitmap bitmap;
        public boolean filter = false;
        public boolean left = false;
        public float x;
        public float y;

        HeartObject() {
        }
    }
}
