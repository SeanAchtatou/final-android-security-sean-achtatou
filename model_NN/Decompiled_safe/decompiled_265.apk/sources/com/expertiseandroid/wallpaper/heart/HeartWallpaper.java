package com.expertiseandroid.wallpaper.heart;

import android.service.wallpaper.WallpaperService;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

public class HeartWallpaper extends WallpaperService {
    public WallpaperService.Engine onCreateEngine() {
        return new SampleEngine();
    }

    public void onCreate() {
        super.onCreate();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public class SampleEngine extends WallpaperService.Engine {
        private WallpaperThread painting;

        SampleEngine() {
            super(HeartWallpaper.this);
            this.painting = new WallpaperThread(getSurfaceHolder(), HeartWallpaper.this.getApplicationContext());
        }

        public void onCreate(SurfaceHolder surfaceHolder) {
            super.onCreate(surfaceHolder);
            setTouchEventsEnabled(true);
        }

        public void onDestroy() {
            super.onDestroy();
            this.painting.stopPainting();
        }

        public void onVisibilityChanged(boolean visible) {
            if (visible) {
                this.painting.resumePainting();
            } else {
                this.painting.pausePainting();
            }
        }

        public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            super.onSurfaceChanged(holder, format, width, height);
            this.painting.setSurfaceSize(width, height);
        }

        public void onSurfaceCreated(SurfaceHolder holder) {
            super.onSurfaceCreated(holder);
            this.painting.start();
        }

        public void onSurfaceDestroyed(SurfaceHolder holder) {
            super.onSurfaceDestroyed(holder);
            boolean retry = true;
            this.painting.stopPainting();
            while (retry) {
                try {
                    this.painting.join();
                    retry = false;
                } catch (InterruptedException e) {
                }
            }
        }

        public void onOffsetsChanged(float xOffset, float yOffset, float xStep, float yStep, int xPixels, int yPixels) {
        }

        public void onTouchEvent(MotionEvent event) {
            super.onTouchEvent(event);
            this.painting.doTouchEvent(event);
        }
    }
}
