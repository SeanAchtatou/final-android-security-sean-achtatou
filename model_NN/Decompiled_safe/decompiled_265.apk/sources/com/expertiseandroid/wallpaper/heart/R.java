package com.expertiseandroid.wallpaper.heart;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int back = 2130837504;
        public static final int heart = 2130837505;
        public static final int heart_small = 2130837506;
        public static final int icon = 2130837507;
        public static final int thumb = 2130837508;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class string {
        public static final int app_name = 2131034112;
        public static final int description = 2131034113;
    }

    public static final class xml {
        public static final int wallpaper = 2130968576;
    }
}
