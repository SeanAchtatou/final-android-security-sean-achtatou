package net.youmi.android;

import android.view.View;
import android.widget.RelativeLayout;
import java.util.ArrayList;

final class fg extends RelativeLayout implements ai, ff {
    AdActivity a;
    private co b;
    private ar c;
    private ca d;

    public fg(AdActivity adActivity, ca caVar) {
        super(adActivity);
        this.a = adActivity;
        this.d = caVar;
        this.b = new co(this.a, caVar);
        this.c = new ar(this.a, this, caVar);
        this.b.setId(1010);
        this.c.setId(1009);
        RelativeLayout.LayoutParams a2 = h.a(caVar.b().a());
        a2.addRule(12);
        addView(this.c, a2);
        RelativeLayout.LayoutParams b2 = h.b();
        b2.addRule(2, this.c.getId());
        addView(this.b, b2);
    }

    private void c() {
        try {
            ArrayList a2 = r.a();
            if (a2 != null && a2.size() > 0) {
                r.b(this.a);
            }
        } catch (Exception e) {
        }
        this.a.a();
    }

    public void a() {
        c();
    }

    public View b() {
        return this;
    }

    public void b_() {
        c();
    }
}
