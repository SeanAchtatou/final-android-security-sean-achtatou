package net.youmi.android;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

class ew implements LocationListener {
    private Context a;

    public ew(Context context) {
        if (context == null) {
            throw new NullPointerException();
        }
        this.a = context;
    }

    private void a() {
        try {
            LocationManager locationManager = (LocationManager) this.a.getSystemService("location");
            if (locationManager != null) {
                locationManager.removeUpdates(this);
            }
        } catch (Exception e) {
            f.a(e);
        }
    }

    public void onLocationChanged(Location location) {
        if (location != null) {
            try {
                if (location.getLatitude() != 0.0d || location.getLongitude() != 0.0d) {
                    ei.a(location);
                    a();
                }
            } catch (Exception e) {
                f.a(e);
            }
        }
    }

    public void onProviderDisabled(String str) {
    }

    public void onProviderEnabled(String str) {
    }

    public void onStatusChanged(String str, int i, Bundle bundle) {
    }
}
