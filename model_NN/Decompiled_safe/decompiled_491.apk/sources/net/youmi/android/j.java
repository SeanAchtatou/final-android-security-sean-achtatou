package net.youmi.android;

import java.util.ArrayList;

class j {
    j() {
    }

    static char a(String str) {
        if (str.equals("0000")) {
            return '0';
        }
        if (str.equals("0001")) {
            return '1';
        }
        if (str.equals("0010")) {
            return '2';
        }
        if (str.equals("0011")) {
            return '3';
        }
        if (str.equals("0100")) {
            return '4';
        }
        if (str.equals("0101")) {
            return '5';
        }
        if (str.equals("0110")) {
            return '6';
        }
        if (str.equals("0111")) {
            return '7';
        }
        if (str.equals("1000")) {
            return '8';
        }
        if (str.equals("1001")) {
            return '9';
        }
        if (str.equals("1010")) {
            return 'a';
        }
        if (str.equals("1011")) {
            return 'b';
        }
        if (str.equals("1100")) {
            return 'c';
        }
        if (str.equals("1101")) {
            return 'd';
        }
        if (str.equals("1110")) {
            return 'e';
        }
        return str.equals("1111") ? 'f' : '0';
    }

    static String a(char c) {
        switch (c) {
            case '0':
                return "0000";
            case '1':
                return "0001";
            case '2':
                return "0010";
            case '3':
                return "0011";
            case '4':
                return "0100";
            case '5':
                return "0101";
            case '6':
                return "0110";
            case '7':
                return "0111";
            case '8':
                return "1000";
            case '9':
                return "1001";
            case 'a':
                return "1010";
            case 'b':
                return "1011";
            case 'c':
                return "1100";
            case 'd':
                return "1101";
            case 'e':
                return "1110";
            case 'f':
                return "1111";
            default:
                return "0000";
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.StringBuilder.insert(int, char):java.lang.StringBuilder}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.StringBuilder.insert(int, java.lang.CharSequence):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, java.lang.Object):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, float):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, java.lang.String):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, long):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, char[]):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, int):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, boolean):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, double):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, char):java.lang.StringBuilder} */
    static String a(StringBuilder sb, int i) {
        if (sb == null) {
            return "";
        }
        int i2 = i * 4;
        try {
            if (sb.length() <= 0) {
                for (int i3 = 0; i3 < i2; i3++) {
                    sb.append('0');
                }
            } else if (sb.length() > i2) {
                sb.delete(0, sb.length() - i2);
            } else if (sb.length() < i2) {
                int length = i2 - sb.length();
                for (int i4 = 0; i4 < length; i4++) {
                    sb.insert(0, '0');
                }
            }
            StringBuilder sb2 = new StringBuilder(i);
            String sb3 = sb.toString();
            for (int i5 = 0; i5 < i2; i5 += 4) {
                if (sb.length() >= i5 + 4) {
                    sb2.append(a(sb3.substring(i5, i5 + 4)));
                }
            }
            return sb2.toString();
        } catch (Exception e) {
            return "";
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.StringBuilder.insert(int, char):java.lang.StringBuilder}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.StringBuilder.insert(int, java.lang.CharSequence):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, java.lang.Object):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, float):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, java.lang.String):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, long):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, char[]):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, int):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, boolean):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, double):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, char):java.lang.StringBuilder} */
    static StringBuilder a(String str, int i) {
        StringBuilder sb = new StringBuilder(32);
        if (str != null) {
            String trim = str.trim();
            if (trim.length() > 0) {
                String lowerCase = trim.toLowerCase();
                int length = lowerCase.length();
                ArrayList arrayList = new ArrayList(length);
                for (int i2 = length - 1; i2 >= 0; i2--) {
                    arrayList.add(a(lowerCase.charAt(i2)));
                }
                for (int size = arrayList.size() - 1; size >= 0; size--) {
                    sb.append((String) arrayList.get(size));
                }
            }
        }
        if (sb.length() <= 0) {
            for (int i3 = 0; i3 < i; i3++) {
                sb.append('0');
            }
        } else if (sb.length() > i) {
            sb.delete(0, sb.length() - i);
        } else if (sb.length() < i) {
            int length2 = i - sb.length();
            for (int i4 = 0; i4 < length2; i4++) {
                sb.insert(0, '0');
            }
        }
        return sb;
    }

    static String[] a(String str, String str2) {
        if (str == null || str2 == null) {
            return null;
        }
        return str.split(str2);
    }
}
