package net.youmi.android;

import java.util.Stack;

class d {
    final /* synthetic */ aw a;
    private o b;
    private Stack c = new Stack();
    private em d;
    private Stack e = new Stack();

    d(aw awVar, o oVar) {
        this.a = awVar;
        this.b = oVar;
    }

    /* access modifiers changed from: package-private */
    public em a() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public em a(em emVar) {
        if (!(this.d == null || this.d == emVar)) {
            this.c.push(this.d);
        }
        this.e.clear();
        this.d = emVar;
        if (this.b != null) {
            this.b.a(this);
        }
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public em b() {
        em emVar;
        if (this.e.size() <= 0 || (emVar = (em) this.e.pop()) == null) {
            return null;
        }
        if (this.d != null) {
            this.c.push(this.d);
        }
        this.d = emVar;
        if (this.b != null) {
            this.b.a(this);
        }
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public em c() {
        em emVar;
        if (this.c.size() <= 0 || (emVar = (em) this.c.pop()) == null) {
            return null;
        }
        if (this.d != null) {
            this.e.push(this.d);
        }
        this.d = emVar;
        if (this.b != null) {
            this.b.a(this);
        }
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        return this.d != null;
    }

    /* access modifiers changed from: package-private */
    public boolean e() {
        return this.e.size() > 0;
    }

    /* access modifiers changed from: package-private */
    public boolean f() {
        return this.c.size() > 0;
    }
}
