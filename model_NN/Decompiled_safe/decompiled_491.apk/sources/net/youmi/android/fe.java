package net.youmi.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.Toast;
import java.io.File;

class fe extends FrameLayout implements dl, eb {
    cx a;
    au b;
    boolean c = false;
    df d;
    AdView e;
    int f = 255;
    Activity g;
    dy h;
    Animation i = new AlphaAnimation(0.0f, 1.0f);
    Animation j = new AlphaAnimation(1.0f, 0.0f);

    fe(Activity activity, AdView adView, ca caVar, int i2, int i3, int i4) {
        super(activity);
        this.g = activity;
        this.e = adView;
        this.f = i4;
        int adWidth = this.e.getAdWidth();
        int adHeight = this.e.getAdHeight();
        c(i2);
        en enVar = new en(adWidth, adHeight, caVar);
        this.b = new au(activity, adView, this, adView.getAdWidth(), adView.getAdHeight(), caVar, enVar);
        int adWidth2 = this.e.getAdWidth() - enVar.a();
        int b2 = caVar.a().d().b() + (caVar.a().d().a() * 2);
        this.a = new cx(activity, adView, this, caVar, i3, ((this.e.getAdWidth() - (adWidth2 < b2 ? b2 : adWidth2)) - caVar.a().c().b()) - (caVar.a().c().a() * 2));
        addView(this.a, new FrameLayout.LayoutParams(adWidth, adHeight));
        addView(this.b, new FrameLayout.LayoutParams(this.e.getAdWidth(), this.e.getAdHeight()));
        this.b.bringToFront();
        this.d = new df(activity, adWidth, adHeight, caVar);
        addView(this.d, new FrameLayout.LayoutParams(adWidth, adHeight));
        this.d.bringToFront();
        this.d.setVisibility(8);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
        layoutParams.gravity = 85;
        layoutParams.rightMargin = caVar.a(caVar.a().d().a());
        layoutParams.bottomMargin = caVar.a(caVar.a().d().a());
        this.i.setDuration(800);
        this.j.setDuration(800);
        setFocusable(true);
        setClickable(true);
        setVisibility(8);
        b();
    }

    public void a() {
        this.e.a().c = false;
        try {
            this.d.setVisibility(8);
            this.d.a();
        } catch (Exception e2) {
        }
        try {
            Toast.makeText(this.g, "无法连接服务器,请稍后重试!", 0).show();
        } catch (Exception e3) {
        }
    }

    public void a(int i2) {
        try {
            this.d.a(i2);
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: package-private */
    public void a(cu cuVar) {
        if (cuVar != null && cuVar.i() != null) {
            dp.a = cuVar;
            AdActivity.a(this.g);
        }
    }

    public void a(dw dwVar, String[] strArr) {
        try {
            this.e.a().c = false;
            if (strArr == null) {
                e();
                return;
            }
            try {
                new Thread(new br(this, dwVar)).start();
            } catch (Exception e2) {
            }
            try {
                AdActivity.a(this.g, strArr, dwVar.e(), dwVar.f());
            } catch (Exception e3) {
            }
            try {
                this.d.setVisibility(8);
                this.d.a();
            } catch (Exception e4) {
            }
        } catch (Exception e5) {
        }
    }

    public void a(i iVar) {
        this.e.a().c = false;
        this.e.a().c = false;
        try {
            this.d.setVisibility(8);
            this.d.a();
        } catch (Exception e2) {
        }
        r.c(this.g);
        ay.a(this.g, "开始下载");
        try {
            bw bwVar = new bw();
            bwVar.a = iVar.c();
            bwVar.c = iVar.a();
            bwVar.d = iVar.b();
            k.a(this.g, bwVar, 1);
        } catch (Exception e3) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.youmi.android.be.b(android.content.Context, java.lang.String):void
     arg types: [android.app.Activity, java.lang.String]
     candidates:
      net.youmi.android.be.b(android.app.Activity, java.lang.String):void
      net.youmi.android.be.b(android.content.Context, java.lang.String):void */
    public void a(i iVar, File file, String str) {
        r.d(this.g);
        if (file != null && file.exists()) {
            ay.a(this.g, "下载成功");
            try {
                bw a2 = k.a(this.g, file.getPath(), str, iVar.a(), iVar.b(), iVar.c());
                k.a(this.g, a2, 2);
                k.a(this.g, a2, 3);
            } catch (Exception e2) {
                f.a(e2);
            }
            be.b((Context) this.g, file.getPath());
        }
    }

    public void a(i iVar, em emVar) {
        try {
            this.e.a().c = false;
            if (emVar == null) {
                e();
                return;
            }
            try {
                cu c2 = this.e.c();
                if (c2 == null) {
                    e();
                    return;
                }
                emVar.a(ba.a(emVar.b(), c2.z(), c2.o(), c2.k()));
                c2.a(emVar);
                try {
                    new Thread(new ds(this, c2, iVar)).start();
                } catch (Exception e2) {
                }
                a(c2);
                try {
                    this.d.setVisibility(8);
                    this.d.a();
                } catch (Exception e3) {
                }
            } catch (Exception e4) {
            }
        } catch (Exception e5) {
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
    }

    public void b(int i2) {
        try {
            this.d.a(i2);
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: package-private */
    public void b(cu cuVar) {
        try {
            this.a.a(cuVar);
            this.b.a(cuVar);
        } catch (Exception e2) {
            f.a(e2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.youmi.android.be.b(android.content.Context, java.lang.String):void
     arg types: [android.app.Activity, java.lang.String]
     candidates:
      net.youmi.android.be.b(android.app.Activity, java.lang.String):void
      net.youmi.android.be.b(android.content.Context, java.lang.String):void */
    public void b(i iVar, File file, String str) {
        this.e.a().c = false;
        try {
            this.d.setVisibility(8);
            this.d.a();
        } catch (Exception e2) {
        }
        if (file != null && file.exists()) {
            ay.a(this.g, "下载成功");
            try {
                k.a(this.g, k.a(this.g, file.getPath(), str, iVar.a(), iVar.b(), iVar.c()), 3);
            } catch (Exception e3) {
                f.a(e3);
            }
            be.b((Context) this.g, file.getPath());
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: package-private */
    public void c() {
        try {
            Toast.makeText(this.g, "正在加载", 0).show();
            cu c2 = this.e.c();
            if (c2 != null) {
                switch (c2.a()) {
                    case -1:
                        if (c2.i() != null) {
                            try {
                                WebView webView = new WebView(this.g);
                                webView.loadDataWithBaseURL(null, c2.i().b(), "text/html", "utf-8", null);
                                new AlertDialog.Builder(this.g).setTitle("有米广告SDK配置简易教程").setView(webView).create().show();
                                return;
                            } catch (Exception e2) {
                                f.a(e2);
                                return;
                            }
                        } else {
                            return;
                        }
                    case 11:
                        if (c2.i() != null) {
                            a(c2);
                            return;
                        }
                        this.e.a().c = true;
                        new i(this.g, this).execute(c2.p());
                        return;
                    case 21:
                        try {
                            this.h = new dy(this.g, c2, System.currentTimeMillis() - c2.A(), 0);
                            be.c(this.g, c2.p());
                            return;
                        } catch (Exception e3) {
                            return;
                        }
                    case 22:
                        try {
                            this.e.a().c = true;
                            new i(this.g, this, c2.f()).execute(c2.p());
                        } catch (Exception e4) {
                        }
                        try {
                            ak.a(this.g, c2);
                            return;
                        } catch (Exception e5) {
                            return;
                        }
                    case 31:
                        try {
                            String[] x = c2.x();
                            if (x != null) {
                                this.e.a().c = true;
                                new dw(this.g, this, c2).execute(x);
                                return;
                            }
                            return;
                        } catch (Exception e6) {
                            return;
                        }
                    case 32:
                    default:
                        return;
                    case 41:
                        try {
                            this.h = new dy(this.g, c2, System.currentTimeMillis() - c2.A(), 0);
                            be.a(this.g, Uri.parse(c2.p()));
                            return;
                        } catch (Exception e7) {
                            return;
                        }
                    case 42:
                        try {
                            this.h = new dy(this.g, c2, System.currentTimeMillis() - c2.A(), 0);
                            z.a(this.g, c2.p());
                            return;
                        } catch (Exception e8) {
                            return;
                        }
                    case 43:
                        try {
                            this.h = new dy(this.g, c2, System.currentTimeMillis() - c2.A(), 0);
                            z.c(this.g, c2.p());
                            return;
                        } catch (Exception e9) {
                            return;
                        }
                    case 44:
                        try {
                            this.h = new dy(this.g, c2, System.currentTimeMillis() - c2.A(), 0);
                            z.b(this.g, c2.p());
                            return;
                        } catch (Exception e10) {
                            return;
                        }
                }
            }
        } catch (Exception e11) {
        }
    }

    /* access modifiers changed from: package-private */
    public void c(int i2) {
        int i3 = this.f;
        int red = Color.red(i2);
        int green = Color.green(i2);
        int blue = Color.blue(i2);
        setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{Color.argb(i3, red + 60 < 255 ? red + 60 : 255, green + 60 < 255 ? green + 60 : 255, blue + 60 < 255 ? blue + 60 : 255), Color.argb(i3, red - 60 > 0 ? red - 60 : 0, green - 60 > 0 ? green - 60 : 0, blue - 60 > 0 ? blue - 60 : 0)}));
    }

    public void e() {
        this.e.a().c = false;
        try {
            this.d.setVisibility(8);
            this.d.a();
        } catch (Exception e2) {
        }
        try {
            Toast.makeText(this.g, "无法连接服务器,请稍后重试!", 0).show();
        } catch (Exception e3) {
        }
    }

    public void f() {
        this.e.a().c = false;
        ay.a(this.g, "正在下载");
        try {
            this.d.setVisibility(8);
            this.d.a();
        } catch (Exception e2) {
        }
    }

    public void g() {
        this.e.a().c = false;
        ay.a(this.g, "存储卡不可用,请启用存储卡", 1);
        try {
            this.d.setVisibility(8);
            this.d.a();
        } catch (Exception e2) {
        }
    }

    public void h() {
        r.d(this.g);
        ay.a(this.g, "下载失败");
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        try {
            if (this.h != null) {
                this.h.a();
            }
        } catch (Exception e2) {
        }
    }
}
