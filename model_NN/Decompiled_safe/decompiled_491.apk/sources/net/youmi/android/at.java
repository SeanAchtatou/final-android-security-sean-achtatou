package net.youmi.android;

import android.app.Activity;
import android.view.animation.Animation;
import android.widget.RelativeLayout;

class at extends RelativeLayout implements bb {
    ca a;
    int b = -1;
    Activity c;
    int d;
    dg e;
    final /* synthetic */ cx f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public at(cx cxVar, Activity activity, ca caVar, int i, int i2) {
        super(activity);
        this.f = cxVar;
        this.a = caVar;
        this.b = i;
        this.c = activity;
        this.d = i2;
        e();
    }

    private void e() {
        if (this.e == null) {
            this.e = new dg(this.f, this.c, this.b, this.a, this.d);
            this.e.setVisibility(0);
        }
        this.e.setId(1003);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        this.a.a().c().b();
        layoutParams.addRule(9);
        layoutParams.addRule(15);
        addView(this.e, layoutParams);
    }

    public void a() {
        setVisibility(8);
    }

    public void a(Animation animation) {
        try {
            startAnimation(animation);
        } catch (Exception e2) {
        }
    }

    public boolean a(cu cuVar) {
        if (cuVar == null) {
            return false;
        }
        try {
            return this.e.a(cuVar);
        } catch (Exception e2) {
            return false;
        }
    }

    public void b() {
        this.e.b();
    }

    public void c() {
        setVisibility(0);
    }

    public void d() {
        this.e.d();
    }
}
