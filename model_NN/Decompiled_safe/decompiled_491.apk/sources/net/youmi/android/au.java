package net.youmi.android;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import android.view.View;

class au extends View {
    Shader a;
    int b;
    int c;
    ca d;
    fe e;
    Drawable f;
    AdView g;
    Paint h = new Paint();
    Rect i = new Rect();
    RectF j = new RectF();
    RectF k = new RectF();
    RectF l = new RectF();
    Rect m = new Rect();
    float n = 0.0f;
    float o = 0.0f;
    float p = 5.0f;
    float q = 5.0f;
    int r = Color.argb(185, 0, 0, 0);
    int s = Color.argb(255, 10, 10, 10);
    boolean t = false;
    String u = "点击查看详情";
    Runnable v = new et(this);
    /* access modifiers changed from: private */
    public cu w;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, int, int, float, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    public au(Context context, AdView adView, fe feVar, int i2, int i3, ca caVar, Drawable drawable) {
        super(context);
        this.d = caVar;
        this.b = i2;
        this.g = adView;
        this.c = i3;
        this.e = feVar;
        this.f = drawable;
        int a2 = caVar.a().d().a();
        int b2 = caVar.a().d().b();
        int c2 = caVar.a().d().c();
        int e2 = caVar.a().d().e();
        int d2 = caVar.a().d().d();
        int i4 = (i3 - e2) / 2;
        this.j.set((float) ((i2 - b2) - a2), (float) ((i3 - b2) - a2), (float) (i2 - a2), (float) (i3 - a2));
        this.h.setTextSize(caVar.a().d().f());
        this.h.getTextBounds("点击查看详情", 0, "点击查看详情".length(), this.m);
        this.k.set((float) ((((i2 - this.m.width()) - c2) - (d2 * 4)) - i4), (float) i4, (float) (i2 - i4), (float) (e2 + i4));
        this.l.set(this.k.left + ((float) d2), this.k.top + ((float) d2), this.k.left + ((float) c2) + ((float) d2), ((float) c2) + this.k.top + ((float) d2));
        this.n = this.l.right + ((float) d2);
        this.o = (float) ((i3 / 2) + caVar.a(5));
        this.a = new LinearGradient(0.0f, 0.0f, 0.0f, (float) i3, Color.argb(255, 255, 255, 255), Color.argb(255, 80, 80, 80), Shader.TileMode.CLAMP);
        this.p = caVar.a(this.p);
        this.q = caVar.a(this.q);
        setClickable(true);
    }

    private void a(MotionEvent motionEvent) {
        try {
            if (this.g.a().c) {
                this.g.a().c();
            } else if (!this.w.y()) {
                this.g.a().c();
                this.e.c();
            } else if (this.g.a().a()) {
                if (this.k.contains(motionEvent.getX(), motionEvent.getY())) {
                    this.g.a().c();
                    this.e.c();
                    return;
                }
                this.g.a().c();
            } else {
                this.g.a().b();
            }
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: package-private */
    public void a(cu cuVar) {
        if (cuVar != null) {
            if (this.w == null || !this.w.f().equals(cuVar.f())) {
                this.w = cuVar;
                switch (this.w.a()) {
                    case 11:
                        this.u = "点击浏览网页";
                        break;
                    case 21:
                        this.u = "点击查看详情";
                        break;
                    case 22:
                        this.u = "点击直接下载";
                        break;
                    case 31:
                        this.u = "点击查看详情";
                        break;
                    case 32:
                        this.u = "点击播放视频";
                        break;
                    case 41:
                        this.u = "点击拨打电话";
                        break;
                    case 42:
                        this.u = "点击发送短信";
                        break;
                    case 43:
                        this.u = "点击查看地图";
                        break;
                    case 44:
                        this.u = "点击发送邮件";
                        break;
                    default:
                        this.u = "点击查看详情";
                        break;
                }
                this.g.a().c();
                try {
                    getHandler().post(this.v);
                } catch (Exception e2) {
                }
                postInvalidate();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void}
     arg types: [float, int, int, int]
     candidates:
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, long):void}
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void} */
    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        try {
            if (this.w != null) {
                Bitmap n2 = this.w.n();
                if (n2 != null) {
                    if (n2.isRecycled()) {
                        n2 = null;
                    } else {
                        this.i.set(0, 0, n2.getWidth(), n2.getHeight());
                    }
                }
                int b2 = this.w.b();
                boolean z = (b2 == 1 || b2 == 0) && n2 != null;
                this.h.reset();
                if (this.g.a().c) {
                    if (z && n2 != null) {
                        canvas.drawBitmap(n2, this.i, this.j, this.h);
                    }
                } else if (this.g.a().a()) {
                    canvas.drawColor(this.r);
                    this.h.reset();
                    this.h.setAntiAlias(true);
                    this.h.setShader(this.a);
                    this.h.setShadowLayer(this.q, 0.0f, 0.0f, -12303292);
                    canvas.drawRoundRect(this.k, this.p, this.p, this.h);
                    this.h.reset();
                    if (n2 != null) {
                        canvas.drawBitmap(n2, this.i, this.l, this.h);
                    }
                    this.h.reset();
                    this.h.setTextSize(this.d.a().d().f());
                    this.h.setTextAlign(Paint.Align.LEFT);
                    this.h.setColor(this.s);
                    this.h.setAntiAlias(true);
                    canvas.drawText(this.u, this.n, this.o, this.h);
                } else {
                    if (z && n2 != null) {
                        canvas.drawBitmap(n2, this.i, this.j, this.h);
                    }
                    if (this.t) {
                        canvas.drawColor(this.r);
                    }
                }
            }
        } catch (Exception e2) {
            f.a(e2);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.t = true;
                break;
            case 1:
                this.t = false;
                a(motionEvent);
                break;
            case 2:
                this.t = true;
                break;
            case 3:
                this.t = false;
                break;
            case 4:
                this.t = false;
                break;
            default:
                this.t = false;
                break;
        }
        postInvalidate();
        return super.onTouchEvent(motionEvent);
    }
}
