package net.youmi.android;

import android.content.Context;
import android.os.AsyncTask;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.http.impl.client.DefaultHttpClient;

class dw extends AsyncTask implements ee {
    private Context a;
    private DefaultHttpClient b;
    private dl c;
    private long d;
    private long e;
    private String f;
    private String g;
    private boolean h = false;
    private String i;
    private String j;
    private long k;
    private long l;

    dw(Context context, dl dlVar, cu cuVar) {
        this.a = context;
        this.c = dlVar;
        if (cuVar == null) {
            this.h = false;
            return;
        }
        this.h = cuVar.s();
        cuVar.v();
        this.k = cuVar.B();
        this.l = cuVar.A();
        this.f = cuVar.g();
        this.g = cuVar.h();
        this.i = cuVar.o();
        this.j = cuVar.k();
    }

    /* access modifiers changed from: package-private */
    public long a() {
        return this.e - this.d;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:100:0x0115, code lost:
        r13.b.getConnectionManager().shutdown();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x011f, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:?, code lost:
        net.youmi.android.f.a(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x0146, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x0147, code lost:
        net.youmi.android.f.a(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x014b, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x014c, code lost:
        net.youmi.android.f.a(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x00d1, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x00d2, code lost:
        r12 = r2;
        r2 = r0;
        r0 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x0103, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x0104, code lost:
        r12 = r2;
        r2 = r0;
        r0 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:100:0x0115 A[Catch:{ Exception -> 0x0150 }] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x00df A[SYNTHETIC, Splitter:B:79:0x00df] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x00e6 A[Catch:{ Exception -> 0x0141 }] */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x0103 A[ExcHandler: all (r2v9 'th' java.lang.Throwable A[CUSTOM_DECLARE]), PHI: r1 
      PHI: (r1v16 java.io.FileOutputStream) = (r1v13 java.io.FileOutputStream), (r1v13 java.io.FileOutputStream), (r1v13 java.io.FileOutputStream), (r1v13 java.io.FileOutputStream), (r1v18 java.io.FileOutputStream), (r1v13 java.io.FileOutputStream), (r1v13 java.io.FileOutputStream), (r1v13 java.io.FileOutputStream), (r1v13 java.io.FileOutputStream), (r1v13 java.io.FileOutputStream), (r1v13 java.io.FileOutputStream), (r1v13 java.io.FileOutputStream), (r1v13 java.io.FileOutputStream), (r1v13 java.io.FileOutputStream) binds: [B:33:0x0067, B:39:0x0078, B:103:0x0120, B:104:?, B:42:0x007c, B:40:?, B:60:0x00a8, B:61:?, B:87:0x00f3, B:88:?, B:69:0x00cd, B:70:?, B:66:0x00ba, B:67:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:33:0x0067] */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x0109 A[SYNTHETIC, Splitter:B:92:0x0109] */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x010e A[SYNTHETIC, Splitter:B:95:0x010e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String a(java.lang.String r14, java.lang.String r15) {
        /*
            r13 = this;
            long r0 = java.lang.System.currentTimeMillis()
            r13.d = r0
            r0 = 0
            r1 = 0
            org.apache.http.impl.client.DefaultHttpClient r2 = r13.b     // Catch:{ Exception -> 0x0187, all -> 0x0180 }
            if (r2 != 0) goto L_0x0014
            android.content.Context r2 = r13.a     // Catch:{ Exception -> 0x0187, all -> 0x0180 }
            org.apache.http.impl.client.DefaultHttpClient r2 = net.youmi.android.s.a(r2, r13)     // Catch:{ Exception -> 0x0187, all -> 0x0180 }
            r13.b = r2     // Catch:{ Exception -> 0x0187, all -> 0x0180 }
        L_0x0014:
            org.apache.http.client.methods.HttpGet r2 = new org.apache.http.client.methods.HttpGet     // Catch:{ Exception -> 0x0187, all -> 0x0180 }
            r2.<init>(r14)     // Catch:{ Exception -> 0x0187, all -> 0x0180 }
            org.apache.http.impl.client.DefaultHttpClient r3 = r13.b     // Catch:{ Exception -> 0x0187, all -> 0x0180 }
            org.apache.http.HttpResponse r3 = r3.execute(r2)     // Catch:{ Exception -> 0x0187, all -> 0x0180 }
            if (r3 == 0) goto L_0x018d
            org.apache.http.HttpEntity r3 = r3.getEntity()     // Catch:{ Exception -> 0x0187, all -> 0x0180 }
            if (r3 == 0) goto L_0x018d
            long r4 = r3.getContentLength()     // Catch:{ Exception -> 0x0187, all -> 0x0180 }
            android.content.Context r6 = r13.a     // Catch:{ Exception -> 0x0187, all -> 0x0180 }
            r7 = 1
            java.io.FileOutputStream r1 = r6.openFileOutput(r15, r7)     // Catch:{ Exception -> 0x0187, all -> 0x0180 }
            if (r1 != 0) goto L_0x005f
            r2.abort()     // Catch:{ Exception -> 0x0187, all -> 0x0180 }
            if (r1 == 0) goto L_0x003c
            r1.close()     // Catch:{ Exception -> 0x0050 }
        L_0x003c:
            if (r0 == 0) goto L_0x0041
            r0.close()     // Catch:{ Exception -> 0x0055 }
        L_0x0041:
            org.apache.http.impl.client.DefaultHttpClient r0 = r13.b     // Catch:{ Exception -> 0x005a }
            if (r0 == 0) goto L_0x004e
            org.apache.http.impl.client.DefaultHttpClient r0 = r13.b     // Catch:{ Exception -> 0x005a }
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()     // Catch:{ Exception -> 0x005a }
            r0.shutdown()     // Catch:{ Exception -> 0x005a }
        L_0x004e:
            r0 = 0
        L_0x004f:
            return r0
        L_0x0050:
            r1 = move-exception
            net.youmi.android.f.a(r1)
            goto L_0x003c
        L_0x0055:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x0041
        L_0x005a:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x004e
        L_0x005f:
            java.io.InputStream r0 = r3.getContent()     // Catch:{ Exception -> 0x0187, all -> 0x0180 }
            if (r0 == 0) goto L_0x0155
            r2 = 1024(0x400, float:1.435E-42)
            byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x00d1, all -> 0x0103 }
            r3 = 0
            r6 = 0
        L_0x006c:
            int r8 = r0.read(r2)     // Catch:{ Exception -> 0x00d1, all -> 0x0103 }
            if (r8 > 0) goto L_0x00a7
            r2 = 0
            int r2 = (r6 > r2 ? 1 : (r6 == r2 ? 0 : -1))
            if (r2 <= 0) goto L_0x0155
            r1.close()     // Catch:{ Exception -> 0x011f, all -> 0x0103 }
            r1 = 0
        L_0x007c:
            android.content.Context r2 = r13.a     // Catch:{ Exception -> 0x00d1, all -> 0x0103 }
            java.io.File r2 = r2.getFileStreamPath(r15)     // Catch:{ Exception -> 0x00d1, all -> 0x0103 }
            if (r2 == 0) goto L_0x0155
            boolean r3 = r2.exists()     // Catch:{ Exception -> 0x00d1, all -> 0x0103 }
            if (r3 == 0) goto L_0x0155
            java.lang.String r2 = r2.getPath()     // Catch:{ Exception -> 0x00d1, all -> 0x0103 }
            if (r1 == 0) goto L_0x0093
            r1.close()     // Catch:{ Exception -> 0x0125 }
        L_0x0093:
            if (r0 == 0) goto L_0x0098
            r0.close()     // Catch:{ Exception -> 0x012b }
        L_0x0098:
            org.apache.http.impl.client.DefaultHttpClient r0 = r13.b     // Catch:{ Exception -> 0x0131 }
            if (r0 == 0) goto L_0x00a5
            org.apache.http.impl.client.DefaultHttpClient r0 = r13.b     // Catch:{ Exception -> 0x0131 }
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()     // Catch:{ Exception -> 0x0131 }
            r0.shutdown()     // Catch:{ Exception -> 0x0131 }
        L_0x00a5:
            r0 = r2
            goto L_0x004f
        L_0x00a7:
            r9 = 0
            r1.write(r2, r9, r8)     // Catch:{ Exception -> 0x00d1, all -> 0x0103 }
            long r8 = (long) r8     // Catch:{ Exception -> 0x00d1, all -> 0x0103 }
            long r6 = r6 + r8
            int r3 = r3 + 1
            int r8 = r3 % 10
            if (r8 != 0) goto L_0x006c
            r8 = 0
            int r8 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
            if (r8 <= 0) goto L_0x00f2
            r8 = 1
            java.lang.Integer[] r8 = new java.lang.Integer[r8]     // Catch:{ Exception -> 0x00cc, all -> 0x0103 }
            r9 = 0
            r10 = 100
            long r10 = r10 * r6
            long r10 = r10 / r4
            int r10 = (int) r10     // Catch:{ Exception -> 0x00cc, all -> 0x0103 }
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x00cc, all -> 0x0103 }
            r8[r9] = r10     // Catch:{ Exception -> 0x00cc, all -> 0x0103 }
            r13.publishProgress(r8)     // Catch:{ Exception -> 0x00cc, all -> 0x0103 }
            goto L_0x006c
        L_0x00cc:
            r8 = move-exception
            net.youmi.android.f.a(r8)     // Catch:{ Exception -> 0x00d1, all -> 0x0103 }
            goto L_0x006c
        L_0x00d1:
            r2 = move-exception
            r12 = r2
            r2 = r0
            r0 = r12
        L_0x00d5:
            net.youmi.android.f.a(r0)     // Catch:{ all -> 0x0185 }
            if (r1 == 0) goto L_0x00dd
            r1.close()     // Catch:{ Exception -> 0x0137 }
        L_0x00dd:
            if (r2 == 0) goto L_0x00e2
            r2.close()     // Catch:{ Exception -> 0x013c }
        L_0x00e2:
            org.apache.http.impl.client.DefaultHttpClient r0 = r13.b     // Catch:{ Exception -> 0x0141 }
            if (r0 == 0) goto L_0x00ef
            org.apache.http.impl.client.DefaultHttpClient r0 = r13.b     // Catch:{ Exception -> 0x0141 }
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()     // Catch:{ Exception -> 0x0141 }
            r0.shutdown()     // Catch:{ Exception -> 0x0141 }
        L_0x00ef:
            r0 = 0
            goto L_0x004f
        L_0x00f2:
            r8 = 1
            java.lang.Integer[] r8 = new java.lang.Integer[r8]     // Catch:{ Exception -> 0x00cc, all -> 0x0103 }
            r9 = 0
            r10 = 50
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x00cc, all -> 0x0103 }
            r8[r9] = r10     // Catch:{ Exception -> 0x00cc, all -> 0x0103 }
            r13.publishProgress(r8)     // Catch:{ Exception -> 0x00cc, all -> 0x0103 }
            goto L_0x006c
        L_0x0103:
            r2 = move-exception
            r12 = r2
            r2 = r0
            r0 = r12
        L_0x0107:
            if (r1 == 0) goto L_0x010c
            r1.close()     // Catch:{ Exception -> 0x0146 }
        L_0x010c:
            if (r2 == 0) goto L_0x0111
            r2.close()     // Catch:{ Exception -> 0x014b }
        L_0x0111:
            org.apache.http.impl.client.DefaultHttpClient r1 = r13.b     // Catch:{ Exception -> 0x0150 }
            if (r1 == 0) goto L_0x011e
            org.apache.http.impl.client.DefaultHttpClient r1 = r13.b     // Catch:{ Exception -> 0x0150 }
            org.apache.http.conn.ClientConnectionManager r1 = r1.getConnectionManager()     // Catch:{ Exception -> 0x0150 }
            r1.shutdown()     // Catch:{ Exception -> 0x0150 }
        L_0x011e:
            throw r0
        L_0x011f:
            r2 = move-exception
            net.youmi.android.f.a(r2)     // Catch:{ Exception -> 0x00d1, all -> 0x0103 }
            goto L_0x007c
        L_0x0125:
            r1 = move-exception
            net.youmi.android.f.a(r1)
            goto L_0x0093
        L_0x012b:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x0098
        L_0x0131:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x00a5
        L_0x0137:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x00dd
        L_0x013c:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x00e2
        L_0x0141:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x00ef
        L_0x0146:
            r1 = move-exception
            net.youmi.android.f.a(r1)
            goto L_0x010c
        L_0x014b:
            r1 = move-exception
            net.youmi.android.f.a(r1)
            goto L_0x0111
        L_0x0150:
            r1 = move-exception
            net.youmi.android.f.a(r1)
            goto L_0x011e
        L_0x0155:
            r12 = r1
            r1 = r0
            r0 = r12
        L_0x0158:
            if (r0 == 0) goto L_0x015d
            r0.close()     // Catch:{ Exception -> 0x0176 }
        L_0x015d:
            if (r1 == 0) goto L_0x0162
            r1.close()     // Catch:{ Exception -> 0x017b }
        L_0x0162:
            org.apache.http.impl.client.DefaultHttpClient r0 = r13.b     // Catch:{ Exception -> 0x0170 }
            if (r0 == 0) goto L_0x00ef
            org.apache.http.impl.client.DefaultHttpClient r0 = r13.b     // Catch:{ Exception -> 0x0170 }
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()     // Catch:{ Exception -> 0x0170 }
            r0.shutdown()     // Catch:{ Exception -> 0x0170 }
            goto L_0x00ef
        L_0x0170:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x00ef
        L_0x0176:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x015d
        L_0x017b:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x0162
        L_0x0180:
            r2 = move-exception
            r12 = r2
            r2 = r0
            r0 = r12
            goto L_0x0107
        L_0x0185:
            r0 = move-exception
            goto L_0x0107
        L_0x0187:
            r2 = move-exception
            r12 = r2
            r2 = r0
            r0 = r12
            goto L_0x00d5
        L_0x018d:
            r12 = r1
            r1 = r0
            r0 = r12
            goto L_0x0158
        */
        throw new UnsupportedOperationException("Method not decompiled: net.youmi.android.dw.a(java.lang.String, java.lang.String):java.lang.String");
    }

    public void a(String str) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onProgressUpdate(Integer... numArr) {
        super.onProgressUpdate(numArr);
        try {
            if (this.c != null && numArr != null && numArr.length > 0) {
                this.c.b(numArr[0].intValue());
            }
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(InputStream inputStream, OutputStream outputStream) {
        if (!(inputStream == null || outputStream == null)) {
            try {
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = inputStream.read(bArr);
                    if (read <= 0) {
                        return true;
                    }
                    outputStream.write(bArr, 0, read);
                }
            } catch (Exception e2) {
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0070, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0071, code lost:
        r4 = r2;
        r2 = r0;
        r0 = r4;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0033 A[SYNTHETIC, Splitter:B:27:0x0033] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0038 A[SYNTHETIC, Splitter:B:30:0x0038] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0042 A[SYNTHETIC, Splitter:B:36:0x0042] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0047 A[SYNTHETIC, Splitter:B:39:0x0047] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0070 A[ExcHandler: all (r2v4 'th' java.lang.Throwable A[CUSTOM_DECLARE]), PHI: r1 
      PHI: (r1v16 java.io.FileOutputStream) = (r1v13 java.io.FileOutputStream), (r1v13 java.io.FileOutputStream), (r1v13 java.io.FileOutputStream), (r1v18 java.io.FileOutputStream), (r1v18 java.io.FileOutputStream), (r1v13 java.io.FileOutputStream) binds: [B:9:0x0014, B:10:?, B:12:0x001a, B:15:0x001e, B:16:?, B:13:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:9:0x0014] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(org.apache.http.HttpResponse r6, java.io.File r7) {
        /*
            r5 = this;
            r3 = 0
            org.apache.http.HttpEntity r0 = r6.getEntity()     // Catch:{ Exception -> 0x002e, all -> 0x003d }
            if (r0 == 0) goto L_0x0081
            if (r7 == 0) goto L_0x0081
            java.io.InputStream r0 = r0.getContent()     // Catch:{ Exception -> 0x002e, all -> 0x003d }
            if (r0 == 0) goto L_0x007e
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0075, all -> 0x006b }
            r1.<init>(r7)     // Catch:{ Exception -> 0x0075, all -> 0x006b }
            boolean r2 = r5.a(r0, r1)     // Catch:{ Exception -> 0x0079, all -> 0x0070 }
            if (r2 == 0) goto L_0x004b
            r1.close()     // Catch:{ Exception -> 0x005b, all -> 0x0070 }
            r1 = r3
        L_0x001e:
            boolean r2 = r7.exists()     // Catch:{ Exception -> 0x0079, all -> 0x0070 }
            if (r1 == 0) goto L_0x0027
            r1.close()     // Catch:{ Exception -> 0x005d }
        L_0x0027:
            if (r0 == 0) goto L_0x002c
            r0.close()     // Catch:{ Exception -> 0x005f }
        L_0x002c:
            r0 = r2
        L_0x002d:
            return r0
        L_0x002e:
            r0 = move-exception
            r0 = r3
            r1 = r3
        L_0x0031:
            if (r0 == 0) goto L_0x0036
            r0.close()     // Catch:{ Exception -> 0x0061 }
        L_0x0036:
            if (r1 == 0) goto L_0x003b
            r1.close()     // Catch:{ Exception -> 0x0063 }
        L_0x003b:
            r0 = 0
            goto L_0x002d
        L_0x003d:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x0040:
            if (r1 == 0) goto L_0x0045
            r1.close()     // Catch:{ Exception -> 0x0065 }
        L_0x0045:
            if (r2 == 0) goto L_0x004a
            r2.close()     // Catch:{ Exception -> 0x0067 }
        L_0x004a:
            throw r0
        L_0x004b:
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x004e:
            if (r0 == 0) goto L_0x0053
            r0.close()     // Catch:{ Exception -> 0x0069 }
        L_0x0053:
            if (r1 == 0) goto L_0x003b
            r1.close()     // Catch:{ Exception -> 0x0059 }
            goto L_0x003b
        L_0x0059:
            r0 = move-exception
            goto L_0x003b
        L_0x005b:
            r2 = move-exception
            goto L_0x001e
        L_0x005d:
            r1 = move-exception
            goto L_0x0027
        L_0x005f:
            r0 = move-exception
            goto L_0x002c
        L_0x0061:
            r0 = move-exception
            goto L_0x0036
        L_0x0063:
            r0 = move-exception
            goto L_0x003b
        L_0x0065:
            r1 = move-exception
            goto L_0x0045
        L_0x0067:
            r1 = move-exception
            goto L_0x004a
        L_0x0069:
            r0 = move-exception
            goto L_0x0053
        L_0x006b:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r3
            goto L_0x0040
        L_0x0070:
            r2 = move-exception
            r4 = r2
            r2 = r0
            r0 = r4
            goto L_0x0040
        L_0x0075:
            r1 = move-exception
            r1 = r0
            r0 = r3
            goto L_0x0031
        L_0x0079:
            r2 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0031
        L_0x007e:
            r1 = r0
            r0 = r3
            goto L_0x004e
        L_0x0081:
            r0 = r3
            r1 = r3
            goto L_0x004e
        */
        throw new UnsupportedOperationException("Method not decompiled: net.youmi.android.dw.a(org.apache.http.HttpResponse, java.io.File):boolean");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00cd, code lost:
        r5 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x010d, code lost:
        r7 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x010e, code lost:
        net.youmi.android.f.a(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x012f, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:?, code lost:
        net.youmi.android.f.a(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x0134, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x0135, code lost:
        r2 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:?, code lost:
        r2.abort();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x0179 A[SYNTHETIC, Splitter:B:123:0x0179] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00a2 A[Catch:{ Exception -> 0x0112, all -> 0x0134 }] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00b2 A[SYNTHETIC, Splitter:B:54:0x00b2] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x00de A[Catch:{ Exception -> 0x0112, all -> 0x0134 }] */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x0134 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:36:0x006f] */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0138 A[SYNTHETIC, Splitter:B:98:0x0138] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String[] a(java.lang.String[] r15) {
        /*
            r14 = this;
            r12 = 0
            r11 = 0
            r10 = 1
            r9 = 0
            if (r15 != 0) goto L_0x001b
            org.apache.http.impl.client.DefaultHttpClient r0 = r14.b     // Catch:{ Exception -> 0x0016 }
            if (r0 == 0) goto L_0x0014
            org.apache.http.impl.client.DefaultHttpClient r0 = r14.b     // Catch:{ Exception -> 0x0016 }
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()     // Catch:{ Exception -> 0x0016 }
            r0.shutdown()     // Catch:{ Exception -> 0x0016 }
        L_0x0014:
            r0 = r11
        L_0x0015:
            return r0
        L_0x0016:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x0014
        L_0x001b:
            int r0 = r15.length     // Catch:{ Exception -> 0x014f }
            if (r0 <= 0) goto L_0x01c0
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ Exception -> 0x014f }
            int r0 = r15.length     // Catch:{ Exception -> 0x014f }
            r1.<init>(r0)     // Catch:{ Exception -> 0x014f }
            r0 = r9
        L_0x0025:
            int r2 = r15.length     // Catch:{ Exception -> 0x014f }
            if (r0 < r2) goto L_0x004a
            int r0 = r1.size()     // Catch:{ Exception -> 0x014f }
            if (r0 <= 0) goto L_0x01c0
            int r0 = r1.size()     // Catch:{ Exception -> 0x014f }
            java.lang.String[] r2 = new java.lang.String[r0]     // Catch:{ Exception -> 0x014f }
            r3 = r9
        L_0x0035:
            int r0 = r1.size()     // Catch:{ Exception -> 0x014f }
            if (r3 < r0) goto L_0x0194
            org.apache.http.impl.client.DefaultHttpClient r0 = r14.b     // Catch:{ Exception -> 0x01a1 }
            if (r0 == 0) goto L_0x0048
            org.apache.http.impl.client.DefaultHttpClient r0 = r14.b     // Catch:{ Exception -> 0x01a1 }
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()     // Catch:{ Exception -> 0x01a1 }
            r0.shutdown()     // Catch:{ Exception -> 0x01a1 }
        L_0x0048:
            r0 = r2
            goto L_0x0015
        L_0x004a:
            r2 = r15[r0]     // Catch:{ Exception -> 0x01e7, all -> 0x01e3 }
            if (r2 == 0) goto L_0x01f1
            java.lang.String r2 = r2.trim()     // Catch:{ Exception -> 0x01e7, all -> 0x01e3 }
            int r3 = r2.length()     // Catch:{ Exception -> 0x01e7, all -> 0x01e3 }
            if (r3 <= 0) goto L_0x01f1
            int r3 = r2.length()     // Catch:{ Exception -> 0x01e7, all -> 0x01e3 }
            if (r3 != r10) goto L_0x005e
        L_0x005e:
            org.apache.http.impl.client.DefaultHttpClient r3 = r14.b     // Catch:{ Exception -> 0x01e7, all -> 0x01e3 }
            if (r3 != 0) goto L_0x006a
            android.content.Context r3 = r14.a     // Catch:{ Exception -> 0x01e7, all -> 0x01e3 }
            org.apache.http.impl.client.DefaultHttpClient r3 = net.youmi.android.s.a(r3, r14)     // Catch:{ Exception -> 0x01e7, all -> 0x01e3 }
            r14.b = r3     // Catch:{ Exception -> 0x01e7, all -> 0x01e3 }
        L_0x006a:
            org.apache.http.client.methods.HttpGet r3 = new org.apache.http.client.methods.HttpGet     // Catch:{ Exception -> 0x01e7, all -> 0x01e3 }
            r3.<init>(r2)     // Catch:{ Exception -> 0x01e7, all -> 0x01e3 }
            org.apache.http.impl.client.DefaultHttpClient r4 = r14.b     // Catch:{ Exception -> 0x0112, all -> 0x0134 }
            org.apache.http.HttpResponse r4 = r4.execute(r3)     // Catch:{ Exception -> 0x0112, all -> 0x0134 }
            if (r4 == 0) goto L_0x0176
            java.lang.String r5 = "Last-Modified"
            org.apache.http.Header r5 = r4.getLastHeader(r5)     // Catch:{ Exception -> 0x0112, all -> 0x0134 }
            java.lang.String r5 = r5.getValue()     // Catch:{ Exception -> 0x00cc, all -> 0x0134 }
            if (r5 == 0) goto L_0x01ee
            java.lang.String r5 = r5.trim()     // Catch:{ Exception -> 0x00cc, all -> 0x0134 }
            int r6 = r5.length()     // Catch:{ Exception -> 0x00cc, all -> 0x0134 }
            if (r6 <= 0) goto L_0x01ee
            java.util.Date r6 = new java.util.Date     // Catch:{ Exception -> 0x00cc, all -> 0x0134 }
            r6.<init>(r5)     // Catch:{ Exception -> 0x00cc, all -> 0x0134 }
            long r5 = r6.getTime()     // Catch:{ Exception -> 0x00cc, all -> 0x0134 }
        L_0x0096:
            net.youmi.android.ed r7 = net.youmi.android.bc.d()     // Catch:{ Exception -> 0x0112, all -> 0x0134 }
            java.io.File r2 = r7.d(r2)     // Catch:{ Exception -> 0x0112, all -> 0x0134 }
            int r7 = (r5 > r12 ? 1 : (r5 == r12 ? 0 : -1))
            if (r7 > 0) goto L_0x00de
            boolean r5 = r2.exists()     // Catch:{ Exception -> 0x0112, all -> 0x0134 }
            if (r5 == 0) goto L_0x00cf
            java.lang.String r2 = r2.getPath()     // Catch:{ Exception -> 0x0112, all -> 0x0134 }
            r1.add(r2)     // Catch:{ Exception -> 0x0112, all -> 0x0134 }
        L_0x00af:
            r2 = r3
        L_0x00b0:
            if (r2 == 0) goto L_0x00b5
            r2.abort()     // Catch:{ Exception -> 0x01d3 }
        L_0x00b5:
            r2 = 1
            java.lang.Integer[] r2 = new java.lang.Integer[r2]     // Catch:{ Exception -> 0x01eb }
            r3 = 0
            int r4 = r0 + 1
            int r4 = r4 * 100
            int r5 = r15.length     // Catch:{ Exception -> 0x01eb }
            int r4 = r4 / r5
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x01eb }
            r2[r3] = r4     // Catch:{ Exception -> 0x01eb }
            r14.publishProgress(r2)     // Catch:{ Exception -> 0x01eb }
        L_0x00c8:
            int r0 = r0 + 1
            goto L_0x0025
        L_0x00cc:
            r5 = move-exception
            r5 = r12
            goto L_0x0096
        L_0x00cf:
            boolean r4 = r14.a(r4, r2)     // Catch:{ Exception -> 0x0112, all -> 0x0134 }
            if (r4 == 0) goto L_0x00af
            java.lang.String r2 = r2.getPath()     // Catch:{ Exception -> 0x0112, all -> 0x0134 }
            r1.add(r2)     // Catch:{ Exception -> 0x0112, all -> 0x0134 }
            r2 = r3
            goto L_0x00b0
        L_0x00de:
            boolean r7 = r2.exists()     // Catch:{ Exception -> 0x0112, all -> 0x0134 }
            if (r7 == 0) goto L_0x0163
            long r7 = r2.lastModified()     // Catch:{ Exception -> 0x0112, all -> 0x0134 }
            int r7 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            if (r7 != 0) goto L_0x00f8
            java.lang.String r2 = r2.getPath()     // Catch:{ Exception -> 0x0112, all -> 0x0134 }
            r1.add(r2)     // Catch:{ Exception -> 0x0112, all -> 0x0134 }
            r3.abort()     // Catch:{ Exception -> 0x0112, all -> 0x0134 }
            r2 = r11
            goto L_0x00b0
        L_0x00f8:
            r2.deleteOnExit()     // Catch:{ Exception -> 0x010d, all -> 0x0134 }
        L_0x00fb:
            boolean r4 = r14.a(r4, r2)     // Catch:{ Exception -> 0x0112, all -> 0x0134 }
            if (r4 == 0) goto L_0x00af
            r2.setLastModified(r5)     // Catch:{ Exception -> 0x012f, all -> 0x0134 }
        L_0x0104:
            java.lang.String r2 = r2.getPath()     // Catch:{ Exception -> 0x0112, all -> 0x0134 }
            r1.add(r2)     // Catch:{ Exception -> 0x0112, all -> 0x0134 }
            r2 = r3
            goto L_0x00b0
        L_0x010d:
            r7 = move-exception
            net.youmi.android.f.a(r7)     // Catch:{ Exception -> 0x0112, all -> 0x0134 }
            goto L_0x00fb
        L_0x0112:
            r2 = move-exception
            r2 = r3
        L_0x0114:
            if (r2 == 0) goto L_0x0119
            r2.abort()     // Catch:{ Exception -> 0x01d8 }
        L_0x0119:
            r2 = 1
            java.lang.Integer[] r2 = new java.lang.Integer[r2]     // Catch:{ Exception -> 0x012d }
            r3 = 0
            int r4 = r0 + 1
            int r4 = r4 * 100
            int r5 = r15.length     // Catch:{ Exception -> 0x012d }
            int r4 = r4 / r5
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x012d }
            r2[r3] = r4     // Catch:{ Exception -> 0x012d }
            r14.publishProgress(r2)     // Catch:{ Exception -> 0x012d }
            goto L_0x00c8
        L_0x012d:
            r2 = move-exception
            goto L_0x00c8
        L_0x012f:
            r4 = move-exception
            net.youmi.android.f.a(r4)     // Catch:{ Exception -> 0x0112, all -> 0x0134 }
            goto L_0x0104
        L_0x0134:
            r1 = move-exception
            r2 = r3
        L_0x0136:
            if (r2 == 0) goto L_0x013b
            r2.abort()     // Catch:{ Exception -> 0x01db }
        L_0x013b:
            r2 = 1
            java.lang.Integer[] r2 = new java.lang.Integer[r2]     // Catch:{ Exception -> 0x01e0 }
            r3 = 0
            int r0 = r0 + 1
            int r0 = r0 * 100
            int r4 = r15.length     // Catch:{ Exception -> 0x01e0 }
            int r0 = r0 / r4
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x01e0 }
            r2[r3] = r0     // Catch:{ Exception -> 0x01e0 }
            r14.publishProgress(r2)     // Catch:{ Exception -> 0x01e0 }
        L_0x014e:
            throw r1     // Catch:{ Exception -> 0x014f }
        L_0x014f:
            r0 = move-exception
            net.youmi.android.f.a(r0)     // Catch:{ all -> 0x01ac }
            org.apache.http.impl.client.DefaultHttpClient r0 = r14.b     // Catch:{ Exception -> 0x01a7 }
            if (r0 == 0) goto L_0x0160
            org.apache.http.impl.client.DefaultHttpClient r0 = r14.b     // Catch:{ Exception -> 0x01a7 }
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()     // Catch:{ Exception -> 0x01a7 }
            r0.shutdown()     // Catch:{ Exception -> 0x01a7 }
        L_0x0160:
            r0 = r11
            goto L_0x0015
        L_0x0163:
            boolean r4 = r14.a(r4, r2)     // Catch:{ Exception -> 0x0112, all -> 0x0134 }
            if (r4 == 0) goto L_0x00af
            r2.setLastModified(r5)     // Catch:{ Exception -> 0x01d6, all -> 0x0134 }
        L_0x016c:
            java.lang.String r2 = r2.getPath()     // Catch:{ Exception -> 0x0112, all -> 0x0134 }
            r1.add(r2)     // Catch:{ Exception -> 0x0112, all -> 0x0134 }
            r2 = r3
            goto L_0x00b0
        L_0x0176:
            r2 = r3
        L_0x0177:
            if (r2 == 0) goto L_0x017c
            r2.abort()     // Catch:{ Exception -> 0x01de }
        L_0x017c:
            r2 = 1
            java.lang.Integer[] r2 = new java.lang.Integer[r2]     // Catch:{ Exception -> 0x0191 }
            r3 = 0
            int r4 = r0 + 1
            int r4 = r4 * 100
            int r5 = r15.length     // Catch:{ Exception -> 0x0191 }
            int r4 = r4 / r5
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x0191 }
            r2[r3] = r4     // Catch:{ Exception -> 0x0191 }
            r14.publishProgress(r2)     // Catch:{ Exception -> 0x0191 }
            goto L_0x00c8
        L_0x0191:
            r2 = move-exception
            goto L_0x00c8
        L_0x0194:
            java.lang.Object r0 = r1.get(r3)     // Catch:{ Exception -> 0x014f }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x014f }
            r2[r3] = r0     // Catch:{ Exception -> 0x014f }
            int r0 = r3 + 1
            r3 = r0
            goto L_0x0035
        L_0x01a1:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x0048
        L_0x01a7:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x0160
        L_0x01ac:
            r0 = move-exception
            org.apache.http.impl.client.DefaultHttpClient r1 = r14.b     // Catch:{ Exception -> 0x01bb }
            if (r1 == 0) goto L_0x01ba
            org.apache.http.impl.client.DefaultHttpClient r1 = r14.b     // Catch:{ Exception -> 0x01bb }
            org.apache.http.conn.ClientConnectionManager r1 = r1.getConnectionManager()     // Catch:{ Exception -> 0x01bb }
            r1.shutdown()     // Catch:{ Exception -> 0x01bb }
        L_0x01ba:
            throw r0
        L_0x01bb:
            r1 = move-exception
            net.youmi.android.f.a(r1)
            goto L_0x01ba
        L_0x01c0:
            org.apache.http.impl.client.DefaultHttpClient r0 = r14.b     // Catch:{ Exception -> 0x01ce }
            if (r0 == 0) goto L_0x0160
            org.apache.http.impl.client.DefaultHttpClient r0 = r14.b     // Catch:{ Exception -> 0x01ce }
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()     // Catch:{ Exception -> 0x01ce }
            r0.shutdown()     // Catch:{ Exception -> 0x01ce }
            goto L_0x0160
        L_0x01ce:
            r0 = move-exception
            net.youmi.android.f.a(r0)
            goto L_0x0160
        L_0x01d3:
            r2 = move-exception
            goto L_0x00b5
        L_0x01d6:
            r4 = move-exception
            goto L_0x016c
        L_0x01d8:
            r2 = move-exception
            goto L_0x0119
        L_0x01db:
            r2 = move-exception
            goto L_0x013b
        L_0x01de:
            r2 = move-exception
            goto L_0x017c
        L_0x01e0:
            r0 = move-exception
            goto L_0x014e
        L_0x01e3:
            r1 = move-exception
            r2 = r11
            goto L_0x0136
        L_0x01e7:
            r2 = move-exception
            r2 = r11
            goto L_0x0114
        L_0x01eb:
            r2 = move-exception
            goto L_0x00c8
        L_0x01ee:
            r5 = r12
            goto L_0x0096
        L_0x01f1:
            r2 = r11
            goto L_0x0177
        */
        throw new UnsupportedOperationException("Method not decompiled: net.youmi.android.dw.a(java.lang.String[]):java.lang.String[]");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String[] doInBackground(String[]... strArr) {
        String[] strArr2;
        File file;
        try {
            publishProgress(1);
            if (strArr != null && strArr.length > 0 && (strArr2 = strArr[0]) != null && strArr2.length > 0) {
                if (az.a(this.a)) {
                    String[] a2 = a(strArr2);
                    try {
                        if (this.b == null) {
                            return a2;
                        }
                        this.b.getConnectionManager().shutdown();
                        return a2;
                    } catch (Exception e2) {
                        f.a(e2);
                        return a2;
                    }
                } else {
                    String str = strArr2[0];
                    if (str != null) {
                        String trim = str.trim();
                        if (trim.length() > 0) {
                            String b2 = cq.b(trim);
                            try {
                                file = this.a.getFileStreamPath(b2);
                            } catch (Exception e3) {
                                file = null;
                            }
                            if (file != null) {
                                if (file.exists()) {
                                    String[] strArr3 = {file.getPath()};
                                    try {
                                        if (this.b == null) {
                                            return strArr3;
                                        }
                                        this.b.getConnectionManager().shutdown();
                                        return strArr3;
                                    } catch (Exception e4) {
                                        f.a(e4);
                                        return strArr3;
                                    }
                                }
                            }
                            String a3 = a(trim, b2);
                            if (a3 != null) {
                                String[] strArr4 = {a3};
                                try {
                                    if (this.b != null) {
                                        this.b.getConnectionManager().shutdown();
                                    }
                                } catch (Exception e5) {
                                    f.a(e5);
                                }
                                return strArr4;
                            }
                        }
                    }
                }
            }
            try {
                if (this.b != null) {
                    this.b.getConnectionManager().shutdown();
                }
            } catch (Exception e6) {
                f.a(e6);
            }
        } catch (Exception e7) {
            f.a(e7);
            try {
                if (this.b != null) {
                    this.b.getConnectionManager().shutdown();
                }
            } catch (Exception e8) {
                f.a(e8);
            }
        } catch (Throwable th) {
            try {
                if (this.b != null) {
                    this.b.getConnectionManager().shutdown();
                }
            } catch (Exception e9) {
                f.a(e9);
            }
            throw th;
        }
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void onPostExecute(String[] strArr) {
        super.onPostExecute(strArr);
        this.e = System.currentTimeMillis();
        if (this.c == null) {
            return;
        }
        if (strArr == null) {
            try {
                this.c.a();
            } catch (Exception e2) {
            }
        } else {
            if (strArr.length > 0) {
                try {
                    this.c.a(this, strArr);
                    return;
                } catch (Exception e3) {
                    return;
                }
            }
            try {
                this.c.a();
            } catch (Exception e4) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public long c() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public long d() {
        return this.d - this.l;
    }

    /* access modifiers changed from: package-private */
    public String e() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public String f() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public String g() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public String h() {
        return this.j;
    }
}
