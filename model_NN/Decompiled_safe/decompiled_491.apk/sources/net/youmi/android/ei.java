package net.youmi.android;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import java.lang.reflect.Method;

class ei {
    private static Location a;
    /* access modifiers changed from: private */
    public static ew b;

    ei() {
    }

    static Location a(Context context) {
        return a;
    }

    static void a(Context context, AdView adView) {
        LocationManager locationManager;
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7 = 0;
        try {
            if (a == null && (locationManager = (LocationManager) context.getSystemService("location")) != null) {
                try {
                    Location lastKnownLocation = locationManager.getLastKnownLocation("network");
                    if (!(lastKnownLocation == null || (lastKnownLocation.getLongitude() == 0.0d && lastKnownLocation.getLatitude() == 0.0d))) {
                        a = lastKnownLocation;
                        return;
                    }
                } catch (Exception e) {
                    f.a(e);
                }
                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
                if (telephonyManager != null && telephonyManager.getPhoneType() == 2) {
                    try {
                        CellLocation cellLocation = telephonyManager.getCellLocation();
                        Method[] methods = cellLocation.getClass().getMethods();
                        if (methods != null) {
                            int i8 = 0;
                            int i9 = 0;
                            while (i7 < methods.length) {
                                try {
                                    Method method = methods[i7];
                                    if (method != null) {
                                        if (method.getName().equals("getBaseStationLatitude")) {
                                            int i10 = i8;
                                            i6 = ((Integer) method.invoke(cellLocation, new Object[0])).intValue();
                                            i5 = i10;
                                        } else if (method.getName().equals("getBaseStationLongitude")) {
                                            i5 = ((Integer) method.invoke(cellLocation, new Object[0])).intValue();
                                            i6 = i9;
                                        }
                                        i7++;
                                        i9 = i6;
                                        i8 = i5;
                                    }
                                    i5 = i8;
                                    i6 = i9;
                                    i7++;
                                    i9 = i6;
                                    i8 = i5;
                                } catch (Exception e2) {
                                    e = e2;
                                    i2 = i8;
                                    i = i9;
                                    try {
                                        f.a(e);
                                        i3 = i2;
                                        i4 = i;
                                        Location location = new Location("network");
                                        location.setLatitude(((double) i4) / 14400.0d);
                                        location.setLongitude(((double) i3) / 14400.0d);
                                        a = location;
                                        return;
                                    } catch (Exception e3) {
                                        f.a(e3);
                                    }
                                }
                            }
                            i3 = i8;
                            i4 = i9;
                        } else {
                            i3 = 0;
                            i4 = 0;
                        }
                    } catch (Exception e4) {
                        e = e4;
                        i2 = 0;
                        i = 0;
                    }
                    if (!(i4 == 0 && i3 == 0)) {
                        Location location2 = new Location("network");
                        location2.setLatitude(((double) i4) / 14400.0d);
                        location2.setLongitude(((double) i3) / 14400.0d);
                        a = location2;
                        return;
                    }
                }
                if (b == null) {
                    b = new ew(context.getApplicationContext());
                    adView.post(new ep(locationManager));
                }
            }
        } catch (Exception e5) {
            f.a(e5);
        }
    }

    static void a(Location location) {
        if (location != null) {
            a = location;
        }
    }
}
