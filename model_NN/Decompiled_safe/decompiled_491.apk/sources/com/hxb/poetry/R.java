package com.hxb.poetry;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int backgroundTransparent = 2130771970;
        public static final int textColor = 2130771969;
    }

    public static final class color {
        public static final int black = 2131034113;
        public static final int blue = 2131034115;
        public static final int gray = 2131034114;
        public static final int white = 2131034112;
    }

    public static final class drawable {
        public static final int bg_main = 2130837504;
        public static final int dock_background = 2130837505;
        public static final int icon = 2130837506;
        public static final int poetry_bg = 2130837507;
        public static final int poetry_dock_normal = 2130837508;
        public static final int poetry_dock_select = 2130837509;
        public static final int poetry_favorite_off = 2130837510;
        public static final int poetry_favorite_on = 2130837511;
        public static final int poetry_operate_btn_bg = 2130837512;
        public static final int poetry_title_bg_1_selector = 2130837513;
        public static final int poetry_title_bg_2_selector = 2130837514;
        public static final int poetry_title_bg_3_selector = 2130837515;
        public static final int poetry_title_bg_4_selector = 2130837516;
        public static final int search_text_focus = 2130837517;
        public static final int separator_line = 2130837518;
        public static final int text_color = 2130837519;
        public static final int title_bg_1 = 2130837520;
        public static final int title_bg_1_press = 2130837521;
        public static final int title_bg_2 = 2130837522;
        public static final int title_bg_2_press = 2130837523;
        public static final int title_bg_3 = 2130837524;
        public static final int title_bg_3_press = 2130837525;
    }

    public static final class id {
        public static final int adView = 2131230751;
        public static final int adViewEditMode = 2131230739;
        public static final int adViewReadMod = 2131230731;
        public static final int article_type_spinner = 2131230734;
        public static final int author_edit_textview = 2131230735;
        public static final int author_textview = 2131230724;
        public static final int back_button = 2131230729;
        public static final int btn_favorite = 2131230746;
        public static final int btn_mingju = 2131230744;
        public static final int btn_other = 2131230745;
        public static final int btn_songci = 2131230743;
        public static final int btn_tangshi = 2131230742;
        public static final int btn_temp_layout = 2131230740;
        public static final int cancel_button = 2131230738;
        public static final int content_edit_textview = 2131230736;
        public static final int content_read_textview = 2131230726;
        public static final int content_scrollview = 2131230725;
        public static final int edit_mode_layout = 2131230732;
        public static final int favorite_button = 2131230728;
        public static final int favorite_img = 2131230757;
        public static final int next_article_button = 2131230730;
        public static final int operation_bar = 2131230741;
        public static final int parent_layout = 2131230720;
        public static final int poetry_search_btn = 2131230749;
        public static final int pre_article_button = 2131230727;
        public static final int read_mode_layout = 2131230721;
        public static final int search_bar = 2131230747;
        public static final int search_edittext = 2131230748;
        public static final int shici_list = 2131230750;
        public static final int tangshi_item_author_textview = 2131230756;
        public static final int tangshi_item_layout = 2131230753;
        public static final int tangshi_item_title_textview = 2131230754;
        public static final int tangshi_item_type_textview = 2131230755;
        public static final int title_edit_textview = 2131230733;
        public static final int title_read_textview = 2131230722;
        public static final int type_textview = 2131230723;
        public static final int update_button = 2131230737;
        public static final int webview = 2131230752;
    }

    public static final class layout {
        public static final int article_activity = 2130903040;
        public static final int main = 2130903041;
        public static final int readonline = 2130903042;
        public static final int tangshi_item = 2130903043;
    }

    public static final class raw {
        public static final int mingju = 2130968576;
        public static final int songci = 2130968577;
        public static final int tangshi = 2130968578;
    }

    public static final class string {
        public static final int app_name = 2131099649;
        public static final int favorite_text = 2131099654;
        public static final int hello = 2131099648;
        public static final int ming_ju_text = 2131099652;
        public static final int other_text = 2131099653;
        public static final int searching_text = 2131099655;
        public static final int song_ci_text = 2131099651;
        public static final int tang_shi_text = 2131099650;
    }

    public static final class style {
        public static final int bottom_tool_bar_button_style = 2131165184;
    }

    public static final class styleable {
        public static final int[] net_youmi_android_AdView = {R.attr.backgroundColor, R.attr.textColor, R.attr.backgroundTransparent};
        public static final int net_youmi_android_AdView_backgroundColor = 0;
        public static final int net_youmi_android_AdView_backgroundTransparent = 2;
        public static final int net_youmi_android_AdView_textColor = 1;
    }
}
