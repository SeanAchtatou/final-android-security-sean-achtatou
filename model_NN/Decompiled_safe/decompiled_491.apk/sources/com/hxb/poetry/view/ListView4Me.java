package com.hxb.poetry.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ListView;

public class ListView4Me extends ListView {
    public float x;

    public ListView4Me(Context context) {
        super(context);
    }

    public ListView4Me(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public boolean onInterceptTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case 0:
                this.x = ev.getX();
                break;
        }
        return super.onInterceptTouchEvent(ev);
    }
}
