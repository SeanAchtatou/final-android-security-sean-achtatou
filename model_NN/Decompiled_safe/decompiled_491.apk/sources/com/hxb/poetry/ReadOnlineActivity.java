package com.hxb.poetry;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class ReadOnlineActivity extends Activity {
    WebView mWebView;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.readonline);
        this.mWebView = (WebView) findViewById(R.id.webview);
        this.mWebView.getSettings().setJavaScriptEnabled(true);
        this.mWebView.loadUrl("http://m.chinapoesy.com/");
    }
}
