package com.hxb.poetry.util;

public class HashMap<Integer, String> extends java.util.HashMap<Integer, String> {
    private static final long serialVersionUID = 1;

    public int getKey(String value) {
        for (Integer i : keySet()) {
            if (getValue(i).equals(value)) {
                return ((Integer) i).intValue();
            }
        }
        return 10;
    }

    public String getValue(Integer key) {
        return super.get(key);
    }
}
