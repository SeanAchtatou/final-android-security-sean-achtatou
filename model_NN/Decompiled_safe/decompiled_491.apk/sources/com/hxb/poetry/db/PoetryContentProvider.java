package com.hxb.poetry.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.provider.BaseColumns;
import com.hxb.poetry.R;
import com.hxb.poetry.util.HashMap;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

public class PoetryContentProvider extends ContentProvider implements BaseColumns {
    public static final String AUTHORITY = "com.hxb.poetry";
    public static final Uri CONTENT_URI = Uri.parse("content://com.hxb.poetry/poetry");
    public static final int POETRY = 0;
    public static final int POETRY_ITEM = 1;
    private static UriMatcher mUriMatcher = new UriMatcher(-1);
    public static HashMap<Integer, String> typeMap = new HashMap<>();
    private PoetryDatabaseHelper dbHelper;

    public interface IPoetry {
        public static final String ANALYSIS = "t_analysis";
        public static final String AUTHOR = "p_author";
        public static final String COMMENT = "t_comment";
        public static final String CONTENT = "t_content";
        public static final String CREATE_TABLE_SQL = "create table t_poetry(_id integer primary key autoincrement,t_type integer not null default 10,p_author varchar(50) ,t_title varchar(50),t_content text not null,t_comment text ,t_analysis text ,t_favorite_status integer default 0)";
        public static final String DB_NAME = "poetry.db";
        public static final String FAVORITE = "t_favorite_status";
        public static final int FAVORITE_MARK = 2;
        public static final int FAVORITE_OFF = 0;
        public static final int FAVORITE_ON = 1;
        public static final String TABLE_NAME = "t_poetry";
        public static final String TITLE = "t_title";
        public static final String TYPE = "t_type";
        public static final int VERSION = 1;
    }

    static {
        typeMap.put(1, "五言古诗");
        typeMap.put(2, "五言律诗");
        typeMap.put(3, "七言古诗");
        typeMap.put(4, "七言律诗");
        typeMap.put(5, "乐府");
        typeMap.put(6, "五言乐府");
        typeMap.put(7, "七言乐府");
        typeMap.put(8, "宋词");
        typeMap.put(9, "名句");
        typeMap.put(10, "其它");
        mUriMatcher.addURI(AUTHORITY, "poetry", 0);
        mUriMatcher.addURI(AUTHORITY, "poetry/#", 1);
    }

    public boolean onCreate() {
        this.dbHelper = new PoetryDatabaseHelper(getContext(), IPoetry.DB_NAME, null, 1);
        return true;
    }

    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(IPoetry.TABLE_NAME);
        switch (mUriMatcher.match(uri)) {
            case 0:
                break;
            default:
                throw new IllegalArgumentException("Unknown uri for " + uri);
            case 1:
                builder.appendWhere("_id=" + uri.getPathSegments().get(1));
                break;
        }
        if (sortOrder == null || sortOrder.equals("")) {
        }
        return builder.query(this.dbHelper.getReadableDatabase(), projection, selection, selectionArgs, null, null, null);
    }

    public String getType(Uri uri) {
        return null;
    }

    public Uri insert(Uri uri, ContentValues values) {
        if (mUriMatcher.match(uri) == 0) {
            return ContentUris.withAppendedId(uri, this.dbHelper.getWritableDatabase().insert(IPoetry.TABLE_NAME, "", values));
        }
        throw new IllegalArgumentException("Unknown uri for " + uri);
    }

    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SQLiteDatabase db = this.dbHelper.getWritableDatabase();
        switch (mUriMatcher.match(uri)) {
            case 0:
                return db.update(IPoetry.TABLE_NAME, values, selection, selectionArgs);
            case 1:
                return db.update(IPoetry.TABLE_NAME, values, "_id=?", new String[]{uri.getPathSegments().get(1)});
            default:
                throw new IllegalArgumentException("Unknown uri for " + uri);
        }
    }

    class PoetryDatabaseHelper extends SQLiteOpenHelper implements Runnable {
        private SQLiteDatabase db;

        public PoetryDatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        public void onCreate(SQLiteDatabase db2) {
            db2.execSQL(IPoetry.CREATE_TABLE_SQL);
            this.db = db2;
            new Thread(this).start();
        }

        public void onUpgrade(SQLiteDatabase db2, int oldVersion, int newVersion) {
            db2.execSQL("drop table t_poetry");
            onCreate(db2);
        }

        public void run() {
            UnsupportedEncodingException e;
            IOException e2;
            Resources.NotFoundException e3;
            BufferedReader br;
            String str;
            String str2;
            String str3;
            String str4;
            String str5;
            String str6;
            String str7;
            try {
                this.db.beginTransaction();
                InputStreamReader isr = new InputStreamReader(PoetryContentProvider.this.getContext().getResources().openRawResource(R.raw.tangshi), "utf-8");
                try {
                    br = new BufferedReader(isr, 1024);
                    while (true) {
                        String content = "";
                        try {
                            ContentValues cv = new ContentValues();
                            do {
                                str = br.readLine();
                                if (str == null) {
                                    break;
                                }
                            } while (str.trim().equals(""));
                            if (str != null) {
                                String title = str.trim();
                                String str8 = br.readLine();
                                if (str8 == null) {
                                    break;
                                }
                                int type = PoetryContentProvider.typeMap.getKey(str8.trim());
                                String str9 = br.readLine();
                                if (str9 == null) {
                                    break;
                                }
                                String author = str9.trim();
                                while (true) {
                                    str2 = br.readLine();
                                    if (str2 == null || str2.trim().startsWith("【注解】")) {
                                        String comment = String.valueOf(str2) + "\n";
                                    } else {
                                        content = String.valueOf(content) + str2.trim() + "\n";
                                    }
                                }
                                String comment2 = String.valueOf(str2) + "\n";
                                while (true) {
                                    str3 = br.readLine();
                                    if (str3 == null || str3.trim().startsWith("【赏析】")) {
                                        String ayalysis = String.valueOf(str3) + "\n";
                                    } else {
                                        comment2 = String.valueOf(comment2) + str3.trim();
                                    }
                                }
                                String ayalysis2 = String.valueOf(str3) + "\n";
                                while (true) {
                                    String str10 = br.readLine();
                                    if (str10 == null || str10.trim().equals("")) {
                                        cv.put(IPoetry.ANALYSIS, ayalysis2);
                                        cv.put(IPoetry.AUTHOR, author);
                                        cv.put(IPoetry.COMMENT, comment2);
                                        cv.put(IPoetry.CONTENT, content);
                                        cv.put(IPoetry.TITLE, title);
                                        cv.put(IPoetry.TYPE, Integer.valueOf(type));
                                        this.db.insert(IPoetry.TABLE_NAME, "", cv);
                                    } else {
                                        ayalysis2 = String.valueOf(ayalysis2) + str10.trim();
                                    }
                                }
                                cv.put(IPoetry.ANALYSIS, ayalysis2);
                                cv.put(IPoetry.AUTHOR, author);
                                cv.put(IPoetry.COMMENT, comment2);
                                cv.put(IPoetry.CONTENT, content);
                                cv.put(IPoetry.TITLE, title);
                                cv.put(IPoetry.TYPE, Integer.valueOf(type));
                                this.db.insert(IPoetry.TABLE_NAME, "", cv);
                            } else {
                                break;
                            }
                        } catch (Resources.NotFoundException e4) {
                            e3 = e4;
                            e3.printStackTrace();
                        } catch (UnsupportedEncodingException e5) {
                            e = e5;
                            e.printStackTrace();
                        } catch (IOException e6) {
                            e2 = e6;
                            e2.printStackTrace();
                        }
                    }
                } catch (Resources.NotFoundException e7) {
                    e3 = e7;
                    e3.printStackTrace();
                } catch (UnsupportedEncodingException e8) {
                    e = e8;
                    e.printStackTrace();
                } catch (IOException e9) {
                    e2 = e9;
                    e2.printStackTrace();
                }
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(PoetryContentProvider.this.getContext().getResources().openRawResource(R.raw.songci), "utf-8"), 1024);
                    int typeSongci = PoetryContentProvider.typeMap.getKey("宋词");
                    while (true) {
                        String content2 = "";
                        ContentValues cv2 = new ContentValues();
                        do {
                            str4 = bufferedReader.readLine();
                            if (str4 == null) {
                                break;
                            }
                        } while (str4.trim().equals(""));
                        if (str4 != null) {
                            String title2 = str4.trim().substring(1, str4.trim().length() - 1);
                            String str11 = bufferedReader.readLine();
                            if (str11 == null) {
                                break;
                            }
                            String author2 = str11.trim();
                            while (true) {
                                str5 = bufferedReader.readLine();
                                if (str5 == null || str5.trim().startsWith("【注解】")) {
                                    String comment3 = String.valueOf(str5.trim()) + "\n";
                                } else {
                                    content2 = String.valueOf(content2) + str5.trim() + "\n";
                                }
                            }
                            String comment32 = String.valueOf(str5.trim()) + "\n";
                            while (true) {
                                str6 = bufferedReader.readLine();
                                if (str6 == null || str6.trim().startsWith("【赏析】")) {
                                    String ayalysis3 = String.valueOf(str6.trim()) + "\n";
                                } else {
                                    comment32 = String.valueOf(comment32) + str6;
                                }
                            }
                            String ayalysis32 = String.valueOf(str6.trim()) + "\n";
                            while (true) {
                                String str12 = bufferedReader.readLine();
                                if (str12 == null || str12.trim().equals("")) {
                                    cv2.put(IPoetry.ANALYSIS, ayalysis32);
                                    cv2.put(IPoetry.AUTHOR, author2);
                                    cv2.put(IPoetry.COMMENT, comment32);
                                    cv2.put(IPoetry.CONTENT, content2);
                                    cv2.put(IPoetry.TITLE, title2);
                                    cv2.put(IPoetry.TYPE, Integer.valueOf(typeSongci));
                                    this.db.insert(IPoetry.TABLE_NAME, "", cv2);
                                } else {
                                    ayalysis32 = String.valueOf(ayalysis32) + str12;
                                }
                            }
                            cv2.put(IPoetry.ANALYSIS, ayalysis32);
                            cv2.put(IPoetry.AUTHOR, author2);
                            cv2.put(IPoetry.COMMENT, comment32);
                            cv2.put(IPoetry.CONTENT, content2);
                            cv2.put(IPoetry.TITLE, title2);
                            cv2.put(IPoetry.TYPE, Integer.valueOf(typeSongci));
                            this.db.insert(IPoetry.TABLE_NAME, "", cv2);
                        } else {
                            break;
                        }
                    }
                    isr = new InputStreamReader(PoetryContentProvider.this.getContext().getResources().openRawResource(R.raw.mingju), "utf-8");
                    br = new BufferedReader(isr, 1024);
                    int typeMingju = PoetryContentProvider.typeMap.getKey("名句");
                    int count = 0;
                    while (true) {
                        ContentValues cv3 = new ContentValues();
                        do {
                            str7 = br.readLine();
                            if (str7 == null) {
                                break;
                            }
                        } while (str7.trim().equals(""));
                        if (str7 == null || str7.trim().equals("")) {
                            this.db.setTransactionSuccessful();
                            this.db.endTransaction();
                            this.db.close();
                            BufferedReader bufferedReader2 = br;
                            InputStreamReader inputStreamReader = isr;
                        } else {
                            String content3 = str7.trim();
                            count++;
                            int i = content3.indexOf(".");
                            if (i == -1) {
                                i = content3.indexOf(" ");
                            }
                            if (i >= 0 && i <= 4) {
                                content3 = String.valueOf(count) + "-" + content3.substring(i + 1);
                            }
                            cv3.put(IPoetry.ANALYSIS, "");
                            cv3.put(IPoetry.AUTHOR, "");
                            cv3.put(IPoetry.COMMENT, "");
                            cv3.put(IPoetry.CONTENT, content3);
                            cv3.put(IPoetry.TITLE, "名言警句");
                            cv3.put(IPoetry.TYPE, Integer.valueOf(typeMingju));
                            this.db.insert(IPoetry.TABLE_NAME, "", cv3);
                        }
                    }
                    this.db.setTransactionSuccessful();
                    this.db.endTransaction();
                    this.db.close();
                    BufferedReader bufferedReader22 = br;
                    InputStreamReader inputStreamReader2 = isr;
                } catch (Resources.NotFoundException e10) {
                    e3 = e10;
                    e3.printStackTrace();
                } catch (UnsupportedEncodingException e11) {
                    e = e11;
                    e.printStackTrace();
                } catch (IOException e12) {
                    e2 = e12;
                    e2.printStackTrace();
                }
            } catch (Resources.NotFoundException e13) {
                e3 = e13;
                e3.printStackTrace();
            } catch (UnsupportedEncodingException e14) {
                e = e14;
                e.printStackTrace();
            } catch (IOException e15) {
                e2 = e15;
                e2.printStackTrace();
            }
        }
    }
}
