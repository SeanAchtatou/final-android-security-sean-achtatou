package com.hxb.poetry;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.hxb.poetry.db.PoetryContentProvider;
import java.util.ArrayList;
import java.util.List;
import net.youmi.android.AdView;

public class ArticleActivity extends Activity implements View.OnClickListener {
    private static final int MENU_ITEM_ABOUT = 1;
    private static final int MENU_ITEM_EDIT = 3;
    private static final int MENU_ITEM_EXIT = 2;
    private static final int MENU_ITEM_SEND_SMS = 0;
    AdView adViewEditMode;
    AdView adViewReadMode;
    private ArticleBean article;
    private EditText mAuthorEditText;
    private TextView mAuthorTextView;
    private Button mBackButton;
    private Button mCancelButton;
    private EditText mContentEditText;
    private EditText mContentReadEditText;
    private ScrollView mContentScrollView;
    private LinearLayout mEditModeLayout;
    private Button mFavoriteButton;
    private Button mNextArticleButton;
    private LinearLayout mParentLayout;
    private Button mPreArticleButton;
    private LinearLayout mReadModeLayout;
    private Spinner mSpinner;
    private EditText mTitleEditText;
    private TextView mTitleTextView;
    private TextView mTypeTextView;
    private Button mUpdateArticleButton;
    private boolean resize = false;
    private boolean resizeEditMode = false;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.article_activity);
        Intent i = getIntent();
        int _id = -1;
        if (!(i == null || i.getExtras() == null)) {
            _id = i.getExtras().getInt("_id");
        }
        if (_id == -1) {
            finish();
        }
        initArticle(_id);
        setupView();
    }

    private void initArticle(int _id) {
        Cursor c = getContentResolver().query(ContentUris.withAppendedId(PoetryContentProvider.CONTENT_URI, (long) _id), null, null, null, null);
        if (c.moveToFirst()) {
            this.article = new ArticleBean();
            this.article.setAuthor(c.getString(c.getColumnIndexOrThrow(PoetryContentProvider.IPoetry.AUTHOR)));
            this.article.setContent(c.getString(c.getColumnIndexOrThrow(PoetryContentProvider.IPoetry.CONTENT)));
            this.article.setFavorite(c.getInt(c.getColumnIndexOrThrow(PoetryContentProvider.IPoetry.FAVORITE)));
            this.article.setId(_id);
            this.article.setTitle(c.getString(c.getColumnIndexOrThrow(PoetryContentProvider.IPoetry.TITLE)));
            this.article.setType(c.getInt(c.getColumnIndexOrThrow(PoetryContentProvider.IPoetry.TYPE)));
            this.article.setComment(c.getString(c.getColumnIndexOrThrow(PoetryContentProvider.IPoetry.COMMENT)));
            this.article.setAnalysis(c.getString(c.getColumnIndexOrThrow(PoetryContentProvider.IPoetry.ANALYSIS)));
        }
    }

    private void setupView() {
        this.mBackButton = (Button) findViewById(R.id.back_button);
        this.mFavoriteButton = (Button) findViewById(R.id.favorite_button);
        this.mNextArticleButton = (Button) findViewById(R.id.next_article_button);
        this.mPreArticleButton = (Button) findViewById(R.id.pre_article_button);
        this.mUpdateArticleButton = (Button) findViewById(R.id.update_button);
        this.mCancelButton = (Button) findViewById(R.id.cancel_button);
        this.mReadModeLayout = (LinearLayout) findViewById(R.id.read_mode_layout);
        this.mEditModeLayout = (LinearLayout) findViewById(R.id.edit_mode_layout);
        this.mContentScrollView = (ScrollView) findViewById(R.id.content_scrollview);
        this.mParentLayout = (LinearLayout) findViewById(R.id.parent_layout);
        this.adViewReadMode = (AdView) findViewById(R.id.adViewReadMod);
        this.mTitleTextView = (TextView) findViewById(R.id.title_read_textview);
        this.mTitleEditText = (EditText) findViewById(R.id.title_edit_textview);
        this.mTypeTextView = (TextView) findViewById(R.id.type_textview);
        this.mAuthorTextView = (TextView) findViewById(R.id.author_textview);
        this.mAuthorEditText = (EditText) findViewById(R.id.author_edit_textview);
        this.mContentReadEditText = (EditText) findViewById(R.id.content_read_textview);
        this.mContentEditText = (EditText) findViewById(R.id.content_edit_textview);
        this.mFavoriteButton.setOnClickListener(this);
        this.mNextArticleButton.setOnClickListener(this);
        this.mPreArticleButton.setOnClickListener(this);
        this.mBackButton.setOnClickListener(this);
        parseArticleToUI();
        this.mUpdateArticleButton.setOnClickListener(this);
        this.mCancelButton.setOnClickListener(this);
        this.mSpinner = (Spinner) findViewById(R.id.article_type_spinner);
        List<ArticleType> typeList = new ArrayList<>();
        for (K key : PoetryContentProvider.typeMap.keySet()) {
            typeList.add(new ArticleType(key, (String) PoetryContentProvider.typeMap.get(key)));
        }
        this.mSpinner.setAdapter((SpinnerAdapter) new ArrayAdapter(this, 17367049, typeList));
    }

    private void parseArticleToUI() {
        if (this.article.getFavorite() == 1) {
            this.mFavoriteButton.setText("取消收藏");
        }
        if (this.article.getFavorite() == 0) {
            this.mFavoriteButton.setText("添加收藏");
        }
        this.mTitleTextView.setText(this.article.getTitle());
        this.mTitleEditText.setText(this.mTitleTextView.getText());
        this.mTypeTextView.setText(PoetryContentProvider.typeMap.getValue(Integer.valueOf(this.article.getType())));
        this.mAuthorTextView.setText(this.article.getAuthor());
        this.mAuthorEditText.setText(this.article.getAuthor());
        this.mContentReadEditText.setText(String.valueOf(this.article.getContent()) + "\n" + this.article.getComment() + "\n" + this.article.getAnalysis());
        this.mContentEditText.setText(this.mContentReadEditText.getText());
    }

    public void onClick(View v) {
        if (v == this.mBackButton) {
            finish();
            return;
        }
        if (v == this.mFavoriteButton) {
            ContentValues cv = new ContentValues();
            boolean favorite = this.article.getFavorite() == 1;
            cv.put(PoetryContentProvider.IPoetry.FAVORITE, Integer.valueOf(favorite ? 0 : 1));
            if (getContentResolver().update(ContentUris.withAppendedId(PoetryContentProvider.CONTENT_URI, (long) this.article.getId()), cv, null, null) != 1) {
                return;
            }
            if (favorite) {
                this.article.setFavorite(0);
                this.mFavoriteButton.setText("添加收藏");
                Toast.makeText(this, "已取消收藏!", 0).show();
                return;
            }
            this.article.setFavorite(1);
            this.mFavoriteButton.setText("取消收藏");
            Toast.makeText(this, "已添加收藏", 0).show();
            return;
        }
        if (v == this.mNextArticleButton) {
            nextPage();
            return;
        }
        if (v == this.mPreArticleButton) {
            prePage();
            return;
        }
        if (v == this.mUpdateArticleButton) {
            try {
                ContentValues cv2 = new ContentValues();
                cv2.put(PoetryContentProvider.IPoetry.TITLE, this.mTitleEditText.getText().toString());
                cv2.put(PoetryContentProvider.IPoetry.AUTHOR, this.mAuthorEditText.getText().toString());
                int commentIndex = this.mContentEditText.getText().toString().indexOf("【注解】");
                int ayalysisIndex = this.mContentEditText.getText().toString().indexOf("【赏析】");
                String content = this.mContentEditText.getText().toString();
                if (commentIndex > 0) {
                    cv2.put(PoetryContentProvider.IPoetry.CONTENT, content.substring(0, commentIndex));
                    if (ayalysisIndex > 0) {
                        cv2.put(PoetryContentProvider.IPoetry.COMMENT, content.substring(commentIndex, ayalysisIndex));
                        cv2.put(PoetryContentProvider.IPoetry.ANALYSIS, content.substring(ayalysisIndex));
                    } else {
                        cv2.put(PoetryContentProvider.IPoetry.COMMENT, content.substring(commentIndex));
                    }
                } else if (ayalysisIndex > 0) {
                    cv2.put(PoetryContentProvider.IPoetry.CONTENT, content.substring(0, ayalysisIndex));
                    cv2.put(PoetryContentProvider.IPoetry.ANALYSIS, content.substring(ayalysisIndex));
                }
                int update = getContentResolver().update(PoetryContentProvider.CONTENT_URI, cv2, "_id=?", new String[]{new StringBuilder(String.valueOf(this.article.getId())).toString()});
                initArticle(this.article.getId());
                parseArticleToUI();
                Toast.makeText(this, "修改成功", 0).show();
            } catch (Exception e) {
                Toast.makeText(this, "对不起，无法修改，请联系技术员.", 1).show();
                e.printStackTrace();
            }
            finish();
            return;
        }
        if (v == this.mCancelButton) {
            System.out.println("click cancel button.");
            finish();
        }
    }

    private void nextPage() {
        Cursor c = null;
        switch (MainActivity.focusedRadioButtonIndex) {
            case 0:
                ContentResolver contentResolver = getContentResolver();
                Uri uri = PoetryContentProvider.CONTENT_URI;
                String[] strArr = new String[4];
                strArr[0] = new StringBuilder(String.valueOf(this.article.getId())).toString();
                strArr[1] = "8";
                strArr[2] = "9";
                strArr[MENU_ITEM_EDIT] = "10";
                c = contentResolver.query(uri, null, "_id>? and t_type!=? and t_type!=? and t_type!=?", strArr, "t_title,p_author,t_content limit 1");
                break;
            case 1:
                c = getContentResolver().query(PoetryContentProvider.CONTENT_URI, null, "_id>? and t_type=?", new String[]{new StringBuilder(String.valueOf(this.article.getId())).toString(), "8"}, "t_title,p_author,t_content limit 1");
                break;
            case 2:
                c = getContentResolver().query(PoetryContentProvider.CONTENT_URI, null, "_id>? and t_type=?", new String[]{new StringBuilder(String.valueOf(this.article.getId())).toString(), "9"}, "t_title,p_author,t_content limit 1");
                break;
            case MENU_ITEM_EDIT /*3*/:
                c = getContentResolver().query(PoetryContentProvider.CONTENT_URI, null, "_id>? and (t_type=?", new String[]{new StringBuilder(String.valueOf(this.article.getId())).toString(), "10"}, "t_title,p_author,t_content limit 1");
                break;
            case 4:
                ContentResolver contentResolver2 = getContentResolver();
                Uri uri2 = PoetryContentProvider.CONTENT_URI;
                String[] strArr2 = new String[MENU_ITEM_EDIT];
                strArr2[0] = new StringBuilder(String.valueOf(this.article.getId())).toString();
                strArr2[1] = "2";
                strArr2[2] = "1";
                c = contentResolver2.query(uri2, null, "_id>? and (t_favorite_status=? or t_favorite_status=?)", strArr2, "t_title,p_author,t_content limit 1");
                break;
        }
        if (c.moveToFirst()) {
            this.article = new ArticleBean();
            this.article.setAuthor(c.getString(c.getColumnIndexOrThrow(PoetryContentProvider.IPoetry.AUTHOR)));
            this.article.setContent(c.getString(c.getColumnIndexOrThrow(PoetryContentProvider.IPoetry.CONTENT)));
            this.article.setFavorite(c.getInt(c.getColumnIndexOrThrow(PoetryContentProvider.IPoetry.FAVORITE)));
            this.article.setId(c.getInt(0));
            this.article.setTitle(c.getString(c.getColumnIndexOrThrow(PoetryContentProvider.IPoetry.TITLE)));
            this.article.setType(c.getInt(c.getColumnIndexOrThrow(PoetryContentProvider.IPoetry.TYPE)));
            this.article.setComment(c.getString(c.getColumnIndexOrThrow(PoetryContentProvider.IPoetry.COMMENT)));
            this.article.setAnalysis(c.getString(c.getColumnIndexOrThrow(PoetryContentProvider.IPoetry.ANALYSIS)));
        }
        parseArticleToUI();
    }

    private void prePage() {
        Cursor c = null;
        switch (MainActivity.focusedRadioButtonIndex) {
            case 0:
                ContentResolver contentResolver = getContentResolver();
                Uri uri = PoetryContentProvider.CONTENT_URI;
                String[] strArr = new String[4];
                strArr[0] = new StringBuilder(String.valueOf(this.article.getId())).toString();
                strArr[1] = "8";
                strArr[2] = "9";
                strArr[MENU_ITEM_EDIT] = "10";
                c = contentResolver.query(uri, null, "_id<? and t_type!=? and t_type!=? and t_type!=?", strArr, "t_title,p_author,t_content");
                break;
            case 1:
                c = getContentResolver().query(PoetryContentProvider.CONTENT_URI, null, "_id<? and t_type=?", new String[]{new StringBuilder(String.valueOf(this.article.getId())).toString(), "8"}, "t_title,p_author,t_content");
                break;
            case 2:
                c = getContentResolver().query(PoetryContentProvider.CONTENT_URI, null, "_id<? and t_type=?", new String[]{new StringBuilder(String.valueOf(this.article.getId())).toString(), "9"}, "t_title,p_author,t_content");
                break;
            case MENU_ITEM_EDIT /*3*/:
                c = getContentResolver().query(PoetryContentProvider.CONTENT_URI, null, "_id<? and (t_type=?", new String[]{new StringBuilder(String.valueOf(this.article.getId())).toString(), "10"}, "t_title,p_author,t_content");
                break;
            case 4:
                ContentResolver contentResolver2 = getContentResolver();
                Uri uri2 = PoetryContentProvider.CONTENT_URI;
                String[] strArr2 = new String[MENU_ITEM_EDIT];
                strArr2[0] = new StringBuilder(String.valueOf(this.article.getId())).toString();
                strArr2[1] = "2";
                strArr2[2] = "1";
                c = contentResolver2.query(uri2, null, "_id<? and (t_favorite_status=? or t_favorite_status=?)", strArr2, "t_title,p_author,t_content");
                break;
        }
        if (c.moveToLast()) {
            this.article = new ArticleBean();
            this.article.setAuthor(c.getString(c.getColumnIndexOrThrow(PoetryContentProvider.IPoetry.AUTHOR)));
            this.article.setContent(c.getString(c.getColumnIndexOrThrow(PoetryContentProvider.IPoetry.CONTENT)));
            this.article.setFavorite(c.getInt(c.getColumnIndexOrThrow(PoetryContentProvider.IPoetry.FAVORITE)));
            this.article.setId(c.getInt(0));
            this.article.setTitle(c.getString(c.getColumnIndexOrThrow(PoetryContentProvider.IPoetry.TITLE)));
            this.article.setType(c.getInt(c.getColumnIndexOrThrow(PoetryContentProvider.IPoetry.TYPE)));
            this.article.setComment(c.getString(c.getColumnIndexOrThrow(PoetryContentProvider.IPoetry.COMMENT)));
            this.article.setAnalysis(c.getString(c.getColumnIndexOrThrow(PoetryContentProvider.IPoetry.ANALYSIS)));
        }
        parseArticleToUI();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, "发送短信");
        menu.add(0, 1, 1, "关于");
        menu.add(0, (int) MENU_ITEM_EDIT, 2, "编辑");
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        String smsContent;
        if (item.getItemId() == 0) {
            Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:"));
            if (this.article.getType() == 9) {
                smsContent = this.article.getContent();
            } else {
                smsContent = String.valueOf(this.article.getTitle()) + "\n" + this.article.getContent();
            }
            intent.putExtra("sms_body", smsContent);
            startActivity(intent);
        } else if (item.getItemId() == 1) {
            String version = "";
            try {
                version = getPackageManager().getPackageInfo(getApplicationInfo().packageName, 0).versionName;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            new AlertDialog.Builder(this).setTitle("唐诗宋词").setMessage("唐诗宋词" + version + ",提供了唐诗，宋词，名言警句，收藏，编辑功能,同时具备文章注解与赏析。长按文章列表选项可以通过短信发送给好友。单击进入文章后，可以直接阅读上一篇，下一篇,无须返回文章列表进行选择，在文章列表和文章阅读中均提供收藏和取消收藏,发送短信功能，操作非常方便。在文章列表主界面，可以通过左右滑动进行唐诗，宋词，名句，收藏切换。欢迎使用唐诗宋词，有好的建议请发送到：huangxibo@126.com").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            }).show();
        } else if (item.getItemId() == MENU_ITEM_EDIT) {
            this.mReadModeLayout.setVisibility(8);
            this.mEditModeLayout.setVisibility(0);
        }
        return super.onOptionsItemSelected(item);
    }

    public void finish() {
        if (this.mReadModeLayout.getVisibility() == 8) {
            this.mReadModeLayout.setVisibility(0);
            this.mEditModeLayout.setVisibility(8);
            return;
        }
        super.finish();
    }

    class ArticleType {
        private int id;
        private String typeName;

        public ArticleType(Integer key, String string) {
            this.id = key.intValue();
            this.typeName = string;
        }

        public int getId() {
            return this.id;
        }

        public void setId(int id2) {
            this.id = id2;
        }

        public String getTypeName() {
            return this.typeName;
        }

        public void setTypeName(String typeName2) {
            this.typeName = typeName2;
        }

        public String toString() {
            return this.typeName;
        }
    }
}
