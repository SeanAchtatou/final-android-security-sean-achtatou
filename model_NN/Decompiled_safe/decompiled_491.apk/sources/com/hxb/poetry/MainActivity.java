package com.hxb.poetry;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.hxb.poetry.db.PoetryContentProvider;
import com.hxb.poetry.view.ListView4Me;
import java.util.ArrayList;
import java.util.List;
import net.youmi.android.AdManager;

public class MainActivity extends Activity implements RadioGroup.OnCheckedChangeListener, View.OnClickListener {
    private static final int MENU_ITEM_ABOUT = 1;
    private static final int MENU_ITEM_EXIT = 2;
    private static final int MENU_ITEM_ONLINE = 3;
    private static final int MENU_ITEM_SEARCH = 0;
    public static int focusedRadioButtonIndex = 0;
    private static final String sortOrderFavorite = "t_type,t_title";
    private static int[] titleBgArr;
    /* access modifiers changed from: private */
    public ListView4Me listView;
    private RadioButton mFavoriteRadioButton;
    private RadioButton mMingjuRadioButton;
    private RadioButton mOtherRadioButton;
    private RadioGroup mRadioGroup;
    private LinearLayout mSearchBarLayout;
    private Button mSearchButton;
    private EditText mSearchEditText;
    private RadioButton mSongciRadioButton;
    /* access modifiers changed from: private */
    public Cursor mTangshiCursor;
    private RadioButton mTangshiRadioButton;
    /* access modifiers changed from: private */
    public List<RadioButton> radioButtonList;
    private boolean resize = false;
    /* access modifiers changed from: private */
    public TangshiCursorAdapter tangshiCursorAdapter;
    /* access modifiers changed from: private */
    public float x;

    static {
        int[] iArr = new int[MENU_ITEM_ONLINE];
        // fill-array-data instruction
        iArr[0] = 2130837520;
        iArr[1] = 2130837522;
        iArr[2] = 2130837524;
        titleBgArr = iArr;
        AdManager.init("ba9533f3f11b6b74", "091cf23ba67f8734", 30, false);
    }

    /* access modifiers changed from: private */
    public int getNextFocusedRadioButtonIndex() {
        focusedRadioButtonIndex++;
        if (focusedRadioButtonIndex == 5) {
            focusedRadioButtonIndex = 0;
        }
        if (focusedRadioButtonIndex == MENU_ITEM_ONLINE) {
            focusedRadioButtonIndex++;
        }
        return focusedRadioButtonIndex;
    }

    /* access modifiers changed from: private */
    public int getPreFocusedRadioButtonIndex() {
        focusedRadioButtonIndex--;
        if (focusedRadioButtonIndex == -1) {
            focusedRadioButtonIndex = 4;
        }
        if (focusedRadioButtonIndex == MENU_ITEM_ONLINE) {
            focusedRadioButtonIndex--;
        }
        return focusedRadioButtonIndex;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        setupView();
    }

    private void setupView() {
        this.mRadioGroup = (RadioGroup) findViewById(R.id.operation_bar);
        this.mTangshiRadioButton = (RadioButton) findViewById(R.id.btn_tangshi);
        this.mSongciRadioButton = (RadioButton) findViewById(R.id.btn_songci);
        this.mMingjuRadioButton = (RadioButton) findViewById(R.id.btn_mingju);
        this.mOtherRadioButton = (RadioButton) findViewById(R.id.btn_other);
        this.mFavoriteRadioButton = (RadioButton) findViewById(R.id.btn_favorite);
        this.mSearchBarLayout = (LinearLayout) findViewById(R.id.search_bar);
        this.mSearchButton = (Button) findViewById(R.id.poetry_search_btn);
        this.mSearchEditText = (EditText) findViewById(R.id.search_edittext);
        this.mRadioGroup.setOnCheckedChangeListener(this);
        this.mSearchButton.setOnClickListener(this);
        this.radioButtonList = new ArrayList();
        this.radioButtonList.add(this.mTangshiRadioButton);
        this.radioButtonList.add(this.mSongciRadioButton);
        this.radioButtonList.add(this.mMingjuRadioButton);
        this.radioButtonList.add(this.mOtherRadioButton);
        this.radioButtonList.add(this.mFavoriteRadioButton);
        this.listView = (ListView4Me) findViewById(R.id.shici_list);
        this.listView.setOnTouchListener(new View.OnTouchListener() {
            /* Debug info: failed to restart local var, previous not found, register: 4 */
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case 1:
                        float access$3 = MainActivity.this.x - event.getX();
                        if (MainActivity.this.listView.x - event.getX() > 200.0f) {
                            ((RadioButton) MainActivity.this.radioButtonList.get(MainActivity.this.getNextFocusedRadioButtonIndex())).setChecked(true);
                        }
                        if (MainActivity.this.listView.x - event.getX() >= -200.0f) {
                            return false;
                        }
                        ((RadioButton) MainActivity.this.radioButtonList.get(MainActivity.this.getPreFocusedRadioButtonIndex())).setChecked(true);
                        return false;
                    default:
                        return false;
                }
            }
        });
        this.mTangshiRadioButton.setChecked(true);
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.btn_tangshi /*2131230742*/:
                focusedRadioButtonIndex = 0;
                ContentResolver contentResolver = getContentResolver();
                Uri uri = PoetryContentProvider.CONTENT_URI;
                String[] strArr = new String[MENU_ITEM_ONLINE];
                strArr[0] = "8";
                strArr[1] = "9";
                strArr[2] = "10";
                this.mTangshiCursor = contentResolver.query(uri, null, "t_type!=? and t_type!=? and t_type!=?", strArr, null);
                startManagingCursor(this.mTangshiCursor);
                this.tangshiCursorAdapter = new TangshiCursorAdapter(this, this.mTangshiCursor);
                this.listView.setAdapter((ListAdapter) this.tangshiCursorAdapter);
                return;
            case R.id.btn_songci /*2131230743*/:
                focusedRadioButtonIndex = 1;
                this.mTangshiCursor = getContentResolver().query(PoetryContentProvider.CONTENT_URI, null, "t_type=?", new String[]{"8"}, null);
                startManagingCursor(this.mTangshiCursor);
                this.tangshiCursorAdapter = new TangshiCursorAdapter(this, this.mTangshiCursor);
                this.listView.setAdapter((ListAdapter) this.tangshiCursorAdapter);
                return;
            case R.id.btn_mingju /*2131230744*/:
                focusedRadioButtonIndex = 2;
                this.mTangshiCursor = getContentResolver().query(PoetryContentProvider.CONTENT_URI, null, "t_type=?", new String[]{"9"}, null);
                startManagingCursor(this.mTangshiCursor);
                this.tangshiCursorAdapter = new TangshiCursorAdapter(this, this.mTangshiCursor);
                this.listView.setAdapter((ListAdapter) this.tangshiCursorAdapter);
                return;
            case R.id.btn_other /*2131230745*/:
                focusedRadioButtonIndex = MENU_ITEM_ONLINE;
                this.mTangshiCursor = getContentResolver().query(PoetryContentProvider.CONTENT_URI, null, "t_type=?", new String[]{"10"}, null);
                startManagingCursor(this.mTangshiCursor);
                this.tangshiCursorAdapter = new TangshiCursorAdapter(this, this.mTangshiCursor);
                this.listView.setAdapter((ListAdapter) this.tangshiCursorAdapter);
                return;
            case R.id.btn_favorite /*2131230746*/:
                focusedRadioButtonIndex = 4;
                this.mTangshiCursor = getContentResolver().query(PoetryContentProvider.CONTENT_URI, null, "t_favorite_status=? or t_favorite_status=?", new String[]{"1", "2"}, sortOrderFavorite);
                startManagingCursor(this.mTangshiCursor);
                this.tangshiCursorAdapter = new TangshiCursorAdapter(this, this.mTangshiCursor);
                this.listView.setAdapter((ListAdapter) this.tangshiCursorAdapter);
                return;
            default:
                return;
        }
    }

    class TangshiCursorAdapter extends CursorAdapter {
        LayoutInflater inflater;

        public TangshiCursorAdapter(Context context, Cursor c) {
            super(context, c);
            this.inflater = (LayoutInflater) MainActivity.this.getSystemService("layout_inflater");
        }

        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            RelativeLayout layout = (RelativeLayout) this.inflater.inflate((int) R.layout.tangshi_item, (ViewGroup) null);
            layout.setTag(new TangshiCompent((TextView) layout.findViewById(R.id.tangshi_item_title_textview), (TextView) layout.findViewById(R.id.tangshi_item_author_textview), (TextView) layout.findViewById(R.id.tangshi_item_type_textview), (ImageView) layout.findViewById(R.id.favorite_img)));
            return layout;
        }

        public void bindView(View view, Context context, Cursor cursor) {
            final int type = cursor.getInt(cursor.getColumnIndexOrThrow(PoetryContentProvider.IPoetry.TYPE));
            final String content = cursor.getString(cursor.getColumnIndexOrThrow(PoetryContentProvider.IPoetry.CONTENT));
            switch (MainActivity.focusedRadioButtonIndex) {
                case 0:
                    view.setBackgroundResource(R.drawable.poetry_title_bg_1_selector);
                    break;
                case 1:
                    view.setBackgroundResource(R.drawable.poetry_title_bg_2_selector);
                    break;
                case 2:
                    view.setBackgroundResource(R.drawable.poetry_title_bg_3_selector);
                    break;
                case MainActivity.MENU_ITEM_ONLINE /*3*/:
                    view.setBackgroundResource(R.drawable.poetry_title_bg_3_selector);
                    break;
                case 4:
                    if (type != 8) {
                        if (type != 9) {
                            view.setBackgroundResource(R.drawable.poetry_title_bg_1_selector);
                            break;
                        } else {
                            view.setBackgroundResource(R.drawable.poetry_title_bg_3_selector);
                            break;
                        }
                    } else {
                        view.setBackgroundResource(R.drawable.poetry_title_bg_2_selector);
                        break;
                    }
            }
            final int _id = cursor.getInt(0);
            final int favorite = cursor.getInt(cursor.getColumnIndexOrThrow(PoetryContentProvider.IPoetry.FAVORITE));
            TangshiCompent c = (TangshiCompent) view.getTag();
            final String title = cursor.getString(cursor.getColumnIndexOrThrow(PoetryContentProvider.IPoetry.TITLE));
            if (MainActivity.focusedRadioButtonIndex == 2) {
                c.titleTextView.setGravity(16);
            }
            if (type == 9) {
                c.titleTextView.setText(content);
            } else {
                c.titleTextView.setText(title);
            }
            c.typeTextView.setText(PoetryContentProvider.typeMap.getValue(Integer.valueOf(cursor.getInt(cursor.getColumnIndexOrThrow(PoetryContentProvider.IPoetry.TYPE)))));
            String author = cursor.getString(cursor.getColumnIndexOrThrow(PoetryContentProvider.IPoetry.AUTHOR));
            if (author == null || author.equals("")) {
                c.authorTextView.setText("");
            } else {
                c.authorTextView.setText(author);
            }
            if (favorite == 1) {
                c.favoriteImage.setImageResource(R.drawable.poetry_favorite_on);
            } else {
                c.favoriteImage.setImageResource(R.drawable.poetry_favorite_off);
            }
            c.favoriteImage.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    int i;
                    ContentValues cv = new ContentValues();
                    if (favorite == 1) {
                        i = 0;
                    } else {
                        i = 1;
                    }
                    cv.put(PoetryContentProvider.IPoetry.FAVORITE, Integer.valueOf(i));
                    int update = MainActivity.this.getContentResolver().update(PoetryContentProvider.CONTENT_URI, cv, "_id=?", new String[]{new StringBuilder(String.valueOf(_id)).toString()});
                    switch (MainActivity.focusedRadioButtonIndex) {
                        case 0:
                            MainActivity access$0 = MainActivity.this;
                            ContentResolver contentResolver = MainActivity.this.getContentResolver();
                            Uri uri = PoetryContentProvider.CONTENT_URI;
                            String[] strArr = new String[MainActivity.MENU_ITEM_ONLINE];
                            strArr[0] = "8";
                            strArr[1] = "9";
                            strArr[2] = "10";
                            access$0.mTangshiCursor = contentResolver.query(uri, null, "t_type!=? and t_type!=? and t_type!=?", strArr, null);
                            break;
                        case 1:
                            MainActivity.this.mTangshiCursor = MainActivity.this.getContentResolver().query(PoetryContentProvider.CONTENT_URI, null, "t_type=?", new String[]{"8"}, null);
                            break;
                        case 2:
                            MainActivity.this.mTangshiCursor = MainActivity.this.getContentResolver().query(PoetryContentProvider.CONTENT_URI, null, "t_type=?", new String[]{"9"}, null);
                            break;
                        case MainActivity.MENU_ITEM_ONLINE /*3*/:
                            MainActivity.this.mTangshiCursor = MainActivity.this.getContentResolver().query(PoetryContentProvider.CONTENT_URI, null, "t_type=?", new String[]{"10"}, null);
                            break;
                        case 4:
                            MainActivity.this.mTangshiCursor = MainActivity.this.getContentResolver().query(PoetryContentProvider.CONTENT_URI, null, "t_favorite_status=? or t_favorite_status=?", new String[]{"1", "2"}, MainActivity.sortOrderFavorite);
                            break;
                    }
                    MainActivity.this.tangshiCursorAdapter.changeCursor(MainActivity.this.mTangshiCursor);
                    MainActivity.this.tangshiCursorAdapter.notifyDataSetChanged();
                }
            });
            view.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent intent = new Intent(MainActivity.this, ArticleActivity.class);
                    intent.putExtra("_id", _id);
                    MainActivity.this.startActivity(intent);
                }
            });
            view.setOnLongClickListener(new View.OnLongClickListener() {
                public boolean onLongClick(View v) {
                    String smsContent;
                    Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:"));
                    if (type == 9) {
                        smsContent = content;
                    } else {
                        smsContent = String.valueOf(title) + "\n" + content;
                    }
                    intent.putExtra("sms_body", smsContent);
                    MainActivity.this.startActivity(intent);
                    return true;
                }
            });
        }

        class TangshiCompent {
            public TextView authorTextView;
            public ImageView favoriteImage;
            public TextView titleTextView;
            public TextView typeTextView;

            public TangshiCompent(TextView title, TextView author, TextView type, ImageView img) {
                this.titleTextView = title;
                this.authorTextView = author;
                this.typeTextView = type;
                this.favoriteImage = img;
            }
        }
    }

    public void onClick(View v) {
        if (v == this.mSearchButton) {
            refreshBySearchText();
        }
    }

    private void refreshBySearchText() {
        String text = this.mSearchEditText.getText().toString();
        switch (focusedRadioButtonIndex) {
            case 0:
                if (text == null || text.equals("")) {
                    ContentResolver contentResolver = getContentResolver();
                    Uri uri = PoetryContentProvider.CONTENT_URI;
                    String[] strArr = new String[MENU_ITEM_ONLINE];
                    strArr[0] = "8";
                    strArr[1] = "9";
                    strArr[2] = "10";
                    this.mTangshiCursor = contentResolver.query(uri, null, "t_type!=? and t_type!=? and t_type!=?", strArr, null);
                } else {
                    ContentResolver contentResolver2 = getContentResolver();
                    Uri uri2 = PoetryContentProvider.CONTENT_URI;
                    String[] strArr2 = new String[6];
                    strArr2[0] = "%" + text + "%";
                    strArr2[1] = "%" + text + "%";
                    strArr2[2] = "%" + text + "%";
                    strArr2[MENU_ITEM_ONLINE] = "8";
                    strArr2[4] = "9";
                    strArr2[5] = "10";
                    this.mTangshiCursor = contentResolver2.query(uri2, null, "(t_title like ? or p_author like ? or t_content like ?) and t_type!=? and t_type!=? and t_type!=?", strArr2, null);
                }
                this.tangshiCursorAdapter.changeCursor(this.mTangshiCursor);
                this.tangshiCursorAdapter.notifyDataSetChanged();
                return;
            case 1:
                if (text == null || text.equals("")) {
                    this.mTangshiCursor = getContentResolver().query(PoetryContentProvider.CONTENT_URI, null, "t_type=?", new String[]{"8"}, null);
                } else {
                    ContentResolver contentResolver3 = getContentResolver();
                    Uri uri3 = PoetryContentProvider.CONTENT_URI;
                    String[] strArr3 = new String[4];
                    strArr3[0] = "%" + text + "%";
                    strArr3[1] = "%" + text + "%";
                    strArr3[2] = "%" + text + "%";
                    strArr3[MENU_ITEM_ONLINE] = "8";
                    this.mTangshiCursor = contentResolver3.query(uri3, null, "(t_title like ? or p_author like ? or t_content like ?) and t_type=?", strArr3, null);
                }
                this.tangshiCursorAdapter.changeCursor(this.mTangshiCursor);
                this.tangshiCursorAdapter.notifyDataSetChanged();
                return;
            case 2:
                if (text == null || text.equals("")) {
                    this.mTangshiCursor = getContentResolver().query(PoetryContentProvider.CONTENT_URI, null, "t_type=?", new String[]{"9"}, null);
                } else {
                    ContentResolver contentResolver4 = getContentResolver();
                    Uri uri4 = PoetryContentProvider.CONTENT_URI;
                    String[] strArr4 = new String[MENU_ITEM_ONLINE];
                    strArr4[0] = "%" + text + "%";
                    strArr4[1] = "%" + text + "%";
                    strArr4[2] = "9";
                    this.mTangshiCursor = contentResolver4.query(uri4, null, "(p_author like ? or t_content like ?) and t_type=?", strArr4, null);
                }
                this.tangshiCursorAdapter.changeCursor(this.mTangshiCursor);
                this.tangshiCursorAdapter.notifyDataSetChanged();
                return;
            case MENU_ITEM_ONLINE /*3*/:
                if (text == null || text.equals("")) {
                    this.mTangshiCursor = getContentResolver().query(PoetryContentProvider.CONTENT_URI, null, "t_type=?", new String[]{"10"}, null);
                } else {
                    ContentResolver contentResolver5 = getContentResolver();
                    Uri uri5 = PoetryContentProvider.CONTENT_URI;
                    String[] strArr5 = new String[MENU_ITEM_ONLINE];
                    strArr5[0] = "%" + text + "%";
                    strArr5[1] = "%" + text + "%";
                    strArr5[2] = "10";
                    this.mTangshiCursor = contentResolver5.query(uri5, null, "(p_author like ? or t_content like ?) and t_type=?", strArr5, null);
                }
                this.tangshiCursorAdapter.changeCursor(this.mTangshiCursor);
                this.tangshiCursorAdapter.notifyDataSetChanged();
                return;
            case 4:
                if (text == null || text.equals("")) {
                    this.mTangshiCursor = getContentResolver().query(PoetryContentProvider.CONTENT_URI, null, "t_favorite_status=? or t_favorite_status=?", new String[]{"2", "1"}, sortOrderFavorite);
                } else {
                    ContentResolver contentResolver6 = getContentResolver();
                    Uri uri6 = PoetryContentProvider.CONTENT_URI;
                    String[] strArr6 = new String[5];
                    strArr6[0] = "%" + text + "%";
                    strArr6[1] = "%" + text + "%";
                    strArr6[2] = "%" + text + "%";
                    strArr6[MENU_ITEM_ONLINE] = "2";
                    strArr6[4] = "1";
                    this.mTangshiCursor = contentResolver6.query(uri6, null, "(t_title like ? or p_author like ? or t_content like ?) and (t_favorite_status=? or t_favorite_status=?)", strArr6, sortOrderFavorite);
                }
                this.tangshiCursorAdapter.changeCursor(this.mTangshiCursor);
                this.tangshiCursorAdapter.notifyDataSetChanged();
                return;
            default:
                return;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, "隐藏搜索");
        menu.add(0, 1, 1, "关于");
        menu.add(0, 2, 2, "退出");
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                if (this.mSearchBarLayout.getVisibility() != 0) {
                    this.mSearchBarLayout.setVisibility(0);
                    this.mSearchEditText.requestFocus();
                    item.setTitle("隐藏搜索");
                    break;
                } else {
                    this.mSearchBarLayout.setVisibility(8);
                    item.setTitle("显示搜索");
                    break;
                }
            case 1:
                String version = "";
                try {
                    version = getPackageManager().getPackageInfo(getApplicationInfo().packageName, 0).versionName;
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                new AlertDialog.Builder(this).setTitle("唐诗宋词").setMessage("唐诗宋词" + version + ",提供了唐诗，宋词，名言警句，收藏，编辑功能,同时具备文章注解与赏析。长按文章列表选项可以通过短信发送给好友。单击进入文章后，可以直接阅读上一篇，下一篇,无须返回文章列表进行选择，在文章列表和文章阅读中均提供收藏和取消收藏,发送短信功能，操作非常方便。在文章列表主界面，可以通过左右滑动进行唐诗，宋词，名句，收藏切换。欢迎使用唐诗宋词，有好的建议请发送到：huangxibo@126.com").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).show();
                break;
            case 2:
                finish();
                break;
            case MENU_ITEM_ONLINE /*3*/:
                startActivity(new Intent(this, ReadOnlineActivity.class));
                break;
        }
        return super.onMenuItemSelected(featureId, item);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        refreshBySearchText();
    }
}
