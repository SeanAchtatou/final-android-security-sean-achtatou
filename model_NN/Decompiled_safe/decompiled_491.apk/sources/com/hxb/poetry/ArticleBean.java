package com.hxb.poetry;

public class ArticleBean {
    private String analysis;
    private String author;
    private String comment;
    private String content;
    private int favorite;
    private int id;
    private String title;
    private int type;

    public String getComment() {
        return this.comment;
    }

    public void setComment(String comment2) {
        String str;
        if (comment2 == null || comment2.equals("null".trim())) {
            str = "";
        } else {
            str = comment2;
        }
        this.comment = str;
    }

    public String getAnalysis() {
        return this.analysis;
    }

    public void setAnalysis(String analysis2) {
        String str;
        if (analysis2 == null || analysis2.equals("null".trim())) {
            str = "";
        } else {
            str = analysis2;
        }
        this.analysis = str;
    }

    public int getFavorite() {
        return this.favorite;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id2) {
        this.id = id2;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type2) {
        this.type = type2;
    }

    public String getAuthor() {
        return this.author;
    }

    public void setAuthor(String author2) {
        this.author = author2;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content2) {
        this.content = content2;
    }

    public int isFavorite() {
        return this.favorite;
    }

    public void setFavorite(int favorite2) {
        this.favorite = favorite2;
    }
}
