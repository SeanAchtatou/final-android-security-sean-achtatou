package us.kidapps.paint;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.Toast;
import com.admob.android.ads.AdManager;
import com.rubycell.moregame.Utility.Common;
import com.rubycell.moregame.list.MoreGameList;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Vector;
import us.colormefree.aanimal.R;
import us.kidapps.paint.PaintView;

public class PaintActivity extends ZebraActivity implements PaintView.LifecycleListener {
    private static final int DIALOG_PROGRESS = 1;
    private static final String MIME_PNG = "image/png";
    private static final int REQUEST_PICK_COLOR = 2251;
    private static final int REQUEST_START_NEW = 2000;
    private static final int SAVE_DIALOG_WAIT_MILLIS = 1500;
    private ColorButtonManager _colorButtonManager;
    public String _fileName;
    /* access modifiers changed from: private */
    public PaintView _paintView;
    /* access modifiers changed from: private */
    public ProgressBar _progressBar;
    /* access modifiers changed from: private */
    public ProgressDialog _progressDialog;
    /* access modifiers changed from: private */
    public boolean _saveInProgress;
    /* access modifiers changed from: private */
    public State _state = new State(null);
    /* access modifiers changed from: private */
    public ProgressDialog dialog;
    public boolean isFirstTime = true;
    public boolean isShare = false;

    public void onCreate(Bundle savedInstanceState) {
        int finalHeight;
        int finalWidth;
        super.onCreate(savedInstanceState);
        Display display = getWindowManager().getDefaultDisplay();
        int width = display.getWidth();
        int height = display.getHeight();
        if (height <= width) {
            finalHeight = width;
        } else {
            finalHeight = height;
        }
        if (height >= width) {
            finalWidth = width;
        } else {
            finalWidth = height;
        }
        setVolumeControlStream(3);
        SoundManager.getInstance();
        SoundManager.initSounds(this);
        SoundManager.loadSounds();
        setContentView(R.layout.paint);
        this._paintView = (PaintView) findViewById(R.id.paint_view);
        this._paintView.setLifecycleListener(this);
        this._paintView.setLayoutParams(new TableLayout.LayoutParams(-1, finalWidth));
        ((LinearLayout) findViewById(R.id.paintframe)).setLayoutParams(new TableLayout.LayoutParams(-1, finalWidth));
        this._progressBar = (ProgressBar) findViewById(R.id.paint_progress);
        this._progressBar.setMax(100);
        this._colorButtonManager = new ColorButtonManager();
        findViewById(R.id.pick_color_button).setOnClickListener(new PickColorListener(this, null));
        ZebraImageButton newPicsButton = (ZebraImageButton) findViewById(R.id.newpic_button);
        newPicsButton.setOnClickListener(new NewPicsListener(this, null));
        newPicsButton.setOriginBitmapisMirrorBitpmap();
        LinearLayout hdiptop = (LinearLayout) findViewById(R.id.hdiptop);
        LinearLayout hdipbottom = (LinearLayout) findViewById(R.id.hdipbottom);
        LinearLayout colorButton = (LinearLayout) findViewById(R.id.colorbutton);
        colorButton.bringToFront();
        if (finalHeight >= 310 && finalHeight <= 330) {
            hdiptop.setLayoutParams(new TableLayout.LayoutParams(-1, 41));
            hdiptop.setBackgroundResource(R.drawable.mdiptop);
            hdiptop.invalidate();
            colorButton.setPadding(4, 5, 0, 0);
            colorButton.setLayoutParams(new TableLayout.LayoutParams(-1, 38));
            colorButton.invalidate();
        } else if (finalHeight >= 450 && finalHeight <= 500) {
            hdiptop.setLayoutParams(new TableLayout.LayoutParams(-1, 105));
            hdiptop.setBackgroundResource(R.drawable.mdiptop);
            hdiptop.invalidate();
            colorButton.setPadding(4, 60, 0, 0);
            colorButton.setLayoutParams(new TableLayout.LayoutParams(-1, 105));
            colorButton.invalidate();
        } else if (finalHeight == 533) {
            hdipbottom.setVisibility(0);
            hdiptop.setVisibility(0);
        } else if (finalHeight < 569 || finalHeight > 900) {
            colorButton.setPadding(4, 170, 0, 0);
            colorButton.setLayoutParams(new TableLayout.LayoutParams(-1, 215));
            colorButton.invalidate();
            hdipbottom.setVisibility(0);
            hdiptop.setVisibility(0);
        } else {
            hdipbottom.setVisibility(0);
            hdiptop.setVisibility(0);
        }
        AdManager.setPublisherId(String.valueOf("a14d") + "786a7" + "780374");
        AdManager.setTestDevices(new String[]{AdManager.TEST_EMULATOR, "04037B820F013004", "E2D6BB48E6366B3F0E87C473F57B9924"});
        Object previousState = getLastNonConfigurationInstance();
        if (previousState == null) {
            this._paintView.setVisibility(4);
            this._progressBar.setVisibility(8);
        } else {
            SavedState state = (SavedState) previousState;
            this._state = state._paintActivityState;
            this._paintView.setState(state._paintViewState);
            this._colorButtonManager.setState(state._colorButtonState);
            this._paintView.setVisibility(0);
            this._progressBar.setVisibility(8);
            if (this._state._loadInProgress) {
                new InitPaintView(this._state._loadedResourceId);
            }
        }
        SoundManager.playSound(R.raw.gamestarted);
        this.isFirstTime = false;
        Toast.makeText(getApplicationContext(), "TIP: You can select other picture in Menu / Album", 1).show();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.paint_menu, menu);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        try {
            SoundManager.cleanup();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onPreparedToLoad() {
        new Handler() {
            public void handleMessage(Message m) {
                new InitPaintView(StartNewActivity.randomOutlineId());
            }
        }.sendEmptyMessage(0);
    }

    public void textToast(String textToDisplay) {
        Toast.makeText(getApplicationContext(), textToDisplay, 0).show();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.open_new:
                SoundManager.playSound(R.raw.openmenu);
                this.dialog = this.dialog != null ? this.dialog : ProgressDialog.show(this, "", "Loading. Please wait...", true);
                startActivityForResult(new Intent(this, StartNewActivity.class), 2000);
                return true;
            case R.id.save:
                SoundManager.playSound(R.raw.savingorsending);
                new BitmapSaver();
                return true;
            case R.id.open_existing:
            case R.id.open_gallery:
            default:
                return false;
            case R.id.publisher:
                Common.openAndSubmitSearchQueryToMarket(this, "colormefree.us");
                return true;
            case R.id.moregame:
                Common.openCustomWebPageInBrowser(this, "http://mobile.kidapps.us");
                return true;
            case R.id.share:
                SoundManager.playSound(R.raw.savingorsending);
                this.isShare = true;
                new BitmapSaver();
                return true;
            case R.id.exit:
                SoundManager.playSound(R.raw.coin_appears);
                return quitConfirmation();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            return quitConfirmation();
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean quitConfirmation() {
        SoundManager.playSound(R.raw.selectcolor);
        Intent moreGame = new Intent(this, MoreGameList.class);
        Bundle activityData = new Bundle();
        activityData.putString(Common.MORE_GAME_LIST_URL, "http://tabletgames.mobi/apis/moregame/moregamelist.php?category=pub:RubyCell");
        moreGame.putExtras(activityData);
        startActivityForResult(moreGame, 0);
        return true;
    }

    public Object onRetainNonConfigurationInstance() {
        SavedState state = new SavedState(null);
        state._paintActivityState = this._state;
        state._paintViewState = this._paintView.getState();
        state._colorButtonState = this._colorButtonManager.getState();
        return state;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (this.dialog != null && this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            if (this.dialog != null && this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } catch (Error e2) {
            e2.printStackTrace();
        }
        switch (resultCode) {
            case Common.MOREGAME_LIST_FINISHED:
                finish();
                Log.i("", "try to PaintActivity.this.finish();");
                return;
            default:
                switch (requestCode) {
                    case 2000:
                        if (resultCode != 0) {
                            new InitPaintView(resultCode);
                            Log.i("", "try to InitPaintView(resultCode)");
                            return;
                        }
                        return;
                    case REQUEST_PICK_COLOR /*2251*/:
                        if (resultCode != 0) {
                            this._colorButtonManager.selectColor(resultCode);
                            Log.i("", "try to selectColor(resultCode)");
                            return;
                        }
                        return;
                    default:
                        return;
                }
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                this._progressDialog = new ProgressDialog(this);
                this._progressDialog.setCancelable(false);
                this._progressDialog.setIcon(17301659);
                this._progressDialog.setTitle((int) R.string.dialog_saving);
                this._progressDialog.setProgressStyle(1);
                this._progressDialog.setMax(100);
                if (!this._saveInProgress) {
                    new Handler() {
                        public void handleMessage(Message m) {
                            PaintActivity.this._progressDialog.dismiss();
                        }
                    }.sendEmptyMessage(0);
                }
                return this._progressDialog;
            default:
                return null;
        }
    }

    private class PickColorListener implements View.OnClickListener {
        private PickColorListener() {
        }

        /* synthetic */ PickColorListener(PaintActivity paintActivity, PickColorListener pickColorListener) {
            this();
        }

        public void onClick(View view) {
            SoundManager.playSound(R.raw.openmenu);
            PaintActivity.this.dialog = PaintActivity.this.dialog != null ? PaintActivity.this.dialog : ProgressDialog.show(PaintActivity.this, "", "Loading. Please wait...", true);
            PaintActivity.this.startActivityForResult(new Intent(PaintActivity.this, PickColorActivity.class), PaintActivity.REQUEST_PICK_COLOR);
        }
    }

    private class NewPicsListener implements View.OnClickListener {
        private NewPicsListener() {
        }

        /* synthetic */ NewPicsListener(PaintActivity paintActivity, NewPicsListener newPicsListener) {
            this();
        }

        public void onClick(View view) {
            SoundManager.playSound(R.raw.openmenu);
            PaintActivity.this.dialog = PaintActivity.this.dialog != null ? PaintActivity.this.dialog : ProgressDialog.show(PaintActivity.this, "", "Loading. Please wait...", true);
            PaintActivity.this.startActivityForResult(new Intent(PaintActivity.this.getBaseContext(), StartNewActivity.class), 2000);
        }
    }

    private class ColorButtonManager implements View.OnClickListener {
        private Vector<ColorButton> _colorButtons = new Vector<>();
        private ColorButton _selectedColorButton;
        private LinkedList<ColorButton> _usedColorButtons = new LinkedList<>();

        public ColorButtonManager() {
            PaintActivity.this.findAllColorButtons(this._colorButtons);
            this._usedColorButtons.addAll(this._colorButtons);
            this._selectedColorButton = this._usedColorButtons.getFirst();
            this._selectedColorButton.setSelected(true);
            Iterator<ColorButton> i = this._usedColorButtons.iterator();
            while (i.hasNext()) {
                i.next().setOnClickListener(this);
            }
            setPaintViewColor();
        }

        public void onClick(View view) {
            if (view instanceof ColorButton) {
                selectButton((ColorButton) view);
            }
        }

        public void selectColor(int color) {
            this._selectedColorButton = selectAndRemove(color);
            if (this._selectedColorButton == null) {
                this._selectedColorButton = this._usedColorButtons.removeLast();
                this._selectedColorButton.setColor(color);
                this._selectedColorButton.setSelected(true);
            }
            this._usedColorButtons.addFirst(this._selectedColorButton);
            setPaintViewColor();
        }

        public Object getState() {
            int[] result = new int[(this._colorButtons.size() + 1)];
            int n = this._colorButtons.size();
            for (int i = 0; i < n; i++) {
                result[i] = this._colorButtons.elementAt(i).getColor();
            }
            result[n] = this._selectedColorButton.getColor();
            return result;
        }

        public void setState(Object o) {
            int[] state = (int[]) o;
            int n = this._colorButtons.size();
            for (int i = 0; i < n; i++) {
                this._colorButtons.elementAt(i).setColor(state[i]);
            }
            selectColor(state[n]);
        }

        private void selectButton(ColorButton button) {
            this._selectedColorButton = selectAndRemove(button.getColor());
            this._usedColorButtons.addFirst(this._selectedColorButton);
            setPaintViewColor();
        }

        private void setPaintViewColor() {
            PaintActivity.this._paintView.setPaintColor(this._selectedColorButton.getColor());
        }

        private ColorButton selectAndRemove(int color) {
            ColorButton result = null;
            Iterator<ColorButton> i = this._usedColorButtons.iterator();
            while (i.hasNext()) {
                ColorButton b = i.next();
                if (b.getColor() == color) {
                    result = b;
                    b.setSelected(true);
                    i.remove();
                } else {
                    b.setSelected(false);
                }
            }
            return result;
        }
    }

    private class InitPaintView implements Runnable {
        private Handler _handler = new Handler() {
            public void handleMessage(Message m) {
                switch (m.what) {
                    case 1:
                        PaintActivity.this._progressBar.incrementProgressBy(m.arg1);
                        return;
                    case 2:
                    case 3:
                        PaintActivity.this._state._loadInProgress = false;
                        PaintActivity.this._paintView.setVisibility(0);
                        PaintActivity.this._progressBar.setVisibility(8);
                        return;
                    default:
                        return;
                }
            }
        };
        private Bitmap _originalOutlineBitmap;

        public InitPaintView(int outlineResourceId) {
            PaintActivity.this._paintView.setVisibility(8);
            PaintActivity.this._progressBar.setProgress(0);
            PaintActivity.this._progressBar.setVisibility(0);
            PaintActivity.this._state._savedImageUri = null;
            PaintActivity.this._state._loadInProgress = true;
            PaintActivity.this._state._loadedResourceId = outlineResourceId;
            this._originalOutlineBitmap = BitmapFactory.decodeResource(PaintActivity.this.getResources(), outlineResourceId);
            new Thread(this).start();
        }

        public void run() {
            PaintActivity.this._paintView.loadFromBitmap(this._originalOutlineBitmap, this._handler);
        }
    }

    private class MediaScannerNotifier implements MediaScannerConnection.MediaScannerConnectionClient {
        private MediaScannerConnection _connection;
        private String _mimeType;
        private String _path;

        public MediaScannerNotifier(Context context, String path, String mimeType) {
            this._path = path;
            this._mimeType = mimeType;
            this._connection = new MediaScannerConnection(context, this);
            this._connection.connect();
        }

        public void onMediaScannerConnected() {
            this._connection.scanFile(this._path, this._mimeType);
        }

        public void onScanCompleted(String path, Uri uri) {
            this._connection.disconnect();
        }
    }

    private class BitmapSaver implements Runnable {
        private File _file;
        private Uri _newImageUri;
        private Bitmap _originalOutlineBitmap;
        private Handler _progressHandler;

        public BitmapSaver() {
            if (PaintActivity.this._paintView.isInitialized()) {
                PaintActivity.this._saveInProgress = true;
                PaintActivity.this.showDialog(1);
                PaintActivity.this._progressDialog.setTitle((int) R.string.dialog_saving);
                PaintActivity.this._progressDialog.setProgress(0);
                this._originalOutlineBitmap = BitmapFactory.decodeResource(PaintActivity.this.getResources(), PaintActivity.this._state._loadedResourceId);
                this._progressHandler = new Handler() {
                    public void handleMessage(Message m) {
                        String title;
                        switch (m.what) {
                            case 1:
                                PaintActivity.this._progressDialog.incrementProgressBy(m.arg1);
                                return;
                            case 2:
                                if (PaintActivity.this._fileName != null && PaintActivity.this.isShare) {
                                    PaintActivity.this.isShare = false;
                                    Intent picMessageIntent = new Intent("android.intent.action.SEND");
                                    picMessageIntent.setType(PaintActivity.MIME_PNG);
                                    picMessageIntent.putExtra("android.intent.extra.STREAM", Uri.fromFile(new File(Environment.getExternalStorageDirectory(), String.valueOf(PaintActivity.this.getString(R.string.saved_image_path_prefix)) + PaintActivity.this._fileName + ".png")));
                                    PaintActivity.this.startActivity(Intent.createChooser(picMessageIntent, "Send your picture using:"));
                                } else if (PaintActivity.this._fileName != null) {
                                    PaintActivity.this.textToast("Save image to SD card. File path:  " + PaintActivity.this.getString(R.string.saved_image_path_prefix) + PaintActivity.this._fileName + ".png");
                                }
                                try {
                                    PaintActivity.this._saveInProgress = false;
                                    PaintActivity.this._progressDialog.dismiss();
                                    return;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    return;
                                }
                            case 3:
                                if (m.what == 2) {
                                    BitmapSaver.this.finishSaving();
                                }
                                String title2 = PaintActivity.this.getString(R.string.dialog_saving);
                                if (m.what == 2) {
                                    title = String.valueOf(title2) + PaintActivity.this.getString(R.string.dialog_saving_ok);
                                } else {
                                    title = String.valueOf(title2) + PaintActivity.this.getString(R.string.dialog_saving_error);
                                }
                                PaintActivity.this._progressDialog.setTitle(title);
                                new Handler() {
                                    public void handleMessage(Message m) {
                                        PaintActivity.this._saveInProgress = false;
                                        PaintActivity.this._progressDialog.dismiss();
                                    }
                                }.sendEmptyMessageDelayed(0, 1500);
                                return;
                            default:
                                return;
                        }
                    }
                };
                new Thread(this).start();
            }
        }

        public void run() {
            PaintActivity.this._fileName = newImageFileName();
            this._file = new File(Environment.getExternalStorageDirectory(), String.valueOf(PaintActivity.this.getString(R.string.saved_image_path_prefix)) + PaintActivity.this._fileName + ".png");
            PaintActivity.this._paintView.saveToFile(this._file, this._originalOutlineBitmap, this._progressHandler);
        }

        /* access modifiers changed from: private */
        public void finishSaving() {
            ContentValues values = new ContentValues();
            values.put("title", PaintActivity.this._fileName);
            values.put("_display_name", PaintActivity.this._fileName);
            values.put("mime_type", PaintActivity.MIME_PNG);
            values.put("datetaken", Long.valueOf(System.currentTimeMillis()));
            values.put("_data", this._file.toString());
            File parentFile = this._file.getParentFile();
            values.put("bucket_id", Integer.valueOf(parentFile.toString().toLowerCase().hashCode()));
            values.put("bucket_display_name", parentFile.getName().toLowerCase());
            this._newImageUri = PaintActivity.this.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            if (PaintActivity.this._state._savedImageUri != null) {
                PaintActivity.this.getContentResolver().delete(PaintActivity.this._state._savedImageUri, null, null);
            }
            PaintActivity.this._state._savedImageUri = this._newImageUri;
            if (this._newImageUri != null) {
                new MediaScannerNotifier(PaintActivity.this, this._file.toString(), PaintActivity.MIME_PNG);
            }
        }

        private String newImageFileName() {
            return new SimpleDateFormat("yyyyMMdd-HHmmss").format(new Date());
        }
    }

    private static class SavedState {
        public Object _colorButtonState;
        public State _paintActivityState;
        public Object _paintViewState;

        private SavedState() {
        }

        /* synthetic */ SavedState(SavedState savedState) {
            this();
        }
    }

    private static class State {
        public boolean _loadInProgress;
        public int _loadedResourceId;
        public Uri _savedImageUri;

        private State() {
        }

        /* synthetic */ State(State state) {
            this();
        }
    }
}
