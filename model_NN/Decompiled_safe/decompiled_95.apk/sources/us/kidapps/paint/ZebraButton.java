package us.kidapps.paint;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import us.colormefree.aanimal.R;

public class ZebraButton extends View {
    private boolean _touchPushed;
    Context mcontext;
    public MediaPlayer mp;

    public ZebraButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setClickable(true);
        this.mcontext = context.getApplicationContext();
    }

    public ZebraButton(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.zebraButtonStyle);
    }

    public ZebraButton(Context context) {
        this(context, null);
    }

    public static int getPreferredWidth() {
        return ZebraActivity.getDisplayWitdh() / 8;
    }

    public static int getPreferredHeight() {
        return ZebraActivity.getDisplayHeight() / 7;
    }

    public boolean onTouchEvent(MotionEvent e) {
        this._touchPushed = isPushed(e, this._touchPushed);
        return super.onTouchEvent(e);
    }

    public boolean isPushedDown() {
        return this._touchPushed;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(measureWidth(widthMeasureSpec), measureHeight(heightMeasureSpec));
    }

    private boolean isPushed(MotionEvent e, boolean original) {
        switch (e.getAction()) {
            case 0:
                invalidate();
                return true;
            case 1:
                invalidate();
                try {
                    this.mp = this.mp == null ? MediaPlayer.create(this.mcontext, (int) R.raw.selectcolor2) : this.mp;
                    this.mp.start();
                } catch (IllegalStateException e2) {
                    e2.printStackTrace();
                } catch (Error e3) {
                }
                return false;
            default:
                return original;
        }
    }

    private int measureWidth(int measureSpec) {
        return getMeasurement(measureSpec, getPreferredWidth());
    }

    private int measureHeight(int measureSpec) {
        return getMeasurement(measureSpec, getPreferredHeight());
    }

    private int getMeasurement(int measureSpec, int preferred) {
        int specSize = View.MeasureSpec.getSize(measureSpec);
        switch (View.MeasureSpec.getMode(measureSpec)) {
            case Integer.MIN_VALUE:
                return Math.min(preferred, specSize);
            case 1073741824:
                return specSize;
            default:
                return preferred;
        }
    }
}
