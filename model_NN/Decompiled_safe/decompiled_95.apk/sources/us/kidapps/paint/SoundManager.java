package us.kidapps.paint;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import java.util.HashMap;
import us.colormefree.aanimal.R;

public class SoundManager {
    private static SoundManager _instance;
    private static AudioManager mAudioManager;
    private static Context mContext;
    private static SoundPool mSoundPool;
    private static HashMap mSoundPoolMap;

    private SoundManager() {
    }

    public static synchronized SoundManager getInstance() {
        SoundManager soundManager;
        synchronized (SoundManager.class) {
            if (_instance == null) {
                _instance = new SoundManager();
            }
            soundManager = _instance;
        }
        return soundManager;
    }

    public static void initSounds(Context theContext) {
        mContext = theContext;
        mSoundPool = new SoundPool(4, 3, 0);
        mSoundPoolMap = new HashMap();
        mAudioManager = (AudioManager) mContext.getSystemService("audio");
    }

    public static void addSound(int Index, int SoundID) {
        mSoundPoolMap.put(Integer.valueOf(Index), Integer.valueOf(mSoundPool.load(mContext, SoundID, 1)));
    }

    public static void loadSounds() {
        mSoundPoolMap.put(Integer.valueOf((int) R.raw.fillcolor), Integer.valueOf(mSoundPool.load(mContext, R.raw.fillcolor, 1)));
        mSoundPoolMap.put(Integer.valueOf((int) R.raw.gamestarted), Integer.valueOf(mSoundPool.load(mContext, R.raw.gamestarted, 1)));
        mSoundPoolMap.put(Integer.valueOf((int) R.raw.openmenu), Integer.valueOf(mSoundPool.load(mContext, R.raw.openmenu, 1)));
        mSoundPoolMap.put(Integer.valueOf((int) R.raw.savingorsending), Integer.valueOf(mSoundPool.load(mContext, R.raw.savingorsending, 1)));
        mSoundPoolMap.put(Integer.valueOf((int) R.raw.selectcolor), Integer.valueOf(mSoundPool.load(mContext, R.raw.selectcolor, 1)));
        mSoundPoolMap.put(Integer.valueOf((int) R.raw.selectcolor2), Integer.valueOf(mSoundPool.load(mContext, R.raw.selectcolor2, 1)));
        mSoundPoolMap.put(Integer.valueOf((int) R.raw.task_complete), Integer.valueOf(mSoundPool.load(mContext, R.raw.task_complete, 1)));
        mSoundPoolMap.put(Integer.valueOf((int) R.raw.coin_appears), Integer.valueOf(mSoundPool.load(mContext, R.raw.coin_appears, 1)));
    }

    public static void playSound(int index, float speed) {
        float streamVolume = ((float) mAudioManager.getStreamVolume(3)) / ((float) mAudioManager.getStreamMaxVolume(3));
        mSoundPool.play(Integer.parseInt(mSoundPoolMap.get(Integer.valueOf(index)).toString()), streamVolume, streamVolume, 1, 0, speed);
    }

    public static void playSound(int index) {
        playSound(index, 1.0f);
    }

    public static void stopSound(int index) {
        mSoundPool.stop(Integer.parseInt(mSoundPoolMap.get(Integer.valueOf(index)).toString()));
    }

    public static void cleanup() {
        mSoundPool.release();
        mSoundPool = null;
        mSoundPoolMap.clear();
        mAudioManager.unloadSoundEffects();
        _instance = null;
    }
}
