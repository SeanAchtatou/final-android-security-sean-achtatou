package us.kidapps.paint;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class FloodFill {

    public interface PixelMatcher {
        boolean match(int i, int i2);
    }

    public interface PixelSetter {
        void set(int i, int i2, int i3);
    }

    /* JADX INFO: Multiple debug info for r0v2 int: [D('px' int), D('px1' int)] */
    /* JADX INFO: Multiple debug info for r7v7 boolean: [D('matchUp' boolean), D('prevMatchUp' boolean)] */
    /* JADX INFO: Multiple debug info for r6v13 boolean: [D('matchDn' boolean), D('prevMatchDn' boolean)] */
    /* JADX INFO: Multiple debug info for r6v16 int: [D('px2' int), D('px1' int)] */
    public static void fill(int x, int y, PixelMatcher matcher, PixelSetter setter) {
        Queue<Pixel> queue = new LinkedList<>();
        queue.add(new Pixel(x, y));
        while (queue.isEmpty() == 0) {
            Pixel p = (Pixel) queue.remove();
            int px1 = p._x;
            int px2 = p._x;
            int py = p._y;
            if (matcher.match(px1, py)) {
                int px12 = px1;
                while (matcher.match(px12, py) != 0) {
                    px12--;
                }
                int px13 = px12 + 1;
                int px22 = px2;
                while (matcher.match(px22, py)) {
                    px22++;
                }
                setter.set(px13, px22, py);
                int px = px13;
                int px3 = 0;
                boolean prevMatchDn = false;
                while (px < px22) {
                    boolean matchUp = matcher.match(px, py - 1);
                    if (matchUp && !prevMatchDn) {
                        queue.add(new Pixel(px, py - 1));
                    }
                    int matchDn = matcher.match(px, py + 1);
                    if (matchDn != 0 && px3 == 0) {
                        queue.add(new Pixel(px, py + 1));
                    }
                    px++;
                    px3 = matchDn;
                    prevMatchDn = matchUp;
                }
            }
        }
    }

    /* JADX INFO: Multiple debug info for r8v4 int: [D('p' us.kidapps.paint.FloodFill$Pixel), D('pp' int)] */
    /* JADX INFO: Multiple debug info for r8v5 int: [D('pp' int), D('px' int)] */
    /* JADX INFO: Multiple debug info for r8v19 boolean: [D('matchUp' boolean), D('prevMatchUp' boolean)] */
    /* JADX INFO: Multiple debug info for r9v8 int: [D('px2' int), D('px1' int)] */
    public static void fillRaw(int x, int y, int width, int height, byte[] mask, int[] pixels, int color) {
        boolean matchDn;
        Queue<Pixel> queue = new LinkedList<>();
        queue.add(new Pixel(x, y));
        while (queue.isEmpty() == 0) {
            Pixel p = (Pixel) queue.remove();
            int px1 = p._x;
            int px2 = p._x;
            int py = p._y;
            int pp = py * width;
            if (mask[pp + px1] != 0) {
                while (px1 >= 0 && mask[pp + px1] != 0) {
                    px1--;
                }
                int px12 = px1 + 1;
                int px22 = px2;
                while (px22 < width && mask[pp + px22] != 0) {
                    px22++;
                }
                Arrays.fill(pixels, pp + px12, pp + px22, color);
                Arrays.fill(mask, pp + px12, pp + px22, (byte) 0);
                boolean prevMatchUp = false;
                boolean px = false;
                int ppUp = pp - width;
                int ppDn = pp + width;
                int px3 = px12;
                while (px3 < px22) {
                    if (py > 0) {
                        boolean matchUp = mask[ppUp + px3] != 0;
                        if (matchUp && !prevMatchUp) {
                            queue.add(new Pixel(px3, py - 1));
                        }
                        prevMatchUp = matchUp;
                    }
                    if (py + 1 < height) {
                        boolean matchDn2 = mask[ppDn + px3] != 0;
                        if (matchDn2 && !px) {
                            queue.add(new Pixel(px3, py + 1));
                        }
                        matchDn = matchDn2;
                    } else {
                        matchDn = px;
                    }
                    px3++;
                    px = matchDn;
                }
            }
        }
    }

    private static class Pixel {
        public int _x;
        public int _y;

        public Pixel(int x, int y) {
            this._x = x;
            this._y = y;
        }
    }
}
