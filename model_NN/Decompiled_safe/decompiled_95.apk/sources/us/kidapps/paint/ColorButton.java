package us.kidapps.paint;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import us.colormefree.aanimal.R;

public class ColorButton extends ZebraButton {
    public static final int INSET_HIGHLIGHT_PERCENT = 7;
    public static final int PADDING_NORMAL_PERCENT = 16;
    public static final int PADDING_PUSHED_PERCENT = 2;
    public static final int PADDING_SELECTED_PERCENT = 7;
    private int _color;
    private GradientDrawable _colorDrawable;
    private GradientDrawable _highlightDrawable;
    private boolean _selected;

    public ColorButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this._color = context.obtainStyledAttributes(attrs, R.styleable.ColorButton, defStyle, 0).getColor(0, -65536);
        GradientDrawable.Orientation orientation = GradientDrawable.Orientation.TOP_BOTTOM;
        int[] iArr = new int[3];
        iArr[0] = -1;
        this._highlightDrawable = new GradientDrawable(orientation, iArr);
        this._highlightDrawable.setShape(1);
        this._colorDrawable = new GradientDrawable();
        this._colorDrawable.setColor(this._color);
        this._colorDrawable.setShape(1);
    }

    public ColorButton(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.colorButtonStyle);
    }

    public ColorButton(Context context) {
        this(context, null);
    }

    public void setSelected(boolean selected) {
        if (this._selected != selected) {
            this._selected = selected;
            invalidate();
        }
    }

    public int getColor() {
        return this._color;
    }

    public void setColor(int color) {
        if (color != this._color) {
            this._color = color;
            this._colorDrawable.setColor(this._color);
            invalidate();
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        int p;
        boolean pushedDown = isPushedDown();
        int w = getWidth();
        int h = getHeight();
        int minwh = Math.min(w, h);
        int i = (minwh * 7) / 100;
        if (pushedDown) {
            p = (minwh * 2) / 100;
        } else if (this._selected) {
            p = (minwh * 7) / 100;
        } else {
            p = (minwh * 16) / 100;
        }
        this._colorDrawable.setBounds(p, p, w - p, h - p);
        this._colorDrawable.draw(canvas);
        this._highlightDrawable.setBounds(p + i, p + i, (w - p) - i, (h - p) - i);
        this._highlightDrawable.draw(canvas);
    }
}
