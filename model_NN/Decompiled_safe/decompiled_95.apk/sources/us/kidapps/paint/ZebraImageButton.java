package us.kidapps.paint;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import us.colormefree.aanimal.R;

public class ZebraImageButton extends ZebraButton {
    public static final int PADDING_NORMAL_PERCENT = 5;
    public static final int PADDING_PUSHED_PERCENT = 10;
    private Bitmap _mirrorBitmap;
    private Bitmap _originalBitmap;
    private Paint _paint;
    private Bitmap _resizedBitmap;

    public ZebraImageButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this._originalBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.palette2);
        this._mirrorBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.newpic2);
        this._paint = new Paint(4);
    }

    public ZebraImageButton(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.zebraButtonStyle);
    }

    public void setOriginBitmapisMirrorBitpmap() {
        this._originalBitmap = this._mirrorBitmap;
        invalidate();
    }

    public ZebraImageButton(Context context) {
        this(context, null);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        int w = getWidth();
        int h = getHeight();
        int p = (Math.min(w, h) * (isPushedDown() ? 10 : 5)) / 100;
        int w2 = w - (p * 2);
        int h2 = h - (p * 2);
        if (!(this._resizedBitmap != null && this._resizedBitmap.getWidth() == w2 && this._resizedBitmap.getHeight() == h2)) {
            this._resizedBitmap = Bitmap.createBitmap(w2, h2, this._originalBitmap.getConfig());
            DrawUtils.convertSizeFill(this._originalBitmap, this._resizedBitmap);
        }
        canvas.drawBitmap(this._resizedBitmap, (float) p, (float) p, this._paint);
    }
}
