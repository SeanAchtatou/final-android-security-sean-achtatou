package us.kidapps.paint;

import android.app.Activity;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import java.util.List;

public class ZebraActivity extends Activity {
    public static final String INTENT_PICK_COLOR = "us.kidapps.paint.paint.PICK_COLOR";
    public static final String INTENT_START_NEW = "us.kidapps.paint.paint.START_NEW";
    protected static int _displayHeight;
    protected static int _displayWidth;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Display d = getWindowManager().getDefaultDisplay();
        _displayWidth = d.getWidth();
        _displayHeight = d.getHeight();
    }

    public static int getDisplayWitdh() {
        return _displayWidth;
    }

    public static int getDisplayHeight() {
        return _displayHeight;
    }

    /* access modifiers changed from: protected */
    public void findAllColorButtons(List<ColorButton> result) {
        findAllColorButtons((ViewGroup) getWindow().getDecorView(), result);
    }

    /* access modifiers changed from: protected */
    public void findAllColorButtons(ViewGroup g, List<ColorButton> result) {
        for (int i = 0; i < g.getChildCount(); i++) {
            View v = g.getChildAt(i);
            if (v instanceof ViewGroup) {
                findAllColorButtons((ViewGroup) v, result);
            }
            if (v instanceof ColorButton) {
                result.add((ColorButton) v);
            }
        }
    }
}
