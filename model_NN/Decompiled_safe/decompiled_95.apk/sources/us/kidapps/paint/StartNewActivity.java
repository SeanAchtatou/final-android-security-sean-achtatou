package us.kidapps.paint;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;
import us.colormefree.aanimal.R;

public class StartNewActivity extends Activity implements View.OnClickListener {
    public View.OnClickListener listenerbuttonBack = new View.OnClickListener() {
        public void onClick(View arg0) {
            StartNewActivity.this.finish();
        }
    };

    public static int randomOutlineId() {
        return new ResourceLoader().randomOutlineId();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(4, 4);
        setContentView((int) R.layout.start_new);
        ((GridView) findViewById(R.id.start_new_grid)).setAdapter((ListAdapter) new ImageAdapter(this));
        ((Button) findViewById(R.id.backbutton)).setOnClickListener(this.listenerbuttonBack);
    }

    public void onClick(View view) {
        setResult(view.getId());
        finish();
    }

    private static class ResourceLoader {
        private static final String PREFIX_OUTLINE = "image";
        private static final String PREFIX_THUMB = "thumb";
        private Integer[] _outlineIds;
        private Integer[] _thumbIds;

        ResourceLoader() {
            Map<String, Integer> outlineMap = new TreeMap<>();
            Map<String, Integer> thumbMap = new TreeMap<>();
            Field[] drawables = R.drawable.class.getDeclaredFields();
            for (int i = 0; i < drawables.length; i++) {
                String name = drawables[i].getName();
                try {
                    if (name.startsWith(PREFIX_OUTLINE)) {
                        outlineMap.put(name.substring(PREFIX_OUTLINE.length()), Integer.valueOf(drawables[i].getInt(null)));
                    }
                    if (name.startsWith(PREFIX_THUMB)) {
                        thumbMap.put(name.substring(PREFIX_THUMB.length()), Integer.valueOf(drawables[i].getInt(null)));
                    }
                } catch (IllegalAccessException e) {
                }
            }
            Set<String> keys = outlineMap.keySet();
            keys.retainAll(thumbMap.keySet());
            this._outlineIds = new Integer[keys.size()];
            this._thumbIds = new Integer[keys.size()];
            int j = 0;
            for (String key : keys) {
                this._outlineIds[j] = (Integer) outlineMap.get(key);
                this._thumbIds[j] = (Integer) thumbMap.get(key);
                j++;
            }
        }

        public Integer[] getThumbIds() {
            return this._thumbIds;
        }

        public Integer[] getOutlineIds() {
            return this._outlineIds;
        }

        public int randomOutlineId() {
            return this._outlineIds[new Random().nextInt(this._outlineIds.length)].intValue();
        }
    }

    private class ImageAdapter extends BaseAdapter {
        private Context _context;
        private ResourceLoader _resourceLoader = new ResourceLoader();

        ImageAdapter(Context c) {
            this._context = c;
        }

        public int getCount() {
            return this._resourceLoader.getThumbIds().length;
        }

        public Object getItem(int i) {
            return null;
        }

        public long getItemId(int i) {
            return 0;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView imageView;
            if (convertView == null) {
                imageView = new ImageView(this._context);
                int px = (int) TypedValue.applyDimension(1, 130.0f, StartNewActivity.this.getResources().getDisplayMetrics());
                imageView.setLayoutParams(new AbsListView.LayoutParams(px, px));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setPadding(8, 8, 8, 8);
                imageView.setOnClickListener(StartNewActivity.this);
            } else {
                imageView = (ImageView) convertView;
            }
            imageView.setImageResource(this._resourceLoader.getThumbIds()[position].intValue());
            imageView.setId(this._resourceLoader.getOutlineIds()[position].intValue());
            return imageView;
        }
    }
}
