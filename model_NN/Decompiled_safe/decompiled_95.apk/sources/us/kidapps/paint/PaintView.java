package us.kidapps.paint;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import us.colormefree.aanimal.R;

public class PaintView extends View {
    private static final int ALPHA_TRESHOLD = 65;
    private LifecycleListener _lifecycleListener;
    private Paint _paint;
    private State _state;
    Context mcontext;
    public MediaPlayer mp;

    public interface LifecycleListener {
        void onPreparedToLoad();
    }

    public PaintView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this._state = new State(null);
        this._paint = new Paint();
        this.mcontext = context.getApplicationContext();
    }

    public PaintView(Context context) {
        this(context, null);
    }

    public synchronized void setLifecycleListener(LifecycleListener l) {
        this._lifecycleListener = l;
    }

    public synchronized Object getState() {
        return this._state;
    }

    public synchronized void setState(Object o) {
        this._state = (State) o;
    }

    public void loadFromBitmap(Bitmap originalOutlineBitmap, Handler progressHandler) {
        int w;
        int h;
        byte b;
        State state = new State(null);
        synchronized (this) {
            w = this._state._width;
            h = this._state._height;
            state._color = this._state._color;
            state._width = w;
            state._height = h;
        }
        int n = w * h;
        Bitmap resizedBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        DrawUtils.convertSizeClip(originalOutlineBitmap, resizedBitmap);
        Progress.sendIncrementProgress(progressHandler, 10);
        state._outlineBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        state._paintMask = new byte[n];
        int[] pixels = new int[n];
        resizedBitmap.getPixels(pixels, 0, w, 0, 0, w, h);
        for (int i2 = 0; i2 < 90; i2++) {
            int iEnd = ((i2 + 1) * n) / 90;
            for (int i = (i2 * n) / 90; i < iEnd; i++) {
                int alpha = 255 - DrawUtils.brightness(pixels[i]);
                byte[] access$9 = state._paintMask;
                if (alpha < ALPHA_TRESHOLD) {
                    b = 1;
                } else {
                    b = 0;
                }
                access$9[i] = b;
                pixels[i] = alpha << 24;
            }
            Progress.sendIncrementProgress(progressHandler, 1);
        }
        state._outlineBitmap.setPixels(pixels, 0, w, 0, 0, w, h);
        try {
            state._paintedBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        } catch (Error e) {
            System.gc();
            state._paintedBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        }
        state._paintedBitmap.eraseColor(-1);
        state._workingMask = new byte[n];
        state._pixels = new int[n];
        Arrays.fill(state._pixels, -1);
        synchronized (this) {
            this._state = state;
        }
        progressHandler.sendEmptyMessage(2);
    }

    public synchronized void saveToFile(File file, Bitmap originalOutlineBitmap, Handler progressHandler) {
        Bitmap painted;
        synchronized (this) {
            painted = this._state._paintedBitmap.copy(this._state._paintedBitmap.getConfig(), true);
        }
        int hp = painted.getHeight();
        int wp = painted.getWidth();
        int np = hp * wp;
        int[] origPixels = new int[np];
        int[] newPixels = new int[np];
        painted.getPixels(newPixels, 0, wp, 0, 0, wp, hp);
        System.arraycopy(newPixels, 0, origPixels, 0, np);
        for (int y2 = 0; y2 < 25; y2++) {
            int yStart = (y2 * hp) / 25;
            int yEnd = ((y2 + 1) * hp) / 25;
            int p = yStart * wp;
            for (int y = yStart; y < yEnd; y++) {
                for (int x = 0; x < wp; x++) {
                    if (origPixels[p] == -1) {
                        if (x > 0 && origPixels[p - 1] != -1) {
                            newPixels[p] = origPixels[p - 1];
                        } else if (y > 0 && origPixels[p - wp] != -1) {
                            newPixels[p] = origPixels[p - wp];
                        } else if (x < wp - 1 && origPixels[p + 1] != -1) {
                            newPixels[p] = origPixels[p + 1];
                        } else if (y < hp - 1 && origPixels[p + wp] != -1) {
                            newPixels[p] = origPixels[p + wp];
                        }
                    }
                    p++;
                }
            }
            Progress.sendIncrementProgress(progressHandler, 1);
        }
        painted.setPixels(newPixels, 0, wp, 0, 0, wp, hp);
        float aspectRatio = ((float) painted.getWidth()) / ((float) painted.getHeight());
        int hr = originalOutlineBitmap.getHeight();
        int wr = (int) (((float) hr) * aspectRatio);
        if (wr > originalOutlineBitmap.getWidth()) {
            wr = originalOutlineBitmap.getWidth();
            hr = (int) (((float) wr) / aspectRatio);
        }
        int nr = wr * hr;
        Bitmap result = Bitmap.createBitmap(wr, hr, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(result);
        Paint paint = new Paint(1);
        canvas.drawBitmap(painted, new Rect(0, 0, painted.getWidth(), painted.getHeight()), new Rect(0, 0, wr, hr), paint);
        Progress.sendIncrementProgress(progressHandler, 5);
        Bitmap cropped = Bitmap.createBitmap(wr, hr, Bitmap.Config.ARGB_8888);
        int[] pixels = new int[nr];
        originalOutlineBitmap.getPixels(pixels, 0, wr, (originalOutlineBitmap.getWidth() - wr) / 2, (originalOutlineBitmap.getHeight() - hr) / 2, wr, hr);
        for (int i2 = 0; i2 < 45; i2++) {
            int iEnd = ((i2 + 1) * nr) / 45;
            for (int i = (i2 * nr) / 45; i < iEnd; i++) {
                pixels[i] = (255 - DrawUtils.brightness(pixels[i])) << 24;
            }
            Progress.sendIncrementProgress(progressHandler, 1);
        }
        cropped.setPixels(pixels, 0, wr, 0, 0, wr, hr);
        canvas.drawBitmap(cropped, 0.0f, 0.0f, paint);
        Progress.sendIncrementProgress(progressHandler, 10);
        try {
            file.getParentFile().mkdirs();
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            result.compress(Bitmap.CompressFormat.PNG, 90, fileOutputStream);
            fileOutputStream.close();
            Progress.sendIncrementProgress(progressHandler, 15);
            progressHandler.sendEmptyMessage(2);
        } catch (IOException e) {
            progressHandler.sendEmptyMessage(3);
            e.printStackTrace();
        }
    }

    public synchronized boolean isInitialized() {
        return this._state._paintedBitmap != null;
    }

    public synchronized void setPaintColor(int color) {
        this._state._color = color;
        this._paint.setColor(color);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        synchronized (this) {
            if (this._state._width == 0 || this._state._height == 0) {
                this._state._width = w;
                this._state._height = h;
                if (this._lifecycleListener != null) {
                    this._lifecycleListener.onPreparedToLoad();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void onDraw(Canvas canvas) {
        if (this._state._paintedBitmap != null) {
            canvas.drawBitmap(this._state._paintedBitmap, 0.0f, 0.0f, this._paint);
        }
        if (this._state._outlineBitmap != null) {
            canvas.drawBitmap(this._state._outlineBitmap, 0.0f, 0.0f, this._paint);
        }
    }

    public boolean onTouchEvent(MotionEvent e) {
        if (e.getAction() == 0) {
            paint((int) e.getX(), (int) e.getY());
        }
        try {
            this.mp = this.mp == null ? MediaPlayer.create(this.mcontext, (int) R.raw.wa) : this.mp;
            this.mp.start();
            return true;
        } catch (IllegalStateException e2) {
            e2.printStackTrace();
            return true;
        } catch (Error e3) {
            return true;
        }
    }

    private synchronized void paint(int x, int y) {
        System.arraycopy(this._state._paintMask, 0, this._state._workingMask, 0, this._state._width * this._state._height);
        FloodFill.fillRaw(x, y, this._state._width, this._state._height, this._state._workingMask, this._state._pixels, this._state._color);
        this._state._paintedBitmap.setPixels(this._state._pixels, 0, this._state._width, 0, 0, this._state._width, this._state._height);
        invalidate();
    }

    private static class State {
        /* access modifiers changed from: private */
        public int _color;
        /* access modifiers changed from: private */
        public int _height;
        /* access modifiers changed from: private */
        public Bitmap _outlineBitmap;
        /* access modifiers changed from: private */
        public byte[] _paintMask;
        /* access modifiers changed from: private */
        public Bitmap _paintedBitmap;
        /* access modifiers changed from: private */
        public int[] _pixels;
        /* access modifiers changed from: private */
        public int _width;
        /* access modifiers changed from: private */
        public byte[] _workingMask;

        private State() {
        }

        /* synthetic */ State(State state) {
            this();
        }
    }
}
