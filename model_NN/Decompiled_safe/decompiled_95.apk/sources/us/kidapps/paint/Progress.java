package us.kidapps.paint;

import android.os.Handler;
import android.os.Message;

public final class Progress {
    public static final int MAX = 100;
    public static final int MESSAGE_DONE_ERROR = 3;
    public static final int MESSAGE_DONE_OK = 2;
    public static final int MESSAGE_INCREMENT_PROGRESS = 1;

    public static final void sendIncrementProgress(Handler h, int diff) {
        h.sendMessage(Message.obtain(h, 1, diff, 0));
    }
}
