package us.kidapps.paint;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import java.util.Vector;
import us.colormefree.aanimal.R;

public class PickColorActivity extends ZebraActivity implements View.OnClickListener {
    public View.OnClickListener listenerbuttonBack = new View.OnClickListener() {
        public void onClick(View arg0) {
            PickColorActivity.this.finish();
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(4, 4);
        setContentView((int) R.layout.pick_color);
        Vector<ColorButton> colorButtons = new Vector<>();
        findAllColorButtons(colorButtons);
        for (int i = 0; i < colorButtons.size(); i++) {
            ((ColorButton) colorButtons.elementAt(i)).setOnClickListener(this);
        }
        ((Button) findViewById(R.id.backbutton)).setOnClickListener(this.listenerbuttonBack);
    }

    public void onClick(View view) {
        if (view instanceof ColorButton) {
            setResult(((ColorButton) view).getColor());
            finish();
        }
    }
}
