package us.colormefree.aanimal;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int color = 2130771975;
        public static final int colorButtonStyle = 2130771974;
        public static final int keywords = 2130771971;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771972;
        public static final int secondaryTextColor = 2130771970;
        public static final int zebraButtonStyle = 2130771973;
    }

    public static final class drawable {
        public static final int bgdown = 2130837504;
        public static final int button = 2130837505;
        public static final int exit = 2130837506;
        public static final int frame_down = 2130837507;
        public static final int frame_left = 2130837508;
        public static final int frame_right = 2130837509;
        public static final int frame_up = 2130837510;
        public static final int hdip800bottom = 2130837511;
        public static final int hdip854bottom = 2130837512;
        public static final int hdiptop = 2130837513;
        public static final int icon = 2130837514;
        public static final int image_001 = 2130837515;
        public static final int image_002 = 2130837516;
        public static final int image_003 = 2130837517;
        public static final int image_004 = 2130837518;
        public static final int image_005 = 2130837519;
        public static final int image_006 = 2130837520;
        public static final int image_007 = 2130837521;
        public static final int image_008 = 2130837522;
        public static final int image_009 = 2130837523;
        public static final int image_010 = 2130837524;
        public static final int image_011 = 2130837525;
        public static final int mdipbg = 2130837526;
        public static final int mdiptop = 2130837527;
        public static final int moregame_button = 2130837528;
        public static final int moregame_center = 2130837529;
        public static final int moregame_footer = 2130837530;
        public static final int moregame_header = 2130837531;
        public static final int moregame_ic = 2130837532;
        public static final int moregame_my_btn_normal = 2130837533;
        public static final int moregame_my_btn_normal_disable = 2130837534;
        public static final int moregame_my_btn_normal_disable_focused = 2130837535;
        public static final int moregame_my_btn_pressed = 2130837536;
        public static final int moregame_my_btn_selected = 2130837537;
        public static final int moregame_my_button = 2130837538;
        public static final int moregame_stub = 2130837539;
        public static final int moregame_title = 2130837540;
        public static final int newpaint = 2130837541;
        public static final int newpic2 = 2130837542;
        public static final int opennewpic = 2130837543;
        public static final int opennewpic2 = 2130837544;
        public static final int palette = 2130837545;
        public static final int palette2 = 2130837546;
        public static final int publisher_ic = 2130837547;
        public static final int save = 2130837548;
        public static final int share = 2130837549;
        public static final int thumb_001 = 2130837550;
        public static final int thumb_002 = 2130837551;
        public static final int thumb_003 = 2130837552;
        public static final int thumb_004 = 2130837553;
        public static final int thumb_005 = 2130837554;
        public static final int thumb_006 = 2130837555;
        public static final int thumb_007 = 2130837556;
        public static final int thumb_008 = 2130837557;
        public static final int thumb_009 = 2130837558;
        public static final int thumb_010 = 2130837559;
        public static final int thumb_011 = 2130837560;
        public static final int translucent_background = 2130837563;
        public static final int upbg = 2130837561;
        public static final int woodbackground = 2130837562;
    }

    public static final class id {
        public static final int ad = 2131230747;
        public static final int backbutton = 2131230748;
        public static final int bt_install = 2131230732;
        public static final int bt_more = 2131230733;
        public static final int bt_quit = 2131230731;
        public static final int button = 2131230722;
        public static final int buttonAllGames = 2131230737;
        public static final int buttonCancel = 2131230738;
        public static final int buttonClose = 2131230736;
        public static final int colorbutton = 2131230740;
        public static final int exit = 2131230757;
        public static final int hdipbottom = 2131230746;
        public static final int hdiptop = 2131230739;
        public static final int image = 2131230720;
        public static final int imageGame1 = 2131230728;
        public static final int imageGame2 = 2131230729;
        public static final int img_icon = 2131230724;
        public static final int list = 2131230734;
        public static final int moregame = 2131230754;
        public static final int namecompany = 2131230726;
        public static final int namegame = 2131230725;
        public static final int newpic_button = 2131230741;
        public static final int open_existing = 2131230752;
        public static final int open_gallery = 2131230755;
        public static final int open_new = 2131230750;
        public static final int paint_progress = 2131230745;
        public static final int paint_view = 2131230744;
        public static final int paintframe = 2131230743;
        public static final int pick_color_button = 2131230742;
        public static final int publisher = 2131230753;
        public static final int relay_button = 2131230730;
        public static final int relay_image = 2131230727;
        public static final int save = 2131230751;
        public static final int share = 2131230756;
        public static final int start_new_grid = 2131230749;
        public static final int text = 2131230721;
        public static final int textView1 = 2131230735;
        public static final int text_logo = 2131230723;
    }

    public static final class layout {
        public static final int moregame_item = 2130903040;
        public static final int moregame_main_quit2 = 2130903041;
        public static final int moregame_moregame2 = 2130903042;
        public static final int moregame_moregamelayout = 2130903043;
        public static final int paint = 2130903044;
        public static final int pick_color = 2130903045;
        public static final int start_new = 2130903046;
    }

    public static final class menu {
        public static final int paint_menu = 2131165184;
    }

    public static final class raw {
        public static final int coin_appears = 2130968576;
        public static final int fillcolor = 2130968577;
        public static final int gamestarted = 2130968578;
        public static final int intro = 2130968579;
        public static final int openmenu = 2130968580;
        public static final int savingorsending = 2130968581;
        public static final int selectcolor = 2130968582;
        public static final int selectcolor2 = 2130968583;
        public static final int task_complete = 2130968584;
        public static final int wa = 2130968585;
    }

    public static final class string {
        public static final int app_name = 2131099648;
        public static final int dialog_saving = 2131099655;
        public static final int dialog_saving_error = 2131099657;
        public static final int dialog_saving_ok = 2131099656;
        public static final int menu_exit = 2131099652;
        public static final int menu_moregame = 2131099654;
        public static final int menu_new = 2131099649;
        public static final int menu_publisher = 2131099653;
        public static final int menu_save = 2131099650;
        public static final int menu_share = 2131099651;
        public static final int saved_image_path_prefix = 2131099658;
    }

    public static final class style {
        public static final int AlertDialogCustom = 2131034115;
        public static final int DialogText = 2131034113;
        public static final int DialogText_Title = 2131034114;
        public static final int MyButton = 2131034112;
        public static final int MyDialogTheme = 2131034117;
        public static final int MyTheme = 2131034116;
        public static final int Theme = 2131034118;
        public static final int Theme_Default = 2131034119;
        public static final int Theme_Transparent = 2131034120;
        public static final int Widget = 2131034121;
        public static final int Widget_Button = 2131034122;
        public static final int Widget_Button_ColorButton = 2131034123;
    }

    public static final class styleable {
        public static final int[] ColorButton = {R.attr.color};
        public static final int ColorButton_color = 0;
        public static final int[] com_admob_android_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
    }
}
