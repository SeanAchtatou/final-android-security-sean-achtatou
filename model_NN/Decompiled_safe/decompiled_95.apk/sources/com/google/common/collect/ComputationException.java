package com.google.common.collect;

public class ComputationException extends RuntimeException {
    private static final long serialVersionUID = 0;

    public ComputationException(Throwable cause) {
        super(cause);
    }
}
