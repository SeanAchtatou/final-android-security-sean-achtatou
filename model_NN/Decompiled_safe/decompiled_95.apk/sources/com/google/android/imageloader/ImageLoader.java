package com.google.android.imageloader;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import java.io.IOException;
import java.lang.Thread;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.net.ContentHandler;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.net.URLStreamHandlerFactory;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

public final class ImageLoader {
    private static final int GINGERBREAD = 9;
    public static final String IMAGE_LOADER_SERVICE = "com.google.android.imageloader";
    private static final int SDK = Integer.parseInt(Build.VERSION.SDK);
    private static final String TAG = "ImageLoader";
    /* access modifiers changed from: private */
    public static final Class[] TYPE_BITMAP = {Bitmap.class};
    private static final int WORKER_COUNT = 3;
    /* access modifiers changed from: private */
    public final ContentHandler mBitmapContentHandler;
    private final Map<String, SoftReference<Bitmap>> mBitmaps;
    private final Map<String, Throwable> mErrors;
    /* access modifiers changed from: private */
    public final Map<ImageView, String> mImageViewBinding;
    /* access modifiers changed from: private */
    public final ContentHandler mPrefetchContentHandler;
    private final Handler mResultHandler;
    private final HashMap<String, URLStreamHandler> mStreamHandlers;
    private final Handler[] mTaskHandlers;
    private final URLStreamHandlerFactory mURLStreamHandlerFactory;

    public enum BindResult {
        OK,
        LOADING,
        ERROR
    }

    public interface Callback {
        void onImageError(ImageView imageView, String str, Throwable th);

        void onImageLoaded(ImageView imageView, String str);
    }

    public static ImageLoader get(Context context) {
        ImageLoader loader = (ImageLoader) context.getSystemService(IMAGE_LOADER_SERVICE);
        if (loader == null) {
            loader = (ImageLoader) context.getApplicationContext().getSystemService(IMAGE_LOADER_SERVICE);
        }
        if (loader != null) {
            return loader;
        }
        throw new IllegalStateException("ImageLoader not available");
    }

    private static boolean isWaiting(Handler handler) {
        Thread.State state = handler.getLooper().getThread().getState();
        return state == Thread.State.WAITING || state == Thread.State.TIMED_WAITING;
    }

    private static int getWhat(String url) {
        return url.hashCode();
    }

    /* access modifiers changed from: private */
    public static String getProtocol(String url) {
        return Uri.parse(url).getScheme();
    }

    public ImageLoader(URLStreamHandlerFactory streamFactory, ContentHandler bitmapHandler, ContentHandler prefetchHandler, Handler handler) {
        this.mURLStreamHandlerFactory = streamFactory;
        this.mStreamHandlers = streamFactory != null ? new HashMap<>() : null;
        this.mBitmapContentHandler = bitmapHandler != null ? bitmapHandler : new BitmapContentHandler();
        this.mPrefetchContentHandler = prefetchHandler;
        this.mResultHandler = handler != null ? handler : new Handler(Looper.getMainLooper());
        this.mTaskHandlers = new Handler[3];
        this.mImageViewBinding = new WeakHashMap();
        this.mBitmaps = Collections.synchronizedMap(new LruCache());
        this.mErrors = Collections.synchronizedMap(new LruCache());
    }

    public ImageLoader() {
        this(null, null, null, null);
    }

    public ImageLoader(ContentHandler bitmapHandler, ContentHandler prefetchHandler) {
        this(null, bitmapHandler, prefetchHandler, null);
    }

    public ImageLoader(ContentResolver resolver) {
        this(new ContentURLStreamHandlerFactory(resolver), null, null, null);
    }

    public ImageLoader(URLStreamHandlerFactory factory) {
        this(factory, null, null, null);
    }

    /* access modifiers changed from: private */
    public URLConnection openConnection(URL url) throws IOException {
        if (SDK < GINGERBREAD) {
            System.setProperty("http.keepAlive", "false");
        }
        return url.openConnection();
    }

    /* access modifiers changed from: private */
    public static void disconnect(URLConnection connection) {
        if (connection instanceof HttpURLConnection) {
            ((HttpURLConnection) connection).disconnect();
        }
    }

    private Handler createTaskHandler() {
        HandlerThread thread = new HandlerThread(TAG, 10);
        thread.start();
        return new TaskHandler(thread.getLooper());
    }

    private Handler getTaskHandler(String url) {
        int what = getWhat(url);
        for (Handler handler : this.mTaskHandlers) {
            if (handler != null && handler.hasMessages(what)) {
                return handler;
            }
        }
        int index = 0;
        while (index < this.mTaskHandlers.length) {
            Handler handler2 = this.mTaskHandlers[index];
            if (handler2 != null && !isWaiting(handler2)) {
                index++;
            } else if (handler2 != null) {
                return handler2;
            } else {
                Handler[] handlerArr = this.mTaskHandlers;
                Handler handler3 = createTaskHandler();
                handlerArr[index] = handler3;
                return handler3;
            }
        }
        String authority = Uri.parse(url).getAuthority();
        if (authority == null) {
            throw new IllegalArgumentException(url);
        }
        int index2 = Math.abs(authority.hashCode()) % this.mTaskHandlers.length;
        Handler handler4 = this.mTaskHandlers[index2];
        if (handler4 != null) {
            return handler4;
        }
        Handler[] handlerArr2 = this.mTaskHandlers;
        Handler handler5 = createTaskHandler();
        handlerArr2[index2] = handler5;
        return handler5;
    }

    /* access modifiers changed from: private */
    public URLStreamHandler getURLStreamHandler(String protocol) {
        URLStreamHandler handler;
        URLStreamHandlerFactory factory = this.mURLStreamHandlerFactory;
        if (factory == null) {
            return null;
        }
        HashMap<String, URLStreamHandler> handlers = this.mStreamHandlers;
        synchronized (handlers) {
            handler = handlers.get(protocol);
            if (handler == null && (handler = factory.createURLStreamHandler(protocol)) != null) {
                handlers.put(protocol, handler);
            }
        }
        return handler;
    }

    private void postTask(ImageTask task) {
        String url = task.getUrl();
        int what = getWhat(url);
        Handler handler = getTaskHandler(url);
        handler.sendMessageAtFrontOfQueue(handler.obtainMessage(what, task));
    }

    private void postTaskAtFrontOfQueue(ImageTask task) {
        String url = task.getUrl();
        int what = getWhat(url);
        Handler handler = getTaskHandler(url);
        handler.sendMessageAtFrontOfQueue(handler.obtainMessage(what, task));
    }

    /* access modifiers changed from: private */
    public void postResult(ImageTask task) {
        this.mResultHandler.post(task);
    }

    public BindResult bind(BaseAdapter adapter, ImageView view, String url) {
        if (adapter == null) {
            throw new NullPointerException();
        } else if (view == null) {
            throw new NullPointerException();
        } else if (url == null) {
            throw new NullPointerException();
        } else {
            Bitmap bitmap = getBitmap(url);
            Throwable error = getError(url);
            if (bitmap != null) {
                view.setImageBitmap(bitmap);
                return BindResult.OK;
            }
            view.setImageDrawable(null);
            if (error != null) {
                return BindResult.ERROR;
            }
            postTaskAtFrontOfQueue(new ImageTask(this, adapter, url));
            return BindResult.LOADING;
        }
    }

    public BindResult bind(ImageView view, String url, Callback callback) {
        if (view == null) {
            throw new NullPointerException();
        } else if (url == null) {
            throw new NullPointerException();
        } else {
            this.mImageViewBinding.put(view, url);
            Bitmap bitmap = getBitmap(url);
            Throwable error = getError(url);
            if (bitmap != null) {
                view.setImageBitmap(bitmap);
                return BindResult.OK;
            }
            view.setImageDrawable(null);
            if (error != null) {
                return BindResult.ERROR;
            }
            postTask(new ImageTask(this, view, url, callback));
            return BindResult.LOADING;
        }
    }

    public void unbind(ImageView view) {
        this.mImageViewBinding.remove(view);
        view.setImageDrawable(null);
    }

    public void clearErrors() {
        this.mErrors.clear();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.imageloader.ImageLoader.ImageTask.<init>(com.google.android.imageloader.ImageLoader, java.lang.String, boolean):void
     arg types: [com.google.android.imageloader.ImageLoader, java.lang.String, int]
     candidates:
      com.google.android.imageloader.ImageLoader.ImageTask.<init>(com.google.android.imageloader.ImageLoader, android.widget.BaseAdapter, java.lang.String):void
      com.google.android.imageloader.ImageLoader.ImageTask.<init>(com.google.android.imageloader.ImageLoader, java.lang.String, boolean):void */
    public void preload(String url) {
        if (url == null) {
            throw new NullPointerException();
        } else if (getBitmap(url) == null && getError(url) == null) {
            postTask(new ImageTask(this, url, true));
        }
    }

    public void preload(Cursor cursor, int columnIndex, int start, int end) {
        for (int position = start; position < end; position++) {
            if (cursor.moveToPosition(position)) {
                String url = cursor.getString(columnIndex);
                if (!TextUtils.isEmpty(url)) {
                    preload(url);
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.imageloader.ImageLoader.ImageTask.<init>(com.google.android.imageloader.ImageLoader, java.lang.String, boolean):void
     arg types: [com.google.android.imageloader.ImageLoader, java.lang.String, int]
     candidates:
      com.google.android.imageloader.ImageLoader.ImageTask.<init>(com.google.android.imageloader.ImageLoader, android.widget.BaseAdapter, java.lang.String):void
      com.google.android.imageloader.ImageLoader.ImageTask.<init>(com.google.android.imageloader.ImageLoader, java.lang.String, boolean):void */
    public void prefetch(String url) {
        if (url == null) {
            throw new NullPointerException();
        } else if (getBitmap(url) == null && getError(url) == null) {
            postTask(new ImageTask(this, url, false));
        }
    }

    public void prefetch(Cursor cursor, int columnIndex) {
        for (int position = 0; cursor.moveToPosition(position); position++) {
            String url = cursor.getString(columnIndex);
            if (!TextUtils.isEmpty(url)) {
                prefetch(url);
            }
        }
    }

    /* access modifiers changed from: private */
    public void putBitmap(String url, Bitmap bitmap) {
        this.mBitmaps.put(url, new SoftReference(bitmap));
    }

    /* access modifiers changed from: private */
    public void putError(String url, Throwable error) {
        this.mErrors.put(url, error);
    }

    /* access modifiers changed from: private */
    public boolean hasError(String url) {
        return this.mErrors.containsKey(url);
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    /* access modifiers changed from: private */
    public Bitmap getBitmap(String url) {
        SoftReference<Bitmap> reference = this.mBitmaps.get(url);
        if (reference != null) {
            return (Bitmap) reference.get();
        }
        return null;
    }

    /* access modifiers changed from: private */
    public Throwable getError(String url) {
        return this.mErrors.get(url);
    }

    public void quit() {
        for (int i = 0; i < this.mTaskHandlers.length; i++) {
            Handler handler = this.mTaskHandlers[i];
            if (handler != null) {
                handler.getLooper().quit();
                this.mTaskHandlers[i] = null;
            }
        }
    }

    private class ImageTask implements Runnable {
        private final WeakReference<BaseAdapter> mAdapterReference;
        private Bitmap mBitmap;
        private final Callback mCallback;
        private Throwable mError;
        private final WeakReference<ImageView> mImageViewReference;
        private final boolean mLoadBitmap;
        private final String mUrl;

        private ImageTask(BaseAdapter adapter, ImageView view, String url, Callback callback, boolean loadBitmap) {
            WeakReference<ImageView> weakReference;
            this.mAdapterReference = adapter != null ? new WeakReference<>(adapter) : null;
            if (view != null) {
                weakReference = new WeakReference<>(view);
            } else {
                weakReference = null;
            }
            this.mImageViewReference = weakReference;
            this.mUrl = url;
            this.mCallback = callback;
            this.mLoadBitmap = loadBitmap;
        }

        public ImageTask(ImageLoader imageLoader, BaseAdapter adapter, String url) {
            this(adapter, null, url, null, true);
        }

        public ImageTask(ImageLoader imageLoader, ImageView view, String url, Callback callback) {
            this(null, view, url, callback, true);
        }

        public ImageTask(ImageLoader imageLoader, String url, boolean loadBitmap) {
            this(null, null, url, null, loadBitmap);
        }

        public String getUrl() {
            return this.mUrl;
        }

        /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean execute() {
            /*
                r9 = this;
                r8 = 0
                r7 = 1
                java.lang.ref.WeakReference<android.widget.BaseAdapter> r5 = r9.mAdapterReference     // Catch:{ IOException -> 0x007d, RuntimeException -> 0x00a2, Error -> 0x00a9 }
                if (r5 == 0) goto L_0x0010
                java.lang.ref.WeakReference<android.widget.BaseAdapter> r5 = r9.mAdapterReference     // Catch:{ IOException -> 0x007d, RuntimeException -> 0x00a2, Error -> 0x00a9 }
                java.lang.Object r5 = r5.get()     // Catch:{ IOException -> 0x007d, RuntimeException -> 0x00a2, Error -> 0x00a9 }
                if (r5 != 0) goto L_0x0010
                r5 = r8
            L_0x000f:
                return r5
            L_0x0010:
                java.lang.ref.WeakReference<android.widget.ImageView> r5 = r9.mImageViewReference     // Catch:{ IOException -> 0x007d, RuntimeException -> 0x00a2, Error -> 0x00a9 }
                if (r5 == 0) goto L_0x001e
                java.lang.ref.WeakReference<android.widget.ImageView> r5 = r9.mImageViewReference     // Catch:{ IOException -> 0x007d, RuntimeException -> 0x00a2, Error -> 0x00a9 }
                java.lang.Object r5 = r5.get()     // Catch:{ IOException -> 0x007d, RuntimeException -> 0x00a2, Error -> 0x00a9 }
                if (r5 != 0) goto L_0x001e
                r5 = r8
                goto L_0x000f
            L_0x001e:
                com.google.android.imageloader.ImageLoader r5 = com.google.android.imageloader.ImageLoader.this     // Catch:{ IOException -> 0x007d, RuntimeException -> 0x00a2, Error -> 0x00a9 }
                java.lang.String r6 = r9.mUrl     // Catch:{ IOException -> 0x007d, RuntimeException -> 0x00a2, Error -> 0x00a9 }
                java.lang.Throwable r5 = r5.getError(r6)     // Catch:{ IOException -> 0x007d, RuntimeException -> 0x00a2, Error -> 0x00a9 }
                r9.mError = r5     // Catch:{ IOException -> 0x007d, RuntimeException -> 0x00a2, Error -> 0x00a9 }
                java.lang.Throwable r5 = r9.mError     // Catch:{ IOException -> 0x007d, RuntimeException -> 0x00a2, Error -> 0x00a9 }
                if (r5 == 0) goto L_0x002e
                r5 = r7
                goto L_0x000f
            L_0x002e:
                com.google.android.imageloader.ImageLoader r5 = com.google.android.imageloader.ImageLoader.this     // Catch:{ IOException -> 0x007d, RuntimeException -> 0x00a2, Error -> 0x00a9 }
                java.lang.String r6 = r9.mUrl     // Catch:{ IOException -> 0x007d, RuntimeException -> 0x00a2, Error -> 0x00a9 }
                android.graphics.Bitmap r5 = r5.getBitmap(r6)     // Catch:{ IOException -> 0x007d, RuntimeException -> 0x00a2, Error -> 0x00a9 }
                r9.mBitmap = r5     // Catch:{ IOException -> 0x007d, RuntimeException -> 0x00a2, Error -> 0x00a9 }
                android.graphics.Bitmap r5 = r9.mBitmap     // Catch:{ IOException -> 0x007d, RuntimeException -> 0x00a2, Error -> 0x00a9 }
                if (r5 == 0) goto L_0x003e
                r5 = r7
                goto L_0x000f
            L_0x003e:
                java.lang.String r5 = r9.mUrl     // Catch:{ IOException -> 0x007d, RuntimeException -> 0x00a2, Error -> 0x00a9 }
                java.lang.String r2 = com.google.android.imageloader.ImageLoader.getProtocol(r5)     // Catch:{ IOException -> 0x007d, RuntimeException -> 0x00a2, Error -> 0x00a9 }
                com.google.android.imageloader.ImageLoader r5 = com.google.android.imageloader.ImageLoader.this     // Catch:{ IOException -> 0x007d, RuntimeException -> 0x00a2, Error -> 0x00a9 }
                java.net.URLStreamHandler r3 = r5.getURLStreamHandler(r2)     // Catch:{ IOException -> 0x007d, RuntimeException -> 0x00a2, Error -> 0x00a9 }
                java.net.URL r4 = new java.net.URL     // Catch:{ IOException -> 0x007d, RuntimeException -> 0x00a2, Error -> 0x00a9 }
                r5 = 0
                java.lang.String r6 = r9.mUrl     // Catch:{ IOException -> 0x007d, RuntimeException -> 0x00a2, Error -> 0x00a9 }
                r4.<init>(r5, r6, r3)     // Catch:{ IOException -> 0x007d, RuntimeException -> 0x00a2, Error -> 0x00a9 }
                com.google.android.imageloader.ImageLoader r5 = com.google.android.imageloader.ImageLoader.this     // Catch:{ IOException -> 0x007d, RuntimeException -> 0x00a2, Error -> 0x00a9 }
                java.net.URLConnection r0 = r5.openConnection(r4)     // Catch:{ IOException -> 0x007d, RuntimeException -> 0x00a2, Error -> 0x00a9 }
                boolean r5 = r9.mLoadBitmap     // Catch:{ all -> 0x0078 }
                if (r5 == 0) goto L_0x0088
                com.google.android.imageloader.ImageLoader r5 = com.google.android.imageloader.ImageLoader.this     // Catch:{ all -> 0x0078 }
                java.net.ContentHandler r5 = r5.mBitmapContentHandler     // Catch:{ all -> 0x0078 }
                java.lang.Class[] r6 = com.google.android.imageloader.ImageLoader.TYPE_BITMAP     // Catch:{ all -> 0x0078 }
                java.lang.Object r5 = r5.getContent(r0, r6)     // Catch:{ all -> 0x0078 }
                android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5     // Catch:{ all -> 0x0078 }
                r9.mBitmap = r5     // Catch:{ all -> 0x0078 }
                android.graphics.Bitmap r5 = r9.mBitmap     // Catch:{ all -> 0x0078 }
                if (r5 != 0) goto L_0x0083
                java.lang.NullPointerException r5 = new java.lang.NullPointerException     // Catch:{ all -> 0x0078 }
                r5.<init>()     // Catch:{ all -> 0x0078 }
                throw r5     // Catch:{ all -> 0x0078 }
            L_0x0078:
                r5 = move-exception
                com.google.android.imageloader.ImageLoader.disconnect(r0)     // Catch:{ IOException -> 0x007d, RuntimeException -> 0x00a2, Error -> 0x00a9 }
                throw r5     // Catch:{ IOException -> 0x007d, RuntimeException -> 0x00a2, Error -> 0x00a9 }
            L_0x007d:
                r5 = move-exception
                r1 = r5
                r9.mError = r1
                r5 = r7
                goto L_0x000f
            L_0x0083:
                com.google.android.imageloader.ImageLoader.disconnect(r0)     // Catch:{ IOException -> 0x007d, RuntimeException -> 0x00a2, Error -> 0x00a9 }
                r5 = r7
                goto L_0x000f
            L_0x0088:
                com.google.android.imageloader.ImageLoader r5 = com.google.android.imageloader.ImageLoader.this     // Catch:{ all -> 0x0078 }
                java.net.ContentHandler r5 = r5.mPrefetchContentHandler     // Catch:{ all -> 0x0078 }
                if (r5 == 0) goto L_0x0099
                com.google.android.imageloader.ImageLoader r5 = com.google.android.imageloader.ImageLoader.this     // Catch:{ all -> 0x0078 }
                java.net.ContentHandler r5 = r5.mPrefetchContentHandler     // Catch:{ all -> 0x0078 }
                r5.getContent(r0)     // Catch:{ all -> 0x0078 }
            L_0x0099:
                r5 = 0
                r9.mBitmap = r5     // Catch:{ all -> 0x0078 }
                com.google.android.imageloader.ImageLoader.disconnect(r0)     // Catch:{ IOException -> 0x007d, RuntimeException -> 0x00a2, Error -> 0x00a9 }
                r5 = r8
                goto L_0x000f
            L_0x00a2:
                r5 = move-exception
                r1 = r5
                r9.mError = r1
                r5 = r7
                goto L_0x000f
            L_0x00a9:
                r5 = move-exception
                r1 = r5
                r9.mError = r1
                r5 = r7
                goto L_0x000f
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.imageloader.ImageLoader.ImageTask.execute():boolean");
        }

        public void run() {
            ImageView view;
            if (this.mBitmap != null) {
                ImageLoader.this.putBitmap(this.mUrl, this.mBitmap);
            } else if (this.mError != null && !ImageLoader.this.hasError(this.mUrl)) {
                Log.e(ImageLoader.TAG, "Failed to load " + this.mUrl, this.mError);
                ImageLoader.this.putError(this.mUrl, this.mError);
            }
            if (this.mAdapterReference != null) {
                BaseAdapter adapter = this.mAdapterReference.get();
                if (adapter != null && !adapter.isEmpty()) {
                    adapter.notifyDataSetChanged();
                }
            } else if (this.mImageViewReference != null && (view = this.mImageViewReference.get()) != null && TextUtils.equals((String) ImageLoader.this.mImageViewBinding.get(view), this.mUrl)) {
                Context context = view.getContext();
                if ((context instanceof Activity) && ((Activity) context).isFinishing()) {
                    return;
                }
                if (this.mBitmap != null) {
                    view.setImageBitmap(this.mBitmap);
                    if (this.mCallback != null) {
                        this.mCallback.onImageLoaded(view, this.mUrl);
                    }
                } else if (this.mError != null && this.mCallback != null) {
                    this.mCallback.onImageError(view, this.mUrl, this.mError);
                }
            }
        }
    }

    private class TaskHandler extends Handler {
        public TaskHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            ImageTask task = (ImageTask) msg.obj;
            if (task.execute()) {
                ImageLoader.this.postResult(task);
            }
        }
    }
}
