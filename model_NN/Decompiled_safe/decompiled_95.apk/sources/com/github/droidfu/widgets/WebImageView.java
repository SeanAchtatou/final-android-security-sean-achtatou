package com.github.droidfu.widgets;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Message;
import android.util.AttributeSet;
import android.view.animation.AlphaAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ViewSwitcher;
import com.github.droidfu.DroidFu;
import com.github.droidfu.imageloader.ImageLoader;
import com.github.droidfu.imageloader.ImageLoaderHandler;

public class WebImageView extends ViewSwitcher {
    private String imageUrl;
    /* access modifiers changed from: private */
    public ImageView imageView;
    /* access modifiers changed from: private */
    public boolean isLoaded;
    private ProgressBar loadingSpinner;
    private Drawable progressDrawable;
    private ImageView.ScaleType scaleType = ImageView.ScaleType.CENTER_CROP;

    public WebImageView(Context context, String imageUrl2, boolean autoLoad) {
        super(context);
        initialize(context, imageUrl2, null, autoLoad);
    }

    public WebImageView(Context context, String imageUrl2, Drawable progressDrawable2, boolean autoLoad) {
        super(context);
        initialize(context, imageUrl2, progressDrawable2, autoLoad);
    }

    public WebImageView(Context context, AttributeSet attributes) {
        super(context, attributes);
        int progressDrawableId = attributes.getAttributeResourceValue(DroidFu.XMLNS, "progressDrawable", 0);
        initialize(context, attributes.getAttributeValue(DroidFu.XMLNS, "imageUrl"), progressDrawableId > 0 ? context.getResources().getDrawable(progressDrawableId) : null, attributes.getAttributeBooleanValue(DroidFu.XMLNS, "autoLoad", true));
    }

    private void initialize(Context context, String imageUrl2, Drawable progressDrawable2, boolean autoLoad) {
        this.imageUrl = imageUrl2;
        this.progressDrawable = progressDrawable2;
        ImageLoader.initialize(context);
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500);
        setInAnimation(anim);
        addLoadingSpinnerView(context);
        addImageView(context);
        if (autoLoad && imageUrl2 != null) {
            loadImage();
        }
    }

    private void addLoadingSpinnerView(Context context) {
        this.loadingSpinner = new ProgressBar(context);
        this.loadingSpinner.setIndeterminate(true);
        if (this.progressDrawable == null) {
            this.progressDrawable = this.loadingSpinner.getIndeterminateDrawable();
        } else {
            this.loadingSpinner.setIndeterminateDrawable(this.progressDrawable);
            if (this.progressDrawable instanceof AnimationDrawable) {
                ((AnimationDrawable) this.progressDrawable).start();
            }
        }
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(this.progressDrawable.getIntrinsicWidth(), this.progressDrawable.getIntrinsicHeight());
        lp.gravity = 17;
        addView(this.loadingSpinner, 0, lp);
    }

    private void addImageView(Context context) {
        this.imageView = new ImageView(context);
        this.imageView.setScaleType(this.scaleType);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(-2, -2);
        lp.gravity = 17;
        addView(this.imageView, 1, lp);
    }

    public void loadImage() {
        if (this.imageUrl == null) {
            throw new IllegalStateException("image URL is null; did you forget to set it for this view?");
        }
        ImageLoader.start(this.imageUrl, new DefaultImageLoaderHandler());
    }

    public boolean isLoaded() {
        return this.isLoaded;
    }

    public void setImageUrl(String imageUrl2) {
        this.imageUrl = imageUrl2;
    }

    public void setProgressDrawable(Drawable progressDrawable2) {
        this.progressDrawable = progressDrawable2;
    }

    public void setNoImageDrawable(int imageResourceId) {
        this.imageView.setImageDrawable(getContext().getResources().getDrawable(imageResourceId));
        setDisplayedChild(1);
    }

    public void reset() {
        super.reset();
        setDisplayedChild(0);
    }

    private class DefaultImageLoaderHandler extends ImageLoaderHandler {
        public DefaultImageLoaderHandler() {
            super(WebImageView.this.imageView);
        }

        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            boolean unused = WebImageView.this.isLoaded = true;
            WebImageView.this.setDisplayedChild(1);
        }
    }
}
