package com.github.droidfu.http;

import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import android.util.Log;
import java.io.IOException;
import java.net.ConnectException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.RequestWrapper;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

public abstract class BetterHttpRequest {
    private static final int CONNECTION_TIMEOUT = 10000;
    protected static final String HTTP_CONTENT_TYPE_HEADER = "Content-Type";
    protected static final String HTTP_USER_AGENT = "Android/DroidFu";
    private static final String LOG_TAG = BetterHttpRequest.class.getSimpleName();
    private static final int MAX_CONNECTIONS = 6;
    private static final int MAX_RETRIES = 5;
    private static final String REQUEST_URI_BACKUP = "request_uri_backup";
    private static final int RETRY_SLEEP_TIME_MILLIS = 3000;
    private static Context appContext;
    private static HashMap<String, String> defaultHeaders = new HashMap<>();
    private static AbstractHttpClient httpClient;
    /* access modifiers changed from: private */
    public List<Integer> expectedStatusCodes = new ArrayList();
    /* access modifiers changed from: private */
    public OAuthConsumer oauthConsumer;
    protected HttpUriRequest request;
    private ResponseHandler<BetterHttpResponse> responseHandler = new ResponseHandler<BetterHttpResponse>() {
        public BetterHttpResponse handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
            int status = response.getStatusLine().getStatusCode();
            if (BetterHttpRequest.this.expectedStatusCodes == null || BetterHttpRequest.this.expectedStatusCodes.contains(Integer.valueOf(status))) {
                return new BetterHttpResponse(response);
            }
            throw new HttpResponseException(status, "Unexpected status code: " + status);
        }
    };
    private HttpRequestRetryHandler retryHandler = new HttpRequestRetryHandler() {
        public boolean retryRequest(IOException exception, int executionCount, HttpContext context) {
            if (executionCount > 5) {
                return false;
            }
            exception.printStackTrace();
            Log.d(BetterHttpRequest.class.getSimpleName(), "Retrying " + BetterHttpRequest.this.request.getRequestLine().getUri() + " (tried: " + executionCount + " times)");
            RequestWrapper request = (RequestWrapper) context.getAttribute("http.request");
            URI rewrittenUri = request.getURI();
            request.setURI((URI) context.getAttribute(BetterHttpRequest.REQUEST_URI_BACKUP));
            if (BetterHttpRequest.this.oauthConsumer != null) {
                try {
                    BetterHttpRequest.this.oauthConsumer.sign(request);
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
            }
            request.setURI(rewrittenUri);
            return true;
        }
    };

    static {
        setupHttpClient();
    }

    private static void setupHttpClient() {
        BasicHttpParams httpParams = new BasicHttpParams();
        ConnManagerParams.setTimeout(httpParams, 10000);
        ConnManagerParams.setMaxConnectionsPerRoute(httpParams, new ConnPerRouteBean((int) MAX_CONNECTIONS));
        ConnManagerParams.setMaxTotalConnections(httpParams, (int) MAX_CONNECTIONS);
        HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setUserAgent(httpParams, HTTP_USER_AGENT);
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        schemeRegistry.register(new Scheme("https", PlainSocketFactory.getSocketFactory(), 443));
        httpClient = new DefaultHttpClient(new ThreadSafeClientConnManager(httpParams, schemeRegistry), httpParams);
    }

    public static void updateProxySettings(Context context) {
        HttpParams httpParams = httpClient.getParams();
        NetworkInfo nwInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (nwInfo != null) {
            if (nwInfo.getType() == 0) {
                String proxyHost = Proxy.getDefaultHost();
                int proxyPort = Proxy.getDefaultPort();
                if (proxyHost != null && proxyPort > -1) {
                    Log.d(LOG_TAG, "Detected carrier proxy " + proxyHost + ":" + proxyPort);
                    httpParams.setParameter("http.route.default-proxy", new HttpHost(proxyHost, proxyPort));
                    return;
                }
                return;
            }
            httpParams.setParameter("http.route.default-proxy", null);
        }
    }

    public static void setContext(Context context) {
        if (appContext == null) {
            appContext = context.getApplicationContext();
            context.registerReceiver(new ConnectionChangedBroadcastReceiver(), new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        }
    }

    public static void setPortForScheme(String scheme, int port) {
        httpClient.getConnectionManager().getSchemeRegistry().register(new Scheme(scheme, PlainSocketFactory.getSocketFactory(), port));
    }

    public static void setDefaultHeader(String header, String value) {
        defaultHeaders.put(header, value);
    }

    public static BetterHttpRequest get(String url) {
        return new HttpGet(url, defaultHeaders);
    }

    public static BetterHttpRequest post(String url, HttpEntity payload) {
        return new HttpPost(url, payload, defaultHeaders);
    }

    public HttpUriRequest unwrap() {
        return this.request;
    }

    public BetterHttpRequest expecting(Integer... statusCodes) {
        this.expectedStatusCodes = Arrays.asList(statusCodes);
        return this;
    }

    public BetterHttpRequest signed(OAuthConsumer oauthConsumer2) throws OAuthMessageSignerException, OAuthExpectationFailedException {
        this.oauthConsumer = oauthConsumer2;
        oauthConsumer2.sign(unwrap());
        return this;
    }

    /* Debug info: failed to restart local var, previous not found, register: 8 */
    public BetterHttpResponse send() throws ConnectException {
        if (appContext != null) {
            updateProxySettings(appContext);
        }
        HttpContext httpContext = new BasicHttpContext();
        httpContext.setAttribute(REQUEST_URI_BACKUP, this.request.getURI());
        httpClient.setHttpRequestRetryHandler(this.retryHandler);
        int numAttempts = 0;
        while (numAttempts < 5) {
            numAttempts++;
            try {
                if (this.oauthConsumer != null) {
                    this.oauthConsumer.sign(this.request);
                }
                return (BetterHttpResponse) httpClient.execute(this.request, this.responseHandler, httpContext);
            } catch (Exception e) {
                waitAndContinue(e, numAttempts, 5);
            }
        }
        return null;
    }

    private void waitAndContinue(Exception cause, int numAttempts, int maxAttempts) throws ConnectException {
        if (numAttempts == maxAttempts) {
            Log.e(LOG_TAG, "request failed after " + numAttempts + " attempts");
            ConnectException ex = new ConnectException();
            ex.initCause(cause);
            throw ex;
        }
        cause.printStackTrace();
        Log.e(LOG_TAG, "request failed, will retry after 3 secs...");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
        }
    }
}
