package com.github.droidfu.http;

import java.util.HashMap;
import org.apache.http.HttpEntity;

class HttpPost extends BetterHttpRequest {
    HttpPost(String url, HttpEntity payload, HashMap<String, String> defaultHeaders) {
        this.request = new org.apache.http.client.methods.HttpPost(url);
        this.request.setEntity(payload);
        this.request.setHeader("Content-Type", payload.getContentType().getValue());
        for (String header : defaultHeaders.keySet()) {
            this.request.setHeader(header, defaultHeaders.get(header));
        }
    }
}
