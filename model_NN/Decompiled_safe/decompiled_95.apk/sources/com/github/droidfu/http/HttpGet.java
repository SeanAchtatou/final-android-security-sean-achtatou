package com.github.droidfu.http;

import java.util.HashMap;

class HttpGet extends BetterHttpRequest {
    HttpGet(String url, HashMap<String, String> defaultHeaders) {
        this.request = new org.apache.http.client.methods.HttpGet(url);
        for (String header : defaultHeaders.keySet()) {
            this.request.setHeader(header, defaultHeaders.get(header));
        }
    }
}
