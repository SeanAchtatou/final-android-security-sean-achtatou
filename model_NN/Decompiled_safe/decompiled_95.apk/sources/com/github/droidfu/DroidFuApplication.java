package com.github.droidfu;

import android.app.Application;
import android.content.Context;
import java.lang.ref.WeakReference;
import java.util.HashMap;

public class DroidFuApplication extends Application {
    private HashMap<String, WeakReference<Context>> contextObjects = new HashMap<>();

    public synchronized Context getActiveContext(String className) {
        Context context;
        WeakReference<Context> ref = this.contextObjects.get(className);
        if (ref == null) {
            context = null;
        } else {
            context = (Context) ref.get();
        }
        return context;
    }

    public synchronized void setActiveContext(String className, Context context) {
        this.contextObjects.put(className, new WeakReference<>(context));
    }

    public synchronized void resetActiveContext(String className) {
        this.contextObjects.remove(className);
    }
}
