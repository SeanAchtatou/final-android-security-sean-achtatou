package com.github.droidfu.imageloader;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.ImageView;
import java.net.URL;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class ImageLoader implements Runnable {
    static final String BITMAP_EXTRA = "droidfu:extra_bitmap";
    private static final int DEFAULT_POOL_SIZE = 2;
    static final int HANDLER_MESSAGE_ID = 0;
    private static ThreadPoolExecutor executor;
    private static ImageCache imageCache;
    private static int numAttempts = 3;
    private Handler handler;
    private String imageUrl;

    public static void setThreadPoolSize(int numThreads) {
        executor.setMaximumPoolSize(numThreads);
    }

    public static void setMaxDownloadAttempts(int numAttempts2) {
        numAttempts = numAttempts2;
    }

    public static synchronized void initialize(Context context) {
        synchronized (ImageLoader.class) {
            if (executor == null) {
                executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(2);
            }
            if (imageCache == null) {
                imageCache = new ImageCache(context, 25, 5);
            }
        }
    }

    private ImageLoader(String imageUrl2, ImageView imageView) {
        this.imageUrl = imageUrl2;
        this.handler = new ImageLoaderHandler(imageView);
    }

    private ImageLoader(String imageUrl2, ImageLoaderHandler handler2) {
        this.imageUrl = imageUrl2;
        this.handler = handler2;
    }

    public static void start(String imageUrl2, ImageView imageView) {
        ImageLoader loader = new ImageLoader(imageUrl2, imageView);
        synchronized (imageCache) {
            Bitmap image = imageCache.get((Object) imageUrl2);
            if (image == null) {
                executor.execute(loader);
            } else {
                imageView.setImageBitmap(image);
            }
        }
    }

    public static void start(String imageUrl2, ImageLoaderHandler handler2) {
        ImageLoader loader = new ImageLoader(imageUrl2, handler2);
        synchronized (imageCache) {
            Bitmap image = imageCache.get((Object) imageUrl2);
            if (image == null) {
                executor.execute(loader);
            } else {
                loader.notifyImageLoaded(image);
            }
        }
    }

    public static void clearCache() {
        synchronized (imageCache) {
            imageCache.clear();
        }
    }

    public void run() {
        Bitmap bitmap = null;
        int timesTried = 1;
        while (true) {
            if (timesTried > numAttempts) {
                break;
            }
            try {
                bitmap = BitmapFactory.decodeStream(new URL(this.imageUrl).openStream());
                synchronized (imageCache) {
                    imageCache.put(this.imageUrl, bitmap);
                    break;
                }
            } catch (Throwable th) {
                Log.w(ImageLoader.class.getSimpleName(), "download for " + this.imageUrl + " failed (attempt " + timesTried + ")");
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                }
                timesTried++;
            }
        }
        if (bitmap != null) {
            notifyImageLoaded(bitmap);
        }
    }

    public void notifyImageLoaded(Bitmap bitmap) {
        Message message = new Message();
        message.what = 0;
        Bundle data = new Bundle();
        data.putParcelable(BITMAP_EXTRA, bitmap);
        message.setData(data);
        this.handler.sendMessage(message);
    }
}
