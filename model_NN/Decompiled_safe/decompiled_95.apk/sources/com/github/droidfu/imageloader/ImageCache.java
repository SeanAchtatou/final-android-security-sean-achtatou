package com.github.droidfu.imageloader;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.google.common.collect.MapMaker;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class ImageCache implements Map<String, Bitmap> {
    private Map<String, Bitmap> cache;
    private int cachedImageQuality = 75;
    private Bitmap.CompressFormat compressedImageFormat = Bitmap.CompressFormat.JPEG;
    private String secondLevelCacheDir;

    public ImageCache(Context context, int initialCapacity, int concurrencyLevel) {
        this.cache = new MapMaker().initialCapacity(initialCapacity).concurrencyLevel(concurrencyLevel).weakValues().makeMap();
        this.secondLevelCacheDir = context.getApplicationContext().getCacheDir() + "/droidfu/imagecache";
        new File(this.secondLevelCacheDir).mkdirs();
    }

    public void setCachedImageQuality(int cachedImageQuality2) {
        this.cachedImageQuality = cachedImageQuality2;
    }

    public int getCachedImageQuality() {
        return this.cachedImageQuality;
    }

    public synchronized Bitmap get(Object key) {
        Bitmap bitmap;
        String imageUrl = (String) key;
        Bitmap bitmap2 = this.cache.get(imageUrl);
        if (bitmap2 != null) {
            bitmap = bitmap2;
        } else {
            File imageFile = getImageFile(imageUrl);
            if (imageFile.exists()) {
                Bitmap bitmap3 = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
                if (bitmap3 == null) {
                    bitmap = null;
                } else {
                    this.cache.put(imageUrl, bitmap3);
                    bitmap = bitmap3;
                }
            } else {
                bitmap = null;
            }
        }
        return bitmap;
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    public Bitmap put(String imageUrl, Bitmap image) {
        File imageFile = getImageFile(imageUrl);
        try {
            imageFile.createNewFile();
            FileOutputStream ostream = new FileOutputStream(imageFile);
            image.compress(this.compressedImageFormat, this.cachedImageQuality, ostream);
            ostream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return this.cache.put(imageUrl, image);
    }

    public void putAll(Map<? extends String, ? extends Bitmap> map) {
        throw new UnsupportedOperationException();
    }

    public boolean containsKey(Object key) {
        return this.cache.containsKey(key);
    }

    public boolean containsValue(Object value) {
        return this.cache.containsValue(value);
    }

    public Bitmap remove(Object key) {
        return this.cache.remove(key);
    }

    public Set<String> keySet() {
        return this.cache.keySet();
    }

    public Set<Map.Entry<String, Bitmap>> entrySet() {
        return this.cache.entrySet();
    }

    public int size() {
        return this.cache.size();
    }

    public boolean isEmpty() {
        return this.cache.isEmpty();
    }

    public void clear() {
        this.cache.clear();
    }

    public Collection<Bitmap> values() {
        return this.cache.values();
    }

    private File getImageFile(String imageUrl) {
        return new File(this.secondLevelCacheDir + "/" + (Integer.toHexString(imageUrl.hashCode()) + "." + this.compressedImageFormat.name()));
    }
}
