package com.github.droidfu.imageloader;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;

public class ImageLoaderHandler extends Handler {
    private ImageView imageView;

    public ImageLoaderHandler(ImageView imageView2) {
        this.imageView = imageView2;
    }

    public void handleMessage(Message msg) {
        if (msg.what == 0) {
            this.imageView.setImageBitmap((Bitmap) msg.getData().getParcelable("droidfu:extra_bitmap"));
        }
    }

    /* access modifiers changed from: package-private */
    public ImageView getImageView() {
        return this.imageView;
    }
}
