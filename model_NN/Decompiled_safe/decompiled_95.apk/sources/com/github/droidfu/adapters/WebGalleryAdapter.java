package com.github.droidfu.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.Gallery;
import com.github.droidfu.widgets.WebImageView;
import java.util.List;

public class WebGalleryAdapter extends BaseAdapter {
    private Context context;
    private List<String> imageUrls;
    private Drawable progressDrawable;

    public WebGalleryAdapter(Context context2) {
        initialize(context2, null, null);
    }

    public WebGalleryAdapter(Context context2, List<String> imageUrls2) {
        initialize(context2, imageUrls2, null);
    }

    public WebGalleryAdapter(Context context2, List<String> imageUrls2, int progressDrawableResId) {
        initialize(context2, imageUrls2, context2.getResources().getDrawable(progressDrawableResId));
    }

    private void initialize(Context context2, List<String> imageUrls2, Drawable progressDrawable2) {
        this.imageUrls = imageUrls2;
        this.context = context2;
        this.progressDrawable = progressDrawable2;
    }

    public int getCount() {
        return this.imageUrls.size();
    }

    public Object getItem(int position) {
        return this.imageUrls.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public void setImageUrls(List<String> imageUrls2) {
        this.imageUrls = imageUrls2;
    }

    public List<String> getImageUrls() {
        return this.imageUrls;
    }

    public void setProgressDrawable(Drawable progressDrawable2) {
        this.progressDrawable = progressDrawable2;
    }

    public Drawable getProgressDrawable() {
        return this.progressDrawable;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        FrameLayout container = new FrameLayout(this.context);
        container.setLayoutParams(new Gallery.LayoutParams(-1, -1));
        WebImageView item = new WebImageView(this.context, (String) getItem(position), this.progressDrawable, false);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(-2, -2);
        lp.gravity = 17;
        item.setLayoutParams(lp);
        container.addView(item);
        item.loadImage();
        onGetView(position, convertView, parent);
        return container;
    }

    /* access modifiers changed from: protected */
    public void onGetView(int position, View convertView, ViewGroup parent) {
    }
}
