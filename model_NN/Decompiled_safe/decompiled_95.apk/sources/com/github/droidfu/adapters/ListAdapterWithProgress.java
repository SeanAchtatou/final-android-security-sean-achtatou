package com.github.droidfu.adapters;

import android.app.ListActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import java.util.LinkedList;
import java.util.List;

public abstract class ListAdapterWithProgress<T> extends BaseAdapter {
    protected ListActivity context;
    protected List<T> data = new LinkedList();
    protected LayoutInflater inflater;
    private boolean isLoadingData;
    protected ListView listView;
    private View progressView;

    /* access modifiers changed from: protected */
    public abstract View doGetView(int i, View view, ViewGroup viewGroup);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.ListView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public ListAdapterWithProgress(ListActivity activity, int progressDrawableResourceId) {
        this.context = activity;
        this.listView = activity.getListView();
        this.progressView = activity.getLayoutInflater().inflate(progressDrawableResourceId, (ViewGroup) this.listView, false);
        this.inflater = LayoutInflater.from(activity);
    }

    public int getCount() {
        int size = 0;
        if (this.data != null) {
            size = 0 + this.data.size();
        }
        if (this.isLoadingData) {
            return size + 1;
        }
        return size;
    }

    public boolean isEmpty() {
        return getCount() == 0 && !this.isLoadingData;
    }

    public int getItemCount() {
        if (this.data != null) {
            return this.data.size();
        }
        return 0;
    }

    public boolean hasItems() {
        return getItemCount() != 0;
    }

    public Object getItem(int position) {
        if (this.data == null) {
            return null;
        }
        return this.data.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public boolean isEnabled(int position) {
        if (isPositionOfProgressElement(position)) {
            return false;
        }
        return true;
    }

    public boolean areAllItemsEnabled() {
        return false;
    }

    public void setIsLoadingData(boolean isLoadingData2) {
        this.isLoadingData = isLoadingData2;
        notifyDataSetChanged();
    }

    public boolean isLoadingData() {
        return this.isLoadingData;
    }

    public final View getView(int position, View convertView, ViewGroup parent) {
        if (isPositionOfProgressElement(position)) {
            return this.progressView;
        }
        if (convertView == this.progressView) {
            convertView = null;
        }
        return doGetView(position, convertView, parent);
    }

    private boolean isPositionOfProgressElement(int position) {
        return this.isLoadingData && position == this.data.size();
    }

    public List<T> getData() {
        return this.data;
    }

    public void addAll(List<T> items) {
        this.data.addAll(items);
        notifyDataSetChanged();
    }

    public void clear() {
        this.data.clear();
        notifyDataSetChanged();
    }

    public void remove(int position) {
        this.data.remove(position);
        notifyDataSetChanged();
    }
}
