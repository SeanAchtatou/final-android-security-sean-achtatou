package com.github.droidfu.activities;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.KeyEvent;
import com.github.droidfu.dialogs.DialogClickListener;
import com.github.droidfu.exception.ResourceMessageException;
import com.github.droidfu.support.DiagnosticSupport;
import com.github.droidfu.support.IntentSupport;
import java.util.List;

public class BetterActivityHelper {
    public static final String ERROR_DIALOG_TITLE_RESOURCE = "droidfu_error_dialog_title";
    private static final String PROGRESS_DIALOG_MESSAGE_RESOURCE = "droidfu_progress_dialog_message";
    private static final String PROGRESS_DIALOG_TITLE_RESOURCE = "droidfu_progress_dialog_title";

    public static int getWindowFeatures(Activity activity) {
        if (activity.getWindow() == null) {
        }
        return 0;
    }

    public static ProgressDialog createProgressDialog(final Activity activity, int progressDialogTitleId, int progressDialogMsgId) {
        ProgressDialog progressDialog = new ProgressDialog(activity);
        if (progressDialogTitleId > 0) {
            progressDialog.setTitle(progressDialogTitleId);
        } else {
            progressDialog.setTitle(activity.getResources().getIdentifier(PROGRESS_DIALOG_TITLE_RESOURCE, "string", activity.getPackageName()));
        }
        if (progressDialogMsgId > 0) {
            progressDialog.setMessage(activity.getString(progressDialogMsgId));
        } else {
            progressDialog.setMessage(activity.getString(activity.getResources().getIdentifier(PROGRESS_DIALOG_MESSAGE_RESOURCE, "string", activity.getPackageName())));
        }
        progressDialog.setIndeterminate(true);
        progressDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                activity.onKeyDown(keyCode, event);
                return false;
            }
        });
        return progressDialog;
    }

    public static AlertDialog newYesNoDialog(Activity activity, String dialogTitle, String screenMessage, int iconResourceId, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        builder.setPositiveButton(17039379, listener);
        builder.setNegativeButton(17039369, listener);
        builder.setTitle(dialogTitle);
        builder.setMessage(screenMessage);
        builder.setIcon(iconResourceId);
        return builder.create();
    }

    public static AlertDialog newMessageDialog(Activity activity, String dialogTitle, String screenMessage, int iconResourceId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setTitle(dialogTitle);
        builder.setMessage(screenMessage);
        builder.setIcon(iconResourceId);
        return builder.create();
    }

    /* JADX INFO: Multiple debug info for r8v13 java.lang.String: [D('bugEmailSubjectId' int), D('bugReportEmailSubject' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r9v1 java.lang.String: [D('diagnosis' java.lang.String), D('error' java.lang.Exception)] */
    /* JADX INFO: Multiple debug info for r8v14 android.content.Intent: [D('bugReportEmailSubject' java.lang.String), D('intent' android.content.Intent)] */
    /* JADX INFO: Multiple debug info for r7v2 android.app.Activity: [D('activity' android.app.Activity), D('a' android.app.Activity)] */
    public static AlertDialog newErrorHandlerDialog(Activity activity, String dialogTitle, Exception error) {
        String screenMessage;
        if (error instanceof ResourceMessageException) {
            screenMessage = activity.getString(((ResourceMessageException) error).getClientMessageResourceId());
        } else {
            screenMessage = error.getLocalizedMessage();
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(dialogTitle);
        builder.setMessage(screenMessage);
        builder.setIcon(17301543);
        builder.setCancelable(false);
        builder.setPositiveButton(activity.getString(17039370), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        if (IntentSupport.isIntentAvailable(activity, "android.intent.action.SEND", IntentSupport.MIME_TYPE_EMAIL)) {
            String buttonText = activity.getString(activity.getResources().getIdentifier("droidfu_dialog_button_send_error_report", "string", activity.getPackageName()));
            final Intent intent = IntentSupport.newEmailIntent(activity, activity.getString(activity.getResources().getIdentifier("droidfu_error_report_email_address", "string", activity.getPackageName())), activity.getString(activity.getResources().getIdentifier("droidfu_error_report_email_subject", "string", activity.getPackageName())), DiagnosticSupport.createDiagnosis(activity, error));
            final Activity a = activity;
            builder.setNegativeButton(buttonText, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    a.startActivity(intent);
                }
            });
        }
        return builder.create();
    }

    public static <T> Dialog newListDialog(Context context, final List<T> elements, final DialogClickListener<T> listener, final boolean closeOnSelect) {
        String[] entries = new String[elements.size()];
        for (int i = 0; i < elements.size(); i++) {
            entries[i] = elements.get(i).toString();
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setSingleChoiceItems(entries, 0, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (closeOnSelect) {
                    dialog.dismiss();
                }
                listener.onClick(which, elements.get(which));
            }
        });
        return builder.create();
    }

    public static boolean isApplicationBroughtToBackground(Context context) {
        List<ActivityManager.RunningTaskInfo> taskInfo = ((ActivityManager) context.getSystemService("activity")).getRunningTasks(1);
        if (taskInfo.isEmpty() || taskInfo.get(0).topActivity.getPackageName().equals(context.getPackageName())) {
            return false;
        }
        return true;
    }
}
