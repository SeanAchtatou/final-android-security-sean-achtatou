package com.github.droidfu.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListAdapter;
import com.github.droidfu.DroidFuApplication;
import com.github.droidfu.adapters.ListAdapterWithProgress;
import com.github.droidfu.dialogs.DialogClickListener;
import java.util.List;

public class BetterListActivity extends ListActivity implements BetterActivity {
    private static final String IS_BUSY_EXTRA = "is_busy";
    private Intent currentIntent;
    private int progressDialogMsgId;
    private int progressDialogTitleId;
    private boolean wasCreated;
    private boolean wasInterrupted;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.wasCreated = true;
        this.currentIntent = getIntent();
        ((DroidFuApplication) getApplication()).setActiveContext(getClass().getCanonicalName(), this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        ListAdapter adapter = getListAdapter();
        if (adapter instanceof ListAdapterWithProgress) {
            outState.putBoolean(IS_BUSY_EXTRA, ((ListAdapterWithProgress) adapter).isLoadingData());
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        ListAdapter adapter = getListAdapter();
        if (adapter instanceof ListAdapterWithProgress) {
            ((ListAdapterWithProgress) adapter).setIsLoadingData(savedInstanceState.getBoolean(IS_BUSY_EXTRA));
        }
        this.wasInterrupted = true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.wasInterrupted = false;
        this.wasCreated = false;
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.currentIntent = intent;
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        return BetterActivityHelper.createProgressDialog(this, this.progressDialogTitleId, this.progressDialogMsgId);
    }

    public void setProgressDialogTitleId(int progressDialogTitleId2) {
        this.progressDialogTitleId = progressDialogTitleId2;
    }

    public void setProgressDialogMsgId(int progressDialogMsgId2) {
        this.progressDialogMsgId = progressDialogMsgId2;
    }

    public int getWindowFeatures() {
        return BetterActivityHelper.getWindowFeatures(this);
    }

    public boolean isRestoring() {
        return this.wasInterrupted;
    }

    public boolean isResuming() {
        return !this.wasCreated;
    }

    public boolean isLaunching() {
        return !this.wasInterrupted && this.wasCreated;
    }

    public boolean isApplicationBroughtToBackground() {
        return BetterActivityHelper.isApplicationBroughtToBackground(this);
    }

    public Intent getCurrentIntent() {
        return this.currentIntent;
    }

    public boolean isLandscapeMode() {
        return getWindowManager().getDefaultDisplay().getOrientation() == 1;
    }

    public boolean isPortraitMode() {
        return !isLandscapeMode();
    }

    public AlertDialog newYesNoDialog(int titleResourceId, int messageResourceId, DialogInterface.OnClickListener listener) {
        return BetterActivityHelper.newYesNoDialog(this, getString(titleResourceId), getString(messageResourceId), 17301659, listener);
    }

    public AlertDialog newInfoDialog(int titleResourceId, int messageResourceId) {
        return BetterActivityHelper.newMessageDialog(this, getString(titleResourceId), getString(messageResourceId), 17301659);
    }

    public AlertDialog newAlertDialog(int titleResourceId, int messageResourceId) {
        return BetterActivityHelper.newMessageDialog(this, getString(titleResourceId), getString(messageResourceId), 17301543);
    }

    public AlertDialog newErrorHandlerDialog(int titleResourceId, Exception error) {
        return BetterActivityHelper.newErrorHandlerDialog(this, getString(titleResourceId), error);
    }

    public AlertDialog newErrorHandlerDialog(Exception error) {
        return newErrorHandlerDialog(getResources().getIdentifier(BetterActivityHelper.ERROR_DIALOG_TITLE_RESOURCE, "string", getPackageName()), error);
    }

    public <T> Dialog newListDialog(List<T> elements, DialogClickListener<T> listener, boolean closeOnSelect) {
        return BetterActivityHelper.newListDialog(this, elements, listener, closeOnSelect);
    }
}
