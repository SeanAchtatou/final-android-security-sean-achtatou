package com.github.droidfu.support;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.provider.Settings;
import java.io.PrintWriter;
import java.io.StringWriter;

public class DiagnosticSupport {
    public static String getApplicationVersionString(Context context) {
        try {
            return "v" + context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String createDiagnosis(Activity context, Exception error) {
        String networkMode;
        StringBuilder sb = new StringBuilder();
        ContentResolver resolver = context.getContentResolver();
        Configuration sysConfig = new Configuration();
        Settings.System.getConfiguration(resolver, sysConfig);
        sb.append("Application version: " + getApplicationVersionString(context) + "\n");
        sb.append("Device locale: " + sysConfig.locale.toString() + "\n\n");
        sb.append("PHONE SPECS\n");
        sb.append("model: " + Build.MODEL + "\n");
        sb.append("brand: " + Build.BRAND + "\n");
        sb.append("product: " + Build.PRODUCT + "\n");
        sb.append("device: " + Build.DEVICE + "\n\n");
        sb.append("PLATFORM INFO\n");
        sb.append("Android " + Build.VERSION.RELEASE + " " + Build.ID + " (build " + Build.VERSION.INCREMENTAL + ")\n");
        sb.append("build tags: " + Build.TAGS + "\n");
        sb.append("build type: " + Build.TYPE + "\n\n");
        sb.append("SYSTEM SETTINGS\n");
        try {
            if (Settings.Secure.getInt(resolver, "wifi_on") == 0) {
                networkMode = "DATA";
            } else {
                networkMode = "WIFI";
            }
            sb.append("network mode: " + networkMode + "\n");
            sb.append("HTTP proxy: " + Settings.Secure.getString(resolver, "http_proxy") + "\n\n");
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        sb.append("STACK TRACE FOLLOWS\n\n");
        StringWriter stackTrace = new StringWriter();
        error.printStackTrace(new PrintWriter(stackTrace));
        sb.append(stackTrace.toString());
        return sb.toString();
    }
}
