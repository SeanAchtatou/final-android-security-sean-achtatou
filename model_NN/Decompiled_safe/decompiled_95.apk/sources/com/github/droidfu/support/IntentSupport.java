package com.github.droidfu.support;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import java.util.Locale;

public class IntentSupport {
    public static final String MIME_TYPE_EMAIL = "message/rfc822";
    public static final String MIME_TYPE_TEXT = "text/*";

    public static boolean isIntentAvailable(Context context, String action, Uri uri, String mimeType) {
        Intent intent = uri != null ? new Intent(action, uri) : new Intent(action);
        if (mimeType != null) {
            intent.setType(mimeType);
        }
        if (!context.getPackageManager().queryIntentActivities(intent, 65536).isEmpty()) {
            return true;
        }
        return false;
    }

    public static boolean isIntentAvailable(Context context, String action, String mimeType) {
        Intent intent = new Intent(action);
        if (mimeType != null) {
            intent.setType(mimeType);
        }
        return !context.getPackageManager().queryIntentActivities(intent, 65536).isEmpty();
    }

    public static boolean isIntentAvailable(Context context, Intent intent) {
        return !context.getPackageManager().queryIntentActivities(intent, 65536).isEmpty();
    }

    public static Intent newEmailIntent(Context context, String address, String subject, String body) {
        Intent intent = new Intent("android.intent.action.SEND");
        intent.putExtra("android.intent.extra.EMAIL", new String[]{address});
        intent.putExtra("android.intent.extra.TEXT", body);
        intent.putExtra("android.intent.extra.SUBJECT", subject);
        intent.setType(MIME_TYPE_EMAIL);
        return intent;
    }

    public static Intent newShareIntent(Context context, String subject, String message, String chooserDialogTitle) {
        Intent shareIntent = new Intent("android.intent.action.SEND");
        shareIntent.putExtra("android.intent.extra.TEXT", message);
        shareIntent.putExtra("android.intent.extra.SUBJECT", subject);
        shareIntent.setType(MIME_TYPE_TEXT);
        return Intent.createChooser(shareIntent, chooserDialogTitle);
    }

    public static Intent newMapsIntent(String address, String placeTitle) {
        StringBuilder sb = new StringBuilder();
        sb.append("geo:0,0?q=");
        sb.append(Uri.encode(address));
        sb.append(Uri.encode("(" + placeTitle + ")"));
        sb.append("&hl=" + Locale.getDefault().getLanguage());
        return new Intent("android.intent.action.VIEW", Uri.parse(sb.toString()));
    }
}
