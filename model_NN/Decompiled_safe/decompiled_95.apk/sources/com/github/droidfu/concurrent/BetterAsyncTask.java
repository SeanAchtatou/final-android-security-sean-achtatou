package com.github.droidfu.concurrent;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import com.github.droidfu.DroidFuApplication;
import com.github.droidfu.activities.BetterActivity;

public abstract class BetterAsyncTask<ParameterT, ProgressT, ReturnT> extends AsyncTask<ParameterT, ProgressT, ReturnT> {
    private DroidFuApplication appContext;
    private BetterAsyncTaskCallable<ParameterT, ProgressT, ReturnT> callable;
    private String callerId;
    private boolean contextIsDroidFuActivity;
    private int dialogId = 0;
    private Exception error;
    private boolean isTitleProgressEnabled;
    private boolean isTitleProgressIndeterminateEnabled = true;

    /* access modifiers changed from: protected */
    public abstract void after(Context context, ReturnT returnt);

    /* access modifiers changed from: protected */
    public abstract void handleError(Context context, Exception exc);

    public BetterAsyncTask(Context context) {
        if (!(context.getApplicationContext() instanceof DroidFuApplication)) {
            throw new IllegalArgumentException("context bound to this task must be a DroidFu context (DroidFuApplication)");
        }
        this.appContext = (DroidFuApplication) context.getApplicationContext();
        this.callerId = context.getClass().getCanonicalName();
        this.contextIsDroidFuActivity = context instanceof BetterActivity;
        this.appContext.setActiveContext(this.callerId, context);
        if (this.contextIsDroidFuActivity) {
            int windowFeatures = ((BetterActivity) context).getWindowFeatures();
            if (2 == (windowFeatures & 2)) {
                this.isTitleProgressEnabled = true;
            } else if (5 == (windowFeatures & 5)) {
                this.isTitleProgressIndeterminateEnabled = true;
            }
        }
    }

    /* access modifiers changed from: protected */
    public Context getCallingContext() {
        try {
            Context caller = this.appContext.getActiveContext(this.callerId);
            if (caller == null || !this.callerId.equals(caller.getClass().getCanonicalName()) || ((caller instanceof Activity) && ((Activity) caller).isFinishing())) {
                return null;
            }
            return caller;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        Context context = getCallingContext();
        if (context == null) {
            Log.d(BetterAsyncTask.class.getSimpleName(), "skipping pre-exec handler for task " + hashCode() + " (context is null)");
            cancel(true);
            return;
        }
        if (this.contextIsDroidFuActivity) {
            Activity activity = (Activity) context;
            if (this.dialogId > -1) {
                activity.showDialog(this.dialogId);
            }
            if (this.isTitleProgressEnabled) {
                activity.setProgressBarVisibility(true);
            } else if (this.isTitleProgressIndeterminateEnabled) {
                activity.setProgressBarIndeterminateVisibility(true);
            }
        }
        before(context);
    }

    /* access modifiers changed from: protected */
    public void before(Context context) {
    }

    /* access modifiers changed from: protected */
    public final ReturnT doInBackground(ParameterT... params) {
        try {
            return doCheckedInBackground(getCallingContext(), params);
        } catch (Exception e) {
            this.error = e;
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public ReturnT doCheckedInBackground(Context context, ParameterT... parametertArr) throws Exception {
        if (this.callable != null) {
            return this.callable.call(this);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final void onPostExecute(ReturnT result) {
        Context context = getCallingContext();
        if (context == null) {
            Log.d(BetterAsyncTask.class.getSimpleName(), "skipping post-exec handler for task " + hashCode() + " (context is null)");
            return;
        }
        if (this.contextIsDroidFuActivity) {
            Activity activity = (Activity) context;
            if (this.dialogId > -1) {
                activity.removeDialog(this.dialogId);
            }
            if (this.isTitleProgressEnabled) {
                activity.setProgressBarVisibility(false);
            } else if (this.isTitleProgressIndeterminateEnabled) {
                activity.setProgressBarIndeterminateVisibility(false);
            }
        }
        if (failed()) {
            handleError(context, this.error);
        } else {
            after(context, result);
        }
    }

    public boolean failed() {
        return this.error != null;
    }

    public void setCallable(BetterAsyncTaskCallable<ParameterT, ProgressT, ReturnT> callable2) {
        this.callable = callable2;
    }

    public void useCustomDialog(int dialogId2) {
        this.dialogId = dialogId2;
    }

    public void disableDialog() {
        this.dialogId = -1;
    }
}
