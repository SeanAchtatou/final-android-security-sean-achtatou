package com.rubycell.moregame.thumb;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.github.droidfu.imageloader.ImageLoader;
import com.github.droidfu.widgets.WebImageView;
import com.rubycell.moregame.Utility.Common;
import us.colormefree.aanimal.R;

public class QuitActivity2 extends Activity {
    private static LayoutInflater inflater = null;
    private WebImageView imageGame1;
    private WebImageView imageGame2;
    private ImageLoader imageLoader;
    private WebImageView imgIcon;
    private Button mBtInstall;
    private Button mBtMore;
    private Button mBtQuit;
    private TextView nameGame;

    public static class ViewHolder {
        public Button button;
        public ImageView icon;
        public ImageView thumb1;
        public ImageView thumb2;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(1);
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.moregame_main_quit2);
        this.nameGame = (TextView) findViewById(R.id.namegame);
        this.imgIcon = (WebImageView) findViewById(R.id.img_icon);
        this.imageGame1 = (WebImageView) findViewById(R.id.imageGame1);
        this.imageGame2 = (WebImageView) findViewById(R.id.imageGame2);
        this.imgIcon.setImageUrl("http://cdn1.iconfinder.com/data/icons/CS5/32/PS_AppIcon%202_file_document.png");
        this.imageGame1.setImageUrl("http://fs01.androidpit.info/ass/x89/1645489-1298007133040-160x240.jpg");
        this.imageGame2.setImageUrl("http://farm5.static.flickr.com/4029/4628674337_b22a29acee_m.jpg");
        this.imgIcon.loadImage();
        this.imageGame1.loadImage();
        this.imageGame2.loadImage();
        this.mBtQuit = (Button) findViewById(R.id.bt_quit);
        this.mBtQuit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                QuitActivity2.this.setResult(Common.MOREGAME_LIST_FINISHED);
                QuitActivity2.this.finish();
            }
        });
        this.mBtInstall = (Button) findViewById(R.id.bt_install);
        this.mBtMore = (Button) findViewById(R.id.bt_more);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }
}
