package com.rubycell.moregame.Utility;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.widget.ImageView;
import java.lang.ref.SoftReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ConcurrentHashMap;

public class WebImageView extends ImageView {
    static final ImageListener defaultListener = new ImageListener() {
        public void onImageLoaded(WebImageView im, Bitmap bm, String url) {
            im.setImageBitmap(bm);
        }
    };
    static final ConcurrentHashMap<String, SoftReference<Bitmap>> imageCache = new ConcurrentHashMap<>();
    boolean cache = false;
    ImageListener listener = null;

    public interface ImageListener {
        void onImageLoaded(WebImageView webImageView, Bitmap bitmap, String str);
    }

    public static ConcurrentHashMap<String, SoftReference<Bitmap>> getImageCache() {
        return imageCache;
    }

    public WebImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        _init();
    }

    public WebImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        _init();
    }

    public WebImageView(Context context) {
        super(context);
        _init();
    }

    public void setCache(boolean cache2) {
        this.cache = cache2;
    }

    public void setImageListener(ImageListener listener2) {
        this.listener = listener2;
    }

    public void setImageFromURL(String imageUrl) {
        if (this.cache && imageCache.contains(imageUrl)) {
            SoftReference<Bitmap> ref = imageCache.get(imageUrl);
            if (ref != null) {
                setImageBitmap((Bitmap) ref.get());
                return;
            }
            imageCache.remove(imageUrl);
        }
        try {
            final URL url = new URL(imageUrl);
            new Thread() {
                /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v5, resolved type: java.lang.Object} */
                /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: java.io.InputStream} */
                /* JADX WARNING: Multi-variable type inference failed */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public void run() {
                    /*
                        r7 = this;
                        r3 = 0
                        java.net.URL r4 = r1     // Catch:{ IOException -> 0x0025 }
                        java.lang.Object r4 = r4.getContent()     // Catch:{ IOException -> 0x0025 }
                        r0 = r4
                        java.io.InputStream r0 = (java.io.InputStream) r0     // Catch:{ IOException -> 0x0025 }
                        r3 = r0
                    L_0x000b:
                        r2 = r3
                        android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeStream(r2)
                        com.rubycell.moregame.Utility.WebImageView r4 = com.rubycell.moregame.Utility.WebImageView.this
                        android.os.Handler r4 = r4.getHandler()
                        if (r4 == 0) goto L_0x0024
                        com.rubycell.moregame.Utility.WebImageView r4 = com.rubycell.moregame.Utility.WebImageView.this
                        com.rubycell.moregame.Utility.WebImageView$2$1 r5 = new com.rubycell.moregame.Utility.WebImageView$2$1
                        java.net.URL r6 = r1
                        r5.<init>(r6, r1)
                        r4.post(r5)
                    L_0x0024:
                        return
                    L_0x0025:
                        r4 = move-exception
                        goto L_0x000b
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.rubycell.moregame.Utility.WebImageView.AnonymousClass2.run():void");
                }
            }.start();
        } catch (MalformedURLException e) {
        }
    }

    private void _init() {
    }
}
