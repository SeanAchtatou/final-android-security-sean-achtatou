package com.rubycell.moregame.Utility;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

public class Common {
    public static final String DISABLE_CLOSE_APP_BUTTON_IN_GAME = "DISABLE_CLOSE_APP_BUTTON_IN_GAME";
    public static final int MOREGAME_LIST_DISMISS = 2000;
    public static final int MOREGAME_LIST_FINISHED = 1000;
    public static final String MORE_GAME_LIST_URL = "MORE_GAME_LIST_URL";

    public static void openAndSubmitSearchQueryToMarket(Activity activity, String string) {
        activity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=" + string)));
    }

    public static void openAppInAndroidMarket(Activity activity, String pPackageName) {
        activity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + pPackageName)));
    }

    public static void openCustomWebPageInBrowser(Activity activity, String url) {
        activity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
    }
}
