package com.rubycell.moregame.list;

import java.util.ArrayList;
import java.util.List;

public class ParsedDataSet {
    private List<String> extractedAppName = new ArrayList();
    private List<String> extractedDownloadURI = new ArrayList();
    private List<String> extractedIconURL = new ArrayList();
    private List<String> extractedPakageName = new ArrayList();
    private int extractedratedStar = 0;

    public String[] getExtractedIconURLStrings() {
        String[] strArray = new String[this.extractedIconURL.size()];
        this.extractedIconURL.toArray(strArray);
        return strArray;
    }

    public String[] getExtractedAppNameStrings() {
        String[] strArray = new String[this.extractedAppName.size()];
        this.extractedAppName.toArray(strArray);
        return strArray;
    }

    public String[] getExtractedDownloadURIStrings() {
        String[] strArray = new String[this.extractedDownloadURI.size()];
        this.extractedDownloadURI.toArray(strArray);
        return strArray;
    }

    public String[] getExtractedPackageNameStrings() {
        String[] strArray = new String[this.extractedPakageName.size()];
        this.extractedPakageName.toArray(strArray);
        return strArray;
    }

    public void setExtractedInfo(String pAppName, String pIconURL, String pPakageName, String pDownloadURI) {
        this.extractedAppName.add(pAppName);
        this.extractedIconURL.add(pIconURL);
        this.extractedPakageName.add(pPakageName);
        this.extractedDownloadURI.add(pDownloadURI);
    }

    public int getExtractedInt() {
        return this.extractedratedStar;
    }

    public void setExtractedInt(int extractedInt) {
        this.extractedratedStar = extractedInt;
    }
}
