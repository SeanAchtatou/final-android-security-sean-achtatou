package com.rubycell.moregame.list;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.rubycell.moregame.Utility.Common;
import java.net.URL;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import us.colormefree.aanimal.R;

public class MoreGameList extends Activity {
    Activity activity;
    LazyAdapter adapter;
    String[] downloadURISet = new String[0];
    String[] gamenameSet = new String[0];
    String[] iconURLSet = new String[0];
    ListView list;
    public View.OnClickListener listenerbuttonAllGames = new View.OnClickListener() {
        public void onClick(View arg0) {
            Common.openCustomWebPageInBrowser(MoreGameList.this, "http://kidapps.us");
        }
    };
    public View.OnClickListener listenerbuttonCancel = new View.OnClickListener() {
        public void onClick(View arg0) {
            MoreGameList.this.setResult(Common.MOREGAME_LIST_DISMISS);
            MoreGameList.this.finish();
        }
    };
    public View.OnClickListener listenerbuttonClose = new View.OnClickListener() {
        public void onClick(View arg0) {
            MoreGameList.this.setResult(Common.MOREGAME_LIST_FINISHED);
            MoreGameList.this.finish();
        }
    };
    String[] packagenameSet = new String[0];

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.moregame_moregamelayout);
        final Handler uiThreadCallback = new Handler();
        final Runnable runInUIThread = new Runnable() {
            public void run() {
                MoreGameList.this._showInUI();
            }
        };
        new Thread() {
            public void run() {
                MoreGameList.this._doInBackgroundPost();
                uiThreadCallback.post(runInUIThread);
            }
        }.start();
        Button buttonClose = (Button) findViewById(R.id.buttonClose);
        buttonClose.setOnClickListener(this.listenerbuttonClose);
        Button buttonCancel = (Button) findViewById(R.id.buttonCancel);
        buttonCancel.setOnClickListener(this.listenerbuttonCancel);
        ((Button) findViewById(R.id.buttonAllGames)).setOnClickListener(this.listenerbuttonAllGames);
        TextView quittextview = (TextView) findViewById(R.id.textView1);
        Bundle b = getIntent().getExtras();
        Log.e("", "------khanh--------" + b.getString(Common.MORE_GAME_LIST_URL));
        if (b.getBoolean(Common.DISABLE_CLOSE_APP_BUTTON_IN_GAME)) {
            buttonClose.setVisibility(8);
            buttonCancel.setText("Close");
            quittextview.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void _doInBackgroundPost() {
        URL url;
        try {
            Log.i("", "BEGIN ------ " + System.currentTimeMillis());
            try {
                Bundle b = getIntent().getExtras();
                if (b.getString(Common.MORE_GAME_LIST_URL) == null || !b.getString(Common.MORE_GAME_LIST_URL).contains("http://")) {
                    url = new URL("http://tabletgames.mobi/apis/moregame/moregamelist.php?category=pub:RubyCell");
                } else {
                    url = new URL(b.getString(Common.MORE_GAME_LIST_URL));
                }
                XMLReader xr = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
                XMLHandler myExampleHandler = new XMLHandler();
                xr.setContentHandler(myExampleHandler);
                xr.parse(new InputSource(url.openStream()));
                ParsedDataSet parsedDataSet = myExampleHandler.getParsedData();
                this.iconURLSet = parsedDataSet.getExtractedIconURLStrings();
                this.gamenameSet = parsedDataSet.getExtractedAppNameStrings();
                this.packagenameSet = parsedDataSet.getExtractedPackageNameStrings();
                this.downloadURISet = parsedDataSet.getExtractedDownloadURIStrings();
                Log.e("", "Error error downloadURISet.size() = " + this.packagenameSet.length);
            } catch (Exception e) {
                Log.e("", "More game ads controller xml query error", e);
            } catch (Error e2) {
                e2.printStackTrace();
            }
            Log.i("", "END ------ " + System.currentTimeMillis());
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void _showInUI() {
        if (this.iconURLSet.length > 0 && this.iconURLSet.length == this.packagenameSet.length && this.packagenameSet.length == this.gamenameSet.length) {
            this.list = (ListView) findViewById(R.id.list);
            this.adapter = new LazyAdapter(this, this.iconURLSet, this.packagenameSet, this.gamenameSet, this.downloadURISet);
            this.list.setAdapter((ListAdapter) this.adapter);
        }
    }

    public void onDestroy() {
        if (this.adapter != null) {
            this.adapter.imageLoader.stopThread();
        }
        if (this.list != null) {
            this.list.setAdapter((ListAdapter) null);
        }
        super.onDestroy();
    }
}
