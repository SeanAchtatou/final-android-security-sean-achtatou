package com.rubycell.moregame.list;

public class ListItemData {
    String applicationName;
    String iconURL;
    String pakageName;

    public String getIconURL() {
        return this.iconURL;
    }

    public void setIconURL(String iconURL2) {
        this.iconURL = iconURL2;
    }

    public String getApplicationName() {
        return this.applicationName;
    }

    public void setApplicationName(String applicationName2) {
        this.applicationName = applicationName2;
    }

    public String getPakageName() {
        return this.pakageName;
    }

    public void setPakageName(String pakageName2) {
        this.pakageName = pakageName2;
    }

    public ListItemData(String pIconURL, String pApplicationName, String pPakageName) {
        this.iconURL = pIconURL;
        this.applicationName = pApplicationName;
        this.pakageName = pPakageName;
    }
}
