package com.rubycell.moregame.list;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.rubycell.moregame.Utility.Common;
import us.colormefree.aanimal.R;

public class LazyAdapter extends BaseAdapter {
    private static LayoutInflater inflater = null;
    /* access modifiers changed from: private */
    public Activity activity;
    public ImageLoader imageLoader = new ImageLoader(this.activity.getApplicationContext());
    private String[] listItemAppNameData;
    private String[] listItemIconURLData;
    /* access modifiers changed from: private */
    public String[] listItemPakageNameData;
    private String[] listItemURIData;

    public static class ViewHolder {
        public Button button;
        public ImageView image;
        public TextView text;
    }

    public LazyAdapter(Activity a, String[] pListItemData, String[] pListItemPakageNameData, String[] pListItemAppNameData, String[] plistItemURIData) {
        this.activity = a;
        this.listItemIconURLData = pListItemData;
        this.listItemPakageNameData = pListItemPakageNameData;
        this.listItemAppNameData = pListItemAppNameData;
        this.listItemURIData = plistItemURIData;
        inflater = (LayoutInflater) this.activity.getSystemService("layout_inflater");
    }

    public int getCount() {
        return this.listItemIconURLData.length;
    }

    public Object getItem(int position) {
        return Integer.valueOf(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View vi = convertView;
        if (convertView == null) {
            vi = inflater.inflate((int) R.layout.moregame_item, (ViewGroup) null);
            holder = new ViewHolder();
            holder.text = (TextView) vi.findViewById(R.id.text);
            holder.button = (Button) vi.findViewById(R.id.button);
            holder.image = (ImageView) vi.findViewById(R.id.image);
            vi.setTag(holder);
        } else {
            holder = (ViewHolder) vi.getTag();
        }
        final String marketURL = " http://market.android.com/details?id=" + this.listItemPakageNameData[position];
        final String URILink = this.listItemURIData[position] == null ? "" : this.listItemURIData[position];
        holder.image.setTag(this.listItemIconURLData[position]);
        holder.text.setText(this.listItemAppNameData[position]);
        holder.image.setClickable(true);
        holder.image.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (URILink.contains("http://")) {
                    try {
                        Common.openCustomWebPageInBrowser(LazyAdapter.this.activity, URILink);
                    } catch (Exception e) {
                        try {
                            Common.openAppInAndroidMarket(LazyAdapter.this.activity, LazyAdapter.this.listItemPakageNameData[position]);
                        } catch (Exception e2) {
                            Common.openCustomWebPageInBrowser(LazyAdapter.this.activity, marketURL);
                        }
                    }
                } else {
                    try {
                        Common.openAppInAndroidMarket(LazyAdapter.this.activity, LazyAdapter.this.listItemPakageNameData[position]);
                    } catch (Exception e3) {
                        Common.openCustomWebPageInBrowser(LazyAdapter.this.activity, marketURL);
                    }
                }
            }
        });
        holder.text.setClickable(true);
        holder.text.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (URILink.contains("http://")) {
                    try {
                        Common.openCustomWebPageInBrowser(LazyAdapter.this.activity, URILink);
                    } catch (Exception e) {
                        try {
                            Common.openAppInAndroidMarket(LazyAdapter.this.activity, LazyAdapter.this.listItemPakageNameData[position]);
                        } catch (Exception e2) {
                            Common.openCustomWebPageInBrowser(LazyAdapter.this.activity, marketURL);
                        }
                    }
                } else {
                    try {
                        Log.i("", "Try to open webpage");
                        Common.openCustomWebPageInBrowser(LazyAdapter.this.activity, marketURL);
                    } catch (Exception e3) {
                        Log.e("", " Cannot open webpage, fallback to backup solution");
                        Common.openAppInAndroidMarket(LazyAdapter.this.activity, LazyAdapter.this.listItemPakageNameData[position]);
                    }
                }
            }
        });
        holder.button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (URILink.contains("http://")) {
                    try {
                        Common.openCustomWebPageInBrowser(LazyAdapter.this.activity, URILink);
                    } catch (Exception e) {
                        try {
                            Common.openAppInAndroidMarket(LazyAdapter.this.activity, LazyAdapter.this.listItemPakageNameData[position]);
                        } catch (Exception e2) {
                            Common.openCustomWebPageInBrowser(LazyAdapter.this.activity, marketURL);
                        }
                    }
                } else {
                    try {
                        Common.openCustomWebPageInBrowser(LazyAdapter.this.activity, marketURL);
                    } catch (Exception e3) {
                        Common.openAppInAndroidMarket(LazyAdapter.this.activity, LazyAdapter.this.listItemPakageNameData[position]);
                    }
                }
            }
        });
        this.imageLoader.DisplayImage(this.listItemIconURLData[position], this.activity, holder.image);
        return vi;
    }
}
