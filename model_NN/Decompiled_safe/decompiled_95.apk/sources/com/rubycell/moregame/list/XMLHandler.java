package com.rubycell.moregame.list;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class XMLHandler extends DefaultHandler {
    private boolean in_gamedata = false;
    private boolean in_gamename = false;
    private boolean in_iconurl = false;
    private ParsedDataSet myParsedDataSet = new ParsedDataSet();

    public ParsedDataSet getParsedData() {
        return this.myParsedDataSet;
    }

    public void startDocument() throws SAXException {
        this.myParsedDataSet = new ParsedDataSet();
    }

    public void endDocument() throws SAXException {
    }

    public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
        if (localName.equals("gamedata")) {
            this.in_gamedata = true;
        } else if (localName.equals("name")) {
            this.in_gamename = true;
        } else if (localName.equals("iconurl")) {
            this.in_iconurl = true;
        } else if (localName.equals("gameinfo")) {
            this.myParsedDataSet.setExtractedInt(Integer.parseInt(atts.getValue("ratedStar")));
            String gameName = atts.getValue("gameName");
            String iconURL = atts.getValue("iconURL");
            String packageName = atts.getValue("packageName");
            String downloadURI = " ";
            try {
                downloadURI = atts.getValue("downloadURI");
            } catch (Exception e) {
            }
            this.myParsedDataSet.setExtractedInfo(gameName, iconURL, packageName, downloadURI);
        }
    }

    public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
        if (localName.equals("gamedata")) {
            this.in_gamedata = false;
        } else if (localName.equals("game")) {
            this.in_gamename = false;
        } else if (localName.equals("iconurl")) {
            this.in_iconurl = false;
        } else {
            localName.equals("gameinfo");
        }
    }

    public void characters(char[] ch, int start, int length) {
    }
}
