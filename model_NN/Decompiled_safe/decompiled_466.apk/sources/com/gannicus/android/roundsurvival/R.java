package com.gannicus.android.roundsurvival;

public final class R {

    public static final class anim {
        public static final int fade = 2130968576;
        public static final int layout_top_to_bottom_slide = 2130968577;
        public static final int slide_top_to_bottom = 2130968578;
    }

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771971;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771972;
        public static final int secondaryTextColor = 2130771970;
    }

    public static final class color {
        public static final int bg = 2131099648;
        public static final int bg2 = 2131099649;
        public static final int black = 2131099651;
        public static final int text_high_score = 2131099652;
        public static final int text_white = 2131099650;
        public static final int white = 2131099653;
    }

    public static final class drawable {
        public static final int car_anim01 = 2130837504;
        public static final int car_anim02 = 2130837505;
        public static final int car_anim03 = 2130837506;
        public static final int car_anim04 = 2130837507;
        public static final int car_anim05 = 2130837508;
        public static final int car_anim06 = 2130837509;
        public static final int car_anim07 = 2130837510;
        public static final int car_anim08 = 2130837511;
        public static final int car_anim09 = 2130837512;
        public static final int car_anim10 = 2130837513;
        public static final int car_anim11 = 2130837514;
        public static final int car_anim12 = 2130837515;
        public static final int car_anim13 = 2130837516;
        public static final int car_anim14 = 2130837517;
        public static final int car_anim15 = 2130837518;
        public static final int car_anim16 = 2130837519;
        public static final int car_anim17 = 2130837520;
        public static final int car_tiny = 2130837521;
        public static final int car_tiny_mid_speed = 2130837522;
        public static final int car_tiny_top_speed = 2130837523;
        public static final int game_btn = 2130837524;
        public static final int game_btn_down = 2130837525;
        public static final int game_btn_up = 2130837526;
        public static final int game_pad = 2130837527;
        public static final int game_pad_down = 2130837528;
        public static final int icon = 2130837529;
        public static final int loading = 2130837530;
        public static final int login_logo = 2130837531;
        public static final int sound_on = 2130837532;
    }

    public static final class id {
        public static final int about = 2131296266;
        public static final int ad = 2131296277;
        public static final int ad_panel = 2131296276;
        public static final int close_btn = 2131296261;
        public static final int exit = 2131296267;
        public static final int exit_to_main = 2131296275;
        public static final int game_panel = 2131296263;
        public static final int high_score_text = 2131296271;
        public static final int high_scores = 2131296265;
        public static final int info = 2131296272;
        public static final int loading_panel = 2131296269;
        public static final int logo = 2131296270;
        public static final int name_eidt = 2131296258;
        public static final int name_input_panel = 2131296257;
        public static final int name_text = 2131296262;
        public static final int new_round_high_scores = 2131296274;
        public static final int restart = 2131296273;
        public static final int score_text = 2131296259;
        public static final int scores_list = 2131296256;
        public static final int sound_swich_btn = 2131296268;
        public static final int start = 2131296264;
        public static final int upload_btn = 2131296260;
    }

    public static final class layout {
        public static final int highscore = 2130903040;
        public static final int highscore_item = 2130903041;
        public static final int main = 2130903042;
    }

    public static final class raw {
        public static final int explosion = 2131034112;
        public static final int game_music = 2131034113;
        public static final int go_cheer = 2131034114;
        public static final int menu_music = 2131034115;
    }

    public static final class string {
        public static final int app_name = 2131165185;
        public static final int developer_name = 2131165184;
        public static final int high_score = 2131165189;
        public static final int how_to_play = 2131165190;
        public static final int pause_panel_info = 2131165187;
        public static final int pause_panel_title = 2131165186;
        public static final int score = 2131165188;
    }

    public static final class style {
        public static final int GameBtn = 2131230720;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
    }
}
