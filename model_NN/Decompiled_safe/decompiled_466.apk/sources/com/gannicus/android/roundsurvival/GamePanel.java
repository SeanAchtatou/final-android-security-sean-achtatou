package com.gannicus.android.roundsurvival;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Process;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.gannicus.android.game.api.GameResources;
import com.gannicus.android.game.api.GameState;
import com.gannicus.android.game.api.GameUtils;
import com.gannicus.android.game.api.LinearMechanicalObj;
import com.gannicus.android.game.api.StaticObj;

public class GamePanel extends SurfaceView {
    private static final String LOG_TAG = "GamePanel";
    /* access modifiers changed from: private */
    public static int TICK_DELAY = 0;
    /* access modifiers changed from: private */
    public final Object GAME_LOCK = new Object();
    private SurfaceHolder.Callback callback;
    private LinearMechanicalObj car = new LinearMechanicalObj();
    private float carOffset;
    private int fps;
    private final char[] fpsChars;
    private StaticObj gamePad = new StaticObj();
    protected GameState gameState = new GameState();
    private Thread gameThread;
    public Handler handler;
    private boolean hideFPS = true;
    /* access modifiers changed from: private */
    public SurfaceHolder holder;
    /* access modifiers changed from: private */
    public boolean isSurfaceCreated = false;
    final float scale = getContext().getResources().getDisplayMetrics().density;
    private int score = 0;
    private final char[] scoreBuf = new char[7];
    /* access modifiers changed from: private */
    public int surfaceHeight = 0;
    /* access modifiers changed from: private */
    public int surfaceWidth = 0;
    /* access modifiers changed from: private */
    public Track track = new Track();

    public GamePanel(Context context, AttributeSet attrs) {
        super(context, attrs);
        char[] cArr = new char[7];
        cArr[0] = 'F';
        cArr[1] = 'P';
        cArr[2] = 'S';
        cArr[3] = ':';
        cArr[4] = ' ';
        this.fpsChars = cArr;
        this.callback = new SurfaceHolder.Callback() {
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                GamePanel.this.surfaceWidth = width;
                GamePanel.this.surfaceHeight = height;
                GamePanel.this.track.init(GamePanel.this.surfaceWidth, GamePanel.this.surfaceHeight);
            }

            public void surfaceCreated(SurfaceHolder holder) {
                GamePanel.this.isSurfaceCreated = true;
            }

            public void surfaceDestroyed(SurfaceHolder holder) {
                GamePanel.this.isSurfaceCreated = false;
                if (GamePanel.this.gameState.state == 2 && !GamePanel.this.gameState.isPaused) {
                    GamePanel.this.gameState.isPaused = true;
                    GamePanel.this.showAd(GamePanel.this.gameState.isPaused);
                }
            }
        };
        this.gameThread = new Thread() {
            public void run() {
                Process.setThreadPriority(-8);
                while (!GamePanel.this.isSurfaceCreated && GamePanel.this.gameState.isRunning) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                    }
                }
                long lastTickMs = System.currentTimeMillis();
                while (GamePanel.this.gameState.isRunning) {
                    while (GamePanel.this.gameState.isPaused && GamePanel.this.gameState.isRunning) {
                        try {
                            if (GamePanel.this.isSurfaceCreated) {
                                GamePanel.this.drawPauseLayer();
                            }
                            Thread.sleep(100);
                        } catch (InterruptedException e2) {
                        }
                        lastTickMs = System.currentTimeMillis();
                    }
                    long now = System.currentTimeMillis();
                    int durationMs = (int) (now - lastTickMs);
                    if (durationMs >= GamePanel.TICK_DELAY) {
                        lastTickMs = now;
                        Canvas canvas = GamePanel.this.holder.lockCanvas();
                        try {
                            synchronized (GamePanel.this.GAME_LOCK) {
                                if (GamePanel.this.isSurfaceCreated && canvas != null) {
                                    GamePanel.this.update(durationMs);
                                    GamePanel.this.doDraw(canvas);
                                }
                            }
                            if (canvas != null) {
                                GamePanel.this.holder.unlockCanvasAndPost(canvas);
                            }
                        } catch (Throwable th) {
                            try {
                                throw new RuntimeException(th);
                            } catch (Throwable th2) {
                                if (canvas != null) {
                                    GamePanel.this.holder.unlockCanvasAndPost(canvas);
                                }
                                throw th2;
                            }
                        }
                    } else {
                        try {
                            Thread.sleep((long) (GamePanel.TICK_DELAY - durationMs));
                        } catch (InterruptedException e3) {
                        }
                    }
                }
            }
        };
        this.holder = getHolder();
        this.holder.addCallback(this.callback);
        setFocusableInTouchMode(true);
        this.gameState.state = 0;
        this.gameState.isRunning = true;
    }

    public void init() {
        this.car.scale = this.scale;
        this.track.scale = this.scale;
        this.car.bgNomalSpeed = GameResources.bgNomalSpeed;
        this.car.bgMidSpeed = GameResources.bgMidSpeed;
        this.car.bgTopSpeed = GameResources.bgTopSpeed;
        this.car.frames = GameResources.carAnim;
        this.car.bounds = new Rect(0, 0, (int) (this.scale * 36.0f), (int) (this.scale * 36.0f));
        this.car.isFixed = true;
        this.car.speed = 0.0f;
        this.car.time = 0;
        this.car.acceleration = 0;
        this.carOffset = 8.0f * this.scale;
        this.score = 0;
        this.gamePad.bg = GameResources.bgGamePad;
        this.gamePad.bounds = new Rect(0, GameResources.screenHeight - ((int) (90.0f * this.scale)), (int) (200.0f * this.scale), GameResources.screenHeight);
    }

    public void startGameThread() {
        if (this.gameThread != null) {
            this.gameThread.start();
        }
    }

    /* access modifiers changed from: private */
    public final void update(int durationMs) {
        if (this.gameState.state == 2) {
            this.car.updateStateIn(durationMs);
            updateScore(durationMs);
            updateCollision();
        }
        updateFPS(durationMs);
        this.car.updateAnim(durationMs);
    }

    private final void updateCollision() {
        if (this.gameState.state == 2) {
            int x = this.car.bounds.centerX();
            int y = this.car.bounds.centerY();
            if (!this.track.isContained(((float) x) - this.carOffset, ((float) y) - this.carOffset) || !this.track.isContained(((float) x) - this.carOffset, ((float) y) + this.carOffset) || !this.track.isContained(((float) x) + this.carOffset, ((float) y) + this.carOffset) || !this.track.isContained(((float) x) + this.carOffset, ((float) y) + this.carOffset)) {
                this.car.setAnimActive(true);
                this.gameState.state = 3;
                Message m = this.handler.obtainMessage();
                Bundle data = new Bundle();
                data.putBoolean("isLose", true);
                data.putInt("score", this.score);
                m.setData(data);
                this.handler.sendMessage(m);
            }
        }
    }

    private final void updateFPS(int durationMs) {
        if (!this.hideFPS) {
            if (durationMs == 0) {
                durationMs++;
            }
            this.fps = 1000 / durationMs;
            this.fps = this.fps >= 100 ? 99 : this.fps;
        }
    }

    private final void updateScore(int durationMs) {
        this.score += durationMs;
    }

    /* access modifiers changed from: private */
    public final void doDraw(Canvas canvas) {
        this.track.draw(canvas);
        if (this.gameState.state != 0) {
            this.car.draw(canvas);
            drawFPS(canvas);
            drawScore(canvas);
            if (this.gameState.state == 2) {
                this.gamePad.draw(canvas);
            }
        }
    }

    private final void drawScore(Canvas canvas) {
        GameUtils.getTimeChars(this.score, this.scoreBuf);
        canvas.drawText(this.scoreBuf, 0, this.scoreBuf.length, (float) ((this.surfaceWidth / 2) - 80), (float) ((this.surfaceHeight / 2) + 30), GameResources.scorePaint);
    }

    /* access modifiers changed from: private */
    public final void drawPauseLayer() {
        Canvas canvas = this.holder.lockCanvas();
        canvas.drawColor(-16777216);
        Paint paint = new Paint();
        paint.setColor(-16776961);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTypeface(Typeface.create(Typeface.SANS_SERIF, 1));
        canvas.drawText(getResources().getString(R.string.pause_panel_title), (float) (this.surfaceWidth / 2), (float) (this.surfaceHeight / 2), paint);
        canvas.drawText(getResources().getString(R.string.pause_panel_info), (float) (this.surfaceWidth / 2), ((float) (this.surfaceHeight / 2)) + (30.0f * this.scale), paint);
        this.holder.unlockCanvasAndPost(canvas);
    }

    private final void drawFPS(Canvas canvas) {
        if (!this.hideFPS) {
            GameUtils.getChars(this.fps, this.fpsChars.length, this.fpsChars);
            Canvas canvas2 = canvas;
            canvas2.drawText(this.fpsChars, 0, this.fpsChars.length, ((float) this.surfaceWidth) - (50.0f * this.scale), 20.0f * this.scale, GameResources.fpsPaint);
        }
    }

    public void go() {
        synchronized (this.GAME_LOCK) {
            this.gameState.isPaused = false;
            this.gameState.isRunning = true;
            this.gamePad.bg = GameResources.bgGamePad;
            this.car.setAnimActive(false);
            this.car.resetAnim();
            this.car.moveTo(240.0f * this.scale, 270.0f * this.scale);
            this.car.isFixed = false;
            this.car.speed = 30.0f;
            this.car.acceleration = 5;
            this.car.angle = 0.0f;
            this.car.clearTails();
            this.score = 0;
            for (int i = 0; i != this.scoreBuf.length; i++) {
                this.scoreBuf[i] = 0;
            }
            this.gameState.state = 2;
        }
    }

    /* access modifiers changed from: private */
    public void showAd(boolean isShow) {
        if (this.handler != null) {
            Message m = this.handler.obtainMessage();
            Bundle data = new Bundle();
            if (isShow) {
                data.putBoolean("showAd", true);
            } else {
                data.putBoolean("offAd", true);
            }
            m.setData(data);
            this.handler.sendMessage(m);
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (this.gameState.state != 2) {
            return false;
        }
        if (this.gameState.isPaused) {
            this.gameState.isPaused = false;
            showAd(this.gameState.isPaused);
            return super.onTouchEvent(event);
        }
        int sign = getSign(event.getX(), event.getY());
        if (sign == 0) {
            return super.onTouchEvent(event);
        }
        switch (event.getAction()) {
            case 0:
                this.gamePad.bg = GameResources.bgGamePadDown;
                this.car.angle -= (float) sign;
                break;
            case 1:
            case 3:
            case 4:
                this.gamePad.bg = GameResources.bgGamePad;
                this.car.angle -= (float) sign;
                break;
            case 2:
                this.gamePad.bg = GameResources.bgGamePadDown;
                float t = (float) (event.getEventTime() - event.getDownTime());
                if (t <= 500.0f) {
                    t = 500.0f;
                }
                if (t >= 4000.0f) {
                    t = 4000.0f;
                }
                this.car.angle -= (((float) (sign * 20)) * t) / 4000.0f;
                break;
            default:
                return super.onTouchEvent(event);
        }
        return true;
    }

    private int getSign(float x, float y) {
        if (this.gamePad.isContained(x, y)) {
            return ((float) this.gamePad.bounds.centerX()) > x ? 1 : -1;
        }
        return 0;
    }

    public void cleanUp() {
        stopGameThread();
        this.car = null;
        this.track = null;
        if (getHolder() != null) {
            getHolder().removeCallback(this.callback);
        }
    }

    private void stopGameThread() {
        if (this.gameThread != null) {
            this.gameState.isRunning = false;
            boolean retry = true;
            while (retry) {
                try {
                    this.gameThread.join();
                    retry = false;
                } catch (InterruptedException e) {
                }
            }
            this.gameThread = null;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 82 && this.gameState.state == 2) {
            this.gameState.isPaused = !this.gameState.isPaused;
            showAd(this.gameState.isPaused);
        }
        return super.onKeyDown(keyCode, event);
    }
}
