package com.gannicus.android.roundsurvival;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import com.gannicus.android.game.api.GameResources;
import com.gannicus.android.game.api.GameUtils;
import com.gannicus.android.game.api.MusicManager;

public class Main extends Activity {
    private static final long LOADING_DELAY = 3000;
    protected static final String LOG_TAG = "Main";
    protected static final int REQUEST_CODE = 3776872;
    private View ad;
    private View adPanel;
    /* access modifiers changed from: private */
    public Button exit;
    /* access modifiers changed from: private */
    public Button exitToMain;
    /* access modifiers changed from: private */
    public GamePanel gamePanel;
    /* access modifiers changed from: private */
    public Button go;
    /* access modifiers changed from: private */
    public Button highScores;
    /* access modifiers changed from: private */
    public TextView high_score_text;
    /* access modifiers changed from: private */
    public TextView info;
    boolean isUploaded;
    private View.OnClickListener listener = new View.OnClickListener() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.start:
                    Main.this.soundSwitch.setVisibility(4);
                    Main.this.go.setVisibility(4);
                    Main.this.highScores.setVisibility(4);
                    Main.this.exit.setVisibility(4);
                    Main.this.showAd(false);
                    Main.this.high_score_text.setVisibility(4);
                    Main.this.gamePanel.go();
                    Toast.makeText(Main.this, "Go!", 0).show();
                    MusicManager.start(Main.this, 1);
                    MusicManager.start(Main.this, 4, false, false);
                    return;
                case R.id.high_scores:
                    Intent i = new Intent(Main.this, HighScoreDialog.class);
                    i.putExtra("score", GameUtils.getScore(Main.this));
                    Main.this.startActivity(i);
                    return;
                case R.id.about:
                case R.id.loading_panel:
                case R.id.logo:
                case R.id.high_score_text:
                case R.id.info:
                default:
                    return;
                case R.id.exit:
                    Main.this.finish();
                    return;
                case R.id.sound_swich_btn:
                    MusicManager.saveMusicIsAllowed(Main.this, Main.this.soundSwitch.isChecked());
                    if (Main.this.soundSwitch.isChecked()) {
                        MusicManager.start(Main.this, 0);
                        return;
                    } else {
                        MusicManager.pause();
                        return;
                    }
                case R.id.restart:
                    Main.this.soundSwitch.setVisibility(4);
                    Main.this.restart.setVisibility(4);
                    Main.this.exitToMain.setVisibility(4);
                    Main.this.newRoundHighScores.setVisibility(4);
                    Main.this.info.setVisibility(4);
                    Main.this.showAd(false);
                    Main.this.high_score_text.setVisibility(4);
                    Main.this.gamePanel.go();
                    MusicManager.start(Main.this, 1);
                    MusicManager.start(Main.this, 4, false, false);
                    return;
                case R.id.new_round_high_scores:
                    Intent i2 = new Intent(Main.this, HighScoreDialog.class);
                    i2.putExtra("score", Main.this.score);
                    i2.putExtra("is_from_new_round", true);
                    i2.putExtra("is_uploaded", Main.this.isUploaded);
                    Main.this.startActivityForResult(i2, Main.REQUEST_CODE);
                    return;
                case R.id.exit_to_main:
                    Main.this.goToMainMenu();
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public Button newRoundHighScores;
    /* access modifiers changed from: private */
    public Button restart;
    long score;
    /* access modifiers changed from: private */
    public ToggleButton soundSwitch;
    private Handler uiThreadHandle = new Handler() {
        public void handleMessage(Message m) {
            Bundle data = m.getData();
            if (data != null) {
                if (data.getBoolean("isWin")) {
                    Main.this.gameWin();
                } else if (data.getBoolean("isLose")) {
                    Main.this.gameOver(data.getInt("score"));
                } else if (data.getBoolean("showAd")) {
                    Main.this.showAd(true);
                } else if (data.getBoolean("offAd")) {
                    Main.this.showAd(false);
                }
            }
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.main);
        long beginLoading = System.currentTimeMillis();
        startAnim();
        GameResources.loading(this);
        this.go = (Button) findViewById(R.id.start);
        this.exit = (Button) findViewById(R.id.exit);
        this.restart = (Button) findViewById(R.id.restart);
        this.exitToMain = (Button) findViewById(R.id.exit_to_main);
        this.soundSwitch = (ToggleButton) findViewById(R.id.sound_swich_btn);
        this.highScores = (Button) findViewById(R.id.high_scores);
        this.newRoundHighScores = (Button) findViewById(R.id.new_round_high_scores);
        this.highScores.setOnClickListener(this.listener);
        this.go.setOnClickListener(this.listener);
        this.exit.setOnClickListener(this.listener);
        this.restart.setOnClickListener(this.listener);
        this.exitToMain.setOnClickListener(this.listener);
        this.soundSwitch.setOnClickListener(this.listener);
        this.newRoundHighScores.setOnClickListener(this.listener);
        this.soundSwitch.setChecked(MusicManager.isBGMusicAllowed(this));
        this.ad = findViewById(R.id.ad);
        this.adPanel = findViewById(R.id.ad_panel);
        this.gamePanel = (GamePanel) findViewById(R.id.game_panel);
        this.gamePanel.handler = this.uiThreadHandle;
        this.info = (TextView) findViewById(R.id.info);
        this.high_score_text = (TextView) findViewById(R.id.high_score_text);
        char[] scoreBuf = new char[7];
        GameUtils.getTimeChars((int) GameUtils.getScore(this), scoreBuf);
        this.high_score_text.setText(String.valueOf(getResources().getString(R.string.high_score)) + new String(scoreBuf).trim() + " seconds");
        MusicManager.start(this, 0);
        this.gamePanel.init();
        long delay = LOADING_DELAY - (System.currentTimeMillis() - beginLoading);
        if (delay <= 0) {
            delay = 0;
        }
        this.uiThreadHandle.postDelayed(new Runnable() {
            public void run() {
                Main.this.startGameEngine();
            }
        }, delay);
    }

    private void startAnim() {
        findViewById(R.id.logo).startAnimation(AnimationUtils.loadAnimation(this, R.anim.fade));
    }

    /* access modifiers changed from: private */
    public void startGameEngine() {
        findViewById(R.id.loading_panel).setVisibility(8);
        this.gamePanel.startGameThread();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.gamePanel.cleanUp();
        MusicManager.release();
        GameResources.release();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        MusicManager.pause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        MusicManager.Resume(this);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || this.gamePanel.gameState.state == 0) {
            return super.onKeyDown(keyCode, event);
        }
        if (!this.gamePanel.gameState.isPaused) {
            goToMainMenu();
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void gameWin() {
    }

    /* access modifiers changed from: private */
    public void showAd(boolean isShow) {
        if (isShow) {
            this.ad.setVisibility(0);
            this.adPanel.setVisibility(0);
            return;
        }
        this.ad.setVisibility(8);
        this.adPanel.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void goToMainMenu() {
        this.restart.setVisibility(4);
        this.newRoundHighScores.setVisibility(4);
        this.exitToMain.setVisibility(4);
        this.info.setVisibility(4);
        this.soundSwitch.setVisibility(0);
        this.go.setVisibility(0);
        this.highScores.setVisibility(0);
        this.exit.setVisibility(0);
        showAd(true);
        this.high_score_text.setVisibility(0);
        this.gamePanel.gameState.state = 0;
        MusicManager.start(this, 0);
    }

    /* access modifiers changed from: private */
    public void gameOver(int score2) {
        this.score = (long) score2;
        this.isUploaded = false;
        char[] scoreBuf = new char[7];
        GameUtils.getTimeChars(score2, scoreBuf);
        String scoreText = new String(scoreBuf).trim();
        this.info.setText(String.valueOf(getResources().getString(R.string.score)) + scoreText + " seconds");
        this.info.setVisibility(0);
        this.restart.setVisibility(0);
        this.exitToMain.setVisibility(0);
        this.newRoundHighScores.setVisibility(0);
        showAd(true);
        this.high_score_text.setVisibility(0);
        if (((long) score2) >= GameUtils.getScore(this)) {
            GameUtils.saveScore((long) score2, this);
            GameUtils.saveUploadedState(false, this);
            this.high_score_text.setText(String.valueOf(getResources().getString(R.string.high_score)) + scoreText + " seconds");
        }
        MusicManager.start(this, 3, false, false);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == -1) {
            this.isUploaded = true;
        }
    }
}
