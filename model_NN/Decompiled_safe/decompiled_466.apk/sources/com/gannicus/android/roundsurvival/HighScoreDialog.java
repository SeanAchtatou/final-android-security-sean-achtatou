package com.gannicus.android.roundsurvival;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.gannicus.android.game.api.AppUtils;
import com.gannicus.android.game.api.Base64;
import com.gannicus.android.game.api.GameUtils;
import com.gannicus.android.game.api.Score;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONObject;

public class HighScoreDialog extends Activity {
    private static final String GET_SCORES_URL = "http://zeddgamehighscoreservice.appspot.com/getscores?game_name=CarSurvival";
    private static final String PUT_SCORE_URL = "http://zeddgamehighscoreservice.appspot.com/putscore?game_name=CarSurvival";
    private Button close;
    /* access modifiers changed from: private */
    public Handler handler = new Handler();
    /* access modifiers changed from: private */
    public boolean isUploaded;
    /* access modifiers changed from: private */
    public EditText nameEdit;
    private View nameInputPanel;
    /* access modifiers changed from: private */
    public boolean newRound;
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            if (v.getId() == R.id.close_btn) {
                HighScoreDialog.this.close();
            } else if (v.getId() == R.id.upload_btn) {
                String name = HighScoreDialog.this.nameEdit.getText().toString();
                if (name == null || name.trim().equals("")) {
                    HighScoreDialog.this.nameEdit.setSelected(true);
                    Toast.makeText(HighScoreDialog.this, "Please input your name!", 0).show();
                    return;
                }
                Score s = new Score();
                s.name = name;
                s.score = HighScoreDialog.this.score;
                HighScoreDialog.this.loadScore(s);
            }
        }
    };
    /* access modifiers changed from: private */
    public long score = 0;
    /* access modifiers changed from: private */
    public ListView scoresList;
    private Button upload;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.highscore);
        Bundle extras = getIntent().getExtras();
        this.score = 0;
        this.newRound = false;
        this.isUploaded = false;
        if (extras != null) {
            this.score = extras.getLong("score");
            this.newRound = extras.getBoolean("is_from_new_round", false);
            this.isUploaded = extras.getBoolean("is_uploaded", false);
        }
        this.scoresList = (ListView) findViewById(R.id.scores_list);
        this.upload = (Button) findViewById(R.id.upload_btn);
        this.upload.setOnClickListener(this.onClickListener);
        this.close = (Button) findViewById(R.id.close_btn);
        this.close.setOnClickListener(this.onClickListener);
        this.nameInputPanel = findViewById(R.id.name_input_panel);
        this.nameEdit = (EditText) findViewById(R.id.name_eidt);
        showUploadPanel(false);
        ((TextView) findViewById(R.id.score_text)).setText(GameUtils.formatTime(this.score));
        loadScore(null);
    }

    /* access modifiers changed from: private */
    public void loadScore(final Score s) {
        final ProgressDialog progressDialog = ProgressDialog.show(this, null, "Loading Online Scores...", true);
        final boolean uploadSession = s != null;
        new Thread() {
            public void run() {
                final ArrayList<Score> scores = uploadSession ? HighScoreDialog.putScore(s) : HighScoreDialog.getScores();
                progressDialog.dismiss();
                if (scores == null) {
                    HighScoreDialog.this.handler.post(new Runnable() {
                        public void run() {
                            Toast.makeText(HighScoreDialog.this, "Loading online scores error, Please try again!", 0).show();
                        }
                    });
                    return;
                }
                Handler access$5 = HighScoreDialog.this.handler;
                final boolean z = uploadSession;
                access$5.post(new Runnable() {
                    public void run() {
                        if (z && (!HighScoreDialog.this.newRound || HighScoreDialog.this.score == GameUtils.getScore(HighScoreDialog.this))) {
                            GameUtils.saveUploadedState(true, HighScoreDialog.this);
                        }
                        if (z) {
                            HighScoreDialog.this.isUploaded = true;
                        }
                        HighScoreDialog.this.showUploadPanel(!z && HighScoreDialog.this.score != 0 && (scores.size() == 0 || HighScoreDialog.this.score > ((Score) scores.get(scores.size() - 1)).score) && ((HighScoreDialog.this.newRound && !HighScoreDialog.this.isUploaded) || (!HighScoreDialog.this.newRound && !GameUtils.isUploaded(HighScoreDialog.this))));
                        HighScoreDialog.this.scoresList.setAdapter((ListAdapter) new HighScoreListAdapter(HighScoreDialog.this, scores));
                        if (z) {
                            HighScoreDialog.this.scoresList.startLayoutAnimation();
                        }
                    }
                });
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public void close() {
        if (this.isUploaded) {
            setResult(-1);
        }
        finish();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        close();
        return false;
    }

    /* access modifiers changed from: private */
    public void showUploadPanel(boolean show) {
        if (show) {
            this.close.setVisibility(8);
            this.upload.setVisibility(0);
            this.nameInputPanel.setVisibility(0);
            this.nameEdit.setSelected(true);
            return;
        }
        this.close.setVisibility(0);
        this.upload.setVisibility(8);
        this.nameInputPanel.setVisibility(8);
    }

    public static final ArrayList<Score> putScore(Score score2) {
        HashMap<String, String> params = new HashMap<>();
        params.put("player_name", score2.name);
        params.put("score", String.valueOf(score2.score));
        return doIt(PUT_SCORE_URL, params);
    }

    /* access modifiers changed from: private */
    public static final ArrayList<Score> getScores() {
        return doIt(GET_SCORES_URL, new HashMap());
    }

    private static final ArrayList<Score> doIt(String url, HashMap<String, String> params) {
        if (params == null) {
            params = new HashMap<>();
        }
        ArrayList<Score> ret = new ArrayList<>();
        try {
            String jsonString = AppUtils.postUrlResponse(url, params, Base64.encode("CarSurvival@3x8fdkj*"));
            Log.d("Main", jsonString);
            JSONArray array = new JSONArray(jsonString);
            int length = array.length();
            for (int i = 0; i != length; i++) {
                JSONObject obj = array.getJSONObject(i);
                Score temp = new Score();
                temp.name = obj.getString("player_name");
                temp.score = obj.getLong("score");
                ret.add(temp);
            }
            return ret;
        } catch (Exception e) {
            return null;
        }
    }
}
