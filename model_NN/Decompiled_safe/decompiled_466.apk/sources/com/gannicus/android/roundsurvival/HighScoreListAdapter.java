package com.gannicus.android.roundsurvival;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.gannicus.android.game.api.GameUtils;
import com.gannicus.android.game.api.Score;
import java.util.ArrayList;

public class HighScoreListAdapter extends BaseAdapter {
    private static final String DEFAULT_NAME = "Anonymous";
    private LayoutInflater inflater;
    private ArrayList<Score> scores;

    public HighScoreListAdapter(Context ctx, ArrayList<Score> scores2) {
        this.inflater = LayoutInflater.from(ctx);
        this.scores = scores2;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View ret = this.inflater.inflate((int) R.layout.highscore_item, (ViewGroup) null);
        TextView nameView = (TextView) ret.findViewById(R.id.name_text);
        TextView scoreView = (TextView) ret.findViewById(R.id.score_text);
        String name = this.scores.get(position).name;
        if (name == null || name.trim().equals("")) {
            name = DEFAULT_NAME;
        }
        nameView.setText(String.valueOf(position + 1) + ". " + name);
        scoreView.setText(formatScore(this.scores.get(position).score));
        return ret;
    }

    public int getCount() {
        return this.scores.size();
    }

    public Object getItem(int position) {
        return this.scores.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    private static final String formatScore(long score) {
        StringBuffer ret = new StringBuffer();
        ret.append(GameUtils.formatTime(score));
        ret.append("s");
        return ret.toString();
    }
}
