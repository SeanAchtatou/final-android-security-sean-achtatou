package com.gannicus.android.roundsurvival;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import com.gannicus.android.game.api.GameObj;
import com.gannicus.android.game.api.GameResources;

public class Track extends GameObj {
    private static final int STROKE_WIDTH = 4;
    private static final RectF ovalInner = new RectF();
    private static final RectF ovalOuter = new RectF();
    private static final Paint paintInside = new Paint();
    private static final Paint paintOutside = new Paint();
    private static final Paint paintStroke = new Paint(1);
    private Bitmap buf = null;

    public boolean isContained(float x, float y) {
        float a = ovalInner.width() / 2.0f;
        float b = ovalInner.height() / 2.0f;
        float x1 = x - (ovalInner.left + a);
        float y1 = y - (ovalInner.top + b);
        boolean b1 = ((x1 * x1) / (a * a)) + ((y1 * y1) / (b * b)) > 1.0f;
        float a2 = ovalOuter.width() / 2.0f;
        float b2 = ovalOuter.height() / 2.0f;
        float x2 = x - (ovalOuter.left + a2);
        float y2 = y - (ovalOuter.top + b2);
        if (!(((x2 * x2) / (a2 * a2)) + ((y2 * y2) / (b2 * b2)) < 1.0f) || !b1) {
            return false;
        }
        return true;
    }

    public void init(int width, int height) {
        if (this.scale == 0.0f) {
            this.scale = 1.0f;
        }
        paintInside.setStyle(Paint.Style.FILL);
        paintInside.setColor(GameResources.bgColor);
        paintStroke.setStrokeWidth(this.scale * 4.0f);
        paintStroke.setStyle(Paint.Style.STROKE);
        paintStroke.setColor(GameResources.trackBorderColor);
        float left = 120.0f * this.scale;
        float top = 80.0f * this.scale;
        ovalInner.left = left - (this.scale * 15.0f);
        ovalInner.top = top - (this.scale * 10.0f);
        RectF rectF = ovalInner;
        rectF.right = (((float) width) - left) - (this.scale * 15.0f);
        RectF rectF2 = ovalInner;
        rectF2.bottom = (((float) height) - top) - (this.scale * 10.0f);
        paintOutside.setStrokeWidth(this.scale * 4.0f);
        paintOutside.setColor(GameResources.trackColor);
        float left2 = 20.0f * this.scale;
        float top2 = 10.0f * this.scale;
        ovalOuter.left = left2;
        ovalOuter.top = top2;
        ovalOuter.right = ((float) width) - left2;
        ovalOuter.bottom = ((float) height) - top2;
    }

    /* access modifiers changed from: protected */
    public void drawObj(Canvas canvas) {
        if (this.buf == null) {
            this.buf = Bitmap.createBitmap(GameResources.screenWidth, GameResources.screenHeight, Bitmap.Config.RGB_565);
            Canvas c = new Canvas(this.buf);
            c.drawColor(GameResources.bgColor);
            c.drawOval(ovalOuter, paintOutside);
            c.drawOval(ovalOuter, paintStroke);
            c.drawOval(ovalInner, paintInside);
            c.drawOval(ovalInner, paintStroke);
        }
        canvas.drawBitmap(this.buf, 0.0f, 0.0f, (Paint) null);
    }
}
