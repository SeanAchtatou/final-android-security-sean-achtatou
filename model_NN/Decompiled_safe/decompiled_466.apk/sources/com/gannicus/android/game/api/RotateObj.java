package com.gannicus.android.game.api;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public class RotateObj extends GameObj {
    public int alpha = 150;
    private int angle = 0;
    public Bitmap bg;
    private int delay = this.frameDelayMs;
    private int frameDelayMs = 100;
    public int increase = 10;
    private Paint p = new Paint();

    public void updateAnim(int timeMs) {
        this.delay -= timeMs;
        if (this.delay <= 0) {
            this.angle += this.increase;
            this.angle %= 360;
            this.delay = this.frameDelayMs;
        }
    }

    public void setFrameDelay(int delay2) {
        this.frameDelayMs = delay2;
        this.delay = this.frameDelayMs;
    }

    /* access modifiers changed from: protected */
    public void drawObj(Canvas canvas) {
        canvas.save();
        canvas.rotate((float) this.angle, this.x, this.y);
        this.p.setAlpha(this.alpha);
        canvas.drawBitmap(this.bg, (Rect) null, this.bounds, this.p);
        canvas.restore();
    }
}
