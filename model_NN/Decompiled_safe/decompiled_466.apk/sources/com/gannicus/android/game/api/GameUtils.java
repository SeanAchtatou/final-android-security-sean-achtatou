package com.gannicus.android.game.api;

import android.content.Context;
import java.text.DecimalFormat;

public class GameUtils {
    private static final char[] DigitOnes = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
    private static final char[] DigitTens = {'0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '3', '3', '3', '3', '3', '3', '3', '3', '3', '3', '4', '4', '4', '4', '4', '4', '4', '4', '4', '4', '5', '5', '5', '5', '5', '5', '5', '5', '5', '5', '6', '6', '6', '6', '6', '6', '6', '6', '6', '6', '7', '7', '7', '7', '7', '7', '7', '7', '7', '7', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9'};
    private static final String KEY = "Go_Round_Highest_Score";
    private static final String LOG_TAG = "GameUtils";
    private static final String PREFS_FILE = "Go_Round_Prefs";
    private static final char[] digits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
    private static final int[] intSizeTable = {9, 99, 999, 9999, 99999, 999999, 9999999, 99999999, 999999999, Integer.MAX_VALUE};

    public static String formatTime(long timeConsumingMs) {
        return new DecimalFormat("###0.00").format((double) (((float) timeConsumingMs) / 1000.0f));
    }

    public static void saveScore(long score, Context ctx) {
        ctx.getSharedPreferences(PREFS_FILE, 0).edit().putLong(KEY, score).commit();
    }

    public static long getScore(Context ctx) {
        return ctx.getSharedPreferences(PREFS_FILE, 0).getLong(KEY, 0);
    }

    public static void saveUploadedState(boolean isUploaded, Context ctx) {
        ctx.getSharedPreferences(PREFS_FILE, 0).edit().putBoolean("is_uploaded", isUploaded).commit();
    }

    public static boolean isUploaded(Context ctx) {
        return ctx.getSharedPreferences(PREFS_FILE, 0).getBoolean("is_uploaded", false);
    }

    public static int stringSize(int x) {
        int i = 0;
        while (x > intSizeTable[i]) {
            i++;
        }
        return i + 1;
    }

    public static final void getChars(int i, int index, char[] buf) {
        if (i == Integer.MIN_VALUE) {
            System.arraycopy("-2147483648".toCharArray(), 0, buf, 0, buf.length);
        }
        int charPos = index;
        char sign = 0;
        if (i < 0) {
            sign = '-';
            i = -i;
        }
        while (i >= 65536) {
            int q = i / 100;
            int r = i - (((q << 6) + (q << 5)) + (q << 2));
            i = q;
            int charPos2 = charPos - 1;
            buf[charPos2] = DigitOnes[r];
            charPos = charPos2 - 1;
            buf[charPos] = DigitTens[r];
        }
        do {
            int q2 = (52429 * i) >>> 19;
            charPos--;
            buf[charPos] = digits[i - ((q2 << 3) + (q2 << 1))];
            i = q2;
        } while (i != 0);
        if (sign != 0) {
            buf[charPos - 1] = sign;
        }
    }

    public static void getTimeChars(int timeMs, char[] buf) {
        int timeMs2 = (timeMs + 5) / 10;
        int index = stringSize(timeMs2);
        getChars(timeMs2, index, buf);
        if (index == 2) {
            buf[2] = buf[0];
            buf[3] = buf[1];
            buf[0] = '0';
            buf[1] = '.';
        } else if (index == 1) {
            buf[2] = '0';
            buf[3] = buf[0];
            buf[0] = '0';
            buf[1] = '.';
        } else {
            for (int i = 0; i != 2; i++) {
                buf[index - i] = buf[(index - i) - 1];
            }
            buf[index - 2] = '.';
        }
    }
}
