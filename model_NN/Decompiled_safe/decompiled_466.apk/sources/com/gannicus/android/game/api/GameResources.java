package com.gannicus.android.game.api;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import com.gannicus.android.roundsurvival.R;
import java.util.ArrayList;
import java.util.Iterator;

public class GameResources {
    public static final Bitmap.Config DEFAULT_BITMAP_CONFIG = Bitmap.Config.ARGB_8888;
    public static final Bitmap.Config FAST_BITMAP_CONFIG = Bitmap.Config.RGB_565;
    private static final String LOG_TAG = "GameResources";
    public static final Bitmap.Config OPTIMIZE_CONFIG = Bitmap.Config.ARGB_4444;
    public static int bgColor;
    public static Bitmap bgGamePad;
    public static Bitmap bgGamePadDown;
    public static Bitmap bgMidSpeed;
    public static Bitmap bgNomalSpeed;
    public static Bitmap bgTopSpeed;
    public static ArrayList<Bitmap> carAnim;
    public static Paint fpsPaint;
    public static float scale;
    public static Paint scorePaint;
    public static int screenHeight;
    public static int screenWidth;
    public static Paint tailPaint;
    public static int trackBorderColor;
    public static int trackColor;

    public static final void loading(Context ctx) {
        loadResources(ctx);
        MusicManager.loadMusics(ctx);
    }

    public static final void loadResources(Context context) {
        Resources r = context.getResources();
        scale = r.getDisplayMetrics().density;
        screenWidth = r.getDisplayMetrics().widthPixels;
        screenHeight = r.getDisplayMetrics().heightPixels;
        carAnim = new ArrayList<>();
        for (int i = R.drawable.car_anim01; i <= R.drawable.car_anim17; i++) {
            carAnim.add(loadBitmap(r.getDrawable(i)));
        }
        bgNomalSpeed = loadBitmap(r.getDrawable(R.drawable.car_tiny));
        bgMidSpeed = loadBitmap(r.getDrawable(R.drawable.car_tiny_mid_speed));
        bgTopSpeed = loadBitmap(r.getDrawable(R.drawable.car_tiny_top_speed));
        bgGamePad = loadBitmap(r.getDrawable(R.drawable.game_pad));
        bgGamePadDown = loadBitmap(r.getDrawable(R.drawable.game_pad_down));
        tailPaint = new Paint();
        tailPaint.setColor(-1);
        tailPaint.setAntiAlias(true);
        tailPaint.setStrokeWidth(12.0f * scale);
        fpsPaint = new Paint();
        fpsPaint.setTypeface(Typeface.create(Typeface.SANS_SERIF, 1));
        fpsPaint.setColor(Color.parseColor("#2a4c25"));
        scorePaint = new Paint();
        Typeface scoreTypeface = Typeface.create(Typeface.SANS_SERIF, 1);
        scorePaint.setTextSize(50.0f);
        scorePaint.setTypeface(scoreTypeface);
        scorePaint.setColor(-1);
        bgColor = Color.parseColor("#407f04");
        trackColor = Color.parseColor("#dfbd6c");
        trackBorderColor = Color.parseColor("#2a4c25");
    }

    public static final void release() {
        if (carAnim != null) {
            Iterator<Bitmap> it = carAnim.iterator();
            while (it.hasNext()) {
                it.next().recycle();
            }
        }
        carAnim = null;
        bgNomalSpeed = null;
        bgMidSpeed = null;
        bgTopSpeed = null;
        bgGamePad = null;
        bgGamePadDown = null;
        fpsPaint = null;
        scorePaint = null;
    }

    public static final Bitmap loadBitmap(Drawable sprite, int width, int height, Bitmap.Config bitmapConfig) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, bitmapConfig);
        Canvas canvas = new Canvas(bitmap);
        sprite.setBounds(0, 0, width, height);
        sprite.draw(canvas);
        return bitmap;
    }

    public static final Bitmap loadBitmap(Drawable sprite, Bitmap.Config bitmapConfig) {
        return loadBitmap(sprite, sprite.getIntrinsicWidth(), sprite.getIntrinsicHeight(), bitmapConfig);
    }

    public static final Bitmap loadBitmap(Drawable sprite) {
        return loadBitmap(sprite, OPTIMIZE_CONFIG);
    }
}
