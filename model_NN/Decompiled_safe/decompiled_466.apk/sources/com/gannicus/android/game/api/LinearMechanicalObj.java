package com.gannicus.android.game.api;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.FloatMath;
import java.lang.reflect.Array;

public class LinearMechanicalObj extends GameObj {
    private static final String LOG_TAG = "MechaniclaObj";
    private static final int PUSH_TO_TAILS_DELAY = 50;
    private static final float SPEED_MAX = 250.0f;
    private static final int TAILS_FADE_DELAY = 100;
    private static final int TAILS_FADE_OUT_TIME = 1900;
    private static final int TIME_MS = 50;
    public int acceleration;
    public float angle;
    public Bitmap bgMidSpeed;
    public Bitmap bgNomalSpeed;
    public Bitmap bgTopSpeed;
    public Bitmap currentBG;
    private long fadeDelay = 0;
    public boolean isFixed;
    private long pushToTailsDelay = 0;
    public float speed;
    public float[][] tails = ((float[][]) Array.newInstance(Float.TYPE, 40, 3));
    private int tailsIndex = 0;
    public long time = 0;

    public void clearTails() {
        for (float[] e : this.tails) {
            e[0] = 0.0f;
            e[1] = 0.0f;
            e[2] = 0.0f;
        }
    }

    public final void updateStateIn(int timeMs) {
        if (!this.isFixed) {
            float speedEnd = this.speed + (((float) (this.acceleration * timeMs)) / 1000.0f);
            if (speedEnd >= SPEED_MAX) {
                speedEnd = SPEED_MAX;
            }
            if (speedEnd < 150.0f) {
                this.currentBG = this.bgNomalSpeed;
            } else if (speedEnd >= 150.0f && speedEnd < SPEED_MAX) {
                this.currentBG = this.bgMidSpeed;
            } else if (speedEnd >= SPEED_MAX) {
                this.currentBG = this.bgTopSpeed;
            }
            float distance = (((this.speed + speedEnd) * ((float) timeMs)) / 2000.0f) * this.scale;
            this.speed = speedEnd;
            this.x += FloatMath.cos((this.angle * 3.1416f) / 180.0f) * distance;
            this.y += FloatMath.sin((this.angle * 3.1416f) / 180.0f) * distance;
            moveTo(this.x, this.y);
            updateTails((long) timeMs);
        }
    }

    private final void updateTails(long timeMs) {
        if (this.pushToTailsDelay <= 0) {
            this.pushToTailsDelay = 50;
        }
        this.pushToTailsDelay -= timeMs;
        if (this.pushToTailsDelay <= 0) {
            this.tailsIndex++;
            this.tailsIndex %= 40;
            this.tails[this.tailsIndex][0] = this.x;
            this.tails[this.tailsIndex][1] = this.y;
            this.tails[this.tailsIndex][2] = 200.0f;
        }
        if (this.fadeDelay <= 0) {
            this.fadeDelay = 100;
        }
        this.fadeDelay -= timeMs;
        if (this.fadeDelay <= 0) {
            for (float[] e : this.tails) {
                e[2] = e[2] - ((float) (255 / 19));
                e[2] = e[2] <= 0.0f ? 0.0f : e[2];
            }
        }
    }

    public void drawObj(Canvas canvas) {
        drawTails(canvas);
        canvas.save();
        canvas.rotate(this.angle, this.x, this.y);
        canvas.drawBitmap(this.currentBG, (Rect) null, this.bounds, (Paint) null);
        canvas.restore();
    }

    private final void drawTails(Canvas canvas) {
        for (int i = this.tails.length - 1; i >= 0; i--) {
            float[] e = this.tails[i];
            int j = i - 1;
            if (j == -1) {
                j = this.tails.length - 1;
            }
            float[] e2 = this.tails[j];
            if ((e[0] > 0.001f || e[1] > 0.001f) && e[2] > 0.001f && (e2[0] > 0.001f || e2[1] > 0.001f)) {
                GameResources.tailPaint.setAlpha((int) e[2]);
                canvas.drawLine(e[0], e[1], e2[0], e2[1], GameResources.tailPaint);
            }
        }
    }
}
