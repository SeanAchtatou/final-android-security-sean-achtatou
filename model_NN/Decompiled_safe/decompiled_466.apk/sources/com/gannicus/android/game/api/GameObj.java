package com.gannicus.android.game.api;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.FloatMath;
import java.util.ArrayList;

public abstract class GameObj {
    public Rect bounds;
    private int delay = this.frameDelayMs;
    private int frameDelayMs = 100;
    public ArrayList<Bitmap> frames = new ArrayList<>();
    private int index = 0;
    private boolean isActive = false;
    private boolean isLoop = false;
    private boolean isVisible = true;
    public float scale;
    protected float x;
    protected float y;

    /* access modifiers changed from: protected */
    public abstract void drawObj(Canvas canvas);

    public void setFrameDelay(int delay2) {
        this.frameDelayMs = delay2;
        this.delay = this.frameDelayMs;
    }

    public void updateAnim(int timeMs) {
        if (this.isActive) {
            this.delay -= timeMs;
            if (this.delay <= 0) {
                this.index++;
                this.index %= this.frames.size();
                this.delay = this.frameDelayMs;
            }
        }
    }

    public void updateAnim(int timeMs, Rect bounds2) {
        updateAnim(timeMs);
        if (bounds2 != null) {
            this.bounds = bounds2;
        }
    }

    public void updateAnim(int timeMs, Point center) {
        updateAnim(timeMs);
        moveTo(this.x, this.y);
    }

    public void moveTo(float x2, float y2) {
        this.x = x2;
        this.y = y2;
        int width = this.bounds.width() / 2;
        int height = this.bounds.height() / 2;
        this.bounds.left = (int) FloatMath.floor((x2 - ((float) width)) + 0.5f);
        this.bounds.right = (int) FloatMath.floor(((float) width) + x2 + 0.5f);
        this.bounds.top = (int) FloatMath.floor((y2 - ((float) height)) + 0.5f);
        this.bounds.bottom = (int) FloatMath.floor(((float) height) + y2 + 0.5f);
    }

    public void setAnimActive(boolean isActive2) {
        this.isActive = isActive2;
    }

    public void resetAnim() {
        this.index = 0;
    }

    public boolean isAnimActive() {
        return this.isActive;
    }

    public void setLoop(boolean isLoop2) {
        this.isLoop = isLoop2;
    }

    public void draw(Canvas canvas) {
        if (this.isVisible) {
            if (this.isActive) {
                canvas.drawBitmap(this.frames.get(this.index), (Rect) null, this.bounds, (Paint) null);
                if (this.index == this.frames.size() - 1 && !this.isLoop) {
                    this.index = 0;
                    this.isActive = this.isLoop;
                    return;
                }
                return;
            }
            drawObj(canvas);
        }
    }

    public boolean isContained(Point p) {
        return this.bounds.contains(p.x, p.y);
    }

    public boolean isContained(float x2, float y2) {
        return this.bounds.contains((int) x2, (int) y2);
    }

    public boolean isMeet(GameObj obj) {
        return this.bounds.contains(obj.bounds);
    }

    public boolean isContained(GameObj obj) {
        return this.bounds.contains(obj.bounds);
    }

    public void setVisible(boolean isVisible2) {
        this.isVisible = isVisible2;
    }

    public boolean isVisible() {
        return this.isVisible;
    }
}
