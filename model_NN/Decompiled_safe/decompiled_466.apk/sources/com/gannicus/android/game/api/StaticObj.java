package com.gannicus.android.game.api;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public class StaticObj extends GameObj {
    public Bitmap bg;

    /* access modifiers changed from: protected */
    public void drawObj(Canvas canvas) {
        canvas.drawBitmap(this.bg, (Rect) null, this.bounds, (Paint) null);
    }
}
