package com.gannicus.android.game.api;

import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.Log;

public abstract class MechanicalObj extends GameObj {
    private static final String TAG_LOG = "MechaniclaObj";
    public int accelerationX;
    public int accelerationY;
    public int angle;
    public Drawable bg;
    public Rect bounds;
    public Point center;
    public boolean isFixed;
    private int scale = 1;
    public float speedH;
    public float speedV;

    public void updateStateIn(float time) {
        if (!this.isFixed) {
            this.center = updateCoordinate(time);
            this.angle = ((int) ((Math.acos(0.5d) * 360.0d) / 3.141592653589793d)) + 180;
            Log.d(TAG_LOG, "angle is: " + this.angle);
            int width = this.bounds.width() / 2;
            int height = this.bounds.height() / 2;
            this.bounds.left = this.center.x - width;
            this.bounds.right = this.center.x + width;
            this.bounds.top = this.center.y - height;
            this.bounds.bottom = this.center.y + height;
        }
    }

    private Point updateCoordinate(float time) {
        float temp = this.speedH;
        this.speedH += ((float) this.accelerationX) * time;
        float temp2 = (float) ((int) (((double) ((this.speedH + temp) * time)) / (((double) this.scale) * 2.0d)));
        Point point = this.center;
        point.x = (int) (((float) point.x) + temp2);
        float temp3 = this.speedV;
        this.speedV += ((float) this.accelerationY) * time;
        float temp4 = (float) ((int) (((double) ((this.speedV + temp3) * time)) / (((double) this.scale) * 2.0d)));
        Point point2 = this.center;
        point2.y = (int) (((float) point2.y) + temp4);
        return this.center;
    }

    public void draw(Canvas canvas) {
        canvas.save();
        canvas.rotate((float) this.angle, (float) this.center.x, (float) this.center.y);
        this.bg.setBounds(this.bounds);
        this.bg.draw(canvas);
        canvas.restore();
    }
}
