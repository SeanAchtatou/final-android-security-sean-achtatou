package com.gannicus.android.game.api;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.Log;
import com.gannicus.android.roundsurvival.R;
import java.util.Collection;
import java.util.HashMap;

public class MusicManager {
    public static final int MUSIC_CHEER = 4;
    public static final int MUSIC_END_GAME = 2;
    public static final int MUSIC_EXPLOSION = 3;
    public static final int MUSIC_GAME = 1;
    public static final int MUSIC_MENU = 0;
    public static final int MUSIC_PREVIOUS = -1;
    private static final String PREFS_FILE = "com.zedd.game.Survival";
    private static final String TAG = "MusicManager";
    private static int currentMusic = -1;
    private static HashMap<Integer, MediaPlayer> players;

    public static void loadMusics(Context context) {
        players = new HashMap<>();
        players.put(0, MediaPlayer.create(context, (int) R.raw.menu_music));
        players.put(1, MediaPlayer.create(context, (int) R.raw.game_music));
        players.put(3, MediaPlayer.create(context, (int) R.raw.explosion));
        players.put(4, MediaPlayer.create(context, (int) R.raw.go_cheer));
    }

    public static void start(Context context, int music) {
        start(context, music, true, true);
    }

    public static void start(Context context, int music, boolean pauseCurrent, boolean isLoop) {
        if (isBGMusicAllowed(context)) {
            if (currentMusic != -1 && pauseCurrent) {
                pause();
            }
            if (pauseCurrent) {
                currentMusic = music;
            }
            MediaPlayer mp = players.get(Integer.valueOf(music));
            if (mp != null && !mp.isPlaying()) {
                mp.setLooping(isLoop);
                mp.start();
            }
        }
    }

    public static void Resume(Context ctx) {
        start(ctx, currentMusic);
    }

    public static void pause() {
        for (MediaPlayer p : players.values()) {
            if (p != null && p.isPlaying()) {
                p.pause();
            }
        }
    }

    public static boolean isBGMusicAllowed(Context ctx) {
        return ctx.getSharedPreferences(PREFS_FILE, 0).getBoolean("is_bg_music_allowed", false);
    }

    public static void saveMusicIsAllowed(Context ctx, boolean state) {
        ctx.getSharedPreferences(PREFS_FILE, 0).edit().putBoolean("is_bg_music_allowed", state).commit();
    }

    public static void release() {
        Collection<MediaPlayer> mps = players.values();
        for (MediaPlayer mp : mps) {
            if (mp != null) {
                try {
                    if (mp.isPlaying()) {
                        mp.stop();
                    }
                    mp.release();
                } catch (Exception e) {
                    Exception e2 = e;
                    Log.e(TAG, e2.getMessage(), e2);
                }
            }
        }
        mps.clear();
        currentMusic = -1;
    }
}
