package com.gannicus.android.game.api;

import android.os.Handler;
import android.os.Message;

public class TimeCounter {
    private final long PERIOD = 100;
    private ScheduleHandler handle = new ScheduleHandler(this, null);
    /* access modifiers changed from: private */
    public boolean isPaused = false;
    /* access modifiers changed from: private */
    public boolean isRunning = false;
    private long last;
    /* access modifiers changed from: private */
    public TimeCountDownListener listener;
    private int sign;
    /* access modifiers changed from: private */
    public long timeMs;

    public interface TimeCountDownListener {
        void onExpired();

        void onNotified(long j);
    }

    public void startCount(long timeMs2, boolean isCountDown) {
        this.sign = isCountDown ? -1 : 1;
        this.timeMs = timeMs2;
        if (!this.isRunning) {
            this.isRunning = true;
            this.last = System.currentTimeMillis();
            this.handle.sleep(100);
        }
    }

    public void setListener(TimeCountDownListener listener2) {
        this.listener = listener2;
    }

    public void stopCount() {
        this.isRunning = false;
    }

    /* access modifiers changed from: private */
    public void process() {
        long now = System.currentTimeMillis();
        long increment = (now - this.last) * ((long) this.sign);
        if (increment >= 100) {
            this.last = now;
            if (!this.isPaused) {
                this.timeMs += increment;
            }
        }
    }

    private class ScheduleHandler extends Handler {
        private ScheduleHandler() {
        }

        /* synthetic */ ScheduleHandler(TimeCounter timeCounter, ScheduleHandler scheduleHandler) {
            this();
        }

        public void handleMessage(Message msg) {
            TimeCounter.this.process();
            if (TimeCounter.this.isPaused) {
                sleep(100);
                return;
            }
            TimeCounter.this.listener.onNotified(TimeCounter.this.timeMs);
            if (TimeCounter.this.timeMs <= 0 && TimeCounter.this.isRunning) {
                TimeCounter.this.isRunning = false;
                TimeCounter.this.listener.onExpired();
            } else if (TimeCounter.this.isRunning) {
                sleep(100);
            }
        }

        public void sleep(long delayMillis) {
            removeMessages(0);
            sendMessageDelayed(obtainMessage(0), delayMillis);
        }
    }

    public void resume() {
        this.isPaused = false;
    }

    public void pause() {
        this.isPaused = true;
    }
}
