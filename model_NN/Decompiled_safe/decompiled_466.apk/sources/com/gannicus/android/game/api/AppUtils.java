package com.gannicus.android.game.api;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.widget.Toast;
import com.gannicus.android.roundsurvival.R;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.HashMap;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

public class AppUtils {
    public static final void showNotificationInfo(final Handler handler, final Activity ctx, final String infoServiceUrl) {
        new Thread() {
            public void run() {
                try {
                    JSONObject info = new JSONObject(AppUtils.getUrlResponse(infoServiceUrl));
                    if (info.getBoolean("active")) {
                        final String id = info.getString("id").trim();
                        if (!AppUtils.hasShowed(ctx, id)) {
                            final String type = info.getString("type").trim();
                            final String title = info.getString("title").trim();
                            final String content = info.getString("content").trim();
                            final int version = info.getInt("version");
                            Handler handler = handler;
                            final Activity activity = ctx;
                            handler.post(new Runnable() {
                                public void run() {
                                    try {
                                        if (type.equals("notify")) {
                                            Notification notification = new Notification(R.drawable.icon, title, System.currentTimeMillis());
                                            notification.setLatestEventInfo(activity, title, content, PendingIntent.getActivity(activity, 0, new Intent(), 0));
                                            notification.flags = 16;
                                            ((NotificationManager) activity.getSystemService("notification")).notify(3287510, notification);
                                            boolean unused = AppUtils.saveShowedId(activity, id);
                                        } else if (type.equals("toast")) {
                                            Toast.makeText(activity, content, 1).show();
                                            boolean unused2 = AppUtils.saveShowedId(activity, id);
                                        } else if (type.equals("dialog")) {
                                            new AlertDialog.Builder(activity).setTitle(title).setMessage(content).setIcon((int) R.drawable.icon).setPositiveButton("Okay", (DialogInterface.OnClickListener) null).show();
                                            boolean unused3 = AppUtils.saveShowedId(activity, id);
                                        } else if (type.equals("new_version")) {
                                            AppUtils.checkVersion(activity, title, content, version);
                                        }
                                    } catch (Exception e) {
                                    }
                                }
                            });
                        }
                    }
                } catch (Exception e) {
                }
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public static final void checkVersion(final Activity ctx, String title, String content, int latestVersion) {
        try {
            final String packageName = ctx.getApplication().getPackageName();
            if (latestVersion > ctx.getApplication().getPackageManager().getPackageInfo(packageName, 0).versionCode) {
                new AlertDialog.Builder(ctx).setIcon((int) R.drawable.icon).setTitle(title).setMessage(content).setPositiveButton("Update now", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        ctx.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=" + packageName)));
                        ctx.finish();
                    }
                }).setNegativeButton("Do it later", (DialogInterface.OnClickListener) null).show();
            }
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: private */
    public static final boolean hasShowed(Context ctx, String id) {
        if (PreferenceManager.getDefaultSharedPreferences(ctx).getString("web_info_last_id", "").equals(id)) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: private */
    public static final boolean saveShowedId(Context ctx, String id) {
        if (PreferenceManager.getDefaultSharedPreferences(ctx).edit().putString("web_info_last_id", id).commit()) {
            return true;
        }
        return false;
    }

    public static final String getUrlResponse(String url) {
        try {
            return convertStreamToString(new DefaultHttpClient().execute(new HttpGet(url)).getEntity().getContent());
        } catch (Exception e) {
            return null;
        }
    }

    public static final String postUrlResponse(String url, HashMap<String, String> paramsMap, String basicAuthCode) {
        try {
            HttpPost post = new HttpPost(url);
            if (paramsMap == null) {
                paramsMap = new HashMap<>();
            }
            StringBuffer buf = new StringBuffer();
            for (String key : paramsMap.keySet()) {
                buf.append("&").append(key).append("=").append(paramsMap.get(key));
            }
            if (buf.length() > 0) {
                buf.deleteCharAt(0);
                StringEntity reqEntity = new StringEntity(buf.toString());
                reqEntity.setContentType("application/x-www-form-urlencoded");
                post.setEntity(reqEntity);
            }
            if (basicAuthCode != null) {
                post.addHeader("Authorization", "Basic " + basicAuthCode);
            }
            return convertStreamToString(new DefaultHttpClient().execute(post).getEntity().getContent());
        } catch (Exception e) {
            return null;
        }
    }

    private static final String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is), 8192);
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String line = reader.readLine();
                if (line == null) {
                    try {
                        break;
                    } catch (IOException e) {
                    }
                } else {
                    sb.append(String.valueOf(line) + "\n");
                }
            } catch (IOException e2) {
                try {
                    is.close();
                } catch (IOException e3) {
                }
            } catch (Throwable th) {
                try {
                    is.close();
                } catch (IOException e4) {
                }
                throw th;
            }
        }
        is.close();
        return sb.toString();
    }

    public static final Bitmap getBitmapFromUrl(String url) throws IOException {
        return getBitmapFromUrl(new URL(url));
    }

    public static final Bitmap getBitmapFromUrl(URL url) {
        InputStream in = null;
        OutputStream out = null;
        try {
            InputStream in2 = new BufferedInputStream(url.openStream(), 4096);
            try {
                ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
                OutputStream out2 = new BufferedOutputStream(dataStream, 4096);
                try {
                    copy(in2, out2);
                    out2.flush();
                    byte[] data = dataStream.toByteArray();
                    Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                    closeStream(in2);
                    closeStream(out2);
                    return bitmap;
                } catch (IOException e) {
                    out = out2;
                    in = in2;
                    closeStream(in);
                    closeStream(out);
                    return null;
                } catch (Throwable th) {
                    th = th;
                    out = out2;
                    in = in2;
                    closeStream(in);
                    closeStream(out);
                    throw th;
                }
            } catch (IOException e2) {
                in = in2;
                closeStream(in);
                closeStream(out);
                return null;
            } catch (Throwable th2) {
                th = th2;
                in = in2;
                closeStream(in);
                closeStream(out);
                throw th;
            }
        } catch (IOException e3) {
            closeStream(in);
            closeStream(out);
            return null;
        } catch (Throwable th3) {
            th = th3;
            closeStream(in);
            closeStream(out);
            throw th;
        }
    }

    private static final void closeStream(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
            }
        }
    }

    public static final boolean download(String url, String fullpath) {
        InputStream in = null;
        FileOutputStream out = null;
        try {
            in = new URL(url).openStream();
            if (in == null) {
                closeStream(in);
                closeStream(null);
                return false;
            }
            File downloadFile = new File(fullpath);
            FileOutputStream out2 = new FileOutputStream(downloadFile);
            try {
                byte[] buf = new byte[16384];
                while (true) {
                    int numread = in.read(buf);
                    if (numread <= 0) {
                        break;
                    }
                    out2.write(buf, 0, numread);
                }
                if (downloadFile.length() == 0) {
                    downloadFile.delete();
                    closeStream(in);
                    closeStream(out2);
                    return false;
                }
                closeStream(in);
                closeStream(out2);
                return true;
            } catch (Exception e) {
                out = out2;
            } catch (Throwable th) {
                th = th;
                out = out2;
                closeStream(in);
                closeStream(out);
                throw th;
            }
        } catch (Exception e2) {
        } catch (Throwable th2) {
            th = th2;
            closeStream(in);
            closeStream(out);
            throw th;
        }
        closeStream(in);
        closeStream(out);
        return false;
    }

    /* JADX INFO: finally extract failed */
    public static final void copyFile(File srcFile, File destFile) throws IOException {
        if (!destFile.exists() || !destFile.isDirectory()) {
            FileInputStream input = new FileInputStream(srcFile);
            FileOutputStream output = new FileOutputStream(destFile);
            try {
                copy(input, output);
                closeStream(input);
                closeStream(output);
                if (srcFile.length() != destFile.length()) {
                    throw new IOException("Failed to copy full contents from '" + srcFile + "' to '" + destFile + "'");
                }
                destFile.setLastModified(srcFile.lastModified());
            } catch (Throwable th) {
                closeStream(input);
                closeStream(output);
                throw th;
            }
        } else {
            throw new IOException("Destination '" + destFile + "' exists but is a directory");
        }
    }

    public static final int copy(InputStream input, OutputStream output) throws IOException {
        long count = copyLarge(input, output);
        if (count > 2147483647L) {
            return -1;
        }
        return (int) count;
    }

    public static final long copyLarge(InputStream input, OutputStream output) throws IOException {
        byte[] buffer = new byte[4096];
        long count = 0;
        while (true) {
            int n = input.read(buffer);
            if (-1 == n) {
                return count;
            }
            output.write(buffer, 0, n);
            count += (long) n;
        }
    }

    public static void showInfo(Handler handler, final Context ctx, final String info) {
        handler.post(new Runnable() {
            public void run() {
                new AlertDialog.Builder(ctx).setMessage(info).setPositiveButton("Okay", (DialogInterface.OnClickListener) null).show();
            }
        });
    }

    public static void showInfo(Handler handler, Context ctx, int stringId) {
        showInfo(handler, ctx, ctx.getResources().getString(stringId));
    }

    public static final void recommend(Context ctx) {
        Intent sendIntent = new Intent("android.intent.action.SEND");
        sendIntent.putExtra("android.intent.extra.SUBJECT", "Android application recommendation");
        sendIntent.putExtra("android.intent.extra.TEXT", "I recommend this android app to you: Awesome Avatars. You can find it in android market by searching for: Awesome Avatars");
        sendIntent.setType("text/plain");
        ctx.startActivity(Intent.createChooser(sendIntent, "Recommend by"));
    }

    public static final void suggestion(Context ctx) {
        Intent sendIntent = new Intent("android.intent.action.SEND");
        sendIntent.putExtra("android.intent.extra.SUBJECT", "Suggestion/Comments for Awesome Avatars");
        sendIntent.putExtra("android.intent.extra.TEXT", "Please tell us about any Application issues you are having or make a suggestion for improving our Application. \n\nAlso, Please tell us what avatars you want but we don't have. We will try to get it done for you:)");
        sendIntent.putExtra("android.intent.extra.EMAIL", new String[]{"AwesomeApps4Me@gmail.com"});
        sendIntent.setType("message/rfc822");
        ctx.startActivity(sendIntent);
    }

    public static final void moreApps(Context ctx) {
        ctx.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:" + "ZEDD")));
    }
}
