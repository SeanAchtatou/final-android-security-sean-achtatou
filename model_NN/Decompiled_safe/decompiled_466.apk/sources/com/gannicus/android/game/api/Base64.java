package com.gannicus.android.game.api;

public class Base64 {
    public static final String BASE64CODE = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    public static final int SPLIT_LINES_AT = 76;

    public static byte[] zeroPad(int length, byte[] bytes) {
        byte[] padded = new byte[length];
        System.arraycopy(bytes, 0, padded, 0, bytes.length);
        return padded;
    }

    public static String encode(String string) {
        byte[] stringArray;
        String encoded = "";
        try {
            stringArray = string.getBytes("UTF-8");
        } catch (Exception e) {
            stringArray = string.getBytes();
        }
        int paddingCount = (3 - (stringArray.length % 3)) % 3;
        byte[] stringArray2 = zeroPad(stringArray.length + paddingCount, stringArray);
        for (int i = 0; i < stringArray2.length; i += 3) {
            int j = (stringArray2[i] << 16) + (stringArray2[i + 1] << 8) + stringArray2[i + 2];
            encoded = String.valueOf(encoded) + BASE64CODE.charAt((j >> 18) & 63) + BASE64CODE.charAt((j >> 12) & 63) + BASE64CODE.charAt((j >> 6) & 63) + BASE64CODE.charAt(j & 63);
        }
        return splitLines(String.valueOf(encoded.substring(0, encoded.length() - paddingCount)) + "==".substring(0, paddingCount)).trim();
    }

    public static String splitLines(String string) {
        String lines = "";
        for (int i = 0; i < string.length(); i += 76) {
            lines = String.valueOf(String.valueOf(lines) + string.substring(i, Math.min(string.length(), i + 76))) + "\r\n";
        }
        return lines;
    }
}
