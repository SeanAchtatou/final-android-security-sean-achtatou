package com.gannicus.android.game.api;

public class GameState {
    public static final int GAME_OVER = 3;
    public static final int GAME_WIN = 4;
    public static final int MAIN_MENU = 0;
    public static final int PLAYING = 2;
    public static final int READY = 1;
    public boolean isPaused = false;
    public boolean isRunning = false;
    public int state = 0;
}
