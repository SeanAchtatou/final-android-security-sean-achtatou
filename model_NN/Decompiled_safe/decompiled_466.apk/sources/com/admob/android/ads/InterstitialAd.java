package com.admob.android.ads;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

public class InterstitialAd {
    public static final String ADMOB_INTENT_BOOLEAN = "admob_activity";
    private static Handler a = null;
    private static Timer b = null;
    private static a c = null;
    private Event d;
    private WeakReference<InterstitialAdListener> e;
    private boolean f = false;
    private boolean g = false;
    /* access modifiers changed from: private */
    public j h = null;
    private String i = null;
    private String j = null;
    private c k = new c(this);
    private long l = -1;

    public enum Event {
        APP_START,
        SCREEN_CHANGE,
        PRE_ROLL,
        POST_ROLL,
        OTHER
    }

    /* compiled from: TestLog */
    public static class c implements m {
        InterstitialAd a;

        public c() {
        }

        public static boolean a(String str, int i) {
            return (i >= 5) || Log.isLoggable(str, i);
        }

        public c(InterstitialAd interstitialAd) {
            this.a = interstitialAd;
        }

        public final void a() {
            if (this.a != null) {
                this.a.c();
            }
        }

        public final void a(j jVar) {
            if (this.a != null) {
                this.a.h = jVar;
                this.a.a();
            }
        }
    }

    static /* synthetic */ void a(InterstitialAd interstitialAd) {
        if (h()) {
            g();
            interstitialAd.k.a = null;
            try {
                c.a = true;
                c = null;
            } catch (SecurityException e2) {
            }
            if (c.a(AdManager.LOG, 5)) {
                float n = j.n();
                Log.w(AdManager.LOG, "request timed out (client timeout" + (n > 0.0f ? ": " + n : "") + ")");
            }
            interstitialAd.c();
        }
    }

    static class a extends Thread {
        boolean a = false;
        private InterstitialAd b;
        private WeakReference<Context> c;

        public a(InterstitialAd interstitialAd, Context context) {
            this.b = interstitialAd;
            this.c = new WeakReference<>(context);
        }

        public final void run() {
            Context context = this.c.get();
            if (context != null) {
                try {
                    j a2 = b.a(this.b.f(), context, this.b.getKeywords(), this.b.getSearchQuery(), this.b.e());
                    if (!this.a && a2 == null) {
                        this.b.c();
                    }
                } catch (Exception e) {
                    if (c.a(AdManager.LOG, 6)) {
                        Log.e(AdManager.LOG, "Unhandled exception requesting a fresh ad.", e);
                    }
                    if (!this.a) {
                        this.b.c();
                    }
                }
            } else if (!this.a) {
                this.b.c();
            }
        }
    }

    public InterstitialAd(Event event, InterstitialAdListener interstitialAdListener) {
        this.d = event;
        this.e = new WeakReference<>(interstitialAdListener);
        if (a == null) {
            a = new Handler();
        }
    }

    public void setListener(InterstitialAdListener interstitialAdListener) {
        this.e = new WeakReference<>(interstitialAdListener);
    }

    static class e implements Runnable {
        private WeakReference<InterstitialAd> a;

        public e(InterstitialAd interstitialAd) {
            this.a = new WeakReference<>(interstitialAd);
        }

        public final void run() {
            InterstitialAd interstitialAd = this.a.get();
            if (interstitialAd != null) {
                interstitialAd.b();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        if (a != null) {
            a.post(new e(this));
        }
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        g();
        if (this.l != -1 && c.a(AdManager.LOG, 2)) {
            Log.v(AdManager.LOG, "total request time: " + (SystemClock.uptimeMillis() - this.l));
        }
        this.f = true;
        c = null;
        InterstitialAdListener interstitialAdListener = this.e.get();
        if (interstitialAdListener != null) {
            try {
                interstitialAdListener.onReceiveInterstitial(this);
            } catch (Exception e2) {
                Log.w(AdManager.LOG, "Unhandled exception raised in your InterstitialAdListener.onReceiveInterstitial.", e2);
            }
        }
    }

    static class b implements Runnable {
        private WeakReference<InterstitialAd> a;

        public b(InterstitialAd interstitialAd) {
            this.a = new WeakReference<>(interstitialAd);
        }

        public final void run() {
            InterstitialAd interstitialAd = this.a.get();
            if (interstitialAd != null) {
                interstitialAd.d();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        if (a != null) {
            a.post(new b(this));
        }
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        c = null;
        InterstitialAdListener interstitialAdListener = this.e.get();
        if (interstitialAdListener != null) {
            try {
                interstitialAdListener.onFailedToReceiveInterstitial(this);
            } catch (Exception e2) {
                Log.w(AdManager.LOG, "Unhandled exception raised in your InterstitialAdListener.onFailedToReceiveInterstitial.", e2);
            }
        }
    }

    public void requestAd(Context context) {
        if (h()) {
            if (c.a(AdManager.LOG, 6)) {
                Log.e(AdManager.LOG, "A request is already in progress.  This request will fail.");
            }
            c();
            return;
        }
        a aVar = new a(this, context);
        c = aVar;
        aVar.start();
        this.l = SystemClock.uptimeMillis();
        float a2 = j.a(context);
        if (a2 > 0.0f) {
            f fVar = new f(this);
            if (b == null) {
                b = new Timer();
            }
            b.schedule(fVar, (long) (a2 * 1000.0f));
        }
    }

    static class f extends TimerTask {
        private WeakReference<InterstitialAd> a;

        public f(InterstitialAd interstitialAd) {
            this.a = new WeakReference<>(interstitialAd);
        }

        public final void run() {
            InterstitialAd interstitialAd = this.a.get();
            if (interstitialAd != null) {
                InterstitialAd.a(interstitialAd);
            }
        }
    }

    private static void g() {
        if (b != null) {
            b.cancel();
            b = null;
        }
    }

    public void show(Activity activity) {
        a.post(new d(activity, this));
    }

    static class d implements Runnable {
        private WeakReference<Activity> a;
        private WeakReference<InterstitialAd> b;

        public d(Activity activity, InterstitialAd interstitialAd) {
            this.a = new WeakReference<>(activity);
            this.b = new WeakReference<>(interstitialAd);
        }

        public final void run() {
            Activity activity = this.a.get();
            InterstitialAd interstitialAd = this.b.get();
            if (activity != null && interstitialAd != null) {
                interstitialAd.a(activity);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(Activity activity) {
        if (this.f) {
            this.g = true;
            this.f = false;
            q a2 = this.h.a();
            if (a2 != null) {
                PackageManager packageManager = activity.getPackageManager();
                Iterator<Intent> it = a2.b.iterator();
                while (it.hasNext()) {
                    Intent next = it.next();
                    if (packageManager.resolveActivity(next, 65536) != null) {
                        try {
                            activity.startActivityForResult(next, 0);
                            return;
                        } catch (Exception e2) {
                        }
                    }
                }
                if (c.a(AdManager.LOG, 6)) {
                    Log.e(AdManager.LOG, "Could not find a resolving intent on ad click");
                }
            }
        } else if (this.g) {
            if (c.a(AdManager.LOG, 6)) {
                Log.e(AdManager.LOG, "Show has already been called.  Please create and request a new interstitial");
            }
        } else if (c.a(AdManager.LOG, 6)) {
            Log.e(AdManager.LOG, "Cannot call show before interstitial is ready");
        }
    }

    /* access modifiers changed from: package-private */
    public final Event e() {
        return this.d;
    }

    public boolean isReady() {
        return this.f;
    }

    private static boolean h() {
        return c != null;
    }

    public String getKeywords() {
        return this.j;
    }

    public String getSearchQuery() {
        return this.i;
    }

    public void setKeywords(String str) {
        this.j = str;
    }

    public void setSearchQuery(String str) {
        this.i = str;
    }

    /* access modifiers changed from: package-private */
    public final c f() {
        return this.k;
    }
}
