package net.rbgrn.android.glwallpaperservice;

import android.view.SurfaceHolder;
import java.util.ArrayList;
import min3d.core.Renderer;

/* compiled from: GLWallpaperService */
class GLThread extends Thread {
    public static final int DEBUG_CHECK_GL_ERROR = 1;
    public static final int DEBUG_LOG_GL_CALLS = 2;
    private static final boolean LOG_THREADS = false;
    public boolean mDone = false;
    private EGLConfigChooser mEGLConfigChooser;
    private EGLContextFactory mEGLContextFactory;
    private EGLWindowSurfaceFactory mEGLWindowSurfaceFactory;
    private EglHelper mEglHelper;
    /* access modifiers changed from: private */
    public GLThread mEglOwner;
    private ArrayList<Runnable> mEventQueue = new ArrayList<>();
    private boolean mEventsWaiting;
    private GLWrapper mGLWrapper;
    private boolean mHasSurface;
    private boolean mHaveEgl;
    private int mHeight = 0;
    public SurfaceHolder mHolder;
    private boolean mPaused;
    private int mRenderMode = 1;
    private Renderer mRenderer;
    private boolean mRequestRender = true;
    private boolean mSizeChanged = true;
    private boolean mWaitingForSurface;
    private int mWidth = 0;
    private final GLThreadManager sGLThreadManager = new GLThreadManager(this, null);

    GLThread(Renderer renderer, EGLConfigChooser chooser, EGLContextFactory contextFactory, EGLWindowSurfaceFactory surfaceFactory, GLWrapper wrapper) {
        this.mRenderer = renderer;
        this.mEGLConfigChooser = chooser;
        this.mEGLContextFactory = contextFactory;
        this.mEGLWindowSurfaceFactory = surfaceFactory;
        this.mGLWrapper = wrapper;
    }

    public void run() {
        setName("GLThread " + getId());
        try {
            guardedRun();
        } catch (InterruptedException e) {
        } finally {
            this.sGLThreadManager.threadExiting(this);
        }
    }

    private void stopEglLocked() {
        if (this.mHaveEgl) {
            this.mHaveEgl = false;
            this.mEglHelper.destroySurface();
            this.sGLThreadManager.releaseEglSurface(this);
        }
    }

    /* JADX WARN: Type inference failed for: r11v12, types: [javax.microedition.khronos.opengles.GL] */
    /* JADX WARNING: Code restructure failed: missing block: B:101:?, code lost:
        r4 = r0.mEglHelper.createSurface(r0.mHolder);
        r8 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x0198, code lost:
        if (r9 == false) goto L_0x01aa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x019a, code lost:
        r0.mRenderer.onSurfaceCreated(r4, r0.mEglHelper.mEglConfig);
        r9 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x01aa, code lost:
        if (r8 == false) goto L_0x01b5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x01ac, code lost:
        r0.mRenderer.onSurfaceChanged(r4, r10, r5);
        r8 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x01b5, code lost:
        if (r10 <= 0) goto L_0x0023;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x01b7, code lost:
        if (r5 <= 0) goto L_0x0023;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x01b9, code lost:
        r0.mRenderer.onDrawFrame(r4);
        r0.mEglHelper.swap();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x0023, code lost:
        continue;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:136:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00c8, code lost:
        if (r3 == false) goto L_0x017f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:?, code lost:
        r7 = getEvent();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00ce, code lost:
        if (r7 == null) goto L_0x0023;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00d0, code lost:
        r7.run();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00d7, code lost:
        if (isDone() == false) goto L_0x00ca;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00d9, code lost:
        r11 = r0.sGLThreadManager;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00de, code lost:
        monitor-enter(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        stopEglLocked();
        r0.mEglHelper.finish();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00ea, code lost:
        monitor-exit(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0124, code lost:
        r2 = r0.mSizeChanged;
        r10 = r0.mWidth;
        r5 = r0.mHeight;
        r1.mSizeChanged = false;
        r1.mRequestRender = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0144, code lost:
        if (r0.mHasSurface == false) goto L_0x00c7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x014b, code lost:
        if (r0.mWaitingForSurface == false) goto L_0x00c7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x014d, code lost:
        r2 = true;
        r1.mWaitingForSurface = false;
        r0.sGLThreadManager.notifyAll();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x017f, code lost:
        if (r6 == false) goto L_0x0183;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x0181, code lost:
        r9 = true;
        r2 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x0183, code lost:
        if (r2 == false) goto L_0x0198;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void guardedRun() throws java.lang.InterruptedException {
        /*
            r17 = this;
            r16 = 1
            net.rbgrn.android.glwallpaperservice.EglHelper r11 = new net.rbgrn.android.glwallpaperservice.EglHelper
            r0 = r17
            net.rbgrn.android.glwallpaperservice.EGLConfigChooser r0 = r0.mEGLConfigChooser
            r12 = r0
            r0 = r17
            net.rbgrn.android.glwallpaperservice.EGLContextFactory r0 = r0.mEGLContextFactory
            r13 = r0
            r0 = r17
            net.rbgrn.android.glwallpaperservice.EGLWindowSurfaceFactory r0 = r0.mEGLWindowSurfaceFactory
            r14 = r0
            r0 = r17
            net.rbgrn.android.glwallpaperservice.GLWrapper r0 = r0.mGLWrapper
            r15 = r0
            r11.<init>(r12, r13, r14, r15)
            r0 = r11
            r1 = r17
            r1.mEglHelper = r0
            r4 = 0
            r9 = 1
            r8 = 1
        L_0x0023:
            boolean r11 = r17.isDone()     // Catch:{ all -> 0x0161 }
            if (r11 == 0) goto L_0x003c
            r0 = r17
            net.rbgrn.android.glwallpaperservice.GLThread$GLThreadManager r0 = r0.sGLThreadManager
            r11 = r0
            monitor-enter(r11)
            r17.stopEglLocked()     // Catch:{ all -> 0x01ce }
            r0 = r17
            net.rbgrn.android.glwallpaperservice.EglHelper r0 = r0.mEglHelper     // Catch:{ all -> 0x01ce }
            r12 = r0
            r12.finish()     // Catch:{ all -> 0x01ce }
            monitor-exit(r11)     // Catch:{ all -> 0x01ce }
        L_0x003b:
            return
        L_0x003c:
            r10 = 0
            r5 = 0
            r2 = 0
            r6 = 0
            r3 = 0
            r0 = r17
            net.rbgrn.android.glwallpaperservice.GLThread$GLThreadManager r0 = r0.sGLThreadManager     // Catch:{ all -> 0x0161 }
            r11 = r0
            monitor-enter(r11)     // Catch:{ all -> 0x0161 }
        L_0x0047:
            r0 = r17
            boolean r0 = r0.mPaused     // Catch:{ all -> 0x015e }
            r12 = r0
            if (r12 == 0) goto L_0x0051
            r17.stopEglLocked()     // Catch:{ all -> 0x015e }
        L_0x0051:
            r0 = r17
            boolean r0 = r0.mHasSurface     // Catch:{ all -> 0x015e }
            r12 = r0
            if (r12 != 0) goto L_0x008e
            r0 = r17
            boolean r0 = r0.mWaitingForSurface     // Catch:{ all -> 0x015e }
            r12 = r0
            if (r12 != 0) goto L_0x0070
            r17.stopEglLocked()     // Catch:{ all -> 0x015e }
            r12 = 1
            r0 = r12
            r1 = r17
            r1.mWaitingForSurface = r0     // Catch:{ all -> 0x015e }
            r0 = r17
            net.rbgrn.android.glwallpaperservice.GLThread$GLThreadManager r0 = r0.sGLThreadManager     // Catch:{ all -> 0x015e }
            r12 = r0
            r12.notifyAll()     // Catch:{ all -> 0x015e }
        L_0x0070:
            r0 = r17
            boolean r0 = r0.mDone     // Catch:{ all -> 0x015e }
            r12 = r0
            if (r12 == 0) goto L_0x00b9
            monitor-exit(r11)     // Catch:{ all -> 0x015e }
            r0 = r17
            net.rbgrn.android.glwallpaperservice.GLThread$GLThreadManager r0 = r0.sGLThreadManager
            r11 = r0
            monitor-enter(r11)
            r17.stopEglLocked()     // Catch:{ all -> 0x008b }
            r0 = r17
            net.rbgrn.android.glwallpaperservice.EglHelper r0 = r0.mEglHelper     // Catch:{ all -> 0x008b }
            r12 = r0
            r12.finish()     // Catch:{ all -> 0x008b }
            monitor-exit(r11)     // Catch:{ all -> 0x008b }
            goto L_0x003b
        L_0x008b:
            r12 = move-exception
            monitor-exit(r11)     // Catch:{ all -> 0x008b }
            throw r12
        L_0x008e:
            r0 = r17
            boolean r0 = r0.mHaveEgl     // Catch:{ all -> 0x015e }
            r12 = r0
            if (r12 != 0) goto L_0x0070
            r0 = r17
            net.rbgrn.android.glwallpaperservice.GLThread$GLThreadManager r0 = r0.sGLThreadManager     // Catch:{ all -> 0x015e }
            r12 = r0
            r0 = r12
            r1 = r17
            boolean r12 = r0.tryAcquireEglSurface(r1)     // Catch:{ all -> 0x015e }
            if (r12 == 0) goto L_0x0070
            r12 = 1
            r0 = r12
            r1 = r17
            r1.mHaveEgl = r0     // Catch:{ all -> 0x015e }
            r0 = r17
            net.rbgrn.android.glwallpaperservice.EglHelper r0 = r0.mEglHelper     // Catch:{ all -> 0x015e }
            r12 = r0
            r12.start()     // Catch:{ all -> 0x015e }
            r12 = 1
            r0 = r12
            r1 = r17
            r1.mRequestRender = r0     // Catch:{ all -> 0x015e }
            r6 = 1
            goto L_0x0070
        L_0x00b9:
            r0 = r17
            boolean r0 = r0.mEventsWaiting     // Catch:{ all -> 0x015e }
            r12 = r0
            if (r12 == 0) goto L_0x00f0
            r3 = 1
            r12 = 0
            r0 = r12
            r1 = r17
            r1.mEventsWaiting = r0     // Catch:{ all -> 0x015e }
        L_0x00c7:
            monitor-exit(r11)     // Catch:{ all -> 0x015e }
            if (r3 == 0) goto L_0x017f
        L_0x00ca:
            java.lang.Runnable r7 = r17.getEvent()     // Catch:{ all -> 0x0161 }
            if (r7 == 0) goto L_0x0023
            r7.run()     // Catch:{ all -> 0x0161 }
            boolean r11 = r17.isDone()     // Catch:{ all -> 0x0161 }
            if (r11 == 0) goto L_0x00ca
            r0 = r17
            net.rbgrn.android.glwallpaperservice.GLThread$GLThreadManager r0 = r0.sGLThreadManager
            r11 = r0
            monitor-enter(r11)
            r17.stopEglLocked()     // Catch:{ all -> 0x00ed }
            r0 = r17
            net.rbgrn.android.glwallpaperservice.EglHelper r0 = r0.mEglHelper     // Catch:{ all -> 0x00ed }
            r12 = r0
            r12.finish()     // Catch:{ all -> 0x00ed }
            monitor-exit(r11)     // Catch:{ all -> 0x00ed }
            goto L_0x003b
        L_0x00ed:
            r12 = move-exception
            monitor-exit(r11)     // Catch:{ all -> 0x00ed }
            throw r12
        L_0x00f0:
            r0 = r17
            boolean r0 = r0.mPaused     // Catch:{ all -> 0x015e }
            r12 = r0
            if (r12 != 0) goto L_0x0175
            r0 = r17
            boolean r0 = r0.mHasSurface     // Catch:{ all -> 0x015e }
            r12 = r0
            if (r12 == 0) goto L_0x0175
            r0 = r17
            boolean r0 = r0.mHaveEgl     // Catch:{ all -> 0x015e }
            r12 = r0
            if (r12 == 0) goto L_0x0175
            r0 = r17
            int r0 = r0.mWidth     // Catch:{ all -> 0x015e }
            r12 = r0
            if (r12 <= 0) goto L_0x0175
            r0 = r17
            int r0 = r0.mHeight     // Catch:{ all -> 0x015e }
            r12 = r0
            if (r12 <= 0) goto L_0x0175
            r0 = r17
            boolean r0 = r0.mRequestRender     // Catch:{ all -> 0x015e }
            r12 = r0
            if (r12 != 0) goto L_0x0124
            r0 = r17
            int r0 = r0.mRenderMode     // Catch:{ all -> 0x015e }
            r12 = r0
            r0 = r12
            r1 = r16
            if (r0 != r1) goto L_0x0175
        L_0x0124:
            r0 = r17
            boolean r0 = r0.mSizeChanged     // Catch:{ all -> 0x015e }
            r2 = r0
            r0 = r17
            int r0 = r0.mWidth     // Catch:{ all -> 0x015e }
            r10 = r0
            r0 = r17
            int r0 = r0.mHeight     // Catch:{ all -> 0x015e }
            r5 = r0
            r12 = 0
            r0 = r12
            r1 = r17
            r1.mSizeChanged = r0     // Catch:{ all -> 0x015e }
            r12 = 0
            r0 = r12
            r1 = r17
            r1.mRequestRender = r0     // Catch:{ all -> 0x015e }
            r0 = r17
            boolean r0 = r0.mHasSurface     // Catch:{ all -> 0x015e }
            r12 = r0
            if (r12 == 0) goto L_0x00c7
            r0 = r17
            boolean r0 = r0.mWaitingForSurface     // Catch:{ all -> 0x015e }
            r12 = r0
            if (r12 == 0) goto L_0x00c7
            r2 = 1
            r12 = 0
            r0 = r12
            r1 = r17
            r1.mWaitingForSurface = r0     // Catch:{ all -> 0x015e }
            r0 = r17
            net.rbgrn.android.glwallpaperservice.GLThread$GLThreadManager r0 = r0.sGLThreadManager     // Catch:{ all -> 0x015e }
            r12 = r0
            r12.notifyAll()     // Catch:{ all -> 0x015e }
            goto L_0x00c7
        L_0x015e:
            r12 = move-exception
            monitor-exit(r11)     // Catch:{ all -> 0x015e }
            throw r12     // Catch:{ all -> 0x0161 }
        L_0x0161:
            r11 = move-exception
            r0 = r17
            net.rbgrn.android.glwallpaperservice.GLThread$GLThreadManager r0 = r0.sGLThreadManager
            r12 = r0
            monitor-enter(r12)
            r17.stopEglLocked()     // Catch:{ all -> 0x01cb }
            r0 = r17
            net.rbgrn.android.glwallpaperservice.EglHelper r0 = r0.mEglHelper     // Catch:{ all -> 0x01cb }
            r13 = r0
            r13.finish()     // Catch:{ all -> 0x01cb }
            monitor-exit(r12)     // Catch:{ all -> 0x01cb }
            throw r11
        L_0x0175:
            r0 = r17
            net.rbgrn.android.glwallpaperservice.GLThread$GLThreadManager r0 = r0.sGLThreadManager     // Catch:{ all -> 0x015e }
            r12 = r0
            r12.wait()     // Catch:{ all -> 0x015e }
            goto L_0x0047
        L_0x017f:
            if (r6 == 0) goto L_0x0183
            r9 = 1
            r2 = 1
        L_0x0183:
            if (r2 == 0) goto L_0x0198
            r0 = r17
            net.rbgrn.android.glwallpaperservice.EglHelper r0 = r0.mEglHelper     // Catch:{ all -> 0x0161 }
            r11 = r0
            r0 = r17
            android.view.SurfaceHolder r0 = r0.mHolder     // Catch:{ all -> 0x0161 }
            r12 = r0
            javax.microedition.khronos.opengles.GL r11 = r11.createSurface(r12)     // Catch:{ all -> 0x0161 }
            r0 = r11
            javax.microedition.khronos.opengles.GL10 r0 = (javax.microedition.khronos.opengles.GL10) r0     // Catch:{ all -> 0x0161 }
            r4 = r0
            r8 = 1
        L_0x0198:
            if (r9 == 0) goto L_0x01aa
            r0 = r17
            min3d.core.Renderer r0 = r0.mRenderer     // Catch:{ all -> 0x0161 }
            r11 = r0
            r0 = r17
            net.rbgrn.android.glwallpaperservice.EglHelper r0 = r0.mEglHelper     // Catch:{ all -> 0x0161 }
            r12 = r0
            javax.microedition.khronos.egl.EGLConfig r12 = r12.mEglConfig     // Catch:{ all -> 0x0161 }
            r11.onSurfaceCreated(r4, r12)     // Catch:{ all -> 0x0161 }
            r9 = 0
        L_0x01aa:
            if (r8 == 0) goto L_0x01b5
            r0 = r17
            min3d.core.Renderer r0 = r0.mRenderer     // Catch:{ all -> 0x0161 }
            r11 = r0
            r11.onSurfaceChanged(r4, r10, r5)     // Catch:{ all -> 0x0161 }
            r8 = 0
        L_0x01b5:
            if (r10 <= 0) goto L_0x0023
            if (r5 <= 0) goto L_0x0023
            r0 = r17
            min3d.core.Renderer r0 = r0.mRenderer     // Catch:{ all -> 0x0161 }
            r11 = r0
            r11.onDrawFrame(r4)     // Catch:{ all -> 0x0161 }
            r0 = r17
            net.rbgrn.android.glwallpaperservice.EglHelper r0 = r0.mEglHelper     // Catch:{ all -> 0x0161 }
            r11 = r0
            r11.swap()     // Catch:{ all -> 0x0161 }
            goto L_0x0023
        L_0x01cb:
            r11 = move-exception
            monitor-exit(r12)     // Catch:{ all -> 0x01cb }
            throw r11
        L_0x01ce:
            r12 = move-exception
            monitor-exit(r11)     // Catch:{ all -> 0x01ce }
            throw r12
        */
        throw new UnsupportedOperationException("Method not decompiled: net.rbgrn.android.glwallpaperservice.GLThread.guardedRun():void");
    }

    private boolean isDone() {
        boolean z;
        synchronized (this.sGLThreadManager) {
            z = this.mDone;
        }
        return z;
    }

    public void setRenderMode(int renderMode) {
        if (renderMode < 0 || renderMode > 1) {
            throw new IllegalArgumentException("renderMode");
        }
        synchronized (this.sGLThreadManager) {
            this.mRenderMode = renderMode;
            if (renderMode == 1) {
                this.sGLThreadManager.notifyAll();
            }
        }
    }

    public int getRenderMode() {
        int i;
        synchronized (this.sGLThreadManager) {
            i = this.mRenderMode;
        }
        return i;
    }

    public void requestRender() {
        synchronized (this.sGLThreadManager) {
            this.mRequestRender = true;
            this.sGLThreadManager.notifyAll();
        }
    }

    public void surfaceCreated(SurfaceHolder holder) {
        this.mHolder = holder;
        synchronized (this.sGLThreadManager) {
            this.mHasSurface = true;
            this.sGLThreadManager.notifyAll();
        }
    }

    public void surfaceDestroyed() {
        synchronized (this.sGLThreadManager) {
            this.mHasSurface = false;
            this.sGLThreadManager.notifyAll();
            while (!this.mWaitingForSurface && isAlive() && !this.mDone) {
                try {
                    this.sGLThreadManager.wait();
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        }
    }

    public void onPause() {
        synchronized (this.sGLThreadManager) {
            this.mPaused = true;
            this.sGLThreadManager.notifyAll();
        }
    }

    public void onResume() {
        synchronized (this.sGLThreadManager) {
            this.mPaused = false;
            this.mRequestRender = true;
            this.sGLThreadManager.notifyAll();
        }
    }

    public void onWindowResize(int w, int h) {
        synchronized (this.sGLThreadManager) {
            this.mWidth = w;
            this.mHeight = h;
            this.mSizeChanged = true;
            this.sGLThreadManager.notifyAll();
        }
    }

    public void requestExitAndWait() {
        synchronized (this.sGLThreadManager) {
            this.mDone = true;
            this.sGLThreadManager.notifyAll();
        }
        try {
            join();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    public void queueEvent(Runnable r) {
        synchronized (this) {
            this.mEventQueue.add(r);
            synchronized (this.sGLThreadManager) {
                this.mEventsWaiting = true;
                this.sGLThreadManager.notifyAll();
            }
        }
    }

    private Runnable getEvent() {
        synchronized (this) {
            if (this.mEventQueue.size() <= 0) {
                return null;
            }
            Runnable remove = this.mEventQueue.remove(0);
            return remove;
        }
    }

    /* compiled from: GLWallpaperService */
    private class GLThreadManager {
        private GLThreadManager() {
        }

        /* synthetic */ GLThreadManager(GLThread gLThread, GLThreadManager gLThreadManager) {
            this();
        }

        public synchronized void threadExiting(GLThread thread) {
            thread.mDone = true;
            if (GLThread.this.mEglOwner == thread) {
                GLThread.this.mEglOwner = null;
            }
            notifyAll();
        }

        public synchronized boolean tryAcquireEglSurface(GLThread thread) {
            boolean z;
            if (GLThread.this.mEglOwner == thread || GLThread.this.mEglOwner == null) {
                GLThread.this.mEglOwner = thread;
                notifyAll();
                z = true;
            } else {
                z = false;
            }
            return z;
        }

        public synchronized void releaseEglSurface(GLThread thread) {
            if (GLThread.this.mEglOwner == thread) {
                GLThread.this.mEglOwner = null;
            }
            notifyAll();
        }
    }
}
