package com.oslwp.clover3d;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class Settings extends PreferenceActivity {
    public static int bgcolor = -16777216;
    public static int canscroll = 0;
    public static int fogfar = 80;
    public static int fognear = 10;
    public static int fogon = 1;
    public static int hascube = 1;
    public static float objmaxrotate = 2.0f;
    public static float objmaxsize = 2.0f;
    public static float objmaxspeed = 0.1f;
    public static float objminrotate = -2.0f;
    public static float objminsize = 1.0f;
    public static float objminspeed = 0.05f;
    public static int objnum = 10;
    public static String sbgcolor = "FFFFFF";
    public static String scanscroll = "0";
    public static String sfogfar = "80";
    public static String sfognear = "10";
    public static String sfogon = "0";
    public static String shascube = "S_HAS_CUBE";
    public static String sobjmaxrotate = "2";
    public static String sobjmaxsize = "2";
    public static String sobjmaxspeed = "0.2";
    public static String sobjminrotate = "-2";
    public static String sobjminsize = "2";
    public static String sobjminspeed = "0.05";
    public static String sobjnum = "10";
    public static int spincube = 1;
    public static String sspincube = "1";
    public static String stunneldepth = "40";
    public static String stunnelwide = "12";
    public static String swideangle = "8";
    public static int tunneldepth = 40;
    public static int tunnelwide = 12;
    public static int wideangle = 15;
    private AdView googleAd;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        boolean z;
        boolean z2;
        boolean z3;
        super.onCreate(bundle);
        getPreferenceManager().setSharedPreferencesName(MyWallpaperService.preferenceName);
        addPreferencesFromResource(R.xml.settings);
        setContentView((int) R.layout.settings);
        Preference pref = findPreference("more");
        if (pref != null) {
            pref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    Settings.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://android.ownskin.com/lwp_phone_moredownload.jsp?t=flybox")));
                    return false;
                }
            });
        }
        Preference pref2 = findPreference("fogon");
        if (pref2 != null) {
            CheckBoxPreference cb = (CheckBoxPreference) pref2;
            if (fogon == 1) {
                z3 = true;
            } else {
                z3 = false;
            }
            cb.setChecked(z3);
        }
        Preference pref3 = findPreference("canscroll");
        if (pref3 != null) {
            CheckBoxPreference cb2 = (CheckBoxPreference) pref3;
            if (canscroll == 1) {
                z2 = true;
            } else {
                z2 = false;
            }
            cb2.setChecked(z2);
        }
        Preference pref4 = findPreference("spincube");
        if (pref4 != null) {
            CheckBoxPreference cb3 = (CheckBoxPreference) pref4;
            if (spincube == 1) {
                z = true;
            } else {
                z = false;
            }
            cb3.setChecked(z);
        }
        this.googleAd = (AdView) findViewById(R.id.adView);
        this.googleAd.loadAd(new AdRequest());
    }

    public static void load(SharedPreferences pref) {
        boolean z;
        boolean z2;
        try {
            objnum = Integer.parseInt(pref.getString("objnum", sobjnum));
        } catch (Exception e) {
        }
        try {
            objmaxsize = Float.parseFloat(pref.getString("objmaxsize", sobjmaxsize));
        } catch (Exception e2) {
        }
        try {
            objmaxspeed = Float.parseFloat(pref.getString("objmaxspeed", sobjmaxspeed));
        } catch (Exception e3) {
        }
        try {
            fogon = pref.getBoolean("fogon", fogon == 1) ? 1 : 0;
        } catch (Exception e4) {
        }
        try {
            bgcolor = Integer.parseInt(pref.getString("bgcolor", sbgcolor), 16);
        } catch (Exception e5) {
        }
        try {
            if (canscroll == 1) {
                z2 = true;
            } else {
                z2 = false;
            }
            canscroll = pref.getBoolean("canscroll", z2) ? 1 : 0;
        } catch (Exception e6) {
        }
        try {
            if (spincube == 1) {
                z = true;
            } else {
                z = false;
            }
            spincube = pref.getBoolean("spincube", z) ? 1 : 0;
        } catch (Exception e7) {
        }
        if (objmaxsize < objminsize) {
            objminsize = objmaxsize;
        }
        if (objmaxspeed < objminspeed) {
            objminspeed = objmaxspeed;
        }
        print();
    }

    public static void loadDefault() {
        try {
            tunneldepth = Integer.parseInt(stunneldepth);
        } catch (Exception e) {
        }
        try {
            tunnelwide = Integer.parseInt(stunnelwide);
        } catch (Exception e2) {
        }
        try {
            wideangle = Integer.parseInt(swideangle);
        } catch (Exception e3) {
        }
        try {
            objnum = Integer.parseInt(sobjnum);
        } catch (Exception e4) {
        }
        try {
            objminsize = Float.parseFloat(sobjminsize);
        } catch (Exception e5) {
        }
        try {
            objmaxsize = Float.parseFloat(sobjmaxsize);
        } catch (Exception e6) {
        }
        try {
            objminspeed = Float.parseFloat(sobjminspeed);
        } catch (Exception e7) {
        }
        try {
            objmaxspeed = Float.parseFloat(sobjmaxspeed);
        } catch (Exception e8) {
        }
        try {
            objminrotate = Float.parseFloat(sobjminrotate);
        } catch (Exception e9) {
        }
        try {
            objmaxrotate = Float.parseFloat(sobjmaxrotate);
        } catch (Exception e10) {
        }
        try {
            bgcolor = Integer.parseInt(sbgcolor, 16);
        } catch (Exception e11) {
        }
        try {
            fogon = Integer.parseInt(sfogon);
        } catch (Exception e12) {
        }
        try {
            fogfar = Integer.parseInt(sfogfar);
        } catch (Exception e13) {
        }
        try {
            fognear = Integer.parseInt(sfognear);
        } catch (Exception e14) {
        }
        try {
            canscroll = Integer.parseInt(scanscroll);
        } catch (Exception e15) {
        }
        try {
            hascube = Integer.parseInt(shascube);
        } catch (Exception e16) {
        }
        try {
            spincube = Integer.parseInt(sspincube);
        } catch (Exception e17) {
        }
        print();
    }

    private static void print() {
    }

    public static int getInt() {
        return 877;
    }
}
