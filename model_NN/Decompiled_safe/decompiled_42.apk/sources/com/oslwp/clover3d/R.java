package com.oslwp.clover3d;

public final class R {

    public static final class array {
        public static final int bgcolor_entries = 2131034118;
        public static final int bgcolor_entryvalues = 2131034119;
        public static final int objmaxsize_entries = 2131034114;
        public static final int objmaxsize_entryvalues = 2131034115;
        public static final int objmaxspeed_entries = 2131034116;
        public static final int objmaxspeed_entryvalues = 2131034117;
        public static final int objnum_entries = 2131034112;
        public static final int objnum_entryvalues = 2131034113;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int icon = 2130837504;
    }

    public static final class id {
        public static final int adView = 2131165185;
        public static final int intro = 2131165186;
        public static final int lwpicon = 2131165184;
        public static final int more = 2131165189;
        public static final int options = 2131165187;
        public static final int use = 2131165188;
    }

    public static final class layout {
        public static final int launcher = 2130903040;
        public static final int settings = 2130903041;
    }

    public static final class string {
        public static final int app_name = 2131099648;
        public static final int bgcolor_summary = 2131099667;
        public static final int bgcolor_title = 2131099666;
        public static final int canscroll_summary = 2131099669;
        public static final int canscroll_title = 2131099668;
        public static final int description = 2131099649;
        public static final int flybox_settings = 2131099657;
        public static final int fogon_summary = 2131099665;
        public static final int fogon_title = 2131099664;
        public static final int intro = 2131099650;
        public static final int more = 2131099654;
        public static final int more_summary = 2131099656;
        public static final int more_title = 2131099655;
        public static final int nolwpsupport = 2131099672;
        public static final int objmaxsize_summary = 2131099661;
        public static final int objmaxsize_title = 2131099660;
        public static final int objmaxspeed_summary = 2131099663;
        public static final int objmaxspeed_title = 2131099662;
        public static final int objnum_summary = 2131099659;
        public static final int objnum_title = 2131099658;
        public static final int options = 2131099652;
        public static final int spincube_summary = 2131099671;
        public static final int spincube_title = 2131099670;
        public static final int use = 2131099653;
        public static final int useprompt = 2131099651;
    }

    public static final class xml {
        public static final int glwallpaper = 2130968576;
        public static final int settings = 2130968577;
    }
}
