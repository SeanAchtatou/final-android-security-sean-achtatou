package min3d.parser;

public class ParseObjectFace {
    public int faceLength;
    public boolean hasn;
    public boolean hasuv;
    public String materialKey;
    public int[] n;
    public int[] uv;
    public int[] v;
}
