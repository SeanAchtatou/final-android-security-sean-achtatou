package min3d.vos;

public enum LightType {
    DIRECTIONAL(0.0f),
    POSITIONAL(1.0f);
    
    private final float _glValue;

    private LightType(float $glValue) {
        this._glValue = $glValue;
    }

    public float glValue() {
        return this._glValue;
    }
}
