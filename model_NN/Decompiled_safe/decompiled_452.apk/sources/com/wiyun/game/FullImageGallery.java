package com.wiyun.game;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.wiyun.game.model.a.n;
import com.wiyun.game.widget.CellLayout;
import com.wiyun.game.widget.Workspace;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FullImageGallery extends Activity implements View.OnClickListener, View.OnLongClickListener, Animation.AnimationListener {
    private List<n> a;
    private int b;
    /* access modifiers changed from: private */
    public View c;
    private Workspace d;
    /* access modifiers changed from: private */
    public Map<String, View> e;
    /* access modifiers changed from: private */
    public Map<String, Bitmap> f;
    /* access modifiers changed from: private */
    public Animation g;
    /* access modifiers changed from: private */
    public Animation h;
    /* access modifiers changed from: private */
    public boolean i;
    private Handler j = new o(this);
    private BroadcastReceiver k = new n(this);

    private void a() {
        Intent intent = getIntent();
        this.a = (List) intent.getSerializableExtra("screenshots");
        this.b = intent.getIntExtra("start", 0);
        this.e = new HashMap();
        this.f = new HashMap();
        this.g = new AlphaAnimation(1.0f, 0.0f);
        this.g.setDuration(300);
        this.g.setFillAfter(true);
        this.h = new AlphaAnimation(0.0f, 1.0f);
        this.h.setDuration(300);
        this.h.setFillAfter(true);
        this.h.setAnimationListener(this);
        registerReceiver(this.k, new IntentFilter("com.wiyun.game.IMAGE_DOWNLOADED"));
    }

    private void b() {
        this.d = (Workspace) findViewById(t.d("wy_workspace"));
        this.c = findViewById(t.d("wy_ll_footer"));
        this.i = true;
        this.j.removeMessages(1);
        this.j.sendEmptyMessageDelayed(1, 3000);
        int size = this.a.size() - 1;
        while (true) {
            int i2 = size - 1;
            if (size <= 0) {
                break;
            }
            this.d.a();
            size = i2;
        }
        LayoutInflater from = LayoutInflater.from(this);
        int b2 = this.d.b();
        for (int i3 = 0; i3 < b2; i3++) {
            View inflate = from.inflate(t.e("wy_view_full_image_panel"), (ViewGroup) null);
            CellLayout a2 = this.d.a(i3);
            a2.setOnLongClickListener(this);
            a2.addView(inflate);
            n nVar = this.a.get(i3);
            inflate.setTag(nVar);
            this.e.put(h.b("bscr_", nVar.a()), inflate);
        }
        this.d.b(this.b);
        ((Button) findViewById(t.d("wy_b_back"))).setOnClickListener(this);
        ((Button) findViewById(t.d("wy_b_prev"))).setOnClickListener(this);
        ((Button) findViewById(t.d("wy_b_next"))).setOnClickListener(this);
    }

    private void c() {
        for (Bitmap bitmap : this.f.values()) {
            if (bitmap != null && !bitmap.isRecycled()) {
                bitmap.recycle();
            }
        }
        this.f.clear();
    }

    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == 0) {
            if (!this.i) {
                this.c.clearAnimation();
                this.j.removeMessages(2);
                this.j.removeMessages(1);
                this.j.sendEmptyMessage(2);
                this.j.sendEmptyMessageDelayed(1, 3000);
            } else {
                this.j.removeMessages(1);
                this.j.sendEmptyMessageDelayed(1, 3000);
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    public void onAnimationEnd(Animation arg0) {
        this.i = true;
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id == t.d("wy_b_back")) {
            finish();
        } else if (id == t.d("wy_b_prev")) {
            if (!this.d.a(false)) {
                Toast.makeText(this, t.f("wy_toast_already_first"), 0).show();
            }
        } else if (id == t.d("wy_b_next") && !this.d.b(false)) {
            Toast.makeText(this, t.f("wy_toast_already_last"), 0).show();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().addFlags(1024);
        setContentView(t.e("wy_activity_full_image_gallery"));
        a();
        b();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.d.e();
        this.h = null;
        this.g = null;
        unregisterReceiver(this.k);
        WiGame.l();
        c();
        super.onDestroy();
    }

    public boolean onLongClick(View v) {
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wiyun.game.h.a(java.util.Map<java.lang.String, android.graphics.Bitmap>, boolean, java.lang.String, java.lang.String):android.graphics.Bitmap
     arg types: [java.util.Map<java.lang.String, android.graphics.Bitmap>, int, java.lang.String, java.lang.String]
     candidates:
      com.wiyun.game.h.a(double, double, double, double):double
      com.wiyun.game.h.a(android.content.Context, java.lang.String, boolean, int):void
      com.wiyun.game.h.a(java.util.Map<java.lang.String, android.graphics.Bitmap>, boolean, java.lang.String, java.lang.String):android.graphics.Bitmap */
    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        for (Map.Entry<String, View> entry : this.e.entrySet()) {
            View panel = (View) entry.getValue();
            Bitmap bitmap = h.a(this.f, false, "bscr_", ((n) panel.getTag()).a());
            ((ImageView) panel.findViewById(t.d("wy_image"))).setImageBitmap(bitmap);
            panel.findViewById(t.d("wy_panel")).setVisibility(bitmap == null ? 0 : 8);
        }
    }

    public boolean onSearchRequested() {
        return false;
    }
}
