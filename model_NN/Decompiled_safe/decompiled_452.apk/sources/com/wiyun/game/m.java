package com.wiyun.game;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

class m implements LocationListener {
    m() {
    }

    public void onLocationChanged(Location location) {
        WiGame.O = location;
        if (location.hasAccuracy() && location.getAccuracy() < 1000.0f && WiGame.S != null) {
            ((LocationManager) WiGame.S.getSystemService("location")).removeUpdates(this);
        }
        WiGame.af();
    }

    public void onProviderDisabled(String provider) {
    }

    public void onProviderEnabled(String provider) {
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
    }
}
