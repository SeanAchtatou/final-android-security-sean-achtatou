package com.wiyun.game;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

class d implements ViewSwitcher.ViewFactory {
    private TextSwitcher a;
    private String b;
    private String[] c;
    private int d;
    private long e;
    private Drawable f;
    private Handler g = new p(this);

    public d(TextSwitcher switcher) {
        this.a = switcher;
        this.a.setFactory(this);
        this.f = t.g("wy_icon_notice");
    }

    /* access modifiers changed from: private */
    public synchronized void c() {
        String notice = null;
        if (this.c != null && this.d < this.c.length) {
            String[] strArr = this.c;
            int i = this.d;
            this.d = i + 1;
            notice = strArr[i];
        }
        if (TextUtils.isEmpty(notice)) {
            this.c = null;
            b();
        } else if (this.a != null) {
            if (!TextUtils.equals(this.b, notice)) {
                ((TextView) this.a.getNextView()).setCompoundDrawablesWithIntrinsicBounds(this.f, (Drawable) null, (Drawable) null, (Drawable) null);
                this.a.setText(notice);
                this.b = notice;
                this.e = System.currentTimeMillis();
            }
            this.g.removeMessages(1);
            this.g.sendEmptyMessageDelayed(1, 5000);
        }
    }

    /* access modifiers changed from: private */
    public synchronized void d() {
        if (this.a != null) {
            String name = String.format(t.h("wy_label_x_welcome_to_wiyun"), WiGame.getMyName());
            if (!TextUtils.equals(this.b, name)) {
                ((TextView) this.a.getNextView()).setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                this.a.setText(name);
                this.b = name;
                this.e = System.currentTimeMillis();
            }
        }
    }

    public void a() {
        this.a = null;
        this.f.setCallback(null);
        this.f = null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public synchronized void a(String[] notices) {
        if (notices != null) {
            if (notices.length != 0) {
                this.c = notices;
                this.d = 0;
                long interval = Math.min(Math.max(0L, 5000 - (System.currentTimeMillis() - this.e)), 5000L);
                this.g.removeMessages(1);
                this.g.sendEmptyMessageDelayed(1, interval);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public synchronized void b() {
        if (this.c == null) {
            long interval = Math.min(Math.max(0L, 5000 - (System.currentTimeMillis() - this.e)), 5000L);
            if (interval == 0) {
                this.g.removeMessages(2);
                this.g.sendEmptyMessageDelayed(2, interval);
            } else {
                d();
            }
        }
    }

    public View makeView() {
        if (this.a == null) {
            return null;
        }
        Context context = this.a.getContext();
        TextView t = new TextView(context);
        t.setGravity(17);
        t.setTextAppearance(context, 16973894);
        t.setTextColor(t.i("wy_notice_bar_text_color"));
        return t;
    }
}
