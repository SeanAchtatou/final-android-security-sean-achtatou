package com.wiyun.game.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;

public class WYWebView extends WebView {
    public WYWebView(Context context) {
        super(context);
    }

    public WYWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public WYWebView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /* access modifiers changed from: protected */
    public int computeVerticalScrollRange() {
        return (super.computeVerticalScrollRange() * 5) / 3;
    }
}
