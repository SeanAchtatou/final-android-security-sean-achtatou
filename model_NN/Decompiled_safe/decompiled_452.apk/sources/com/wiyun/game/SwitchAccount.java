package com.wiyun.game;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.wiyun.game.SingleListActivity;
import com.wiyun.game.b.e;
import com.wiyun.game.model.a.ag;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SwitchAccount extends SingleListActivity {
    private List<ag> e;
    private boolean f;
    private long g;
    /* access modifiers changed from: private */
    public ViewGroup h;
    /* access modifiers changed from: private */
    public View i;
    private TextView j;
    /* access modifiers changed from: private */
    public boolean k;
    /* access modifiers changed from: private */
    public boolean l;
    private boolean m;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wiyun.game.h.a(java.util.Map<java.lang.String, android.graphics.Bitmap>, boolean, java.lang.String, java.lang.String, boolean):android.graphics.Bitmap
     arg types: [java.util.Map, int, java.lang.String, java.lang.String, boolean]
     candidates:
      com.wiyun.game.h.a(int, int, int, int, android.net.Uri):android.content.Intent
      com.wiyun.game.h.a(android.content.Context, java.lang.String, int, android.view.View$OnClickListener, int[]):void
      com.wiyun.game.h.a(java.util.Map<java.lang.String, android.graphics.Bitmap>, boolean, java.lang.String, java.lang.String, boolean):android.graphics.Bitmap */
    private View a(View view, ViewGroup viewGroup, ag agVar) {
        View view2;
        if (view == null || ((SingleListActivity.b) view.getTag()).a != 13) {
            view2 = LayoutInflater.from(this).inflate(t.e("wy_list_item_account"), (ViewGroup) null);
            SingleListActivity.b bVar = new SingleListActivity.b();
            bVar.a = 13;
            bVar.b = (TextView) view2.findViewById(t.d("wy_tv_name"));
            bVar.c = (TextView) view2.findViewById(t.d("wy_tv_hint"));
            bVar.d = (TextView) view2.findViewById(t.d("wy_tv_honor"));
            bVar.e = (ImageView) view2.findViewById(t.d("wy_iv_portrait"));
            view2.setTag(bVar);
        } else {
            view2 = view;
        }
        SingleListActivity.b bVar2 = (SingleListActivity.b) view2.getTag();
        bVar2.b.setText(agVar.getName());
        if (TextUtils.isEmpty(agVar.getLastAppName())) {
            bVar2.c.setVisibility(8);
        } else {
            bVar2.c.setText(String.format(t.h("wy_label_last_played_x"), agVar.getLastAppName()));
        }
        bVar2.d.setText(String.valueOf(agVar.getHonor()));
        bVar2.e.setImageBitmap(h.a((Map<String, Bitmap>) this.b, false, h.b("p_", agVar.getId()), agVar.getAvatarUrl(), agVar.isFemale()));
        return view2;
    }

    /* access modifiers changed from: protected */
    public int a(int position) {
        return 13;
    }

    /* access modifiers changed from: protected */
    public View a(int i2, int i3, View view, ViewGroup viewGroup) {
        switch (i2) {
            case 13:
                break;
            default:
                throw new IllegalArgumentException("unknown tag");
        }
        return a(view, viewGroup, (ag) b(i3));
    }

    /* access modifiers changed from: protected */
    public void a(Intent intent) {
        super.a(intent);
        this.k = intent.getBooleanExtra("enable_force", false);
        this.l = intent.getBooleanExtra("prompt_binding", false);
    }

    public void a(e e2) {
        switch (e2.a) {
            case 14:
                if (this.f) {
                    runOnUiThread(new Runnable() {
                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
                         arg types: [java.lang.String, int]
                         candidates:
                          ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
                        public void run() {
                            if (!SwitchAccount.this.l || WiGame.u().d()) {
                                WiGame.z();
                            } else {
                                Intent intent = new Intent(SwitchAccount.this, AccountRetrieval.class);
                                intent.putExtra("process_pending", true);
                                SwitchAccount.this.startActivity(intent);
                            }
                            SwitchAccount.this.finish();
                        }
                    });
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public boolean a() {
        return true;
    }

    /* access modifiers changed from: protected */
    public Object b(int position) {
        return this.e.get(position);
    }

    public void b(final e e2) {
        switch (e2.a) {
            case 3:
                if (e2.j != this.g) {
                    return;
                }
                if (e2.c) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(SwitchAccount.this, (String) e2.e, 0).show();
                            SwitchAccount.this.finish();
                        }
                    });
                    return;
                } else if (e2.e instanceof String) {
                    runOnUiThread(new Runnable() {
                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
                         arg types: [java.lang.String, int]
                         candidates:
                          ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
                        public void run() {
                            Intent intent = new Intent(SwitchAccount.this, Login.class);
                            intent.putExtra("prompt_binding", SwitchAccount.this.l);
                            if (SwitchAccount.this.k) {
                                intent.putExtra("force", true);
                            }
                            SwitchAccount.this.startActivity(intent);
                            SwitchAccount.this.finish();
                        }
                    });
                    return;
                } else {
                    this.e.addAll((List) e2.e);
                    runOnUiThread(new Runnable() {
                        public void run() {
                            h.b(SwitchAccount.this.h);
                            SwitchAccount.this.i.setVisibility(4);
                            SwitchAccount.this.m();
                        }
                    });
                    return;
                }
            case 4:
            case 5:
            default:
                return;
            case 6:
                if (e2.c) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            h.b(SwitchAccount.this.h);
                            SwitchAccount.this.i.setVisibility(4);
                            Toast.makeText(SwitchAccount.this, (String) e2.e, 0).show();
                        }
                    });
                } else {
                    this.f = true;
                }
                e2.d = false;
                return;
        }
    }

    /* access modifiers changed from: protected */
    public boolean b() {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        return this.e.isEmpty();
    }

    /* access modifiers changed from: protected */
    public void d() {
        h.a(this.h);
        this.i.setVisibility(0);
        this.j.setText(t.f("wy_progress_checking_account"));
        this.g = f.f();
    }

    /* access modifiers changed from: protected */
    public boolean e() {
        return true;
    }

    /* access modifiers changed from: protected */
    public int f() {
        return t.e("wy_activity_switch_account");
    }

    /* access modifiers changed from: protected */
    public void g() {
        super.g();
        this.e = new ArrayList();
        ArrayList users = getIntent().getParcelableArrayListExtra("bound_users");
        if (users != null && !users.isEmpty()) {
            this.e.addAll(users);
        }
    }

    /* access modifiers changed from: protected */
    public void h() {
        super.h();
        this.h = (ViewGroup) findViewById(t.d("wy_ll_main_panel"));
        this.i = findViewById(t.d("wy_ll_progress_panel"));
        this.j = (TextView) findViewById(t.d("wy_tv_progress_hint"));
    }

    /* access modifiers changed from: protected */
    public void i() {
        ((Button) findViewById(t.d("wy_b_use_bound_account"))).setOnClickListener(this);
        ((Button) findViewById(t.d("wy_b_cancel"))).setOnClickListener(this);
        ((Button) findViewById(t.d("wy_b_new_account"))).setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public int j() {
        return this.e.size();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View v) {
        int id = v.getId();
        if (id == t.d("wy_b_use_bound_account")) {
            Intent intent = new Intent(this, UseAnotherAccount.class);
            intent.putExtra("from_login", false);
            intent.putExtra("prompt_binding", this.l);
            startActivity(intent);
            finish();
        } else if (id == t.d("wy_b_cancel")) {
            finish();
        } else if (id == t.d("wy_b_new_account")) {
            Intent intent2 = new Intent(this, Login.class);
            intent2.putExtra("prompt_binding", this.l);
            startActivity(intent2);
            finish();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (!this.e.isEmpty() || !this.k || keyCode != 4 || event.getRepeatCount() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        this.m = true;
        return true;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (!this.e.isEmpty() || !this.k || keyCode != 4 || !this.m) {
            return super.onKeyUp(keyCode, event);
        }
        this.m = false;
        finish();
        if (WiGame.getContext() instanceof Activity) {
            ((Activity) WiGame.getContext()).finish();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView listView, View view, int i2, long j2) {
        switch (a(i2)) {
            case 13:
                h.a(this.h);
                this.i.setVisibility(0);
                this.j.setText(t.f("wy_progress_logging_in"));
                f.b(((ag) b(i2)).getId());
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (WiGame.b == null || WiGame.b.isEmpty()) {
            f.i();
        }
    }
}
