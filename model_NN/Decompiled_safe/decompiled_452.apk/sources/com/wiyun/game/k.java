package com.wiyun.game;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

class k extends BroadcastReceiver {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ SubmitScore a;

    k(SubmitScore submitScore) {
        this.a = submitScore;
    }

    public void onReceive(Context context, Intent intent) {
        if ("com.wiyun.game.IMAGE_DOWNLOADED".equals(intent.getAction())) {
            final String id = intent.getStringExtra("image_id");
            if (this.a.a.containsKey(id)) {
                this.a.runOnUiThread(new Runnable() {
                    public void run() {
                        k.this.a.d.setImageBitmap(h.b(k.this.a.a, true, id, null));
                    }
                });
            }
        }
    }
}
