package com.wiyun.game.a;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;

class g extends BroadcastReceiver {
    final /* synthetic */ d a;

    g(d dVar) {
        this.a = dVar;
    }

    public void onReceive(Context context, Intent intent) {
        NetworkInfo networkInfo = (NetworkInfo) intent.getParcelableExtra("networkInfo");
        boolean connected = networkInfo != null && networkInfo.isConnected();
        if (this.a.c != connected) {
            this.a.c = connected;
            if (connected) {
                if (this.a.d != null) {
                    this.a.d.a();
                }
            } else if (this.a.d != null) {
                this.a.d.b();
            }
        }
    }
}
