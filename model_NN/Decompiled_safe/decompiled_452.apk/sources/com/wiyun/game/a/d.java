package com.wiyun.game.a;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.net.Proxy;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.TextUtils;
import org.apache.http.HttpHost;

public class d {
    private static d a;
    private Context b;
    /* access modifiers changed from: private */
    public boolean c;
    /* access modifiers changed from: private */
    public a d;
    private BroadcastReceiver e = new g(this);

    public interface a {
        void a();

        void b();
    }

    private d(Context context, a callback) {
        this.b = context.getApplicationContext();
        this.d = callback;
        this.b.registerReceiver(this.e, new IntentFilter("android.net.wifi.STATE_CHANGE"));
        this.c = a(context);
    }

    public static void a() {
        if (a != null) {
            a.b.unregisterReceiver(a.e);
            a.b = null;
            a.d = null;
            a = null;
        }
    }

    public static void a(Context context, a callback) {
        if (a == null) {
            a = new d(context, callback);
        }
    }

    public static boolean a(Context context) {
        WifiManager wm = (WifiManager) context.getSystemService("wifi");
        if (wm == null) {
            return false;
        }
        WifiInfo info = wm.getConnectionInfo();
        return info != null && info.getSupplicantState() == SupplicantState.COMPLETED;
    }

    public static boolean b() {
        if (a == null) {
            return false;
        }
        return a.c;
    }

    public static boolean c() {
        return !TextUtils.isEmpty(Proxy.getDefaultHost());
    }

    public static HttpHost d() {
        return new HttpHost(Proxy.getDefaultHost(), Proxy.getDefaultPort());
    }
}
