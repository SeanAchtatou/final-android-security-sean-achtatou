package com.wiyun.game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.wiyun.game.b.b;
import com.wiyun.game.b.d;
import com.wiyun.game.b.e;
import com.wiyun.game.e.a;
import com.wiyun.game.model.a.ag;

public class AccountRetrieval extends Activity implements View.OnClickListener, b {
    private EditText a;
    private EditText b;
    private EditText c;
    private EditText d;
    /* access modifiers changed from: private */
    public ViewGroup e;
    /* access modifiers changed from: private */
    public View f;
    private boolean g;
    private boolean h;
    private boolean i;

    private void a() {
        d.a().a(this);
    }

    private void b() {
        this.e = (ViewGroup) findViewById(t.d("wy_ll_main_panel"));
        this.f = findViewById(t.d("wy_ll_progress_panel"));
        this.a = (EditText) findViewById(t.d("wy_et_username"));
        this.b = (EditText) findViewById(t.d("wy_et_old_password"));
        this.c = (EditText) findViewById(t.d("wy_et_new_password"));
        this.d = (EditText) findViewById(t.d("wy_et_confirm_password"));
        ag myself = WiGame.u();
        this.b.setVisibility(myself.d() ? 0 : 8);
        ((TextView) findViewById(t.d("wy_tv_old_password"))).setVisibility(myself.d() ? 0 : 8);
        ((TextView) findViewById(t.d("wy_tv_new_password"))).setText(myself.d() ? t.f("wy_label_new_password_colon") : t.f("wy_label_password_colon"));
        if (!TextUtils.isEmpty(myself.g())) {
            this.a.setText(myself.g());
        } else if (!TextUtils.isEmpty(myself.f())) {
            this.a.setText(myself.f());
        }
        ((Button) findViewById(t.d("wy_b_submit"))).setOnClickListener(this);
        Button btn = (Button) findViewById(t.d("wy_b_skip"));
        btn.setOnClickListener(this);
        btn.setVisibility(this.h ? 8 : 0);
    }

    public void b(final e e2) {
        switch (e2.a) {
            case 20:
                if (e2.c) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            AccountRetrieval.this.f.setVisibility(4);
                            h.b(AccountRetrieval.this.e);
                            Toast.makeText(AccountRetrieval.this, (String) e2.e, 0).show();
                        }
                    });
                } else {
                    ag myself = WiGame.u();
                    String info = this.a.getText().toString().trim();
                    if (info.indexOf(64) != -1) {
                        myself.e(info);
                    } else {
                        myself.f(info);
                    }
                    myself.d(true);
                    f.d(WiGame.getMyId());
                }
                if (this.g) {
                    WiGame.z();
                }
                finish();
                return;
            default:
                return;
        }
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id == t.d("wy_b_submit")) {
            String trim = this.a.getText().toString().trim();
            boolean z = trim.indexOf(64) != -1;
            String editable = this.b.getText().toString();
            String editable2 = this.c.getText().toString();
            String editable3 = this.d.getText().toString();
            if (TextUtils.isEmpty(trim)) {
                Toast.makeText(this, t.f("wy_toast_please_input_bound_info"), 0).show();
            } else if (z && !h.a((CharSequence) trim)) {
                Toast.makeText(this, t.f("wy_toast_email_is_invalid"), 0).show();
            } else if (WiGame.u().d() && TextUtils.isEmpty(editable)) {
                Toast.makeText(this, t.f("wy_toast_old_password_cannot_be_empty"), 0).show();
            } else if (TextUtils.isEmpty(editable2)) {
                Toast.makeText(this, t.f("wy_toast_new_password_cannot_be_empty"), 0).show();
            } else if (TextUtils.isEmpty(editable3)) {
                Toast.makeText(this, t.f("wy_toast_confirm_password_cannot_be_empty"), 0).show();
            } else if (!editable2.equals(editable3)) {
                Toast.makeText(this, t.f("wy_toast_two_passwords_not_match"), 0).show();
            } else {
                this.f.setVisibility(0);
                h.a(this.e);
                String str = z ? trim : null;
                if (z) {
                    trim = null;
                }
                f.a(str, trim, a.b(h.d(editable)), a.b(h.d(editable2)));
            }
        } else if (id == t.d("wy_b_skip")) {
            if (this.g) {
                WiGame.z();
            }
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        this.g = intent.getBooleanExtra("process_pending", false);
        this.h = intent.getBooleanExtra("force", false);
        setRequestedOrientation(WiGame.i);
        requestWindowFeature(1);
        setContentView(t.e("wy_activity_account_retrieval"));
        a();
        b();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        d.a().b(this);
        super.onDestroy();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (!this.h || keyCode != 4 || event.getRepeatCount() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        this.i = true;
        return true;
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (!this.h || keyCode != 4 || !this.i) {
            return super.onKeyUp(keyCode, event);
        }
        this.i = false;
        return true;
    }
}
