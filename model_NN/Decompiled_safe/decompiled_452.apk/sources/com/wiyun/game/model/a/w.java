package com.wiyun.game.model.a;

import android.text.TextUtils;
import org.json.JSONObject;

public class w {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;

    public static final w a(JSONObject dict) {
        w m = new w();
        String url = dict.optString("url");
        if (TextUtils.isEmpty(url)) {
            return null;
        }
        m.a(url);
        m.b(dict.optString("icon"));
        m.c(dict.optString("name"));
        m.d(dict.optString("market_name"));
        m.e(dict.optString("note"));
        m.f(dict.optString("platform", "android"));
        return m;
    }

    public void a(String url) {
        this.a = url;
    }

    public void b(String iconUrl) {
        this.b = iconUrl;
    }

    public void c(String name) {
        this.c = name;
    }

    public void d(String marketName) {
        this.d = marketName;
    }

    public void e(String note) {
        this.e = note;
    }

    public void f(String platform) {
        this.f = platform;
    }
}
