package com.wiyun.game.model.a;

import android.text.TextUtils;
import org.json.JSONObject;

public class b {
    private String a;
    private String b;
    private String c;
    private String[] d;
    private int e;
    private int f;
    private boolean g;
    private boolean h;
    private int i;

    public static b a(JSONObject dict) {
        b app = new b();
        String id = dict.optString("id");
        if (TextUtils.isEmpty(id)) {
            return null;
        }
        app.a(id);
        app.b(dict.optString("name"));
        app.c(dict.optString("icon"));
        app.a(dict.optBoolean("favorite"));
        app.b(dict.optBoolean("my_favorite"));
        app.a(dict.optInt("point"));
        app.c(dict.optInt("my_point", -1));
        app.b(dict.optInt("favorite_count"));
        String platforms = dict.optString("platform", "android");
        if (platforms.indexOf(44) == -1) {
            app.a(new String[]{platforms});
        } else {
            app.a(platforms.split(","));
        }
        return app;
    }

    public void a(int point) {
        this.e = point;
    }

    public void a(String id) {
        this.a = id;
    }

    public void a(boolean favorite) {
        this.g = favorite;
    }

    public void a(String[] platforms) {
        this.d = platforms;
    }

    public void b(int favoriteCount) {
        this.i = favoriteCount;
    }

    public void b(String name) {
        this.b = name;
    }

    public void b(boolean myFavorite) {
        this.h = myFavorite;
    }

    public void c(int myPoint) {
        this.f = myPoint;
    }

    public void c(String iconUrl) {
        this.c = iconUrl;
    }
}
