package com.wiyun.game;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.wiyun.game.b.b;
import com.wiyun.game.b.d;
import com.wiyun.game.b.e;
import java.io.File;

public class ChangeMyPortrait extends Activity implements View.OnClickListener, b {
    private View a;
    private ViewGroup b;
    private ImageView c;
    private TextView d;
    private Bitmap e;
    private Uri f;

    private void a() {
        d.a().a(this);
    }

    private void b() {
        this.d = (TextView) findViewById(t.d("wy_tv_hint"));
        this.c = (ImageView) findViewById(t.d("wy_iv_portrait"));
        this.a = findViewById(t.d("wy_ll_progress_panel"));
        this.b = (ViewGroup) findViewById(t.d("wy_ll_main_panel"));
        ((Button) findViewById(t.d("wy_b_cancel"))).setOnClickListener(this);
        ((Button) findViewById(t.d("wy_b_upload"))).setOnClickListener(this);
        ((ImageButton) findViewById(t.d("wy_ib_camera"))).setOnClickListener(this);
        ((ImageButton) findViewById(t.d("wy_ib_images"))).setOnClickListener(this);
    }

    public void b(final e e2) {
        switch (e2.a) {
            case 18:
                if (e2.c) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(ChangeMyPortrait.this, (String) e2.e, 0).show();
                            ChangeMyPortrait.this.finish();
                        }
                    });
                    return;
                } else {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(ChangeMyPortrait.this, t.f("wy_toast_portrait_updated"), 0).show();
                            ChangeMyPortrait.this.finish();
                        }
                    });
                    return;
                }
            default:
                return;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bundle extras;
        switch (requestCode) {
            case 1:
                if (resultCode == -1 && (extras = data.getExtras()) != null) {
                    this.e = (Bitmap) extras.getParcelable("data");
                    if (this.e == null) {
                        String filepath = extras.getString("filePath");
                        if (!TextUtils.isEmpty(filepath)) {
                            try {
                                this.e = BitmapFactory.decodeFile(filepath);
                                File f2 = new File(filepath);
                                if (f2 != null) {
                                    f2.delete();
                                }
                            } catch (OutOfMemoryError e2) {
                                File f3 = new File(filepath);
                                if (f3 != null) {
                                    f3.delete();
                                }
                            } catch (Throwable th) {
                                File f4 = new File(filepath);
                                if (f4 != null) {
                                    f4.delete();
                                }
                                throw th;
                            }
                        }
                    }
                    if (this.e != null) {
                        this.d.setVisibility(4);
                        this.c.setVisibility(0);
                        this.c.setImageBitmap(this.e);
                        this.c.requestLayout();
                    }
                }
                if (this.f != null) {
                    File f5 = new File(this.f.getPath());
                    if (f5.exists()) {
                        f5.delete();
                    }
                    this.f = null;
                    return;
                }
                return;
            case 2:
                if (resultCode == -1) {
                    startActivityForResult(h.a(256, 256, 1, 1, this.f), 1);
                    return;
                }
                return;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                return;
        }
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id == t.d("wy_b_upload")) {
            if (this.e == null) {
                Toast.makeText(this, t.f("wy_toast_please_select_portrait"), 0).show();
                return;
            }
            byte[] a2 = h.a(this.e);
            if (a2 != null) {
                this.a.setVisibility(0);
                h.a(this.b);
                f.a(a2);
            }
        } else if (id == t.d("wy_b_cancel")) {
            finish();
        } else if (id == t.d("wy_ib_camera")) {
            try {
                this.f = Uri.fromFile(new File(Environment.getExternalStorageDirectory(), "pic_" + String.valueOf(System.currentTimeMillis()) + ".jpg"));
                startActivityForResult(h.a(this.f), 2);
            } catch (ActivityNotFoundException e2) {
                Toast.makeText(this, t.f("wy_toast_no_camera"), 0).show();
            }
        } else if (id == t.d("wy_ib_images")) {
            try {
                startActivityForResult(h.a(256, 256, 1, 1, (Uri) null), 1);
            } catch (ActivityNotFoundException e3) {
                Toast.makeText(this, t.f("wy_toast_no_image_gallery"), 0).show();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(WiGame.h);
        requestWindowFeature(1);
        if (WiGame.j) {
            getWindow().addFlags(1024);
        }
        setContentView(t.e("wy_activity_change_my_portrait"));
        a();
        b();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        d.a().b(this);
        if (this.e != null) {
            this.e.recycle();
            this.e = null;
        }
        super.onDestroy();
    }
}
