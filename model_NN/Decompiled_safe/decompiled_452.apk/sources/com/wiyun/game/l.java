package com.wiyun.game;

import android.content.Intent;
import android.os.Message;
import android.text.TextUtils;
import android.webkit.CookieManager;
import com.wiyun.game.WiGame;
import com.wiyun.game.b.a;
import com.wiyun.game.b.e;
import com.wiyun.game.model.ChallengeResult;
import com.wiyun.game.model.a.ag;
import com.wiyun.game.model.a.ah;
import com.wiyun.game.model.a.ai;
import com.wiyun.game.model.a.c;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

class l implements a {
    l() {
    }

    public void a(e eVar) {
        if (WiGame.T) {
            if (eVar.a != 6) {
                if (!eVar.c) {
                    f.c(eVar.a);
                } else if (eVar.b == 401) {
                    WiGame.d((String) null);
                    if (f.b(eVar.a)) {
                        f.c(eVar.a);
                    } else {
                        eVar.d = false;
                        f.b(WiGame.R.getId());
                    }
                } else {
                    f.a(eVar.a);
                }
            }
            if (eVar.a == 6) {
                WiGame.a(false);
                if (eVar.c) {
                    f.e();
                    f.a(eVar.a);
                    if (eVar.b == 403) {
                        WiGame.j();
                        WiGame.k();
                    } else if (eVar.b == 406) {
                        WiGame.X.sendEmptyMessage(13);
                    } else if (!WiGame.ae && !WiGame.x) {
                        WiGame.X.sendEmptyMessage(2);
                    }
                    WiGame.X.sendEmptyMessage(1001);
                } else {
                    ah ahVar = (ah) eVar.e;
                    WiGame.d(ahVar.a());
                    WiGame.a(ahVar.b());
                    WiGame.b(ahVar.c());
                    WiGame.R.i(ahVar.b());
                    WiGame.R.a(ahVar.e());
                    WiGame.R.j(ahVar.c());
                    WiGame.R.h(ahVar.d());
                    WiGame.R.c(ahVar.g());
                    WiGame.R.b(ahVar.f());
                    WiGame.R.d(1);
                    WiGame.R.f(ahVar.h());
                    WiGame.R.e(ahVar.i());
                    WiGame.R.d(ahVar.k());
                    WiGame.R.g(ahVar.l());
                    WiGame.a = null;
                    WiGame.F = 0;
                    WiGame.I = 0;
                    WiGame.G = 0;
                    WiGame.H = 0;
                    CookieManager.getInstance().setCookie("http://wiyun.com", String.format("user_id=%s;domain=%s", WiGame.v(), "wiyun.com"));
                    WiGame.ae();
                    if (!WiGame.ae && !WiGame.x) {
                        WiGame.X.sendEmptyMessage(1);
                    }
                    f.d();
                    f.d(WiGame.R.getId());
                    i.g();
                    String j = i.j();
                    WiGame.m = ahVar.j();
                    if (!j.equals(WiGame.m)) {
                        f.k(null);
                    }
                    new v().a();
                    WiGame.X.sendEmptyMessage(1000);
                }
                WiGame.setHideWelcomeBackToast(true);
            }
            if (eVar.d) {
                switch (eVar.a) {
                    case 2:
                        if (!eVar.c) {
                            String a = eVar.a("user_id");
                            if (!TextUtils.isEmpty(a) && WiGame.getMyId().equals(a)) {
                                Intent intent = new Intent("com.wiyun.game.EXIT");
                                intent.setFlags(1073741824);
                                WiGame.S.sendBroadcast(intent);
                                new File(WiGame.S.getFilesDir(), "wiyun_id").delete();
                                WiGame.a();
                                f.g();
                                break;
                            }
                        }
                        break;
                    case 3:
                        if (!eVar.c && !(eVar.e instanceof String)) {
                            WiGame.a = (ArrayList) eVar.e;
                            break;
                        }
                    case 4:
                        if (!eVar.c && !(eVar.e instanceof String)) {
                            WiGame.a = (ArrayList) eVar.e;
                            if (WiGame.a.size() == 1) {
                                ag agVar = WiGame.a.get(0);
                                WiGame.R.j(agVar.getName());
                                WiGame.R.i(agVar.getId());
                                WiGame.R.a(agVar.getAvatarUrl());
                                f.b(WiGame.getMyId());
                            }
                        }
                        WiGame.y = true;
                        WiGame.ac();
                        eVar.d = false;
                        break;
                    case 7:
                        eVar.d = false;
                        if (System.currentTimeMillis() - WiGame.c > 60000) {
                            f.d(WiGame.getMyId());
                            break;
                        }
                        break;
                    case 8:
                        WiGame.B = true;
                        if (!eVar.c) {
                            ai aiVar = (ai) eVar.e;
                            x.a(aiVar.c());
                            x.b(aiVar.d());
                            WiGame.z = aiVar.a();
                            WiGame.A = aiVar.b();
                            WiGame.a(h.d((String) eVar.f));
                        } else {
                            WiGame.z = false;
                            WiGame.A = true;
                        }
                        WiGame.ac();
                        break;
                    case 12:
                        WiGame.d a2 = WiGame.a(eVar.j);
                        if (a2 != null) {
                            if (!eVar.c) {
                                a2.h = true;
                            }
                            if (!eVar.c) {
                                String a3 = eVar.a("leaderboard_id");
                                int c = h.c(eVar.a("score"));
                                c b = x.b(a3);
                                if (b != null && b.e() && c > i.c(a3)) {
                                    i.a(a3, c);
                                }
                                WiGame.X.sendMessage(WiGame.X.obtainMessage(1012, h.c(eVar.a("score")), eVar.g, eVar.a("leaderboard_id")));
                                if (!WiGame.af) {
                                    WiGame.X.sendMessage(WiGame.X.obtainMessage(7, eVar));
                                }
                            } else if (!WiGame.af) {
                                WiGame.X.sendEmptyMessage(8);
                            }
                            i.a(a2);
                            break;
                        }
                        break;
                    case 14:
                        if (eVar.c) {
                            WiGame.X.sendMessage(Message.obtain(WiGame.X, 1005, eVar));
                            break;
                        } else {
                            WiGame.c = System.currentTimeMillis();
                            ag agVar2 = (ag) eVar.e;
                            if (agVar2.getId().equals(WiGame.R.getId())) {
                                WiGame.R.j(agVar2.getName());
                                WiGame.R.h(agVar2.getHonor());
                                WiGame.R.g(agVar2.getCoins());
                                WiGame.R.c(agVar2.getAppCount());
                                WiGame.R.d(agVar2.c());
                                WiGame.R.b(agVar2.getFriendCount());
                                WiGame.R.a(agVar2.a());
                                WiGame.R.b(agVar2.b());
                                WiGame.R.l(agVar2.getPendingFriendCount());
                                WiGame.R.m(agVar2.getPendingSharingCount());
                                WiGame.R.a(agVar2.getUnreadMessageCount());
                                WiGame.R.j(agVar2.getPendingChallengeCount());
                                WiGame.R.k(agVar2.getAppPendingChallengeCount());
                                WiGame.R.i(agVar2.getNoticeCount());
                                WiGame.R.d(agVar2.d());
                                WiGame.R.e(agVar2.isFemale());
                                if (WiGame.d == null) {
                                    int pendingFriendCount = agVar2.getPendingFriendCount();
                                    int appPendingChallengeCount = agVar2.getAppPendingChallengeCount();
                                    int unreadMessageCount = agVar2.getUnreadMessageCount();
                                    int noticeCount = agVar2.getNoticeCount();
                                    if (!(pendingFriendCount == WiGame.I && appPendingChallengeCount == WiGame.F && unreadMessageCount == WiGame.G && noticeCount == WiGame.H)) {
                                        WiGame.I = pendingFriendCount;
                                        WiGame.F = appPendingChallengeCount;
                                        WiGame.G = unreadMessageCount;
                                        WiGame.H = noticeCount;
                                        if (!WiGame.ad) {
                                            WiGame.showPendingToast(WiGame.S);
                                        }
                                    }
                                }
                                String avatarUrl = agVar2.getAvatarUrl() == null ? "" : agVar2.getAvatarUrl();
                                if (!(WiGame.R.getAvatarUrl() == null ? "" : WiGame.R.getAvatarUrl()).equals(avatarUrl)) {
                                    WiGame.R.a(avatarUrl);
                                    WiGame.U.a(h.b("p_", WiGame.R.getId()));
                                }
                                WiGame.E = true;
                            }
                            WiGame.X.sendMessage(Message.obtain(WiGame.X, 1004, eVar));
                            break;
                        }
                    case 17:
                        if (!eVar.c) {
                            String a4 = eVar.a("username");
                            if (!TextUtils.isEmpty(a4)) {
                                WiGame.R.j(a4);
                                WiGame.b(a4);
                                break;
                            }
                        }
                        break;
                    case 18:
                        if (!eVar.c) {
                            WiGame.U.a(h.b("p_", WiGame.getMyId()));
                            break;
                        }
                        break;
                    case 23:
                        if (eVar.c) {
                            WiGame.X.sendMessage(Message.obtain(WiGame.X, 1007, eVar));
                            break;
                        } else {
                            WiGame.X.sendMessage(Message.obtain(WiGame.X, 1006, eVar));
                            break;
                        }
                    case 25:
                        if (eVar.c) {
                            WiGame.X.sendMessage(Message.obtain(WiGame.X, 1009, eVar.a("user_id")));
                            break;
                        } else {
                            WiGame.X.sendMessage(Message.obtain(WiGame.X, 1008, eVar.a("user_id")));
                            break;
                        }
                    case 26:
                        if (!eVar.c) {
                            f.d(WiGame.getMyId());
                            break;
                        }
                        break;
                    case 41:
                        WiGame.b l = WiGame.n(eVar.a("achievement_id"));
                        if (!eVar.c && l != null) {
                            l.d = true;
                            if (WiGame.isLoggedIn()) {
                                f.d(WiGame.getMyId());
                            }
                        }
                        if (l != null) {
                            i.a(l);
                        }
                        eVar.d = false;
                        if (WiGame.ac != null) {
                            WiGame.ac.remove(l);
                            break;
                        }
                        break;
                    case 51:
                        if (!eVar.c && WiGame.isLoggedIn()) {
                            f.d(WiGame.getMyId());
                            break;
                        }
                    case 53:
                        if (eVar.c) {
                            WiGame.X.sendEmptyMessage(5);
                            i.a(WiGame.ab);
                        } else {
                            WiGame.X.sendEmptyMessage(4);
                            if (WiGame.isLoggedIn()) {
                                f.d(WiGame.getMyId());
                            }
                        }
                        WiGame.X.sendMessage(Message.obtain(WiGame.X, 1013, WiGame.ab));
                        eVar.d = false;
                        WiGame.ab = (ChallengeResult) null;
                        break;
                    case 77:
                        if (!eVar.c) {
                            WiGame.R.g(eVar.g);
                            break;
                        }
                        break;
                    case 86:
                        if (!eVar.c) {
                            String j2 = i.j();
                            String str = (String) eVar.f;
                            if (!j2.equals(str)) {
                                i.a((List) eVar.e);
                                i.b(str);
                                break;
                            }
                        }
                        break;
                    case 87:
                        if (!eVar.c) {
                            i.b((String) eVar.e);
                            break;
                        }
                        break;
                    case 88:
                        if (!eVar.c) {
                            i.l();
                            i.h();
                            i.b((String) eVar.e);
                            break;
                        }
                        break;
                    case 90:
                        if (!eVar.c) {
                            if (WiGame.b == null) {
                                WiGame.b = new ArrayList<>();
                            }
                            WiGame.b.clear();
                            WiGame.b.addAll((ArrayList) eVar.e);
                            break;
                        }
                        break;
                }
            }
            if (!eVar.c) {
                WiGame.X.removeMessages(6);
                WiGame.X.sendEmptyMessageDelayed(6, 300000);
            }
        }
    }
}
