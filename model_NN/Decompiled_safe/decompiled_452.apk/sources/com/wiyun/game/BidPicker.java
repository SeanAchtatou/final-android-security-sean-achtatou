package com.wiyun.game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class BidPicker extends Activity implements View.OnClickListener {
    private TextView a;
    private EditText b;
    private int c;
    private int d;
    private int e;

    private void a() {
        Intent intent = getIntent();
        this.c = intent.getIntExtra("min", 0);
        this.d = intent.getIntExtra("max", 0);
        this.e = intent.getIntExtra("init", 0);
        if (this.e == 0) {
            this.e = this.c;
        }
    }

    private void b() {
        this.a = (TextView) findViewById(t.d("wy_text"));
        this.b = (EditText) findViewById(t.d("wy_edit"));
        TextView textView = this.a;
        String h = t.h("wy_label_about_make_a_bet");
        Object[] objArr = new Object[3];
        objArr[0] = String.valueOf(this.c);
        objArr[1] = this.d == 0 ? t.h("wy_label_unlimited") : String.valueOf(this.d);
        objArr[2] = Integer.valueOf(WiGame.u().getHonor());
        textView.setText(String.format(h, objArr));
        this.b.setText(String.valueOf(this.e));
        ((Button) findViewById(t.d("wy_button"))).setOnClickListener(this);
        ((Button) findViewById(t.d("wy_button2"))).setOnClickListener(this);
        ((Button) findViewById(t.d("wy_button3"))).setOnClickListener(this);
    }

    public void onClick(View v) {
        int id = v.getId();
        if (id == t.d("wy_button")) {
            int bid = h.c(this.b.getText().toString().trim());
            if (bid < this.c) {
                Toast.makeText(this, t.f("wy_toast_bid_is_less_than_min"), 0).show();
            } else if (this.d > 0 && bid > this.d) {
                Toast.makeText(this, t.f("wy_toast_bid_is_larger_than_max"), 0).show();
            } else if (bid > WiGame.u().getHonor()) {
                Toast.makeText(this, t.f("wy_toast_bid_is_larger_than_your_points"), 0).show();
            } else {
                Intent data = new Intent();
                data.putExtra("value", bid);
                setResult(-1, data);
                finish();
            }
        } else if (id == t.d("wy_button2")) {
            Intent data2 = new Intent();
            data2.putExtra("value", 0);
            setResult(-1, data2);
            finish();
        } else if (id == t.d("wy_button3")) {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(t.e("wy_activity_bid_picker"));
        a();
        b();
    }
}
