package com.wiyun.game;

import android.database.Cursor;
import android.text.TextUtils;
import android.util.Log;
import com.wiyun.game.a.i;
import com.wiyun.game.b.a;
import com.wiyun.game.b.d;
import com.wiyun.game.b.e;
import java.util.Arrays;

class v implements a, Runnable {
    private Cursor a;
    private boolean b;
    private boolean c;
    private boolean d;

    v() {
    }

    /* access modifiers changed from: package-private */
    public void a() {
        Thread t = new Thread(this);
        t.setName("Cache Committer");
        t.setDaemon(true);
        t.setPriority(1);
        t.start();
    }

    public void a(e e) {
        switch (e.a) {
            case 13:
                if (!e.c) {
                    i.b(this.a.getLong(0));
                    synchronized (this) {
                        this.c = true;
                        notify();
                    }
                    return;
                } else if (e.b >= 500) {
                    synchronized (this) {
                        this.b = true;
                        notify();
                    }
                    return;
                } else if (e.b == 404) {
                    i.b(this.a.getLong(0));
                    synchronized (this) {
                        this.c = true;
                        notify();
                    }
                    return;
                } else {
                    int fc = this.a.getInt(7);
                    long id = this.a.getLong(0);
                    if (fc < 3) {
                        i.b(id, fc);
                    } else {
                        i.b(id);
                    }
                    synchronized (this) {
                        this.c = true;
                        notify();
                    }
                    return;
                }
            case 42:
                if (!e.c) {
                    i.c(this.a.getLong(0));
                    this.d = true;
                    synchronized (this) {
                        this.c = true;
                        notify();
                    }
                    return;
                } else if (e.b >= 500) {
                    synchronized (this) {
                        this.b = true;
                        notify();
                    }
                    return;
                } else if (e.b == 404) {
                    i.c(this.a.getLong(0));
                    synchronized (this) {
                        this.c = true;
                        notify();
                    }
                    return;
                } else {
                    int fc2 = this.a.getInt(5);
                    long id2 = this.a.getLong(0);
                    if (fc2 < 3) {
                        i.c(id2, fc2);
                    } else {
                        i.c(id2);
                    }
                    synchronized (this) {
                        this.c = true;
                        notify();
                    }
                    return;
                }
            case 54:
                if (!e.c) {
                    i.a(this.a.getLong(0));
                    this.d = true;
                    synchronized (this) {
                        this.c = true;
                        notify();
                    }
                    return;
                } else if (e.b >= 500) {
                    synchronized (this) {
                        this.b = true;
                        notify();
                    }
                    return;
                } else {
                    int fc3 = this.a.getInt(7);
                    long id3 = this.a.getLong(0);
                    if (fc3 < 3) {
                        i.a(id3, fc3);
                    } else {
                        i.a(id3);
                    }
                    synchronized (this) {
                        this.c = true;
                        notify();
                    }
                    return;
                }
            case 88:
                if (e.c) {
                    synchronized (this) {
                        this.b = true;
                        notify();
                    }
                    return;
                }
                synchronized (this) {
                    this.c = true;
                    notify();
                }
                return;
            default:
                return;
        }
    }

    public void run() {
        i.a(WiGame.f);
        d.a().a(this);
        i.f();
        try {
            this.a = i.b();
            if (this.a.moveToFirst()) {
                loop0:
                while (true) {
                    if (this.a.isAfterLast()) {
                        break;
                    }
                    if (this.a.getInt(2) == 0) {
                        i a2 = new i();
                        a2.a(true);
                        int score = 0;
                        int result = 0;
                        byte[] blob = null;
                        String ctuId = this.a.getString(1);
                        boolean submit = !TextUtils.isEmpty(ctuId);
                        if (submit) {
                            score = this.a.getInt(3);
                            result = this.a.getInt(4);
                            byte[] sig = this.a.getBlob(5);
                            blob = this.a.getBlob(6);
                            StringBuilder buf = new StringBuilder();
                            buf.append(ctuId).append(':').append(score).append(':').append(result).append(':').append(blob == null ? "" : Arrays.toString(com.wiyun.game.e.a.a(blob)));
                            submit = Arrays.equals(sig, h.a(a2, com.wiyun.game.e.a.a(h.d(buf.toString())), "wiyun.db"));
                        }
                        if (submit) {
                            f.b(ctuId, result, score, blob);
                        } else {
                            i.a(this.a.getLong(0));
                        }
                    }
                    synchronized (this) {
                        while (!this.b && !this.c) {
                            try {
                                wait(2000);
                            } catch (InterruptedException e) {
                            }
                        }
                        if (!this.b) {
                            this.c = false;
                        }
                    }
                    this.a.moveToNext();
                }
            }
            if (this.a != null) {
                this.a.close();
                this.a = null;
            }
            this.b = false;
            this.c = false;
        } catch (Exception e2) {
            try {
                Log.w("WiYun", "failed to submit all cached PCR");
            } finally {
                if (this.a != null) {
                    this.a.close();
                    this.a = null;
                }
                this.b = false;
                this.c = false;
            }
        }
        try {
            this.a = i.c();
            if (this.a.moveToFirst()) {
                loop1:
                while (true) {
                    if (this.a.isAfterLast()) {
                        break;
                    }
                    if (this.a.getInt(2) == 0) {
                        String lbId = this.a.getString(1);
                        int score2 = this.a.getInt(3);
                        long createTime = this.a.getLong(4);
                        byte[] sig2 = this.a.getBlob(5);
                        byte[] blob2 = this.a.getBlob(6);
                        double lat = this.a.getDouble(8);
                        double lon = this.a.getDouble(9);
                        i a3 = new i();
                        a3.a(true);
                        StringBuilder buf2 = new StringBuilder();
                        buf2.append(lbId).append(':').append(score2).append(':').append(createTime).append(':').append(lat).append(':').append(lon).append(':').append(blob2 == null ? "" : Arrays.toString(com.wiyun.game.e.a.a(blob2)));
                        if (Arrays.equals(sig2, h.a(a3, com.wiyun.game.e.a.a(h.d(buf2.toString())), "wiyun.db"))) {
                            f.b(lbId, score2, blob2, lat, lon);
                        } else {
                            i.b(this.a.getLong(0));
                        }
                    }
                    synchronized (this) {
                        while (!this.b && !this.c) {
                            try {
                                wait(2000);
                            } catch (InterruptedException e3) {
                            }
                        }
                        if (this.b) {
                            break;
                        }
                        this.c = false;
                        this.a.moveToNext();
                    }
                }
            }
            if (this.a != null) {
                this.a.close();
                this.a = null;
            }
            this.b = false;
            this.c = false;
        } catch (Exception e4) {
            try {
                Log.w("WiYun", "failed to submit cached scores");
            } finally {
                if (this.a != null) {
                    this.a.close();
                    this.a = null;
                }
                this.b = false;
                this.c = false;
            }
        }
        try {
            this.a = i.d();
            if (this.a.moveToFirst()) {
                loop2:
                while (true) {
                    if (this.a.isAfterLast()) {
                        break;
                    }
                    if (this.a.getInt(2) == 0) {
                        String achId = this.a.getString(1);
                        long unlockTime = this.a.getLong(3);
                        byte[] sig3 = this.a.getBlob(4);
                        i a4 = new i();
                        a4.a(true);
                        StringBuilder buf3 = new StringBuilder();
                        buf3.append(achId).append(':').append(unlockTime);
                        if (Arrays.equals(sig3, h.a(a4, com.wiyun.game.e.a.a(h.d(buf3.toString())), "wiyun.db"))) {
                            f.f(achId);
                        } else {
                            i.c(this.a.getLong(0));
                        }
                    }
                    synchronized (this) {
                        while (!this.b && !this.c) {
                            try {
                                wait(2000);
                            } catch (InterruptedException e5) {
                            }
                        }
                        if (this.b) {
                            break;
                        }
                        this.c = false;
                        this.a.moveToNext();
                    }
                }
            }
            if (this.a != null) {
                this.a.close();
                this.a = null;
            }
            this.b = false;
            this.c = false;
        } catch (Exception e6) {
            try {
                Log.w("WiYun", "failed to submit cached scores");
            } finally {
                if (this.a != null) {
                    this.a.close();
                    this.a = null;
                }
                this.b = false;
                this.c = false;
            }
        }
        try {
            this.a = i.k();
            StringBuilder buf4 = new StringBuilder();
            if (this.a.moveToFirst()) {
                while (!this.a.isAfterLast()) {
                    int usedCount = this.a.getInt(9);
                    if (usedCount > 0) {
                        buf4.append(this.a.getString(2)).append(':').append(usedCount).append(',');
                    }
                    this.a.moveToNext();
                }
            }
            if (buf4.length() > 0) {
                buf4.deleteCharAt(buf4.length() - 1);
            }
            if (buf4.length() > 0) {
                f.l(buf4.toString());
            } else {
                this.c = true;
            }
            synchronized (this) {
                while (!this.b && !this.c) {
                    try {
                        wait(2000);
                    } catch (InterruptedException e7) {
                    }
                }
                if (this.a != null) {
                    this.a.close();
                    this.a = null;
                }
                this.b = false;
                this.c = false;
                if (this.d && WiGame.isLoggedIn()) {
                    f.d(WiGame.getMyId());
                }
                d.a().b(this);
                i.a();
            }
        } catch (Exception e8) {
            Log.w("WiYun", "failed to submit pending used count");
            if (this.a != null) {
                this.a.close();
                this.a = null;
            }
            this.b = false;
            this.c = false;
        } catch (Throwable th) {
            if (this.a != null) {
                this.a.close();
                this.a = null;
            }
            this.b = false;
            this.c = false;
            throw th;
        }
    }
}
