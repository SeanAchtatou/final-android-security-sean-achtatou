package com.wiyun.game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.wiyun.game.b.a;
import com.wiyun.game.b.b;
import com.wiyun.game.b.d;
import com.wiyun.game.b.e;
import com.wiyun.game.model.a.ac;
import com.wiyun.game.model.a.ag;
import java.util.ArrayList;
import java.util.Iterator;

public class UseAnotherAccount extends Activity implements View.OnClickListener, a, b {
    /* access modifiers changed from: private */
    public View a;
    /* access modifiers changed from: private */
    public ViewGroup b;
    private EditText c;
    private EditText d;
    private LinearLayout e;
    private TextView f;
    private boolean g;
    private boolean h;
    private boolean i;
    private boolean j;
    private boolean k;

    private void a() {
        Intent intent = getIntent();
        this.g = intent.getBooleanExtra("from_login", false);
        this.i = intent.getBooleanExtra("force", false);
        this.j = intent.getBooleanExtra("prompt_binding", false);
        d.a().a((b) this);
        d.a().a((a) this);
    }

    private void b() {
        this.a = findViewById(t.d("wy_ll_progress_panel"));
        this.b = (ViewGroup) findViewById(t.d("wy_ll_main_panel"));
        this.c = (EditText) findViewById(t.d("wy_et_username"));
        this.d = (EditText) findViewById(t.d("wy_et_password"));
        this.e = (LinearLayout) findViewById(t.d("wy_ll_sns_panel"));
        this.f = (TextView) findViewById(t.d("wy_tv_hint"));
        ((Button) findViewById(t.d("wy_b_cancel"))).setOnClickListener(this);
        ((Button) findViewById(t.d("wy_b_submit"))).setOnClickListener(this);
    }

    /* access modifiers changed from: private */
    public void c() {
        if (WiGame.b != null && !WiGame.b.isEmpty()) {
            this.f.setVisibility(8);
            int i2 = 0;
            Iterator<ac> it = WiGame.b.iterator();
            while (true) {
                int i3 = i2;
                if (it.hasNext()) {
                    ImageButton imageButton = new ImageButton(this);
                    imageButton.setBackgroundResource(t.c("wy_sns_button_bg"));
                    String a2 = it.next().a();
                    if ("sina".equals(a2)) {
                        imageButton.setImageResource(t.c("wy_sns_sina"));
                    } else if ("qq".equals(a2)) {
                        imageButton.setImageResource(t.c("wy_sns_qq"));
                    } else if ("sohu".equals(a2)) {
                        imageButton.setImageResource(t.c("wy_sns_sohu"));
                    } else if ("renren".equals(a2)) {
                        imageButton.setImageResource(t.c("wy_sns_renren"));
                    } else if ("kaixin".equals(a2)) {
                        imageButton.setImageResource(t.c("wy_sns_kaixin"));
                    } else if ("douban".equals(a2)) {
                        imageButton.setImageResource(t.c("wy_sns_douban"));
                    } else if ("139".equals(a2)) {
                        imageButton.setImageResource(t.c("wy_sns_139"));
                    }
                    i2 = i3 + 1;
                    imageButton.setId(i3 + 10000);
                    imageButton.setOnClickListener(this);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
                    layoutParams.weight = 1.0f;
                    this.e.addView(imageButton, layoutParams);
                } else {
                    return;
                }
            }
        }
    }

    private void d() {
        Intent intent = new Intent(this, this.g ? Login.class : SwitchAccount.class);
        intent.setFlags(67108864);
        if (!this.g && WiGame.a != null) {
            intent.putExtra("bound_users", WiGame.a);
            intent.putExtra("prompt_binding", this.j);
        }
        if (this.g) {
            intent.putExtra("force", this.i);
            intent.putExtra("prompt_binding", this.j);
        }
        startActivity(intent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void a(e eVar) {
        switch (eVar.a) {
            case 14:
                if (this.h) {
                    if (!this.j || WiGame.u().d()) {
                        WiGame.z();
                    } else {
                        Intent intent = new Intent(this, AccountRetrieval.class);
                        intent.putExtra("process_pending", true);
                        startActivity(intent);
                    }
                    finish();
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void b(final e e2) {
        switch (e2.a) {
            case 5:
                if (e2.c) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            UseAnotherAccount.this.a.setVisibility(4);
                            h.b(UseAnotherAccount.this.b);
                            Toast.makeText(UseAnotherAccount.this, (String) e2.e, 0).show();
                        }
                    });
                    return;
                } else {
                    f.b(((ag) e2.e).getId());
                    return;
                }
            case 6:
                if (e2.c) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            UseAnotherAccount.this.a.setVisibility(4);
                            h.b(UseAnotherAccount.this.b);
                            Toast.makeText(UseAnotherAccount.this, (String) e2.e, 0).show();
                        }
                    });
                } else {
                    this.h = true;
                }
                e2.d = false;
                return;
            case 90:
                e2.d = false;
                if (!e2.c) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            UseAnotherAccount.this.c();
                        }
                    });
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View view) {
        int id = view.getId();
        if (id == t.d("wy_b_submit")) {
            String trim = this.c.getText().toString().trim();
            boolean z = trim.indexOf(64) != -1;
            String trim2 = this.d.getText().toString().trim();
            if (TextUtils.isEmpty(trim)) {
                Toast.makeText(this, t.f("wy_toast_please_input_bound_info"), 0).show();
            } else if (z && !h.a((CharSequence) trim)) {
                Toast.makeText(this, t.f("wy_toast_email_is_invalid"), 0).show();
            } else if (TextUtils.isEmpty(trim2)) {
                Toast.makeText(this, t.f("wy_toast_password_cannot_be_empty"), 0).show();
            } else {
                this.a.setVisibility(0);
                h.a(this.b);
                String b2 = com.wiyun.game.e.a.b(h.d(trim2));
                String str = z ? trim : null;
                if (z) {
                    trim = null;
                }
                f.a(str, trim, b2);
            }
        } else if (id == t.d("wy_b_cancel")) {
            d();
            finish();
        } else if (id >= 10000) {
            ArrayList<ac> arrayList = WiGame.b;
            Intent intent = new Intent(this, OAuth.class);
            intent.putExtra("type", arrayList.get(id - 10000).a());
            intent.putExtra("login", true);
            intent.putExtra("prompt_binding", this.j);
            intent.putExtra("from_use_another_account", true);
            startActivity(intent);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(WiGame.i);
        requestWindowFeature(1);
        setContentView(t.e("wy_activity_use_another_account"));
        a();
        b();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        d.a().b((b) this);
        d.a().b((a) this);
        super.onDestroy();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (!this.i || keyCode != 4 || event.getRepeatCount() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        this.k = true;
        return true;
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (!this.i || keyCode != 4 || !this.k) {
            return super.onKeyUp(keyCode, event);
        }
        this.k = false;
        d();
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        c();
    }

    public boolean onSearchRequested() {
        return false;
    }
}
