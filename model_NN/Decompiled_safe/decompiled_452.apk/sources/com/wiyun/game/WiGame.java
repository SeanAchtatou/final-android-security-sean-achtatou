package com.wiyun.game;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.Toast;
import com.wiyun.game.a.d;
import com.wiyun.game.a.f;
import com.wiyun.game.a.i;
import com.wiyun.game.b.e;
import com.wiyun.game.model.ChallengeRequest;
import com.wiyun.game.model.ChallengeResult;
import com.wiyun.game.model.User;
import com.wiyun.game.model.a.ac;
import com.wiyun.game.model.a.ag;
import com.wiyun.game.model.a.ai;
import com.wiyun.game.model.a.u;
import com.wiyun.game.model.a.v;
import java.io.File;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;

public class WiGame {
    /* access modifiers changed from: private */
    public static boolean A = false;
    /* access modifiers changed from: private */
    public static boolean B = false;
    private static boolean C = false;
    private static String D = null;
    /* access modifiers changed from: private */
    public static boolean E = false;
    /* access modifiers changed from: private */
    public static int F = 0;
    /* access modifiers changed from: private */
    public static int G = 0;
    /* access modifiers changed from: private */
    public static int H = 0;
    /* access modifiers changed from: private */
    public static int I = 0;
    private static int J = -1;
    private static String K = null;
    private static String L = null;
    private static String M = null;
    private static String N = null;
    /* access modifiers changed from: private */
    public static Location O = null;
    private static int P = -1;
    private static int Q = -1;
    /* access modifiers changed from: private */
    public static ag R = null;
    /* access modifiers changed from: private */
    public static Context S = null;
    /* access modifiers changed from: private */
    public static boolean T = false;
    public static final int TOAST_SIDE_BOTTOM = 1;
    public static final int TOAST_SIDE_TOP = 0;
    /* access modifiers changed from: private */
    public static c U;
    private static List<WiGameClient> V;
    private static Object W = new Object();
    /* access modifiers changed from: private */
    public static Handler X;
    private static int Y;
    private static String Z;
    static ArrayList<ag> a;
    private static Intent aa;
    /* access modifiers changed from: private */
    public static ChallengeResult ab;
    /* access modifiers changed from: private */
    public static List<b> ac;
    /* access modifiers changed from: private */
    public static boolean ad;
    /* access modifiers changed from: private */
    public static boolean ae;
    /* access modifiers changed from: private */
    public static boolean af;
    private static boolean ag;
    private static boolean ah;
    private static boolean ai;
    private static final com.wiyun.game.b.a aj = new l();
    /* access modifiers changed from: private */
    public static final LocationListener ak = new m();
    static ArrayList<ac> b;
    static long c;
    static Context d;
    static String e;
    static File f;
    static File g;
    static int h = 1;
    static int i = 1;
    static boolean j;
    static List<d> k;
    static a l;
    static String m;
    static boolean n;
    static boolean o;
    private static String p;
    private static String q;
    private static String r;
    private static String s;
    private static String t = null;
    private static boolean u = false;
    private static int v = 0;
    private static boolean w = false;
    /* access modifiers changed from: private */
    public static boolean x;
    /* access modifiers changed from: private */
    public static boolean y;
    /* access modifiers changed from: private */
    public static boolean z = true;

    static final class a {
        String a;
        String b;
        byte[] c;
        byte[] d;
        boolean e;

        a() {
        }
    }

    static final class b {
        String a;
        String b;
        long c;
        boolean d;

        b() {
        }

        public void a(ContentValues values) {
            values.put("uid", this.a);
            values.put("ach_id", this.b);
            values.put("ut", Long.valueOf(this.c));
            values.put("done", Boolean.valueOf(this.d));
            i a2 = new i();
            a2.a(true);
            StringBuilder buf = new StringBuilder();
            buf.append(this.b).append(':').append(this.c);
            values.put("sig", h.a(a2, com.wiyun.game.e.a.a(h.d(buf.toString())), "wiyun.db"));
        }
    }

    private static final class c implements View.OnClickListener {
        private int a;

        c(int mode) {
            this.a = mode;
        }

        public void onClick(View v) {
            int id = v.getId();
            if (id == t.d("wy_b_show_challenge")) {
                switch (this.a) {
                    case 1:
                        WiGame.openPendingChallenges();
                        break;
                    case 2:
                        WiGame.openPendingFriends();
                        break;
                    case 3:
                        WiGame.openUnreadMessages();
                        break;
                    case 4:
                        WiGame.openUnreadNotices();
                        break;
                    case 5:
                        WiGame.openMyProfile();
                        break;
                }
                WiGame.X.removeMessages(11, v.getTag());
                WiGame.X.sendMessage(Message.obtain(WiGame.X, 11, v.getTag()));
            } else if (id == t.d("wy_b_challenge_later")) {
                WiGame.X.removeMessages(11, v.getTag());
                WiGame.X.sendMessage(Message.obtain(WiGame.X, 11, v.getTag()));
            }
        }
    }

    static final class d implements Serializable {
        String a;
        String b;
        int c;
        byte[] d;
        long e;
        double f;
        double g;
        boolean h;
        boolean i;
        long j;

        d() {
        }

        public void a(ContentValues values) {
            values.put("uid", this.a);
            values.put("lb_id", this.b);
            values.put("score", Integer.valueOf(this.c));
            values.put("ct", Long.valueOf(this.e));
            values.put("done", Boolean.valueOf(this.h));
            values.put("lat", Double.valueOf(this.f));
            values.put("lon", Double.valueOf(this.g));
            if (this.d != null) {
                values.put("_blob", this.d);
            }
            i a2 = new i();
            a2.a(true);
            StringBuilder buf = new StringBuilder();
            buf.append(this.b).append(':').append(this.c).append(':').append(this.e).append(':').append(this.f).append(':').append(this.g).append(':').append(this.d == null ? "" : Arrays.toString(com.wiyun.game.e.a.a(this.d)));
            values.put("sig", h.a(a2, com.wiyun.game.e.a.a(h.d(buf.toString())), "wiyun.db"));
        }
    }

    static {
        try {
            Field field = Build.VERSION.class.getField("SDK_INT");
            n = field != null && field.getInt(null) > 3;
        } catch (Exception e2) {
            n = false;
        }
    }

    static void A() {
        if (V != null) {
            for (WiGameClient client : V) {
                client.wyLogInFailed();
            }
        }
    }

    static String B() {
        try {
            Class<?> cls = Class.forName("android.os.SystemProperties");
            return (String) cls.getMethod("get", String.class).invoke(cls, "ro.jq.channel.name");
        } catch (Exception e2) {
            return "unknown";
        }
    }

    static String C() {
        try {
            Class<?> cls = Class.forName("android.os.SystemProperties");
            Method method = cls.getMethod("get", String.class);
            int c2 = h.c((String) method.invoke(cls, "ro.jq.channel.packages"));
            if (c2 <= 0) {
                return null;
            }
            StringBuilder sb = new StringBuilder();
            for (int i2 = 1; i2 <= c2; i2++) {
                sb.append((String) method.invoke(cls, "ro.jq.channel.package." + i2)).append(',');
            }
            if (sb.length() > 0) {
                sb.deleteCharAt(sb.length() - 1);
            }
            return sb.toString();
        } catch (Exception e2) {
            return null;
        }
    }

    static void D() {
        boolean z2;
        boolean z3;
        if (S != null) {
            PackageManager packageManager = S.getPackageManager();
            try {
                z2 = packageManager.getPackageGids("com.lenovo.appstore") != null;
            } catch (PackageManager.NameNotFoundException e2) {
                z2 = false;
            }
            if (!z2) {
                try {
                    z3 = packageManager.getPackageGids("com.lenovo.leos.appstore") != null;
                } catch (PackageManager.NameNotFoundException e3) {
                    z3 = z2;
                }
            } else {
                z3 = z2;
            }
            if (z3) {
                M = "lenovo";
                if (S.getResources().getDisplayMetrics().heightPixels > 800) {
                    N = "lepad";
                } else {
                    N = "lephone";
                }
            } else {
                M = Build.BRAND;
                N = Build.MODEL;
            }
        }
    }

    static String E() {
        if (M == null) {
            D();
        }
        return M;
    }

    static String F() {
        if (N == null) {
            D();
        }
        return N;
    }

    static boolean G() {
        E();
        return "lephone".equals(N);
    }

    static boolean H() {
        if (P == -1) {
            try {
                P = Class.forName("com.wiyun.game.UserMap") != null ? 1 : 0;
            } catch (Throwable th) {
                P = 0;
            }
        }
        return P == 1;
    }

    static int I() {
        if (Q == -1) {
            Q = (int) (S.getResources().getDisplayMetrics().density * 160.0f);
        }
        return Q;
    }

    static Bitmap a(String id, String url, int defaultId) {
        if (U == null) {
            return null;
        }
        return U.a(id, url, defaultId);
    }

    static d a(long callId) {
        if (k != null) {
            for (d ps : k) {
                if (ps.j == callId) {
                    return ps;
                }
            }
        }
        return null;
    }

    static void a() {
        synchronized (W) {
            x = false;
            t = null;
            R = new ag();
            E = false;
            a = null;
        }
    }

    static void a(long callId, User user) {
        if (V != null) {
            for (WiGameClient client : V) {
                client.wyUserInfoGot(callId, user);
            }
        }
    }

    static void a(long callId, String userId) {
        if (V != null) {
            for (WiGameClient client : V) {
                client.wyGetUserInfoFailed(callId, userId);
            }
        }
    }

    static void a(long callId, String userId, int start) {
        if (V != null) {
            for (WiGameClient client : V) {
                client.wyGetFriendListFailed(callId, userId, start);
            }
        }
    }

    static void a(long callId, String userId, ArrayList<User> friends, int start) {
        if (V != null) {
            for (WiGameClient client : V) {
                client.wyFriendListGot(callId, userId, friends, start);
            }
        }
    }

    static void a(Context context) {
        if (S == null) {
            init(context, p, q, e, h == 0, j);
        }
    }

    static void a(Intent intent) {
        if (S != null) {
            S.sendBroadcast(intent);
        }
    }

    static void a(Bundle outState) {
        outState.putString("app_key", p);
        outState.putString("secret_key", q);
        outState.putString("session_key", t);
    }

    static void a(ChallengeRequest request) {
        if (V != null) {
            for (WiGameClient client : V) {
                client.wyPlayChallenge(request);
            }
        }
    }

    static void a(ChallengeResult result) {
        if (V != null) {
            for (WiGameClient client : V) {
                client.wyChallengeResultSubmitted(result);
            }
        }
    }

    static void a(String id) {
        h.a(new File(f, "wiyun_id"), h.a((i) null, id, "wiyun_id"));
    }

    static void a(String name, int totalSize) {
        if (V != null) {
            for (WiGameClient client : V) {
                client.wyGameSaveStart(name, totalSize);
            }
        }
    }

    static void a(String leaderboardId, int score, int rank) {
        if (V != null) {
            for (WiGameClient client : V) {
                client.wyScoreSubmitted(leaderboardId, score, rank);
            }
        }
    }

    static void a(String str, v vVar, u uVar, byte[] bArr) {
        if (S != null) {
            ChallengeRequest challengeRequest = new ChallengeRequest();
            challengeRequest.setChallengeDefinitionId(str);
            challengeRequest.setCtuId(vVar.a());
            challengeRequest.setFromUsername(uVar.b());
            challengeRequest.setScore(uVar.d());
            challengeRequest.setPortraitUrl(uVar.c());
            challengeRequest.setBid(uVar.a());
            challengeRequest.setFromUserId(uVar.e());
            if (bArr != null) {
                challengeRequest.setBlob(bArr);
            }
            X.sendMessage(Message.obtain(X, 1014, challengeRequest));
            S.sendBroadcast(new Intent("com.wiyun.game.RESET"));
        }
    }

    static void a(boolean loggingIn) {
        w = loggingIn;
    }

    static void a(byte[] data) {
        h.a(new File(f, "wiyun_app_config"), data);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public static void ac() {
        boolean z2;
        if (y && B) {
            if (z) {
                z = false;
                if ((a == null || a.isEmpty()) && Y == 0) {
                    Intent intent = new Intent(S, Login.class);
                    intent.setFlags(67108864);
                    intent.putExtra("force", !A);
                    intent.putExtra("prompt_binding", true);
                    S.startActivity(intent);
                    z2 = true;
                    if (!z2 && C && Y == 0 && a != null && !a.isEmpty()) {
                        C = false;
                        Intent intent2 = new Intent(S, SwitchAccount.class);
                        intent2.setFlags(67108864);
                        intent2.putParcelableArrayListExtra("bound_users", a);
                        S.startActivity(intent2);
                        return;
                    }
                    return;
                }
            }
            z2 = false;
            if (!z2) {
            }
        }
    }

    private static void ad() {
        X.sendEmptyMessage(3);
        f.a(ab.getCtuId(), ab.getResult(), ab.getScore(), ab.getBlob());
    }

    public static void addFriend(String userId, String message) {
        f.b(userId, message);
    }

    public static void addWiGameClient(WiGameClient client) {
        if (V != null && !V.contains(client)) {
            V.add(client);
        }
    }

    /* access modifiers changed from: private */
    public static void ae() {
        synchronized (W) {
            if (O == null) {
                LocationManager locationManager = (LocationManager) S.getSystemService("location");
                Location lastKnownLocation = locationManager.getLastKnownLocation("network");
                Location lastKnownLocation2 = lastKnownLocation == null ? locationManager.getLastKnownLocation("gps") : lastKnownLocation;
                if (lastKnownLocation2 == null) {
                    if (S.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
                        x().post(new Runnable() {
                            public void run() {
                                try {
                                    ((LocationManager) WiGame.S.getSystemService("location")).requestLocationUpdates("network", 600000, 1000.0f, WiGame.ak);
                                } catch (Exception e) {
                                }
                            }
                        });
                    }
                    if (S.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0) {
                        x().post(new Runnable() {
                            public void run() {
                                try {
                                    ((LocationManager) WiGame.S.getSystemService("location")).requestLocationUpdates("gps", 600000, 1000.0f, WiGame.ak);
                                } catch (Exception e) {
                                }
                            }
                        });
                    }
                } else {
                    O = lastKnownLocation2;
                    af();
                }
            } else {
                af();
            }
        }
    }

    /* access modifiers changed from: private */
    public static void af() {
        if (R != null && !TextUtils.isEmpty(getMyId()) && O != null && !w()) {
            f.a(O.getLatitude(), O.getLongitude());
            try {
                CookieManager.getInstance().setCookie("http://wiyun.com", String.format("wigame_config_location=%f,%f;domain=%s", Double.valueOf(getLatitude()), Double.valueOf(getLongitude()), "wiyun.com"));
            } catch (Throwable th) {
            }
        }
    }

    private static String ag() {
        Context context = getContext();
        if (context == null) {
            c("Context is not set");
            return null;
        } else if (context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == -1) {
            c("Cannot get a device ID.  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.READ_PHONE_STATE\" />");
            return null;
        } else {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            if (telephonyManager != null) {
                return telephonyManager.getDeviceId();
            }
            return null;
        }
    }

    private static boolean ah() {
        Context context = getContext();
        if (context == null) {
            return false;
        }
        SensorManager sensorManager = (SensorManager) context.getSystemService("sensor");
        if (sensorManager == null) {
            return false;
        }
        return sensorManager.getDefaultSensor(2) != null;
    }

    static void b() {
        com.wiyun.game.b.d.a().a(aj);
    }

    static void b(Bundle state) {
        p = state.getString("app_key");
        q = state.getString("secret_key");
        t = state.getString("session_key");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wiyun.game.h.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.wiyun.game.h.a(int, java.lang.String):int
      com.wiyun.game.h.a(java.lang.String, java.lang.String):java.lang.String
      com.wiyun.game.h.a(android.view.View, int):void
      com.wiyun.game.h.a(java.io.File, byte[]):boolean
      com.wiyun.game.h.a(android.content.Context, int):byte[]
      com.wiyun.game.h.a(java.lang.String, boolean):void */
    /* access modifiers changed from: private */
    public static void b(Message message) {
        switch (message.what) {
            case 1:
                if (S != null && R != null) {
                    h.i(String.format(t.h("wy_toast_welcome_back"), R.getName()));
                    return;
                }
                return;
            case 2:
                if (S != null && R != null) {
                    h.a(String.format(t.h("wy_toast_welcome_back"), R.getName()), true);
                    return;
                }
                return;
            case 3:
                if (S != null) {
                    h.i(t.h("wy_toast_submitting_challenge_result"));
                    return;
                }
                return;
            case 4:
                if (S != null) {
                    h.i(t.h("wy_toast_challenge_result_submitted"));
                    return;
                }
                return;
            case 5:
                if (S != null) {
                    h.a(t.h("wy_toast_challenge_result_cached"), true);
                    return;
                }
                return;
            case 6:
                f.h();
                e.b(S, p);
                return;
            case 7:
                if (S != null) {
                    e eVar = (e) message.obj;
                    if (eVar.g == 0) {
                        String a2 = eVar.a("leaderboard_id");
                        int c2 = i.c(a2);
                        com.wiyun.game.model.a.c b2 = x.b(a2);
                        if (b2 == null) {
                            return;
                        }
                        if (b2.c()) {
                            int i2 = c2 % 1000;
                            int i3 = c2 / 1000;
                            String format = String.format("%d:%d.%d", Integer.valueOf(i3 / 60), Integer.valueOf(i3 % 60), Integer.valueOf(i2));
                            h.i(String.format(t.h("wy_toast_score_submitted_with_rank_0"), format));
                            return;
                        }
                        h.i(String.format(t.h("wy_toast_score_submitted_with_rank_0"), String.valueOf(c2)));
                        return;
                    }
                    h.i(String.format(t.h("wy_toast_score_submitted_with_rank"), Integer.valueOf(eVar.g)));
                    return;
                }
                return;
            case 8:
                h.i(t.h("wy_toast_score_cached"));
                return;
            case 9:
                if (S == null) {
                    return;
                }
                if (message.obj != null) {
                    h.i(String.format(t.h("wy_toast_x_achievement_unlocked"), (String) message.obj));
                    return;
                }
                h.i(t.h("wy_toast_achievement_unlocked"));
                return;
            case 10:
                h.a(S, (String) message.obj, t.a("wy_toast_enter"), new c(message.arg1), t.d("wy_b_show_challenge"), t.d("wy_b_challenge_later"));
                return;
            case 11:
                h.a((View) message.obj);
                return;
            case 12:
                View view = (View) message.obj;
                if (view != null && view.getParent() != null) {
                    ((ViewGroup) view.getParent()).removeView(view);
                    return;
                }
                return;
            case 13:
                if (S != null && R != null) {
                    h.i(t.h("wy_toast_device_banned"));
                    return;
                }
                return;
            case 14:
                f.b();
                X.sendEmptyMessageDelayed(14, 300000);
                return;
            case 1000:
                e(t);
                return;
            case 1001:
                A();
                return;
            case 1002:
                h((String) message.obj);
                return;
            case 1003:
                a((String) message.obj, message.arg1);
                return;
            case 1004:
                e eVar2 = (e) message.obj;
                a(h.b(eVar2.a("call_id")), (User) eVar2.e);
                return;
            case 1005:
                e eVar3 = (e) message.obj;
                a(h.b(eVar3.a("call_id")), eVar3.a("user_id"));
                return;
            case 1006:
                e eVar4 = (e) message.obj;
                a(h.b(eVar4.a("call_id")), eVar4.a("user_id"), (ArrayList) eVar4.e, h.c(eVar4.a("start")));
                return;
            case 1007:
                e eVar5 = (e) message.obj;
                a(h.b(eVar5.a("call_id")), eVar5.a("user_id"), h.c(eVar5.a("start")));
                return;
            case 1008:
                i((String) message.obj);
                return;
            case 1009:
                j((String) message.obj);
                return;
            case 1010:
                k((String) message.obj);
                return;
            case 1012:
                a((String) message.obj, message.arg1, message.arg2);
                return;
            case 1013:
                a((ChallengeResult) message.obj);
                return;
            case 1014:
                a((ChallengeRequest) message.obj);
                return;
            default:
                return;
        }
    }

    static void b(String name) {
        h.a(new File(f, "wiyun_name"), h.a((i) null, name, "wiyun_name"));
    }

    static void c(String message) {
        Log.e("WiYun", message);
        throw new IllegalArgumentException(message);
    }

    static boolean c() {
        return E;
    }

    static String d() {
        try {
            return String.valueOf(S.getPackageManager().getPackageInfo(S.getPackageName(), 0).versionCode);
        } catch (Exception e2) {
            return "1.0";
        }
    }

    static void d(String sessionKey) {
        t = sessionKey;
    }

    public static void destroy(Context context) {
        synchronized (W) {
            if (S != null) {
                if (context.hashCode() == S.hashCode()) {
                    T = false;
                    com.wiyun.game.a.d.a();
                    f.b();
                    com.wiyun.game.b.d.a().d();
                    com.wiyun.game.b.d.a().b(aj);
                    if (U != null) {
                        U.b();
                        U = null;
                    }
                    ((LocationManager) S.getSystemService("location")).removeUpdates(ak);
                    f.b();
                    f.c();
                    y();
                    i.a();
                    x.c();
                    X.removeMessages(6);
                    X.removeMessages(14);
                    X = null;
                    e.a = false;
                    w = false;
                    ac.clear();
                    ac = null;
                    if (b != null) {
                        b.clear();
                        b = null;
                    }
                    V.clear();
                    V = null;
                    k.clear();
                    k = null;
                    Y = 0;
                    a = null;
                    ae = false;
                    ai = false;
                    O = null;
                    t = null;
                    S = null;
                    p = null;
                    q = null;
                    R = null;
                    E = false;
                    x = false;
                    C = false;
                    z = true;
                    A = false;
                    B = false;
                    y = false;
                    ab = null;
                }
            }
        }
    }

    static void e(String sessionKey) {
        if (V != null) {
            for (WiGameClient client : V) {
                client.wyLoggedIn(sessionKey);
            }
        }
    }

    static boolean e() {
        if (S == null) {
            return false;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) S.getSystemService("connectivity");
        if (connectivityManager == null) {
            return true;
        }
        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(1);
        if (networkInfo != null && networkInfo.isAvailable()) {
            return true;
        }
        NetworkInfo networkInfo2 = connectivityManager.getNetworkInfo(0);
        if (networkInfo2 != null) {
            return networkInfo2.isAvailable();
        }
        return false;
    }

    static void f(String name) {
        if (V != null) {
            for (WiGameClient client : V) {
                client.wyGameSaved(name);
            }
        }
    }

    static boolean f() {
        return O != null;
    }

    static void g(String name) {
        if (V != null) {
            for (WiGameClient client : V) {
                client.wyGameSaveFailed(name);
            }
        }
    }

    static boolean g() {
        return O != null;
    }

    public static String getChannel() {
        if (D == null) {
            D = B();
        }
        return D;
    }

    public static Context getContext() {
        return S;
    }

    public static long getFriends(String userId) {
        return f.a(userId, 0, 25);
    }

    public static long getFriends(String userId, int start, int count) {
        return f.a(userId, start, count);
    }

    public static double getLatitude() {
        if (O == null) {
            return 0.0d;
        }
        if (O.getLatitude() < 4.999999873689376E-5d) {
            return 0.0d;
        }
        return O.getLatitude();
    }

    public static double getLongitude() {
        if (O == null) {
            return 0.0d;
        }
        if (O.getLongitude() < 4.999999873689376E-5d) {
            return 0.0d;
        }
        return O.getLongitude();
    }

    public static String getMyId() {
        return (R != null && !TextUtils.isEmpty(R.getId())) ? R.getId() : "";
    }

    public static String getMyName() {
        return R.getName() == null ? "" : R.getName();
    }

    public static Bitmap getMyPortrait() {
        return h.b(null, true, h.b("p_", getMyId()), u().getAvatarUrl());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wiyun.game.h.a(java.util.Map<java.lang.String, android.graphics.Bitmap>, boolean, java.lang.String, java.lang.String, boolean):android.graphics.Bitmap
     arg types: [?[OBJECT, ARRAY], int, java.lang.String, java.lang.String, boolean]
     candidates:
      com.wiyun.game.h.a(int, int, int, int, android.net.Uri):android.content.Intent
      com.wiyun.game.h.a(android.content.Context, java.lang.String, int, android.view.View$OnClickListener, int[]):void
      com.wiyun.game.h.a(java.util.Map<java.lang.String, android.graphics.Bitmap>, boolean, java.lang.String, java.lang.String, boolean):android.graphics.Bitmap */
    public static Bitmap getPortrait(User user) {
        return h.a((Map<String, Bitmap>) null, true, h.b("p_", user.getId()), user.getAvatarUrl(), user.isFemale());
    }

    public static Bitmap getPortrait(String str) {
        return h.b(null, true, h.b("p_", str), null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wiyun.game.h.a(java.util.Map<java.lang.String, android.graphics.Bitmap>, boolean, java.lang.String, java.lang.String, boolean):android.graphics.Bitmap
     arg types: [?[OBJECT, ARRAY], int, java.lang.String, java.lang.String, int]
     candidates:
      com.wiyun.game.h.a(int, int, int, int, android.net.Uri):android.content.Intent
      com.wiyun.game.h.a(android.content.Context, java.lang.String, int, android.view.View$OnClickListener, int[]):void
      com.wiyun.game.h.a(java.util.Map<java.lang.String, android.graphics.Bitmap>, boolean, java.lang.String, java.lang.String, boolean):android.graphics.Bitmap */
    public static Bitmap getPortrait(String str, String str2) {
        return h.a((Map<String, Bitmap>) null, true, h.b("p_", str), str2, false);
    }

    public static int getToastSide() {
        return v;
    }

    public static long getUserInfo(String userId) {
        return f.d(userId);
    }

    static void h(String blobPath) {
        if (V != null) {
            for (WiGameClient client : V) {
                client.wyLoadGame(blobPath);
            }
        }
    }

    static boolean h() {
        return f() && g();
    }

    static void i() {
        File file = new File(f, "wiyun_app_config");
        if (file.exists()) {
            String json = h.b(h.b(file));
            if (!TextUtils.isEmpty(json)) {
                try {
                    ai c2 = ai.a(new JSONObject(json));
                    x.a(c2.c());
                    x.b(c2.d());
                } catch (JSONException e2) {
                }
            }
        }
    }

    static void i(String friendId) {
        if (V != null) {
            for (WiGameClient client : V) {
                client.wyFriendRequestSent(friendId);
            }
        }
    }

    public static void init(Context context, String appKey, String secretKey) {
        init(context, appKey, secretKey, false, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wiyun.game.h.a(byte[], byte[], boolean):boolean
     arg types: [byte[], byte[], int]
     candidates:
      com.wiyun.game.h.a(int, java.lang.String, int):int
      com.wiyun.game.h.a(java.lang.String, java.lang.String, int):int
      com.wiyun.game.h.a(android.graphics.Bitmap, int, int):android.graphics.Bitmap
      com.wiyun.game.h.a(byte[], int, int):java.lang.String
      com.wiyun.game.h.a(android.content.Context, java.lang.String, boolean):void
      com.wiyun.game.h.a(java.lang.String, boolean, int):void
      com.wiyun.game.h.a(java.lang.StringBuilder, java.lang.String, java.lang.String):void
      com.wiyun.game.h.a(com.wiyun.game.a.i, java.lang.String, java.lang.String):byte[]
      com.wiyun.game.h.a(com.wiyun.game.a.i, byte[], java.lang.String):byte[]
      com.wiyun.game.h.a(byte[], byte[], boolean):boolean */
    public static void init(Context context, String str, String str2, String str3, boolean z2, boolean z3) {
        if (S != null) {
            destroy(S);
        }
        if (context instanceof Activity) {
            i = ((Activity) context).getRequestedOrientation();
        }
        f.a();
        S = context;
        CookieSyncManager.createInstance(S);
        e = str3;
        f = context.getFilesDir();
        g = context.getCacheDir();
        c = 0;
        p = str;
        q = str2;
        R = new ag();
        j = z3;
        h = z2 ? 0 : 1;
        ac = new ArrayList();
        V = new ArrayList();
        k = new ArrayList();
        X = new Handler(Looper.getMainLooper()) {
            public void handleMessage(Message msg) {
                WiGame.b(msg);
            }
        };
        byte[] b2 = h.b(new File(f, "wiyun_id"));
        if (b2 != null) {
            R.i(h.b((i) null, h.b(b2), "wiyun_id"));
            R.j(h.b((i) null, h.b(h.b(new File(f, "wiyun_name"))), "wiyun_name"));
        }
        i.a(f);
        i();
        com.wiyun.game.a.d.a(S, (d.a) null);
        U = new c(S);
        U.a();
        b();
        com.wiyun.game.b.d.a().c();
        if (TextUtils.isEmpty(getMyId())) {
            x = true;
            f.g();
        } else {
            f.b(getMyId());
        }
        f.c((String) null);
        if (s()) {
            setSandboxMode(true);
        }
        T = true;
        X.sendEmptyMessageDelayed(14, 300000);
        try {
            Class<?> cls = Class.forName("com.wiyun.distribute.DistributeConfig");
            if (cls != null) {
                Field field = cls.getField("channelName");
                if (field != null) {
                    D = (String) field.get(null);
                }
                Field field2 = cls.getField("disableCharge");
                if (field2 != null) {
                    ag = field2.getBoolean(null);
                }
                Field field3 = cls.getField("checkRomInfo");
                if (field3 != null) {
                    ai = field3.getBoolean(null);
                }
            }
        } catch (Exception e2) {
        }
        if (ai) {
            String B2 = B();
            if (!TextUtils.isEmpty(B2) && !"unknown".equals(B2)) {
                D = B2;
            }
            if (!o) {
                String C2 = C();
                if (!TextUtils.isEmpty(C2)) {
                    o = m(C2);
                    if (o) {
                        e.a(S, com.wiyun.game.e.a.c(C2));
                    }
                }
            }
        }
        e.b(S, p);
        if (context.checkCallingOrSelfPermission("android.permission.INTERNET") != 0) {
            Toast.makeText(context, "you must add INTERNET permission", 0).show();
        }
        if (context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") != 0) {
            Toast.makeText(context, "you must add READ_PHONE_STATE permission", 0).show();
        }
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_WIFI_STATE") != 0) {
            Toast.makeText(context, "you must add ACCESS_WIFI_STATE permission", 0).show();
        }
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") != 0) {
            Toast.makeText(context, "you must add android.permission.ACCESS_NETWORK_STATE permission", 0).show();
        }
        if (context.checkCallingOrSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE") != 0) {
            Toast.makeText(context, "you must add android.permission.WRITE_EXTERNAL_STORAGE permission", 0).show();
        }
        if (!n) {
            Toast.makeText(context, "WiGame only support Android 1.6 or higher version", 0).show();
        }
        if (!"com.wiyun.game".equals(S.getPackageName()) && h.a(h.a(S, t.e("wy_activity_usermap")), h.d("0OY_NeVg2RsyrJJdZLDHjGwb0u-KU29bKFt6YYQ"), true)) {
            Toast.makeText(S, t.f("wy_toast_replace_map_key"), 1).show();
        }
    }

    public static void init(Context context, String appKey, String secretKey, boolean landscape, boolean fullscreen) {
        init(context, appKey, secretKey, d(), landscape, fullscreen);
    }

    public static boolean isAchievementUnlocked(String str) {
        if (S == null) {
            throw new IllegalStateException("You should call init before unlock achievement");
        } else if (!TextUtils.isEmpty(str)) {
            return i.a(str);
        } else {
            throw new IllegalArgumentException("achievement id is empty");
        }
    }

    public static boolean isHideNoticeToast() {
        return ad;
    }

    public static boolean isHideRecharge() {
        return ag;
    }

    public static boolean isHideScoreToast() {
        return af;
    }

    public static boolean isHideWelcomeBackToast() {
        return ae;
    }

    public static boolean isInited() {
        return S != null;
    }

    public static boolean isLoggedIn() {
        return !TextUtils.isEmpty(t);
    }

    public static boolean isSandboxMode() {
        return u;
    }

    public static boolean isSwitchAccountForbidden() {
        return ah;
    }

    static void j() {
        new File(f, "wiyun_id").delete();
    }

    static void j(String userId) {
        if (V != null) {
            for (WiGameClient client : V) {
                client.wySendFriendRequestFailed(userId);
            }
        }
    }

    static void k() {
        new File(f, "wiyun_name").delete();
    }

    static void k(String userId) {
        if (V != null) {
            for (WiGameClient client : V) {
                client.wyPortraitGot(userId);
            }
        }
    }

    static void l() {
        if (U != null) {
            U.c();
        }
    }

    static String m() {
        return p;
    }

    private static boolean m(String str) {
        String[] split = str.split(",");
        PackageManager packageManager = S.getPackageManager();
        int length = split.length;
        int i2 = 0;
        while (i2 < length) {
            try {
                if (packageManager.getPackageGids(split[i2]) == null) {
                    return false;
                }
                i2++;
            } catch (PackageManager.NameNotFoundException e2) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: private */
    public static b n(String achievementId) {
        if (ac != null) {
            for (b pa : ac) {
                if (pa.b.equals(achievementId)) {
                    return pa;
                }
            }
        }
        return null;
    }

    static String n() {
        return q;
    }

    static String o() {
        Context context = getContext();
        if (context == null) {
            return null;
        }
        if (context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == -1) {
            return null;
        }
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        String line1Number = telephonyManager == null ? null : telephonyManager.getLine1Number();
        if (TextUtils.isEmpty(line1Number)) {
            return null;
        }
        byte[] d2 = h.d(line1Number);
        for (int i2 = 0; i2 < d2.length; i2++) {
            d2[i2] = (byte) (((((d2[i2] >> 4) & 15) | ((d2[i2] & 15) << 4)) ^ 255) & 255);
        }
        return com.wiyun.game.e.c.b(d2);
    }

    private static boolean o(String s2) {
        if (s2 == null) {
            return false;
        }
        int length = s2.length();
        for (int i2 = 0; i2 < length; i2++) {
            if (s2.charAt(i2) != '0') {
                return true;
            }
        }
        return false;
    }

    public static void openAchievements() {
        if (S == null) {
            throw new IllegalStateException("You should call init before open achievements");
        } else if (!e() || !TextUtils.isEmpty(getMyId())) {
            Intent intent = new Intent(S, Home.class);
            intent.setFlags(268435456);
            intent.putExtra("pending_action", 11);
            S.startActivity(intent);
        } else {
            x = true;
            Intent intent2 = new Intent(S, SwitchAccount.class);
            intent2.setFlags(67108864);
            if (a != null) {
                intent2.putParcelableArrayListExtra("bound_users", a);
            }
            S.startActivity(intent2);
            Y = 11;
        }
    }

    public static void openDeveloperGames() {
        if (S == null) {
            throw new IllegalStateException("You should call init before open developer games");
        } else if (!e() || !TextUtils.isEmpty(getMyId())) {
            Intent intent = new Intent(S, Home.class);
            intent.setFlags(268435456);
            intent.putExtra("pending_action", 12);
            S.startActivity(intent);
        } else {
            x = true;
            Intent intent2 = new Intent(S, SwitchAccount.class);
            intent2.setFlags(67108864);
            if (a != null) {
                intent2.putParcelableArrayListExtra("bound_users", a);
            }
            S.startActivity(intent2);
            Y = 12;
        }
    }

    public static void openDiscussion() {
        if (S == null) {
            throw new IllegalStateException("You should call init before open discussion");
        } else if (!e() || !TextUtils.isEmpty(getMyId())) {
            Intent intent = new Intent(S, Home.class);
            intent.setFlags(268435456);
            intent.putExtra("pending_action", 10);
            S.startActivity(intent);
        } else {
            x = true;
            Intent intent2 = new Intent(S, SwitchAccount.class);
            intent2.setFlags(67108864);
            if (a != null) {
                intent2.putParcelableArrayListExtra("bound_users", a);
            }
            S.startActivity(intent2);
            Y = 10;
        }
    }

    public static void openLeaderboard(String str) {
        if (S == null) {
            throw new IllegalStateException("You should call init before open leaderboard");
        } else if (!e() || !TextUtils.isEmpty(getMyId())) {
            Intent intent = new Intent(S, Home.class);
            intent.setFlags(268435456);
            intent.putExtra("pending_action", 2);
            intent.putExtra("lb_id", str);
            S.startActivity(intent);
        } else {
            x = true;
            Intent intent2 = new Intent(S, SwitchAccount.class);
            intent2.setFlags(67108864);
            if (a != null) {
                intent2.putParcelableArrayListExtra("bound_users", a);
            }
            S.startActivity(intent2);
            Y = 2;
            Z = str;
        }
    }

    public static void openLeaderboards() {
        if (S == null) {
            throw new IllegalStateException("You should call init before open leaderboard");
        } else if (!e() || !TextUtils.isEmpty(getMyId())) {
            Intent intent = new Intent(S, Home.class);
            intent.setFlags(268435456);
            intent.putExtra("pending_action", 3);
            S.startActivity(intent);
        } else {
            x = true;
            Intent intent2 = new Intent(S, SwitchAccount.class);
            intent2.setFlags(67108864);
            if (a != null) {
                intent2.putParcelableArrayListExtra("bound_users", a);
            }
            S.startActivity(intent2);
            Y = 3;
        }
    }

    public static void openMyProfile() {
        if (S == null) {
            throw new IllegalStateException("You should call init before open my profile");
        } else if (!e() || !TextUtils.isEmpty(getMyId())) {
            Intent intent = new Intent(S, Home.class);
            intent.setFlags(268435456);
            intent.putExtra("pending_action", 14);
            S.startActivity(intent);
        } else {
            x = true;
            Intent intent2 = new Intent(S, SwitchAccount.class);
            intent2.setFlags(67108864);
            if (a != null) {
                intent2.putParcelableArrayListExtra("bound_users", a);
            }
            S.startActivity(intent2);
            Y = 14;
        }
    }

    public static void openPendingChallenges() {
        if (S == null) {
            throw new IllegalStateException("You should call init before open pending challenges");
        } else if (!e() || !TextUtils.isEmpty(getMyId())) {
            Intent intent = new Intent(S, Home.class);
            intent.setFlags(268435456);
            intent.putExtra("pending_action", 13);
            S.startActivity(intent);
        } else {
            x = true;
            Intent intent2 = new Intent(S, SwitchAccount.class);
            intent2.setFlags(67108864);
            if (a != null) {
                intent2.putParcelableArrayListExtra("bound_users", a);
            }
            S.startActivity(intent2);
            Y = 13;
        }
    }

    public static void openPendingFriends() {
        if (S == null) {
            throw new IllegalStateException("You should call init before open pending friends");
        } else if (!e() || !TextUtils.isEmpty(getMyId())) {
            Intent intent = new Intent(S, Home.class);
            intent.setFlags(268435456);
            intent.putExtra("pending_action", 15);
            S.startActivity(intent);
        } else {
            x = true;
            Intent intent2 = new Intent(S, SwitchAccount.class);
            intent2.setFlags(67108864);
            if (a != null) {
                intent2.putParcelableArrayListExtra("bound_users", a);
            }
            S.startActivity(intent2);
            Y = 15;
        }
    }

    public static void openShareUI() {
        if (S == null) {
            throw new IllegalStateException("You should call init before open share ui");
        } else if (!e() || !TextUtils.isEmpty(getMyId())) {
            Intent intent = new Intent(S, Home.class);
            intent.setFlags(268435456);
            intent.putExtra("pending_action", 19);
            S.startActivity(intent);
        } else {
            x = true;
            Intent intent2 = new Intent(S, SwitchAccount.class);
            intent2.setFlags(67108864);
            if (a != null) {
                intent2.putParcelableArrayListExtra("bound_users", a);
            }
            S.startActivity(intent2);
            Y = 19;
        }
    }

    public static void openUnreadMessage(String str) {
        if (S == null) {
            throw new IllegalStateException("You should call init before open unread messages");
        } else if (!e() || !TextUtils.isEmpty(getMyId())) {
            Intent intent = new Intent(S, Home.class);
            intent.setFlags(268435456);
            intent.putExtra("pending_action", 18);
            intent.putExtra("lb_id", str);
            S.startActivity(intent);
        } else {
            x = true;
            Intent intent2 = new Intent(S, SwitchAccount.class);
            intent2.setFlags(67108864);
            if (a != null) {
                intent2.putParcelableArrayListExtra("bound_users", a);
            }
            S.startActivity(intent2);
            Y = 18;
            Z = str;
        }
    }

    public static void openUnreadMessages() {
        if (S == null) {
            throw new IllegalStateException("You should call init before open unread messages");
        } else if (!e() || !TextUtils.isEmpty(getMyId())) {
            Intent intent = new Intent(S, Home.class);
            intent.setFlags(268435456);
            intent.putExtra("pending_action", 16);
            S.startActivity(intent);
        } else {
            x = true;
            Intent intent2 = new Intent(S, SwitchAccount.class);
            intent2.setFlags(67108864);
            if (a != null) {
                intent2.putParcelableArrayListExtra("bound_users", a);
            }
            S.startActivity(intent2);
            Y = 16;
        }
    }

    public static void openUnreadNotices() {
        if (S == null) {
            throw new IllegalStateException("You should call init before open unread notices");
        } else if (!e() || !TextUtils.isEmpty(getMyId())) {
            Intent intent = new Intent(S, Home.class);
            intent.setFlags(268435456);
            intent.putExtra("pending_action", 17);
            S.startActivity(intent);
        } else {
            x = true;
            Intent intent2 = new Intent(S, SwitchAccount.class);
            intent2.setFlags(67108864);
            if (a != null) {
                intent2.putParcelableArrayListExtra("bound_users", a);
            }
            S.startActivity(intent2);
            Y = 17;
        }
    }

    public static void openWiYunPlaza() {
        if (S == null) {
            throw new IllegalStateException("You should call init before open wiyun plaza");
        } else if (!e() || !TextUtils.isEmpty(getMyId())) {
            Intent intent = new Intent(S, Home.class);
            intent.setFlags(268435456);
            intent.putExtra("pending_action", 20);
            intent.putExtra("tab", 0);
            S.startActivity(intent);
        } else {
            x = true;
            Intent intent2 = new Intent(S, SwitchAccount.class);
            intent2.setFlags(67108864);
            if (a != null) {
                intent2.putParcelableArrayListExtra("bound_users", a);
            }
            S.startActivity(intent2);
            Y = 20;
        }
    }

    static String p() {
        TelephonyManager telephonyManager;
        if (L == null && S != null && S.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0 && (telephonyManager = (TelephonyManager) S.getSystemService("phone")) != null) {
            L = telephonyManager.getNetworkOperatorName();
        }
        return L;
    }

    static String q() {
        byte[] b2;
        byte[] b3;
        if (r == null) {
            if (s()) {
                File file = new File(f, "wiyun_fdi");
                String b4 = (!file.exists() || (b2 = h.b(file)) == null || (b3 = h.b(null, b2, "wiyun_fdi")) == null) ? null : h.b(b3);
                if (TextUtils.isEmpty(b4)) {
                    r = "emu" + UUID.randomUUID().toString();
                    h.a(file, h.a((i) null, r, "wiyun_fdi"));
                } else {
                    r = b4;
                }
            } else {
                Context context = getContext();
                if (context == null) {
                    c("Context is not set");
                } else if (context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == -1) {
                    c("Cannot get a device ID.  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.READ_PHONE_STATE\" />");
                } else {
                    TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
                    if (telephonyManager != null) {
                        r = telephonyManager.getDeviceId();
                        if (!o(r)) {
                            r = r();
                        }
                    } else {
                        r = r();
                        Log.w("WiYun", "No device ID available.");
                    }
                }
            }
        }
        return r;
    }

    static String r() {
        WifiInfo connectionInfo;
        if (!TextUtils.isEmpty(s)) {
            return s;
        }
        Context context = getContext();
        StringBuilder sb = new StringBuilder();
        sb.append(ag());
        sb.append(Settings.Secure.getString(context.getContentResolver(), "android_id"));
        sb.append(context.getResources().getDisplayMetrics().density);
        WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
        if (!(wifiManager == null || (connectionInfo = wifiManager.getConnectionInfo()) == null)) {
            sb.append(connectionInfo.getMacAddress());
        }
        s = h.a(com.wiyun.game.e.a.b(sb.toString()));
        return s;
    }

    public static void removeWiGameClient(WiGameClient client) {
        if (V != null) {
            V.remove(client);
        }
    }

    static boolean s() {
        if (J == -1) {
            String ag2 = ag();
            boolean startsWith = Build.FINGERPRINT.startsWith("generic");
            boolean equalsIgnoreCase = "sdk".equalsIgnoreCase(Build.MODEL);
            boolean ah2 = ah();
            int i2 = (TextUtils.isEmpty(ag2) || !o(ag2)) ? 0 : 0 + 30;
            if (!startsWith) {
                i2 += 30;
            }
            if (!equalsIgnoreCase) {
                i2 += 20;
            }
            if (ah2) {
                i2 += 50;
            }
            J = i2 >= 50 ? 0 : 1;
        }
        return J != 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wiyun.game.h.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.wiyun.game.h.a(int, java.lang.String):int
      com.wiyun.game.h.a(java.lang.String, java.lang.String):java.lang.String
      com.wiyun.game.h.a(android.view.View, int):void
      com.wiyun.game.h.a(java.io.File, byte[]):boolean
      com.wiyun.game.h.a(android.content.Context, int):byte[]
      com.wiyun.game.h.a(java.lang.String, boolean):void */
    public static void saveGame(String str, String str2, byte[] bArr, byte[] bArr2, boolean z2) {
        if (S == null) {
            throw new IllegalStateException("You should call init before submit score");
        } else if (e()) {
            l = new a();
            l.c = bArr;
            l.d = bArr2;
            l.a = str;
            l.b = str2;
            l.e = z2;
            if (TextUtils.isEmpty(getMyId())) {
                Y = 5;
                Intent intent = new Intent(S, SwitchAccount.class);
                intent.setFlags(67108864);
                if (a != null) {
                    intent.putParcelableArrayListExtra("bound_users", a);
                }
                S.startActivity(intent);
                return;
            }
            Y = 5;
            z();
        } else {
            h.a(t.h("wy_label_no_network_to_save_game"), true);
        }
    }

    public static void sendChallenge(String definitionId, int score, byte[] blob) {
        sendChallenge(definitionId, score, blob, null);
    }

    public static void sendChallenge(String str, int i2, byte[] bArr, String str2) {
        if (S == null) {
            throw new IllegalStateException("You should call init before send challenge");
        }
        Intent intent = new Intent(S, SendChallenge.class);
        intent.setFlags(268435456);
        intent.putExtra("def_id", str);
        intent.putExtra("score", i2);
        intent.putExtra("blob", bArr);
        intent.putExtra("leaderboard_id", str2);
        if (TextUtils.isEmpty(getMyId())) {
            aa = intent;
            Y = 1;
            Intent intent2 = new Intent(S, SwitchAccount.class);
            intent2.setFlags(67108864);
            if (a != null) {
                intent2.putParcelableArrayListExtra("bound_users", a);
            }
            S.startActivity(intent2);
            return;
        }
        S.startActivity(intent);
    }

    public static void setChannel(String channel) {
        D = channel;
    }

    public static void setForbidSwitchAccount(boolean forbid) {
        ah = forbid;
    }

    public static void setHideNoticeToast(boolean hide) {
        ad = hide;
    }

    public static void setHideRecharge(boolean flag) {
        ag = flag;
    }

    public static void setHideScoreToast(boolean hide) {
        af = hide;
    }

    public static void setHideWelcomeBackToast(boolean hide) {
        ae = hide;
    }

    public static void setHintLogin(boolean hint) {
        C = hint;
    }

    public static void setSandboxMode(boolean sandbox) {
        u = sandbox;
        f.a(sandbox);
    }

    public static void setToastSide(int side) {
        v = side;
    }

    public static void showLoadGameDialog() {
        if (S == null) {
            throw new IllegalStateException("You should call init before start cloud");
        }
        Y = 8;
        if (!e() || !TextUtils.isEmpty(getMyId())) {
            z();
            return;
        }
        x = true;
        Intent intent = new Intent(S, SwitchAccount.class);
        intent.setFlags(67108864);
        if (a != null) {
            intent.putParcelableArrayListExtra("bound_users", a);
        }
        S.startActivity(intent);
    }

    public static void showPendingToast(Context context) {
        int i2;
        int i3;
        StringBuffer stringBuffer = new StringBuffer();
        if (I > 0) {
            stringBuffer.append(String.format(t.h("wy_label_my_friends_hint2"), Integer.valueOf(I)));
            i3 = 0 + 1;
            i2 = 2;
        } else {
            i2 = 0;
            i3 = 0;
        }
        if (F > 0) {
            int i4 = i3 + 1;
            if (stringBuffer.length() > 0) {
                stringBuffer.append("\n");
            }
            stringBuffer.append(String.format(t.h("wy_label_pending_challenge_hint"), Integer.valueOf(F)));
            i3 = i4;
            i2 = 1;
        }
        if (G > 0) {
            int i5 = i3 + 1;
            if (stringBuffer.length() > 0) {
                stringBuffer.append("\n");
            }
            stringBuffer.append(String.format(t.h("wy_label_my_message_hint2"), Integer.valueOf(G)));
            i3 = i5;
            i2 = 3;
        }
        if (H > 0) {
            int i6 = i3 + 1;
            if (stringBuffer.length() > 0) {
                stringBuffer.append("\n");
            }
            stringBuffer.append(String.format(t.h("wy_label_system_notice_hint2"), Integer.valueOf(H)));
            i3 = i6;
            i2 = 4;
        }
        if (i3 > 0) {
            if (i3 > 1) {
                i2 = 5;
            }
            X.sendMessage(Message.obtain(X, 10, i2, 0, stringBuffer.toString()));
        }
    }

    public static void showSwitchAccountDialog() {
        Intent intent = new Intent(S, SwitchAccount.class);
        intent.setFlags(67108864);
        if (a != null) {
            intent.putParcelableArrayListExtra("bound_users", a);
        }
        S.startActivity(intent);
    }

    public static void startUI() {
        if (S == null) {
            throw new IllegalStateException("You should call init before start cloud");
        } else if (!e() || !TextUtils.isEmpty(getMyId())) {
            Intent intent = new Intent(S, Home.class);
            intent.setFlags(268435456);
            S.startActivity(intent);
        } else {
            x = true;
            Intent intent2 = new Intent(S, SwitchAccount.class);
            intent2.setFlags(67108864);
            if (a != null) {
                intent2.putParcelableArrayListExtra("bound_users", a);
            }
            S.startActivity(intent2);
            Y = 4;
        }
    }

    public static void submitChallengeResult(String str, int i2, int i3, byte[] bArr) {
        if (S == null) {
            throw new IllegalStateException("You should call init before send challenge");
        } else if (str == null) {
            throw new IllegalArgumentException("ChallengeToUser id is empty");
        } else if ("test_ctu_id".equals(str)) {
            X.sendEmptyMessage(4);
        } else if (ab != null) {
            Log.w("WiYun", "another challenge result is in submitting, so quickly you finish another challenge?");
        } else if (TextUtils.isEmpty(getMyId())) {
            Log.w("WiYun", "There is no bound user found, where do you receive this challenge?");
        } else {
            ab = new ChallengeResult();
            ab.setUserId(getMyId());
            ab.setCtuId(str);
            ab.setScore(i3);
            ab.setResult(i2 > 0 ? 1 : i2 < 0 ? -1 : 0);
            ab.setBlob(bArr);
            ad();
        }
    }

    public static void submitScore(String leaderboardId, int score, byte[] blob) {
        submitScore(leaderboardId, score, blob, false);
    }

    public static void submitScore(String str, int i2, byte[] bArr, boolean z2) {
        if (S == null) {
            throw new IllegalStateException("You should call init before submit score");
        } else if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("leaderboard id is empty");
        } else {
            d dVar = new d();
            dVar.a = getMyId();
            dVar.b = str;
            dVar.c = i2;
            dVar.d = bArr;
            dVar.e = System.currentTimeMillis();
            dVar.f = getLatitude();
            dVar.g = getLongitude();
            dVar.i = z2;
            if (!e()) {
                if (!af) {
                    X.sendEmptyMessage(8);
                }
                i.a(dVar);
            } else if (TextUtils.isEmpty(getMyId())) {
                Y = 9;
                k.add(dVar);
                Intent intent = new Intent(S, SwitchAccount.class);
                intent.setFlags(67108864);
                if (a != null) {
                    intent.putParcelableArrayListExtra("bound_users", a);
                }
                S.startActivity(intent);
            } else if (z2) {
                k.add(dVar);
                dVar.j = f.a(dVar.b, dVar.c, dVar.d, dVar.f, dVar.g);
            } else {
                Intent intent2 = new Intent(S, SubmitScore.class);
                intent2.putExtra("pending_score", dVar);
                S.startActivity(intent2);
            }
        }
    }

    static String t() {
        if (!TextUtils.isEmpty(K)) {
            return K;
        }
        if (S == null) {
            return "";
        }
        Configuration configuration = S.getResources().getConfiguration();
        K = String.valueOf(configuration.mnc + (configuration.mcc * 100));
        return K;
    }

    public static void testPlayChallenge(ChallengeRequest challengeRequest) {
        if (S != null) {
            challengeRequest.setCtuId("test_ctu_id");
            X.sendMessage(Message.obtain(X, 1014, challengeRequest));
            S.sendBroadcast(new Intent("com.wiyun.game.RESET"));
        }
    }

    static ag u() {
        return R;
    }

    public static void unlockAchievement(String str) {
        if (S == null) {
            throw new IllegalStateException("You should call init before unlock achievement");
        } else if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("achievement id is empty");
        } else if (n(str) != null) {
            Log.w("WiYun", "same achievement is in unlocking, you want to unlock twice?");
        } else if (!i.a(str)) {
            b bVar = new b();
            bVar.a = getMyId();
            bVar.b = str;
            bVar.c = System.currentTimeMillis();
            com.wiyun.game.model.a.f a2 = x.a(str);
            if (a2 == null) {
                X.sendEmptyMessage(9);
            } else {
                X.sendMessage(Message.obtain(X, 9, a2.b()));
            }
            if (TextUtils.isEmpty(getMyId())) {
                i.a(bVar);
                return;
            }
            ac.add(bVar);
            f.e(bVar.b);
        }
    }

    static String v() {
        return t;
    }

    static boolean w() {
        return w;
    }

    public static void wyGameSaveProgress(String name, int uploadedSize) {
        if (V != null) {
            for (WiGameClient client : V) {
                client.wyGameSaveProgress(name, uploadedSize);
            }
        }
    }

    static Handler x() {
        return X;
    }

    static void y() {
        aa = null;
    }

    static void z() {
        switch (Y) {
            case 1:
                S.startActivity(aa);
                aa = null;
                break;
            case 2:
            case 3:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
                if (!Home.a) {
                    Intent intent = new Intent(S, Home.class);
                    intent.setFlags(268435456);
                    intent.putExtra("pending_action", Y);
                    switch (Y) {
                        case 2:
                            intent.putExtra("lb_id", Z);
                            break;
                        case 18:
                            intent.putExtra("lb_id", Z);
                            break;
                        case 20:
                            intent.putExtra("tab", 0);
                            break;
                    }
                    S.startActivity(intent);
                    Z = null;
                    break;
                }
                break;
            case 4:
                S.startActivity(new Intent(S, Home.class));
                break;
            case 5:
                if (l.e) {
                    Intent intent2 = new Intent(S, SaveGameDialog.class);
                    intent2.putExtra("blob", l.c);
                    intent2.putExtra("image", l.d);
                    intent2.putExtra("name", l.a);
                    intent2.putExtra("description", l.b);
                    intent2.putExtra("message", t.h("wy_label_saving_game_data"));
                    S.startActivity(intent2);
                } else {
                    f.a((String) null, l.a, l.b, l.c, l.d);
                }
                l = null;
                break;
            case 8:
                S.startActivity(new Intent(S, LoadGameDialog.class));
                break;
            case 9:
                if (k != null) {
                    for (d next : k) {
                        if (next.j == 0) {
                            if (next.i) {
                                next.j = f.a(next.b, next.c, next.d, next.f, next.g);
                            } else {
                                Intent intent3 = new Intent(S, SubmitScore.class);
                                intent3.putExtra("pending_score", next);
                                S.startActivity(intent3);
                            }
                        }
                    }
                    break;
                }
                break;
        }
        Y = 0;
    }
}
