package com.wiyun.game;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Toast;
import com.wiyun.game.b.a;
import com.wiyun.game.b.b;
import com.wiyun.game.b.d;
import com.wiyun.game.b.e;
import java.io.File;

public class Login extends Activity implements View.OnClickListener, a, b {
    /* access modifiers changed from: private */
    public View a;
    /* access modifiers changed from: private */
    public ViewGroup b;
    private Button c;
    private Button d;
    private EditText e;
    private View f;
    private ImageButton g;
    private ImageButton h;
    private ImageButton i;
    private ImageButton j;
    private ImageButton k;
    private View l;
    private boolean m;
    private String n;
    private boolean o;
    private boolean p;
    private int q;
    private boolean r;
    private Bitmap s;

    private void a() {
        Intent intent = getIntent();
        this.o = intent.getBooleanExtra("force", false);
        this.p = intent.getBooleanExtra("prompt_binding", false);
        this.q = 1;
        d.a().a((b) this);
        d.a().a((a) this);
    }

    private void a(int i2) {
        this.q = i2;
        ((FrameLayout.LayoutParams) this.l.getLayoutParams()).leftMargin = (int) TypedValue.applyDimension(1, (float) (((this.q - 1) * 50) + 40), getResources().getDisplayMetrics());
        this.l.requestLayout();
        if (i2 == 5) {
            try {
                startActivityForResult(h.a(256, 256, 1, 1, (Uri) null), 1);
            } catch (ActivityNotFoundException e2) {
                Toast.makeText(this, t.f("wy_toast_no_image_gallery"), 0).show();
            }
        }
    }

    private void b() {
        this.c = (Button) findViewById(t.d("wy_b_male"));
        this.d = (Button) findViewById(t.d("wy_b_female"));
        this.a = findViewById(t.d("wy_ll_progress_panel"));
        this.b = (ViewGroup) findViewById(t.d("wy_ll_main_panel"));
        this.e = (EditText) findViewById(t.d("wy_et_username"));
        this.f = findViewById(t.d("wy_iv_indicator"));
        this.l = findViewById(t.d("wy_iv_hook"));
        this.g = (ImageButton) findViewById(t.d("wy_ib_portrait_1"));
        this.h = (ImageButton) findViewById(t.d("wy_ib_portrait_2"));
        this.i = (ImageButton) findViewById(t.d("wy_ib_portrait_3"));
        this.j = (ImageButton) findViewById(t.d("wy_ib_portrait_4"));
        this.k = (ImageButton) findViewById(t.d("wy_ib_portrait_5"));
        this.g.setOnClickListener(this);
        this.h.setOnClickListener(this);
        this.i.setOnClickListener(this);
        this.j.setOnClickListener(this);
        this.k.setOnClickListener(this);
        this.c.setSelected(true);
        this.c.setOnClickListener(this);
        this.d = (Button) findViewById(t.d("wy_b_female"));
        this.d.setOnClickListener(this);
        Button btn = (Button) findViewById(t.d("wy_b_cancel"));
        btn.setOnClickListener(this);
        btn.setVisibility(this.o ? 8 : 0);
        ((Button) findViewById(t.d("wy_b_submit"))).setOnClickListener(this);
        ((Button) findViewById(t.d("wy_b_use_bound_account"))).setOnClickListener(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void a(e eVar) {
        switch (eVar.a) {
            case 14:
                if (this.m) {
                    if (!this.p || WiGame.u().d()) {
                        WiGame.z();
                    } else {
                        Intent intent = new Intent(this, AccountRetrieval.class);
                        intent.putExtra("process_pending", true);
                        startActivity(intent);
                    }
                    finish();
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void b(final e eVar) {
        switch (eVar.a) {
            case 1:
                if (eVar.c) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Login.this.a.setVisibility(4);
                            h.b(Login.this.b);
                            Toast.makeText(Login.this, (String) eVar.e, 0).show();
                        }
                    });
                    return;
                }
                this.n = (String) eVar.e;
                f.b(this.n);
                return;
            case 6:
                if (eVar.c) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Login.this.a.setVisibility(4);
                            h.b(Login.this.b);
                            Toast.makeText(Login.this, (String) eVar.e, 0).show();
                        }
                    });
                } else {
                    this.m = true;
                    if (this.q == 5) {
                        byte[] a2 = h.a(this.s);
                        this.s.recycle();
                        this.s = null;
                        f.a(a2);
                    } else {
                        f.i(h.c(h.a(this, t.c(String.format(this.c.isSelected() ? "wy_portrait_male_%d" : "wy_portrait_female_%d", Integer.valueOf(this.q))))));
                    }
                }
                eVar.d = false;
                return;
            default:
                return;
        }
    }

    public void onActivityResult(int i2, int i3, Intent intent) {
        switch (i2) {
            case 1:
                if (i3 == -1) {
                    Bundle extras = intent.getExtras();
                    if (extras != null) {
                        if (this.s != null) {
                            this.s.recycle();
                            this.s = null;
                        }
                        int applyDimension = (int) TypedValue.applyDimension(1, 36.0f, getResources().getDisplayMetrics());
                        this.s = (Bitmap) extras.getParcelable("data");
                        if (this.s == null) {
                            String string = extras.getString("filePath");
                            if (!TextUtils.isEmpty(string)) {
                                try {
                                    this.s = BitmapFactory.decodeFile(string);
                                    File file = new File(string);
                                    if (file != null) {
                                        file.delete();
                                    }
                                } catch (OutOfMemoryError e2) {
                                    File file2 = new File(string);
                                    if (file2 != null) {
                                        file2.delete();
                                    }
                                } catch (Throwable th) {
                                    File file3 = new File(string);
                                    if (file3 != null) {
                                        file3.delete();
                                    }
                                    throw th;
                                }
                            }
                        }
                        if (this.s != null) {
                            this.k.setImageBitmap(h.a(this.s, applyDimension, applyDimension));
                            return;
                        }
                        return;
                    }
                    return;
                } else if (this.s == null) {
                    a(1);
                    return;
                } else {
                    return;
                }
            default:
                super.onActivityResult(i2, i3, intent);
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View view) {
        int id = view.getId();
        if (id == t.d("wy_b_male")) {
            this.c.setSelected(true);
            this.d.setSelected(false);
            ((FrameLayout.LayoutParams) this.f.getLayoutParams()).leftMargin = (int) TypedValue.applyDimension(1, -20.0f, getResources().getDisplayMetrics());
            this.f.requestLayout();
            this.g.setImageResource(t.c("wy_portrait_male_1"));
            this.h.setImageResource(t.c("wy_portrait_male_2"));
            this.i.setImageResource(t.c("wy_portrait_male_3"));
            this.j.setImageResource(t.c("wy_portrait_male_4"));
        } else if (id == t.d("wy_b_female")) {
            this.c.setSelected(false);
            this.d.setSelected(true);
            ((FrameLayout.LayoutParams) this.f.getLayoutParams()).leftMargin = (int) TypedValue.applyDimension(1, 70.0f, getResources().getDisplayMetrics());
            this.f.requestLayout();
            this.g.setImageResource(t.c("wy_portrait_female_1"));
            this.h.setImageResource(t.c("wy_portrait_female_2"));
            this.i.setImageResource(t.c("wy_portrait_female_3"));
            this.j.setImageResource(t.c("wy_portrait_female_4"));
        } else if (id == t.d("wy_b_cancel")) {
            finish();
        } else if (id == t.d("wy_b_submit")) {
            String trim = this.e.getText().toString().trim();
            if (TextUtils.isEmpty(trim)) {
                Toast.makeText(this, t.f("wy_toast_nickname_cannot_be_empty"), 0).show();
                return;
            }
            this.a.setVisibility(0);
            h.a(this.b);
            f.a(trim, this.c.isSelected() ? "M" : "F");
        } else if (id == t.d("wy_b_use_bound_account")) {
            Intent intent = new Intent(this, UseAnotherAccount.class);
            intent.putExtra("from_login", true);
            intent.putExtra("force", this.o);
            intent.putExtra("prompt_binding", this.p);
            startActivity(intent);
            finish();
        } else if (id == t.d("wy_ib_portrait_1")) {
            a(1);
        } else if (id == t.d("wy_ib_portrait_2")) {
            a(2);
        } else if (id == t.d("wy_ib_portrait_3")) {
            a(3);
        } else if (id == t.d("wy_ib_portrait_4")) {
            a(4);
        } else if (id == t.d("wy_ib_portrait_5")) {
            a(5);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(WiGame.i);
        requestWindowFeature(1);
        setContentView(t.e("wy_activity_login"));
        a();
        b();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.s != null) {
            this.s.recycle();
            this.s = null;
        }
        d.a().b((b) this);
        d.a().b((a) this);
        super.onDestroy();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (!this.o || keyCode != 4 || event.getRepeatCount() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        this.r = true;
        return true;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (!this.o || keyCode != 4 || !this.r) {
            return super.onKeyUp(keyCode, event);
        }
        this.r = false;
        finish();
        if (WiGame.getContext() instanceof Activity) {
            ((Activity) WiGame.getContext()).finish();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (WiGame.b == null || WiGame.b.isEmpty()) {
            f.i();
        }
    }

    public boolean onSearchRequested() {
        return false;
    }
}
