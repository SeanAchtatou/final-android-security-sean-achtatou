package com.wiyun.game;

import android.os.Build;
import android.text.TextUtils;
import com.google.ads.AdActivity;
import com.wiyun.game.a.d;
import com.wiyun.game.b.e;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

class f {
    static final String a = ("wiyun.com".equals("wiyun.com") ? "80" : "8000");
    static String b = ("wiyun.com".equals("wiyun.com") ? "443" : "8000");
    static String c = (String.valueOf("wiyun.com".equals("wiyun.com") ? "api." : "") + "wiyun.com");
    static String d = (String.valueOf("wiyun.com".equals("wiyun.com") ? "game." : "") + "wiyun.com");
    private static String e = (String.valueOf("wiyun.com".equals("wiyun.com") ? "https://" : "http://") + c + ":" + b + (d.endsWith(".wiyun.com") ? "" : "/wiapi"));
    private static String f = ("http://" + c + ":" + a + (d.endsWith(".wiyun.com") ? "" : "/wiapi"));
    private static Map<Integer, a> g = new HashMap();
    private static Map<Integer, a> h = new HashMap();
    private static Map<Integer, a> i = new HashMap();
    private static HttpResponseInterceptor j = new j();

    static final class a {
        boolean a;
        String b;
        String c;
        int d;
        e e;
        String[] f;
        byte[][] g;

        a() {
        }
    }

    f() {
    }

    static long a(double d2, double d3, int i2, int i3) {
        long currentTimeMillis = System.currentTimeMillis();
        a(21, 200, "EV_GET_NEIGHBORS", "/user/range", "GET", "lat", String.valueOf(d2), "lon", String.valueOf(d3), "start", String.valueOf(i2), "count", String.valueOf(i3), "call_id", String.valueOf(currentTimeMillis));
        return currentTimeMillis;
    }

    static long a(String str, int i2) {
        long currentTimeMillis = System.currentTimeMillis();
        a(22, 200, "EV_GET_BEST_COMPETITOR", "/score/range", "GET", "leaderboard_id", str, "score", String.valueOf(i2), "call_id", String.valueOf(currentTimeMillis));
        return currentTimeMillis;
    }

    static long a(String str, int i2, int i3) {
        long currentTimeMillis = System.currentTimeMillis();
        a(23, 200, "EV_GET_FRIENDS", "/friends", "GET", "user_id", str, "start", String.valueOf(i2), "count", String.valueOf(i3), "call_id", String.valueOf(currentTimeMillis));
        return currentTimeMillis;
    }

    static long a(String str, int i2, byte[] bArr, double d2, double d3) {
        long currentTimeMillis = System.currentTimeMillis();
        String[] strArr = new String[10];
        strArr[0] = "leaderboard_id";
        strArr[1] = str;
        strArr[2] = "score";
        strArr[3] = String.valueOf(i2);
        strArr[4] = "lat";
        strArr[5] = d2 == 0.0d ? null : String.valueOf(d2);
        strArr[6] = "lon";
        strArr[7] = d3 == 0.0d ? null : String.valueOf(d3);
        strArr[8] = "call_id";
        strArr[9] = String.valueOf(currentTimeMillis);
        a(12, 401, "EV_SUBMIT_SCORE", "/score", "blob", bArr, strArr);
        return currentTimeMillis;
    }

    static long a(String str, String str2, String str3, double d2, double d3, int i2, int i3) {
        long currentTimeMillis = System.currentTimeMillis();
        a(10, 200, "EV_GET_SCORES_near", "/score", "GET", "app_id", str, "leaderboard_id", str2, "lat", String.valueOf(d2), "lon", String.valueOf(d3), "start", String.valueOf(i2), "count", String.valueOf(i3), "time_tab", str3, "call_id", String.valueOf(currentTimeMillis));
        return currentTimeMillis;
    }

    static long a(String str, String str2, String str3, byte[] bArr, byte[] bArr2) {
        long currentTimeMillis = System.currentTimeMillis();
        a(true, 76, 200, "EV_SAVE_GAME_SLOT", "/savecard", new String[]{"blob", "image"}, new byte[][]{bArr, bArr2}, "app_id", str, "name", str2, "description", str3, "call_id", String.valueOf(currentTimeMillis));
        return currentTimeMillis;
    }

    static long a(String str, boolean z) {
        long currentTimeMillis = System.currentTimeMillis();
        a(91, 200, "EV_GET_SNS_AUTH_URL", "/sns/auth", "GET", "type", str, "login", Boolean.toString(z), "call_id", String.valueOf(currentTimeMillis));
        return currentTimeMillis;
    }

    static String a(String str, String str2, String... strArr) throws UnsupportedEncodingException {
        StringBuilder sb = new StringBuilder(m(str));
        String country = Locale.getDefault().getCountry();
        if ("GET".equalsIgnoreCase(str2) || "DELETE".equalsIgnoreCase(str2)) {
            HashMap hashMap = new HashMap();
            int length = strArr.length;
            for (int i2 = 0; i2 < length; i2 += 2) {
                if (!TextUtils.isEmpty(strArr[i2 + 1])) {
                    hashMap.put(strArr[i2], strArr[i2 + 1]);
                }
            }
            hashMap.put("app_key", WiGame.m());
            hashMap.put("device_id", WiGame.q());
            hashMap.put("device_uuid", WiGame.r());
            if (!TextUtils.isEmpty(country)) {
                hashMap.put("country", country);
            }
            if (!TextUtils.isEmpty(WiGame.v())) {
                hashMap.put("session_key", WiGame.v());
            }
            a(hashMap, str2, str);
            boolean z = true;
            for (Map.Entry entry : hashMap.entrySet()) {
                if (z) {
                    a(sb, (String) entry.getKey(), (String) entry.getValue());
                    z = false;
                } else {
                    b(sb, (String) entry.getKey(), (String) entry.getValue());
                }
            }
        }
        return sb.toString();
    }

    private static String a(HttpEntity entity) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buf = new byte[2048];
            InputStream is = entity.getContent();
            for (int i2 = 0; i2 != -1; i2 = is.read(buf)) {
                baos.write(buf, 0, i2);
            }
            return h.b(baos.toByteArray());
        } catch (Exception e2) {
            return "";
        }
    }

    private static String a(String[] keyValues, String key) {
        if (keyValues == null) {
            return null;
        }
        for (int i2 = 0; i2 < keyValues.length - 1; i2 += 2) {
            if (keyValues[i2].equals(key)) {
                return keyValues[i2 + 1];
            }
        }
        return null;
    }

    private static HttpUriRequest a(int i2, String str, String str2, String... strArr) throws UnsupportedEncodingException {
        HttpPost httpPost = null;
        String country = Locale.getDefault().getCountry();
        String a2 = a(str, str2, strArr);
        if (!"GET".equalsIgnoreCase(str2) && !"DELETE".equalsIgnoreCase(str2)) {
            ArrayList arrayList = new ArrayList();
            int length = strArr.length;
            for (int i3 = 0; i3 < length; i3 += 2) {
                if (!TextUtils.isEmpty(strArr[i3 + 1])) {
                    arrayList.add(new BasicNameValuePair(strArr[i3], strArr[i3 + 1]));
                }
            }
            arrayList.add(new BasicNameValuePair("app_key", WiGame.m()));
            arrayList.add(new BasicNameValuePair("device_id", WiGame.q()));
            arrayList.add(new BasicNameValuePair("device_uuid", WiGame.r()));
            if (!TextUtils.isEmpty(country)) {
                arrayList.add(new BasicNameValuePair("country", country));
            }
            if (!TextUtils.isEmpty(WiGame.v())) {
                arrayList.add(new BasicNameValuePair("session_key", WiGame.v()));
            }
            if (i2 == 6) {
                arrayList.add(new BasicNameValuePair(AdActivity.ORIENTATION_PARAM, WiGame.s() ? "Android Emulator" : "Android"));
                arrayList.add(new BasicNameValuePair("v", Build.VERSION.RELEASE));
                arrayList.add(new BasicNameValuePair("b", Build.BRAND));
                arrayList.add(new BasicNameValuePair(AdActivity.TYPE_PARAM, Build.MODEL));
                arrayList.add(new BasicNameValuePair("c", WiGame.t()));
                arrayList.add(new BasicNameValuePair("cv", "3.0"));
            }
            b(arrayList, str2, str);
            if ("POST".equalsIgnoreCase(str2)) {
                httpPost = new HttpPost(a2);
                httpPost.setEntity(new UrlEncodedFormEntity(arrayList, "utf-8"));
            } else if ("PUT".equalsIgnoreCase(str2)) {
                httpPost = new HttpPut(a2);
                httpPost.setEntity(new UrlEncodedFormEntity(arrayList, "utf-8"));
            }
        } else if ("GET".equalsIgnoreCase(str2)) {
            httpPost = new HttpGet(a2);
        } else if ("DELETE".equalsIgnoreCase(str2)) {
            httpPost = new HttpDelete(a2);
        }
        a((HttpUriRequest) httpPost);
        return httpPost;
    }

    static DefaultHttpClient a() {
        HttpHost proxy;
        boolean wifi = d.b();
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        try {
            schemeRegistry.register(new Scheme("https", new com.wiyun.game.a.a(), 443));
        } catch (Exception e2) {
        }
        HttpParams params = new BasicHttpParams();
        HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(params, "UTF-8");
        HttpProtocolParams.setUseExpectContinue(params, false);
        HttpClientParams.setCookiePolicy(params, "compatibility");
        HttpConnectionParams.setConnectionTimeout(params, wifi ? 10000 : 30000);
        HttpConnectionParams.setSoTimeout(params, wifi ? 10000 : 30000);
        DefaultHttpClient client = new DefaultHttpClient(new ThreadSafeClientConnManager(params, schemeRegistry), params);
        client.addResponseInterceptor(j);
        if (!wifi && d.c() && (proxy = d.d()) != null) {
            client.getParams().setParameter("http.route.default-proxy", proxy);
        }
        return client;
    }

    static void a(double d2, double d3) {
        a(16, 200, "EV_UPDATE_MY_LOCATION", "/user/info", "POST", "lat", String.valueOf(d2), "lon", String.valueOf(d3));
    }

    static void a(int eventId) {
        synchronized (g) {
            g.remove(Integer.valueOf(eventId));
        }
    }

    static void a(int eventId, int statusCode, String testAsset, String path, String partName, byte[] partData, String... keyValues) {
        String[] strArr = {partName};
        a(false, eventId, statusCode, testAsset, path, strArr, new byte[][]{partData}, keyValues);
    }

    static void a(final int i2, int i3, String str, final String str2, final String str3, final String... strArr) {
        AnonymousClass4 r0 = new Thread() {
            public void run() {
                f.a(str2, str3, i2, strArr);
            }
        };
        r0.setPriority(1);
        r0.setName("WiGame HTTP thread");
        r0.start();
    }

    static void a(String str) {
        a(2, 200, "EV_UNBIND_ACCOUNT", "/device/bind", "DELETE", "user_id", str);
    }

    static void a(String str, int i2, int i3, byte[] bArr) {
        a(53, 200, "EV_SUBMIT_CHALLENGE_RESULT", "/challenge/result", "blob", bArr, "challenge_to_user_id", str, "score", String.valueOf(i3), "result", String.valueOf(i2));
    }

    static void a(String str, String str2) {
        a(1, 200, "EV_BIND_BY_USERNAME", "/device/bind", "POST", "username", str, "gender", str2, "unique_id", WiGame.o());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:101:0x01bc, code lost:
        r1.getConnectionManager().shutdown();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x01c5, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x01c6, code lost:
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x01c9, code lost:
        r1.getConnectionManager().shutdown();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x01e8, code lost:
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x01eb, code lost:
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x01ee, code lost:
        r0 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:126:0x01f1, code lost:
        r0 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00d6, code lost:
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0134, code lost:
        r0.getConnectionManager().shutdown();
        r0 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x0185, code lost:
        r0.getConnectionManager().shutdown();
        r0 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x0194, code lost:
        r0 = null;
        r1 = null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:101:0x01bc  */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x01c5 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x0007] */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x01c9  */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x01e7 A[ExcHandler: UnknownHostException (e java.net.UnknownHostException), Splitter:B:1:0x0007] */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x01ea A[ExcHandler: SocketTimeoutException (e java.net.SocketTimeoutException), Splitter:B:1:0x0007] */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x01ee  */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x01f1  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x008f A[SYNTHETIC, Splitter:B:41:0x008f] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00d5 A[ExcHandler: SocketException (e java.net.SocketException), Splitter:B:1:0x0007] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0134  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x0185  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void a(java.lang.String r10, java.lang.String r11, int r12, java.lang.String r13, java.lang.String... r14) {
        /*
            r7 = 300(0x12c, float:4.2E-43)
            r3 = 0
            r6 = 0
            r2 = 1
            java.lang.String r0 = "GET"
            boolean r0 = r0.equalsIgnoreCase(r11)     // Catch:{ SocketException -> 0x00d5, SocketTimeoutException -> 0x01ea, UnknownHostException -> 0x01e7, Exception -> 0x0193, all -> 0x01c5 }
            if (r0 == 0) goto L_0x00cf
            java.lang.String r0 = com.wiyun.game.a.f.a(r10, r14)     // Catch:{ SocketException -> 0x00d5, SocketTimeoutException -> 0x01ea, UnknownHostException -> 0x01e7, Exception -> 0x0193, all -> 0x01c5 }
        L_0x0011:
            boolean r1 = android.text.TextUtils.isEmpty(r0)     // Catch:{ SocketException -> 0x00d5, SocketTimeoutException -> 0x01ea, UnknownHostException -> 0x01e7, Exception -> 0x01e1, all -> 0x01c5 }
            if (r1 == 0) goto L_0x00f7
            r1 = r6
        L_0x0018:
            if (r1 != 0) goto L_0x01f7
            org.apache.http.impl.client.DefaultHttpClient r2 = a()     // Catch:{ SocketException -> 0x00d5, SocketTimeoutException -> 0x01ea, UnknownHostException -> 0x01e7, Exception -> 0x01e1, all -> 0x01c5 }
        L_0x001e:
            if (r1 == 0) goto L_0x00fa
        L_0x0020:
            if (r1 == 0) goto L_0x0105
            r4 = 200(0xc8, float:2.8E-43)
        L_0x0024:
            if (r1 != 0) goto L_0x002e
            org.apache.http.HttpEntity r5 = r3.getEntity()     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
            java.lang.String r0 = a(r5)     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
        L_0x002e:
            if (r1 != 0) goto L_0x0051
            if (r4 >= r7) goto L_0x0051
            java.lang.String r1 = "GET"
            boolean r1 = r1.equalsIgnoreCase(r11)     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
            if (r1 == 0) goto L_0x0051
            java.lang.String r1 = "Cache-Control"
            org.apache.http.Header r1 = r3.getFirstHeader(r1)     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
            if (r1 == 0) goto L_0x010f
            java.lang.String r5 = "no-cache"
            java.lang.String r1 = r1.getValue()     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
            boolean r1 = r5.equalsIgnoreCase(r1)     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
            if (r1 != 0) goto L_0x0051
            com.wiyun.game.a.f.a(r0, r10, r14)     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
        L_0x0051:
            com.wiyun.game.b.e r1 = new com.wiyun.game.b.e     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
            r1.<init>()     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
            r1.b = r4     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
            r1.a = r12     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
            if (r4 < r7) goto L_0x0117
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
            r4.<init>(r0)     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
            java.lang.String r5 = "msg"
            java.lang.String r4 = r4.optString(r5)     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
            r1.e = r4     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
        L_0x0069:
            r1.f = r13     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
            r1.i = r14     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
            if (r3 == 0) goto L_0x0079
            org.apache.http.Header[] r3 = r3.getAllHeaders()     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
            if (r3 == 0) goto L_0x0079
            int r4 = r3.length     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
            r5 = r6
        L_0x0077:
            if (r5 < r4) goto L_0x013e
        L_0x0079:
            if (r2 == 0) goto L_0x0082
            org.apache.http.conn.ClientConnectionManager r0 = r2.getConnectionManager()
            r0.shutdown()
        L_0x0082:
            r0 = r1
        L_0x0083:
            java.lang.String r1 = "call_id"
            java.lang.String r1 = r0.a(r1)
            boolean r2 = android.text.TextUtils.isEmpty(r1)
            if (r2 != 0) goto L_0x0095
            long r1 = java.lang.Long.parseLong(r1)     // Catch:{ NumberFormatException -> 0x01d4 }
            r0.j = r1     // Catch:{ NumberFormatException -> 0x01d4 }
        L_0x0095:
            boolean r1 = r0.c
            if (r1 == 0) goto L_0x00c7
            com.wiyun.game.f$a r1 = new com.wiyun.game.f$a
            r1.<init>()
            r1.b = r10
            r1.c = r11
            int r2 = r0.b
            r1.d = r2
            r1.e = r0
            java.util.Map<java.lang.Integer, com.wiyun.game.f$a> r2 = com.wiyun.game.f.g
            monitor-enter(r2)
            java.util.Map<java.lang.Integer, com.wiyun.game.f$a> r3 = com.wiyun.game.f.g     // Catch:{ all -> 0x01d1 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r12)     // Catch:{ all -> 0x01d1 }
            r3.put(r4, r1)     // Catch:{ all -> 0x01d1 }
            monitor-exit(r2)     // Catch:{ all -> 0x01d1 }
            java.lang.Object r1 = r0.e
            if (r1 == 0) goto L_0x00bf
            int r1 = r0.b
            r2 = 500(0x1f4, float:7.0E-43)
            if (r1 != r2) goto L_0x00c7
        L_0x00bf:
            java.lang.String r1 = "wy_toast_server_error"
            java.lang.String r1 = com.wiyun.game.t.h(r1)
            r0.e = r1
        L_0x00c7:
            com.wiyun.game.b.d r1 = com.wiyun.game.b.d.a()
            r1.a(r0)
            return
        L_0x00cf:
            r0 = r3
            com.wiyun.game.a.f.b()     // Catch:{ SocketException -> 0x00d5, SocketTimeoutException -> 0x01ea, UnknownHostException -> 0x01e7, Exception -> 0x0193, all -> 0x01c5 }
            goto L_0x0011
        L_0x00d5:
            r0 = move-exception
            r0 = r3
        L_0x00d7:
            java.lang.String r1 = "wy_label_connection_problem"
            com.wiyun.game.t.h(r1)     // Catch:{ all -> 0x01da }
            com.wiyun.game.b.e r1 = new com.wiyun.game.b.e     // Catch:{ all -> 0x01da }
            r1.<init>()     // Catch:{ all -> 0x01da }
            r1.a = r12     // Catch:{ all -> 0x01da }
            r2 = 501(0x1f5, float:7.02E-43)
            r1.b = r2     // Catch:{ all -> 0x01da }
            r2 = 1
            r1.c = r2     // Catch:{ all -> 0x01da }
            r1.i = r14     // Catch:{ all -> 0x01da }
            if (r0 == 0) goto L_0x01f4
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()
            r0.shutdown()
            r0 = r1
            goto L_0x0083
        L_0x00f7:
            r1 = r2
            goto L_0x0018
        L_0x00fa:
            r3 = -1
            org.apache.http.client.methods.HttpUriRequest r3 = a(r3, r10, r11, r14)     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
            org.apache.http.HttpResponse r3 = r2.execute(r3)     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
            goto L_0x0020
        L_0x0105:
            org.apache.http.StatusLine r4 = r3.getStatusLine()     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
            int r4 = r4.getStatusCode()     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
            goto L_0x0024
        L_0x010f:
            com.wiyun.game.a.f.a(r0, r10, r14)     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
            goto L_0x0051
        L_0x0114:
            r0 = move-exception
            r0 = r2
            goto L_0x00d7
        L_0x0117:
            r1.e = r0     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
            goto L_0x0069
        L_0x011b:
            r0 = move-exception
            r0 = r2
        L_0x011d:
            java.lang.String r1 = "wy_label_connection_problem"
            com.wiyun.game.t.h(r1)     // Catch:{ all -> 0x01da }
            com.wiyun.game.b.e r1 = new com.wiyun.game.b.e     // Catch:{ all -> 0x01da }
            r1.<init>()     // Catch:{ all -> 0x01da }
            r1.a = r12     // Catch:{ all -> 0x01da }
            r2 = 501(0x1f5, float:7.02E-43)
            r1.b = r2     // Catch:{ all -> 0x01da }
            r2 = 1
            r1.c = r2     // Catch:{ all -> 0x01da }
            r1.i = r14     // Catch:{ all -> 0x01da }
            if (r0 == 0) goto L_0x01f1
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()
            r0.shutdown()
            r0 = r1
            goto L_0x0083
        L_0x013e:
            r6 = r3[r5]     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
            java.lang.String r7 = "Set-Cookie"
            java.lang.String r8 = r6.getName()     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
            boolean r7 = r7.equalsIgnoreCase(r8)     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
            if (r7 == 0) goto L_0x018f
            android.webkit.CookieManager r3 = android.webkit.CookieManager.getInstance()     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
            java.lang.String r4 = "http://wiyun.com"
            java.lang.String r5 = "%s;domain=%s"
            r7 = 2
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
            r8 = 0
            java.lang.String r6 = r6.getValue()     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
            r7[r8] = r6     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
            r6 = 1
            java.lang.String r8 = "wiyun.com"
            r7[r6] = r8     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
            java.lang.String r5 = java.lang.String.format(r5, r7)     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
            r3.setCookie(r4, r5)     // Catch:{ SocketException -> 0x0114, SocketTimeoutException -> 0x011b, UnknownHostException -> 0x016c, Exception -> 0x01e4, all -> 0x01d7 }
            goto L_0x0079
        L_0x016c:
            r0 = move-exception
            r0 = r2
        L_0x016e:
            java.lang.String r1 = "wy_label_connection_problem"
            com.wiyun.game.t.h(r1)     // Catch:{ all -> 0x01da }
            com.wiyun.game.b.e r1 = new com.wiyun.game.b.e     // Catch:{ all -> 0x01da }
            r1.<init>()     // Catch:{ all -> 0x01da }
            r1.a = r12     // Catch:{ all -> 0x01da }
            r2 = 501(0x1f5, float:7.02E-43)
            r1.b = r2     // Catch:{ all -> 0x01da }
            r2 = 1
            r1.c = r2     // Catch:{ all -> 0x01da }
            r1.i = r14     // Catch:{ all -> 0x01da }
            if (r0 == 0) goto L_0x01ee
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()
            r0.shutdown()
            r0 = r1
            goto L_0x0083
        L_0x018f:
            int r5 = r5 + 1
            goto L_0x0077
        L_0x0193:
            r0 = move-exception
            r0 = r3
            r1 = r3
        L_0x0196:
            java.lang.String r2 = "WiYun"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x01df }
            java.lang.String r4 = "failed opearation body: "
            r3.<init>(r4)     // Catch:{ all -> 0x01df }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x01df }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x01df }
            android.util.Log.w(r2, r0)     // Catch:{ all -> 0x01df }
            com.wiyun.game.b.e r0 = new com.wiyun.game.b.e     // Catch:{ all -> 0x01df }
            r0.<init>()     // Catch:{ all -> 0x01df }
            r0.a = r12     // Catch:{ all -> 0x01df }
            r2 = 500(0x1f4, float:7.0E-43)
            r0.b = r2     // Catch:{ all -> 0x01df }
            r2 = 1
            r0.c = r2     // Catch:{ all -> 0x01df }
            r0.i = r14     // Catch:{ all -> 0x01df }
            if (r1 == 0) goto L_0x0083
            org.apache.http.conn.ClientConnectionManager r1 = r1.getConnectionManager()
            r1.shutdown()
            goto L_0x0083
        L_0x01c5:
            r0 = move-exception
            r1 = r3
        L_0x01c7:
            if (r1 == 0) goto L_0x01d0
            org.apache.http.conn.ClientConnectionManager r1 = r1.getConnectionManager()
            r1.shutdown()
        L_0x01d0:
            throw r0
        L_0x01d1:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x01d1 }
            throw r0
        L_0x01d4:
            r1 = move-exception
            goto L_0x0095
        L_0x01d7:
            r0 = move-exception
            r1 = r2
            goto L_0x01c7
        L_0x01da:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x01c7
        L_0x01df:
            r0 = move-exception
            goto L_0x01c7
        L_0x01e1:
            r1 = move-exception
            r1 = r3
            goto L_0x0196
        L_0x01e4:
            r1 = move-exception
            r1 = r2
            goto L_0x0196
        L_0x01e7:
            r0 = move-exception
            r0 = r3
            goto L_0x016e
        L_0x01ea:
            r0 = move-exception
            r0 = r3
            goto L_0x011d
        L_0x01ee:
            r0 = r1
            goto L_0x0083
        L_0x01f1:
            r0 = r1
            goto L_0x0083
        L_0x01f4:
            r0 = r1
            goto L_0x0083
        L_0x01f7:
            r2 = r3
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wiyun.game.f.a(java.lang.String, java.lang.String, int, java.lang.String, java.lang.String[]):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:115:0x01d1, code lost:
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00bc, code lost:
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x0154, code lost:
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x0178, code lost:
        r0 = null;
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x01ab, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x01ac, code lost:
        r1 = null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x01d0 A[ExcHandler: SocketTimeoutException (e java.net.SocketTimeoutException), Splitter:B:1:0x0005] */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x01d4  */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x01d8  */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x01db  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0087 A[SYNTHETIC, Splitter:B:40:0x0087] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00bb A[ExcHandler: SocketException (e java.net.SocketException), Splitter:B:1:0x0005] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0145  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0153 A[ExcHandler: UnknownHostException (e java.net.UnknownHostException), Splitter:B:1:0x0005] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x016d  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x01a0  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x01ab A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x0005] */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x01af  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void a(java.lang.String r10, java.lang.String r11, int r12, java.lang.String... r13) {
        /*
            r3 = 0
            r6 = 0
            r2 = 1
            java.lang.String r0 = "GET"
            boolean r0 = r0.equalsIgnoreCase(r11)     // Catch:{ SocketException -> 0x00bb, SocketTimeoutException -> 0x01d0, UnknownHostException -> 0x0153, Exception -> 0x0177, all -> 0x01ab }
            if (r0 == 0) goto L_0x00b5
            java.lang.String r0 = com.wiyun.game.a.f.a(r10, r13)     // Catch:{ SocketException -> 0x00bb, SocketTimeoutException -> 0x01d0, UnknownHostException -> 0x0153, Exception -> 0x0177, all -> 0x01ab }
        L_0x000f:
            boolean r1 = android.text.TextUtils.isEmpty(r0)     // Catch:{ SocketException -> 0x00bb, SocketTimeoutException -> 0x01d0, UnknownHostException -> 0x0153, Exception -> 0x01c7, all -> 0x01ab }
            if (r1 == 0) goto L_0x00de
            r1 = r6
        L_0x0016:
            if (r1 != 0) goto L_0x01e1
            org.apache.http.impl.client.DefaultHttpClient r2 = a()     // Catch:{ SocketException -> 0x00bb, SocketTimeoutException -> 0x01d0, UnknownHostException -> 0x0153, Exception -> 0x01c7, all -> 0x01ab }
        L_0x001c:
            if (r1 == 0) goto L_0x00e1
        L_0x001e:
            if (r1 == 0) goto L_0x00eb
            r4 = 200(0xc8, float:2.8E-43)
        L_0x0022:
            if (r1 != 0) goto L_0x002c
            org.apache.http.HttpEntity r5 = r3.getEntity()     // Catch:{ SocketException -> 0x00fa, SocketTimeoutException -> 0x012b, UnknownHostException -> 0x01cd, Exception -> 0x01ca, all -> 0x01bd }
            java.lang.String r0 = a(r5)     // Catch:{ SocketException -> 0x00fa, SocketTimeoutException -> 0x012b, UnknownHostException -> 0x01cd, Exception -> 0x01ca, all -> 0x01bd }
        L_0x002c:
            if (r1 != 0) goto L_0x0051
            r1 = 300(0x12c, float:4.2E-43)
            if (r4 >= r1) goto L_0x0051
            java.lang.String r1 = "GET"
            boolean r1 = r1.equalsIgnoreCase(r11)     // Catch:{ SocketException -> 0x00fa, SocketTimeoutException -> 0x012b, UnknownHostException -> 0x01cd, Exception -> 0x01ca, all -> 0x01bd }
            if (r1 == 0) goto L_0x0051
            java.lang.String r1 = "Cache-Control"
            org.apache.http.Header r1 = r3.getFirstHeader(r1)     // Catch:{ SocketException -> 0x00fa, SocketTimeoutException -> 0x012b, UnknownHostException -> 0x01cd, Exception -> 0x01ca, all -> 0x01bd }
            if (r1 == 0) goto L_0x00f5
            java.lang.String r5 = "no-cache"
            java.lang.String r1 = r1.getValue()     // Catch:{ SocketException -> 0x00fa, SocketTimeoutException -> 0x012b, UnknownHostException -> 0x01cd, Exception -> 0x01ca, all -> 0x01bd }
            boolean r1 = r5.equalsIgnoreCase(r1)     // Catch:{ SocketException -> 0x00fa, SocketTimeoutException -> 0x012b, UnknownHostException -> 0x01cd, Exception -> 0x01ca, all -> 0x01bd }
            if (r1 != 0) goto L_0x0051
            com.wiyun.game.a.f.a(r0, r10, r13)     // Catch:{ SocketException -> 0x00fa, SocketTimeoutException -> 0x012b, UnknownHostException -> 0x01cd, Exception -> 0x01ca, all -> 0x01bd }
        L_0x0051:
            com.wiyun.game.b.e r1 = new com.wiyun.game.b.e     // Catch:{ SocketException -> 0x00fa, SocketTimeoutException -> 0x012b, UnknownHostException -> 0x01cd, Exception -> 0x01ca, all -> 0x01bd }
            r1.<init>()     // Catch:{ SocketException -> 0x00fa, SocketTimeoutException -> 0x012b, UnknownHostException -> 0x01cd, Exception -> 0x01ca, all -> 0x01bd }
            r1.b = r4     // Catch:{ SocketException -> 0x00fa, SocketTimeoutException -> 0x012b, UnknownHostException -> 0x01cd, Exception -> 0x01ca, all -> 0x01bd }
            r1.a = r12     // Catch:{ SocketException -> 0x00fa, SocketTimeoutException -> 0x012b, UnknownHostException -> 0x01cd, Exception -> 0x01ca, all -> 0x01bd }
            r1.i = r13     // Catch:{ SocketException -> 0x00fa, SocketTimeoutException -> 0x012b, UnknownHostException -> 0x01cd, Exception -> 0x01ca, all -> 0x01bd }
            r4 = 91
            if (r12 != r4) goto L_0x006c
            if (r3 == 0) goto L_0x006c
            org.apache.http.Header[] r3 = r3.getAllHeaders()     // Catch:{ SocketException -> 0x00fa, SocketTimeoutException -> 0x012b, UnknownHostException -> 0x01cd, Exception -> 0x01ca, all -> 0x01bd }
            if (r3 == 0) goto L_0x006c
            int r4 = r3.length     // Catch:{ SocketException -> 0x00fa, SocketTimeoutException -> 0x012b, UnknownHostException -> 0x01cd, Exception -> 0x01ca, all -> 0x01bd }
            r5 = r6
        L_0x006a:
            if (r5 < r4) goto L_0x00fd
        L_0x006c:
            if (r2 == 0) goto L_0x0075
            org.apache.http.conn.ClientConnectionManager r2 = r2.getConnectionManager()
            r2.shutdown()
        L_0x0075:
            r9 = r1
            r1 = r0
            r0 = r9
        L_0x0078:
            com.wiyun.game.b.c.a(r1, r0)
            java.lang.String r1 = "call_id"
            java.lang.String r1 = r0.a(r1)
            boolean r2 = android.text.TextUtils.isEmpty(r1)
            if (r2 != 0) goto L_0x008d
            long r1 = java.lang.Long.parseLong(r1)     // Catch:{ NumberFormatException -> 0x01ba }
            r0.j = r1     // Catch:{ NumberFormatException -> 0x01ba }
        L_0x008d:
            boolean r1 = r0.c
            if (r1 == 0) goto L_0x00ad
            com.wiyun.game.f$a r1 = new com.wiyun.game.f$a
            r1.<init>()
            r1.b = r10
            r1.c = r11
            int r2 = r0.b
            r1.d = r2
            r1.e = r0
            java.util.Map<java.lang.Integer, com.wiyun.game.f$a> r2 = com.wiyun.game.f.g
            monitor-enter(r2)
            java.util.Map<java.lang.Integer, com.wiyun.game.f$a> r3 = com.wiyun.game.f.g     // Catch:{ all -> 0x01b7 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r12)     // Catch:{ all -> 0x01b7 }
            r3.put(r4, r1)     // Catch:{ all -> 0x01b7 }
            monitor-exit(r2)     // Catch:{ all -> 0x01b7 }
        L_0x00ad:
            com.wiyun.game.b.d r1 = com.wiyun.game.b.d.a()
            r1.a(r0)
            return
        L_0x00b5:
            r0 = r3
            com.wiyun.game.a.f.b()     // Catch:{ SocketException -> 0x00bb, SocketTimeoutException -> 0x01d0, UnknownHostException -> 0x0153, Exception -> 0x0177, all -> 0x01ab }
            goto L_0x000f
        L_0x00bb:
            r0 = move-exception
            r0 = r3
        L_0x00bd:
            java.lang.String r1 = "wy_label_connection_problem"
            java.lang.String r1 = com.wiyun.game.t.h(r1)     // Catch:{ all -> 0x01c0 }
            com.wiyun.game.b.e r2 = new com.wiyun.game.b.e     // Catch:{ all -> 0x01c0 }
            r2.<init>()     // Catch:{ all -> 0x01c0 }
            r2.a = r12     // Catch:{ all -> 0x01c0 }
            r3 = 501(0x1f5, float:7.02E-43)
            r2.b = r3     // Catch:{ all -> 0x01c0 }
            r3 = 1
            r2.c = r3     // Catch:{ all -> 0x01c0 }
            r2.i = r13     // Catch:{ all -> 0x01c0 }
            if (r0 == 0) goto L_0x01de
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()
            r0.shutdown()
            r0 = r2
            goto L_0x0078
        L_0x00de:
            r1 = r2
            goto L_0x0016
        L_0x00e1:
            org.apache.http.client.methods.HttpUriRequest r3 = a(r12, r10, r11, r13)     // Catch:{ SocketException -> 0x00fa, SocketTimeoutException -> 0x012b, UnknownHostException -> 0x01cd, Exception -> 0x01ca, all -> 0x01bd }
            org.apache.http.HttpResponse r3 = r2.execute(r3)     // Catch:{ SocketException -> 0x00fa, SocketTimeoutException -> 0x012b, UnknownHostException -> 0x01cd, Exception -> 0x01ca, all -> 0x01bd }
            goto L_0x001e
        L_0x00eb:
            org.apache.http.StatusLine r4 = r3.getStatusLine()     // Catch:{ SocketException -> 0x00fa, SocketTimeoutException -> 0x012b, UnknownHostException -> 0x01cd, Exception -> 0x01ca, all -> 0x01bd }
            int r4 = r4.getStatusCode()     // Catch:{ SocketException -> 0x00fa, SocketTimeoutException -> 0x012b, UnknownHostException -> 0x01cd, Exception -> 0x01ca, all -> 0x01bd }
            goto L_0x0022
        L_0x00f5:
            com.wiyun.game.a.f.a(r0, r10, r13)     // Catch:{ SocketException -> 0x00fa, SocketTimeoutException -> 0x012b, UnknownHostException -> 0x01cd, Exception -> 0x01ca, all -> 0x01bd }
            goto L_0x0051
        L_0x00fa:
            r0 = move-exception
            r0 = r2
            goto L_0x00bd
        L_0x00fd:
            r6 = r3[r5]     // Catch:{ SocketException -> 0x00fa, SocketTimeoutException -> 0x012b, UnknownHostException -> 0x01cd, Exception -> 0x01ca, all -> 0x01bd }
            java.lang.String r7 = "Set-Cookie"
            java.lang.String r8 = r6.getName()     // Catch:{ SocketException -> 0x00fa, SocketTimeoutException -> 0x012b, UnknownHostException -> 0x01cd, Exception -> 0x01ca, all -> 0x01bd }
            boolean r7 = r7.equalsIgnoreCase(r8)     // Catch:{ SocketException -> 0x00fa, SocketTimeoutException -> 0x012b, UnknownHostException -> 0x01cd, Exception -> 0x01ca, all -> 0x01bd }
            if (r7 == 0) goto L_0x014f
            android.webkit.CookieManager r3 = android.webkit.CookieManager.getInstance()     // Catch:{ SocketException -> 0x00fa, SocketTimeoutException -> 0x012b, UnknownHostException -> 0x01cd, Exception -> 0x01ca, all -> 0x01bd }
            java.lang.String r4 = "http://wiyun.com"
            java.lang.String r5 = "%s;domain=%s"
            r7 = 2
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ SocketException -> 0x00fa, SocketTimeoutException -> 0x012b, UnknownHostException -> 0x01cd, Exception -> 0x01ca, all -> 0x01bd }
            r8 = 0
            java.lang.String r6 = r6.getValue()     // Catch:{ SocketException -> 0x00fa, SocketTimeoutException -> 0x012b, UnknownHostException -> 0x01cd, Exception -> 0x01ca, all -> 0x01bd }
            r7[r8] = r6     // Catch:{ SocketException -> 0x00fa, SocketTimeoutException -> 0x012b, UnknownHostException -> 0x01cd, Exception -> 0x01ca, all -> 0x01bd }
            r6 = 1
            java.lang.String r8 = "wiyun.com"
            r7[r6] = r8     // Catch:{ SocketException -> 0x00fa, SocketTimeoutException -> 0x012b, UnknownHostException -> 0x01cd, Exception -> 0x01ca, all -> 0x01bd }
            java.lang.String r5 = java.lang.String.format(r5, r7)     // Catch:{ SocketException -> 0x00fa, SocketTimeoutException -> 0x012b, UnknownHostException -> 0x01cd, Exception -> 0x01ca, all -> 0x01bd }
            r3.setCookie(r4, r5)     // Catch:{ SocketException -> 0x00fa, SocketTimeoutException -> 0x012b, UnknownHostException -> 0x01cd, Exception -> 0x01ca, all -> 0x01bd }
            goto L_0x006c
        L_0x012b:
            r0 = move-exception
            r0 = r2
        L_0x012d:
            java.lang.String r1 = "wy_label_connection_problem"
            java.lang.String r1 = com.wiyun.game.t.h(r1)     // Catch:{ all -> 0x01c0 }
            com.wiyun.game.b.e r2 = new com.wiyun.game.b.e     // Catch:{ all -> 0x01c0 }
            r2.<init>()     // Catch:{ all -> 0x01c0 }
            r2.a = r12     // Catch:{ all -> 0x01c0 }
            r3 = 501(0x1f5, float:7.02E-43)
            r2.b = r3     // Catch:{ all -> 0x01c0 }
            r3 = 1
            r2.c = r3     // Catch:{ all -> 0x01c0 }
            r2.i = r13     // Catch:{ all -> 0x01c0 }
            if (r0 == 0) goto L_0x01db
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()
            r0.shutdown()
            r0 = r2
            goto L_0x0078
        L_0x014f:
            int r5 = r5 + 1
            goto L_0x006a
        L_0x0153:
            r0 = move-exception
            r0 = r3
        L_0x0155:
            java.lang.String r1 = "wy_label_connection_problem"
            java.lang.String r1 = com.wiyun.game.t.h(r1)     // Catch:{ all -> 0x01c0 }
            com.wiyun.game.b.e r2 = new com.wiyun.game.b.e     // Catch:{ all -> 0x01c0 }
            r2.<init>()     // Catch:{ all -> 0x01c0 }
            r2.a = r12     // Catch:{ all -> 0x01c0 }
            r3 = 501(0x1f5, float:7.02E-43)
            r2.b = r3     // Catch:{ all -> 0x01c0 }
            r3 = 1
            r2.c = r3     // Catch:{ all -> 0x01c0 }
            r2.i = r13     // Catch:{ all -> 0x01c0 }
            if (r0 == 0) goto L_0x01d8
            org.apache.http.conn.ClientConnectionManager r0 = r0.getConnectionManager()
            r0.shutdown()
            r0 = r2
            goto L_0x0078
        L_0x0177:
            r0 = move-exception
            r0 = r3
            r1 = r3
        L_0x017a:
            java.lang.String r2 = "WiYun"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x01c5 }
            java.lang.String r4 = "failed opearation body: "
            r3.<init>(r4)     // Catch:{ all -> 0x01c5 }
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ all -> 0x01c5 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x01c5 }
            android.util.Log.w(r2, r3)     // Catch:{ all -> 0x01c5 }
            com.wiyun.game.b.e r2 = new com.wiyun.game.b.e     // Catch:{ all -> 0x01c5 }
            r2.<init>()     // Catch:{ all -> 0x01c5 }
            r2.a = r12     // Catch:{ all -> 0x01c5 }
            r3 = 500(0x1f4, float:7.0E-43)
            r2.b = r3     // Catch:{ all -> 0x01c5 }
            r3 = 1
            r2.c = r3     // Catch:{ all -> 0x01c5 }
            r2.i = r13     // Catch:{ all -> 0x01c5 }
            if (r1 == 0) goto L_0x01d4
            org.apache.http.conn.ClientConnectionManager r1 = r1.getConnectionManager()
            r1.shutdown()
            r1 = r0
            r0 = r2
            goto L_0x0078
        L_0x01ab:
            r0 = move-exception
            r1 = r3
        L_0x01ad:
            if (r1 == 0) goto L_0x01b6
            org.apache.http.conn.ClientConnectionManager r1 = r1.getConnectionManager()
            r1.shutdown()
        L_0x01b6:
            throw r0
        L_0x01b7:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x01b7 }
            throw r0
        L_0x01ba:
            r1 = move-exception
            goto L_0x008d
        L_0x01bd:
            r0 = move-exception
            r1 = r2
            goto L_0x01ad
        L_0x01c0:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x01ad
        L_0x01c5:
            r0 = move-exception
            goto L_0x01ad
        L_0x01c7:
            r1 = move-exception
            r1 = r3
            goto L_0x017a
        L_0x01ca:
            r1 = move-exception
            r1 = r2
            goto L_0x017a
        L_0x01cd:
            r0 = move-exception
            r0 = r2
            goto L_0x0155
        L_0x01d0:
            r0 = move-exception
            r0 = r3
            goto L_0x012d
        L_0x01d4:
            r1 = r0
            r0 = r2
            goto L_0x0078
        L_0x01d8:
            r0 = r2
            goto L_0x0078
        L_0x01db:
            r0 = r2
            goto L_0x0078
        L_0x01de:
            r0 = r2
            goto L_0x0078
        L_0x01e1:
            r2 = r3
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wiyun.game.f.a(java.lang.String, java.lang.String, int, java.lang.String[]):void");
    }

    static void a(String str, String str2, String str3) {
        a(5, 200, "EV_BIND_BY_EMAIL_MOBILE_PASSWORD", "/device/bind", "POST", "email", str, "mobile", str2, "password", str3);
    }

    static void a(String str, String str2, String str3, int i2, int i3, byte[] bArr) {
        a(51, 200, "EV_SEND_CHALLENGE", "/challenge", "blob", bArr, "challenge_definition_id", str, "message", str2, "to_user_ids", str3, "score", String.valueOf(i2), "bid_point", String.valueOf(i3));
    }

    static void a(String str, String str2, String str3, String str4) {
        a(20, 200, "EV_ENABLE_ACCOUNT_RETRIEVAL", "/user/info", "POST", "email", str, "mobile", str2, "old_password", str3, "new_password", str4);
    }

    static void a(String str, String str2, String str3, byte[] bArr) {
        a(60, 200, "EV_REPLY_TOPIC", "/replys", "pic", bArr, "app_id", str, "topic_id", str2, "content", str3);
    }

    static void a(final String str, final String str2, final String str3, final String... strArr) {
        AnonymousClass3 r0 = new Thread() {
            public void run() {
                f.a(str, str2, 89, str3, strArr);
            }
        };
        r0.setPriority(1);
        r0.setName("WiGame HTTP thread");
        r0.start();
    }

    static void a(String str, byte[] bArr) {
        a(55, 200, "EV_POST_ACTIVITY", "/feeds", "pic", bArr, "content", str);
    }

    private static void a(StringBuilder sb, String str, String str2) throws UnsupportedEncodingException {
        if (str2 != null && str2.length() > 0) {
            if (sb.length() > 0) {
                sb.append('?');
            }
            sb.append(str).append("=").append(URLEncoder.encode(str2, "UTF-8"));
        }
    }

    private static void a(List<com.wiyun.game.c.e> list, String str, String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append(str).append(str2);
        ArrayList<com.wiyun.game.c.a> arrayList = new ArrayList<>();
        for (com.wiyun.game.c.e next : list) {
            if (next instanceof com.wiyun.game.c.a) {
                arrayList.add((com.wiyun.game.c.a) next);
            }
        }
        Collections.sort(arrayList, new Comparator<com.wiyun.game.c.a>() {
            /* renamed from: a */
            public int compare(com.wiyun.game.c.a object1, com.wiyun.game.c.a object2) {
                return object1.c().compareToIgnoreCase(object2.c());
            }
        });
        for (com.wiyun.game.c.a aVar : arrayList) {
            sb.append(aVar.c()).append('=').append(aVar.b());
        }
        sb.append(WiGame.n());
        byte[] a2 = com.wiyun.game.e.a.a(h.d(sb.toString()));
        sb.setLength(0);
        for (int i2 = 0; i2 < a2.length; i2++) {
            sb.append(String.format("%02x", Integer.valueOf(a2[i2] & 255)));
        }
        list.add(new com.wiyun.game.c.a("sig", sb.toString()));
    }

    private static void a(Map<String, String> map, String str, String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append(str).append(str2);
        ArrayList<String> arrayList = new ArrayList<>(map.keySet());
        Collections.sort(arrayList);
        for (String str3 : arrayList) {
            sb.append(str3).append('=').append(map.get(str3));
        }
        sb.append(WiGame.n());
        byte[] a2 = com.wiyun.game.e.a.a(h.d(sb.toString()));
        sb.setLength(0);
        for (int i2 = 0; i2 < a2.length; i2++) {
            sb.append(String.format("%02x", Integer.valueOf(a2[i2] & 255)));
        }
        map.put("sig", sb.toString());
    }

    static void a(HttpUriRequest httpUriRequest) {
        String language = Locale.getDefault().getLanguage();
        if (!TextUtils.isEmpty(language)) {
            httpUriRequest.setHeader("Accept-Language", language);
        }
        httpUriRequest.setHeader("Accept-Encoding", "gzip,deflate");
        httpUriRequest.setHeader("WiYun-SDK-Version", "3.0");
        httpUriRequest.setHeader("WiYun-Api-Version", "3.0");
        httpUriRequest.setHeader("WiYun-App-Version", WiGame.e);
        httpUriRequest.setHeader("WiYun-Platform", "android");
        httpUriRequest.setHeader("WiYun-Brand", WiGame.E());
        httpUriRequest.setHeader("WiYun-Model", WiGame.F());
        httpUriRequest.setHeader("WiYun-HiDpi", WiGame.I() >= 240 ? "2" : "1");
        httpUriRequest.setHeader("WiYun-Dpi", String.valueOf(WiGame.I()));
        httpUriRequest.setHeader("WiYun-Channel", WiGame.getChannel());
    }

    static void a(boolean sandbox) {
        if (sandbox) {
            c = String.valueOf("wiyun.com".equals("wiyun.com") ? "api.sandbox." : "") + "wiyun.com";
            d = String.valueOf("wiyun.com".equals("wiyun.com") ? "game.sandbox." : "") + "wiyun.com";
            b = "wiyun.com".equals("wiyun.com") ? "80" : "8000";
            e = "http://" + c + ":" + b + (d.endsWith(".wiyun.com") ? "" : "/wiapi");
            f = "http://" + c + ":" + a + (d.endsWith(".wiyun.com") ? "" : "/wiapi");
            return;
        }
        c = String.valueOf("wiyun.com".equals("wiyun.com") ? "api" : "") + "wiyun.com";
        d = String.valueOf("wiyun.com".equals("wiyun.com") ? "game." : "") + "wiyun.com";
        e = String.valueOf("wiyun.com".equals("wiyun.com") ? "https://" : "http://") + c + ":" + b + (d.endsWith(".wiyun.com") ? "" : "/wiapi");
        f = "http://" + c + ":" + a + (d.endsWith(".wiyun.com") ? "" : "/wiapi");
    }

    static void a(boolean z, int i2, int i3, String str, String str2, String[] strArr, byte[][] bArr, String... strArr2) {
        final boolean z2 = z;
        final String str3 = str2;
        final int i4 = i2;
        final String[] strArr3 = strArr;
        final byte[][] bArr2 = bArr;
        final String[] strArr4 = strArr2;
        AnonymousClass6 r0 = new Thread() {
            public void run() {
                f.a(z2, str3, i4, strArr3, bArr2, strArr4);
            }
        };
        r0.setPriority(1);
        r0.setName("WiGame HTTP thread");
        r0.start();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:57:?, code lost:
        r3 = com.wiyun.game.t.h("wy_label_connection_problem");
        r5 = new com.wiyun.game.b.e();
        r5.a = r19;
        r5.b = 501;
        r5.c = true;
        r5.i = r22;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x01e4, code lost:
        r4.getConnectionManager().shutdown();
        r22 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:?, code lost:
        r3 = com.wiyun.game.t.h("wy_label_connection_problem");
        r5 = new com.wiyun.game.b.e();
        r5.a = r19;
        r5.b = 501;
        r5.c = true;
        r5.i = r22;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x020c, code lost:
        r4.getConnectionManager().shutdown();
        r22 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:?, code lost:
        r3 = com.wiyun.game.t.h("wy_label_connection_problem");
        r5 = new com.wiyun.game.b.e();
        r5.a = r19;
        r5.b = 501;
        r5.c = true;
        r5.i = r22;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0234, code lost:
        r4.getConnectionManager().shutdown();
        r22 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0276, code lost:
        r17 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0277, code lost:
        r4.getConnectionManager().shutdown();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x027e, code lost:
        throw r17;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01c7 A[ExcHandler: SocketException (e java.net.SocketException), Splitter:B:1:0x0006] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x01ef A[ExcHandler: SocketTimeoutException (e java.net.SocketTimeoutException), Splitter:B:1:0x0006] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0217 A[ExcHandler: UnknownHostException (e java.net.UnknownHostException), Splitter:B:1:0x0006] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void a(boolean r17, java.lang.String r18, int r19, java.lang.String[] r20, byte[][] r21, java.lang.String... r22) {
        /*
            org.apache.http.impl.client.DefaultHttpClient r4 = a()
            r3 = 0
            r5 = 0
            com.wiyun.game.a.f.b()     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            java.lang.String r7 = m(r18)     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            r6.<init>(r7)     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            java.util.Locale r7 = java.util.Locale.getDefault()     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            java.lang.String r7 = r7.getCountry()     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            java.util.ArrayList r8 = new java.util.ArrayList     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            r8.<init>()     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            r0 = r22
            int r0 = r0.length     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            r9 = r0
            if (r21 == 0) goto L_0x002e
            if (r20 == 0) goto L_0x002e
            r0 = r20
            int r0 = r0.length     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            r10 = r0
            r11 = 0
        L_0x002c:
            if (r11 < r10) goto L_0x0171
        L_0x002e:
            r10 = 0
        L_0x002f:
            if (r10 < r9) goto L_0x0194
            com.wiyun.game.c.a r9 = new com.wiyun.game.c.a     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            java.lang.String r10 = "app_key"
            java.lang.String r11 = com.wiyun.game.WiGame.m()     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            r9.<init>(r10, r11)     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            r8.add(r9)     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            com.wiyun.game.c.a r9 = new com.wiyun.game.c.a     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            java.lang.String r10 = "device_id"
            java.lang.String r11 = com.wiyun.game.WiGame.q()     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            r9.<init>(r10, r11)     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            r8.add(r9)     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            com.wiyun.game.c.a r9 = new com.wiyun.game.c.a     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            java.lang.String r10 = "device_uuid"
            java.lang.String r11 = com.wiyun.game.WiGame.r()     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            r9.<init>(r10, r11)     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            r8.add(r9)     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            boolean r9 = android.text.TextUtils.isEmpty(r7)     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            if (r9 != 0) goto L_0x006b
            com.wiyun.game.c.a r9 = new com.wiyun.game.c.a     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            java.lang.String r10 = "country"
            r9.<init>(r10, r7)     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            r8.add(r9)     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
        L_0x006b:
            java.lang.String r7 = com.wiyun.game.WiGame.v()     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            boolean r7 = android.text.TextUtils.isEmpty(r7)     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            if (r7 != 0) goto L_0x0083
            com.wiyun.game.c.a r7 = new com.wiyun.game.c.a     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            java.lang.String r9 = "session_key"
            java.lang.String r10 = com.wiyun.game.WiGame.v()     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            r7.<init>(r9, r10)     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            r8.add(r7)     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
        L_0x0083:
            java.lang.String r7 = "POST"
            r0 = r8
            r1 = r7
            r2 = r18
            a(r0, r1, r2)     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            java.lang.String r7 = "name"
            r0 = r22
            r1 = r7
            java.lang.String r7 = a(r0, r1)     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            if (r17 == 0) goto L_0x0287
            java.util.Iterator r9 = r8.iterator()     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            r10 = r3
        L_0x009c:
            boolean r3 = r9.hasNext()     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            if (r3 != 0) goto L_0x01b0
            r9 = r10
        L_0x00a3:
            org.apache.http.client.methods.HttpPost r10 = new org.apache.http.client.methods.HttpPost     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            java.lang.String r3 = r6.toString()     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            r10.<init>(r3)     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            com.wiyun.game.c.d r6 = new com.wiyun.game.c.d     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            int r3 = r8.size()     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            com.wiyun.game.c.e[] r3 = new com.wiyun.game.c.e[r3]     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            java.lang.Object[] r3 = r8.toArray(r3)     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            com.wiyun.game.c.e[] r3 = (com.wiyun.game.c.e[]) r3     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            r6.<init>(r3)     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            r10.setEntity(r6)     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            a(r10)     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            if (r17 == 0) goto L_0x00d7
            android.os.Handler r3 = com.wiyun.game.WiGame.x()     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            r6 = 1003(0x3eb, float:1.406E-42)
            r8 = 0
            android.os.Message r3 = r3.obtainMessage(r6, r9, r8, r7)     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            android.os.Handler r6 = com.wiyun.game.WiGame.x()     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            r6.sendMessage(r3)     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
        L_0x00d7:
            org.apache.http.HttpResponse r3 = r4.execute(r10)     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            org.apache.http.StatusLine r6 = r3.getStatusLine()     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            int r6 = r6.getStatusCode()     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            org.apache.http.HttpEntity r3 = r3.getEntity()     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            java.lang.String r3 = a(r3)     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            com.wiyun.game.b.e r5 = new com.wiyun.game.b.e     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x0285 }
            r5.<init>()     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x0285 }
            r5.b = r6     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x0285 }
            r0 = r19
            r1 = r5
            r1.a = r0     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x0285 }
            r0 = r22
            r1 = r5
            r1.i = r0     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x0285 }
            org.apache.http.conn.ClientConnectionManager r22 = r4.getConnectionManager()
            r22.shutdown()
            r22 = r5
        L_0x0105:
            r0 = r3
            r1 = r22
            com.wiyun.game.b.c.a(r0, r1)
            java.lang.String r3 = "call_id"
            r0 = r22
            r1 = r3
            java.lang.String r3 = r0.a(r1)
            boolean r4 = android.text.TextUtils.isEmpty(r3)
            if (r4 != 0) goto L_0x0123
            long r3 = java.lang.Long.parseLong(r3)     // Catch:{ NumberFormatException -> 0x0282 }
            r0 = r3
            r2 = r22
            r2.j = r0     // Catch:{ NumberFormatException -> 0x0282 }
        L_0x0123:
            r0 = r22
            boolean r0 = r0.c
            r3 = r0
            if (r3 == 0) goto L_0x0165
            com.wiyun.game.f$a r3 = new com.wiyun.game.f$a
            r3.<init>()
            r0 = r17
            r1 = r3
            r1.a = r0
            r0 = r18
            r1 = r3
            r1.b = r0
            r0 = r22
            int r0 = r0.b
            r17 = r0
            r0 = r17
            r1 = r3
            r1.d = r0
            r0 = r22
            r1 = r3
            r1.e = r0
            r0 = r20
            r1 = r3
            r1.f = r0
            r0 = r21
            r1 = r3
            r1.g = r0
            java.util.Map<java.lang.Integer, com.wiyun.game.f$a> r17 = com.wiyun.game.f.g
            monitor-enter(r17)
            java.util.Map<java.lang.Integer, com.wiyun.game.f$a> r18 = com.wiyun.game.f.g     // Catch:{ all -> 0x027f }
            java.lang.Integer r19 = java.lang.Integer.valueOf(r19)     // Catch:{ all -> 0x027f }
            r0 = r18
            r1 = r19
            r2 = r3
            r0.put(r1, r2)     // Catch:{ all -> 0x027f }
            monitor-exit(r17)     // Catch:{ all -> 0x027f }
        L_0x0165:
            com.wiyun.game.b.d r17 = com.wiyun.game.b.d.a()
            r0 = r17
            r1 = r22
            r0.a(r1)
            return
        L_0x0171:
            r12 = r20[r11]     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            boolean r12 = android.text.TextUtils.isEmpty(r12)     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            if (r12 != 0) goto L_0x0190
            r12 = r21[r11]     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            if (r12 == 0) goto L_0x0190
            com.wiyun.game.c.b r12 = new com.wiyun.game.c.b     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            r13 = r20[r11]     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            com.wiyun.game.c.c r14 = new com.wiyun.game.c.c     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            java.lang.String r15 = "blob"
            r16 = r21[r11]     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            r14.<init>(r15, r16)     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            r12.<init>(r13, r14)     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            r8.add(r12)     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
        L_0x0190:
            int r11 = r11 + 1
            goto L_0x002c
        L_0x0194:
            int r11 = r10 + 1
            r11 = r22[r11]     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            boolean r11 = android.text.TextUtils.isEmpty(r11)     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            if (r11 != 0) goto L_0x01ac
            com.wiyun.game.c.a r11 = new com.wiyun.game.c.a     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            r12 = r22[r10]     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            int r13 = r10 + 1
            r13 = r22[r13]     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            r11.<init>(r12, r13)     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            r8.add(r11)     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
        L_0x01ac:
            int r10 = r10 + 2
            goto L_0x002f
        L_0x01b0:
            java.lang.Object r3 = r9.next()     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            com.wiyun.game.c.e r3 = (com.wiyun.game.c.e) r3     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            r11 = 1
            r3.a(r11)     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            r3.a(r7)     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            long r10 = (long) r10     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            long r12 = r3.a()     // Catch:{ SocketException -> 0x01c7, SocketTimeoutException -> 0x01ef, UnknownHostException -> 0x0217, Exception -> 0x023f }
            long r10 = r10 + r12
            int r3 = (int) r10
            r10 = r3
            goto L_0x009c
        L_0x01c7:
            r3 = move-exception
            java.lang.String r3 = "wy_label_connection_problem"
            java.lang.String r3 = com.wiyun.game.t.h(r3)     // Catch:{ all -> 0x0276 }
            com.wiyun.game.b.e r5 = new com.wiyun.game.b.e     // Catch:{ all -> 0x0276 }
            r5.<init>()     // Catch:{ all -> 0x0276 }
            r0 = r19
            r1 = r5
            r1.a = r0     // Catch:{ all -> 0x0276 }
            r6 = 501(0x1f5, float:7.02E-43)
            r5.b = r6     // Catch:{ all -> 0x0276 }
            r6 = 1
            r5.c = r6     // Catch:{ all -> 0x0276 }
            r0 = r22
            r1 = r5
            r1.i = r0     // Catch:{ all -> 0x0276 }
            org.apache.http.conn.ClientConnectionManager r22 = r4.getConnectionManager()
            r22.shutdown()
            r22 = r5
            goto L_0x0105
        L_0x01ef:
            r3 = move-exception
            java.lang.String r3 = "wy_label_connection_problem"
            java.lang.String r3 = com.wiyun.game.t.h(r3)     // Catch:{ all -> 0x0276 }
            com.wiyun.game.b.e r5 = new com.wiyun.game.b.e     // Catch:{ all -> 0x0276 }
            r5.<init>()     // Catch:{ all -> 0x0276 }
            r0 = r19
            r1 = r5
            r1.a = r0     // Catch:{ all -> 0x0276 }
            r6 = 501(0x1f5, float:7.02E-43)
            r5.b = r6     // Catch:{ all -> 0x0276 }
            r6 = 1
            r5.c = r6     // Catch:{ all -> 0x0276 }
            r0 = r22
            r1 = r5
            r1.i = r0     // Catch:{ all -> 0x0276 }
            org.apache.http.conn.ClientConnectionManager r22 = r4.getConnectionManager()
            r22.shutdown()
            r22 = r5
            goto L_0x0105
        L_0x0217:
            r3 = move-exception
            java.lang.String r3 = "wy_label_connection_problem"
            java.lang.String r3 = com.wiyun.game.t.h(r3)     // Catch:{ all -> 0x0276 }
            com.wiyun.game.b.e r5 = new com.wiyun.game.b.e     // Catch:{ all -> 0x0276 }
            r5.<init>()     // Catch:{ all -> 0x0276 }
            r0 = r19
            r1 = r5
            r1.a = r0     // Catch:{ all -> 0x0276 }
            r6 = 501(0x1f5, float:7.02E-43)
            r5.b = r6     // Catch:{ all -> 0x0276 }
            r6 = 1
            r5.c = r6     // Catch:{ all -> 0x0276 }
            r0 = r22
            r1 = r5
            r1.i = r0     // Catch:{ all -> 0x0276 }
            org.apache.http.conn.ClientConnectionManager r22 = r4.getConnectionManager()
            r22.shutdown()
            r22 = r5
            goto L_0x0105
        L_0x023f:
            r3 = move-exception
            r3 = r5
        L_0x0241:
            java.lang.String r5 = "WiYun"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0276 }
            java.lang.String r7 = "failed opearation body: "
            r6.<init>(r7)     // Catch:{ all -> 0x0276 }
            java.lang.StringBuilder r6 = r6.append(r3)     // Catch:{ all -> 0x0276 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x0276 }
            android.util.Log.w(r5, r6)     // Catch:{ all -> 0x0276 }
            com.wiyun.game.b.e r5 = new com.wiyun.game.b.e     // Catch:{ all -> 0x0276 }
            r5.<init>()     // Catch:{ all -> 0x0276 }
            r0 = r19
            r1 = r5
            r1.a = r0     // Catch:{ all -> 0x0276 }
            r6 = 500(0x1f4, float:7.0E-43)
            r5.b = r6     // Catch:{ all -> 0x0276 }
            r6 = 1
            r5.c = r6     // Catch:{ all -> 0x0276 }
            r0 = r22
            r1 = r5
            r1.i = r0     // Catch:{ all -> 0x0276 }
            org.apache.http.conn.ClientConnectionManager r22 = r4.getConnectionManager()
            r22.shutdown()
            r22 = r5
            goto L_0x0105
        L_0x0276:
            r17 = move-exception
            org.apache.http.conn.ClientConnectionManager r18 = r4.getConnectionManager()
            r18.shutdown()
            throw r17
        L_0x027f:
            r18 = move-exception
            monitor-exit(r17)     // Catch:{ all -> 0x027f }
            throw r18
        L_0x0282:
            r3 = move-exception
            goto L_0x0123
        L_0x0285:
            r5 = move-exception
            goto L_0x0241
        L_0x0287:
            r9 = r3
            goto L_0x00a3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wiyun.game.f.a(boolean, java.lang.String, int, java.lang.String[], byte[][], java.lang.String[]):void");
    }

    static void a(boolean z, boolean z2) {
        a(15, 200, "EV_UPDATE_USER_SETTINGS", "/user/info", "POST", "share_location", String.valueOf(z), "share_status", String.valueOf(z2));
    }

    static void a(byte[] bArr) {
        a(18, 200, "EV_UPDATE_MY_PORTRAIT", "/user/info", "avatar", bArr, new String[0]);
    }

    static long b(String str, int i2, int i3) {
        long currentTimeMillis = System.currentTimeMillis();
        a(74, 200, "EV_GET_GAME_SLOTS", "/savecard", "GET", "app_id", str, "call_id", String.valueOf(currentTimeMillis));
        return currentTimeMillis;
    }

    static long b(String str, String str2) {
        long currentTimeMillis = System.currentTimeMillis();
        a(25, 200, "EV_ADD_FRIEND", "/friends", "POST", "user_id", str, "call_id", String.valueOf(currentTimeMillis));
        return currentTimeMillis;
    }

    static long b(String str, String str2, String str3, String... strArr) {
        long currentTimeMillis = System.currentTimeMillis();
        String[] strArr2 = new String[(strArr == null ? 2 : strArr.length + 2)];
        int i2 = 0;
        if (strArr != null) {
            while (i2 < strArr.length) {
                strArr2[i2] = strArr[i2];
                i2++;
            }
        }
        strArr2[i2] = "call_id";
        strArr2[i2 + 1] = String.valueOf(currentTimeMillis);
        a(str, str2, str3, strArr2);
        return currentTimeMillis;
    }

    static void b() {
        synchronized (g) {
            g.clear();
        }
    }

    static void b(String str) {
        if (!WiGame.w()) {
            WiGame.a(true);
            a(6, 200, "EV_LOGIN", "/device/login", "POST", "platform", "android", "brand", WiGame.E(), "user_id", str);
        }
    }

    static void b(String str, int i2, int i3, byte[] bArr) {
        a(54, 200, "EV_SUBMIT_CACHED_CHALLENGE_RESULT", "/challenge/result", "blob", bArr, "challenge_to_user_id", str, "score", String.valueOf(i3), "result", String.valueOf(i2));
    }

    static void b(String str, int i2, byte[] bArr, double d2, double d3) {
        String[] strArr = new String[8];
        strArr[0] = "leaderboard_id";
        strArr[1] = str;
        strArr[2] = "score";
        strArr[3] = String.valueOf(i2);
        strArr[4] = "lat";
        strArr[5] = d2 == 0.0d ? null : String.valueOf(d2);
        strArr[6] = "lon";
        strArr[7] = d3 == 0.0d ? null : String.valueOf(d3);
        a(13, 200, "EV_SUBMIT_CACHED_SCORE", "/score", "blob", bArr, strArr);
    }

    static void b(String str, String str2, String str3, byte[] bArr) {
        a(58, 200, "EV_POST_TOPIC", "/topics", "pic", bArr, "app_id", str, "title", str2, "content", str3);
    }

    private static void b(StringBuilder sb, String str, String str2) throws UnsupportedEncodingException {
        if (str2 != null && str2.length() > 0) {
            if (sb.length() > 0) {
                sb.append('&');
            }
            sb.append(str).append("=").append(URLEncoder.encode(str2, "UTF-8"));
        }
    }

    private static void b(List<NameValuePair> list, String str, String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append(str).append(str2);
        Collections.sort(list, new Comparator<NameValuePair>() {
            /* renamed from: a */
            public int compare(NameValuePair object1, NameValuePair object2) {
                return object1.getName().compareToIgnoreCase(object2.getName());
            }
        });
        for (NameValuePair next : list) {
            sb.append(next.getName()).append('=').append(next.getValue());
        }
        sb.append(WiGame.n());
        byte[] a2 = com.wiyun.game.e.a.a(h.d(sb.toString()));
        sb.setLength(0);
        for (int i2 = 0; i2 < a2.length; i2++) {
            sb.append(String.format("%02x", Integer.valueOf(a2[i2] & 255)));
        }
        list.add(new BasicNameValuePair("sig", sb.toString()));
    }

    static boolean b(int eventId) {
        boolean containsKey;
        synchronized (i) {
            containsKey = i.containsKey(Integer.valueOf(eventId));
        }
        return containsKey;
    }

    static void c() {
        synchronized (i) {
            i.clear();
        }
    }

    static void c(int eventId) {
        synchronized (i) {
            i.remove(Integer.valueOf(eventId));
        }
    }

    static void c(String str) {
        a(8, 200, "EV_GET_APP_CONFIG", "/app/config", "GET", "offline", "true", "app_id", str);
    }

    static long d(String str) {
        long currentTimeMillis = System.currentTimeMillis();
        a(14, 200, "EV_GET_USER_INFO", "/user/info", "GET", "user_id", str, "call_id", String.valueOf(currentTimeMillis));
        return currentTimeMillis;
    }

    static void d() {
        synchronized (g) {
            h.clear();
            h.putAll(g);
        }
        for (Map.Entry<Integer, a> entry : h.entrySet()) {
            a op = (a) entry.getValue();
            if (op.d == 401) {
                d(op.e.a);
            }
        }
    }

    static void d(int i2) {
        synchronized (g) {
            final a remove = g.remove(Integer.valueOf(i2));
            if (remove != null) {
                synchronized (i) {
                    i.put(Integer.valueOf(remove.e.a), remove);
                }
                AnonymousClass2 r1 = new Thread() {
                    public void run() {
                        if (a.this.f == null) {
                            f.a(a.this.b, a.this.c, a.this.e.a, a.this.e.i);
                        } else {
                            f.a(a.this.a, a.this.b, a.this.e.a, a.this.f, a.this.g, a.this.e.i);
                        }
                    }
                };
                r1.setPriority(1);
                r1.setName("WiGame HTTP thread");
                r1.start();
            }
        }
    }

    static void e() {
        synchronized (g) {
            h.clear();
            h.putAll(g);
        }
        for (Map.Entry entry : h.entrySet()) {
            a op = (a) entry.getValue();
            if (op.d == 401) {
                g.remove(entry.getKey());
                op.e.d = true;
                synchronized (i) {
                    i.put(Integer.valueOf(op.e.a), op);
                }
                com.wiyun.game.b.d.a().a(op.e);
            }
        }
    }

    static void e(String str) {
        a(41, 200, "EV_UNLOCK_ACHIEVEMENT", "/achievement", "POST", "achievement_id", str);
    }

    static long f() {
        long currentTimeMillis = System.currentTimeMillis();
        a(3, 200, "EV_GET_BOUND_USERS_new", "/device/bind", "GET", "call_id", String.valueOf(currentTimeMillis));
        return currentTimeMillis;
    }

    static void f(String str) {
        a(42, 200, "EV_UNLOCK_CACHED_ACHIEVEMENT", "/achievement", "POST", "achievement_id", str);
    }

    static long g() {
        long currentTimeMillis = System.currentTimeMillis();
        a(4, 200, "EV_CHECK_BOUND_USERS", "/device/bind", "GET", "call_id", String.valueOf(currentTimeMillis));
        return currentTimeMillis;
    }

    static void g(String str) {
        a(44, 200, "EV_ACCEPT_CHALLENGE", "/challenge/pending", "POST", "challenge_to_user_id", str);
    }

    static void h() {
        a(7, 200, "EV_HEART_BEAT", "/ping", "GET", "user_id", WiGame.getMyId());
    }

    static void h(String str) {
        a(50, 200, "EV_GET_CHALLENGE_DEFINITION", "/challenge/define", "GET", "challenge_definition_id", str);
    }

    static long i() {
        long currentTimeMillis = System.currentTimeMillis();
        a(90, 200, "EV_GET_SNS_CHANNELS", "/sns/list", "GET", "call_id", String.valueOf(currentTimeMillis));
        return currentTimeMillis;
    }

    static void i(String str) {
        a(18, 200, "EV_UPDATE_MY_PORTRAIT", "/user/info", "POST", "portrait", str);
    }

    static long j(String str) {
        long currentTimeMillis = System.currentTimeMillis();
        a(75, 200, "EV_DELETE_GAME_SLOT", "/savecard", "DELETE", "save_id", str, "call_id", String.valueOf(currentTimeMillis));
        return currentTimeMillis;
    }

    static long k(String str) {
        long currentTimeMillis = System.currentTimeMillis();
        a(86, 200, "EV_GET_MY_ITEMS", "/shop/me", "GET", "app_id", str, "call_id", String.valueOf(currentTimeMillis));
        return currentTimeMillis;
    }

    static long l(String str) {
        long currentTimeMillis = System.currentTimeMillis();
        a(88, 200, "EV_USE_ITEMS", "/shop/use", "POST", "items", str, "call_id", String.valueOf(currentTimeMillis));
        return currentTimeMillis;
    }

    private static String m(String path) {
        return ((d.b() || !d.c()) ? e : f) + path;
    }
}
