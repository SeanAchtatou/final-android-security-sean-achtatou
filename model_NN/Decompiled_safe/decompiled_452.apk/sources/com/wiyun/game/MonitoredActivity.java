package com.wiyun.game;

import android.app.Activity;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.Iterator;

public class MonitoredActivity extends Activity {
    private final ArrayList<a> a = new ArrayList<>();

    public interface a {
        void a(MonitoredActivity monitoredActivity);

        void b(MonitoredActivity monitoredActivity);

        void c(MonitoredActivity monitoredActivity);

        void d(MonitoredActivity monitoredActivity);
    }

    public static class b implements a {
        public void a(MonitoredActivity activity) {
        }

        public void b(MonitoredActivity activity) {
        }

        public void c(MonitoredActivity activity) {
        }

        public void d(MonitoredActivity activity) {
        }
    }

    public void a(a listener) {
        if (!this.a.contains(listener)) {
            this.a.add(listener);
        }
    }

    public void b(a listener) {
        this.a.remove(listener);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Iterator<a> it = this.a.iterator();
        while (it.hasNext()) {
            it.next().d(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        Iterator<a> it = this.a.iterator();
        while (it.hasNext()) {
            it.next().a(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        Iterator<a> it = this.a.iterator();
        while (it.hasNext()) {
            it.next().c(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        Iterator<a> it = this.a.iterator();
        while (it.hasNext()) {
            it.next().b(this);
        }
    }
}
