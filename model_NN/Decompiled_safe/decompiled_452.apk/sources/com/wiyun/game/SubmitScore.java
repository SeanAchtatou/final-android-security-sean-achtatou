package com.wiyun.game;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.wiyun.game.WiGame;
import com.wiyun.game.b.b;
import com.wiyun.game.b.d;
import com.wiyun.game.b.e;
import com.wiyun.game.model.a.ag;
import com.wiyun.game.model.a.c;
import java.util.HashMap;
import java.util.Map;

public class SubmitScore extends Activity implements View.OnClickListener, b {
    protected Map<String, Bitmap> a;
    /* access modifiers changed from: private */
    public View b;
    /* access modifiers changed from: private */
    public ViewGroup c;
    /* access modifiers changed from: private */
    public ImageView d;
    private TextView e;
    private TextView f;
    /* access modifiers changed from: private */
    public TextView g;
    private long h;
    private WiGame.d i;
    private BroadcastReceiver j = new k(this);

    private void a() {
        this.i = (WiGame.d) getIntent().getSerializableExtra("pending_score");
        i.a(getFilesDir());
        this.a = new HashMap();
        d.a().a(this);
        registerReceiver(this.j, new IntentFilter("com.wiyun.game.IMAGE_DOWNLOADED"));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wiyun.game.h.a(java.util.Map<java.lang.String, android.graphics.Bitmap>, boolean, java.lang.String, java.lang.String, boolean):android.graphics.Bitmap
     arg types: [java.util.Map<java.lang.String, android.graphics.Bitmap>, int, java.lang.String, java.lang.String, boolean]
     candidates:
      com.wiyun.game.h.a(int, int, int, int, android.net.Uri):android.content.Intent
      com.wiyun.game.h.a(android.content.Context, java.lang.String, int, android.view.View$OnClickListener, int[]):void
      com.wiyun.game.h.a(java.util.Map<java.lang.String, android.graphics.Bitmap>, boolean, java.lang.String, java.lang.String, boolean):android.graphics.Bitmap */
    private void b() {
        this.b = findViewById(t.d("wy_ll_progress_panel"));
        this.c = (ViewGroup) findViewById(t.d("wy_ll_main_panel"));
        this.d = (ImageView) findViewById(t.d("wy_iv_portrait"));
        this.f = (TextView) findViewById(t.d("wy_tv_username"));
        this.e = (TextView) findViewById(t.d("wy_tv_score"));
        this.g = (TextView) findViewById(t.d("wy_tv_rank"));
        this.f.setText(WiGame.getMyName());
        c b2 = x.b(this.i.b);
        if (b2 == null) {
            this.e.setText(String.valueOf(this.i.c));
        } else if (b2.c()) {
            int i2 = this.i.c / 1000;
            int i3 = i2 / 60;
            int i4 = i2 % 60;
            if (this.i.c % 1000 != 0) {
                int i5 = this.i.c % 1000;
                if (i3 == 0) {
                    this.e.setText(String.format("%d.%d", Integer.valueOf(i4), Integer.valueOf(i5)));
                } else {
                    this.e.setText(String.format("%d:%d.%d", Integer.valueOf(i3), Integer.valueOf(i4), Integer.valueOf(i5)));
                }
            } else if (i3 == 0) {
                this.e.setText(String.valueOf(i4));
            } else {
                this.e.setText(String.format("%d:%d", Integer.valueOf(i3), Integer.valueOf(i4)));
            }
        } else {
            this.e.setText(String.valueOf(this.i.c));
        }
        ag u = WiGame.u();
        this.d.setImageBitmap(h.a(this.a, false, h.b("p_", u.getId()), u.getAvatarUrl(), u.isFemale()));
        ((Button) findViewById(t.d("wy_b_close"))).setOnClickListener(this);
        ((Button) findViewById(t.d("wy_b_open_leaderboard"))).setOnClickListener(this);
    }

    private void c() {
        if (this.a != null) {
            for (Bitmap bitmap : this.a.values()) {
                if (bitmap != null && !bitmap.isRecycled()) {
                    bitmap.recycle();
                }
            }
            this.a.clear();
        }
    }

    public void b(final e e2) {
        switch (e2.a) {
            case 12:
                if (e2.j == this.h) {
                    e2.d = false;
                    if (e2.c) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                SubmitScore.this.g.setText(t.f("wy_label_your_score_cached"));
                                SubmitScore.this.b.setVisibility(4);
                                h.b(SubmitScore.this.c);
                            }
                        });
                        return;
                    } else if (e2.g == 0) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                String lbId = e2.a("leaderboard_id");
                                int score = h.c(e2.a("score"));
                                c lb = x.b(lbId);
                                if (lb != null && lb.e()) {
                                    int highest = i.c(lbId);
                                    if (score > highest) {
                                        i.a(lbId, score);
                                    } else {
                                        score = highest;
                                    }
                                }
                                if (lb != null) {
                                    if (lb.c()) {
                                        int second = score / 1000;
                                        String timeScore = String.format("%d:%d.%d", Integer.valueOf(second / 60), Integer.valueOf(second % 60), Integer.valueOf(score % 1000));
                                        SubmitScore.this.g.setText(String.format(t.h("wy_label_your_best_score_is_x"), timeScore));
                                    } else {
                                        SubmitScore.this.g.setText(String.format(t.h("wy_label_your_best_score_is_x"), String.valueOf(score)));
                                    }
                                }
                                SubmitScore.this.b.setVisibility(4);
                                h.b(SubmitScore.this.c);
                            }
                        });
                        return;
                    } else {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                String lbId = e2.a("leaderboard_id");
                                int score = h.c(e2.a("score"));
                                c lb = x.b(lbId);
                                if (lb != null && lb.e() && score > i.c(lbId)) {
                                    i.a(lbId, score);
                                }
                                SubmitScore.this.g.setText(String.format(t.h("wy_label_your_today_rank_is_x"), Integer.valueOf(e2.g)));
                                SubmitScore.this.b.setVisibility(4);
                                h.b(SubmitScore.this.c);
                            }
                        });
                        return;
                    }
                } else {
                    return;
                }
            default:
                return;
        }
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id == t.d("wy_b_close")) {
            finish();
        } else if (id == t.d("wy_b_open_leaderboard")) {
            WiGame.openLeaderboard(this.i.b);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(WiGame.i);
        requestWindowFeature(1);
        setContentView(t.e("wy_activity_submit_score"));
        a();
        b();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        d.a().b(this);
        unregisterReceiver(this.j);
        i.a();
        this.d.setImageBitmap(null);
        c();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        this.b.setVisibility(0);
        h.a(this.c);
        this.h = f.a(this.i.b, this.i.c, this.i.d, this.i.f, this.i.g);
    }
}
