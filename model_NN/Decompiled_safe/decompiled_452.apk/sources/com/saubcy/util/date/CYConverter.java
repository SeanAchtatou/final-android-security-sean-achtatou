package com.saubcy.util.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CYConverter {
    public static long getTimestamp() {
        Calendar calendar = Calendar.getInstance();
        return transDate2Timestamp(calendar.get(1), calendar.get(2) + 1, calendar.get(5));
    }

    public static String transTimestamp2Date(long timestamp, String format) {
        return new SimpleDateFormat(format).format(new Date(timestamp));
    }

    public static String transTimestamp2Time(long timestamp) {
        return transTimestamp2Date(timestamp, "HH:mm:ss");
    }

    public static int transTimestamp2Year(long timestamp) {
        return Integer.parseInt(transTimestamp2Date(timestamp, "yyyy"));
    }

    public static int transTimestamp2Month(long timestamp) {
        return Integer.parseInt(transTimestamp2Date(timestamp, "MM"));
    }

    public static int transTimestamp2Day(long timestamp) {
        return Integer.parseInt(transTimestamp2Date(timestamp, "dd"));
    }

    public static int transTimestamp2Hour(long timestamp) {
        return Integer.parseInt(transTimestamp2Date(timestamp, "HH"));
    }

    public static int transTimestamp2DayOfWeek(long timestamp) {
        Date dt = new Date(timestamp);
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        return cal.get(7) - 1;
    }

    public static long transDate2Timestamp(int year, int month, int day) {
        try {
            return new SimpleDateFormat("MM/dd/yyyy").parse(String.valueOf(month) + "/" + day + "/" + year).getTime();
        } catch (ParseException e) {
            return -1;
        }
    }

    public static Date transStr2DateClass(String timestr, String format) {
        try {
            return new SimpleDateFormat(format).parse(timestr);
        } catch (ParseException e) {
            return null;
        }
    }

    public static String transSeconds2Timestr(int s) {
        int hour = s / 3600;
        int min = (s - ((hour * 60) * 60)) / 60;
        return String.valueOf(String.format("%02d", Integer.valueOf(hour))) + ":" + String.format("%02d", Integer.valueOf(min)) + ":" + String.format("%02d", Integer.valueOf((s - ((hour * 60) * 60)) - (min * 60)));
    }

    public static int[] getDayOfWeekHaveByStartEnd(long start, long end) {
        int[] dayOfWeekHave = new int[7];
        long end2 = end + 86400000;
        for (long now = start; now < end2; now += 86400000) {
            int transTimestamp2DayOfWeek = transTimestamp2DayOfWeek(now) % 7;
            dayOfWeekHave[transTimestamp2DayOfWeek] = dayOfWeekHave[transTimestamp2DayOfWeek] + 1;
        }
        return dayOfWeekHave;
    }
}
