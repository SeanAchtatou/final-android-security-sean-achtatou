package com.saubcy.games.othello.market;

public class Position {
    int col = -1;
    int row = -1;

    public Position(int r, int c) {
        this.row = r;
        this.col = c;
    }

    public Position(Position pos) {
        this.row = pos.row;
        this.col = pos.col;
    }
}
