package com.saubcy.games.othello.market;

public class Grid {
    protected float bottom = -1.0f;
    protected int col = -1;
    protected float left = -1.0f;
    protected float right = -1.0f;
    protected int row = -1;
    protected int status = -1;
    protected float top = -1.0f;

    public Grid(int row2, int col2, float unit, float xOffset, float yOffset) {
        this.row = row2;
        this.col = col2;
        this.left = (((float) col2) * unit) + xOffset;
        this.top = (((float) row2) * unit) + yOffset;
        this.right = this.left + unit;
        this.bottom = this.top + unit;
    }
}
