package com.saubcy.games.othello.market;

import android.os.Handler;
import android.os.Message;

public class UIHandler extends Handler {
    protected static final int REFRESH_CHESS_BOARD = 0;
    protected static final int SUBMIT_SCORE = 2;
    protected static final int SWITCH_USR = 1;
    private ChessBoard parent = null;

    public UIHandler(ChessBoard p) {
        this.parent = p;
    }

    public void handleMessage(Message msg) {
        switch (msg.what) {
            case 0:
                this.parent.cv.refresh();
                return;
            case 1:
                this.parent.isLocked = false;
                this.parent.doSwitch();
                this.parent.ibv.refresh();
                this.parent.cv.refresh();
                return;
            case 2:
                this.parent.endGame();
                return;
            default:
                return;
        }
    }
}
