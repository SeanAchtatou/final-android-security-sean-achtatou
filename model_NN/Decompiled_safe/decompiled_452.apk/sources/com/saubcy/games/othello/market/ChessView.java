package com.saubcy.games.othello.market;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.saubcy.games.othello.market.ShowFactory;

public class ChessView extends SurfaceView implements SurfaceHolder.Callback {
    protected SurfaceHolder holder = null;
    protected ChessBoard parent = null;

    public ChessView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.parent = (ChessBoard) context;
        init();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int wMeasureSpec, int hMeasureSpec) {
        setMeasuredDimension((int) this.parent.Width, (int) (this.parent.Height - this.parent.info_board_heigt));
    }

    public void surfaceCreated(SurfaceHolder holder2) {
        refresh();
    }

    public void surfaceChanged(SurfaceHolder holder2, int format, int width, int height) {
    }

    public void surfaceDestroyed(SurfaceHolder holder2) {
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (this.parent.isLocked.booleanValue()) {
            return false;
        }
        if (this.parent.moveSide != this.parent.side_user) {
            return false;
        }
        Position pos = trans2Position(event.getX(), event.getY());
        if (pos == null) {
            return false;
        }
        switch (event.getAction()) {
            case 0:
                Log.d("trace", "touch(" + pos.row + "," + pos.col + ")");
                return tryMove(pos);
            case 1:
            case 2:
            default:
                return false;
        }
    }

    private boolean tryMove(Position pos) {
        if (100 != this.parent.grids[pos.row][pos.col].status) {
            Log.d("trace", "failed");
            return true;
        }
        this.parent.recoverPossible();
        int res = this.parent.takeMove(pos, this.parent.moveSide);
        Log.d("trace", "take res: " + res);
        if (res > 0) {
            this.parent.doEffect();
        }
        return true;
    }

    private void init() {
        this.holder = getHolder();
        this.holder.addCallback(this);
    }

    /* access modifiers changed from: protected */
    public void refresh() {
        new Thread(new ShowFactory.ChessBoardBuild(this.parent)).start();
    }

    /* access modifiers changed from: protected */
    public Position trans2Position(float x, float y) {
        if (x < this.parent.grids[0][0].left || x > this.parent.grids[0][7].right || y < this.parent.grids[0][0].top || y > this.parent.grids[7][0].bottom) {
            return null;
        }
        return new Position((int) ((y - this.parent.grids[0][0].top) / this.parent.GridUnit), (int) ((x - this.parent.grids[0][0].left) / this.parent.GridUnit));
    }
}
