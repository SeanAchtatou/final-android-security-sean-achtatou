package com.saubcy.games.othello.market;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.saubcy.games.othello.market.ShowFactory;
import com.saubcy.util.device.CYManager;
import com.waps.AppConnect;
import com.waps.ads.AdGroupLayout;
import com.wiyun.game.WiGame;
import com.wiyun.game.WiGameClient;
import com.wiyun.game.model.ChallengeRequest;
import java.lang.reflect.Array;

public class ChessBoard extends Activity {
    protected static final float BASE_HEIGHT = 480.0f;
    protected static final float BASE_WIDTH = 320.0f;
    public static final String BEST = "com.saubcy.games.othello.BEST";
    protected static final float BOARD_H_FENCE_WIDTH = 28.0f;
    protected static final float BOARD_V_FENCE_WIDTH = 26.0f;
    protected static final int CHESS_BLACK = 0;
    protected static final int CHESS_BLANK = -1;
    protected static final int CHESS_CAN = 100;
    protected static final float CHESS_COUNT_SIZE = 25.0f;
    protected static final int CHESS_WHITE = 8;
    protected static final float GRID_SIDE_LINE = 0.5f;
    protected static final float INFO_BOARD_HEIGHT = 80.0f;
    protected static final float INFO_NAME_SIZE = 25.0f;
    public static final String LEVEL = "com.saubcy.games.othello.LEVEL";
    protected static final int MAX_COL = 8;
    protected static final int MAX_ROW = 8;
    protected static final int SOUND_CLICK = 2;
    protected static final int SOUND_FLIP = 3;
    protected static final int SOUND_LOST = 1;
    protected static final int SOUND_WIN = 0;
    protected static final String[] achievementIds = {"89fb09d3517bba3a", "9cdfdec5dfc83ffd", "2a08cb01e8157282"};
    protected static final String appKey = "b05e27e7643b8b8e";
    protected static final String challengeId = "c260c66bf7d5156b";
    protected static final String[] leaderboardId = {"f5706e7d114a74c1", "03cb747ba250a546", "9c77e87bac3706c8", "a28e749dba5e50e8", "630c03b42e44c915"};
    protected static final String secretKey = "528KtHeQcfs9Ajd4ZjbDcnB7FWxPTNKX";
    protected int[] ChessCount;
    private SharedPreferences GameSettings;
    protected float GridUnit = 0.0f;
    protected float Height = 0.0f;
    protected float Width = 0.0f;
    protected int[][] backup = null;
    protected int best = 0;
    protected String black_name = null;
    protected float board_h_fence_width = 0.0f;
    protected float board_v_fence_width = 0.0f;
    protected ChessView cv = null;
    protected Grid[][] grids = null;
    protected UIHandler handler = null;
    protected InfoBoardView ibv = null;
    protected float info_board_heigt = 0.0f;
    protected float info_board_unit = 0.0f;
    protected Boolean isLocked = false;
    private int isMute = 1;
    protected int level = 0;
    protected String mChallengeToUserId = null;
    private WiGameClient mClient = new WiGameClient() {
        public void wyLoggedIn(String sessionKey) {
        }

        public void wyLogInFailed() {
        }

        public void wyPlayChallenge(ChallengeRequest request) {
            ChessBoard.this.mChallengeToUserId = request.getCtuId();
            ChessBoard.this.mCompetitorScore = request.getScore();
            ChessBoard.this.newGame();
        }

        public void wyPortraitGot(String userId) {
        }

        public void wyGameSaveStart(String name, int totalSize) {
        }

        public void wyGameSaveProgress(String name, int uploadedSize) {
        }

        public void wyGameSaved(String name) {
        }

        public void wyGameSaveFailed(String name) {
        }

        public void wyLoadGame(String blobPath) {
        }
    };
    protected int mCompetitorScore = -1;
    protected int moveSide = 0;
    protected Paint pCountBlack = null;
    protected Paint pCountWhite = null;
    protected Paint pGrid = null;
    protected Paint pInfoName = null;
    protected int side_cpu = 0;
    protected int side_user = 0;
    private int soundClick = -1;
    private int soundFlip = -1;
    private int soundLost = -1;
    private SoundPool soundPool;
    private int soundWin = -1;
    protected int tmp = 0;
    protected Bitmap useBackGroud = null;
    protected Bitmap useBlack = null;
    protected Bitmap useCan = null;
    protected Bitmap[] useChess = null;
    protected Bitmap useHead = null;
    protected Bitmap useTag = null;
    protected Bitmap useWhite = null;
    protected LinearLayout wapsView = null;
    protected String white_name = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setRequestedOrientation(1);
        getWindow().setFlags(1024, 1024);
        getWindow().setFlags(128, 128);
        init();
        loadViews();
        newGame();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_newgame:
                showRestartDialog();
                return true;
            case R.id.menu_color:
                showSwitchDialog();
                return true;
            case R.id.menu_level:
                showLevelDialog();
                return true;
            case R.id.menu_best:
                WiGame.openLeaderboard(leaderboardId[this.level - 1]);
                return true;
            case R.id.menu_mute:
                this.isMute *= -1;
                return true;
            case R.id.menu_challenge:
                WiGame.sendChallenge(challengeId, this.best, null, leaderboardId[this.level - 1]);
                return true;
            case R.id.menu_more:
                more();
                return true;
            default:
                return false;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        showExitDialog();
        return true;
    }

    /* access modifiers changed from: protected */
    public void more() {
        try {
            Intent installIntent = new Intent("android.intent.action.VIEW");
            try {
                installIntent.setData(Uri.parse("market://search?q=pub:saubcyj"));
                startActivity(installIntent);
            } catch (Exception e) {
                AppConnect.getInstance(this).showOffers(this);
            }
        } catch (Exception e2) {
            AppConnect.getInstance(this).showOffers(this);
        }
    }

    /* access modifiers changed from: private */
    public void rateit() {
        try {
            Intent installIntent = new Intent("android.intent.action.VIEW");
            try {
                installIntent.setData(Uri.parse("market://search?q=pname:com.saubcy.games.othello.market"));
                startActivity(installIntent);
            } catch (Exception e) {
                finish();
            }
        } catch (Exception e2) {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void showRestartDialog() {
        new AlertDialog.Builder(this).setMessage(getBaseContext().getResources().getString(R.string.restart)).setCancelable(false).setPositiveButton(getBaseContext().getResources().getString(R.string.positive), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ChessBoard.this.newGame();
            }
        }).setNegativeButton(getBaseContext().getResources().getString(R.string.negative), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        }).show();
    }

    /* access modifiers changed from: protected */
    public void showSwitchDialog() {
        new AlertDialog.Builder(this).setMessage(getBaseContext().getResources().getString(R.string.switch_color)).setCancelable(false).setPositiveButton(getBaseContext().getResources().getString(R.string.positive), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ChessBoard.this.switchChessColor();
            }
        }).setNegativeButton(getBaseContext().getResources().getString(R.string.negative), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        }).show();
    }

    /* access modifiers changed from: protected */
    public void switchChessColor() {
        if (this.side_user == 0) {
            this.side_user = 8;
            this.side_cpu = 0;
        } else {
            this.side_user = 0;
            this.side_cpu = 8;
        }
        newGame();
    }

    /* access modifiers changed from: protected */
    public void showLevelDialog() {
        new AlertDialog.Builder(this).setIcon((int) R.drawable.menu_level).setTitle((int) R.string.level_choose).setItems((int) R.array.LevelList, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                ChessBoard.this.showLevelCheck(i + 1);
            }
        }).show();
    }

    /* access modifiers changed from: protected */
    public void showLevelCheck(int l) {
        this.tmp = l;
        new AlertDialog.Builder(this).setMessage(getBaseContext().getResources().getString(R.string.level_choose_check)).setCancelable(false).setPositiveButton(getBaseContext().getResources().getString(R.string.positive), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ChessBoard.this.level = ChessBoard.this.tmp;
                ChessBoard.this.newGame();
            }
        }).setNegativeButton(getBaseContext().getResources().getString(R.string.negative), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        }).show();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private void init() {
        AppConnect.getInstance(this);
        WiGame.init(this, appKey, secretKey, "1.0", false, false);
        WiGame.addWiGameClient(this.mClient);
        this.GameSettings = getPreferences(0);
        this.best = this.GameSettings.getInt(BEST, 0);
        this.level = this.GameSettings.getInt(LEVEL, 3);
        this.side_user = 0;
        this.side_cpu = 8;
        this.Width = (float) CYManager.getScreenSize(this).nWidth;
        this.Height = (float) CYManager.getScreenSize(this).nHeight;
        this.info_board_heigt = INFO_BOARD_HEIGHT * (this.Width / BASE_WIDTH);
        Bitmap sawBackGroud = ((BitmapDrawable) getResources().getDrawable(R.drawable.bg_base)).getBitmap();
        Matrix matrix = new Matrix();
        matrix.postScale(this.Width / ((float) sawBackGroud.getWidth()), (this.Height - this.info_board_heigt) / ((float) sawBackGroud.getHeight()));
        this.useBackGroud = Bitmap.createBitmap(sawBackGroud, 0, 0, sawBackGroud.getWidth(), sawBackGroud.getHeight(), matrix, true);
        this.board_v_fence_width = BOARD_V_FENCE_WIDTH * (((float) this.useBackGroud.getWidth()) / ((float) sawBackGroud.getWidth()));
        this.board_h_fence_width = BOARD_H_FENCE_WIDTH * (((float) this.useBackGroud.getHeight()) / ((float) sawBackGroud.getHeight()));
        this.GridUnit = (this.Width - (2.0f * this.board_v_fence_width)) / 8.0f;
        this.pGrid = new Paint(1);
        this.pGrid.setColor(getResources().getColor(R.color.grid_line));
        this.info_board_unit = this.Width / 20.0f;
        Bitmap sawHead = ((BitmapDrawable) getResources().getDrawable(R.drawable.head)).getBitmap();
        Matrix matrix2 = new Matrix();
        matrix2.postScale(this.Width / ((float) sawHead.getWidth()), this.info_board_heigt / ((float) sawHead.getHeight()));
        this.useHead = Bitmap.createBitmap(sawHead, 0, 0, sawHead.getWidth(), sawHead.getHeight(), matrix2, true);
        Bitmap sawInfoChess = ((BitmapDrawable) getResources().getDrawable(R.drawable.black)).getBitmap();
        Matrix matrix3 = new Matrix();
        matrix3.postScale((this.info_board_heigt - (2.0f * this.info_board_unit)) / ((float) sawInfoChess.getWidth()), (this.info_board_heigt - (2.0f * this.info_board_unit)) / ((float) sawInfoChess.getHeight()));
        this.useBlack = Bitmap.createBitmap(sawInfoChess, 0, 0, sawInfoChess.getWidth(), sawInfoChess.getHeight(), matrix3, true);
        Bitmap sawInfoChess2 = ((BitmapDrawable) getResources().getDrawable(R.drawable.white)).getBitmap();
        this.useWhite = Bitmap.createBitmap(sawInfoChess2, 0, 0, sawInfoChess2.getWidth(), sawInfoChess2.getHeight(), matrix3, true);
        Bitmap sawTag = ((BitmapDrawable) getResources().getDrawable(R.drawable.tag)).getBitmap();
        this.useTag = Bitmap.createBitmap(sawTag, 0, 0, sawTag.getWidth(), sawTag.getHeight(), matrix3, true);
        this.pInfoName = new Paint(1);
        this.pInfoName.setColor(getResources().getColor(R.color.info_name));
        this.pInfoName.setTextSize(25.0f * (this.Width / BASE_WIDTH));
        this.pInfoName.setTypeface(Typeface.DEFAULT_BOLD);
        this.pCountBlack = new Paint(1);
        this.pCountBlack.setColor(getResources().getColor(R.color.count_black));
        this.pCountBlack.setTextSize(25.0f * (this.Width / BASE_WIDTH));
        this.pCountBlack.setTextAlign(Paint.Align.CENTER);
        this.pCountBlack.setTypeface(Typeface.DEFAULT_BOLD);
        this.pCountWhite = new Paint(1);
        this.pCountWhite.setColor(getResources().getColor(R.color.count_white));
        this.pCountWhite.setTextSize(25.0f * (this.Width / BASE_WIDTH));
        this.pCountWhite.setTextAlign(Paint.Align.CENTER);
        this.pCountWhite.setTypeface(Typeface.DEFAULT_BOLD);
        this.useChess = new Bitmap[9];
        Bitmap chessGroup = ((BitmapDrawable) getResources().getDrawable(R.drawable.chess)).getBitmap();
        Matrix matrix4 = new Matrix();
        matrix4.postScale(this.GridUnit / (((float) chessGroup.getWidth()) / 3.0f), this.GridUnit / (((float) chessGroup.getWidth()) / 3.0f));
        int count = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                this.useChess[count] = Bitmap.createBitmap(chessGroup, (int) (((float) j) * (((float) chessGroup.getWidth()) / 3.0f)), (int) (((float) i) * (((float) chessGroup.getWidth()) / 3.0f)), (int) (((float) chessGroup.getWidth()) / 3.0f), (int) (((float) chessGroup.getWidth()) / 3.0f), matrix4, true);
                count++;
            }
        }
        Bitmap sawCan = ((BitmapDrawable) getResources().getDrawable(R.drawable.possible_move)).getBitmap();
        this.useCan = Bitmap.createBitmap(sawCan, 0, 0, sawCan.getWidth(), sawCan.getHeight(), matrix4, true);
        this.backup = (int[][]) Array.newInstance(Integer.TYPE, 8, 8);
        this.handler = new UIHandler(this);
        this.ChessCount = new int[9];
        this.soundPool = new SoundPool(10, 1, 5);
        this.soundWin = this.soundPool.load(this, R.raw.win, 0);
        this.soundLost = this.soundPool.load(this, R.raw.lost, 0);
        this.soundClick = this.soundPool.load(this, R.raw.click, 0);
        this.soundFlip = this.soundPool.load(this, R.raw.flip, 0);
    }

    /* access modifiers changed from: protected */
    public void playSound(int sound) {
        if (this.isMute >= 0) {
            switch (sound) {
                case 0:
                    this.soundPool.play(this.soundWin, 1.0f, 1.0f, 0, 0, 1.0f);
                    return;
                case 1:
                    this.soundPool.play(this.soundLost, 1.0f, 1.0f, 0, 0, 1.0f);
                    return;
                case 2:
                    this.soundPool.play(this.soundClick, 1.0f, 1.0f, 0, 0, 1.0f);
                    return;
                case 3:
                    this.soundPool.play(this.soundFlip, 1.0f, 1.0f, 0, 0, 1.0f);
                    return;
                default:
                    return;
            }
        }
    }

    private void loadViews() {
        setContentView((int) R.layout.main);
        this.ibv = (InfoBoardView) findViewById(R.id.ibv);
        this.cv = (ChessView) findViewById(R.id.cv);
        this.wapsView = (LinearLayout) findViewById(R.id.AdLinearLayout);
        this.wapsView.setVisibility(0);
        this.wapsView.addView(new AdGroupLayout(this));
    }

    /* access modifiers changed from: protected */
    public void newGame() {
        Log.d("trace", "new game");
        this.moveSide = 0;
        if (this.side_user == 0) {
            this.black_name = getBaseContext().getResources().getString(R.string.info_name_you);
            this.white_name = String.valueOf(getBaseContext().getResources().getString(R.string.info_name_com)) + this.level;
        } else {
            this.black_name = String.valueOf(getBaseContext().getResources().getString(R.string.info_name_com)) + this.level;
            this.white_name = getBaseContext().getResources().getString(R.string.info_name_you);
        }
        float xOffset = this.board_v_fence_width;
        float yOffset = this.board_h_fence_width;
        this.grids = (Grid[][]) Array.newInstance(Grid.class, 8, 8);
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                this.grids[i][j] = new Grid(i, j, this.GridUnit, xOffset, yOffset);
            }
        }
        this.grids[3][3].status = 0;
        this.grids[3][4].status = 8;
        this.grids[4][3].status = 8;
        this.grids[4][4].status = 0;
        countChess();
        setPossible();
        this.cv.refresh();
        this.ibv.refresh();
        if (this.side_cpu == 0) {
            doAI();
        }
    }

    /* access modifiers changed from: protected */
    public void endGame() {
        if (this.ChessCount[this.side_user] > this.ChessCount[this.side_cpu]) {
            playSound(0);
            Toast.makeText(getApplicationContext(), getBaseContext().getResources().getString(R.string.win), 1).show();
        } else {
            playSound(1);
            Toast.makeText(getApplicationContext(), getBaseContext().getResources().getString(R.string.lost), 1).show();
        }
        unlockAchievement();
        submitScore();
    }

    private void submitScore() {
        if (this.mCompetitorScore >= 0) {
            WiGame.submitChallengeResult(this.mChallengeToUserId, (this.ChessCount[this.side_user] - this.ChessCount[this.side_cpu]) - this.mCompetitorScore, this.ChessCount[this.side_user], null);
            this.mCompetitorScore = -1;
        }
        unlockAchievement();
        setBest(this.ChessCount[this.side_user] - this.ChessCount[this.side_cpu]);
        showSubmitCheckDialog();
    }

    /* access modifiers changed from: protected */
    public void showSubmitCheckDialog() {
        new AlertDialog.Builder(this).setMessage(getBaseContext().getResources().getString(R.string.submit_check)).setCancelable(false).setPositiveButton(getBaseContext().getResources().getString(R.string.positive), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                WiGame.submitScore(ChessBoard.leaderboardId[ChessBoard.this.level - 1], ChessBoard.this.ChessCount[ChessBoard.this.side_user] - ChessBoard.this.ChessCount[ChessBoard.this.side_cpu], null, false);
                ChessBoard.this.newGame();
            }
        }).setNegativeButton(getBaseContext().getResources().getString(R.string.negative), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ChessBoard.this.newGame();
            }
        }).show();
    }

    /* access modifiers changed from: protected */
    public void showExitDialog() {
        new AlertDialog.Builder(this).setMessage(getBaseContext().getResources().getString(R.string.exit)).setCancelable(true).setPositiveButton(getBaseContext().getResources().getString(R.string.positive), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ChessBoard.this.finish();
            }
        }).setNeutralButton(getBaseContext().getResources().getString(R.string.rate), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ChessBoard.this.rateit();
            }
        }).setNegativeButton(getBaseContext().getResources().getString(R.string.menu_more), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ChessBoard.this.more();
            }
        }).show();
    }

    private void setBest(int value) {
        SharedPreferences.Editor editor = this.GameSettings.edit();
        if (value > this.best) {
            this.best = value;
            editor.putInt(BEST, this.best);
            editor.commit();
        }
    }

    private int getVSSide() {
        if (this.moveSide == this.side_user) {
            return this.side_cpu;
        }
        return this.side_user;
    }

    private void backup() {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                this.backup[i][j] = this.grids[i][j].status;
            }
        }
    }

    private void restore() {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                this.grids[i][j].status = this.backup[i][j];
            }
        }
    }

    /* access modifiers changed from: protected */
    public int takeMove(Position pos, int side) {
        backup();
        if (side == this.side_user) {
            return GetKills(pos, side, true);
        }
        TryPut(this.side_cpu, this.level);
        GetPos(this.side_cpu, null);
        if (!doEffect()) {
            doSwitch();
            this.ibv.refresh();
            this.cv.refresh();
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public int GetKills(Position pos, int side, boolean isKill) {
        int count = 0;
        if (-1 != this.grids[pos.row][pos.col].status && CHESS_CAN != this.grids[pos.row][pos.col].status) {
            return 0;
        }
        for (int dirx = -1; dirx < 2; dirx++) {
            for (int diry = -1; diry < 2; diry++) {
                if (!(dirx == 0 && diry == 0) && pos.row + diry >= 0 && pos.row + diry < 8 && pos.col + dirx >= 0 && pos.col + dirx < 8 && this.grids[pos.row + diry][pos.col + dirx].status == getVSSide()) {
                    int temp = 0 + 1;
                    int i = pos.row + (diry * 2);
                    int j = pos.col + (dirx * 2);
                    while (true) {
                        if (i < 0 || j < 0 || i >= 8 || j >= 8) {
                            break;
                        } else if (this.grids[i][j].status == getVSSide()) {
                            temp++;
                            i += diry;
                            j += dirx;
                        } else if (this.grids[i][j].status == side) {
                            count += temp;
                            Log.d("trace", "count: " + count);
                            if (isKill) {
                                int m = pos.row + diry;
                                int n = pos.col + dirx;
                                while (m <= pos.row + temp && m >= pos.row - temp && n <= pos.col + temp && n >= pos.col - temp) {
                                    this.grids[m][n].status = side;
                                    m += diry;
                                    n += dirx;
                                }
                            }
                        }
                    }
                }
            }
        }
        if (isKill && count > 0) {
            this.grids[pos.row][pos.col].status = side;
        }
        return count;
    }

    /* access modifiers changed from: protected */
    public int TryPut(int side, int depth) {
        int[] goal = new int[64];
        int[][] point = (int[][]) Array.newInstance(Integer.TYPE, 64, 2);
        if (depth == 0) {
            return 0;
        }
        int N = GetPos(side, point);
        for (int i = 0; i < N; i++) {
            goal[i] = GetKills(new Position(point[i][0], point[i][1]), side, true) - TryPut(getVSSide(), depth - 1);
            if ((point[i][0] == 0 && point[i][1] == 0) || ((point[i][0] == 7 && point[i][1] == 7) || ((point[i][0] == 7 && point[i][1] == 0) || (point[i][0] == 0 && point[i][1] == 7)))) {
                goal[i] = goal[i] + 10;
            }
            if (point[i][0] == 0 || point[i][1] == 0 || point[i][0] == 7 || point[i][1] == 7) {
                goal[i] = goal[i] + 5;
            }
            restore();
        }
        int maxi = 0;
        for (int i2 = 1; i2 < N; i2++) {
            if (goal[i2] > goal[maxi]) {
                maxi = i2;
            }
        }
        if (depth == this.level && N > 0) {
            GetKills(new Position(point[maxi][0], point[maxi][1]), side, true);
        }
        return goal[maxi];
    }

    /* access modifiers changed from: protected */
    public int GetPos(int side, int[][] point) {
        int count = 0;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (GetKills(new Position(i, j), side, false) > 0) {
                    if (point != null) {
                        point[count][0] = i;
                        point[count][1] = j;
                    }
                    count++;
                }
            }
        }
        return count;
    }

    /* access modifiers changed from: protected */
    public boolean doEffect() {
        int op;
        int saw = getVSSide();
        int target = this.moveSide;
        if (saw == 0) {
            op = 1;
        } else {
            op = -1;
        }
        int saw2 = saw + op;
        int flag = 0;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (this.backup[i][j] != this.grids[i][j].status) {
                    if (-1 == this.backup[i][j] || CHESS_CAN == this.backup[i][j]) {
                        this.grids[i][j].status = target;
                        flag++;
                    } else {
                        this.grids[i][j].status = this.backup[i][j] + op;
                    }
                }
            }
        }
        if (flag == 0) {
            return false;
        }
        this.isLocked = true;
        playSound(2);
        new Thread(new ShowFactory.PlayAnimat(this, saw2, target, op)).start();
        return true;
    }

    /* access modifiers changed from: protected */
    public void doSwitch() {
        Log.d("trace", "doSwitch() now:" + this.moveSide);
        if (this.moveSide == this.side_user) {
            this.moveSide = this.side_cpu;
            doAI();
        } else {
            this.moveSide = this.side_user;
        }
        countChess();
        setPossible();
    }

    /* access modifiers changed from: protected */
    public void doAI() {
        new Thread(new Robot(this)).start();
    }

    /* access modifiers changed from: protected */
    public void debug() {
        String[] values = new String[8];
        for (int i = 0; i < 8; i++) {
            values[i] = new String();
        }
        for (int i2 = 0; i2 < 8; i2++) {
            for (int j = 0; j < 8; j++) {
                values[i2] = String.valueOf(values[i2]) + this.grids[i2][j].status + " ";
            }
        }
        Log.d("trace", "values:");
        for (int i3 = 0; i3 < 8; i3++) {
            Log.d("trace", values[i3]);
        }
    }

    /* access modifiers changed from: protected */
    public void countChess() {
        for (int i = 0; i < 9; i++) {
            this.ChessCount[i] = 0;
        }
        for (int i2 = 0; i2 < 8; i2++) {
            for (int j = 0; j < 8; j++) {
                if (this.grids[i2][j].status == 0) {
                    int[] iArr = this.ChessCount;
                    iArr[0] = iArr[0] + 1;
                } else if (8 == this.grids[i2][j].status) {
                    int[] iArr2 = this.ChessCount;
                    iArr2[8] = iArr2[8] + 1;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void setPossible() {
        if (this.moveSide != this.side_cpu) {
            recoverPossible();
            Log.d("trace", "setPossible()");
            debug();
            int count = 0;
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    if (-1 == this.grids[i][j].status) {
                        int tmp2 = GetKills(new Position(i, j), this.moveSide, false);
                        if (tmp2 > 0) {
                            Log.d("trace", "setPossible(" + i + "," + j + ") " + tmp2);
                        }
                        if (tmp2 > 0) {
                            count += tmp2;
                            this.grids[i][j].status = CHESS_CAN;
                        }
                    }
                }
            }
            Log.d("trace", "setPossible() finish" + count);
            if (count == 0) {
                Message message = new Message();
                message.what = 2;
                this.handler.sendMessage(message);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void recoverPossible() {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (CHESS_CAN == this.grids[i][j].status) {
                    this.grids[i][j].status = -1;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        WiGame.destroy(this);
        AppConnect.getInstance(this).finalize();
        super.onDestroy();
    }

    private void unlockAchievement() {
        if (this.ChessCount[this.side_user] >= 32) {
            WiGame.unlockAchievement(achievementIds[0]);
        }
        if (this.ChessCount[this.side_user] >= 55) {
            WiGame.unlockAchievement(achievementIds[1]);
        }
        if (this.ChessCount[this.side_user] >= 64) {
            WiGame.unlockAchievement(achievementIds[2]);
        }
    }

    public static class Robot implements Runnable {
        private ChessBoard parent = null;
        private int span = 50;

        public Robot(ChessBoard p) {
            this.parent = p;
        }

        public void run() {
            try {
                Thread.sleep((long) this.span);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Log.d("trace", "doAI");
            this.parent.takeMove(null, this.parent.moveSide);
        }
    }
}
