package com.saubcy.games.othello.market;

import android.content.Context;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.saubcy.games.othello.market.ShowFactory;

public class InfoBoardView extends SurfaceView implements SurfaceHolder.Callback {
    protected SurfaceHolder holder = null;
    protected ChessBoard parent = null;

    public InfoBoardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.parent = (ChessBoard) context;
        init();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int wMeasureSpec, int hMeasureSpec) {
        setMeasuredDimension((int) this.parent.Width, (int) this.parent.info_board_heigt);
    }

    public void surfaceCreated(SurfaceHolder holder2) {
        refresh();
    }

    public void surfaceChanged(SurfaceHolder holder2, int format, int width, int height) {
    }

    public void surfaceDestroyed(SurfaceHolder holder2) {
    }

    private void init() {
        this.holder = getHolder();
        this.holder.addCallback(this);
    }

    /* access modifiers changed from: protected */
    public void refresh() {
        new Thread(new ShowFactory.InfoBoardBuild(this.parent)).start();
    }
}
