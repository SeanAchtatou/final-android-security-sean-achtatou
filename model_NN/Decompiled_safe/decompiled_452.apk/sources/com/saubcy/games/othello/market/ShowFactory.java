package com.saubcy.games.othello.market;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.SurfaceHolder;
import com.waps.AnimationType;

public class ShowFactory {
    protected static void drawInfoBoard(Canvas canvas, ChessBoard cb) {
        float left;
        canvas.drawBitmap(cb.useHead, 0.0f, 0.0f, (Paint) null);
        float left2 = cb.info_board_unit;
        float top = cb.info_board_unit;
        canvas.drawBitmap(cb.useBlack, left2, top, (Paint) null);
        canvas.drawText(new StringBuilder(String.valueOf(cb.ChessCount[0])).toString(), (((float) cb.useBlack.getWidth()) / 2.0f) + left2, ((((float) cb.useBlack.getHeight()) * 2.0f) / 3.0f) + top, cb.pCountBlack);
        float left3 = left2 + cb.info_board_unit;
        canvas.drawText(cb.black_name, (((float) cb.useBlack.getWidth()) + left3) - ((float) cb.useTag.getWidth()), top + ((((float) cb.useBlack.getHeight()) * 2.0f) / 3.0f), cb.pInfoName);
        float left4 = 10.0f * cb.info_board_unit;
        float top2 = cb.info_board_unit;
        canvas.drawBitmap(cb.useWhite, left4, top2, (Paint) null);
        canvas.drawText(new StringBuilder(String.valueOf(cb.ChessCount[8])).toString(), (((float) cb.useWhite.getWidth()) / 2.0f) + left4, ((((float) cb.useWhite.getHeight()) * 2.0f) / 3.0f) + top2, cb.pCountWhite);
        float left5 = left4 + cb.info_board_unit;
        canvas.drawText(cb.white_name, (((float) cb.useWhite.getWidth()) + left5) - ((float) cb.useTag.getWidth()), top2 + ((((float) cb.useBlack.getHeight()) * 2.0f) / 3.0f), cb.pInfoName);
        if (cb.moveSide == 0) {
            left = cb.info_board_unit - ((float) cb.useTag.getWidth());
        } else {
            left = (cb.info_board_unit * 10.0f) - ((float) cb.useTag.getWidth());
        }
        canvas.drawBitmap(cb.useTag, left, (cb.info_board_heigt / 2.0f) - (((float) cb.useTag.getHeight()) / 2.0f), (Paint) null);
    }

    protected static void drawChessBoard(Canvas canvas, ChessBoard cb) {
        canvas.drawBitmap(cb.useBackGroud, 0.0f, 0.0f, (Paint) null);
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                drawGrid(canvas, cb, cb.grids[i][j]);
            }
        }
    }

    protected static void drawGrid(Canvas canvas, ChessBoard cb, Grid grid) {
        float left = grid.left;
        canvas.drawRect(left, grid.top, left + 0.5f, grid.bottom, cb.pGrid);
        float left2 = grid.left;
        float top = grid.top;
        canvas.drawRect(left2, top, grid.right, top + 0.5f, cb.pGrid);
        canvas.drawRect(grid.right - 0.5f, grid.top, grid.right, grid.bottom, cb.pGrid);
        canvas.drawRect(grid.left, grid.bottom - 0.5f, grid.right, grid.bottom, cb.pGrid);
        switch (grid.status) {
            case AnimationType.NONE /*-1*/:
                return;
            case 100:
                if (cb.moveSide == cb.side_user) {
                    canvas.drawBitmap(cb.useCan, grid.left, grid.top, (Paint) null);
                    return;
                }
                return;
            default:
                canvas.drawBitmap(cb.useChess[grid.status], grid.left, grid.top, (Paint) null);
                return;
        }
    }

    public static class InfoBoardBuild implements Runnable {
        ChessBoard parent = null;
        SurfaceHolder surfaceHolder;

        public InfoBoardBuild(ChessBoard p) {
            this.parent = p;
            this.surfaceHolder = this.parent.ibv.holder;
        }

        public void run() {
            Canvas canvas = null;
            try {
                canvas = this.surfaceHolder.lockCanvas(null);
                synchronized (this.surfaceHolder) {
                    ShowFactory.drawInfoBoard(canvas, this.parent);
                }
                if (canvas != null) {
                    this.surfaceHolder.unlockCanvasAndPost(canvas);
                }
            } catch (Exception e) {
                try {
                    e.printStackTrace();
                } finally {
                    if (canvas != null) {
                        this.surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                }
            }
        }
    }

    public static class ChessBoardBuild implements Runnable {
        private ChessBoard parent = null;
        private SurfaceHolder surfaceHolder;

        public ChessBoardBuild(ChessBoard p) {
            this.parent = p;
            this.surfaceHolder = this.parent.cv.holder;
        }

        public void run() {
            Canvas canvas = null;
            try {
                canvas = this.surfaceHolder.lockCanvas(null);
                synchronized (this.surfaceHolder) {
                    ShowFactory.drawChessBoard(canvas, this.parent);
                }
                if (canvas != null) {
                    this.surfaceHolder.unlockCanvasAndPost(canvas);
                }
            } catch (Exception e) {
                try {
                    e.printStackTrace();
                } finally {
                    if (canvas != null) {
                        this.surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                }
            }
        }
    }

    public static class PlayAnimat implements Runnable {
        private static final int soundCount = 4;
        private int op = 0;
        private ChessBoard parent = null;
        private int saw = -1;
        private int span = 50;
        private int target = -1;

        public PlayAnimat(ChessBoard p, int saw2, int target2, int op2) {
            this.saw = saw2;
            this.parent = p;
            this.target = target2;
            this.op = op2;
        }

        /* JADX WARNING: Removed duplicated region for block: B:40:0x00da  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r13 = this;
                r12 = 100
                r11 = -1
                r10 = 1
                r6 = 0
                r9 = 8
                r0 = 0
                r4 = 0
                android.os.Message r4 = new android.os.Message
                r4.<init>()
                r4.what = r6
                com.saubcy.games.othello.market.ChessBoard r6 = r13.parent
                com.saubcy.games.othello.market.UIHandler r6 = r6.handler
                r6.sendMessage(r4)
                r5 = r4
            L_0x0018:
                int r6 = r13.saw     // Catch:{ InterruptedException -> 0x00ac }
                int r7 = r13.target     // Catch:{ InterruptedException -> 0x00ac }
                int r8 = r13.op     // Catch:{ InterruptedException -> 0x00ac }
                int r7 = r7 + r8
                if (r6 != r7) goto L_0x0037
                android.os.Message r4 = new android.os.Message
                r4.<init>()
                r4.what = r10
                com.saubcy.games.othello.market.ChessBoard r6 = r13.parent
                com.saubcy.games.othello.market.UIHandler r6 = r6.handler
                r6.sendMessage(r4)
                java.lang.String r6 = "trace"
                java.lang.String r7 = "finally"
                android.util.Log.d(r6, r7)
            L_0x0036:
                return
            L_0x0037:
                int r6 = r13.span     // Catch:{ InterruptedException -> 0x00ac }
                long r6 = (long) r6     // Catch:{ InterruptedException -> 0x00ac }
                java.lang.Thread.sleep(r6)     // Catch:{ InterruptedException -> 0x00ac }
                r2 = 0
            L_0x003e:
                if (r2 < r9) goto L_0x0063
                int r6 = r13.saw     // Catch:{ InterruptedException -> 0x00ac }
                int r7 = r13.op     // Catch:{ InterruptedException -> 0x00ac }
                int r6 = r6 + r7
                r13.saw = r6     // Catch:{ InterruptedException -> 0x00ac }
                android.os.Message r4 = new android.os.Message     // Catch:{ InterruptedException -> 0x00ac }
                r4.<init>()     // Catch:{ InterruptedException -> 0x00ac }
                r6 = 0
                r4.what = r6     // Catch:{ InterruptedException -> 0x013a }
                com.saubcy.games.othello.market.ChessBoard r6 = r13.parent     // Catch:{ InterruptedException -> 0x013a }
                com.saubcy.games.othello.market.UIHandler r6 = r6.handler     // Catch:{ InterruptedException -> 0x013a }
                r6.sendMessage(r4)     // Catch:{ InterruptedException -> 0x013a }
                int r0 = r0 + 1
                r6 = 4
                if (r6 != r0) goto L_0x0061
                com.saubcy.games.othello.market.ChessBoard r6 = r13.parent     // Catch:{ InterruptedException -> 0x013a }
                r7 = 3
                r6.playSound(r7)     // Catch:{ InterruptedException -> 0x013a }
            L_0x0061:
                r5 = r4
                goto L_0x0018
            L_0x0063:
                r3 = 0
            L_0x0064:
                if (r3 < r9) goto L_0x0069
                int r2 = r2 + 1
                goto L_0x003e
            L_0x0069:
                com.saubcy.games.othello.market.ChessBoard r6 = r13.parent     // Catch:{ InterruptedException -> 0x00ac }
                com.saubcy.games.othello.market.Grid[][] r6 = r6.grids     // Catch:{ InterruptedException -> 0x00ac }
                r6 = r6[r2]     // Catch:{ InterruptedException -> 0x00ac }
                r6 = r6[r3]     // Catch:{ InterruptedException -> 0x00ac }
                int r6 = r6.status     // Catch:{ InterruptedException -> 0x00ac }
                if (r6 == 0) goto L_0x0099
                com.saubcy.games.othello.market.ChessBoard r6 = r13.parent     // Catch:{ InterruptedException -> 0x00ac }
                com.saubcy.games.othello.market.Grid[][] r6 = r6.grids     // Catch:{ InterruptedException -> 0x00ac }
                r6 = r6[r2]     // Catch:{ InterruptedException -> 0x00ac }
                r6 = r6[r3]     // Catch:{ InterruptedException -> 0x00ac }
                int r6 = r6.status     // Catch:{ InterruptedException -> 0x00ac }
                if (r9 == r6) goto L_0x0099
                com.saubcy.games.othello.market.ChessBoard r6 = r13.parent     // Catch:{ InterruptedException -> 0x00ac }
                com.saubcy.games.othello.market.Grid[][] r6 = r6.grids     // Catch:{ InterruptedException -> 0x00ac }
                r6 = r6[r2]     // Catch:{ InterruptedException -> 0x00ac }
                r6 = r6[r3]     // Catch:{ InterruptedException -> 0x00ac }
                int r6 = r6.status     // Catch:{ InterruptedException -> 0x00ac }
                if (r11 == r6) goto L_0x0099
                com.saubcy.games.othello.market.ChessBoard r6 = r13.parent     // Catch:{ InterruptedException -> 0x00ac }
                com.saubcy.games.othello.market.Grid[][] r6 = r6.grids     // Catch:{ InterruptedException -> 0x00ac }
                r6 = r6[r2]     // Catch:{ InterruptedException -> 0x00ac }
                r6 = r6[r3]     // Catch:{ InterruptedException -> 0x00ac }
                int r6 = r6.status     // Catch:{ InterruptedException -> 0x00ac }
                if (r12 != r6) goto L_0x009c
            L_0x0099:
                int r3 = r3 + 1
                goto L_0x0064
            L_0x009c:
                com.saubcy.games.othello.market.ChessBoard r6 = r13.parent     // Catch:{ InterruptedException -> 0x00ac }
                com.saubcy.games.othello.market.Grid[][] r6 = r6.grids     // Catch:{ InterruptedException -> 0x00ac }
                r6 = r6[r2]     // Catch:{ InterruptedException -> 0x00ac }
                r6 = r6[r3]     // Catch:{ InterruptedException -> 0x00ac }
                int r7 = r6.status     // Catch:{ InterruptedException -> 0x00ac }
                int r8 = r13.op     // Catch:{ InterruptedException -> 0x00ac }
                int r7 = r7 + r8
                r6.status = r7     // Catch:{ InterruptedException -> 0x00ac }
                goto L_0x0099
            L_0x00ac:
                r6 = move-exception
                r1 = r6
            L_0x00ae:
                r1.printStackTrace()     // Catch:{ all -> 0x0120 }
                r2 = 0
            L_0x00b2:
                if (r2 < r9) goto L_0x00da
                android.os.Message r4 = new android.os.Message     // Catch:{ all -> 0x0120 }
                r4.<init>()     // Catch:{ all -> 0x0120 }
                r6 = 0
                r4.what = r6     // Catch:{ all -> 0x0138 }
                com.saubcy.games.othello.market.ChessBoard r6 = r13.parent     // Catch:{ all -> 0x0138 }
                com.saubcy.games.othello.market.UIHandler r6 = r6.handler     // Catch:{ all -> 0x0138 }
                r6.sendMessage(r4)     // Catch:{ all -> 0x0138 }
                android.os.Message r4 = new android.os.Message
                r4.<init>()
                r4.what = r10
                com.saubcy.games.othello.market.ChessBoard r6 = r13.parent
                com.saubcy.games.othello.market.UIHandler r6 = r6.handler
                r6.sendMessage(r4)
                java.lang.String r6 = "trace"
                java.lang.String r7 = "finally"
                android.util.Log.d(r6, r7)
                goto L_0x0036
            L_0x00da:
                r3 = 0
            L_0x00db:
                if (r3 < r9) goto L_0x00e0
                int r2 = r2 + 1
                goto L_0x00b2
            L_0x00e0:
                com.saubcy.games.othello.market.ChessBoard r6 = r13.parent     // Catch:{ all -> 0x0120 }
                com.saubcy.games.othello.market.Grid[][] r6 = r6.grids     // Catch:{ all -> 0x0120 }
                r6 = r6[r2]     // Catch:{ all -> 0x0120 }
                r6 = r6[r3]     // Catch:{ all -> 0x0120 }
                int r6 = r6.status     // Catch:{ all -> 0x0120 }
                if (r6 == 0) goto L_0x0110
                com.saubcy.games.othello.market.ChessBoard r6 = r13.parent     // Catch:{ all -> 0x0120 }
                com.saubcy.games.othello.market.Grid[][] r6 = r6.grids     // Catch:{ all -> 0x0120 }
                r6 = r6[r2]     // Catch:{ all -> 0x0120 }
                r6 = r6[r3]     // Catch:{ all -> 0x0120 }
                int r6 = r6.status     // Catch:{ all -> 0x0120 }
                if (r9 == r6) goto L_0x0110
                com.saubcy.games.othello.market.ChessBoard r6 = r13.parent     // Catch:{ all -> 0x0120 }
                com.saubcy.games.othello.market.Grid[][] r6 = r6.grids     // Catch:{ all -> 0x0120 }
                r6 = r6[r2]     // Catch:{ all -> 0x0120 }
                r6 = r6[r3]     // Catch:{ all -> 0x0120 }
                int r6 = r6.status     // Catch:{ all -> 0x0120 }
                if (r11 == r6) goto L_0x0110
                com.saubcy.games.othello.market.ChessBoard r6 = r13.parent     // Catch:{ all -> 0x0120 }
                com.saubcy.games.othello.market.Grid[][] r6 = r6.grids     // Catch:{ all -> 0x0120 }
                r6 = r6[r2]     // Catch:{ all -> 0x0120 }
                r6 = r6[r3]     // Catch:{ all -> 0x0120 }
                int r6 = r6.status     // Catch:{ all -> 0x0120 }
                if (r12 != r6) goto L_0x0113
            L_0x0110:
                int r3 = r3 + 1
                goto L_0x00db
            L_0x0113:
                com.saubcy.games.othello.market.ChessBoard r6 = r13.parent     // Catch:{ all -> 0x0120 }
                com.saubcy.games.othello.market.Grid[][] r6 = r6.grids     // Catch:{ all -> 0x0120 }
                r6 = r6[r2]     // Catch:{ all -> 0x0120 }
                r6 = r6[r3]     // Catch:{ all -> 0x0120 }
                int r7 = r13.target     // Catch:{ all -> 0x0120 }
                r6.status = r7     // Catch:{ all -> 0x0120 }
                goto L_0x0110
            L_0x0120:
                r6 = move-exception
                r4 = r5
            L_0x0122:
                android.os.Message r4 = new android.os.Message
                r4.<init>()
                r4.what = r10
                com.saubcy.games.othello.market.ChessBoard r7 = r13.parent
                com.saubcy.games.othello.market.UIHandler r7 = r7.handler
                r7.sendMessage(r4)
                java.lang.String r7 = "trace"
                java.lang.String r8 = "finally"
                android.util.Log.d(r7, r8)
                throw r6
            L_0x0138:
                r6 = move-exception
                goto L_0x0122
            L_0x013a:
                r6 = move-exception
                r1 = r6
                r5 = r4
                goto L_0x00ae
            */
            throw new UnsupportedOperationException("Method not decompiled: com.saubcy.games.othello.market.ShowFactory.PlayAnimat.run():void");
        }
    }
}
