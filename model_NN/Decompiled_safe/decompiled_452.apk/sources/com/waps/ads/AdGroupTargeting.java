package com.waps.ads;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;

public class AdGroupTargeting {
    private static boolean a = false;
    private static g b;
    private static GregorianCalendar c;
    private static String d;
    private static String e;
    private static Set f;

    static {
        resetData();
    }

    public static void addKeyword(String str) {
        if (f == null) {
            f = new HashSet();
        }
        f.add(str);
    }

    public static int getAge() {
        if (c != null) {
            return Calendar.getInstance().get(1) - c.get(1);
        }
        return -1;
    }

    public static GregorianCalendar getBirthDate() {
        return c;
    }

    public static g getGender() {
        return b;
    }

    public static Set getKeywordSet() {
        return f;
    }

    public static String getKeywords() {
        return e;
    }

    public static String getPostalCode() {
        return d;
    }

    public static boolean getTestMode() {
        return a;
    }

    public static void resetData() {
        a = false;
        b = g.UNKNOWN;
        c = null;
        d = null;
        e = null;
        f = null;
    }

    public static void setAge(int i) {
        c = new GregorianCalendar(Calendar.getInstance().get(1) - i, 0, 1);
    }

    public static void setBirthDate(GregorianCalendar gregorianCalendar) {
        c = gregorianCalendar;
    }

    public static void setGender(g gVar) {
        b = gVar == null ? g.UNKNOWN : gVar;
    }

    public static void setKeywordSet(Set set) {
        f = set;
    }

    public static void setKeywords(String str) {
        e = str;
    }

    public static void setPostalCode(String str) {
        d = str;
    }

    public static void setTestMode(boolean z) {
        a = z;
        if (a) {
            AdGroupManager.setConfigExpireTimeout(-1);
        }
    }
}
