package com.waps.ads.adapters;

import android.graphics.Color;
import android.util.Log;
import android.view.ViewGroup;
import com.waps.ads.AdGroupLayout;
import com.waps.ads.AdGroupTargeting;
import com.waps.ads.a.a;
import com.waps.ads.b.b;
import com.waps.ads.b.c;
import com.waps.ads.f;
import com.wiyun.ad.AdView;

public class WiyunAdapter extends a implements AdView.AdListener {
    private AdView a = null;

    public WiyunAdapter(AdGroupLayout adGroupLayout, c cVar) {
        super(adGroupLayout, cVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.waps.ads.f.<init>(com.waps.ads.AdGroupLayout, android.view.ViewGroup):void
     arg types: [com.waps.ads.AdGroupLayout, com.wiyun.ad.AdView]
     candidates:
      com.waps.ads.f.<init>(com.waps.ads.AdGroupLayout, android.view.View):void
      com.waps.ads.f.<init>(com.waps.ads.AdGroupLayout, android.view.ViewGroup):void */
    public void handle() {
        if (AdGroupTargeting.getTestMode()) {
            Log.d("AdView SDK", "Into Wiyun");
        }
        AdGroupLayout adGroupLayout = (AdGroupLayout) this.c.get();
        if (adGroupLayout != null) {
            b bVar = adGroupLayout.d;
            int rgb = Color.rgb(bVar.e, bVar.f, bVar.g);
            int rgb2 = Color.rgb(bVar.a, bVar.b, bVar.c);
            this.a = new AdView(adGroupLayout.getContext());
            this.a.setListener(this);
            this.a.setBackgroundColor(rgb);
            this.a.setTextColor(rgb2);
            this.a.setResId(this.d.e);
            this.a.setTestMode(AdGroupTargeting.getTestMode());
            this.a.setTransitionType(5);
            this.a.setRefreshInterval(0);
            this.a.requestAd();
            adGroupLayout.j.resetRollover();
            adGroupLayout.b.post(new f(adGroupLayout, (ViewGroup) this.a));
            adGroupLayout.rotateThreadedDelayed();
        }
    }

    public void onAdClicked() {
    }

    public void onAdLoadFailed() {
        if (AdGroupTargeting.getTestMode()) {
            Log.d("AdView SDK", "Wiyun failure");
        }
        this.a.setListener(null);
        AdGroupLayout adGroupLayout = (AdGroupLayout) this.c.get();
        if (adGroupLayout != null) {
            adGroupLayout.j.resetRollover();
            adGroupLayout.rollover();
        }
    }

    public void onAdLoaded() {
    }

    public void onExitButtonClicked() {
    }
}
