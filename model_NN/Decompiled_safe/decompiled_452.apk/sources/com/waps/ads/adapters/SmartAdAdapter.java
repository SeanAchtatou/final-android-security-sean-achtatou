package com.waps.ads.adapters;

import android.app.Activity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ViewGroup;
import com.madhouse.android.ads.AdListener;
import com.madhouse.android.ads.AdManager;
import com.madhouse.android.ads.AdView;
import com.waps.ads.AdGroupLayout;
import com.waps.ads.AdGroupTargeting;
import com.waps.ads.a.a;
import com.waps.ads.b.b;
import com.waps.ads.b.c;
import com.waps.ads.f;

public class SmartAdAdapter extends a implements AdListener {
    public SmartAdAdapter(AdGroupLayout adGroupLayout, c cVar) {
        super(adGroupLayout, cVar);
    }

    public void handle() {
        if (AdGroupTargeting.getTestMode()) {
            Log.d("AdView SDK", "Into SmartAd");
        }
        AdGroupLayout adGroupLayout = (AdGroupLayout) this.c.get();
        if (adGroupLayout != null) {
            b bVar = adGroupLayout.d;
            Activity activity = (Activity) adGroupLayout.a.get();
            if (activity != null) {
                AdManager.setApplicationId(activity, this.d.e);
                new AdView(activity, (AttributeSet) null, 0, this.d.f, bVar.i, 0, AdGroupTargeting.getTestMode()).setListener(this);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.waps.ads.f.<init>(com.waps.ads.AdGroupLayout, android.view.ViewGroup):void
     arg types: [com.waps.ads.AdGroupLayout, com.madhouse.android.ads.AdView]
     candidates:
      com.waps.ads.f.<init>(com.waps.ads.AdGroupLayout, android.view.View):void
      com.waps.ads.f.<init>(com.waps.ads.AdGroupLayout, android.view.ViewGroup):void */
    public void onAdEvent(AdView adView, int i) {
        switch (i) {
            case 1:
            case 3:
                if (AdGroupTargeting.getTestMode()) {
                    Log.d("AdView SDK", "SmartAd new Ad");
                }
                AdGroupLayout adGroupLayout = (AdGroupLayout) this.c.get();
                if (adGroupLayout != null) {
                    adGroupLayout.j.resetRollover();
                    adGroupLayout.b.post(new f(adGroupLayout, (ViewGroup) adView));
                    adGroupLayout.rotateThreadedDelayed();
                    adView.setListener((AdListener) null);
                    return;
                }
                return;
            case 2:
                if (AdGroupTargeting.getTestMode()) {
                    Log.d("AdView SDK", "SmartAd success");
                }
                adView.setListener((AdListener) null);
                return;
            case 4:
                if (AdGroupTargeting.getTestMode()) {
                    Log.d("AdView SDK", "SmartAd invalid ad");
                }
                adView.setListener((AdListener) null);
                AdGroupLayout adGroupLayout2 = (AdGroupLayout) this.c.get();
                if (adGroupLayout2 != null) {
                    adGroupLayout2.j.resetRollover();
                    adGroupLayout2.rollover();
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onAdStatus(int i) {
        if (i != 200) {
            if (AdGroupTargeting.getTestMode()) {
                Log.d("AdView SDK", "SmartAd fail ad");
            }
            AdGroupLayout adGroupLayout = (AdGroupLayout) this.c.get();
            if (adGroupLayout != null) {
                adGroupLayout.j.resetRollover();
                adGroupLayout.rollover();
            }
        }
    }
}
