package com.waps.ads.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import android.view.ViewGroup;
import com.waps.ads.AdGroupLayout;
import com.waps.ads.AdGroupTargeting;
import com.waps.ads.a.a;
import com.waps.ads.b.b;
import com.waps.ads.b.c;
import com.waps.ads.f;
import net.youmi.android.AdManager;
import net.youmi.android.AdView;

public class YoumiAdapter extends a {
    public YoumiAdapter(AdGroupLayout adGroupLayout, c cVar) {
        super(adGroupLayout, cVar);
        AdManager.init(new String(cVar.e), new String(cVar.f), adGroupLayout.d.i, AdGroupTargeting.getTestMode(), 2.2d);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.waps.ads.f.<init>(com.waps.ads.AdGroupLayout, android.view.ViewGroup):void
     arg types: [com.waps.ads.AdGroupLayout, net.youmi.android.AdView]
     candidates:
      com.waps.ads.f.<init>(com.waps.ads.AdGroupLayout, android.view.View):void
      com.waps.ads.f.<init>(com.waps.ads.AdGroupLayout, android.view.ViewGroup):void */
    public void handle() {
        if (AdGroupTargeting.getTestMode()) {
            Log.d("AdView SDK", "Into Youmi");
        }
        AdGroupLayout adGroupLayout = (AdGroupLayout) this.c.get();
        if (adGroupLayout != null) {
            b bVar = adGroupLayout.d;
            int rgb = Color.rgb(bVar.e, bVar.f, bVar.g);
            int rgb2 = Color.rgb(bVar.a, bVar.b, bVar.c);
            Activity activity = (Activity) adGroupLayout.a.get();
            if (activity != null) {
                AdView adView = new AdView(activity, rgb, rgb2, 255);
                adGroupLayout.j.resetRollover();
                adGroupLayout.b.post(new f(adGroupLayout, (ViewGroup) adView));
                adGroupLayout.rotateThreadedDelayed();
            }
        }
    }

    public void onConnectFailed(AdView adView) {
    }

    public void onReceiveAd(AdView adView) {
    }
}
