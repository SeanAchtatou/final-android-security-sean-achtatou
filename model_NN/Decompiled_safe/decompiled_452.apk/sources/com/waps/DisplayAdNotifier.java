package com.waps;

import android.view.View;

public interface DisplayAdNotifier {
    void getDisplayAdResponse(View view);

    void getDisplayAdResponseFailed(String str);
}
