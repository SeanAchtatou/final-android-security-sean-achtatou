package com.millennialmedia.android;

import android.util.Log;
import java.io.IOException;
import java.net.URI;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class HttpGetRequest {
    private static final String TAG = "MillennialMediaSDK";

    public void trackConversion(String goalId, String auid) throws Exception {
        try {
            String url = "http://cvt.mydas.mobi/handleConversion?goalId=" + goalId + "&auid=" + auid + ");";
            Log.d(TAG, "Sending conversion tracker report :" + url);
            HttpClient client = new DefaultHttpClient();
            HttpGet getRequest = new HttpGet();
            getRequest.setURI(new URI(url));
            HttpResponse response = client.execute(getRequest);
            if (response.getStatusLine().getStatusCode() == 200) {
                Log.i(TAG, "Conversion tracker reponse code: " + response.getStatusLine().getStatusCode());
            } else {
                Log.e(TAG, "Conversion tracker unable to complete report: " + response.getStatusLine().getStatusCode());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
