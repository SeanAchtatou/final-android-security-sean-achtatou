package com.millennialmedia.android;

import android.app.Activity;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.VideoView;

public class VideoPlayer extends Activity {
    private static final String TAG = "MillennailMediaSDK";
    private RelativeLayout controlsLayout;
    /* access modifiers changed from: private */
    public String current;
    private Button mPause;
    private Button mPlay;
    private Button mStop;
    /* access modifiers changed from: private */
    public VideoView mVideoView;
    private RelativeLayout relLayout;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.relLayout = new RelativeLayout(this);
        this.relLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        RelativeLayout.LayoutParams videoLp = new RelativeLayout.LayoutParams(-2, -2);
        videoLp.addRule(13);
        this.mVideoView = new VideoView(this);
        this.controlsLayout = new RelativeLayout(this);
        this.controlsLayout.setBackgroundColor(-16777216);
        RelativeLayout.LayoutParams controlsLp = new RelativeLayout.LayoutParams(-1, -2);
        this.controlsLayout.setLayoutParams(controlsLp);
        controlsLp.addRule(12);
        this.mPlay = new Button(this);
        this.mPause = new Button(this);
        this.mStop = new Button(this);
        this.mPlay.setBackgroundResource(17301540);
        this.mPause.setBackgroundResource(17301539);
        this.mStop.setBackgroundResource(17301560);
        RelativeLayout.LayoutParams pauseLp = new RelativeLayout.LayoutParams(-2, -2);
        RelativeLayout.LayoutParams stopLp = new RelativeLayout.LayoutParams(-2, -2);
        pauseLp.addRule(14);
        this.controlsLayout.addView(this.mPause, pauseLp);
        this.controlsLayout.addView(this.mPlay);
        stopLp.addRule(11);
        this.controlsLayout.addView(this.mStop, stopLp);
        this.mPlay.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                VideoPlayer.this.playVideo();
            }
        });
        this.mPause.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (VideoPlayer.this.mVideoView != null) {
                    VideoPlayer.this.mVideoView.pause();
                }
            }
        });
        this.mStop.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (VideoPlayer.this.mVideoView != null) {
                    VideoPlayer.this.current = null;
                    VideoPlayer.this.mVideoView.stopPlayback();
                    VideoPlayer.this.dismiss();
                }
            }
        });
        runOnUiThread(new Runnable() {
            public void run() {
                VideoPlayer.this.playVideo();
            }
        });
        this.relLayout.addView(this.mVideoView, videoLp);
        this.relLayout.addView(this.controlsLayout, controlsLp);
        this.controlsLayout.bringToFront();
        setContentView(this.relLayout);
        playVideo();
    }

    /* access modifiers changed from: private */
    public void playVideo() {
        try {
            Uri uri = getIntent().getData();
            Log.v(TAG, "uri: " + uri);
            String path = uri.toString();
            Log.v(TAG, "path: " + path);
            if (path == null || path.length() == 0) {
                Toast.makeText(this, "Sorry. There was a problem playing the video", 1).show();
            } else if (!path.equals(this.current) || this.mVideoView == null) {
                this.current = path;
                Log.i(TAG, "Current: " + this.current);
                Log.i(TAG, "Path: " + path);
                if (this.mVideoView != null) {
                    this.mVideoView.setVideoPath(path);
                    this.mVideoView.start();
                    this.mVideoView.requestFocus();
                    return;
                }
                Log.e(TAG, "Video Player is Null");
            } else {
                this.mVideoView.start();
                this.mVideoView.requestFocus();
            }
        } catch (Exception e) {
            Exception e2 = e;
            Log.e(TAG, "error: " + e2.getMessage(), e2);
            Toast.makeText(this, "Sorry. There was a problem playing the video", 1).show();
            if (this.mVideoView != null) {
                this.mVideoView.stopPlayback();
            }
        }
    }

    /* access modifiers changed from: private */
    public void dismiss() {
        Log.i(TAG, "Video Ad overlay closed");
        finish();
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
}
