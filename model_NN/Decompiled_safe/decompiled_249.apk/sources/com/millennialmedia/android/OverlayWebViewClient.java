package com.millennialmedia.android;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/* compiled from: MMAdViewWebOverlay */
final class OverlayWebViewClient extends WebViewClient {
    private static final String TAG = "MillennialMediaSDK";

    OverlayWebViewClient() {
    }

    public boolean shouldOverrideUrlLoading(WebView view, String urlString) {
        String locationString;
        int rc;
        if (urlString != null) {
            String mimeTypeString = null;
            do {
                locationString = urlString;
                try {
                    URL connectURL = new URL(urlString);
                    HttpURLConnection.setFollowRedirects(false);
                    HttpURLConnection conn = (HttpURLConnection) connectURL.openConnection();
                    conn.connect();
                    urlString = conn.getHeaderField("Location");
                    mimeTypeString = conn.getHeaderField("Content-Type");
                    rc = conn.getResponseCode();
                    Log.d(TAG, "Response Code:" + conn.getResponseCode() + "Response Message:" + conn.getResponseMessage());
                    Log.i(TAG, "urlString: " + urlString);
                    if (rc < 300) {
                        break;
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            } while (rc < 400);
            Activity activity = (Activity) view.getContext();
            if (activity == null) {
                Log.i(TAG, "Activity is null. Returning from click");
                return false;
            } else if (locationString == null) {
                return false;
            } else {
                Uri destinationURI = Uri.parse(locationString);
                Log.i(TAG, "DestinationURI: " + destinationURI.toString());
                if (destinationURI.getScheme().equalsIgnoreCase("market")) {
                    Log.i(TAG, "Android Market URL, launch the Market Application");
                    activity.startActivity(new Intent("android.intent.action.VIEW", destinationURI));
                } else if (destinationURI.getScheme().equalsIgnoreCase("rtsp") || (destinationURI.getScheme().equalsIgnoreCase("http") && (mimeTypeString.equalsIgnoreCase("video/mp4") || mimeTypeString.equalsIgnoreCase("video/3gpp")))) {
                    Log.i(TAG, "Ignore MalFormedUrlException for RTSP");
                    Log.i(TAG, "Video, launch the video player for video at: " + destinationURI);
                    Intent intent = new Intent(activity, VideoPlayer.class);
                    intent.setData(destinationURI);
                    activity.startActivityForResult(intent, 0);
                } else if (destinationURI.getScheme().equalsIgnoreCase("tel")) {
                    Log.i(TAG, "Telephone Number, launch the phone");
                    activity.startActivity(new Intent("android.intent.action.DIAL", destinationURI));
                } else if (!destinationURI.getScheme().equalsIgnoreCase("http") || destinationURI.getLastPathSegment() == null) {
                    view.loadUrl(destinationURI.toString());
                } else if (destinationURI.getLastPathSegment().endsWith(".mp4") || destinationURI.getLastPathSegment().endsWith(".3gp")) {
                    Log.i(TAG, "Video, launch the video player for video at: " + destinationURI);
                    Intent intent2 = new Intent(activity, VideoPlayer.class);
                    intent2.setData(destinationURI);
                    activity.startActivityForResult(intent2, 0);
                } else {
                    view.loadUrl(destinationURI.toString());
                }
            }
        }
        return true;
    }

    public void onReceivedError(WebView view, Error errorCode, String description, String failingUrl) {
        Log.e(TAG, "Error: " + errorCode + "  " + description);
    }
}
