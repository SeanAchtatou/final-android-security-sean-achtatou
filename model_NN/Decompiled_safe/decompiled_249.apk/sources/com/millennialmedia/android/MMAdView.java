package com.millennialmedia.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.MotionEvent;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.qwapi.adclient.android.utils.Utils;
import com.qwapi.adclient.android.view.AdViewConstants;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import net.mony.more.qaqad.utils.Constants;

public class MMAdView extends WebView {
    private static final String TAG = "MillennialMediaAdSDK";
    private final String SDKVER = "3.6.2-10.10.12.a";
    public boolean accelerate = true;
    private String acid = null;
    /* access modifiers changed from: private */
    public final WeakReference<Activity> activityWeakReference;
    public String adType;
    private String age = null;
    private String apid = "15062";
    /* access modifiers changed from: private */
    public String auid = "12.34.43.21";
    private boolean autoCallForAds = true;
    private String baseUrl = "http://androidsdk.ads.mp.mydas.mobi/getAd.php5?";
    protected boolean canAccelerate = false;
    private String children = null;
    private ConnectivityManager cm;
    private String education = null;
    private String ethnicity = null;
    private String gender = null;
    /* access modifiers changed from: private */
    public String goalId;
    /* access modifiers changed from: private */
    public Handler handler;
    private String height = null;
    private String income = null;
    private String keywords = null;
    private String latitude = null;
    public MMAdListener listener;
    private String longitude = null;
    private String marital = null;
    private String modeString = "&mode=live";
    /* access modifiers changed from: private */
    public String nextUrl;
    private String orientation = null;
    /* access modifiers changed from: private */
    public String overlayTitle = "Advertisement";
    /* access modifiers changed from: private */
    public String overlayTransition = "bottomtotop";
    private String politics = null;
    /* access modifiers changed from: private */
    public int refreshIntervalInMilliseconds;
    private int refreshIntervalInSeconds = 60;
    /* access modifiers changed from: private */
    public boolean refreshTimerOn;
    private Runnable runnable = new Runnable() {
        public void run() {
            MMAdView.this.getAd();
            if (MMAdView.this.refreshTimerOn) {
                MMAdView.this.handler.postDelayed(this, (long) MMAdView.this.refreshIntervalInMilliseconds);
            }
        }
    };
    /* access modifiers changed from: private */
    public boolean shouldLaunchToOverlay = false;
    /* access modifiers changed from: private */
    public int shouldResizeOverlay = 0;
    /* access modifiers changed from: private */
    public boolean shouldShowNavbar = true;
    /* access modifiers changed from: private */
    public boolean shouldShowTitlebar = false;
    public boolean testMode = false;
    /* access modifiers changed from: private */
    public long transitionTime = 600;
    private String ua;
    private String width = null;
    private String zip = null;

    public interface MMAdListener {
        void MMAdClickedToNewBrowser(MMAdView mMAdView);

        void MMAdClickedToOverlay(MMAdView mMAdView);

        void MMAdFailed(MMAdView mMAdView);

        void MMAdOverlayLaunched(MMAdView mMAdView);

        void MMAdReturned(MMAdView mMAdView);
    }

    public MMAdView(Activity context, String apid2, String adType2, int refreshInterval) {
        super(context);
        this.activityWeakReference = new WeakReference<>(context);
        this.apid = apid2;
        this.adType = adType2;
        this.refreshIntervalInSeconds = refreshInterval;
        init();
    }

    public MMAdView(Activity context, String apid2, String adType2, int refreshInterval, boolean testMode2) {
        super(context);
        this.activityWeakReference = new WeakReference<>(context);
        this.apid = apid2;
        this.adType = adType2;
        this.refreshIntervalInSeconds = refreshInterval;
        this.testMode = testMode2;
        init();
    }

    public MMAdView(Activity context, String apid2, String adType2, int refreshInterval, boolean testMode2, boolean accelerate2) {
        super(context);
        this.activityWeakReference = new WeakReference<>(context);
        this.apid = apid2;
        this.adType = adType2;
        this.refreshIntervalInSeconds = refreshInterval;
        this.testMode = testMode2;
        this.accelerate = accelerate2;
        init();
    }

    public MMAdView(Activity context, String apid2, String adType2, int refreshInterval, Hashtable<String, String> metaMap) {
        super(context);
        this.activityWeakReference = new WeakReference<>(context);
        this.apid = apid2;
        this.adType = adType2;
        this.refreshIntervalInSeconds = refreshInterval;
        setMetaValues(metaMap);
        init();
    }

    public MMAdView(Activity context, String apid2, String adType2, int refreshInterval, boolean testMode2, Hashtable<String, String> metaMap) {
        super(context);
        this.activityWeakReference = new WeakReference<>(context);
        this.apid = apid2;
        this.adType = adType2;
        this.testMode = testMode2;
        this.refreshIntervalInSeconds = refreshInterval;
        setMetaValues(metaMap);
        init();
    }

    public MMAdView(Activity context, String apid2, String adType2, int refreshInterval, boolean testMode2, boolean accelerate2, Hashtable<String, String> metaMap) {
        super(context);
        this.activityWeakReference = new WeakReference<>(context);
        this.apid = apid2;
        this.adType = adType2;
        this.refreshIntervalInSeconds = refreshInterval;
        this.testMode = testMode2;
        this.accelerate = accelerate2;
        setMetaValues(metaMap);
        init();
    }

    private void init() {
        setId(15062);
        Log.d(TAG, "New MMAdView Started");
        getSettings().setJavaScriptEnabled(true);
        getSettings().setCacheMode(2);
        setBackgroundColor(0);
        this.auid = "android_id";
        setWillNotDraw(false);
        setClickable(true);
        addJavascriptInterface(new MMJSInterface(), "interface");
        this.ua = String.valueOf(getSettings().getUserAgentString()) + Build.MODEL;
        this.auid = "android_id";
        Activity activity = this.activityWeakReference.get();
        if (this.activityWeakReference != null) {
            PackageManager pm = activity.getPackageManager();
            try {
                Log.i(TAG, "Activity: " + pm.getActivityInfo(new ComponentName(activity, "com.millennialmedia.android.VideoPlayer"), 128).toString());
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
                final AlertDialog videoDialog = new AlertDialog.Builder(activity).create();
                videoDialog.setTitle("Whoops!");
                videoDialog.setMessage("Looks like you forgot to declare the Millennial Media Video Player in your manifest file.");
                videoDialog.setButton(-3, "OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        videoDialog.cancel();
                    }
                });
                videoDialog.show();
            }
            try {
                Log.i(TAG, "Activity: " + pm.getActivityInfo(new ComponentName(activity, "com.millennialmedia.android.MMAdViewOverlayActivity"), 128).toString());
            } catch (PackageManager.NameNotFoundException e2) {
                e2.printStackTrace();
                final AlertDialog overlayDialog = new AlertDialog.Builder(activity).create();
                overlayDialog.setTitle("Whoops!");
                overlayDialog.setMessage("Looks like you forgot to declare the Millennial Media MMAdViewOverlayActivity in your manifest file.");
                overlayDialog.setButton(-3, "OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        overlayDialog.cancel();
                    }
                });
                overlayDialog.show();
            }
            this.auid = ((TelephonyManager) activity.getSystemService("phone")).getDeviceId();
            this.cm = (ConnectivityManager) activity.getApplicationContext().getSystemService("connectivity");
            getFirstAd(this.refreshIntervalInSeconds);
        }
    }

    public void halt() {
        stopUpdateTimer();
    }

    private void stopUpdateTimer() {
        if (this.handler != null) {
            this.handler.removeCallbacks(this.runnable);
        }
    }

    private void getFirstAd(int refreshInt) {
        setRefreshIntervalForTimer(refreshInt);
        if (this.autoCallForAds) {
            getAd();
        }
    }

    private void setRefreshIntervalForTimer(int refreshInterval) {
        if (refreshInterval >= 0 && refreshInterval < 30) {
            this.refreshTimerOn = false;
            Log.d(TAG, "Refresh interval is " + refreshInterval + ". Change to at least 30 to refresh ads.");
        } else if (refreshInterval < 0) {
            this.refreshTimerOn = false;
            this.autoCallForAds = false;
            Log.d(TAG, "Automatic ad fetching is off with " + refreshInterval + ". You must manually call for ads.");
        } else {
            this.refreshTimerOn = true;
            this.refreshIntervalInMilliseconds = refreshInterval * Constants.MAX_DOWNLOADS;
            administerRefreshTimer(true);
        }
    }

    /* access modifiers changed from: private */
    public void administerRefreshTimer(boolean refreshAds) {
        synchronized (this) {
            if (refreshAds) {
                Log.d(TAG, "Refresh Timer is on");
                if (this.handler == null) {
                    this.handler = new Handler();
                }
                this.handler.removeCallbacks(this.runnable);
                this.handler.postDelayed(this.runnable, (long) this.refreshIntervalInMilliseconds);
            } else {
                this.handler.removeCallbacks(this.runnable);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        try {
            if (this.handler != null) {
                this.handler.removeCallbacks(this.runnable);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void onWindowFocusChanged(boolean windowInFocus) {
        if (this.refreshTimerOn) {
            administerRefreshTimer(windowInFocus);
            Log.d(TAG, "Window Focus Changed. Window in focus?: " + windowInFocus);
        }
    }

    public void updateUserLocation(Location userLocation) {
        if (userLocation != null) {
            if (userLocation.getLatitude() <= 90.0d && userLocation.getLatitude() >= -90.0d) {
                this.latitude = String.valueOf(userLocation.getLatitude());
            }
            if (userLocation.getLongitude() <= 180.0d && userLocation.getLongitude() >= -180.0d) {
                this.longitude = String.valueOf(userLocation.getLongitude());
            }
        }
    }

    public void setMetaValues(Hashtable<String, String> metaHash) {
        if (metaHash.containsKey("age")) {
            this.age = metaHash.get("age");
        }
        if (metaHash.containsKey("gender")) {
            this.gender = metaHash.get("gender");
        }
        if (metaHash.containsKey("zip")) {
            this.zip = metaHash.get("zip");
        }
        if (metaHash.containsKey("marital")) {
            this.marital = metaHash.get("marital");
        }
        if (metaHash.containsKey("income")) {
            this.income = metaHash.get("income");
        }
        if (metaHash.containsKey("keywords")) {
            this.keywords = metaHash.get("keywords");
        }
        if (metaHash.containsKey("mmacid")) {
            this.acid = metaHash.get("mmacid");
        }
        if (metaHash.containsKey("height")) {
            this.height = metaHash.get("height");
        }
        if (metaHash.containsKey("width")) {
            this.width = metaHash.get("width");
        }
        if (metaHash.containsKey("ethnicity")) {
            this.ethnicity = metaHash.get("ethnicity");
        }
        if (metaHash.containsKey("orientation")) {
            this.orientation = metaHash.get("orientation");
        }
        if (metaHash.containsKey("education")) {
            this.education = metaHash.get("education");
        }
        if (metaHash.containsKey("children")) {
            this.children = metaHash.get("children");
        }
        if (metaHash.containsKey("politics")) {
            this.politics = metaHash.get("politics");
        }
    }

    private String getMetaValues() {
        String metaString = Utils.EMPTY_STRING;
        if (this.age != null) {
            metaString = String.valueOf(metaString) + "&age=" + this.age;
        }
        if (this.gender != null) {
            metaString = String.valueOf(metaString) + "&gender=" + this.gender;
        }
        if (this.zip != null) {
            metaString = String.valueOf(metaString) + "&zip=" + this.zip;
        }
        if (this.marital == "single" || this.marital == "married" || this.marital == "divorced" || this.marital == "swinger" || this.marital == "relationship" || this.marital == "engaged") {
            metaString = String.valueOf(metaString) + "&marital=" + this.marital;
        }
        if (this.income != null) {
            metaString = String.valueOf(metaString) + "&income=" + this.income;
        }
        if (this.keywords != null) {
            metaString = String.valueOf(metaString) + "&kw=" + this.keywords;
        }
        if (this.latitude != null) {
            metaString = String.valueOf(metaString) + "&lat=" + this.latitude;
        }
        if (this.longitude != null) {
            metaString = String.valueOf(metaString) + "&long=" + this.longitude;
        }
        if (this.acid != null) {
            metaString = String.valueOf(metaString) + "&acid=" + this.acid;
        }
        if (this.height != null) {
            metaString = String.valueOf(metaString) + "&hsht=" + this.height;
        }
        if (this.width != null) {
            metaString = String.valueOf(metaString) + "&hswd=" + this.width;
        }
        if (this.ethnicity != null) {
            metaString = String.valueOf(metaString) + "&ethnicity=" + this.ethnicity;
        }
        if (this.orientation == "straight" || this.orientation == "gay" || this.orientation == "bisexual" || this.orientation == "notsure") {
            metaString = String.valueOf(metaString) + "&orientation=" + this.orientation;
        }
        if (this.education != null) {
            metaString = String.valueOf(metaString) + "&edu=" + this.education;
        }
        if (this.children != null) {
            metaString = String.valueOf(metaString) + "&children=" + this.children;
        }
        if (this.politics != null) {
            metaString = String.valueOf(metaString) + "&politics=" + this.politics;
        }
        if (metaString != null) {
            return metaString;
        }
        return null;
    }

    public void getTestMode(boolean testMode2) {
        if (testMode2) {
            this.modeString = "&mode=inapptest";
            Log.d(TAG, "*********** advertising test mode **************");
        }
    }

    public String getAdType(String adtype) {
        if (adtype == "MMBannerAdTop") {
            return "&adtype=MMBannerAdTop";
        }
        if (adtype == "MMBannerAdBottom") {
            return "&adtype=MMBannerAdBottom";
        }
        if (adtype == "MMBannerAdRectangle") {
            return "&adtype=MMBannerAdRectangle";
        }
        if (adtype == "MMFullScreenAdLaunch") {
            return "&adtype=MMFullScreenAdLaunch";
        }
        if (adtype == "MMFullScreenAdTransition") {
            return "&adtype=MMFullScreenAdTransition";
        }
        Log.e(TAG, "******* ERROR: INCORRECT AD TYPE IN MMADVIEW OBJECT PARAMETERS **********");
        Log.e(TAG, "******* SDK DEFAULTED TO MMBannerAdTop. THIS MAY AFFECT THE ADS YOU RECIEVE!!! **********");
        return "&adtype=MMBannerAdTop";
    }

    public void callForAd() {
        if (!this.autoCallForAds) {
            getAd();
        }
    }

    /* access modifiers changed from: private */
    public void getAd() {
        if (this.cm.getActiveNetworkInfo() == null || !this.cm.getActiveNetworkInfo().isConnected()) {
            Log.i(TAG, "No network available, can't call for ads.");
            return;
        }
        String metaValues = getMetaValues();
        getTestMode(this.testMode);
        String adTypeString = getAdType(this.adType);
        String adUrl = String.valueOf(this.baseUrl) + "sdkapid=" + this.apid + "&auid=" + this.auid + "&ua=" + this.ua + "&mmisdk=" + "3.6.2-10.10.12.a" + metaValues + this.modeString;
        if (adTypeString != null) {
            adUrl = String.valueOf(adUrl) + adTypeString;
        }
        Log.d(TAG, "Calling for an advertisement: " + adUrl);
        getWebView().setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                MMAdView.this.getWebView().loadUrl("javascript:window.interface.countimages(document.images.length)");
                MMAdView.this.getWebView().loadUrl("javascript:window.interface.getUrl(document.links[0].href)");
            }

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return true;
            }

            public void onScaleChanged(WebView view, float oldScale, float newScale) {
                Log.e(MMAdView.TAG, "Scale Changed");
            }
        });
        loadUrl(adUrl);
    }

    public MMAdView getWebView() {
        return this;
    }

    public boolean dispatchTouchEvent(MotionEvent e) {
        int action = e.getAction();
        if (action == 1) {
            Log.v(TAG, "Ad clicked: action=" + action + " x=" + e.getX() + " y=" + e.getY());
            new Thread(new Runnable() {
                public void run() {
                    if (MMAdView.this.shouldLaunchToOverlay) {
                        if (MMAdView.this.listener != null) {
                            try {
                                MMAdView.this.listener.MMAdClickedToOverlay(MMAdView.this.getWebView());
                            } catch (Exception e) {
                                Log.w(MMAdView.TAG, "Exception raised in your MMAdListener: ", e);
                            }
                        }
                        if (MMAdView.this.nextUrl != null) {
                            MMAdView.this.touchUpInside(MMAdView.this.nextUrl);
                        } else {
                            Log.e(MMAdView.TAG, "No ad returned, no overlay launched");
                        }
                    } else if (MMAdView.this.nextUrl != null) {
                        Log.i(MMAdView.TAG, "Ad clicked, launching new browser");
                        Activity activity = (Activity) MMAdView.this.activityWeakReference.get();
                        if (activity != null) {
                            activity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(MMAdView.this.nextUrl)));
                            if (MMAdView.this.listener != null) {
                                try {
                                    MMAdView.this.listener.MMAdClickedToNewBrowser(MMAdView.this.getWebView());
                                } catch (Exception e2) {
                                    Log.w(MMAdView.TAG, "Exception raised in your MMAdListener: ", e2);
                                }
                            }
                        }
                    } else {
                        Log.e(MMAdView.TAG, "No ad returned, no new browser launched");
                    }
                }
            }).start();
        }
        return super.dispatchTouchEvent(e);
    }

    public void touchUpInside(String urlString) {
        String locationString;
        int rc;
        Log.i(TAG, "touch event happened, touchUpInside called");
        if (urlString != null) {
            String mimeTypeString = null;
            do {
                locationString = urlString;
                try {
                    URL connectURL = new URL(urlString);
                    HttpURLConnection.setFollowRedirects(false);
                    HttpURLConnection conn = (HttpURLConnection) connectURL.openConnection();
                    conn.setRequestMethod("GET");
                    conn.connect();
                    urlString = conn.getHeaderField("Location");
                    mimeTypeString = conn.getHeaderField("Content-Type");
                    rc = conn.getResponseCode();
                    Log.d("urlapp", "Response Code:" + conn.getResponseCode());
                    Log.d("urlapp", "Response Message:" + conn.getResponseMessage());
                    Log.i(TAG, "urlString: " + urlString);
                    if (rc < 300) {
                        break;
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            } while (rc < 400);
            Log.i(TAG, "locationString: " + locationString);
            Activity activity = this.activityWeakReference.get();
            if (activity == null) {
                Log.i(TAG, "Activity is null. Returning from click");
            } else if (locationString != null) {
                Uri destinationURI = Uri.parse(locationString);
                if ((destinationURI.getScheme().equalsIgnoreCase("http") || destinationURI.getScheme().equalsIgnoreCase("https")) && mimeTypeString.equalsIgnoreCase(AdViewConstants.TEXT_HTML)) {
                    Intent intent = new Intent(activity, MMAdViewOverlayActivity.class);
                    intent.putExtra("canAccelerate", this.canAccelerate);
                    intent.putExtra("overlayTransition", this.overlayTransition);
                    intent.putExtra("transitionTime", this.transitionTime);
                    intent.putExtra("shouldResizeOverlay", this.shouldResizeOverlay);
                    intent.putExtra("shouldShowTitlebar", this.shouldShowTitlebar);
                    intent.putExtra("shouldShowNavbar", this.shouldShowNavbar);
                    intent.putExtra("overlayTitle", this.overlayTitle);
                    Log.i(TAG, "Accelerometer on?: " + this.canAccelerate);
                    intent.setData(destinationURI);
                    activity.startActivityForResult(intent, 0);
                } else if (destinationURI.getScheme().equalsIgnoreCase("market")) {
                    Log.i(TAG, "Android Market URL, launch the Market Application");
                    activity.startActivity(new Intent("android.intent.action.VIEW", destinationURI));
                } else if (destinationURI.getScheme().equalsIgnoreCase("rtsp") || (destinationURI.getScheme().equalsIgnoreCase("http") && (mimeTypeString.equalsIgnoreCase("video/mp4") || mimeTypeString.equalsIgnoreCase("video/3gpp")))) {
                    Log.i(TAG, "Video, launch the video player for video at: " + destinationURI);
                    Intent intent2 = new Intent(activity, VideoPlayer.class);
                    intent2.setData(destinationURI);
                    activity.startActivityForResult(intent2, 0);
                } else if (destinationURI.getScheme().equalsIgnoreCase("tel")) {
                    Log.i(TAG, "Telephone Number, launch the phone");
                    activity.startActivity(new Intent("android.intent.action.DIAL", destinationURI));
                } else if (!destinationURI.getScheme().equalsIgnoreCase("http") || destinationURI.getLastPathSegment() == null) {
                    if (destinationURI.getScheme().equalsIgnoreCase("http")) {
                        Intent intent3 = new Intent(activity, MMAdViewOverlayActivity.class);
                        intent3.putExtra("canAccelerate", this.canAccelerate);
                        intent3.putExtra("overlayTransition", this.overlayTransition);
                        intent3.putExtra("transitionTime", this.transitionTime);
                        intent3.putExtra("shouldResizeOverlay", this.shouldResizeOverlay);
                        intent3.putExtra("shouldShowTitlebar", this.shouldShowTitlebar);
                        intent3.putExtra("shouldShowNavbar", this.shouldShowNavbar);
                        intent3.putExtra("overlayTitle", this.overlayTitle);
                        Log.i(TAG, "Accelerometer on?: " + this.canAccelerate);
                        intent3.setData(destinationURI);
                        activity.startActivityForResult(intent3, 0);
                        return;
                    }
                    activity.startActivity(new Intent("android.intent.action.VIEW", destinationURI));
                } else if (destinationURI.getLastPathSegment().endsWith(".mp4") || destinationURI.getLastPathSegment().endsWith(".3gp")) {
                    Log.i(TAG, "Video, launch the video player for video at: " + destinationURI);
                    Intent intent4 = new Intent(activity, VideoPlayer.class);
                    intent4.setData(destinationURI);
                    activity.startActivityForResult(intent4, 0);
                } else {
                    Intent intent5 = new Intent(activity, MMAdViewOverlayActivity.class);
                    intent5.putExtra("canAccelerate", this.canAccelerate);
                    intent5.putExtra("overlayTransition", this.overlayTransition);
                    intent5.putExtra("transitionTime", this.transitionTime);
                    intent5.putExtra("shouldResizeOverlay", this.shouldResizeOverlay);
                    intent5.putExtra("shouldShowTitlebar", this.shouldShowTitlebar);
                    intent5.putExtra("shouldShowNavbar", this.shouldShowNavbar);
                    intent5.putExtra("overlayTitle", this.overlayTitle);
                    Log.i(TAG, "Accelerometer on?: " + this.canAccelerate);
                    intent5.setData(destinationURI);
                    activity.startActivityForResult(intent5, 0);
                }
            }
        }
    }

    public void setListener(MMAdListener listener2) {
        synchronized (this) {
            this.listener = listener2;
        }
    }

    public void startConversionTrackerWithGoalId(String goalId2) {
        this.goalId = goalId2;
        if (this.cm.getActiveNetworkInfo() == null || !this.cm.getActiveNetworkInfo().isConnected()) {
            Log.i(TAG, "No network available, can't call for ads.");
        } else {
            new Thread(new Runnable() {
                public void run() {
                    try {
                        new HttpGetRequest().trackConversion(MMAdView.this.getWebView().goalId, MMAdView.this.auid);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }

    public class MMJSInterface {
        private static final String TAG = "MillennialMediaAdSDK";

        public MMJSInterface() {
        }

        public void countimages(String size) {
            int num;
            Log.d(TAG, "size: " + size);
            if (size != null) {
                num = new Integer(size).intValue();
            } else {
                num = 0;
                Log.e(TAG, "Image count is null");
            }
            Log.d(TAG, "num: " + num);
            if (num > 0) {
                if (MMAdView.this.listener != null) {
                    try {
                        MMAdView.this.listener.MMAdReturned(MMAdView.this.getWebView());
                        Log.i(TAG, "Millennial ad return success");
                        Log.d(TAG, "View height: " + MMAdView.this.getHeight());
                    } catch (Exception e) {
                        Log.w(TAG, "Exception raised in your MMAdListener: ", e);
                    }
                }
            } else if (MMAdView.this.listener != null) {
                try {
                    Log.i(TAG, "Millennial ad return failed");
                    MMAdView.this.listener.MMAdFailed(MMAdView.this.getWebView());
                } catch (Exception e2) {
                    Log.w(TAG, "Exception raised in your MMAdListener: ", e2);
                }
            }
        }

        public void getUrl(String url) {
            MMAdView.this.nextUrl = url;
            Log.d(TAG, "nextUrl: " + MMAdView.this.nextUrl);
        }

        public void shouldOpen(final String url) {
            new Thread(new Runnable() {
                public void run() {
                    MMAdView.this.touchUpInside(url);
                }
            }).start();
            if (MMAdView.this.listener != null) {
                try {
                    MMAdView.this.listener.MMAdOverlayLaunched(MMAdView.this.getWebView());
                } catch (Exception e) {
                    Log.w(TAG, "Exception raised in your MMAdListener: ", e);
                }
            }
        }

        public void shouldOverlay(boolean shouldLaunchToOverlay) {
            MMAdView.this.getWebView().shouldLaunchToOverlay = shouldLaunchToOverlay;
        }

        public void overlayTitle(String title) {
            MMAdView.this.getWebView().overlayTitle = title;
        }

        public void overlayTransition(String transition, long time) {
            MMAdView.this.getWebView().overlayTransition = transition;
            MMAdView.this.getWebView().transitionTime = time;
        }

        public void shouldAccelerate(boolean shouldAccelerate) {
            if (MMAdView.this.accelerate) {
                MMAdView.this.getWebView().canAccelerate = shouldAccelerate;
            } else {
                MMAdView.this.getWebView().canAccelerate = false;
            }
        }

        public void shouldResizeOverlay(int padding) {
            MMAdView.this.getWebView().shouldResizeOverlay = padding;
        }

        public void shouldShowTitlebar(boolean showTitlebar) {
            MMAdView.this.getWebView().shouldShowTitlebar = showTitlebar;
        }

        public void shouldShowNavbar(boolean showNavbar) {
            MMAdView.this.getWebView().shouldShowNavbar = showNavbar;
        }
    }

    public class ScreenReceiver extends BroadcastReceiver {
        public ScreenReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("android.intent.action.SCREEN_OFF")) {
                Log.d(MMAdView.TAG, "Screen is locked");
                if (MMAdView.this.refreshTimerOn) {
                    MMAdView.this.administerRefreshTimer(false);
                }
            } else if (intent.getAction().equals("android.intent.action.SCREEN_ON")) {
                Log.d(MMAdView.TAG, "Screen is unlocked");
                if (MMAdView.this.refreshTimerOn) {
                    MMAdView.this.administerRefreshTimer(true);
                }
            }
        }
    }
}
