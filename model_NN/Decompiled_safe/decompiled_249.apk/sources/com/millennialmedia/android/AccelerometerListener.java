package com.millennialmedia.android;

public interface AccelerometerListener {
    void didAccelerate(float f, float f2, float f3);

    void didShake(float f);
}
