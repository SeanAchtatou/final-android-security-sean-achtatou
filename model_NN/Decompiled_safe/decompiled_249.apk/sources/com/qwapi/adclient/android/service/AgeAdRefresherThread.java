package com.qwapi.adclient.android.service;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.qwapi.adclient.android.data.Ad;
import com.qwapi.adclient.android.data.AdResponse;
import com.qwapi.adclient.android.data.Status;
import com.qwapi.adclient.android.requestparams.DisplayMode;
import com.qwapi.adclient.android.view.QWAdView;
import java.lang.ref.WeakReference;
import net.mony.more.qaqad.utils.Constants;

public class AgeAdRefresherThread extends Thread {
    private static final int AGE_AD_MSG = 8888;
    private static final int MSG = 9999;
    /* access modifiers changed from: private */
    public int adInterval = 30;
    WeakReference<QWAdView> adView = null;
    /* access modifiers changed from: private */
    public DisplayMode displayMode = null;
    boolean first = true;
    private Looper looper = null;
    private Handler mAgeAdRefreshHandler;
    private volatile boolean stopRequested = false;
    /* access modifiers changed from: private */
    public volatile boolean suspendAutoRefresh = false;

    public AgeAdRefresherThread(QWAdView qWAdView, DisplayMode displayMode2, int i) {
        this.adView = new WeakReference<>(qWAdView);
        this.displayMode = displayMode2;
        this.adInterval = i;
    }

    public Handler getRefreshHandler() {
        return this.mAgeAdRefreshHandler;
    }

    public void resumeThread() {
        this.suspendAutoRefresh = false;
    }

    public void run() {
        while (!this.stopRequested) {
            Looper.prepare();
            this.mAgeAdRefreshHandler = new Handler() {
                boolean first = true;

                public void handleMessage(Message message) {
                    if (this.first) {
                        this.first = false;
                        if (AgeAdRefresherThread.this.displayMode == DisplayMode.aged) {
                            sendMessageDelayed(obtainMessage(AgeAdRefresherThread.MSG), (long) (AgeAdRefresherThread.this.adInterval * Constants.MAX_DOWNLOADS));
                        }
                    } else if (!AgeAdRefresherThread.this.suspendAutoRefresh) {
                        boolean unused = AgeAdRefresherThread.this.suspendAutoRefresh = true;
                        Ad ad = new Ad();
                        ad.served();
                        AgeAdRefresherThread.this.adView.get().setAdResponse(new AdResponse(ad, Status.getSuccess()));
                        AgeAdRefresherThread.this.adView.get().showNextAd();
                        if (AgeAdRefresherThread.this.displayMode == DisplayMode.aged) {
                            sendMessageDelayed(obtainMessage(AgeAdRefresherThread.AGE_AD_MSG), (long) (AgeAdRefresherThread.this.adInterval * Constants.MAX_DOWNLOADS));
                        }
                    }
                }
            };
            if (this.first) {
                this.mAgeAdRefreshHandler.sendEmptyMessage(AGE_AD_MSG);
                this.first = false;
            }
            this.looper = Looper.myLooper();
            Looper.loop();
        }
    }

    public void stopThread() {
        this.stopRequested = true;
        if (this.looper != null) {
            this.looper.quit();
        }
    }

    public void suspendThread() {
        this.suspendAutoRefresh = true;
    }
}
