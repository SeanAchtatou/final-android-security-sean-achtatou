package com.qwapi.adclient.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import com.qwapi.adclient.android.utils.Utils;

public class AdTextView extends TextView {
    /* access modifiers changed from: private */
    public String clickUrl;

    public AdTextView(Context context) {
        super(context);
        init();
    }

    public AdTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    private void init() {
        setTextColor(17170435);
        setPadding(3, 3, 3, 3);
        setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ((QWAdView) AdTextView.this.getParent()).testParent();
                Log.d("AdTextView", "I got a click");
                Utils.invokeLandingPage(view, AdTextView.this.clickUrl);
            }
        });
    }

    public String getClickUrl() {
        return this.clickUrl;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    public void setClickUrl(String str) {
        this.clickUrl = str;
    }
}
