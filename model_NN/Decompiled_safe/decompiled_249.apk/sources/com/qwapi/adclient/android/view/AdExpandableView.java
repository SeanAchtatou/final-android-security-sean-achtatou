package com.qwapi.adclient.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.qwapi.adclient.android.AdApiConfig;
import com.qwapi.adclient.android.AdApiConstants;
import com.qwapi.adclient.android.DeviceContext;
import com.qwapi.adclient.android.data.Ad;
import java.lang.ref.WeakReference;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.List;

public class AdExpandableView extends WebView {
    private static final String ANCHOR_COLOR_CHANGE_FUNCTION = "javascript:(function() '{' var anchors = document.getElementsByTagName(\"a\"); var count = anchors.length;for(var i = 0; i<count;i++) '{' anchors[i].style.color = \"rgb({0})\";'}' '}' )()";
    private static final String CLICK_URL_PARAM = "curl=";
    private static final String CLIENT_ID_COOKIE_PARAM = "QuattroClientIdCookie=";
    private static final String CLOSE_URL = "http://close.this.ad/";
    private static final String INAPP_PARAM = "inApp=true";
    private static final String ISANDROID_PARAM = "ia=true";
    private static final String LIVE_ADPROXY_URL = "/ajaxextended/index.php";
    private static final String QWAPI_DOMAIN = "qwapi.com";
    private static final String STAGING_ADPROXY_URL = "/static/adproxy/index.php";
    private static final String TRACKING_PIXEL_PARAM = "tp";
    private static final String URL_PARAM = "u=";
    /* access modifiers changed from: private */
    public boolean userClosed = false;
    /* access modifiers changed from: private */
    public WeakReference<QWAdView> wAdView = null;

    private class AdExpandableViewClient extends WebViewClient {
        WeakReference<Ad> wAd;
        WeakReference<EventDispatcher> wDispatacher;

        public AdExpandableViewClient(EventDispatcher eventDispatcher, Ad ad) {
            this.wDispatacher = new WeakReference<>(eventDispatcher);
            this.wAd = new WeakReference<>(ad);
        }

        public boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
            jsResult.confirm();
            return true;
        }

        public void onLoadResource(WebView webView, String str) {
            if (str == null) {
                return;
            }
            if (str.contains(AdExpandableView.LIVE_ADPROXY_URL) || str.contains(AdExpandableView.STAGING_ADPROXY_URL)) {
                ((QWAdView) AdExpandableView.this.wAdView.get()).suspendRefresherThreads();
            }
        }

        public void onPageFinished(WebView webView, String str) {
            webView.loadUrl(MessageFormat.format(AdExpandableView.ANCHOR_COLOR_CHANGE_FUNCTION, ((QWAdView) AdExpandableView.this.wAdView.get()).getTextColor()));
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            Log.d(AdApiConstants.SDK, "shouldOverrideUrlLoading : " + str);
            if (!str.equals(AdExpandableView.CLOSE_URL)) {
                return false;
            }
            Log.d(AdApiConstants.SDK, "Closing the Interstitial WebView");
            boolean unused = AdExpandableView.this.userClosed = true;
            webView.setVisibility(8);
            webView.invalidate();
            ((QWAdView) AdExpandableView.this.wAdView.get()).setVisibility(8);
            ((QWAdView) AdExpandableView.this.wAdView.get()).setAdView(new AdExpandableView(webView.getContext(), this.wAd.get(), this.wDispatacher.get(), (QWAdView) AdExpandableView.this.wAdView.get(), false));
            ((QWAdView) AdExpandableView.this.wAdView.get()).setVisibility(0);
            ((QWAdView) AdExpandableView.this.wAdView.get()).resumeRefresherThreads();
            return true;
        }
    }

    final class MyWebChromeClient extends WebChromeClient {
        MyWebChromeClient() {
        }

        public boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
            jsResult.confirm();
            return true;
        }
    }

    public AdExpandableView(Context context, AttributeSet attributeSet, Ad ad, EventDispatcher eventDispatcher, QWAdView qWAdView, boolean z) {
        super(context, attributeSet);
        init(ad, eventDispatcher, qWAdView, z);
    }

    public AdExpandableView(Context context, Ad ad, EventDispatcher eventDispatcher, QWAdView qWAdView, boolean z) {
        super(context);
        init(ad, eventDispatcher, qWAdView, z);
    }

    private void init(Ad ad, EventDispatcher eventDispatcher, QWAdView qWAdView, boolean z) {
        getSettings().setJavaScriptEnabled(true);
        getSettings().setCacheMode(0);
        this.wAdView = new WeakReference<>(qWAdView);
        setWebViewClient(new AdExpandableViewClient(eventDispatcher, ad));
        getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        getSettings().setSupportMultipleWindows(true);
        getSettings().setDefaultFontSize(12);
        load(ad, z);
        setBackgroundColor(0);
    }

    private void load(Ad ad, boolean z) {
        if (ad != null) {
            Log.d(AdApiConstants.SDK, ad.getXhtmlAdContent(this.wAdView.get().getTextColor()));
        }
        StringBuffer stringBuffer = new StringBuffer(ad.getData().getUrl());
        if (stringBuffer.toString().contains(AdViewConstants.QUESTION)) {
            stringBuffer.append(AdViewConstants.AMP);
        } else {
            stringBuffer.append(AdViewConstants.QUESTION);
        }
        stringBuffer.append(INAPP_PARAM);
        List<String> trackingPixels = ad.getTrackingPixels();
        StringBuffer stringBuffer2 = new StringBuffer();
        stringBuffer2.append(AdApiConfig.getProxyUrl());
        stringBuffer2.append(AdViewConstants.QUESTION);
        stringBuffer2.append(ISANDROID_PARAM);
        for (String encode : trackingPixels) {
            stringBuffer2.append(AdViewConstants.AMP);
            stringBuffer2.append(TRACKING_PIXEL_PARAM);
            stringBuffer2.append(URLEncoder.encode(encode));
        }
        if (ad.getClickUrl() != null && ad.getClickUrl().length() > 0) {
            stringBuffer2.append(AdViewConstants.AMP);
            stringBuffer2.append(CLICK_URL_PARAM);
            stringBuffer2.append(URLEncoder.encode(ad.getClickUrl()));
        }
        stringBuffer2.append(AdViewConstants.AMP);
        stringBuffer2.append(URL_PARAM);
        stringBuffer2.append(URLEncoder.encode(stringBuffer.toString()));
        CookieManager.getInstance().setCookie(QWAPI_DOMAIN, CLIENT_ID_COOKIE_PARAM + DeviceContext.getDeviceId(getContext()));
        CookieSyncManager.getInstance().sync();
        loadUrl(stringBuffer2.toString());
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeAllViews();
        if (!this.userClosed) {
            destroy();
        }
        this.userClosed = false;
    }
}
