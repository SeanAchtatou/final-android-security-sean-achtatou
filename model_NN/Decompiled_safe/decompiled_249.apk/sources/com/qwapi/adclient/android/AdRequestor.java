package com.qwapi.adclient.android;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Process;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import com.qwapi.adclient.android.AdApiConfig;
import com.qwapi.adclient.android.data.Ad;
import com.qwapi.adclient.android.data.AdResponse;
import com.qwapi.adclient.android.requestparams.AnimationType;
import com.qwapi.adclient.android.requestparams.DisplayMode;
import com.qwapi.adclient.android.requestparams.MediaType;
import com.qwapi.adclient.android.requestparams.Placement;
import com.qwapi.adclient.android.requestparams.RequestMode;
import com.qwapi.adclient.android.view.AdViewConstants;
import com.qwapi.adclient.android.view.QWAdView;
import java.util.List;
import net.mony.more.qaqad.R;

public class AdRequestor extends Activity {
    private void updatePreferences() {
        int i;
        int i2;
        int i3;
        int i4;
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        QWAdView qWAdView = (QWAdView) findViewById(R.array.DOWNLOAD_PAUSE);
        Boolean valueOf = Boolean.valueOf(defaultSharedPreferences.getBoolean(AdRequestorPreferences.BATCH_PREFERENCE, false));
        if (valueOf.booleanValue()) {
            qWAdView.setRequestMode(RequestMode.batch);
        } else {
            qWAdView.setRequestMode(RequestMode.single);
        }
        qWAdView.getMediaTypes().clear();
        Boolean valueOf2 = Boolean.valueOf(defaultSharedPreferences.getBoolean("banner", true));
        Boolean valueOf3 = Boolean.valueOf(defaultSharedPreferences.getBoolean("interstitial", false));
        Boolean valueOf4 = Boolean.valueOf(defaultSharedPreferences.getBoolean("text", false));
        Boolean valueOf5 = Boolean.valueOf(defaultSharedPreferences.getBoolean(AdRequestorPreferences.EXPANDABLE_PREFERENCE, false));
        int i5 = 1;
        if (valueOf.booleanValue()) {
            int intValue = Integer.valueOf(defaultSharedPreferences.getString(AdRequestorPreferences.BANNER_COUNT_PREFERENCE, "5")).intValue();
            int intValue2 = Integer.valueOf(defaultSharedPreferences.getString(AdRequestorPreferences.INTERSTITIAL_COUNT_PREFERENCE, "5")).intValue();
            int intValue3 = Integer.valueOf(defaultSharedPreferences.getString(AdRequestorPreferences.TEXT_COUNT_PREFERENCE, "5")).intValue();
            i2 = intValue;
            i = Integer.valueOf(defaultSharedPreferences.getString(AdRequestorPreferences.EXPANDABLE_COUNT_PREFERENCE, "5")).intValue();
            int i6 = intValue2;
            i3 = intValue3;
            i5 = i6;
        } else {
            i = 1;
            i2 = 1;
            i3 = 1;
        }
        if (valueOf2.booleanValue()) {
            qWAdView.addMediaType(MediaType.banner, i2);
        }
        if (valueOf3.booleanValue()) {
            qWAdView.addMediaType(MediaType.interstitial, i5);
        }
        if (valueOf4.booleanValue()) {
            qWAdView.addMediaType(MediaType.text, i3);
        }
        if (valueOf5.booleanValue()) {
            qWAdView.addMediaType(MediaType.expandable, i);
        }
        String string = defaultSharedPreferences.getString(AdRequestorPreferences.PLACEMENT_PREFERENCE, null);
        if (string == null) {
            qWAdView.setPlacement(null);
        } else if (string.equals("None")) {
            qWAdView.setPlacement(null);
        } else if (string.equals("Top")) {
            qWAdView.setPlacement(Placement.top);
        } else if (string.equals("Middle")) {
            qWAdView.setPlacement(Placement.middle);
        } else if (string.equals("Bottom")) {
            qWAdView.setPlacement(Placement.bottom);
        }
        String string2 = defaultSharedPreferences.getString(AdRequestorPreferences.SECTION_PREFERENCE, null);
        if (string2 == null || string2.length() <= 0) {
            qWAdView.setSection(null);
        } else {
            qWAdView.setSection(string2);
        }
        String string3 = defaultSharedPreferences.getString(AdRequestorPreferences.ANINMATION_PREFERENCE, "Slide");
        if (AnimationType.slide.toString().equals(string3)) {
            qWAdView.setAnimationType(this, AnimationType.slide);
        } else if (AnimationType.fade.toString().equals(string3)) {
            qWAdView.setAnimationType(this, AnimationType.fade);
        } else if (AnimationType.pushLeft.toString().equals(string3)) {
            qWAdView.setAnimationType(this, AnimationType.pushLeft);
        } else if (AnimationType.pushUp.toString().equals(string3)) {
            qWAdView.setAnimationType(this, AnimationType.pushUp);
        } else if (AnimationType.hyperspace.toString().equals(string3)) {
            qWAdView.setAnimationType(this, AnimationType.hyperspace);
        } else {
            qWAdView.setAnimationType(this, AnimationType.slide);
        }
        qWAdView.setTestMode(Boolean.valueOf(defaultSharedPreferences.getBoolean(AdRequestorPreferences.TESTFLAG_PREFERENCE, true)).booleanValue());
        qWAdView.setPublisherId(defaultSharedPreferences.getString(AdRequestorPreferences.PUBLISHER_ID_PREFERENCE, "b33eb94ffbce48bd8301a5476039cb63"));
        qWAdView.setSiteId(defaultSharedPreferences.getString(AdRequestorPreferences.SITE_ID_PREFERENCE, "itest3"));
        String string4 = defaultSharedPreferences.getString(AdRequestorPreferences.DISPLAY_MODE_PREFERENCE, "normal");
        String string5 = defaultSharedPreferences.getString(AdRequestorPreferences.REFRESH_INTERVAL_PREFERENCE, "30");
        if (string5 != null) {
            try {
                i4 = Integer.parseInt(string5);
                if (i4 < 30) {
                    i4 = 30;
                }
            } catch (Exception e) {
                i4 = 30;
            }
        } else {
            i4 = 30;
        }
        Button button = (Button) findViewById(R.array.DOWNLOAD_RESTART);
        if (DisplayMode.autoRotate.toString().equals(string4)) {
            qWAdView.setDisplayMode(DisplayMode.autoRotate);
            qWAdView.setAdInterval(i4);
            qWAdView.resetAutoRefresh();
            button.setVisibility(8);
        } else if (DisplayMode.normal.toString().equals(string4)) {
            qWAdView.setDisplayMode(DisplayMode.normal);
            qWAdView.setAdInterval(0);
            qWAdView.resetAutoRefresh();
            button.setVisibility(0);
        } else if (DisplayMode.aged.toString().equals(string4)) {
            qWAdView.setDisplayMode(DisplayMode.aged);
            qWAdView.setAdInterval(i4);
            qWAdView.resetAutoRefresh();
            button.setVisibility(8);
        } else {
            qWAdView.resetAutoRefresh();
        }
        String string6 = defaultSharedPreferences.getString(AdRequestorPreferences.EXECUTION_MODE_PREFERENCE, "STAGING");
        if ("STAGING".equals(string6)) {
            AdApiConfig.setExecutionMode(AdApiConfig.ExecutionMode.STAGING);
        } else if ("QA".equals(string6)) {
            AdApiConfig.setExecutionMode(AdApiConfig.ExecutionMode.QA);
        } else if ("LIVE".equals(string6)) {
            AdApiConfig.setExecutionMode(AdApiConfig.ExecutionMode.LIVE);
        } else {
            AdApiConfig.setExecutionMode(AdApiConfig.ExecutionMode.STAGING);
        }
        String string7 = defaultSharedPreferences.getString(AdRequestorPreferences.BACKGROUND_COLOR_PREFERENCE, "black");
        if (string7 != null) {
            try {
                qWAdView.setBackgroundColor(Color.parseColor(string7));
            } catch (IllegalArgumentException e2) {
                Log.e(AdApiConstants.SDK, "Invalid color format in preferences for background color defaulting to black");
                qWAdView.setBackgroundColor(-16777216);
            }
        }
        String string8 = defaultSharedPreferences.getString(AdRequestorPreferences.TEXT_COLOR_PREFERENCE, "blue");
        if (string8 != null) {
            try {
                qWAdView.setTextColor(Color.parseColor(string8));
            } catch (IllegalArgumentException e3) {
                Log.e(AdApiConstants.SDK, "Invalid color format in preferences for textColor defaulting to blue");
                qWAdView.setTextColor(-16776961);
            }
        }
        qWAdView.displayNextAd();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 1) {
            updatePreferences();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PreferenceManager.setDefaultValues(this, R.raw.feed, false);
        AdApiConfig.setExecutionMode(AdApiConfig.ExecutionMode.STAGING);
        setContentView((int) R.layout.about);
        TextView textView = (TextView) findViewById(R.array.DOWNLOAD_RESUME);
        textView.setBackgroundColor(-3355444);
        textView.setTextColor(-16777216);
        textView.setVerticalFadingEdgeEnabled(true);
        textView.setGravity(17);
        QWAdView qWAdView = (QWAdView) findViewById(R.array.DOWNLOAD_PAUSE);
        Button button = (Button) findViewById(R.array.DOWNLOAD_RESTART);
        button.setGravity(1);
        button.setGravity(17);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ((QWAdView) AdRequestor.this.findViewById(R.array.DOWNLOAD_PAUSE)).displayNextAd();
            }
        });
        Button button2 = (Button) findViewById(R.array.DOWNLOADED_MENU);
        button2.setGravity(1);
        button2.setGravity(17);
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClassName("com.qwapi.adclient.android", "com.qwapi.adclient.android.AdRequestorPreferences");
                AdRequestor.this.startActivityForResult(intent, 1);
            }
        });
        Button button3 = (Button) findViewById(R.array.SEARCH_MENU);
        button3.setGravity(1);
        button3.setGravity(17);
        button3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                List<AdResponse> cachedBatchAds = ((QWAdView) AdRequestor.this.findViewById(R.array.DOWNLOAD_PAUSE)).getCachedBatchAds();
                if (cachedBatchAds == null) {
                    WebView webView = (WebView) AdRequestor.this.findViewById(R.id.cachedAdsView);
                    webView.clearView();
                    webView.loadData("<div style='font-size : 10px;'>Batch Mode Not Enabled</div>", AdViewConstants.TEXT_HTML, "UTF-8");
                    return;
                }
                WebView webView2 = (WebView) AdRequestor.this.findViewById(R.id.cachedAdsView);
                webView2.clearView();
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("<table style='font-size:10px;' border='1'><tr><td><b>AdId</b></td><td><b>AdType</b></td><td><b>Impression Count</b></td></tr>");
                for (AdResponse next : cachedBatchAds) {
                    stringBuffer.append("<tr><td>");
                    stringBuffer.append(next.getAd(0).getId());
                    stringBuffer.append("</td><td>");
                    String adType = next.getAd(0).getAdType();
                    if (adType.equals(Ad.AD_TYPE_EXPANDABLE_BANNER)) {
                        adType = AdRequestorPreferences.EXPANDABLE_PREFERENCE;
                    } else if (adType.equals(Ad.AD_TYPE_ANIMATED_BANNER)) {
                        adType = "animated";
                    }
                    stringBuffer.append(adType);
                    stringBuffer.append("</td><td>");
                    stringBuffer.append(next.getAd(0).getImpressions());
                    stringBuffer.append("</td></tr>");
                }
                stringBuffer.append("</table>");
                webView2.loadDataWithBaseURL(null, stringBuffer.toString(), AdViewConstants.TEXT_HTML, "utf-8", null);
            }
        });
        Button button4 = (Button) findViewById(R.array.ring_types);
        button4.setText(new String("generate memory dump"));
        button4.setGravity(1);
        button4.setGravity(17);
        button4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Process.sendSignal(Process.myPid(), 10);
            }
        });
        updatePreferences();
    }
}
