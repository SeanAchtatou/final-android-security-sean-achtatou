package com.qwapi.adclient.android.view;

public class QWAdViewException extends Exception {
    private static final long serialVersionUID = -1;

    public QWAdViewException() {
    }

    public QWAdViewException(String str) {
        super(str);
    }

    public QWAdViewException(String str, Throwable th) {
        super(str, th);
    }

    public QWAdViewException(Throwable th) {
        super(th);
    }
}
