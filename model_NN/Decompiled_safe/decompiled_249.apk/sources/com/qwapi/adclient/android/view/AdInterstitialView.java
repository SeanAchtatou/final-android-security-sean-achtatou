package com.qwapi.adclient.android.view;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.qwapi.adclient.android.data.Ad;
import net.mony.more.qaqad.R;

public class AdInterstitialView extends LinearLayout {
    /* access modifiers changed from: private */
    public ImageView imgSkip;
    private SkipListener skipListener;
    /* access modifiers changed from: private */
    public TextView textView;
    /* access modifiers changed from: private */
    public AdInterstitialWebView webView;

    public static class AdInterstitialWebView extends AdWebView {
        public AdInterstitialWebView(Context context, AttributeSet attributeSet, Ad ad, EventDispatcher eventDispatcher, QWAdView qWAdView) {
            super(context, attributeSet, ad, eventDispatcher, qWAdView);
        }

        public AdInterstitialWebView(Context context, Ad ad, EventDispatcher eventDispatcher, QWAdView qWAdView) {
            super(context, ad, eventDispatcher, qWAdView);
        }

        /* access modifiers changed from: protected */
        public void load(Ad ad) {
            String xhtmlAdContent;
            String str;
            if (ad == null) {
                str = AdViewConstants.EMPTY_DATA;
            } else {
                String textColor = ((QWAdView) this.wAdView.get()).getTextColor();
                int i = getResources().getConfiguration().orientation;
                getResources().getConfiguration();
                if (i != 2) {
                    xhtmlAdContent = "<div><br/> <br/><br/><br/><br/><br/></div>" + ad.getXhtmlAdContent(textColor, true);
                } else {
                    xhtmlAdContent = ad.getXhtmlAdContent(textColor, false);
                }
                Log.d("QuattroWirelessSDK/2.1 interstitial:", xhtmlAdContent);
                str = xhtmlAdContent;
            }
            loadDataWithBaseURL(null, str, AdViewConstants.TEXT_HTML, "utf-8", null);
        }
    }

    public AdInterstitialView(Context context, AttributeSet attributeSet, Ad ad, EventDispatcher eventDispatcher, QWAdView qWAdView) {
        super(context, attributeSet);
        init(context, attributeSet, ad, eventDispatcher, qWAdView);
    }

    public AdInterstitialView(Context context, Ad ad, EventDispatcher eventDispatcher, QWAdView qWAdView) {
        super(context);
        setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        setOrientation(1);
        init(context, null, ad, eventDispatcher, qWAdView);
        setBackgroundColor(0);
    }

    private void init(Context context, AttributeSet attributeSet, Ad ad, EventDispatcher eventDispatcher, QWAdView qWAdView) {
        this.webView = new AdInterstitialWebView(context, attributeSet, ad, eventDispatcher, qWAdView);
        this.webView.setPadding(0, 0, 0, 0);
        this.webView.getSettings().setCacheMode(0);
        this.imgSkip = new ImageView(context);
        this.imgSkip.setImageResource(R.drawable.appwidget_pause_normal);
        this.imgSkip.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AdInterstitialView.this.webView.setVisibility(8);
                AdInterstitialView.this.imgSkip.setVisibility(8);
                AdInterstitialView.this.textView.setVisibility(8);
                AdInterstitialView.this.invalidate();
                AdInterstitialView.this.getParent().invalidateChild(AdInterstitialView.this, new Rect(AdInterstitialView.this.getLeft(), AdInterstitialView.this.getTop(), AdInterstitialView.this.getRight(), AdInterstitialView.this.getBottom()));
                if (AdInterstitialView.this.getSkipListener() != null) {
                    AdInterstitialView.this.getSkipListener().onSkip();
                }
            }
        });
        this.webView.setBackgroundColor(0);
        this.textView = new TextView(context);
        this.textView.setPadding(5, 50, 5, 70);
        this.textView.setBackgroundColor(0);
        addView(this.webView, new LinearLayout.LayoutParams(-1, -1));
        addView(this.imgSkip, new LinearLayout.LayoutParams(-2, -2));
        int i = getResources().getConfiguration().orientation;
        getResources().getConfiguration();
        if (i != 2) {
            addView(this.textView, new LinearLayout.LayoutParams(-1, -1));
            setPadding(5, 10, 5, 10);
        }
        setGravity(17);
        invalidate();
    }

    public void cleanup() {
        removeAllViews();
        if (this.webView != null) {
            this.webView.setVisibility(8);
            this.webView.destroy();
            this.webView = null;
        }
        if (this.imgSkip != null) {
            this.imgSkip.setVisibility(8);
            this.imgSkip = null;
        }
        if (this.textView != null) {
            this.textView.setVisibility(8);
            this.textView = null;
        }
        if (this.skipListener != null) {
            this.skipListener = null;
        }
    }

    public SkipListener getSkipListener() {
        return this.skipListener;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        cleanup();
    }

    public void setSkipListener(SkipListener skipListener2) {
        this.skipListener = skipListener2;
    }
}
