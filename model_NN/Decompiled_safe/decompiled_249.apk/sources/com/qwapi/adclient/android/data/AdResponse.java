package com.qwapi.adclient.android.data;

import android.util.Log;
import java.util.ArrayList;
import java.util.List;

public class AdResponse {
    private List<Ad> ads;
    private String batchId;
    private Status status;

    public AdResponse() {
    }

    public AdResponse(Ad ad, Status status2) {
        init(ad, status2);
    }

    public AdResponse(Ad ad, Status status2, String str) {
        init(ad, status2);
        this.batchId = str;
    }

    public static AdResponse createParseErrorAdReseponse(String str) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new ErrorMessage(ErrorMessage.PARSE_ERROR, str, str));
        return new AdResponse(new Ad(), new Status(Status.FAILURE, arrayList));
    }

    private void init(Ad ad, Status status2) {
        this.ads = new ArrayList();
        this.status = status2;
        addAd(ad);
    }

    public synchronized void addAd(Ad ad) {
        if (ad != null) {
            this.ads.add(ad);
            ad.setStatus(this.status);
        }
    }

    public final int count() {
        return this.ads.size();
    }

    public Ad getAd(int i) {
        if (count() < 1) {
            return null;
        }
        try {
            return this.ads.get(i);
        } catch (Exception e) {
            Log.e("ad list error", e.getMessage());
            return null;
        }
    }

    public List<Ad> getAds() {
        return this.ads;
    }

    public String getBatchId() {
        return this.batchId;
    }

    public Status getStatus() {
        return this.status;
    }

    public boolean isSuccessful() {
        return this.status.isSuccessful() && count() != 0;
    }

    public synchronized void setAd(int i, Ad ad) {
        if (ad != null) {
            this.ads.set(i, ad);
            ad.setStatus(this.status);
        }
    }

    public void setAd(Ad ad) {
        setAd(0, ad);
    }

    public void setStatus(Status status2) {
        this.status = status2;
    }

    public boolean updateAd() {
        Ad ad = getAd(0);
        if (ad == null) {
            return false;
        }
        ad.served();
        return ad.getStatus().isSuccessful() && !ad.isExpired() && ad.getImpressions() >= 1;
    }
}
