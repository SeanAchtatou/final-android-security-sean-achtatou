package com.qwapi.adclient.android.data;

public class ErrorMessage {
    public static int PARSE_ERROR = 9999;
    String description;
    String message;
    int msgCode;

    public ErrorMessage(int i, String str, String str2) {
        this.msgCode = i;
        this.description = str;
        this.message = str2;
    }

    public String getDescription() {
        return this.description;
    }

    public String getMessage() {
        return this.message;
    }

    public int getMsgCode() {
        return this.msgCode;
    }
}
