package com.qwapi.adclient.android.service;

import android.util.Log;
import com.qwapi.adclient.android.data.Ad;
import com.qwapi.adclient.android.data.AdResponse;
import com.qwapi.adclient.android.data.Data;
import com.qwapi.adclient.android.data.ErrorMessage;
import com.qwapi.adclient.android.data.Image;
import com.qwapi.adclient.android.data.Status;
import com.qwapi.adclient.android.data.Text;
import com.qwapi.adclient.android.utils.Utils;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class AdXmlResponseConverter {
    private static final String ATTRIBUTE_NAME_BATCHID = "batchid";
    private static final String ATTRIBUTE_NAME_CODE = "code";
    private static final String ATTRIBUTE_NAME_DESCRIPTION = "description";
    private static final String ATTRIBUTE_NAME_EXPIRY = "expiry";
    private static final String ATTRIBUTE_NAME_ID = "id";
    private static final String ATTRIBUTE_NAME_IMPRESSIONS = "availableImpressions";
    private static final String ATTRIBUTE_NAME_TYPE = "type";
    private static final String ELEMENT_NAME_ACTION_TEXT = "actionText";
    private static final String ELEMENT_NAME_AD = "ad";
    private static final String ELEMENT_NAME_ADS = "ads";
    private static final String ELEMENT_NAME_ALT_TEXT = "altText";
    private static final String ELEMENT_NAME_ANIMATED_BANNER = "animatedbanner";
    private static final String ELEMENT_NAME_BANNER = "banner";
    private static final String ELEMENT_NAME_BODY = "body";
    private static final String ELEMENT_NAME_BODY_TEXT = "bodyText";
    private static final String ELEMENT_NAME_CLICK_URL = "clickUrl";
    private static final String ELEMENT_NAME_DATA = "data";
    private static final String ELEMENT_NAME_EXPANDABLE = "expandablebanner";
    private static final String ELEMENT_NAME_EXTERNALLY_HOSTED = "externallyHosted";
    private static final String ELEMENT_NAME_HEALDINE = "headline";
    private static final String ELEMENT_NAME_HEIGHT = "height";
    private static final String ELEMENT_NAME_IMAGE = "image";
    private static final String ELEMENT_NAME_INTERSTITIAL = "interstitial";
    private static final String ELEMENT_NAME_ISBOT = "isBot";
    private static final String ELEMENT_NAME_MESSAGE = "message";
    private static final String ELEMENT_NAME_MESSAGES = "messages";
    private static final String ELEMENT_NAME_STATUS = "status";
    private static final String ELEMENT_NAME_TEXT = "text";
    private static final String ELEMENT_NAME_TRACKING_PIXELS = "trackingPixels";
    private static final String ELEMENT_NAME_TRACKING_PIXEL_URL = "trackingPixelUrl";
    private static final String ELEMENT_NAME_URL = "url";
    private static final String ELEMENT_NAME_WIDTH = "width";

    public static AdResponse getAdResponse(String str) {
        return parseXmlResponse(str);
    }

    private static boolean getBooleanValue(Element element, String str) {
        try {
            return Boolean.getBoolean(getTextValue(element, str));
        } catch (Exception e) {
            return false;
        }
    }

    private static Element getElementByTagName(Element element, String str) {
        NodeList elementsByTagName = element.getElementsByTagName(str);
        if (elementsByTagName == null || elementsByTagName.getLength() <= 0) {
            return null;
        }
        return (Element) elementsByTagName.item(0);
    }

    private static int getIntValue(Element element, String str) {
        try {
            return Integer.parseInt(getTextValue(element, str));
        } catch (Exception e) {
            return 0;
        }
    }

    private static String getTextValue(Element element, String str) {
        return getTextValue(element, str, false);
    }

    private static String getTextValue(Element element, String str, boolean z) {
        NodeList elementsByTagName = element.getElementsByTagName(str);
        if (elementsByTagName == null || elementsByTagName.getLength() <= 0) {
            return Utils.EMPTY_STRING;
        }
        Element element2 = (Element) elementsByTagName.item(0);
        if (element2.getFirstChild() == null) {
            return Utils.EMPTY_STRING;
        }
        if (!z) {
            return element2.getFirstChild().getNodeValue();
        }
        NodeList childNodes = element2.getChildNodes();
        String str2 = Utils.EMPTY_STRING;
        for (int i = 0; i < childNodes.getLength(); i++) {
            str2 = str2 + childNodes.item(i).getNodeValue();
        }
        return str2;
    }

    private static List<String> getValues(Element element, String str) {
        NodeList elementsByTagName;
        ArrayList arrayList = new ArrayList();
        if (!(element == null || (elementsByTagName = element.getElementsByTagName(str)) == null || elementsByTagName.getLength() <= 0)) {
            int length = elementsByTagName.getLength();
            for (int i = 0; i < length; i++) {
                Element element2 = (Element) elementsByTagName.item(i);
                if (element2.getFirstChild() != null) {
                    arrayList.add(getTextValue(element2, str, true));
                }
            }
        }
        return arrayList;
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:47:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.qwapi.adclient.android.data.AdResponse parseDocument(org.w3c.dom.Document r11) throws com.qwapi.adclient.android.service.InvalidAdResponseException {
        /*
            r10 = 0
            r6 = 0
            org.w3c.dom.Element r0 = r11.getDocumentElement()
            java.lang.String r1 = "status"
            org.w3c.dom.NodeList r1 = r0.getElementsByTagName(r1)
            if (r1 == 0) goto L_0x00ab
            int r2 = r1.getLength()
            if (r2 <= 0) goto L_0x00ab
            org.w3c.dom.Node r11 = r1.item(r6)
            org.w3c.dom.Element r11 = (org.w3c.dom.Element) r11
            com.qwapi.adclient.android.data.Status r1 = processStatusElement(r11)
        L_0x001e:
            if (r1 == 0) goto L_0x00a3
            boolean r2 = r1.isSuccessful()
            if (r2 == 0) goto L_0x00a3
            java.lang.String r2 = "ads"
            org.w3c.dom.NodeList r0 = r0.getElementsByTagName(r2)
            if (r0 == 0) goto L_0x00a9
            int r2 = r0.getLength()
            if (r2 <= 0) goto L_0x00a9
            org.w3c.dom.Node r11 = r0.item(r6)
            org.w3c.dom.Element r11 = (org.w3c.dom.Element) r11
            java.lang.String r0 = "batchid"
            java.lang.String r0 = r11.getAttribute(r0)
            java.lang.String r2 = "expiry"
            java.lang.String r2 = r11.getAttribute(r2)
            if (r0 == 0) goto L_0x008d
            int r3 = r0.length()
            if (r3 <= 0) goto L_0x008d
            r3 = 1
        L_0x004f:
            java.lang.String r4 = "ad"
            org.w3c.dom.NodeList r4 = r11.getElementsByTagName(r4)
            if (r4 == 0) goto L_0x00a9
            int r5 = r4.getLength()
            if (r5 <= 0) goto L_0x00a9
            r5 = r6
            r6 = r10
        L_0x005f:
            int r7 = r4.getLength()
            if (r5 >= r7) goto L_0x009a
            org.w3c.dom.Node r11 = r4.item(r5)
            org.w3c.dom.Element r11 = (org.w3c.dom.Element) r11
            com.qwapi.adclient.android.data.Ad r7 = processAd(r11)
            if (r3 == 0) goto L_0x0083
            r7.setExpiry(r2)
            java.lang.String r8 = "availableImpressions"
            java.lang.String r8 = r11.getAttribute(r8)
            if (r8 == 0) goto L_0x0083
            int r9 = java.lang.Integer.parseInt(r8)     // Catch:{ Exception -> 0x008f }
            r7.setImpressionCount(r9)     // Catch:{ Exception -> 0x008f }
        L_0x0083:
            if (r6 != 0) goto L_0x0096
            com.qwapi.adclient.android.data.AdResponse r6 = new com.qwapi.adclient.android.data.AdResponse
            r6.<init>(r7, r1, r0)
        L_0x008a:
            int r5 = r5 + 1
            goto L_0x005f
        L_0x008d:
            r3 = r6
            goto L_0x004f
        L_0x008f:
            r9 = move-exception
            java.lang.String r9 = "invalid impression count"
            android.util.Log.e(r9, r8)
            goto L_0x0083
        L_0x0096:
            r6.addAd(r7)
            goto L_0x008a
        L_0x009a:
            r0 = r6
        L_0x009b:
            if (r0 != 0) goto L_0x00a2
            com.qwapi.adclient.android.data.AdResponse r0 = new com.qwapi.adclient.android.data.AdResponse
            r0.<init>(r10, r1)
        L_0x00a2:
            return r0
        L_0x00a3:
            com.qwapi.adclient.android.data.AdResponse r0 = new com.qwapi.adclient.android.data.AdResponse
            r0.<init>(r10, r1)
            goto L_0x00a2
        L_0x00a9:
            r0 = r10
            goto L_0x009b
        L_0x00ab:
            r1 = r10
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qwapi.adclient.android.service.AdXmlResponseConverter.parseDocument(org.w3c.dom.Document):com.qwapi.adclient.android.data.AdResponse");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    private static AdResponse parseXmlResponse(String str) {
        String message;
        ParserConfigurationException parserConfigurationException;
        try {
            return parseDocument(DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(str))));
        } catch (ParserConfigurationException e) {
            message = e.getMessage();
            parserConfigurationException = e;
        } catch (SAXException e2) {
            message = e2.getMessage();
            parserConfigurationException = e2;
        } catch (IOException e3) {
            message = e3.getMessage();
            parserConfigurationException = e3;
        } catch (InvalidAdResponseException e4) {
            message = e4.getMessage();
            parserConfigurationException = e4;
        } catch (Throwable th) {
            message = th.getMessage();
            parserConfigurationException = th;
        }
        if (parserConfigurationException != null) {
            Log.e("Problem parsing response", message, parserConfigurationException);
        }
        return AdResponse.createParseErrorAdReseponse(message);
    }

    private static Ad processAd(Element element) throws InvalidAdResponseException {
        Ad ad = new Ad();
        ad.setId(element.getAttribute(ATTRIBUTE_NAME_ID));
        ad.setBot(getBooleanValue(element, ELEMENT_NAME_ISBOT));
        ad.setClickUrl(getTextValue(element, ELEMENT_NAME_CLICK_URL, true));
        Element elementByTagName = getElementByTagName(element, ELEMENT_NAME_BODY);
        String str = null;
        if (elementByTagName != null) {
            str = elementByTagName.getAttribute(ATTRIBUTE_NAME_TYPE);
        }
        if (Utils.isGoodString(str)) {
            ad.setAdType(str);
            if ("banner".equalsIgnoreCase(str)) {
                processImageAdElement(ad, getElementByTagName(elementByTagName, "banner"), str);
            } else if ("animatedbanner".equalsIgnoreCase(str)) {
                processImageAdElement(ad, getElementByTagName(elementByTagName, "animatedbanner"), str);
            } else if ("interstitial".equalsIgnoreCase(str)) {
                processImageAdElement(ad, getElementByTagName(elementByTagName, "interstitial"), str);
            } else if ("text".equalsIgnoreCase(str)) {
                processTextElement(ad, getElementByTagName(elementByTagName, "text"));
            } else if ("expandablebanner".equalsIgnoreCase(str)) {
                processExpandableElement(ad, getElementByTagName(elementByTagName, "expandablebanner"));
            }
            return ad;
        }
        throw new InvalidAdResponseException("Adtype is invalid:" + str);
    }

    private static void processExpandableElement(Ad ad, Element element) {
        if (element != null && ad != null) {
            Element elementByTagName = getElementByTagName(element, ELEMENT_NAME_DATA);
            ad.setData(new Data(getTextValue(elementByTagName, ELEMENT_NAME_URL, true), getIntValue(elementByTagName, ELEMENT_NAME_WIDTH), getIntValue(elementByTagName, ELEMENT_NAME_HEIGHT)));
            ad.setTrackingPixels(getValues(getElementByTagName(element, ELEMENT_NAME_TRACKING_PIXELS), ELEMENT_NAME_TRACKING_PIXEL_URL));
            ad.setExternallyHosted(getBooleanValue(element, ELEMENT_NAME_EXTERNALLY_HOSTED));
        }
    }

    private static void processImageAdElement(Ad ad, Element element, String str) {
        if (element != null && ad != null) {
            Element element2 = null;
            if ("banner".equals(str)) {
                element2 = getElementByTagName(element, "banner");
            } else if ("animatedbanner".equals(str)) {
                element2 = getElementByTagName(element, "animatedbanner");
            } else if ("interstitial".equals(str)) {
                element2 = getElementByTagName(element, ELEMENT_NAME_IMAGE);
            }
            String textValue = getTextValue(element2, ELEMENT_NAME_URL, true);
            ad.setImage(new Image(Image.encodeUrl(textValue), getIntValue(element2, ELEMENT_NAME_WIDTH), getIntValue(element2, ELEMENT_NAME_HEIGHT), getTextValue(element2, ELEMENT_NAME_ALT_TEXT)));
            ad.setTrackingPixels(getValues(getElementByTagName(element, ELEMENT_NAME_TRACKING_PIXELS), ELEMENT_NAME_TRACKING_PIXEL_URL));
            ad.setActionText(getTextValue(element, ELEMENT_NAME_ACTION_TEXT));
            ad.setExternallyHosted(getBooleanValue(element, ELEMENT_NAME_EXTERNALLY_HOSTED));
        }
    }

    private static Status processStatusElement(Element element) {
        NodeList elementsByTagName;
        if (element == null) {
            return null;
        }
        String attribute = element.getAttribute(ATTRIBUTE_NAME_CODE);
        ArrayList arrayList = new ArrayList();
        Element elementByTagName = getElementByTagName(element, ELEMENT_NAME_MESSAGES);
        if (!(elementByTagName == null || (elementsByTagName = elementByTagName.getElementsByTagName(ELEMENT_NAME_MESSAGE)) == null || elementsByTagName.getLength() <= 0)) {
            int length = elementsByTagName.getLength();
            int i = 0;
            for (int i2 = 0; i2 < length; i2++) {
                Element element2 = (Element) elementsByTagName.item(i2);
                String attribute2 = element2.getAttribute(ATTRIBUTE_NAME_CODE);
                try {
                    i = Integer.parseInt(attribute2);
                } catch (Exception e) {
                    Log.e("invalid msg code", attribute2);
                }
                arrayList.add(new ErrorMessage(i, element2.getAttribute(ATTRIBUTE_NAME_DESCRIPTION), element2.getFirstChild().getNodeValue()));
            }
        }
        return new Status(attribute, arrayList);
    }

    private static void processTextElement(Ad ad, Element element) {
        if (element != null && ad != null) {
            ad.setText(new Text(getTextValue(element, ELEMENT_NAME_BODY_TEXT, false), getTextValue(element, ELEMENT_NAME_HEALDINE, false)));
            ad.setTrackingPixels(getValues(getElementByTagName(element, ELEMENT_NAME_TRACKING_PIXELS), ELEMENT_NAME_TRACKING_PIXEL_URL));
            ad.setExternallyHosted(getBooleanValue(element, ELEMENT_NAME_EXTERNALLY_HOSTED));
        }
    }
}
