package com.qwapi.adclient.android.service;

import android.content.Context;
import android.util.Log;
import com.qwapi.adclient.android.AdApiConstants;
import com.qwapi.adclient.android.data.Ad;
import com.qwapi.adclient.android.data.AdResponse;
import com.qwapi.adclient.android.db.DBHelper;
import com.qwapi.adclient.android.requestparams.AdRequestParams;
import com.qwapi.adclient.android.requestparams.MediaType;
import com.qwapi.adclient.android.requestparams.Placement;
import com.qwapi.adclient.android.utils.HttpResponse;
import java.util.Collection;
import java.util.List;

public class BatchAdRequestService extends SingleAdRequestService {
    private int _size = 10;
    /* access modifiers changed from: private */
    public DBHelper dbHelper;

    public BatchAdRequestService(Context context) {
        this.dbHelper = new DBHelper(context);
    }

    private synchronized void deleteAd(AdResponse adResponse) {
        if (adResponse.getAds() != null && adResponse.getAds().size() > 0) {
            this.dbHelper.deleteAd(adResponse.getAd(0));
        }
    }

    private synchronized void deleteExpiredAds() {
        new Thread(new Runnable() {
            public void run() {
                BatchAdRequestService.this.dbHelper.deleteExpiredAds();
            }
        }).start();
    }

    private synchronized AdResponse readAd(Collection<MediaType> collection, Placement placement, String str) {
        return this.dbHelper.getAd(collection, placement, str);
    }

    private synchronized void updateAd(AdResponse adResponse, AdRequestParams adRequestParams) {
        this.dbHelper.updateAd(adResponse.getAd(0), adResponse.getBatchId(), adRequestParams.getPlacement(), adRequestParams.getSection());
    }

    private synchronized void writeAds(AdResponse adResponse, AdRequestParams adRequestParams) {
        if (adResponse == null || adRequestParams == null) {
            Log.e(AdApiConstants.SDK, "Unable to write Ads");
        } else {
            for (Ad insertAd : adResponse.getAds()) {
                this.dbHelper.insertAd(insertAd, adResponse.getBatchId(), adRequestParams.getPlacement(), adRequestParams.getSection());
            }
        }
    }

    public synchronized void closeDatabase() {
        this.dbHelper.cleanup();
    }

    public AdResponse getAd(AdRequestParams adRequestParams) {
        HttpResponse response;
        String response2;
        AdResponse readAd = readAd(adRequestParams.getMediaTypes().keySet(), adRequestParams.getPlacement(), adRequestParams.getSection());
        if (!(readAd != null || adRequestParams == null || (response = getResponse(adRequestParams, true, this._size)) == null || (response2 = response.getResponse()) == null)) {
            long currentTimeMillis = System.currentTimeMillis();
            readAd = AdXmlResponseConverter.getAdResponse(response2);
            if (readAd.getStatus().isSuccessful() && readAd.getAds() != null && readAd.getAds().size() > 0) {
                writeAds(readAd, adRequestParams);
                readAd = readAd(adRequestParams.getMediaTypes().keySet(), adRequestParams.getPlacement(), adRequestParams.getSection());
                deleteExpiredAds();
                Log.d(AdApiConstants.SDK, "batch ads returned : " + (readAd != null ? Integer.valueOf(readAd.count()) : "null adResponse") + " in " + Long.toString(System.currentTimeMillis() - currentTimeMillis));
            }
        }
        if (readAd != null) {
            if (readAd.updateAd()) {
                updateAd(readAd, adRequestParams);
            } else {
                deleteAd(readAd);
            }
        }
        return readAd;
    }

    public synchronized List<AdResponse> getAllAds() {
        return this.dbHelper.getAllAds();
    }

    public void setBatchSize(int i) {
        this._size = (i < 2 || i > 15) ? 10 : i;
    }
}
