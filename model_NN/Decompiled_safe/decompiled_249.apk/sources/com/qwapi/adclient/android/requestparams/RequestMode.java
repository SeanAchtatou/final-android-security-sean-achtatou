package com.qwapi.adclient.android.requestparams;

import com.qwapi.adclient.android.AdRequestorPreferences;

public enum RequestMode {
    single,
    batch;

    public String toString() {
        switch (ordinal()) {
            case 0:
                return "single";
            case 1:
                return AdRequestorPreferences.BATCH_PREFERENCE;
            default:
                return null;
        }
    }
}
