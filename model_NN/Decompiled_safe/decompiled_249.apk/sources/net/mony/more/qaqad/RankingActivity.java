package net.mony.more.qaqad;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.util.LinkedList;
import java.util.List;
import net.mony.more.qaqad.task.RankingTask;
import net.mony.more.qaqad.utils.HeaderViewHelper;

public class RankingActivity extends ListActivity implements RankingTask.Listener {
    private static final String P_RANKNAME = "rname";
    private static final String P_RANKURL = "rurl";
    private MyAdapter mAdapter = null;
    private HeaderViewHelper mHeaderView = null;
    private RankingTask mTask = null;

    public static void startActivity(Context ctx, String rankName, String rankURL) {
        Intent intent = new Intent(ctx, RankingActivity.class);
        intent.putExtra(P_RANKNAME, rankName);
        intent.putExtra(P_RANKURL, rankURL);
        ctx.startActivity(intent);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.ranking);
        AdsView.createQWAd(this);
        Bundle bundle = getIntent().getExtras();
        this.mHeaderView = new HeaderViewHelper(this);
        this.mHeaderView.setTitle(bundle.getString(P_RANKNAME));
        this.mHeaderView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                RankingActivity.this.finish();
            }
        });
        this.mAdapter = (MyAdapter) getLastNonConfigurationInstance();
        if (this.mAdapter == null) {
            this.mAdapter = new MyAdapter(this);
            setListAdapter(this.mAdapter);
            this.mTask = new RankingTask(this);
            this.mTask.execute(bundle.getString(P_RANKURL));
            return;
        }
        this.mAdapter.mActivity = this;
        setListAdapter(this.mAdapter);
    }

    public Object onRetainNonConfigurationInstance() {
        return this.mAdapter;
    }

    public void onDestroy() {
        if (this.mTask != null) {
            this.mTask.cancel(true);
            this.mTask = null;
        }
        getListView().setAdapter((ListAdapter) null);
        this.mAdapter = null;
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        if (this.mAdapter != null && this.mAdapter.getItem(position) == null) {
        }
    }

    private static class MyAdapter extends BaseAdapter {
        /* access modifiers changed from: private */
        public RankingActivity mActivity = null;
        private List<String> mArtists = new LinkedList();
        private List<String> mSongs = new LinkedList();

        public MyAdapter(RankingActivity a) {
            this.mActivity = a;
        }

        private class ViewHolder {
            TextView mTVArtist;
            TextView mTVSong;

            private ViewHolder() {
            }

            /* synthetic */ ViewHolder(MyAdapter myAdapter, ViewHolder viewHolder) {
                this();
            }
        }

        public int getCount() {
            return this.mSongs.size();
        }

        public String getItem(int position) {
            try {
                return this.mSongs.get(position);
            } catch (Exception e) {
                return null;
            }
        }

        public long getItemId(int position) {
            return (long) position;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (convertView == null) {
                view = LayoutInflater.from(this.mActivity).inflate((int) R.layout.ranking_item, parent, false);
                ViewHolder vh = new ViewHolder(this, null);
                vh.mTVArtist = (TextView) view.findViewById(R.id.Artist);
                vh.mTVSong = (TextView) view.findViewById(R.id.Title);
                view.setTag(vh);
            }
            ViewHolder vh2 = (ViewHolder) view.getTag();
            String trackName = getItem(position);
            String artist = this.mArtists.get(position);
            if (trackName != null) {
                vh2.mTVSong.setText(trackName);
                vh2.mTVArtist.setText(artist);
            }
            return view;
        }

        public void add(String trackName, String artist) {
            this.mSongs.add(trackName);
            this.mArtists.add(artist);
            notifyDataSetChanged();
        }
    }

    public void RT_OnBegin() {
        this.mHeaderView.showProgress();
    }

    public void RT_OnFinished(boolean bError) {
        this.mHeaderView.hideProgress();
    }

    public void RT_OnReady(String trackName, String artist) {
        if (this.mAdapter != null) {
            this.mAdapter.add(trackName, artist);
        }
    }
}
