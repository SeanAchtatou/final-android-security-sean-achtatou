package net.mony.more.qaqad.service;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.os.StatFs;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.webkit.MimeTypeMap;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.mony.more.qaqad.data.DownloadQueue;
import net.mony.more.qaqad.utils.Constants;

public class DownloadHelper {
    private static final Pattern CONTENT_DISPOSITION_PATTERN = Pattern.compile("attachment;\\s*filename\\s*=\\s*\"([^\"]*)\"");
    public static Random sRandom = new Random(SystemClock.uptimeMillis());

    private DownloadHelper() {
    }

    private static String parseContentDisposition(String contentDisposition) {
        try {
            Matcher m = CONTENT_DISPOSITION_PATTERN.matcher(contentDisposition);
            if (m.find()) {
                return m.group(1);
            }
        } catch (IllegalStateException e) {
        }
        return null;
    }

    /* JADX INFO: Multiple debug info for r6v1 java.lang.String: [D('hint' java.lang.String), D('filename' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r4v3 int: [D('extension' java.lang.String), D('dotIndex' int)] */
    /* JADX INFO: Multiple debug info for r4v4 java.lang.String: [D('dotIndex' int), D('filename' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r4v20 java.lang.String: [D('fullFilename' java.lang.String), D('filename' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r4v31 java.lang.String: [D('extension' java.lang.String), D('dotIndex' int)] */
    public static DownloadFileInfo generateSaveFile(Context context, String url, String hint, String contentDisposition, String contentLocation, String mimeType, int contentLength) throws FileNotFoundException {
        String url2;
        if (mimeType == null) {
            if (Constants.LOGD) {
                Log.d(Constants.TAG, "external download with no mime type not allowed");
            }
            return new DownloadFileInfo(null, null, DownloadQueue.STATUS_NOT_ACCEPTABLE);
        }
        String filename = chooseFilename(url, hint, contentDisposition, contentLocation);
        int dotIndex = filename.indexOf(46);
        if (dotIndex < 0) {
            url2 = chooseExtensionFromMimeType(mimeType, true);
        } else {
            url2 = chooseExtensionFromFilename(mimeType, filename, dotIndex);
            filename = filename.substring(0, dotIndex);
        }
        if (Environment.getExternalStorageState().equals("mounted")) {
            File base = new File(String.valueOf(Environment.getExternalStorageDirectory().getPath()) + Constants.DEFAULT_DL_SUBDIR);
            if (base.isDirectory() || base.mkdir()) {
                StatFs stat = new StatFs(base.getPath());
                if (((long) stat.getBlockSize()) * (((long) stat.getAvailableBlocks()) - 4) < ((long) contentLength)) {
                    if (Constants.LOGD) {
                        Log.d(Constants.TAG, "download aborted - not enough free space " + contentLength);
                    }
                    return new DownloadFileInfo(null, null, DownloadQueue.STATUS_FILE_ERROR);
                }
                String filename2 = chooseUniqueFilename(String.valueOf(base.getPath()) + File.separator + filename, url2);
                if (filename2 != null) {
                    return new DownloadFileInfo(filename2, new FileOutputStream(filename2), 0);
                }
                return new DownloadFileInfo(null, null, DownloadQueue.STATUS_FILE_ERROR);
            }
            if (Constants.LOGD) {
                Log.d(Constants.TAG, "download aborted - can't create base directory " + base.getPath());
            }
            return new DownloadFileInfo(null, null, DownloadQueue.STATUS_FILE_ERROR);
        }
        if (Constants.LOGD) {
            Log.d(Constants.TAG, "download aborted - no external storage");
        }
        return new DownloadFileInfo(null, null, DownloadQueue.STATUS_FILE_ERROR);
    }

    private static String chooseFilename(String url, String hint, String contentDisposition, String contentLocation) {
        String decodedUrl;
        int index;
        String decodedContentLocation;
        String filename = null;
        if (0 == 0 && hint != null && !hint.endsWith("/")) {
            if (Constants.LOGV) {
                Log.v(Constants.TAG, "getting filename from hint");
            }
            int index2 = hint.lastIndexOf(47) + 1;
            filename = index2 > 0 ? hint.substring(index2) : hint;
        }
        if (!(filename != null || contentDisposition == null || (filename = parseContentDisposition(contentDisposition)) == null)) {
            if (Constants.LOGV) {
                Log.v(Constants.TAG, "getting filename from content-disposition");
            }
            int index3 = filename.lastIndexOf(47) + 1;
            if (index3 > 0) {
                filename = filename.substring(index3);
            }
        }
        if (filename == null && contentLocation != null && (decodedContentLocation = Uri.decode(contentLocation)) != null && !decodedContentLocation.endsWith("/") && decodedContentLocation.indexOf(63) < 0) {
            int index4 = decodedContentLocation.lastIndexOf(47) + 1;
            if (index4 > 0) {
                filename = decodedContentLocation.substring(index4);
            } else {
                filename = decodedContentLocation;
            }
        }
        if (filename == null && (decodedUrl = Uri.decode(url)) != null && !decodedUrl.endsWith("/") && decodedUrl.indexOf(63) < 0 && (index = decodedUrl.lastIndexOf(47) + 1) > 0) {
            filename = decodedUrl.substring(index);
        }
        if (filename == null) {
            filename = Constants.DEFAULT_DL_FILENAME;
        }
        return filename.replaceAll("[^a-zA-Z0-9\\.\\-_]+", "_");
    }

    private static String chooseExtensionFromMimeType(String mimeType, boolean useDefaults) {
        String extension = null;
        if (!(mimeType == null || (extension = MimeTypeMap.getSingleton().getExtensionFromMimeType(mimeType)) == null)) {
            extension = "." + extension;
        }
        if (extension == null) {
            return ".mp3";
        }
        return extension;
    }

    private static String chooseExtensionFromFilename(String mimeType, String filename, int dotIndex) {
        String extension = null;
        if (mimeType != null) {
            String typeFromExt = MimeTypeMap.getSingleton().getMimeTypeFromExtension(filename.substring(filename.lastIndexOf(46) + 1));
            if ((typeFromExt == null || !typeFromExt.equalsIgnoreCase(mimeType)) && (extension = chooseExtensionFromMimeType(mimeType, false)) != null) {
            }
        }
        if (extension == null) {
            return filename.substring(dotIndex);
        }
        return extension;
    }

    private static String chooseUniqueFilename(String filename, String extension) {
        String fullFilename = String.valueOf(filename) + extension;
        if (!new File(fullFilename).exists()) {
            return fullFilename;
        }
        String filename2 = String.valueOf(filename) + "-";
        int sequence = 1;
        for (int magnitude = 1; magnitude < 1000000000; magnitude *= 10) {
            for (int iteration = 0; iteration < 9; iteration++) {
                String fullFilename2 = String.valueOf(filename2) + sequence + extension;
                if (!new File(fullFilename2).exists()) {
                    return fullFilename2;
                }
                sequence += sRandom.nextInt(magnitude) + 1;
            }
        }
        return null;
    }

    public static final boolean discardPurgeableFiles(Context context, long targetBytes) {
        return true;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivity == null) {
            Log.w(Constants.TAG, "couldn't get connectivity manager");
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (NetworkInfo state : info) {
                    if (state.getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static boolean isNetworkRoaming(Context context) {
        TelephonyManager teleMan;
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivity == null) {
            Log.w(Constants.TAG, "couldn't get connectivity manager");
        } else {
            NetworkInfo info = connectivity.getActiveNetworkInfo();
            if (info == null || info.getType() != 0 || (teleMan = (TelephonyManager) context.getSystemService("phone")) == null || !teleMan.isNetworkRoaming()) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean isFilenameValid(String filename) {
        File dir = new File(filename).getParentFile();
        return dir.equals(Environment.getDownloadCacheDirectory()) || dir.equals(new File(new StringBuilder().append(Environment.getExternalStorageDirectory()).append(Constants.DEFAULT_DL_SUBDIR).toString()));
    }

    public static void validateSelection(String selection, Set<String> allowedColumns) {
        if (selection != null) {
            try {
                Lexer lexer = new Lexer(selection, allowedColumns);
                parseExpression(lexer);
                if (lexer.currentToken() != 9) {
                    throw new IllegalArgumentException("syntax error");
                }
            } catch (RuntimeException e) {
                RuntimeException ex = e;
                Log.d(Constants.TAG, "invalid selection triggered " + ex);
                throw ex;
            }
        }
    }

    private static void parseExpression(Lexer lexer) {
        while (true) {
            if (lexer.currentToken() == 1) {
                lexer.advance();
                parseExpression(lexer);
                if (lexer.currentToken() != 2) {
                    throw new IllegalArgumentException("syntax error, unmatched parenthese");
                }
                lexer.advance();
            } else {
                parseStatement(lexer);
            }
            if (lexer.currentToken() == 3) {
                lexer.advance();
            } else {
                return;
            }
        }
    }

    private static void parseStatement(Lexer lexer) {
        if (lexer.currentToken() != 4) {
            throw new IllegalArgumentException("syntax error, expected column name");
        }
        lexer.advance();
        if (lexer.currentToken() == 5) {
            lexer.advance();
            if (lexer.currentToken() != 6) {
                throw new IllegalArgumentException("syntax error, expected quoted string");
            }
            lexer.advance();
        } else if (lexer.currentToken() == 7) {
            lexer.advance();
            if (lexer.currentToken() != 8) {
                throw new IllegalArgumentException("syntax error, expected NULL");
            }
            lexer.advance();
        } else {
            throw new IllegalArgumentException("syntax error after column name");
        }
    }

    private static class Lexer {
        public static final int TOKEN_AND_OR = 3;
        public static final int TOKEN_CLOSE_PAREN = 2;
        public static final int TOKEN_COLUMN = 4;
        public static final int TOKEN_COMPARE = 5;
        public static final int TOKEN_END = 9;
        public static final int TOKEN_IS = 7;
        public static final int TOKEN_NULL = 8;
        public static final int TOKEN_OPEN_PAREN = 1;
        public static final int TOKEN_START = 0;
        public static final int TOKEN_VALUE = 6;
        private final Set<String> mAllowedColumns;
        private final char[] mChars;
        private int mCurrentToken = 0;
        private int mOffset = 0;
        private final String mSelection;

        public Lexer(String selection, Set<String> allowedColumns) {
            this.mSelection = selection;
            this.mAllowedColumns = allowedColumns;
            this.mChars = new char[this.mSelection.length()];
            this.mSelection.getChars(0, this.mChars.length, this.mChars, 0);
            advance();
        }

        public int currentToken() {
            return this.mCurrentToken;
        }

        public void advance() {
            char[] chars = this.mChars;
            while (this.mOffset < chars.length && chars[this.mOffset] == ' ') {
                this.mOffset++;
            }
            if (this.mOffset == chars.length) {
                this.mCurrentToken = 9;
            } else if (chars[this.mOffset] == '(') {
                this.mOffset++;
                this.mCurrentToken = 1;
            } else if (chars[this.mOffset] == ')') {
                this.mOffset++;
                this.mCurrentToken = 2;
            } else if (chars[this.mOffset] == '?') {
                this.mOffset++;
                this.mCurrentToken = 6;
            } else if (chars[this.mOffset] == '=') {
                this.mOffset++;
                this.mCurrentToken = 5;
                if (this.mOffset < chars.length && chars[this.mOffset] == '=') {
                    this.mOffset++;
                }
            } else if (chars[this.mOffset] == '>') {
                this.mOffset++;
                this.mCurrentToken = 5;
                if (this.mOffset < chars.length && chars[this.mOffset] == '=') {
                    this.mOffset++;
                }
            } else if (chars[this.mOffset] == '<') {
                this.mOffset++;
                this.mCurrentToken = 5;
                if (this.mOffset >= chars.length) {
                    return;
                }
                if (chars[this.mOffset] == '=' || chars[this.mOffset] == '>') {
                    this.mOffset++;
                }
            } else if (chars[this.mOffset] == '!') {
                this.mOffset++;
                this.mCurrentToken = 5;
                if (this.mOffset >= chars.length || chars[this.mOffset] != '=') {
                    throw new IllegalArgumentException("Unexpected character after !");
                }
                this.mOffset++;
            } else if (isIdentifierStart(chars[this.mOffset])) {
                int startOffset = this.mOffset;
                this.mOffset++;
                while (this.mOffset < chars.length && isIdentifierChar(chars[this.mOffset])) {
                    this.mOffset++;
                }
                String word = this.mSelection.substring(startOffset, this.mOffset);
                if (this.mOffset - startOffset <= 4) {
                    if (word.equals("IS")) {
                        this.mCurrentToken = 7;
                        return;
                    } else if (word.equals("OR") || word.equals("AND")) {
                        this.mCurrentToken = 3;
                        return;
                    } else if (word.equals("NULL")) {
                        this.mCurrentToken = 8;
                        return;
                    }
                }
                if (this.mAllowedColumns.contains(word)) {
                    this.mCurrentToken = 4;
                    return;
                }
                throw new IllegalArgumentException("unrecognized column or keyword");
            } else if (chars[this.mOffset] == '\'') {
                this.mOffset++;
                while (this.mOffset < chars.length) {
                    if (chars[this.mOffset] == '\'') {
                        if (this.mOffset + 1 >= chars.length || chars[this.mOffset + 1] != '\'') {
                            break;
                        }
                        this.mOffset++;
                    }
                    this.mOffset++;
                }
                if (this.mOffset == chars.length) {
                    throw new IllegalArgumentException("unterminated string");
                }
                this.mOffset++;
                this.mCurrentToken = 6;
            } else {
                throw new IllegalArgumentException("illegal character");
            }
        }

        private static final boolean isIdentifierStart(char c) {
            return c == '_' || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z');
        }

        private static final boolean isIdentifierChar(char c) {
            return c == '_' || (c >= 'A' && c <= 'Z') || ((c >= 'a' && c <= 'z') || (c >= '0' && c <= '9'));
        }
    }
}
