package net.mony.more.qaqad;

import android.app.ListActivity;
import android.content.AsyncQueryHandler;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import net.mony.more.qaqad.data.History;
import net.mony.more.qaqad.utils.HeaderViewHelper;

public class HistoryActivity extends ListActivity {
    /* access modifiers changed from: private */
    public HistoryAdapter mAdapter = null;
    private boolean mAdapterSent = false;
    /* access modifiers changed from: private */
    public Cursor mCursor = null;
    private HeaderViewHelper mHeaderView = null;
    /* access modifiers changed from: private */
    public Handler mReScanHandler = new Handler() {
        public void handleMessage(Message msg) {
            try {
                Cursor unused = HistoryActivity.this.getHistoryCursor(HistoryActivity.this.mAdapter.getQueryHandler());
            } catch (Exception e) {
            }
        }
    };
    private BroadcastReceiver mScanListener = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            HistoryActivity.this.mReScanHandler.sendEmptyMessage(0);
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.history);
        IntentFilter f = new IntentFilter();
        f.addAction("android.intent.action.MEDIA_UNMOUNTED");
        f.addDataScheme("file");
        registerReceiver(this.mScanListener, f);
        this.mHeaderView = new HeaderViewHelper(this);
        this.mHeaderView.setTitle("History");
        this.mHeaderView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HistoryActivity.this.finish();
            }
        });
        this.mAdapter = (HistoryAdapter) getLastNonConfigurationInstance();
        if (this.mAdapter == null) {
            this.mAdapter = new HistoryAdapter(getApplication(), this, R.layout.search_item, this.mCursor, new String[0], new int[0]);
            setListAdapter(this.mAdapter);
            getHistoryCursor(this.mAdapter.getQueryHandler());
            return;
        }
        this.mAdapter.setActivity(this);
        setListAdapter(this.mAdapter);
        this.mCursor = this.mAdapter.getCursor();
        if (this.mCursor != null) {
            init(this.mCursor);
        } else {
            getHistoryCursor(this.mAdapter.getQueryHandler());
        }
    }

    public Object onRetainNonConfigurationInstance() {
        this.mAdapterSent = true;
        return this.mAdapter;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        Cursor c;
        unregisterReceiver(this.mScanListener);
        if (!this.mAdapterSent && (c = this.mAdapter.getCursor()) != null) {
            c.close();
        }
        setListAdapter(null);
        this.mAdapter = null;
        super.onDestroy();
    }

    public void onPause() {
        this.mReScanHandler.removeCallbacksAndMessages(null);
        super.onPause();
    }

    /* access modifiers changed from: private */
    public void init(Cursor c) {
        this.mAdapter.changeCursor(c);
        if (this.mCursor == null) {
            this.mReScanHandler.sendEmptyMessageDelayed(0, 1000);
        }
    }

    /* access modifiers changed from: private */
    public Cursor getHistoryCursor(AsyncQueryHandler async) {
        String[] cols = {"_id", History.COL_SONG, "artist", "album"};
        if (async != null) {
            async.startQuery(0, null, History.CONTENT_URI, cols, null, null, null);
            return null;
        }
        try {
            ContentResolver resolver = getContentResolver();
            if (resolver != null) {
                return resolver.query(History.CONTENT_URI, cols, null, null, null);
            }
            return null;
        } catch (UnsupportedOperationException e) {
            return null;
        }
    }

    private static class HistoryAdapter extends SimpleCursorAdapter {
        /* access modifiers changed from: private */
        public HistoryActivity mActivity;
        private int mAlbumIdx;
        private int mArtistIdx;
        private AsyncQueryHandler mQueryHandler;
        private int mTitleIdx;

        static class ViewHolder {
            TextView mTVAlbum;
            TextView mTVArtist;
            TextView mTVTitle;

            ViewHolder() {
            }
        }

        class QueryHandler extends AsyncQueryHandler {
            public QueryHandler(ContentResolver cr) {
                super(cr);
            }

            /* access modifiers changed from: protected */
            public void onQueryComplete(int token, Object cookie, Cursor cursor) {
                HistoryAdapter.this.mActivity.init(cursor);
            }
        }

        HistoryAdapter(Context context, HistoryActivity currentActivity, int layout, Cursor cursor, String[] from, int[] to) {
            super(context, layout, cursor, from, to);
            this.mActivity = currentActivity;
            this.mQueryHandler = new QueryHandler(context.getContentResolver());
            getColumnIndices(cursor);
        }

        private void getColumnIndices(Cursor c) {
            if (c != null) {
                this.mTitleIdx = c.getColumnIndex(History.COL_SONG);
                this.mArtistIdx = c.getColumnIndex("artist");
                this.mAlbumIdx = c.getColumnIndex("album");
            }
        }

        public void setActivity(HistoryActivity newActivity) {
            this.mActivity = newActivity;
        }

        public AsyncQueryHandler getQueryHandler() {
            return this.mQueryHandler;
        }

        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            View v = super.newView(context, cursor, parent);
            ViewHolder vh = new ViewHolder();
            vh.mTVTitle = (TextView) v.findViewById(R.id.title);
            vh.mTVArtist = (TextView) v.findViewById(R.id.Artist);
            vh.mTVAlbum = (TextView) v.findViewById(R.id.Album);
            v.setTag(vh);
            return v;
        }

        public void bindView(View view, Context context, Cursor c) {
            ViewHolder vh = (ViewHolder) view.getTag();
            vh.mTVTitle.setText(c.getString(this.mTitleIdx));
            vh.mTVArtist.setText(c.getString(this.mArtistIdx));
            vh.mTVAlbum.setText(c.getString(this.mAlbumIdx));
        }

        public void changeCursor(Cursor cursor) {
            if (cursor != this.mActivity.mCursor) {
                this.mActivity.mCursor = cursor;
                getColumnIndices(cursor);
                super.changeCursor(cursor);
            }
        }

        public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
            return this.mActivity.getHistoryCursor(null);
        }
    }
}
