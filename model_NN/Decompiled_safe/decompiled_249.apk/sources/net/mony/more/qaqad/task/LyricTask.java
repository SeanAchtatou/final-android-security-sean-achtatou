package net.mony.more.qaqad.task;

import android.os.AsyncTask;
import android.text.Html;
import net.mony.more.qaqad.utils.NetUtils;

public class LyricTask extends AsyncTask<String, String, Integer> {
    private Listener mListener = null;

    public interface Listener {
        void LT_OnBegin();

        void LT_OnEnd(boolean z);

        void LT_OnReady(String str);
    }

    public LyricTask(Listener l) {
        this.mListener = l;
    }

    /* access modifiers changed from: protected */
    public Integer doInBackground(String... reqURLs) {
        String httpResp = NetUtils.loadStringAsPCZH(reqURLs[0]);
        if (httpResp == null) {
            return 0;
        }
        String lyric = SearchSongTask.getRangeText(httpResp, "<div class=\"lyrbox\"", "</div>");
        if (lyric == null) {
            return 0;
        }
        publishProgress(Html.fromHtml(lyric.substring(lyric.indexOf(">") + 1)).toString());
        return 1;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        if (this.mListener != null) {
            this.mListener.LT_OnBegin();
        }
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(String... lyrics) {
        if (this.mListener != null) {
            this.mListener.LT_OnReady(lyrics[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Integer result) {
        if (this.mListener != null) {
            this.mListener.LT_OnEnd(result.intValue() != 1);
        }
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        this.mListener = null;
    }
}
