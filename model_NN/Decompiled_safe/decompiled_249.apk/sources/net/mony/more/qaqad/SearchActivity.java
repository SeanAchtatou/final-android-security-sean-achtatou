package net.mony.more.qaqad;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.LinkedList;
import java.util.List;
import net.mony.more.qaqad.data.DownloadQueue;
import net.mony.more.qaqad.task.SearchPage;
import net.mony.more.qaqad.task.SearchResult;
import net.mony.more.qaqad.task.SearchSongTask;
import net.mony.more.qaqad.task.SongLinkTask;
import net.mony.more.qaqad.utils.PagerBarHelper;

public class SearchActivity extends ListActivity implements SearchSongTask.Listener, SongLinkTask.Listener {
    private static final int DLG_MENU_SEARCH = 1;
    private static final String P_KEYWORDS = "kw";
    /* access modifiers changed from: private */
    public boolean bDownload = false;
    /* access modifiers changed from: private */
    public MyAdapter mAdapter = null;
    /* access modifiers changed from: private */
    public EditText mETKeywords = null;
    private View mLoadingView = null;
    /* access modifiers changed from: private */
    public PagerBarHelper mPagerView = null;
    /* access modifiers changed from: private */
    public SearchResult mSR = null;
    /* access modifiers changed from: private */
    public SongLinkTask mSongTask = null;
    /* access modifiers changed from: private */
    public SearchSongTask mTask = null;

    public static void startActivity(Context ctx) {
        ctx.startActivity(new Intent(ctx, SearchActivity.class));
    }

    public static void startSearch(Context ctx, String keywords) {
        Intent intent = new Intent(ctx, SearchActivity.class);
        intent.putExtra(P_KEYWORDS, keywords);
        ctx.startActivity(intent);
    }

    public void onCreate(Bundle savedInstanceState) {
        String kw;
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.search);
        AdsView.createAdWhirl(this);
        this.mLoadingView = findViewById(R.id.center_text);
        this.mPagerView = new PagerBarHelper(findViewById(R.id.FooterBar), this);
        this.mPagerView.hideFooter();
        this.mPagerView.setNextOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (SearchActivity.this.mAdapter != null && SearchActivity.this.mAdapter.mPager.mNextLink != null) {
                    SearchActivity.this.mAdapter.mSR.clear();
                    SearchActivity.this.mAdapter.notifyDataSetChanged();
                    SearchActivity.this.mPagerView.hideFooter();
                    SearchActivity.this.mAdapter.mCurrentPageNo++;
                    SearchActivity.this.mTask = new SearchSongTask(SearchActivity.this);
                    SearchActivity.this.mTask.execute(SearchSongTask.CMD_URL, SearchActivity.this.mAdapter.mPager.mNextLink);
                }
            }
        });
        this.mPagerView.setPrevOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (SearchActivity.this.mAdapter != null && SearchActivity.this.mAdapter.mPager.mPrevLink != null) {
                    SearchActivity.this.mAdapter.mSR.clear();
                    SearchActivity.this.mAdapter.notifyDataSetChanged();
                    SearchActivity.this.mPagerView.hideFooter();
                    if (SearchActivity.this.mAdapter.mCurrentPageNo > 1) {
                        SearchActivity.this.mAdapter.mCurrentPageNo--;
                    }
                    SearchActivity.this.mTask = new SearchSongTask(SearchActivity.this);
                    SearchActivity.this.mTask.execute(SearchSongTask.CMD_URL, SearchActivity.this.mAdapter.mPager.mPrevLink);
                }
            }
        });
        this.mAdapter = (MyAdapter) getLastNonConfigurationInstance();
        if (this.mAdapter != null) {
            this.mAdapter.mActivity = this;
            setListAdapter(this.mAdapter);
        }
        this.mETKeywords = (EditText) findViewById(R.id.Keyword);
        ((ImageButton) findViewById(R.id.SearchBtn)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String kw = SearchActivity.this.mETKeywords.getText().toString();
                if (kw.length() >= 1) {
                    if (SearchActivity.this.mTask != null) {
                        SearchActivity.this.mTask.cancel(true);
                        SearchActivity.this.mTask = null;
                    }
                    Log.e("Music", "search... " + kw);
                    SearchActivity.this.mAdapter = new MyAdapter(SearchActivity.this);
                    SearchActivity.this.setListAdapter(SearchActivity.this.mAdapter);
                    SearchActivity.this.mTask = new SearchSongTask(SearchActivity.this);
                    SearchActivity.this.mTask.execute("1", kw);
                }
            }
        });
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && (kw = bundle.getString(P_KEYWORDS)) != null) {
            if (this.mTask != null) {
                this.mTask.cancel(true);
                this.mTask = null;
            }
            this.mETKeywords.setText(kw);
            Log.e("Music", "search... " + kw);
            this.mAdapter = new MyAdapter(this);
            setListAdapter(this.mAdapter);
            this.mTask = new SearchSongTask(this);
            this.mTask.execute("1", kw);
        }
    }

    public void onDestroy() {
        if (this.mTask != null) {
            this.mTask.cancel(true);
            this.mTask = null;
        }
        getListView().setAdapter((ListAdapter) null);
        this.mAdapter = null;
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        if (this.mAdapter != null) {
            this.mSR = this.mAdapter.getItem(position);
            if (this.mSR != null) {
                showDialog(1);
            }
        }
    }

    private static class MyAdapter extends BaseAdapter {
        /* access modifiers changed from: private */
        public SearchActivity mActivity = null;
        int mCurrentPageNo = 1;
        SearchPage mPager = new SearchPage();
        List<SearchResult> mSR = new LinkedList();

        public MyAdapter(SearchActivity activity) {
            this.mActivity = activity;
        }

        private static class ViewHolder {
            TextView mTVAlbum;
            TextView mTVArtist;
            TextView mTVRowNo;
            TextView mTVSize;
            TextView mTVSong;

            private ViewHolder() {
            }

            /* synthetic */ ViewHolder(ViewHolder viewHolder) {
                this();
            }
        }

        public int getCount() {
            return this.mSR.size();
        }

        public SearchResult getItem(int pos) {
            try {
                return this.mSR.get(pos);
            } catch (Exception e) {
                return null;
            }
        }

        public long getItemId(int pos) {
            return (long) pos;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int pos, View convertView, ViewGroup parent) {
            View view;
            if (convertView == null) {
                view = LayoutInflater.from(this.mActivity).inflate((int) R.layout.search_item, parent, false);
                ViewHolder vh = new ViewHolder(null);
                vh.mTVRowNo = (TextView) view.findViewById(R.id.RowNo);
                vh.mTVSong = (TextView) view.findViewById(R.id.Song);
                vh.mTVArtist = (TextView) view.findViewById(R.id.Artist);
                vh.mTVAlbum = (TextView) view.findViewById(R.id.Album);
                vh.mTVSize = (TextView) view.findViewById(R.id.Size);
                view.setTag(vh);
            } else {
                view = convertView;
            }
            ViewHolder vh2 = (ViewHolder) view.getTag();
            SearchResult sr = getItem(pos);
            if (sr != null) {
                vh2.mTVRowNo.setText(String.valueOf(pos + 1 + ((this.mCurrentPageNo - 1) * 30)) + ".");
                vh2.mTVSong.setText(sr.mSong);
                vh2.mTVSize.setText(sr.mSize);
                vh2.mTVArtist.setText(sr.mArtist.length() > 0 ? sr.mArtist : this.mActivity.getResources().getString(R.string.UNKNOWN_ARTIST));
                vh2.mTVAlbum.setText(sr.mAlbum.length() > 0 ? sr.mAlbum : this.mActivity.getResources().getString(R.string.UNKNOWN_ALBUM));
            }
            return view;
        }

        public void add(SearchResult sr) {
            this.mSR.add(sr);
            notifyDataSetChanged();
        }
    }

    public void SST_OnBegin() {
        this.mLoadingView.setVisibility(0);
        this.mPagerView.hideFooter();
    }

    public void SST_OnDataReady(SearchResult sr) {
        if (this.mLoadingView.getVisibility() != 8) {
            this.mLoadingView.setVisibility(8);
        }
        if (this.mAdapter != null) {
            Log.e("Music", "mAdapter.add : " + sr.mSong);
            this.mAdapter.add(sr);
        }
    }

    public void SST_OnFinished(boolean bError) {
    }

    public void SST_OnPagerReady(SearchPage sp) {
        if (sp.mNextLink == null) {
            this.mPagerView.hideNext();
        } else {
            this.mPagerView.showNext();
        }
        if (sp.mPrevLink == null) {
            this.mPagerView.hidePrev();
        } else {
            this.mPagerView.showPrev();
        }
        this.mPagerView.showFooter();
        if (this.mAdapter != null) {
            this.mAdapter.mPager = sp;
            this.mPagerView.setPageNo(this.mAdapter.mCurrentPageNo);
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return new AlertDialog.Builder(this).setTitle("Options").setItems((int) R.array.SEARCH_MENU, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                if (SearchActivity.this.mSongTask != null) {
                                    SearchActivity.this.mSongTask.cancel(true);
                                }
                                SearchActivity.this.mSongTask = new SongLinkTask(SearchActivity.this);
                                SearchActivity.this.mSongTask.execute(SearchActivity.this.mSR.mSongURL);
                                SearchActivity.this.bDownload = false;
                                return;
                            case 1:
                                if (SearchActivity.this.mSongTask != null) {
                                    SearchActivity.this.mSongTask.cancel(true);
                                }
                                SearchActivity.this.mSongTask = new SongLinkTask(SearchActivity.this);
                                SearchActivity.this.mSongTask.execute(SearchActivity.this.mSR.mSongURL);
                                SearchActivity.this.bDownload = true;
                                return;
                            default:
                                return;
                        }
                    }
                }).create();
            default:
                return null;
        }
    }

    public void SLT_OnBegin() {
    }

    public void SLT_OnEnd(boolean bError) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void SLT_OnReady(String url) {
        if (this.bDownload) {
            String songTitle = this.mSR.mSong;
            String songArtist = this.mSR.mArtist;
            String songAlbum = this.mSR.mAlbum;
            try {
                songTitle = URLDecoder.decode(songTitle, "gb2312").toString();
                songArtist = URLDecoder.decode(songArtist, "gb2312").toString();
                songAlbum = URLDecoder.decode(songAlbum, "gb2312").toString();
            } catch (UnsupportedEncodingException | Exception e) {
            }
            ContentValues values = new ContentValues();
            values.put("title", songTitle);
            values.put("artist", songArtist);
            values.put("album", songAlbum);
            values.put("lyric", this.mSR.mLyricURL);
            values.put(DownloadQueue.COL_CONTROL, (Integer) 0);
            values.put("uri", url);
            values.put("mimetype", "audio/mp3");
            values.put(DownloadQueue.COL_FILE_NAME_HINT, String.valueOf(songTitle) + "-" + songArtist);
            if (getContentResolver().insert(DownloadQueue.CONTENT_URI, values) == null) {
                Toast.makeText(this, (int) R.string.ERR_DATABASE_FAILURE, 1).show();
                return;
            }
            Toast.makeText(this, getString(R.string.INFO_DOWNLOAD_STARTED, new Object[]{songTitle}), 1).show();
            return;
        }
        StreamStarterActivity.startActivity(this, url, this.mSR.mSong, this.mSR.mArtist, this.mSR.mAlbum, this.mSR.mLyricURL);
    }
}
