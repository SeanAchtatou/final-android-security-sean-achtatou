package net.mony.more.qaqad;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.AsyncQueryHandler;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import net.mony.more.qaqad.data.Downloaded;
import net.mony.more.qaqad.utils.Ringtone;

public class MyDownloadsActivity extends ListActivity {
    private static final int DIALOG_DELETE_YESNO = 2;
    private static final int DIALOG_RING_PICKER = 3;
    private static final int MENU_STANDARD = 1;
    /* access modifiers changed from: private */
    public MyDownloadsAdapter mAdapter = null;
    private boolean mAdapterSent = false;
    /* access modifiers changed from: private */
    public Cursor mCursor = null;
    /* access modifiers changed from: private */
    public Handler mReScanHandler = new Handler() {
        public void handleMessage(Message msg) {
            try {
                Cursor unused = MyDownloadsActivity.this.getMyDownloadsCursor(MyDownloadsActivity.this.mAdapter.getQueryHandler());
            } catch (Exception e) {
            }
        }
    };
    private BroadcastReceiver mScanListener = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            MyDownloadsActivity.this.mReScanHandler.sendEmptyMessage(0);
        }
    };
    /* access modifiers changed from: private */
    public int mSelectedItem = -1;
    /* access modifiers changed from: private */
    public int ring_button_type;
    String title = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.mydownloads);
        AdsView.createAdWhirl(this);
        IntentFilter f = new IntentFilter();
        f.addAction("android.intent.action.MEDIA_UNMOUNTED");
        f.addDataScheme("file");
        registerReceiver(this.mScanListener, f);
        this.mAdapter = (MyDownloadsAdapter) getLastNonConfigurationInstance();
        if (this.mAdapter == null) {
            this.mAdapter = new MyDownloadsAdapter(getApplication(), this, R.layout.search_item, this.mCursor, new String[0], new int[0]);
            setListAdapter(this.mAdapter);
            getMyDownloadsCursor(this.mAdapter.getQueryHandler());
            return;
        }
        this.mAdapter.setActivity(this);
        setListAdapter(this.mAdapter);
        this.mCursor = this.mAdapter.getCursor();
        if (this.mCursor != null) {
            init(this.mCursor);
        } else {
            getMyDownloadsCursor(this.mAdapter.getQueryHandler());
        }
    }

    public Object onRetainNonConfigurationInstance() {
        this.mAdapterSent = true;
        return this.mAdapter;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        Cursor c;
        unregisterReceiver(this.mScanListener);
        if (!this.mAdapterSent && (c = this.mAdapter.getCursor()) != null) {
            c.close();
        }
        setListAdapter(null);
        this.mAdapter = null;
        super.onDestroy();
    }

    public void onPause() {
        this.mReScanHandler.removeCallbacksAndMessages(null);
        super.onPause();
    }

    /* access modifiers changed from: private */
    public void init(Cursor c) {
        if (this.mAdapter != null) {
            this.mAdapter.changeCursor(c);
            if (this.mCursor == null) {
                this.mReScanHandler.sendEmptyMessageDelayed(0, 1000);
            }
        }
    }

    /* access modifiers changed from: private */
    public Cursor getMyDownloadsCursor(AsyncQueryHandler async) {
        String[] cols = {"_id", "_data", "title", "artist", "lyric", "album", "lastmod"};
        if (async != null) {
            async.startQuery(0, null, Downloaded.CONTENT_URI, cols, null, null, "lastmod DESC");
            return null;
        }
        try {
            ContentResolver resolver = getContentResolver();
            if (resolver != null) {
                return resolver.query(Downloaded.CONTENT_URI, cols, null, null, "lastmod DESC");
            }
            return null;
        } catch (UnsupportedOperationException e) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int pos, long id) {
        Cursor c = (Cursor) this.mAdapter.getItem(pos);
        if (c != null) {
            this.mSelectedItem = c.getInt(c.getColumnIndex("_id"));
            showDialog(1);
        }
    }

    private static class MyDownloadsAdapter extends SimpleCursorAdapter {
        /* access modifiers changed from: private */
        public MyDownloadsActivity mActivity;
        private int mAlbumIdx;
        private int mArtistIdx;
        private AsyncQueryHandler mQueryHandler;
        private int mSizeIdx;
        private int mTitleIdx;

        static class ViewHolder {
            TextView mTVAlbum;
            TextView mTVArtist;
            TextView mTVNo;
            TextView mTVTitle;

            ViewHolder() {
            }
        }

        class QueryHandler extends AsyncQueryHandler {
            public QueryHandler(ContentResolver cr) {
                super(cr);
            }

            /* access modifiers changed from: protected */
            public void onQueryComplete(int token, Object cookie, Cursor cursor) {
                MyDownloadsAdapter.this.mActivity.init(cursor);
            }
        }

        MyDownloadsAdapter(Context context, MyDownloadsActivity currentActivity, int layout, Cursor cursor, String[] from, int[] to) {
            super(context, layout, cursor, from, to);
            this.mActivity = currentActivity;
            this.mQueryHandler = new QueryHandler(context.getContentResolver());
            getColumnIndices(cursor);
        }

        private void getColumnIndices(Cursor c) {
            if (c != null) {
                this.mTitleIdx = c.getColumnIndex("title");
                this.mArtistIdx = c.getColumnIndex("artist");
                this.mAlbumIdx = c.getColumnIndex("album");
            }
        }

        public void setActivity(MyDownloadsActivity newActivity) {
            this.mActivity = newActivity;
        }

        public AsyncQueryHandler getQueryHandler() {
            return this.mQueryHandler;
        }

        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            View v = super.newView(context, cursor, parent);
            ViewHolder vh = new ViewHolder();
            vh.mTVNo = (TextView) v.findViewById(R.id.RowNo);
            vh.mTVTitle = (TextView) v.findViewById(R.id.Song);
            vh.mTVArtist = (TextView) v.findViewById(R.id.Artist);
            vh.mTVAlbum = (TextView) v.findViewById(R.id.Album);
            v.setTag(vh);
            return v;
        }

        public void bindView(View view, Context context, Cursor c) {
            ViewHolder vh = (ViewHolder) view.getTag();
            vh.mTVNo.setText(String.valueOf(c.getPosition() + 1) + ".");
            vh.mTVTitle.setText(c.getString(this.mTitleIdx));
            vh.mTVArtist.setText(c.getString(this.mArtistIdx));
            vh.mTVAlbum.setText(c.getString(this.mAlbumIdx));
        }

        public void changeCursor(Cursor cursor) {
            if (cursor != this.mActivity.mCursor) {
                this.mActivity.mCursor = cursor;
                getColumnIndices(cursor);
                super.changeCursor(cursor);
            }
        }

        public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
            return this.mActivity.getMyDownloadsCursor(null);
        }
    }

    /* access modifiers changed from: private */
    public void playSong(Cursor c) {
        String fPath = c.getString(c.getColumnIndex("_data"));
        if (fPath != null) {
            File songFile = new File(fPath);
            if (songFile.exists() && songFile.isFile()) {
                StreamStarterActivity.startActivity(this, fPath, c.getString(c.getColumnIndex("title")), c.getString(c.getColumnIndex("artist")), c.getString(c.getColumnIndex("album")), c.getString(c.getColumnIndex("lyric")));
                return;
            }
        }
        Toast.makeText(this, getResources().getString(R.string.ERR_FILE_NOT_EXIST), 1).show();
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return new AlertDialog.Builder(this).setTitle((int) R.string.ACTION).setItems((int) R.array.DOWNLOADED_MENU, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Cursor c;
                        switch (which) {
                            case 0:
                                if (MyDownloadsActivity.this.mSelectedItem != -1 && (c = MyDownloadsActivity.this.getContentResolver().query(Uri.parse(Downloaded.CONTENT_URI + "/" + MyDownloadsActivity.this.mSelectedItem), null, null, null, null)) != null) {
                                    try {
                                        c.moveToFirst();
                                        if (!c.isAfterLast()) {
                                            MyDownloadsActivity.this.playSong(c);
                                            c.close();
                                            return;
                                        }
                                        return;
                                    } finally {
                                        c.close();
                                    }
                                } else {
                                    return;
                                }
                            case 1:
                                MyDownloadsActivity.this.showDialog(2);
                                return;
                            case 2:
                                MyDownloadsActivity.this.showDialog(3);
                                return;
                            default:
                                return;
                        }
                    }
                }).create();
            case 2:
                Cursor c = getContentResolver().query(Uri.parse(Downloaded.CONTENT_URI + "/" + this.mSelectedItem), null, null, null, null);
                if (c == null) {
                    return null;
                }
                try {
                    c.moveToFirst();
                    if (c.isAfterLast()) {
                        c.close();
                        return null;
                    }
                    String title2 = c.getString(c.getColumnIndex("title"));
                    c.close();
                    if (title2 == null) {
                        return null;
                    }
                    View titleView = LayoutInflater.from(this).inflate((int) R.layout.delsong_msg, (ViewGroup) null);
                    ((TextView) titleView.findViewById(R.id.Title)).setText(String.valueOf(title2) + getString(R.string.DELETE_SONG));
                    return new AlertDialog.Builder(this).setIcon(17301543).setCustomTitle(titleView).setPositiveButton((int) R.string.ALERT_OK, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            MyDownloadsActivity.this.deleteSong(MyDownloadsActivity.this.mSelectedItem);
                        }
                    }).setNegativeButton((int) R.string.ALERT_CANCEL, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                        }
                    }).create();
                } catch (Throwable th) {
                    c.close();
                    throw th;
                }
            case 3:
                return new AlertDialog.Builder(this).setIcon(17301543).setTitle((int) R.string.ring_picker_title).setSingleChoiceItems((int) R.array.ring_types, 0, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        MyDownloadsActivity.this.ring_button_type = whichButton;
                    }
                }).setPositiveButton((int) R.string.alertdialog_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        int ring_type;
                        Cursor c;
                        if (MyDownloadsActivity.this.ring_button_type == 0) {
                            ring_type = 1;
                        } else if (MyDownloadsActivity.this.ring_button_type == 1) {
                            ring_type = 2;
                        } else {
                            ring_type = 4;
                        }
                        String filePath = null;
                        String title = null;
                        try {
                            if (MyDownloadsActivity.this.mSelectedItem != -1) {
                                c = MyDownloadsActivity.this.getContentResolver().query(Uri.parse(Downloaded.CONTENT_URI + "/" + MyDownloadsActivity.this.mSelectedItem), null, null, null, null);
                                if (c != null) {
                                    c.moveToFirst();
                                    if (c.isAfterLast()) {
                                        c.close();
                                        return;
                                    }
                                    filePath = c.getString(c.getColumnIndex("_data"));
                                    title = c.getString(c.getColumnIndex("title"));
                                    c.close();
                                    if (filePath == null) {
                                        return;
                                    }
                                } else {
                                    return;
                                }
                            }
                            Uri uri = Ringtone.insertRingtone(MyDownloadsActivity.this.getContentResolver(), filePath, title);
                            if (uri != null) {
                                RingtoneManager.setActualDefaultRingtoneUri(MyDownloadsActivity.this, ring_type, uri);
                                Toast.makeText(MyDownloadsActivity.this, String.valueOf(title) + " set ok", 0).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        } catch (Throwable th) {
                            c.close();
                            throw th;
                        }
                    }
                }).setNegativeButton((int) R.string.alertdialog_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                }).create();
            default:
                return null;
        }
    }

    /* access modifiers changed from: private */
    public void deleteSong(int nItemID) {
        Cursor c = getContentResolver().query(Uri.parse(Downloaded.CONTENT_URI + "/" + nItemID), null, null, null, null);
        if (c != null) {
            try {
                c.moveToFirst();
                if (!c.isAfterLast()) {
                    File f = new File(c.getString(c.getColumnIndex("_data")));
                    if (f.exists() && f.isFile() && !f.delete()) {
                        f.deleteOnExit();
                    }
                    getContentResolver().delete(Uri.parse(Downloaded.CONTENT_URI + "/" + nItemID), null, null);
                    c.close();
                }
            } finally {
                c.close();
            }
        }
    }
}
