package net.mony.more.qaqad.task;

public class SearchResult {
    public String mAlbum;
    public String mAlbumURL;
    public String mArtist;
    public String mLyricURL;
    public String mSize;
    public String mSong;
    public String mSongURL;
}
