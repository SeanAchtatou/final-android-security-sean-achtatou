package net.mony.more.qaqad;

public final class R {

    public static final class anim {
        public static final int footer_appear = 2130968576;
        public static final int footer_disappear = 2130968577;
    }

    public static final class array {
        public static final int DOWNLOADED_MENU = 2131230723;
        public static final int DOWNLOAD_PAUSE = 2131230720;
        public static final int DOWNLOAD_RESTART = 2131230722;
        public static final int DOWNLOAD_RESUME = 2131230721;
        public static final int SEARCH_MENU = 2131230724;
        public static final int ring_types = 2131230725;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int COLOR_BLACK = 2131099649;
        public static final int COLOR_BRIGHTBLUE = 2131099653;
        public static final int COLOR_CYAN_TEXT = 2131099650;
        public static final int COLOR_DARKGRAY_TEXT = 2131099651;
        public static final int COLOR_LIGHTGRAY_TEXT = 2131099652;
        public static final int COLOR_ORANGE = 2131099654;
        public static final int COLOR_RED = 2131099655;
        public static final int COLOR_WHITE = 2131099648;
    }

    public static final class drawable {
        public static final int albumart_mp_unknown = 2130837504;
        public static final int appwidget_pause = 2130837505;
        public static final int appwidget_pause_focused = 2130837506;
        public static final int appwidget_pause_normal = 2130837507;
        public static final int appwidget_pause_pressed = 2130837508;
        public static final int appwidget_play = 2130837509;
        public static final int appwidget_play_focused = 2130837510;
        public static final int appwidget_play_normal = 2130837511;
        public static final int appwidget_play_pressed = 2130837512;
        public static final int blue_divider = 2130837513;
        public static final int blue_gradient = 2130837514;
        public static final int btn = 2130837515;
        public static final int btn_back = 2130837516;
        public static final int btn_back_focused = 2130837517;
        public static final int btn_back_normal = 2130837518;
        public static final int btn_back_pressed = 2130837519;
        public static final int btn_focused = 2130837520;
        public static final int btn_forward = 2130837521;
        public static final int btn_forward_focused = 2130837522;
        public static final int btn_forward_normal = 2130837523;
        public static final int btn_forward_pressed = 2130837524;
        public static final int btn_normal = 2130837525;
        public static final int btn_pressed = 2130837526;
        public static final int cyan_gradient = 2130837527;
        public static final int ic_mp_album_playback = 2130837528;
        public static final int ic_mp_artist_playback = 2130837529;
        public static final int ic_search = 2130837530;
        public static final int icon = 2130837531;
        public static final int logo = 2130837532;
        public static final int progbar_blue = 2130837533;
        public static final int progbar_red = 2130837534;
        public static final int right_arrow = 2130837535;
        public static final int semi_black = 2130837536;
        public static final int semi_white = 2130837537;
        public static final int small_favorite = 2130837538;
        public static final int small_favorite_active = 2130837539;
        public static final int stat_notify_musicplayer = 2130837540;
    }

    public static final class id {
        public static final int AdsView = 2131427329;
        public static final int Album = 2131427332;
        public static final int AlbumArt = 2131427361;
        public static final int Artist = 2131427331;
        public static final int BackBtn = 2131427338;
        public static final int BtnDownload = 2131427379;
        public static final int BtnPlay = 2131427377;
        public static final int BtnShare = 2131427378;
        public static final int ChartsBtn = 2131427343;
        public static final int ContentView = 2131427348;
        public static final int CurrentTime = 2131427367;
        public static final int DownloadBtn = 2131427344;
        public static final int DownloadingBlock = 2131427333;
        public static final int ErrorMsg = 2131427337;
        public static final int FooterBar = 2131427355;
        public static final int Header = 2131427347;
        public static final int Icon = 2131427330;
        public static final int Image = 2131427385;
        public static final int ImageView01 = 2131427387;
        public static final int Keyword = 2131427372;
        public static final int LibraryBtn = 2131427342;
        public static final int LoadingProg = 2131427339;
        public static final int Lyric = 2131427363;
        public static final int LyricBtn = 2131427360;
        public static final int LyricPanel = 2131427362;
        public static final int Name = 2131427380;
        public static final int NextBtn = 2131427358;
        public static final int PageNo = 2131427357;
        public static final int PauseBtn = 2131427365;
        public static final int Percent = 2131427334;
        public static final int PlayBtn = 2131427364;
        public static final int PrevBtn = 2131427356;
        public static final int ProgressBar = 2131427335;
        public static final int ProgressBar2 = 2131427336;
        public static final int Rank = 2131427386;
        public static final int RatingBtn = 2131427345;
        public static final int RowNo = 2131427373;
        public static final int SearchBtn = 2131427341;
        public static final int SearchContent = 2131427374;
        public static final int SeekBar = 2131427366;
        public static final int ShareBtn = 2131427346;
        public static final int Size = 2131427376;
        public static final int Song = 2131427375;
        public static final int SongTitle = 2131427359;
        public static final int Text = 2131427349;
        public static final int Title = 2131427328;
        public static final int TotalTime = 2131427368;
        public static final int ViewTitle = 2131427340;
        public static final int about = 2131427388;
        public static final int appIcon = 2131427350;
        public static final int artistalbum = 2131427383;
        public static final int center_text = 2131427369;
        public static final int description = 2131427353;
        public static final int icon = 2131427381;
        public static final int loading = 2131427370;
        public static final int popular = 2131427371;
        public static final int progress_bar = 2131427354;
        public static final int progress_small = 16842752;
        public static final int progress_text = 2131427351;
        public static final int streamloading = 2131427384;
        public static final int title = 2131427352;
        public static final int trackname = 2131427382;
    }

    public static final class layout {
        public static final int about = 2130903040;
        public static final int delsong_msg = 2130903041;
        public static final int download = 2130903042;
        public static final int download_item = 2130903043;
        public static final int header_bar = 2130903044;
        public static final int history = 2130903045;
        public static final int home = 2130903046;
        public static final int home_list_item = 2130903047;
        public static final int mydownloads = 2130903048;
        public static final int notify_downloading = 2130903049;
        public static final int pager_bar = 2130903050;
        public static final int player = 2130903051;
        public static final int popular = 2130903052;
        public static final int ranking = 2130903053;
        public static final int ranking_item = 2130903054;
        public static final int search = 2130903055;
        public static final int search_item = 2130903056;
        public static final int song = 2130903057;
        public static final int song_btn = 2130903058;
        public static final int song_header = 2130903059;
        public static final int song_item = 2130903060;
        public static final int statusbar = 2130903061;
        public static final int streamstarter = 2130903062;
        public static final int toptracks_item = 2130903063;
    }

    public static final class menu {
        public static final int menu = 2131361792;
    }

    public static final class raw {
        public static final int feed = 2131034112;
    }

    public static final class string {
        public static final int ACTION = 2131165212;
        public static final int ALERT_CANCEL = 2131165218;
        public static final int ALERT_OK = 2131165219;
        public static final int DELETE_SONG = 2131165217;
        public static final int ERR_DATABASE_FAILURE = 2131165210;
        public static final int ERR_FILE_NOT_EXIST = 2131165215;
        public static final int INFO_DOWNLOAD_STARTED = 2131165211;
        public static final int SERVER_ERROR = 2131165207;
        public static final int STATUS_BAD_REQUEST = 2131165195;
        public static final int STATUS_CANCELED = 2131165199;
        public static final int STATUS_FILE_ERROR = 2131165201;
        public static final int STATUS_HTTP_DATA_ERROR = 2131165204;
        public static final int STATUS_HTTP_EXCEPTION = 2131165205;
        public static final int STATUS_LENGTH_REQUIRED = 2131165197;
        public static final int STATUS_NOT_ACCEPTABLE = 2131165196;
        public static final int STATUS_PRECONDITION_FAILED = 2131165198;
        public static final int STATUS_TOO_MANY_REDIRECTS = 2131165206;
        public static final int STATUS_UNHANDLED_HTTP_CODE = 2131165203;
        public static final int STATUS_UNHANDLED_REDIRECT = 2131165202;
        public static final int STATUS_UNKNOWN_ERROR = 2131165200;
        public static final int UNKNOWN_ALBUM = 2131165194;
        public static final int UNKNOWN_ARTIST = 2131165193;
        public static final int about_label = 2131165227;
        public static final int about_text = 2131165225;
        public static final int about_title = 2131165224;
        public static final int about_url = 2131165226;
        public static final int alertdialog_cancel = 2131165222;
        public static final int alertdialog_ok = 2131165221;
        public static final int alertdialog_select = 2131165220;
        public static final int app_name = 2131165184;
        public static final int chart = 2131165188;
        public static final int downloads = 2131165190;
        public static final int durationformatlong = 2131165214;
        public static final int durationformatshort = 2131165213;
        public static final int fail_to_start_stream = 2131165192;
        public static final int library = 2131165189;
        public static final int notification_artist_album = 2131165216;
        public static final int notification_download_complete = 2131165209;
        public static final int notification_download_failed = 2131165208;
        public static final int rate_me = 2131165185;
        public static final int ring_picker_title = 2131165223;
        public static final int search = 2131165187;
        public static final int share_to = 2131165186;
        public static final int streamloadingtext = 2131165191;
    }

    public static final class style {
        public static final int LabelRow = 2131296257;
        public static final int ListLinkItem = 2131296258;
        public static final int ListSeparator = 2131296256;
        public static final int ListSimpleItem = 2131296259;
        public static final int ListSmallItem = 2131296260;
        public static final int MyListView = 2131296261;
        public static final int Theme_BlueMood = 2131296262;
    }
}
