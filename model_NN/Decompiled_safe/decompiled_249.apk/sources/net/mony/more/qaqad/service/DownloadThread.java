package net.mony.more.qaqad.service;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import java.util.Locale;
import net.mony.more.qaqad.data.DownloadQueue;
import net.mony.more.qaqad.utils.Constants;

public class DownloadThread extends Thread {
    private Context mContext;
    private DownloadInfo mInfo;
    /* access modifiers changed from: private */
    public MediaScannerConnection mScanner = null;

    public DownloadThread(Context context, DownloadInfo info) {
        this.mContext = context;
        this.mInfo = info;
    }

    private String userAgent() {
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException} */
    /* JADX WARNING: Code restructure failed: missing block: B:329:?, code lost:
        r53 = new android.content.ContentValues();
        r53.put(net.mony.more.qaqad.data.DownloadQueue.COL_CURRENT_BYTES, java.lang.Integer.valueOf(r21));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:330:0x0960, code lost:
        if (r37 != null) goto L_0x096f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:331:0x0962, code lost:
        r53.put(net.mony.more.qaqad.data.DownloadQueue.COL_TOTAL_BYTES, java.lang.Integer.valueOf(r21));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:332:0x096f, code lost:
        r0.mContext.getContentResolver().update(r24, r53, null, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:333:0x0984, code lost:
        if (r37 == null) goto L_0x0c7b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:335:0x098d, code lost:
        if (r21 == java.lang.Integer.parseInt(r37)) goto L_0x0c7b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:337:0x0998, code lost:
        if (net.mony.more.qaqad.service.DownloadHelper.isNetworkAvailable(r0.mContext) != false) goto L_0x0aec;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:338:0x099a, code lost:
        r6 = net.mony.more.qaqad.data.DownloadQueue.STATUS_RUNNING_PAUSED;
        r12 = null;
        r9 = r44;
        r8 = 0;
        r7 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:383:0x0af4, code lost:
        if (r0.mInfo.mNumFailed >= 5) goto L_0x0b01;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:384:0x0af6, code lost:
        r6 = net.mony.more.qaqad.data.DownloadQueue.STATUS_RUNNING_PAUSED;
        r7 = true;
        r12 = null;
        r9 = r44;
        r8 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:386:0x0b03, code lost:
        if (net.mony.more.qaqad.utils.Constants.LOGV == false) goto L_0x0b2c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:387:0x0b05, code lost:
        android.util.Log.v(net.mony.more.qaqad.utils.Constants.TAG, "closed socket for " + r0.mInfo.mUri);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:388:0x0b20, code lost:
        r6 = net.mony.more.qaqad.data.DownloadQueue.STATUS_HTTP_DATA_ERROR;
        r12 = null;
        r9 = r44;
        r8 = 0;
        r7 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:390:0x0b2e, code lost:
        if (net.mony.more.qaqad.utils.Constants.LOGD == false) goto L_0x0b20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:391:0x0b30, code lost:
        android.util.Log.d(net.mony.more.qaqad.utils.Constants.TAG, "closed socket for download " + r0.mInfo.mId);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:454:0x0c7d, code lost:
        if (net.mony.more.qaqad.utils.Constants.LOGV == false) goto L_0x0c9a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:455:0x0c7f, code lost:
        android.util.Log.v(net.mony.more.qaqad.utils.Constants.TAG, "download completed for " + r0.mInfo.mUri);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:456:0x0c9a, code lost:
        r6 = net.mony.more.qaqad.data.DownloadQueue.STATUS_SUCCESS;
        r12 = null;
        r9 = r44;
        r8 = 0;
        r7 = false;
     */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x032d A[Catch:{ all -> 0x0d80 }] */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x035a  */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x0361  */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x0368 A[SYNTHETIC, Splitter:B:119:0x0368] */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x036d  */
    /* JADX WARNING: Removed duplicated region for block: B:140:0x0401 A[Catch:{ all -> 0x0d80 }] */
    /* JADX WARNING: Removed duplicated region for block: B:143:0x042c  */
    /* JADX WARNING: Removed duplicated region for block: B:145:0x0433  */
    /* JADX WARNING: Removed duplicated region for block: B:147:0x043a A[SYNTHETIC, Splitter:B:147:0x043a] */
    /* JADX WARNING: Removed duplicated region for block: B:150:0x043f  */
    /* JADX WARNING: Removed duplicated region for block: B:179:0x04f6  */
    /* JADX WARNING: Removed duplicated region for block: B:181:0x04fd  */
    /* JADX WARNING: Removed duplicated region for block: B:183:0x0504 A[SYNTHETIC, Splitter:B:183:0x0504] */
    /* JADX WARNING: Removed duplicated region for block: B:186:0x0509  */
    /* JADX WARNING: Removed duplicated region for block: B:473:0x0d5b A[SYNTHETIC, Splitter:B:473:0x0d5b] */
    /* JADX WARNING: Removed duplicated region for block: B:568:0x1096 A[LOOP:1: B:395:0x0b5c->B:568:0x1096, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:571:0x0c26 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x029f  */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x02a6  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x02ad A[SYNTHETIC, Splitter:B:86:0x02ad] */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x02b2  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:109:0x0329=Splitter:B:109:0x0329, B:137:0x03fd=Splitter:B:137:0x03fd} */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:160:0x0467=Splitter:B:160:0x0467, B:345:0x09b8=Splitter:B:345:0x09b8} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r55 = this;
            r5 = 10
            android.os.Process.setThreadPriority(r5)
            r6 = 491(0x1eb, float:6.88E-43)
            r26 = 0
            r47 = 0
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo
            r5 = r0
            r0 = r5
            int r0 = r0.mRedirectCount
            r44 = r0
            r40 = 0
            r34 = 0
            r11 = 0
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo
            r5 = r0
            java.lang.String r5 = r5.mMimeType
            r0 = r55
            r1 = r5
            java.lang.String r17 = r0.sanitizeMimeType(r1)
            r49 = 0
            r22 = 0
            r54 = 0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            android.net.Uri r7 = net.mony.more.qaqad.data.DownloadQueue.CONTENT_URI
            java.lang.StringBuilder r5 = r5.append(r7)
            java.lang.String r7 = "/"
            java.lang.StringBuilder r5 = r5.append(r7)
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo
            r7 = r0
            int r7 = r7.mId
            java.lang.StringBuilder r5 = r5.append(r7)
            java.lang.String r5 = r5.toString()
            android.net.Uri r24 = android.net.Uri.parse(r5)
            r25 = 0
            r36 = 0
            r15 = 0
            r37 = 0
            r16 = 0
            r38 = 0
            r39 = 0
            r5 = 4096(0x1000, float:5.74E-42)
            r0 = r5
            byte[] r0 = new byte[r0]     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r27 = r0
            r21 = 0
            r0 = r55
            android.content.Context r0 = r0.mContext     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r5 = r0
            java.lang.String r7 = "power"
            java.lang.Object r43 = r5.getSystemService(r7)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            android.os.PowerManager r43 = (android.os.PowerManager) r43     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r5 = 1
            java.lang.String r7 = "MuzicWizard"
            r0 = r43
            r1 = r5
            r2 = r7
            android.os.PowerManager$WakeLock r54 = r0.newWakeLock(r1, r2)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r54.acquire()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r5 = r0
            java.lang.String r11 = r5.mFileName     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r11 == 0) goto L_0x01bb
            boolean r5 = net.mony.more.qaqad.service.DownloadHelper.isFilenameValid(r11)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r5 != 0) goto L_0x019f
            r6 = 492(0x1ec, float:6.9E-43)
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r12 = 0
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r5 = r0
            java.lang.String r13 = r5.mMimeType     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r5 = r55
            r5.notifyDownloadCompleted(r6, r7, r8, r9, r10, r11, r12, r13)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo
            r5 = r0
            r7 = 0
            r5.mHasActiveThread = r7
            if (r54 == 0) goto L_0x00b4
            r54.release()
            r54 = 0
        L_0x00b4:
            if (r22 == 0) goto L_0x00bb
            r22.close()
            r22 = 0
        L_0x00bb:
            if (r49 == 0) goto L_0x00c0
            r49.close()     // Catch:{ IOException -> 0x00ed }
        L_0x00c0:
            if (r11 == 0) goto L_0x00d1
            boolean r5 = net.mony.more.qaqad.data.DownloadQueue.isStatusError(r6)
            if (r5 == 0) goto L_0x010a
            java.io.File r5 = new java.io.File
            r5.<init>(r11)
            r5.delete()
            r11 = 0
        L_0x00d1:
            r5 = r55
            r7 = r26
            r8 = r47
            r9 = r44
            r10 = r34
            r12 = r40
            r13 = r17
            r5.notifyDownloadCompleted(r6, r7, r8, r9, r10, r11, r12, r13)
            r10 = r34
            r12 = r40
            r9 = r44
            r8 = r47
            r7 = r26
        L_0x00ec:
            return
        L_0x00ed:
            r29 = move-exception
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGV
            if (r5 == 0) goto L_0x00c0
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            java.lang.String r8 = "exception when closing the file after download : "
            r7.<init>(r8)
            r0 = r7
            r1 = r29
            java.lang.StringBuilder r7 = r0.append(r1)
            java.lang.String r7 = r7.toString()
            android.util.Log.v(r5, r7)
            goto L_0x00c0
        L_0x010a:
            boolean r5 = net.mony.more.qaqad.data.DownloadQueue.isStatusSuccess(r6)
            if (r5 == 0) goto L_0x00d1
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x011e, SyncFailedException -> 0x0143, IOException -> 0x0169, RuntimeException -> 0x018f }
            r7 = 1
            r5.<init>(r11, r7)     // Catch:{ FileNotFoundException -> 0x011e, SyncFailedException -> 0x0143, IOException -> 0x0169, RuntimeException -> 0x018f }
            java.io.FileDescriptor r5 = r5.getFD()     // Catch:{ FileNotFoundException -> 0x011e, SyncFailedException -> 0x0143, IOException -> 0x0169, RuntimeException -> 0x018f }
            r5.sync()     // Catch:{ FileNotFoundException -> 0x011e, SyncFailedException -> 0x0143, IOException -> 0x0169, RuntimeException -> 0x018f }
            goto L_0x00d1
        L_0x011e:
            r5 = move-exception
            r29 = r5
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            java.lang.String r8 = "file "
            r7.<init>(r8)
            java.lang.StringBuilder r7 = r7.append(r11)
            java.lang.String r8 = " not found: "
            java.lang.StringBuilder r7 = r7.append(r8)
            r0 = r7
            r1 = r29
            java.lang.StringBuilder r7 = r0.append(r1)
            java.lang.String r7 = r7.toString()
            android.util.Log.w(r5, r7)
            goto L_0x00d1
        L_0x0143:
            r5 = move-exception
            r29 = r5
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            java.lang.String r8 = "file "
            r7.<init>(r8)
            java.lang.StringBuilder r7 = r7.append(r11)
            java.lang.String r8 = " sync failed: "
            java.lang.StringBuilder r7 = r7.append(r8)
            r0 = r7
            r1 = r29
            java.lang.StringBuilder r7 = r0.append(r1)
            java.lang.String r7 = r7.toString()
            android.util.Log.w(r5, r7)
            goto L_0x00d1
        L_0x0169:
            r5 = move-exception
            r29 = r5
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            java.lang.String r8 = "IOException trying to sync "
            r7.<init>(r8)
            java.lang.StringBuilder r7 = r7.append(r11)
            java.lang.String r8 = ": "
            java.lang.StringBuilder r7 = r7.append(r8)
            r0 = r7
            r1 = r29
            java.lang.StringBuilder r7 = r0.append(r1)
            java.lang.String r7 = r7.toString()
            android.util.Log.w(r5, r7)
            goto L_0x00d1
        L_0x018f:
            r5 = move-exception
            r29 = r5
            java.lang.String r5 = "MuzicWizard"
            java.lang.String r7 = "exception while syncing file: "
            r0 = r5
            r1 = r7
            r2 = r29
            android.util.Log.w(r0, r1, r2)
            goto L_0x00d1
        L_0x019f:
            java.io.File r30 = new java.io.File     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r30
            r1 = r11
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            boolean r5 = r30.exists()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r5 == 0) goto L_0x01bb
            long r32 = r30.length()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r13 = 0
            int r5 = (r32 > r13 ? 1 : (r32 == r13 ? 0 : -1))
            if (r5 != 0) goto L_0x02ca
            r30.delete()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r11 = 0
        L_0x01bb:
            r19 = r21
            r51 = 0
            java.lang.String r5 = r55.userAgent()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            net.mony.more.qaqad.service.MusicHttpClient r22 = net.mony.more.qaqad.service.MusicHttpClient.newInstance(r5)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r49 == 0) goto L_0x01ce
            r49.close()     // Catch:{ IOException -> 0x02fe }
            r49 = 0
        L_0x01ce:
            org.apache.http.client.methods.HttpGet r45 = new org.apache.http.client.methods.HttpGet     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r5 = r0
            java.lang.String r5 = r5.mUri     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r45
            r1 = r5
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGV     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r5 == 0) goto L_0x01fc
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = "initiating download for "
            r7.<init>(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r8 = r0
            java.lang.String r8 = r8.mUri     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            android.util.Log.v(r5, r7)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
        L_0x01fc:
            if (r25 == 0) goto L_0x022b
            if (r38 == 0) goto L_0x020a
            java.lang.String r5 = "If-Match"
            r0 = r45
            r1 = r5
            r2 = r38
            r0.addHeader(r1, r2)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
        L_0x020a:
            java.lang.String r5 = "Range"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = "bytes="
            r7.<init>(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r7
            r1 = r21
            java.lang.StringBuilder r7 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = "-"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r45
            r1 = r5
            r2 = r7
            r0.addHeader(r1, r2)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
        L_0x022b:
            r0 = r22
            r1 = r45
            org.apache.http.HttpResponse r46 = r0.execute(r1)     // Catch:{ IllegalArgumentException -> 0x0385, IOException -> 0x0457 }
            org.apache.http.StatusLine r5 = r46.getStatusLine()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            int r48 = r5.getStatusCode()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r5 = 503(0x1f7, float:7.05E-43)
            r0 = r48
            r1 = r5
            if (r0 != r1) goto L_0x053c
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r5 = r0
            int r5 = r5.mNumFailed     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r7 = 5
            if (r5 >= r7) goto L_0x053c
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGV     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r5 == 0) goto L_0x0257
            java.lang.String r5 = "MuzicWizard"
            java.lang.String r7 = "got HTTP response code 503"
            android.util.Log.v(r5, r7)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
        L_0x0257:
            r6 = 193(0xc1, float:2.7E-43)
            r7 = 1
            java.lang.String r5 = "Retry-After"
            r0 = r46
            r1 = r5
            org.apache.http.Header r35 = r0.getFirstHeader(r1)     // Catch:{ FileNotFoundException -> 0x104a, RuntimeException -> 0x0ffc, all -> 0x0fb3 }
            if (r35 == 0) goto L_0x109e
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGV     // Catch:{ NumberFormatException -> 0x108e }
            if (r5 == 0) goto L_0x0281
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ NumberFormatException -> 0x108e }
            java.lang.String r9 = "Retry-After :"
            r8.<init>(r9)     // Catch:{ NumberFormatException -> 0x108e }
            java.lang.String r9 = r35.getValue()     // Catch:{ NumberFormatException -> 0x108e }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ NumberFormatException -> 0x108e }
            java.lang.String r8 = r8.toString()     // Catch:{ NumberFormatException -> 0x108e }
            android.util.Log.v(r5, r8)     // Catch:{ NumberFormatException -> 0x108e }
        L_0x0281:
            java.lang.String r5 = r35.getValue()     // Catch:{ NumberFormatException -> 0x108e }
            int r8 = java.lang.Integer.parseInt(r5)     // Catch:{ NumberFormatException -> 0x108e }
            if (r8 >= 0) goto L_0x0520
            r8 = 0
        L_0x028c:
            r45.abort()     // Catch:{ FileNotFoundException -> 0x1057, RuntimeException -> 0x1009, all -> 0x0fbf }
            r10 = r34
            r12 = r40
            r9 = r44
        L_0x0295:
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo
            r5 = r0
            r13 = 0
            r5.mHasActiveThread = r13
            if (r54 == 0) goto L_0x02a4
            r54.release()
            r54 = 0
        L_0x02a4:
            if (r22 == 0) goto L_0x02ab
            r22.close()
            r22 = 0
        L_0x02ab:
            if (r49 == 0) goto L_0x02b0
            r49.close()     // Catch:{ IOException -> 0x0eee }
        L_0x02b0:
            if (r11 == 0) goto L_0x02c1
            boolean r5 = net.mony.more.qaqad.data.DownloadQueue.isStatusError(r6)
            if (r5 == 0) goto L_0x0f0c
            java.io.File r5 = new java.io.File
            r5.<init>(r11)
            r5.delete()
            r11 = 0
        L_0x02c1:
            r5 = r55
            r13 = r17
            r5.notifyDownloadCompleted(r6, r7, r8, r9, r10, r11, r12, r13)
            goto L_0x00ec
        L_0x02ca:
            java.io.FileOutputStream r50 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r5 = 1
            r0 = r50
            r1 = r11
            r2 = r5
            r0.<init>(r1, r2)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r32
            int r0 = (int) r0
            r21 = r0
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x1039, RuntimeException -> 0x0feb, all -> 0x0fa3 }
            r5 = r0
            int r5 = r5.mTotalBytes     // Catch:{ FileNotFoundException -> 0x1039, RuntimeException -> 0x0feb, all -> 0x0fa3 }
            r7 = -1
            if (r5 == r7) goto L_0x02ee
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x1039, RuntimeException -> 0x0feb, all -> 0x0fa3 }
            r5 = r0
            int r5 = r5.mTotalBytes     // Catch:{ FileNotFoundException -> 0x1039, RuntimeException -> 0x0feb, all -> 0x0fa3 }
            java.lang.String r37 = java.lang.Integer.toString(r5)     // Catch:{ FileNotFoundException -> 0x1039, RuntimeException -> 0x0feb, all -> 0x0fa3 }
        L_0x02ee:
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x1039, RuntimeException -> 0x0feb, all -> 0x0fa3 }
            r5 = r0
            r0 = r5
            java.lang.String r0 = r0.mETag     // Catch:{ FileNotFoundException -> 0x1039, RuntimeException -> 0x0feb, all -> 0x0fa3 }
            r38 = r0
            r25 = 1
            r49 = r50
            goto L_0x01bb
        L_0x02fe:
            r29 = move-exception
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGV     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r5 == 0) goto L_0x01ce
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = "exception when closing the file before download : "
            r7.<init>(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r7
            r1 = r29
            java.lang.StringBuilder r7 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            android.util.Log.v(r5, r7)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            goto L_0x01ce
        L_0x031c:
            r5 = move-exception
            r29 = r5
            r10 = r34
            r12 = r40
            r9 = r44
            r8 = r47
            r7 = r26
        L_0x0329:
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGD     // Catch:{ all -> 0x0d80 }
            if (r5 == 0) goto L_0x034e
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ all -> 0x0d80 }
            java.lang.String r14 = "FileNotFoundException for "
            r13.<init>(r14)     // Catch:{ all -> 0x0d80 }
            java.lang.StringBuilder r13 = r13.append(r11)     // Catch:{ all -> 0x0d80 }
            java.lang.String r14 = " : "
            java.lang.StringBuilder r13 = r13.append(r14)     // Catch:{ all -> 0x0d80 }
            r0 = r13
            r1 = r29
            java.lang.StringBuilder r13 = r0.append(r1)     // Catch:{ all -> 0x0d80 }
            java.lang.String r13 = r13.toString()     // Catch:{ all -> 0x0d80 }
            android.util.Log.d(r5, r13)     // Catch:{ all -> 0x0d80 }
        L_0x034e:
            r6 = 492(0x1ec, float:6.9E-43)
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo
            r5 = r0
            r13 = 0
            r5.mHasActiveThread = r13
            if (r54 == 0) goto L_0x035f
            r54.release()
            r54 = 0
        L_0x035f:
            if (r22 == 0) goto L_0x0366
            r22.close()
            r22 = 0
        L_0x0366:
            if (r49 == 0) goto L_0x036b
            r49.close()     // Catch:{ IOException -> 0x0ca6 }
        L_0x036b:
            if (r11 == 0) goto L_0x037c
            boolean r5 = net.mony.more.qaqad.data.DownloadQueue.isStatusError(r6)
            if (r5 == 0) goto L_0x0cc4
            java.io.File r5 = new java.io.File
            r5.<init>(r11)
            r5.delete()
            r11 = 0
        L_0x037c:
            r5 = r55
            r13 = r17
            r5.notifyDownloadCompleted(r6, r7, r8, r9, r10, r11, r12, r13)
            goto L_0x00ec
        L_0x0385:
            r29 = move-exception
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGV     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r5 == 0) goto L_0x03c3
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = "Arg exception trying to execute request for "
            r7.<init>(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r8 = r0
            java.lang.String r8 = r8.mUri     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = " : "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r7
            r1 = r29
            java.lang.StringBuilder r7 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            android.util.Log.d(r5, r7)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
        L_0x03b2:
            r6 = 400(0x190, float:5.6E-43)
            r45.abort()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r10 = r34
            r12 = r40
            r9 = r44
            r8 = r47
            r7 = r26
            goto L_0x0295
        L_0x03c3:
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGD     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r5 == 0) goto L_0x03b2
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = "Arg exception trying to execute request for "
            r7.<init>(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r8 = r0
            int r8 = r8.mId     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = " : "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r7
            r1 = r29
            java.lang.StringBuilder r7 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            android.util.Log.d(r5, r7)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            goto L_0x03b2
        L_0x03f0:
            r5 = move-exception
            r29 = r5
            r10 = r34
            r12 = r40
            r9 = r44
            r8 = r47
            r7 = r26
        L_0x03fd:
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGV     // Catch:{ all -> 0x0d80 }
            if (r5 == 0) goto L_0x0d5b
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ all -> 0x0d80 }
            java.lang.String r14 = "Exception for "
            r13.<init>(r14)     // Catch:{ all -> 0x0d80 }
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ all -> 0x0d80 }
            r14 = r0
            java.lang.String r14 = r14.mUri     // Catch:{ all -> 0x0d80 }
            java.lang.StringBuilder r13 = r13.append(r14)     // Catch:{ all -> 0x0d80 }
            java.lang.String r13 = r13.toString()     // Catch:{ all -> 0x0d80 }
            r0 = r5
            r1 = r13
            r2 = r29
            android.util.Log.d(r0, r1, r2)     // Catch:{ all -> 0x0d80 }
        L_0x0420:
            r6 = 491(0x1eb, float:6.88E-43)
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo
            r5 = r0
            r13 = 0
            r5.mHasActiveThread = r13
            if (r54 == 0) goto L_0x0431
            r54.release()
            r54 = 0
        L_0x0431:
            if (r22 == 0) goto L_0x0438
            r22.close()
            r22 = 0
        L_0x0438:
            if (r49 == 0) goto L_0x043d
            r49.close()     // Catch:{ IOException -> 0x0d84 }
        L_0x043d:
            if (r11 == 0) goto L_0x044e
            boolean r5 = net.mony.more.qaqad.data.DownloadQueue.isStatusError(r6)
            if (r5 == 0) goto L_0x0da2
            java.io.File r5 = new java.io.File
            r5.<init>(r11)
            r5.delete()
            r11 = 0
        L_0x044e:
            r5 = r55
            r13 = r17
            r5.notifyDownloadCompleted(r6, r7, r8, r9, r10, r11, r12, r13)
            goto L_0x00ec
        L_0x0457:
            r29 = move-exception
            r0 = r55
            android.content.Context r0 = r0.mContext     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r5 = r0
            boolean r5 = net.mony.more.qaqad.service.DownloadHelper.isNetworkAvailable(r5)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r5 != 0) goto L_0x0474
            r6 = 193(0xc1, float:2.7E-43)
            r7 = r26
        L_0x0467:
            r45.abort()     // Catch:{ FileNotFoundException -> 0x104a, RuntimeException -> 0x0ffc, all -> 0x0fb3 }
            r10 = r34
            r12 = r40
            r9 = r44
            r8 = r47
            goto L_0x0295
        L_0x0474:
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r5 = r0
            int r5 = r5.mNumFailed     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r7 = 5
            if (r5 >= r7) goto L_0x0482
            r6 = 193(0xc1, float:2.7E-43)
            r7 = 1
            goto L_0x0467
        L_0x0482:
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGV     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r5 == 0) goto L_0x04b3
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = "IOException trying to execute request for "
            r7.<init>(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r8 = r0
            java.lang.String r8 = r8.mUri     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = " : "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r7
            r1 = r29
            java.lang.StringBuilder r7 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            android.util.Log.d(r5, r7)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
        L_0x04ae:
            r6 = 495(0x1ef, float:6.94E-43)
            r7 = r26
            goto L_0x0467
        L_0x04b3:
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGD     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r5 == 0) goto L_0x04ae
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = "IOException trying to execute request for "
            r7.<init>(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r8 = r0
            int r8 = r8.mId     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = " : "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r7
            r1 = r29
            java.lang.StringBuilder r7 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            android.util.Log.d(r5, r7)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            goto L_0x04ae
        L_0x04e0:
            r5 = move-exception
            r14 = r5
            r10 = r34
            r12 = r40
            r9 = r44
            r8 = r47
            r7 = r26
        L_0x04ec:
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo
            r5 = r0
            r13 = 0
            r5.mHasActiveThread = r13
            if (r54 == 0) goto L_0x04fb
            r54.release()
            r54 = 0
        L_0x04fb:
            if (r22 == 0) goto L_0x0502
            r22.close()
            r22 = 0
        L_0x0502:
            if (r49 == 0) goto L_0x0507
            r49.close()     // Catch:{ IOException -> 0x0e39 }
        L_0x0507:
            if (r11 == 0) goto L_0x0518
            boolean r5 = net.mony.more.qaqad.data.DownloadQueue.isStatusError(r6)
            if (r5 == 0) goto L_0x0e57
            java.io.File r5 = new java.io.File
            r5.<init>(r11)
            r5.delete()
            r11 = 0
        L_0x0518:
            r5 = r55
            r13 = r17
            r5.notifyDownloadCompleted(r6, r7, r8, r9, r10, r11, r12, r13)
            throw r14
        L_0x0520:
            r5 = 30
            if (r8 >= r5) goto L_0x0533
            r8 = 30
        L_0x0526:
            java.util.Random r5 = net.mony.more.qaqad.service.DownloadHelper.sRandom     // Catch:{ NumberFormatException -> 0x1093 }
            r9 = 31
            int r5 = r5.nextInt(r9)     // Catch:{ NumberFormatException -> 0x1093 }
            int r8 = r8 + r5
            int r8 = r8 * 1000
            goto L_0x028c
        L_0x0533:
            r5 = 86400(0x15180, float:1.21072E-40)
            if (r8 <= r5) goto L_0x0526
            r8 = 86400(0x15180, float:1.21072E-40)
            goto L_0x0526
        L_0x053c:
            r5 = 301(0x12d, float:4.22E-43)
            r0 = r48
            r1 = r5
            if (r0 == r1) goto L_0x0558
            r5 = 302(0x12e, float:4.23E-43)
            r0 = r48
            r1 = r5
            if (r0 == r1) goto L_0x0558
            r5 = 303(0x12f, float:4.25E-43)
            r0 = r48
            r1 = r5
            if (r0 == r1) goto L_0x0558
            r5 = 307(0x133, float:4.3E-43)
            r0 = r48
            r1 = r5
            if (r0 != r1) goto L_0x068e
        L_0x0558:
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGV     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r5 == 0) goto L_0x0573
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = "got HTTP redirect "
            r7.<init>(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r7
            r1 = r48
            java.lang.StringBuilder r7 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            android.util.Log.v(r5, r7)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
        L_0x0573:
            r5 = 5
            r0 = r44
            r1 = r5
            if (r0 < r1) goto L_0x05da
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGV     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r5 == 0) goto L_0x05ba
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = "too many redirects for download "
            r7.<init>(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r8 = r0
            int r8 = r8.mId     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = " at "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r8 = r0
            java.lang.String r8 = r8.mUri     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            android.util.Log.d(r5, r7)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
        L_0x05a9:
            r6 = 497(0x1f1, float:6.96E-43)
            r45.abort()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r10 = r34
            r12 = r40
            r9 = r44
            r8 = r47
            r7 = r26
            goto L_0x0295
        L_0x05ba:
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGD     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r5 == 0) goto L_0x05a9
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = "too many redirects for download "
            r7.<init>(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r8 = r0
            int r8 = r8.mId     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            android.util.Log.d(r5, r7)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            goto L_0x05a9
        L_0x05da:
            java.lang.String r5 = "Location"
            r0 = r46
            r1 = r5
            org.apache.http.Header r35 = r0.getFirstHeader(r1)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r35 == 0) goto L_0x068e
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGV     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r5 == 0) goto L_0x0601
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = "Location :"
            r7.<init>(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = r35.getValue()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            android.util.Log.v(r5, r7)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
        L_0x0601:
            java.net.URI r5 = new java.net.URI     // Catch:{ URISyntaxException -> 0x062d }
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ URISyntaxException -> 0x062d }
            r7 = r0
            java.lang.String r7 = r7.mUri     // Catch:{ URISyntaxException -> 0x062d }
            r5.<init>(r7)     // Catch:{ URISyntaxException -> 0x062d }
            java.net.URI r7 = new java.net.URI     // Catch:{ URISyntaxException -> 0x062d }
            java.lang.String r8 = r35.getValue()     // Catch:{ URISyntaxException -> 0x062d }
            r7.<init>(r8)     // Catch:{ URISyntaxException -> 0x062d }
            java.net.URI r5 = r5.resolve(r7)     // Catch:{ URISyntaxException -> 0x062d }
            java.lang.String r12 = r5.toString()     // Catch:{ URISyntaxException -> 0x062d }
            int r9 = r44 + 1
            r6 = 193(0xc1, float:2.7E-43)
            r45.abort()     // Catch:{ FileNotFoundException -> 0x1062, RuntimeException -> 0x1014, all -> 0x0fc9 }
            r10 = r34
            r8 = r47
            r7 = r26
            goto L_0x0295
        L_0x062d:
            r5 = move-exception
            r29 = r5
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGV     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r5 == 0) goto L_0x066e
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = "Couldn't resolve redirect URI "
            r7.<init>(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = r35.getValue()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = " for "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r8 = r0
            java.lang.String r8 = r8.mUri     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            android.util.Log.d(r5, r7)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
        L_0x065d:
            r6 = 400(0x190, float:5.6E-43)
            r45.abort()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r10 = r34
            r12 = r40
            r9 = r44
            r8 = r47
            r7 = r26
            goto L_0x0295
        L_0x066e:
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGD     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r5 == 0) goto L_0x065d
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = "Couldn't resolve redirect URI for download "
            r7.<init>(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r8 = r0
            int r8 = r8.mId     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            android.util.Log.d(r5, r7)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            goto L_0x065d
        L_0x068e:
            if (r25 != 0) goto L_0x0697
            r5 = 200(0xc8, float:2.8E-43)
            r0 = r48
            r1 = r5
            if (r0 != r1) goto L_0x06a0
        L_0x0697:
            if (r25 == 0) goto L_0x0730
            r5 = 206(0xce, float:2.89E-43)
            r0 = r48
            r1 = r5
            if (r0 == r1) goto L_0x0730
        L_0x06a0:
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGV     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r5 == 0) goto L_0x06e3
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = "http error "
            r7.<init>(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r7
            r1 = r48
            java.lang.StringBuilder r7 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = " for "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r8 = r0
            java.lang.String r8 = r8.mUri     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            android.util.Log.d(r5, r7)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
        L_0x06cc:
            boolean r5 = net.mony.more.qaqad.data.DownloadQueue.isStatusError(r48)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r5 == 0) goto L_0x0710
            r6 = r48
        L_0x06d4:
            r45.abort()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r10 = r34
            r12 = r40
            r9 = r44
            r8 = r47
            r7 = r26
            goto L_0x0295
        L_0x06e3:
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGD     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r5 == 0) goto L_0x06cc
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = "http error "
            r7.<init>(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r7
            r1 = r48
            java.lang.StringBuilder r7 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = " for download "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r8 = r0
            int r8 = r8.mId     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            android.util.Log.d(r5, r7)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            goto L_0x06cc
        L_0x0710:
            r5 = 300(0x12c, float:4.2E-43)
            r0 = r48
            r1 = r5
            if (r0 < r1) goto L_0x0721
            r5 = 400(0x190, float:5.6E-43)
            r0 = r48
            r1 = r5
            if (r0 >= r1) goto L_0x0721
            r6 = 493(0x1ed, float:6.91E-43)
            goto L_0x06d4
        L_0x0721:
            if (r25 == 0) goto L_0x072d
            r5 = 200(0xc8, float:2.8E-43)
            r0 = r48
            r1 = r5
            if (r0 != r1) goto L_0x072d
            r6 = 412(0x19c, float:5.77E-43)
            goto L_0x06d4
        L_0x072d:
            r6 = 494(0x1ee, float:6.92E-43)
            goto L_0x06d4
        L_0x0730:
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGV     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r5 == 0) goto L_0x074f
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = "received response for "
            r7.<init>(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r8 = r0
            java.lang.String r8 = r8.mUri     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            android.util.Log.v(r5, r7)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
        L_0x074f:
            if (r25 != 0) goto L_0x0936
            java.lang.String r5 = "Accept-Ranges"
            r0 = r46
            r1 = r5
            org.apache.http.Header r35 = r0.getFirstHeader(r1)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r35 == 0) goto L_0x0760
            java.lang.String r36 = r35.getValue()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
        L_0x0760:
            java.lang.String r5 = "Content-Disposition"
            r0 = r46
            r1 = r5
            org.apache.http.Header r35 = r0.getFirstHeader(r1)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r35 == 0) goto L_0x076f
            java.lang.String r15 = r35.getValue()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
        L_0x076f:
            java.lang.String r5 = "Content-Location"
            r0 = r46
            r1 = r5
            org.apache.http.Header r35 = r0.getFirstHeader(r1)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r35 == 0) goto L_0x077e
            java.lang.String r16 = r35.getValue()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
        L_0x077e:
            if (r17 != 0) goto L_0x0796
            java.lang.String r5 = "Content-Type"
            r0 = r46
            r1 = r5
            org.apache.http.Header r35 = r0.getFirstHeader(r1)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r35 == 0) goto L_0x0796
            java.lang.String r5 = r35.getValue()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r55
            r1 = r5
            java.lang.String r17 = r0.sanitizeMimeType(r1)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
        L_0x0796:
            java.lang.String r5 = "ETag"
            r0 = r46
            r1 = r5
            org.apache.http.Header r35 = r0.getFirstHeader(r1)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r35 == 0) goto L_0x07a5
            java.lang.String r38 = r35.getValue()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
        L_0x07a5:
            java.lang.String r5 = "Transfer-Encoding"
            r0 = r46
            r1 = r5
            org.apache.http.Header r35 = r0.getFirstHeader(r1)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r35 == 0) goto L_0x07b4
            java.lang.String r39 = r35.getValue()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
        L_0x07b4:
            if (r39 != 0) goto L_0x08a1
            java.lang.String r5 = "Content-Length"
            r0 = r46
            r1 = r5
            org.apache.http.Header r35 = r0.getFirstHeader(r1)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r35 == 0) goto L_0x07c5
            java.lang.String r37 = r35.getValue()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
        L_0x07c5:
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGV     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r5 == 0) goto L_0x0867
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = "Accept-Ranges: "
            r7.<init>(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r7
            r1 = r36
            java.lang.StringBuilder r7 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            android.util.Log.v(r5, r7)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = "Content-Disposition: "
            r7.<init>(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.StringBuilder r7 = r7.append(r15)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            android.util.Log.v(r5, r7)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = "Content-Length: "
            r7.<init>(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r7
            r1 = r37
            java.lang.StringBuilder r7 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            android.util.Log.v(r5, r7)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = "Content-Location: "
            r7.<init>(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r7
            r1 = r16
            java.lang.StringBuilder r7 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            android.util.Log.v(r5, r7)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = "Content-Type: "
            r7.<init>(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r7
            r1 = r17
            java.lang.StringBuilder r7 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            android.util.Log.v(r5, r7)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = "ETag: "
            r7.<init>(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r7
            r1 = r38
            java.lang.StringBuilder r7 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            android.util.Log.v(r5, r7)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = "Transfer-Encoding: "
            r7.<init>(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r7
            r1 = r39
            java.lang.StringBuilder r7 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            android.util.Log.v(r5, r7)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
        L_0x0867:
            r0 = r55
            android.content.Context r0 = r0.mContext     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r12 = r0
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r5 = r0
            java.lang.String r13 = r5.mUri     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r5 = r0
            java.lang.String r14 = r5.mHint     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r37 == 0) goto L_0x08ae
            int r5 = java.lang.Integer.parseInt(r37)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r18 = r5
        L_0x0882:
            net.mony.more.qaqad.service.DownloadFileInfo r31 = net.mony.more.qaqad.service.DownloadHelper.generateSaveFile(r12, r13, r14, r15, r16, r17, r18)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r31
            java.lang.String r0 = r0.mFileName     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r5 = r0
            if (r5 != 0) goto L_0x08b2
            r0 = r31
            int r0 = r0.mStatus     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r6 = r0
            r45.abort()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r10 = r34
            r12 = r40
            r9 = r44
            r8 = r47
            r7 = r26
            goto L_0x0295
        L_0x08a1:
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGV     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r5 == 0) goto L_0x07c5
            java.lang.String r5 = "MuzicWizard"
            java.lang.String r7 = "ignoring content-length because of xfer-encoding"
            android.util.Log.v(r5, r7)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            goto L_0x07c5
        L_0x08ae:
            r5 = 0
            r18 = r5
            goto L_0x0882
        L_0x08b2:
            r0 = r31
            java.lang.String r0 = r0.mFileName     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r11 = r0
            r0 = r31
            java.io.FileOutputStream r0 = r0.mStream     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r49 = r0
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGV     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r5 == 0) goto L_0x08e6
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = "writing "
            r7.<init>(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r8 = r0
            java.lang.String r8 = r8.mUri     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = " to "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            android.util.Log.v(r5, r7)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
        L_0x08e6:
            android.content.ContentValues r53 = new android.content.ContentValues     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r53.<init>()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r5 = "_data"
            r0 = r53
            r1 = r5
            r2 = r11
            r0.put(r1, r2)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r38 == 0) goto L_0x0900
            java.lang.String r5 = "etag"
            r0 = r53
            r1 = r5
            r2 = r38
            r0.put(r1, r2)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
        L_0x0900:
            if (r17 == 0) goto L_0x090c
            java.lang.String r5 = "mimetype"
            r0 = r53
            r1 = r5
            r2 = r17
            r0.put(r1, r2)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
        L_0x090c:
            r23 = -1
            if (r37 == 0) goto L_0x0914
            int r23 = java.lang.Integer.parseInt(r37)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
        L_0x0914:
            java.lang.String r5 = "total_bytes"
            java.lang.Integer r7 = java.lang.Integer.valueOf(r23)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r53
            r1 = r5
            r2 = r7
            r0.put(r1, r2)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r55
            android.content.Context r0 = r0.mContext     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r5 = r0
            android.content.ContentResolver r5 = r5.getContentResolver()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r7 = 0
            r8 = 0
            r0 = r5
            r1 = r24
            r2 = r53
            r3 = r7
            r4 = r8
            r0.update(r1, r2, r3, r4)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
        L_0x0936:
            org.apache.http.HttpEntity r5 = r46.getEntity()     // Catch:{ IOException -> 0x09a6 }
            java.io.InputStream r28 = r5.getContent()     // Catch:{ IOException -> 0x09a6 }
            r10 = r34
        L_0x0940:
            r0 = r28
            r1 = r27
            int r20 = r0.read(r1)     // Catch:{ IOException -> 0x0a31 }
            r5 = -1
            r0 = r20
            r1 = r5
            if (r0 != r1) goto L_0x0b59
            android.content.ContentValues r53 = new android.content.ContentValues     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r53.<init>()     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            java.lang.String r5 = "current_bytes"
            java.lang.Integer r7 = java.lang.Integer.valueOf(r21)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r0 = r53
            r1 = r5
            r2 = r7
            r0.put(r1, r2)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            if (r37 != 0) goto L_0x096f
            java.lang.String r5 = "total_bytes"
            java.lang.Integer r7 = java.lang.Integer.valueOf(r21)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r0 = r53
            r1 = r5
            r2 = r7
            r0.put(r1, r2)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
        L_0x096f:
            r0 = r55
            android.content.Context r0 = r0.mContext     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r5 = r0
            android.content.ContentResolver r5 = r5.getContentResolver()     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r7 = 0
            r8 = 0
            r0 = r5
            r1 = r24
            r2 = r53
            r3 = r7
            r4 = r8
            r0.update(r1, r2, r3, r4)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            if (r37 == 0) goto L_0x0c7b
            int r5 = java.lang.Integer.parseInt(r37)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r0 = r21
            r1 = r5
            if (r0 == r1) goto L_0x0c7b
            r0 = r55
            android.content.Context r0 = r0.mContext     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r5 = r0
            boolean r5 = net.mony.more.qaqad.service.DownloadHelper.isNetworkAvailable(r5)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            if (r5 != 0) goto L_0x0aec
            r6 = 193(0xc1, float:2.7E-43)
            r12 = r40
            r9 = r44
            r8 = r47
            r7 = r26
            goto L_0x0295
        L_0x09a6:
            r5 = move-exception
            r29 = r5
            r0 = r55
            android.content.Context r0 = r0.mContext     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r5 = r0
            boolean r5 = net.mony.more.qaqad.service.DownloadHelper.isNetworkAvailable(r5)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r5 != 0) goto L_0x09c5
            r6 = 193(0xc1, float:2.7E-43)
            r7 = r26
        L_0x09b8:
            r45.abort()     // Catch:{ FileNotFoundException -> 0x104a, RuntimeException -> 0x0ffc, all -> 0x0fb3 }
            r10 = r34
            r12 = r40
            r9 = r44
            r8 = r47
            goto L_0x0295
        L_0x09c5:
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r5 = r0
            int r5 = r5.mNumFailed     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r7 = 5
            if (r5 >= r7) goto L_0x09d3
            r6 = 193(0xc1, float:2.7E-43)
            r7 = 1
            goto L_0x09b8
        L_0x09d3:
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGV     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r5 == 0) goto L_0x0a04
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = "IOException getting entity for "
            r7.<init>(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r8 = r0
            java.lang.String r8 = r8.mUri     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = " : "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r7
            r1 = r29
            java.lang.StringBuilder r7 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            android.util.Log.d(r5, r7)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
        L_0x09ff:
            r6 = 495(0x1ef, float:6.94E-43)
            r7 = r26
            goto L_0x09b8
        L_0x0a04:
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGD     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            if (r5 == 0) goto L_0x09ff
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = "IOException getting entity for download "
            r7.<init>(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r8 = r0
            int r8 = r8.mId     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r8 = " : "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            r0 = r7
            r1 = r29
            java.lang.StringBuilder r7 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            android.util.Log.d(r5, r7)     // Catch:{ FileNotFoundException -> 0x031c, RuntimeException -> 0x03f0, all -> 0x04e0 }
            goto L_0x09ff
        L_0x0a31:
            r29 = move-exception
            android.content.ContentValues r53 = new android.content.ContentValues     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r53.<init>()     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            java.lang.String r5 = "current_bytes"
            java.lang.Integer r7 = java.lang.Integer.valueOf(r21)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r0 = r53
            r1 = r5
            r2 = r7
            r0.put(r1, r2)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r0 = r55
            android.content.Context r0 = r0.mContext     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r5 = r0
            android.content.ContentResolver r5 = r5.getContentResolver()     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r7 = 0
            r8 = 0
            r0 = r5
            r1 = r24
            r2 = r53
            r3 = r7
            r4 = r8
            r0.update(r1, r2, r3, r4)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r0 = r55
            android.content.Context r0 = r0.mContext     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r5 = r0
            boolean r5 = net.mony.more.qaqad.service.DownloadHelper.isNetworkAvailable(r5)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            if (r5 != 0) goto L_0x0a73
            r6 = 193(0xc1, float:2.7E-43)
            r7 = r26
        L_0x0a68:
            r45.abort()     // Catch:{ FileNotFoundException -> 0x106d, RuntimeException -> 0x101f, all -> 0x0fd3 }
            r12 = r40
            r9 = r44
            r8 = r47
            goto L_0x0295
        L_0x0a73:
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r5 = r0
            int r5 = r5.mNumFailed     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r7 = 5
            if (r5 >= r7) goto L_0x0a81
            r6 = 193(0xc1, float:2.7E-43)
            r7 = 1
            goto L_0x0a68
        L_0x0a81:
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGV     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            if (r5 == 0) goto L_0x0ab2
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            java.lang.String r8 = "download IOException for "
            r7.<init>(r8)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r8 = r0
            java.lang.String r8 = r8.mUri     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            java.lang.String r8 = " : "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r0 = r7
            r1 = r29
            java.lang.StringBuilder r7 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            android.util.Log.v(r5, r7)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
        L_0x0aad:
            r6 = 495(0x1ef, float:6.94E-43)
            r7 = r26
            goto L_0x0a68
        L_0x0ab2:
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGD     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            if (r5 == 0) goto L_0x0aad
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            java.lang.String r8 = "download IOException for download "
            r7.<init>(r8)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r8 = r0
            int r8 = r8.mId     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            java.lang.String r8 = " : "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r0 = r7
            r1 = r29
            java.lang.StringBuilder r7 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            android.util.Log.d(r5, r7)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            goto L_0x0aad
        L_0x0adf:
            r5 = move-exception
            r29 = r5
            r12 = r40
            r9 = r44
            r8 = r47
            r7 = r26
            goto L_0x0329
        L_0x0aec:
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r5 = r0
            int r5 = r5.mNumFailed     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r7 = 5
            if (r5 >= r7) goto L_0x0b01
            r6 = 193(0xc1, float:2.7E-43)
            r7 = 1
            r12 = r40
            r9 = r44
            r8 = r47
            goto L_0x0295
        L_0x0b01:
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGV     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            if (r5 == 0) goto L_0x0b2c
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            java.lang.String r8 = "closed socket for "
            r7.<init>(r8)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r8 = r0
            java.lang.String r8 = r8.mUri     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            android.util.Log.v(r5, r7)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
        L_0x0b20:
            r6 = 495(0x1ef, float:6.94E-43)
            r12 = r40
            r9 = r44
            r8 = r47
            r7 = r26
            goto L_0x0295
        L_0x0b2c:
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGD     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            if (r5 == 0) goto L_0x0b20
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            java.lang.String r8 = "closed socket for download "
            r7.<init>(r8)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r8 = r0
            int r8 = r8.mId     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            android.util.Log.d(r5, r7)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            goto L_0x0b20
        L_0x0b4c:
            r5 = move-exception
            r29 = r5
            r12 = r40
            r9 = r44
            r8 = r47
            r7 = r26
            goto L_0x03fd
        L_0x0b59:
            r10 = 1
            r50 = r49
        L_0x0b5c:
            if (r50 != 0) goto L_0x109a
            java.io.FileOutputStream r49 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x1087, FileNotFoundException -> 0x1078, RuntimeException -> 0x102a, all -> 0x0fdd }
            r5 = 1
            r0 = r49
            r1 = r11
            r2 = r5
            r0.<init>(r1, r2)     // Catch:{ IOException -> 0x1087, FileNotFoundException -> 0x1078, RuntimeException -> 0x102a, all -> 0x0fdd }
        L_0x0b68:
            r5 = 0
            r0 = r49
            r1 = r27
            r2 = r5
            r3 = r20
            r0.write(r1, r2, r3)     // Catch:{ IOException -> 0x0c16 }
            r49.close()     // Catch:{ IOException -> 0x0bf8 }
            r49 = 0
        L_0x0b78:
            int r21 = r21 + r20
            long r41 = java.lang.System.currentTimeMillis()     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            int r5 = r21 - r19
            r7 = 4096(0x1000, float:5.74E-42)
            if (r5 <= r7) goto L_0x0bb7
            long r13 = r41 - r51
            r29 = 1500(0x5dc, double:7.41E-321)
            int r5 = (r13 > r29 ? 1 : (r13 == r29 ? 0 : -1))
            if (r5 <= 0) goto L_0x0bb7
            android.content.ContentValues r53 = new android.content.ContentValues     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r53.<init>()     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            java.lang.String r5 = "current_bytes"
            java.lang.Integer r7 = java.lang.Integer.valueOf(r21)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r0 = r53
            r1 = r5
            r2 = r7
            r0.put(r1, r2)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r0 = r55
            android.content.Context r0 = r0.mContext     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r5 = r0
            android.content.ContentResolver r5 = r5.getContentResolver()     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r7 = 0
            r8 = 0
            r0 = r5
            r1 = r24
            r2 = r53
            r3 = r7
            r4 = r8
            r0.update(r1, r2, r3, r4)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r19 = r21
            r51 = r41
        L_0x0bb7:
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGV     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r5 = r0
            monitor-enter(r5)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ all -> 0x0c69 }
            r7 = r0
            int r7 = r7.mControl     // Catch:{ all -> 0x0c69 }
            r8 = 1
            if (r7 != r8) goto L_0x0c32
            boolean r7 = net.mony.more.qaqad.utils.Constants.LOGV     // Catch:{ all -> 0x0c69 }
            if (r7 == 0) goto L_0x0be8
            java.lang.String r7 = "MuzicWizard"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x0c69 }
            java.lang.String r9 = "paused "
            r8.<init>(r9)     // Catch:{ all -> 0x0c69 }
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ all -> 0x0c69 }
            r9 = r0
            java.lang.String r9 = r9.mUri     // Catch:{ all -> 0x0c69 }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x0c69 }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x0c69 }
            android.util.Log.e(r7, r8)     // Catch:{ all -> 0x0c69 }
        L_0x0be8:
            r6 = 193(0xc1, float:2.7E-43)
            r45.abort()     // Catch:{ all -> 0x0c69 }
            monitor-exit(r5)     // Catch:{ all -> 0x0c69 }
            r12 = r40
            r9 = r44
            r8 = r47
            r7 = r26
            goto L_0x0295
        L_0x0bf8:
            r29 = move-exception
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGV     // Catch:{ IOException -> 0x0c16 }
            if (r5 == 0) goto L_0x0b78
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0c16 }
            java.lang.String r8 = "exception when closing the file during download : "
            r7.<init>(r8)     // Catch:{ IOException -> 0x0c16 }
            r0 = r7
            r1 = r29
            java.lang.StringBuilder r7 = r0.append(r1)     // Catch:{ IOException -> 0x0c16 }
            java.lang.String r7 = r7.toString()     // Catch:{ IOException -> 0x0c16 }
            android.util.Log.v(r5, r7)     // Catch:{ IOException -> 0x0c16 }
            goto L_0x0b78
        L_0x0c16:
            r5 = move-exception
            r29 = r5
        L_0x0c19:
            r0 = r55
            android.content.Context r0 = r0.mContext     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r5 = r0
            r13 = 4096(0x1000, double:2.0237E-320)
            boolean r5 = net.mony.more.qaqad.service.DownloadHelper.discardPurgeableFiles(r5, r13)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            if (r5 != 0) goto L_0x1096
            r6 = 492(0x1ec, float:6.9E-43)
            r12 = r40
            r9 = r44
            r8 = r47
            r7 = r26
            goto L_0x0295
        L_0x0c32:
            monitor-exit(r5)     // Catch:{ all -> 0x0c69 }
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r5 = r0
            int r5 = r5.mStatus     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r7 = 490(0x1ea, float:6.87E-43)
            if (r5 != r7) goto L_0x0940
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGV     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            if (r5 == 0) goto L_0x0c78
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            java.lang.String r8 = "canceled "
            r7.<init>(r8)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r8 = r0
            java.lang.String r8 = r8.mUri     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            android.util.Log.d(r5, r7)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
        L_0x0c5d:
            r6 = 490(0x1ea, float:6.87E-43)
            r12 = r40
            r9 = r44
            r8 = r47
            r7 = r26
            goto L_0x0295
        L_0x0c69:
            r7 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x0c69 }
            throw r7     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
        L_0x0c6c:
            r5 = move-exception
            r14 = r5
            r12 = r40
            r9 = r44
            r8 = r47
            r7 = r26
            goto L_0x04ec
        L_0x0c78:
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGD     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            goto L_0x0c5d
        L_0x0c7b:
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGV     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            if (r5 == 0) goto L_0x0c9a
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            java.lang.String r8 = "download completed for "
            r7.<init>(r8)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            r8 = r0
            java.lang.String r8 = r8.mUri     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
            android.util.Log.v(r5, r7)     // Catch:{ FileNotFoundException -> 0x0adf, RuntimeException -> 0x0b4c, all -> 0x0c6c }
        L_0x0c9a:
            r6 = 200(0xc8, float:2.8E-43)
            r12 = r40
            r9 = r44
            r8 = r47
            r7 = r26
            goto L_0x0295
        L_0x0ca6:
            r29 = move-exception
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGV
            if (r5 == 0) goto L_0x036b
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            java.lang.String r14 = "exception when closing the file after download : "
            r13.<init>(r14)
            r0 = r13
            r1 = r29
            java.lang.StringBuilder r13 = r0.append(r1)
            java.lang.String r13 = r13.toString()
            android.util.Log.v(r5, r13)
            goto L_0x036b
        L_0x0cc4:
            boolean r5 = net.mony.more.qaqad.data.DownloadQueue.isStatusSuccess(r6)
            if (r5 == 0) goto L_0x037c
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0cd9, SyncFailedException -> 0x0cff, IOException -> 0x0d25, RuntimeException -> 0x0d4b }
            r13 = 1
            r5.<init>(r11, r13)     // Catch:{ FileNotFoundException -> 0x0cd9, SyncFailedException -> 0x0cff, IOException -> 0x0d25, RuntimeException -> 0x0d4b }
            java.io.FileDescriptor r5 = r5.getFD()     // Catch:{ FileNotFoundException -> 0x0cd9, SyncFailedException -> 0x0cff, IOException -> 0x0d25, RuntimeException -> 0x0d4b }
            r5.sync()     // Catch:{ FileNotFoundException -> 0x0cd9, SyncFailedException -> 0x0cff, IOException -> 0x0d25, RuntimeException -> 0x0d4b }
            goto L_0x037c
        L_0x0cd9:
            r5 = move-exception
            r29 = r5
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            java.lang.String r14 = "file "
            r13.<init>(r14)
            java.lang.StringBuilder r13 = r13.append(r11)
            java.lang.String r14 = " not found: "
            java.lang.StringBuilder r13 = r13.append(r14)
            r0 = r13
            r1 = r29
            java.lang.StringBuilder r13 = r0.append(r1)
            java.lang.String r13 = r13.toString()
            android.util.Log.w(r5, r13)
            goto L_0x037c
        L_0x0cff:
            r5 = move-exception
            r29 = r5
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            java.lang.String r14 = "file "
            r13.<init>(r14)
            java.lang.StringBuilder r13 = r13.append(r11)
            java.lang.String r14 = " sync failed: "
            java.lang.StringBuilder r13 = r13.append(r14)
            r0 = r13
            r1 = r29
            java.lang.StringBuilder r13 = r0.append(r1)
            java.lang.String r13 = r13.toString()
            android.util.Log.w(r5, r13)
            goto L_0x037c
        L_0x0d25:
            r5 = move-exception
            r29 = r5
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            java.lang.String r14 = "IOException trying to sync "
            r13.<init>(r14)
            java.lang.StringBuilder r13 = r13.append(r11)
            java.lang.String r14 = ": "
            java.lang.StringBuilder r13 = r13.append(r14)
            r0 = r13
            r1 = r29
            java.lang.StringBuilder r13 = r0.append(r1)
            java.lang.String r13 = r13.toString()
            android.util.Log.w(r5, r13)
            goto L_0x037c
        L_0x0d4b:
            r5 = move-exception
            r29 = r5
            java.lang.String r5 = "MuzicWizard"
            java.lang.String r13 = "exception while syncing file: "
            r0 = r5
            r1 = r13
            r2 = r29
            android.util.Log.w(r0, r1, r2)
            goto L_0x037c
        L_0x0d5b:
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGD     // Catch:{ all -> 0x0d80 }
            if (r5 == 0) goto L_0x0420
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ all -> 0x0d80 }
            java.lang.String r14 = "Exception for id "
            r13.<init>(r14)     // Catch:{ all -> 0x0d80 }
            r0 = r55
            net.mony.more.qaqad.service.DownloadInfo r0 = r0.mInfo     // Catch:{ all -> 0x0d80 }
            r14 = r0
            int r14 = r14.mId     // Catch:{ all -> 0x0d80 }
            java.lang.StringBuilder r13 = r13.append(r14)     // Catch:{ all -> 0x0d80 }
            java.lang.String r13 = r13.toString()     // Catch:{ all -> 0x0d80 }
            r0 = r5
            r1 = r13
            r2 = r29
            android.util.Log.d(r0, r1, r2)     // Catch:{ all -> 0x0d80 }
            goto L_0x0420
        L_0x0d80:
            r5 = move-exception
            r14 = r5
            goto L_0x04ec
        L_0x0d84:
            r29 = move-exception
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGV
            if (r5 == 0) goto L_0x043d
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            java.lang.String r14 = "exception when closing the file after download : "
            r13.<init>(r14)
            r0 = r13
            r1 = r29
            java.lang.StringBuilder r13 = r0.append(r1)
            java.lang.String r13 = r13.toString()
            android.util.Log.v(r5, r13)
            goto L_0x043d
        L_0x0da2:
            boolean r5 = net.mony.more.qaqad.data.DownloadQueue.isStatusSuccess(r6)
            if (r5 == 0) goto L_0x044e
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0db7, SyncFailedException -> 0x0ddd, IOException -> 0x0e03, RuntimeException -> 0x0e29 }
            r13 = 1
            r5.<init>(r11, r13)     // Catch:{ FileNotFoundException -> 0x0db7, SyncFailedException -> 0x0ddd, IOException -> 0x0e03, RuntimeException -> 0x0e29 }
            java.io.FileDescriptor r5 = r5.getFD()     // Catch:{ FileNotFoundException -> 0x0db7, SyncFailedException -> 0x0ddd, IOException -> 0x0e03, RuntimeException -> 0x0e29 }
            r5.sync()     // Catch:{ FileNotFoundException -> 0x0db7, SyncFailedException -> 0x0ddd, IOException -> 0x0e03, RuntimeException -> 0x0e29 }
            goto L_0x044e
        L_0x0db7:
            r5 = move-exception
            r29 = r5
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            java.lang.String r14 = "file "
            r13.<init>(r14)
            java.lang.StringBuilder r13 = r13.append(r11)
            java.lang.String r14 = " not found: "
            java.lang.StringBuilder r13 = r13.append(r14)
            r0 = r13
            r1 = r29
            java.lang.StringBuilder r13 = r0.append(r1)
            java.lang.String r13 = r13.toString()
            android.util.Log.w(r5, r13)
            goto L_0x044e
        L_0x0ddd:
            r5 = move-exception
            r29 = r5
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            java.lang.String r14 = "file "
            r13.<init>(r14)
            java.lang.StringBuilder r13 = r13.append(r11)
            java.lang.String r14 = " sync failed: "
            java.lang.StringBuilder r13 = r13.append(r14)
            r0 = r13
            r1 = r29
            java.lang.StringBuilder r13 = r0.append(r1)
            java.lang.String r13 = r13.toString()
            android.util.Log.w(r5, r13)
            goto L_0x044e
        L_0x0e03:
            r5 = move-exception
            r29 = r5
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            java.lang.String r14 = "IOException trying to sync "
            r13.<init>(r14)
            java.lang.StringBuilder r13 = r13.append(r11)
            java.lang.String r14 = ": "
            java.lang.StringBuilder r13 = r13.append(r14)
            r0 = r13
            r1 = r29
            java.lang.StringBuilder r13 = r0.append(r1)
            java.lang.String r13 = r13.toString()
            android.util.Log.w(r5, r13)
            goto L_0x044e
        L_0x0e29:
            r5 = move-exception
            r29 = r5
            java.lang.String r5 = "MuzicWizard"
            java.lang.String r13 = "exception while syncing file: "
            r0 = r5
            r1 = r13
            r2 = r29
            android.util.Log.w(r0, r1, r2)
            goto L_0x044e
        L_0x0e39:
            r29 = move-exception
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGV
            if (r5 == 0) goto L_0x0507
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            java.lang.String r15 = "exception when closing the file after download : "
            r13.<init>(r15)
            r0 = r13
            r1 = r29
            java.lang.StringBuilder r13 = r0.append(r1)
            java.lang.String r13 = r13.toString()
            android.util.Log.v(r5, r13)
            goto L_0x0507
        L_0x0e57:
            boolean r5 = net.mony.more.qaqad.data.DownloadQueue.isStatusSuccess(r6)
            if (r5 == 0) goto L_0x0518
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0e6c, SyncFailedException -> 0x0e92, IOException -> 0x0eb8, RuntimeException -> 0x0ede }
            r13 = 1
            r5.<init>(r11, r13)     // Catch:{ FileNotFoundException -> 0x0e6c, SyncFailedException -> 0x0e92, IOException -> 0x0eb8, RuntimeException -> 0x0ede }
            java.io.FileDescriptor r5 = r5.getFD()     // Catch:{ FileNotFoundException -> 0x0e6c, SyncFailedException -> 0x0e92, IOException -> 0x0eb8, RuntimeException -> 0x0ede }
            r5.sync()     // Catch:{ FileNotFoundException -> 0x0e6c, SyncFailedException -> 0x0e92, IOException -> 0x0eb8, RuntimeException -> 0x0ede }
            goto L_0x0518
        L_0x0e6c:
            r5 = move-exception
            r29 = r5
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            java.lang.String r15 = "file "
            r13.<init>(r15)
            java.lang.StringBuilder r13 = r13.append(r11)
            java.lang.String r15 = " not found: "
            java.lang.StringBuilder r13 = r13.append(r15)
            r0 = r13
            r1 = r29
            java.lang.StringBuilder r13 = r0.append(r1)
            java.lang.String r13 = r13.toString()
            android.util.Log.w(r5, r13)
            goto L_0x0518
        L_0x0e92:
            r5 = move-exception
            r29 = r5
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            java.lang.String r15 = "file "
            r13.<init>(r15)
            java.lang.StringBuilder r13 = r13.append(r11)
            java.lang.String r15 = " sync failed: "
            java.lang.StringBuilder r13 = r13.append(r15)
            r0 = r13
            r1 = r29
            java.lang.StringBuilder r13 = r0.append(r1)
            java.lang.String r13 = r13.toString()
            android.util.Log.w(r5, r13)
            goto L_0x0518
        L_0x0eb8:
            r5 = move-exception
            r29 = r5
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            java.lang.String r15 = "IOException trying to sync "
            r13.<init>(r15)
            java.lang.StringBuilder r13 = r13.append(r11)
            java.lang.String r15 = ": "
            java.lang.StringBuilder r13 = r13.append(r15)
            r0 = r13
            r1 = r29
            java.lang.StringBuilder r13 = r0.append(r1)
            java.lang.String r13 = r13.toString()
            android.util.Log.w(r5, r13)
            goto L_0x0518
        L_0x0ede:
            r5 = move-exception
            r29 = r5
            java.lang.String r5 = "MuzicWizard"
            java.lang.String r13 = "exception while syncing file: "
            r0 = r5
            r1 = r13
            r2 = r29
            android.util.Log.w(r0, r1, r2)
            goto L_0x0518
        L_0x0eee:
            r29 = move-exception
            boolean r5 = net.mony.more.qaqad.utils.Constants.LOGV
            if (r5 == 0) goto L_0x02b0
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            java.lang.String r14 = "exception when closing the file after download : "
            r13.<init>(r14)
            r0 = r13
            r1 = r29
            java.lang.StringBuilder r13 = r0.append(r1)
            java.lang.String r13 = r13.toString()
            android.util.Log.v(r5, r13)
            goto L_0x02b0
        L_0x0f0c:
            boolean r5 = net.mony.more.qaqad.data.DownloadQueue.isStatusSuccess(r6)
            if (r5 == 0) goto L_0x02c1
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0f21, SyncFailedException -> 0x0f47, IOException -> 0x0f6d, RuntimeException -> 0x0f93 }
            r13 = 1
            r5.<init>(r11, r13)     // Catch:{ FileNotFoundException -> 0x0f21, SyncFailedException -> 0x0f47, IOException -> 0x0f6d, RuntimeException -> 0x0f93 }
            java.io.FileDescriptor r5 = r5.getFD()     // Catch:{ FileNotFoundException -> 0x0f21, SyncFailedException -> 0x0f47, IOException -> 0x0f6d, RuntimeException -> 0x0f93 }
            r5.sync()     // Catch:{ FileNotFoundException -> 0x0f21, SyncFailedException -> 0x0f47, IOException -> 0x0f6d, RuntimeException -> 0x0f93 }
            goto L_0x02c1
        L_0x0f21:
            r5 = move-exception
            r29 = r5
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            java.lang.String r14 = "file "
            r13.<init>(r14)
            java.lang.StringBuilder r13 = r13.append(r11)
            java.lang.String r14 = " not found: "
            java.lang.StringBuilder r13 = r13.append(r14)
            r0 = r13
            r1 = r29
            java.lang.StringBuilder r13 = r0.append(r1)
            java.lang.String r13 = r13.toString()
            android.util.Log.w(r5, r13)
            goto L_0x02c1
        L_0x0f47:
            r5 = move-exception
            r29 = r5
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            java.lang.String r14 = "file "
            r13.<init>(r14)
            java.lang.StringBuilder r13 = r13.append(r11)
            java.lang.String r14 = " sync failed: "
            java.lang.StringBuilder r13 = r13.append(r14)
            r0 = r13
            r1 = r29
            java.lang.StringBuilder r13 = r0.append(r1)
            java.lang.String r13 = r13.toString()
            android.util.Log.w(r5, r13)
            goto L_0x02c1
        L_0x0f6d:
            r5 = move-exception
            r29 = r5
            java.lang.String r5 = "MuzicWizard"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            java.lang.String r14 = "IOException trying to sync "
            r13.<init>(r14)
            java.lang.StringBuilder r13 = r13.append(r11)
            java.lang.String r14 = ": "
            java.lang.StringBuilder r13 = r13.append(r14)
            r0 = r13
            r1 = r29
            java.lang.StringBuilder r13 = r0.append(r1)
            java.lang.String r13 = r13.toString()
            android.util.Log.w(r5, r13)
            goto L_0x02c1
        L_0x0f93:
            r5 = move-exception
            r29 = r5
            java.lang.String r5 = "MuzicWizard"
            java.lang.String r13 = "exception while syncing file: "
            r0 = r5
            r1 = r13
            r2 = r29
            android.util.Log.w(r0, r1, r2)
            goto L_0x02c1
        L_0x0fa3:
            r5 = move-exception
            r14 = r5
            r49 = r50
            r10 = r34
            r12 = r40
            r9 = r44
            r8 = r47
            r7 = r26
            goto L_0x04ec
        L_0x0fb3:
            r5 = move-exception
            r14 = r5
            r10 = r34
            r12 = r40
            r9 = r44
            r8 = r47
            goto L_0x04ec
        L_0x0fbf:
            r5 = move-exception
            r14 = r5
            r10 = r34
            r12 = r40
            r9 = r44
            goto L_0x04ec
        L_0x0fc9:
            r5 = move-exception
            r14 = r5
            r10 = r34
            r8 = r47
            r7 = r26
            goto L_0x04ec
        L_0x0fd3:
            r5 = move-exception
            r14 = r5
            r12 = r40
            r9 = r44
            r8 = r47
            goto L_0x04ec
        L_0x0fdd:
            r5 = move-exception
            r14 = r5
            r49 = r50
            r12 = r40
            r9 = r44
            r8 = r47
            r7 = r26
            goto L_0x04ec
        L_0x0feb:
            r5 = move-exception
            r29 = r5
            r49 = r50
            r10 = r34
            r12 = r40
            r9 = r44
            r8 = r47
            r7 = r26
            goto L_0x03fd
        L_0x0ffc:
            r5 = move-exception
            r29 = r5
            r10 = r34
            r12 = r40
            r9 = r44
            r8 = r47
            goto L_0x03fd
        L_0x1009:
            r5 = move-exception
            r29 = r5
            r10 = r34
            r12 = r40
            r9 = r44
            goto L_0x03fd
        L_0x1014:
            r5 = move-exception
            r29 = r5
            r10 = r34
            r8 = r47
            r7 = r26
            goto L_0x03fd
        L_0x101f:
            r5 = move-exception
            r29 = r5
            r12 = r40
            r9 = r44
            r8 = r47
            goto L_0x03fd
        L_0x102a:
            r5 = move-exception
            r29 = r5
            r49 = r50
            r12 = r40
            r9 = r44
            r8 = r47
            r7 = r26
            goto L_0x03fd
        L_0x1039:
            r5 = move-exception
            r29 = r5
            r49 = r50
            r10 = r34
            r12 = r40
            r9 = r44
            r8 = r47
            r7 = r26
            goto L_0x0329
        L_0x104a:
            r5 = move-exception
            r29 = r5
            r10 = r34
            r12 = r40
            r9 = r44
            r8 = r47
            goto L_0x0329
        L_0x1057:
            r5 = move-exception
            r29 = r5
            r10 = r34
            r12 = r40
            r9 = r44
            goto L_0x0329
        L_0x1062:
            r5 = move-exception
            r29 = r5
            r10 = r34
            r8 = r47
            r7 = r26
            goto L_0x0329
        L_0x106d:
            r5 = move-exception
            r29 = r5
            r12 = r40
            r9 = r44
            r8 = r47
            goto L_0x0329
        L_0x1078:
            r5 = move-exception
            r29 = r5
            r49 = r50
            r12 = r40
            r9 = r44
            r8 = r47
            r7 = r26
            goto L_0x0329
        L_0x1087:
            r5 = move-exception
            r29 = r5
            r49 = r50
            goto L_0x0c19
        L_0x108e:
            r5 = move-exception
            r8 = r47
            goto L_0x028c
        L_0x1093:
            r5 = move-exception
            goto L_0x028c
        L_0x1096:
            r50 = r49
            goto L_0x0b5c
        L_0x109a:
            r49 = r50
            goto L_0x0b68
        L_0x109e:
            r8 = r47
            goto L_0x028c
        */
        throw new UnsupportedOperationException("Method not decompiled: net.mony.more.qaqad.service.DownloadThread.run():void");
    }

    private void notifyDownloadCompleted(int status, boolean countRetry, int retryAfter, int redirectCount, boolean gotData, String filename, String uri, String mimeType) {
        notifyThroughDatabase(status, countRetry, retryAfter, redirectCount, gotData, filename, uri, mimeType);
        if (DownloadQueue.isStatusCompleted(status)) {
            notifyThroughIntent();
        }
        ScanMediafile(this.mContext, this.mInfo.mFileName);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private void notifyThroughDatabase(int status, boolean countRetry, int retryAfter, int redirectCount, boolean gotData, String filename, String uri, String mimeType) {
        ContentValues values = new ContentValues();
        values.put(DownloadQueue.COL_STATUS, Integer.valueOf(status));
        values.put("_data", filename);
        if (uri != null) {
            values.put("uri", uri);
        }
        values.put("mimetype", mimeType);
        values.put("lastmod", Long.valueOf(System.currentTimeMillis()));
        values.put(DownloadQueue.RETRY_AFTER_X_REDIRECT_COUNT, Integer.valueOf((redirectCount << 28) + retryAfter));
        if (!countRetry) {
            values.put(DownloadQueue.FAILED_CONNECTIONS, (Integer) 0);
        } else if (gotData) {
            values.put(DownloadQueue.FAILED_CONNECTIONS, (Integer) 1);
        } else {
            values.put(DownloadQueue.FAILED_CONNECTIONS, Integer.valueOf(this.mInfo.mNumFailed + 1));
        }
        this.mContext.getContentResolver().update(ContentUris.withAppendedId(DownloadQueue.CONTENT_URI, (long) this.mInfo.mId), values, null, null);
    }

    private void notifyThroughIntent() {
        Uri uri = Uri.parse(DownloadQueue.CONTENT_URI + "/" + this.mInfo.mId);
        Intent intent = new Intent(Constants.ACTION_MOVE);
        intent.setClassName(this.mContext, DownloadReceiver.class.getName());
        intent.setData(uri);
        this.mContext.sendBroadcast(intent);
    }

    private String sanitizeMimeType(String mimeType) {
        try {
            String mimeType2 = mimeType.trim().toLowerCase(Locale.ENGLISH);
            int semicolonIndex = mimeType2.indexOf(59);
            if (semicolonIndex != -1) {
                mimeType2 = mimeType2.substring(0, semicolonIndex);
            }
            return mimeType2;
        } catch (NullPointerException e) {
            return null;
        }
    }

    private void ScanMediafile(Context context, final String fullpathame) {
        this.mScanner = new MediaScannerConnection(context, new MediaScannerConnection.MediaScannerConnectionClient() {
            public void onMediaScannerConnected() {
                DownloadThread.this.mScanner.scanFile(fullpathame, null);
            }

            public void onScanCompleted(String path, Uri uri) {
                if (path.equals(fullpathame)) {
                    DownloadThread.this.mScanner.disconnect();
                }
            }
        });
        this.mScanner.connect();
    }
}
