package net.mony.more.qaqad.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.qwapi.adclient.android.utils.Utils;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class NetUtils {
    private static final int IO_BUFFER_SIZE = 4096;

    public static Bitmap loadBitmap(String url) {
        ByteArrayOutputStream dataStream;
        BufferedOutputStream out;
        InputStream in = null;
        BufferedOutputStream out2 = null;
        try {
            InputStream in2 = new BufferedInputStream(new URL(url).openStream(), 4096);
            try {
                dataStream = new ByteArrayOutputStream();
                out = new BufferedOutputStream(dataStream, 4096);
            } catch (IOException e) {
                in = in2;
                closeStream(in);
                closeStream(out2);
                return null;
            } catch (Throwable th) {
                th = th;
                in = in2;
                closeStream(in);
                closeStream(out2);
                throw th;
            }
            try {
                copy(in2, out);
                out.flush();
                byte[] data = dataStream.toByteArray();
                Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                closeStream(in2);
                closeStream(out);
                return bitmap;
            } catch (IOException e2) {
                out2 = out;
                in = in2;
                closeStream(in);
                closeStream(out2);
                return null;
            } catch (Throwable th2) {
                th = th2;
                out2 = out;
                in = in2;
                closeStream(in);
                closeStream(out2);
                throw th;
            }
        } catch (IOException e3) {
            closeStream(in);
            closeStream(out2);
            return null;
        } catch (Throwable th3) {
            th = th3;
            closeStream(in);
            closeStream(out2);
            throw th;
        }
    }

    public static String loadString(String url) {
        InputStream in = null;
        BufferedOutputStream out = null;
        try {
            InputStream in2 = new BufferedInputStream(new URL(url).openStream(), 4096);
            try {
                ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
                BufferedOutputStream out2 = new BufferedOutputStream(dataStream, 4096);
                try {
                    copy(in2, out2);
                    out2.flush();
                    String str = new String(dataStream.toByteArray());
                    closeStream(in2);
                    closeStream(out2);
                    return str;
                } catch (IOException e) {
                    out = out2;
                    in = in2;
                    closeStream(in);
                    closeStream(out);
                    return Utils.EMPTY_STRING;
                } catch (Throwable th) {
                    th = th;
                    out = out2;
                    in = in2;
                    closeStream(in);
                    closeStream(out);
                    throw th;
                }
            } catch (IOException e2) {
                in = in2;
                closeStream(in);
                closeStream(out);
                return Utils.EMPTY_STRING;
            } catch (Throwable th2) {
                th = th2;
                in = in2;
                closeStream(in);
                closeStream(out);
                throw th;
            }
        } catch (IOException e3) {
            closeStream(in);
            closeStream(out);
            return Utils.EMPTY_STRING;
        } catch (Throwable th3) {
            th = th3;
            closeStream(in);
            closeStream(out);
            throw th;
        }
    }

    public static String loadStringAsPC(String url) {
        Exception e;
        ByteArrayOutputStream dataStream;
        BufferedOutputStream out;
        InputStream in = null;
        BufferedOutputStream out2 = null;
        try {
            HttpURLConnection urlConn = (HttpURLConnection) new URL(url).openConnection();
            urlConn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; ja; rv:1.9.0.6) Gecko/2009011913 Firefox/3.0.6");
            urlConn.setDoInput(true);
            InputStream in2 = new BufferedInputStream(urlConn.getInputStream(), 4096);
            try {
                dataStream = new ByteArrayOutputStream();
                out = new BufferedOutputStream(dataStream, 4096);
            } catch (Exception e2) {
                e = e2;
                in = in2;
                try {
                    e.printStackTrace();
                    closeStream(in);
                    closeStream(out2);
                    return Utils.EMPTY_STRING;
                } catch (Throwable th) {
                    th = th;
                    closeStream(in);
                    closeStream(out2);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                in = in2;
                closeStream(in);
                closeStream(out2);
                throw th;
            }
            try {
                copy(in2, out);
                out.flush();
                String retval = new String(dataStream.toByteArray());
                closeStream(in2);
                closeStream(out);
                return retval;
            } catch (Exception e3) {
                e = e3;
                out2 = out;
                in = in2;
                e.printStackTrace();
                closeStream(in);
                closeStream(out2);
                return Utils.EMPTY_STRING;
            } catch (Throwable th3) {
                th = th3;
                out2 = out;
                in = in2;
                closeStream(in);
                closeStream(out2);
                throw th;
            }
        } catch (Exception e4) {
            e = e4;
            e.printStackTrace();
            closeStream(in);
            closeStream(out2);
            return Utils.EMPTY_STRING;
        }
    }

    public static String loadStringAsPCZH(String url) {
        Exception e;
        String retval = Utils.EMPTY_STRING;
        InputStreamReader is = null;
        try {
            HttpURLConnection urlConn = (HttpURLConnection) new URL(url).openConnection();
            urlConn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; ja; rv:1.9.0.6) Gecko/2009011913 Firefox/3.0.6");
            urlConn.setDoInput(true);
            InputStreamReader is2 = new InputStreamReader(urlConn.getInputStream(), "GB2312");
            try {
                StringBuilder builder = new StringBuilder(4096);
                char[] buff = new char[4096];
                while (true) {
                    int len = is2.read(buff, 0, 4096);
                    if (len <= 0) {
                        break;
                    }
                    builder.append(buff, 0, len);
                }
                retval = builder.toString();
                closeStream(is2);
                closeStream(null);
            } catch (Exception e2) {
                e = e2;
                is = is2;
            } catch (Throwable th) {
                th = th;
                is = is2;
                closeStream(is);
                closeStream(null);
                throw th;
            }
        } catch (Exception e3) {
            e = e3;
            try {
                e.printStackTrace();
                closeStream(is);
                closeStream(null);
                return retval;
            } catch (Throwable th2) {
                th = th2;
                closeStream(is);
                closeStream(null);
                throw th;
            }
        }
        return retval;
    }

    private static void closeStream(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
            }
        }
    }

    private static void copy(InputStream in, OutputStream out) throws IOException {
        byte[] b = new byte[4096];
        while (true) {
            int read = in.read(b);
            if (read != -1) {
                out.write(b, 0, read);
            } else {
                return;
            }
        }
    }
}
