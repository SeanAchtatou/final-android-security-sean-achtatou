package net.mony.more.qaqad;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.SystemClock;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import net.mony.more.qaqad.service.PlaybackService;
import net.mony.more.qaqad.service.PlaybackServiceHelper;
import net.mony.more.qaqad.task.AlbumArtTask;
import net.mony.more.qaqad.task.LyricTask;
import net.mony.more.qaqad.utils.Constants;

public class PlayerActivity extends Activity implements AlbumArtTask.Listener, LyricTask.Listener {
    private static final int MSG_QUIT = 2;
    private static final int MSG_REFRESH = 1;
    private static final String P_ALBUM = "album";
    private static final String P_ARTIST = "artist";
    private static final String P_LYRICURL = "lyricurl";
    private static final String P_SONG = "song";
    private static final String P_SONGURL = "url";
    private TextView mCurrentTime;
    /* access modifiers changed from: private */
    public long mDuration;
    /* access modifiers changed from: private */
    public boolean mFromTouch = false;
    private final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    PlayerActivity.this.queueNextRefresh(PlayerActivity.this.refreshNow());
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public long mLastSeekEventTime;
    private Button mLyricBtn = null;
    private LyricTask mLyricTask = null;
    /* access modifiers changed from: private */
    public View mLyricView = null;
    private ServiceConnection mOsc = new ServiceConnection() {
        public void onServiceConnected(ComponentName name, IBinder service) {
            PlayerActivity.this.updateTrackInfo();
            PlayerActivity.this.showPlayPauseButton();
        }

        public void onServiceDisconnected(ComponentName name) {
        }
    };
    private Button mPauseBtn;
    private Button mPlayBtn;
    /* access modifiers changed from: private */
    public long mPosOverride = -1;
    private SeekBar mProgress = null;
    private SeekBar.OnSeekBarChangeListener mSeekListener = new SeekBar.OnSeekBarChangeListener() {
        public void onStartTrackingTouch(SeekBar seekBar) {
            PlayerActivity.this.mLastSeekEventTime = 0;
            PlayerActivity.this.mFromTouch = true;
        }

        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (fromUser && PlaybackServiceHelper.mService != null) {
                long now = SystemClock.elapsedRealtime();
                if (now - PlayerActivity.this.mLastSeekEventTime > 250) {
                    PlayerActivity.this.mLastSeekEventTime = now;
                    PlayerActivity.this.mPosOverride = (PlayerActivity.this.mDuration * ((long) progress)) / 1000;
                    PlaybackServiceHelper.mService.seek(PlayerActivity.this.mPosOverride);
                    if (!PlayerActivity.this.mFromTouch) {
                        long unused = PlayerActivity.this.refreshNow();
                        PlayerActivity.this.mPosOverride = -1;
                    }
                }
            }
        }

        public void onStopTrackingTouch(SeekBar seekBar) {
            PlayerActivity.this.mPosOverride = -1;
            PlayerActivity.this.mFromTouch = false;
        }
    };
    private BroadcastReceiver mStatusListener = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(PlaybackService.META_CHANGED)) {
                PlayerActivity.this.updateTrackInfo();
                PlayerActivity.this.showPlayPauseButton();
                PlayerActivity.this.queueNextRefresh(1);
            } else if (action.equals(PlaybackService.PLAYBACK_COMPLETE)) {
                PlayerActivity.this.finish();
            } else if (action.equals(PlaybackService.PLAYSTATE_CHANGED)) {
                PlayerActivity.this.showPlayPauseButton();
            }
        }
    };
    private AlbumArtTask mTask = null;
    private TextView mTotalTime;
    private boolean mbPaused;

    public static void startActivity(Context ctx, String url, String song, String artist, String album, String lyricURL) {
        Intent intent = new Intent(ctx, PlayerActivity.class);
        intent.putExtra(P_SONGURL, url);
        intent.putExtra("song", song);
        intent.putExtra("artist", artist);
        intent.putExtra("album", album);
        intent.putExtra(P_LYRICURL, lyricURL);
        ctx.startActivity(intent);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setVolumeControlStream(3);
        requestWindowFeature(1);
        setContentView((int) R.layout.player);
        AdsView.createAdWhirl(this);
        this.mProgress = (SeekBar) findViewById(R.id.SeekBar);
        this.mProgress.setOnSeekBarChangeListener(this.mSeekListener);
        this.mProgress.setMax(Constants.MAX_DOWNLOADS);
        this.mCurrentTime = (TextView) findViewById(R.id.CurrentTime);
        this.mTotalTime = (TextView) findViewById(R.id.TotalTime);
        this.mLyricView = findViewById(R.id.LyricPanel);
        this.mLyricBtn = (Button) findViewById(R.id.LyricBtn);
        this.mLyricBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (PlayerActivity.this.mLyricView.getVisibility() == 4) {
                    PlayerActivity.this.mLyricView.setVisibility(0);
                } else {
                    PlayerActivity.this.mLyricView.setVisibility(4);
                }
            }
        });
        this.mPlayBtn = (Button) findViewById(R.id.PlayBtn);
        this.mPlayBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (PlaybackServiceHelper.mService != null) {
                    PlaybackServiceHelper.mService.play();
                    long unused = PlayerActivity.this.refreshNow();
                    PlayerActivity.this.showPlayPauseButton();
                }
            }
        });
        this.mPauseBtn = (Button) findViewById(R.id.PauseBtn);
        this.mPauseBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (PlaybackServiceHelper.mService != null) {
                    PlaybackServiceHelper.mService.pause();
                    long unused = PlayerActivity.this.refreshNow();
                    PlayerActivity.this.showPlayPauseButton();
                }
            }
        });
        ((Button) findViewById(R.id.BackBtn)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PlayerActivity.this.finish();
            }
        });
        Bundle b = getIntent().getExtras();
        ((TextView) findViewById(R.id.SongTitle)).setText(b.getString("song"));
        ((TextView) findViewById(R.id.Artist)).setText(b.getString("artist"));
        ((TextView) findViewById(R.id.Album)).setText(b.getString("album"));
        this.mTask = new AlbumArtTask(this, this);
        this.mTask.execute(b.getString("artist"), b.getString("album"));
        this.mLyricTask = new LyricTask(this);
        this.mLyricTask.execute(b.getString(P_LYRICURL));
    }

    public void onStart() {
        super.onStart();
        this.mbPaused = false;
        if (!PlaybackServiceHelper.bindToService(this, this.mOsc)) {
            this.mHandler.sendEmptyMessage(2);
        }
        IntentFilter f = new IntentFilter();
        f.addAction(PlaybackService.PLAYSTATE_CHANGED);
        f.addAction(PlaybackService.META_CHANGED);
        f.addAction(PlaybackService.PLAYBACK_COMPLETE);
        registerReceiver(this.mStatusListener, new IntentFilter(f));
        updateTrackInfo();
        queueNextRefresh(refreshNow());
    }

    public void onStop() {
        this.mbPaused = true;
        this.mHandler.removeMessages(1);
        unregisterReceiver(this.mStatusListener);
        PlaybackServiceHelper.unbindFromService(this);
        super.onStop();
    }

    public void onResume() {
        super.onResume();
        updateTrackInfo();
    }

    public void onDestroy() {
        if (this.mTask != null) {
            this.mTask.cancel(true);
        }
        super.onDestroy();
    }

    public void AAT_OnBegin() {
    }

    public void AAT_OnFinished(boolean bError) {
    }

    public void AAT_OnReady(Bitmap bmp) {
        ((ImageView) findViewById(R.id.AlbumArt)).setImageBitmap(bmp);
    }

    /* access modifiers changed from: private */
    public long refreshNow() {
        int i;
        if (PlaybackServiceHelper.mService == null) {
            return 500;
        }
        long pos = this.mPosOverride < 0 ? PlaybackServiceHelper.mService.position() : this.mPosOverride;
        long remaining = 1000 - (pos % 1000);
        if (pos < 0 || this.mDuration <= 0) {
            this.mCurrentTime.setText("--:--");
            this.mProgress.setProgress(Constants.MAX_DOWNLOADS);
        } else {
            this.mCurrentTime.setText(PlaybackServiceHelper.makeTimeString(this, pos / 1000));
            if (PlaybackServiceHelper.mService.isPlaying()) {
                this.mCurrentTime.setVisibility(0);
            } else {
                int vis = this.mCurrentTime.getVisibility();
                TextView textView = this.mCurrentTime;
                if (vis == 4) {
                    i = 0;
                } else {
                    i = 4;
                }
                textView.setVisibility(i);
                remaining = 500;
            }
            this.mProgress.setProgress((int) ((1000 * pos) / this.mDuration));
        }
        return remaining;
    }

    /* access modifiers changed from: private */
    public void queueNextRefresh(long delay) {
        if (!this.mbPaused) {
            Message msg = this.mHandler.obtainMessage(1);
            this.mHandler.removeMessages(1);
            this.mHandler.sendMessageDelayed(msg, delay);
        }
    }

    /* access modifiers changed from: private */
    public void updateTrackInfo() {
        if (PlaybackServiceHelper.mService != null) {
            this.mDuration = PlaybackServiceHelper.mService.duration();
            this.mTotalTime.setText(PlaybackServiceHelper.makeTimeString(this, this.mDuration / 1000));
        }
    }

    /* access modifiers changed from: private */
    public void showPlayPauseButton() {
        if (PlaybackServiceHelper.mService == null || !PlaybackServiceHelper.mService.isPlaying()) {
            this.mPlayBtn.setVisibility(0);
            this.mPauseBtn.setVisibility(8);
            return;
        }
        this.mPlayBtn.setVisibility(8);
        this.mPauseBtn.setVisibility(0);
    }

    public void LT_OnBegin() {
        this.mLyricView.setVisibility(4);
        this.mLyricBtn.setVisibility(4);
    }

    public void LT_OnEnd(boolean bError) {
        if (bError) {
            this.mLyricView.setVisibility(4);
            this.mLyricBtn.setVisibility(4);
        }
    }

    public void LT_OnReady(String lyric) {
        this.mLyricView.setVisibility(0);
        this.mLyricBtn.setVisibility(0);
        ((TextView) findViewById(R.id.Lyric)).setText(lyric);
    }
}
