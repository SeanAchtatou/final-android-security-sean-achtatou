package net.mony.more.qaqad.utils;

import android.text.TextUtils;
import com.qwapi.adclient.android.utils.Utils;
import org.json.JSONException;
import org.json.JSONObject;

public class UpdateInfo {
    private String mDescript = Utils.EMPTY_STRING;
    private String mName = Utils.EMPTY_STRING;
    private String mUrl = Utils.EMPTY_STRING;

    public UpdateInfo(String json) throws JSONException {
        if (TextUtils.isEmpty(json)) {
            throw new IllegalArgumentException("Update Json string can't be empty");
        }
        JSONObject update = new JSONObject(json);
        this.mUrl = update.getString("uri");
        this.mName = update.getString("name");
        this.mDescript = update.getString("descript");
    }

    public String getUrl() {
        return this.mUrl;
    }

    public String getDescription() {
        return this.mDescript;
    }

    public String getName() {
        return this.mName;
    }
}
