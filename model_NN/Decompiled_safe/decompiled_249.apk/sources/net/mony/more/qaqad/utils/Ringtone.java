package net.mony.more.qaqad.utils;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import java.util.Date;

public class Ringtone {
    public String mArtist;
    public String mAuthor;
    public Bitmap mBmp = null;
    public String mCategory;
    public Date mDate;
    public int mDownloads;
    public String mKey;
    public String mLink;
    public int mRating;
    public int mSize;
    public String mThumbnail;
    public String mTitle;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    public static Uri insertRingtone(ContentResolver cv, String path, String title) {
        ContentValues values = new ContentValues();
        values.put("is_ringtone", (Boolean) true);
        Uri uri = updateRingtoneMedia(cv, path, values);
        if (uri != null) {
            return uri;
        }
        return insertRingtoneMedia(cv, path, title, true, false, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    public static Uri insertAlarm(ContentResolver cv, String path, String title) {
        ContentValues values = new ContentValues();
        values.put("is_alarm", (Boolean) true);
        Uri uri = updateRingtoneMedia(cv, path, values);
        if (uri != null) {
            return uri;
        }
        return insertRingtoneMedia(cv, path, title, false, false, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    public static Uri insertNotification(ContentResolver cv, String path, String title) {
        ContentValues values = new ContentValues();
        values.put("is_notification", (Boolean) true);
        Uri uri = updateRingtoneMedia(cv, path, values);
        if (uri != null) {
            return uri;
        }
        return insertRingtoneMedia(cv, path, title, false, true, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    private static Uri insertRingtoneMedia(ContentResolver cv, String path, String title, boolean bRingtone, boolean bNotification, boolean bAlarm) {
        ContentValues values = new ContentValues();
        values.put("title", title);
        values.put("_data", path);
        values.put("is_ringtone", Boolean.valueOf(bRingtone));
        values.put("is_notification", Boolean.valueOf(bNotification));
        values.put("is_alarm", Boolean.valueOf(bAlarm));
        values.put("is_music", (Boolean) true);
        values.put("mime_type", "audio/mpeg");
        return cv.insert(MediaStore.Audio.Media.getContentUriForPath(path), values);
    }

    private static Uri updateRingtoneMedia(ContentResolver cv, String path, ContentValues values) {
        Cursor c = null;
        try {
            Uri uri = MediaStore.Audio.Media.getContentUriForPath(path);
            c = cv.query(MediaStore.Audio.Media.getContentUriForPath(path), new String[]{"_id"}, "_data='" + path + "'", null, null);
            if (c.getCount() != 1) {
                if (c != null) {
                    c.close();
                }
                return null;
            }
            c.moveToFirst();
            int rowID = c.getInt(c.getColumnIndex("_id"));
            if (1 == cv.update(uri, values, "_id=" + rowID, null)) {
                Uri parse = Uri.parse(String.valueOf(uri.toString()) + "/" + rowID);
                if (c == null) {
                    return parse;
                }
                c.close();
                return parse;
            }
            if (c != null) {
                c.close();
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            if (c != null) {
                c.close();
            }
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            throw th;
        }
    }
}
