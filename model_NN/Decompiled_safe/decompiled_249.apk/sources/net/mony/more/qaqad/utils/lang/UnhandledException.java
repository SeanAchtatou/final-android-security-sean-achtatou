package net.mony.more.qaqad.utils.lang;

public class UnhandledException extends RuntimeException {
    private static final long serialVersionUID = 1832101364842773720L;

    public UnhandledException(Throwable cause) {
    }

    public UnhandledException(String message, Throwable cause) {
    }
}
