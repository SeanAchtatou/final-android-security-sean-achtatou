package net.mony.more.qaqad.service;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Locale;
import net.mony.more.qaqad.R;
import net.mony.more.qaqad.service.PlaybackService;

public class PlaybackServiceHelper {
    public static PlaybackService mService = null;
    private static HashMap<Context, ServiceBinder> sConnectionMap = new HashMap<>();
    private static StringBuilder sFormatBuilder = new StringBuilder();
    private static Formatter sFormatter = new Formatter(sFormatBuilder, Locale.getDefault());
    private static final Object[] sTimeArgs = new Object[5];

    public static boolean bindToService(Context context) {
        return bindToService(context, null);
    }

    public static boolean bindToService(Context context, ServiceConnection callback) {
        Class<PlaybackService> cls = PlaybackService.class;
        Class<PlaybackService> cls2 = PlaybackService.class;
        context.startService(new Intent(context, cls));
        ServiceBinder sb = new ServiceBinder(callback);
        sConnectionMap.put(context, sb);
        Class<PlaybackService> cls3 = PlaybackService.class;
        return context.bindService(new Intent().setClass(context, cls), sb, 0);
    }

    public static void unbindFromService(Context context) {
        ServiceBinder sb = sConnectionMap.remove(context);
        if (sb == null) {
            Log.e("PlaybackServiceHelper", "Trying to unbind for unknown Context");
            return;
        }
        context.unbindService(sb);
        if (sConnectionMap.isEmpty()) {
            mService = null;
        }
    }

    private static class ServiceBinder implements ServiceConnection {
        ServiceConnection mCallback;

        ServiceBinder(ServiceConnection callback) {
            this.mCallback = callback;
        }

        public void onServiceConnected(ComponentName className, IBinder service) {
            PlaybackServiceHelper.mService = ((PlaybackService.LocalBinder) service).getService();
            if (this.mCallback != null) {
                this.mCallback.onServiceConnected(className, service);
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            if (this.mCallback != null) {
                this.mCallback.onServiceDisconnected(className);
            }
            PlaybackServiceHelper.mService = null;
        }
    }

    public static String makeTimeString(Context context, long secs) {
        String durationformat = context.getString(secs < 3600 ? R.string.durationformatshort : R.string.durationformatlong);
        sFormatBuilder.setLength(0);
        Object[] timeArgs = sTimeArgs;
        timeArgs[0] = Long.valueOf(secs / 3600);
        timeArgs[1] = Long.valueOf(secs / 60);
        timeArgs[2] = Long.valueOf((secs / 60) % 60);
        timeArgs[3] = Long.valueOf(secs);
        timeArgs[4] = Long.valueOf(secs % 60);
        return sFormatter.format(durationformat, timeArgs).toString();
    }
}
