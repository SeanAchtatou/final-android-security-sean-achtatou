package net.mony.more.qaqad.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.PowerManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.RemoteViews;
import com.qwapi.adclient.android.utils.Utils;
import java.io.IOException;
import net.mony.more.qaqad.R;
import net.mony.more.qaqad.StreamStarterActivity;

public class PlaybackService extends Service {
    public static final String ASYNC_OPEN_COMPLETE = "net.mony.more.qaqad.asyncopencomplete";
    private static final int FADEIN = 4;
    private static final int IDLE_DELAY = 60000;
    private static final String LOGTAG = "PlaybackService";
    public static final String META_CHANGED = "net.mony.more.qaqadmetachanged";
    public static final int PLAYBACKSERVICE_STATUS = 100;
    public static final String PLAYBACK_COMPLETE = "net.mony.more.qaqadplaybackcomplete";
    public static final String PLAYSTATE_CHANGED = "net.mony.more.qaqadplaystatechanged";
    private static final int RELEASE_WAKELOCK = 2;
    public static final int REPEAT_ALL = 2;
    public static final int REPEAT_CURRENT = 1;
    public static final int REPEAT_NONE = 0;
    private static final int SERVER_DIED = 3;
    private static final int TRACK_ENDED = 1;
    private String mAlbum = Utils.EMPTY_STRING;
    private String mArtist = Utils.EMPTY_STRING;
    private final IBinder mBinder = new LocalBinder();
    private Handler mDelayedStopHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (!PlaybackService.this.isPlaying() && !PlaybackService.this.mResumeAfterCall && !PlaybackService.this.mServiceInUse && !PlaybackService.this.mMediaplayerHandler.hasMessages(1)) {
                PlaybackService.this.stopSelf(PlaybackService.this.mServiceStartId);
            }
        }
    };
    /* access modifiers changed from: private */
    public boolean mIsSupposedToBePlaying = false;
    private String mLyricURL = Utils.EMPTY_STRING;
    /* access modifiers changed from: private */
    public Handler mMediaplayerHandler = new Handler() {
        float mCurrentVolume = 1.0f;

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    if (PlaybackService.this.mRepeatMode == 1) {
                        PlaybackService.this.seek(0);
                        PlaybackService.this.play();
                        return;
                    } else if (PlaybackService.this.mOneShot) {
                        PlaybackService.this.notifyChange(PlaybackService.PLAYBACK_COMPLETE);
                        PlaybackService.this.mIsSupposedToBePlaying = false;
                        return;
                    } else {
                        return;
                    }
                case 2:
                    PlaybackService.this.mWakeLock.release();
                    return;
                case 3:
                default:
                    return;
                case 4:
                    if (!PlaybackService.this.isPlaying()) {
                        this.mCurrentVolume = 0.0f;
                        PlaybackService.this.mPlayer.setVolume(this.mCurrentVolume);
                        PlaybackService.this.play();
                        PlaybackService.this.mMediaplayerHandler.sendEmptyMessageDelayed(4, 10);
                        return;
                    }
                    this.mCurrentVolume += 0.01f;
                    if (this.mCurrentVolume < 1.0f) {
                        PlaybackService.this.mMediaplayerHandler.sendEmptyMessageDelayed(4, 10);
                    } else {
                        this.mCurrentVolume = 1.0f;
                    }
                    PlaybackService.this.mPlayer.setVolume(this.mCurrentVolume);
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public boolean mOneShot = true;
    private PhoneStateListener mPhoneStateListener = new PhoneStateListener() {
        public void onCallStateChanged(int state, String incomingNumber) {
            boolean z;
            boolean z2;
            if (state == 1) {
                if (((AudioManager) PlaybackService.this.getSystemService("audio")).getStreamVolume(2) > 0) {
                    PlaybackService playbackService = PlaybackService.this;
                    if (PlaybackService.this.isPlaying() || PlaybackService.this.mResumeAfterCall) {
                        z2 = true;
                    } else {
                        z2 = false;
                    }
                    playbackService.mResumeAfterCall = z2;
                    PlaybackService.this.pause();
                }
            } else if (state == 2) {
                PlaybackService playbackService2 = PlaybackService.this;
                if (PlaybackService.this.isPlaying() || PlaybackService.this.mResumeAfterCall) {
                    z = true;
                } else {
                    z = false;
                }
                playbackService2.mResumeAfterCall = z;
                PlaybackService.this.pause();
            } else if (state == 0 && PlaybackService.this.mResumeAfterCall) {
                PlaybackService.this.startAndFadeIn();
                PlaybackService.this.mResumeAfterCall = false;
            }
        }
    };
    /* access modifiers changed from: private */
    public MultiPlayer mPlayer;
    /* access modifiers changed from: private */
    public int mRepeatMode = 0;
    /* access modifiers changed from: private */
    public boolean mResumeAfterCall = false;
    /* access modifiers changed from: private */
    public boolean mServiceInUse = false;
    /* access modifiers changed from: private */
    public int mServiceStartId = -1;
    private String mSongURL = Utils.EMPTY_STRING;
    private String mTrackName = Utils.EMPTY_STRING;
    private BroadcastReceiver mUnmountReceiver = null;
    /* access modifiers changed from: private */
    public PowerManager.WakeLock mWakeLock;

    public class LocalBinder extends Binder {
        public LocalBinder() {
        }

        public PlaybackService getService() {
            return PlaybackService.this;
        }
    }

    public IBinder onBind(Intent intent) {
        this.mDelayedStopHandler.removeCallbacksAndMessages(null);
        this.mServiceInUse = true;
        return this.mBinder;
    }

    public void onRebind(Intent intent) {
        this.mDelayedStopHandler.removeCallbacksAndMessages(null);
        this.mServiceInUse = true;
    }

    public boolean onUnbind(Intent intent) {
        this.mServiceInUse = false;
        if (isPlaying() || this.mResumeAfterCall) {
            return true;
        }
        stopSelf(this.mServiceStartId);
        return true;
    }

    public void onCreate() {
        super.onCreate();
        registerExternalStorageListener();
        this.mPlayer = new MultiPlayer();
        this.mPlayer.setHandler(this.mMediaplayerHandler);
        ((NotificationManager) getSystemService("notification")).cancel(100);
        ((TelephonyManager) getSystemService("phone")).listen(this.mPhoneStateListener, 32);
        this.mWakeLock = ((PowerManager) getSystemService("power")).newWakeLock(1, getClass().getName());
        this.mWakeLock.setReferenceCounted(false);
        this.mDelayedStopHandler.sendMessageDelayed(this.mDelayedStopHandler.obtainMessage(), 60000);
    }

    public void onDestroy() {
        ((NotificationManager) getSystemService("notification")).cancel(100);
        if (isPlaying()) {
            Log.e(LOGTAG, "Service being destroyed while still playing.");
        }
        this.mPlayer.release();
        this.mPlayer = null;
        this.mDelayedStopHandler.removeCallbacksAndMessages(null);
        this.mMediaplayerHandler.removeCallbacksAndMessages(null);
        ((TelephonyManager) getSystemService("phone")).listen(this.mPhoneStateListener, 0);
        if (this.mUnmountReceiver != null) {
            unregisterReceiver(this.mUnmountReceiver);
            this.mUnmountReceiver = null;
        }
        this.mWakeLock.release();
        super.onDestroy();
    }

    public void onStart(Intent intent, int startId) {
        this.mServiceStartId = startId;
        this.mDelayedStopHandler.removeCallbacksAndMessages(null);
        this.mDelayedStopHandler.sendMessageDelayed(this.mDelayedStopHandler.obtainMessage(), 60000);
    }

    public void registerExternalStorageListener() {
        if (this.mUnmountReceiver == null) {
            this.mUnmountReceiver = new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();
                    if (action.equals("android.intent.action.MEDIA_EJECT")) {
                        PlaybackService.this.mOneShot = true;
                        PlaybackService.this.closeExternalStorageFiles(intent.getData().getPath());
                    } else if (action.equals("android.intent.action.MEDIA_MOUNTED")) {
                        PlaybackService.this.notifyChange(PlaybackService.META_CHANGED);
                    }
                }
            };
            IntentFilter iFilter = new IntentFilter();
            iFilter.addAction("android.intent.action.MEDIA_EJECT");
            iFilter.addAction("android.intent.action.MEDIA_MOUNTED");
            iFilter.addDataScheme("file");
            registerReceiver(this.mUnmountReceiver, iFilter);
        }
    }

    public void closeExternalStorageFiles(String storagePath) {
        stop(true);
        notifyChange(META_CHANGED);
    }

    /* access modifiers changed from: private */
    public void startAndFadeIn() {
        this.mMediaplayerHandler.sendEmptyMessageDelayed(4, 10);
    }

    public void openAsync(String songURL, String trackName, String artist, String album, String lyricURL) {
        synchronized (this) {
            if (songURL != null) {
                this.mRepeatMode = 0;
                this.mSongURL = songURL;
                this.mTrackName = trackName;
                this.mArtist = artist;
                this.mAlbum = album;
                this.mLyricURL = lyricURL;
                this.mPlayer.setDataSourceAsync(songURL);
                this.mOneShot = true;
            }
        }
    }

    public void play() {
        if (this.mPlayer.isInitialized()) {
            this.mPlayer.start();
            setForeground(true);
            NotificationManager nm = (NotificationManager) getSystemService("notification");
            RemoteViews views = new RemoteViews(getPackageName(), (int) R.layout.statusbar);
            views.setImageViewResource(R.id.icon, R.drawable.stat_notify_musicplayer);
            String artist = getArtistName();
            views.setTextViewText(R.id.trackname, getTrackName());
            if (artist.length() <= 0) {
                artist = getString(R.string.UNKNOWN_ARTIST);
            }
            String album = getAlbumName();
            if (album.length() <= 0) {
                album = getString(R.string.UNKNOWN_ALBUM);
            }
            views.setTextViewText(R.id.artistalbum, getString(R.string.notification_artist_album, new Object[]{artist, album}));
            Notification status = new Notification();
            status.contentView = views;
            status.flags |= 2;
            status.icon = R.drawable.stat_notify_musicplayer;
            Intent intent = StreamStarterActivity.makeIntent(this, getSongURL(), getTrackName(), artist, album, getLyricURL(), true);
            intent.addFlags(268435456);
            status.contentIntent = PendingIntent.getActivity(this, 0, intent, 0);
            nm.notify(100, status);
            setForeground(true);
            if (!this.mIsSupposedToBePlaying) {
                this.mIsSupposedToBePlaying = true;
                notifyChange(PLAYSTATE_CHANGED);
            }
        }
    }

    public void pause() {
        synchronized (this) {
            if (isPlaying()) {
                this.mPlayer.pause();
                gotoIdleState();
                setForeground(false);
                this.mIsSupposedToBePlaying = false;
                notifyChange(PLAYSTATE_CHANGED);
            }
        }
    }

    public void stop(boolean remove_status_icon) {
        if (this.mPlayer.isInitialized()) {
            this.mPlayer.stop();
        }
        if (remove_status_icon) {
            gotoIdleState();
        }
        setForeground(false);
        if (remove_status_icon) {
            this.mIsSupposedToBePlaying = false;
        }
    }

    public void stop() {
        stop(true);
    }

    public boolean isPlaying() {
        return this.mIsSupposedToBePlaying;
    }

    public long duration() {
        if (this.mPlayer.isInitialized()) {
            return this.mPlayer.duration();
        }
        return -1;
    }

    public long position() {
        if (this.mPlayer.isInitialized()) {
            return this.mPlayer.position();
        }
        return -1;
    }

    public long seek(long pos) {
        if (!this.mPlayer.isInitialized()) {
            return -1;
        }
        if (pos < 0) {
            pos = 0;
        }
        if (pos > this.mPlayer.duration()) {
            pos = this.mPlayer.duration();
        }
        return this.mPlayer.seek(pos);
    }

    public void setRepeatMode(int repeatmode) {
        synchronized (this) {
            this.mRepeatMode = repeatmode;
        }
    }

    public int getRepeatMode() {
        return this.mRepeatMode;
    }

    private class MultiPlayer {
        MediaPlayer.OnErrorListener errorListener = new MediaPlayer.OnErrorListener() {
            public boolean onError(MediaPlayer mp, int what, int extra) {
                switch (what) {
                    case PlaybackService.PLAYBACKSERVICE_STATUS /*100*/:
                        MultiPlayer.this.mIsInitialized = false;
                        MultiPlayer.this.mMediaPlayer.release();
                        MultiPlayer.this.mMediaPlayer = new MediaPlayer();
                        MultiPlayer.this.mMediaPlayer.setWakeMode(PlaybackService.this, 1);
                        MultiPlayer.this.mHandler.sendMessageDelayed(MultiPlayer.this.mHandler.obtainMessage(3), 2000);
                        return true;
                    default:
                        Log.d("MultiPlayer", "Error: " + what + "," + extra);
                        return false;
                }
            }
        };
        MediaPlayer.OnCompletionListener listener = new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                PlaybackService.this.mWakeLock.acquire(30000);
                MultiPlayer.this.mHandler.sendEmptyMessage(1);
                MultiPlayer.this.mHandler.sendEmptyMessage(2);
            }
        };
        /* access modifiers changed from: private */
        public Handler mHandler;
        /* access modifiers changed from: private */
        public boolean mIsInitialized = false;
        /* access modifiers changed from: private */
        public MediaPlayer mMediaPlayer = new MediaPlayer();
        MediaPlayer.OnPreparedListener preparedlistener = new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                PlaybackService.this.notifyChange(PlaybackService.ASYNC_OPEN_COMPLETE);
            }
        };

        public MultiPlayer() {
            this.mMediaPlayer.setWakeMode(PlaybackService.this, 1);
        }

        public void setDataSourceAsync(String path) {
            try {
                this.mMediaPlayer.reset();
                this.mMediaPlayer.setDataSource(path);
                this.mMediaPlayer.setAudioStreamType(3);
                this.mMediaPlayer.setOnPreparedListener(this.preparedlistener);
                this.mMediaPlayer.prepareAsync();
                this.mMediaPlayer.setOnCompletionListener(this.listener);
                this.mMediaPlayer.setOnErrorListener(this.errorListener);
                this.mIsInitialized = true;
            } catch (IOException e) {
                this.mIsInitialized = false;
            } catch (IllegalArgumentException e2) {
                this.mIsInitialized = false;
            }
        }

        public void setDataSource(String path) {
            try {
                this.mMediaPlayer.reset();
                this.mMediaPlayer.setOnPreparedListener(null);
                if (path.startsWith("content://")) {
                    this.mMediaPlayer.setDataSource(PlaybackService.this, Uri.parse(path));
                } else {
                    this.mMediaPlayer.setDataSource(path);
                }
                this.mMediaPlayer.setAudioStreamType(3);
                this.mMediaPlayer.prepare();
                this.mMediaPlayer.setOnCompletionListener(this.listener);
                this.mMediaPlayer.setOnErrorListener(this.errorListener);
                this.mIsInitialized = true;
            } catch (IOException e) {
                this.mIsInitialized = false;
            } catch (IllegalArgumentException e2) {
                this.mIsInitialized = false;
            }
        }

        public boolean isInitialized() {
            return this.mIsInitialized;
        }

        public void start() {
            this.mMediaPlayer.start();
        }

        public void stop() {
            this.mMediaPlayer.reset();
            this.mIsInitialized = false;
        }

        public void release() {
            stop();
            this.mMediaPlayer.release();
        }

        public void pause() {
            this.mMediaPlayer.pause();
        }

        public void setHandler(Handler handler) {
            this.mHandler = handler;
        }

        public long duration() {
            return (long) this.mMediaPlayer.getDuration();
        }

        public long position() {
            return (long) this.mMediaPlayer.getCurrentPosition();
        }

        public long seek(long whereto) {
            this.mMediaPlayer.seekTo((int) whereto);
            return whereto;
        }

        public void setVolume(float vol) {
            this.mMediaPlayer.setVolume(vol, vol);
        }
    }

    /* access modifiers changed from: private */
    public void notifyChange(String what) {
        sendBroadcast(new Intent(what));
    }

    private void gotoIdleState() {
        ((NotificationManager) getSystemService("notification")).cancel(100);
        this.mDelayedStopHandler.removeCallbacksAndMessages(null);
        this.mDelayedStopHandler.sendMessageDelayed(this.mDelayedStopHandler.obtainMessage(), 60000);
        setForeground(false);
    }

    public String getArtistName() {
        return this.mArtist;
    }

    public String getTrackName() {
        return this.mTrackName;
    }

    public String getAlbumName() {
        return this.mAlbum;
    }

    public String getSongURL() {
        return this.mSongURL;
    }

    public String getLyricURL() {
        return this.mLyricURL;
    }
}
