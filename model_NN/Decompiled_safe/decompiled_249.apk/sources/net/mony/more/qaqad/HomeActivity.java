package net.mony.more.qaqad;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Browser;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import net.mony.more.qaqad.utils.Util;

public class HomeActivity extends Activity {
    public static final int DLG_RATEME = 51000;
    public static final int DLG_SHARE = 52000;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.home);
        AdsView.createAdWhirl(this);
        ((Button) findViewById(R.id.SearchBtn)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SearchActivity.startActivity(HomeActivity.this);
            }
        });
        ((Button) findViewById(R.id.ChartsBtn)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HomeActivity.this.startActivity(new Intent(HomeActivity.this, MusicCharts.class));
            }
        });
        ((Button) findViewById(R.id.DownloadBtn)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HomeActivity.this.startActivity(new Intent(HomeActivity.this, DownloadActivity.class));
            }
        });
        ((Button) findViewById(R.id.LibraryBtn)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HomeActivity.this.startActivity(new Intent(HomeActivity.this, MyDownloadsActivity.class));
            }
        });
        ((Button) findViewById(R.id.RatingBtn)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HomeActivity.this.showDialog(HomeActivity.DLG_RATEME);
            }
        });
        ((Button) findViewById(R.id.ShareBtn)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HomeActivity.this.showDialog(HomeActivity.DLG_SHARE);
            }
        });
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case Util.DOWNLOAD_APP_DIG /*10000*/:
                return Util.createDownloadDialog(this);
            case DLG_RATEME /*51000*/:
                return new AlertDialog.Builder(this).setTitle("Rate me in market").setMessage("If you like MP3 Music Download, please rate it in Android Market. Thanks a lot.").setPositiveButton("Rate me", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        try {
                            HomeActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pname:" + HomeActivity.class.getPackage().getName())));
                        } catch (Exception e) {
                        }
                    }
                }).setNegativeButton("Later", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                }).setCancelable(true).create();
            case DLG_SHARE /*52000*/:
                return new AlertDialog.Builder(this).setTitle("Share to friends").setMessage("If you like MP3 Music Download, you can share app to your friends. Thanks a lot.").setPositiveButton("Share to", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        try {
                            Browser.sendString(HomeActivity.this, "The app of " + HomeActivity.this.getString(R.string.app_name) + " " + "provides millions of songs for you to enjoy online listening with brilliant sound quality or download for free." + "Therefore I highly recommend it to you, sincerely hope you will love it. " + "http://market.android.com/details?id=" + HomeActivity.this.getApplication().getPackageName() + " " + "or download from website : http://www.mp3musicdownload.me/ ");
                        } catch (Exception e) {
                        }
                    }
                }).setNegativeButton("Later", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                }).setCancelable(true).create();
            default:
                return null;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.about /*2131427388*/:
                startActivity(new Intent(this, About.class));
                return true;
            default:
                return false;
        }
    }
}
