package net.mony.more.qaqad.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class SiteHelper {
    public static final String getSearchLink(String keywords) {
        try {
            return String.valueOf("http://mp3.sogou.com/music.so?pf=mp3&ac=1&query=") + URLEncoder.encode(keywords, "GBK");
        } catch (UnsupportedEncodingException e) {
            return String.valueOf("http://mp3.sogou.com/music.so?pf=mp3&ac=1&query=") + keywords;
        }
    }

    public static final String getHot100() {
        return "http://twuzic.appspot.com/fetch/lftrack50";
    }

    public static final String getTop100() {
        return "http://twuzic.appspot.com/fetch/bb100";
    }
}
