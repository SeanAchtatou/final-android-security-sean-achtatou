package net.mony.more.qaqad;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.AsyncQueryHandler;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import com.qwapi.adclient.android.utils.Utils;
import java.io.File;
import net.mony.more.qaqad.data.DownloadQueue;
import net.mony.more.qaqad.service.DownloadService;
import net.mony.more.qaqad.utils.Constants;
import net.mony.more.qaqad.utils.HeaderViewHelper;

public class DownloadActivity extends ListActivity {
    private static final int DIALOG_PAUSE = 1;
    private static final int DIALOG_RESTART = 3;
    private static final int DIALOG_RESUME = 2;
    /* access modifiers changed from: private */
    public DownloadAdapter mAdapter = null;
    private boolean mAdapterSent = false;
    /* access modifiers changed from: private */
    public Cursor mCursor = null;
    private HeaderViewHelper mHeaderView = null;
    /* access modifiers changed from: private */
    public Bitmap mIconDownloading = null;
    /* access modifiers changed from: private */
    public Bitmap mIconError = null;
    /* access modifiers changed from: private */
    public Bitmap mIconQueue = null;
    /* access modifiers changed from: private */
    public Handler mReScanHandler = new Handler() {
        public void handleMessage(Message msg) {
            Cursor unused = DownloadActivity.this.getDownloadCursor(DownloadActivity.this.mAdapter.getQueryHandler());
        }
    };
    private BroadcastReceiver mScanListener = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            DownloadActivity.this.mReScanHandler.sendEmptyMessage(0);
        }
    };
    /* access modifiers changed from: private */
    public long mSelectedID = -1;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.download);
        AdsView.createAdWhirl(this);
        this.mIconError = BitmapFactory.decodeResource(getResources(), 17301608);
        this.mIconDownloading = BitmapFactory.decodeResource(getResources(), 17301611);
        this.mIconQueue = BitmapFactory.decodeResource(getResources(), 17301607);
        IntentFilter f = new IntentFilter();
        f.addAction("android.intent.action.MEDIA_UNMOUNTED");
        f.addDataScheme("file");
        registerReceiver(this.mScanListener, f);
        this.mHeaderView = new HeaderViewHelper(this);
        this.mHeaderView.setTitle("Download Manager");
        this.mHeaderView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DownloadActivity.this.finish();
            }
        });
        this.mAdapter = (DownloadAdapter) getLastNonConfigurationInstance();
        if (this.mAdapter == null) {
            startService(new Intent(this, DownloadService.class));
            this.mAdapter = new DownloadAdapter(getApplication(), this, R.layout.download_item, this.mCursor, new String[0], new int[0]);
            setListAdapter(this.mAdapter);
            getDownloadCursor(this.mAdapter.getQueryHandler());
            return;
        }
        this.mAdapter.setActivity(this);
        setListAdapter(this.mAdapter);
        this.mCursor = this.mAdapter.getCursor();
        if (this.mCursor != null) {
            init(this.mCursor);
        } else {
            getDownloadCursor(this.mAdapter.getQueryHandler());
        }
    }

    public Object onRetainNonConfigurationInstance() {
        this.mAdapterSent = true;
        return this.mAdapter;
    }

    public void onDestroy() {
        Cursor c;
        unregisterReceiver(this.mScanListener);
        if (!this.mAdapterSent && (c = this.mAdapter.getCursor()) != null) {
            c.close();
        }
        setListAdapter(null);
        this.mAdapter = null;
        super.onDestroy();
    }

    public void onPause() {
        this.mReScanHandler.removeCallbacksAndMessages(null);
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void init(Cursor c) {
        if (this.mAdapter != null) {
            this.mAdapter.changeCursor(c);
            if (this.mCursor == null) {
                this.mReScanHandler.sendEmptyMessageDelayed(0, 1000);
            }
        }
    }

    /* access modifiers changed from: private */
    public Cursor getDownloadCursor(AsyncQueryHandler async) {
        String[] cols = {"_id", "title", "album", "artist", DownloadQueue.COL_STATUS, DownloadQueue.COL_CURRENT_BYTES, DownloadQueue.COL_TOTAL_BYTES, DownloadQueue.COL_CONTROL};
        if (async != null) {
            async.startQuery(0, null, DownloadQueue.CONTENT_URI, cols, "(status<200) OR (status>=300)", null, "status ASC,lastmod DESC");
            return null;
        }
        try {
            ContentResolver resolver = getContentResolver();
            if (resolver != null) {
                return resolver.query(DownloadQueue.CONTENT_URI, cols, "(status<200) OR (status>=300)", null, "status ASC,lastmod DESC");
            }
            return null;
        } catch (UnsupportedOperationException e) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int pos, long id) {
        Cursor c = (Cursor) this.mAdapter.getItem(pos);
        if (c != null) {
            this.mSelectedID = c.getLong(c.getColumnIndex("_id"));
            int status = c.getInt(this.mAdapter.mStatusIdx);
            if (DownloadQueue.isStatusRunning(status)) {
                showDialog(1);
            } else if (DownloadQueue.isStatusSuspended(status)) {
                if (1 == c.getInt(this.mAdapter.mControlIdx)) {
                    showDialog(2);
                } else {
                    showDialog(1);
                }
            } else if (DownloadQueue.isStatusError(status)) {
                showDialog(3);
            }
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return new AlertDialog.Builder(this).setTitle((int) R.string.ACTION).setItems((int) R.array.DOWNLOAD_PAUSE, new DialogInterface.OnClickListener() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
                     arg types: [java.lang.String, int]
                     candidates:
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                ContentValues values = new ContentValues();
                                values.put(DownloadQueue.COL_CONTROL, (Integer) 1);
                                DownloadActivity.this.getContentResolver().update(Uri.parse(DownloadQueue.CONTENT_URI + "/" + DownloadActivity.this.mSelectedID), values, null, null);
                                return;
                            default:
                                return;
                        }
                    }
                }).create();
            case 2:
                return new AlertDialog.Builder(this).setTitle((int) R.string.ACTION).setItems((int) R.array.DOWNLOAD_RESUME, new DialogInterface.OnClickListener() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
                     arg types: [java.lang.String, int]
                     candidates:
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                ContentValues values = new ContentValues();
                                values.put(DownloadQueue.COL_CONTROL, (Integer) 0);
                                DownloadActivity.this.getContentResolver().update(Uri.parse(DownloadQueue.CONTENT_URI + "/" + DownloadActivity.this.mSelectedID), values, null, null);
                                return;
                            case 1:
                                Cursor c = DownloadActivity.this.getContentResolver().query(Uri.parse(DownloadQueue.CONTENT_URI + "/" + DownloadActivity.this.mSelectedID), null, null, null, null);
                                if (c != null) {
                                    c.moveToFirst();
                                    if (!c.isAfterLast()) {
                                        String filePath = c.getString(c.getColumnIndex("_data"));
                                        if (filePath != null) {
                                            File songFile = new File(filePath);
                                            if (songFile.exists() && songFile.isFile()) {
                                                songFile.delete();
                                            }
                                        }
                                        DownloadActivity.this.getContentResolver().delete(Uri.parse(DownloadQueue.CONTENT_URI + "/" + DownloadActivity.this.mSelectedID), null, null);
                                        return;
                                    }
                                    return;
                                }
                                return;
                            default:
                                return;
                        }
                    }
                }).create();
            case 3:
                return new AlertDialog.Builder(this).setTitle((int) R.string.ACTION).setItems((int) R.array.DOWNLOAD_RESTART, new DialogInterface.OnClickListener() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
                     arg types: [java.lang.String, int]
                     candidates:
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                Cursor c = DownloadActivity.this.getContentResolver().query(Uri.parse(DownloadQueue.CONTENT_URI + "/" + DownloadActivity.this.mSelectedID), null, null, null, null);
                                if (c != null) {
                                    c.moveToFirst();
                                    if (!c.isAfterLast()) {
                                        try {
                                            String filePath = c.getString(c.getColumnIndex("_data"));
                                            if (filePath != null) {
                                                File songFile = new File(filePath);
                                                if (songFile.exists() && songFile.isFile()) {
                                                    songFile.delete();
                                                }
                                            }
                                            ContentValues values = new ContentValues();
                                            values.put("uri", c.getString(c.getColumnIndex("uri")));
                                            values.put(DownloadQueue.COL_FILE_NAME_HINT, c.getString(c.getColumnIndex(DownloadQueue.COL_FILE_NAME_HINT)));
                                            values.put("mimetype", c.getString(c.getColumnIndex("mimetype")));
                                            values.put(DownloadQueue.COL_CONTROL, (Integer) 0);
                                            values.put("title", c.getString(c.getColumnIndex("title")));
                                            values.put("artist", c.getString(c.getColumnIndex("artist")));
                                            values.put("album", c.getString(c.getColumnIndex("album")));
                                            DownloadActivity.this.getContentResolver().delete(Uri.parse(DownloadQueue.CONTENT_URI + "/" + DownloadActivity.this.mSelectedID), null, null);
                                            DownloadActivity.this.getContentResolver().insert(DownloadQueue.CONTENT_URI, values);
                                            return;
                                        } catch (Exception e) {
                                            Log.e(Constants.TAG, e.toString());
                                            return;
                                        } finally {
                                            c.close();
                                        }
                                    } else {
                                        return;
                                    }
                                } else {
                                    return;
                                }
                            case 1:
                                Cursor c2 = DownloadActivity.this.getContentResolver().query(Uri.parse(DownloadQueue.CONTENT_URI + "/" + DownloadActivity.this.mSelectedID), null, null, null, null);
                                if (c2 != null) {
                                    c2.moveToFirst();
                                    if (!c2.isAfterLast()) {
                                        String filePath2 = c2.getString(c2.getColumnIndex("_data"));
                                        if (filePath2 != null) {
                                            File songFile2 = new File(filePath2);
                                            if (songFile2.exists() && songFile2.isFile()) {
                                                songFile2.delete();
                                            }
                                        }
                                        DownloadActivity.this.getContentResolver().delete(Uri.parse(DownloadQueue.CONTENT_URI + "/" + DownloadActivity.this.mSelectedID), null, null);
                                        return;
                                    }
                                    return;
                                }
                                return;
                            default:
                                return;
                        }
                    }
                }).create();
            default:
                return null;
        }
    }

    private static class DownloadAdapter extends SimpleCursorAdapter {
        DownloadActivity mActivity;
        private int mAlbumIdx;
        private int mArtistIdx;
        public int mControlIdx;
        private int mCurrentBytesIdx;
        private AsyncQueryHandler mQueryHandler;
        public int mStatusIdx;
        private int mTitleIdx;
        private int mTotalBytesIdx;

        static class ViewHolder {
            View mDownloadingBlock;
            ImageView mIVIcon;
            ProgressBar mProgBar;
            ProgressBar mProgBar2;
            TextView mTVAlbum;
            TextView mTVArtist;
            TextView mTVErrMsg;
            TextView mTVPercent;
            TextView mTVTitle;

            ViewHolder() {
            }
        }

        class QueryHandler extends AsyncQueryHandler {
            public QueryHandler(ContentResolver cr) {
                super(cr);
            }

            /* access modifiers changed from: protected */
            public void onQueryComplete(int token, Object cookie, Cursor cursor) {
                DownloadAdapter.this.mActivity.init(cursor);
            }
        }

        DownloadAdapter(Context context, DownloadActivity currentActivity, int layout, Cursor cursor, String[] from, int[] to) {
            super(context, layout, cursor, from, to);
            this.mActivity = currentActivity;
            this.mQueryHandler = new QueryHandler(context.getContentResolver());
            getColumnIndices(cursor);
        }

        private void getColumnIndices(Cursor c) {
            if (c != null) {
                this.mTitleIdx = c.getColumnIndex("title");
                this.mStatusIdx = c.getColumnIndex(DownloadQueue.COL_STATUS);
                this.mCurrentBytesIdx = c.getColumnIndex(DownloadQueue.COL_CURRENT_BYTES);
                this.mTotalBytesIdx = c.getColumnIndex(DownloadQueue.COL_TOTAL_BYTES);
                this.mControlIdx = c.getColumnIndex(DownloadQueue.COL_CONTROL);
                this.mArtistIdx = c.getColumnIndex("artist");
                this.mAlbumIdx = c.getColumnIndex("album");
            }
        }

        public void setActivity(DownloadActivity newactivity) {
            this.mActivity = newactivity;
        }

        public AsyncQueryHandler getQueryHandler() {
            return this.mQueryHandler;
        }

        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            View v = super.newView(context, cursor, parent);
            ViewHolder vh = new ViewHolder();
            vh.mTVTitle = (TextView) v.findViewById(R.id.Title);
            vh.mIVIcon = (ImageView) v.findViewById(R.id.Icon);
            vh.mProgBar = (ProgressBar) v.findViewById(R.id.ProgressBar);
            vh.mProgBar2 = (ProgressBar) v.findViewById(R.id.ProgressBar2);
            vh.mDownloadingBlock = v.findViewById(R.id.DownloadingBlock);
            vh.mTVErrMsg = (TextView) v.findViewById(R.id.ErrorMsg);
            vh.mTVPercent = (TextView) v.findViewById(R.id.Percent);
            vh.mTVArtist = (TextView) v.findViewById(R.id.Artist);
            vh.mTVAlbum = (TextView) v.findViewById(R.id.Album);
            v.setTag(vh);
            return v;
        }

        public void bindView(View view, Context context, Cursor c) {
            ViewHolder vh = (ViewHolder) view.getTag();
            int status = c.getInt(this.mStatusIdx);
            if (DownloadQueue.isStatusError(status)) {
                vh.mIVIcon.setImageBitmap(this.mActivity.mIconError);
            } else if (DownloadQueue.isStatusSuspended(status)) {
                vh.mIVIcon.setImageBitmap(this.mActivity.mIconQueue);
            } else if (DownloadQueue.isStatusRunning(status)) {
                vh.mIVIcon.setImageBitmap(this.mActivity.mIconDownloading);
            }
            vh.mTVTitle.setText(c.getString(this.mTitleIdx));
            vh.mTVArtist.setText(c.getString(this.mArtistIdx));
            vh.mTVAlbum.setText(c.getString(this.mAlbumIdx));
            if (DownloadQueue.isStatusError(status)) {
                vh.mDownloadingBlock.setVisibility(8);
                vh.mTVErrMsg.setVisibility(0);
                vh.mTVErrMsg.setText(DownloadQueue.translateErrMsg(this.mActivity.getResources(), status));
                return;
            }
            vh.mDownloadingBlock.setVisibility(0);
            vh.mTVErrMsg.setVisibility(8);
            int currentBytes = c.getInt(this.mCurrentBytesIdx);
            int totalBytes = c.getInt(this.mTotalBytesIdx);
            vh.mTVPercent.setText(makeDownloadingText((long) totalBytes, (long) currentBytes));
            if (c.getInt(this.mControlIdx) == 0) {
                vh.mProgBar.setVisibility(0);
                vh.mProgBar2.setVisibility(8);
                if (totalBytes > 0) {
                    vh.mProgBar.setIndeterminate(false);
                    vh.mProgBar.setMax(totalBytes);
                    vh.mProgBar.setProgress(currentBytes);
                    return;
                }
                vh.mProgBar.setProgress(0);
                vh.mProgBar.setIndeterminate(true);
                return;
            }
            vh.mProgBar2.setVisibility(0);
            vh.mProgBar.setVisibility(8);
            if (totalBytes > 0) {
                vh.mProgBar2.setIndeterminate(false);
                vh.mProgBar2.setMax(totalBytes);
                vh.mProgBar2.setProgress(currentBytes);
                return;
            }
            vh.mProgBar2.setProgress(0);
            vh.mProgBar2.setIndeterminate(true);
        }

        public void changeCursor(Cursor cursor) {
            if (cursor != this.mActivity.mCursor) {
                this.mActivity.mCursor = cursor;
                getColumnIndices(cursor);
                super.changeCursor(cursor);
            }
        }

        public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
            return this.mActivity.getDownloadCursor(null);
        }

        private static String makeDownloadingText(long totalBytes, long currentBytes) {
            if (totalBytes <= 0) {
                return Utils.EMPTY_STRING;
            }
            StringBuilder sb = new StringBuilder();
            sb.append((100 * currentBytes) / totalBytes);
            sb.append('%');
            return sb.toString();
        }
    }
}
