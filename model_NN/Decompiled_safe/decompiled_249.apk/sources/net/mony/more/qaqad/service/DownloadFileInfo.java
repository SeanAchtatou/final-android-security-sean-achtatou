package net.mony.more.qaqad.service;

import java.io.FileOutputStream;

public class DownloadFileInfo {
    String mFileName;
    int mStatus;
    FileOutputStream mStream;

    public DownloadFileInfo(String fileName, FileOutputStream stream, int status) {
        this.mFileName = fileName;
        this.mStream = stream;
        this.mStatus = status;
    }
}
