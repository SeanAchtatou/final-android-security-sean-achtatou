package net.mony.more.qaqad.utils;

import android.os.Environment;

public class Constants {
    public static final String ACTION_HIDE = "muzilla.intent.action.DOWNLOAD_HIDE";
    public static final String ACTION_LIST = "muzilla.intent.action.DOWNLOAD_LIST";
    public static final String ACTION_MOVE = "muzilla.intent.action.DOWNLOAD_MOVE";
    public static final String ACTION_OPEN = "muzilla.intent.action.DOWNLOAD_OPEN";
    public static final String ACTION_RETRY = "muzilla.intent.action.DOWNLOAD_WAKEUP";
    private static final String APP_DIR = "/musicdownload";
    public static final int BUFFER_SIZE = 4096;
    private static final String DB_DIR = "/musicdownload/db/";
    public static final String DEFAULT_DL_FILENAME = "downloadfile";
    public static final String DEFAULT_DL_SUBDIR = "/musicdownload";
    public static final String KNOWN_SPURIOUS_FILENAME = "lost+found";
    public static boolean LOGD = true;
    public static boolean LOGV = true;
    public static final int MAX_DOWNLOADS = 1000;
    public static final int MAX_DOWNLOAD_JOB = 1;
    public static final int MAX_REDIRECTS = 5;
    public static final int MAX_RETRIES = 5;
    public static final int MAX_RETRY_AFTER = 86400;
    public static final int MIN_PROGRESS_STEP = 4096;
    public static final long MIN_PROGRESS_TIME = 1500;
    public static final int MIN_RETRY_AFTER = 30;
    public static final String PKG_NAME = "net.mony.more.qaqad";
    public static final int RETRY_FIRST_DELAY = 30;
    public static final String TAG = "MuzicWizard";

    public static String getAppDBDir() {
        return String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + DB_DIR;
    }
}
