package com.oceanlifepuzzle;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771971;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771972;
        public static final int secondaryTextColor = 2130771970;
    }

    public static final class drawable {
        public static final int background = 2130837504;
        public static final int background_bottom = 2130837505;
        public static final int background_top = 2130837506;
        public static final int icon = 2130837507;
        public static final int image_1 = 2130837508;
        public static final int image_10 = 2130837509;
        public static final int image_11 = 2130837510;
        public static final int image_12 = 2130837511;
        public static final int image_13 = 2130837512;
        public static final int image_14 = 2130837513;
        public static final int image_15 = 2130837514;
        public static final int image_16 = 2130837515;
        public static final int image_17 = 2130837516;
        public static final int image_18 = 2130837517;
        public static final int image_19 = 2130837518;
        public static final int image_2 = 2130837519;
        public static final int image_20 = 2130837520;
        public static final int image_3 = 2130837521;
        public static final int image_4 = 2130837522;
        public static final int image_5 = 2130837523;
        public static final int image_6 = 2130837524;
        public static final int image_7 = 2130837525;
        public static final int image_8 = 2130837526;
        public static final int image_9 = 2130837527;
        public static final int thumbnail_1 = 2130837528;
        public static final int thumbnail_10 = 2130837529;
        public static final int thumbnail_11 = 2130837530;
        public static final int thumbnail_12 = 2130837531;
        public static final int thumbnail_13 = 2130837532;
        public static final int thumbnail_14 = 2130837533;
        public static final int thumbnail_15 = 2130837534;
        public static final int thumbnail_16 = 2130837535;
        public static final int thumbnail_17 = 2130837536;
        public static final int thumbnail_18 = 2130837537;
        public static final int thumbnail_19 = 2130837538;
        public static final int thumbnail_2 = 2130837539;
        public static final int thumbnail_20 = 2130837540;
        public static final int thumbnail_3 = 2130837541;
        public static final int thumbnail_4 = 2130837542;
        public static final int thumbnail_5 = 2130837543;
        public static final int thumbnail_6 = 2130837544;
        public static final int thumbnail_7 = 2130837545;
        public static final int thumbnail_8 = 2130837546;
        public static final int thumbnail_9 = 2130837547;
        public static final int wallpaper_1 = 2130837548;
        public static final int wallpaper_10 = 2130837549;
        public static final int wallpaper_11 = 2130837550;
        public static final int wallpaper_12 = 2130837551;
        public static final int wallpaper_13 = 2130837552;
        public static final int wallpaper_14 = 2130837553;
        public static final int wallpaper_15 = 2130837554;
        public static final int wallpaper_16 = 2130837555;
        public static final int wallpaper_17 = 2130837556;
        public static final int wallpaper_18 = 2130837557;
        public static final int wallpaper_19 = 2130837558;
        public static final int wallpaper_2 = 2130837559;
        public static final int wallpaper_20 = 2130837560;
        public static final int wallpaper_3 = 2130837561;
        public static final int wallpaper_4 = 2130837562;
        public static final int wallpaper_5 = 2130837563;
        public static final int wallpaper_6 = 2130837564;
        public static final int wallpaper_7 = 2130837565;
        public static final int wallpaper_8 = 2130837566;
        public static final int wallpaper_9 = 2130837567;
    }

    public static final class id {
        public static final int ad = 2131034115;
        public static final int gallery = 2131034114;
        public static final int image = 2131034113;
        public static final int puzzle = 2131034116;
        public static final int root = 2131034112;
    }

    public static final class layout {
        public static final int list_game_type = 2130903040;
        public static final int menu = 2130903041;
        public static final int puzzle = 2130903042;
    }

    public static final class string {
        public static final int app_loading = 2130968577;
        public static final int app_name = 2130968576;
        public static final int app_quit = 2130968578;
        public static final int app_quit_text = 2130968579;
        public static final int dialog_no = 2130968594;
        public static final int dialog_yes = 2130968593;
        public static final int game_cancel = 2130968587;
        public static final int game_cancel_message = 2130968588;
        public static final int game_completed = 2130968586;
        public static final int game_start = 2130968585;
        public static final int game_type = 2130968584;
        public static final int game_x3 = 2130968580;
        public static final int game_x4 = 2130968581;
        public static final int game_x5 = 2130968582;
        public static final int game_x6 = 2130968583;
        public static final int wallpaper_choose_action = 2130968595;
        public static final int wallpaper_choose_play = 2130968597;
        public static final int wallpaper_choose_set = 2130968596;
        public static final int wallpaper_dialog = 2130968589;
        public static final int wallpaper_dialog_completed = 2130968590;
        public static final int wallpaper_loading = 2130968591;
        public static final int wallpaper_set = 2130968592;
        /* added by JADX */

        /* renamed from: app.name  reason: not valid java name */
        public static final int f0appname = 2130968576;
        /* added by JADX */

        /* renamed from: app.loading  reason: not valid java name */
        public static final int f1apploading = 2130968577;
        /* added by JADX */

        /* renamed from: app.quit  reason: not valid java name */
        public static final int f2appquit = 2130968578;
        /* added by JADX */

        /* renamed from: app.quit.text  reason: not valid java name */
        public static final int f3appquittext = 2130968579;
        /* added by JADX */

        /* renamed from: game.x3  reason: not valid java name */
        public static final int f4gamex3 = 2130968580;
        /* added by JADX */

        /* renamed from: game.x4  reason: not valid java name */
        public static final int f5gamex4 = 2130968581;
        /* added by JADX */

        /* renamed from: game.x5  reason: not valid java name */
        public static final int f6gamex5 = 2130968582;
        /* added by JADX */

        /* renamed from: game.x6  reason: not valid java name */
        public static final int f7gamex6 = 2130968583;
        /* added by JADX */

        /* renamed from: game.type  reason: not valid java name */
        public static final int f8gametype = 2130968584;
        /* added by JADX */

        /* renamed from: game.start  reason: not valid java name */
        public static final int f9gamestart = 2130968585;
        /* added by JADX */

        /* renamed from: game.completed  reason: not valid java name */
        public static final int f10gamecompleted = 2130968586;
        /* added by JADX */

        /* renamed from: game.cancel  reason: not valid java name */
        public static final int f11gamecancel = 2130968587;
        /* added by JADX */

        /* renamed from: game.cancel.message  reason: not valid java name */
        public static final int f12gamecancelmessage = 2130968588;
        /* added by JADX */

        /* renamed from: wallpaper.dialog  reason: not valid java name */
        public static final int f13wallpaperdialog = 2130968589;
        /* added by JADX */

        /* renamed from: wallpaper.dialog.completed  reason: not valid java name */
        public static final int f14wallpaperdialogcompleted = 2130968590;
        /* added by JADX */

        /* renamed from: wallpaper.loading  reason: not valid java name */
        public static final int f15wallpaperloading = 2130968591;
        /* added by JADX */

        /* renamed from: wallpaper.set  reason: not valid java name */
        public static final int f16wallpaperset = 2130968592;
        /* added by JADX */

        /* renamed from: dialog.yes  reason: not valid java name */
        public static final int f17dialogyes = 2130968593;
        /* added by JADX */

        /* renamed from: dialog.no  reason: not valid java name */
        public static final int f18dialogno = 2130968594;
        /* added by JADX */

        /* renamed from: wallpaper.choose.action  reason: not valid java name */
        public static final int f19wallpaperchooseaction = 2130968595;
        /* added by JADX */

        /* renamed from: wallpaper.choose.set  reason: not valid java name */
        public static final int f20wallpaperchooseset = 2130968596;
        /* added by JADX */

        /* renamed from: wallpaper.choose.play  reason: not valid java name */
        public static final int f21wallpaperchooseplay = 2130968597;
    }

    public static final class styleable {
        public static final int[] MenuTheme = {16842828};
        public static final int MenuTheme_android_galleryItemBackground = 0;
        public static final int[] com_admob_android_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
    }
}
