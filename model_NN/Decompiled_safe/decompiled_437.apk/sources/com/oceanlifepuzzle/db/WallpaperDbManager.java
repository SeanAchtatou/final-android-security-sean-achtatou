package com.oceanlifepuzzle.db;

import android.app.Activity;
import android.database.Cursor;

public class WallpaperDbManager {
    private static String TAG = "WallpaperDbManager";
    private static Activity mContext;
    private static WallpaperDbAdapter mDbHelper = null;
    private static WallpaperDbManager mWallpaperDbManager = null;

    private WallpaperDbManager(Activity pContext) {
        mContext = pContext;
        mDbHelper = new WallpaperDbAdapter(mContext);
        mDbHelper.open();
    }

    public static void initWallpaperDbManager(Activity pContext) {
        if (mWallpaperDbManager == null) {
            mWallpaperDbManager = new WallpaperDbManager(pContext);
        }
    }

    public static Integer getItemById(int itemPosition) {
        if (mWallpaperDbManager == null) {
            return null;
        }
        Cursor cursor = mDbHelper.fetchItem((long) itemPosition);
        mContext.startManagingCursor(cursor);
        if (cursor.moveToFirst()) {
            return Integer.valueOf(cursor.getInt(cursor.getColumnIndexOrThrow(WallpaperDbAdapter.ITEM_ID)));
        }
        return null;
    }

    public static void saveItem(int itemPosition) {
        if (mWallpaperDbManager != null && getItemById(itemPosition) == null) {
            mDbHelper.createItem(Integer.valueOf(itemPosition));
        }
    }
}
