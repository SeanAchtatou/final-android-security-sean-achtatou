package com.oceanlifepuzzle.game;

import com.oceanlifepuzzle.R;

public enum WallpaperGame {
    wallpaper(R.string.f20wallpaperchooseset),
    play(R.string.f21wallpaperchooseplay);
    
    private int action;

    private WallpaperGame(int pAction) {
        this.action = pAction;
    }

    public int getAction() {
        return this.action;
    }

    public void setAction(int action2) {
        this.action = action2;
    }
}
