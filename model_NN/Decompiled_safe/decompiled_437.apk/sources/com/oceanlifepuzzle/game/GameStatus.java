package com.oceanlifepuzzle.game;

public enum GameStatus {
    beforeStart,
    started,
    completed,
    wallpaper
}
