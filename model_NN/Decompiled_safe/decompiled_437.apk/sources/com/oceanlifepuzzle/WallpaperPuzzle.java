package com.oceanlifepuzzle;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.util.Log;
import com.oceanlifepuzzle.game.GameStatus;
import com.oceanlifepuzzle.game.GameType;
import com.oceanlifepuzzle.game.Panel;
import com.oceanlifepuzzle.game.SerializeUtility;
import java.io.IOException;
import java.util.List;

public class WallpaperPuzzle extends Activity {
    private static final String GAME_STATUS = "gameStatus";
    private static final String GAME_TYPE = "gameType";
    private static final String ITEM_POSITION = "itemPosition";
    private static final String SHUFFLE_BITMAP = "shuffleBitmap";
    private static final String TAG = "WallpaperPuzzle";
    private AlertDialog cancelDialog;
    private GameStatus gameStatus = GameStatus.beforeStart;
    private GameType gameType;
    private Integer itemPosition;
    private Panel panel;

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v12, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: java.util.Vector} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r16) {
        /*
            r15 = this;
            r13 = 0
            super.onCreate(r16)
            r12 = 2130903042(0x7f030002, float:1.741289E38)
            r15.setContentView(r12)
            r11 = 0
            if (r16 != 0) goto L_0x008e
            android.content.Intent r12 = r15.getIntent()
            android.os.Bundle r4 = r12.getExtras()
            if (r4 == 0) goto L_0x002d
            java.lang.String r12 = "itemPosition"
            int r12 = r4.getInt(r12)
            java.lang.Integer r12 = java.lang.Integer.valueOf(r12)
            r15.itemPosition = r12
            java.lang.String r12 = "itemGameType"
            java.io.Serializable r12 = r4.getSerializable(r12)
            com.oceanlifepuzzle.game.GameType r12 = (com.oceanlifepuzzle.game.GameType) r12
            r15.gameType = r12
        L_0x002d:
            com.oceanlifepuzzle.game.Panel r12 = new com.oceanlifepuzzle.game.Panel
            com.oceanlifepuzzle.game.GameType r13 = r15.gameType
            java.lang.Integer r14 = r15.itemPosition
            int r14 = r14.intValue()
            r12.<init>(r15, r13, r14)
            r15.panel = r12
            com.oceanlifepuzzle.game.Panel r12 = r15.panel
            com.oceanlifepuzzle.game.GameStatus r13 = r15.gameStatus
            r12.setGameStatus(r13)
            if (r11 == 0) goto L_0x004a
            com.oceanlifepuzzle.game.Panel r12 = r15.panel
            r12.setShufleBitmap(r11)
        L_0x004a:
            android.graphics.drawable.Drawable r12 = com.oceanlifepuzzle.game.ResourceLauncher.getBackgroundPuzzleBottom()
            if (r12 != 0) goto L_0x0067
            android.content.res.Resources r12 = r15.getResources()
            r13 = 2130837505(0x7f020001, float:1.7279966E38)
            android.graphics.Bitmap r2 = android.graphics.BitmapFactory.decodeResource(r12, r13)
            android.graphics.drawable.BitmapDrawable r12 = new android.graphics.drawable.BitmapDrawable
            android.content.res.Resources r13 = r15.getResources()
            r12.<init>(r13, r2)
            com.oceanlifepuzzle.game.ResourceLauncher.setBackgroundPuzzleBottom(r12)
        L_0x0067:
            r12 = 2131034115(0x7f050003, float:1.7678738E38)
            android.view.View r1 = r15.findViewById(r12)
            com.admob.android.ads.AdView r1 = (com.admob.android.ads.AdView) r1
            r12 = 60
            r1.setRequestInterval(r12)
            android.graphics.drawable.Drawable r12 = com.oceanlifepuzzle.game.ResourceLauncher.getBackgroundPuzzleBottom()
            r1.setBackgroundDrawable(r12)
            r12 = 2131034116(0x7f050004, float:1.767874E38)
            android.view.View r5 = r15.findViewById(r12)
            android.widget.LinearLayout r5 = (android.widget.LinearLayout) r5
            com.oceanlifepuzzle.game.Panel r12 = r15.panel
            r5.addView(r12)
            r15.initCancelDialog()
            return
        L_0x008e:
            r12 = 0
            android.content.SharedPreferences r10 = r15.getPreferences(r12)
            java.lang.String r12 = "shuffleBitmap"
            java.lang.String r9 = r10.getString(r12, r13)
            java.lang.String r12 = "gameType"
            java.lang.String r7 = r10.getString(r12, r13)
            java.lang.String r12 = "itemPosition"
            java.lang.String r8 = r10.getString(r12, r13)
            java.lang.String r12 = "gameStatus"
            java.lang.String r6 = r10.getString(r12, r13)
            java.lang.Object r12 = com.oceanlifepuzzle.game.SerializeUtility.deserializeString(r8)     // Catch:{ IOException -> 0x00cd, ClassNotFoundException -> 0x00da }
            java.lang.Integer r12 = (java.lang.Integer) r12     // Catch:{ IOException -> 0x00cd, ClassNotFoundException -> 0x00da }
            r15.itemPosition = r12     // Catch:{ IOException -> 0x00cd, ClassNotFoundException -> 0x00da }
            java.lang.Object r12 = com.oceanlifepuzzle.game.SerializeUtility.deserializeString(r9)     // Catch:{ IOException -> 0x00cd, ClassNotFoundException -> 0x00da }
            r0 = r12
            java.util.Vector r0 = (java.util.Vector) r0     // Catch:{ IOException -> 0x00cd, ClassNotFoundException -> 0x00da }
            r11 = r0
            java.lang.Object r12 = com.oceanlifepuzzle.game.SerializeUtility.deserializeString(r7)     // Catch:{ IOException -> 0x00cd, ClassNotFoundException -> 0x00da }
            com.oceanlifepuzzle.game.GameType r12 = (com.oceanlifepuzzle.game.GameType) r12     // Catch:{ IOException -> 0x00cd, ClassNotFoundException -> 0x00da }
            r15.gameType = r12     // Catch:{ IOException -> 0x00cd, ClassNotFoundException -> 0x00da }
            java.lang.Object r12 = com.oceanlifepuzzle.game.SerializeUtility.deserializeString(r6)     // Catch:{ IOException -> 0x00cd, ClassNotFoundException -> 0x00da }
            com.oceanlifepuzzle.game.GameStatus r12 = (com.oceanlifepuzzle.game.GameStatus) r12     // Catch:{ IOException -> 0x00cd, ClassNotFoundException -> 0x00da }
            r15.gameStatus = r12     // Catch:{ IOException -> 0x00cd, ClassNotFoundException -> 0x00da }
            goto L_0x002d
        L_0x00cd:
            r12 = move-exception
            r3 = r12
            java.lang.String r12 = "WallpaperPuzzle"
            java.lang.String r13 = r3.getMessage()
            android.util.Log.e(r12, r13)
            goto L_0x002d
        L_0x00da:
            r12 = move-exception
            r3 = r12
            java.lang.String r12 = "WallpaperPuzzle"
            java.lang.String r13 = r3.getMessage()
            android.util.Log.e(r12, r13)
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.oceanlifepuzzle.WallpaperPuzzle.onCreate(android.os.Bundle):void");
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (WallpaperMenu.mProgressDialog != null) {
            WallpaperMenu.mProgressDialog.dismiss();
        }
        List<Integer> shuffleBitmapList = this.panel.getShuffleBitmap();
        GameStatus gameStatus2 = this.panel.getGameStatus();
        try {
            SharedPreferences.Editor editor = getPreferences(0).edit();
            editor.putString(SHUFFLE_BITMAP, SerializeUtility.serializeString(shuffleBitmapList));
            editor.putString(GAME_TYPE, SerializeUtility.serializeString(this.gameType));
            editor.putString("itemPosition", SerializeUtility.serializeString(this.itemPosition));
            editor.putString(GAME_STATUS, SerializeUtility.serializeString(gameStatus2));
            editor.commit();
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    public void onBackPressed() {
        if (GameStatus.started.equals(this.panel.getGameStatus())) {
            this.cancelDialog.show();
        } else {
            super.onBackPressed();
        }
    }

    private void initCancelDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.f11gamecancel);
        builder.setIcon((int) R.drawable.icon);
        builder.setMessage((int) R.string.f12gamecancelmessage).setCancelable(true).setPositiveButton((int) R.string.f17dialogyes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                WallpaperPuzzle.super.onBackPressed();
            }
        }).setNegativeButton((int) R.string.f18dialogno, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        this.cancelDialog = builder.create();
    }
}
