package com.admob.android.ads;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.admob.android.ads.InterstitialAd;
import com.admob.android.ads.view.AdMobWebView;
import com.oceanlifepuzzle.WallpaperMenu;
import java.lang.ref.WeakReference;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.StringTokenizer;

/* compiled from: AdMobWebViewClient */
public class ad extends WebViewClient {
    private WeakReference<Activity> a;
    private ag b;
    private Map<String, String> c;
    protected WeakReference<AdMobWebView> d;

    public ad(AdMobWebView adMobWebView) {
        this(adMobWebView, null);
    }

    public ad(AdMobWebView adMobWebView, WeakReference<Activity> weakReference) {
        this.d = new WeakReference<>(adMobWebView);
        this.a = weakReference;
        this.b = new ag(weakReference.get(), this.d);
        this.c = null;
        adMobWebView.addJavascriptInterface(this.b, "JsProxy");
    }

    public void onPageFinished(WebView webView, String str) {
        AdMobWebView adMobWebView = this.d.get();
        if (adMobWebView != null) {
            if (str != null && str.equals(adMobWebView.c)) {
                if (this.c == null) {
                    this.c = new HashMap();
                    Context context = adMobWebView.getContext();
                    this.c.put("sdkVersion", AdManager.SDK_VERSION);
                    this.c.put("ua", f.h());
                    this.c.put("portrait", AdManager.getOrientation(context));
                    this.c.put("width", String.valueOf(adMobWebView.getWidth()));
                    this.c.put("height", String.valueOf(adMobWebView.getHeight()));
                    this.c.put("isu", AdManager.getUserId(context));
                }
                adMobWebView.a("onEvent", WallpaperMenu.LOADED, this.c);
            } else if (InterstitialAd.c.a(AdManager.LOG, 4)) {
                Log.i(AdManager.LOG, "Unexpected page loaded, urlThatFinished: " + str);
            }
        }
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        Hashtable<String, String> a2;
        String str2;
        Hashtable<String, String> a3;
        String str3;
        Hashtable<String, String> a4;
        String str4;
        if (InterstitialAd.c.a(AdManager.LOG, 2)) {
            Log.v(AdManager.LOG, "shouldOverrideUrlLoading, url: " + str);
        }
        AdMobWebView adMobWebView = this.d.get();
        if (adMobWebView == null) {
            return false;
        }
        Context context = adMobWebView.getContext();
        try {
            URI uri = new URI(str);
            if ("admob".equals(uri.getScheme())) {
                String host = uri.getHost();
                if ("launch".equals(host)) {
                    String query = uri.getQuery();
                    if (!(query == null || (a4 = a(query)) == null || (str4 = a4.get("url")) == null)) {
                        if (!(context instanceof Activity)) {
                            context = this.a.get();
                        }
                        if (context != null) {
                            context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str4)));
                        }
                        return true;
                    }
                } else if ("open".equals(host)) {
                    String query2 = uri.getQuery();
                    if (!(query2 == null || (a3 = a(query2)) == null || (str3 = a3.get("vars")) == null)) {
                        adMobWebView.loadUrl("javascript: JsProxy.setDataAndOpen(" + str3 + ")");
                        return true;
                    }
                } else if ("closecanvas".equals(host)) {
                    if (webView == adMobWebView) {
                        adMobWebView.a();
                        return true;
                    }
                } else if ("log".equals(host)) {
                    String query3 = uri.getQuery();
                    if (!(query3 == null || (a2 = a(query3)) == null || (str2 = a2.get("string")) == null)) {
                        if (InterstitialAd.c.a(AdManager.LOG, 3)) {
                            Log.d(AdManager.LOG, "<AdMob:WebView>: " + str2);
                        }
                        return true;
                    }
                } else {
                    if (InterstitialAd.c.a(AdManager.LOG, 3)) {
                        Log.d(AdManager.LOG, "Received message from JS but didn't know how to handle: " + str);
                    }
                    return true;
                }
            }
        } catch (URISyntaxException e) {
            Log.w(AdManager.LOG, "Bad link URL in AdMob web view.", e);
        }
        return false;
    }

    public static Hashtable<String, String> a(String str) {
        Hashtable<String, String> hashtable = null;
        if (str != null) {
            hashtable = new Hashtable<>();
            StringTokenizer stringTokenizer = new StringTokenizer(str, "&");
            while (stringTokenizer.hasMoreTokens()) {
                String nextToken = stringTokenizer.nextToken();
                int indexOf = nextToken.indexOf(61);
                if (indexOf != -1) {
                    String substring = nextToken.substring(0, indexOf);
                    String substring2 = nextToken.substring(indexOf + 1);
                    if (!(substring == null || substring2 == null)) {
                        hashtable.put(substring, substring2);
                    }
                }
            }
        }
        return hashtable;
    }
}
