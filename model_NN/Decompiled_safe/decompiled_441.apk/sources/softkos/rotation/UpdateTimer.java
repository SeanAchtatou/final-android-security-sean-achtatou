package softkos.rotation;

import java.util.TimerTask;
import softkos.rotation.Vars;

public class UpdateTimer extends TimerTask {
    public void run() {
        if (Vars.getInstance().gameState == Vars.GameState.Game) {
            Vars.getInstance().advanceFrame();
            if (GameCanvas.getInstance() != null) {
                GameCanvas.getInstance().postInvalidate();
            }
        }
    }
}
