package softkos.rotation;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.Uri;
import android.view.MotionEvent;
import android.view.View;

public class MenuCanvas extends View {
    static MenuCanvas instance = null;
    gButton howPlayBtn = null;
    gButton mobilsoftBtn = null;
    gButton otherAppBtn = null;
    Paint paint = new Paint();
    PaintManager paintMgr;
    gButton startGame = null;
    Vars vars;

    public MenuCanvas(Context c) {
        super(c);
        instance = this;
        this.vars = Vars.getInstance();
        this.paintMgr = PaintManager.getInstance();
        initUI();
    }

    public void showUI(boolean show) {
        for (int i = 0; i < this.vars.getButtonListSize(); i++) {
            this.vars.getButton(i).hide();
        }
        if (show) {
            this.startGame.show();
            this.mobilsoftBtn.show();
            this.howPlayBtn.show();
            this.otherAppBtn.show();
        }
    }

    public void initUI() {
        this.startGame = new gButton();
        this.startGame.setSize(240, 50);
        this.startGame.setId(Vars.getInstance().START_GAME);
        this.startGame.show();
        this.startGame.setBackImages("playoffbtn", "playonbtn");
        Vars.getInstance().addButton(this.startGame);
        this.otherAppBtn = new gButton();
        this.otherAppBtn.setSize(240, 50);
        this.otherAppBtn.setId(Vars.getInstance().OTHER_APP);
        this.otherAppBtn.show();
        this.otherAppBtn.setBackImages("otherapp_off", "otherapp_on");
        Vars.getInstance().addButton(this.otherAppBtn);
        this.howPlayBtn = new gButton();
        this.howPlayBtn.setSize(240, 50);
        this.howPlayBtn.setId(Vars.getInstance().HOWTOPLAY);
        this.howPlayBtn.show();
        this.howPlayBtn.setBackImages("howplayoffbtn", "howplayonbtn");
        Vars.getInstance().addButton(this.howPlayBtn);
        this.mobilsoftBtn = new gButton();
        this.mobilsoftBtn.setSize(240, 50);
        this.mobilsoftBtn.setId(Vars.getInstance().GO_TO_MOBILSOFT);
        this.mobilsoftBtn.show();
        this.mobilsoftBtn.setBackImages("moreappsoffbtn", "moreappsonbtn");
        Vars.getInstance().addButton(this.mobilsoftBtn);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        layoutUI();
    }

    public void layoutUI() {
        this.mobilsoftBtn.setPosition((getWidth() / 2) - (this.startGame.getWidth() / 2), getHeight() - ((int) (((double) this.startGame.getHeight()) * 1.05d)));
        this.otherAppBtn.setPosition((getWidth() / 2) - (this.startGame.getWidth() / 2), getHeight() - ((int) (((double) this.startGame.getHeight()) * 2.1d)));
        this.howPlayBtn.setPosition((getWidth() / 2) - (this.startGame.getWidth() / 2), getHeight() - ((int) (((double) this.startGame.getHeight()) * 3.15d)));
        this.startGame.setPosition((getWidth() / 2) - (this.startGame.getWidth() / 2), getHeight() - ((int) (((double) this.startGame.getHeight()) * 4.2d)));
    }

    public void mouseDown(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDown(x, y);
            if (pressed != 0) {
                invalidate();
                return;
            }
        }
    }

    public void mouseUp(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseUp(x, y);
            if (pressed != 0) {
                handleCommand(Vars.getInstance().getButton(i).getId());
                invalidate();
                return;
            }
        }
    }

    public void mouseDrag(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDrag(x, y);
            if (pressed != 0) {
                invalidate();
                return;
            }
        }
    }

    public void handleCommand(int id) {
        if (id == Vars.getInstance().START_GAME) {
            MainActivity.getInstance().showGame();
        } else if (id == Vars.getInstance().GO_TO_MOBILSOFT) {
            MainActivity.getInstance().startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.mobilsoft.pl")));
        } else if (id == Vars.getInstance().HOWTOPLAY) {
            MainActivity.getInstance().showHowPlay();
        } else if (id == Vars.getInstance().OTHER_APP) {
            MainActivity.getInstance().startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=sofkos.frogsjump")));
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();
        if (event.getAction() == 0) {
            mouseDown(x, y);
        } else if (2 == event.getAction()) {
            mouseDrag(x, y);
        } else if (1 == event.getAction()) {
            mouseUp(x, y);
        }
        invalidate();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.paint.setAntiAlias(true);
        canvas.drawColor(-1);
        this.paintMgr.drawImage(canvas, "gameback", 0, 0, getWidth(), getHeight());
        this.paintMgr.drawImage(canvas, "gamename", 0, 0, getWidth(), getWidth());
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            Vars.getInstance().getButton(i).draw(canvas, this.paint);
        }
    }

    public static MenuCanvas getInstance() {
        return instance;
    }
}
