package softkos.rotation;

import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;

public class Vars {
    static Vars instance = null;
    public int GO_TO_MOBILSOFT;
    public int HOWTOPLAY;
    public int OTHER_APP;
    public int PLAY_NEXT;
    public int START_GAME;
    int board_height;
    int board_pos_x;
    int board_pos_y;
    int board_width;
    ArrayList<gButton> buttonList;
    boolean can_rotate;
    int cell_size;
    int[] currLevel;
    ArrayList<Element> elementList;
    boolean endLevel;
    public GameState gameState;
    int level;
    public int painting;
    Random random;
    int screenHeight;
    int screenWidth;
    Timer updateTimer;
    int xspacing;

    public enum GameState {
        Menu,
        Game,
        Highscores,
        HowPlay
    }

    public int getLevel() {
        return this.level;
    }

    public Vars() {
        this.painting = 0;
        this.level = 0;
        this.buttonList = null;
        this.cell_size = 0;
        this.can_rotate = true;
        this.xspacing = 0;
        this.endLevel = false;
        this.elementList = null;
        this.START_GAME = 100;
        this.GO_TO_MOBILSOFT = 101;
        this.HOWTOPLAY = 102;
        this.OTHER_APP = 103;
        this.PLAY_NEXT = 200;
        this.buttonList = new ArrayList<>();
        this.buttonList.clear();
        this.random = new Random();
        this.updateTimer = new Timer();
        this.updateTimer.schedule(new UpdateTimer(), 10, 60);
        this.currLevel = new int[19];
    }

    public int getBoardPosX() {
        return this.board_pos_x;
    }

    public int getBoardPosY() {
        return this.board_pos_y;
    }

    public int getBoardWidth() {
        return this.board_width;
    }

    public int getBoardHeight() {
        return this.board_height;
    }

    public boolean canRotate() {
        return this.can_rotate;
    }

    public void advanceFrame() {
        boolean cr = false;
        for (int i = 0; i < this.elementList.size(); i++) {
            cr |= getElement(i).move();
        }
        if (cr) {
            this.can_rotate = false;
        } else {
            this.can_rotate = true;
        }
        checkEndLevel();
    }

    public void hideAllButtons() {
        for (int i = 0; i < this.buttonList.size(); i++) {
            getButton(i).hide();
        }
    }

    public void initLevel() {
        this.endLevel = false;
        calcBoard();
        this.currLevel = new int[Levels.getInstance().getLevel(this.level).length];
        for (int i = 0; i < this.currLevel.length; i++) {
            this.currLevel[i] = Levels.getInstance().getLevel(this.level)[i];
        }
        this.elementList.clear();
        int px = (getBoardPosX() + (getBoardWidth() / 2)) - ((getCellSize() * 3) / 2);
        int py = getBoardPosY() + (getCellSize() / 2);
        for (int i2 = 0; i2 < getCurrLevel().length; i2++) {
            int val = getCurrLevelAt(i2);
            Element e = new Element(true);
            e.SetPos(px, py);
            e.setId(i2);
            e.setDestPos(px, py);
            e.setType(val);
            e.setSize(getCellSize(), getCellSize());
            this.elementList.add(e);
            px += getCellSize() + this.xspacing;
            if (i2 == 2 || i2 == 11) {
                px = ((getBoardPosX() + (getBoardWidth() / 2)) - ((getCellSize() * 4) / 2)) - (this.xspacing / 2);
                py += getCellSize();
            }
            if (i2 == 6) {
                px = ((getBoardPosX() + (getBoardWidth() / 2)) - ((getCellSize() * 5) / 2)) - this.xspacing;
                py += getCellSize();
            }
            if (i2 == 15) {
                px = (getBoardPosX() + (getBoardWidth() / 2)) - ((getCellSize() * 3) / 2);
                py += getCellSize();
            }
        }
        int small_size = getCellSize() / 2;
        int px2 = ((small_size * 3) / 2) + 10;
        int py2 = (small_size / 2) + 36;
        int[] solved = Levels.getInstance().getSolvedLevel(this.level);
        for (int i3 = 0; i3 < solved.length; i3++) {
            int val2 = solved[i3];
            Element e2 = new Element(false);
            e2.SetPos(px2, py2);
            e2.setId(i3 + 100);
            e2.setDestPos(px2, py2);
            e2.setType(val2);
            e2.setSize(small_size, small_size);
            this.elementList.add(e2);
            px2 += small_size;
            if (i3 == 2 || i3 == 11) {
                px2 = (((small_size * 3) / 2) + 10) - (small_size / 2);
                py2 += small_size;
            }
            if (i3 == 6) {
                px2 = (((small_size * 3) / 2) + 10) - small_size;
                py2 += small_size;
            }
            if (i3 == 15) {
                px2 = ((small_size * 3) / 2) + 10;
                py2 += small_size;
            }
        }
    }

    public Element getElementById(int id) {
        for (int i = 0; i < this.elementList.size(); i++) {
            if (getElement(i).getId() == id) {
                return getElement(i);
            }
        }
        return null;
    }

    public int getElementIndexById(int id) {
        for (int i = 0; i < this.elementList.size(); i++) {
            if (getElement(i).getId() == id) {
                return i;
            }
        }
        return -1;
    }

    public void makeRotation(int[] elements1, int[] elements2) {
        int tmp = getCurrLevelAt(elements1[5]);
        this.currLevel[elements1[5]] = this.currLevel[elements1[4]];
        this.currLevel[elements1[4]] = this.currLevel[elements1[3]];
        this.currLevel[elements1[3]] = this.currLevel[elements1[2]];
        this.currLevel[elements1[2]] = this.currLevel[elements1[1]];
        this.currLevel[elements1[1]] = this.currLevel[elements1[0]];
        this.currLevel[elements1[0]] = tmp;
        getElementById(elements1[0]).setDestPos(getElementById(elements2[0]).getPx(), getElementById(elements2[0]).getPy());
        getElementById(elements1[1]).setDestPos(getElementById(elements2[1]).getPx(), getElementById(elements2[1]).getPy());
        getElementById(elements1[2]).setDestPos(getElementById(elements2[2]).getPx(), getElementById(elements2[2]).getPy());
        getElementById(elements1[3]).setDestPos(getElementById(elements2[3]).getPx(), getElementById(elements2[3]).getPy());
        getElementById(elements1[4]).setDestPos(getElementById(elements2[4]).getPx(), getElementById(elements2[4]).getPy());
        getElementById(elements1[5]).setDestPos(getElementById(elements2[5]).getPx(), getElementById(elements2[5]).getPy());
        int[] indexes = {getElementIndexById(elements1[0]), getElementIndexById(elements1[1]), getElementIndexById(elements1[2]), getElementIndexById(elements1[3]), getElementIndexById(elements1[4]), getElementIndexById(elements1[5])};
        getElement(indexes[0]).setId(elements2[0]);
        getElement(indexes[1]).setId(elements2[1]);
        getElement(indexes[2]).setId(elements2[2]);
        getElement(indexes[3]).setId(elements2[3]);
        getElement(indexes[4]).setId(elements2[4]);
        getElement(indexes[5]).setId(elements2[5]);
    }

    public void rotateBoardLeft(int index) {
        if (index == 4) {
            int[] tab1 = new int[6];
            tab1[1] = 3;
            tab1[2] = 8;
            tab1[3] = 9;
            tab1[4] = 5;
            tab1[5] = 1;
            int[] tab2 = new int[6];
            tab2[0] = 3;
            tab2[1] = 8;
            tab2[2] = 9;
            tab2[3] = 5;
            tab2[4] = 1;
            makeRotation(tab1, tab2);
        }
        if (index == 5) {
            makeRotation(new int[]{1, 4, 9, 10, 6, 2}, new int[]{4, 9, 10, 6, 2, 1});
        }
        if (index == 8) {
            makeRotation(new int[]{3, 7, 12, 13, 9, 4}, new int[]{7, 12, 13, 9, 4, 3});
        }
        if (index == 9) {
            makeRotation(new int[]{4, 8, 13, 14, 10, 5}, new int[]{8, 13, 14, 10, 5, 4});
        }
        if (index == 10) {
            makeRotation(new int[]{5, 9, 14, 15, 11, 6}, new int[]{9, 14, 15, 11, 6, 5});
        }
        if (index == 13) {
            makeRotation(new int[]{8, 12, 16, 17, 14, 9}, new int[]{12, 16, 17, 14, 9, 8});
        }
        if (index == 14) {
            makeRotation(new int[]{9, 13, 17, 18, 15, 10}, new int[]{13, 17, 18, 15, 10, 9});
        }
    }

    public void rotateBoardRight(int index) {
        if (index == 4) {
            int[] tab1 = new int[6];
            tab1[1] = 1;
            tab1[2] = 5;
            tab1[3] = 9;
            tab1[4] = 8;
            tab1[5] = 3;
            int[] tab2 = new int[6];
            tab2[0] = 1;
            tab2[1] = 5;
            tab2[2] = 9;
            tab2[3] = 8;
            tab2[4] = 3;
            makeRotation(tab1, tab2);
        }
        if (index == 5) {
            makeRotation(new int[]{1, 2, 6, 10, 9, 4}, new int[]{2, 6, 10, 9, 4, 1});
        }
        if (index == 8) {
            makeRotation(new int[]{3, 4, 9, 13, 12, 7}, new int[]{4, 9, 13, 12, 7, 3});
        }
        if (index == 9) {
            makeRotation(new int[]{4, 5, 10, 14, 13, 8}, new int[]{5, 10, 14, 13, 8, 4});
        }
        if (index == 10) {
            makeRotation(new int[]{5, 6, 11, 15, 14, 9}, new int[]{6, 11, 15, 14, 9, 5});
        }
        if (index == 13) {
            makeRotation(new int[]{8, 9, 14, 17, 16, 12}, new int[]{9, 14, 17, 16, 12, 8});
        }
        if (index == 14) {
            makeRotation(new int[]{9, 10, 15, 18, 17, 13}, new int[]{10, 15, 18, 17, 13, 9});
        }
    }

    public void rotateAt(int x, int y) {
        int id;
        for (int i = 0; i < this.elementList.size(); i++) {
            if (getElement(i).canClick() && (((id = getElement(i).getId()) == 4 || id == 5 || id == 8 || id == 9 || id == 10 || id == 13 || id == 14) && x > getElement(i).getPx() && x < getElement(i).getPx() + getElement(i).getWidth() && y > getElement(i).getPy() && y < getElement(i).getPy() + getElement(i).getHeight())) {
                rotateBoardLeft(id);
            }
        }
    }

    public void calcBoard() {
        int size = (int) (((double) this.screenWidth) * 0.85d);
        this.board_height = size;
        this.board_width = size;
        this.board_pos_x = (this.screenWidth - 10) - size;
        this.board_pos_y = (this.screenHeight - 70) - size;
        this.cell_size = size / 6;
        this.xspacing = (int) ((((double) size) * 0.1d) / 6.0d);
    }

    public boolean isEndLevel() {
        return this.endLevel;
    }

    public void checkEndLevel() {
        boolean solved = true;
        this.endLevel = false;
        if (Levels.getInstance().getSolvedLevel(this.level) != null) {
            int[] lev_solved = Levels.getInstance().getSolvedLevel(this.level);
            int i = 0;
            while (true) {
                if (i >= lev_solved.length) {
                    break;
                } else if (lev_solved[i] != getCurrLevelAt(i)) {
                    solved = false;
                    break;
                } else {
                    i++;
                }
            }
            if (this.can_rotate) {
                this.endLevel = solved;
                if (solved) {
                    Levels.getInstance().setSolved(this.level);
                }
            }
        }
    }

    public int getCellSize() {
        return this.cell_size;
    }

    public double dist(int x1, int y1, int x2, int y2) {
        return Math.sqrt((double) (((x1 - x2) * (x1 - x2)) + ((y1 - y2) * (y1 - y2))));
    }

    public void setLevel(int l) {
        this.level = l;
    }

    public void newGame() {
        setLevel(0);
        nextLevel();
    }

    public void nextLevel() {
        FileWR.getInstance().saveGame();
        this.level++;
        if (this.level >= Levels.getInstance().getLevelCount()) {
            this.level--;
        } else {
            initLevel();
        }
    }

    public void prevLevel() {
        FileWR.getInstance().saveGame();
        this.level--;
        if (this.level < 1) {
            this.level = 1;
        } else {
            initLevel();
        }
    }

    public int getButtonListSize() {
        return this.buttonList.size();
    }

    public void addButton(gButton b) {
        this.buttonList.add(b);
    }

    public ArrayList<gButton> getButtonList() {
        return this.buttonList;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public gButton getButton(int b) {
        if (b < 0 || b >= this.buttonList.size()) {
            return null;
        }
        return this.buttonList.get(b);
    }

    public int getScreenWidth() {
        return this.screenWidth;
    }

    public int getScreenHeight() {
        return this.screenHeight;
    }

    public void init() {
        this.random = new Random();
        this.elementList = new ArrayList<>();
        this.elementList.clear();
    }

    public int[] getCurrLevel() {
        return this.currLevel;
    }

    public int getCurrLevelAt(int i) {
        return this.currLevel[i];
    }

    public int GetNextInt(int max) {
        if (max == 0) {
            max = 1;
        }
        return Math.abs(this.random.nextInt() % max);
    }

    public void screenSize(int w, int h) {
        this.screenWidth = w;
        this.screenHeight = h;
    }

    public static Vars getInstance() {
        if (instance == null) {
            instance = new Vars();
            instance.init();
        }
        return instance;
    }

    public int nextInt(int max) {
        return this.random.nextInt(max);
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public Element getElement(int i) {
        if (i < 0 || i >= this.elementList.size()) {
            return null;
        }
        return this.elementList.get(i);
    }

    public ArrayList<Element> getElementList() {
        return this.elementList;
    }
}
