package softkos.rotation;

import android.graphics.drawable.Drawable;

public class ImageLoader {
    static ImageLoader instance = null;
    String[] image_names = {"gameback", "gamename", "playoffbtn", "playonbtn", "moreappsoffbtn", "moreappsonbtn", "howplayoffbtn", "howplayonbtn", "playnextoffbtn", "playnextonbtn", "retryoffbtn", "retryonbtn", "howplay1", "howplay2", "endlevelpassed", "circle", "arrow_left", "arrow_right", "element0", "element1", "element2", "element3", "element4", "otherapp_off", "otherapp_on"};
    Drawable[] images;
    int[] img_id = {R.drawable.gameback, R.drawable.gamename, R.drawable.playoff, R.drawable.playon, R.drawable.moreappsoff, R.drawable.moreappson, R.drawable.howplayoff, R.drawable.howplayon, R.drawable.playnextoffbtn, R.drawable.playnextonbtn, R.drawable.retryoffbtn, R.drawable.retryonbtn, R.drawable.howplay1, R.drawable.howplay2, R.drawable.end_passed, R.drawable.circle, R.drawable.arrow_left, R.drawable.arrow_right, R.drawable.element1, R.drawable.element2, R.drawable.element3, R.drawable.element4, R.drawable.element5, R.drawable.otherapp_off, R.drawable.otherapp_on};

    public static ImageLoader getInstance() {
        if (instance == null) {
            instance = new ImageLoader();
        }
        return instance;
    }

    public void LoadImages() {
        int len = this.img_id.length;
        this.images = new Drawable[this.image_names.length];
        for (int i = 0; i < len; i++) {
            this.images[i] = MainActivity.getInstance().getResources().getDrawable(this.img_id[i]);
        }
    }

    public Drawable getImage(String img) {
        for (int i = 0; i < this.image_names.length; i++) {
            if (img.equals(this.image_names[i])) {
                return this.images[i];
            }
        }
        return null;
    }
}
