package softkos.rotation;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int arrow_left = 2130837504;
        public static final int arrow_right = 2130837505;
        public static final int circle = 2130837506;
        public static final int element1 = 2130837507;
        public static final int element2 = 2130837508;
        public static final int element3 = 2130837509;
        public static final int element4 = 2130837510;
        public static final int element5 = 2130837511;
        public static final int end_passed = 2130837512;
        public static final int gameback = 2130837513;
        public static final int gamename = 2130837514;
        public static final int howplay1 = 2130837515;
        public static final int howplay2 = 2130837516;
        public static final int howplayoff = 2130837517;
        public static final int howplayon = 2130837518;
        public static final int icon = 2130837519;
        public static final int moreappsoff = 2130837520;
        public static final int moreappson = 2130837521;
        public static final int otherapp_off = 2130837522;
        public static final int otherapp_on = 2130837523;
        public static final int playnextoffbtn = 2130837524;
        public static final int playnextonbtn = 2130837525;
        public static final int playoff = 2130837526;
        public static final int playon = 2130837527;
        public static final int retryoffbtn = 2130837528;
        public static final int retryonbtn = 2130837529;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class string {
        public static final int app_name = 2130968576;
    }
}
