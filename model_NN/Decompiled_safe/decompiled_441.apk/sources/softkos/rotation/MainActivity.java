package softkos.rotation;

import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import softkos.rotation.Vars;

public class MainActivity extends Activity {
    static MainActivity instance;
    GameCanvas gameCanvas = null;
    LinearLayout gameLayout = null;
    HowPlayCanvas howPlayCanvas = null;
    MenuCanvas menuCanvas = null;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        instance = this;
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        Vars.getInstance().screenSize(dm.widthPixels, dm.heightPixels);
        ImageLoader.getInstance().LoadImages();
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        FileWR.getInstance().loadGame();
        showMenu();
    }

    public void showMenu() {
        Vars.getInstance().gameState = Vars.GameState.Menu;
        if (this.menuCanvas == null) {
            this.menuCanvas = new MenuCanvas(this);
        }
        if (this.gameCanvas != null) {
            this.gameCanvas.showUI(false);
        }
        this.menuCanvas.showUI(true);
        setContentView(this.menuCanvas);
    }

    public void showHowPlay() {
        Vars.getInstance().gameState = Vars.GameState.HowPlay;
        if (this.howPlayCanvas == null) {
            this.howPlayCanvas = new HowPlayCanvas(this);
        }
        if (this.menuCanvas != null) {
            this.menuCanvas.showUI(false);
        }
        setContentView(this.howPlayCanvas);
    }

    public void showHighscores() {
    }

    public void showGame() {
        if (this.menuCanvas != null) {
            this.menuCanvas.showUI(false);
        }
        if (this.gameCanvas == null) {
            this.gameLayout = new LinearLayout(this);
            this.gameLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
            this.gameLayout.setOrientation(1);
            this.gameCanvas = new GameCanvas(this);
            AdView adView = new AdView(this, AdSize.BANNER, "a14cc1c387cfd80");
            adView.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
            this.gameCanvas.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
            adView.loadAd(new AdRequest());
            this.gameLayout.addView(adView);
            this.gameLayout.addView(this.gameCanvas);
        }
        Vars.getInstance().gameState = Vars.GameState.Game;
        Vars.getInstance().newGame();
        this.gameCanvas.showUI(true);
        setContentView(this.gameLayout);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4 && (Vars.getInstance().gameState == Vars.GameState.Game || Vars.getInstance().gameState == Vars.GameState.Highscores || Vars.getInstance().gameState == Vars.GameState.HowPlay)) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyUp(keyCode, event);
        }
        if (Vars.getInstance().gameState == Vars.GameState.Game || Vars.getInstance().gameState == Vars.GameState.Highscores || Vars.getInstance().gameState == Vars.GameState.HowPlay) {
            showMenu();
        }
        return true;
    }

    public static MainActivity getInstance() {
        return instance;
    }
}
