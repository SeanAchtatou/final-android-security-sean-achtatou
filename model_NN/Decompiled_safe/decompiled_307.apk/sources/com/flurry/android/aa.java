package com.flurry.android;

import android.view.View;
import android.widget.TextView;

final class aa implements View.OnFocusChangeListener {
    private /* synthetic */ TextView a;
    private /* synthetic */ z b;

    aa(z zVar, TextView textView) {
        this.b = zVar;
        this.a = textView;
    }

    public final void onFocusChange(View view, boolean z) {
        this.a.setText(z ? this.b.b : this.b.a);
    }
}
