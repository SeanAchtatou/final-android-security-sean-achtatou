package com.flurry.android;

import android.os.Handler;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

final class af {
    private List a = new ArrayList();
    private Handler b;
    private Handler c;
    private int d;
    private Runnable e;

    af(Handler handler, int i) {
        this.b = handler;
        this.c = new Handler();
        this.d = i;
        this.e = new r(this);
        b();
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(ad adVar) {
        adVar.a();
        this.a.add(new WeakReference(adVar));
    }

    /* access modifiers changed from: private */
    public synchronized void a() {
        ArrayList arrayList = new ArrayList();
        for (WeakReference weakReference : this.a) {
            ad adVar = (ad) weakReference.get();
            if (adVar != null) {
                arrayList.add(adVar);
            }
        }
        this.c.post(new p(arrayList));
        b();
    }

    private synchronized void b() {
        Iterator it = this.a.iterator();
        while (it.hasNext()) {
            if (((WeakReference) it.next()).get() == null) {
                it.remove();
            }
        }
        this.b.removeCallbacks(this.e);
        this.b.postDelayed(this.e, (long) this.d);
    }
}
