package com.softdyssee.travel.a_u.s.state_tennessee;

public final class R {

    public static final class anim {
        public static final int slide_in = 2130968576;
        public static final int slide_out = 2130968577;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int arrow_left = 2130837504;
        public static final int arrow_right = 2130837505;
        public static final int awof = 2130837506;
        public static final int display_all = 2130837507;
        public static final int display_favorite = 2130837508;
        public static final int favorite = 2130837509;
        public static final int favorite_disabled = 2130837510;
        public static final int flurry_catalog_background = 2130837511;
        public static final int flurry_catalog_croix = 2130837512;
        public static final int flurry_catalog_croix_button = 2130837513;
        public static final int flurry_catalog_croix_pressed = 2130837514;
        public static final int flurry_default_icon = 2130837515;
        public static final int icon = 2130837516;
        public static final int inmobi_background = 2130837517;
        public static final int other_apps = 2130837518;
        public static final int quit = 2130837519;
        public static final int sdcard = 2130837520;
        public static final int share = 2130837521;
        public static final int wallpaper = 2130837522;
    }

    public static final class id {
        public static final int adView = 2131165206;
        public static final int arrow_left = 2131165193;
        public static final int arrow_left_cell = 2131165192;
        public static final int arrow_right = 2131165205;
        public static final int arrow_right_cell = 2131165204;
        public static final int backgroundLayout = 2131165189;
        public static final int display_all = 2131165197;
        public static final int display_all_cell = 2131165196;
        public static final int favorite = 2131165195;
        public static final int favorite_cell = 2131165194;
        public static final int flurryAppcircleTitle = 2131165209;
        public static final int flurryCatalog = 2131165208;
        public static final int flurry_catalog_close_button = 2131165211;
        public static final int flurry_list = 2131165210;
        public static final int gallery = 2131165190;
        public static final int imageHolder = 2131165188;
        public static final int infoView = 2131165191;
        public static final int mobFoxView = 2131165207;
        public static final int offerDescription = 2131165187;
        public static final int offerImage = 2131165184;
        public static final int offerPrice = 2131165185;
        public static final int offerTitle = 2131165186;
        public static final int save_to_sd = 2131165201;
        public static final int sdcard_cell = 2131165200;
        public static final int set_as_wallpaper = 2131165199;
        public static final int share = 2131165203;
        public static final int share_cell = 2131165202;
        public static final int wallpaper_cell = 2131165198;
    }

    public static final class layout {
        public static final int flurry_catalog_list_item = 2130903040;
        public static final int gallery_webview_item = 2130903041;
        public static final int main = 2130903042;
    }

    public static final class raw {
        public static final int photos = 2131034112;
    }

    public static final class string {
        public static final int app_name = 2131099648;
    }
}
