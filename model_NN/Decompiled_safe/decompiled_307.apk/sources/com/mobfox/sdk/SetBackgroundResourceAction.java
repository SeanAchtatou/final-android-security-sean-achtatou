package com.mobfox.sdk;

import android.webkit.WebView;

class SetBackgroundResourceAction implements Runnable {
    private int backgroundResource;
    private WebView view;

    public SetBackgroundResourceAction(WebView view2, int backgroundResource2) {
        this.view = view2;
        this.backgroundResource = backgroundResource2;
    }

    public void run() {
        try {
            this.view.setBackgroundResource(this.backgroundResource);
        } catch (Exception e) {
        }
    }
}
