package jp.co.fullyear.Sokudoku;

public final class R {

    public static final class anim {
        public static final int anim = 2130968576;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int blue = 2131034113;
        public static final int custom_dialog_background = 2131034114;
        public static final int custom_dialog_button_foreground = 2131034117;
        public static final int custom_dialog_text_background = 2131034118;
        public static final int custom_dialog_text_foreground = 2131034119;
        public static final int custom_dialog_title_background = 2131034115;
        public static final int custom_dialog_title_foreground = 2131034116;
        public static final int red = 2131034112;
    }

    public static final class dimen {
        public static final int font_size = 2131099649;
        public static final int footer_height = 2131099648;
        public static final int seekbar_width = 2131099650;
    }

    public static final class drawable {
        public static final int back = 2130837504;
        public static final int bt1 = 2130837505;
        public static final int bt2 = 2130837506;
        public static final int custom_dialog_button_background = 2130837507;
        public static final int icon = 2130837508;
        public static final int lesson1 = 2130837509;
        public static final int lesson2 = 2130837510;
        public static final int lesson2_1 = 2130837511;
        public static final int lesson2_2 = 2130837512;
        public static final int lesson2_3 = 2130837513;
        public static final int lesson2_4 = 2130837514;
        public static final int lesson2_5 = 2130837515;
        public static final int lesson2_6 = 2130837516;
        public static final int lesson2_7 = 2130837517;
        public static final int lesson2_8 = 2130837518;
        public static final int lesson3 = 2130837519;
        public static final int lesson4 = 2130837520;
        public static final int lesson5 = 2130837521;
        public static final int lesson6 = 2130837522;
        public static final int point = 2130837523;
        public static final int title = 2130837524;
    }

    public static final class id {
        public static final int MainLayout = 2131296259;
        public static final int admakerview = 2131296266;
        public static final int bt1 = 2131296268;
        public static final int bt2 = 2131296269;
        public static final int custom_dialog_ok = 2131296258;
        public static final int custom_dialog_text = 2131296257;
        public static final int custom_dialog_title = 2131296256;
        public static final int imageView1 = 2131296267;
        public static final int ll1 = 2131296260;
        public static final int ll2 = 2131296262;
        public static final int seekbar = 2131296263;
        public static final int sv = 2131296261;
        public static final int timer = 2131296265;
        public static final int tv1 = 2131296264;
    }

    public static final class layout {
        public static final int custom_dialog = 2130903040;
        public static final int intro = 2130903041;
        public static final int lesson = 2130903042;
        public static final int top = 2130903043;
    }

    public static final class string {
        public static final int adurl = 2131165190;
        public static final int app_name = 2131165184;
        public static final int lesson1_title = 2131165186;
        public static final int lesson2_title = 2131165187;
        public static final int package_name = 2131165192;
        public static final int site_id = 2131165189;
        public static final int surfaceview_height = 2131165185;
        public static final int timer_format = 2131165191;
        public static final int zone_id = 2131165188;
    }

    public static final class style {
        public static final int Theme_CustomDialog = 2131230720;
    }
}
