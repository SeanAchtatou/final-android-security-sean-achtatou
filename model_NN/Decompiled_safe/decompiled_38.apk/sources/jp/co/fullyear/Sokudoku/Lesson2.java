package jp.co.fullyear.Sokudoku;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import java.util.Timer;
import jp.co.nobot.libAdMaker.libAdMaker;

public class Lesson2 extends Activity {
    static final int FP = -1;
    static final int WC = -2;
    private static final int count = 8;
    private static final int odds = 4;
    private static final int sleepNum = 40;
    private static final int sleepVal = 1;
    static int t = sleepVal;
    private libAdMaker AdMaker;
    /* access modifiers changed from: private */
    public int SurfaceHeight;
    /* access modifiers changed from: private */
    public int SurfaceWidth;
    private Canvas canvas;
    /* access modifiers changed from: private */
    public SurfaceHolder holder;
    private Bitmap[] imgs;
    Handler mHandler = new Handler();
    public double mLaptime;
    private Timer mTimer;
    /* access modifiers changed from: private */
    public int sleepLv;
    /* access modifiers changed from: private */
    public SurfaceView sv;
    /* access modifiers changed from: private */
    public Thread thread;
    private TextView timerTv;
    private TextView tv1;
    private String txt1 = "[0,1,2,3,4,5,6,7,8,9,10]";

    public void onDestroy() {
        super.onDestroy();
        this.AdMaker.destroy();
        this.AdMaker = null;
        for (int i = 0; i < this.imgs.length; i += sleepVal) {
            this.imgs[i].recycle();
        }
    }

    public void onRestart() {
        super.onRestart();
        this.AdMaker.start();
    }

    public void onStop() {
        super.onStop();
    }

    public void onPause() {
        super.onPause();
        finish();
    }

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        this.sleepLv = sleepVal;
        Resources r = getResources();
        this.imgs = new Bitmap[count];
        for (int i = 0; i < count; i += sleepVal) {
            this.imgs[i] = BitmapFactory.decodeResource(r, r.getIdentifier("lesson2_" + (i + sleepVal), "drawable", r.getString(R.string.package_name)));
        }
        setContentView((int) R.layout.lesson);
        this.sv = (SurfaceView) findViewById(R.id.sv);
        int footerHeight = getResources().getDimensionPixelSize(R.dimen.footer_height);
        Display display = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        this.SurfaceHeight = display.getHeight() - footerHeight;
        this.SurfaceWidth = display.getWidth();
        ((LinearLayout) findViewById(R.id.ll1)).setLayoutParams(new LinearLayout.LayoutParams((int) FP, this.SurfaceHeight));
        new SurfaceViewView(this);
        new CustomDialog(this, getString(R.string.lesson2_title), "視野拡大のトレーニングです。\n\n視点を中心に置いたまま、四角の角にある赤い印を追って追ってください。\n\nスマートフォンの本体を傾けることで、縦、横、斜め方向のトレーニングができます。\n\n自分のレベルに合った速さで30秒間行って下さい。\n\n画面下部のバーでスピード調節ができます。").show();
        SeekBar seekBar = (SeekBar) findViewById(R.id.seekbar);
        seekBar.setMax(10);
        seekBar.setProgress(this.sleepLv);
        this.tv1 = (TextView) findViewById(R.id.tv1);
        changeSleepLv(this.sleepLv);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onStartTrackingTouch(SeekBar seekBar) {
                Lesson2.this.sleepLv = Integer.valueOf(seekBar.getProgress()).intValue();
                Lesson2.this.changeSleepLv(Lesson2.this.sleepLv);
            }

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromTouch) {
                Lesson2.this.sleepLv = Integer.valueOf(progress).intValue();
                Lesson2.this.changeSleepLv(Lesson2.this.sleepLv);
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                Lesson2.this.sleepLv = Integer.valueOf(seekBar.getProgress()).intValue();
                Lesson2.this.changeSleepLv(Lesson2.this.sleepLv);
            }
        });
        this.AdMaker = (libAdMaker) findViewById(R.id.admakerview);
        this.AdMaker.siteId = getString(R.string.site_id);
        this.AdMaker.zoneId = getString(R.string.zone_id);
        this.AdMaker.setUrl(getString(R.string.adurl));
        this.AdMaker.start();
    }

    /* access modifiers changed from: package-private */
    public void changeSleepLv(int sleepLv2) {
        this.tv1.setText(String.valueOf(sleepLv2));
    }

    public class SurfaceViewView extends SurfaceView implements SurfaceHolder.Callback, Runnable {
        public SurfaceViewView(Context context) {
            super(context);
            Resources resources = getResources();
            Lesson2.this.holder = Lesson2.this.sv.getHolder();
            Lesson2.this.holder.addCallback(this);
            Lesson2.this.holder.setFixedSize(Lesson2.this.SurfaceWidth, Lesson2.this.SurfaceHeight);
        }

        public void surfaceCreated(SurfaceHolder holder) {
            Lesson2.this.thread = new Thread(this);
            Lesson2.this.thread.start();
        }

        public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        }

        public void surfaceDestroyed(SurfaceHolder holder) {
            Lesson2.this.thread = null;
        }

        public void run() {
            Lesson2.this.doDraw();
        }
    }

    public void doDraw() {
        int i = 0;
        while (this.thread != null) {
            if (this.sleepLv >= sleepVal) {
                if (t <= 0) {
                    t = sleepVal;
                }
                this.canvas = this.holder.lockCanvas();
                this.canvas.drawColor((int) FP);
                this.canvas.drawBitmap(this.imgs[i], 0.0f, 0.0f, (Paint) null);
                this.holder.unlockCanvasAndPost(this.canvas);
                i += sleepVal;
                if (i >= count) {
                    i = 0;
                }
            }
            try {
                Thread.sleep((long) ((sleepNum / this.sleepLv) * odds));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
