package jp.co.fullyear.Sokudoku;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import jp.co.nobot.libAdMaker.libAdMaker;

public class Top extends Activity implements View.OnClickListener {
    private libAdMaker AdMaker;
    private ImageView bt1;
    private ImageView bt2;
    private Button btHelp;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView((int) R.layout.top);
        this.bt1 = (ImageView) findViewById(R.id.bt1);
        this.bt1.setOnClickListener(this);
        this.bt2 = (ImageView) findViewById(R.id.bt2);
        this.bt2.setOnClickListener(this);
        this.AdMaker = (libAdMaker) findViewById(R.id.admakerview);
        this.AdMaker.siteId = getString(R.string.site_id);
        this.AdMaker.zoneId = getString(R.string.zone_id);
        this.AdMaker.setUrl(getString(R.string.adurl));
        this.AdMaker.start();
    }

    public void onDestroy() {
        super.onDestroy();
        this.AdMaker.destroy();
        this.AdMaker = null;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    public void onRestart() {
        super.onRestart();
        this.AdMaker.start();
    }

    public void onStop() {
        super.onStop();
    }

    public void onClick(View v) {
        if (v == this.bt1) {
            startActivity(new Intent(this, Lesson1.class));
        } else if (v == this.bt2) {
            startActivity(new Intent(this, Lesson2.class));
        }
    }
}
