package jp.co.fullyear.Sokudoku;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CustomDialog extends Dialog {
    private Button iButton = ((Button) findViewById(R.id.custom_dialog_ok));
    private TextView iText = ((TextView) findViewById(R.id.custom_dialog_text));
    private TextView iTitle = ((TextView) findViewById(R.id.custom_dialog_title));

    public CustomDialog(Context context, String title, String text) {
        super(context, R.style.Theme_CustomDialog);
        setContentView((int) R.layout.custom_dialog);
        this.iButton.setText("START");
        this.iTitle.setText(title);
        this.iText.setText(text);
        this.iButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CustomDialog.this.dismiss();
            }
        });
    }
}
