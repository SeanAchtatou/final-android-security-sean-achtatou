package com.openfeint.internal.resource;

public class ServerException extends Resource {
    public String exceptionClass;
    public String message;
    public boolean needsDeveloperAttention;

    public ServerException(String klass, String message2) {
        this.exceptionClass = klass;
        this.message = message2;
    }

    public ServerException() {
    }

    public static ResourceClass getResourceClass() {
        ResourceClass klass = new ResourceClass(ServerException.class, "exception") {
            public Resource factory() {
                return new ServerException();
            }
        };
        klass.mProperties.put("class", new StringResourceProperty() {
            public String get(Resource obj) {
                return ((ServerException) obj).exceptionClass;
            }

            public void set(Resource obj, String val) {
                ((ServerException) obj).exceptionClass = val;
            }
        });
        klass.mProperties.put("message", new StringResourceProperty() {
            public String get(Resource obj) {
                return ((ServerException) obj).message;
            }

            public void set(Resource obj, String val) {
                ((ServerException) obj).message = val;
            }
        });
        klass.mProperties.put("needs_developer_attention", new BooleanResourceProperty() {
            public boolean get(Resource obj) {
                return ((ServerException) obj).needsDeveloperAttention;
            }

            public void set(Resource obj, boolean val) {
                ((ServerException) obj).needsDeveloperAttention = val;
            }
        });
        return klass;
    }
}
