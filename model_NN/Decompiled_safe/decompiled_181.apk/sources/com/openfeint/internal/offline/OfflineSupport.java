package com.openfeint.internal.offline;

import android.content.Context;
import com.openfeint.api.Notification;
import com.openfeint.api.OpenFeint;
import com.openfeint.api.resource.Leaderboard;
import com.openfeint.api.resource.Score;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.Util;
import com.openfeint.internal.notifications.SimpleNotification;
import com.openfeint.internal.request.BaseRequest;
import com.openfeint.internal.request.OrderedArgList;
import com.openfeint.internal.resource.DateResourceProperty;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicBoolean;
import net.droidstick.finger.R;
import org.apache.commons.codec.binary.Hex;

public class OfflineSupport {
    private static final String TAG = "OfflineSupport";
    private static final String TEMPORARY_USER_ID = "0";
    /* access modifiers changed from: private */
    public static DB db = null;
    /* access modifiers changed from: private */
    public static AtomicBoolean updateInProgress = new AtomicBoolean(false);
    private static String userID = null;

    /* access modifiers changed from: private */
    public static boolean streq(String lhs, String rhs) {
        if (lhs == null) {
            return rhs == null;
        }
        return lhs.equals(rhs);
    }

    public static class OfflineAchievement {
        public float clientCompletionPercentage;
        public String resourceID;
        public float serverCompletionPercentage;
        public String timestamp;

        public OfflineAchievement dup() {
            OfflineAchievement rv = new OfflineAchievement();
            rv.resourceID = this.resourceID;
            rv.clientCompletionPercentage = this.clientCompletionPercentage;
            rv.serverCompletionPercentage = this.serverCompletionPercentage;
            rv.timestamp = this.timestamp;
            return rv;
        }

        public boolean eq(OfflineAchievement rhs) {
            return OfflineSupport.streq(this.resourceID, rhs.resourceID) && this.clientCompletionPercentage == rhs.clientCompletionPercentage && this.serverCompletionPercentage == rhs.serverCompletionPercentage && OfflineSupport.streq(this.timestamp, rhs.timestamp);
        }
    }

    public static class OfflineScore {
        public String blobFileName;
        public String customData;
        public String displayText;
        public String leaderboardID;
        public long score;
        public String timestamp;

        public OfflineScore dup() {
            OfflineScore rv = new OfflineScore();
            rv.leaderboardID = this.leaderboardID;
            rv.score = this.score;
            rv.displayText = this.displayText;
            rv.customData = this.customData;
            rv.blobFileName = this.blobFileName;
            rv.timestamp = this.timestamp;
            return rv;
        }

        public boolean eq(OfflineScore rhs) {
            return OfflineSupport.streq(this.leaderboardID, rhs.leaderboardID) && this.score == rhs.score && OfflineSupport.streq(this.displayText, rhs.displayText) && OfflineSupport.streq(this.customData, rhs.customData) && OfflineSupport.streq(this.blobFileName, rhs.blobFileName) && OfflineSupport.streq(this.timestamp, rhs.timestamp);
        }
    }

    public static class DB {
        private static final int STREAM_VERSION = 0;
        public ArrayList<OfflineAchievement> achievements = new ArrayList<>();
        public ArrayList<OfflineScore> scores = new ArrayList<>();

        public DB dup() {
            DB rv = new DB();
            Iterator<OfflineScore> it = this.scores.iterator();
            while (it.hasNext()) {
                rv.scores.add(it.next().dup());
            }
            Iterator<OfflineAchievement> it2 = this.achievements.iterator();
            while (it2.hasNext()) {
                rv.achievements.add(it2.next().dup());
            }
            return rv;
        }

        public void merge(DB newUserDB) {
            Iterator<OfflineAchievement> it = newUserDB.achievements.iterator();
            while (it.hasNext()) {
                OfflineAchievement newUserAchievement = it.next();
                OfflineAchievement currentUserAchievement = findAchievement(newUserAchievement.resourceID);
                if (currentUserAchievement == null) {
                    this.achievements.add(newUserAchievement.dup());
                } else {
                    if (currentUserAchievement.clientCompletionPercentage < newUserAchievement.clientCompletionPercentage) {
                        currentUserAchievement.clientCompletionPercentage = newUserAchievement.clientCompletionPercentage;
                        currentUserAchievement.timestamp = newUserAchievement.timestamp;
                    }
                    currentUserAchievement.serverCompletionPercentage = Math.max(currentUserAchievement.serverCompletionPercentage, newUserAchievement.serverCompletionPercentage);
                }
            }
            this.scores.addAll(newUserDB.scores);
        }

        public void updateOnUpload(DB otherDB) {
            Iterator<OfflineAchievement> it = otherDB.achievements.iterator();
            while (it.hasNext()) {
                OfflineAchievement otherAchievement = it.next();
                OfflineAchievement myAchievement = findAchievement(otherAchievement.resourceID);
                if (myAchievement == null) {
                    myAchievement = otherAchievement.dup();
                    this.achievements.add(myAchievement);
                }
                myAchievement.serverCompletionPercentage = Math.max(myAchievement.serverCompletionPercentage, otherAchievement.clientCompletionPercentage);
            }
            ArrayList<OfflineScore> oldScores = this.scores;
            this.scores = new ArrayList<>();
            Iterator<OfflineScore> it2 = oldScores.iterator();
            while (it2.hasNext()) {
                OfflineScore myScore = it2.next();
                if (myScore.blobFileName != null) {
                    this.scores.add(myScore);
                } else {
                    boolean found = false;
                    Iterator<OfflineScore> it3 = otherDB.scores.iterator();
                    while (true) {
                        if (it3.hasNext()) {
                            if (myScore.eq(it3.next())) {
                                found = true;
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                    if (!found) {
                        this.scores.add(myScore);
                    }
                }
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:15:0x0061 A[SYNTHETIC, Splitter:B:15:0x0061] */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x00df A[SYNTHETIC, Splitter:B:33:0x00df] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public static com.openfeint.internal.offline.OfflineSupport.DB load(java.lang.String r15) {
            /*
                com.openfeint.internal.offline.OfflineSupport$DB r7 = new com.openfeint.internal.offline.OfflineSupport$DB
                r7.<init>()
                r5 = 0
                if (r15 == 0) goto L_0x0064
                com.openfeint.internal.OpenFeintInternal r10 = com.openfeint.internal.OpenFeintInternal.getInstance()     // Catch:{ Exception -> 0x00ef }
                android.content.Context r10 = r10.getContext()     // Catch:{ Exception -> 0x00ef }
                java.io.FileInputStream r10 = r10.openFileInput(r15)     // Catch:{ Exception -> 0x00ef }
                javax.crypto.CipherInputStream r2 = com.openfeint.internal.Encryption.decryptionWrap(r10)     // Catch:{ Exception -> 0x00ef }
                java.io.ObjectInputStream r6 = new java.io.ObjectInputStream     // Catch:{ Exception -> 0x00ef }
                r6.<init>(r2)     // Catch:{ Exception -> 0x00ef }
                int r9 = r6.readInt()     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                switch(r9) {
                    case 0: goto L_0x0065;
                    default: goto L_0x0024;
                }     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
            L_0x0024:
                java.lang.Exception r10 = new java.lang.Exception     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                java.lang.String r11 = "Unrecognized stream version %d"
                r12 = 1
                java.lang.Object[] r12 = new java.lang.Object[r12]     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                r13 = 0
                java.lang.Integer r14 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                r12[r13] = r14     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                java.lang.String r11 = java.lang.String.format(r11, r12)     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                r10.<init>(r11)     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                throw r10     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
            L_0x003a:
                r10 = move-exception
                r1 = r10
                r5 = r6
            L_0x003d:
                java.lang.String r10 = "OfflineSupport"
                java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x00dc }
                java.lang.String r12 = "Couldn't load offline achievements - "
                r11.<init>(r12)     // Catch:{ all -> 0x00dc }
                java.lang.String r12 = r1.getMessage()     // Catch:{ all -> 0x00dc }
                java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ all -> 0x00dc }
                java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x00dc }
                com.openfeint.internal.OpenFeintInternal.log(r10, r11)     // Catch:{ all -> 0x00dc }
                java.util.ArrayList<com.openfeint.internal.offline.OfflineSupport$OfflineAchievement> r10 = r7.achievements     // Catch:{ all -> 0x00dc }
                r10.clear()     // Catch:{ all -> 0x00dc }
                java.util.ArrayList<com.openfeint.internal.offline.OfflineSupport$OfflineScore> r10 = r7.scores     // Catch:{ all -> 0x00dc }
                r10.clear()     // Catch:{ all -> 0x00dc }
                if (r5 == 0) goto L_0x0064
                r5.close()     // Catch:{ IOException -> 0x00e7 }
            L_0x0064:
                return r7
            L_0x0065:
                int r3 = r6.readInt()     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
            L_0x0069:
                if (r3 > 0) goto L_0x0078
                int r4 = r6.readInt()     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
            L_0x006f:
                if (r4 > 0) goto L_0x00a1
                if (r6 == 0) goto L_0x00f3
                r6.close()     // Catch:{ IOException -> 0x00e3 }
                r5 = r6
                goto L_0x0064
            L_0x0078:
                com.openfeint.internal.offline.OfflineSupport$OfflineAchievement r0 = new com.openfeint.internal.offline.OfflineSupport$OfflineAchievement     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                r0.<init>()     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                java.lang.Object r15 = r6.readObject()     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                java.lang.String r15 = (java.lang.String) r15     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                r0.resourceID = r15     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                float r10 = r6.readFloat()     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                r0.clientCompletionPercentage = r10     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                float r10 = r6.readFloat()     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                r0.serverCompletionPercentage = r10     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                java.lang.Object r15 = r6.readObject()     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                java.lang.String r15 = (java.lang.String) r15     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                r0.timestamp = r15     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                java.util.ArrayList<com.openfeint.internal.offline.OfflineSupport$OfflineAchievement> r10 = r7.achievements     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                r10.add(r0)     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                int r3 = r3 + -1
                goto L_0x0069
            L_0x00a1:
                com.openfeint.internal.offline.OfflineSupport$OfflineScore r8 = new com.openfeint.internal.offline.OfflineSupport$OfflineScore     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                r8.<init>()     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                java.lang.Object r15 = r6.readObject()     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                java.lang.String r15 = (java.lang.String) r15     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                r8.leaderboardID = r15     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                long r10 = r6.readLong()     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                r8.score = r10     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                java.lang.Object r15 = r6.readObject()     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                java.lang.String r15 = (java.lang.String) r15     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                r8.displayText = r15     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                java.lang.Object r15 = r6.readObject()     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                java.lang.String r15 = (java.lang.String) r15     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                r8.customData = r15     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                java.lang.Object r15 = r6.readObject()     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                java.lang.String r15 = (java.lang.String) r15     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                r8.blobFileName = r15     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                java.lang.Object r15 = r6.readObject()     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                java.lang.String r15 = (java.lang.String) r15     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                r8.timestamp = r15     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                java.util.ArrayList<com.openfeint.internal.offline.OfflineSupport$OfflineScore> r10 = r7.scores     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                r10.add(r8)     // Catch:{ Exception -> 0x003a, all -> 0x00ec }
                int r4 = r4 + -1
                goto L_0x006f
            L_0x00dc:
                r10 = move-exception
            L_0x00dd:
                if (r5 == 0) goto L_0x00e2
                r5.close()     // Catch:{ IOException -> 0x00ea }
            L_0x00e2:
                throw r10
            L_0x00e3:
                r10 = move-exception
                r5 = r6
                goto L_0x0064
            L_0x00e7:
                r10 = move-exception
                goto L_0x0064
            L_0x00ea:
                r11 = move-exception
                goto L_0x00e2
            L_0x00ec:
                r10 = move-exception
                r5 = r6
                goto L_0x00dd
            L_0x00ef:
                r10 = move-exception
                r1 = r10
                goto L_0x003d
            L_0x00f3:
                r5 = r6
                goto L_0x0064
            */
            throw new UnsupportedOperationException("Method not decompiled: com.openfeint.internal.offline.OfflineSupport.DB.load(java.lang.String):com.openfeint.internal.offline.OfflineSupport$DB");
        }

        /* JADX WARNING: Removed duplicated region for block: B:35:0x00a8 A[SYNTHETIC, Splitter:B:35:0x00a8] */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x00ad A[SYNTHETIC, Splitter:B:38:0x00ad] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void save(java.lang.String r9) {
            /*
                r8 = this;
                r1 = 0
                r3 = 0
                com.openfeint.internal.OpenFeintInternal r5 = com.openfeint.internal.OpenFeintInternal.getInstance()     // Catch:{ Exception -> 0x00be, all -> 0x00bc }
                android.content.Context r5 = r5.getContext()     // Catch:{ Exception -> 0x00be, all -> 0x00bc }
                r6 = 0
                java.io.FileOutputStream r5 = r5.openFileOutput(r9, r6)     // Catch:{ Exception -> 0x00be, all -> 0x00bc }
                javax.crypto.CipherOutputStream r3 = com.openfeint.internal.Encryption.encryptionWrap(r5)     // Catch:{ Exception -> 0x00be, all -> 0x00bc }
                java.io.ObjectOutputStream r2 = new java.io.ObjectOutputStream     // Catch:{ Exception -> 0x00be, all -> 0x00bc }
                r2.<init>(r3)     // Catch:{ Exception -> 0x00be, all -> 0x00bc }
                r5 = 0
                r2.writeInt(r5)     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                java.util.ArrayList<com.openfeint.internal.offline.OfflineSupport$OfflineAchievement> r5 = r8.achievements     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                int r5 = r5.size()     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                r2.writeInt(r5)     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                java.util.ArrayList<com.openfeint.internal.offline.OfflineSupport$OfflineAchievement> r5 = r8.achievements     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                java.util.Iterator r5 = r5.iterator()     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
            L_0x002b:
                boolean r6 = r5.hasNext()     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                if (r6 != 0) goto L_0x0055
                java.util.ArrayList<com.openfeint.internal.offline.OfflineSupport$OfflineScore> r5 = r8.scores     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                int r5 = r5.size()     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                r2.writeInt(r5)     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                java.util.ArrayList<com.openfeint.internal.offline.OfflineSupport$OfflineScore> r5 = r8.scores     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                java.util.Iterator r5 = r5.iterator()     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
            L_0x0040:
                boolean r6 = r5.hasNext()     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                if (r6 != 0) goto L_0x007f
                r2.close()     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                if (r2 == 0) goto L_0x004e
                r2.close()     // Catch:{ IOException -> 0x00ba }
            L_0x004e:
                if (r3 == 0) goto L_0x00c0
                r3.close()     // Catch:{ IOException -> 0x00b1 }
                r1 = r2
            L_0x0054:
                return
            L_0x0055:
                java.lang.Object r0 = r5.next()     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                com.openfeint.internal.offline.OfflineSupport$OfflineAchievement r0 = (com.openfeint.internal.offline.OfflineSupport.OfflineAchievement) r0     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                java.lang.String r6 = r0.resourceID     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                r2.writeObject(r6)     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                float r6 = r0.clientCompletionPercentage     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                r2.writeFloat(r6)     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                float r6 = r0.serverCompletionPercentage     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                r2.writeFloat(r6)     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                java.lang.String r6 = r0.timestamp     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                r2.writeObject(r6)     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                goto L_0x002b
            L_0x0070:
                r5 = move-exception
                r1 = r2
            L_0x0072:
                if (r1 == 0) goto L_0x0077
                r1.close()     // Catch:{ IOException -> 0x00b4 }
            L_0x0077:
                if (r3 == 0) goto L_0x0054
                r3.close()     // Catch:{ IOException -> 0x007d }
                goto L_0x0054
            L_0x007d:
                r5 = move-exception
                goto L_0x0054
            L_0x007f:
                java.lang.Object r4 = r5.next()     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                com.openfeint.internal.offline.OfflineSupport$OfflineScore r4 = (com.openfeint.internal.offline.OfflineSupport.OfflineScore) r4     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                java.lang.String r6 = r4.leaderboardID     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                r2.writeObject(r6)     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                long r6 = r4.score     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                r2.writeLong(r6)     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                java.lang.String r6 = r4.displayText     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                r2.writeObject(r6)     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                java.lang.String r6 = r4.customData     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                r2.writeObject(r6)     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                java.lang.String r6 = r4.blobFileName     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                r2.writeObject(r6)     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                java.lang.String r6 = r4.timestamp     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                r2.writeObject(r6)     // Catch:{ Exception -> 0x0070, all -> 0x00a4 }
                goto L_0x0040
            L_0x00a4:
                r5 = move-exception
                r1 = r2
            L_0x00a6:
                if (r1 == 0) goto L_0x00ab
                r1.close()     // Catch:{ IOException -> 0x00b6 }
            L_0x00ab:
                if (r3 == 0) goto L_0x00b0
                r3.close()     // Catch:{ IOException -> 0x00b8 }
            L_0x00b0:
                throw r5
            L_0x00b1:
                r5 = move-exception
                r1 = r2
                goto L_0x0054
            L_0x00b4:
                r5 = move-exception
                goto L_0x0077
            L_0x00b6:
                r6 = move-exception
                goto L_0x00ab
            L_0x00b8:
                r6 = move-exception
                goto L_0x00b0
            L_0x00ba:
                r5 = move-exception
                goto L_0x004e
            L_0x00bc:
                r5 = move-exception
                goto L_0x00a6
            L_0x00be:
                r5 = move-exception
                goto L_0x0072
            L_0x00c0:
                r1 = r2
                goto L_0x0054
            */
            throw new UnsupportedOperationException("Method not decompiled: com.openfeint.internal.offline.OfflineSupport.DB.save(java.lang.String):void");
        }

        public void removeReferencedBlobs() {
            Iterator<OfflineScore> it = this.scores.iterator();
            while (it.hasNext()) {
                OfflineSupport.deleteDataFile(it.next().blobFileName);
            }
        }

        public void clear() {
            this.achievements.clear();
            this.scores.clear();
        }

        public OfflineAchievement findAchievement(String resourceID) {
            Iterator<OfflineAchievement> it = this.achievements.iterator();
            while (it.hasNext()) {
                OfflineAchievement a = it.next();
                if (a.resourceID.equals(resourceID)) {
                    return a;
                }
            }
            return null;
        }
    }

    /* access modifiers changed from: private */
    public static void deleteDataFile(String fileName) {
        try {
            Context context = OpenFeintInternal.getInstance().getContext();
            context.deleteFile(context.getFileStreamPath(fileName).getPath());
        } catch (Exception e) {
        }
    }

    private static String fullPath(String fileName) {
        return OpenFeintInternal.getInstance().getContext().getFileStreamPath(fileName).getPath();
    }

    private static String filename(String forUserID) {
        if (forUserID == null) {
            return null;
        }
        String appID = OpenFeintInternal.getInstance().getAppID();
        if (appID == null) {
            return null;
        }
        return "of.offline." + forUserID + "." + appID;
    }

    private static String now() {
        return DateResourceProperty.sDateParser.format(new Date());
    }

    public static void setUserDeclined() {
        setUserID(null);
    }

    public static void setUserTemporary() {
        setUserID(TEMPORARY_USER_ID);
    }

    private static boolean isUserTemporary() {
        return TEMPORARY_USER_ID.equals(userID);
    }

    private static void removeDBForUser(String forUserID) {
        deleteDataFile(filename(forUserID));
    }

    public static void setUserID(String newUserID) {
        if (newUserID == null || !newUserID.equals(userID)) {
            DB newUserDB = DB.load(filename(newUserID));
            if (isUserTemporary()) {
                db.merge(newUserDB);
                removeDBForUser(TEMPORARY_USER_ID);
            } else {
                db = newUserDB;
            }
            userID = newUserID;
            trySubmitOfflineData();
        }
    }

    public static void trySubmitOfflineData() {
        if (userID != null && !userID.equals(TEMPORARY_USER_ID) && OpenFeint.isUserLoggedIn()) {
            updateToServer();
        }
    }

    static final void removeAndUploadNext(OfflineScore os) {
        db.scores.remove(os);
        save();
        uploadScoresWithBlobs();
    }

    /* access modifiers changed from: private */
    public static void uploadScoresWithBlobs() {
        Iterator<OfflineScore> it = db.scores.iterator();
        while (it.hasNext()) {
            final OfflineScore os = it.next();
            if (os.blobFileName != null) {
                Score s = new Score(os.score);
                s.customData = os.customData;
                s.displayText = os.displayText;
                try {
                    s.blob = Util.readWholeFile(fullPath(os.blobFileName));
                } catch (IOException e) {
                }
                if (s.blob == null) {
                    removeAndUploadNext(os);
                    return;
                } else {
                    s.submitToFromOffline(new Leaderboard(os.leaderboardID), os.timestamp, new Score.SubmitToCB() {
                        public void onSuccess(boolean newHighScore) {
                            OfflineSupport.removeAndUploadNext(OfflineScore.this);
                        }

                        public void onFailure(String failureMessage) {
                            OfflineSupport.updateInProgress.set(false);
                        }
                    });
                    return;
                }
            }
        }
        updateInProgress.set(false);
    }

    private static synchronized void updateToServer() {
        synchronized (OfflineSupport.class) {
            if (updateInProgress.compareAndSet(false, true)) {
                final DB clonedDB = db.dup();
                OrderedArgList oal = new OrderedArgList();
                int currAchievement = 0;
                Iterator<OfflineAchievement> it = clonedDB.achievements.iterator();
                while (it.hasNext()) {
                    OfflineAchievement oa = it.next();
                    if (oa.clientCompletionPercentage != oa.serverCompletionPercentage) {
                        OpenFeintInternal.log(TAG, String.format("Updating achievement %s from known %f to %f completion", oa.resourceID, Float.valueOf(oa.serverCompletionPercentage), Float.valueOf(oa.clientCompletionPercentage)));
                        oal.put(String.format("achievements[%d][id]", Integer.valueOf(currAchievement)), oa.resourceID);
                        oal.put(String.format("achievements[%d][percent_complete]", Integer.valueOf(currAchievement)), Float.toString(oa.clientCompletionPercentage));
                        oal.put(String.format("achievements[%d][timestamp]", Integer.valueOf(currAchievement)), oa.timestamp);
                        currAchievement++;
                    }
                }
                int currScore = 0;
                Iterator<OfflineScore> it2 = clonedDB.scores.iterator();
                while (it2.hasNext()) {
                    OfflineScore os = it2.next();
                    if (os.blobFileName == null) {
                        OpenFeintInternal.log(TAG, String.format("Posting score %d to leaderboard %s", Long.valueOf(os.score), os.leaderboardID));
                        oal.put(String.format("high_scores[%d][leaderboard_id]", Integer.valueOf(currScore)), os.leaderboardID);
                        oal.put(String.format("high_scores[%d][score]", Integer.valueOf(currScore)), Long.toString(os.score));
                        if (os.displayText != null) {
                            oal.put(String.format("high_scores[%d][display_text]", Integer.valueOf(currScore)), os.displayText);
                        }
                        if (os.customData != null) {
                            oal.put(String.format("high_scores[%d][custom_data]", Integer.valueOf(currScore)), os.customData);
                        }
                        oal.put(String.format("high_scores[%d][timestamp]", Integer.valueOf(currScore)), os.timestamp);
                        currScore++;
                    }
                }
                if (currAchievement == 0 && currScore == 0) {
                    uploadScoresWithBlobs();
                } else {
                    final String path = "/xp/games/" + OpenFeintInternal.getInstance().getAppID() + "/offline_syncs";
                    new BaseRequest(oal) {
                        public String method() {
                            return "POST";
                        }

                        public String path() {
                            return path;
                        }

                        public void onResponse(int responseCode, byte[] body) {
                        }

                        public void onResponseOffMainThread(int responseCode, byte[] body) {
                            if (200 > responseCode || responseCode >= 300) {
                                if (responseCode != 0 && 500 > responseCode) {
                                    OfflineSupport.db.removeReferencedBlobs();
                                    OfflineSupport.db.clear();
                                    OfflineSupport.save();
                                }
                                OfflineSupport.updateInProgress.set(false);
                                return;
                            }
                            OfflineSupport.db.updateOnUpload(clonedDB);
                            OfflineSupport.save();
                            OfflineSupport.uploadScoresWithBlobs();
                        }
                    }.launch();
                }
            }
        }
    }

    public static void updateClientCompletionPercentage(String resourceID, float completionPercentage) {
        if (userID != null) {
            OfflineAchievement a = db.findAchievement(resourceID);
            if (a == null) {
                OfflineAchievement a2 = new OfflineAchievement();
                a2.resourceID = resourceID;
                a2.serverCompletionPercentage = 0.0f;
                a2.clientCompletionPercentage = completionPercentage;
                a2.timestamp = now();
                db.achievements.add(a2);
                save();
            } else if (a.clientCompletionPercentage < completionPercentage) {
                a.clientCompletionPercentage = completionPercentage;
                a.timestamp = now();
                save();
            }
        }
    }

    public static void updateServerCompletionPercentage(String resourceID, float completionPercentage) {
        if (userID != null) {
            OfflineAchievement a = db.findAchievement(resourceID);
            if (a == null) {
                a = new OfflineAchievement();
                a.resourceID = resourceID;
                a.clientCompletionPercentage = completionPercentage;
                db.achievements.add(a);
            }
            a.serverCompletionPercentage = completionPercentage;
            a.timestamp = now();
            save();
        }
    }

    public static float getClientCompletionPercentage(String resourceID) {
        OfflineAchievement a = null;
        try {
            a = db.findAchievement(resourceID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (a == null) {
            return 0.0f;
        }
        return a.clientCompletionPercentage;
    }

    public static void postOfflineScore(Score s, Leaderboard l) {
        if (userID != null) {
            OfflineScore os = new OfflineScore();
            os.leaderboardID = l.resourceID();
            os.score = s.score;
            os.displayText = s.displayText;
            os.customData = s.customData;
            os.timestamp = now();
            if (s.blob != null) {
                OfflineScore existingScore = null;
                Iterator<OfflineScore> it = db.scores.iterator();
                while (true) {
                    if (it.hasNext()) {
                        OfflineScore scan = it.next();
                        if (os.leaderboardID.equals(scan.leaderboardID)) {
                            existingScore = scan;
                            break;
                        }
                    } else {
                        break;
                    }
                }
                if (existingScore != null) {
                    if (l.allowsWorseScores || ((l.descendingSortOrder && existingScore.score < s.score) || (!l.descendingSortOrder && existingScore.score > s.score))) {
                        deleteDataFile(existingScore.blobFileName);
                        db.scores.remove(existingScore);
                    } else {
                        return;
                    }
                }
                String filename = "unknown.blob";
                try {
                    MessageDigest md = MessageDigest.getInstance("SHA1");
                    md.update(s.blob);
                    filename = String.format("%s.blob", new String(Hex.encodeHex(md.digest())));
                } catch (NoSuchAlgorithmException e) {
                }
                try {
                    Util.saveFile(s.blob, fullPath(filename));
                    os.blobFileName = filename;
                } catch (IOException e2) {
                    return;
                }
            }
            db.scores.add(os);
            save();
            if (!isUserTemporary()) {
                SimpleNotification.show(OpenFeintInternal.getRString(R.string.of_score_submitted_notification), "@drawable/of_icon_highscore_notification", Notification.Category.HighScore, Notification.Type.Success);
            }
        }
    }

    /* access modifiers changed from: private */
    public static void save() {
        String fileName = filename(userID);
        if (fileName != null) {
            db.save(fileName);
        }
    }
}
