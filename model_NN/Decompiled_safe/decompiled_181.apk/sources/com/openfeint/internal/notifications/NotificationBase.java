package com.openfeint.internal.notifications;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.Toast;
import com.openfeint.api.Notification;
import com.openfeint.internal.OpenFeintInternal;
import java.util.HashMap;
import java.util.Map;

public abstract class NotificationBase extends Notification {
    static Map<String, Drawable> drawableCache = new HashMap();
    private Notification.Category category;
    View displayView;
    protected String imageName;
    private String text;
    Toast toast;
    private Notification.Type type;
    private Map<String, Object> userData;

    /* access modifiers changed from: protected */
    public abstract boolean createView();

    /* access modifiers changed from: protected */
    public abstract void drawView(Canvas canvas);

    public String getText() {
        return this.text;
    }

    public Notification.Category getCategory() {
        return this.category;
    }

    public Notification.Type getType() {
        return this.type;
    }

    public Map<String, Object> getUserData() {
        return this.userData;
    }

    protected NotificationBase(String _text, String _imageName, Notification.Category _cat, Notification.Type _type, Map<String, Object> _userData) {
        this.text = _text;
        this.imageName = _imageName;
        this.category = _cat;
        this.type = _type;
        this.userData = _userData;
    }

    /* access modifiers changed from: protected */
    public String clippedText(Paint paint, String text2, int length) {
        String outText = text2;
        int endLength = paint.breakText(text2, true, (float) length, null);
        if (endLength < outText.length()) {
            return String.valueOf(outText.substring(0, endLength - 1)) + "...";
        }
        return outText;
    }

    /* access modifiers changed from: protected */
    public void showToast() {
        OpenFeintInternal.getInstance().runOnUiThread(new Runnable() {
            public void run() {
                Context appContext = OpenFeintInternal.getInstance().getContext();
                NotificationBase.this.toast = new Toast(appContext);
                NotificationBase.this.toast.setGravity(80, 0, 0);
                NotificationBase.this.toast.setDuration(1);
                NotificationBase.this.toast.setView(NotificationBase.this.displayView);
                NotificationBase.this.toast.show();
            }
        });
    }

    static Drawable getResourceDrawable(String name) {
        if (!drawableCache.containsKey(name)) {
            OpenFeintInternal ofi = OpenFeintInternal.getInstance();
            int bitmapHandle = ofi.getResource(name);
            if (bitmapHandle == 0) {
                drawableCache.put(name, null);
            } else {
                drawableCache.put(name, ofi.getContext().getResources().getDrawable(bitmapHandle));
            }
        }
        return drawableCache.get(name);
    }

    /* access modifiers changed from: protected */
    public void checkDelegateAndView() {
        if (getDelegate().canShowNotification(this)) {
            getDelegate().notificationWillShow(this);
            if (createView()) {
                showToast();
                return;
            }
            return;
        }
        getDelegate().displayNotification(this);
    }
}
