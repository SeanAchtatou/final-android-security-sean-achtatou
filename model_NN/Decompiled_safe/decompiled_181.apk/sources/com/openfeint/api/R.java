package com.openfeint.api;

public final class R {

    public static final class array {
        public static final int entries_list_preference = 2131296256;
        public static final int entries_paddle_preference = 2131296258;
        public static final int entryvalues_list_preference = 2131296257;
        public static final int entryvalues_paddle_preference = 2131296259;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int dark_green = 2131099652;
        public static final int of_transparent = 2131099648;
        public static final int solid_blue = 2131099650;
        public static final int solid_green = 2131099651;
        public static final int solid_red = 2131099649;
        public static final int solid_white = 2131099654;
        public static final int solid_yellow = 2131099653;
    }

    public static final class drawable {
        public static final int blue = 2130837562;
        public static final int button01_focused = 2130837504;
        public static final int button01_idle = 2130837505;
        public static final int button01_pressed = 2130837506;
        public static final int button_bg = 2130837507;
        public static final int frame_layout_shape = 2130837508;
        public static final int green = 2130837563;
        public static final int heyzap_button = 2130837509;
        public static final int icon = 2130837510;
        public static final int icon_ad_btb = 2130837511;
        public static final int icon_ad_btb_002 = 2130837512;
        public static final int icon_ad_click = 2130837513;
        public static final int icon_ad_cpingpong = 2130837514;
        public static final int icon_ad_cpingpong_002 = 2130837515;
        public static final int icon_ad_dsg = 2130837516;
        public static final int icon_ad_dsg2 = 2130837517;
        public static final int icon_ad_dsg2_002 = 2130837518;
        public static final int icon_ad_dsg_002 = 2130837519;
        public static final int icon_ad_finger = 2130837520;
        public static final int icon_ad_paddlebounce = 2130837521;
        public static final int icon_ad_paddlebounce_002 = 2130837522;
        public static final int icon_ad_shuriken = 2130837523;
        public static final int icon_ad_stomp = 2130837524;
        public static final int img_hand_00 = 2130837525;
        public static final int img_hand_01 = 2130837526;
        public static final int img_hand_02 = 2130837527;
        public static final int img_hand_03 = 2130837528;
        public static final int img_tut01 = 2130837529;
        public static final int img_tut02 = 2130837530;
        public static final int of_achievement_icon_frame = 2130837531;
        public static final int of_achievement_icon_locked = 2130837532;
        public static final int of_achievement_icon_unlocked = 2130837533;
        public static final int of_achievement_notification_bkg = 2130837534;
        public static final int of_achievement_notification_locked = 2130837535;
        public static final int of_feint_points_white = 2130837536;
        public static final int of_icon_dashboard_exit = 2130837537;
        public static final int of_icon_dashboard_home = 2130837538;
        public static final int of_icon_dashboard_settings = 2130837539;
        public static final int of_icon_highscore_notification = 2130837540;
        public static final int of_ll_logo = 2130837541;
        public static final int of_native_loader = 2130837542;
        public static final int of_native_loader_frame = 2130837543;
        public static final int of_native_loader_leaf = 2130837544;
        public static final int of_native_loader_progress = 2130837545;
        public static final int of_native_loader_progress_01 = 2130837546;
        public static final int of_native_loader_progress_02 = 2130837547;
        public static final int of_native_loader_progress_03 = 2130837548;
        public static final int of_native_loader_progress_04 = 2130837549;
        public static final int of_native_loader_progress_05 = 2130837550;
        public static final int of_native_loader_progress_06 = 2130837551;
        public static final int of_native_loader_progress_07 = 2130837552;
        public static final int of_native_loader_progress_08 = 2130837553;
        public static final int of_native_loader_progress_09 = 2130837554;
        public static final int of_native_loader_progress_10 = 2130837555;
        public static final int of_native_loader_progress_11 = 2130837556;
        public static final int of_native_loader_progress_12 = 2130837557;
        public static final int of_notification_bkg = 2130837558;
        public static final int pid = 2130837559;
        public static final int red = 2130837561;
        public static final int screen_background_black = 2130837565;
        public static final int took = 2130837560;
        public static final int translucent_background = 2130837566;
        public static final int transparent_background = 2130837567;
        public static final int yellow = 2130837564;
    }

    public static final class id {
        public static final int adviewmain = 2131427368;
        public static final int adviewmore = 2131427328;
        public static final int bottom_ui_container = 2131427358;
        public static final int button_heyzap = 2131427372;
        public static final int button_moregames_back = 2131427331;
        public static final int button_postscore_heyzap = 2131427369;
        public static final int button_resume_game = 2131427366;
        public static final int button_retry = 2131427367;
        public static final int exit_feint = 2131427375;
        public static final int frameLayout = 2131427344;
        public static final int gameover_blackbar_container = 2131427362;
        public static final int home = 2131427373;
        public static final int howto_blackbar_container = 2131427354;
        public static final int id_button_more = 2131427371;
        public static final int id_button_settings = 2131427370;
        public static final int id_button_startgame = 2131427365;
        public static final int list_icon = 2131427332;
        public static final int list_moregames = 2131427330;
        public static final int list_text = 2131427333;
        public static final int list_text2 = 2131427334;
        public static final int loading_blackbar_container = 2131427352;
        public static final int lunar = 2131427351;
        public static final int nested_window_root = 2131427343;
        public static final int of_achievement_icon = 2131427336;
        public static final int of_achievement_icon_frame = 2131427337;
        public static final int of_achievement_notification = 2131427335;
        public static final int of_achievement_progress_icon = 2131427341;
        public static final int of_achievement_score = 2131427339;
        public static final int of_achievement_score_icon = 2131427340;
        public static final int of_achievement_text = 2131427338;
        public static final int of_icon = 2131427347;
        public static final int of_ll_logo_image = 2131427346;
        public static final int of_text = 2131427348;
        public static final int of_text1 = 2131427349;
        public static final int of_text2 = 2131427350;
        public static final int pause_blackbar_layout = 2131427360;
        public static final int progress = 2131427342;
        public static final int settings = 2131427374;
        public static final int text_combo = 2131427364;
        public static final int text_gameover = 2131427363;
        public static final int text_howto_00 = 2131427355;
        public static final int text_howto_01 = 2131427356;
        public static final int text_howto_02 = 2131427357;
        public static final int text_loading = 2131427353;
        public static final int textview_click_to_view = 2131427329;
        public static final int textview_game_paused_title = 2131427361;
        public static final int title_button_container = 2131427359;
        public static final int web_view = 2131427345;
    }

    public static final class layout {
        public static final int about = 2130903040;
        public static final int linear_layout_9 = 2130903041;
        public static final int list_item_icon_text = 2130903042;
        public static final int of_achievement_notification = 2130903043;
        public static final int of_native_loader = 2130903044;
        public static final int of_nested_window = 2130903045;
        public static final int of_simple_notification = 2130903046;
        public static final int of_two_line_notification = 2130903047;
        public static final int surface_view_overlay = 2130903048;
    }

    public static final class menu {
        public static final int of_dashboard = 2131361792;
    }

    public static final class raw {
        public static final int grab = 2131034112;
        public static final int pid = 2131034113;
        public static final int took = 2131034114;
    }

    public static final class string {
        public static final int Back = 2131165256;
        public static final int ClickToView = 2131165258;
        public static final int Combo = 2131165254;
        public static final int More = 2131165255;
        public static final int MoreGames = 2131165257;
        public static final int app_name = 2131165227;
        public static final int avoided = 2131165270;
        public static final int eula_accept = 2131165232;
        public static final int eula_refuse = 2131165233;
        public static final int eula_title = 2131165231;
        public static final int feint_achievements = 2131165266;
        public static final int feint_dashboard = 2131165259;
        public static final int feint_dashboard_summary = 2131165260;
        public static final int feint_leaderboards = 2131165265;
        public static final int feint_logout = 2131165267;
        public static final int feint_logout_summary = 2131165268;
        public static final int feint_social_network_settings = 2131165264;
        public static final int game_over = 2131165236;
        public static final int game_paused = 2131165235;
        public static final int game_settings = 2131165234;
        public static final int help_about = 2131165261;
        public static final int heyzap_postscore = 2131165272;
        public static final int howto_00 = 2131165243;
        public static final int howto_01 = 2131165244;
        public static final int howto_02 = 2131165245;
        public static final int howto_03 = 2131165246;
        public static final int loading = 2131165263;
        public static final int of_achievement_load_null = 2131165192;
        public static final int of_achievement_unlock_null = 2131165191;
        public static final int of_achievement_unlocked = 2131165207;
        public static final int of_banned_dialog = 2131165220;
        public static final int of_bitmap_decode_error = 2131165209;
        public static final int of_cancel = 2131165203;
        public static final int of_cant_compress_blob = 2131165205;
        public static final int of_crash_report_query = 2131165217;
        public static final int of_device = 2131165198;
        public static final int of_error_parsing_error_message = 2131165211;
        public static final int of_exit_feint = 2131165222;
        public static final int of_file_not_found = 2131165210;
        public static final int of_home = 2131165219;
        public static final int of_id_cannot_be_null = 2131165186;
        public static final int of_io_exception_on_download = 2131165204;
        public static final int of_ioexception_reading_body = 2131165213;
        public static final int of_key_cannot_be_null = 2131165184;
        public static final int of_loading_feint = 2131165199;
        public static final int of_low_memory_profile_pic = 2131165194;
        public static final int of_malformed_request_error = 2131165226;
        public static final int of_name_cannot_be_null = 2131165187;
        public static final int of_no = 2131165200;
        public static final int of_no_blob = 2131165206;
        public static final int of_no_video = 2131165218;
        public static final int of_nodisk = 2131165196;
        public static final int of_now_logged_in_as_format = 2131165215;
        public static final int of_null_icon_url = 2131165190;
        public static final int of_offline_notification = 2131165223;
        public static final int of_offline_notification_line2 = 2131165224;
        public static final int of_ok = 2131165202;
        public static final int of_profile_pic_changed = 2131165216;
        public static final int of_profile_picture_download_failed = 2131165195;
        public static final int of_profile_url_null = 2131165193;
        public static final int of_score_submitted_notification = 2131165225;
        public static final int of_sdcard = 2131165197;
        public static final int of_secret_cannot_be_null = 2131165185;
        public static final int of_server_error_code_format = 2131165212;
        public static final int of_settings = 2131165221;
        public static final int of_switched_accounts = 2131165214;
        public static final int of_timeout = 2131165208;
        public static final int of_unexpected_response_format = 2131165188;
        public static final int of_unknown_server_error = 2131165189;
        public static final int of_yes = 2131165201;
        public static final int resume = 2131165237;
        public static final int retry = 2131165230;
        public static final int return_to_title = 2131165262;
        public static final int seconds = 2131165269;
        public static final int settings = 2131165229;
        public static final int start_game = 2131165228;
        public static final int string_about = 2131165247;
        public static final int string_about_body = 2131165249;
        public static final int string_about_body2 = 2131165251;
        public static final int string_about_body3 = 2131165253;
        public static final int string_about_title1 = 2131165248;
        public static final int string_about_title2 = 2131165250;
        public static final int string_about_title3 = 2131165252;
        public static final int summary_sound_preference = 2131165242;
        public static final int summary_vibration_preference = 2131165239;
        public static final int times = 2131165271;
        public static final int title_music_preference = 2131165241;
        public static final int title_sound_preference = 2131165240;
        public static final int title_vibration_preference = 2131165238;
    }

    public static final class style {
        public static final int OFLoading = 2131230720;
        public static final int OFNestedWindow = 2131230721;
    }

    public static final class xml {
        public static final int preferences = 2130968576;
    }
}
