package com.openfeint.api.resource;

import android.graphics.Bitmap;
import com.openfeint.internal.APICallback;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.notifications.AchievementNotification;
import com.openfeint.internal.offline.OfflineSupport;
import com.openfeint.internal.request.BitmapRequest;
import com.openfeint.internal.request.JSONRequest;
import com.openfeint.internal.request.OrderedArgList;
import com.openfeint.internal.resource.BooleanResourceProperty;
import com.openfeint.internal.resource.DateResourceProperty;
import com.openfeint.internal.resource.FloatResourceProperty;
import com.openfeint.internal.resource.IntResourceProperty;
import com.openfeint.internal.resource.Resource;
import com.openfeint.internal.resource.ResourceClass;
import com.openfeint.internal.resource.StringResourceProperty;
import java.util.Date;
import java.util.List;
import net.droidstick.finger.R;

public class Achievement extends Resource {
    public String description;
    public String endVersion;
    public int gamerscore;
    public String iconUrl;
    public boolean isSecret;
    public boolean isUnlocked;
    public float percentComplete;
    public int position;
    public String startVersion;
    public String title;
    public Date unlockDate;

    public static abstract class DownloadIconCB extends APICallback {
        public abstract void onSuccess(Bitmap bitmap);
    }

    public static abstract class ListCB extends APICallback {
        public abstract void onSuccess(List<Achievement> list);
    }

    public static abstract class LoadCB extends APICallback {
        public abstract void onSuccess();
    }

    public static abstract class UnlockCB extends APICallback {
        public abstract void onSuccess(boolean z);
    }

    public static abstract class UpdateProgressionCB extends APICallback {
        public abstract void onSuccess(boolean z);
    }

    public Achievement(String resourceID) {
        setResourceID(resourceID);
    }

    public static void list(final ListCB cb) {
        final String path = "/xp/games/" + OpenFeintInternal.getInstance().getAppID() + "/achievements";
        new JSONRequest() {
            public String method() {
                return "GET";
            }

            public String path() {
                return path;
            }

            public void onSuccess(Object responseBody) {
                if (cb != null) {
                    try {
                        cb.onSuccess((List) responseBody);
                    } catch (Exception e) {
                        onFailure(OpenFeintInternal.getRString(R.string.of_unexpected_response_format));
                    }
                }
            }

            public void onFailure(String exceptionMessage) {
                super.onFailure(exceptionMessage);
                if (cb != null) {
                    cb.onFailure(exceptionMessage);
                }
            }
        }.launch();
    }

    public void downloadIcon(final DownloadIconCB cb) {
        if (this.iconUrl != null) {
            new BitmapRequest() {
                public String method() {
                    return "GET";
                }

                public String url() {
                    return Achievement.this.iconUrl;
                }

                public String path() {
                    return "";
                }

                public void onSuccess(Bitmap responseBody) {
                    if (cb != null) {
                        cb.onSuccess(responseBody);
                    }
                }

                public void onFailure(String exceptionMessage) {
                    if (cb != null) {
                        cb.onFailure(exceptionMessage);
                    }
                }
            }.launch();
        } else if (cb != null) {
            cb.onFailure(OpenFeintInternal.getRString(R.string.of_null_icon_url));
        }
    }

    public void unlock(final UnlockCB cb) {
        UpdateProgressionCB upCB = null;
        if (cb != null) {
            upCB = new UpdateProgressionCB() {
                public void onSuccess(boolean complete) {
                    cb.onSuccess(complete);
                }

                public void onFailure(String exceptionMessage) {
                    cb.onFailure(exceptionMessage);
                }
            };
        }
        updateProgression(100.0f, upCB);
    }

    public void updateProgression(float pctComplete, UpdateProgressionCB cb) {
        final float clampedPctComplete;
        boolean z;
        if (pctComplete < 0.0f) {
            clampedPctComplete = 0.0f;
        } else {
            clampedPctComplete = pctComplete > 100.0f ? 100.0f : pctComplete;
        }
        final String resID = resourceID();
        if (resID == null) {
            if (cb != null) {
                cb.onFailure(OpenFeintInternal.getRString(R.string.of_achievement_unlock_null));
            }
        } else if (clampedPctComplete > OfflineSupport.getClientCompletionPercentage(resID)) {
            OfflineSupport.updateClientCompletionPercentage(resID, clampedPctComplete);
            if (OpenFeintInternal.getInstance().getUserID() == null) {
                this.percentComplete = OfflineSupport.getClientCompletionPercentage(resID);
                if (this.percentComplete == 100.0f) {
                    z = true;
                } else {
                    z = false;
                }
                this.isUnlocked = z;
                if (cb != null) {
                    cb.onSuccess(false);
                    return;
                }
                return;
            }
            final String path = "/xp/games/" + OpenFeintInternal.getInstance().getAppID() + "/achievements/" + resID + "/unlock";
            OrderedArgList args = new OrderedArgList();
            args.put("percent_complete", new Float(clampedPctComplete).toString());
            final UpdateProgressionCB updateProgressionCB = cb;
            new JSONRequest(args) {
                public String method() {
                    return "PUT";
                }

                public String path() {
                    return path;
                }

                /* access modifiers changed from: protected */
                public void onResponse(int responseCode, Object responseBody) {
                    if (responseCode >= 200 && responseCode < 300) {
                        Achievement achievement = (Achievement) responseBody;
                        float oldPercentComplete = Achievement.this.percentComplete;
                        Achievement.this.shallowCopy(achievement);
                        float newPercentComplete = Achievement.this.percentComplete;
                        OfflineSupport.updateServerCompletionPercentage(resID, newPercentComplete);
                        if (201 == responseCode || newPercentComplete > oldPercentComplete) {
                            AchievementNotification.showStatus(achievement);
                        }
                        if (updateProgressionCB != null) {
                            updateProgressionCB.onSuccess(201 == responseCode);
                        }
                    } else if (400 > responseCode || responseCode >= 500) {
                        if (100.0f == clampedPctComplete) {
                            Achievement.this.percentComplete = clampedPctComplete;
                            Achievement.this.isUnlocked = true;
                            AchievementNotification.showStatus(Achievement.this);
                        }
                        if (updateProgressionCB != null) {
                            updateProgressionCB.onSuccess(false);
                        }
                    } else {
                        onFailure(responseBody);
                    }
                }

                public void onFailure(String exceptionMessage) {
                    super.onFailure(exceptionMessage);
                    if (updateProgressionCB != null) {
                        updateProgressionCB.onFailure(exceptionMessage);
                    }
                }
            }.launch();
        } else if (cb != null) {
            cb.onSuccess(false);
        }
    }

    public void load(final LoadCB cb) {
        String resID = resourceID();
        if (resID != null) {
            final String path = "/xp/games/" + OpenFeintInternal.getInstance().getAppID() + "/achievements/" + resID;
            new JSONRequest() {
                public String method() {
                    return "GET";
                }

                public String path() {
                    return path;
                }

                public void onSuccess(Object responseBody) {
                    Achievement.this.shallowCopy((Achievement) responseBody);
                    if (cb != null) {
                        cb.onSuccess();
                    }
                }

                public void onFailure(String exceptionMessage) {
                    super.onFailure(exceptionMessage);
                    if (cb != null) {
                        cb.onFailure(exceptionMessage);
                    }
                }
            }.launch();
        } else if (cb != null) {
            cb.onFailure(OpenFeintInternal.getRString(R.string.of_achievement_load_null));
        }
    }

    public Achievement() {
    }

    public static ResourceClass getResourceClass() {
        ResourceClass klass = new ResourceClass(Achievement.class, "achievement") {
            public Resource factory() {
                return new Achievement();
            }
        };
        klass.mProperties.put("title", new StringResourceProperty() {
            public String get(Resource obj) {
                return ((Achievement) obj).title;
            }

            public void set(Resource obj, String val) {
                ((Achievement) obj).title = val;
            }
        });
        klass.mProperties.put("description", new StringResourceProperty() {
            public String get(Resource obj) {
                return ((Achievement) obj).description;
            }

            public void set(Resource obj, String val) {
                ((Achievement) obj).description = val;
            }
        });
        klass.mProperties.put("gamerscore", new IntResourceProperty() {
            public int get(Resource obj) {
                return ((Achievement) obj).gamerscore;
            }

            public void set(Resource obj, int val) {
                ((Achievement) obj).gamerscore = val;
            }
        });
        klass.mProperties.put("icon_url", new StringResourceProperty() {
            public String get(Resource obj) {
                return ((Achievement) obj).iconUrl;
            }

            public void set(Resource obj, String val) {
                ((Achievement) obj).iconUrl = val;
            }
        });
        klass.mProperties.put("is_secret", new BooleanResourceProperty() {
            public boolean get(Resource obj) {
                return ((Achievement) obj).isSecret;
            }

            public void set(Resource obj, boolean val) {
                ((Achievement) obj).isSecret = val;
            }
        });
        klass.mProperties.put("is_unlocked", new BooleanResourceProperty() {
            public boolean get(Resource obj) {
                return ((Achievement) obj).isUnlocked;
            }

            public void set(Resource obj, boolean val) {
                ((Achievement) obj).isUnlocked = val;
            }
        });
        klass.mProperties.put("percent_complete", new FloatResourceProperty() {
            public float get(Resource obj) {
                return ((Achievement) obj).percentComplete;
            }

            public void set(Resource obj, float val) {
                ((Achievement) obj).percentComplete = val;
            }
        });
        klass.mProperties.put("unlocked_at", new DateResourceProperty() {
            public Date get(Resource obj) {
                return ((Achievement) obj).unlockDate;
            }

            public void set(Resource obj, Date val) {
                ((Achievement) obj).unlockDate = val;
            }
        });
        klass.mProperties.put("position", new IntResourceProperty() {
            public int get(Resource obj) {
                return ((Achievement) obj).position;
            }

            public void set(Resource obj, int val) {
                ((Achievement) obj).position = val;
            }
        });
        klass.mProperties.put("end_version", new StringResourceProperty() {
            public String get(Resource obj) {
                return ((Achievement) obj).endVersion;
            }

            public void set(Resource obj, String val) {
                ((Achievement) obj).endVersion = val;
            }
        });
        klass.mProperties.put("start_version", new StringResourceProperty() {
            public String get(Resource obj) {
                return ((Achievement) obj).startVersion;
            }

            public void set(Resource obj, String val) {
                ((Achievement) obj).startVersion = val;
            }
        });
        return klass;
    }
}
