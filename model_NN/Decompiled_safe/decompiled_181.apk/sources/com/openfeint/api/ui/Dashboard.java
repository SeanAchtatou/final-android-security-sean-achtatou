package com.openfeint.api.ui;

import android.content.Intent;
import android.content.res.Resources;
import android.view.Menu;
import android.view.MenuItem;
import com.openfeint.api.Notification;
import com.openfeint.api.OpenFeint;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.notifications.TwoLineNotification;
import com.openfeint.internal.ui.Settings;
import com.openfeint.internal.ui.WebNav;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import net.droidstick.finger.R;

public class Dashboard extends WebNav {
    private static List<Dashboard> sOpenDashboards = new ArrayList();
    boolean mRootIsHome = true;

    public static void open() {
        open(null, false);
    }

    public static void openFromSpotlight() {
        open("user.json?spotlight=true", true);
    }

    public static void close() {
        for (Dashboard d : sOpenDashboards) {
            d.finish();
        }
    }

    public static void openLeaderboards() {
        open("leaderboards", false);
    }

    public static void openLeaderboard(String leaderboardId) {
        open("leaderboard?leaderboard_id=" + leaderboardId, false);
    }

    public static void openAchievements() {
        open("achievements", false);
    }

    public static void openGameDetail(String appId) {
        open("game?game_id=" + appId, false);
    }

    private static void open(String screenName, boolean spotlight) {
        OpenFeint.trySubmitOfflineData();
        OpenFeintInternal ofi = OpenFeintInternal.getInstance();
        if (!ofi.isFeintServerReachable()) {
            Resources r = OpenFeintInternal.getInstance().getContext().getResources();
            TwoLineNotification.show(r.getString(R.string.of_offline_notification), r.getString(R.string.of_offline_notification_line2), Notification.Category.Foreground, Notification.Type.NetworkOffline);
            return;
        }
        ofi.getAnalytics().markDashboardOpen();
        Intent intent = new Intent(ofi.getContext(), Dashboard.class);
        if (screenName != null) {
            intent.putExtra("screenName", screenName);
        }
        ofi.submitIntent(intent, spotlight);
    }

    public void onResume() {
        super.onResume();
        if (!sOpenDashboards.contains(this)) {
            sOpenDashboards.add(this);
        }
        if (OpenFeintInternal.getInstance().getCurrentUser() == null) {
            finish();
        }
    }

    public void onDestroy() {
        super.onDestroy();
        sOpenDashboards.remove(this);
        OpenFeintInternal.getInstance().getAnalytics().markDashboardClose();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.of_dashboard, menu);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean z;
        MenuItem findItem = menu.findItem(R.id.home);
        if (!this.mRootIsHome || this.pageStackCount > 1) {
            z = true;
        } else {
            z = false;
        }
        findItem.setVisible(z);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        String menuButtonName = null;
        if (item.getItemId() == R.id.home) {
            menuButtonName = "home";
            this.mRootIsHome = true;
        } else if (item.getItemId() == R.id.settings) {
            menuButtonName = "settings";
        } else if (item.getItemId() == R.id.exit_feint) {
            menuButtonName = "exit";
        }
        if (menuButtonName == null) {
            return super.onOptionsItemSelected(item);
        }
        executeJavascript(String.format("OF.menu('%s')", menuButtonName));
        return true;
    }

    /* access modifiers changed from: protected */
    public String initialContentPath() {
        String screenName = getIntent().getStringExtra("screenName");
        if (screenName == null) {
            return "dashboard/user";
        }
        this.mRootIsHome = false;
        return "dashboard/" + screenName;
    }

    /* access modifiers changed from: protected */
    public WebNav.ActionHandler createActionHandler(WebNav webNav) {
        return new DashboardActionHandler(webNav);
    }

    private class DashboardActionHandler extends WebNav.ActionHandler {
        public DashboardActionHandler(WebNav webNav) {
            super(webNav);
        }

        /* access modifiers changed from: protected */
        public void populateActionList(List<String> actionList) {
            super.populateActionList(actionList);
            actionList.add("openSettings");
        }

        public final void openSettings(Map<String, String> map) {
            Settings.open();
        }
    }
}
