package com.mopub.mobileads;

import android.util.Log;
import java.util.HashMap;

public abstract class BaseAdapter {
    private static final HashMap<String, String> sAdapterMap = new HashMap<>();

    public abstract void invalidate();

    public abstract void loadAd();

    static {
        sAdapterMap.put("admob_native", "com.mopub.mobileads.GoogleAdMobAdapter");
        sAdapterMap.put("millennial_native", "com.mopub.mobileads.MillennialAdapter");
        sAdapterMap.put("adsense", "com.mopub.mobileads.AdSenseAdapter");
    }

    public static BaseAdapter getAdapterForType(MoPubView view, String type, HashMap<String, String> params) {
        if (type == null) {
            return null;
        }
        Class<?> adapterClass = classForAdapterType(type, params);
        if (adapterClass == null) {
            return null;
        }
        Class[] parameterTypes = {MoPubView.class, String.class};
        try {
            return (BaseAdapter) adapterClass.getConstructor(parameterTypes).newInstance(view, params.get("X-Nativeparams"));
        } catch (Exception e) {
            Log.d("MoPub", "Couldn't create native adapter for type: " + type);
            return null;
        }
    }

    private static String classStringForAdapterType(String type, HashMap<String, String> hashMap) {
        return sAdapterMap.get(type);
    }

    private static Class<?> classForAdapterType(String type, HashMap<String, String> params) {
        String className = classStringForAdapterType(type, params);
        if (className == null) {
            Log.d("MoPub", "Couldn't find a handler for this ad type: " + type + "." + " MoPub for Android does not support it at this time.");
            return null;
        }
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            Log.d("MoPub", "Couldn't find " + className + " class." + " Make sure the project includes the adapter library for " + className + " from the extras folder");
            return null;
        }
    }
}
