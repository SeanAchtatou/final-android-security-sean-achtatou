package com.mopub.mobileads;

import android.location.Location;
import android.util.Log;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.InterstitialAd;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class GoogleAdMobInterstitialAdapter extends BaseInterstitialAdapter implements AdListener {
    private boolean mHasAlreadyRegisteredClick;
    private InterstitialAd mInterstitialAd;
    private String mParams;

    public GoogleAdMobInterstitialAdapter(MoPubInterstitial interstitial, String params) {
        super(interstitial);
        this.mParams = params;
        try {
            this.mInterstitialAd = new InterstitialAd(this.mInterstitial.getActivity(), ((JSONObject) new JSONTokener(this.mParams).nextValue()).getString("adUnitID"));
            this.mInterstitialAd.setAdListener(this);
        } catch (JSONException e) {
            this.mInterstitial.interstitialFailed();
        }
    }

    public void loadInterstitial() {
        if (this.mInterstitial != null) {
            AdRequest adRequest = new AdRequest();
            Location location = this.mInterstitial.getLocation();
            if (location != null) {
                adRequest.setLocation(location);
            }
            this.mInterstitialAd.loadAd(adRequest);
        }
    }

    public void invalidate() {
        this.mInterstitial = null;
    }

    public void showInterstitial() {
        this.mInterstitialAd.show();
    }

    public void onDismissScreen(Ad arg0) {
    }

    public void onFailedToReceiveAd(Ad arg0, AdRequest.ErrorCode arg1) {
        Log.d("MoPub", "Google AdMob interstitial failed. Trying another");
        if (this.mInterstitial != null) {
            this.mInterstitial.interstitialFailed();
        }
    }

    public void onLeaveApplication(Ad arg0) {
        Log.d("MoPub", "Google AdMob interstitial was clicked, leaving application");
        if (this.mInterstitial != null && !this.mHasAlreadyRegisteredClick) {
            this.mHasAlreadyRegisteredClick = true;
            this.mInterstitial.interstitialClicked();
        }
    }

    public void onPresentScreen(Ad arg0) {
    }

    public void onReceiveAd(Ad arg0) {
        Log.d("MoPub", "Google AdMob interstitial received an ad successfully.");
        if (this.mInterstitial != null) {
            this.mInterstitial.interstitialLoaded();
            this.mInterstitialAd.show();
        }
    }
}
