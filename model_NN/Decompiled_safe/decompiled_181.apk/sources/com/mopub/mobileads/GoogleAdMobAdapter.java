package com.mopub.mobileads;

import android.location.Location;
import android.util.Log;
import android.widget.FrameLayout;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class GoogleAdMobAdapter extends BaseAdapter implements AdListener {
    private AdView mAdView;
    private MoPubView mMoPubView;
    private String mParams;

    public GoogleAdMobAdapter(MoPubView view, String params) {
        this.mMoPubView = view;
        this.mParams = params;
    }

    public void loadAd() {
        if (this.mMoPubView != null) {
            try {
                this.mAdView = new AdView(this.mMoPubView.getActivity(), AdSize.BANNER, ((JSONObject) new JSONTokener(this.mParams).nextValue()).getString("adUnitID"));
                this.mAdView.setAdListener(this);
                AdRequest request = new AdRequest();
                Location location = this.mMoPubView.getLocation();
                if (location != null) {
                    request.setLocation(location);
                }
                this.mAdView.loadAd(request);
            } catch (JSONException e) {
                this.mMoPubView.adFailed();
            }
        }
    }

    public void invalidate() {
        this.mMoPubView = null;
    }

    public void onDismissScreen(Ad ad) {
    }

    public void onFailedToReceiveAd(Ad ad, AdRequest.ErrorCode error) {
        if (this.mMoPubView != null) {
            Log.d("MoPub", "Google AdMob failed. Trying another");
            this.mMoPubView.loadFailUrl();
        }
    }

    public void onLeaveApplication(Ad ad) {
    }

    public void onPresentScreen(Ad ad) {
        Log.d("MoPub", "Google AdMob clicked");
        if (this.mMoPubView != null) {
            this.mMoPubView.registerClick();
        }
    }

    public void onReceiveAd(Ad ad) {
        if (this.mMoPubView != null) {
            Log.d("MoPub", "Google AdMob load succeeded. Showing ad...");
            this.mMoPubView.removeAllViews();
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
            layoutParams.gravity = 17;
            this.mMoPubView.addView(this.mAdView, layoutParams);
            this.mMoPubView.nativeAdLoaded();
            this.mMoPubView.trackNativeImpression();
        }
    }
}
