package com.mopub.mobileads;

import android.util.Log;
import java.util.HashMap;

public abstract class BaseInterstitialAdapter {
    private static final HashMap<String, String> sInterstitialAdapterMap = new HashMap<>();
    protected MoPubInterstitial mInterstitial;

    public abstract void invalidate();

    public abstract void loadInterstitial();

    public abstract void showInterstitial();

    static {
        sInterstitialAdapterMap.put("admob_full", "com.mopub.mobileads.GoogleAdMobInterstitialAdapter");
        sInterstitialAdapterMap.put("millennial_full", "com.mopub.mobileads.MillennialInterstitialAdapter");
    }

    protected BaseInterstitialAdapter(MoPubInterstitial interstitial) {
        this.mInterstitial = interstitial;
    }

    public static BaseInterstitialAdapter getAdapterForType(MoPubInterstitial interstitial, String type, HashMap<String, String> params) {
        if (type == null) {
            return null;
        }
        Class<?> adapterClass = classForAdapterType(type, params);
        if (adapterClass == null) {
            return null;
        }
        Class[] parameterTypes = {MoPubInterstitial.class, String.class};
        try {
            return (BaseInterstitialAdapter) adapterClass.getConstructor(parameterTypes).newInstance(interstitial, params.get("X-Nativeparams"));
        } catch (Exception e) {
            Log.d("MoPub", "Couldn't create native interstitial adapter for type: " + type);
            return null;
        }
    }

    protected static String classStringForAdapterType(String type, HashMap<String, String> params) {
        String fullAdType = params.get("X-Fulladtype");
        if (!type.equals("interstitial") || fullAdType == null) {
            return null;
        }
        return sInterstitialAdapterMap.get(fullAdType);
    }

    protected static Class<?> classForAdapterType(String type, HashMap<String, String> params) {
        String className = classStringForAdapterType(type, params);
        if (className == null) {
            Log.d("MoPub", "Couldn't find a handler for this ad type: " + type + "." + " MoPub for Android does not support it at this time.");
            return null;
        }
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            Log.d("MoPub", "Couldn't find " + className + "class." + " Make sure the project includes the adapter library for " + className + " from the extras folder");
            return null;
        }
    }
}
