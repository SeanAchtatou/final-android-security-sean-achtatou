package com.mopub.mobileads;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import com.google.ads.AdActivity;
import com.mopub.mobileads.MoPubView;
import com.openfeint.internal.request.multipart.StringPart;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

public class AdView extends WebView {
    public static final String AD_ORIENTATION_BOTH = "b";
    public static final String AD_ORIENTATION_LANDSCAPE_ONLY = "l";
    public static final String AD_ORIENTATION_PORTRAIT_ONLY = "p";
    public static final String DEVICE_ORIENTATION_LANDSCAPE = "l";
    public static final String DEVICE_ORIENTATION_PORTRAIT = "p";
    public static final String DEVICE_ORIENTATION_SQUARE = "s";
    public static final String DEVICE_ORIENTATION_UNKNOWN = "u";
    public static final String EXTRA_AD_CLICK_DATA = "com.mopub.intent.extra.AD_CLICK_DATA";
    public static final long MINIMUM_REFRESH_TIME_MILLISECONDS = 10000;
    private String mAdOrientation;
    private String mAdUnitId;
    private boolean mAutorefreshEnabled;
    /* access modifiers changed from: private */
    public String mClickthroughUrl;
    private String mFailUrl;
    private int mHeight;
    /* access modifiers changed from: private */
    public String mImpressionUrl;
    /* access modifiers changed from: private */
    public boolean mIsLoading;
    private String mKeywords;
    private Location mLocation;
    protected MoPubView mMoPubView;
    private String mRedirectUrl;
    private Handler mRefreshHandler = new Handler();
    private Runnable mRefreshRunnable = new Runnable() {
        public void run() {
            AdView.this.loadAd();
        }
    };
    private long mRefreshTimeMilliseconds = 0;
    private HttpResponse mResponse;
    /* access modifiers changed from: private */
    public String mResponseString;
    private int mTimeoutMilliseconds = -1;
    private String mUrl;
    /* access modifiers changed from: private */
    public String mUserAgent;
    private int mWidth;

    private interface LoadUrlTaskResult {
        void execute();
    }

    public AdView(Context context, MoPubView view) {
        super(context);
        this.mMoPubView = view;
        this.mAutorefreshEnabled = true;
        setHorizontalScrollBarEnabled(false);
        setHorizontalScrollbarOverlay(false);
        setVerticalScrollBarEnabled(false);
        setVerticalScrollbarOverlay(false);
        getSettings().setSupportZoom(false);
        this.mUserAgent = getSettings().getUserAgentString();
        getSettings().setJavaScriptEnabled(true);
        getSettings().setPluginsEnabled(true);
        setBackgroundColor(0);
        setWebViewClient(new AdWebViewClient(this, null));
    }

    private class AdWebViewClient extends WebViewClient {
        private AdWebViewClient() {
        }

        /* synthetic */ AdWebViewClient(AdView adView, AdWebViewClient adWebViewClient) {
            this();
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            AdView adView = (AdView) view;
            if (url.startsWith("mopub://")) {
                Uri uri = Uri.parse(url);
                String host = uri.getHost();
                if (host.equals("finishLoad")) {
                    adView.pageFinished();
                } else if (host.equals("close")) {
                    adView.pageClosed();
                } else if (host.equals("failLoad")) {
                    adView.loadFailUrl();
                } else if (host.equals("custom")) {
                    adView.handleCustomIntentFromUri(uri);
                }
                return true;
            } else if (url.startsWith("tel:") || url.startsWith("voicemail:") || url.startsWith("sms:") || url.startsWith("mailto:") || url.startsWith("geo:") || url.startsWith("google.streetview:")) {
                try {
                    AdView.this.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
                } catch (ActivityNotFoundException e) {
                    Log.w("MoPub", "Could not handle intent with URI: " + url + ". Is this intent unsupported on your phone?");
                }
                return true;
            } else {
                String url2 = urlWithClickTrackingRedirect(adView, url);
                Log.d("MoPub", "Ad clicked. Click URL: " + url2);
                AdView.this.mMoPubView.adClicked();
                AdView.this.showBrowserAfterFollowingRedirectsForUrl(url2);
                return true;
            }
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            AdView adView = (AdView) view;
            String redirectUrl = adView.getRedirectUrl();
            if (redirectUrl != null && url.startsWith(redirectUrl)) {
                String url2 = urlWithClickTrackingRedirect(adView, url);
                view.stopLoading();
                AdView.this.showBrowserAfterFollowingRedirectsForUrl(url2);
            }
        }

        private String urlWithClickTrackingRedirect(AdView adView, String url) {
            String clickthroughUrl = adView.getClickthroughUrl();
            if (clickthroughUrl == null) {
                return url;
            }
            return String.valueOf(clickthroughUrl) + "&r=" + Uri.encode(url);
        }
    }

    /* access modifiers changed from: private */
    public void pageFinished() {
        Log.i("MoPub", "Ad successfully loaded.");
        this.mIsLoading = false;
        scheduleRefreshTimerIfEnabled();
        this.mMoPubView.removeAllViews();
        this.mMoPubView.addView(this, new FrameLayout.LayoutParams(-2, -2, 17));
        this.mMoPubView.adLoaded();
    }

    /* access modifiers changed from: private */
    public void pageFailed() {
        Log.i("MoPub", "Ad failed to load.");
        this.mIsLoading = false;
        scheduleRefreshTimerIfEnabled();
        this.mMoPubView.adFailed();
    }

    /* access modifiers changed from: private */
    public void pageClosed() {
        this.mMoPubView.adClosed();
    }

    /* access modifiers changed from: private */
    public void handleCustomIntentFromUri(Uri uri) {
        this.mMoPubView.adClicked();
        String action = uri.getQueryParameter("fnc");
        String adData = uri.getQueryParameter("data");
        Intent customIntent = new Intent(action);
        if (adData != null) {
            customIntent.putExtra(EXTRA_AD_CLICK_DATA, adData);
        }
        try {
            getContext().startActivity(customIntent);
        } catch (ActivityNotFoundException e) {
            Log.w("MoPub", "Could not handle custom intent: " + action + ". Is your intent spelled correctly?");
        }
    }

    /* access modifiers changed from: private */
    public void showBrowserAfterFollowingRedirectsForUrl(String url) {
        new LoadClickedUrlTask(this, null).execute(url);
    }

    private class LoadClickedUrlTask extends AsyncTask<String, Void, String> {
        private LoadClickedUrlTask() {
        }

        /* synthetic */ LoadClickedUrlTask(AdView adView, LoadClickedUrlTask loadClickedUrlTask) {
            this();
        }

        /* JADX WARN: Type inference failed for: r9v16, types: [java.net.URLConnection] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.String doInBackground(java.lang.String... r12) {
            /*
                r11 = this;
                r9 = 0
                r5 = r12[r9]
                r7 = 0
                java.net.URL r8 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0041 }
                r8.<init>(r5)     // Catch:{ MalformedURLException -> 0x0041 }
                r6 = -1
                r1 = 0
                java.lang.String r3 = r8.toString()
                java.util.HashSet r4 = new java.util.HashSet
                r4.<init>()
                r4.add(r3)
            L_0x0017:
                java.net.URLConnection r9 = r8.openConnection()     // Catch:{ IOException -> 0x007c, all -> 0x009b }
                r0 = r9
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x007c, all -> 0x009b }
                r1 = r0
                java.lang.String r9 = "User-Agent"
                com.mopub.mobileads.AdView r10 = com.mopub.mobileads.AdView.this     // Catch:{ IOException -> 0x007c, all -> 0x009b }
                java.lang.String r10 = r10.mUserAgent     // Catch:{ IOException -> 0x007c, all -> 0x009b }
                r1.setRequestProperty(r9, r10)     // Catch:{ IOException -> 0x007c, all -> 0x009b }
                r9 = 0
                r1.setInstanceFollowRedirects(r9)     // Catch:{ IOException -> 0x007c, all -> 0x009b }
                int r6 = r1.getResponseCode()     // Catch:{ IOException -> 0x007c, all -> 0x009b }
                r9 = 200(0xc8, float:2.8E-43)
                if (r6 != r9) goto L_0x0050
                r1.disconnect()     // Catch:{ IOException -> 0x007c, all -> 0x009b }
                r7 = r8
            L_0x003a:
                if (r1 == 0) goto L_0x003f
                r1.disconnect()
            L_0x003f:
                r9 = r3
            L_0x0040:
                return r9
            L_0x0041:
                r9 = move-exception
                r2 = r9
                java.lang.String r9 = "market://"
                boolean r9 = r5.startsWith(r9)
                if (r9 == 0) goto L_0x004d
                r9 = r5
                goto L_0x0040
            L_0x004d:
                java.lang.String r9 = ""
                goto L_0x0040
            L_0x0050:
                java.lang.String r9 = "location"
                java.lang.String r3 = r1.getHeaderField(r9)     // Catch:{ IOException -> 0x007c, all -> 0x009b }
                r1.disconnect()     // Catch:{ IOException -> 0x007c, all -> 0x009b }
                boolean r9 = r4.add(r3)     // Catch:{ IOException -> 0x007c, all -> 0x009b }
                if (r9 != 0) goto L_0x006f
                java.lang.String r9 = "MoPub"
                java.lang.String r10 = "Click redirect cycle detected -- will show blank."
                android.util.Log.d(r9, r10)     // Catch:{ IOException -> 0x007c, all -> 0x009b }
                if (r1 == 0) goto L_0x006b
                r1.disconnect()
            L_0x006b:
                java.lang.String r9 = ""
                r7 = r8
                goto L_0x0040
            L_0x006f:
                java.net.URL r7 = new java.net.URL     // Catch:{ IOException -> 0x007c, all -> 0x009b }
                r7.<init>(r3)     // Catch:{ IOException -> 0x007c, all -> 0x009b }
                boolean r9 = r11.isStatusCodeForRedirection(r6)     // Catch:{ IOException -> 0x00a5 }
                if (r9 == 0) goto L_0x003a
                r8 = r7
                goto L_0x0017
            L_0x007c:
                r9 = move-exception
                r2 = r9
                r7 = r8
            L_0x007f:
                if (r3 == 0) goto L_0x0093
                java.lang.String r9 = "market://"
                boolean r9 = r3.startsWith(r9)     // Catch:{ all -> 0x00a3 }
                if (r9 == 0) goto L_0x0090
                r9 = r3
            L_0x008a:
                if (r1 == 0) goto L_0x0040
                r1.disconnect()
                goto L_0x0040
            L_0x0090:
                java.lang.String r9 = ""
                goto L_0x008a
            L_0x0093:
                if (r1 == 0) goto L_0x0098
                r1.disconnect()
            L_0x0098:
                java.lang.String r9 = ""
                goto L_0x0040
            L_0x009b:
                r9 = move-exception
                r7 = r8
            L_0x009d:
                if (r1 == 0) goto L_0x00a2
                r1.disconnect()
            L_0x00a2:
                throw r9
            L_0x00a3:
                r9 = move-exception
                goto L_0x009d
            L_0x00a5:
                r9 = move-exception
                r2 = r9
                goto L_0x007f
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mopub.mobileads.AdView.LoadClickedUrlTask.doInBackground(java.lang.String[]):java.lang.String");
        }

        private boolean isStatusCodeForRedirection(int statusCode) {
            return statusCode == 302 || statusCode == 301 || statusCode == 307 || statusCode == 303;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String uri) {
            if (uri == null || uri.equals("")) {
                uri = "about:blank";
            }
            Log.d("MoPub", "Final URI to show in browser: " + uri);
            Intent actionIntent = new Intent("android.intent.action.VIEW", Uri.parse(uri));
            try {
                AdView.this.getContext().startActivity(actionIntent);
            } catch (ActivityNotFoundException e) {
                String action = actionIntent.getAction();
                if (action.startsWith("market://")) {
                    Log.w("MoPub", "Could not handle market action: " + action + ". Perhaps you're running in the emulator, which does not have " + "the Android Market?");
                } else {
                    Log.w("MoPub", "Could not handle intent action: " + action);
                }
                AdView.this.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse("about:blank")));
            }
        }
    }

    public void loadAd() {
        if (this.mAdUnitId == null) {
            throw new RuntimeException("AdUnitId is null for this view. You may have forgotten to call setAdUnitId().");
        }
        if (this.mLocation == null) {
            this.mLocation = getLastKnownLocation();
        }
        String adUrl = generateAdUrl();
        this.mMoPubView.adWillLoad(adUrl);
        loadUrl(adUrl);
    }

    private Location getLastKnownLocation() {
        Location result;
        MoPubView.LocationAwareness locationAwareness = this.mMoPubView.getLocationAwareness();
        int locationPrecision = this.mMoPubView.getLocationPrecision();
        if (locationAwareness == MoPubView.LocationAwareness.LOCATION_AWARENESS_DISABLED) {
            return null;
        }
        LocationManager lm = (LocationManager) getContext().getSystemService("location");
        Location gpsLocation = null;
        try {
            gpsLocation = lm.getLastKnownLocation("gps");
        } catch (SecurityException e) {
            Log.d("MoPub", "Failed to retrieve GPS location: access appears to be disabled.");
        } catch (IllegalArgumentException e2) {
            Log.d("MoPub", "Failed to retrieve GPS location: device has no GPS provider.");
        }
        Location networkLocation = null;
        try {
            networkLocation = lm.getLastKnownLocation("network");
        } catch (SecurityException e3) {
            Log.d("MoPub", "Failed to retrieve network location: access appears to be disabled.");
        } catch (IllegalArgumentException e4) {
            Log.d("MoPub", "Failed to retrieve network location: device has no network provider.");
        }
        if (gpsLocation == null && networkLocation == null) {
            return null;
        }
        if (gpsLocation == null || networkLocation == null) {
            if (gpsLocation != null) {
                result = gpsLocation;
            } else {
                result = networkLocation;
            }
        } else if (gpsLocation.getTime() > networkLocation.getTime()) {
            result = gpsLocation;
        } else {
            result = networkLocation;
        }
        if (locationAwareness == MoPubView.LocationAwareness.LOCATION_AWARENESS_TRUNCATED) {
            result.setLatitude(BigDecimal.valueOf(result.getLatitude()).setScale(locationPrecision, 5).doubleValue());
            result.setLongitude(BigDecimal.valueOf(result.getLongitude()).setScale(locationPrecision, 5).doubleValue());
        }
        return result;
    }

    private String generateAdUrl() {
        StringBuilder sz = new StringBuilder("http://" + MoPubView.HOST + MoPubView.AD_HANDLER);
        sz.append("?v=4&id=" + this.mAdUnitId);
        String udid = Settings.Secure.getString(getContext().getContentResolver(), "android_id");
        sz.append("&udid=sha:" + (udid == null ? "" : sha1(udid)));
        if (this.mKeywords != null) {
            sz.append("&q=" + Uri.encode(this.mKeywords));
        }
        if (this.mLocation != null) {
            sz.append("&ll=" + this.mLocation.getLatitude() + "," + this.mLocation.getLongitude());
        }
        sz.append("&z=" + getTimeZoneOffsetString());
        int orientation = getResources().getConfiguration().orientation;
        String orString = "u";
        if (orientation == 1) {
            orString = "p";
        } else if (orientation == 2) {
            orString = "l";
        } else if (orientation == 3) {
            orString = DEVICE_ORIENTATION_SQUARE;
        }
        sz.append("&o=" + orString);
        DisplayMetrics metrics = new DisplayMetrics();
        ((Activity) getContext()).getWindowManager().getDefaultDisplay().getMetrics(metrics);
        sz.append("&sc_a=" + metrics.density);
        return sz.toString();
    }

    private String sha1(String s) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            digest.update(s.getBytes());
            byte[] messageDigest = digest.digest();
            StringBuffer hexString = new StringBuffer();
            for (byte b : messageDigest) {
                hexString.append(Integer.toHexString(b & 255));
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            return "";
        }
    }

    private String getTimeZoneOffsetString() {
        SimpleDateFormat format = new SimpleDateFormat("Z");
        format.setTimeZone(TimeZone.getDefault());
        return format.format(new Date());
    }

    public void loadUrl(String url) {
        if (url.startsWith("javascript:")) {
            super.loadUrl(url);
        } else if (this.mIsLoading) {
            Log.i("MoPub", "Already loading an ad for " + this.mAdUnitId + ", wait to finish.");
        } else {
            this.mUrl = url;
            this.mIsLoading = true;
            new LoadUrlTask(this, null).execute(this.mUrl);
        }
    }

    private class LoadUrlTask extends AsyncTask<String, Void, LoadUrlTaskResult> {
        private Exception error;

        private LoadUrlTask() {
        }

        /* synthetic */ LoadUrlTask(AdView adView, LoadUrlTask loadUrlTask) {
            this();
        }

        /* access modifiers changed from: protected */
        public LoadUrlTaskResult doInBackground(String... urls) {
            try {
                return AdView.this.loadAdFromNetwork(urls[0]);
            } catch (Exception e) {
                this.error = e;
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(LoadUrlTaskResult result) {
            if (this.error != null || result == null) {
                AdView.this.pageFailed();
            } else {
                result.execute();
            }
        }
    }

    private class PerformCustomEventTaskResult implements LoadUrlTaskResult {
        protected Header mHeader;

        public PerformCustomEventTaskResult(Header header) {
            this.mHeader = header;
        }

        public void execute() {
            AdView.this.performCustomEventFromHeader(this.mHeader);
        }
    }

    /* access modifiers changed from: private */
    public void performCustomEventFromHeader(Header methodHeader) {
        if (methodHeader == null) {
            Log.i("MoPub", "Couldn't call custom method because the server did not specify one.");
            this.mMoPubView.adFailed();
            return;
        }
        String methodName = methodHeader.getValue();
        Log.i("MoPub", "Trying to call method named " + methodName);
        Activity userActivity = this.mMoPubView.getActivity();
        try {
            userActivity.getClass().getMethod(methodName, MoPubView.class).invoke(userActivity, this.mMoPubView);
        } catch (NoSuchMethodException e) {
            Log.d("MoPub", "Couldn't perform custom method named " + methodName + "(MoPubView view) because your activity class has no such method");
        } catch (Exception e2) {
            Log.d("MoPub", "Couldn't perform custom method named " + methodName);
        }
    }

    private class LoadNativeAdTaskResult implements LoadUrlTaskResult {
        protected HashMap<String, String> mParamsHash;

        public LoadNativeAdTaskResult(HashMap<String, String> hash) {
            this.mParamsHash = hash;
        }

        public void execute() {
            AdView.this.mIsLoading = false;
            AdView.this.mMoPubView.loadNativeSDK(this.mParamsHash);
        }
    }

    private class LoadHtmlAdTaskResult implements LoadUrlTaskResult {
        protected String mData;

        public LoadHtmlAdTaskResult(String data) {
            this.mData = data;
        }

        public void execute() {
            AdView.this.mResponseString = this.mData;
            AdView.this.loadDataWithBaseURL("http://" + MoPubView.HOST + "/", this.mData, StringPart.DEFAULT_CONTENT_TYPE, "utf-8", null);
        }
    }

    /* access modifiers changed from: private */
    public LoadUrlTaskResult loadAdFromNetwork(String url) throws Exception {
        HttpGet httpGet = new HttpGet(url);
        httpGet.addHeader("User-Agent", this.mUserAgent);
        this.mResponse = getAdViewHttpClient().execute(httpGet);
        HttpEntity entity = this.mResponse.getEntity();
        if (this.mResponse.getStatusLine().getStatusCode() != 200 || entity == null || entity.getContentLength() == 0) {
            throw new Exception("MoPub server returned invalid response.");
        }
        Header atHeader = this.mResponse.getFirstHeader("X-Adtype");
        if (atHeader == null || atHeader.getValue().equals("clear")) {
            throw new Exception("MoPub server returned no ad.");
        }
        configureAdViewUsingHeadersFromHttpResponse(this.mResponse);
        if (atHeader.getValue().equals("custom")) {
            Log.i("MoPub", "Performing custom event.");
            Header cmHeader = this.mResponse.getFirstHeader("X-Customselector");
            this.mIsLoading = false;
            return new PerformCustomEventTaskResult(cmHeader);
        } else if (!atHeader.getValue().equals(AdActivity.HTML_PARAM)) {
            Log.i("MoPub", "Loading native ad");
            Header npHeader = this.mResponse.getFirstHeader("X-Nativeparams");
            if (npHeader != null) {
                this.mIsLoading = false;
                HashMap<String, String> paramsHash = new HashMap<>();
                paramsHash.put("X-Adtype", atHeader.getValue());
                paramsHash.put("X-Nativeparams", npHeader.getValue());
                Header ftHeader = this.mResponse.getFirstHeader("X-Fulladtype");
                if (ftHeader != null) {
                    paramsHash.put("X-Fulladtype", ftHeader.getValue());
                }
                return new LoadNativeAdTaskResult(paramsHash);
            }
            throw new Exception("Could not load native ad; MoPub provided no parameters.");
        } else {
            InputStream is = entity.getContent();
            StringBuffer out = new StringBuffer();
            byte[] b = new byte[4096];
            while (true) {
                int n = is.read(b);
                if (n == -1) {
                    return new LoadHtmlAdTaskResult(out.toString());
                }
                out.append(new String(b, 0, n));
            }
        }
    }

    private DefaultHttpClient getAdViewHttpClient() {
        HttpParams httpParameters = new BasicHttpParams();
        if (this.mTimeoutMilliseconds > 0) {
            HttpConnectionParams.setConnectionTimeout(httpParameters, this.mTimeoutMilliseconds);
            HttpConnectionParams.setSoTimeout(httpParameters, this.mTimeoutMilliseconds);
        }
        HttpConnectionParams.setSocketBufferSize(httpParameters, 8192);
        return new DefaultHttpClient(httpParameters);
    }

    private void configureAdViewUsingHeadersFromHttpResponse(HttpResponse response) {
        Header ntHeader = response.getFirstHeader("X-Networktype");
        if (ntHeader != null) {
            Log.i("MoPub", "Fetching ad network type: " + ntHeader.getValue());
        }
        Header rdHeader = response.getFirstHeader("X-Launchpage");
        if (rdHeader != null) {
            this.mRedirectUrl = rdHeader.getValue();
        } else {
            this.mRedirectUrl = null;
        }
        Header ctHeader = response.getFirstHeader("X-Clickthrough");
        if (ctHeader != null) {
            this.mClickthroughUrl = ctHeader.getValue();
        } else {
            this.mClickthroughUrl = null;
        }
        Header flHeader = response.getFirstHeader("X-Failurl");
        if (flHeader != null) {
            this.mFailUrl = flHeader.getValue();
        } else {
            this.mFailUrl = null;
        }
        Header imHeader = response.getFirstHeader("X-Imptracker");
        if (imHeader != null) {
            this.mImpressionUrl = imHeader.getValue();
        } else {
            this.mImpressionUrl = null;
        }
        Header scHeader = response.getFirstHeader("X-Scrollable");
        boolean enabled = false;
        if (scHeader != null) {
            enabled = scHeader.getValue().equals("1");
        }
        setWebViewScrollingEnabled(enabled);
        Header wHeader = response.getFirstHeader("X-Width");
        Header hHeader = response.getFirstHeader("X-Height");
        if (wHeader == null || hHeader == null) {
            this.mWidth = 0;
            this.mHeight = 0;
        } else {
            this.mWidth = Integer.parseInt(wHeader.getValue().trim());
            this.mHeight = Integer.parseInt(hHeader.getValue().trim());
        }
        Header rtHeader = response.getFirstHeader("X-Refreshtime");
        if (rtHeader != null) {
            this.mRefreshTimeMilliseconds = Long.valueOf(rtHeader.getValue()).longValue() * 1000;
            if (this.mRefreshTimeMilliseconds < MINIMUM_REFRESH_TIME_MILLISECONDS) {
                this.mRefreshTimeMilliseconds = MINIMUM_REFRESH_TIME_MILLISECONDS;
            }
        } else {
            this.mRefreshTimeMilliseconds = 0;
        }
        Header orHeader = response.getFirstHeader("X-Orientation");
        this.mAdOrientation = orHeader != null ? orHeader.getValue() : null;
    }

    private void setWebViewScrollingEnabled(boolean enabled) {
        if (enabled) {
            setOnTouchListener(null);
        } else {
            setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    return event.getAction() == 2;
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void cleanup() {
        setAutorefreshEnabled(false);
    }

    public void reload() {
        Log.d("MoPub", "Reload ad: " + this.mUrl);
        loadUrl(this.mUrl);
    }

    public void loadFailUrl() {
        this.mIsLoading = false;
        if (this.mFailUrl != null) {
            Log.d("MoPub", "Loading failover url: " + this.mFailUrl);
            loadUrl(this.mFailUrl);
            return;
        }
        pageFailed();
    }

    /* access modifiers changed from: protected */
    public void loadResponseString(String responseString) {
        loadDataWithBaseURL("http://" + MoPubView.HOST + "/", responseString, StringPart.DEFAULT_CONTENT_TYPE, "utf-8", null);
    }

    /* access modifiers changed from: protected */
    public void trackImpression() {
        if (this.mImpressionUrl != null) {
            new Thread(new Runnable() {
                public void run() {
                    DefaultHttpClient httpclient = new DefaultHttpClient();
                    HttpGet httpget = new HttpGet(AdView.this.mImpressionUrl);
                    httpget.addHeader("User-Agent", AdView.this.mUserAgent);
                    try {
                        httpclient.execute(httpget);
                    } catch (ClientProtocolException e) {
                        Log.i("MoPub", "Impression tracking failed: " + AdView.this.mImpressionUrl);
                    } catch (IOException e2) {
                        Log.i("MoPub", "Impression tracking failed: " + AdView.this.mImpressionUrl);
                    }
                }
            }).start();
        }
    }

    /* access modifiers changed from: protected */
    public void registerClick() {
        if (this.mClickthroughUrl != null) {
            new Thread(new Runnable() {
                public void run() {
                    DefaultHttpClient httpclient = new DefaultHttpClient();
                    HttpGet httpget = new HttpGet(AdView.this.mClickthroughUrl);
                    httpget.addHeader("User-Agent", AdView.this.mUserAgent);
                    try {
                        httpclient.execute(httpget);
                    } catch (ClientProtocolException e) {
                        Log.i("MoPub", "Click tracking failed: " + AdView.this.mClickthroughUrl);
                    } catch (IOException e2) {
                        Log.i("MoPub", "Click tracking failed: " + AdView.this.mClickthroughUrl);
                    }
                }
            }).start();
        }
    }

    /* access modifiers changed from: protected */
    public void adAppeared() {
        loadUrl("javascript:webviewDidAppear();");
    }

    /* access modifiers changed from: protected */
    public void scheduleRefreshTimerIfEnabled() {
        cancelRefreshTimer();
        if (this.mAutorefreshEnabled && this.mRefreshTimeMilliseconds > 0) {
            this.mRefreshHandler.postDelayed(this.mRefreshRunnable, this.mRefreshTimeMilliseconds);
        }
    }

    /* access modifiers changed from: protected */
    public void cancelRefreshTimer() {
        this.mRefreshHandler.removeCallbacks(this.mRefreshRunnable);
    }

    public String getKeywords() {
        return this.mKeywords;
    }

    public void setKeywords(String keywords) {
        this.mKeywords = keywords;
    }

    public Location getLocation() {
        return this.mLocation;
    }

    public void setLocation(Location location) {
        this.mLocation = location;
    }

    public String getAdUnitId() {
        return this.mAdUnitId;
    }

    public void setAdUnitId(String adUnitId) {
        this.mAdUnitId = adUnitId;
    }

    public void setTimeout(int milliseconds) {
        this.mTimeoutMilliseconds = milliseconds;
    }

    public int getAdWidth() {
        return this.mWidth;
    }

    public int getAdHeight() {
        return this.mHeight;
    }

    public String getAdOrientation() {
        return this.mAdOrientation;
    }

    public String getClickthroughUrl() {
        return this.mClickthroughUrl;
    }

    public void setClickthroughUrl(String url) {
        this.mClickthroughUrl = url;
    }

    public String getRedirectUrl() {
        return this.mRedirectUrl;
    }

    public HttpResponse getResponse() {
        return this.mResponse;
    }

    public String getResponseString() {
        return this.mResponseString;
    }

    public void setAutorefreshEnabled(boolean enabled) {
        this.mAutorefreshEnabled = enabled;
        if (!this.mAutorefreshEnabled) {
            cancelRefreshTimer();
        } else {
            scheduleRefreshTimerIfEnabled();
        }
    }

    public boolean getAutorefreshEnabled() {
        return this.mAutorefreshEnabled;
    }
}
