package com.heyzap.sdk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Canvas;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;

class ClickableToast extends FrameLayout {
    private static DismissToastBroadcastReceiver dismissReceiver = new DismissToastBroadcastReceiver();
    protected WindowManager windowManager;

    public static class DismissToastBroadcastReceiver extends BroadcastReceiver {
        private ClickableToast toast;

        public void onReceive(Context context, Intent intent) {
            if (this.toast != null) {
                this.toast.hide();
            }
        }

        public void setToast(ClickableToast clickableToast) {
            this.toast = clickableToast;
        }
    }

    public ClickableToast(Context context) {
        super(context);
        init();
    }

    public static void show(Context context, View view, int i) {
        ClickableToast clickableToast = new ClickableToast(context);
        clickableToast.addView(view);
        clickableToast.show(i);
    }

    public WindowManager.LayoutParams getWmParams() {
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.height = -2;
        layoutParams.width = -2;
        layoutParams.alpha = 1.0f;
        layoutParams.format = 1;
        layoutParams.gravity = 17;
        layoutParams.verticalMargin = 0.05f;
        layoutParams.flags = 40;
        return layoutParams;
    }

    public void hide() {
        try {
            this.windowManager.removeView(this);
        } catch (RuntimeException e) {
        }
        try {
            getContext().unregisterReceiver(dismissReceiver);
        } catch (RuntimeException e2) {
        }
    }

    public void init() {
        dismissReceiver.setToast(this);
        setBackgroundColor(0);
        this.windowManager = (WindowManager) getContext().getSystemService("window");
    }

    public void onAttachedToWindow() {
        try {
            this.windowManager.updateViewLayout(this, getWmParams());
        } catch (RuntimeException e) {
        }
    }

    public void onDraw(Canvas canvas) {
        canvas.rotate(180.0f);
        super.onDraw(canvas);
        canvas.rotate(180.0f);
    }

    public void show() {
        WindowManager.LayoutParams wmParams = getWmParams();
        wmParams.type = 2005;
        this.windowManager.addView(this, wmParams);
        getContext().registerReceiver(dismissReceiver, new IntentFilter("android.intent.action.CLOSE_SYSTEM_DIALOGS"));
    }

    public void show(int i) {
        show();
        postDelayed(new Runnable() {
            public void run() {
                ClickableToast.this.hide();
            }
        }, (long) i);
    }
}
