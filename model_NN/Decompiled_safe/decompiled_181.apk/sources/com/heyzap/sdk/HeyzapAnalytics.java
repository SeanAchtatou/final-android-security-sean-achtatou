package com.heyzap.sdk;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class HeyzapAnalytics {
    private static final String HEYZAP_ANALYTICS_ID_PREF = "heyzap_button_analytics_id";
    private static final String HEYZAP_ENDPOINT = "http://android.heyzap.com/mobile/track_sdk_event";
    public static final String LOG_TAG = "HeyzapSDK";
    /* access modifiers changed from: private */
    public static String deviceId = "unknown";
    private static boolean loaded = false;
    /* access modifiers changed from: private */
    public static String packageName = "unknown";
    private static ExecutorService requestThread = Executors.newSingleThreadExecutor();
    /* access modifiers changed from: private */
    public static String trackHash = "";

    HeyzapAnalytics() {
    }

    /* access modifiers changed from: private */
    public static String convertStreamToString(InputStream inputStream) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    sb.append(readLine + "\n");
                } else {
                    try {
                        break;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (IOException e2) {
                e2.printStackTrace();
                try {
                    inputStream.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            } catch (Throwable th) {
                try {
                    inputStream.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
                throw th;
            }
        }
        inputStream.close();
        return sb.toString();
    }

    public static String getAnalyticsReferrer(Context context) {
        return getAnalyticsReferrer(context, null);
    }

    public static String getAnalyticsReferrer(Context context, String str) {
        String trackHash2 = getTrackHash(context);
        String str2 = trackHash2 != null ? "utm_medium=device&utm_source=heyzap_track&utm_campaign=" + trackHash2 : "utm_medium=device&utm_source=sdk&utm_campaign=" + context.getPackageName();
        if (str != null) {
            str2 = str2 + "&" + str;
        }
        return URLEncoder.encode(str2);
    }

    /* access modifiers changed from: private */
    public static String getTrackHash(Context context) {
        SharedPreferences sharedPreferences;
        if (trackHash == null && (sharedPreferences = context.getSharedPreferences(HEYZAP_ANALYTICS_ID_PREF, 0)) != null) {
            trackHash = sharedPreferences.getString(HEYZAP_ANALYTICS_ID_PREF, null);
        }
        if (trackHash == null || !trackHash.trim().equals("")) {
            return trackHash;
        }
        return null;
    }

    private static void init(final Context context) {
        String str = Build.PRODUCT;
        String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
        if (!(str == null || string == null)) {
            deviceId = str + "_" + string;
        }
        new Thread(new Runnable() {
            public void run() {
                String access$500 = HeyzapAnalytics.getTrackHash(context);
                if (access$500 != null) {
                    String unused = HeyzapAnalytics.trackHash = access$500;
                }
            }
        }).start();
        packageName = context.getPackageName();
    }

    /* access modifiers changed from: private */
    public static void setTrackHash(Context context, String str) {
        if (str != null && !str.trim().equals("") && !trackHash.equals(str)) {
            trackHash = str;
            SharedPreferences.Editor edit = context.getSharedPreferences(HEYZAP_ANALYTICS_ID_PREF, 0).edit();
            edit.putString(HEYZAP_ANALYTICS_ID_PREF, trackHash);
            edit.commit();
        }
    }

    public static synchronized void trackEvent(final Context context, final String str) {
        synchronized (HeyzapAnalytics.class) {
            Log.d(LOG_TAG, "Tracking " + str + " event.");
            if (!loaded) {
                init(context);
                loaded = true;
            }
            requestThread.execute(new Runnable() {
                /* JADX INFO: finally extract failed */
                public void run() {
                    try {
                        Uri.Builder builder = new Uri.Builder();
                        builder.appendQueryParameter("game_package", HeyzapAnalytics.packageName);
                        builder.appendQueryParameter("device_id", HeyzapAnalytics.deviceId);
                        builder.appendQueryParameter("track_hash", HeyzapAnalytics.trackHash);
                        builder.appendQueryParameter("type", str);
                        String encodedQuery = builder.build().getEncodedQuery();
                        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(HeyzapAnalytics.HEYZAP_ENDPOINT).openConnection();
                        try {
                            httpURLConnection.setDoOutput(true);
                            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(httpURLConnection.getOutputStream());
                            outputStreamWriter.write(encodedQuery);
                            outputStreamWriter.flush();
                            outputStreamWriter.close();
                            HeyzapAnalytics.setTrackHash(context, HeyzapAnalytics.convertStreamToString(httpURLConnection.getInputStream()).trim());
                            httpURLConnection.disconnect();
                        } catch (IOException e) {
                            e.printStackTrace();
                            httpURLConnection.disconnect();
                        } catch (Throwable th) {
                            httpURLConnection.disconnect();
                            throw th;
                        }
                    } catch (IOException e2) {
                    } catch (UnsupportedOperationException e3) {
                        e3.printStackTrace();
                    }
                }
            });
        }
    }
}
