package com.heyzap.sdk;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Build;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;

class Drawables {
    public static final int DIALOG_BACKGROUND = 0;
    public static final int DIALOG_BUTTON_BACKGROUND = 1;
    public static final int PRIMARY_BUTTON_BACKGROUND = 2;
    public static final int SECONDARY_BUTTON_BACKGROUND = 3;

    public static class BitmapDensitySetter {
        public BitmapDrawable setDensity(BitmapDrawable bitmapDrawable, int i, Resources resources) {
            Bitmap bitmap = bitmapDrawable.getBitmap();
            bitmap.setDensity(i);
            return new BitmapDrawable(resources, bitmap);
        }
    }

    Drawables() {
    }

    public static Drawable getDialogBackground(Context context) {
        int dpToPx = Utils.dpToPx(context, 5);
        ShapeDrawable shapeDrawable = new ShapeDrawable(new RoundRectShape(new float[]{(float) dpToPx, (float) dpToPx, (float) dpToPx, (float) dpToPx, (float) dpToPx, (float) dpToPx, (float) dpToPx, (float) dpToPx}, null, new float[]{(float) dpToPx, (float) dpToPx, (float) dpToPx, (float) dpToPx, (float) dpToPx, (float) dpToPx, (float) dpToPx, (float) dpToPx}));
        shapeDrawable.getPaint().setColor(Color.parseColor("#0d1a2e"));
        ShapeDrawable shapeDrawable2 = new ShapeDrawable(new RoundRectShape(new float[]{(float) dpToPx, (float) dpToPx, (float) dpToPx, (float) dpToPx, (float) dpToPx, (float) dpToPx, (float) dpToPx, (float) dpToPx}, null, new float[]{(float) dpToPx, (float) dpToPx, (float) dpToPx, (float) dpToPx, (float) dpToPx, (float) dpToPx, (float) dpToPx, (float) dpToPx}));
        shapeDrawable2.getPaint().setColor(Color.parseColor("#ffffff"));
        LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{shapeDrawable2, shapeDrawable});
        int dpToPx2 = Utils.dpToPx(context, 2);
        layerDrawable.setLayerInset(1, dpToPx2, dpToPx2, dpToPx2, dpToPx2);
        return layerDrawable;
    }

    public static Drawable getDialogButtonBackground(Context context) {
        int dpToPx = Utils.dpToPx(context, 5);
        ShapeDrawable shapeDrawable = new ShapeDrawable(new RoundRectShape(new float[]{0.0f, 0.0f, 0.0f, 0.0f, (float) dpToPx, (float) dpToPx, (float) dpToPx, (float) dpToPx}, null, new float[]{(float) dpToPx, (float) dpToPx, (float) dpToPx, (float) dpToPx, (float) dpToPx, (float) dpToPx, (float) dpToPx, (float) dpToPx}));
        shapeDrawable.getPaint().setColor(Color.parseColor("#bdbebd"));
        return shapeDrawable;
    }

    public static Drawable getDrawable(Context context, int i) {
        switch (i) {
            case 0:
                return getDialogBackground(context);
            case 1:
                return getDialogButtonBackground(context);
            case 2:
                return getPrimaryButtonBackground(context);
            case 3:
                return getSecondaryButtonBackground(context);
            default:
                return null;
        }
    }

    public static Drawable getDrawable(Context context, String str) {
        BitmapDrawable bitmapDrawable = (BitmapDrawable) BitmapDrawable.createFromStream(Utils.class.getClassLoader().getResourceAsStream("res/drawable/" + str), str);
        try {
            return Integer.parseInt(Build.VERSION.SDK) >= 4 ? new BitmapDensitySetter().setDensity(bitmapDrawable, 240, context.getResources()) : bitmapDrawable;
        } catch (Exception e) {
            return bitmapDrawable;
        }
    }

    public static Drawable getPrimaryButtonBackground(Context context) {
        StateListDrawable stateListDrawable = new StateListDrawable();
        Drawable drawable = getDrawable((Context) null, "dialog_grn_btn_sel.png");
        stateListDrawable.addState(new int[]{16842919}, drawable);
        stateListDrawable.addState(new int[]{16842908}, drawable);
        stateListDrawable.addState(new int[]{16842910}, getDrawable((Context) null, "dialog_grn_btn.png"));
        return stateListDrawable;
    }

    public static Drawable getSecondaryButtonBackground(Context context) {
        StateListDrawable stateListDrawable = new StateListDrawable();
        Drawable drawable = getDrawable((Context) null, "dialog_btn_sel.png");
        stateListDrawable.addState(new int[]{16842919}, drawable);
        stateListDrawable.addState(new int[]{16842908}, drawable);
        stateListDrawable.addState(new int[]{16842910}, getDrawable((Context) null, "dialog_btn.png"));
        return stateListDrawable;
    }

    public static void setBackgroundDrawable(final Context context, final View view, final int i) {
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            public void run() {
                final Drawable drawable = Drawables.getDrawable(context, i);
                handler.post(new Runnable() {
                    public void run() {
                        view.setBackgroundDrawable(drawable);
                    }
                });
            }
        }).start();
    }

    public static void setBackgroundDrawable(final Context context, final View view, final String str) {
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            public void run() {
                final Drawable drawable = Drawables.getDrawable(context, str);
                handler.post(new Runnable() {
                    public void run() {
                        view.setBackgroundDrawable(drawable);
                    }
                });
            }
        }).start();
    }

    public static void setImageDrawable(final Context context, final ImageView imageView, final int i) {
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            public void run() {
                final Drawable drawable = Drawables.getDrawable(context, i);
                handler.post(new Runnable() {
                    public void run() {
                        imageView.setImageDrawable(drawable);
                    }
                });
            }
        }).start();
    }

    public static void setImageDrawable(final Context context, final ImageView imageView, final String str) {
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            public void run() {
                final Drawable drawable = Drawables.getDrawable(context, str);
                handler.post(new Runnable() {
                    public void run() {
                        imageView.setImageDrawable(drawable);
                    }
                });
            }
        }).start();
    }
}
