package com.heyzap.sdk;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.util.Log;
import android.widget.Toast;

public class HeyzapLib {
    private static final String HEYZAP_INTENT_CLASS = ".CheckinForm";
    public static final String HEYZAP_PACKAGE = "com.heyzap.android";
    private static int NO_NOTIFICATION_FLAG = 8388608;
    private static int flags = 0;
    private static String packageName;

    static void broadcastEnableSDK(Context context) {
        Intent intent = new Intent("com.heyzap.android.enableSDK");
        intent.putExtra("packageName", context.getPackageName());
        context.sendBroadcast(intent);
    }

    public static void checkin(Context context) {
        checkin(context, null);
    }

    public static void checkin(Context context, String str) {
        sendNotification(context);
        rawCheckin(context, str);
    }

    public static boolean isSupported(Context context) {
        return Utils.isSupported(context);
    }

    static void launchCheckinForm(Context context, String str) {
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.putExtra("message", str);
        intent.setAction(HEYZAP_PACKAGE);
        intent.addCategory("android.intent.category.LAUNCHER");
        intent.putExtra("packageName", context.getPackageName());
        intent.addFlags(402653184);
        intent.setComponent(new ComponentName(HEYZAP_PACKAGE, "com.heyzap.android.CheckinForm"));
        context.startActivity(intent);
    }

    public static void load(Context context) {
        load(context, true);
    }

    public static void load(Context context, boolean z) {
        sendNotification(context);
        String appLabel = Utils.getAppLabel(context);
        if (appLabel != null && !packageIsInstalled(HEYZAP_PACKAGE, context) && Utils.isSupported(context) && z) {
            new PopupToast(context.getApplicationContext(), appLabel).show(8000);
        }
    }

    static boolean packageIsInstalled(String str, Context context) {
        try {
            PackageManager packageManager = context.getPackageManager();
            Intent launchIntentForPackage = packageManager.getLaunchIntentForPackage(str);
            return launchIntentForPackage != null && packageManager.queryIntentActivities(launchIntentForPackage, 65536).size() > 0;
        } catch (Exception e) {
            return false;
        }
    }

    static void rawCheckin(Context context, String str) {
        packageName = context.getPackageName();
        Log.v(HeyzapAnalytics.LOG_TAG, "checkin-called");
        if (!Utils.isSupported(context)) {
            Toast.makeText(context, "Sorry, your device is not supported by Heyzap.", 1).show();
        } else if (packageIsInstalled(HEYZAP_PACKAGE, context)) {
            launchCheckinForm(context, str);
        } else {
            try {
                HeyzapAnalytics.trackEvent(context, "checkin-button-clicked");
                new PreMarketDialog(context.getApplicationContext(), packageName).show();
            } catch (ActivityNotFoundException e) {
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0008, code lost:
        r0 = com.heyzap.sdk.Utils.getAppLabel(r3);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void sendNotification(final android.content.Context r3) {
        /*
            int r0 = com.heyzap.sdk.HeyzapLib.NO_NOTIFICATION_FLAG
            int r1 = com.heyzap.sdk.HeyzapLib.flags
            r0 = r0 & r1
            if (r0 <= 0) goto L_0x0008
        L_0x0007:
            return
        L_0x0008:
            java.lang.String r0 = com.heyzap.sdk.Utils.getAppLabel(r3)
            if (r0 == 0) goto L_0x0007
            java.lang.String r1 = "com.heyzap.android"
            boolean r1 = packageIsInstalled(r1, r3)
            if (r1 != 0) goto L_0x0007
            boolean r1 = com.heyzap.sdk.Utils.isSupported(r3)
            if (r1 == 0) goto L_0x0007
            java.lang.String r1 = "hz-notificationnnn"
            com.heyzap.sdk.HeyzapLib$1 r2 = new com.heyzap.sdk.HeyzapLib$1
            r2.<init>(r3, r0)
            com.heyzap.sdk.Utils.runOnlyOnce(r3, r1, r2)
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: com.heyzap.sdk.HeyzapLib.sendNotification(android.content.Context):void");
    }

    public static void setFlags(int i) {
        flags |= i;
    }
}
