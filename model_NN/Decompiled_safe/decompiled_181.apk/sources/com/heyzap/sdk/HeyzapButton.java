package com.heyzap.sdk;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;

public class HeyzapButton extends ImageButton {

    private final class ButtonOnClickListener implements View.OnClickListener {
        private ButtonOnClickListener() {
        }

        public void onClick(View view) {
            HeyzapLib.rawCheckin(HeyzapButton.this.getContext().getApplicationContext(), null);
        }
    }

    public HeyzapButton(Context context) {
        super(context);
        init(context);
    }

    public HeyzapButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(context);
    }

    public HeyzapButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init(context);
    }

    public void init(Context context) {
        if (!Utils.isSupported(context)) {
            setVisibility(4);
            return;
        }
        HeyzapLib.sendNotification(context);
        setBackgroundColor(0);
        setAdjustViewBounds(true);
        drawableStateChanged();
        setOnClickListener(new ButtonOnClickListener());
        HeyzapAnalytics.trackEvent(context, "checkin-button-shown");
        HeyzapLib.broadcastEnableSDK(context);
        Drawables.setImageDrawable(context, this, "heyzap_button.png");
    }
}
