package com.heyzap.sdk;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;

class Utils {
    private static float density = -1.0f;

    Utils() {
    }

    public static String capitalize(String str) {
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

    public static int dpToPx(Context context, int i) {
        density = density > 0.0f ? density : context.getResources().getDisplayMetrics().density;
        return (int) ((((float) i) * density) + 0.5f);
    }

    public static String getAppLabel(Context context) {
        CharSequence charSequence;
        try {
            charSequence = context.getPackageManager().getApplicationLabel(context.getPackageManager().getApplicationInfo(context.getPackageName(), 0));
        } catch (PackageManager.NameNotFoundException e) {
            charSequence = null;
        }
        if (charSequence == null) {
            return null;
        }
        return charSequence.toString();
    }

    public static boolean isSupported(Context context) {
        try {
            if (Integer.parseInt(Build.VERSION.SDK) < 7) {
                return false;
            }
            return !context.getPackageManager().queryIntentActivities(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.heyzap.android")), 65536).isEmpty();
        } catch (Exception e) {
            return false;
        }
    }

    public static void runOnlyOnce(final Context context, final String str, final Runnable runnable) {
        new Thread(new Runnable() {
            public void run() {
                if (context.getSharedPreferences("HeyzapFirstRun", 0).getBoolean(str, true)) {
                    runnable.run();
                    context.getSharedPreferences("HeyzapFirstRun", 0).edit().putBoolean(str, false).commit();
                }
            }
        }).start();
    }

    public static String truncate(String str, int i) {
        return str.length() > i ? str.substring(0, i) + "..." : str;
    }
}
