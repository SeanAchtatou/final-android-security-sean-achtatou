package net.droidstick.finger;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.util.concurrent.ArrayBlockingQueue;

public class LunarView extends SurfaceView implements SurfaceHolder.Callback {
    public static final int INPUT_QUEUE_SIZE = 30;
    public static final int REQUEST_CODE_PAUSE = 51;
    public static final int RESULT_CODE_TO_MAIN_MENU = 52;
    private ArrayBlockingQueue<InputObject> inputObjectPool;
    private boolean mFirstRun = true;
    private GameActivity mGameActivity;
    private LunarThread mLunarThread;

    public LunarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        this.mLunarThread = new LunarThread(holder, context, this);
        setFocusable(true);
    }

    public LunarThread getThread() {
        return this.mLunarThread;
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (this.mLunarThread.getGameState() != 4 && this.mLunarThread.getGameState() != 12 && this.mLunarThread.getGameState() != 13 && this.mLunarThread.getGameState() != 14 && this.mLunarThread.getGameState() != 11 && this.mLunarThread.getGameState() != 1 && this.mLunarThread.getGameState() != 15) {
            return true;
        }
        if (!isShown()) {
            return true;
        }
        try {
            int hist = event.getHistorySize();
            if (hist > 0) {
                for (int i = 0; i < hist; i++) {
                    if (!this.inputObjectPool.isEmpty()) {
                        InputObject input = this.inputObjectPool.take();
                        input.useEventHistory(event, i);
                        this.mLunarThread.feedInput(input);
                    }
                }
            }
            if (!this.inputObjectPool.isEmpty()) {
                InputObject input2 = this.inputObjectPool.take();
                input2.useEvent(event);
                this.mLunarThread.feedInput(input2);
            }
        } catch (InterruptedException e) {
        }
        try {
            Thread.sleep(16);
        } catch (InterruptedException e2) {
        }
        return true;
    }

    public void setGameActivity(GameActivity _gameActivity) {
        this.mGameActivity = _gameActivity;
    }

    public void onWindowFocusChanged(boolean hasWindowFocus) {
        if (!hasWindowFocus) {
            try {
                if (this.mLunarThread.getGameState() == 4 || this.mLunarThread.getGameState() == 12 || this.mLunarThread.getGameState() == 13 || this.mLunarThread.getGameState() == 14 || this.mLunarThread.getGameState() == 11 || this.mLunarThread.getGameState() == 1 || this.mLunarThread.getGameState() == 15) {
                    this.mLunarThread.setState(2, 4);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        this.mLunarThread.setSurfaceSize(width, height);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        if (this.mFirstRun) {
            this.mFirstRun = false;
            this.mGameActivity.mGameActivityHandler.sendEmptyMessage(0);
            if (this.mLunarThread != null) {
                this.mLunarThread.setRunning(true);
                this.mLunarThread.start();
            }
        }
        if (this.mLunarThread != null) {
            this.mLunarThread.resumeGameLoop();
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        try {
            this.mLunarThread.pauseGameLoop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createInputObjectPool() {
        this.inputObjectPool = new ArrayBlockingQueue<>(30);
        for (int i = 0; i < 30; i++) {
            this.inputObjectPool.add(new InputObject(this.inputObjectPool));
        }
    }
}
