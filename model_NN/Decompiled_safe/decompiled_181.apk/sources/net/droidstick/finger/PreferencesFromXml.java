package net.droidstick.finger;

import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import com.openfeint.api.OpenFeint;
import com.openfeint.api.ui.Dashboard;

public class PreferencesFromXml extends PreferenceActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        setResult(-1);
        if (preference.getKey().equals("preference_feint_dashboard")) {
            Dashboard.open();
        } else if (preference.getKey().equals("preference_feint_leaderboards")) {
            Dashboard.openLeaderboards();
        } else if (preference.getKey().equals("preference_feint_achievements")) {
            Dashboard.openAchievements();
        } else if (preference.getKey().equals("preference_feint_logout")) {
            OpenFeint.logoutUser();
        } else if (preference.getKey().equals("preference_help_about")) {
            startActivity(new Intent().setClass(this, About.class));
        } else if (preference.getKey().equals("preference_return_to_title")) {
            setResult(52);
            finish();
        }
        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }
}
