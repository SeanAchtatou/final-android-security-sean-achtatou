package net.droidstick.finger;

import android.view.KeyEvent;
import android.view.MotionEvent;
import java.util.concurrent.ArrayBlockingQueue;

public class InputObject {
    public static final int ACTION_KEY_DOWN = 1;
    public static final int ACTION_KEY_UP = 2;
    public static final int ACTION_TOUCH_DOWN = 3;
    public static final int ACTION_TOUCH_MOVE = 4;
    public static final int ACTION_TOUCH_UP = 5;
    public static final byte EVENT_TYPE_KEY = 1;
    public static final byte EVENT_TYPE_TOUCH = 2;
    public int action;
    public byte eventType;
    public int keyCode;
    public ArrayBlockingQueue<InputObject> pool;
    public long time;
    public int x;
    public int y;

    public InputObject(ArrayBlockingQueue<InputObject> pool2) {
        this.pool = pool2;
    }

    public void useEvent(KeyEvent event) {
        this.eventType = 1;
        switch (event.getAction()) {
            case 0:
                this.action = 1;
                break;
            case 1:
                this.action = 2;
                break;
            default:
                this.action = 0;
                break;
        }
        this.time = event.getEventTime();
        this.keyCode = event.getKeyCode();
    }

    public void useEvent(MotionEvent event) {
        this.eventType = 2;
        switch (event.getAction()) {
            case 0:
                this.action = 3;
                break;
            case 1:
                this.action = 5;
                break;
            case 2:
                this.action = 4;
                break;
            default:
                this.action = 0;
                break;
        }
        this.time = event.getEventTime();
        this.x = (int) event.getX();
        this.y = (int) event.getY();
    }

    public void useEventHistory(MotionEvent event, int historyItem) {
        this.eventType = 2;
        this.action = 4;
        this.time = event.getHistoricalEventTime(historyItem);
        this.x = (int) event.getHistoricalX(historyItem);
        this.y = (int) event.getHistoricalY(historyItem);
    }

    public void returnToPool() {
        this.pool.add(this);
    }
}
