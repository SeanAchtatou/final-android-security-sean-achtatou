package net.droidstick.finger;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.mopub.mobileads.MoPubView;

public class MoreGamesActivity extends Activity implements AdapterView.OnItemClickListener {
    /* access modifiers changed from: private */
    public static final String[] DATA = {"...", "...", "...", "...", "...", "...", "...", "...", "..."};
    View.OnClickListener mBackListener = new View.OnClickListener() {
        public void onClick(View v) {
            MoreGamesActivity.this.finish();
        }
    };
    private Button mButtonBack;
    private Typeface mFace;
    private TextView mMoreTitle;

    private static class EfficientAdapter extends BaseAdapter {
        private Typeface mFace2;
        private Bitmap mIconBtb;
        private Bitmap mIconClick;
        private Bitmap mIconCpingpong;
        private Bitmap mIconDsg;
        private Bitmap mIconDsg2;
        private Bitmap mIconFinger;
        private Bitmap mIconPaddleBounce;
        private Bitmap mIconPicross;
        private Bitmap mIconShuriken;
        private LayoutInflater mInflater;

        public EfficientAdapter(Context context) {
            this.mFace2 = Typeface.createFromAsset(context.getAssets(), "tribal.ttf");
            this.mInflater = LayoutInflater.from(context);
            this.mIconPaddleBounce = BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_ad_paddlebounce);
            this.mIconShuriken = BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_ad_shuriken);
            this.mIconPicross = BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_ad_stomp);
            this.mIconDsg = BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_ad_dsg);
            this.mIconDsg2 = BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_ad_dsg2);
            this.mIconBtb = BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_ad_btb);
            this.mIconCpingpong = BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_ad_cpingpong);
            this.mIconFinger = BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_ad_finger);
            this.mIconClick = BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_ad_click);
        }

        public int getCount() {
            return MoreGamesActivity.DATA.length;
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = this.mInflater.inflate((int) R.layout.list_item_icon_text, (ViewGroup) null);
                holder = new ViewHolder();
                holder.text = (TextView) convertView.findViewById(R.id.list_text);
                holder.text2 = (TextView) convertView.findViewById(R.id.list_text2);
                holder.icon = (ImageView) convertView.findViewById(R.id.list_icon);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            switch (position) {
                case 0:
                    holder.text.setText("Flick Ninja");
                    holder.text2.setText("FREE");
                    holder.text.setTypeface(this.mFace2);
                    holder.icon.setImageBitmap(this.mIconShuriken);
                    break;
                case 1:
                    holder.text.setText("~Dynamite Defuse~");
                    holder.text2.setText("FREE");
                    holder.text.setTypeface(this.mFace2);
                    holder.icon.setImageBitmap(this.mIconPicross);
                    break;
                case 2:
                    holder.text.setText("~Slide Pencil Lead~");
                    holder.text2.setText("FREE");
                    holder.text.setTypeface(this.mFace2);
                    holder.icon.setImageBitmap(this.mIconDsg);
                    break;
                case 3:
                    holder.text.setText("~Paddle Bounce~");
                    holder.text2.setText("FREE");
                    holder.text.setTypeface(this.mFace2);
                    holder.icon.setImageBitmap(this.mIconPaddleBounce);
                    break;
                case 4:
                    holder.text.setText("~Balance the Bomb~");
                    holder.text2.setText("FREE");
                    holder.text.setTypeface(this.mFace2);
                    holder.icon.setImageBitmap(this.mIconBtb);
                    break;
                case 5:
                    holder.text.setText("~Load Pencil Lead~");
                    holder.text2.setText("FREE");
                    holder.text.setTypeface(this.mFace2);
                    holder.icon.setImageBitmap(this.mIconDsg2);
                    break;
                case 6:
                    holder.text.setText("~Grab me if you can~");
                    holder.text2.setText("FREE");
                    holder.text.setTypeface(this.mFace2);
                    holder.icon.setImageBitmap(this.mIconFinger);
                    break;
                case 7:
                    holder.text.setText("~World Fastest Clicker~");
                    holder.text2.setText("FREE");
                    holder.text.setTypeface(this.mFace2);
                    holder.icon.setImageBitmap(this.mIconClick);
                    break;
                case 8:
                    holder.text.setText("~Paddle Bounce G~");
                    holder.text2.setText("FREE");
                    holder.text.setTypeface(this.mFace2);
                    holder.icon.setImageBitmap(this.mIconCpingpong);
                    break;
            }
            return convertView;
        }

        static class ViewHolder {
            ImageView icon;
            TextView text;
            TextView text2;

            ViewHolder() {
            }
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.linear_layout_9);
        this.mFace = Typeface.createFromAsset(getAssets(), "tribal.ttf");
        ListView list = (ListView) findViewById(R.id.list_moregames);
        list.setAdapter((ListAdapter) new EfficientAdapter(this));
        list.setOnItemClickListener(this);
        this.mButtonBack = (Button) findViewById(R.id.button_moregames_back);
        this.mButtonBack.setTypeface(this.mFace);
        this.mButtonBack.setOnClickListener(this.mBackListener);
        ((TextView) findViewById(R.id.textview_click_to_view)).setTypeface(this.mFace);
        MoPubView mAdView = (MoPubView) findViewById(R.id.adviewmore);
        mAdView.setAdUnitId("agltb3B1Yi1pbmNyDQsSBFNpdGUYj9SgDQw");
        mAdView.loadAd();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Intent intent;
        switch (position) {
            case 0:
                intent = new Intent().setAction("android.intent.action.VIEW").setData(Uri.parse("market://details?id=net.droidstick.shuriken"));
                break;
            case 1:
                intent = new Intent().setAction("android.intent.action.VIEW").setData(Uri.parse("market://details?id=net.droidstick.stomp"));
                break;
            case 2:
                intent = new Intent().setAction("android.intent.action.VIEW").setData(Uri.parse("market://details?id=net.droidstick.dsg"));
                break;
            case 3:
                intent = new Intent().setAction("android.intent.action.VIEW").setData(Uri.parse("market://details?id=net.droidstick.pingpong"));
                break;
            case 4:
                intent = new Intent().setAction("android.intent.action.VIEW").setData(Uri.parse("market://details?id=net.droidstick.whoa"));
                break;
            case 5:
                intent = new Intent().setAction("android.intent.action.VIEW").setData(Uri.parse("market://details?id=net.droidstick.dsg2"));
                break;
            case 6:
                intent = new Intent().setAction("android.intent.action.VIEW").setData(Uri.parse("market://details?id=net.droidstick.finger"));
                break;
            case 7:
                intent = new Intent().setAction("android.intent.action.VIEW").setData(Uri.parse("market://details?id=net.droidstick.click"));
                break;
            case 8:
                intent = new Intent().setAction("android.intent.action.VIEW").setData(Uri.parse("market://details?id=net.droidstick.pb2"));
                break;
            default:
                intent = new Intent().setAction("android.intent.action.VIEW").setData(Uri.parse("market://search?q=pub:DroidStick"));
                break;
        }
        startActivity(intent);
    }
}
