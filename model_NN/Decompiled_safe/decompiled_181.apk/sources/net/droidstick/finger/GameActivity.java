package net.droidstick.finger;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.heyzap.sdk.HeyzapLib;
import com.mopub.mobileads.MoPubView;
import com.openfeint.api.OpenFeint;
import com.openfeint.api.OpenFeintDelegate;
import com.openfeint.api.OpenFeintSettings;
import java.util.HashMap;
import java.util.Map;
import net.droidstick.audio.RokonMusic;
import net.droidstick.audio.SoundManager;
import net.droidstick.finger.Eula;

public class GameActivity extends Activity implements Eula.OnEulaAgreedTo {
    public static final String ACHIEVEMENT_FRIDAY = "887212";
    public static final String ACHIEVEMENT_GODLIKE = "887272";
    public static final String ACHIEVEMENT_GREATREFLEX = "887252";
    public static final String ACHIEVEMENT_MARVELOUS = "887262";
    public static final String ACHIEVEMENT_MONDAY = "887172";
    public static final String ACHIEVEMENT_OUCH = "887162";
    public static final String ACHIEVEMENT_PULL_TOO_FAST = "887152";
    public static final String ACHIEVEMENT_SATURDAY = "887222";
    public static final String ACHIEVEMENT_SUNDAY = "887232";
    public static final String ACHIEVEMENT_THURSDAY = "887202";
    public static final String ACHIEVEMENT_TUESDAY = "887182";
    public static final String ACHIEVEMENT_WEDNESDAY = "887192";
    public static final String ACHIEVEMENT_WELLDONE = "887242";
    public static final String LEADERBOARD_ID = "684446";
    private static final int MENU_EASY = 1;
    private static final int MENU_HARD = 2;
    private static final int MENU_MEDIUM = 3;
    private static final int MENU_PAUSE = 4;
    private static final int MENU_RESUME = 5;
    private static final int MENU_START = 6;
    private static final int MENU_STOP = 7;
    public static final int MSG_FIRST_LOAD_SURFACE_CREATED_DONE = 0;
    public static final int MSG_OPENFEINT_INIT_DONE = 1;
    public static final int REQUEST_CODE_PAUSE = 51;
    public static final int REQUEST_CODE_PREFERENCE = 53;
    public static final int RESULT_CODE_TO_MAIN_MENU = 52;
    static final String gameID = "261803";
    static final String gameKey = "IH3lUKpWJsgcJ9iajcknkQ";
    static final String gameName = "~Grab me if you can~";
    static final String gameSecret = "SWaB8uFi3L7flkL0GsXsfOfU9CwFHHpHHREfaaVxtl8";
    private static GameActivity mGameActivityInstance;
    public Button buttonEasy;
    public Button buttonHard;
    public Button buttonHeyzap;
    public Button buttonMainMenu2;
    public Button buttonNormal;
    public Button buttonPostScoreHeyzap;
    public Button buttonSettings;
    public Button buttonStartGame;
    public LinearLayout mBottomButtonsContainer;
    private Typeface mFace;
    public Handler mGameActivityHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    sendEmptyMessage(1);
                    return;
                case 1:
                    GameActivity.this.firstInit();
                    return;
                default:
                    super.handleMessage(msg);
                    return;
            }
        }
    };
    public TextView mGameOverCombo;
    public LinearLayout mGameOverContainer;
    public LinearLayout mHowtoBlackBarContainer;
    public LinearLayout mLoadingContainer;
    public LunarThread mLunarThread;
    public LunarView mLunarView;
    public LinearLayout mPauseBlackBarContainer;
    View.OnClickListener mSettingsListener = new View.OnClickListener() {
        public void onClick(View v) {
            GameActivity.this.startActivityForResult(new Intent().setClass(GameActivity.this, PreferencesFromXml.class), 53);
        }
    };
    View.OnClickListener mStartGameListener = new View.OnClickListener() {
        public void onClick(View v) {
            GameActivity.this.mLunarThread.doStart();
        }
    };
    public Button resumeButton;
    public Button retryButton;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGameActivityInstance = this;
        setContentView((int) R.layout.surface_view_overlay);
        assignAndHidePrimaryLayouts();
        initOpenFeint();
        SoundManager.getInstance();
        SoundManager.initSounds(this);
        SoundManager.loadSounds();
        HeyzapLib.load(this);
        Eula.show(this);
        MoPubView mAdView = (MoPubView) findViewById(R.id.adviewmain);
        mAdView.setAdUnitId("agltb3B1Yi1pbmNyDQsSBFNpdGUYj9SgDQw");
        mAdView.loadAd();
    }

    public void onEulaAgreedTo() {
    }

    public void firstInit() {
        assignViewHandles();
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        if (this.mLunarThread == null) {
            this.mLunarThread = this.mLunarView.getThread();
        }
        this.mLunarThread.loadGameThreadResources();
        RokonMusic.initRokonMusic(this);
        this.mLunarThread.setState(1);
    }

    private void assignViewHandles() {
        this.mBottomButtonsContainer = (LinearLayout) findViewById(R.id.title_button_container);
        ((TextView) findViewById(R.id.textview_game_paused_title)).setTypeface(this.mFace);
        ((TextView) findViewById(R.id.text_gameover)).setTypeface(this.mFace);
        this.mGameOverCombo = (TextView) findViewById(R.id.text_combo);
        this.mGameOverCombo.setTypeface(this.mFace);
        Button buttonMore = (Button) findViewById(R.id.id_button_more);
        buttonMore.setTypeface(this.mFace);
        buttonMore.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                GameActivity.this.startActivity(new Intent().setClass(GameActivity.this, MoreGamesActivity.class));
            }
        });
        this.resumeButton = (Button) findViewById(R.id.button_resume_game);
        this.resumeButton.setTypeface(this.mFace);
        this.resumeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (GameActivity.this.mLunarThread.getGameState() == 2) {
                    GameActivity.this.mLunarThread.setState(9);
                }
            }
        });
        this.buttonStartGame = (Button) findViewById(R.id.id_button_startgame);
        this.buttonStartGame.setTypeface(this.mFace);
        this.buttonStartGame.setOnClickListener(this.mStartGameListener);
        this.retryButton = (Button) findViewById(R.id.button_retry);
        this.retryButton.setTypeface(this.mFace);
        this.retryButton.setOnClickListener(this.mStartGameListener);
        ((TextView) findViewById(R.id.text_howto_00)).setTypeface(this.mFace);
        ((TextView) findViewById(R.id.text_howto_01)).setTypeface(this.mFace);
        ((TextView) findViewById(R.id.text_howto_02)).setTypeface(this.mFace);
        this.buttonSettings = (Button) findViewById(R.id.id_button_settings);
        this.buttonSettings.setTypeface(this.mFace);
        this.buttonSettings.setOnClickListener(this.mSettingsListener);
    }

    public void assignAndHidePrimaryLayouts() {
        this.mFace = Typeface.createFromAsset(getAssets(), "tribal.ttf");
        this.mLunarView = (LunarView) findViewById(R.id.lunar);
        this.mLunarView.setGameActivity(this);
        this.mLunarView.createInputObjectPool();
        this.mLunarThread = this.mLunarView.getThread();
        this.mLunarThread.setGameActivity(this);
        this.buttonHeyzap = (Button) findViewById(R.id.button_heyzap);
        this.buttonHeyzap.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                HeyzapLib.checkin(GameActivity.this);
            }
        });
        this.buttonPostScoreHeyzap = (Button) findViewById(R.id.button_postscore_heyzap);
        this.buttonPostScoreHeyzap.setTypeface(this.mFace);
        this.buttonPostScoreHeyzap.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                HeyzapLib.checkin(GameActivity.this, "I just avoid being grabbed " + LunarThread.heyZapScoreToPost + " times in a row!");
                GameActivity.this.buttonPostScoreHeyzap.setVisibility(8);
            }
        });
        this.mBottomButtonsContainer = (LinearLayout) findViewById(R.id.title_button_container);
        this.mPauseBlackBarContainer = (LinearLayout) findViewById(R.id.pause_blackbar_layout);
        this.mHowtoBlackBarContainer = (LinearLayout) findViewById(R.id.howto_blackbar_container);
        this.mGameOverContainer = (LinearLayout) findViewById(R.id.gameover_blackbar_container);
        this.mLoadingContainer = (LinearLayout) findViewById(R.id.loading_blackbar_container);
        ((TextView) findViewById(R.id.text_loading)).setTypeface(this.mFace);
        setUILayersGone();
        this.mLoadingContainer.setVisibility(0);
    }

    public void setUILayersGone() {
        try {
            this.mBottomButtonsContainer.setVisibility(8);
            this.mPauseBlackBarContainer.setVisibility(8);
            this.mHowtoBlackBarContainer.setVisibility(8);
            this.mGameOverContainer.setVisibility(8);
            this.mLoadingContainer.setVisibility(8);
            this.buttonHeyzap.setVisibility(8);
            this.buttonPostScoreHeyzap.setVisibility(8);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        int state = 1;
        if (this.mLunarThread != null) {
            state = this.mLunarThread.getGameState();
        }
        switch (event.getKeyCode()) {
            case 4:
                if (state == 2) {
                    this.mLunarThread.setState(9);
                    return true;
                } else if (state == 1) {
                    try {
                        this.mLunarThread.setState(10);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        releaseGameResources();
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    finish();
                    return true;
                } else {
                    this.mLunarThread.setState(1);
                    return true;
                }
            case 82:
                if (this.mLunarThread.getGameState() != 4 && this.mLunarThread.getGameState() != 12 && this.mLunarThread.getGameState() != 13 && this.mLunarThread.getGameState() != 14 && this.mLunarThread.getGameState() != 11 && this.mLunarThread.getGameState() != 1 && this.mLunarThread.getGameState() != 15) {
                    return false;
                }
                this.mLunarThread.setState(2, 4);
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        try {
            SoundManager.stopAllStream();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        if (this.mLunarThread == null) {
            this.mLunarThread = this.mLunarView.getThread();
        }
        try {
            if (this.mLunarThread.mIsMusicOn) {
                RokonMusic.onPause();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.mLunarThread == null) {
            this.mLunarThread = this.mLunarView.getThread();
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == 53 && resultCode == 52) {
            this.mLunarThread.setState(1);
        }
    }

    public void showInterruptScreen() {
        this.mBottomButtonsContainer.setVisibility(0);
        this.mPauseBlackBarContainer.setVisibility(0);
        this.buttonStartGame.setVisibility(8);
        this.retryButton.setVisibility(8);
        this.resumeButton.setVisibility(0);
        this.buttonHeyzap.setVisibility(0);
        this.buttonPostScoreHeyzap.setVisibility(8);
    }

    public void hideInterruptScreen() {
        this.mBottomButtonsContainer.setVisibility(8);
        this.mPauseBlackBarContainer.setVisibility(8);
        this.buttonHeyzap.setVisibility(8);
    }

    private void initOpenFeint() {
        Map<String, Object> options = new HashMap<>();
        options.put(OpenFeintSettings.SettingCloudStorageCompressionStrategy, OpenFeintSettings.CloudStorageCompressionStrategyDefault);
        OpenFeint.initializeWithoutLoggingIn(this, new OpenFeintSettings(gameName, gameKey, gameSecret, gameID, options), new OpenFeintDelegate() {
        });
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        releaseGameResources();
    }

    public void releaseGameResources() {
        try {
            SoundManager.stopAllStream();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        try {
            RokonMusic.onDestroy();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            SoundManager.cleanup();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        try {
            this.mLunarThread.setRunning(false);
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        this.mLunarThread = null;
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
