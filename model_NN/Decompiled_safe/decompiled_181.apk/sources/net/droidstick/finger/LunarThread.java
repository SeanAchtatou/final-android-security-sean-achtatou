package net.droidstick.finger;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.view.SurfaceHolder;
import com.openfeint.api.resource.Achievement;
import com.openfeint.api.resource.Leaderboard;
import com.openfeint.api.resource.Score;
import java.util.Calendar;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import net.droidstick.audio.RokonMusic;
import net.droidstick.audio.SoundManager;
import net.droidstick.gameobject.TookEmo;
import net.droidstick.gameobject.TutorialMessage;

public class LunarThread extends Thread {
    public static final int DIFFICULTY_EASY = 0;
    public static final int DIFFICULTY_HARD = 1;
    public static final int DIFFICULTY_MEDIUM = 2;
    public static final String KEY_CURRENT_SCORE = "key_current_score";
    private static final String KEY_DIFFICULTY = "mDifficulty";
    private static final String KEY_DX = "mDX";
    private static final String KEY_DY = "mDY";
    private static final String KEY_FUEL = "mFuel";
    private static final String KEY_GOAL_ANGLE = "mGoalAngle";
    private static final String KEY_GOAL_SPEED = "mGoalSpeed";
    private static final String KEY_GOAL_WIDTH = "mGoalWidth";
    private static final String KEY_GOAL_X = "mGoalX";
    private static final String KEY_HEADING = "mHeading";
    private static final String KEY_LANDER_HEIGHT = "mLanderHeight";
    private static final String KEY_LANDER_WIDTH = "mLanderWidth";
    public static final String KEY_LOCAL_HIGHSCORE = "key_local_highscore";
    public static final String KEY_MILLISEC = "key_millisec";
    private static final String KEY_WINS = "mWinsInARow";
    private static final String KEY_X = "mX";
    private static final String KEY_Y = "mY";
    public static final int PHYS_DOWN_ACCEL_SEC = 35;
    public static final int PHYS_FIRE_ACCEL_SEC = 80;
    public static final int PHYS_FUEL_INIT = 60;
    public static final int PHYS_FUEL_MAX = 100;
    public static final int PHYS_FUEL_SEC = 10;
    public static final int PHYS_SLEW_SEC = 120;
    public static final int PHYS_SPEED_HYPERSPACE = 180;
    public static final int PHYS_SPEED_INIT = 30;
    public static final int PHYS_SPEED_MAX = 120;
    public static final float REF_HEIGHT = 800.0f;
    public static final float REF_HEIGHT_ABOVE_ADS = 752.0f;
    public static final float REF_HEIGHT_ABOVE_ADS_AND_BUTTON = 704.0f;
    public static final float REF_WIDTH = 480.0f;
    public static final int STATE_BACK_FROM_PAUSE_MENU = 9;
    public static final int STATE_DOAN_JUB = 12;
    public static final int STATE_FINISHING = 10;
    public static final int STATE_GAMEOVER = 6;
    public static final int STATE_JA_JUB = 14;
    public static final int STATE_JIM = 11;
    public static final int STATE_JUB_MAI_DOAN = 13;
    public static final int STATE_LOSE = 8;
    public static final int STATE_PAUSE_MENU = 2;
    public static final int STATE_PLAYING = 4;
    public static final int STATE_PRE_GAMEOVER = 5;
    public static final int STATE_PULL_OUT_TOO_FAST = 15;
    public static final int STATE_READY = 3;
    public static final int STATE_TITLE = 1;
    public static final int STATE_WIN = 7;
    public static final int TARGET_ANGLE = 18;
    public static final int TARGET_BOTTOM_PADDING = 17;
    public static final int TARGET_PAD_HEIGHT = 8;
    public static final int TARGET_SPEED = 28;
    public static final double TARGET_WIDTH = 1.6d;
    public static final int UI_BAR = 100;
    public static final int UI_BAR_HEIGHT = 10;
    public static int heyZapScoreToPost;
    private final int BOMB_NUM = 4;
    private final int DY = 30;
    private final float LENGTH_PRE_GAMEOVER = 0.6f;
    private final float SHAKE_INTERVAL = 0.01f;
    private final int TOUCH_STATE_NOT_TOUCHING = 1;
    private final int TOUCH_STATE_TOUCHING = 0;
    private boolean gameOverDoanJub = true;
    private ArrayBlockingQueue<InputObject> inputQueue = new ArrayBlockingQueue<>(30);
    private Object inputQueueMutex = new Object();
    private int lengthUntilStopMillis = 10000;
    private int mCanvasHeight = 1;
    private int mCanvasWidth = 1;
    private Context mContext;
    private Drawable mCrashedImage;
    public int mCurrentScore;
    private double mDX;
    private double mDY;
    public float mDrawScaleH;
    public float mDrawScaleW;
    private int mDsgGameState;
    private boolean mEngineFiring;
    private Typeface mFace;
    private Drawable mFiringImage;
    private double mFuel;
    GameActivity mGameActivity;
    private int mGoalAngle;
    private int mGoalSpeed;
    private int mGoalWidth;
    private int mGoalX;
    private Bitmap mHandBg;
    private Bitmap mHandBg01;
    private Bitmap mHandBg02;
    private Bitmap mHandBg03;
    private double mHeading;
    public float mInputScaleH;
    public float mInputScaleW;
    public boolean mIsMusicOn;
    public boolean mIsSoundOn;
    public boolean mIsVibrationOn;
    private float mJubTime = 7.0f;
    private int mLanderHeight;
    private int mLanderWidth;
    private long mLastTime;
    public int mLocalHighScore;
    public LunarView mLunarView;
    private int mMilliSec;
    public int mNumActiveBomb = 1;
    SharedPreferences.OnSharedPreferenceChangeListener mOnSPListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            if (key.equals("gender_preference")) {
                return;
            }
            if (key.equals("sound_preference")) {
                LunarThread.this.prefChangedSound(sharedPreferences.getBoolean(key, true));
            } else if (key.equals("music_preference")) {
                LunarThread.this.prefChangedMusic(sharedPreferences.getBoolean(key, true));
            } else if (key.equals("vibration_preference")) {
                LunarThread.this.prefChangedVibration(sharedPreferences.getBoolean(key, true));
            }
        }
    };
    private Random mRandom;
    private int mRotating;
    private boolean mRun = false;
    private Paint mScorePaint;
    private RectF mScratchRect;
    private float mShakeInterval;
    public float mShakeX;
    public float mShakeY;
    public SharedPreferences mSharedPreferences;
    private int mStateBeforePause = 1;
    private float mStateTimeElapsed;
    private float mStompTimerSeconds = -1.0f;
    String mStringSeconds;
    String mStringTimes;
    private SurfaceHolder mSurfaceHolder;
    private TookEmo mTookEmo;
    private int mTouchState = 1;
    private TutorialMessage mTutor;
    private Vibrator mVibrator;
    private double mX;
    private double mY;
    private Object mutex = new Object();
    private boolean pleaseWait;

    public int getmCanvasHeight() {
        return this.mCanvasHeight;
    }

    public int getmCanvasWidth() {
        return this.mCanvasWidth;
    }

    public LunarThread(SurfaceHolder surfaceHolder, Context context, LunarView _lunarView) {
        this.mSurfaceHolder = surfaceHolder;
        this.mContext = context;
        this.mLunarView = _lunarView;
    }

    public void doStart() {
        synchronized (this.mutex) {
            this.mLastTime = System.currentTimeMillis() + 100;
            setState(4);
        }
    }

    public void loadGameThreadResources() {
        this.mFace = Typeface.createFromAsset(this.mContext.getAssets(), "tribal.ttf");
        Resources res = this.mContext.getResources();
        this.mStringSeconds = res.getString(R.string.avoided);
        this.mStringTimes = res.getString(R.string.times);
        this.mHandBg = BitmapFactory.decodeResource(res, R.drawable.img_hand_00);
        this.mHandBg01 = BitmapFactory.decodeResource(res, R.drawable.img_hand_01);
        this.mHandBg02 = BitmapFactory.decodeResource(res, R.drawable.img_hand_02);
        this.mHandBg03 = BitmapFactory.decodeResource(res, R.drawable.img_hand_03);
        this.mScorePaint = new Paint();
        this.mScorePaint.setAntiAlias(true);
        this.mScorePaint.setTextSize(30.0f);
        this.mScorePaint.setTypeface(this.mFace);
        this.mScratchRect = new RectF(0.0f, 0.0f, 0.0f, 0.0f);
        this.mX = (double) this.mLanderWidth;
        this.mY = (double) (this.mLanderHeight * 2);
        this.mFuel = 60.0d;
        this.mDX = 0.0d;
        this.mDY = 0.0d;
        this.mHeading = 0.0d;
        this.mEngineFiring = true;
        this.mStateBeforePause = this.mDsgGameState;
        this.mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.mContext);
        this.mSharedPreferences.registerOnSharedPreferenceChangeListener(this.mOnSPListener);
        this.mIsSoundOn = this.mSharedPreferences.getBoolean("sound_preference", true);
        this.mIsMusicOn = this.mSharedPreferences.getBoolean("music_preference", true);
        this.mIsVibrationOn = this.mSharedPreferences.getBoolean("vibration_preference", true);
        this.mCurrentScore = this.mSharedPreferences.getInt(KEY_CURRENT_SCORE, 0);
        this.mLocalHighScore = this.mSharedPreferences.getInt(KEY_LOCAL_HIGHSCORE, 0);
        this.mRandom = new Random();
        this.mTookEmo = new TookEmo(this.mGameActivity, R.drawable.took);
        this.mTutor = new TutorialMessage(this.mGameActivity, R.drawable.img_tut01);
        this.mVibrator = (Vibrator) this.mGameActivity.getSystemService("vibrator");
    }

    public void pause(int _stateBeforePause) {
        synchronized (this.mutex) {
            this.mStateBeforePause = _stateBeforePause;
            if (this.mIsMusicOn) {
                RokonMusic.onPause();
            }
            SoundManager.stopAllStream();
            pauseGameLoop();
            this.mGameActivity.showInterruptScreen();
        }
    }

    public void unpause() {
        synchronized (this.mutex) {
            this.mDsgGameState = this.mStateBeforePause;
            this.mLastTime = System.currentTimeMillis() + 100;
            resumeGameLoop();
            this.mGameActivity.hideInterruptScreen();
        }
    }

    public void run() {
        while (this.mRun) {
            synchronized (this) {
                while (this.pleaseWait) {
                    try {
                        wait();
                    } catch (Exception e) {
                    }
                }
            }
            Canvas c = this.mSurfaceHolder.lockCanvas(null);
            if (c != null) {
                processInput();
                updatePhysics();
                doDraw(c);
                this.mSurfaceHolder.unlockCanvasAndPost(c);
            }
        }
    }

    public void pauseGameLoop() {
        synchronized (this) {
            this.pleaseWait = true;
        }
    }

    public void resumeGameLoop() {
        synchronized (this) {
            this.pleaseWait = false;
            notify();
        }
    }

    public void setFiring(boolean firing) {
        synchronized (this.mutex) {
            this.mEngineFiring = firing;
        }
    }

    public void setRunning(boolean b) {
        this.mRun = b;
    }

    public int getGameState() {
        int i;
        synchronized (this.mutex) {
            i = this.mDsgGameState;
        }
        return i;
    }

    public void setState(int mode) {
        synchronized (this.mutex) {
            setState(mode, 1);
        }
    }

    public void setState(int mode, int _stateBeforePause) {
        synchronized (this.mutex) {
            this.mDsgGameState = mode;
            this.mStateTimeElapsed = 0.0f;
            switch (this.mDsgGameState) {
                case 1:
                    this.mGameActivity.mGameActivityHandler.post(new Runnable() {
                        public void run() {
                            LunarThread.this.mGameActivity.setUILayersGone();
                            LunarThread.this.mGameActivity.mBottomButtonsContainer.setVisibility(0);
                            LunarThread.this.mGameActivity.resumeButton.setVisibility(8);
                            LunarThread.this.mGameActivity.retryButton.setVisibility(8);
                            LunarThread.this.mGameActivity.buttonStartGame.setVisibility(0);
                            LunarThread.this.mGameActivity.buttonHeyzap.setVisibility(0);
                            LunarThread.this.mGameActivity.buttonPostScoreHeyzap.setVisibility(8);
                        }
                    });
                    if (this.mIsMusicOn) {
                        RokonMusic.stopAllPlayers();
                        break;
                    }
                    break;
                case 2:
                    pause(_stateBeforePause);
                    break;
                case 4:
                    this.mGameActivity.mGameActivityHandler.post(new Runnable() {
                        public void run() {
                            LunarThread.this.mGameActivity.setUILayersGone();
                        }
                    });
                    if (this.mIsMusicOn) {
                        RokonMusic.stopAllPlayers();
                    }
                    if (this.mCurrentScore == 0) {
                        this.mTutor.setObjectState(1);
                        break;
                    }
                    break;
                case 6:
                    SharedPreferences.Editor editor2 = this.mGameActivity.mLunarThread.mSharedPreferences.edit();
                    editor2.putInt(KEY_CURRENT_SCORE, 0);
                    this.mGameActivity.mLunarThread.mLocalHighScore = this.mGameActivity.mLunarThread.mSharedPreferences.getInt(KEY_LOCAL_HIGHSCORE, 0);
                    if (this.mGameActivity.mLunarThread.mCurrentScore > this.mGameActivity.mLunarThread.mLocalHighScore) {
                        this.mGameActivity.mLunarThread.mLocalHighScore = this.mGameActivity.mLunarThread.mCurrentScore;
                        editor2.putInt(KEY_LOCAL_HIGHSCORE, this.mGameActivity.mLunarThread.mLocalHighScore);
                    }
                    editor2.commit();
                    if (this.mGameActivity.mLunarThread.mCurrentScore >= 3) {
                        benPostScore(this.mGameActivity.mLunarThread.mCurrentScore, GameActivity.LEADERBOARD_ID);
                    }
                    this.mGameActivity.mGameActivityHandler.post(new Runnable() {
                        public void run() {
                            LunarThread.this.mGameActivity.setUILayersGone();
                            LunarThread.this.mGameActivity.mGameOverContainer.setVisibility(0);
                            LunarThread.this.mGameActivity.mGameOverCombo.setText(String.valueOf(LunarThread.this.mStringSeconds) + " " + LunarThread.this.mCurrentScore + " " + LunarThread.this.mStringTimes);
                            LunarThread.this.mGameActivity.mBottomButtonsContainer.setVisibility(0);
                            LunarThread.this.mGameActivity.resumeButton.setVisibility(8);
                            LunarThread.this.mGameActivity.retryButton.setVisibility(0);
                            LunarThread.this.mGameActivity.buttonStartGame.setVisibility(8);
                            LunarThread.this.mGameActivity.buttonHeyzap.setVisibility(0);
                            LunarThread.this.mGameActivity.buttonPostScoreHeyzap.setVisibility(0);
                        }
                    });
                    heyZapScoreToPost = this.mCurrentScore;
                    this.mGameActivity.mLunarThread.mCurrentScore = 0;
                    break;
                case 9:
                    unpause();
                    break;
                case STATE_JIM /*11*/:
                    if (this.mIsMusicOn) {
                        RokonMusic.playRandom(false);
                    }
                    randomLengthUntilStop();
                    if (this.mCurrentScore == 0) {
                        this.mTutor.setObjectState(2);
                        break;
                    }
                    break;
                case STATE_DOAN_JUB /*12*/:
                    shakeSeconds(0.5f);
                    if (this.mIsVibrationOn) {
                        this.mVibrator.vibrate(500);
                    }
                    if (this.mGameActivity.mLunarThread.mIsSoundOn) {
                        SoundManager.playSound(2, 1.0f);
                    }
                    benUnlockAchievement(GameActivity.ACHIEVEMENT_OUCH);
                    this.mTutor.setObjectState(0);
                    break;
                case STATE_JUB_MAI_DOAN /*13*/:
                    shakeSeconds(0.5f);
                    if (this.mGameActivity.mLunarThread.mIsSoundOn) {
                        SoundManager.playSound(2, 1.0f);
                    }
                    this.mTutor.setObjectState(0);
                    this.mCurrentScore++;
                    if (this.mCurrentScore == 5) {
                        switch (Calendar.getInstance().get(7)) {
                            case 1:
                                benUnlockAchievement(GameActivity.ACHIEVEMENT_SUNDAY);
                                break;
                            case 2:
                                benUnlockAchievement(GameActivity.ACHIEVEMENT_MONDAY);
                                break;
                            case 3:
                                benUnlockAchievement(GameActivity.ACHIEVEMENT_TUESDAY);
                                break;
                            case 4:
                                benUnlockAchievement(GameActivity.ACHIEVEMENT_WEDNESDAY);
                                break;
                            case 5:
                                benUnlockAchievement(GameActivity.ACHIEVEMENT_THURSDAY);
                                break;
                            case 6:
                                benUnlockAchievement(GameActivity.ACHIEVEMENT_FRIDAY);
                                break;
                            case 7:
                                benUnlockAchievement(GameActivity.ACHIEVEMENT_SATURDAY);
                                break;
                        }
                    } else if (this.mCurrentScore == 10) {
                        benUnlockAchievement(GameActivity.ACHIEVEMENT_WELLDONE);
                    } else if (this.mCurrentScore == 15) {
                        benUnlockAchievement(GameActivity.ACHIEVEMENT_GREATREFLEX);
                    } else if (this.mCurrentScore == 20) {
                        benUnlockAchievement(GameActivity.ACHIEVEMENT_MARVELOUS);
                    } else if (this.mCurrentScore == 25) {
                        benUnlockAchievement(GameActivity.ACHIEVEMENT_GODLIKE);
                    }
                    SharedPreferences.Editor editor = this.mGameActivity.mLunarThread.mSharedPreferences.edit();
                    editor.putInt(KEY_CURRENT_SCORE, this.mGameActivity.mLunarThread.mCurrentScore);
                    this.mGameActivity.mLunarThread.mLocalHighScore = this.mGameActivity.mLunarThread.mSharedPreferences.getInt(KEY_LOCAL_HIGHSCORE, 0);
                    if (this.mGameActivity.mLunarThread.mCurrentScore > this.mGameActivity.mLunarThread.mLocalHighScore) {
                        this.mGameActivity.mLunarThread.mLocalHighScore = this.mGameActivity.mLunarThread.mCurrentScore;
                        editor.putInt(KEY_LOCAL_HIGHSCORE, this.mGameActivity.mLunarThread.mLocalHighScore);
                    }
                    editor.commit();
                    break;
                case STATE_JA_JUB /*14*/:
                    RokonMusic.stopAllPlayers();
                    this.mJubTime = 0.7f - (((float) this.mCurrentScore) * 0.04f);
                    if (this.mJubTime < 0.5f) {
                        this.mJubTime = 0.5f;
                        break;
                    }
                    break;
                case STATE_PULL_OUT_TOO_FAST /*15*/:
                    benUnlockAchievement(GameActivity.ACHIEVEMENT_PULL_TOO_FAST);
                    this.mTutor.setObjectState(0);
                    break;
            }
        }
    }

    private void shakeSeconds(float seconds) {
        this.mStompTimerSeconds = seconds;
    }

    private void randomLengthUntilStop() {
        this.mMilliSec = 0;
        if (this.mCurrentScore == 0) {
            this.lengthUntilStopMillis = this.mMilliSec + 3000 + this.mRandom.nextInt(12001);
        } else {
            this.lengthUntilStopMillis = this.mMilliSec + 1000 + this.mRandom.nextInt(14001);
        }
    }

    public void setSurfaceSize(int width, int height) {
        synchronized (this.mutex) {
            this.mCanvasWidth = width;
            this.mCanvasHeight = height;
            this.mDrawScaleW = ((float) this.mCanvasWidth) / 480.0f;
            this.mInputScaleW = 480.0f / ((float) this.mCanvasWidth);
            this.mDrawScaleH = ((float) this.mCanvasHeight) / 800.0f;
            this.mInputScaleH = 800.0f / ((float) this.mCanvasHeight);
        }
    }

    private void doDraw(Canvas canvas) {
        if (this.mDsgGameState != 0 && canvas != null) {
            canvas.save();
            canvas.translate(this.mShakeX, this.mShakeY);
            switch (this.mDsgGameState) {
                case 1:
                    canvas.save();
                    drawBG(canvas);
                    canvas.restore();
                    break;
                case 2:
                    drawBG(canvas);
                    drawScore(canvas);
                    break;
                case 4:
                case STATE_JIM /*11*/:
                case STATE_DOAN_JUB /*12*/:
                case STATE_JUB_MAI_DOAN /*13*/:
                case STATE_JA_JUB /*14*/:
                case STATE_PULL_OUT_TOO_FAST /*15*/:
                    canvas.save();
                    drawBG(canvas);
                    drawScore(canvas);
                    canvas.restore();
                    break;
                case 5:
                case 6:
                    drawBG(canvas);
                    break;
            }
            if (this.mTookEmo != null) {
                this.mTookEmo.draw(canvas);
            }
            if (this.mTutor != null) {
                this.mTutor.draw(canvas);
            }
            canvas.restore();
        }
    }

    private void drawBG(Canvas canvas) {
        switch (this.mDsgGameState) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 9:
            case 10:
            case STATE_PULL_OUT_TOO_FAST /*15*/:
                canvas.save();
                canvas.scale(this.mDrawScaleW, this.mDrawScaleH, 0.0f, 0.0f);
                canvas.drawBitmap(this.mHandBg, 0.0f, 0.0f, (Paint) null);
                canvas.restore();
                return;
            case 5:
                canvas.save();
                canvas.scale(this.mDrawScaleW, this.mDrawScaleH, 0.0f, 0.0f);
                canvas.restore();
                return;
            case 6:
                canvas.save();
                canvas.scale(this.mDrawScaleW, this.mDrawScaleH, 0.0f, 0.0f);
                if (this.gameOverDoanJub) {
                    canvas.drawBitmap(this.mHandBg02, 0.0f, 0.0f, (Paint) null);
                } else {
                    canvas.drawBitmap(this.mHandBg, 0.0f, 0.0f, (Paint) null);
                }
                canvas.restore();
                return;
            case 7:
            case 8:
            default:
                canvas.save();
                canvas.scale(this.mDrawScaleW, this.mDrawScaleH, 0.0f, 0.0f);
                canvas.drawBitmap(this.mHandBg, 0.0f, 0.0f, (Paint) null);
                canvas.restore();
                return;
            case STATE_JIM /*11*/:
            case STATE_JA_JUB /*14*/:
                canvas.save();
                canvas.scale(this.mDrawScaleW, this.mDrawScaleH, 0.0f, 0.0f);
                if (this.mTouchState == 0) {
                    canvas.drawBitmap(this.mHandBg01, 0.0f, 0.0f, (Paint) null);
                } else if (this.mTouchState == 1) {
                    canvas.drawBitmap(this.mHandBg, 0.0f, 0.0f, (Paint) null);
                } else {
                    canvas.drawBitmap(this.mHandBg, 0.0f, 0.0f, (Paint) null);
                }
                canvas.restore();
                return;
            case STATE_DOAN_JUB /*12*/:
                canvas.save();
                canvas.scale(this.mDrawScaleW, this.mDrawScaleH, 0.0f, 0.0f);
                canvas.drawBitmap(this.mHandBg02, 0.0f, 0.0f, (Paint) null);
                canvas.restore();
                return;
            case STATE_JUB_MAI_DOAN /*13*/:
                canvas.save();
                canvas.scale(this.mDrawScaleW, this.mDrawScaleH, 0.0f, 0.0f);
                canvas.drawBitmap(this.mHandBg03, 0.0f, 0.0f, (Paint) null);
                canvas.restore();
                return;
        }
    }

    public void setGameActivity(GameActivity _gameActivity) {
        this.mGameActivity = _gameActivity;
    }

    private void drawScore(Canvas canvas) {
        canvas.save();
        Paint p = this.mScorePaint;
        p.setColor(-16711936);
        p.setTextAlign(Paint.Align.RIGHT);
        canvas.drawText(String.valueOf(this.mStringSeconds) + ": " + this.mCurrentScore, (float) this.mCanvasWidth, 30.0f, p);
        p.setColor(-16776961);
        p.setTextAlign(Paint.Align.LEFT);
        canvas.drawText("Local Best: " + this.mLocalHighScore, 0.0f, 30.0f, p);
        canvas.restore();
    }

    public void feedInput(InputObject input) {
        synchronized (this.inputQueueMutex) {
            try {
                this.inputQueue.put(input);
            } catch (InterruptedException e) {
            }
        }
    }

    private void updateState() {
        switch (this.mDsgGameState) {
        }
    }

    private void updateAI() {
    }

    private void updatePhysics() {
        long now = System.currentTimeMillis();
        if (this.mLastTime <= now) {
            float elapsedSeconds = ((float) (now - this.mLastTime)) / 1000.0f;
            this.mStateTimeElapsed += elapsedSeconds;
            if (this.mStompTimerSeconds > 0.0f) {
                this.mStompTimerSeconds -= elapsedSeconds;
                if (this.mShakeInterval < 0.0f) {
                    this.mShakeX = (float) (this.mRandom.nextBoolean() ? this.mRandom.nextInt((int) (this.mGameActivity.mLunarThread.mDrawScaleW * 15.0f)) : -this.mRandom.nextInt((int) (this.mGameActivity.mLunarThread.mDrawScaleW * 15.0f)));
                    this.mShakeY = (float) (this.mRandom.nextBoolean() ? this.mRandom.nextInt((int) (this.mGameActivity.mLunarThread.mDrawScaleH * 15.0f)) : -this.mRandom.nextInt((int) (this.mGameActivity.mLunarThread.mDrawScaleH * 15.0f)));
                    this.mShakeInterval = 0.01f;
                } else {
                    this.mShakeInterval -= elapsedSeconds;
                }
            } else {
                this.mShakeX = 0.0f;
                this.mShakeY = 0.0f;
            }
            if (this.mTookEmo != null) {
                this.mTookEmo.update(elapsedSeconds);
            }
            if (this.mTutor != null) {
                this.mTutor.update(elapsedSeconds);
            }
            switch (this.mDsgGameState) {
                case 1:
                    this.mNumActiveBomb = 1;
                    break;
                case 5:
                    if (this.mStateTimeElapsed > 0.6f) {
                        setState(6);
                        break;
                    }
                    break;
                case STATE_JIM /*11*/:
                    this.mMilliSec = (int) (((float) this.mMilliSec) + (1000.0f * elapsedSeconds));
                    if (this.mTouchState == 1) {
                        this.mTookEmo.mTookOrPid = 0;
                        this.mTookEmo.setObjectState(2);
                        if (this.mGameActivity.mLunarThread.mIsSoundOn) {
                            SoundManager.playSound(1, 1.0f);
                        }
                        setState(15);
                    }
                    if (this.mMilliSec > this.lengthUntilStopMillis) {
                        setState(14);
                        break;
                    }
                    break;
                case STATE_DOAN_JUB /*12*/:
                    if (this.mStateTimeElapsed > 0.5f && this.mTookEmo.mObjectState == 0) {
                        if (this.mGameActivity.mLunarThread.mIsSoundOn) {
                            SoundManager.playSound(1, 1.0f);
                        }
                        this.mTookEmo.mTookOrPid = 0;
                        this.mTookEmo.setObjectState(2);
                    }
                    if (this.mStateTimeElapsed > 1.2f) {
                        this.gameOverDoanJub = true;
                        setState(6);
                        break;
                    }
                    break;
                case STATE_JUB_MAI_DOAN /*13*/:
                    if (this.mStateTimeElapsed > 0.5f && this.mTookEmo.mObjectState == 0) {
                        if (this.mGameActivity.mLunarThread.mIsSoundOn) {
                            SoundManager.playSound(0, 1.0f);
                        }
                        this.mTookEmo.mTookOrPid = 1;
                        this.mTookEmo.setObjectState(2);
                    }
                    if (this.mStateTimeElapsed > 1.0f) {
                        setState(4);
                        break;
                    }
                    break;
                case STATE_JA_JUB /*14*/:
                    if (this.mStateTimeElapsed > this.mJubTime) {
                        if (this.mTouchState != 0) {
                            if (this.mTouchState != 1) {
                                setState(13);
                                break;
                            } else {
                                setState(13);
                                break;
                            }
                        } else {
                            setState(12);
                            break;
                        }
                    }
                    break;
                case STATE_PULL_OUT_TOO_FAST /*15*/:
                    if (this.mStateTimeElapsed > 1.2f) {
                        this.gameOverDoanJub = false;
                        setState(6);
                        break;
                    }
                    break;
            }
            this.mLastTime = now;
        }
    }

    private void updateAnimations() {
    }

    private void updateSound() {
        switch (this.mDsgGameState) {
        }
    }

    private void processInput() {
        synchronized (this.inputQueueMutex) {
            ArrayBlockingQueue<InputObject> inputQueue2 = this.inputQueue;
            while (!inputQueue2.isEmpty()) {
                try {
                    InputObject input = inputQueue2.take();
                    if (input.eventType == 1) {
                        processKeyEvent(input);
                    } else if (input.eventType == 2) {
                        processMotionEvent(input);
                    }
                    input.returnToPool();
                } catch (InterruptedException e) {
                }
            }
        }
    }

    private void processKeyEvent(InputObject _input) {
    }

    private void processMotionEvent(InputObject _input) {
        float f = ((float) _input.x) * this.mInputScaleW;
        float f2 = ((float) _input.y) * this.mInputScaleH;
        switch (_input.action) {
            case 3:
                this.mTouchState = 0;
                break;
            case 4:
                this.mTouchState = 0;
                break;
            case 5:
                this.mTouchState = 1;
                break;
        }
        switch (this.mDsgGameState) {
            case 1:
            case 2:
            case 3:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case STATE_JIM /*11*/:
            case STATE_DOAN_JUB /*12*/:
            case STATE_JUB_MAI_DOAN /*13*/:
            case STATE_JA_JUB /*14*/:
            default:
                return;
            case 4:
                switch (_input.action) {
                    case 3:
                        setState(11);
                        return;
                    case 4:
                    case 5:
                    default:
                        return;
                }
        }
    }

    public void prefChangedSound(boolean _newValue) {
        this.mIsSoundOn = _newValue;
    }

    public void prefChangedMusic(boolean _newValue) {
        this.mIsMusicOn = _newValue;
        if (this.mIsMusicOn) {
            RokonMusic.allMustResumeTrue();
            if ((this.mDsgGameState == 4 || this.mDsgGameState == 5 || this.mDsgGameState == 6 || this.mDsgGameState == 2) && !RokonMusic.isThisPlayerPlaying(0)) {
                RokonMusic.play(0, true);
            }
        } else if ((this.mDsgGameState == 4 || this.mDsgGameState == 5 || this.mDsgGameState == 6 || this.mDsgGameState == 2) && RokonMusic.isThisPlayerPlaying(0)) {
            RokonMusic.stop(0);
        }
    }

    public void prefChangedVibration(boolean _newValue) {
        this.mIsVibrationOn = _newValue;
    }

    private void benPostScore(int longScoreValue, String _leaderBoardId) {
        new Score((long) longScoreValue, null).submitTo(new Leaderboard(_leaderBoardId), new Score.SubmitToCB() {
            public void onSuccess(boolean newHighScore) {
            }

            public void onFailure(String exceptionMessage) {
            }
        });
    }

    private void benUnlockAchievement(String _id) {
        new Achievement(_id).unlock(new Achievement.UnlockCB() {
            public void onSuccess(boolean newUnlock) {
            }

            public void onFailure(String exceptionMessage) {
            }
        });
    }
}
