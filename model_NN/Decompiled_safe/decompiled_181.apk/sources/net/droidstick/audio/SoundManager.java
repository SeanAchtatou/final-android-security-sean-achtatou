package net.droidstick.audio;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import java.util.HashMap;
import java.util.Random;
import net.droidstick.finger.R;

public class SoundManager {
    public static final int SOUND_INDEX_GRAB = 2;
    public static final int SOUND_INDEX_PID = 1;
    public static final int SOUND_INDEX_TOOK = 0;
    private static SoundManager _instance;
    public static int latestStreamId;
    private static AudioManager mAudioManager;
    private static Context mContext;
    private static Random mRandom = new Random();
    private static SoundPool mSoundPool;
    private static HashMap<Integer, Integer> mSoundPoolMap;

    private SoundManager() {
    }

    public static synchronized SoundManager getInstance() {
        SoundManager soundManager;
        synchronized (SoundManager.class) {
            if (_instance == null) {
                _instance = new SoundManager();
            }
            soundManager = _instance;
        }
        return soundManager;
    }

    public static void initSounds(Context theContext) {
        mContext = theContext;
        mSoundPool = new SoundPool(10, 3, 0);
        mSoundPoolMap = new HashMap<>();
        mAudioManager = (AudioManager) mContext.getSystemService("audio");
    }

    public static void addSound(int Index, int SoundID) {
        mSoundPoolMap.put(Integer.valueOf(Index), Integer.valueOf(mSoundPool.load(mContext, SoundID, 1)));
    }

    public static void loadSounds() {
        mSoundPoolMap.put(0, Integer.valueOf(mSoundPool.load(mContext, R.raw.took, 1)));
        mSoundPoolMap.put(1, Integer.valueOf(mSoundPool.load(mContext, R.raw.pid, 1)));
        mSoundPoolMap.put(2, Integer.valueOf(mSoundPool.load(mContext, R.raw.grab, 1)));
    }

    public static int playSound(int index, float speed) {
        float streamVolume = ((float) mAudioManager.getStreamVolume(3)) / ((float) mAudioManager.getStreamMaxVolume(3));
        latestStreamId = mSoundPool.play(mSoundPoolMap.get(Integer.valueOf(index)).intValue(), streamVolume, streamVolume, 1, 0, speed);
        return latestStreamId;
    }

    public static int playSoundLoop(int index, float speed) {
        float streamVolume = ((float) mAudioManager.getStreamVolume(3)) / ((float) mAudioManager.getStreamMaxVolume(3));
        latestStreamId = mSoundPool.play(mSoundPoolMap.get(Integer.valueOf(index)).intValue(), streamVolume, streamVolume, 1, -1, speed);
        return latestStreamId;
    }

    public static void stopSound(int streamId) {
        if (mSoundPool != null) {
            mSoundPool.stop(streamId);
        }
    }

    public static void stopAllStream() {
        for (int i = 0; i <= latestStreamId + 10; i++) {
            if (mSoundPool != null) {
                mSoundPool.stop(i);
            }
        }
    }

    public static void cleanup() {
        mSoundPool.unload(mSoundPoolMap.get(0).intValue());
        mSoundPool.unload(mSoundPoolMap.get(1).intValue());
        mSoundPool.unload(mSoundPoolMap.get(2).intValue());
        mSoundPool.release();
        mSoundPool = null;
        mSoundPoolMap.clear();
        mAudioManager.unloadSoundEffects();
        _instance = null;
    }
}
