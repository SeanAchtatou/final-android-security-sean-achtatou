package net.droidstick.audio;

import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import java.io.IOException;
import java.util.Random;
import net.droidstick.finger.GameActivity;

public class RokonMusic {
    public static final int PLAYER_INDEX_CANON1 = 2;
    public static final int PLAYER_INDEX_CANON2 = 3;
    public static final int PLAYER_INDEX_DANUBE1 = 0;
    public static final int PLAYER_INDEX_DANUBE2 = 1;
    public static final int PLAYER_INDEX_MARS1 = 4;
    public static final int PLAYER_INDEX_MARS2 = 5;
    public static final int PLAYER_INDEX_RONDO1 = 6;
    public static final int PLAYER_INDEX_RONDO2 = 7;
    public static final int PLAYER_INDEX_WINTER1 = 8;
    public static final int PLAYER_INDEX_WINTER2 = 9;
    private static final int PLAYER_NUM = 10;
    protected static GameActivity gameActivity;
    private static Random mRandom = new Random();
    protected static MediaPlayer[] mediaPlayer;
    public static boolean[] mustResume = new boolean[10];

    private static void prepareMediaPlayer(int playerIndex) {
        if (mediaPlayer[playerIndex] == null) {
            mediaPlayer[playerIndex] = new MediaPlayer();
        }
    }

    private static void prapareAllMediaPlayer() {
        for (int i = 0; i < mediaPlayer.length; i++) {
            mediaPlayer[i] = new MediaPlayer();
        }
    }

    public static void play(int playerIndex) {
        play(playerIndex, false);
    }

    public static void play(int playerIndex, boolean loop) {
        prepareMediaPlayer(playerIndex);
        try {
            AssetFileDescriptor afd = gameActivity.getAssets().openFd(convertIndexToFileName(playerIndex));
            try {
                mediaPlayer[playerIndex].reset();
                mediaPlayer[playerIndex].setLooping(loop);
                mediaPlayer[playerIndex].setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                mediaPlayer[playerIndex].setAudioStreamType(3);
                afd.close();
                try {
                    mediaPlayer[playerIndex].prepare();
                    mediaPlayer[playerIndex].start();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (IOException e2) {
                e2.printStackTrace();
            } catch (IllegalStateException e3) {
                e3.printStackTrace();
            }
        } catch (Exception e4) {
            e4.printStackTrace();
        }
    }

    public static void playRandom(boolean loop) {
        int randomIndex = mRandom.nextInt(10);
        prepareMediaPlayer(randomIndex);
        try {
            AssetFileDescriptor afd = gameActivity.getAssets().openFd(convertIndexToFileName(randomIndex));
            try {
                mediaPlayer[randomIndex].reset();
                mediaPlayer[randomIndex].setLooping(loop);
                mediaPlayer[randomIndex].setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                mediaPlayer[randomIndex].setAudioStreamType(3);
                afd.close();
                try {
                    mediaPlayer[randomIndex].prepare();
                    mediaPlayer[randomIndex].start();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (IOException e2) {
                e2.printStackTrace();
            } catch (IllegalStateException e3) {
                e3.printStackTrace();
            }
        } catch (Exception e4) {
            e4.printStackTrace();
        }
    }

    public static void stop(int playerIndex) {
        if (mediaPlayer != null && mediaPlayer[playerIndex] != null) {
            mediaPlayer[playerIndex].stop();
        }
    }

    public static void reset(int playerIndex) {
        if (mediaPlayer != null && mediaPlayer[playerIndex] != null) {
            mediaPlayer[playerIndex].reset();
        }
    }

    public static boolean isThisPlayerPlaying(int playerIndex) {
        if (mediaPlayer == null) {
            return false;
        }
        if (mediaPlayer[playerIndex] != null) {
            return mediaPlayer[playerIndex].isPlaying();
        }
        return false;
    }

    public static void stopAllPlayers() {
        if (mediaPlayer != null) {
            for (int i = 0; i < mediaPlayer.length; i++) {
                if (mediaPlayer[i] != null) {
                    mediaPlayer[i].stop();
                }
            }
        }
    }

    public static void pause(int playerIndex) {
        if (mediaPlayer != null && mediaPlayer[playerIndex] != null) {
            mediaPlayer[playerIndex].pause();
        }
    }

    public static void onPause() {
        if (mediaPlayer != null) {
            for (int i = 0; i < mediaPlayer.length; i++) {
                if (mediaPlayer[i] != null && mediaPlayer[i].isPlaying()) {
                    mediaPlayer[i].pause();
                    mustResume[i] = true;
                }
            }
        }
    }

    public static void allMustResumeTrue() {
        if (mediaPlayer != null) {
            for (int i = 0; i < mediaPlayer.length; i++) {
                mustResume[i] = true;
            }
        }
    }

    public static void onResume() {
        if (mediaPlayer != null) {
            for (int i = 0; i < mediaPlayer.length; i++) {
                if (mediaPlayer[i] != null && mustResume[i]) {
                    mediaPlayer[i].start();
                    mustResume[i] = false;
                }
            }
        }
    }

    public static MediaPlayer getMediaPlayer(int playerIndex) {
        return mediaPlayer[playerIndex];
    }

    public static void onDestroy() {
        if (mediaPlayer != null) {
            for (int i = 0; i < mediaPlayer.length; i++) {
                if (mediaPlayer[i] != null) {
                    mediaPlayer[i].stop();
                    mediaPlayer[i].release();
                    mediaPlayer[i] = null;
                }
            }
            mediaPlayer = null;
        }
    }

    public static void initRokonMusic(GameActivity ga) {
        gameActivity = ga;
        mediaPlayer = new MediaPlayer[10];
        prapareAllMediaPlayer();
    }

    private static String convertIndexToFileName(int playerIndex) {
        switch (playerIndex) {
            case 0:
                return "bluedanube1.ogg";
            case 1:
                return "bluedanube2.ogg";
            case 2:
                return "canon1.ogg";
            case 3:
                return "canon2.ogg";
            case 4:
                return "mars1.ogg";
            case 5:
                return "mars2.ogg";
            case 6:
                return "rondo1.ogg";
            case 7:
                return "rondo2.ogg";
            case 8:
                return "winter1.ogg";
            case 9:
                return "winter2.ogg";
            default:
                return "...";
        }
    }
}
