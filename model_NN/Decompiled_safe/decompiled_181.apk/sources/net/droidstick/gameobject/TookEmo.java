package net.droidstick.gameobject;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import net.droidstick.finger.GameActivity;
import net.droidstick.finger.R;

public class TookEmo extends GameObject {
    public static final int PID = 0;
    public static final int STATE_FADE_IN = 2;
    public static final int STATE_FADE_IN_LAEW = 3;
    public static final int STATE_FADE_OUT = 4;
    public static final int TOOK = 1;
    private final int FADE_SPEED = 100;
    private final int MOVING_SPEED = 500;
    private float mAlpha = 0.0f;
    protected Bitmap mPidBitmap = BitmapFactory.decodeResource(this.mGameActivity.getResources(), R.drawable.pid);
    public int mTookOrPid = 0;

    public TookEmo(GameActivity _ga, int drawaBleId) {
        super(_ga, drawaBleId);
        setObjectState(0);
    }

    public void touchStart(float x, float y) {
    }

    public void touchMove(float x, float y) {
    }

    public void touchUp(float x, float y) {
    }

    public void setObjectState(int state) {
        this.mObjectState = state;
        this.mStateTimeElapsed = 0.0f;
        switch (this.mObjectState) {
            case 0:
                this.mPaint.setAlpha(0);
                this.mPosX = -500.0f;
                this.mPosY = -500.0f;
                return;
            case 1:
            case 3:
            case 4:
            default:
                return;
            case 2:
                this.mPosX = 390.0f;
                this.mPosY = 350.0f;
                return;
        }
    }

    public void update(float elapsedSeconds) {
        this.mStateTimeElapsed += elapsedSeconds;
        switch (this.mObjectState) {
            case 0:
            default:
                return;
            case 1:
                updateMatrixAndTransformCollisionPoints();
                return;
            case 2:
                this.mPosY -= 500.0f * elapsedSeconds;
                this.mAlpha = (350.0f - this.mPosY) * 2.55f;
                if (this.mAlpha > 255.0f) {
                    this.mAlpha = 255.0f;
                }
                this.mPaint.setAlpha((int) this.mAlpha);
                updateMatrixAndTransformCollisionPoints();
                if (this.mPosY < 250.0f) {
                    this.mPosY = 250.0f;
                    setObjectState(3);
                    return;
                }
                return;
            case 3:
                updateMatrixAndTransformCollisionPoints();
                if (this.mStateTimeElapsed > 0.7f) {
                    setObjectState(4);
                    return;
                }
                return;
            case 4:
                this.mPosY -= 500.0f * elapsedSeconds;
                this.mAlpha = 255.0f - ((250.0f - this.mPosY) * 2.55f);
                if (this.mAlpha < 0.0f) {
                    this.mAlpha = 0.0f;
                }
                this.mPaint.setAlpha((int) this.mAlpha);
                updateMatrixAndTransformCollisionPoints();
                if (this.mPosY < 150.0f) {
                    this.mPosY = 150.0f;
                    setObjectState(0);
                    return;
                }
                return;
        }
    }

    public void draw(Canvas canvas) {
        if (canvas != null) {
            switch (this.mObjectState) {
                case 0:
                default:
                    return;
                case 1:
                case 2:
                case 3:
                case 4:
                    canvas.save();
                    canvas.setMatrix(this.mMatrix);
                    canvas.translate(this.mShakeX, this.mShakeY);
                    if (this.mTookOrPid == 1) {
                        canvas.drawBitmap(this.mMainBitmap, 0.0f, 0.0f, this.mPaint);
                    } else {
                        canvas.drawBitmap(this.mPidBitmap, 0.0f, 0.0f, this.mPaint);
                    }
                    canvas.restore();
                    return;
            }
        }
    }
}
