package net.droidstick.gameobject;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import net.droidstick.finger.GameActivity;
import net.droidstick.finger.R;

public class TutorialMessage extends GameObject {
    public static final int STATE_DRAW_MESSAGE2 = 2;
    protected final int POS_X = 240;
    protected final int POS_Y = 200;
    protected Bitmap mTutorialText2 = BitmapFactory.decodeResource(this.mGameActivity.getResources(), R.drawable.img_tut02);

    public TutorialMessage(GameActivity _ga, int drawaBleId) {
        super(_ga, drawaBleId);
        setObjectState(0);
    }

    public void touchStart(float x, float y) {
    }

    public void touchMove(float x, float y) {
    }

    public void touchUp(float x, float y) {
    }

    public void setObjectState(int state) {
        this.mObjectState = state;
        this.mStateTimeElapsed = 0.0f;
        switch (this.mObjectState) {
            case 0:
                this.mPosX = -500.0f;
                this.mPosY = -500.0f;
                return;
            case 1:
            case 2:
                this.mPosX = 240.0f;
                this.mPosY = 200.0f;
                return;
            default:
                return;
        }
    }

    public void update(float elapsedSeconds) {
        switch (this.mObjectState) {
            case 0:
            default:
                return;
            case 1:
            case 2:
                updateMatrixAndTransformCollisionPoints();
                return;
        }
    }

    public void draw(Canvas canvas) {
        if (canvas != null) {
            switch (this.mObjectState) {
                case 0:
                default:
                    return;
                case 1:
                    canvas.save();
                    canvas.setMatrix(this.mMatrix);
                    canvas.translate(this.mShakeX, this.mShakeY);
                    canvas.drawBitmap(this.mMainBitmap, 0.0f, 0.0f, this.mPaint);
                    canvas.restore();
                    return;
                case 2:
                    canvas.save();
                    canvas.setMatrix(this.mMatrix);
                    canvas.translate(this.mShakeX, this.mShakeY);
                    canvas.drawBitmap(this.mTutorialText2, 0.0f, 0.0f, this.mPaint);
                    canvas.restore();
                    return;
            }
        }
    }
}
