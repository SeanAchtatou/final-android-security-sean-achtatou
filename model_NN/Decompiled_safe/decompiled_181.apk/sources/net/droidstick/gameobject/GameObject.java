package net.droidstick.gameobject;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import java.util.Random;
import net.droidstick.finger.GameActivity;

public abstract class GameObject {
    public static final int STATE_DRAW = 1;
    public static final int STATE_IDLE = 0;
    protected final float SHAKE_INTERVAL = 0.01f;
    protected float mAnchorX;
    protected float mAnchorY;
    protected float mAngle;
    protected Paint mCollisionAreaPaint = new Paint();
    protected GameActivity mGameActivity;
    protected Bitmap mMainBitmap;
    protected Matrix mMatrix;
    protected float mNoTransBottomLeftX;
    protected float mNoTransBottomLeftY;
    protected float mNoTransBottomRightX;
    protected float mNoTransBottomRightY;
    protected float mNoTransTopLeftX;
    protected float mNoTransTopLeftY;
    protected float mNoTransTopRightX;
    protected float mNoTransTopRightY;
    public int mObjectState = 0;
    protected Paint mPaint = new Paint();
    protected float mPosX;
    protected float mPosY;
    protected Random mRandom = new Random();
    protected float mShakeTimeLeft;
    public float mShakeX;
    public float mShakeY;
    protected float mStateTimeElapsed = 0.0f;
    protected int mTeenBitmapHalfHeight;
    protected int mTeenBitmapHalfWidth;
    protected int mTeenBitmapHeight;
    protected int mTeenBitmapWidth;
    protected int polySides = 4;
    public int[] polyX = new int[4];
    public int[] polyY = new int[4];
    int streamIdStomp = 50;

    public abstract void setObjectState(int i);

    public abstract void touchMove(float f, float f2);

    public abstract void touchStart(float f, float f2);

    public abstract void touchUp(float f, float f2);

    public abstract void update(float f);

    public GameObject(GameActivity _ga, int drawaBleId) {
        this.mGameActivity = _ga;
        this.mMainBitmap = BitmapFactory.decodeResource(this.mGameActivity.getResources(), drawaBleId);
        this.mTeenBitmapWidth = this.mMainBitmap.getWidth();
        this.mTeenBitmapHalfWidth = this.mTeenBitmapWidth / 2;
        this.mTeenBitmapHeight = this.mMainBitmap.getHeight();
        this.mTeenBitmapHalfHeight = this.mTeenBitmapHeight / 2;
        this.mNoTransTopLeftX = 0.0f;
        this.mNoTransTopLeftY = 0.0f;
        this.mNoTransTopRightX = (float) this.mTeenBitmapWidth;
        this.mNoTransTopRightY = 0.0f;
        this.mNoTransBottomRightX = (float) this.mTeenBitmapWidth;
        this.mNoTransBottomRightY = (float) this.mTeenBitmapHeight;
        this.mNoTransBottomLeftX = 0.0f;
        this.mNoTransBottomLeftY = (float) this.mTeenBitmapHeight;
        this.mAnchorX = (float) this.mTeenBitmapHalfWidth;
        this.mAnchorY = (float) this.mTeenBitmapHalfHeight;
        this.mMatrix = new Matrix();
        updateMatrixAndTransformCollisionPoints();
    }

    private void updateShake(float elapsedSeconds) {
        if (this.mShakeTimeLeft < 0.0f) {
            this.mShakeX = (float) (this.mRandom.nextBoolean() ? this.mRandom.nextInt((int) (this.mGameActivity.mLunarThread.mDrawScaleW * 15.0f)) : -this.mRandom.nextInt((int) (this.mGameActivity.mLunarThread.mDrawScaleW * 15.0f)));
            this.mShakeY = (float) (this.mRandom.nextBoolean() ? this.mRandom.nextInt((int) (this.mGameActivity.mLunarThread.mDrawScaleH * 15.0f)) : -this.mRandom.nextInt((int) (this.mGameActivity.mLunarThread.mDrawScaleH * 15.0f)));
            this.mShakeTimeLeft = 0.01f;
            return;
        }
        this.mShakeTimeLeft -= elapsedSeconds;
    }

    public void draw(Canvas canvas) {
        if (canvas != null) {
            switch (this.mObjectState) {
                case 0:
                default:
                    return;
                case 1:
                    canvas.save();
                    canvas.setMatrix(this.mMatrix);
                    canvas.translate(this.mShakeX, this.mShakeY);
                    canvas.drawBitmap(this.mMainBitmap, 0.0f, 0.0f, this.mPaint);
                    canvas.restore();
                    drawCollisionArea(canvas);
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void drawCollisionArea(Canvas canvas) {
        this.mCollisionAreaPaint.setARGB(255, 255, 0, 0);
        canvas.drawLine((float) this.polyX[0], (float) this.polyY[0], (float) this.polyX[1], (float) this.polyY[1], this.mCollisionAreaPaint);
        canvas.drawLine((float) this.polyX[1], (float) this.polyY[1], (float) this.polyX[2], (float) this.polyY[2], this.mCollisionAreaPaint);
        canvas.drawLine((float) this.polyX[2], (float) this.polyY[2], (float) this.polyX[3], (float) this.polyY[3], this.mCollisionAreaPaint);
        canvas.drawLine((float) this.polyX[3], (float) this.polyY[3], (float) this.polyX[0], (float) this.polyY[0], this.mCollisionAreaPaint);
    }

    /* access modifiers changed from: protected */
    public void updateMatrixAndTransformCollisionPoints() {
        float[] transformedPos = {this.mNoTransTopLeftX, this.mNoTransTopLeftY, this.mNoTransTopRightX, this.mNoTransTopRightY, this.mNoTransBottomRightX, this.mNoTransBottomRightY, this.mNoTransBottomLeftX, this.mNoTransBottomLeftY};
        this.mMatrix.reset();
        this.mMatrix.postTranslate(-this.mAnchorX, -this.mAnchorY);
        this.mMatrix.postTranslate(this.mPosX, this.mPosY);
        this.mMatrix.postRotate(-this.mAngle, this.mPosX, this.mPosY);
        this.mMatrix.postScale(this.mGameActivity.mLunarView.getThread().mDrawScaleW, this.mGameActivity.mLunarView.getThread().mDrawScaleH);
        this.mMatrix.mapPoints(transformedPos);
        this.polyX[0] = (int) transformedPos[0];
        this.polyY[0] = (int) transformedPos[1];
        this.polyX[1] = (int) transformedPos[2];
        this.polyY[1] = (int) transformedPos[3];
        this.polyX[2] = (int) transformedPos[4];
        this.polyY[2] = (int) transformedPos[5];
        this.polyX[3] = (int) transformedPos[6];
        this.polyY[3] = (int) transformedPos[7];
    }

    /* access modifiers changed from: protected */
    public boolean isPointInPoly(int cursor_x, int cursor_y, int[] polyX2, int[] polyY2, int polySides2) {
        boolean result = false;
        int j = polySides2 - 1;
        for (int i = 0; i < polySides2; i++) {
            int x1 = polyX2[i];
            int y1 = polyY2[i];
            int x2 = polyX2[j];
            int y2 = polyY2[j];
            if ((y1 > cursor_y) != (y2 > cursor_y) && cursor_x < (((x2 - x1) * (cursor_y - y1)) / (y2 - y1)) + x1) {
                if (result) {
                    result = false;
                } else {
                    result = true;
                }
            }
            j = i;
        }
        return result;
    }
}
