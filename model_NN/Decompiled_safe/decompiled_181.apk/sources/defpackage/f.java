package defpackage;

import android.os.SystemClock;
import com.google.ads.util.a;
import java.util.LinkedList;

/* renamed from: f  reason: default package */
public final class f {
    private static long f = 0;
    public String a;
    private LinkedList<Long> b = new LinkedList<>();
    private long c;
    private long d;
    private LinkedList<Long> e = new LinkedList<>();
    private String g;
    private boolean h = false;
    private boolean i = false;

    f() {
        a();
    }

    static long i() {
        return f;
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        this.b.clear();
        this.c = 0;
        this.d = 0;
        this.e.clear();
        this.g = null;
        this.h = false;
        this.i = false;
    }

    public final void a(String str) {
        a.d("Prior ad identifier = " + str);
        this.g = str;
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        a.d("Ad clicked.");
        this.b.add(Long.valueOf(SystemClock.elapsedRealtime()));
    }

    public final void b(String str) {
        a.d("Prior impression ticket = " + str);
        this.a = str;
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        a.d("Ad request loaded.");
        this.c = SystemClock.elapsedRealtime();
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        a.d("Ad request started.");
        this.d = SystemClock.elapsedRealtime();
        f++;
    }

    /* access modifiers changed from: package-private */
    public final long e() {
        if (this.b.size() != this.e.size()) {
            return -1;
        }
        return (long) this.b.size();
    }

    /* access modifiers changed from: package-private */
    public final String f() {
        if (this.b.isEmpty() || this.b.size() != this.e.size()) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.b.size()) {
                return sb.toString();
            }
            if (i3 != 0) {
                sb.append(",");
            }
            sb.append(Long.toString(this.e.get(i3).longValue() - this.b.get(i3).longValue()));
            i2 = i3 + 1;
        }
    }

    /* access modifiers changed from: package-private */
    public final String g() {
        if (this.b.isEmpty()) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.b.size()) {
                return sb.toString();
            }
            if (i3 != 0) {
                sb.append(",");
            }
            sb.append(Long.toString(this.b.get(i3).longValue() - this.c));
            i2 = i3 + 1;
        }
    }

    /* access modifiers changed from: package-private */
    public final long h() {
        return this.c - this.d;
    }

    /* access modifiers changed from: package-private */
    public final String j() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public final boolean k() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public final void l() {
        a.d("Interstitial network error.");
        this.h = true;
    }

    /* access modifiers changed from: package-private */
    public final boolean m() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public final void n() {
        a.d("Interstitial no fill.");
        this.i = true;
    }

    public final void o() {
        a.d("Landing page dismissed.");
        this.e.add(Long.valueOf(SystemClock.elapsedRealtime()));
    }

    /* access modifiers changed from: package-private */
    public final String p() {
        return this.a;
    }
}
