package android.engine;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import com.mine.test_lite.R;

public class FeedBackForm extends Activity {
    public static String Desc;
    public static String EMail;
    Config Config;
    AddManager addManager;
    /* access modifiers changed from: private */
    public Dialog dialog;
    EditText emaiEditText;
    EngineIO engineIO;
    EditText feedBackeditText;
    boolean finishActivity = false;
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            FeedBackForm.this.progressDialog.dismiss();
            if (msg.getData().getString("Response").equals("Password")) {
                if (msg.getData().getBoolean("PasswordResponse")) {
                    FeedBackForm.this.startActivity(new Intent(FeedBackForm.this, TestingModule.class));
                    return;
                }
                FeedBackForm.this.engineIO.showPrompt(FeedBackForm.this, "Wrong Password");
            } else if (msg.getData().getString("Response").equals("Feedback")) {
                if (msg.getData().getBoolean("FeedbackResponse")) {
                    FeedBackForm.this.finishActivity = true;
                    FeedBackForm.this.showPrompt("Thanks for your valuable feedback");
                    return;
                }
                FeedBackForm.this.engineIO.showPrompt(FeedBackForm.this, "Unfortunately, your feedback is not submitted. Please try later.");
            } else if (msg.getData().getString("Response").equals("Dialog")) {
                FeedBackForm.this.engineIO.showPrompt(FeedBackForm.this, "Email id is not valid");
            }
        }
    };
    Handler handler2 = new Handler() {
        public void handleMessage(Message msg) {
            FeedBackForm.this.progressDialog.dismiss();
            FeedBackForm.this.showPrompt("Response =" + msg.getData().getString("FeedbackResponse"));
        }
    };
    NetHandler netHandler;
    ProgressDialog progressDialog;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.engine_feedback_dialog);
        this.netHandler = new NetHandler(this);
        this.Config = new Config(this);
        this.engineIO = new EngineIO(this);
        this.addManager = new AddManager(this);
        this.feedBackeditText = (EditText) findViewById(R.id.EditText_feedback_text);
        this.emaiEditText = (EditText) findViewById(R.id.EditText_feedback_email);
        ((Button) findViewById(R.id.Button_submit_feedback)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (FeedBackForm.this.feedBackeditText.getText().toString().equals("123")) {
                    FeedBackForm.this.showPasswordDialog();
                } else if (FeedBackForm.this.validate()) {
                    FeedBackForm.this.submitFeedBack();
                }
            }
        });
        ((Button) findViewById(R.id.Button_cancel_feedback)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                FeedBackForm.this.finish();
            }
        });
    }

    /* access modifiers changed from: private */
    public boolean validateEmailID(String email) {
        int atCount = 0;
        int atPos = -1;
        int lastDotPos = -1;
        boolean dotExits = false;
        if (email.length() < 6) {
            return false;
        }
        for (int i = 0; i < email.length(); i++) {
            if (email.charAt(i) == ' ') {
                return false;
            }
            if (email.charAt(i) == '#') {
                return false;
            }
            if (email.charAt(i) == '%') {
                return false;
            }
            if (email.charAt(i) == '^') {
                return false;
            }
            if (email.charAt(i) == '&') {
                return false;
            }
            if (email.charAt(i) == '*') {
                return false;
            }
            if (email.charAt(i) == '(') {
                return false;
            }
            if (email.charAt(i) == ')') {
                return false;
            }
            if (email.charAt(i) == '+') {
                return false;
            }
            if (email.charAt(i) == '=') {
                return false;
            }
            if (email.charAt(i) == ']') {
                return false;
            }
            if (email.charAt(i) == '[') {
                return false;
            }
            if (email.charAt(i) == '}') {
                return false;
            }
            if (email.charAt(i) == '{') {
                return false;
            }
            if (email.charAt(i) == '>') {
                return false;
            }
            if (email.charAt(i) == '<') {
                return false;
            }
            if (email.charAt(i) == '/') {
                return false;
            }
            if (email.charAt(i) == '|') {
                return false;
            }
            if (email.charAt(i) == '@') {
                atCount++;
                atPos = i;
            }
            if (email.charAt(i) == '.') {
                dotExits = true;
                lastDotPos = i;
            }
            if (lastDotPos == atPos + 1) {
                return false;
            }
        }
        System.out.println("@ positon" + atPos);
        System.out.println("Dot positon" + lastDotPos);
        if (atCount != 1) {
            return false;
        }
        if (lastDotPos < atPos) {
            return false;
        }
        if (lastDotPos > email.length() - 2) {
            return false;
        }
        return dotExits;
    }

    public boolean validate() {
        EMail = this.emaiEditText.getText().toString();
        Desc = this.feedBackeditText.getText().toString();
        if (!validateEmailID(EMail)) {
            showPrompt("Please enter valid Email Id");
        } else if (Desc == null || Desc.length() > 0) {
            return true;
        } else {
            showPrompt("Please write your feedback");
        }
        return false;
    }

    public void submitFeedBack() {
        EMail = this.emaiEditText.getText().toString();
        Desc = this.feedBackeditText.getText().toString();
        System.out.println("EMail = " + EMail + " and Desc = " + Desc);
        this.progressDialog = ProgressDialog.show(this, "Processing", "Please wait...");
        new Thread() {
            public void run() {
                Message msg = new Message();
                Bundle bundle = new Bundle();
                if (FeedBackForm.this.validateEmailID(FeedBackForm.EMail)) {
                    bundle.putString("Response", "Feedback");
                    String response = FeedBackForm.this.netHandler.postFeedBackData(String.valueOf(AppConstants.primaryFeedBackLink) + "&email=" + FeedBackForm.EMail, FeedBackForm.Desc);
                    if (response == null || !response.equals("1")) {
                        bundle.putBoolean("FeedbackResponse", false);
                    } else {
                        bundle.putBoolean("FeedbackResponse", true);
                    }
                    msg.setData(bundle);
                    FeedBackForm.this.handler.sendMessage(msg);
                    return;
                }
                bundle.putString("Response", "Dialog");
                msg.setData(bundle);
                FeedBackForm.this.handler.sendMessage(msg);
            }
        }.start();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (UpdateDialog.exitApp || UpdateDialog.installUpdate) {
            finish();
        }
        this.addManager.init(14);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        AddManager.activityState = "Paused";
    }

    public void showPasswordDialog() {
        View v = LayoutInflater.from(this).inflate((int) R.layout.engine_passwordialog, (ViewGroup) null);
        this.dialog = new Dialog(this);
        this.dialog.setTitle("Enter Password");
        this.dialog.setContentView(v);
        this.dialog.setCanceledOnTouchOutside(false);
        final EditText current_password = (EditText) v.findViewById(R.id.EditText_pass);
        ((Button) v.findViewById(R.id.Button_login)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                System.out.println("current_password.getText().toString() = " + current_password.getText().toString());
                FeedBackForm.this.checkPassword(current_password.getText().toString());
                FeedBackForm.this.dialog.dismiss();
            }
        });
        ((Button) v.findViewById(R.id.Button_cancel_login)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                FeedBackForm.this.dialog.dismiss();
            }
        });
        this.dialog.show();
    }

    public void showPrompt(String msg) {
        AlertDialog prompt = new AlertDialog.Builder(this).create();
        prompt.setTitle("Note:");
        prompt.setMessage(msg);
        prompt.setButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dia, int arg1) {
                dia.dismiss();
                if (FeedBackForm.this.finishActivity) {
                    FeedBackForm.this.finishActivity = false;
                    FeedBackForm.this.finish();
                }
            }
        });
        prompt.show();
    }

    public void checkPassword(final String password) {
        this.progressDialog = ProgressDialog.show(this, "Processing", "Please wait...");
        new Thread() {
            public void run() {
                Message msg = new Message();
                Bundle bundle = new Bundle();
                bundle.putString("Response", "Password");
                String response = FeedBackForm.this.netHandler.getDataFrmUrl(String.valueOf(AppConstants.primaryTestingPasswordUrl) + password);
                if (response == null || !response.equals("1")) {
                    bundle.putBoolean("PasswordResponse", false);
                } else {
                    bundle.putBoolean("PasswordResponse", true);
                }
                msg.setData(bundle);
                FeedBackForm.this.handler.sendMessage(msg);
            }
        }.start();
    }

    public String getEmail() {
        return this.Config.removeWhiteSpace(EMail);
    }

    public String getDesc() {
        return this.Config.removeWhiteSpace(Desc);
    }
}
