package android.engine;

public class KeyGenerator {
    public static boolean DMODE = true;

    public static long convertHextoNum(String s) {
        int temp;
        long result = 0;
        char[] c = s.toCharArray();
        int i = c.length - 1;
        int m_loopcounter = 0;
        while (i >= 0) {
            char c2 = c[i];
            if (c2 > '@' && c2 <= 'F') {
                temp = c2 - '7';
            } else if (c2 <= '`' || c2 > 'f') {
                temp = c2 - '0';
            } else {
                temp = c2 - 'W';
            }
            result += ((long) temp) * pow(m_loopcounter);
            i--;
            m_loopcounter++;
        }
        return result;
    }

    private static long pow(int c) {
        long p = 1;
        for (int k = c; k > 0; k--) {
            p *= 16;
        }
        return p;
    }

    public String generateAutheticationKey(String imei, String offset, String duc) {
        System.out.println("imei = " + imei + "offset = " + offset + "DUC = " + duc);
        StringBuffer TransferKey = new StringBuffer();
        StringBuffer bufferImei = new StringBuffer();
        bufferImei.append(imei);
        StringBuffer bufferoffser = new StringBuffer();
        bufferoffser.append(offset);
        StringBuffer bufferduc = new StringBuffer();
        bufferduc.append(duc);
        System.out.println("g key 1 = " + imei);
        TransferKey.append(Mid(bufferduc, 1, 1));
        TransferKey.append(Mid(bufferImei, 0, 1));
        TransferKey.append(Mid(bufferoffser, 0, 1));
        System.out.println("g key 2");
        TransferKey.append(Mid(bufferImei, 1, 1));
        TransferKey.append(Mid(bufferduc, 2, 1));
        TransferKey.append(Mid(bufferImei, 2, 1));
        TransferKey.append(Mid(bufferImei, 6, 1));
        TransferKey.append(Mid(bufferImei, 7, 1));
        System.out.println("g key 3");
        TransferKey.append(Mid(bufferoffser, 1, 1));
        TransferKey.append(Mid(bufferImei, 8, 1));
        TransferKey.append(Mid(bufferoffser, 2, 1));
        TransferKey.append(Mid(bufferImei, 12, 1));
        System.out.println("g key 4");
        TransferKey.append(Mid(bufferduc, 3, 1));
        System.out.println("g key 4 _1 =" + imei);
        TransferKey.append(Mid(bufferImei, 13, 1));
        TransferKey.append(Mid(bufferImei, 14, 1));
        System.out.println("g key 5 = " + TransferKey.toString());
        String temp23 = TransferKey.toString();
        StringBuffer TransferKey2 = new StringBuffer();
        TransferKey2.append(DecToHex(temp23));
        System.out.println("g key 6 *****= " + TransferKey2.toString());
        StringBuffer tmp1 = new StringBuffer();
        tmp1.append("A");
        for (int i = TransferKey2.toString().length(); i > 0; i--) {
            tmp1.append(Mid(TransferKey2, i - 1, 1));
        }
        return tmp1.toString();
    }

    private String HexValGenerator(String aTmp) {
        long temp;
        System.out.println("HexValGenerator 1 = " + aTmp);
        StringBuffer result = new StringBuffer();
        try {
            temp = Long.parseLong(aTmp);
        } catch (NumberFormatException nfe) {
            temp = 201;
            System.out.println("NumberFormatException = " + nfe);
        }
        System.out.println("HexValGenerator 2");
        long r = temp % 16;
        System.out.println("HexValGenerator 3 =" + temp + "r = " + r);
        if (temp - r == 0) {
            System.out.println("HexValGenerator 4");
            result.append(ToChar((int) r));
        } else {
            System.out.println("HexValGenerator 5");
            result.append(ToChar((int) r));
            StringBuffer tmp = new StringBuffer();
            tmp.append((temp - ((long) ((int) r))) / 16);
            result.append(HexValGenerator(tmp.toString()));
        }
        return result.toString();
    }

    private String DecToHex(String aIMEI) {
        try {
            System.out.println("in DecToHex 1= " + aIMEI);
            StringBuffer key = new StringBuffer();
            StringBuffer tmp1 = new StringBuffer();
            StringBuffer tmp2 = new StringBuffer();
            StringBuffer tmp3 = new StringBuffer();
            StringBuffer tmp4 = new StringBuffer();
            StringBuffer tmp5 = new StringBuffer();
            StringBuffer bufferaIMEI = new StringBuffer();
            bufferaIMEI.append(aIMEI);
            System.out.println("in DecToHex =2 ");
            tmp1.append(Left(aIMEI, 4));
            System.out.println("tmp1 = " + tmp1.toString());
            tmp2.append(Mid(bufferaIMEI, 4, 4));
            System.out.println("tmp2 = " + tmp2.toString());
            tmp3.append(Mid(bufferaIMEI, 8, 4));
            System.out.println("tmp3 = " + tmp3.toString());
            tmp4.append(Right(aIMEI, 3));
            System.out.println("tmp4 = " + tmp4.toString());
            System.out.println("in DecToHex =3 ");
            String t1 = tmp1.toString();
            String t2 = tmp2.toString();
            String t3 = tmp3.toString();
            String t4 = tmp4.toString();
            System.out.println("in DecToHex =4 ");
            tmp5.append(t3);
            tmp5.append(t4);
            tmp5.append(t1);
            tmp5.append(t2);
            System.out.println("in DecToHex =5 " + tmp5.toString());
            key.append(HexValGenerator(tmp5.toString()));
            System.out.println("in DecToHex =6 = " + key.toString());
            return key.toString();
        } catch (Exception e) {
            System.out.println("in DecToHex");
            e.printStackTrace();
            return aIMEI;
        }
    }

    private String Left(String str, int len) {
        return str.substring(0, len);
    }

    private String Right(String str, int len) {
        return str.substring(str.length() - len, str.length());
    }

    public String genereateKeyL(String IMEI_NUMBER, int OFFSET) {
        int int4;
        int int3;
        int int2;
        int int1;
        System.out.println("IMEI_NUMBER = " + IMEI_NUMBER + " and OFFSET = " + OFFSET);
        int int12 = 0;
        int int22 = 0;
        int int32 = 0;
        int int42 = 0;
        StringBuffer buffer = new StringBuffer();
        StringBuffer buffer2 = new StringBuffer();
        StringBuffer temp3 = new StringBuffer();
        StringBuffer temp4 = new StringBuffer();
        StringBuffer temp5 = new StringBuffer();
        StringBuffer temp6 = new StringBuffer();
        buffer.append(IMEI_NUMBER);
        int maxno = -1;
        try {
            int12 = Integer.parseInt(Mid(buffer, 11, 4));
            int22 = Integer.parseInt(Mid(buffer, 8, 4));
            int32 = Integer.parseInt(Mid(buffer, 4, 4));
            int42 = Integer.parseInt(Mid(buffer, 0, 4));
        } catch (NumberFormatException e) {
        }
        int tempint = int1 + OFFSET;
        for (int i = 0; i < 4; i++) {
            if (tempint % 10 == 0) {
                int1 = (int1 * 9) + 537;
            } else {
                int1 *= tempint % 10;
            }
            tempint /= 10;
            while (int1 >= 10000) {
                int1 = (int1 / 10000) + (int1 % 10000);
            }
        }
        int tempint2 = int2;
        for (int i2 = 0; i2 < 4; i2++) {
            if (tempint2 % 10 == 0) {
                int2 = (int2 * 9) + 537;
            } else {
                int2 *= tempint2 % 10;
            }
            tempint2 /= 10;
            while (int2 >= 10000) {
                int2 = (int2 / 10000) + (int2 % 10000);
            }
        }
        int tempint3 = int3;
        for (int i3 = 0; i3 < 4; i3++) {
            if (tempint3 % 10 == 0) {
                int3 = (int3 * 9) + 537;
            } else {
                int3 *= tempint3 % 10;
            }
            tempint3 /= 10;
            while (int3 >= 10000) {
                int3 = (int3 / 10000) + (int3 % 10000);
            }
        }
        int tempint4 = int4;
        for (int i4 = 0; i4 < 4; i4++) {
            if (tempint4 % 10 == 0) {
                int4 = (int4 * 9) + 537;
            } else {
                int4 *= tempint4 % 10;
            }
            tempint4 /= 10;
            while (int4 >= 10000) {
                int4 = (int4 / 10000) + (int4 % 10000);
            }
        }
        for (int i5 = 0; i5 < 9; i5++) {
            int1 = (int1 + int2) / 2;
            int2 = (int2 + int3) / 2;
            int3 = (int3 + int4) / 2;
            int4 = (int4 + int1) / 2;
        }
        buffer2.append(int4);
        buffer2.append(int2);
        buffer2.append(int1);
        buffer2.append(int3);
        buffer.delete(0, buffer.length());
        buffer.append(buffer2.toString());
        temp5.delete(0, temp5.length());
        temp5.append(buffer2.toString());
        int loop = 6;
        while (loop > 0) {
            int nos = 0;
            temp4.append(buffer.toString());
            for (int ch = '9'; ch > 48; ch = (char) (ch - 1)) {
                int tnos = 0;
                while (true) {
                    int loc1 = findIndexOf(temp4, ch);
                    if (loc1 == -1) {
                        break;
                    }
                    tnos++;
                    temp4.deleteCharAt(loc1);
                }
                if (tnos > nos) {
                    maxno = ch;
                    nos = tnos;
                }
            }
            for (int i9 = 0; i9 < nos; i9++) {
                int loc12 = findIndexOf(buffer, (char) maxno);
                if (loc12 == 0) {
                    temp6.delete(0, temp6.length());
                    temp6.append(temp5.charAt(loc12));
                    temp6.append(temp5.charAt(14));
                    int tch = Integer.parseInt(new String(temp6));
                    temp3.delete(0, temp3.length());
                    temp3.append(returnchar(tch));
                    buffer = doReplaceString(buffer, loc12, 1, temp3);
                    buffer2 = doReplaceString(buffer2, loc12, 1, temp3);
                } else {
                    int tch2 = Integer.parseInt(Mid(temp5, loc12 - 1, 2).toString());
                    temp3.delete(0, temp3.length());
                    temp3.append(returnchar(tch2));
                    buffer2 = doReplaceString(doReplaceString(buffer, loc12, 1, temp3), loc12, 1, temp3);
                }
                loop--;
                if (loop == 0) {
                    break;
                }
            }
        }
        StringBuffer iGeneratedKey = new StringBuffer();
        iGeneratedKey.append(buffer2.toString());
        return iGeneratedKey.toString();
    }

    public int findIndexOf(StringBuffer buffer, char c) {
        for (int i = 0; i < buffer.length(); i++) {
            if (buffer.charAt(i) == c) {
                return i;
            }
        }
        return -1;
    }

    public char returnchar(int aA) {
        if (aA > 51) {
            aA %= 52;
        }
        int temp = aA + 65;
        if (temp > 90) {
            temp += 6;
        }
        if (temp > 96 && temp <= 122) {
            temp -= 32;
        }
        return (char) temp;
    }

    public String ToChar(int index) {
        StringBuffer textHex = new StringBuffer();
        textHex.append("0123456789ABCDEF");
        return Mid(textHex, index, 1);
    }

    public String Mid(StringBuffer buffer, int loc1, int numChars) {
        try {
            StringBuffer s = new StringBuffer();
            int length = buffer.length();
            if (loc1 <= -1 || loc1 > length - 1) {
                throw new StringIndexOutOfBoundsException();
            } else if (loc1 + numChars > length) {
                throw new StringIndexOutOfBoundsException();
            } else {
                for (int i = loc1; i < loc1 + numChars; i++) {
                    s.append(buffer.charAt(i));
                }
                System.out.println("Return Char = " + s.toString());
                return s.toString();
            }
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
            return buffer.toString();
        }
    }

    public StringBuffer doSubString(StringBuffer buffer, int beginIndex, int endIndex) {
        StringBuffer s = new StringBuffer();
        int count = buffer.length();
        if (beginIndex < 0 || endIndex > count || beginIndex > endIndex) {
            throw new StringIndexOutOfBoundsException();
        } else if (beginIndex == 0 && endIndex == count) {
            return buffer;
        } else {
            for (int i = beginIndex; i <= endIndex; i++) {
                s.append(buffer.charAt(i));
            }
            return s;
        }
    }

    public StringBuffer doReplaceString(StringBuffer buffer1, int loc1, int numChars, StringBuffer buffer2) {
        int length1 = buffer1.length();
        if (loc1 >= 0 && loc1 <= length1 - 1 && length1 >= loc1 + numChars && buffer1.length() > loc1) {
            buffer1.setCharAt(loc1, buffer2.charAt(0));
        }
        return buffer1;
    }
}
