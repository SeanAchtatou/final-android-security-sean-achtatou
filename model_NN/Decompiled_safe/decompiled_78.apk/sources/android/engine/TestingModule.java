package android.engine;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.mine.test_lite.R;

public class TestingModule extends Activity {
    Button adsTesting;
    Button configTesting;
    private String testing;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.engine_test1);
        this.configTesting = (Button) findViewById(R.id.Button_config);
        this.adsTesting = (Button) findViewById(R.id.Button_ads);
        this.configTesting.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                TestingModule.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(AppConstants.primaryConfigTestingLink)));
            }
        });
        this.adsTesting.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                TestingModule.this.showPrompt("Ads fetching from " + AppConstants.AdsHostIP + " IP");
            }
        });
    }

    public void showPrompt(String msg) {
        AlertDialog prompt = new AlertDialog.Builder(this).create();
        prompt.setTitle("Ads IP:");
        prompt.setMessage(msg);
        prompt.setButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dia, int arg1) {
                dia.dismiss();
            }
        });
        prompt.show();
    }
}
