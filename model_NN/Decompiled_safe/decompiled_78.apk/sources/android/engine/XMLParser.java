package android.engine;

import java.io.ByteArrayInputStream;
import java.util.Vector;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class XMLParser {
    public static String ACU;
    public static String AdsID;
    public static String AdsStatus;
    public static String AppBuildNo;
    public static Vector<String> AppIcon;
    public static Vector<String> AppName;
    public static String AppVersionNo;
    public static String ApplicationName;
    public static String BuyAppStatus;
    public static String CPButtonStatus;
    public static Vector<String> Currency;
    public static String DUC;
    public static String EngVerNo;
    public static Vector<String> FString;
    public static String FileType;
    public static String FreeAppStatus;
    public static Vector<String> LongDesc;
    public static String MORE;
    public static String Offset;
    public static Vector<String> Price;
    public static Vector<String> Rating;
    public static Vector<String> SmallDesc;
    public static Vector<String> SreenShot1;
    public static Vector<String> SreenShot2;
    public static Vector<String> appBuy;
    public static Vector<String> appDesc;
    public static Vector<String> appLogo;
    public static Vector<String> appLogo1;
    public static Vector<String> appMsg;
    public static Vector<String> id;
    public static Vector<String> link;
    public static Vector<String> src;
    boolean isACU = false;
    boolean isAds = false;
    boolean isAdsID = false;
    boolean isAdsStatus = false;
    boolean isAppBuildNo = false;
    boolean isAppDesc = false;
    boolean isAppIcon = false;
    boolean isAppLogo = false;
    boolean isAppName = false;
    boolean isAppVersionNo = false;
    boolean isApplicationName = false;
    boolean isBuy = false;
    boolean isBuyAppStatus = false;
    boolean isCPButton = false;
    boolean isCurrency = false;
    boolean isDUC = false;
    boolean isEngVerNo = false;
    boolean isFString = false;
    boolean isFileType = false;
    boolean isFreeAppStatus = false;
    boolean isId = false;
    boolean isImgSrc = false;
    boolean isLink = false;
    boolean isLongDesc = false;
    boolean isMore = false;
    boolean isMsg = false;
    boolean isOffset = false;
    boolean isPrice = false;
    boolean isRating = false;
    boolean isRecord = false;
    boolean isRoot = false;
    boolean isSmallDesc = false;
    boolean isSrc = false;
    boolean isSreenShot1 = false;
    boolean isSreenShot2 = false;

    public XMLParser() {
        AppName = new Vector<>();
        AppIcon = new Vector<>();
        SmallDesc = new Vector<>();
        LongDesc = new Vector<>();
        SreenShot1 = new Vector<>();
        SreenShot2 = new Vector<>();
        FString = new Vector<>();
        Rating = new Vector<>();
        Currency = new Vector<>();
        Price = new Vector<>();
        src = new Vector<>();
        link = new Vector<>();
        id = new Vector<>();
        appLogo1 = new Vector<>();
        appLogo = new Vector<>();
        appDesc = new Vector<>();
        appMsg = new Vector<>();
        appBuy = new Vector<>();
    }

    public void parseXML(String xml) {
        try {
            initVectors();
            XMLReader xr = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
            xr.setContentHandler(new MyXMLHandler());
            xr.parse(new InputSource(new ByteArrayInputStream(replaceChars(xml).getBytes())));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initVectors() {
        ACU = "";
        MORE = "";
        ApplicationName = "";
        Offset = "";
        DUC = "";
        AppVersionNo = "";
        AppBuildNo = "";
        AdsID = "";
        FileType = "";
        BuyAppStatus = "";
        FreeAppStatus = "";
        EngVerNo = "";
        AdsStatus = "";
        CPButtonStatus = "";
        if (AppName != null && AppName.size() > 0) {
            AppName.clear();
        }
        if (AppIcon != null && AppIcon.size() > 0) {
            AppIcon.clear();
        }
        if (SmallDesc != null && SmallDesc.size() > 0) {
            SmallDesc.clear();
        }
        if (LongDesc != null && LongDesc.size() > 0) {
            LongDesc.clear();
        }
        if (SreenShot1 != null && SreenShot1.size() > 0) {
            SreenShot1.clear();
        }
        if (SreenShot2 != null && SreenShot2.size() > 0) {
            SreenShot2.clear();
        }
        if (FString != null && FString.size() > 0) {
            FString.clear();
        }
        if (Rating != null && Rating.size() > 0) {
            Rating.clear();
        }
        if (Currency != null && Currency.size() > 0) {
            Currency.clear();
        }
        if (Price != null && Price.size() > 0) {
            Price.clear();
        }
        if (src != null && src.size() > 0) {
            src.clear();
        }
        if (link != null && link.size() > 0) {
            link.clear();
        }
        if (id != null && id.size() > 0) {
            id.clear();
        }
        if (appLogo != null && appLogo.size() > 0) {
            appLogo.clear();
        }
        if (appDesc != null && appDesc.size() > 0) {
            appDesc.clear();
        }
        if (appMsg != null && appMsg.size() > 0) {
            appMsg.clear();
        }
        if (appBuy != null && appBuy.size() > 0) {
            appBuy.clear();
        }
    }

    public String replaceChars(String xmlString) {
        char[] ch = new char[xmlString.length()];
        if (xmlString.indexOf("&") == -1) {
            return xmlString;
        }
        xmlString.getChars(0, xmlString.length(), ch, 0);
        for (int i = 0; i < ch.length; i++) {
            if (ch[i] == '&') {
                ch[i] = '*';
            }
        }
        return new String(ch);
    }

    public int getUpgradedAppCount() {
        return AppIcon.size();
    }

    public Vector<String> getAppName() {
        System.out.println("AppName = " + AppName.size());
        return AppName;
    }

    public Vector<String> getAppIcon() {
        System.out.println("AppIcon = " + AppIcon.size());
        return AppIcon;
    }

    public Vector<String> getSmallDesc() {
        System.out.println("SmallDesc = " + SmallDesc.size());
        return SmallDesc;
    }

    public Vector<String> getLongDesc() {
        System.out.println("LongDesc = " + LongDesc.size());
        return LongDesc;
    }

    public Vector<String> getSreenShot1() {
        System.out.println("SreenShot1 = " + SreenShot1.size());
        return SreenShot1;
    }

    public Vector<String> getSreenShot2() {
        System.out.println("SreenShot2 = " + SreenShot2.size());
        return SreenShot2;
    }

    public Vector<String> getFString() {
        System.out.println("FString = " + FString.size());
        return FString;
    }

    public Vector<String> getRating() {
        System.out.println("FString = " + FString.size());
        return Rating;
    }

    public Vector<String> getCurrency() {
        System.out.println("FString = " + FString.size());
        return Currency;
    }

    public Vector<String> getPrice() {
        System.out.println("FString = " + FString.size());
        return Price;
    }

    public Vector<String> getSrc() {
        System.out.println("src.size() = " + src.size());
        return src;
    }

    public Vector<String> getLink() {
        System.out.println("link.size() = " + link.size());
        return link;
    }

    public Vector<String> getId() {
        System.out.println("id.size() = " + id.size());
        return id;
    }

    public String getACU() {
        return ACU;
    }

    public Vector<String> getAppLogo1() {
        return appLogo1;
    }

    public Vector<String> getAppLogo() {
        return appLogo;
    }

    public Vector<String> getAppDesc() {
        return appDesc;
    }

    public Vector<String> getAppBuyLinks() {
        return appBuy;
    }

    public Vector<String> getAppMsg() {
        return appMsg;
    }

    public String getMoreURL() {
        return MORE;
    }

    public String getApplicationName() {
        return ApplicationName;
    }

    public String getOffset() {
        return Offset;
    }

    public String getDUC() {
        return DUC;
    }

    public String getAppVersionNo() {
        return AppVersionNo;
    }

    public String getAppBuildNo() {
        return AppBuildNo;
    }

    public String getAdsID() {
        return AdsID;
    }

    public String getFileType() {
        return FileType;
    }

    public String getBuyAppStatus() {
        return BuyAppStatus;
    }

    public String getFreeAppStatus() {
        return FreeAppStatus;
    }

    public String getEngVerNo() {
        return EngVerNo;
    }

    public String getAdsStatus() {
        return AdsStatus;
    }

    public String getCPButtonStatus() {
        return CPButtonStatus;
    }

    class MyXMLHandler extends DefaultHandler {
        MyXMLHandler() {
        }

        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if (localName.equalsIgnoreCase("AppName")) {
                XMLParser.this.isAppName = true;
            } else if (localName.equalsIgnoreCase("AppIcon")) {
                XMLParser.this.isAppIcon = true;
            } else if (localName.equalsIgnoreCase("SmallDesc")) {
                XMLParser.this.isSmallDesc = true;
            } else if (localName.equalsIgnoreCase("LongDesc")) {
                XMLParser.this.isLongDesc = true;
            } else if (localName.equalsIgnoreCase("SreenShot1")) {
                XMLParser.this.isSreenShot1 = true;
            } else if (localName.equalsIgnoreCase("SreenShot2")) {
                XMLParser.this.isSreenShot2 = true;
            } else if (localName.equalsIgnoreCase("Fstring")) {
                XMLParser.this.isFString = true;
            } else if (localName.equalsIgnoreCase("Rating")) {
                XMLParser.this.isRating = true;
            } else if (localName.equalsIgnoreCase("Currency")) {
                XMLParser.this.isCurrency = true;
            } else if (localName.equalsIgnoreCase("Price")) {
                XMLParser.this.isPrice = true;
            } else if (localName.equalsIgnoreCase("Src")) {
                XMLParser.this.isSrc = true;
            } else if (localName.equalsIgnoreCase("Link")) {
                XMLParser.this.isLink = true;
            } else if (localName.equalsIgnoreCase("Id")) {
                XMLParser.this.isId = true;
            } else if (localName.equalsIgnoreCase("ACU")) {
                XMLParser.this.isACU = true;
            } else if (localName.equalsIgnoreCase("AppLogo")) {
                XMLParser.this.isAppLogo = true;
            } else if (localName.equalsIgnoreCase("AppDesc")) {
                XMLParser.this.isAppDesc = true;
            } else if (localName.equalsIgnoreCase("Buy")) {
                XMLParser.this.isBuy = true;
            } else if (localName.equalsIgnoreCase("Msg")) {
                XMLParser.this.isMsg = true;
            } else if (localName.equalsIgnoreCase("More")) {
                XMLParser.this.isMore = true;
            } else if (localName.equalsIgnoreCase("ApplicationName")) {
                XMLParser.this.isApplicationName = true;
            } else if (localName.equalsIgnoreCase("Offset")) {
                XMLParser.this.isOffset = true;
            } else if (localName.equalsIgnoreCase("DUC")) {
                XMLParser.this.isDUC = true;
            } else if (localName.equalsIgnoreCase("AppVersionNo")) {
                XMLParser.this.isAppVersionNo = true;
            } else if (localName.equalsIgnoreCase("AppBuildNo")) {
                XMLParser.this.isAppBuildNo = true;
            } else if (localName.equalsIgnoreCase("AdsID")) {
                XMLParser.this.isAdsID = true;
            } else if (localName.equalsIgnoreCase("FileType")) {
                XMLParser.this.isFileType = true;
            } else if (localName.equalsIgnoreCase("BuyAppStatus")) {
                XMLParser.this.isBuyAppStatus = true;
            } else if (localName.equalsIgnoreCase("FreeAppStatus")) {
                XMLParser.this.isFreeAppStatus = true;
            } else if (localName.equalsIgnoreCase("EngVerNo")) {
                XMLParser.this.isEngVerNo = true;
            } else if (localName.equalsIgnoreCase("AdsStatus")) {
                XMLParser.this.isAdsStatus = true;
            } else if (localName.equalsIgnoreCase("CPButton")) {
                XMLParser.this.isCPButton = true;
            }
        }

        public void characters(char[] ch, int start, int length) {
            try {
                if (XMLParser.this.isAppName) {
                    XMLParser.this.isAppName = false;
                    for (int i = 0; i < ch.length; i++) {
                        if (ch[i] == '*') {
                            ch[i] = '&';
                        }
                    }
                    String str = new String(ch, start, length);
                    XMLParser.AppName.add(str);
                    System.out.println("AppName = " + str);
                } else if (XMLParser.this.isAppIcon) {
                    XMLParser.this.isAppIcon = false;
                    for (int i2 = 0; i2 < ch.length; i2++) {
                        if (ch[i2] == '*') {
                            ch[i2] = '&';
                        }
                    }
                    XMLParser.AppIcon.add(new String(ch, start, length));
                } else if (XMLParser.this.isSmallDesc) {
                    XMLParser.this.isSmallDesc = false;
                    for (int i3 = 0; i3 < ch.length; i3++) {
                        if (ch[i3] == '*') {
                            ch[i3] = '&';
                        }
                    }
                    XMLParser.SmallDesc.add(new String(ch, start, length));
                } else if (XMLParser.this.isLongDesc) {
                    XMLParser.this.isLongDesc = false;
                    for (int i4 = 0; i4 < ch.length; i4++) {
                        if (ch[i4] == '*') {
                            ch[i4] = '&';
                        }
                    }
                    XMLParser.LongDesc.add(new String(ch, start, length));
                } else if (XMLParser.this.isSreenShot1) {
                    XMLParser.this.isSreenShot1 = false;
                    for (int i5 = 0; i5 < ch.length; i5++) {
                        if (ch[i5] == '*') {
                            ch[i5] = '&';
                        }
                    }
                    XMLParser.SreenShot1.add(new String(ch, start, length));
                } else if (XMLParser.this.isSreenShot2) {
                    XMLParser.this.isSreenShot2 = false;
                    for (int i6 = 0; i6 < ch.length; i6++) {
                        if (ch[i6] == '*') {
                            ch[i6] = '&';
                        }
                    }
                    XMLParser.SreenShot2.add(new String(ch, start, length));
                } else if (XMLParser.this.isFString) {
                    XMLParser.this.isFString = false;
                    for (int i7 = 0; i7 < ch.length; i7++) {
                        if (ch[i7] == '*') {
                            ch[i7] = '&';
                        }
                    }
                    XMLParser.FString.add(new String(ch, start, length));
                } else if (XMLParser.this.isRating) {
                    XMLParser.this.isRating = false;
                    for (int i8 = 0; i8 < ch.length; i8++) {
                        if (ch[i8] == '*') {
                            ch[i8] = '&';
                        }
                    }
                    XMLParser.Rating.add(new String(ch, start, length));
                } else if (XMLParser.this.isCurrency) {
                    XMLParser.this.isCurrency = false;
                    for (int i9 = 0; i9 < ch.length; i9++) {
                        if (ch[i9] == '*') {
                            ch[i9] = '&';
                        }
                    }
                    XMLParser.Currency.add(new String(ch, start, length));
                } else if (XMLParser.this.isPrice) {
                    XMLParser.this.isPrice = false;
                    for (int i10 = 0; i10 < ch.length; i10++) {
                        if (ch[i10] == '*') {
                            ch[i10] = '&';
                        }
                    }
                    XMLParser.Price.add(new String(ch, start, length));
                } else if (XMLParser.this.isSrc) {
                    XMLParser.this.isSrc = false;
                    for (int i11 = 0; i11 < ch.length; i11++) {
                        if (ch[i11] == '*') {
                            ch[i11] = '&';
                        }
                    }
                    XMLParser.src.add(new String(ch, start, length));
                } else if (XMLParser.this.isLink) {
                    XMLParser.this.isLink = false;
                    for (int i12 = 0; i12 < ch.length; i12++) {
                        if (ch[i12] == '*') {
                            ch[i12] = '&';
                        }
                    }
                    XMLParser.link.add(new String(ch, start, length));
                } else if (XMLParser.this.isId) {
                    XMLParser.this.isId = false;
                    for (int i13 = 0; i13 < ch.length; i13++) {
                        if (ch[i13] == '*') {
                            ch[i13] = '&';
                        }
                    }
                    XMLParser.id.add(new String(ch, start, length));
                } else if (XMLParser.this.isACU) {
                    XMLParser.this.isACU = false;
                    for (int i14 = 0; i14 < ch.length; i14++) {
                        if (ch[i14] == '*') {
                            ch[i14] = '&';
                        }
                    }
                    XMLParser.ACU = new String(ch, start, length);
                } else if (XMLParser.this.isAppLogo) {
                    XMLParser.this.isAppLogo = false;
                    for (int i15 = 0; i15 < ch.length; i15++) {
                        if (ch[i15] == '*') {
                            ch[i15] = '&';
                        }
                    }
                    XMLParser.appLogo.add(new String(ch, start, length));
                } else if (XMLParser.this.isAppDesc) {
                    XMLParser.this.isAppDesc = false;
                    for (int i16 = 0; i16 < ch.length; i16++) {
                        if (ch[i16] == '*') {
                            ch[i16] = '&';
                        }
                    }
                    XMLParser.appDesc.add(handleString(new String(ch, start, length)));
                } else if (XMLParser.this.isBuy) {
                    XMLParser.this.isBuy = false;
                    for (int i17 = 0; i17 < ch.length; i17++) {
                        if (ch[i17] == '*') {
                            ch[i17] = '&';
                        }
                    }
                    XMLParser.appBuy.add(handleString(new String(ch, start, length)));
                } else if (XMLParser.this.isMsg) {
                    XMLParser.this.isMsg = false;
                    String str2 = new String(ch, start, length);
                    XMLParser.appMsg.add(str2);
                    System.out.println("appMsg =  " + str2);
                } else if (XMLParser.this.isMore) {
                    XMLParser.this.isMore = false;
                    for (int i18 = 0; i18 < ch.length; i18++) {
                        if (ch[i18] == '*') {
                            ch[i18] = '&';
                        }
                    }
                    XMLParser.MORE = new String(ch, start, length);
                } else if (XMLParser.this.isApplicationName) {
                    XMLParser.this.isApplicationName = false;
                    for (int i19 = 0; i19 < ch.length; i19++) {
                        if (ch[i19] == '*') {
                            ch[i19] = '&';
                        }
                    }
                    XMLParser.ApplicationName = new String(ch, start, length);
                } else if (XMLParser.this.isOffset) {
                    XMLParser.this.isOffset = false;
                    for (int i20 = 0; i20 < ch.length; i20++) {
                        if (ch[i20] == '*') {
                            ch[i20] = '&';
                        }
                    }
                    XMLParser.Offset = new String(ch, start, length);
                } else if (XMLParser.this.isDUC) {
                    XMLParser.this.isDUC = false;
                    for (int i21 = 0; i21 < ch.length; i21++) {
                        if (ch[i21] == '*') {
                            ch[i21] = '&';
                        }
                    }
                    XMLParser.DUC = new String(ch, start, length);
                } else if (XMLParser.this.isAppVersionNo) {
                    XMLParser.this.isAppVersionNo = false;
                    for (int i22 = 0; i22 < ch.length; i22++) {
                        if (ch[i22] == '*') {
                            ch[i22] = '&';
                        }
                    }
                    XMLParser.AppVersionNo = new String(ch, start, length);
                } else if (XMLParser.this.isAppBuildNo) {
                    XMLParser.this.isAppBuildNo = false;
                    for (int i23 = 0; i23 < ch.length; i23++) {
                        if (ch[i23] == '*') {
                            ch[i23] = '&';
                        }
                    }
                    XMLParser.AppBuildNo = new String(ch, start, length);
                } else if (XMLParser.this.isAdsID) {
                    XMLParser.this.isAdsID = false;
                    for (int i24 = 0; i24 < ch.length; i24++) {
                        if (ch[i24] == '*') {
                            ch[i24] = '&';
                        }
                    }
                    XMLParser.AdsID = new String(ch, start, length);
                } else if (XMLParser.this.isFileType) {
                    XMLParser.this.isFileType = false;
                    for (int i25 = 0; i25 < ch.length; i25++) {
                        if (ch[i25] == '*') {
                            ch[i25] = '&';
                        }
                    }
                    XMLParser.FileType = new String(ch, start, length);
                } else if (XMLParser.this.isBuyAppStatus) {
                    XMLParser.this.isBuyAppStatus = false;
                    for (int i26 = 0; i26 < ch.length; i26++) {
                        if (ch[i26] == '*') {
                            ch[i26] = '&';
                        }
                    }
                    XMLParser.BuyAppStatus = new String(ch, start, length);
                } else if (XMLParser.this.isFreeAppStatus) {
                    XMLParser.this.isFreeAppStatus = false;
                    for (int i27 = 0; i27 < ch.length; i27++) {
                        if (ch[i27] == '*') {
                            ch[i27] = '&';
                        }
                    }
                    XMLParser.FreeAppStatus = new String(ch, start, length);
                } else if (XMLParser.this.isEngVerNo) {
                    XMLParser.this.isEngVerNo = false;
                    for (int i28 = 0; i28 < ch.length; i28++) {
                        if (ch[i28] == '*') {
                            ch[i28] = '&';
                        }
                    }
                    XMLParser.EngVerNo = new String(ch, start, length);
                } else if (XMLParser.this.isAdsStatus) {
                    XMLParser.this.isAdsStatus = false;
                    for (int i29 = 0; i29 < ch.length; i29++) {
                        if (ch[i29] == '*') {
                            ch[i29] = '&';
                        }
                    }
                    XMLParser.AdsStatus = new String(ch, start, length);
                } else if (XMLParser.this.isCPButton) {
                    XMLParser.this.isCPButton = false;
                    for (int i30 = 0; i30 < ch.length; i30++) {
                        if (ch[i30] == '*') {
                            ch[i30] = '&';
                        }
                    }
                    XMLParser.CPButtonStatus = new String(ch, start, length);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public String handleString(String str) {
            boolean repeat = true;
            while (repeat) {
                if (str.contains("&amp;")) {
                    str = str.replaceFirst("&amp;", "&");
                    repeat = true;
                } else {
                    repeat = false;
                }
            }
            return str;
        }
    }
}
