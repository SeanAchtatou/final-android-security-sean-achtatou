package android.engine;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.mine.test_lite.MainMenu;

public class UpdateDialog extends Activity {
    static Config config;
    public static boolean exitApp = false;
    public static boolean installUpdate = false;
    private static int installationCode;
    public static boolean showingUpdateDialog = false;
    EngineIO engineIO;
    Handler handler2 = new Handler() {
        public void handleMessage(Message msg) {
            UpdateDialog.this.progressDialog.dismiss();
            if (msg.getData().getBoolean("DownLoadResult")) {
                UpdateDialog.this.showInstallDialog("Download Complete. Install Now ", true);
            } else {
                UpdateDialog.this.showInstallDialog("Error in downloading. Please try later", false);
            }
        }
    };
    NetHandler netHandler;
    ProgressDialog progressDialog;
    String status = "";
    private String tag = "Engine";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        this.netHandler = new NetHandler(this);
        config = new Config(this);
        this.engineIO = new EngineIO(this);
        this.status = getIntent().getExtras().getString("Status");
        showUpdateDownloadDialog();
    }

    public void showUpdateDownloadDialog() {
        showingUpdateDialog = true;
        final AlertDialog updateDownloadDialog = new AlertDialog.Builder(this).create();
        updateDownloadDialog.setTitle("Download Updates");
        updateDownloadDialog.setMessage("Updated version of '" + config.getAppName() + "' available click yes to download");
        updateDownloadDialog.setButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                if (Environment.getExternalStorageState().equals("mounted")) {
                    UpdateDialog.this.progressDialog = ProgressDialog.show(UpdateDialog.this, "Downloading...", "Please wait..", true);
                    new Thread() {
                        public void run() {
                            super.run();
                            Message message = new Message();
                            Bundle bundle = new Bundle();
                            try {
                                bundle.putBoolean("DownLoadResult", UpdateDialog.this.netHandler.DownloadFromUrl(UpdateDialog.this.engineIO.getforceUpdateLinkToDownload(), String.valueOf(UpdateDialog.config.getAppName()) + ".apk"));
                                message.setData(bundle);
                                UpdateDialog.this.handler2.sendMessage(message);
                            } catch (Exception e) {
                                System.out.println("exception occoured in downloading updates = " + e.toString());
                                bundle.putBoolean("DownLoadResult", false);
                                message.setData(bundle);
                                UpdateDialog.this.handler2.sendMessage(message);
                            }
                        }
                    }.start();
                    return;
                }
                UpdateDialog.this.engineIO.showPrompt(UpdateDialog.this, "Please insert SD Card");
            }
        });
        String buttonText = "";
        final String updateType = this.engineIO.getUpdateType();
        if (updateType.equals("ForceUpdate")) {
            buttonText = "Exit";
        } else if (updateType.equals("ChoiceUpdate")) {
            buttonText = "Cancel";
        }
        updateDownloadDialog.setButton2(buttonText, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                updateDownloadDialog.dismiss();
                UpdateDialog.this.finish();
                if (updateType.equals("ForceUpdate")) {
                    UpdateDialog.exitApp = true;
                } else if (updateType.equals("ChoiceUpdate")) {
                    UpdateDialog.exitApp = false;
                    UpdateDialog.this.finish();
                }
            }
        });
        updateDownloadDialog.show();
    }

    /* access modifiers changed from: private */
    public void showInstallDialog(String msg, final boolean status1) {
        AlertDialog prompt = new AlertDialog.Builder(this).create();
        prompt.setTitle("Note:");
        prompt.setMessage(msg);
        prompt.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dia, int arg1) {
                dia.dismiss();
                if (UpdateDialog.this.status.equals("Finish")) {
                    if (status1 && MainMenu.mainMenuStarted) {
                        UpdateDialog.installUpdate = true;
                    } else if (!MainMenu.mainMenuStarted) {
                        UpdateDialog.installUpdatedFile(UpdateDialog.this);
                        UpdateDialog.this.finish();
                    } else if (!status1) {
                        UpdateDialog.exitApp = true;
                    }
                    UpdateDialog.this.finish();
                } else if (!UpdateDialog.this.status.equals("install")) {
                } else {
                    if (status1) {
                        UpdateDialog.installUpdatedFile(UpdateDialog.this);
                        UpdateDialog.this.finish();
                        return;
                    }
                    UpdateDialog.this.finish();
                }
            }
        });
        prompt.show();
    }

    public static void installUpdatedFile(Context context) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.setDataAndType(Uri.parse("file:///sdcard/" + config.getAppName() + ".apk"), "application/vnd.android.package-archive");
        ((Activity) context).startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.v(this.tag, "resultCode = " + resultCode);
        if (requestCode == installationCode && resultCode == 0) {
            this.engineIO.showPrompt(this, "Please uninstall current version to install updated version and install updated version from SD Card/" + AppConstants.updatedFileName);
        }
    }
}
