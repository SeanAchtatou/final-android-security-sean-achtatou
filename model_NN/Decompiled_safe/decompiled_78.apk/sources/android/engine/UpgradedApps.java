package android.engine;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AbsListView;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mine.test_lite.R;
import java.util.Vector;

public class UpgradedApps extends Activity {
    static Bitmap[] icon;
    Vector<String> SmallDesc = new Vector<>();
    Vector<String> appIcon = new Vector<>();
    Vector<String> appName = new Vector<>();
    Button cancel;
    Bitmap defaultIcon;
    EngineIO engineIO;
    public Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            UpgradedApps.this.upgradedAppsList.setAdapter(new MyExpandableListAdapter(UpgradedApps.this));
        }
    };
    LayoutInflater layoutInflater;
    Vector<String> longDesc = new Vector<>();
    NetHandler netHandler;
    Resources resources;
    Vector<String> screenShot1 = new Vector<>();
    Vector<String> screenShot2 = new Vector<>();
    ExpandableListView upgradedAppsList;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.engine_upgraded_apps);
        this.layoutInflater = getLayoutInflater();
        this.engineIO = new EngineIO(this);
        this.netHandler = new NetHandler(this);
        this.resources = getResources();
        this.defaultIcon = BitmapFactory.decodeResource(this.resources, R.drawable.android_icon);
        icon = new Bitmap[this.engineIO.getUpgradedAppCount()];
        for (int i = 0; i < icon.length; i++) {
            icon[i] = this.defaultIcon;
        }
        initIcons();
        this.upgradedAppsList = (ExpandableListView) findViewById(R.id.ExpandableListView_upgraded_apps);
        this.upgradedAppsList.setAdapter(new MyExpandableListAdapter(this));
        this.cancel = (Button) findViewById(R.id.Button_cancel_upgraded_app);
        String appState = getIntent().getExtras().getString("AppState");
        if (appState.equals("Entry")) {
            this.cancel.setText("Back");
        } else if (appState.equals("Exit")) {
            this.cancel.setText("Exit");
        }
        this.cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                UpgradedApps.this.finish();
            }
        });
    }

    public void initIcons() {
        new Thread() {
            public void run() {
                super.run();
                for (int i = 0; i < UpgradedApps.this.engineIO.getUpgradedAppCount(); i++) {
                    Bitmap bmp = UpgradedApps.this.netHandler.getImageFromUrl(UpgradedApps.this.engineIO.getAppIcon().get(i));
                    if (bmp != null) {
                        UpgradedApps.icon[i] = bmp;
                    }
                    System.out.println("gettinng " + i + " icon");
                    UpgradedApps.this.handler.sendEmptyMessage(0);
                }
            }
        }.start();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.engineIO.close();
    }

    public class MyExpandableListAdapter extends BaseExpandableListAdapter {
        Context context;
        private String[] groups;

        public MyExpandableListAdapter(Context context2) {
            this.context = context2;
            this.groups = new String[UpgradedApps.this.engineIO.getAppName().size()];
        }

        public long getChildId(int groupPosition, int childPosition) {
            return (long) childPosition;
        }

        public int getChildrenCount(int groupPosition) {
            return 1;
        }

        public TextView getGenericView() {
            AbsListView.LayoutParams lp = new AbsListView.LayoutParams(-2, 64);
            TextView textView = new TextView(UpgradedApps.this);
            textView.setTextSize(20.0f);
            textView.setLayoutParams(lp);
            textView.setGravity(19);
            textView.setPadding(36, 0, 0, 0);
            return textView;
        }

        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            View v = UpgradedApps.this.layoutInflater.inflate((int) R.layout.engine_upgraded_file2, (ViewGroup) null);
            WebView iconWebView = (WebView) v.findViewById(R.id.webview_icon);
            WebView screenShot1WebView = (WebView) v.findViewById(R.id.webview_ss1);
            WebView screenShot2WebView = (WebView) v.findViewById(R.id.webview_ss2);
            final int i = groupPosition;
            ((Button) v.findViewById(R.id.Button_buy_upgraded_app)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    UpgradedApps.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(UpgradedApps.this.engineIO.getUpgradedAppBuyLinks().get(i))));
                }
            });
            iconWebView.loadData("<html><body><img src='" + UpgradedApps.this.engineIO.getAppIcon().get(groupPosition) + "' width=50 height=50></body></html>", "text/html", "utf-8");
            iconWebView.setWebViewClient(new HelloWebViewClient());
            screenShot1WebView.loadData("<html><body><img src='" + UpgradedApps.this.engineIO.getScreenShot1().get(groupPosition) + "' width=150 height=200></body></html>", "text/html", "utf-8");
            screenShot1WebView.setWebViewClient(new HelloWebViewClient());
            screenShot2WebView.loadData("<html><body><img src='" + UpgradedApps.this.engineIO.getScreenShot2().get(groupPosition) + "' width=150 height=200></body></html>", "text/html", "utf-8");
            screenShot2WebView.setWebViewClient(new HelloWebViewClient());
            ((TextView) v.findViewById(R.id.TextView_upgraded_app_name)).setText(UpgradedApps.this.engineIO.getAppName().get(groupPosition));
            ((TextView) v.findViewById(R.id.TextView_upgraded_app_sdesc)).setText(UpgradedApps.this.engineIO.getSmallDesc().get(groupPosition));
            ((TextView) v.findViewById(R.id.TextView_upgraded_app_ldesc)).setText(UpgradedApps.this.engineIO.getLongDesc().get(groupPosition));
            return v;
        }

        public Object getGroup(int groupPosition) {
            return this.groups[groupPosition];
        }

        public int getGroupCount() {
            return this.groups.length;
        }

        public long getGroupId(int groupPosition) {
            return (long) groupPosition;
        }

        /* Debug info: failed to restart local var, previous not found, register: 24 */
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            try {
                RelativeLayout relativeLayout = new RelativeLayout(this.context);
                relativeLayout.setPadding(0, 5, 0, 5);
                new RelativeLayout.LayoutParams(-1, -1);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams.addRule(9);
                ImageView imageView = new ImageView(this.context);
                imageView.setId(1);
                imageView.setPadding(5, 0, 0, 0);
                imageView.setImageBitmap(UpgradedApps.icon[groupPosition]);
                relativeLayout.addView(imageView, layoutParams);
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams2.addRule(1, imageView.getId());
                TextView textView = new TextView(this.context);
                textView.setText(UpgradedApps.this.engineIO.getAppName().get(groupPosition));
                textView.setTextColor(-16777216);
                textView.setTextSize(20.0f);
                textView.setPadding(5, 0, 0, 0);
                textView.setTypeface(Typeface.SERIF);
                textView.setId(2);
                RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams3.addRule(1, imageView.getId());
                layoutParams3.addRule(3, textView.getId());
                relativeLayout.addView(textView, layoutParams2);
                TextView textView2 = new TextView(this.context);
                String desc = UpgradedApps.this.engineIO.getSmallDesc().get(groupPosition);
                if (desc.length() > 18) {
                    desc = desc.substring(0, 18);
                }
                textView2.setText(String.valueOf(desc) + "...");
                textView2.setPadding(5, 0, 0, 0);
                textView2.setId(3);
                textView2.setTextColor(-16777216);
                relativeLayout.addView(textView2, layoutParams3);
                RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams4.addRule(11);
                TextView textView3 = new TextView(this.context);
                if (UpgradedApps.this.engineIO.getPrice().get(groupPosition) == null || UpgradedApps.this.engineIO.getPrice().get(groupPosition).charAt(0) != '0') {
                    textView3.setText(UpgradedApps.this.engineIO.getPrice().get(groupPosition));
                } else {
                    textView3.setText("Free");
                }
                textView3.setPadding(0, 5, 5, 0);
                textView3.setTextColor(-16777216);
                textView3.setId(4);
                relativeLayout.addView(textView3, layoutParams4);
                RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(50, -2);
                layoutParams5.addRule(11);
                layoutParams5.addRule(3, textView3.getId());
                ImageView imageView2 = new ImageView(this.context);
                imageView2.setId(5);
                imageView2.setPadding(0, 0, 5, 0);
                if (Integer.parseInt(UpgradedApps.this.engineIO.getRating().get(groupPosition)) == 1) {
                    imageView2.setImageResource(R.drawable.star1);
                } else if (Integer.parseInt(UpgradedApps.this.engineIO.getRating().get(groupPosition)) == 2) {
                    imageView2.setImageResource(R.drawable.star2);
                } else if (Integer.parseInt(UpgradedApps.this.engineIO.getRating().get(groupPosition)) == 3) {
                    imageView2.setImageResource(R.drawable.star3);
                } else if (Integer.parseInt(UpgradedApps.this.engineIO.getRating().get(groupPosition)) == 4) {
                    imageView2.setImageResource(R.drawable.star4);
                } else if (Integer.parseInt(UpgradedApps.this.engineIO.getRating().get(groupPosition)) == 5) {
                    imageView2.setImageResource(R.drawable.star5);
                } else if (Integer.parseInt(UpgradedApps.this.engineIO.getRating().get(groupPosition)) == 0) {
                    imageView2.setImageResource(R.drawable.star0);
                }
                relativeLayout.addView(imageView2, layoutParams5);
                return relativeLayout;
            } catch (Exception e) {
                e.printStackTrace();
                return convertView;
            }
        }

        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

        public boolean hasStableIds() {
            return true;
        }

        public Object getChild(int arg0, int arg1) {
            return null;
        }
    }
}
