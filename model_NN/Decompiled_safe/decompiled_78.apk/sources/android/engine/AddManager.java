package android.engine;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Handler;
import android.view.View;
import android.webkit.WebSettings;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.mine.test_lite.Help;
import com.mine.test_lite.MainMenu;
import com.mine.test_lite.MineSweeper_Easy;
import com.mine.test_lite.MineSweeper_Expert;
import com.mine.test_lite.MineSweeper_Medium;
import com.mine.test_lite.Options;
import com.mine.test_lite.R;
import com.mine.test_lite.ScoreSubmit;
import com.mine.test_lite.Settings;
import com.mine.test_lite.ShareOnFacebook;
import com.mine.test_lite.score;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Timer;
import java.util.TimerTask;

public class AddManager {
    public static String ACU;
    static int CPAddsDuration = 30;
    public static String activityState = "Resumed";
    static int addCounter = 0;
    static long addInfoFetchedTime = 0;
    public static String addPosions;
    public static int addsDuration = 30;
    public static String backGroundAdds = "";
    static Config config;
    public static String cpStatus = "Starting";
    public static Activity currentActivity;
    static int currentActivityCode = 1;
    public static EngineIO engineIO;
    public static boolean fetchCPAds = true;
    static Handler handle;
    static boolean isProcessing;
    static ImageView ivDown;
    static ImageView ivUp;
    public static Bitmap lowerBitmap;
    static Bitmap lowerBmp;
    public static ImageView lowerImage;
    /* access modifiers changed from: private */
    public static String lower_redirection_url;
    static int migitalBanner = 0;
    static boolean migitalBannerFetched = false;
    static NetHandler netHandler;
    static int secCounter = 0;
    static TextView tvDown;
    static TextView tvUp;
    public static Bitmap upperBitmap;
    static Bitmap upperBmp;
    public static ImageView upperImage;
    /* access modifiers changed from: private */
    public static String upper_redirection_url;
    public static XMLParser xmlParser;
    Context context;
    Runnable defaultAddsRunnable = new Runnable() {
        public void run() {
            AddManager.showDefaultMigitalImages();
        }
    };
    Runnable enableLowerAdd = new Runnable() {
        /* Debug info: failed to restart local var, previous not found, register: 4 */
        public void run() {
            try {
                if (AppConstants.lower_add.equals("image")) {
                    if (AppConstants.bottom_add == null) {
                        AppConstants.lower_add = "default";
                        AddManager.tvDown.setVisibility(8);
                        AddManager.ivDown.setVisibility(0);
                        AddManager.ivDown.setImageBitmap(AppConstants.default_Bottom);
                        AddManager.lower_redirection_url = AppConstants.default_migital_adds_Url;
                        return;
                    }
                    AddManager.tvDown.setVisibility(8);
                    AddManager.ivDown.setVisibility(0);
                    AddManager.ivDown.setImageBitmap(AppConstants.bottom_add);
                    AddManager.lower_redirection_url = "http://" + AppConstants.AdsHostIP + "/Android/V1/AdsClick.aspx?id=" + AppConstants.Id.get(1).toString() + "&Target=" + AppConstants.Link.get(1).toString();
                } else if (AppConstants.lower_add.equals("text")) {
                    AddManager.ivDown.setVisibility(8);
                    AddManager.tvDown.setVisibility(0);
                    AddManager.tvDown.setText(AppConstants.Src.get(1).toString());
                    AddManager.lower_redirection_url = "http://" + AppConstants.AdsHostIP + "/Android/V1/AdsClick.aspx?id=" + AppConstants.Id.get(1).toString() + "&Target=" + AppConstants.Link.get(1).toString();
                } else if (AppConstants.lower_add.equals("default")) {
                    AddManager.tvDown.setVisibility(8);
                    AddManager.ivDown.setVisibility(0);
                    AddManager.ivDown.setImageBitmap(AppConstants.default_Bottom);
                    AddManager.lower_redirection_url = AppConstants.default_migital_adds_Url;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    Runnable enableUpperAdd = new Runnable() {
        /* Debug info: failed to restart local var, previous not found, register: 4 */
        public void run() {
            try {
                if (AppConstants.upper_add.equals("image")) {
                    if (AppConstants.top_add == null) {
                        AppConstants.upper_add = "default";
                        AddManager.tvUp.setVisibility(8);
                        AddManager.ivUp.setVisibility(0);
                        AddManager.ivUp.setImageBitmap(AppConstants.default_top);
                        AddManager.upper_redirection_url = AppConstants.default_migital_adds_Url;
                        return;
                    }
                    AddManager.tvUp.setVisibility(8);
                    AddManager.ivUp.setVisibility(0);
                    AddManager.ivUp.setImageBitmap(AppConstants.top_add);
                    AddManager.upper_redirection_url = "http://" + AppConstants.AdsHostIP + "/Android/V1/AdsClick.aspx?id=" + AppConstants.Id.get(0).toString() + "&Target=" + AppConstants.Link.get(0).toString();
                } else if (AppConstants.upper_add.equals("text")) {
                    AddManager.ivUp.setVisibility(8);
                    AddManager.tvUp.setVisibility(0);
                    AddManager.tvUp.setText(AppConstants.Src.get(0).toString());
                    AddManager.upper_redirection_url = "http://" + AppConstants.AdsHostIP + "/Android/V1/AdsClick.aspx?id=" + AppConstants.Id.get(0).toString() + "&Target=" + AppConstants.Link.get(0).toString();
                } else if (AppConstants.upper_add.equals("default")) {
                    AddManager.tvUp.setVisibility(8);
                    AddManager.ivUp.setVisibility(0);
                    AddManager.ivUp.setImageBitmap(AppConstants.default_top);
                    AddManager.upper_redirection_url = AppConstants.default_migital_adds_Url;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    private Button hideButton;
    HorizontalScrollView horizontalScrollView;
    ImageView leftArrow;
    View.OnClickListener lowerAddClick = new View.OnClickListener() {
        public void onClick(View arg0) {
            System.out.println("lower_redirection_url = " + AddManager.lower_redirection_url);
            AddManager.this.context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(AddManager.lower_redirection_url == null ? AppConstants.default_migital_adds_Url : AddManager.lower_redirection_url)));
        }
    };
    LinearLayout lowerLinearLayout;
    View.OnClickListener moreAppClick = new View.OnClickListener() {
        public void onClick(View arg0) {
            if (AppConstants.more_cp_apps == null) {
                AppConstants.more_cp_apps = "http://migital.com/scms/wap/index.aspx";
            }
            if (AppConstants.more_cp_apps.startsWith("http://") || AppConstants.more_cp_apps.startsWith("https://")) {
                AddManager.this.context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(AppConstants.more_cp_apps)));
                return;
            }
            if (AppConstants.more_cp_apps == null) {
                AppConstants.more_cp_apps = "Migital";
            }
            AddManager.this.startMarket(AppConstants.more_cp_apps);
        }
    };
    LinearLayout moreAppContainerLayout;
    MyTimerTask mtt;
    CrossPromAds promAds;
    private Resources res;
    ImageView rightArrow;
    public Timer timer;
    boolean timerScheduled;
    View.OnClickListener upperAddClick = new View.OnClickListener() {
        public void onClick(View arg0) {
            System.out.println("upper_redirection_url = " + AddManager.upper_redirection_url);
            AddManager.this.context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(AddManager.upper_redirection_url == null ? AppConstants.default_migital_adds_Url : AddManager.upper_redirection_url)));
        }
    };
    LinearLayout upperLinearLayout;
    WebSettings webSettings;

    public AddManager(Context context2) {
        this.context = context2;
        config = new Config(context2);
        xmlParser = new XMLParser();
        netHandler = new NetHandler(context2);
        this.mtt = new MyTimerTask();
        handle = new Handler();
        engineIO = new EngineIO(context2);
        addPosions = engineIO.getAddPosions();
        this.promAds = new CrossPromAds(context2);
    }

    public void init(int activityCode) {
        currentActivityCode = activityCode;
        getCurrentActivity(currentActivityCode);
        initializeAllControls();
    }

    public void displayAdds() {
        activityState = "Resumed";
        System.out.println("engineIO.isAddEnable() = " + engineIO.isAddEnable() + " engineIO.getAuthorizationStatus() = " + engineIO.getAuthorizationStatus());
        if (engineIO.isAddEnable() || !engineIO.getAuthorizationStatus()) {
            if (!this.timerScheduled) {
                schedulTimer();
                this.timerScheduled = true;
            }
        } else if (config.getFTYPE().equals("3") && !this.timerScheduled) {
            schedulTimer();
            this.timerScheduled = true;
        }
    }

    public void pauseAdds() {
        if (this.timerScheduled) {
            this.timer.cancel();
            this.timerScheduled = false;
        }
    }

    public void schedulTimer() {
        this.timer = new Timer();
        this.mtt = new MyTimerTask();
        this.timer.schedule(this.mtt, 0, 1000);
        this.timerScheduled = true;
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public void getCurrentActivity(int activityCode) {
        switch (activityCode) {
            case 1:
                currentActivity = (MainMenu) this.context;
                break;
            case 2:
                currentActivity = (MineSweeper_Easy) this.context;
                break;
            case 3:
                currentActivity = (MineSweeper_Medium) this.context;
                break;
            case 4:
                currentActivity = (MineSweeper_Expert) this.context;
                break;
            case 5:
                currentActivity = (Help) this.context;
                break;
            case 6:
                currentActivity = (AboutUsMenu) this.context;
                break;
            case 7:
                currentActivity = (AboutCompany) this.context;
                break;
            case 8:
                currentActivity = (AboutProduct) this.context;
                break;
            case 9:
                currentActivity = (Options) this.context;
                break;
            case 10:
                currentActivity = (Settings) this.context;
                break;
            case 11:
                currentActivity = (ScoreSubmit) this.context;
                break;
            case 12:
                currentActivity = (ShareOnFacebook) this.context;
                break;
            case 13:
                currentActivity = (score) this.context;
                break;
            case 14:
                currentActivity = (FeedBackForm) this.context;
                break;
        }
        System.out.println("currentActivity = " + currentActivity);
    }

    public void removeAdds() {
        this.upperLinearLayout.setVisibility(8);
        this.lowerLinearLayout.setVisibility(8);
    }

    public void initializeAllControls() {
        this.upperLinearLayout = (LinearLayout) currentActivity.findViewById(R.id.LinearLayout_upper);
        this.lowerLinearLayout = (LinearLayout) currentActivity.findViewById(R.id.LinearLayout_lower);
        this.moreAppContainerLayout = (LinearLayout) currentActivity.findViewById(R.id.LinearLayout_more_app);
        ivUp = (ImageView) currentActivity.findViewById(R.id.ImageView_upper);
        ivDown = (ImageView) currentActivity.findViewById(R.id.ImageView_lower);
        tvUp = (TextView) currentActivity.findViewById(R.id.TextView_upper);
        tvDown = (TextView) currentActivity.findViewById(R.id.TextView_lower);
        if (currentActivityCode == 1) {
            this.hideButton = (Button) currentActivity.findViewById(R.id.Button_hide);
            if (cpStatus.equals("Starting")) {
                this.hideButton.setVisibility(8);
            }
        }
        this.res = this.context.getResources();
        if (AppConstants.default_top == null) {
            AppConstants.default_top = BitmapFactory.decodeResource(this.res, R.drawable.staticadd1);
        }
        if (AppConstants.default_Bottom == null) {
            AppConstants.default_Bottom = BitmapFactory.decodeResource(this.res, R.drawable.staticadd2);
        }
        this.promAds.getCPAdsControls();
        setControlFunctionality();
        showAdds();
    }

    public void showAdds() {
        System.out.println("addPosions = " + addPosions);
        if (addPosions.equals("ThirdParty(Upper & Lower) Adds only")) {
            this.upperLinearLayout.setVisibility(0);
            this.lowerLinearLayout.setVisibility(0);
            this.moreAppContainerLayout.setVisibility(8);
            if (!AppConstants.upper_add.equals("default")) {
                handle.post(this.enableUpperAdd);
            }
            if (!AppConstants.lower_add.equals("default")) {
                handle.post(this.enableLowerAdd);
            }
            if (AppConstants.upper_add.equals("default")) {
                showDefaultMigitalImages();
            }
        } else if (addPosions.equals("ThirdParty(Upper) Adds only")) {
            this.upperLinearLayout.setVisibility(0);
            this.lowerLinearLayout.setVisibility(8);
            this.moreAppContainerLayout.setVisibility(8);
            if (!AppConstants.upper_add.equals("default")) {
                handle.post(this.enableUpperAdd);
            }
            if (AppConstants.upper_add.equals("default")) {
                showDefaultMigitalImages();
            }
        } else if (addPosions.equals("ThirdParty(Lower) Adds only")) {
            this.moreAppContainerLayout.setVisibility(8);
            this.upperLinearLayout.setVisibility(8);
            this.lowerLinearLayout.setVisibility(0);
            if (!AppConstants.lower_add.equals("default")) {
                handle.post(this.enableLowerAdd);
            }
        } else if (addPosions.equals("Cross Promotional Adds only")) {
            this.upperLinearLayout.setVisibility(8);
            this.lowerLinearLayout.setVisibility(8);
            if (showCPAdds()) {
                this.promAds.displayCPAds();
            } else {
                this.moreAppContainerLayout.setVisibility(8);
            }
        } else if (addPosions.equals("ThirdParty(Upper) & Cross Promotional Adds only")) {
            this.upperLinearLayout.setVisibility(0);
            this.lowerLinearLayout.setVisibility(8);
            if (!AppConstants.upper_add.equals("default")) {
                handle.post(this.enableUpperAdd);
            }
            if (AppConstants.upper_add.equals("default")) {
                showDefaultMigitalImages();
            }
            if (showCPAdds()) {
                this.promAds.displayCPAds();
            } else {
                this.moreAppContainerLayout.setVisibility(8);
            }
        } else if (addPosions.equals("ThirdParty(Lower) & Cross Promotional Adds only")) {
            this.upperLinearLayout.setVisibility(8);
            this.lowerLinearLayout.setVisibility(0);
            if (!AppConstants.lower_add.equals("default")) {
                handle.post(this.enableLowerAdd);
            }
            if (showCPAdds()) {
                this.promAds.displayCPAds();
            } else {
                this.moreAppContainerLayout.setVisibility(8);
            }
        } else if (addPosions.equals("Both ThirdParty and Cross Promotional Adds")) {
            this.upperLinearLayout.setVisibility(0);
            this.lowerLinearLayout.setVisibility(0);
            if (!AppConstants.upper_add.equals("default")) {
                handle.post(this.enableUpperAdd);
            }
            if (!AppConstants.lower_add.equals("default")) {
                handle.post(this.enableLowerAdd);
            }
            if (AppConstants.upper_add.equals("default")) {
                showDefaultMigitalImages();
            }
            if (showCPAdds()) {
                this.promAds.displayCPAds();
            } else {
                this.moreAppContainerLayout.setVisibility(8);
            }
        }
    }

    public void setControlFunctionality() {
        tvUp.setOnClickListener(this.upperAddClick);
        tvDown.setOnClickListener(this.lowerAddClick);
        ivUp.setOnClickListener(this.upperAddClick);
        ivDown.setOnClickListener(this.lowerAddClick);
    }

    public void startMarket(String keyword) {
        if (keyword == null) {
            keyword = "migital";
        } else if (keyword.equals("") || keyword.length() == 0) {
            keyword = "migital";
        }
        currentActivity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=" + keyword)));
    }

    public String getAddType(String str) {
        if (str.startsWith("http:") || str.startsWith("https:")) {
            return "image";
        }
        return "text";
    }

    public synchronized void getAdds() {
        System.out.println("getting adds addscounter = " + addCounter + " and secCounter = " + secCounter);
        try {
            new Thread(new Runnable() {
                public void run() {
                    try {
                        AppConstants.addsXml = AddManager.netHandler.getDataFrmUrl(AppConstants.add_request_url);
                        if (AppConstants.addsXml != null) {
                            AddManager.xmlParser.parseXML(AppConstants.addsXml);
                            if (AddManager.xmlParser.getSrc().size() > 0) {
                                AppConstants.Src = AddManager.xmlParser.getSrc();
                            }
                            if (AddManager.xmlParser.getLink().size() > 0) {
                                AppConstants.Link = AddManager.xmlParser.getLink();
                            }
                            if (AddManager.xmlParser.getId().size() > 0) {
                                AppConstants.Id = AddManager.xmlParser.getId();
                            }
                            if (!AddManager.xmlParser.getACU().equals("")) {
                                AddManager.ACU = AddManager.xmlParser.getACU();
                            }
                            AddManager.this.hitBackGroundAdd();
                            if (AddManager.addPosions.equals("ThirdParty(Upper & Lower) Adds only") || AddManager.addPosions.equals("ThirdParty(Upper) Adds only") || AddManager.addPosions.equals("ThirdParty(Upper) & Cross Promotional Adds only") || AddManager.addPosions.equals("Both ThirdParty and Cross Promotional Adds")) {
                                if (AddManager.this.getAddType(AppConstants.Src.get(0)).equals("image")) {
                                    AppConstants.upper_add = "image";
                                    AddManager.upperBmp = AddManager.netHandler.getImageFromUrl(AppConstants.Src.get(0).toString());
                                    if (AddManager.upperBmp != null && !AddManager.this.isBlankImage(AddManager.upperBmp)) {
                                        AppConstants.top_add = AddManager.upperBmp;
                                    }
                                } else if (AddManager.this.getAddType(AppConstants.Src.get(0)).equals("text")) {
                                    AppConstants.upper_add = "text";
                                }
                                AddManager.handle.post(AddManager.this.enableUpperAdd);
                            }
                            if (AddManager.addPosions.equals("ThirdParty(Upper & Lower) Adds only") || AddManager.addPosions.equals("ThirdParty(Lower) Adds only") || AddManager.addPosions.equals("ThirdParty(Lower) & Cross Promotional Adds only") || AddManager.addPosions.equals("Both ThirdParty and Cross Promotional Adds")) {
                                if (AddManager.this.getAddType(AppConstants.Src.get(1)).equals("image")) {
                                    AppConstants.lower_add = "image";
                                    AddManager.lowerBmp = AddManager.netHandler.getImageFromUrl(AppConstants.Src.get(1).toString());
                                    if (AddManager.lowerBmp != null && !AddManager.this.isBlankImage(AddManager.lowerBmp)) {
                                        AppConstants.bottom_add = AddManager.lowerBmp;
                                    }
                                    if (AppConstants.bottom_add == null) {
                                        AppConstants.lower_add = "default";
                                    }
                                } else if (AddManager.this.getAddType(AppConstants.Src.get(1)).equals("text")) {
                                    AppConstants.lower_add = "text";
                                }
                                AddManager.handle.post(AddManager.this.enableLowerAdd);
                            }
                        }
                    } catch (Exception e) {
                        System.out.println("Exception = " + e.toString());
                        if (!AddManager.addPosions.equals("ThirdParty(Upper & Lower) Adds only") && !AddManager.addPosions.equals("ThirdParty(Upper) Adds only") && !AddManager.addPosions.equals("ThirdParty(Upper) & Cross Promotional Adds only") && !AddManager.addPosions.equals("Both ThirdParty and Cross Promotional Adds")) {
                            return;
                        }
                        if (AppConstants.bottom_add == null || AppConstants.top_add == null) {
                            AppConstants.upper_add = "default";
                            AppConstants.lower_add = "default";
                            AddManager.handle.post(AddManager.this.defaultAddsRunnable);
                            return;
                        }
                        AppConstants.upper_add = "image";
                        AppConstants.lower_add = "image";
                        AddManager.handle.post(AddManager.this.enableLowerAdd);
                        AddManager.handle.post(AddManager.this.enableUpperAdd);
                    }
                }
            }).start();
        } catch (Exception e) {
            System.out.println("Exception Found = " + e);
            if (addPosions.equals("ThirdParty(Upper & Lower) Adds only") || addPosions.equals("ThirdParty(Upper) Adds only") || addPosions.equals("ThirdParty(Upper) & Cross Promotional Adds only") || addPosions.equals("Both ThirdParty and Cross Promotional Adds")) {
                if (AppConstants.bottom_add == null || AppConstants.top_add == null) {
                    AppConstants.upper_add = "default";
                    AppConstants.lower_add = "default";
                    handle.post(this.defaultAddsRunnable);
                } else {
                    AppConstants.upper_add = "image";
                    AppConstants.lower_add = "image";
                    handle.post(this.enableLowerAdd);
                    handle.post(this.enableUpperAdd);
                }
            }
        }
    }

    public static void showDefaultMigitalImages() {
        tvUp.setVisibility(8);
        tvDown.setVisibility(8);
        ivUp.setVisibility(0);
        ivDown.setVisibility(0);
        ivUp.setImageBitmap(getMigtalBanner());
        ivDown.setImageBitmap(getMigtalBanner());
    }

    class MyTimerTask extends TimerTask {
        MyTimerTask() {
        }

        public void run() {
            if (AddManager.activityState.equals("Resumed")) {
                if (!AddManager.config.getFTYPE().equals("3")) {
                    AddManager.this.callMethodsForAdds();
                } else if (AddManager.showCPAdds() && AddManager.secCounter % AddManager.CPAddsDuration == 0 && AddManager.fetchCPAds) {
                    AddManager.this.promAds.getCPAds();
                }
            } else if (AddManager.activityState.equals("Paused") && AddManager.backGroundAdds.equalsIgnoreCase("1")) {
                if (!AddManager.config.getFTYPE().equals("3")) {
                    AddManager.this.callMethodsForAdds();
                } else if (AddManager.showCPAdds() && AddManager.secCounter % AddManager.CPAddsDuration == 0 && AddManager.fetchCPAds) {
                    AddManager.this.promAds.getCPAds();
                }
            }
            AddManager.secCounter++;
        }
    }

    public static boolean showCPAdds() {
        if (currentActivityCode == 1 || currentActivityCode == 5 || currentActivityCode == 6 || currentActivityCode == 7 || currentActivityCode == 8 || currentActivityCode == 9 || currentActivityCode == 10 || currentActivityCode == 14) {
            return true;
        }
        if (activityState.equals("Paused")) {
            return false;
        }
        return false;
    }

    public void hitBackGroundAdd() {
        addCounter++;
        if (ACU != null && !ACU.equals("")) {
            String[] tokens = ACU.split("#");
            String tempCounter = tokens[0];
            if (addCounter % Integer.parseInt(tokens[0]) == 0) {
                System.out.println("counter = " + addCounter + " and tempcounter = " + tempCounter);
                netHandler.getDataFrmUrl(tokens[1]);
            }
        }
    }

    public void fetchAddsDetails() {
        new Thread() {
            public void run() {
                try {
                    super.run();
                    if (AddManager.secCounter % (Integer.parseInt(AppConstants.sessionTime) * 60) == 0) {
                        System.out.println("AddManager.fetchAddsDetails() secCounter = " + AddManager.secCounter + " and sessionTime = " + AppConstants.sessionTime + " AppConstants.primaryAddsInfoFetchingUrl = " + AppConstants.primaryAddsInfoFetchingUrl);
                        String[] strArr = null;
                        String response = AddManager.netHandler.getDataFrmUrl(AppConstants.primaryAddsInfoFetchingUrl);
                        if (response == null || response.equals("")) {
                            response = AddManager.netHandler.getDataFrmUrl(AppConstants.secondaryAddsInfoFetchingUrl);
                        }
                        if (response == null || response.equals("")) {
                            response = AddManager.netHandler.getDataFrmUrl(AppConstants.thirdAddsInfoFetchingUrl);
                        }
                        if (response != null && !response.equals("")) {
                            String[] tokens = response.split("#");
                            if (!tokens[0].equals("NA")) {
                                AppConstants.sessionID = tokens[0];
                            }
                            if (!tokens[1].equals("0")) {
                                AppConstants.sessionTime = tokens[1];
                            }
                            if (!tokens[2].equals("0")) {
                                AddManager.addsDuration = Integer.parseInt(tokens[2]);
                            }
                            if (!tokens[3].equals("0")) {
                                AppConstants.AdsHostIP = tokens[3];
                            }
                            if (!tokens[4].equals("")) {
                                AddManager.backGroundAdds = tokens[4];
                            }
                            AppConstants.add_request_url = "http://" + AppConstants.AdsHostIP + "/Android/V1/AdsReq.aspx?Duc=" + AddManager.config.getDUC() + "&PID=" + AddManager.config.getPID() + "&W=480&H=50&TotAds=2&AdsId=" + AddManager.config.getAdsID() + "&SessionId=" + AppConstants.sessionID + "&IMEI=" + AddManager.config.getIMEI();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public void callMethodsForAdds() {
        int reminder = secCounter % addsDuration;
        if (addPosions.equals("ThirdParty(Upper & Lower) Adds only")) {
            fetchAddsDetails();
            if (reminder == 0) {
                getAdds();
            }
        } else if (addPosions.equals("ThirdParty(Upper) Adds only")) {
            fetchAddsDetails();
            if (reminder == 0) {
                getAdds();
            }
        } else if (addPosions.equals("ThirdParty(Lower) Adds only")) {
            fetchAddsDetails();
            if (reminder == 0) {
                getAdds();
            }
        } else if (addPosions.equals("Cross Promotional Adds only")) {
            if (showCPAdds() && reminder == 0 && fetchCPAds) {
                this.promAds.getCPAds();
            }
        } else if (addPosions.equals("ThirdParty(Upper) & Cross Promotional Adds only")) {
            if (showCPAdds() && reminder == 0 && fetchCPAds) {
                this.promAds.getCPAds();
            }
            fetchAddsDetails();
            if (reminder == 0) {
                getAdds();
            }
        } else if (addPosions.equals("ThirdParty(Lower) & Cross Promotional Adds only")) {
            if (showCPAdds() && reminder == 0 && fetchCPAds) {
                this.promAds.getCPAds();
            }
            fetchAddsDetails();
            if (reminder == 0) {
                getAdds();
            }
        } else if (addPosions.equals("Both ThirdParty and Cross Promotional Adds")) {
            if (showCPAdds() && reminder == 0 && fetchCPAds) {
                this.promAds.getCPAds();
            }
            fetchAddsDetails();
            if (reminder == 0) {
                getAdds();
            }
        }
    }

    public boolean hasNetworkConnection() {
        if (((ConnectivityManager) currentActivity.getSystemService("connectivity")).getActiveNetworkInfo() == null) {
            return false;
        }
        return true;
    }

    public boolean isBlankImage(Bitmap img) {
        if (img.getHeight() <= 10 || img.getWidth() <= 10) {
            return true;
        }
        return false;
    }

    public void getUpdatedMigitalBanners() {
        if (!migitalBannerFetched) {
            migitalBannerFetched = true;
            new Thread() {
                public void run() {
                    if (AddManager.engineIO.checkAds5thDay()) {
                        System.out.println("fetching migital banners");
                        String response = AddManager.netHandler.getDataFrmUrl(AppConstants.primaryBannerFetchingLink);
                        if (response == null || response.equals("")) {
                            response = AddManager.netHandler.getDataFrmUrl(AppConstants.secondaryBannerFetchingLink);
                        }
                        if (response == null || response.equals("")) {
                            response = AddManager.netHandler.getDataFrmUrl(AppConstants.thirdBannerFetchingLink);
                        }
                        if (response != null && !response.equals("")) {
                            AddManager.this.deletePreviousBanners();
                            String[] bannerLinks = response.split("#");
                            AddManager.config.setBANNER_VERSION(bannerLinks[0]);
                            AddManager.config.setBANNER_COUNT(bannerLinks.length - 1);
                            for (int i = 1; i < bannerLinks.length; i++) {
                                AddManager.this.saveImage(AddManager.netHandler.getImageArray(bannerLinks[i]), "banner" + i + ".png");
                            }
                        }
                    }
                }
            }.start();
        }
    }

    public void deletePreviousBanners() {
        int bannerCount = config.getBANNER_COUNT();
        if (bannerCount != 0) {
            for (int i = bannerCount; i > 0; i--) {
                File f = currentActivity.getFileStreamPath("banner" + i + ".png");
                if (f != null && f.exists()) {
                    f.delete();
                }
            }
        }
    }

    public static Bitmap getMigtalBanner() {
        try {
            System.out.println("migitalBanner = " + migitalBanner);
            migitalBanner++;
            if (migitalBanner > 4) {
                migitalBanner = 1;
            }
            Bitmap bmp = BitmapFactory.decodeFile(currentActivity.getFileStreamPath("banner" + migitalBanner + ".png").getAbsolutePath());
            if (bmp == null && migitalBanner % 2 == 1) {
                return AppConstants.default_top;
            }
            return (bmp == null && migitalBanner % 2 == 0) ? AppConstants.default_Bottom : bmp;
        } catch (Exception e) {
            if (migitalBanner % 2 == 1) {
                return AppConstants.default_top;
            }
            return AppConstants.default_Bottom;
        }
    }

    public void saveImage(byte[] bArray, String fileName) {
        System.out.println("saving image = " + fileName);
        try {
            FileOutputStream fOut = this.context.openFileOutput(fileName, 0);
            fOut.write(bArray);
            fOut.flush();
            fOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean showCPButton() {
        if (cpStatus.equals("Starting")) {
            cpStatus = "Visible";
        }
        if (config.getCPButtonStatus().equals("1")) {
            return true;
        }
        return false;
    }
}
