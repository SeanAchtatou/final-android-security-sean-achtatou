package android.engine;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.mine.test_lite.R;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PurchasingActivity extends Activity {
    public static String CardHolderName = "";
    public static String CardNumber = "";
    public static String CvvNumber = "";
    public static String ExpiryDate = "";
    public static String PhoneNumber = "1234567890";
    public static String UserEmail = "";
    public static String bangoUserPassword = "";
    public static String bangoUserid = "";
    public static String iExpiryDateMonth = "";
    public static String iExpiryDateYear = "";
    public static boolean isSubmitCreditCard = false;
    ImageView app_icon;
    ImageView bangoTitle;
    Button cancel;
    Config config;
    EditText cvv;
    EditText email;
    EditText expiryMonth;
    EditText expiryYear;
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            PurchasingActivity.this.myProgressDialog.dismiss();
            String bangoRes = msg.getData().getString("bango");
            if (bangoRes == null) {
                bangoRes = "Purchasing Failed";
            }
            PurchasingActivity.this.handleBillingResponce(bangoRes);
        }
    };
    /* access modifiers changed from: private */
    public boolean isRagister = false;
    ImageView lock;
    MainActivity ma;
    /* access modifiers changed from: private */
    public ProgressDialog myProgressDialog;
    EditText name;
    NetHandler netHandler;
    EditText number;
    EditText phone;
    TextView price_text_view;
    String response;
    Button submit;
    private Thread t;
    private String tag;
    ImageView visa;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.engine_bango2);
        this.ma = new MainActivity();
        this.netHandler = new NetHandler(this);
        this.config = new Config(this);
        setData();
        this.submit = (Button) findViewById(R.id.Button_submit);
        this.submit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                PurchasingActivity.this.submitDetails();
            }
        });
        this.cancel = (Button) findViewById(R.id.Button_cancel);
        this.cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                PurchasingActivity.this.finish();
            }
        });
    }

    public void setData() {
        String app_price = getIntent().getExtras().getString("android.engine.app_price");
        this.price_text_view = (TextView) findViewById(R.id.TextView_price);
        this.price_text_view.setText(" " + app_price + " (inclusive of all taxes)");
        this.app_icon = (ImageView) findViewById(R.id.ImageView_app_icon);
        this.bangoTitle = (ImageView) findViewById(R.id.ImageView01_bango_title);
        if (this.config.getCountryCode().equals("310") || this.config.getCountryCode().equals("311") || this.config.getCountryCode().equals("312") || this.config.getCountryCode().equals("313") || this.config.getCountryCode().equals("314") || this.config.getCountryCode().equals("315") || this.config.getCountryCode().equals("316")) {
            this.bangoTitle.setImageResource(R.drawable.bango);
        }
        this.lock = (ImageView) findViewById(R.id.ImageView_lock);
        this.lock.setImageResource(R.drawable.lock);
        this.visa = (ImageView) findViewById(R.id.ImageView_visa_cards);
    }

    public void submitDetails() {
        this.name = (EditText) findViewById(R.id.EditText_card_name);
        this.number = (EditText) findViewById(R.id.EditText_card_no);
        this.expiryMonth = (EditText) findViewById(R.id.EditText_exp_month);
        this.expiryYear = (EditText) findViewById(R.id.EditText_exp_year);
        this.cvv = (EditText) findViewById(R.id.EditText_cvv_code);
        this.email = (EditText) findViewById(R.id.EditText_email);
        this.phone = (EditText) findViewById(R.id.EditText_phone);
        CardHolderName = this.name.getText().toString();
        CardNumber = this.number.getText().toString();
        CvvNumber = this.cvv.getText().toString();
        UserEmail = this.email.getText().toString();
        PhoneNumber = this.phone.getText().toString();
        if (validate().equals("ValidParameter")) {
            showProgressDialog();
        }
    }

    public String validate() {
        if (CardHolderName.length() < 1) {
            showPrompt("Please enter name");
        } else if (CardNumber.length() < 16) {
            showPrompt("Card number is not valid");
        } else if (!isDateValid()) {
            showPrompt("Expiry Date is not valid");
        } else if (CvvNumber.length() != 3) {
            showPrompt("CVV number is not valid");
        } else if (UserEmail.length() < 3) {
            showPrompt("Email id is not valid");
        } else if (UserEmail.length() > 3 && !validateEmailID(UserEmail)) {
            showPrompt("Email id is not valid");
        } else if (PhoneNumber.length() >= 10) {
            return "ValidParameter";
        } else {
            showPrompt("Phone number is not valid");
        }
        return "";
    }

    public boolean isDateValid() {
        String[] tokens = new SimpleDateFormat("MM-yy").format(new Date(System.currentTimeMillis())).split("-");
        try {
            int currentMonth = Integer.parseInt(tokens[0]);
            int currentYear = Integer.parseInt(tokens[1]);
            String mm = this.expiryMonth.getText().toString();
            if (mm.length() == 1) {
                mm = "0" + mm;
            }
            String yy = this.expiryYear.getText().toString();
            if (yy.length() == 1) {
                yy = "0" + yy;
            }
            int month = Integer.parseInt(mm);
            int year = Integer.parseInt(yy);
            if ((month < 1 || month > 12) && month < currentMonth) {
                return false;
            }
            if (year < currentYear) {
                return false;
            }
            ExpiryDate = String.valueOf(mm) + yy;
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean validateEmailID(String email2) {
        int atCount = 0;
        int atPos = -1;
        int lastDotPos = -1;
        boolean dotExits = false;
        if (email2.length() < 6) {
            return false;
        }
        for (int i = 0; i < email2.length(); i++) {
            if (email2.charAt(i) == ' ') {
                return false;
            }
            if (email2.charAt(i) == '@') {
                atCount++;
                atPos = i;
            }
            if (email2.charAt(i) == '.') {
                dotExits = true;
                lastDotPos = i;
            }
            if (lastDotPos == atPos + 1) {
                return false;
            }
        }
        System.out.println("@ positon" + atPos);
        System.out.println("Dot positon" + lastDotPos);
        if (atCount != 1) {
            return false;
        }
        if (lastDotPos < atPos) {
            return false;
        }
        if (lastDotPos > email2.length() - 2) {
            return false;
        }
        return dotExits;
    }

    public void showProgressDialog() {
        this.myProgressDialog = ProgressDialog.show(this, "Processing!", "Please wait..", true);
        new Thread() {
            public void run() {
                try {
                    String response = PurchasingActivity.this.netHandler.postBangoData(PurchasingActivity.this.config.removeWhiteSpace(PurchasingActivity.CardHolderName), PurchasingActivity.CardNumber, PurchasingActivity.ExpiryDate, PurchasingActivity.CvvNumber, PurchasingActivity.UserEmail, PurchasingActivity.PhoneNumber);
                    Message msg = new Message();
                    Bundle b = new Bundle();
                    b.putString("bango", response);
                    msg.setData(b);
                    PurchasingActivity.this.handler.sendMessage(msg);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
        Log.v(this.tag, "progress thread started");
    }

    public void showPrompt(String msg) {
        AlertDialog prompt = new AlertDialog.Builder(this).create();
        prompt.setTitle("Note:");
        prompt.setMessage(msg);
        prompt.setButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dia, int arg1) {
                dia.dismiss();
                if (PurchasingActivity.this.isRagister) {
                    PurchasingActivity.this.isRagister = false;
                    PurchasingActivity.this.finish();
                }
            }
        });
        prompt.show();
    }

    /* access modifiers changed from: private */
    public void handleBillingResponce(String billingResponce) {
        String key = null;
        if (billingResponce.equals("-1")) {
            showPrompt("Invalid Parameters");
        } else if (billingResponce.startsWith("Error:")) {
            showPrompt(billingResponce.substring(6));
        } else if (billingResponce.startsWith("Key:")) {
            try {
                key = new KeyGenerator().genereateKeyL(this.config.getIMEI(), Integer.parseInt(this.config.getPID()));
                System.out.println("key = " + key);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Exception occoured key = " + key);
            }
            System.out.println("App key = " + key);
            if (billingResponce.substring(4).equals(key)) {
                this.isRagister = true;
                EngineIO engineIO = new EngineIO(this);
                this.config.setFTYPE("3");
                engineIO.setIsAddEnable("0");
                engineIO.setBuyAppEnableStatus("0");
                engineIO.setAuthorizationStatus("1");
                AppConstants.appPurchased = true;
                showPrompt("You are registered successfully.Thanks for buying migital application");
                return;
            }
            showPrompt("Invalid " + billingResponce);
        } else {
            showPrompt("UnKnown Responce " + billingResponce);
        }
    }

    public void readFile2(String filename) {
        try {
            char[] inputBuffer = new char[30];
            new InputStreamReader(openFileInput(filename)).read(inputBuffer);
            Log.v("Test", "Data from file2  =  " + new String(inputBuffer).trim().trim());
        } catch (FileNotFoundException e) {
            writeFile2(filename, "not_registered");
            readFile2(filename);
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    public void writeFile2(String filename, String value) {
        try {
            Log.v(value, "write file called with = " + value);
            OutputStreamWriter osw = new OutputStreamWriter(openFileOutput(filename, 0));
            osw.write(value);
            osw.flush();
            osw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
