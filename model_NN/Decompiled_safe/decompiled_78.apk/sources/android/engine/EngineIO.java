package android.engine;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.mine.test_lite.MainMenu;
import com.mine.test_lite.R;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

public class EngineIO extends SQLiteOpenHelper {
    private static String DB_NAME = "EngineIO.DB";
    public String AddsPosition_key = "Addpositons";
    public String AppIcon_key = "AppIcon";
    public String AppName_key = "AppName";
    public String AppRestrictions_key = "AppRestrictions";
    public String BillingType_key = "BillingType";
    public String Buy_key = "Buy";
    private final String CPAppsTable = "CPApps";
    public String CP_AppLogo_key = "AppLogo";
    public String CP_AppName_key = "AppName";
    public String CP_Buy_key = "Buy";
    public String CP_More_App_key = "More";
    private String DB_PATH;
    public String ForceUpdateLinkToDownload_key = "ForceUpdateLinkToDownload";
    public String FreeAppButtonImage_key = "FreeAppButtonImage";
    public String FreeAppButtonText_key = "FreeAppButtonText";
    public String LongDesc_key = "LongDesc";
    private final String MasterResponseTable = "MasterResponseTable";
    public String Price_key = "Price";
    public String Rating_key = "Rating";
    public String ScreenShot1_key = "ScreenShot1";
    public String ScreenShot2_key = "ScreenShot2";
    public String SmallDesc_key = "SmallDesc";
    public String UpgradeInfoLink_key = "UpgradeInfoLink";
    private final String UpgradedAppTable = "UpgradedAppTable";
    String adsMultipleOf5;
    int appUsageCount;
    Config config;
    Context context;
    int count = 0;
    private SQLiteDatabase db = null;
    String engineDefaults = "0#0#0#0#1#0#1#0#0#primaryLink#secondaryLink#thirdLink";
    String engineFile = "EngineIO.txt";
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.getData().getString("Method").equals("showMoreAppWindow")) {
                EngineIO.this.showPrompt(EngineIO.this.tempContext, "Service not available");
            }
            if (msg.getData().getString("Method").equals("showUpdateDownloadDialog")) {
                EngineIO.this.showPrompt(EngineIO.this.tempContext, "DownLoad Complete time consumed = " + ((System.currentTimeMillis() - msg.getData().getLong("StartTime")) / 1000) + " sec");
            }
        }
    };
    Handler handler2 = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.getData().getBoolean("DownLoadResult")) {
                EngineIO.this.showInstallDialog(EngineIO.this.tempContext, "Application download sucessfully. Install now");
            } else {
                EngineIO.this.showPrompt(EngineIO.this.tempContext, "Error in downloading. Please try later");
            }
        }
    };
    public String id_key = "_id";
    private int installationCode = 1;
    long installtionTime;
    String isAddEnable;
    public String isAppAuthorized_key = "isAppAuthorized";
    public String isAppRestricted_key = "isAppRestricted";
    public String isBuyAppEnable_key = "isBuyAppEnable";
    public String isForceUpdateEnable_key = "isForceUpdateEnable";
    public String isFreeAppEnable_key = "isFreeAppEnable";
    public String isUpgradedVirsionAvl_key = "isUpgradedVirsionAvl";
    String multipleOf15;
    int multipleOf5;
    ProgressDialog myProgressDialog;
    NetHandler netHandler;
    String newFields;
    String newFields2;
    String price;
    Handler priceHandler = new Handler() {
        public void handleMessage(Message msg) {
            EngineIO.this.myProgressDialog.dismiss();
            if (EngineIO.this.price == null || EngineIO.this.price.equals("")) {
                EngineIO.this.showPrompt(EngineIO.this.context, "Unable to connect");
                return;
            }
            Intent new_bango_intent = new Intent(EngineIO.this.context, PurchasingActivity.class);
            new_bango_intent.putExtra("android.engine.app_price", EngineIO.this.price);
            EngineIO.this.context.startActivity(new_bango_intent);
        }
    };
    String primaryLink;
    String secondaryLink;
    String tag = "Engine";
    Context tempContext;
    String thirdLink;
    int updateAppCount;
    Intent updateDialogIntent;
    int upgradedAppCount;
    int urlVersion;
    private ContextWrapper wrapper = null;

    public EngineIO(Context ctx) {
        super(ctx, DB_NAME, (SQLiteDatabase.CursorFactory) null, 1);
        this.context = ctx;
        this.DB_PATH = String.valueOf(this.context.getFilesDir().getAbsolutePath()) + "/";
        try {
            createDataBase();
            openDataBase();
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.wrapper = new ContextWrapper(this.context);
        this.config = new Config(this.context);
        this.netHandler = new NetHandler(this.context);
        this.updateDialogIntent = new Intent();
        this.updateDialogIntent.setFlags(268435456);
        this.updateDialogIntent.setClass(this.context, UpdateDialog.class);
        if (!this.wrapper.getFileStreamPath(this.engineFile).exists()) {
            createFile(this.engineFile, this.engineDefaults);
        }
    }

    public void createFile(String fileName, String data) {
        try {
            System.out.println("EngineIO.createFile()");
            FileOutputStream outputStream = this.wrapper.openFileOutput(fileName, 0);
            outputStream.write(data.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(String.valueOf(fileName) + " has been created");
    }

    public String readFile(String fileName) {
        try {
            FileInputStream fIn = this.wrapper.openFileInput(fileName);
            InputStreamReader isr = new InputStreamReader(fIn);
            char[] inputBuffer = new char[fIn.available()];
            isr.read(inputBuffer);
            return new String(inputBuffer).trim().trim();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public void writeFile(String fileName, String data) {
        try {
            OutputStreamWriter osw = new OutputStreamWriter(this.wrapper.openFileOutput(fileName, 0));
            osw.write(data);
            osw.flush();
            osw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void reCreateFile() {
        writeFile(this.engineFile, this.engineDefaults);
    }

    public void splitEngineData() {
        try {
            String[] tokens = readFile(this.engineFile).split("#");
            this.installtionTime = Long.parseLong(tokens[0]);
            this.multipleOf15 = tokens[1];
            this.upgradedAppCount = Integer.parseInt(tokens[2]);
            this.updateAppCount = Integer.parseInt(tokens[3]);
            this.isAddEnable = tokens[4];
            this.appUsageCount = Integer.parseInt(tokens[5]);
            this.urlVersion = Integer.parseInt(tokens[6]);
            this.multipleOf5 = Integer.parseInt(tokens[7]);
            this.adsMultipleOf5 = tokens[8];
            this.primaryLink = tokens[9];
            this.secondaryLink = tokens[10];
            this.thirdLink = tokens[11];
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setInstallationTime(long time) {
        splitEngineData();
        this.newFields2 = String.valueOf(time) + "#" + this.multipleOf15 + "#" + this.upgradedAppCount + "#" + this.updateAppCount + "#" + this.isAddEnable + "#" + this.appUsageCount + "#" + this.urlVersion + "#" + this.multipleOf5 + "#" + this.adsMultipleOf5 + "#" + this.primaryLink + "#" + this.secondaryLink + "#" + this.thirdLink;
        writeFile(this.engineFile, this.newFields2);
    }

    public long getInstallationTime() {
        splitEngineData();
        return this.installtionTime;
    }

    public String getInstallationDate() {
        return new SimpleDateFormat("ddMMyy").format(new Date(getInstallationTime()));
    }

    public void setMultipleOf15(int data) {
        splitEngineData();
        this.newFields2 = String.valueOf(this.installtionTime) + "#" + data + "#" + this.upgradedAppCount + "#" + this.updateAppCount + "#" + this.isAddEnable + "#" + this.appUsageCount + "#" + this.urlVersion + "#" + this.multipleOf5 + "#" + this.adsMultipleOf5 + "#" + this.primaryLink + "#" + this.secondaryLink + "#" + this.thirdLink;
        writeFile(this.engineFile, this.newFields2);
    }

    public int getMultipleOf15() {
        splitEngineData();
        return Integer.parseInt(this.multipleOf15);
    }

    public int getUpgradeAppShownUsage() {
        splitEngineData();
        return this.upgradedAppCount;
    }

    public void setUpgradeAppShownUsage(int data) {
        splitEngineData();
        this.newFields2 = String.valueOf(this.installtionTime) + "#" + this.multipleOf15 + "#" + data + "#" + this.updateAppCount + "#" + this.isAddEnable + "#" + this.appUsageCount + "#" + this.urlVersion + "#" + this.multipleOf5 + "#" + this.adsMultipleOf5 + "#" + this.primaryLink + "#" + this.secondaryLink + "#" + this.thirdLink;
        writeFile(this.engineFile, this.newFields2);
    }

    public int getUpdateAppShownUsage() {
        splitEngineData();
        return this.updateAppCount;
    }

    public void setUpdateAppShownUsage(int data) {
        System.out.println("setUpdateAppShownUsage = " + data);
        splitEngineData();
        this.newFields2 = String.valueOf(this.installtionTime) + "#" + this.multipleOf15 + "#" + this.upgradedAppCount + "#" + data + "#" + this.isAddEnable + "#" + this.appUsageCount + "#" + this.urlVersion + "#" + this.multipleOf5 + "#" + this.adsMultipleOf5 + "#" + this.primaryLink + "#" + this.secondaryLink + "#" + this.thirdLink;
        writeFile(this.engineFile, this.newFields2);
    }

    public boolean isAddEnable() {
        try {
            splitEngineData();
            if (this.isAddEnable.equals("1")) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public void setIsAddEnable(String data) {
        splitEngineData();
        this.newFields2 = String.valueOf(this.installtionTime) + "#" + this.multipleOf15 + "#" + this.upgradedAppCount + "#" + this.updateAppCount + "#" + data + "#" + this.appUsageCount + "#" + this.urlVersion + "#" + this.multipleOf5 + "#" + this.adsMultipleOf5 + "#" + this.primaryLink + "#" + this.secondaryLink + "#" + this.thirdLink;
        writeFile(this.engineFile, this.newFields2);
    }

    public int getAppUsageCount() {
        splitEngineData();
        return this.appUsageCount;
    }

    public void setAppUsageCount(int data) {
        splitEngineData();
        this.newFields2 = String.valueOf(this.installtionTime) + "#" + this.multipleOf15 + "#" + this.upgradedAppCount + "#" + this.updateAppCount + "#" + this.isAddEnable + "#" + data + "#" + this.urlVersion + "#" + this.multipleOf5 + "#" + this.adsMultipleOf5 + "#" + this.primaryLink + "#" + this.secondaryLink + "#" + this.thirdLink;
        writeFile(this.engineFile, this.newFields2);
    }

    public int getUrlVersion() {
        splitEngineData();
        return this.urlVersion;
    }

    public void setUrlVersion(String data) {
        splitEngineData();
        this.newFields2 = String.valueOf(this.installtionTime) + "#" + this.multipleOf15 + "#" + this.upgradedAppCount + "#" + this.updateAppCount + "#" + this.isAddEnable + "#" + this.appUsageCount + "#" + data + "#" + this.multipleOf5 + "#" + this.adsMultipleOf5 + "#" + this.primaryLink + "#" + this.secondaryLink + "#" + this.thirdLink;
        writeFile(this.engineFile, this.newFields2);
    }

    public int getMultipleOf5() {
        splitEngineData();
        return this.multipleOf5;
    }

    public void setMultipleOf5(int data) {
        splitEngineData();
        this.newFields2 = String.valueOf(this.installtionTime) + "#" + this.multipleOf15 + "#" + this.upgradedAppCount + "#" + this.updateAppCount + "#" + this.isAddEnable + "#" + this.appUsageCount + "#" + this.urlVersion + "#" + data + "#" + this.adsMultipleOf5 + "#" + this.primaryLink + "#" + this.secondaryLink + "#" + this.thirdLink;
        writeFile(this.engineFile, this.newFields2);
    }

    public int getAdsMultipleOf5() {
        splitEngineData();
        if (this.adsMultipleOf5 == null || this.adsMultipleOf5.equals("")) {
            return 0;
        }
        return Integer.parseInt(this.adsMultipleOf5);
    }

    public void setAdsMultipleOf5(int data) {
        splitEngineData();
        this.newFields2 = String.valueOf(this.installtionTime) + "#" + this.multipleOf15 + "#" + this.upgradedAppCount + "#" + this.updateAppCount + "#" + this.isAddEnable + "#" + this.appUsageCount + "#" + this.urlVersion + "#" + this.multipleOf5 + "#" + data + "#" + this.primaryLink + "#" + this.secondaryLink + "#" + this.thirdLink;
        writeFile(this.engineFile, this.newFields2);
    }

    public String getPrimaryLink() {
        splitEngineData();
        if (this.primaryLink == null || this.primaryLink.equals("primaryLink")) {
            return "scms.migital.com";
        }
        return this.primaryLink;
    }

    public void setPrimaryLink(String data) {
        splitEngineData();
        this.newFields2 = String.valueOf(this.installtionTime) + "#" + this.multipleOf15 + "#" + this.upgradedAppCount + "#" + this.updateAppCount + "#" + this.isAddEnable + "#" + this.appUsageCount + "#" + this.urlVersion + "#" + this.multipleOf5 + "#" + this.adsMultipleOf5 + "#" + data + "#" + this.secondaryLink + "#" + this.thirdLink;
        writeFile(this.engineFile, this.newFields2);
    }

    public String getSecondaryLink() {
        splitEngineData();
        if (this.secondaryLink == null || this.secondaryLink.equals("secondaryLink")) {
            return "scms.migital.net";
        }
        return this.secondaryLink;
    }

    public void setSecondaryLink(String data) {
        splitEngineData();
        this.newFields2 = String.valueOf(this.installtionTime) + "#" + this.multipleOf15 + "#" + this.upgradedAppCount + "#" + this.updateAppCount + "#" + this.isAddEnable + "#" + this.appUsageCount + "#" + this.urlVersion + "#" + this.multipleOf5 + "#" + this.adsMultipleOf5 + "#" + this.primaryLink + "#" + data + "#" + this.thirdLink;
        writeFile(this.engineFile, this.newFields2);
    }

    public String getThirdLink() {
        splitEngineData();
        if (this.thirdLink == null || this.thirdLink.equals("thirdLink")) {
            return "scms.migital.in";
        }
        return this.thirdLink;
    }

    public void setThirdLink(String data) {
        splitEngineData();
        this.newFields2 = String.valueOf(this.installtionTime) + "#" + this.multipleOf15 + "#" + this.upgradedAppCount + "#" + this.updateAppCount + "#" + this.isAddEnable + "#" + this.appUsageCount + "#" + this.urlVersion + "#" + this.multipleOf5 + "#" + this.adsMultipleOf5 + "#" + this.primaryLink + "#" + this.secondaryLink + "#" + data;
        writeFile(this.engineFile, this.newFields2);
    }

    public int getDaySpent(long startTime) {
        return (int) ((System.currentTimeMillis() - startTime) / 86400000);
    }

    public boolean check15thDay() {
        int multipleOf152 = getMultipleOf15();
        if (multipleOf152 == 0 && getDaySpent(getInstallationTime()) <= 3 && getDaySpent(getInstallationTime()) > 1) {
            setMultipleOf15(multipleOf152 + 1);
            return true;
        } else if (getDaySpent(getInstallationTime()) / 15 < multipleOf152) {
            return false;
        } else {
            setMultipleOf15(multipleOf152 + 1);
            return true;
        }
    }

    public boolean check5thDay() {
        int multipleOf52 = getMultipleOf5();
        if (multipleOf52 == 0 && getDaySpent(getInstallationTime()) <= 3) {
            setMultipleOf5(multipleOf52 + 1);
            return true;
        } else if (getDaySpent(getInstallationTime()) / 5 < multipleOf52) {
            return false;
        } else {
            setMultipleOf5(multipleOf52 + 1);
            return true;
        }
    }

    public boolean checkAds5thDay() {
        int multipleOf52 = getAdsMultipleOf5();
        if (multipleOf52 == 0 && getDaySpent(getInstallationTime()) <= 3) {
            setAdsMultipleOf5(multipleOf52 + 1);
            return true;
        } else if (getDaySpent(getInstallationTime()) / 5 < multipleOf52) {
            return false;
        } else {
            setAdsMultipleOf5(multipleOf52 + 1);
            return true;
        }
    }

    public void createDataBase() throws IOException {
        if (!checkDataBase()) {
            getReadableDatabase();
            try {
                copyDataBase();
            } catch (IOException e) {
                throw new Error("Error copying database");
            }
        }
    }

    private boolean checkDataBase() {
        SQLiteDatabase checkDB = null;
        try {
            checkDB = SQLiteDatabase.openDatabase(String.valueOf(this.DB_PATH) + DB_NAME, null, 0);
        } catch (SQLiteException e) {
        }
        if (checkDB != null) {
            checkDB.close();
        }
        return checkDB != null;
    }

    private void copyDataBase() throws IOException {
        InputStream myInput = this.context.getAssets().open(DB_NAME);
        OutputStream myOutput = new FileOutputStream(String.valueOf(this.DB_PATH) + DB_NAME);
        byte[] buffer = new byte[myInput.available()];
        while (true) {
            int length = myInput.read(buffer);
            if (length <= 0) {
                myOutput.flush();
                myOutput.close();
                myInput.close();
                return;
            }
            myOutput.write(buffer, 0, length);
        }
    }

    public void openDataBase() throws SQLException {
        this.db = SQLiteDatabase.openDatabase(String.valueOf(this.DB_PATH) + DB_NAME, null, 0);
    }

    public synchronized void close() {
        if (this.db != null) {
            this.db.close();
        }
        super.close();
    }

    public void onCreate(SQLiteDatabase db2) {
    }

    public void onUpgrade(SQLiteDatabase db2, int oldVersion, int newVersion) {
    }

    public boolean tableExists(String tableName) {
        Cursor cursor = null;
        try {
            Cursor cursor2 = this.db.query(tableName, null, null, null, null, null, null);
            if (cursor2 == null) {
                cursor2.close();
                return false;
            }
            cursor2.close();
            return true;
        } catch (Exception e) {
            if (cursor != null) {
                cursor.close();
            }
            return false;
        }
    }

    public void saveMasterResponse(String masterResponse) {
        if (masterResponse.indexOf("#") != -1) {
            String[] tokens = masterResponse.split("#");
            Cursor cursor = this.db.query("MasterResponseTable", null, null, null, null, null, null);
            if (cursor != null && cursor.getCount() == 0) {
                insertMasterResponse(tokens[0], tokens[1], tokens[2], tokens[3], tokens[4], tokens[5], tokens[6], tokens[7], tokens[8], tokens[9], tokens[10], tokens[11]);
            } else if (tokens[0] != null && !tokens[0].equals("null") && !tokens[0].equals(getAuthStatus())) {
                setAuthorizationStatus(tokens[0]);
            }
            if (tokens[1] != null && !tokens[1].equals("null") && ((tokens[1].equals("1") && !getBuyAppEnableStatus()) || (tokens[1].equals("0") && getBuyAppEnableStatus()))) {
                setBuyAppEnableStatus(tokens[1]);
            }
            if (tokens[2] != null && !tokens[2].equals("null") && !tokens[2].equals(getBillingMode())) {
                setBillingMode(tokens[2]);
            }
            if (tokens[3] != null && !tokens[3].equals("null") && ((tokens[3].equals("1") && !getFreeAppEnableStatus()) || (tokens[3].equals("0") && getFreeAppEnableStatus()))) {
                setFreeAppEnableStatus(tokens[3]);
            }
            if (tokens[4] != null && !tokens[4].equals("null") && !tokens[4].equals(getFreeAppButtonText())) {
                setFreeAppButtonText(tokens[4]);
            }
            if (tokens[5] != null && !tokens[5].equals("null") && !tokens[5].equals(getFreeAppButtonImageLink())) {
                setFreeAppButtonImageLink(tokens[5]);
            }
            if (tokens[6] != null && !tokens[6].equals("null") && ((tokens[6].equals("1") && !getForceUpdateEnableStatus()) || (tokens[6].equals("0") && getForceUpdateEnableStatus()))) {
                setForceUpdateEnableStatus(tokens[6]);
            }
            if (tokens[7] != null && !tokens[7].equals("null") && ((tokens[7].equals("1") && !getUpgradedVirsionAvlStatus()) || (tokens[7].equals("0") && getUpgradedVirsionAvlStatus()))) {
                setUpgradedVirsionAvlStatus(tokens[7]);
            }
            if (tokens[8] != null && !tokens[8].equals("null") && !tokens[8].equals(getforceUpdateLinkToDownload())) {
                setforceUpdateLinkToDownload(tokens[8]);
            }
            if (tokens[9] != null && !tokens[9].equals("null") && !tokens[9].equals(getUpgradeInfoLink())) {
                setUpgradeInfoLink(tokens[9]);
            }
            if (tokens[10] != null && !tokens[10].equals("null") && !tokens[10].equals(getTempAddPosions())) {
                setAddPosions(tokens[10]);
            }
            if (tokens[11] != null && !tokens[11].equals("null") && !tokens[11].equals(getAppRestrictions())) {
                setAppRestrictions(tokens[11]);
            }
        }
    }

    public void insertMasterResponse(String str1, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(this.isAppAuthorized_key, str1);
        contentValues.put(this.isBuyAppEnable_key, str2);
        contentValues.put(this.BillingType_key, str3);
        contentValues.put(this.isFreeAppEnable_key, str4);
        contentValues.put(this.FreeAppButtonText_key, str5);
        contentValues.put(this.FreeAppButtonImage_key, str6);
        contentValues.put(this.isForceUpdateEnable_key, str7);
        contentValues.put(this.isUpgradedVirsionAvl_key, str8);
        contentValues.put(this.ForceUpdateLinkToDownload_key, str9);
        contentValues.put(this.UpgradeInfoLink_key, str10);
        contentValues.put(this.AddsPosition_key, str11);
        contentValues.put(this.AppRestrictions_key, str12);
        System.out.println("data inserted id = " + this.db.insert("MasterResponseTable", null, contentValues));
    }

    public void setAuthorizationStatus(String data) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(this.isAppAuthorized_key, data);
        this.db.update("MasterResponseTable", contentValues, String.valueOf(this.id_key) + "=1", null);
    }

    public boolean getAuthorizationStatus() {
        String data = "";
        Cursor c = this.db.query("MasterResponseTable", new String[]{this.isAppAuthorized_key}, null, null, null, null, null);
        if (c.moveToFirst()) {
            data = c.getString(0);
        }
        c.close();
        if (data.equals("0")) {
            return false;
        }
        return true;
    }

    public String getAuthStatus() {
        String data = "";
        Cursor c = this.db.query("MasterResponseTable", new String[]{this.isAppAuthorized_key}, null, null, null, null, null);
        if (c.moveToFirst()) {
            data = c.getString(0);
        }
        c.close();
        if (data.equals("")) {
            return "0";
        }
        return data;
    }

    public String authStatus() {
        String data = "";
        Cursor c = this.db.query("MasterResponseTable", new String[]{this.isAppAuthorized_key}, null, null, null, null, null);
        if (c.moveToFirst()) {
            data = c.getString(0);
        }
        c.close();
        if (data.equals("")) {
            return "0";
        }
        return "1";
    }

    public void setBuyAppEnableStatus(String data) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(this.isBuyAppEnable_key, data);
        this.db.update("MasterResponseTable", contentValues, String.valueOf(this.id_key) + "=1", null);
    }

    public boolean getBuyAppEnableStatus() {
        String data = "";
        Cursor c = this.db.query("MasterResponseTable", new String[]{this.isBuyAppEnable_key}, null, null, null, null, null);
        if (c.moveToFirst()) {
            data = c.getString(0);
        }
        c.close();
        if (!data.equals("1") || !isAddEnable()) {
            return false;
        }
        return true;
    }

    public String getBuyAppStatus() {
        String data = "";
        Cursor c = this.db.query("MasterResponseTable", new String[]{this.isBuyAppEnable_key}, null, null, null, null, null);
        if (c.moveToFirst()) {
            data = c.getString(0);
        }
        c.close();
        if (!data.equals("1") || !isAddEnable()) {
            return "0";
        }
        return "1";
    }

    public String BuyAppStatus() {
        String data = "";
        Cursor c = this.db.query("MasterResponseTable", new String[]{this.isBuyAppEnable_key}, null, null, null, null, null);
        if (c.moveToFirst()) {
            data = c.getString(0);
        }
        c.close();
        if (data.equals("")) {
            return "0";
        }
        return "1";
    }

    public String FreeAppStatus() {
        String data = "";
        Cursor c = this.db.query("MasterResponseTable", new String[]{this.isFreeAppEnable_key}, null, null, null, null, null);
        if (c.moveToFirst()) {
            data = c.getString(0);
        }
        c.close();
        if (data.equals("")) {
            return "0";
        }
        return "1";
    }

    public void setBillingMode(String data) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(this.BillingType_key, data);
        this.db.update("MasterResponseTable", contentValues, String.valueOf(this.id_key) + "=1", null);
    }

    public String getBillingMode() {
        String data = "Bango";
        Cursor c = this.db.query("MasterResponseTable", new String[]{this.BillingType_key}, null, null, null, null, null);
        if (c.moveToFirst()) {
            data = c.getString(0);
        }
        c.close();
        return data;
    }

    public void setFreeAppEnableStatus(String data) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(this.isFreeAppEnable_key, data);
        this.db.update("MasterResponseTable", contentValues, String.valueOf(this.id_key) + "=1", null);
    }

    public boolean getFreeAppEnableStatus() {
        String data = "";
        Cursor c = this.db.query("MasterResponseTable", new String[]{this.isFreeAppEnable_key}, null, null, null, null, null);
        if (c.moveToFirst()) {
            data = c.getString(0);
        }
        c.close();
        if (data.equals("0")) {
            return false;
        }
        return true;
    }

    public String getFreeAppStatus() {
        String data = "";
        Cursor c = this.db.query("MasterResponseTable", new String[]{this.isFreeAppEnable_key}, null, null, null, null, null);
        if (c.moveToFirst()) {
            data = c.getString(0);
        }
        c.close();
        if (data.equals("")) {
            return "0";
        }
        return data;
    }

    public void setFreeAppButtonText(String data) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(this.FreeAppButtonText_key, data);
        this.db.update("MasterResponseTable", contentValues, String.valueOf(this.id_key) + "=1", null);
    }

    public String getFreeAppButtonText() {
        String data = "";
        Cursor c = this.db.query("MasterResponseTable", new String[]{this.FreeAppButtonText_key}, null, null, null, null, null);
        if (c.moveToFirst()) {
            data = c.getString(0);
        }
        c.close();
        if (data.equals("")) {
            return "More Apps";
        }
        return data;
    }

    public void setFreeAppButtonImageLink(String data) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(this.FreeAppButtonImage_key, data);
        this.db.update("MasterResponseTable", contentValues, String.valueOf(this.id_key) + "=1", null);
    }

    public String getFreeAppButtonImageLink() {
        String data = "";
        Cursor c = this.db.query("MasterResponseTable", new String[]{this.FreeAppButtonImage_key}, null, null, null, null, null);
        if (c != null && c.moveToFirst()) {
            data = c.getString(0);
        }
        if (c != null) {
            c.close();
        }
        if (data.equals("")) {
            return "";
        }
        return data;
    }

    public String getFreeAppButtonImageName() {
        String data = getFreeAppButtonImageLink();
        if (data.equals("")) {
            return "";
        }
        String[] tokens = data.split("/");
        return tokens[tokens.length - 1];
    }

    public Drawable getFreeAppButtonImage() {
        try {
            Drawable d = Drawable.createFromPath(this.context.getFileStreamPath(getFreeAppButtonImageName()).getAbsolutePath());
            System.out.println("free app button image = " + d);
            return d;
        } catch (Exception e) {
            return null;
        }
    }

    public void setForceUpdateEnableStatus(String data) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(this.isForceUpdateEnable_key, data);
        this.db.update("MasterResponseTable", contentValues, String.valueOf(this.id_key) + "=1", null);
    }

    public boolean getForceUpdateEnableStatus() {
        String data = "";
        Cursor c = this.db.query("MasterResponseTable", new String[]{this.isForceUpdateEnable_key}, null, null, null, null, null);
        if (c.moveToFirst()) {
            data = c.getString(0);
        }
        c.close();
        if (data.equals("") || data.charAt(0) == 0) {
            return false;
        }
        if (data.charAt(0) == '1' || data.charAt(0) == '2') {
            return true;
        }
        return false;
    }

    public String getUpdateType() {
        String data = "";
        Cursor c = this.db.query("MasterResponseTable", new String[]{this.isForceUpdateEnable_key}, null, null, null, null, null);
        if (c.moveToFirst()) {
            data = c.getString(0);
        }
        c.close();
        if (!data.equals("") && data.charAt(0) != 0 && data.charAt(0) == '1') {
            return "ForceUpdate";
        }
        if (data.equals("") || data.charAt(0) == 0 || data.charAt(0) != '2') {
            return "NoUpdate";
        }
        return "ChoiceUpdate";
    }

    public int getForceUpdateRepetitionUses() {
        String data = "";
        Cursor c = this.db.query("MasterResponseTable", new String[]{this.isForceUpdateEnable_key}, null, null, null, null, null);
        if (c.moveToFirst()) {
            data = c.getString(0);
        }
        c.close();
        if (data.equals("") || data.equals("0")) {
            return 0;
        }
        return Integer.parseInt(data.substring(1, data.length()));
    }

    public void setUpgradedVirsionAvlStatus(String data) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(this.isUpgradedVirsionAvl_key, data);
        this.db.update("MasterResponseTable", contentValues, String.valueOf(this.id_key) + "=1", null);
    }

    public boolean getUpgradedVirsionAvlStatus() {
        String data = "";
        Cursor c = this.db.query("MasterResponseTable", new String[]{this.isUpgradedVirsionAvl_key}, null, null, null, null, null);
        if (c.moveToFirst()) {
            data = c.getString(0);
        }
        c.close();
        System.out.println("getUpgradedVirsionAvlStatus = " + data);
        if (data.equals("") || data.equals("0")) {
            return false;
        }
        if (data.indexOf("*") == -1 || !splitString(data, "*")[0].equals("1")) {
            return false;
        }
        return true;
    }

    public int getUpgradedAppsDays() {
        String data = "";
        Cursor c = this.db.query("MasterResponseTable", new String[]{this.isUpgradedVirsionAvl_key}, null, null, null, null, null);
        if (c.moveToFirst()) {
            data = c.getString(0);
        }
        c.close();
        if (data.equals("") || data.equals("0")) {
            return 0;
        }
        if (data.indexOf("*") != -1) {
            return Integer.parseInt(splitString(data, "*")[1]);
        }
        return 0;
    }

    public String getUpgradedAppsLocation() {
        String data = "";
        Cursor c = this.db.query("MasterResponseTable", new String[]{this.isUpgradedVirsionAvl_key}, null, null, null, null, null);
        if (c.moveToFirst()) {
            data = c.getString(0);
        }
        c.close();
        if (data.equals("") || data.equals("0")) {
            return "";
        }
        if (data.indexOf("*") != -1) {
            return splitString(data, "*")[2];
        }
        return "";
    }

    public void setforceUpdateLinkToDownload(String data) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(this.ForceUpdateLinkToDownload_key, data);
        this.db.update("MasterResponseTable", contentValues, String.valueOf(this.id_key) + "=1", null);
    }

    public String getforceUpdateLinkToDownload() {
        String data = "";
        Cursor c = this.db.query("MasterResponseTable", new String[]{this.ForceUpdateLinkToDownload_key}, null, null, null, null, null);
        if (c.moveToFirst()) {
            data = c.getString(0);
        }
        c.close();
        if (data.equals("") || data.equals("null")) {
            return "No Link";
        }
        return data;
    }

    public void setUpgradeInfoLink(String data) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(this.UpgradeInfoLink_key, data);
        this.db.update("MasterResponseTable", contentValues, String.valueOf(this.id_key) + "=1", null);
    }

    public String getUpgradeInfoLink() {
        String data = "";
        Cursor c = this.db.query("MasterResponseTable", new String[]{this.UpgradeInfoLink_key}, null, null, null, null, null);
        if (c.moveToFirst()) {
            data = c.getString(0);
        }
        c.close();
        if (data.equals("")) {
            return "";
        }
        return data;
    }

    public void setAddPosions(String data) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(this.AddsPosition_key, data);
        this.db.update("MasterResponseTable", contentValues, String.valueOf(this.id_key) + "=1", null);
    }

    public void setIsAppRestricted(String data) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(this.isAppRestricted_key, data);
        this.db.update("MasterResponseTable", contentValues, String.valueOf(this.id_key) + "=1", null);
    }

    public void setAppRestrictions(String data) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(this.AppRestrictions_key, data);
        this.db.update("MasterResponseTable", contentValues, String.valueOf(this.id_key) + "=1", null);
    }

    public String getTempAddPosions() {
        String data = "";
        Cursor c = this.db.query("MasterResponseTable", new String[]{this.AddsPosition_key}, null, null, null, null, null);
        if (c.moveToFirst()) {
            data = c.getString(0);
        }
        c.close();
        return data;
    }

    public String getAddPosions() {
        String data = "";
        Cursor c = this.db.query("MasterResponseTable", new String[]{this.AddsPosition_key}, null, null, null, null, null);
        if (c.moveToFirst()) {
            data = c.getString(0);
        }
        c.close();
        if (data.equals("") && this.config.getFTYPE().equals("3")) {
            return "Cross Promotional Adds only";
        }
        if (data.equals("1")) {
            return "ThirdParty(Upper) Adds only";
        }
        if (data.equals("2")) {
            return "ThirdParty(Lower) Adds only";
        }
        if (data.equals("4")) {
            return "ThirdParty(Upper & Lower) Adds only";
        }
        if (data.equals("3")) {
            return "Cross Promotional Adds only";
        }
        if (data.equals("5")) {
            return "ThirdParty(Upper) & Cross Promotional Adds only";
        }
        if (data.equals("6")) {
            return "ThirdParty(Lower) & Cross Promotional Adds only";
        }
        return "Both ThirdParty and Cross Promotional Adds";
    }

    public boolean isAppRestricted() {
        String data = "";
        Cursor c = this.db.query("MasterResponseTable", new String[]{this.isAppRestricted_key}, null, null, null, null, null);
        if (c.moveToFirst()) {
            data = c.getString(0);
        }
        c.close();
        if (data.equals("")) {
            return true;
        }
        if (data.equals("1")) {
            return true;
        }
        return false;
    }

    public String getAppRestrictions() {
        String data = "";
        Cursor c = this.db.query("MasterResponseTable", new String[]{this.AppRestrictions_key}, null, null, null, null, null);
        if (c.moveToFirst()) {
            data = c.getString(0);
        }
        c.close();
        if (data.equals("")) {
            return "1*1";
        }
        return data;
    }

    public long createRow(String appName, String appIcon, String smallDesc, String longDesc, String ss1, String ss2, String buyLink, String price2, String rating) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(this.AppName_key, appName);
        initialValues.put(this.AppIcon_key, appIcon);
        initialValues.put(this.SmallDesc_key, smallDesc);
        initialValues.put(this.LongDesc_key, longDesc);
        initialValues.put(this.ScreenShot1_key, ss1);
        initialValues.put(this.ScreenShot2_key, ss2);
        initialValues.put(this.Buy_key, buyLink);
        initialValues.put(this.Price_key, price2);
        initialValues.put(this.Rating_key, rating);
        return this.db.insert("UpgradedAppTable", null, initialValues);
    }

    public void deleteRow(long rowId) {
        this.db.delete("UpgradedAppTable", "_id=" + rowId, null);
    }

    public void deleteUpgradedAppDetails() {
        this.db.execSQL("delete from UpgradedAppTable");
    }

    public int getUpgradedAppCount() {
        int count2;
        Cursor c = this.db.query("UpgradedAppTable", new String[]{this.id_key, this.AppName_key, this.AppIcon_key, this.SmallDesc_key, this.LongDesc_key, this.ScreenShot1_key, this.ScreenShot2_key}, null, null, null, null, null);
        if (c != null) {
            count2 = c.getCount();
        } else {
            count2 = 0;
        }
        c.close();
        return count2;
    }

    public Vector<String> getAppName() {
        Vector<String> v = new Vector<>();
        Cursor c = this.db.query("UpgradedAppTable", new String[]{this.AppName_key}, null, null, null, null, null);
        while (c.moveToNext()) {
            v.add(c.getString(0));
        }
        c.close();
        return v;
    }

    public Vector<String> getAppIcon() {
        Vector<String> v = new Vector<>();
        Cursor c = this.db.query("UpgradedAppTable", new String[]{this.AppIcon_key}, null, null, null, null, null);
        while (c.moveToNext()) {
            v.add(c.getString(0));
        }
        c.close();
        return v;
    }

    public Vector<String> getSmallDesc() {
        Vector<String> v = new Vector<>();
        Cursor c = this.db.query("UpgradedAppTable", new String[]{this.SmallDesc_key}, null, null, null, null, null);
        while (c.moveToNext()) {
            v.add(c.getString(0));
        }
        c.close();
        return v;
    }

    public Vector<String> getLongDesc() {
        Vector<String> v = new Vector<>();
        Cursor c = this.db.query("UpgradedAppTable", new String[]{this.LongDesc_key}, null, null, null, null, null);
        while (c.moveToNext()) {
            v.add(c.getString(0));
        }
        c.close();
        return v;
    }

    public Vector<String> getScreenShot1() {
        Vector<String> v = new Vector<>();
        Cursor c = this.db.query("UpgradedAppTable", new String[]{this.ScreenShot1_key}, null, null, null, null, null);
        while (c.moveToNext()) {
            v.add(c.getString(0));
        }
        c.close();
        return v;
    }

    public Vector<String> getScreenShot2() {
        Vector<String> v = new Vector<>();
        Cursor c = this.db.query("UpgradedAppTable", new String[]{this.ScreenShot2_key}, null, null, null, null, null);
        while (c.moveToNext()) {
            v.add(c.getString(0));
        }
        c.close();
        return v;
    }

    public Vector<String> getPrice() {
        Vector<String> v = new Vector<>();
        Cursor c = this.db.query("UpgradedAppTable", new String[]{this.Price_key}, null, null, null, null, null);
        while (c.moveToNext()) {
            v.add(c.getString(0));
        }
        c.close();
        return v;
    }

    public Vector<String> getRating() {
        Vector<String> v = new Vector<>();
        Cursor c = this.db.query("UpgradedAppTable", new String[]{this.Rating_key}, null, null, null, null, null);
        while (c.moveToNext()) {
            v.add(new StringBuilder().append(c.getInt(0)).toString());
        }
        c.close();
        return v;
    }

    public Vector<String> getUpgradedAppBuyLinks() {
        Vector<String> v = new Vector<>();
        Cursor c = this.db.query("UpgradedAppTable", new String[]{this.Buy_key}, null, null, null, null, null);
        while (c.moveToNext()) {
            v.add(c.getString(0));
        }
        c.close();
        return v;
    }

    public int getUpgradedAppRepetitionUsage() {
        String data = "";
        Cursor c = this.db.query("MasterResponseTable", new String[]{this.isUpgradedVirsionAvl_key}, null, null, null, null, null);
        if (c.moveToFirst()) {
            data = c.getString(0);
        }
        c.close();
        if (data.equals("") || data.equals("0")) {
            return 0;
        }
        if (data.indexOf("*") != -1) {
            return Integer.parseInt(splitString(data, "*")[1]);
        }
        return 5;
    }

    public void updateValues(String isAppAuthorized, String isBuyAppEnable, String BillingType, String isFreeAppEnable, String FreeAppButtonText, String FreeAppButtonImage, String isForceUpdateEnable, String isUpgradedVirsionAvl, String ForceUpdateLinkToDownload, String UpgradeInfoLink) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(this.isAppAuthorized_key, isAppAuthorized);
        initialValues.put(this.isBuyAppEnable_key, isBuyAppEnable);
        initialValues.put(this.BillingType_key, BillingType);
        initialValues.put(this.isFreeAppEnable_key, isFreeAppEnable);
        initialValues.put(this.FreeAppButtonText_key, FreeAppButtonText);
        initialValues.put(this.FreeAppButtonImage_key, FreeAppButtonImage);
        initialValues.put(this.isForceUpdateEnable_key, isForceUpdateEnable);
        initialValues.put(this.isUpgradedVirsionAvl_key, isUpgradedVirsionAvl);
        initialValues.put(this.ForceUpdateLinkToDownload_key, ForceUpdateLinkToDownload);
        initialValues.put(this.UpgradeInfoLink_key, UpgradeInfoLink);
        Cursor c = this.db.query("MasterResponseTable", null, null, null, null, null, null);
        if (c != null && c.getCount() == 1) {
            this.db.update("MasterResponseTable", initialValues, String.valueOf(this.id_key) + "=1", null);
        } else if (c != null && c.getCount() == 0) {
            this.db.insert("MasterResponseTable", null, initialValues);
        } else if (c == null) {
            this.db.insert("MasterResponseTable", null, initialValues);
        }
    }

    public void deleteMasterResponseRow(long rowId) {
        this.db.delete("UpgradedAppTable", "_id=" + rowId, null);
    }

    public void deleteMasterResponseDetails() {
        this.db.execSQL("delete from MasterResponseTable");
    }

    public String getValueOf(String key) {
        Cursor c = this.db.query("MasterResponseTable", new String[]{key}, null, null, null, null, null);
        String data = c.getString(0);
        c.close();
        return data;
    }

    public void setValueOf(String key, String value) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(key, value);
        this.db.update("MasterResponseTable", contentValues, String.valueOf(this.id_key) + "=1", null);
    }

    public long insertCPRow(String appName, String AppLogo, String Buy, String More_App) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(this.CP_AppName_key, appName);
        initialValues.put(this.CP_AppLogo_key, AppLogo);
        initialValues.put(this.CP_Buy_key, Buy);
        initialValues.put(this.CP_More_App_key, More_App);
        return this.db.insert("CPApps", null, initialValues);
    }

    public void deleteCPRow(long rowId) {
        this.db.delete("CPApps", "_id=" + rowId, null);
    }

    public void deleteCPApps() {
        this.db.execSQL("delete from CPApps");
    }

    public int getCPCount() {
        int count2;
        Cursor c = this.db.query("CPApps", new String[]{this.id_key, this.CP_AppName_key, this.CP_AppLogo_key, this.CP_Buy_key, this.CP_More_App_key}, null, null, null, null, null);
        if (c != null) {
            count2 = c.getCount();
        } else {
            count2 = 0;
        }
        c.close();
        return count2;
    }

    public boolean appExists(String appName) {
        int count2;
        Cursor c = this.db.query("CPApps", new String[]{this.CP_AppName_key}, String.valueOf(this.CP_AppName_key) + " = '" + appName + "'", null, null, null, null);
        if (c != null) {
            count2 = c.getCount();
        } else {
            count2 = 0;
        }
        c.close();
        if (count2 > 0) {
            return true;
        }
        return false;
    }

    public Vector<String> getCPAppName() {
        Vector<String> v = new Vector<>();
        Cursor c = this.db.query("CPApps", new String[]{this.CP_AppName_key}, null, null, null, null, null);
        while (c.moveToNext()) {
            v.add(c.getString(0));
        }
        c.close();
        return v;
    }

    public Vector<String> getCPAppLogo() {
        Vector<String> v = new Vector<>();
        Cursor c = this.db.query("CPApps", new String[]{this.CP_AppLogo_key}, null, null, null, null, null);
        while (c.moveToNext()) {
            v.add(c.getString(0));
        }
        c.close();
        return v;
    }

    public Vector<String> getCPBuyLinks() {
        Vector<String> v = new Vector<>();
        Cursor c = this.db.query("CPApps", new String[]{this.CP_Buy_key}, null, null, null, null, null);
        while (c.moveToNext()) {
            v.add(c.getString(0));
        }
        c.close();
        return v;
    }

    public String getCPMoreLink() {
        String v = "";
        Cursor c = this.db.query("CPApps", new String[]{this.CP_More_App_key}, null, null, null, null, null);
        while (c.moveToNext()) {
            v = c.getString(0);
        }
        c.close();
        return v;
    }

    public String onMoreClick() {
        return "ShowMigitalWap";
    }

    public void handleBuyApp(Context context2) {
        if (getBillingMode().equals("Bango")) {
            showProgressDialogForBango(context2);
        } else {
            context2.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(getBillingMode())));
        }
    }

    public void showUpgradedAppList(Context context2) {
        if (getUpgradedVirsionAvlStatus()) {
            int repUsage = getUpgradedAppRepetitionUsage();
            int usageCount = getUpgradeAppShownUsage();
            if ((getUpgradedAppsLocation().equals("1") || getUpgradedAppsLocation().equals("3")) && usageCount % repUsage == 0) {
                showUpgradedAppDialog(context2, "Entry");
            }
        }
    }

    public void showUpgradedAppDialog(final Context context2, final String state) {
        AlertDialog alertDialog = new AlertDialog.Builder(context2).create();
        alertDialog.setTitle("Upgraded Applications");
        alertDialog.setMessage("You can upgrade this App. to a Higher version with much more advanced and amazing features. Click 'continue' to check the upgraded applications!");
        alertDialog.setButton("Continue", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                if (state.equals("Exit")) {
                    ((MainMenu) context2).finish();
                }
                Intent intent = new Intent(context2, UpgradedApps.class);
                intent.putExtra("AppState", state);
                context2.startActivity(intent);
            }
        });
        alertDialog.setButton2("No Thanks", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                if (state.equals("Exit")) {
                    ((MainMenu) context2).finish();
                }
            }
        });
        alertDialog.show();
    }

    public void showDlgOnEntryExit() {
    }

    public void showBackPressedDialog(Context context2) {
        if (!getUpgradedVirsionAvlStatus() || ((!getUpgradedAppsLocation().equals("2") && !getUpgradedAppsLocation().equals("3")) || getUpgradeAppShownUsage() % getUpgradedAppRepetitionUsage() != 0)) {
            showExitDialog(context2);
        } else {
            showUpgradedAppDialog(context2, "Exit");
        }
    }

    public void showExitDialog(final Context context2) {
        View v = LayoutInflater.from(context2).inflate((int) R.layout.exit_buttons, (ViewGroup) null);
        final MainMenu mainMenu = (MainMenu) context2;
        AlertDialog.Builder exitDialogBuilder = new AlertDialog.Builder(context2);
        exitDialogBuilder.setTitle(this.config.getAppName());
        exitDialogBuilder.setIcon((int) R.drawable.mine_sweeper_small_icon);
        exitDialogBuilder.setMessage("Do you really want to exit ?");
        exitDialogBuilder.setView(v);
        final AlertDialog exitDialog = exitDialogBuilder.create();
        exitDialog.show();
        ((Button) v.findViewById(R.id.Button01)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                mainMenu.finish();
            }
        });
        ((Button) v.findViewById(R.id.Button02)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                exitDialog.dismiss();
                ((Activity) context2).startActivity(new Intent("android.intent.action.VIEW", Uri.parse(AppConstants.free_apps_url)));
            }
        });
        ((Button) v.findViewById(R.id.Button03)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                exitDialog.dismiss();
            }
        });
        exitDialog.setCancelable(false);
    }

    public void showProgressDialogForBango(Context context2) {
        this.context = context2;
        this.myProgressDialog = ProgressDialog.show(context2, "Processing !", "Please wait..", true);
        new Thread() {
            public void run() {
                try {
                    EngineIO.this.price = EngineIO.this.netHandler.getDataFrmUrl(AppConstants.primaryCheckPriceUrl);
                    System.out.println("price = " + EngineIO.this.price);
                    EngineIO.this.priceHandler.sendEmptyMessage(0);
                } catch (Exception e) {
                    e.printStackTrace();
                    EngineIO.this.priceHandler.sendEmptyMessage(0);
                }
            }
        }.start();
    }

    public void showPrompt(Context context2, String msg) {
        AlertDialog prompt = new AlertDialog.Builder(context2).create();
        prompt.setTitle("Note:");
        prompt.setMessage(msg);
        prompt.setButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dia, int arg1) {
                dia.dismiss();
            }
        });
        prompt.show();
    }

    public void handleUpdateButton(Context context2) {
        if (getForceUpdateEnableStatus()) {
            this.updateDialogIntent.putExtra("Status", "Finish");
            ((Activity) context2).startActivity(this.updateDialogIntent);
            return;
        }
        showPrompt(context2, " No update available now, please try later.");
    }

    public void showUpdateDownloadDialog(final Context context2) {
        final AlertDialog updateDownloadDialog = new AlertDialog.Builder(context2).create();
        updateDownloadDialog.setTitle("Download Updates");
        updateDownloadDialog.setMessage("Updated version of '" + this.config.getAppName() + "' available click yes to download");
        updateDownloadDialog.setButton("Yes", new DialogInterface.OnClickListener() {
            private ProgressDialog progressDialog;

            public void onClick(DialogInterface arg0, int arg1) {
                if (Environment.getExternalStorageState().equals("mounted")) {
                    this.progressDialog = ProgressDialog.show(context2, "Downloading...", "Please wait..", true);
                    final Context context = context2;
                    new Thread() {
                        /* Debug info: failed to restart local var, previous not found, register: 9 */
                        public void run() {
                            super.run();
                            Message message = new Message();
                            Bundle bundle = new Bundle();
                            try {
                                boolean DownloadFromUrl = EngineIO.this.netHandler.DownloadFromUrl(EngineIO.this.getforceUpdateLinkToDownload(), String.valueOf(EngineIO.this.config.getAppName()) + ".apk");
                                bundle.putString("DownLoadResult", "DownLoad sucessfull");
                                message.setData(bundle);
                                ((MainActivity) context).handler2.sendMessage(message);
                            } catch (Exception e) {
                                System.out.println("exception occoured in downloading updates = " + e.toString());
                                bundle.putString("DownLoadResult", "DownLoad unsucessfull");
                                message.setData(bundle);
                                ((MainActivity) context).handler2.sendMessage(message);
                            }
                        }
                    }.start();
                    return;
                }
                EngineIO.this.showPrompt(context2, "Please insert SD Card");
            }
        });
        updateDownloadDialog.setButton2("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                updateDownloadDialog.dismiss();
                Message message = new Message();
                Bundle bundle = new Bundle();
                bundle.putString("DownLoadResult", "DownLoad Cancel");
                message.setData(bundle);
                ((MainActivity) context2).handler2.sendMessage(message);
            }
        });
        updateDownloadDialog.show();
    }

    /* access modifiers changed from: private */
    public void showInstallDialog(final Context context2, String msg) {
        AlertDialog prompt = new AlertDialog.Builder(context2).create();
        prompt.setTitle("Note:");
        prompt.setMessage(msg);
        prompt.setButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dia, int arg1) {
                dia.dismiss();
                ((Activity) context2).finish();
                EngineIO.this.installUpdatedFile(context2);
            }
        });
        prompt.show();
    }

    public void installUpdatedFile(Context context2) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.setDataAndType(Uri.parse("file:///sdcard/" + AppConstants.updatedFileName), "application/vnd.android.package-archive");
        ((Activity) context2).startActivity(intent);
    }

    /* Debug info: failed to restart local var, previous not found, register: 8 */
    public String[] splitString(String original, String sep) {
        Vector nodes = new Vector();
        String separator = sep;
        int index = original.indexOf(separator);
        while (index >= 0) {
            String part = original.substring(0, index);
            if (!part.equals("")) {
                nodes.addElement(part.trim());
            }
            original = original.substring(separator.length() + index);
            index = original.indexOf(separator);
        }
        if (!original.equals("")) {
            nodes.addElement(original);
        }
        String[] result = new String[nodes.size()];
        if (nodes.size() > 0) {
            for (int loop = 0; loop < nodes.size(); loop++) {
                result[loop] = (String) nodes.elementAt(loop);
            }
        }
        return result;
    }
}
