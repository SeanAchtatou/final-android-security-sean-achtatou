package android.engine;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.mine.test_lite.R;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;

public class CrossPromAds {
    public static int CPAppsCount;
    public static Vector<String> appBuy = new Vector<>();
    public static LinearLayout[] appLayout = new LinearLayout[10];
    public static Vector<String> appLogo = new Vector<>();
    public static Vector<String> appName = new Vector<>();
    public static TextView[] cpAppNames = new TextView[10];
    public static ImageView[] cpIcons = new ImageView[10];
    public static Button hideButton;
    public static boolean isProcessing;
    public static LinearLayout moreAppContainerLayout;
    public static TextView moreAppTextView;
    public static String moreCPApps;
    public static boolean reChecked;
    public static boolean reChecking;
    public static Vector<String> tempAppBuy = new Vector<>();
    public static Vector<String> tempAppLogo = new Vector<>();
    public static Vector<String> tempAppName = new Vector<>();
    public static XMLParser xmlParser;
    Bitmap bmp;
    Context context;
    String cpXml;
    View.OnClickListener crossPromotion1 = new View.OnClickListener() {
        public void onClick(View arg0) {
            try {
                CrossPromAds.this.context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(CrossPromAds.tempAppBuy.get(0).toString())));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    View.OnClickListener crossPromotion10 = new View.OnClickListener() {
        public void onClick(View arg0) {
            try {
                CrossPromAds.this.context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(CrossPromAds.tempAppBuy.get(9).toString())));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    View.OnClickListener crossPromotion2 = new View.OnClickListener() {
        public void onClick(View arg0) {
            try {
                CrossPromAds.this.context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(CrossPromAds.tempAppBuy.get(1).toString())));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    View.OnClickListener crossPromotion3 = new View.OnClickListener() {
        public void onClick(View arg0) {
            try {
                CrossPromAds.this.context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(CrossPromAds.tempAppBuy.get(2).toString())));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    View.OnClickListener crossPromotion4 = new View.OnClickListener() {
        public void onClick(View arg0) {
            try {
                CrossPromAds.this.context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(CrossPromAds.tempAppBuy.get(3).toString())));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    View.OnClickListener crossPromotion5 = new View.OnClickListener() {
        public void onClick(View arg0) {
            try {
                CrossPromAds.this.context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(CrossPromAds.tempAppBuy.get(4).toString())));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    View.OnClickListener crossPromotion6 = new View.OnClickListener() {
        public void onClick(View arg0) {
            try {
                CrossPromAds.this.context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(CrossPromAds.tempAppBuy.get(5).toString())));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    View.OnClickListener crossPromotion7 = new View.OnClickListener() {
        public void onClick(View arg0) {
            try {
                CrossPromAds.this.context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(CrossPromAds.tempAppBuy.get(6).toString())));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    View.OnClickListener crossPromotion8 = new View.OnClickListener() {
        public void onClick(View arg0) {
            try {
                CrossPromAds.this.context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(CrossPromAds.tempAppBuy.get(7).toString())));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    View.OnClickListener crossPromotion9 = new View.OnClickListener() {
        public void onClick(View arg0) {
            try {
                CrossPromAds.this.context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(CrossPromAds.tempAppBuy.get(8).toString())));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    EngineIO engineIO;
    Handler handler;
    View.OnClickListener hideButtonListener = new View.OnClickListener() {
        public void onClick(View arg0) {
            if (AddManager.cpStatus.equals("Visible")) {
                CrossPromAds.moreAppContainerLayout.startAnimation(CrossPromAds.this.rightAnimation);
                AddManager.cpStatus = "Hidden";
                AddManager.fetchCPAds = false;
            } else if (AddManager.cpStatus.equals("Hidden")) {
                CrossPromAds.moreAppContainerLayout.startAnimation(CrossPromAds.this.leftAnimation);
                AddManager.cpStatus = "Visible";
                AddManager.fetchCPAds = true;
            }
        }
    };
    int[] imgViewIds = {R.id.ImageView1, R.id.ImageView2, R.id.ImageView3, R.id.ImageView4, R.id.ImageView5, R.id.ImageView6, R.id.ImageView7, R.id.ImageView8, R.id.ImageView9, R.id.ImageView10};
    int[] layoutIds = {R.id.LinearLayout1, R.id.LinearLayout2, R.id.LinearLayout3, R.id.LinearLayout4, R.id.LinearLayout5, R.id.LinearLayout6, R.id.LinearLayout7, R.id.LinearLayout8, R.id.LinearLayout8, R.id.LinearLayout10};
    Animation leftAnimation;
    View.OnClickListener moreAppClick = new View.OnClickListener() {
        public void onClick(View arg0) {
            if (AppConstants.more_cp_apps == null) {
                AppConstants.more_cp_apps = "http://migital.com/scms/wap/index.aspx";
            }
            if (AppConstants.more_cp_apps.startsWith("http://") || AppConstants.more_cp_apps.startsWith("https://")) {
                CrossPromAds.this.context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(AppConstants.more_cp_apps)));
                return;
            }
            if (AppConstants.more_cp_apps == null) {
                AppConstants.more_cp_apps = "Migital";
            }
            CrossPromAds.this.startMarket(AppConstants.more_cp_apps);
        }
    };
    NetHandler netHandler;
    Animation rightAnimation;
    Runnable runnable = new Runnable() {
        public void run() {
            if (AddManager.showCPAdds()) {
                CrossPromAds.this.showCPAds();
            }
        }
    };
    int[] textViewIds = {R.id.TextView1, R.id.TextView2, R.id.TextView3, R.id.TextView4, R.id.TextView5, R.id.TextView6, R.id.TextView7, R.id.TextView8, R.id.TextView9, R.id.TextView10};

    public CrossPromAds(Context context2) {
        this.context = context2;
        this.handler = new Handler();
        this.netHandler = new NetHandler(context2);
        xmlParser = new XMLParser();
        this.engineIO = new EngineIO(context2);
    }

    public void getCPAdsControls() {
        moreAppContainerLayout = (LinearLayout) AddManager.currentActivity.findViewById(R.id.LinearLayout_more_app);
        for (int i = 0; i < this.layoutIds.length; i++) {
            appLayout[i] = (LinearLayout) AddManager.currentActivity.findViewById(this.layoutIds[i]);
        }
        for (int i2 = 0; i2 < this.imgViewIds.length; i2++) {
            cpIcons[i2] = (ImageView) AddManager.currentActivity.findViewById(this.imgViewIds[i2]);
        }
        for (int i3 = 0; i3 < this.textViewIds.length; i3++) {
            cpAppNames[i3] = (TextView) AddManager.currentActivity.findViewById(this.textViewIds[i3]);
        }
        if (AddManager.currentActivityCode == 1) {
            this.leftAnimation = AnimationUtils.loadAnimation(this.context, R.anim.push_left);
            this.leftAnimation.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationStart(Animation arg0) {
                }

                public void onAnimationRepeat(Animation arg0) {
                }

                public void onAnimationEnd(Animation arg0) {
                    CrossPromAds.moreAppContainerLayout.setVisibility(0);
                    CrossPromAds.hideButton.setBackgroundResource(R.drawable.right);
                }
            });
            this.rightAnimation = AnimationUtils.loadAnimation(this.context, R.anim.push_right);
            this.rightAnimation.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationStart(Animation arg0) {
                }

                public void onAnimationRepeat(Animation arg0) {
                }

                public void onAnimationEnd(Animation arg0) {
                    CrossPromAds.moreAppContainerLayout.setVisibility(8);
                    CrossPromAds.hideButton.setBackgroundResource(R.drawable.left);
                }
            });
            hideButton = (Button) AddManager.currentActivity.findViewById(R.id.Button_hide);
            if (AddManager.cpStatus.equals("Starting")) {
                hideButton.setVisibility(8);
            }
        }
        moreAppTextView = (TextView) AddManager.currentActivity.findViewById(R.id.TextView_more_app);
        setControlFunctionality();
    }

    public void setControlFunctionality() {
        cpIcons[0].setOnClickListener(this.crossPromotion1);
        cpIcons[1].setOnClickListener(this.crossPromotion2);
        cpIcons[2].setOnClickListener(this.crossPromotion3);
        cpIcons[3].setOnClickListener(this.crossPromotion4);
        cpIcons[4].setOnClickListener(this.crossPromotion5);
        cpIcons[5].setOnClickListener(this.crossPromotion6);
        cpIcons[6].setOnClickListener(this.crossPromotion7);
        cpIcons[7].setOnClickListener(this.crossPromotion8);
        cpIcons[8].setOnClickListener(this.crossPromotion9);
        cpIcons[9].setOnClickListener(this.crossPromotion10);
        if (AddManager.currentActivityCode == 1) {
            hideButton.setOnClickListener(this.hideButtonListener);
        }
        moreAppTextView.setOnClickListener(this.moreAppClick);
    }

    public void startMarket(String keyword) {
        if (keyword == null) {
            keyword = "migital";
        } else if (keyword.equals("") || keyword.length() == 0) {
            keyword = "migital";
        }
        AddManager.currentActivity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=" + keyword)));
    }

    public synchronized void getCPAds() {
        new Thread() {
            public synchronized void run() {
                try {
                    System.out.println("1  appName.size()  = " + CrossPromAds.appName.size());
                    if (CrossPromAds.appName != null && CrossPromAds.appName.size() <= 0 && CrossPromAds.appBuy != null && CrossPromAds.appBuy.size() <= 0 && !CrossPromAds.isProcessing) {
                        CrossPromAds.isProcessing = true;
                        String cpXML = CrossPromAds.this.netHandler.getDataFrmUrl(AppConstants.primaryCrossPromotionLink);
                        if (cpXML == null) {
                            cpXML = CrossPromAds.this.netHandler.getDataFrmUrl(AppConstants.secondaryCrossPromotionLink);
                        }
                        if (cpXML == null) {
                            cpXML = CrossPromAds.this.netHandler.getDataFrmUrl(AppConstants.thirdCrossPromotionLink);
                        }
                        System.out.println("cpXML = " + cpXML);
                        CrossPromAds.xmlParser.parseXML(cpXML);
                        CrossPromAds.isProcessing = false;
                        if (CrossPromAds.appName != null && CrossPromAds.appName.size() > 0) {
                            CrossPromAds.appName.clear();
                        }
                        if (CrossPromAds.appLogo != null && CrossPromAds.appLogo.size() > 0) {
                            CrossPromAds.appLogo.clear();
                        }
                        if (CrossPromAds.appBuy != null && CrossPromAds.appBuy.size() > 0) {
                            CrossPromAds.appBuy.clear();
                        }
                        if (CrossPromAds.appName.size() <= 0) {
                            CrossPromAds.appName.addAll(CrossPromAds.xmlParser.getAppName());
                        }
                        if (CrossPromAds.appLogo.size() <= 0) {
                            CrossPromAds.appLogo.addAll(CrossPromAds.xmlParser.getAppLogo());
                        }
                        if (CrossPromAds.appBuy.size() <= 0) {
                            CrossPromAds.appBuy.addAll(CrossPromAds.xmlParser.getAppBuyLinks());
                        }
                        AppConstants.more_cp_apps = CrossPromAds.xmlParser.getMoreURL();
                        CrossPromAds.CPAppsCount = CrossPromAds.appName.size();
                        CrossPromAds.this.checkCPApps();
                    }
                    if (CrossPromAds.appName != null && CrossPromAds.appName.size() > 0) {
                        CrossPromAds.this.reCheckCPApps();
                        CrossPromAds.this.checkCPApps();
                        CrossPromAds.this.handleCPVectors();
                        CrossPromAds.this.handler.post(CrossPromAds.this.runnable);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    CrossPromAds.isProcessing = false;
                }
                return;
            }
        }.start();
    }

    public void reCheckCPApps() {
        new Thread() {
            public synchronized void run() {
                try {
                    System.out.println("reChecking cross promotional adds");
                    if (!CrossPromAds.reChecking && !CrossPromAds.reChecked) {
                        CrossPromAds.reChecking = true;
                        String cpXML = CrossPromAds.this.netHandler.getDataFrmUrl(AppConstants.primaryCrossPromotionLink);
                        if (cpXML == null) {
                            cpXML = CrossPromAds.this.netHandler.getDataFrmUrl(AppConstants.secondaryCrossPromotionLink);
                        }
                        if (cpXML == null) {
                            cpXML = CrossPromAds.this.netHandler.getDataFrmUrl(AppConstants.thirdCrossPromotionLink);
                        }
                        CrossPromAds.xmlParser.parseXML(cpXML);
                        CrossPromAds.reChecking = false;
                        if (CrossPromAds.xmlParser.getAppName().size() > CrossPromAds.appName.size()) {
                            if (CrossPromAds.appName != null && CrossPromAds.appName.size() > 0) {
                                CrossPromAds.appName.clear();
                            }
                            if (CrossPromAds.appLogo != null && CrossPromAds.appLogo.size() > 0) {
                                CrossPromAds.appLogo.clear();
                            }
                            if (CrossPromAds.appBuy != null && CrossPromAds.appBuy.size() > 0) {
                                CrossPromAds.appBuy.clear();
                            }
                            if (CrossPromAds.appName.size() <= 0) {
                                CrossPromAds.appName.addAll(CrossPromAds.xmlParser.getAppName());
                            }
                            if (CrossPromAds.appLogo.size() <= 0) {
                                CrossPromAds.appLogo.addAll(CrossPromAds.xmlParser.getAppLogo());
                            }
                            if (CrossPromAds.appBuy.size() <= 0) {
                                CrossPromAds.appBuy.addAll(CrossPromAds.xmlParser.getAppBuyLinks());
                            }
                            CrossPromAds.this.checkCPApps();
                            AppConstants.more_cp_apps = CrossPromAds.xmlParser.getMoreURL();
                        }
                        CrossPromAds.reChecked = true;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    CrossPromAds.reChecking = false;
                }
                return;
            }
        }.start();
    }

    public void initVectors() {
        new Thread() {
            public void run() {
                if (CrossPromAds.appName.size() <= 0) {
                    if (CrossPromAds.appName != null && CrossPromAds.appName.size() > 0) {
                        CrossPromAds.appName.clear();
                    }
                    if (CrossPromAds.appLogo != null && CrossPromAds.appLogo.size() > 0) {
                        CrossPromAds.appLogo.clear();
                    }
                    if (CrossPromAds.appBuy != null && CrossPromAds.appBuy.size() > 0) {
                        CrossPromAds.appBuy.clear();
                    }
                    if (CrossPromAds.this.engineIO.getCPAppName() != null && CrossPromAds.this.engineIO.getCPAppName().size() > 0) {
                        CrossPromAds.appName.addAll(CrossPromAds.this.engineIO.getCPAppName());
                    }
                    if (CrossPromAds.this.engineIO.getCPAppLogo() != null && CrossPromAds.this.engineIO.getCPAppLogo().size() > 0) {
                        CrossPromAds.appLogo.addAll(CrossPromAds.this.engineIO.getCPAppLogo());
                    }
                    if (CrossPromAds.this.engineIO.getCPBuyLinks() != null && CrossPromAds.this.engineIO.getCPBuyLinks().size() > 0) {
                        CrossPromAds.appBuy.addAll(CrossPromAds.this.engineIO.getCPBuyLinks());
                    }
                    if (CrossPromAds.this.engineIO.getCPMoreLink() != null && CrossPromAds.this.engineIO.getCPMoreLink().length() > 0) {
                        CrossPromAds.moreCPApps = CrossPromAds.this.engineIO.getCPMoreLink();
                    }
                }
            }
        }.start();
    }

    public synchronized void displayCPAds() {
        System.out.println("CrossPromAds.displayCPAds()");
        if (appName.size() <= 0) {
            if (appName != null && appName.size() > 0) {
                appName.clear();
            }
            if (appLogo != null && appLogo.size() > 0) {
                appLogo.clear();
            }
            if (appBuy != null && appBuy.size() > 0) {
                appBuy.clear();
            }
            if (this.engineIO.getCPAppName() != null && this.engineIO.getCPAppName().size() > 0) {
                appName.addAll(this.engineIO.getCPAppName());
            }
            if (this.engineIO.getCPAppLogo() != null && this.engineIO.getCPAppLogo().size() > 0) {
                appLogo.addAll(this.engineIO.getCPAppLogo());
            }
            if (this.engineIO.getCPBuyLinks() != null && this.engineIO.getCPBuyLinks().size() > 0) {
                appBuy.addAll(this.engineIO.getCPBuyLinks());
            }
            if (this.engineIO.getCPMoreLink() != null && this.engineIO.getCPMoreLink().length() > 0) {
                moreCPApps = this.engineIO.getCPMoreLink();
            }
            handleCPVectors();
            this.handler.post(this.runnable);
        } else {
            this.handler.post(this.runnable);
        }
    }

    public void checkCPApps() {
        int i = 0;
        while (i < appName.size()) {
            try {
                if (!this.context.getFileStreamPath(String.valueOf(appName.get(i)) + ".png").exists()) {
                    saveImage(i);
                }
                if (!this.engineIO.appExists(appName.get(i))) {
                    this.engineIO.insertCPRow(appName.get(i), appLogo.get(i), appBuy.get(i), AppConstants.more_cp_apps);
                }
                i++;
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        }
    }

    public void showCPAds() {
        boolean cpAvailable = false;
        try {
            if (tempAppName.size() > 0) {
                if (AddManager.cpStatus.equals("Starting")) {
                    AddManager.cpStatus = "Visible";
                }
                if (AddManager.cpStatus.equals("Visible")) {
                    moreAppContainerLayout.setVisibility(0);
                    for (int i = 0; i < tempAppName.size(); i++) {
                        this.bmp = BitmapFactory.decodeFile(this.context.getFileStreamPath(String.valueOf(tempAppName.get(i)) + ".png").getAbsolutePath());
                        if (this.bmp == null || isImgBlank(this.bmp)) {
                            appLayout[i].setVisibility(8);
                        } else {
                            cpIcons[i].setImageBitmap(this.bmp);
                            cpAppNames[i].setText(tempAppName.get(i));
                            appLayout[i].setVisibility(0);
                            cpAvailable = true;
                        }
                    }
                    if (!cpAvailable) {
                        moreAppContainerLayout.setVisibility(8);
                    } else if (AddManager.showCPButton()) {
                        hideButton.setVisibility(0);
                    }
                } else {
                    moreAppContainerLayout.setVisibility(8);
                }
            } else {
                moreAppContainerLayout.setVisibility(8);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isImgBlank(Bitmap bmp2) {
        if (bmp2.getHeight() <= 5 || bmp2.getWidth() <= 5) {
            return true;
        }
        return false;
    }

    public void saveImage(int index) {
        System.out.println("creating file = " + appName.get(index) + ".png");
        try {
            InputStream inputStream = this.netHandler.getInputStream(appLogo.get(index));
            if (inputStream != null) {
                OutputStream outputStream = this.context.openFileOutput(String.valueOf(appName.get(index)) + ".png", 0);
                byte[] buffer = new byte[1024];
                while (true) {
                    int numRead = inputStream.read(buffer);
                    if (numRead < 0) {
                        outputStream.flush();
                        outputStream.close();
                        inputStream.close();
                        return;
                    }
                    outputStream.write(buffer, 0, numRead);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void handleCPVectors() {
        System.out.println("CrossPromAds.handleCPVectors()");
        if (tempAppName != null && tempAppName.size() > 0) {
            tempAppName.clear();
        }
        if (tempAppLogo != null && tempAppLogo.size() > 0) {
            tempAppLogo.clear();
        }
        if (tempAppBuy != null && tempAppBuy.size() > 0) {
            tempAppBuy.clear();
        }
        if (appName != null && appName.size() > 0) {
            int nameCount = 0;
            int i = 0;
            while (i < 10 && appName.size() > i) {
                tempAppName.add(appName.get(i));
                nameCount++;
                i++;
            }
            for (int i2 = 0; i2 < nameCount; i2++) {
                appName.remove(0);
                appName.add(tempAppName.get(i2));
            }
            System.out.println("appName.size() = " + appName.size() + " tempAppName.size()= " + tempAppName.size());
        }
        if (appLogo != null && appLogo.size() > 0) {
            int logoCount = 0;
            int i3 = 0;
            while (i3 < 10 && appLogo.size() > i3) {
                tempAppLogo.add(appLogo.get(i3));
                logoCount++;
                i3++;
            }
            for (int i4 = 0; i4 < logoCount; i4++) {
                appLogo.remove(0);
                appLogo.add(tempAppLogo.get(i4));
            }
            System.out.println("appLogo.size() = " + appLogo.size() + " tempAppLogo.size()= " + tempAppLogo.size());
        }
        if (appBuy != null && appBuy.size() > 0) {
            int buyCount = 0;
            int i5 = 0;
            while (i5 < 10 && appBuy.size() > i5) {
                tempAppBuy.add(appBuy.get(i5));
                buyCount++;
                i5++;
            }
            for (int i6 = 0; i6 < buyCount; i6++) {
                appBuy.remove(0);
                appBuy.add(tempAppBuy.get(i6));
            }
            System.out.println("appBuy.size() = " + appBuy.size());
        }
    }
}
