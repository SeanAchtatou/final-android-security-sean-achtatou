package android.engine;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.webkit.WebView;
import java.io.File;
import java.io.FileOutputStream;

public class MainActivity extends Activity {
    String DownLoadResult;
    AddManager addManager;
    AppConstants appConstants;
    Bundle bundle;
    Config config;
    Context currentContext;
    EngineIO engineIO;
    boolean finishApp = false;
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            try {
                if (MainActivity.this.progressDialog != null) {
                    MainActivity.this.progressDialog.dismiss();
                }
                Log.v(MainActivity.this.tag, "in handler");
                if (!MainActivity.this.engineIO.getForceUpdateEnableStatus() || (!MainActivity.this.config.getFTYPE().equals("1") && !MainActivity.this.config.getFTYPE().equals("2"))) {
                    if (MainActivity.this.engineIO.getForceUpdateEnableStatus() && MainActivity.this.config.getFTYPE().equals("3")) {
                        Log.v(MainActivity.this.tag, "showing dialog for full file");
                        int usageCount = MainActivity.this.engineIO.getUpdateAppShownUsage();
                        int repUsage = MainActivity.this.engineIO.getForceUpdateRepetitionUses();
                        System.out.println("getUpdateAppShownUsage = " + usageCount);
                        if (usageCount % repUsage == 0) {
                            MainActivity.this.startActivity(MainActivity.this.updateDialogIntent);
                        }
                    }
                } else if (Config.isAppRunning) {
                    UpdateDialog.showingUpdateDialog = true;
                    Log.v(MainActivity.this.tag, "showing dialog for add file 1");
                    MainActivity.this.updateDialogIntent.putExtra("Status", "Finish");
                    MainActivity.this.startActivity(MainActivity.this.updateDialogIntent);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    Handler handler2 = new Handler() {
        public void handleMessage(Message msg) {
            MainActivity.this.DownLoadResult = msg.getData().getString("DownLoadResult");
            if (MainActivity.this.DownLoadResult.equals("DownLoad sucessfull")) {
                MainActivity.this.showInstallDialog("Download sucessfull. Install now");
            } else if (MainActivity.this.DownLoadResult.equals("DownLoad unsucessfull")) {
                MainActivity.this.showInstallDialog("Error in download. Please try later");
            } else if (MainActivity.this.DownLoadResult.equals("DownLoad Cancel")) {
                MainActivity.this.startApp();
            }
        }
    };
    int installationCode = 1;
    String masterResponse = "";
    Message message;
    NetHandler netHandler;
    ProgressDialog progressDialog;
    CrossPromAds promAds;
    boolean startApp = false;
    long startTime;
    String tag = "Engine";
    String timeConsumed;
    Intent updateDialogIntent;
    XMLParser xmlParser;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            this.updateDialogIntent = new Intent();
            this.updateDialogIntent.setFlags(268435456);
            this.updateDialogIntent.setClass(this, UpdateDialog.class);
            changeContext(this);
            init();
            this.config.initialize2(this);
            this.promAds = new CrossPromAds(this);
            this.promAds.initVectors();
            setUserAgent();
            setInstallationTime();
            this.appConstants.initURLs(this.currentContext);
            Log.v(this.tag, "engineIO.getForceUpdateEnableStatus() = " + this.engineIO.getForceUpdateEnableStatus() + " and UpdateType = " + this.engineIO.getUpdateType());
            increaseUpdateUpgradeCount();
        } catch (Exception e) {
            System.out.println("exception in shareOnFacebook");
        }
        if (this.engineIO.isAddEnable()) {
            if (this.engineIO.getForceUpdateEnableStatus() && this.engineIO.getUpdateType().equals("ForceUpdate")) {
                this.updateDialogIntent.putExtra("Status", "install");
                startActivity(this.updateDialogIntent);
                finish();
            } else if (!this.engineIO.getForceUpdateEnableStatus() || !this.engineIO.getUpdateType().equals("ChoiceUpdate")) {
                getAdds();
                hitMasterLink();
                startApp();
                getUpdatedMigitalBanners();
            } else {
                this.engineIO.showUpdateDownloadDialog(this);
            }
        } else if (this.engineIO.check15thDay()) {
            showAuthanticateDialog();
        } else {
            startApp();
        }
    }

    public void init() {
        this.config = new Config(this.currentContext);
        this.netHandler = new NetHandler(this.currentContext);
        this.xmlParser = new XMLParser();
        this.engineIO = new EngineIO(this.currentContext);
        this.appConstants = new AppConstants();
    }

    public void increaseUpdateUpgradeCount() {
        if (!this.engineIO.getForceUpdateEnableStatus()) {
            this.engineIO.setUpdateAppShownUsage(0);
        } else {
            this.engineIO.setUpdateAppShownUsage(this.engineIO.getUpdateAppShownUsage() + 1);
        }
        if (!this.engineIO.getUpgradedVirsionAvlStatus()) {
            this.engineIO.setUpgradeAppShownUsage(0);
            this.engineIO.deleteUpgradedAppDetails();
            return;
        }
        this.engineIO.setUpgradeAppShownUsage(this.engineIO.getUpgradeAppShownUsage() + 1);
    }

    public void showAuthanticateDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Recommendation :");
        alertDialog.setMessage("Check for New Version of '" + this.config.getAppName() + "' and get Latest Updates and Fixes !" + "\n" + "Connect Now?");
        alertDialog.setButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                MainActivity.this.hitMasterLink();
                MainActivity.this.startApp();
            }
        });
        alertDialog.setButton2("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                MainActivity.this.startApp();
            }
        });
        alertDialog.show();
    }

    public synchronized void hitMasterLink() {
        this.startTime = System.currentTimeMillis();
        new Thread() {
            public void run() {
                super.run();
                try {
                    MainActivity.this.checkUpdatedUrls();
                    MainActivity.this.masterResponse = MainActivity.this.netHandler.getDataFrmUrl(AppConstants.primaryMasterLink);
                    String prevImgName = MainActivity.this.engineIO.getFreeAppButtonImageName();
                    if (MainActivity.this.masterResponse == null || MainActivity.this.masterResponse.indexOf("#") == -1) {
                        System.out.println("fails first time and masterResponse = " + MainActivity.this.masterResponse);
                        MainActivity.this.masterResponse = MainActivity.this.netHandler.getDataFrmUrl(AppConstants.secondaryMasterLink);
                        if (MainActivity.this.masterResponse == null || MainActivity.this.masterResponse.indexOf("#") == -1) {
                            System.out.println("fails second time time and masterResponse = " + MainActivity.this.masterResponse);
                            MainActivity.this.masterResponse = MainActivity.this.netHandler.getDataFrmUrl(AppConstants.thirdMasterLink);
                            if (!(MainActivity.this.masterResponse == null || MainActivity.this.masterResponse.indexOf("#") == -1)) {
                                MainActivity.this.engineIO.saveMasterResponse(MainActivity.this.masterResponse);
                            }
                        } else {
                            MainActivity.this.engineIO.saveMasterResponse(MainActivity.this.masterResponse);
                        }
                    } else {
                        MainActivity.this.engineIO.saveMasterResponse(MainActivity.this.masterResponse);
                    }
                    Log.v(MainActivity.this.tag, "masterResponse = " + MainActivity.this.masterResponse);
                    if (!prevImgName.equals(MainActivity.this.engineIO.getFreeAppButtonImageName()) || !MainActivity.this.currentContext.getFileStreamPath(prevImgName).exists()) {
                        if (MainActivity.this.currentContext.getFileStreamPath(prevImgName).exists()) {
                            MainActivity.this.currentContext.getFileStreamPath(prevImgName).delete();
                        }
                        byte[] img = MainActivity.this.netHandler.getImageArray(MainActivity.this.engineIO.getFreeAppButtonImageLink());
                        if (img != null) {
                            MainActivity.this.saveImage(img, MainActivity.this.engineIO.getFreeAppButtonImageName());
                        }
                    }
                    if (MainActivity.this.engineIO.getUpgradedVirsionAvlStatus()) {
                        MainActivity.this.getUpgradedAppDetails();
                    }
                    MainActivity.this.handler.sendEmptyMessage(0);
                } catch (Exception e) {
                    e.printStackTrace();
                    MainActivity.this.handler.sendEmptyMessage(0);
                }
            }
        }.start();
    }

    public void checkUpdatedUrls() {
        try {
            if (this.engineIO.check5thDay()) {
                String urlInfo = this.netHandler.getDataFrmUrl(AppConstants.primaryCheckURLVersionLink);
                if (urlInfo == null || urlInfo.equals("")) {
                    urlInfo = this.netHandler.getDataFrmUrl(AppConstants.secondaryCheckURLVersionLink);
                }
                if (urlInfo == null || urlInfo.equals("")) {
                    String urlInfo2 = this.netHandler.getDataFrmUrl(AppConstants.thirdCheckURLVersionLink);
                } else if (urlInfo.indexOf("#") != -1 && !urlInfo.equals("0#0")) {
                    String[] links = urlInfo.split("#");
                    if (Integer.parseInt(links[0]) > this.engineIO.getUrlVersion()) {
                        this.engineIO.setUrlVersion(links[0]);
                        this.engineIO.setPrimaryLink(links[1]);
                        this.engineIO.setSecondaryLink(links[2]);
                        this.engineIO.setThirdLink(links[3]);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setUserAgent() {
        this.config.setUSER_AGENT(new WebView(this).getSettings().getUserAgentString());
    }

    public void saveImage(byte[] bArray, String fileName) {
        try {
            FileOutputStream fOut = openFileOutput(fileName, 0);
            fOut.write(bArray);
            fOut.flush();
            fOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startApp() {
        if (!AppConstants.testing) {
            this.engineIO.setAppUsageCount(this.engineIO.getAppUsageCount() + 1);
            startActivity(new Intent(this, LogoDisplay.class));
            finish();
        }
    }

    public void getUpgradedAppDetails() {
        int usageCount = this.engineIO.getUpgradeAppShownUsage();
        System.out.println("getUpgradeAppShownUsage = " + usageCount + " and repUsage = " + this.engineIO.getUpgradedAppRepetitionUsage());
        String upgradedAppsXML = this.netHandler.getDataFrmUrl(this.engineIO.getUpgradeInfoLink());
        if (upgradedAppsXML != null) {
            this.xmlParser.parseXML(upgradedAppsXML);
            System.out.println("myXMLHandler.getUpgradedAppCount() = " + this.xmlParser.getUpgradedAppCount() + "  and engineIO.getUpgradedAppCount() = " + this.engineIO.getUpgradedAppCount());
            if (this.xmlParser.getUpgradedAppCount() > this.engineIO.getUpgradedAppCount()) {
                this.engineIO.deleteUpgradedAppDetails();
                for (int i = 0; i < this.xmlParser.getUpgradedAppCount(); i++) {
                    this.engineIO.createRow(this.xmlParser.getAppName().get(i), this.xmlParser.getAppIcon().get(i), this.xmlParser.getSmallDesc().get(i), this.xmlParser.getLongDesc().get(i), this.xmlParser.getSreenShot1().get(i), this.xmlParser.getSreenShot2().get(i), this.xmlParser.getAppBuyLinks().get(i), String.valueOf(this.xmlParser.getPrice().get(i)) + " " + this.xmlParser.getCurrency().get(i), this.xmlParser.getRating().get(i));
                }
            }
        }
    }

    public void setInstallationTime() {
        if (this.engineIO.getInstallationTime() == 0) {
            this.engineIO.setInstallationTime(System.currentTimeMillis());
            System.out.println("Setting installation time = " + System.currentTimeMillis());
        }
    }

    public void changeContext(Context context) {
        this.currentContext = context;
    }

    public void showPrompt(String msg) {
        AlertDialog prompt = new AlertDialog.Builder(this).create();
        prompt.setTitle("Note:");
        prompt.setMessage(msg);
        prompt.setButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dia, int arg1) {
                dia.dismiss();
                if (MainActivity.this.startApp) {
                    MainActivity.this.startApp = false;
                } else if (MainActivity.this.finishApp) {
                    MainActivity.this.finishApp = false;
                    MainActivity.this.finish();
                }
            }
        });
        prompt.show();
    }

    public String getAddType(String str) {
        if (str.startsWith("http:")) {
            return "image";
        }
        return "text";
    }

    public synchronized void getAdds() {
        try {
            this.addManager = new AddManager(this);
            new Thread(new Runnable() {
                public void run() {
                    try {
                        String[] strArr = null;
                        String response = MainActivity.this.netHandler.getDataFrmUrl(AppConstants.primaryAddsInfoFetchingUrl);
                        if (response == null || response.equals("")) {
                            response = MainActivity.this.netHandler.getDataFrmUrl(AppConstants.secondaryAddsInfoFetchingUrl);
                        }
                        if (response == null || response.equals("")) {
                            response = MainActivity.this.netHandler.getDataFrmUrl(AppConstants.thirdAddsInfoFetchingUrl);
                        }
                        if (response != null && !response.equals("")) {
                            String[] tokens = response.split("#");
                            if (!tokens[0].equals("NA")) {
                                AppConstants.sessionID = tokens[0];
                            }
                            if (!tokens[1].equals("0")) {
                                AppConstants.sessionTime = tokens[1];
                            }
                            if (!tokens[2].equals("0")) {
                                AddManager.addsDuration = Integer.parseInt(tokens[2]);
                            }
                            if (!tokens[3].equals("0")) {
                                AppConstants.AdsHostIP = tokens[3];
                            }
                            AppConstants.add_request_url = "http://" + AppConstants.AdsHostIP + "/Android/V1/AdsReq.aspx?Duc=" + MainActivity.this.config.getDUC() + "&PID=" + MainActivity.this.config.getPID() + "&W=480&H=50&TotAds=2&AdsId=" + MainActivity.this.config.getAdsID() + "&SessionId=" + AppConstants.sessionID + "&IMEI=" + MainActivity.this.config.getIMEI();
                        }
                        AppConstants.addsXml = MainActivity.this.netHandler.getDataFrmUrl(AppConstants.add_request_url);
                        if (AppConstants.addsXml != null) {
                            MainActivity.this.xmlParser.parseXML(AppConstants.addsXml);
                            AppConstants.Src = MainActivity.this.xmlParser.getSrc();
                            AppConstants.Link = MainActivity.this.xmlParser.getLink();
                            AppConstants.Id = MainActivity.this.xmlParser.getId();
                            if (MainActivity.this.getAddType(AppConstants.Src.get(0)).equals("image")) {
                                AppConstants.upper_add = "image";
                                AppConstants.top_add = MainActivity.this.netHandler.getImageFromUrl(AppConstants.Src.get(0).toString());
                                System.out.println("AppConstants.top_add= " + AppConstants.top_add);
                                if (AppConstants.top_add == null) {
                                    AppConstants.upper_add = "default";
                                }
                            } else if (MainActivity.this.getAddType(AppConstants.Src.get(0)).equals("text")) {
                                AppConstants.upper_add = "text";
                            }
                            AddManager.handle.post(MainActivity.this.addManager.enableUpperAdd);
                            if (MainActivity.this.getAddType(AppConstants.Src.get(1)).equals("image")) {
                                AppConstants.lower_add = "image";
                                AppConstants.bottom_add = MainActivity.this.netHandler.getImageFromUrl(AppConstants.Src.get(1).toString());
                                System.out.println("AppConstants.bottom_add= " + AppConstants.bottom_add);
                                if (AppConstants.bottom_add == null) {
                                    AppConstants.lower_add = "default";
                                }
                            } else if (MainActivity.this.getAddType(AppConstants.Src.get(1)).equals("text")) {
                                AppConstants.lower_add = "text";
                            }
                            AddManager.handle.post(MainActivity.this.addManager.enableLowerAdd);
                            System.out.println("Adds fetched in main Activity");
                        }
                    } catch (Exception e) {
                        System.out.println("Exception = " + e.toString());
                        AppConstants.upper_add = "default";
                        AppConstants.lower_add = "default";
                    }
                }
            }).start();
        } catch (Exception e) {
            System.out.println("Exception Found = " + e);
            AppConstants.upper_add = "default";
            AppConstants.lower_add = "default";
        }
        return;
    }

    public void getUpdatedMigitalBanners() {
        new Thread() {
            public void run() {
                try {
                    if (MainActivity.this.engineIO.checkAds5thDay()) {
                        System.out.println("fetching migital banners");
                        String response = MainActivity.this.netHandler.getDataFrmUrl(AppConstants.primaryBannerFetchingLink);
                        if (response == null || response.equals("")) {
                            response = MainActivity.this.netHandler.getDataFrmUrl(AppConstants.secondaryBannerFetchingLink);
                        }
                        if (response == null || response.equals("")) {
                            response = MainActivity.this.netHandler.getDataFrmUrl(AppConstants.thirdBannerFetchingLink);
                        }
                        if (response != null && !response.equals("") && response.indexOf("#") != -1) {
                            MainActivity.this.deletePreviousBanners();
                            String[] bannerLinks = response.split("#");
                            if (Integer.parseInt(bannerLinks[0]) > Integer.parseInt(MainActivity.this.config.getBANNER_VERSION())) {
                                MainActivity.this.config.setBANNER_VERSION(bannerLinks[0]);
                                MainActivity.this.config.setBANNER_COUNT(bannerLinks.length - 1);
                                for (int i = 1; i < bannerLinks.length; i++) {
                                    MainActivity.this.saveImage(MainActivity.this.netHandler.getImageArray(bannerLinks[i]), "banner" + i + ".png");
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public void deletePreviousBanners() {
        int bannerCount = this.config.getBANNER_COUNT();
        if (bannerCount != 0) {
            for (int i = bannerCount; i > 0; i--) {
                File f = getFileStreamPath("banner" + i + ".png");
                if (f != null && f.exists()) {
                    f.delete();
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void showInstallDialog(String msg) {
        AlertDialog prompt = new AlertDialog.Builder(this).create();
        prompt.setTitle("Note:");
        prompt.setMessage(msg);
        prompt.setButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dia, int arg1) {
                dia.dismiss();
                if (MainActivity.this.DownLoadResult.equals("DownLoad sucessfull")) {
                    MainActivity.this.finish();
                    MainActivity.this.engineIO.installUpdatedFile(MainActivity.this);
                } else if (MainActivity.this.DownLoadResult.equals("DownLoad unsucessfull")) {
                    MainActivity.this.startApp();
                }
            }
        });
        prompt.show();
    }
}
