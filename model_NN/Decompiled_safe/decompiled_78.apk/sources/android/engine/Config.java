package android.engine;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Locale;

public class Config extends SQLiteOpenHelper {
    private static String DB_NAME = "Config.db";
    public static boolean isAppRunning = false;
    public String ADD_ID;
    public String ADD_ID_KEY = "ADD_ID";
    public String APP_NAME;
    public String APP_NAME_KEY = "APP_NAME";
    public String BANNER_COUNT_KEY = "BANNER_COUNT";
    public String BANNER_VERSION_KEY = "BANNER_VERSION";
    public String BUILD_NUMBER;
    public String BUILD_NUMBER_KEY = "BUILD_NUMBER";
    public String CCODE;
    public String CCODE_KEY = "CCODE";
    public String CPButton_KEY = "CPButton";
    private String DB_PATH;
    public String DUC;
    public String DUC_KEY = "DUC";
    public String ENGINE_VERSION;
    public String ENGINE_VERSION_KEY = "ENGINE_VERSION";
    public String FTYPE;
    public String FTYPE_KEY = "FTYPE";
    public String IMEI;
    public String IMEI_KEY = "IMEI";
    public String IMSI;
    public String IMSI_KEY = "IMSI";
    public String LANGUAGE;
    public String LANGUAGE_KEY = "LANGUAGE";
    public String MODEL;
    public String MODEL_KEY = "MODEL";
    public String ONAME;
    public String ONAME_KEY = "ONAME";
    public String PID;
    public String PID_KEY = "PID";
    public String USER_AGENT;
    public String USER_AGENT_KEY = "USER_AGENT";
    public String UTYPE;
    public String UTYPE_KEY = "UTYPE";
    public String VERSION;
    public String VERSION_KEY = "VERSION";
    public String _ID_KEY = "_id";
    String configData;
    String configFileName = "Configxml.txt";
    String[] configValues;
    String createTable = ("create table " + this.tableName + "(_id integer primary key autoincrement," + this.APP_NAME_KEY + " text," + this.PID_KEY + " text," + this.DUC_KEY + " text," + this.VERSION_KEY + " text," + this.BUILD_NUMBER_KEY + " text," + this.FTYPE_KEY + " text," + this.UTYPE_KEY + " text," + this.IMEI_KEY + " text," + this.IMSI_KEY + " text," + this.LANGUAGE_KEY + " text," + this.ONAME_KEY + " text," + this.CCODE_KEY + " text," + this.MODEL_KEY + " text," + this.ADD_ID_KEY + " text," + this.USER_AGENT_KEY + " text," + this.ENGINE_VERSION_KEY + " text," + this.BANNER_VERSION_KEY + " text," + this.BANNER_COUNT_KEY + " text," + this.CPButton_KEY + " text);");
    String databaseName = "ConfigurationDB";
    int dbVersion = 1;
    EngineIO engineIO;
    Context mContext;
    private final Context myContext;
    private SQLiteDatabase myDB;
    String tableName = "ConfigTable";
    String tag = "Engine";
    TelephonyManager telephonyManager;
    XMLParser xmlParser;

    public void getConfigFileVersion() {
        try {
            String data = readConfigFrmAsset(this.configFileName);
            if (data != null) {
                this.xmlParser.parseXML(data);
                int newFileVersion = Integer.parseInt(this.xmlParser.getAppVersionNo());
                int newFileBuild = Integer.parseInt(this.xmlParser.getAppBuildNo());
                int newFileType = Integer.parseInt(this.xmlParser.getFileType());
                Log.v(this.tag, "fileVersion = " + newFileVersion);
                if (tableExists()) {
                    int oldFileVersion = Integer.parseInt(getVersion());
                    int oldFileBuild = Integer.parseInt(getBuildNumber());
                    int oldFileType = Integer.parseInt(getFTYPE());
                    Log.v(this.tag, "fileVersion = " + newFileVersion + " and dbVersion= " + this.dbVersion);
                    if (newFileVersion > oldFileVersion || newFileBuild > oldFileBuild || ((oldFileType == 1 && newFileType == 3) || (oldFileType == 2 && newFileType == 3))) {
                        Log.v(this.tag, "Updating config");
                        this.engineIO.deleteMasterResponseDetails();
                        this.engineIO.deleteUpgradedAppDetails();
                        this.engineIO.reCreateFile();
                        getDataFromXML("Updating Table");
                    }
                }
            }
        } catch (Exception e) {
            Log.v(this.tag, "exception occurd in getConfigversio = " + e);
        }
    }

    public boolean tableExists() {
        Cursor cursor = null;
        try {
            Cursor cursor2 = this.myDB.query(this.tableName, null, null, null, null, null, null);
            if (cursor2 == null) {
                cursor2.close();
                return false;
            } else if (cursor2.getCount() > 0) {
                cursor2.close();
                return true;
            } else {
                cursor2.close();
                return false;
            }
        } catch (Exception e) {
            if (cursor != null) {
                cursor.close();
            }
            return false;
        }
    }

    public String readConfigFrmAsset(String fileName) {
        Exception e;
        String data = null;
        try {
            InputStream inputStream = this.mContext.getAssets().open(fileName);
            byte[] buffer = new byte[inputStream.available()];
            do {
            } while (inputStream.read(buffer) != -1);
            String data2 = new String(buffer);
            try {
                return data2.trim();
            } catch (Exception e2) {
                e = e2;
                data = data2;
                e.printStackTrace();
                return data;
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return data;
        }
    }

    public long insertValues(String APP_NAME2, String PID2, String DUC2, String VERSION2, String BUILD_NUMBER2, String FTYPE2, String UTYPE2, String IMEI2, String IMSI2, String LANGUAGE2, String ONAME2, String CCODE2, String MODEL2, String ADD_ID2, String USER_AGENT2, String ENGINE_VERSION2, String BANNER_VERSION, String BANNER_COUNT, String CPButton) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(this.APP_NAME_KEY, APP_NAME2);
        contentValues.put(this.PID_KEY, PID2);
        contentValues.put(this.DUC_KEY, DUC2);
        contentValues.put(this.VERSION_KEY, VERSION2);
        contentValues.put(this.BUILD_NUMBER_KEY, BUILD_NUMBER2);
        contentValues.put(this.FTYPE_KEY, FTYPE2);
        contentValues.put(this.UTYPE_KEY, UTYPE2);
        contentValues.put(this.IMEI_KEY, IMEI2);
        contentValues.put(this.IMSI_KEY, IMSI2);
        contentValues.put(this.LANGUAGE_KEY, LANGUAGE2);
        contentValues.put(this.ONAME_KEY, ONAME2);
        contentValues.put(this.CCODE_KEY, CCODE2);
        contentValues.put(this.MODEL_KEY, MODEL2);
        contentValues.put(this.ADD_ID_KEY, ADD_ID2);
        contentValues.put(this.USER_AGENT_KEY, USER_AGENT2);
        contentValues.put(this.ENGINE_VERSION_KEY, ENGINE_VERSION2);
        contentValues.put(this.BANNER_VERSION_KEY, BANNER_VERSION);
        contentValues.put(this.BANNER_COUNT_KEY, BANNER_COUNT);
        contentValues.put(this.CPButton_KEY, CPButton);
        return this.myDB.insert(this.tableName, null, contentValues);
    }

    public void updateValues(String APP_NAME2, String PID2, String DUC2, String VERSION2, String BUILD_NUMBER2, String FTYPE2, String UTYPE2, String IMEI2, String IMSI2, String LANGUAGE2, String ONAME2, String CCODE2, String MODEL2, String ADD_ID2, String USER_AGENT2, String ENGINE_VERSION2, String BANNER_VERSION, String BANNER_COUNT, String CPButton) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(this.APP_NAME_KEY, APP_NAME2);
        contentValues.put(this.PID_KEY, PID2);
        contentValues.put(this.DUC_KEY, DUC2);
        contentValues.put(this.VERSION_KEY, VERSION2);
        contentValues.put(this.BUILD_NUMBER_KEY, BUILD_NUMBER2);
        contentValues.put(this.FTYPE_KEY, FTYPE2);
        contentValues.put(this.UTYPE_KEY, UTYPE2);
        contentValues.put(this.IMEI_KEY, IMEI2);
        contentValues.put(this.IMSI_KEY, IMSI2);
        contentValues.put(this.LANGUAGE_KEY, LANGUAGE2);
        contentValues.put(this.ONAME_KEY, ONAME2);
        contentValues.put(this.CCODE_KEY, CCODE2);
        contentValues.put(this.MODEL_KEY, MODEL2);
        contentValues.put(this.ADD_ID_KEY, ADD_ID2);
        contentValues.put(this.USER_AGENT_KEY, USER_AGENT2);
        contentValues.put(this.ENGINE_VERSION_KEY, ENGINE_VERSION2);
        contentValues.put(this.BANNER_VERSION_KEY, BANNER_VERSION);
        contentValues.put(this.BANNER_COUNT_KEY, BANNER_COUNT);
        contentValues.put(this.CPButton_KEY, CPButton);
        int update = this.myDB.update(this.tableName, contentValues, String.valueOf(this._ID_KEY) + "=1", null);
    }

    public void initAllVariables() {
        Cursor c = this.myDB.query(this.tableName, new String[]{this.APP_NAME_KEY, this.PID_KEY, this.DUC_KEY, this.VERSION_KEY, this.BUILD_NUMBER_KEY, this.FTYPE_KEY, this.UTYPE_KEY, this.IMEI_KEY, this.IMSI_KEY, this.LANGUAGE_KEY, this.ONAME_KEY, this.CCODE_KEY, this.MODEL_KEY, this.ADD_ID_KEY, this.USER_AGENT_KEY}, null, null, null, null, null);
        c.moveToFirst();
        this.APP_NAME = c.getString(0);
        this.PID = c.getString(1);
        this.DUC = c.getString(2);
        this.VERSION = c.getString(3);
        this.BUILD_NUMBER = c.getString(4);
        this.FTYPE = c.getString(5);
        this.UTYPE = c.getString(6);
        this.IMEI = c.getString(7);
        this.IMSI = c.getString(8);
        this.LANGUAGE = c.getString(9);
        this.ONAME = c.getString(10);
        this.CCODE = c.getString(11);
        this.MODEL = c.getString(12);
        this.ADD_ID = c.getString(13);
        this.USER_AGENT = c.getString(14);
        System.out.println("**********USER_AGENT   " + this.USER_AGENT);
    }

    public String getAppName() {
        String data = "";
        try {
            Cursor c = this.myDB.query(this.tableName, new String[]{this.APP_NAME_KEY}, null, null, null, null, null);
            if (c.moveToNext()) {
                data = c.getString(0);
            }
            c.close();
            if (!data.equals("")) {
                return data;
            }
            return "Minesweeper";
        } catch (Exception e) {
            return "Minesweeper";
        }
    }

    public String getPID() {
        String data = "";
        Cursor c = this.myDB.query(this.tableName, new String[]{this.PID_KEY}, null, null, null, null, null);
        if (c.moveToNext()) {
            data = c.getString(0);
        }
        c.close();
        if (!data.equals("")) {
            return data;
        }
        return "0";
    }

    public String getDUC() {
        String data = "";
        Cursor c = this.myDB.query(this.tableName, new String[]{this.DUC_KEY}, null, null, null, null, null);
        if (c.moveToNext()) {
            data = c.getString(0);
        }
        c.close();
        if (!data.equals("")) {
            return data;
        }
        return "0";
    }

    public String getVersion() {
        String data = "";
        Cursor c = this.myDB.query(this.tableName, new String[]{this.VERSION_KEY}, null, null, null, null, null);
        if (c.moveToNext()) {
            data = c.getString(0);
        }
        c.close();
        if (!data.equals("")) {
            return data;
        }
        return "0";
    }

    public String getBuildNumber() {
        String data = "";
        Cursor c = this.myDB.query(this.tableName, new String[]{this.BUILD_NUMBER_KEY}, null, null, null, null, null);
        if (c.moveToNext()) {
            data = c.getString(0);
        }
        c.close();
        if (!data.equals("")) {
            return data;
        }
        return "0";
    }

    public int setFTYPE(String data) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(this.FTYPE_KEY, data);
        return this.myDB.update(this.tableName, contentValues, String.valueOf(this._ID_KEY) + "=1", null);
    }

    public String getFTYPE() {
        String data = "";
        Cursor c = this.myDB.query(this.tableName, new String[]{this.FTYPE_KEY}, null, null, null, null, null);
        if (c.moveToNext()) {
            data = c.getString(0);
        }
        c.close();
        if (!data.equals("")) {
            return data;
        }
        return "0";
    }

    public String getUtype() {
        String data = "";
        Cursor c = this.myDB.query(this.tableName, new String[]{this.UTYPE_KEY}, null, null, null, null, null);
        if (c.moveToNext()) {
            data = c.getString(0);
        }
        c.close();
        if (!data.equals("")) {
            return data;
        }
        return "0";
    }

    public int setIMEI(String data) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(this.IMEI_KEY, data);
        return this.myDB.update(this.tableName, contentValues, String.valueOf(this._ID_KEY) + "=1", null);
    }

    public String getIMEI() {
        String data = "";
        Cursor c = this.myDB.query(this.tableName, new String[]{this.IMEI_KEY}, null, null, null, null, null);
        if (c.moveToNext()) {
            data = c.getString(0);
        }
        c.close();
        if (data == null || data.equals("")) {
            return "0";
        }
        return data;
    }

    public int setIMSI(String data) {
        System.out.println(" IMSI = " + data + " and _ID_KEY+=1 = " + this._ID_KEY + "=1");
        ContentValues contentValues = new ContentValues();
        contentValues.put(this.IMSI_KEY, data);
        return this.myDB.update(this.tableName, contentValues, String.valueOf(this._ID_KEY) + "=1", null);
    }

    public String getIMSI() {
        String data = "";
        Cursor c = this.myDB.query(this.tableName, new String[]{this.IMSI_KEY}, null, null, null, null, null);
        if (c.moveToNext()) {
            data = c.getString(0);
        }
        c.close();
        if (data == null || data.equals("")) {
            return "0";
        }
        return data;
    }

    public void setPhoneLanguage(String data) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(this.LANGUAGE_KEY, data);
        this.myDB.update(this.tableName, contentValues, String.valueOf(this._ID_KEY) + "=1", null);
    }

    public String getPhoneLanguage() {
        String data = "";
        Cursor c = this.myDB.query(this.tableName, new String[]{this.LANGUAGE_KEY}, null, null, null, null, null);
        if (c.moveToNext()) {
            data = c.getString(0);
        }
        c.close();
        if (data == null || data.equals("")) {
            return "0";
        }
        return data;
    }

    public int setONAME(String data) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(this.ONAME_KEY, data);
        return this.myDB.update(this.tableName, contentValues, String.valueOf(this._ID_KEY) + "=1", null);
    }

    public String getONAME() {
        String data = "";
        Cursor c = this.myDB.query(this.tableName, new String[]{this.ONAME_KEY}, null, null, null, null, null);
        if (c.moveToNext()) {
            data = c.getString(0);
        }
        c.close();
        if (data == null || data.equals("")) {
            return "0";
        }
        return data;
    }

    public void setCountryCode(String data) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(this.CCODE_KEY, data);
        this.myDB.update(this.tableName, contentValues, String.valueOf(this._ID_KEY) + "=1", null);
    }

    public String getCountryCode() {
        String data = "";
        Cursor c = this.myDB.query(this.tableName, new String[]{this.CCODE_KEY}, null, null, null, null, null);
        if (c.moveToNext()) {
            data = c.getString(0);
        }
        c.close();
        if (data == null || data.equals("")) {
            return "0";
        }
        return data;
    }

    public int setMODEL(String data) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(this.MODEL_KEY, data);
        return this.myDB.update(this.tableName, contentValues, String.valueOf(this._ID_KEY) + "=1", null);
    }

    public String getMODEL() {
        String data = "";
        Cursor c = this.myDB.query(this.tableName, new String[]{this.MODEL_KEY}, null, null, null, null, null);
        if (c.moveToNext()) {
            data = c.getString(0);
        }
        c.close();
        if (data == null || data.equals("")) {
            return "0";
        }
        return data;
    }

    public void setUSER_AGENT(String data) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(this.USER_AGENT_KEY, data);
        this.myDB.update(this.tableName, contentValues, String.valueOf(this._ID_KEY) + "=1", null);
    }

    public String getUserAgent() {
        String data = "";
        Cursor c = this.myDB.query(this.tableName, new String[]{this.USER_AGENT_KEY}, null, null, null, null, null);
        if (c.moveToNext()) {
            data = c.getString(0);
        }
        c.close();
        if (data == null || data.equals("")) {
            return "0";
        }
        return data;
    }

    public String getAdsID() {
        String data = "";
        Cursor c = this.myDB.query(this.tableName, new String[]{this.ADD_ID_KEY}, null, null, null, null, null);
        if (c.moveToNext()) {
            data = c.getString(0);
        }
        c.close();
        if (data == null || data.equals("")) {
            return "0";
        }
        return data;
    }

    public String getEngineVersion() {
        String data = "";
        Cursor c = this.myDB.query(this.tableName, new String[]{this.ENGINE_VERSION_KEY}, null, null, null, null, null);
        if (c.moveToNext()) {
            data = c.getString(0);
        }
        c.close();
        if (!data.equals("")) {
            return data;
        }
        return "0";
    }

    public int setBANNER_VERSION(String data) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(this.BANNER_VERSION_KEY, data);
        return this.myDB.update(this.tableName, contentValues, String.valueOf(this._ID_KEY) + "=1", null);
    }

    public String getBANNER_VERSION() {
        String data = "";
        Cursor c = this.myDB.query(this.tableName, new String[]{this.BANNER_VERSION_KEY}, null, null, null, null, null);
        if (c.moveToNext()) {
            data = c.getString(0);
        }
        c.close();
        if (data == null || data.equals("")) {
            return "0";
        }
        return data;
    }

    public int setBANNER_COUNT(int data) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(this.BANNER_COUNT_KEY, Integer.valueOf(data));
        return this.myDB.update(this.tableName, contentValues, String.valueOf(this._ID_KEY) + "=1", null);
    }

    public int getBANNER_COUNT() {
        String data = "";
        Cursor c = this.myDB.query(this.tableName, new String[]{this.BANNER_COUNT_KEY}, null, null, null, null, null);
        if (c.moveToNext()) {
            data = c.getString(0);
        }
        c.close();
        if (data == null || data.equals("")) {
            return 0;
        }
        return Integer.parseInt(data);
    }

    public int setCPButtonStatus(int data) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(this.CPButton_KEY, Integer.valueOf(data));
        return this.myDB.update(this.tableName, contentValues, String.valueOf(this._ID_KEY) + "=1", null);
    }

    public String getCPButtonStatus() {
        String data = "";
        Cursor c = this.myDB.query(this.tableName, new String[]{this.CPButton_KEY}, null, null, null, null, null);
        if (c.moveToNext()) {
            data = c.getString(0);
        }
        c.close();
        if (data == null || data.equals("")) {
            return "0";
        }
        return data;
    }

    public void deleteConfigTable() {
        this.myDB.execSQL("delete from " + this.tableName);
    }

    public String removeWhiteSpace(String s) {
        if (s == null) {
            return null;
        }
        char[] ch = s.toCharArray();
        StringBuffer strbfr = new StringBuffer();
        for (int i = 0; i < s.length(); i++) {
            if (ch[i] != ' ') {
                strbfr.append(ch[i]);
            } else {
                strbfr.append("%20");
            }
        }
        return strbfr.toString();
    }

    public void getDataFromXML(String status) {
        String CCODE2;
        String xmlConfig = readConfigFrmAsset(this.configFileName);
        if (xmlConfig != null) {
            this.xmlParser.parseXML(xmlConfig);
            String APP_NAME2 = this.xmlParser.getApplicationName();
            String PID2 = this.xmlParser.getOffset();
            String DUC2 = this.xmlParser.getDUC();
            String VERSION2 = this.xmlParser.getAppVersionNo();
            String BUILD_NUMBER2 = this.xmlParser.getAppBuildNo();
            String FTYPE2 = this.xmlParser.getFileType();
            String ADD_ID2 = this.xmlParser.getAdsID();
            String ENGINE_VERSION2 = this.xmlParser.getEngVerNo();
            this.engineIO.setFreeAppEnableStatus(this.xmlParser.getFreeAppStatus());
            this.engineIO.setBuyAppEnableStatus(this.xmlParser.getBuyAppStatus());
            this.engineIO.setIsAddEnable(this.xmlParser.getAdsStatus());
            String CPButton = this.xmlParser.getCPButtonStatus();
            String IMEI2 = this.telephonyManager.getDeviceId();
            String IMSI2 = this.telephonyManager.getSimSerialNumber();
            String LANGUAGE2 = Locale.getDefault().getDisplayLanguage();
            String ONAME2 = this.telephonyManager.getSimOperatorName();
            String simOperator = this.telephonyManager.getSimOperator();
            System.out.println("simOperator = " + simOperator + " and simOperator.length() = " + simOperator.length());
            if (simOperator == null || simOperator.length() < 3) {
                CCODE2 = "404";
            } else {
                CCODE2 = simOperator.substring(0, 3);
            }
            String MODEL2 = Build.MODEL;
            if (MODEL2 == null) {
                MODEL2 = "HTC";
            }
            if (status.equals("Creating Table")) {
                insertValues(APP_NAME2, PID2, DUC2, VERSION2, BUILD_NUMBER2, FTYPE2, "1", IMEI2, IMSI2, LANGUAGE2, ONAME2, CCODE2, MODEL2, ADD_ID2, "htc", ENGINE_VERSION2, "0", "0", CPButton);
            } else if (status.equals("Updating Table")) {
                updateValues(APP_NAME2, PID2, DUC2, VERSION2, BUILD_NUMBER2, FTYPE2, "1", IMEI2, IMSI2, LANGUAGE2, ONAME2, CCODE2, MODEL2, ADD_ID2, "htc", ENGINE_VERSION2, "0", "0", CPButton);
            }
        }
    }

    public void initialize2(Context context) {
        Log.v(this.tag, "initialize");
        this.mContext = context;
        isAppRunning = true;
        this.xmlParser = new XMLParser();
        this.engineIO = new EngineIO(this.mContext);
        this.telephonyManager = (TelephonyManager) this.mContext.getSystemService("phone");
        getConfigFileVersion();
        if (!tableExists() && readConfigFrmAsset("Configxml.txt") != null) {
            getDataFromXML("Creating Table");
        }
        if (tableExists()) {
            String SIMNO = this.telephonyManager.getSimSerialNumber();
            if ((SIMNO == null || SIMNO.equals("") || SIMNO.length() <= 0) && !getIMSI().equals("0")) {
                setIMSI("0");
            } else if (!getIMSI().equals(SIMNO)) {
                setIMSI(SIMNO);
            }
            String OP_NAME = this.telephonyManager.getSimOperatorName();
            if ((OP_NAME == null || OP_NAME.equals("") || OP_NAME.length() <= 0) && !getONAME().equals("0")) {
                setONAME("0");
            } else if (!getONAME().equals(OP_NAME)) {
                setONAME(OP_NAME);
            }
            String countryCode = this.telephonyManager.getSimOperator();
            if (countryCode != null && countryCode.length() > 3) {
                countryCode = countryCode.substring(0, 3);
            }
            if ((countryCode == null || countryCode.equals("") || countryCode.length() <= 0) && !getCountryCode().equals("000000")) {
                setCountryCode("00000");
            } else if (!getCountryCode().equals(countryCode.substring(0, 3))) {
                setCountryCode(countryCode.substring(0, 3));
            }
            String language = Locale.getDefault().getDisplayLanguage();
            if (language != null && language.length() > 0 && !getPhoneLanguage().equals(language)) {
                setPhoneLanguage(language);
            }
        }
        if (getFTYPE().equals("3") && !this.engineIO.getAuthorizationStatus()) {
            setFTYPE("1");
            this.engineIO.setIsAddEnable("1");
        }
        if (this.engineIO.getAuthStatus().equals("2")) {
            this.engineIO.setIsAddEnable("0");
            this.engineIO.setBuyAppEnableStatus("0");
            setFTYPE("3");
            this.engineIO.setAuthorizationStatus("1");
        }
        Log.v(this.tag, "fileVersion = " + getVersion());
    }

    public Config(Context context) {
        super(context, DB_NAME, (SQLiteDatabase.CursorFactory) null, 1);
        this.DB_PATH = String.valueOf(context.getFilesDir().getAbsolutePath()) + "/";
        this.myContext = context;
        try {
            createDataBase();
            openDataBase();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createDataBase() throws IOException {
        if (!checkDataBase()) {
            getReadableDatabase();
            try {
                copyDataBase();
            } catch (IOException e) {
                throw new Error("Error copying database");
            }
        }
    }

    private boolean checkDataBase() {
        SQLiteDatabase checkDB = null;
        try {
            checkDB = SQLiteDatabase.openDatabase(String.valueOf(this.DB_PATH) + DB_NAME, null, 0);
        } catch (SQLiteException e) {
        }
        if (checkDB != null) {
            checkDB.close();
        }
        return checkDB != null;
    }

    private void copyDataBase() throws IOException {
        InputStream myInput = this.myContext.getAssets().open(DB_NAME);
        OutputStream myOutput = new FileOutputStream(String.valueOf(this.DB_PATH) + DB_NAME);
        byte[] buffer = new byte[myInput.available()];
        while (true) {
            int length = myInput.read(buffer);
            if (length <= 0) {
                myOutput.flush();
                myOutput.close();
                myInput.close();
                return;
            }
            myOutput.write(buffer, 0, length);
        }
    }

    public void openDataBase() throws SQLException {
        this.myDB = SQLiteDatabase.openDatabase(String.valueOf(this.DB_PATH) + DB_NAME, null, 0);
    }

    public synchronized void close() {
        if (this.myDB != null) {
            this.myDB.close();
        }
        super.close();
    }

    public void onCreate(SQLiteDatabase db) {
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}
