package android.engine;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.ByteArrayBuffer;

public class NetHandler {
    Config config;
    DefaultHttpClient hc;
    Bitmap myBitmap;
    HttpPost postMethod;
    ResponseHandler<String> res;
    String stringres;

    public NetHandler(Context context) {
        this.config = new Config(context);
    }

    public String getDataFrmUrl(String url) {
        long currentTimeMillis = System.currentTimeMillis();
        try {
            System.out.println("mUrl to get data = " + url);
            HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
            conn.addRequestProperty("User-Agent", this.config.getUserAgent());
            conn.addRequestProperty("TempUA", this.config.getUserAgent());
            conn.setDoInput(true);
            conn.setConnectTimeout(60000);
            conn.connect();
            InputStream is = conn.getInputStream();
            byte[] buffer = new byte[is.available()];
            StringBuilder sb = new StringBuilder();
            while (true) {
                int n = is.read(buffer);
                if (n == -1) {
                    String response = sb.toString();
                    System.out.println("Response = " + response);
                    return response;
                }
                sb.append(new String(buffer, 0, n));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public InputStream getInputStream(String url) {
        System.out.println("url given to server = " + url);
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
            conn.setRequestProperty("User-Agent", this.config.getUserAgent());
            conn.setDoInput(true);
            conn.connect();
            InputStream is = conn.getInputStream();
            System.out.println("is = " + is);
            return is;
        } catch (Exception e) {
            System.out.println("Exception in getting input stream = ");
            e.printStackTrace();
            return null;
        }
    }

    public InputStream getMoreAppInputStream(String url) {
        System.out.println("url given to server = " + url);
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
            conn.setRequestProperty("User-Agent", this.config.getUserAgent());
            conn.setDoInput(true);
            conn.connect();
            InputStream is = conn.getInputStream();
            System.out.println("is = " + is);
            return is;
        } catch (Exception e) {
            System.out.println("Exception in getting input stream = ");
            e.printStackTrace();
            return null;
        }
    }

    public Bitmap getImageFromUrl(String imageURL) {
        Exception e;
        try {
            try {
                HttpURLConnection connection = (HttpURLConnection) new URL(imageURL).openConnection();
                connection.setRequestProperty("User-Agent", this.config.getUserAgent());
                connection.setDoInput(true);
                connection.connect();
                this.myBitmap = BitmapFactory.decodeStream(connection.getInputStream());
                System.out.println("Bitmap recieved from url = " + this.myBitmap);
                return this.myBitmap;
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                return null;
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return null;
        }
    }

    public byte[] getImageArray(String imageURL) {
        Exception e;
        System.out.println("downloading image from = " + imageURL);
        try {
            long startTime = System.currentTimeMillis();
            URL url = new URL(imageURL);
            try {
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestProperty("User-Agent", this.config.getUserAgent());
                connection.setDoInput(true);
                connection.connect();
                InputStream is = connection.getInputStream();
                BufferedInputStream bis = new BufferedInputStream(is);
                ByteArrayBuffer baf = new ByteArrayBuffer(is.available());
                while (true) {
                    int current = bis.read();
                    if (current == -1) {
                        System.out.println("Image downloading complete in " + ((System.currentTimeMillis() - startTime) / 1000) + " sec and buffer.length = " + baf.toByteArray().length);
                        URL url2 = url;
                        return baf.toByteArray();
                    }
                    baf.append((byte) current);
                }
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                return null;
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return null;
        }
    }

    public boolean DownloadFromUrl(String imageURL, String fileName) {
        try {
            URL url = new URL(imageURL);
            new File(fileName);
            long startTime = System.currentTimeMillis();
            Log.d("ImageManager", "download url:" + url);
            Log.d("ImageManager", "downloaded file name:" + fileName);
            URLConnection ucon = url.openConnection();
            ucon.setRequestProperty("User-Agent", this.config.getUserAgent());
            ucon.setConnectTimeout(60000);
            Log.d("ImageManager", "ucon = :" + ucon);
            InputStream is = ucon.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            ByteArrayBuffer byteArrayBuffer = new ByteArrayBuffer(is.available());
            Log.d("ImageManager", "baf = :" + byteArrayBuffer);
            while (true) {
                int current = bis.read();
                if (current == -1) {
                    File file = new File(Environment.getExternalStorageDirectory(), fileName);
                    file.createNewFile();
                    FileOutputStream fos = new FileOutputStream(file);
                    Log.d("ImageManager", "fos = :" + fos);
                    fos.write(byteArrayBuffer.toByteArray());
                    fos.flush();
                    fos.close();
                    Log.d("ImageManager", "download ready in" + ((System.currentTimeMillis() - startTime) / 1000) + " sec");
                    return true;
                }
                byteArrayBuffer.append((byte) current);
            }
        } catch (IOException e) {
            Log.d("ImageManager", "Error: " + e);
            return false;
        }
    }

    public String postFeedBackData(String url, String desc) {
        try {
            System.out.println("url = " + url);
            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters, 60000);
            HttpConnectionParams.setSoTimeout(httpParameters, 60000);
            DefaultHttpClient hc2 = new DefaultHttpClient(httpParameters);
            ResponseHandler<String> res2 = new BasicResponseHandler<>();
            HttpPost postMethod2 = new HttpPost(url);
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("Desc", desc));
            postMethod2.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            return (String) hc2.execute(postMethod2, res2);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String postBangoData(String User_Name, String CNO, String EDate, String CVV, String EmailId, String PhNo) {
        try {
            DefaultHttpClient hc2 = new DefaultHttpClient();
            ResponseHandler<String> res2 = new BasicResponseHandler<>();
            HttpPost postMethod2 = new HttpPost(AppConstants.primaryBangoBillingLink);
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("DUC", this.config.getDUC()));
            nameValuePairs.add(new BasicNameValuePair("Pid", this.config.getPID()));
            nameValuePairs.add(new BasicNameValuePair("Version", this.config.getVersion()));
            nameValuePairs.add(new BasicNameValuePair("Build", this.config.getBuildNumber()));
            nameValuePairs.add(new BasicNameValuePair("IMEI", this.config.getIMEI()));
            nameValuePairs.add(new BasicNameValuePair("IMSI", this.config.getIMSI()));
            nameValuePairs.add(new BasicNameValuePair("Ftype", this.config.getFTYPE()));
            nameValuePairs.add(new BasicNameValuePair("Model", this.config.getMODEL()));
            nameValuePairs.add(new BasicNameValuePair("Name", User_Name));
            nameValuePairs.add(new BasicNameValuePair("CCNO", CNO));
            nameValuePairs.add(new BasicNameValuePair("EDate", EDate));
            nameValuePairs.add(new BasicNameValuePair("CVV", CVV));
            nameValuePairs.add(new BasicNameValuePair("EmailId", EmailId));
            nameValuePairs.add(new BasicNameValuePair("PhNo", PhNo));
            System.out.println();
            postMethod2.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            String stringres2 = (String) hc2.execute(postMethod2, res2);
            System.out.println("stringres = " + stringres2);
            return stringres2;
        } catch (Exception e) {
            return null;
        }
    }
}
