package android.engine;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import com.mine.test_lite.R;

public class AboutUsMenu extends Activity {
    AddManager addManager;
    Button comp;
    private Dialog dialog;
    EngineIO engineIO;
    Button feedbackButton;
    public Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            AboutUsMenu.this.showInstallDialog("Download Complete ,Install Now ?");
        }
    };
    private int installationCode = 1;
    ProgressDialog myProgressDialog;
    NetHandler netHandler;
    Button prod;
    private ProgressDialog progressDialog;
    Button updateAppButton;
    Button upgradeAppButton;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.engine_about);
        this.engineIO = new EngineIO(this);
        this.netHandler = new NetHandler(this);
        this.addManager = new AddManager(this);
        this.comp = (Button) findViewById(R.id.comp);
        this.prod = (Button) findViewById(R.id.prod);
        this.updateAppButton = (Button) findViewById(R.id.update_app);
        this.upgradeAppButton = (Button) findViewById(R.id.upgrade_app);
        this.feedbackButton = (Button) findViewById(R.id.feedback);
        this.comp.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View arg0, MotionEvent me) {
                switch (me.getAction()) {
                    case 0:
                        AboutUsMenu.this.changeButtonImage();
                        AboutUsMenu.this.comp.setBackgroundResource(R.drawable.select);
                        return false;
                    case 1:
                        AboutUsMenu.this.changeButtonImage();
                        AboutUsMenu.this.startActivity(new Intent(AboutUsMenu.this.getApplicationContext(), AboutCompany.class));
                        return false;
                    default:
                        return false;
                }
            }
        });
        this.prod.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View arg0, MotionEvent me) {
                switch (me.getAction()) {
                    case 0:
                        AboutUsMenu.this.changeButtonImage();
                        AboutUsMenu.this.prod.setBackgroundResource(R.drawable.select);
                        return false;
                    case 1:
                        AboutUsMenu.this.changeButtonImage();
                        AboutUsMenu.this.startActivity(new Intent(AboutUsMenu.this.getApplicationContext(), AboutProduct.class));
                        return false;
                    default:
                        return false;
                }
            }
        });
        this.updateAppButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View arg0, MotionEvent me) {
                switch (me.getAction()) {
                    case 0:
                        AboutUsMenu.this.changeButtonImage();
                        AboutUsMenu.this.updateAppButton.setBackgroundResource(R.drawable.select);
                        return false;
                    case 1:
                        AboutUsMenu.this.changeButtonImage();
                        if (!AboutUsMenu.this.engineIO.getForceUpdateEnableStatus()) {
                            AboutUsMenu.this.hitMasterLink();
                        }
                        AboutUsMenu.this.engineIO.handleUpdateButton(AboutUsMenu.this);
                        return false;
                    default:
                        return false;
                }
            }
        });
        this.upgradeAppButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View arg0, MotionEvent me) {
                switch (me.getAction()) {
                    case 0:
                        AboutUsMenu.this.changeButtonImage();
                        AboutUsMenu.this.upgradeAppButton.setBackgroundResource(R.drawable.select);
                        return false;
                    case 1:
                        if (!AboutUsMenu.this.engineIO.getUpgradedVirsionAvlStatus()) {
                            AboutUsMenu.this.hitMasterLink();
                        }
                        AboutUsMenu.this.showUpgradedApps();
                        AboutUsMenu.this.changeButtonImage();
                        return false;
                    default:
                        return false;
                }
            }
        });
        this.feedbackButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View arg0, MotionEvent me) {
                switch (me.getAction()) {
                    case 0:
                        AboutUsMenu.this.changeButtonImage();
                        AboutUsMenu.this.feedbackButton.setBackgroundResource(R.drawable.select);
                        return false;
                    case 1:
                        AboutUsMenu.this.startActivity(new Intent(AboutUsMenu.this, FeedBackForm.class));
                        AboutUsMenu.this.changeButtonImage();
                        return false;
                    default:
                        return false;
                }
            }
        });
    }

    public void changeButtonImage() {
        this.comp.setBackgroundResource(R.drawable.unselect);
        this.prod.setBackgroundResource(R.drawable.unselect);
        this.updateAppButton.setBackgroundResource(R.drawable.unselect);
        this.upgradeAppButton.setBackgroundResource(R.drawable.unselect);
        this.feedbackButton.setBackgroundResource(R.drawable.unselect);
    }

    public void showUpgradedApps() {
        if (!this.engineIO.getUpgradedVirsionAvlStatus() || this.engineIO.getUpgradedAppCount() <= 0) {
            this.engineIO.showPrompt(this, "No upgraded version available now, please try later.");
            return;
        }
        Intent intent = new Intent(this, UpgradedApps.class);
        intent.putExtra("AppState", "Entry");
        startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void showInstallDialog(String msg) {
        AlertDialog prompt = new AlertDialog.Builder(this).create();
        prompt.setTitle("Note:");
        prompt.setMessage(msg);
        prompt.setButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dia, int arg1) {
                dia.dismiss();
                AboutUsMenu.this.installUpdatedFile();
            }
        });
        prompt.setButton2("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
            }
        });
        prompt.show();
    }

    public void installUpdatedFile() {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.setDataAndType(Uri.parse("file:///sdcard/Migital Apps/BPTracker.apk"), "application/vnd.android.package-archive");
        startActivityForResult(intent, this.installationCode);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode != this.installationCode) {
            return;
        }
        if (resultCode == -1) {
            this.engineIO.showPrompt(this, "Application Sucessfully installed");
        } else if (resultCode == 0) {
            this.engineIO.showPrompt(this, "Please uninstall current version to install updated version and install updated version from SD Card/" + AppConstants.updatedFileName);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (UpdateDialog.exitApp || UpdateDialog.installUpdate) {
            finish();
        }
        this.addManager.init(6);
    }

    public synchronized void hitMasterLink() {
        new Thread() {
            private String masterResponse;

            public void run() {
                super.run();
                try {
                    this.masterResponse = AboutUsMenu.this.netHandler.getDataFrmUrl(AppConstants.primaryMasterLink);
                    if (this.masterResponse == null || this.masterResponse.indexOf("#") == -1) {
                        System.out.println("fails first time and masterResponse = " + this.masterResponse);
                        this.masterResponse = AboutUsMenu.this.netHandler.getDataFrmUrl(AppConstants.secondaryMasterLink);
                        if (this.masterResponse == null || this.masterResponse.indexOf("#") == -1) {
                            System.out.println("fails second time time and masterResponse = " + this.masterResponse);
                            this.masterResponse = AboutUsMenu.this.netHandler.getDataFrmUrl(AppConstants.thirdMasterLink);
                            if (this.masterResponse != null && this.masterResponse.indexOf("#") != -1) {
                                AboutUsMenu.this.engineIO.saveMasterResponse(this.masterResponse);
                                return;
                            }
                            return;
                        }
                        AboutUsMenu.this.engineIO.saveMasterResponse(this.masterResponse);
                        return;
                    }
                    AboutUsMenu.this.engineIO.saveMasterResponse(this.masterResponse);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }
}
