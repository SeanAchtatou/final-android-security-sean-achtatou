package android.engine;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import com.mine.test_lite.R;
import java.util.Timer;
import java.util.TimerTask;

public class LogoDisplay extends Activity {
    int count = 0;
    Intent it;
    MyCount mc;
    Timer time;
    TextView timeDisplay;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        this.mc = new MyCount();
        this.time = new Timer();
        this.time.schedule(this.mc, 100, 1000);
        setContentView((int) R.layout.logopage);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (UpdateDialog.exitApp) {
            finish();
        } else if (UpdateDialog.installUpdate) {
            UpdateDialog.installUpdatedFile(this);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        System.out.println("SpleshScreen.onPause() = " + UpdateDialog.showingUpdateDialog);
        if (UpdateDialog.showingUpdateDialog) {
            finish();
        }
    }

    public void onBackPressed() {
        super.onBackPressed();
        this.time.cancel();
    }

    public void gotoNext() {
        if (this.count > 5) {
            this.time.cancel();
            this.it = new Intent(getApplicationContext(), SpleshScreen.class);
            startActivity(this.it);
            finish();
        }
    }

    public class MyCount extends TimerTask {
        public MyCount() {
        }

        public void run() {
            LogoDisplay.this.gotoNext();
            LogoDisplay.this.count++;
        }
    }
}
