package android.engine;

import android.app.Activity;
import android.os.Bundle;
import com.mine.test_lite.R;

public class AboutProduct extends Activity {
    AddManager addManager;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.engine_abt_prod);
        this.addManager = new AddManager(this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        AddManager.activityState = "Paused";
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        AddManager.activityState = "Resumed";
        if (UpdateDialog.exitApp || UpdateDialog.installUpdate) {
            finish();
        }
        this.addManager.init(8);
    }
}
