package android.engine;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import com.mine.test_lite.MainMenu;
import com.mine.test_lite.R;
import java.util.Timer;
import java.util.TimerTask;

public class SpleshScreen extends Activity {
    int count = 2;
    Handler han;
    Intent i2;
    Runnable textchange = new Runnable() {
        public void run() {
            if (SpleshScreen.this.count >= 5) {
                SpleshScreen.this.startActivity(new Intent(SpleshScreen.this, MainMenu.class));
                SpleshScreen.this.tim.cancel();
                SpleshScreen.this.finish();
            }
        }
    };
    Timer tim;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.spleshscreen);
        this.han = new Handler();
        this.tim = new Timer();
        this.tim.schedule(new MyTimer(), 0, 1000);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        System.out.println("SpleshScreen.onResume() = " + UpdateDialog.showingUpdateDialog);
        if (UpdateDialog.showingUpdateDialog) {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        System.out.println("SpleshScreen.onPause() = " + UpdateDialog.showingUpdateDialog);
        if (UpdateDialog.showingUpdateDialog) {
            finish();
        }
    }

    public void onBackPressed() {
        super.onBackPressed();
        this.tim.cancel();
    }

    public void setColorToText() {
        this.han.post(this.textchange);
    }

    class MyTimer extends TimerTask {
        MyTimer() {
        }

        public void run() {
            SpleshScreen.this.setColorToText();
            SpleshScreen.this.count++;
        }
    }
}
