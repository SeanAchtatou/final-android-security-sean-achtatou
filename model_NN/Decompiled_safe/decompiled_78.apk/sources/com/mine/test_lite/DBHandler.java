package com.mine.test_lite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DBHandler {
    private static final String CREATE_TABLE1 = "create table localScoreBigneer(_id integer primary key autoincrement, name text ,score integer);";
    private static final String CREATE_TABLE2 = "create table localScoreIntermediate(_id integer primary key autoincrement, name text ,score integer);";
    private static final String CREATE_TABLE3 = "create table localScoreExpert(_id integer primary key autoincrement, name text ,score integer);";
    private static final String DATABASE_NAME = "score";
    private static final String DATABASE_TABLE1 = "localScoreBigneer";
    private static final String DATABASE_TABLE2 = "localScoreIntermediate";
    private static final String DATABASE_TABLE3 = "localScoreExpert";
    private static final int DATABASE_VERSION = 1;
    String _Id_key = "_Id";
    Context ctx;
    Cursor cur;
    private SQLiteDatabase db = null;
    String name_key = "name";
    String score_key = DATABASE_NAME;
    long updated_row_id;

    public class Detail {
        public long _Id;
        public int name;
        public String score;

        public Detail() {
        }
    }

    public boolean dbExists() {
        try {
            this.db = this.ctx.openOrCreateDatabase(DATABASE_NAME, DATABASE_VERSION, null);
            if (this.db == null) {
                System.out.println("Database not exists");
                return false;
            }
            System.out.println("Database already exists");
            return DATABASE_VERSION;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void close() {
        this.db.close();
    }

    public DBHandler(Context ctx2) {
        this.ctx = ctx2;
        try {
            this.db = ctx2.openOrCreateDatabase(DATABASE_NAME, DATABASE_VERSION, null);
            this.db.execSQL(CREATE_TABLE1);
            this.db.execSQL(CREATE_TABLE2);
            this.db.execSQL(CREATE_TABLE3);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public long insertBigneerScore(String name, int score) {
        System.out.println("hello migital  ");
        ContentValues initialValues = new ContentValues();
        System.out.println("hello migital  11");
        initialValues.put(this.name_key, name);
        initialValues.put(this.score_key, Integer.valueOf(score));
        System.out.println("hello migital  wqrfserfsf");
        long id = this.db.insert(DATABASE_TABLE1, null, initialValues);
        System.out.println("in insert locations method in dbhandler class");
        return id;
    }

    public long insertIntermediateScore(String name, int score) {
        System.out.println("hello migital  ");
        ContentValues initialValues = new ContentValues();
        System.out.println("hello migital  11");
        initialValues.put(this.name_key, name);
        initialValues.put(this.score_key, Integer.valueOf(score));
        System.out.println("hello migital  wqrfserfsf");
        long id = this.db.insert(DATABASE_TABLE2, null, initialValues);
        System.out.println("in insert locations method in dbhandler class");
        return id;
    }

    public long insertExpertScore(String name, int score) {
        System.out.println("hello migital  ");
        ContentValues initialValues = new ContentValues();
        System.out.println("hello migital  11");
        initialValues.put(this.name_key, name);
        initialValues.put(this.score_key, Integer.valueOf(score));
        System.out.println("hello migital  wqrfserfsf");
        long id = this.db.insert(DATABASE_TABLE3, null, initialValues);
        System.out.println("in insert locations method in dbhandler class");
        return id;
    }

    public void deleteDetailBegineer(long rowId) {
        System.out.println("delete detail method called in data base for rowid  " + rowId);
        this.db.delete(DATABASE_TABLE1, "_id=" + rowId, null);
    }

    public void deleteDetailIntermidate(long rowId) {
        System.out.println("delete detail method called in data base for rowid  " + rowId);
        this.db.delete(DATABASE_TABLE2, "_id=" + rowId, null);
    }

    public void deleteDetailExpert(long rowId) {
        System.out.println("delete detail method called in data base for rowid  " + rowId);
        this.db.delete(DATABASE_TABLE3, "_id=" + rowId, null);
    }

    public void deleteAllDetailsBegineer() {
        this.db.execSQL("delete from localScoreBigneer");
    }

    public void deleteAllDetailsIntermediate() {
        this.db.execSQL("delete from localScoreIntermediate");
    }

    public void deleteAllDetailsExpert() {
        this.db.execSQL("delete from localScoreExpert");
    }

    public Cursor fetchAllScoreDetailsForBegineer() {
        try {
            SQLiteDatabase sQLiteDatabase = this.db;
            String[] strArr = new String[3];
            strArr[0] = this._Id_key;
            strArr[DATABASE_VERSION] = this.name_key;
            strArr[2] = this.score_key;
            this.cur = sQLiteDatabase.query(DATABASE_TABLE1, strArr, null, null, null, null, this.score_key);
        } catch (SQLException e) {
            Log.e("Exception on query", e.toString());
        }
        return this.cur;
    }

    public Cursor fetchAllScoreDetailsForIntermediate() {
        try {
            SQLiteDatabase sQLiteDatabase = this.db;
            String[] strArr = new String[3];
            strArr[0] = this._Id_key;
            strArr[DATABASE_VERSION] = this.name_key;
            strArr[2] = this.score_key;
            this.cur = sQLiteDatabase.query(DATABASE_TABLE2, strArr, null, null, null, null, this.score_key);
        } catch (SQLException e) {
            Log.e("Exception on query", e.toString());
        }
        return this.cur;
    }

    public Cursor fetchAllScoreDetailsForExpert() {
        try {
            SQLiteDatabase sQLiteDatabase = this.db;
            String[] strArr = new String[3];
            strArr[0] = this._Id_key;
            strArr[DATABASE_VERSION] = this.name_key;
            strArr[2] = this.score_key;
            this.cur = sQLiteDatabase.query(DATABASE_TABLE3, strArr, null, null, null, null, this.score_key);
        } catch (SQLException e) {
            Log.e("Exception on query", e.toString());
        }
        return this.cur;
    }

    public long updateScoreDetailForBegineer(long rowId, String name, int score) {
        ContentValues args = new ContentValues();
        args.put(this._Id_key, Long.valueOf(rowId));
        args.put(this.name_key, name);
        args.put(this.score_key, Integer.valueOf(score));
        long id = (long) this.db.update(DATABASE_TABLE1, args, "_id=" + rowId, null);
        System.out.println("data is updated rowId = " + rowId);
        return id;
    }

    public long updateScoreDetailForIntermediate(long rowId, String name, int score) {
        ContentValues args = new ContentValues();
        args.put(this._Id_key, Long.valueOf(rowId));
        args.put(this.name_key, name);
        args.put(this.score_key, Integer.valueOf(score));
        long id = (long) this.db.update(DATABASE_TABLE2, args, "_id=" + rowId, null);
        System.out.println("data is updated rowId = " + rowId);
        return id;
    }

    public long updateScoreDetailForExpert(long rowId, String name, int score) {
        ContentValues args = new ContentValues();
        args.put(this._Id_key, Long.valueOf(rowId));
        args.put(this.name_key, name);
        args.put(this.score_key, Integer.valueOf(score));
        long id = (long) this.db.update(DATABASE_TABLE3, args, "_id=" + rowId, null);
        System.out.println("data is updated rowId = " + rowId);
        return id;
    }
}
