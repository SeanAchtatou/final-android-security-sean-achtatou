package com.mine.test_lite;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;

public class MyDialog extends Dialog {
    public static Activity currentActivity;
    int code;
    Context context;

    public MyDialog(Context context2, int code2) {
        super(context2);
        this.context = context2;
        this.code = code2;
    }

    public void onBackPressed() {
        super.onBackPressed();
        System.out.println("on backe presed called of mydialog");
        finishActivity(this.code);
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public void finishActivity(int code2) {
        switch (code2) {
            case 1:
                currentActivity = (MineSweeper_Easy) this.context;
                break;
            case 2:
                currentActivity = (MineSweeper_Medium) this.context;
                break;
            case 3:
                currentActivity = (MineSweeper_Expert) this.context;
                break;
            case 4:
                currentActivity = (score) this.context;
                break;
        }
        currentActivity.finish();
    }
}
