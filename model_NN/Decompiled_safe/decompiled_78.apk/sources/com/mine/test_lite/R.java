package com.mine.test_lite;

public final class R {

    public static final class anim {
        public static final int push_left = 2130968576;
        public static final int push_right = 2130968577;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int Aqua = 2131034122;
        public static final int Azure = 2131034127;
        public static final int Beige = 2131034128;
        public static final int Black = 2131034120;
        public static final int BlanchedAlmond = 2131034129;
        public static final int BurlyWood = 2131034130;
        public static final int CadetBlue = 2131034131;
        public static final int CornflowerBlue = 2131034132;
        public static final int DarkGray = 2131034133;
        public static final int DarkGreen = 2131034121;
        public static final int DarkSeaGreen3 = 2131034149;
        public static final int GhostWhite = 2131034134;
        public static final int GoldenRod = 2131034135;
        public static final int HoneyDew = 2131034136;
        public static final int Ivory = 2131034137;
        public static final int Khaki = 2131034148;
        public static final int LavenderBlush = 2131034138;
        public static final int LemonChiffon4 = 2131034146;
        public static final int LightGoldenRodYellow = 2131034118;
        public static final int LightGreen = 2131034125;
        public static final int LightPink3 = 2131034147;
        public static final int LightYellow = 2131034119;
        public static final int MediumPurple = 2131034117;
        public static final int MintCream = 2131034139;
        public static final int MistyRose = 2131034140;
        public static final int PaleGreen = 2131034141;
        public static final int SeaShell = 2131034143;
        public static final int Silver = 2131034124;
        public static final int SkyBlue = 2131034142;
        public static final int SkyBlue3 = 2131034145;
        public static final int SlateGray = 2131034116;
        public static final int SteelBlue1 = 2131034144;
        public static final int SteelBlue3 = 2131034150;
        public static final int White = 2131034123;
        public static final int Yellow = 2131034158;
        public static final int YellowGreen = 2131034126;
        public static final int black = 2131034161;
        public static final int blue = 2131034153;
        public static final int blue_bg = 2131034155;
        public static final int cyan = 2131034160;
        public static final int deep_red = 2131034152;
        public static final int deep_white = 2131034151;
        public static final int font_color = 2131034154;
        public static final int orange = 2131034156;
        public static final int red2 = 2131034157;
        public static final int solid_blue = 2131034113;
        public static final int solid_green = 2131034114;
        public static final int solid_red = 2131034112;
        public static final int solid_yellow = 2131034115;
        public static final int white = 2131034159;
        /* added by JADX */

        /* renamed from: SlateGray   reason: not valid java name */
        public static final int f0SlateGray = 2131034116;
        /* added by JADX */

        /* renamed from: MediumPurple   reason: not valid java name */
        public static final int f1MediumPurple = 2131034117;
        /* added by JADX */

        /* renamed from: LightGoldenRodYellow   reason: not valid java name */
        public static final int f2LightGoldenRodYellow = 2131034118;
        /* added by JADX */

        /* renamed from: LightYellow   reason: not valid java name */
        public static final int f3LightYellow = 2131034119;
        /* added by JADX */

        /* renamed from: DarkGreen   reason: not valid java name */
        public static final int f4DarkGreen = 2131034121;
        /* added by JADX */

        /* renamed from: Aqua   reason: not valid java name */
        public static final int f5Aqua = 2131034122;
        /* added by JADX */

        /* renamed from: White   reason: not valid java name */
        public static final int f6White = 2131034123;
        /* added by JADX */

        /* renamed from: Silver   reason: not valid java name */
        public static final int f7Silver = 2131034124;
        /* added by JADX */

        /* renamed from: LightGreen   reason: not valid java name */
        public static final int f8LightGreen = 2131034125;
        /* added by JADX */

        /* renamed from: YellowGreen   reason: not valid java name */
        public static final int f9YellowGreen = 2131034126;
        /* added by JADX */

        /* renamed from: Azure   reason: not valid java name */
        public static final int f10Azure = 2131034127;
        /* added by JADX */

        /* renamed from: Beige   reason: not valid java name */
        public static final int f11Beige = 2131034128;
        /* added by JADX */

        /* renamed from: BlanchedAlmond   reason: not valid java name */
        public static final int f12BlanchedAlmond = 2131034129;
        /* added by JADX */

        /* renamed from: BurlyWood   reason: not valid java name */
        public static final int f13BurlyWood = 2131034130;
        /* added by JADX */

        /* renamed from: CadetBlue   reason: not valid java name */
        public static final int f14CadetBlue = 2131034131;
        /* added by JADX */

        /* renamed from: CornflowerBlue   reason: not valid java name */
        public static final int f15CornflowerBlue = 2131034132;
        /* added by JADX */

        /* renamed from: DarkGray   reason: not valid java name */
        public static final int f16DarkGray = 2131034133;
        /* added by JADX */

        /* renamed from: GhostWhite   reason: not valid java name */
        public static final int f17GhostWhite = 2131034134;
        /* added by JADX */

        /* renamed from: GoldenRod   reason: not valid java name */
        public static final int f18GoldenRod = 2131034135;
        /* added by JADX */

        /* renamed from: HoneyDew   reason: not valid java name */
        public static final int f19HoneyDew = 2131034136;
        /* added by JADX */

        /* renamed from: Ivory   reason: not valid java name */
        public static final int f20Ivory = 2131034137;
        /* added by JADX */

        /* renamed from: LavenderBlush   reason: not valid java name */
        public static final int f21LavenderBlush = 2131034138;
        /* added by JADX */

        /* renamed from: MintCream   reason: not valid java name */
        public static final int f22MintCream = 2131034139;
        /* added by JADX */

        /* renamed from: MistyRose   reason: not valid java name */
        public static final int f23MistyRose = 2131034140;
        /* added by JADX */

        /* renamed from: PaleGreen   reason: not valid java name */
        public static final int f24PaleGreen = 2131034141;
        /* added by JADX */

        /* renamed from: SkyBlue   reason: not valid java name */
        public static final int f25SkyBlue = 2131034142;
        /* added by JADX */

        /* renamed from: SeaShell   reason: not valid java name */
        public static final int f26SeaShell = 2131034143;
    }

    public static final class drawable {
        public static final int about_product = 2130837504;
        public static final int abt_comp = 2130837505;
        public static final int android_icon = 2130837506;
        public static final int bango = 2130837507;
        public static final int blank = 2130837508;
        public static final int blank1 = 2130837509;
        public static final int blue = 2130837571;
        public static final int border = 2130837510;
        public static final int button_1 = 2130837511;
        public static final int button_2 = 2130837512;
        public static final int button_3 = 2130837513;
        public static final int button_4 = 2130837514;
        public static final int button_5 = 2130837515;
        public static final int button_6 = 2130837516;
        public static final int button_7 = 2130837517;
        public static final int button_8 = 2130837518;
        public static final int button_bg = 2130837519;
        public static final int facebook = 2130837520;
        public static final int facebook_icon = 2130837521;
        public static final int first_set = 2130837522;
        public static final int flag = 2130837523;
        public static final int flag_select = 2130837524;
        public static final int flag_unselect = 2130837525;
        public static final int footer = 2130837526;
        public static final int green = 2130837572;
        public static final int image = 2130837527;
        public static final int left = 2130837528;
        public static final int lock = 2130837529;
        public static final int migital = 2130837530;
        public static final int mine = 2130837531;
        public static final int mine_sweeper = 2130837532;
        public static final int mine_sweeper_icon = 2130837533;
        public static final int mine_sweeper_small_icon = 2130837534;
        public static final int more_app_background = 2130837535;
        public static final int more_apps = 2130837536;
        public static final int no_bg_mine = 2130837537;
        public static final int plain = 2130837538;
        public static final int play_image = 2130837539;
        public static final int ques = 2130837540;
        public static final int ques_select = 2130837541;
        public static final int ques_unselect = 2130837542;
        public static final int red = 2130837570;
        public static final int right = 2130837543;
        public static final int screen_background_black = 2130837574;
        public static final int scroll1 = 2130837544;
        public static final int scroll2 = 2130837545;
        public static final int scroll_thumb = 2130837546;
        public static final int scroll_track = 2130837547;
        public static final int secbg = 2130837548;
        public static final int select = 2130837549;
        public static final int smile_badguess = 2130837550;
        public static final int smile_happy = 2130837551;
        public static final int smile_normal = 2130837552;
        public static final int smile_oopss = 2130837553;
        public static final int star0 = 2130837554;
        public static final int star1 = 2130837555;
        public static final int star2 = 2130837556;
        public static final int star3 = 2130837557;
        public static final int star4 = 2130837558;
        public static final int star5 = 2130837559;
        public static final int startlogo = 2130837560;
        public static final int staticadd1 = 2130837561;
        public static final int staticadd2 = 2130837562;
        public static final int timer_image = 2130837563;
        public static final int translucent_background = 2130837575;
        public static final int transp = 2130837564;
        public static final int transparent_background = 2130837576;
        public static final int unselect = 2130837565;
        public static final int visa = 2130837566;
        public static final int watch = 2130837567;
        public static final int yellow = 2130837573;
        public static final int zoom_in = 2130837568;
        public static final int zoom_out = 2130837569;
    }

    public static final class id {
        public static final int Button01 = 2131230841;
        public static final int Button02 = 2131230842;
        public static final int Button03 = 2131230843;
        public static final int Button_about_us = 2131230875;
        public static final int Button_ads = 2131230824;
        public static final int Button_buy_app = 2131230870;
        public static final int Button_buy_upgraded_app = 2131230835;
        public static final int Button_cancel = 2131230814;
        public static final int Button_cancel_feedback = 2131230818;
        public static final int Button_cancel_login = 2131230822;
        public static final int Button_cancel_upgraded_app = 2131230831;
        public static final int Button_config = 2131230823;
        public static final int Button_easy = 2131230879;
        public static final int Button_hard = 2131230881;
        public static final int Button_help = 2131230876;
        public static final int Button_hide = 2131230867;
        public static final int Button_login = 2131230821;
        public static final int Button_loss_play_again = 2131230877;
        public static final int Button_loss_quit = 2131230878;
        public static final int Button_medium = 2131230880;
        public static final int Button_more_apps = 2131230874;
        public static final int Button_save = 2131230883;
        public static final int Button_score = 2131230872;
        public static final int Button_score_easy = 2131230851;
        public static final int Button_score_expert = 2131230853;
        public static final int Button_score_medium = 2131230852;
        public static final int Button_settings = 2131230873;
        public static final int Button_start = 2131230871;
        public static final int Button_submit = 2131230813;
        public static final int Button_submit_feedback = 2131230817;
        public static final int Button_win_play_again = 2131230981;
        public static final int Button_win_quit = 2131230982;
        public static final int Button_win_submitt_score = 2131230983;
        public static final int Button_zoom_in = 2131230862;
        public static final int CheckBox_time_limit = 2131230889;
        public static final int CheckBox_vibration = 2131230884;
        public static final int EditText01 = 2131230897;
        public static final int EditText_card_name = 2131230791;
        public static final int EditText_card_no = 2131230794;
        public static final int EditText_cvv_code = 2131230802;
        public static final int EditText_email = 2131230806;
        public static final int EditText_exp_month = 2131230798;
        public static final int EditText_exp_year = 2131230799;
        public static final int EditText_feedback_email = 2131230815;
        public static final int EditText_feedback_text = 2131230816;
        public static final int EditText_pass = 2131230820;
        public static final int EditText_phone = 2131230809;
        public static final int EnterName = 2131230896;
        public static final int ExpandableListView_upgraded_apps = 2131230830;
        public static final int FacebookShareButton = 2131230844;
        public static final int FacebookShareNotButton = 2131230845;
        public static final int HorizontalScrollView01 = 2131230728;
        public static final int ImageButton_flag = 2131230864;
        public static final int ImageButton_ques = 2131230865;
        public static final int ImageButton_smily_types = 2131230861;
        public static final int ImageView01 = 2131230856;
        public static final int ImageView01_bango_title = 2131230781;
        public static final int ImageView1 = 2131230731;
        public static final int ImageView10 = 2131230758;
        public static final int ImageView2 = 2131230734;
        public static final int ImageView3 = 2131230737;
        public static final int ImageView4 = 2131230740;
        public static final int ImageView5 = 2131230743;
        public static final int ImageView6 = 2131230746;
        public static final int ImageView7 = 2131230749;
        public static final int ImageView8 = 2131230752;
        public static final int ImageView9 = 2131230755;
        public static final int ImageView_app_icon = 2131230776;
        public static final int ImageView_enter_card_details = 2131230788;
        public static final int ImageView_left = 2131230868;
        public static final int ImageView_lock = 2131230783;
        public static final int ImageView_lower = 2131230765;
        public static final int ImageView_right = 2131230869;
        public static final int ImageView_upper = 2131230762;
        public static final int ImageView_visa_cards = 2131230786;
        public static final int LinearLayout01 = 2131230720;
        public static final int LinearLayout02 = 2131230769;
        public static final int LinearLayout03 = 2131230775;
        public static final int LinearLayout04 = 2131230777;
        public static final int LinearLayout05 = 2131230782;
        public static final int LinearLayout06 = 2131230797;
        public static final int LinearLayout07 = 2131230863;
        public static final int LinearLayout08 = 2131230888;
        public static final int LinearLayout1 = 2131230730;
        public static final int LinearLayout10 = 2131230757;
        public static final int LinearLayout12 = 2131230882;
        public static final int LinearLayout2 = 2131230733;
        public static final int LinearLayout3 = 2131230736;
        public static final int LinearLayout4 = 2131230739;
        public static final int LinearLayout5 = 2131230742;
        public static final int LinearLayout6 = 2131230745;
        public static final int LinearLayout7 = 2131230748;
        public static final int LinearLayout8 = 2131230751;
        public static final int LinearLayout9 = 2131230754;
        public static final int LinearLayout_app = 2131230767;
        public static final int LinearLayout_bottom = 2131230723;
        public static final int LinearLayout_container = 2131230729;
        public static final int LinearLayout_cp = 2131230722;
        public static final int LinearLayout_lower = 2131230764;
        public static final int LinearLayout_more_app = 2131230724;
        public static final int LinearLayout_top = 2131230760;
        public static final int LinearLayout_upper = 2131230761;
        public static final int MineCount = 2131230859;
        public static final int R0Name = 2131230905;
        public static final int R0Rank = 2131230904;
        public static final int R0Score = 2131230906;
        public static final int R10Name = 2131230938;
        public static final int R10Rank = 2131230937;
        public static final int R10Score = 2131230939;
        public static final int R11Name = 2131230942;
        public static final int R11Rank = 2131230941;
        public static final int R11Score = 2131230943;
        public static final int R12Name = 2131230946;
        public static final int R12Rank = 2131230945;
        public static final int R12Score = 2131230947;
        public static final int R13Name = 2131230950;
        public static final int R13Rank = 2131230949;
        public static final int R13Score = 2131230951;
        public static final int R14Name = 2131230954;
        public static final int R14Rank = 2131230953;
        public static final int R14Score = 2131230955;
        public static final int R15Name = 2131230958;
        public static final int R15Rank = 2131230957;
        public static final int R15Score = 2131230959;
        public static final int R16Name = 2131230962;
        public static final int R16Rank = 2131230961;
        public static final int R16Score = 2131230963;
        public static final int R17Name = 2131230966;
        public static final int R17Rank = 2131230965;
        public static final int R17Score = 2131230967;
        public static final int R18Name = 2131230970;
        public static final int R18Rank = 2131230969;
        public static final int R18Score = 2131230971;
        public static final int R19Name = 2131230974;
        public static final int R19Rank = 2131230973;
        public static final int R19Score = 2131230975;
        public static final int R1Name = 2131230908;
        public static final int R1Rank = 2131230907;
        public static final int R1Score = 2131230909;
        public static final int R20Name = 2131230978;
        public static final int R20Rank = 2131230977;
        public static final int R20Score = 2131230979;
        public static final int R2Name = 2131230911;
        public static final int R2Rank = 2131230910;
        public static final int R2Score = 2131230912;
        public static final int R3Name = 2131230914;
        public static final int R3Rank = 2131230913;
        public static final int R3Score = 2131230915;
        public static final int R4Name = 2131230917;
        public static final int R4Rank = 2131230916;
        public static final int R4Score = 2131230918;
        public static final int R5Name = 2131230920;
        public static final int R5Rank = 2131230919;
        public static final int R5Score = 2131230921;
        public static final int R6Name = 2131230923;
        public static final int R6Rank = 2131230922;
        public static final int R6Score = 2131230924;
        public static final int R7Name = 2131230926;
        public static final int R7Rank = 2131230925;
        public static final int R7Score = 2131230927;
        public static final int R8Name = 2131230930;
        public static final int R8Rank = 2131230929;
        public static final int R8Score = 2131230931;
        public static final int R9Name = 2131230934;
        public static final int R9Rank = 2131230933;
        public static final int R9Score = 2131230935;
        public static final int RadioButton_10min = 2131230894;
        public static final int RadioButton_2min = 2131230892;
        public static final int RadioButton_5min = 2131230893;
        public static final int RadioButton_off = 2131230891;
        public static final int RadioButton_vibr_off = 2131230887;
        public static final int RadioButton_vibr_on = 2131230886;
        public static final int RadioGroup_time_limit = 2131230890;
        public static final int RadioGroup_vibration_on_off = 2131230885;
        public static final int RelativeLayout01 = 2131230721;
        public static final int RelativeLayout02 = 2131230725;
        public static final int RelativeLayout03 = 2131230860;
        public static final int RelativeLayout04 = 2131230866;
        public static final int ScrollView01 = 2131230768;
        public static final int Smiley = 2131230858;
        public static final int TableLayout01 = 2131230780;
        public static final int TableLayout02 = 2131230855;
        public static final int TableRow00 = 2131230903;
        public static final int TableRow01 = 2131230789;
        public static final int TableRow02 = 2131230792;
        public static final int TableRow03 = 2131230795;
        public static final int TableRow04 = 2131230800;
        public static final int TableRow05 = 2131230804;
        public static final int TableRow06 = 2131230807;
        public static final int TableRow07 = 2131230810;
        public static final int TableRow08 = 2131230928;
        public static final int TableRow09 = 2131230932;
        public static final int TableRow10 = 2131230936;
        public static final int TableRow11 = 2131230940;
        public static final int TableRow12 = 2131230944;
        public static final int TableRow13 = 2131230948;
        public static final int TableRow14 = 2131230952;
        public static final int TableRow15 = 2131230956;
        public static final int TableRow16 = 2131230960;
        public static final int TableRow17 = 2131230964;
        public static final int TableRow18 = 2131230968;
        public static final int TableRow19 = 2131230972;
        public static final int TableRow20 = 2131230976;
        public static final int TextView = 2131230819;
        public static final int TextView01 = 2131230812;
        public static final int TextView02 = 2131230803;
        public static final int TextView03 = 2131230811;
        public static final int TextView04 = 2131230849;
        public static final int TextView05 = 2131230850;
        public static final int TextView1 = 2131230732;
        public static final int TextView10 = 2131230759;
        public static final int TextView2 = 2131230735;
        public static final int TextView3 = 2131230738;
        public static final int TextView4 = 2131230741;
        public static final int TextView5 = 2131230744;
        public static final int TextView6 = 2131230747;
        public static final int TextView7 = 2131230750;
        public static final int TextView8 = 2131230753;
        public static final int TextView9 = 2131230756;
        public static final int TextView_app_desc = 2131230834;
        public static final int TextView_app_name = 2131230833;
        public static final int TextView_app_title = 2131230778;
        public static final int TextView_billing = 2131230787;
        public static final int TextView_card_name = 2131230790;
        public static final int TextView_card_no = 2131230793;
        public static final int TextView_cvv_code = 2131230801;
        public static final int TextView_desc = 2131230827;
        public static final int TextView_download_more_apps = 2131230726;
        public static final int TextView_email = 2131230805;
        public static final int TextView_expiry = 2131230796;
        public static final int TextView_game_note = 2131230895;
        public static final int TextView_help1 = 2131230846;
        public static final int TextView_help2 = 2131230847;
        public static final int TextView_help3 = 2131230848;
        public static final int TextView_lower = 2131230766;
        public static final int TextView_more_app = 2131230727;
        public static final int TextView_phone = 2131230808;
        public static final int TextView_price = 2131230779;
        public static final int TextView_rate = 2131230828;
        public static final int TextView_secure_page = 2131230784;
        public static final int TextView_thanks = 2131230785;
        public static final int TextView_upgraded_app_ldesc = 2131230838;
        public static final int TextView_upgraded_app_name = 2131230826;
        public static final int TextView_upgraded_app_sdesc = 2131230837;
        public static final int TextView_upgraded_app_title = 2131230829;
        public static final int TextView_upper = 2131230763;
        public static final int TimerTextView = 2131230857;
        public static final int View_app_name = 2131230832;
        public static final int both = 2131230901;
        public static final int comp = 2131230770;
        public static final int facebook = 2131230902;
        public static final int feedback = 2131230774;
        public static final int global = 2131230899;
        public static final int localdata = 2131230900;
        public static final int logopage = 2131230854;
        public static final int prod = 2131230771;
        public static final int reset = 2131230980;
        public static final int submitscore = 2131230898;
        public static final int update_app = 2131230772;
        public static final int upgrade_app = 2131230773;
        public static final int web_view_icon = 2131230825;
        public static final int webview_icon = 2131230836;
        public static final int webview_ss1 = 2131230839;
        public static final int webview_ss2 = 2131230840;
    }

    public static final class layout {
        public static final int engine_about = 2130903040;
        public static final int engine_abt_comp = 2130903041;
        public static final int engine_abt_prod = 2130903042;
        public static final int engine_abt_prod2 = 2130903043;
        public static final int engine_bango2 = 2130903044;
        public static final int engine_feedback_dialog = 2130903045;
        public static final int engine_passwordialog = 2130903046;
        public static final int engine_test1 = 2130903047;
        public static final int engine_upgraded_app_item = 2130903048;
        public static final int engine_upgraded_apps = 2130903049;
        public static final int engine_upgraded_file = 2130903050;
        public static final int engine_upgraded_file2 = 2130903051;
        public static final int exit_buttons = 2130903052;
        public static final int facebook_dialog = 2130903053;
        public static final int help = 2130903054;
        public static final int level_for_score_dialog = 2130903055;
        public static final int logopage = 2130903056;
        public static final int main = 2130903057;
        public static final int main_easy = 2130903058;
        public static final int main_expert = 2130903059;
        public static final int main_medium = 2130903060;
        public static final int main_page = 2130903061;
        public static final int open_mine_dialog = 2130903062;
        public static final int option = 2130903063;
        public static final int setting = 2130903064;
        public static final int show_exit_dialog = 2130903065;
        public static final int spleshscreen = 2130903066;
        public static final int submit = 2130903067;
        public static final int tabs = 2130903068;
        public static final int win_dialog = 2130903069;
    }

    public static final class string {
        public static final int app_name = 2131099649;
        public static final int hello = 2131099648;
    }

    public static final class style {
        public static final int Transparent = 2131165184;
    }
}
