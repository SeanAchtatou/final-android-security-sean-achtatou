package com.mine.test_lite;

import android.content.Context;
import android.widget.Button;
import java.util.Vector;

public class Block_Easy extends Button {
    MineSweeper_Easy minesweeper = new MineSweeper_Easy();

    public Block_Easy(Context context) {
        super(context);
    }

    public boolean hasMine() {
        boolean minesIsThere = false;
        int size = MineSweeper_Easy.myMinesList.size();
        if (size == 0) {
            minesIsThere = false;
        } else {
            for (int i = 0; i < size; i++) {
                if (MineSweeper_Easy.myMinesList.get(i).equals(this)) {
                    return true;
                }
            }
        }
        return minesIsThere;
    }

    public boolean isClicked() {
        boolean buttonClicked = false;
        int size = MineSweeper_Easy.myClickedButtonList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Easy.myClickedButtonList.get(i).equals(this)) {
                return true;
            }
            buttonClicked = false;
        }
        return buttonClicked;
    }

    public boolean isLongClicked() {
        boolean buttonLongClicked = false;
        int size = MineSweeper_Easy.myLongClickedList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Easy.myLongClickedList.get(i).equals(this)) {
                return true;
            }
            buttonLongClicked = false;
        }
        return buttonLongClicked;
    }

    public boolean isBlank() {
        boolean buttonBlabk = false;
        int size = MineSweeper_Easy.myBlankList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Easy.myBlankList.get(i).equals(this)) {
                return true;
            }
            buttonBlabk = false;
        }
        return buttonBlabk;
    }

    public boolean hasAnyNumber() {
        boolean buttonNumber = false;
        int size = MineSweeper_Easy.myAllNumberList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Easy.myAllNumberList.get(i).equals(this)) {
                return true;
            }
            buttonNumber = false;
        }
        return buttonNumber;
    }

    public boolean hasOne() {
        boolean buttonOne = false;
        int size = MineSweeper_Easy.myOneNumberList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Easy.myOneNumberList.get(i).equals(this)) {
                return true;
            }
            buttonOne = false;
        }
        return buttonOne;
    }

    public boolean hasTwo() {
        boolean buttonTwo = false;
        int size = MineSweeper_Easy.myTwoNumberList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Easy.myTwoNumberList.get(i).equals(this)) {
                return true;
            }
            buttonTwo = false;
        }
        return buttonTwo;
    }

    public boolean hasThree() {
        boolean buttonThree = false;
        int size = MineSweeper_Easy.myThreeNumberList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Easy.myThreeNumberList.get(i).equals(this)) {
                return true;
            }
            buttonThree = false;
        }
        return buttonThree;
    }

    public boolean hasFour() {
        boolean buttonFour = false;
        int size = MineSweeper_Easy.myFourNumberList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Easy.myFourNumberList.get(i).equals(this)) {
                return true;
            }
            buttonFour = false;
        }
        return buttonFour;
    }

    public boolean hasFive() {
        boolean buttonFive = false;
        int size = MineSweeper_Easy.myFiveNumberList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Easy.myFiveNumberList.get(i).equals(this)) {
                return true;
            }
            buttonFive = false;
        }
        return buttonFive;
    }

    public boolean hasSix() {
        boolean buttonSix = false;
        int size = MineSweeper_Easy.mySixNumberList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Easy.mySixNumberList.get(i).equals(this)) {
                return true;
            }
            buttonSix = false;
        }
        return buttonSix;
    }

    public boolean hasSeven() {
        boolean buttonSeven = false;
        int size = MineSweeper_Easy.mySevenNumberList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Easy.mySevenNumberList.get(i).equals(this)) {
                return true;
            }
            buttonSeven = false;
        }
        return buttonSeven;
    }

    public boolean hasEight() {
        boolean buttonEight = false;
        int size = MineSweeper_Easy.myEightNumberList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Easy.myEightNumberList.get(i).equals(this)) {
                return true;
            }
            buttonEight = false;
        }
        return buttonEight;
    }

    public boolean isFlagged() {
        return true;
    }

    public boolean isCovered() {
        return true;
    }

    public boolean isQuestionMarked() {
        return true;
    }

    public void setBlockAsDisabled(boolean disable_block) {
    }

    public void setQuestionMarked(boolean set_ques_mark) {
    }

    public int getNumberOfMinesInSorrounding() {
        return 10;
    }

    public void setFlagIcon(boolean set_flag_icon) {
    }

    public void setQuestionMarkIcon(boolean set_ques_mark_icon) {
    }

    public void setFlagged(boolean set_flag) {
    }

    public void clearAllIcons() {
    }

    public void OpenBlock() {
    }

    public void plantMine() {
        new Vector<>().add(this);
        System.out.println("b in plant mine ");
    }
}
