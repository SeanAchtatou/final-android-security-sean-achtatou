package com.mine.test_lite;

import android.app.Activity;
import android.content.Intent;
import android.engine.AboutUsMenu;
import android.engine.AddManager;
import android.engine.AppConstants;
import android.engine.Config;
import android.engine.EngineIO;
import android.engine.NetHandler;
import android.engine.UpdateDialog;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ScrollView;

public class MainMenu extends Activity {
    public static boolean mainMenuStarted;
    Button about_us;
    AddManager addManager;
    Button buy_app;
    EngineIO engineIO;
    private Drawable freeAppButtonImg;
    GameLevelOptions game_level;
    String game_level_type;
    Button help;
    Button more_apps;
    private NetHandler netHandler;
    Button score;
    Button settings;
    Button start;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.main_page);
        this.addManager = new AddManager(this);
        this.addManager.displayAdds();
        this.buy_app = (Button) findViewById(R.id.Button_buy_app);
        this.start = (Button) findViewById(R.id.Button_start);
        this.score = (Button) findViewById(R.id.Button_score);
        this.settings = (Button) findViewById(R.id.Button_settings);
        this.more_apps = (Button) findViewById(R.id.Button_more_apps);
        this.about_us = (Button) findViewById(R.id.Button_about_us);
        this.help = (Button) findViewById(R.id.Button_help);
        doEngineWork();
        setScrollViewHeight();
        this.game_level = new GameLevelOptions(this);
        System.out.println("game_level_type = " + this.game_level_type);
        try {
            this.buy_app.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    MainMenu.this.engineIO.handleBuyApp(MainMenu.this);
                }
            });
            this.start.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    MainMenu.this.startActivity(new Intent(MainMenu.this.getApplicationContext(), Options.class));
                }
            });
            this.score.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    MainMenu.this.startActivity(new Intent(MainMenu.this.getApplicationContext(), score.class));
                }
            });
            this.settings.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    MainMenu.this.startActivity(new Intent(MainMenu.this.getApplicationContext(), Settings.class));
                }
            });
            this.more_apps.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    MainMenu.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(AppConstants.free_apps_url)));
                }
            });
            this.help.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    MainMenu.this.startActivity(new Intent(MainMenu.this.getApplicationContext(), Help.class));
                }
            });
            this.about_us.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    MainMenu.this.startActivity(new Intent(MainMenu.this.getApplicationContext(), AboutUsMenu.class));
                }
            });
            this.buy_app.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View arg0, MotionEvent me) {
                    switch (me.getAction()) {
                        case 0:
                            MainMenu.this.setButtonBackGround(1);
                            return false;
                        case 1:
                            MainMenu.this.refreshButtons();
                            return false;
                        default:
                            return false;
                    }
                }
            });
            this.start.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View arg0, MotionEvent me) {
                    switch (me.getAction()) {
                        case 0:
                            MainMenu.this.setButtonBackGround(2);
                            return false;
                        case 1:
                            MainMenu.this.refreshButtons();
                            return false;
                        default:
                            return false;
                    }
                }
            });
            this.score.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View arg0, MotionEvent me) {
                    switch (me.getAction()) {
                        case 0:
                            MainMenu.this.setButtonBackGround(3);
                            return false;
                        case 1:
                            MainMenu.this.refreshButtons();
                            return false;
                        default:
                            return false;
                    }
                }
            });
            this.settings.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View arg0, MotionEvent me) {
                    switch (me.getAction()) {
                        case 0:
                            MainMenu.this.setButtonBackGround(4);
                            return false;
                        case 1:
                            MainMenu.this.refreshButtons();
                            return false;
                        default:
                            return false;
                    }
                }
            });
            this.more_apps.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View arg0, MotionEvent me) {
                    switch (me.getAction()) {
                        case 0:
                            MainMenu.this.setButtonBackGround(5);
                            return false;
                        case 1:
                            MainMenu.this.refreshButtons();
                            return false;
                        default:
                            return false;
                    }
                }
            });
            this.about_us.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View arg0, MotionEvent me) {
                    switch (me.getAction()) {
                        case 0:
                            MainMenu.this.setButtonBackGround(6);
                            return false;
                        case 1:
                            MainMenu.this.refreshButtons();
                            return false;
                        default:
                            return false;
                    }
                }
            });
            this.help.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View arg0, MotionEvent me) {
                    switch (me.getAction()) {
                        case 0:
                            MainMenu.this.setButtonBackGround(7);
                            return false;
                        case 1:
                            MainMenu.this.refreshButtons();
                            return false;
                        default:
                            return false;
                    }
                }
            });
        } catch (Exception e) {
            System.out.println("exception in mainmenu");
            e.printStackTrace();
        }
    }

    public void setButtonBackGround(int buttonId) {
        refreshButtons();
        if (buttonId == 1) {
            this.buy_app.setBackgroundResource(R.drawable.select);
        } else if (buttonId == 2) {
            this.start.setBackgroundResource(R.drawable.select);
        } else if (buttonId == 3) {
            this.score.setBackgroundResource(R.drawable.select);
        } else if (buttonId == 4) {
            this.settings.setBackgroundResource(R.drawable.select);
        } else if (buttonId == 5) {
            this.more_apps.setBackgroundResource(R.drawable.select);
        } else if (buttonId == 6) {
            this.about_us.setBackgroundResource(R.drawable.select);
        } else if (buttonId == 7) {
            this.help.setBackgroundResource(R.drawable.select);
        }
    }

    public void refreshButtons() {
        this.buy_app.setBackgroundResource(R.drawable.unselect);
        this.start.setBackgroundResource(R.drawable.unselect);
        this.settings.setBackgroundResource(R.drawable.unselect);
        this.score.setBackgroundResource(R.drawable.unselect);
        if (this.freeAppButtonImg != null) {
            this.more_apps.setBackgroundDrawable(this.freeAppButtonImg);
        } else {
            this.more_apps.setBackgroundResource(R.drawable.more_apps);
        }
        this.about_us.setBackgroundResource(R.drawable.unselect);
        this.help.setBackgroundResource(R.drawable.unselect);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        mainMenuStarted = true;
        mainMenuStarted = true;
        if (UpdateDialog.exitApp) {
            finish();
        } else if (UpdateDialog.installUpdate) {
            UpdateDialog.installUpdatedFile(this);
            finish();
        }
        if (AppConstants.appPurchased) {
            AppConstants.appPurchased = false;
            this.buy_app.setVisibility(8);
            this.addManager.removeAdds();
        }
        this.addManager.init(1);
        AddManager.activityState = "Resumed";
    }

    public void doEngineWork() {
        this.engineIO = new EngineIO(this);
        this.netHandler = new NetHandler(this);
        this.freeAppButtonImg = this.engineIO.getFreeAppButtonImage();
        if (this.engineIO.getBuyAppEnableStatus()) {
            this.buy_app.setVisibility(0);
        } else {
            this.buy_app.setVisibility(8);
        }
        if (this.engineIO.getFreeAppEnableStatus()) {
            this.more_apps.setVisibility(0);
            this.more_apps.setText(this.engineIO.getFreeAppButtonText());
            if (this.freeAppButtonImg != null) {
                this.more_apps.setBackgroundDrawable(this.freeAppButtonImg);
            } else {
                this.more_apps.setBackgroundResource(R.drawable.more_apps);
            }
        } else {
            this.more_apps.setVisibility(8);
        }
        this.engineIO.showUpgradedAppList(this);
    }

    public void setScrollViewHeight() {
        ScrollView scrollView = (ScrollView) findViewById(R.id.ScrollView01);
        int phight = getWindowManager().getDefaultDisplay().getHeight();
        ViewGroup.LayoutParams layoutParams = scrollView.getLayoutParams();
        if (this.engineIO.isAddEnable()) {
            layoutParams.height = (phight * 42) / 100;
        } else {
            layoutParams.height = (phight * 58) / 100;
        }
        scrollView.setLayoutParams(layoutParams);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        this.engineIO.showBackPressedDialog(this);
        return false;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.engineIO.close();
        Config.isAppRunning = false;
        this.addManager.pauseAdds();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        AddManager.activityState = "Paused";
    }
}
