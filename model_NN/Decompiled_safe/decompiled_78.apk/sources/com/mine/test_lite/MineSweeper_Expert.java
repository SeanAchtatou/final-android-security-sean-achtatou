package com.mine.test_lite;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.engine.AddManager;
import android.engine.Config;
import android.engine.UpdateDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.util.TypedValue;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class MineSweeper_Expert extends Activity {
    static ArrayList<Block_Expert> myAllNumberList = new ArrayList<>();
    static ArrayList<Block_Expert> myBlankList = new ArrayList<>();
    static ArrayList<Block_Expert> myClickedButtonList = new ArrayList<>();
    static ArrayList<Block_Expert> myEightNumberList = new ArrayList<>();
    static ArrayList<Block_Expert> myFiveNumberList = new ArrayList<>();
    static ArrayList<Block_Expert> myFourNumberList = new ArrayList<>();
    static ArrayList<Block_Expert> myLongClickedList = new ArrayList<>();
    static ArrayList<Block_Expert> myMinesList = new ArrayList<>();
    static ArrayList<Block_Expert> myOneNumberList = new ArrayList<>();
    static ArrayList<Block_Expert> mySevenNumberList = new ArrayList<>();
    static ArrayList<Block_Expert> mySixNumberList = new ArrayList<>();
    static ArrayList<Block_Expert> myThreeNumberList = new ArrayList<>();
    static ArrayList<Block_Expert> myTwoNumberList = new ArrayList<>();
    AddManager addManager;
    AlertDialog alertDialog = null;
    /* access modifiers changed from: private */
    public boolean areMinesSet = false;
    /* access modifiers changed from: private */
    public Block_Expert[][] blocks;
    View.OnClickListener clickListener = new View.OnClickListener() {
        public void onClick(View v) {
            int id = v.getId();
            System.out.println("in single click-------------------");
            String new_button_id = String.valueOf(id);
            System.out.println("button_id in click  =" + new_button_id);
            String row_string = new_button_id.substring(0, 2);
            String column_string = new_button_id.substring(2);
            if (Integer.parseInt(row_string) > 21) {
                row_string = row_string.replaceFirst("9", "0");
            }
            MineSweeper_Expert.this.currentRow = Integer.parseInt(row_string);
            MineSweeper_Expert.this.currentColumn = Integer.parseInt(column_string);
            System.out.println("Current row ************ " + MineSweeper_Expert.this.currentRow);
            System.out.println("Current cloumn ************ " + MineSweeper_Expert.this.currentColumn);
            if (MineSweeper_Expert.this.for_unzoom) {
                MineSweeper_Expert.this.for_zoom = true;
                MineSweeper_Expert.this.for_unzoom = false;
                MineSweeper_Expert.this.setButtonHeightWidthFromZoom(MineSweeper_Expert.this.numberForButtonSize - 14);
                MineSweeper_Expert.this.zoom_button.setBackgroundResource(R.drawable.zoom_out);
            } else if (MineSweeper_Expert.this.isPlayButtonClicked) {
                if (!MineSweeper_Expert.this.isTimerStarted) {
                    MineSweeper_Expert.this.startTimer();
                    MineSweeper_Expert.this.isTimerStarted = true;
                }
                if (!MineSweeper_Expert.this.areMinesSet) {
                    if (MineSweeper_Expert.this.vibr_mode.getVibrationMode().equals("1")) {
                        MineSweeper_Expert.this.vibrator.vibrate(100);
                    }
                    MineSweeper_Expert.this.areMinesSet = true;
                    MineSweeper_Expert.myClickedButtonList.add(MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn]);
                    MineSweeper_Expert.this.setMines(MineSweeper_Expert.this.currentRow, MineSweeper_Expert.this.currentColumn);
                    for (int c_row = 0; c_row < MineSweeper_Expert.this.numberOfRowsInMineField; c_row++) {
                        for (int c_column = 0; c_column < MineSweeper_Expert.this.numberOfRowsInMineField; c_column++) {
                            if (!MineSweeper_Expert.this.blocks[c_row][c_column].hasMine()) {
                                System.out.println("numOnButton for c_row " + c_row + " and c_column " + c_column + " is " + MineSweeper_Expert.this.setAccordingToNeighbours(c_row, c_column));
                                if (MineSweeper_Expert.this.noWillSet == 0) {
                                    MineSweeper_Expert.myBlankList.add(MineSweeper_Expert.this.blocks[c_row][c_column]);
                                } else if (MineSweeper_Expert.this.noWillSet == 1) {
                                    MineSweeper_Expert.myAllNumberList.add(MineSweeper_Expert.this.blocks[c_row][c_column]);
                                    MineSweeper_Expert.myOneNumberList.add(MineSweeper_Expert.this.blocks[c_row][c_column]);
                                } else if (MineSweeper_Expert.this.noWillSet == 2) {
                                    MineSweeper_Expert.myAllNumberList.add(MineSweeper_Expert.this.blocks[c_row][c_column]);
                                    MineSweeper_Expert.myTwoNumberList.add(MineSweeper_Expert.this.blocks[c_row][c_column]);
                                } else if (MineSweeper_Expert.this.noWillSet == 3) {
                                    MineSweeper_Expert.myAllNumberList.add(MineSweeper_Expert.this.blocks[c_row][c_column]);
                                    MineSweeper_Expert.myThreeNumberList.add(MineSweeper_Expert.this.blocks[c_row][c_column]);
                                } else if (MineSweeper_Expert.this.noWillSet == 4) {
                                    MineSweeper_Expert.myAllNumberList.add(MineSweeper_Expert.this.blocks[c_row][c_column]);
                                    MineSweeper_Expert.myFourNumberList.add(MineSweeper_Expert.this.blocks[c_row][c_column]);
                                } else if (MineSweeper_Expert.this.noWillSet == 5) {
                                    MineSweeper_Expert.myAllNumberList.add(MineSweeper_Expert.this.blocks[c_row][c_column]);
                                    MineSweeper_Expert.myFiveNumberList.add(MineSweeper_Expert.this.blocks[c_row][c_column]);
                                } else if (MineSweeper_Expert.this.noWillSet == 6) {
                                    MineSweeper_Expert.myAllNumberList.add(MineSweeper_Expert.this.blocks[c_row][c_column]);
                                    MineSweeper_Expert.mySixNumberList.add(MineSweeper_Expert.this.blocks[c_row][c_column]);
                                } else if (MineSweeper_Expert.this.noWillSet == 7) {
                                    MineSweeper_Expert.myAllNumberList.add(MineSweeper_Expert.this.blocks[c_row][c_column]);
                                    MineSweeper_Expert.mySevenNumberList.add(MineSweeper_Expert.this.blocks[c_row][c_column]);
                                } else if (MineSweeper_Expert.this.noWillSet == 8) {
                                    MineSweeper_Expert.myAllNumberList.add(MineSweeper_Expert.this.blocks[c_row][c_column]);
                                    MineSweeper_Expert.myEightNumberList.add(MineSweeper_Expert.this.blocks[c_row][c_column]);
                                }
                            }
                        }
                    }
                    System.out.println("my all list size " + MineSweeper_Expert.myAllNumberList.size());
                    System.out.println("my balnk list size " + MineSweeper_Expert.myBlankList.size());
                    System.out.println("my one list size " + MineSweeper_Expert.myOneNumberList.size());
                    System.out.println("my 2 list size " + MineSweeper_Expert.myTwoNumberList.size());
                    System.out.println("my 3 list size " + MineSweeper_Expert.myThreeNumberList.size());
                    System.out.println("my 4 list size " + MineSweeper_Expert.myFourNumberList.size());
                    System.out.println("my 5 list size " + MineSweeper_Expert.myFiveNumberList.size());
                    System.out.println("my 6 list size " + MineSweeper_Expert.mySixNumberList.size());
                    System.out.println("my 7 list size " + MineSweeper_Expert.mySevenNumberList.size());
                    System.out.println("my 8 list size " + MineSweeper_Expert.myEightNumberList.size());
                    if (!MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].hasAnyNumber()) {
                        MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].setBackgroundResource(R.drawable.blank1);
                        System.out.println("currentRow in first click " + MineSweeper_Expert.this.currentRow);
                        System.out.println("currentColumn in first click " + MineSweeper_Expert.this.currentColumn);
                        MineSweeper_Expert.this.checkBlankAccordingToNeighbours(MineSweeper_Expert.this.currentRow, MineSweeper_Expert.this.currentColumn);
                    } else if (MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].hasOne()) {
                        MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].setBackgroundResource(R.drawable.button_1);
                    } else if (MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].hasTwo()) {
                        MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].setBackgroundResource(R.drawable.button_2);
                    } else if (MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].hasThree()) {
                        MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].setBackgroundResource(R.drawable.button_3);
                    } else if (MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].hasFour()) {
                        MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].setBackgroundResource(R.drawable.button_4);
                    } else if (MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].hasFive()) {
                        MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].setBackgroundResource(R.drawable.button_5);
                    } else if (MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].hasSix()) {
                        MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].setBackgroundResource(R.drawable.button_6);
                    } else if (MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].hasSeven()) {
                        MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].setBackgroundResource(R.drawable.button_7);
                    } else if (MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].hasEight()) {
                        MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].setBackgroundResource(R.drawable.button_8);
                    }
                } else {
                    System.out.println("myClickedButtonList in else part of first click  " + MineSweeper_Expert.myClickedButtonList.size());
                    if (MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].isClicked()) {
                        System.out.println("in else part is clicked part");
                    } else if (MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].hasMine()) {
                        System.out.println("after second click mine found ");
                        if (MineSweeper_Expert.this.vibr_mode.getVibrationMode().equals("1")) {
                            MineSweeper_Expert.this.vibrator.vibrate(100);
                        }
                        MineSweeper_Expert.this.finishGame();
                    } else {
                        if (MineSweeper_Expert.this.vibr_mode.getVibrationMode().equals("1")) {
                            MineSweeper_Expert.this.vibrator.vibrate(100);
                        }
                        System.out.println("after second click getting neighbours  ");
                        MineSweeper_Expert.myClickedButtonList.add(MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn]);
                        if (!MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].hasAnyNumber()) {
                            if (MineSweeper_Expert.this.vibr_mode.getVibrationMode().equals("1")) {
                                MineSweeper_Expert.this.vibrator.vibrate(100);
                            }
                            MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].setBackgroundResource(R.drawable.blank1);
                            System.out.println("currentRow in second click " + MineSweeper_Expert.this.currentRow);
                            System.out.println("currentColumn in second click " + MineSweeper_Expert.this.currentColumn);
                            MineSweeper_Expert.this.checkBlankAccordingToNeighbours(MineSweeper_Expert.this.currentRow, MineSweeper_Expert.this.currentColumn);
                        } else if (MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].hasOne()) {
                            MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].setBackgroundResource(R.drawable.button_1);
                        } else if (MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].hasTwo()) {
                            MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].setBackgroundResource(R.drawable.button_2);
                        } else if (MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].hasThree()) {
                            MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].setBackgroundResource(R.drawable.button_3);
                        } else if (MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].hasFour()) {
                            MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].setBackgroundResource(R.drawable.button_4);
                        } else if (MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].hasFive()) {
                            MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].setBackgroundResource(R.drawable.button_5);
                        } else if (MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].hasSix()) {
                            MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].setBackgroundResource(R.drawable.button_6);
                        } else if (MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].hasSeven()) {
                            MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].setBackgroundResource(R.drawable.button_7);
                        } else if (MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].hasEight()) {
                            MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].setBackgroundResource(R.drawable.button_8);
                        }
                    }
                }
            } else if (MineSweeper_Expert.this.isFlagButtonClicked) {
                if (!MineSweeper_Expert.this.areMinesSet) {
                    MineSweeper_Expert.this.showPrompt("You can not start the game by Setting a flag.");
                } else if (!MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].isClicked() && MineSweeper_Expert.this.minesToFind > 0) {
                    MineSweeper_Expert.myLongClickedList.add(MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn]);
                    MineSweeper_Expert.myClickedButtonList.add(MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn]);
                    MineSweeper_Expert mineSweeper_Expert = MineSweeper_Expert.this;
                    mineSweeper_Expert.minesToFind = mineSweeper_Expert.minesToFind - 1;
                    MineSweeper_Expert.this.txtMineCount.setText("   ".concat(String.valueOf(MineSweeper_Expert.this.minesToFind)));
                    MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].setBackgroundResource(R.drawable.flag);
                }
            } else if (MineSweeper_Expert.this.isQuesMarkButtonClicked) {
                if (!MineSweeper_Expert.this.areMinesSet) {
                    MineSweeper_Expert.this.showPrompt("You can not start the game by Setting a question Mark.");
                } else if (MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].isClicked() && MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].isLongClicked()) {
                    if (MineSweeper_Expert.this.vibr_mode.getVibrationMode().equals("1")) {
                        MineSweeper_Expert.this.vibrator.vibrate(100);
                    }
                    MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn].setBackgroundResource(R.drawable.ques);
                    MineSweeper_Expert mineSweeper_Expert2 = MineSweeper_Expert.this;
                    mineSweeper_Expert2.minesToFind = mineSweeper_Expert2.minesToFind + 1;
                    MineSweeper_Expert.this.txtMineCount.setText("   ".concat(String.valueOf(MineSweeper_Expert.this.minesToFind)));
                    MineSweeper_Expert.myLongClickedList.remove(MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn]);
                    MineSweeper_Expert.myClickedButtonList.remove(MineSweeper_Expert.this.blocks[MineSweeper_Expert.this.currentRow][MineSweeper_Expert.this.currentColumn]);
                }
            }
            if (MineSweeper_Expert.this.checkGameWin()) {
                MineSweeper_Expert.this.winGameCalled = true;
                boolean isTopper = true;
                MineSweeper_Expert.this.imageButton_smily.setBackgroundResource(R.drawable.smile_happy);
                MineSweeper_Expert.this.stopTimer();
                Cursor c = MineSweeper_Expert.this.db.fetchAllScoreDetailsForExpert();
                if (c.getCount() != 0) {
                    int i = 0;
                    while (true) {
                        if (i >= c.getCount()) {
                            break;
                        }
                        c.moveToNext();
                        if (c.getInt(2) < MineSweeper_Expert.this.secondsPassed) {
                            isTopper = false;
                            break;
                        }
                        i++;
                    }
                } else {
                    isTopper = true;
                }
                if (isTopper) {
                    MineSweeper_Expert.this.showWinDialog("You have the fastest time for expert level.");
                } else {
                    MineSweeper_Expert.this.showWinDialog("You have won the game in " + MineSweeper_Expert.this.secondsPassed + " seconds");
                }
            }
        }
    };
    Config config;
    int currentColumn;
    int currentRow;
    DBHandler db;
    boolean finishGameCalled = false;
    boolean for_unzoom = true;
    boolean for_zoom = false;
    String game_level_type;
    /* access modifiers changed from: private */
    public Handler handler = new Handler();
    ImageButton imageButton_flag;
    ImageButton imageButton_ques;
    ImageButton imageButton_smily;
    boolean isFlagButtonClicked = false;
    boolean isPlayButtonClicked = true;
    boolean isQuesMarkButtonClicked = false;
    boolean isShowWindowButtonClicked = false;
    /* access modifiers changed from: private */
    public boolean isTimerStarted = false;
    LinearLayout lowerLayout;
    Context mContext;
    private TableLayout mineField;
    /* access modifiers changed from: private */
    public int minesToFind = 60;
    MyTimer mt;
    int noWillSet;
    int numberForButtonSize = 20;
    int numberOfColumnsInMineField = 20;
    int numberOfRowsInMineField = 20;
    boolean onPauseCalled = false;
    /* access modifiers changed from: private */
    public int secondsPassed = 0;
    int time_left_for_game;
    TimeLimit time_limit;
    Timer timer;
    int totalNumberOfMines = 60;
    View.OnTouchListener touchListener = new View.OnTouchListener() {
        public boolean onTouch(View arg0, MotionEvent me) {
            if (!MineSweeper_Expert.this.for_zoom) {
                return false;
            }
            switch (me.getAction()) {
                case 0:
                    MineSweeper_Expert.this.imageButton_smily.setBackgroundResource(R.drawable.smile_oopss);
                    return false;
                case 1:
                    MineSweeper_Expert.this.imageButton_smily.setBackgroundResource(R.drawable.smile_normal);
                    return false;
                case 2:
                    MineSweeper_Expert.this.imageButton_smily.setBackgroundResource(R.drawable.smile_normal);
                    return false;
                case 3:
                    System.out.println("in ACTION_CANCEL");
                    MineSweeper_Expert.this.imageButton_smily.setBackgroundResource(R.drawable.smile_normal);
                    return false;
                case 4:
                    System.out.println("in ACTION_OUTSIDE");
                    MineSweeper_Expert.this.imageButton_smily.setBackgroundResource(R.drawable.smile_normal);
                    return false;
                default:
                    return false;
            }
        }
    };
    /* access modifiers changed from: private */
    public TextView txtMineCount;
    /* access modifiers changed from: private */
    public TextView txtTimer;
    /* access modifiers changed from: private */
    public Runnable updateTimeElasped = new Runnable() {
        public void run() {
            MineSweeper_Expert.this.txtTimer.setText("   " + Integer.toString(MineSweeper_Expert.this.secondsPassed));
            if (MineSweeper_Expert.this.time_left_for_game != 0 && MineSweeper_Expert.this.secondsPassed == MineSweeper_Expert.this.time_left_for_game) {
                MineSweeper_Expert.this.stopTimer();
                MineSweeper_Expert.this.showPrompt2("Time up!!");
            }
            if (MineSweeper_Expert.this.minesToFind < 0) {
                MineSweeper_Expert.this.finishGame();
                System.out.println("in run method and in if minesToFind<0 condition");
            }
        }
    };
    LinearLayout upperLayout;
    VibrationOnOff vibr_mode;
    Vibrator vibrator;
    boolean winGameCalled = false;
    Button zoom_button;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.main_expert);
        this.noWillSet = 0;
        this.secondsPassed = 0;
        this.vibrator = (Vibrator) getSystemService("vibrator");
        this.timer = new Timer();
        this.mt = new MyTimer();
        System.out.println("MineSweeper_Expert.onCreate()");
        clearAllList();
        System.out.println("MineSweeper_Expert.onCreate()");
        this.txtMineCount = (TextView) findViewById(R.id.MineCount);
        this.txtTimer = (TextView) findViewById(R.id.TimerTextView);
        this.zoom_button = (Button) findViewById(R.id.Button_zoom_in);
        this.imageButton_smily = (ImageButton) findViewById(R.id.ImageButton_smily_types);
        this.imageButton_flag = (ImageButton) findViewById(R.id.ImageButton_flag);
        this.imageButton_ques = (ImageButton) findViewById(R.id.ImageButton_ques);
        this.upperLayout = (LinearLayout) findViewById(R.id.LinearLayout05);
        this.lowerLayout = (LinearLayout) findViewById(R.id.LinearLayout04);
        this.blocks = (Block_Expert[][]) Array.newInstance(Block_Expert.class, 20, 20);
        this.vibr_mode = new VibrationOnOff(this);
        this.time_limit = new TimeLimit(this);
        this.addManager = new AddManager(this);
        this.config = new Config(this);
        this.mContext = this;
        this.db = new DBHandler(this);
        this.time_left_for_game = Integer.parseInt(this.time_limit.getTimeLimit());
        this.time_left_for_game *= 60;
        for (int row = 0; row < this.numberOfRowsInMineField; row++) {
            for (int column = 0; column < this.numberOfColumnsInMineField; column++) {
                this.blocks[row][column] = new Block_Expert(this);
            }
        }
        try {
            showMineField();
            this.txtMineCount.setText("   " + String.valueOf(this.totalNumberOfMines));
            for (int row2 = 0; row2 < this.numberOfRowsInMineField; row2++) {
                for (int column2 = 0; column2 < this.numberOfColumnsInMineField; column2++) {
                    this.blocks[row2][column2].setOnClickListener(this.clickListener);
                    this.blocks[row2][column2].setOnTouchListener(this.touchListener);
                }
            }
            this.zoom_button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    if (MineSweeper_Expert.this.for_unzoom) {
                        MineSweeper_Expert.this.for_zoom = true;
                        MineSweeper_Expert.this.for_unzoom = false;
                        MineSweeper_Expert.this.setButtonHeightWidthFromZoom(MineSweeper_Expert.this.numberForButtonSize - 14);
                        MineSweeper_Expert.this.zoom_button.setBackgroundResource(R.drawable.zoom_out);
                    } else if (MineSweeper_Expert.this.for_zoom) {
                        MineSweeper_Expert.this.for_unzoom = true;
                        MineSweeper_Expert.this.for_zoom = false;
                        MineSweeper_Expert.this.setButtonHeightWidthFromZoom(MineSweeper_Expert.this.numberForButtonSize);
                        MineSweeper_Expert.this.zoom_button.setBackgroundResource(R.drawable.zoom_in);
                    }
                }
            });
            this.imageButton_smily.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    MineSweeper_Expert.this.showNewGameDialog("Do you want to start a new game?");
                }
            });
            this.imageButton_flag.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    if (MineSweeper_Expert.this.isFlagButtonClicked) {
                        MineSweeper_Expert.this.isFlagButtonClicked = false;
                        MineSweeper_Expert.this.isQuesMarkButtonClicked = false;
                        MineSweeper_Expert.this.isPlayButtonClicked = true;
                        MineSweeper_Expert.this.imageButton_ques.setBackgroundResource(R.drawable.ques_unselect);
                        MineSweeper_Expert.this.imageButton_flag.setBackgroundResource(R.drawable.flag_unselect);
                        return;
                    }
                    MineSweeper_Expert.this.isFlagButtonClicked = true;
                    MineSweeper_Expert.this.isPlayButtonClicked = false;
                    MineSweeper_Expert.this.isQuesMarkButtonClicked = false;
                    MineSweeper_Expert.this.imageButton_ques.setBackgroundResource(R.drawable.ques_unselect);
                    MineSweeper_Expert.this.imageButton_flag.setBackgroundResource(R.drawable.flag_select);
                }
            });
            this.imageButton_ques.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    if (MineSweeper_Expert.this.isQuesMarkButtonClicked) {
                        MineSweeper_Expert.this.isFlagButtonClicked = false;
                        MineSweeper_Expert.this.isQuesMarkButtonClicked = false;
                        MineSweeper_Expert.this.isPlayButtonClicked = true;
                        MineSweeper_Expert.this.imageButton_flag.setBackgroundResource(R.drawable.flag_unselect);
                        MineSweeper_Expert.this.imageButton_ques.setBackgroundResource(R.drawable.ques_unselect);
                        return;
                    }
                    MineSweeper_Expert.this.isFlagButtonClicked = false;
                    MineSweeper_Expert.this.isPlayButtonClicked = false;
                    MineSweeper_Expert.this.isQuesMarkButtonClicked = true;
                    MineSweeper_Expert.this.imageButton_flag.setBackgroundResource(R.drawable.flag_unselect);
                    MineSweeper_Expert.this.imageButton_ques.setBackgroundResource(R.drawable.ques_select);
                }
            });
        } catch (Exception e) {
            System.out.println("exception in shareOnFacebook");
        }
    }

    /* access modifiers changed from: private */
    public int setAccordingToNeighbours(int currentClickedRow, int currentClickedCloumn) {
        int CR = currentClickedRow;
        int CC = currentClickedCloumn;
        this.noWillSet = 0;
        if (currentClickedRow == 19) {
            if (currentClickedCloumn == 0) {
                checkOutNeighbour(currentClickedRow - 1, currentClickedCloumn + 1);
                checkOutNeighbour(CR - 1, CC);
                checkOutNeighbour(CR, CC + 1);
            } else if (currentClickedCloumn == 19) {
                checkOutNeighbour(CR, CC - 1);
                checkOutNeighbour(CR - 1, CC);
                checkOutNeighbour(CR - 1, CC - 1);
            } else {
                checkOutNeighbour(CR - 1, CC - 1);
                checkOutNeighbour(CR - 1, CC + 1);
                checkOutNeighbour(CR - 1, CC);
                checkOutNeighbour(CR, CC - 1);
                checkOutNeighbour(CR, CC + 1);
            }
        } else if (currentClickedRow == 0) {
            if (currentClickedCloumn == 0) {
                checkOutNeighbour(CR, CC + 1);
                checkOutNeighbour(CR + 1, CC + 1);
                checkOutNeighbour(CR + 1, CC);
            } else if (currentClickedCloumn == 19) {
                checkOutNeighbour(CR, CC - 1);
                checkOutNeighbour(CR + 1, CC - 1);
                checkOutNeighbour(CR + 1, CC);
            } else {
                checkOutNeighbour(CR, CC + 1);
                checkOutNeighbour(CR, CC - 1);
                checkOutNeighbour(CR + 1, CC + 1);
                checkOutNeighbour(CR + 1, CC - 1);
                checkOutNeighbour(CR + 1, CC);
            }
        } else if (currentClickedCloumn == 0) {
            checkOutNeighbour(CR, CC + 1);
            checkOutNeighbour(CR + 1, CC + 1);
            checkOutNeighbour(CR + 1, CC);
            checkOutNeighbour(CR - 1, CC + 1);
            checkOutNeighbour(CR - 1, CC);
        } else if (currentClickedCloumn == 19) {
            checkOutNeighbour(CR, CC - 1);
            checkOutNeighbour(CR + 1, CC - 1);
            checkOutNeighbour(CR + 1, CC);
            checkOutNeighbour(CR - 1, CC - 1);
            checkOutNeighbour(CR - 1, CC);
        } else {
            checkOutNeighbour(CR, CC + 1);
            checkOutNeighbour(CR, CC - 1);
            checkOutNeighbour(CR + 1, CC + 1);
            checkOutNeighbour(CR + 1, CC - 1);
            checkOutNeighbour(CR + 1, CC);
            checkOutNeighbour(CR - 1, CC + 1);
            checkOutNeighbour(CR - 1, CC - 1);
            checkOutNeighbour(CR - 1, CC);
        }
        return this.noWillSet;
    }

    public void checkOutNeighbour(int cr, int cc) {
        if (this.blocks[cr][cc].hasMine()) {
            this.noWillSet++;
            return;
        }
        if (this.blocks[cr][cc].isClicked()) {
        }
    }

    /* access modifiers changed from: private */
    public void checkBlankAccordingToNeighbours(int currentClickedRow, int currentClickedCloumn) {
        int CR = currentClickedRow;
        int CC = currentClickedCloumn;
        System.out.println("currentClickedRow in checkBlankAccordingToNeighbours " + currentClickedRow);
        System.out.println("currentClickedCloumn in checkBlankAccordingToNeighbours " + currentClickedCloumn);
        if (currentClickedRow == 19) {
            if (currentClickedCloumn == 0) {
                System.out.println("1");
                checkBlank(currentClickedRow - 1, currentClickedCloumn + 1);
                checkBlank(CR - 1, CC);
                checkBlank(CR, CC + 1);
            } else if (currentClickedCloumn == 19) {
                System.out.println("2");
                checkBlank(CR, CC - 1);
                checkBlank(CR - 1, CC);
                checkBlank(CR - 1, CC - 1);
            } else {
                System.out.println("3");
                checkBlank(CR - 1, CC - 1);
                checkBlank(CR - 1, CC + 1);
                checkBlank(CR - 1, CC);
                checkBlank(CR, CC - 1);
                checkBlank(CR, CC + 1);
            }
        } else if (currentClickedRow == 0) {
            if (currentClickedCloumn == 0) {
                System.out.println("4");
                checkBlank(CR, CC + 1);
                checkBlank(CR + 1, CC + 1);
                checkBlank(CR + 1, CC);
            } else if (currentClickedCloumn == 19) {
                System.out.println("5");
                checkBlank(CR, CC - 1);
                checkBlank(CR + 1, CC - 1);
                checkBlank(CR + 1, CC);
            } else {
                System.out.println("6");
                checkBlank(CR, CC + 1);
                checkBlank(CR, CC - 1);
                checkBlank(CR + 1, CC + 1);
                checkBlank(CR + 1, CC - 1);
                checkBlank(CR + 1, CC);
            }
        } else if (currentClickedCloumn == 0) {
            System.out.println("7");
            checkBlank(CR, CC + 1);
            checkBlank(CR + 1, CC + 1);
            checkBlank(CR + 1, CC);
            checkBlank(CR - 1, CC + 1);
            checkBlank(CR - 1, CC);
        } else if (currentClickedCloumn == 19) {
            System.out.println("8");
            checkBlank(CR, CC - 1);
            checkBlank(CR + 1, CC - 1);
            checkBlank(CR + 1, CC);
            checkBlank(CR - 1, CC - 1);
            checkBlank(CR - 1, CC);
        } else {
            System.out.println("9");
            checkBlank(CR, CC + 1);
            checkBlank(CR, CC - 1);
            checkBlank(CR + 1, CC + 1);
            checkBlank(CR + 1, CC - 1);
            checkBlank(CR + 1, CC);
            checkBlank(CR - 1, CC + 1);
            checkBlank(CR - 1, CC - 1);
            checkBlank(CR - 1, CC);
        }
    }

    public void checkBlank(int cr, int cc) {
        int currentClickedRow = cr;
        int currentClickedCloumn = cc;
        System.out.println("currentClickedRow in check blank method " + currentClickedRow);
        System.out.println("currentClickedCloumn in check blank method " + currentClickedCloumn);
        if (this.blocks[currentClickedRow][currentClickedCloumn].isClicked()) {
            System.out.println("is clicked");
        } else if (this.blocks[currentClickedRow][currentClickedCloumn].hasAnyNumber()) {
            myClickedButtonList.add(this.blocks[currentClickedRow][currentClickedCloumn]);
            System.out.println("has number");
            if (this.blocks[currentClickedRow][currentClickedCloumn].hasOne()) {
                this.blocks[currentClickedRow][currentClickedCloumn].setBackgroundResource(R.drawable.button_1);
            } else if (this.blocks[currentClickedRow][currentClickedCloumn].hasTwo()) {
                this.blocks[currentClickedRow][currentClickedCloumn].setBackgroundResource(R.drawable.button_2);
            } else if (this.blocks[currentClickedRow][currentClickedCloumn].hasThree()) {
                this.blocks[currentClickedRow][currentClickedCloumn].setBackgroundResource(R.drawable.button_3);
            } else if (this.blocks[currentClickedRow][currentClickedCloumn].hasFour()) {
                this.blocks[currentClickedRow][currentClickedCloumn].setBackgroundResource(R.drawable.button_4);
            } else if (this.blocks[currentClickedRow][currentClickedCloumn].hasFive()) {
                this.blocks[currentClickedRow][currentClickedCloumn].setBackgroundResource(R.drawable.button_5);
            } else if (this.blocks[currentClickedRow][currentClickedCloumn].hasSix()) {
                this.blocks[currentClickedRow][currentClickedCloumn].setBackgroundResource(R.drawable.button_6);
            } else if (this.blocks[currentClickedRow][currentClickedCloumn].hasSeven()) {
                this.blocks[currentClickedRow][currentClickedCloumn].setBackgroundResource(R.drawable.button_7);
            } else if (this.blocks[currentClickedRow][currentClickedCloumn].hasEight()) {
                this.blocks[currentClickedRow][currentClickedCloumn].setBackgroundResource(R.drawable.button_8);
            }
        } else if (this.blocks[currentClickedRow][currentClickedCloumn].isBlank()) {
            System.out.println("is blank and called again checkBlankAccordingToNeighbours");
            myClickedButtonList.add(this.blocks[currentClickedRow][currentClickedCloumn]);
            this.blocks[currentClickedRow][currentClickedCloumn].setBackgroundResource(R.drawable.blank1);
            checkBlankAccordingToNeighbours(currentClickedRow, currentClickedCloumn);
        }
    }

    public void checkBlankColumn(int current_row, int current_column) {
        int cur_col = current_column;
        System.out.println("in checkBlankColumn value of current_row =" + current_row + "and current_column =" + current_column);
        if (current_column == 0) {
            while (current_column != 20) {
                System.out.println("in while condition value of current_column=" + current_column);
                boolean yes = checkBlanksButton(current_row, current_column);
                if (!yes) {
                    setNumberAtButton(current_row, current_column);
                    System.out.println("in while condition value of yes=" + yes);
                    return;
                }
                this.blocks[current_row][current_column].setBackgroundResource(R.drawable.blank1);
                current_column++;
            }
        } else if (current_column == 19) {
            while (current_column != -1) {
                System.out.println("in while condition value of current_column=" + current_column);
                boolean yes2 = checkBlanksButton(current_row, current_column);
                if (!yes2) {
                    setNumberAtButton(current_row, current_column);
                    System.out.println("in while condition value of yes=" + yes2);
                    return;
                }
                this.blocks[current_row][current_column].setBackgroundResource(R.drawable.blank1);
                current_column--;
            }
        } else {
            while (current_column != -1) {
                System.out.println("in while condition value of current_column=" + current_column);
                boolean yes3 = checkBlanksButton(current_row, current_column);
                if (!yes3) {
                    setNumberAtButton(current_row, current_column);
                    System.out.println("in while condition value of yes=" + yes3);
                    return;
                }
                this.blocks[current_row][current_column].setBackgroundResource(R.drawable.blank1);
                current_column--;
            }
            for (int current_column2 = cur_col; current_column2 != 20; current_column2++) {
                System.out.println("in while condition value of current_column=" + current_column2);
                boolean yes4 = checkBlanksButton(current_row, current_column2);
                if (!yes4) {
                    setNumberAtButton(current_row, current_column2);
                    System.out.println("in while condition value of yes=" + yes4);
                    return;
                }
                this.blocks[current_row][current_column2].setBackgroundResource(R.drawable.blank1);
            }
        }
    }

    public void setNumberAtButton(int current_row, int current_column) {
        System.out.println("current_row in setNumberAtButton =" + current_row);
        System.out.println("current_column in setNumberAtButton =" + current_column);
        if (this.blocks[current_row][current_column].hasOne()) {
            this.blocks[current_row][current_column].setBackgroundResource(R.drawable.button_1);
        } else if (this.blocks[current_row][current_column].hasTwo()) {
            this.blocks[current_row][current_column].setBackgroundResource(R.drawable.button_2);
        } else if (this.blocks[current_row][current_column].hasThree()) {
            this.blocks[current_row][current_column].setBackgroundResource(R.drawable.button_3);
        } else if (this.blocks[current_row][current_column].hasFour()) {
            this.blocks[current_row][current_column].setBackgroundResource(R.drawable.button_4);
        } else if (this.blocks[current_row][current_column].hasFive()) {
            this.blocks[current_row][current_column].setBackgroundResource(R.drawable.button_5);
        } else if (this.blocks[current_row][current_column].hasSix()) {
            this.blocks[current_row][current_column].setBackgroundResource(R.drawable.button_6);
        } else if (this.blocks[current_row][current_column].hasSeven()) {
            this.blocks[current_row][current_column].setBackgroundResource(R.drawable.button_7);
        } else if (this.blocks[current_row][current_column].hasEight()) {
            this.blocks[current_row][current_column].setBackgroundResource(R.drawable.button_8);
        }
    }

    public void checkBlanksForAllRow(int current_row, int current_column) {
        int cur_row = current_row;
        System.out.println("row in checkBlanksForAllRow  " + current_row);
        System.out.println("column in checkBlanksForAllRow  " + current_column);
        if (current_row == 0) {
            while (current_row != 20) {
                System.out.println("row in checkBlanksForAllRow in while condition " + current_row);
                System.out.println("column in checkBlanksForAllRow in while condition " + current_column);
                checkBlankColumn(current_row, current_column);
                current_row++;
            }
        } else if (current_row == 19) {
            while (current_row != -1) {
                System.out.println("row in checkBlanksForAllRow in while condition " + current_row);
                System.out.println("column in checkBlanksForAllRow in while condition " + current_column);
                checkBlankColumn(current_row, current_column);
                current_row--;
            }
        } else {
            while (current_row != -1) {
                System.out.println("row in checkBlanksForAllRow in while condition " + current_row);
                System.out.println("column in checkBlanksForAllRow in while condition " + current_column);
                checkBlankColumn(current_row, current_column);
                current_row--;
            }
            for (int current_row2 = cur_row; current_row2 != 20; current_row2++) {
                System.out.println("row in checkBlanksForAllRow in while condition " + current_row2);
                System.out.println("column in checkBlanksForAllRow in while condition " + current_column);
                checkBlankColumn(current_row2, current_column);
            }
        }
    }

    public boolean checkBlanksButton(int current_row, int current_column) {
        return this.blocks[current_row][current_column].isBlank();
    }

    private void showMineField() {
        float px;
        String row_button;
        for (int row = 0; row < this.numberOfRowsInMineField; row++) {
            this.mineField = (TableLayout) findViewById(R.id.TableLayout01);
            Display display = getWindowManager().getDefaultDisplay();
            int pwidth = display.getWidth();
            int phight = display.getHeight();
            Resources r = getResources();
            if (this.config.getFTYPE().equals("3")) {
                px = TypedValue.applyDimension(1, 105.0f, r.getDisplayMetrics());
            } else {
                px = TypedValue.applyDimension(1, 190.0f, r.getDisplayMetrics());
            }
            TableRow tableRow = new TableRow(this);
            if (row < 0 || row > 9) {
                row_button = String.valueOf(row);
            } else if (row == 0) {
                row_button = String.valueOf(9).concat(String.valueOf(0));
            } else {
                row_button = String.valueOf(9).concat(String.valueOf(row));
            }
            for (int column = 0; column < this.numberOfColumnsInMineField; column++) {
                String buttonId = row_button.concat(String.valueOf(column));
                this.blocks[row][column].setPadding(1, 1, 1, 1);
                this.blocks[row][column].setId(Integer.parseInt(buttonId));
                this.blocks[row][column].setBackgroundResource(R.drawable.first_set);
                tableRow.addView(this.blocks[row][column]);
                if (((float) pwidth) == ((float) phight) - px) {
                    ViewGroup.LayoutParams layoutParams = this.blocks[row][column].getLayoutParams();
                    layoutParams.height = pwidth / 20;
                    layoutParams.width = pwidth / 20;
                    this.blocks[row][column].setLayoutParams(layoutParams);
                } else {
                    int difference = (int) ((((float) phight) - px) - ((float) pwidth));
                    this.lowerLayout.layout(0, difference / 2, 0, 0);
                    this.upperLayout.layout(0, 0, 0, difference / 2);
                    ViewGroup.LayoutParams layoutParams2 = this.blocks[row][column].getLayoutParams();
                    layoutParams2.height = pwidth / 20;
                    layoutParams2.width = pwidth / 20;
                    this.blocks[row][column].setLayoutParams(layoutParams2);
                }
            }
            this.mineField.addView(tableRow, new TableLayout.LayoutParams(this.numberOfColumnsInMineField * 7, 7));
        }
    }

    /* access modifiers changed from: private */
    public void setMines(int currentRow2, int currentColumn2) {
        Random rand = new Random();
        int row = 0;
        while (row < this.totalNumberOfMines) {
            int mineRow = rand.nextInt(this.numberOfColumnsInMineField);
            int mineColumn = rand.nextInt(this.numberOfRowsInMineField);
            System.out.println("mineRow  " + mineRow);
            System.out.println("mineColumn  " + mineColumn);
            if (mineRow == currentRow2 && mineColumn == currentColumn2) {
                System.out.println("button is first clicked ");
                row--;
            } else if (this.blocks[mineRow][mineColumn].isClicked()) {
                System.out.println("button is already clicked ");
                row--;
            } else if (this.blocks[mineRow][mineColumn].hasMine()) {
                System.out.println("mine is already there " + this.blocks[mineColumn][mineRow].hasMine());
                row--;
            } else {
                myMinesList.add(this.blocks[mineRow][mineColumn]);
            }
            row++;
        }
    }

    /* access modifiers changed from: private */
    public boolean checkGameWin() {
        for (int row = 0; row < this.numberOfRowsInMineField; row++) {
            for (int column = 0; column < this.numberOfColumnsInMineField; column++) {
                if (!this.blocks[row][column].isClicked()) {
                    System.out.println("In checkGameWin() return false \\\\\\\\\\\\\\\\");
                    return false;
                }
            }
        }
        if (this.minesToFind < 0) {
            return false;
        }
        System.out.println("In checkGameWin() return true \\\\\\\\\\\\\\\\");
        return true;
    }

    public void updateMineCountDisplay() {
    }

    public void finishGame() {
        setButtonHeightWidthFromZoom(this.numberForButtonSize);
        for (int i = 0; i < myMinesList.size(); i++) {
            String new_mines_id = String.valueOf(myMinesList.get(i).getId());
            System.out.println("button_id in click  =" + new_mines_id);
            String row_string = new_mines_id.substring(0, 2);
            String column_string = new_mines_id.substring(2);
            if (Integer.parseInt(row_string) > 21) {
                row_string = row_string.replaceFirst("9", "0");
            }
            int mineRow = Integer.parseInt(row_string);
            int mineColumn = Integer.parseInt(column_string);
            System.out.println("mine row ---------------  " + mineRow);
            System.out.println("mine Column ---------------  " + mineColumn);
            this.blocks[mineRow][mineColumn].setBackgroundResource(R.drawable.mine);
        }
        stopTimer();
        this.finishGameCalled = false;
        this.imageButton_smily.setBackgroundResource(R.drawable.smile_badguess);
        System.out.println("in finish game method ");
        showPrompt2("Oops, you opened a Mine !!");
    }

    public void showPrompt2(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon((int) R.drawable.mine_sweeper_small_icon);
        builder.setTitle("Note:");
        builder.setMessage(msg);
        builder.setPositiveButton("Play Again", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                MineSweeper_Expert.this.clearAllList();
                MineSweeper_Expert.this.finish();
                MineSweeper_Expert.this.startActivity(new Intent(MineSweeper_Expert.this.getApplicationContext(), MineSweeper_Expert.class));
            }
        });
        builder.setNegativeButton("Quit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                MineSweeper_Expert.this.clearAllList();
                MineSweeper_Expert.this.alertDialog.dismiss();
                MineSweeper_Expert.this.finish();
            }
        });
        this.alertDialog = builder.create();
        this.alertDialog.setCancelable(false);
        this.alertDialog.show();
    }

    public void showWinDialog(String msg) {
        View v = LayoutInflater.from(this).inflate((int) R.layout.win_dialog, (ViewGroup) null);
        AlertDialog.Builder exitDialogBuilder = new AlertDialog.Builder(this);
        exitDialogBuilder.setTitle("Congratulation");
        exitDialogBuilder.setIcon((int) R.drawable.mine_sweeper_small_icon);
        exitDialogBuilder.setMessage(msg);
        exitDialogBuilder.setView(v);
        final AlertDialog winDialog = exitDialogBuilder.create();
        winDialog.show();
        ((Button) v.findViewById(R.id.Button_win_play_again)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MineSweeper_Expert.this.clearAllList();
                MineSweeper_Expert.this.finish();
                MineSweeper_Expert.this.startActivity(new Intent(MineSweeper_Expert.this.getApplicationContext(), MineSweeper_Expert.class));
            }
        });
        ((Button) v.findViewById(R.id.Button_win_quit)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                winDialog.dismiss();
                MineSweeper_Expert.this.clearAllList();
                MineSweeper_Expert.this.finish();
            }
        });
        ((Button) v.findViewById(R.id.Button_win_submitt_score)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                winDialog.dismiss();
                MineSweeper_Expert.this.clearAllList();
                MineSweeper_Expert.this.finish();
                Intent i = new Intent(MineSweeper_Expert.this.getApplicationContext(), ScoreSubmit.class);
                i.putExtra("score", MineSweeper_Expert.this.secondsPassed);
                i.putExtra("level", "expert");
                MineSweeper_Expert.this.startActivity(i);
            }
        });
        winDialog.setCancelable(false);
    }

    /* access modifiers changed from: private */
    public void showPrompt(String msg) {
        AlertDialog prompt = new AlertDialog.Builder(this).create();
        prompt.setTitle("Note:");
        prompt.setMessage(msg);
        prompt.setButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dia, int arg1) {
                dia.dismiss();
            }
        });
        prompt.show();
    }

    public void startTimer() {
        if (this.secondsPassed == 0) {
            this.timer.schedule(this.mt, 0, 1000);
            System.out.println("in start timer method");
        } else if (this.onPauseCalled) {
            System.out.println("in resume timer method");
            this.timer.schedule(this.mt, 0, 1000);
        }
    }

    public void stopTimer() {
        this.timer.cancel();
        System.out.println("in stop timer");
    }

    public class MyTimer extends TimerTask {
        public MyTimer() {
        }

        public void run() {
            MineSweeper_Expert mineSweeper_Expert = MineSweeper_Expert.this;
            mineSweeper_Expert.secondsPassed = mineSweeper_Expert.secondsPassed + 1;
            MineSweeper_Expert.this.handler.post(MineSweeper_Expert.this.updateTimeElasped);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        showExitDialog("Do you really want to exit ?");
        return false;
    }

    public void showExitDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon((int) R.drawable.mine_sweeper_small_icon);
        builder.setTitle("Note:");
        builder.setMessage(msg);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                MineSweeper_Expert.this.clearAllList();
                MineSweeper_Expert.this.finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                MineSweeper_Expert.this.alertDialog.dismiss();
            }
        });
        this.alertDialog = builder.create();
        this.alertDialog.setCancelable(false);
        this.alertDialog.show();
    }

    public void showNewGameDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon((int) R.drawable.mine_sweeper_small_icon);
        builder.setTitle("Note:");
        builder.setMessage(msg);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                MineSweeper_Expert.this.clearAllList();
                MineSweeper_Expert.this.finish();
                MineSweeper_Expert.this.startActivity(new Intent(MineSweeper_Expert.this.getApplicationContext(), MineSweeper_Expert.class));
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                MineSweeper_Expert.this.alertDialog.dismiss();
            }
        });
        this.alertDialog = builder.create();
        this.alertDialog.setCancelable(false);
        this.alertDialog.show();
    }

    public void setButtonHeightWidthFromZoom(int number) {
        float px;
        Resources r = getResources();
        Display display = getWindowManager().getDefaultDisplay();
        int pwidth = display.getWidth();
        int phight = display.getHeight();
        if (this.config.getFTYPE().equals("3")) {
            px = TypedValue.applyDimension(1, 105.0f, r.getDisplayMetrics());
        } else {
            px = TypedValue.applyDimension(1, 190.0f, r.getDisplayMetrics());
        }
        for (int i = 0; i < this.numberOfRowsInMineField; i++) {
            for (int j = 0; j < this.numberOfColumnsInMineField; j++) {
                ViewGroup.LayoutParams layoutParams = this.blocks[i][j].getLayoutParams();
                if (((float) pwidth) == ((float) phight) - px) {
                    layoutParams.height = pwidth / number;
                    layoutParams.width = pwidth / number;
                    this.blocks[i][j].setLayoutParams(layoutParams);
                } else {
                    layoutParams.height = pwidth / number;
                    layoutParams.width = pwidth / number;
                    this.blocks[i][j].setLayoutParams(layoutParams);
                }
            }
        }
    }

    public void clearAllList() {
        System.out.println("MineSweeper_Expert.clearAllList()");
        myMinesList.removeAll(myMinesList);
        myClickedButtonList.removeAll(myClickedButtonList);
        myAllNumberList.removeAll(myAllNumberList);
        myBlankList.removeAll(myBlankList);
        myOneNumberList.removeAll(myOneNumberList);
        myTwoNumberList.removeAll(myTwoNumberList);
        myThreeNumberList.removeAll(myThreeNumberList);
        myFourNumberList.removeAll(myFourNumberList);
        myFiveNumberList.removeAll(myFiveNumberList);
        mySixNumberList.removeAll(mySixNumberList);
        mySevenNumberList.removeAll(mySevenNumberList);
        myEightNumberList.removeAll(myEightNumberList);
        myLongClickedList.removeAll(myEightNumberList);
        myMinesList.clear();
        myClickedButtonList.clear();
        myAllNumberList.clear();
        myBlankList.clear();
        myOneNumberList.clear();
        myTwoNumberList.clear();
        myThreeNumberList.clear();
        myFourNumberList.clear();
        myFiveNumberList.clear();
        mySixNumberList.clear();
        mySevenNumberList.clear();
        myEightNumberList.clear();
        myLongClickedList.clear();
        System.gc();
        System.out.println("myMinesList .size()  " + myMinesList.size());
        System.out.println("myClickedButtonList .size()  " + myClickedButtonList.size());
        System.out.println("myBlankList .size()  " + myBlankList.size());
        System.out.println("myOneNumberList .size()  " + myOneNumberList.size());
        System.out.println("myTwoNumberList .size()  " + myTwoNumberList.size());
        System.out.println("myThreeNumberList .size()  " + myThreeNumberList.size());
        System.out.println("myFourNumberList .size()  " + myFourNumberList.size());
        System.out.println("myMinesList.size()  " + myMinesList.size());
        System.out.println("myFiveNumberList .size()  " + myFiveNumberList.size());
        System.out.println("mySixNumberList .size()  " + mySixNumberList.size());
        System.out.println("mySevenNumberList .size()  " + mySevenNumberList.size());
        System.out.println("myEightNumberList .size()  " + myEightNumberList.size());
        System.out.println("myLongClickedList .size()  " + myLongClickedList.size());
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.onPauseCalled && this.isTimerStarted && !this.winGameCalled && !this.finishGameCalled) {
            this.timer = new Timer();
            this.mt = new MyTimer();
            startTimer();
        }
        AddManager.activityState = "Resumed";
        if (UpdateDialog.exitApp || UpdateDialog.installUpdate) {
            finish();
        }
        this.addManager.init(4);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        AddManager.activityState = "Paused";
        if (this.isTimerStarted) {
            this.timer.cancel();
        }
        this.onPauseCalled = true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        System.out.println("MineSweeper_Expert.onDestroy()");
    }
}
