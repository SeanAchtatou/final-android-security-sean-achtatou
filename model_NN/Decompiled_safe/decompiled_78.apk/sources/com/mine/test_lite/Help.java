package com.mine.test_lite;

import android.app.Activity;
import android.engine.AddManager;
import android.engine.EngineIO;
import android.engine.UpdateDialog;
import android.os.Bundle;
import android.widget.TextView;

public class Help extends Activity {
    String about;
    AddManager addManager;
    EngineIO engineIO;
    TextView tv1;
    TextView tv2;
    TextView tv3;
    TextView tv4;
    TextView tv5;
    String use1;
    String use2;
    String use3;
    String use4;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.help);
        this.addManager = new AddManager(this);
        this.tv1 = (TextView) findViewById(R.id.TextView_help1);
        this.tv2 = (TextView) findViewById(R.id.TextView_help3);
        this.tv3 = (TextView) findViewById(R.id.TextView03);
        this.tv4 = (TextView) findViewById(R.id.TextView04);
        this.tv5 = (TextView) findViewById(R.id.TextView05);
        this.about = "Get ready to play this addictive Minesweeper Game! The aim of the game is to uncover all the squares that do not contain a mine and mark the flag on the squares containing a mine.";
        this.tv1.setText(this.about);
        this.use1 = "Select 'Start' option.\nThere are three modes : Beginner, Intermediate and Expert.\n Select any one mode.";
        this.use2 = "\n- To start the game, make  few wild guesses.Initially all the squares on the grid are empty, and you need to uncover all the squares that do not contain a mine and Mark the flag on the squares containing a mine.";
        this.use3 = "- When you uncover a square, either it will be blank or there will be some number.These little colored numbers are the clues that will help you to guess how many mines are adjacent to those squares.Use this information & figure out which square has a mine and which doesn't.";
        this.use4 = "- Put a flag on a doubted Square, and if you want to Remove the flag put a question mark on the flag, Even if a single mine is Clicked, you will loose the game. Be fast if you want to Publish your score Globally.";
        this.tv2.setText(this.use1);
        this.tv3.setText(this.use2);
        this.tv4.setText(this.use3);
        this.tv5.setText(this.use4);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (UpdateDialog.exitApp || UpdateDialog.installUpdate) {
            finish();
        }
        this.addManager.init(5);
        AddManager.activityState = "Resumed";
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        AddManager.activityState = "Paused";
    }
}
