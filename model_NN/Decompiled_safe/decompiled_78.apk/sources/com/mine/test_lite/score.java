package com.mine.test_lite;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TabActivity;
import android.database.Cursor;
import android.engine.AddManager;
import android.engine.UpdateDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TabHost;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class score extends TabActivity {
    private AddManager addManager;
    DBHandler dbobj;
    Dialog dialog;
    Handler handler = new Handler() {
        int temp;

        public void handleMessage(Message msg) {
            String[] strArr = new String[10];
            String data = msg.getData().getString("Response");
            System.out.println("*data " + data);
            try {
                String[] a = data.split("#");
                int l = a.length;
                int j = 0;
                for (int i = 1; i <= l / 2; i++) {
                    String name1 = a[j];
                    int j2 = j + 1;
                    int score1 = Integer.parseInt(a[j2]);
                    j = j2 + 1;
                    score.this.name[i] = (TextView) score.this.findViewById(score.this.textNameids[i]);
                    score.this.score[i] = (TextView) score.this.findViewById(score.this.textScoreids[i]);
                    score.this.name[i] = (TextView) score.this.findViewById(score.this.textNameids[i]);
                    score.this.rank[i] = (TextView) score.this.findViewById(score.this.textRankids[i]);
                    score.this.tableRows[i] = (TableRow) score.this.findViewById(score.this.tablerow[i]);
                    score.this.tableRows[i].setVisibility(0);
                    score.this.score[i].setText(Integer.toString(score1));
                    score.this.rank[i].setText(Integer.toString(i));
                    score.this.name[i].setText(name1);
                    this.temp = i;
                }
            } catch (Exception e) {
                System.out.println("exception comes on score " + e);
            }
            for (int i2 = this.temp + 1; i2 <= 5; i2++) {
                score.this.tableRows[i2] = (TableRow) score.this.findViewById(score.this.tablerow[i2]);
                score.this.tableRows[i2].setVisibility(8);
            }
        }
    };
    String level;
    int loopCounter;
    TabHost mTabHost;
    TextView[] name;
    ProgressDialog progressDialog;
    TextView[] rank;
    Button resetButton;
    TextView[] score;
    TableRow[] tableRows;
    int[] tablerow;
    int[] textNameids;
    int[] textRankids;
    int[] textScoreids;

    public void ArrayInitializer() {
        this.textNameids[1] = R.id.R1Name;
        this.textNameids[2] = R.id.R2Name;
        this.textNameids[3] = R.id.R3Name;
        this.textNameids[4] = R.id.R4Name;
        this.textNameids[5] = R.id.R5Name;
        this.textNameids[6] = R.id.R6Name;
        this.textNameids[7] = R.id.R7Name;
        this.textNameids[8] = R.id.R8Name;
        this.textNameids[9] = R.id.R9Name;
        this.textNameids[10] = R.id.R10Name;
        this.textNameids[11] = R.id.R11Name;
        this.textNameids[12] = R.id.R12Name;
        this.textNameids[13] = R.id.R13Name;
        this.textNameids[14] = R.id.R14Name;
        this.textNameids[15] = R.id.R15Name;
        this.textNameids[16] = R.id.R16Name;
        this.textNameids[17] = R.id.R17Name;
        this.textNameids[18] = R.id.R18Name;
        this.textNameids[19] = R.id.R19Name;
        this.textNameids[20] = R.id.R20Name;
        this.textScoreids[1] = R.id.R1Score;
        this.textScoreids[2] = R.id.R2Score;
        this.textScoreids[3] = R.id.R3Score;
        this.textScoreids[4] = R.id.R4Score;
        this.textScoreids[5] = R.id.R5Score;
        this.textScoreids[6] = R.id.R6Score;
        this.textScoreids[7] = R.id.R7Score;
        this.textScoreids[8] = R.id.R8Score;
        this.textScoreids[9] = R.id.R9Score;
        this.textScoreids[10] = R.id.R10Score;
        this.textScoreids[11] = R.id.R11Score;
        this.textScoreids[12] = R.id.R12Score;
        this.textScoreids[13] = R.id.R13Score;
        this.textScoreids[14] = R.id.R14Score;
        this.textScoreids[15] = R.id.R15Score;
        this.textScoreids[16] = R.id.R16Score;
        this.textScoreids[17] = R.id.R17Score;
        this.textScoreids[18] = R.id.R18Score;
        this.textScoreids[19] = R.id.R19Score;
        this.textScoreids[20] = R.id.R20Score;
        this.textRankids[1] = R.id.R1Rank;
        this.textRankids[2] = R.id.R2Rank;
        this.textRankids[3] = R.id.R3Rank;
        this.textRankids[4] = R.id.R4Rank;
        this.textRankids[5] = R.id.R5Rank;
        this.textRankids[6] = R.id.R6Rank;
        this.textRankids[7] = R.id.R7Rank;
        this.textRankids[8] = R.id.R8Rank;
        this.textRankids[9] = R.id.R9Rank;
        this.textRankids[10] = R.id.R10Rank;
        this.textRankids[11] = R.id.R11Rank;
        this.textRankids[12] = R.id.R12Rank;
        this.textRankids[13] = R.id.R13Rank;
        this.textRankids[14] = R.id.R14Rank;
        this.textRankids[15] = R.id.R15Rank;
        this.textRankids[16] = R.id.R16Rank;
        this.textRankids[17] = R.id.R17Rank;
        this.textRankids[18] = R.id.R18Rank;
        this.textRankids[19] = R.id.R19Rank;
        this.textRankids[20] = R.id.R20Rank;
        this.tablerow[1] = R.id.TableRow01;
        this.tablerow[2] = R.id.TableRow02;
        this.tablerow[3] = R.id.TableRow03;
        this.tablerow[4] = R.id.TableRow04;
        this.tablerow[5] = R.id.TableRow05;
        this.tablerow[5] = R.id.TableRow05;
        this.tablerow[6] = R.id.TableRow06;
        this.tablerow[7] = R.id.TableRow07;
        this.tablerow[8] = R.id.TableRow08;
        this.tablerow[9] = R.id.TableRow09;
        this.tablerow[10] = R.id.TableRow10;
        this.tablerow[11] = R.id.TableRow11;
        this.tablerow[12] = R.id.TableRow12;
        this.tablerow[13] = R.id.TableRow13;
        this.tablerow[14] = R.id.TableRow14;
        this.tablerow[15] = R.id.TableRow15;
        this.tablerow[16] = R.id.TableRow16;
        this.tablerow[17] = R.id.TableRow17;
        this.tablerow[18] = R.id.TableRow18;
        this.tablerow[19] = R.id.TableRow19;
        this.tablerow[20] = R.id.TableRow20;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.tabs);
        this.dbobj = new DBHandler(this);
        this.addManager = new AddManager(this);
        this.resetButton = (Button) findViewById(R.id.reset);
        if (getTabHost().getCurrentTab() == 0) {
            this.resetButton.setVisibility(0);
        } else {
            this.resetButton.setVisibility(4);
        }
        this.resetButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (score.this.level.equalsIgnoreCase("easy")) {
                    score.this.dbobj.deleteAllDetailsBegineer();
                    Toast.makeText(score.this.getApplicationContext(), "Data reset successfully", 0).show();
                    score.this.finish();
                } else if (score.this.level.equalsIgnoreCase("medium")) {
                    score.this.dbobj.deleteAllDetailsIntermediate();
                    Toast.makeText(score.this.getApplicationContext(), "Data reset successfully", 0).show();
                    score.this.finish();
                } else if (score.this.level.equalsIgnoreCase("expert")) {
                    score.this.dbobj.deleteAllDetailsExpert();
                    Toast.makeText(score.this.getApplicationContext(), "Data reset successfully", 0).show();
                    score.this.finish();
                }
            }
        });
        this.mTabHost = getTabHost();
        this.name = new TextView[21];
        this.score = new TextView[21];
        this.rank = new TextView[21];
        this.textNameids = new int[21];
        this.textScoreids = new int[21];
        this.textRankids = new int[21];
        this.tablerow = new int[21];
        this.tableRows = new TableRow[21];
        ArrayInitializer();
        this.mTabHost.addTab(this.mTabHost.newTabSpec("local data").setIndicator("Local Data").setContent((int) R.id.TableLayout01));
        this.mTabHost.addTab(this.mTabHost.newTabSpec("global data").setIndicator("Global Data").setContent((int) R.id.TableLayout01));
        this.mTabHost.setCurrentTabByTag("global data");
        showSelectLevelDialogForGlobal();
        this.mTabHost.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                System.out.println("getTabHost().getCurrentTab()  " + score.this.getTabHost().getCurrentTab());
                if (score.this.getTabHost().getCurrentTab() == 0) {
                    score.this.showSelectLevelDialogForLocal();
                    score.this.resetButtonVissibility();
                } else if (score.this.getTabHost().getCurrentTab() == 1) {
                    if (score.this.resetButton.getVisibility() == 0) {
                        score.this.resetButton.setVisibility(4);
                    }
                    score.this.showSelectLevelDialogForGlobal();
                }
            }
        });
        this.mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            public void onTabChanged(String tabId) {
                if (tabId.equalsIgnoreCase("local data")) {
                    score.this.showSelectLevelDialogForLocal();
                } else if (tabId.equalsIgnoreCase("global data")) {
                    if (score.this.resetButton.getVisibility() == 0) {
                        score.this.resetButton.setVisibility(4);
                    }
                    score.this.showSelectLevelDialogForGlobal();
                }
            }
        });
    }

    public void resetButtonVissibility() {
        if (getTabHost().getCurrentTab() == 0) {
            this.resetButton.setVisibility(0);
        } else {
            this.resetButton.setVisibility(4);
        }
    }

    public void showLocalData() {
        if (this.level.equalsIgnoreCase("easy")) {
            int temp = 0;
            Cursor c = this.dbobj.fetchAllScoreDetailsForBegineer();
            if (c.getCount() > 5) {
                this.loopCounter = 5;
            } else {
                this.loopCounter = c.getCount();
            }
            if (c.getCount() > 0 && this.resetButton.getVisibility() == 4) {
                this.resetButton.setVisibility(0);
            }
            for (int i = 1; i <= this.loopCounter; i++) {
                c.moveToNext();
                System.out.println("INSERTED VALUED");
                String name1 = c.getString(1);
                int parseInt = Integer.parseInt(c.getString(0));
                int score1 = Integer.parseInt(c.getString(2));
                this.name[i] = (TextView) findViewById(this.textNameids[i]);
                this.score[i] = (TextView) findViewById(this.textScoreids[i]);
                this.name[i] = (TextView) findViewById(this.textNameids[i]);
                this.rank[i] = (TextView) findViewById(this.textRankids[i]);
                this.tableRows[i] = (TableRow) findViewById(this.tablerow[i]);
                this.tableRows[i].setVisibility(0);
                this.score[i].setText(Integer.toString(score1));
                this.rank[i].setText(Integer.toString(i));
                this.name[i].setText(name1);
                temp = i;
            }
            for (int i2 = this.loopCounter + 1; i2 < 20; i2++) {
                this.tableRows[i2] = (TableRow) findViewById(this.tablerow[i2]);
                this.tableRows[i2].setVisibility(8);
            }
            for (int i3 = temp + 1; i3 <= 5; i3++) {
                this.tableRows[i3] = (TableRow) findViewById(this.tablerow[i3]);
                this.tableRows[i3].setVisibility(8);
            }
            this.mTabHost.setCurrentTabByTag("local data");
        } else if (this.level.equalsIgnoreCase("medium")) {
            int temp2 = 0;
            Cursor c2 = this.dbobj.fetchAllScoreDetailsForIntermediate();
            if (c2.getCount() > 5) {
                this.loopCounter = 5;
            } else {
                this.loopCounter = c2.getCount();
            }
            for (int i4 = 1; i4 <= this.loopCounter; i4++) {
                c2.moveToNext();
                System.out.println("INSERTED VALUED");
                String name12 = c2.getString(1);
                int parseInt2 = Integer.parseInt(c2.getString(0));
                int score12 = Integer.parseInt(c2.getString(2));
                this.name[i4] = (TextView) findViewById(this.textNameids[i4]);
                this.score[i4] = (TextView) findViewById(this.textScoreids[i4]);
                this.name[i4] = (TextView) findViewById(this.textNameids[i4]);
                this.rank[i4] = (TextView) findViewById(this.textRankids[i4]);
                this.tableRows[i4] = (TableRow) findViewById(this.tablerow[i4]);
                this.tableRows[i4].setVisibility(0);
                this.score[i4].setText(Integer.toString(score12));
                this.rank[i4].setText(Integer.toString(i4));
                this.name[i4].setText(name12);
                temp2 = i4;
            }
            for (int i5 = this.loopCounter + 1; i5 < 20; i5++) {
                this.tableRows[i5] = (TableRow) findViewById(this.tablerow[i5]);
                this.tableRows[i5].setVisibility(8);
            }
            for (int i6 = temp2 + 1; i6 <= 5; i6++) {
                this.tableRows[i6] = (TableRow) findViewById(this.tablerow[i6]);
                this.tableRows[i6].setVisibility(8);
            }
            this.mTabHost.setCurrentTabByTag("local data");
        } else if (this.level.equalsIgnoreCase("expert")) {
            int temp3 = 0;
            Cursor c3 = this.dbobj.fetchAllScoreDetailsForExpert();
            if (c3.getCount() > 5) {
                this.loopCounter = 5;
            } else {
                this.loopCounter = c3.getCount();
            }
            for (int i7 = 1; i7 <= this.loopCounter; i7++) {
                c3.moveToNext();
                System.out.println("INSERTED VALUED");
                String name13 = c3.getString(1);
                int parseInt3 = Integer.parseInt(c3.getString(0));
                int score13 = Integer.parseInt(c3.getString(2));
                this.name[i7] = (TextView) findViewById(this.textNameids[i7]);
                this.score[i7] = (TextView) findViewById(this.textScoreids[i7]);
                this.name[i7] = (TextView) findViewById(this.textNameids[i7]);
                this.rank[i7] = (TextView) findViewById(this.textRankids[i7]);
                this.tableRows[i7] = (TableRow) findViewById(this.tablerow[i7]);
                this.tableRows[i7].setVisibility(0);
                this.score[i7].setText(Integer.toString(score13));
                this.rank[i7].setText(Integer.toString(i7));
                this.name[i7].setText(name13);
                temp3 = i7;
            }
            for (int i8 = this.loopCounter + 1; i8 < 20; i8++) {
                this.tableRows[i8] = (TableRow) findViewById(this.tablerow[i8]);
                this.tableRows[i8].setVisibility(8);
            }
            for (int i9 = temp3 + 1; i9 <= 5; i9++) {
                this.tableRows[i9] = (TableRow) findViewById(this.tablerow[i9]);
                this.tableRows[i9].setVisibility(8);
            }
            this.mTabHost.setCurrentTabByTag("local data");
        }
    }

    public void showGlobalData() {
        this.progressDialog = ProgressDialog.show(this, "Processing!", "Please wait..", true);
        new Thread() {
            public void run() {
                super.run();
                String url = null;
                try {
                    if (score.this.level.equalsIgnoreCase("easy")) {
                        url = "http://scms.migital.com/android/score.aspx?id=1050001&top=20";
                    } else if (score.this.level.equalsIgnoreCase("medium")) {
                        url = "http://scms.migital.com/android/score.aspx?id=1050002&top=20";
                    } else if (score.this.level.equalsIgnoreCase("expert")) {
                        url = "http://scms.migital.com/android/score.aspx?id=10500013&top=20";
                    }
                    String scoreData = score.this.getDataFrmUrl(url);
                    System.out.println("after connection  1");
                    Message message = new Message();
                    Bundle bundle = new Bundle();
                    bundle.putString("Response", scoreData);
                    message.setData(bundle);
                    score.this.progressDialog.dismiss();
                    score.this.handler.sendMessage(message);
                } catch (Exception e) {
                    e.printStackTrace();
                    score.this.progressDialog.dismiss();
                }
            }
        }.start();
    }

    public void showSelectLevelDialogForLocal() {
        View v = LayoutInflater.from(this).inflate((int) R.layout.level_for_score_dialog, (ViewGroup) null);
        this.dialog = new Dialog(this);
        this.dialog.setContentView(v);
        this.dialog.setTitle("  Select a level for local score.");
        this.dialog.setCanceledOnTouchOutside(true);
        ((Button) v.findViewById(R.id.Button_score_easy)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                score.this.level = "easy";
                score.this.dialog.dismiss();
                score.this.showLocalData();
            }
        });
        ((Button) v.findViewById(R.id.Button_score_medium)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                score.this.level = "medium";
                score.this.dialog.dismiss();
                score.this.showLocalData();
            }
        });
        ((Button) v.findViewById(R.id.Button_score_expert)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                score.this.level = "expert";
                score.this.dialog.dismiss();
                score.this.showLocalData();
            }
        });
        this.dialog.show();
    }

    public void showSelectLevelDialogForGlobal() {
        View v = LayoutInflater.from(this).inflate((int) R.layout.level_for_score_dialog, (ViewGroup) null);
        this.dialog = new Dialog(this);
        this.dialog.setContentView(v);
        this.dialog.setTitle("  Select a level for global score.");
        this.dialog.setCanceledOnTouchOutside(true);
        ((Button) v.findViewById(R.id.Button_score_easy)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                score.this.level = "easy";
                score.this.dialog.dismiss();
                score.this.showGlobalData();
            }
        });
        ((Button) v.findViewById(R.id.Button_score_medium)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                score.this.level = "medium";
                score.this.dialog.dismiss();
                score.this.showGlobalData();
            }
        });
        ((Button) v.findViewById(R.id.Button_score_expert)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                score.this.level = "expert";
                score.this.dialog.dismiss();
                score.this.showGlobalData();
            }
        });
        this.dialog.show();
    }

    public String getDataFrmUrl(String url) {
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
            conn.setDoInput(true);
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(60000);
            conn.connect();
            System.out.println("after conn.connect");
            InputStream is = conn.getInputStream();
            System.out.println("after conn.connect  1");
            byte[] buffer = new byte[is.available()];
            StringBuilder sb = new StringBuilder();
            while (true) {
                int n = is.read(buffer);
                if (n == -1) {
                    String response = sb.toString();
                    System.out.println("before response  ");
                    return response;
                }
                sb.append(new String(buffer, 0, n));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (UpdateDialog.exitApp || UpdateDialog.installUpdate) {
            finish();
        }
        this.addManager.init(13);
        AddManager.activityState = "Resumed";
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        AddManager.activityState = "Paused";
    }
}
