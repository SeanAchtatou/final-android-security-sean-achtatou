package com.mine.test_lite;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.KeyEvent;

public class MyDialogForBackHandling extends Dialog {
    public static Activity currentActivity;
    int code;
    Context context;

    public MyDialogForBackHandling(Context context2) {
        super(context2);
        this.context = context2;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }
}
