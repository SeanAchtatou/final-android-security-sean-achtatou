package com.mine.test_lite;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.engine.AddManager;
import android.engine.UpdateDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class ScoreSubmit extends Activity {
    Button Both;
    Button Global;
    Button Local;
    AddManager addManager;
    DBHandler db;
    EditText editText;
    Button facebook;
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            ScoreSubmit.this.showPrompt(ScoreSubmit.this.rank);
        }
    };
    String level;
    int posRow;
    ProgressDialog progressDialog;
    String rank;
    int score;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.submit);
        this.score = getIntent().getExtras().getInt("score");
        this.level = getIntent().getExtras().getString("level");
        this.db = new DBHandler(this);
        this.addManager = new AddManager(this);
        this.Global = (Button) findViewById(R.id.global);
        this.Local = (Button) findViewById(R.id.localdata);
        this.Both = (Button) findViewById(R.id.both);
        this.facebook = (Button) findViewById(R.id.facebook);
        this.editText = (EditText) findViewById(R.id.EditText01);
        try {
            this.Global.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    String name = ScoreSubmit.this.editText.getText().toString();
                    if (name == null || name.equals(" ") || name.length() <= 0) {
                        ScoreSubmit.this.showPrompt1("Enter the name of player.");
                    } else {
                        ScoreSubmit.this.showglobaldata(name);
                    }
                }
            });
            this.Local.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    String name = ScoreSubmit.this.editText.getText().toString();
                    if (name == null || name.equals(" ") || name.length() <= 0) {
                        ScoreSubmit.this.showPrompt1("Enter the name of player.");
                    } else {
                        ScoreSubmit.this.submittLocalScore(name);
                    }
                }
            });
            this.Both.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    String name = ScoreSubmit.this.editText.getText().toString();
                    if (name == null || name.equals(" ") || name.length() <= 0) {
                        ScoreSubmit.this.showPrompt1("Enter the name of player.");
                        return;
                    }
                    ScoreSubmit.this.submittLocalScore(name);
                    ScoreSubmit.this.showglobaldata(name);
                }
            });
            this.facebook.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    Intent faceActivity = new Intent(ScoreSubmit.this, ShareOnFacebook.class);
                    faceActivity.putExtra("facebookMessage", "I just Win the Minesweeper game in  " + ScoreSubmit.this.score + " Seconds. beat me !");
                    ScoreSubmit.this.startActivity(faceActivity);
                }
            });
        } catch (Exception e) {
            System.out.println("exception in shareOnFacebook");
        }
    }

    public void submittLocalScore(String name) {
        if (this.level.equalsIgnoreCase("easy")) {
            Cursor c = this.db.fetchAllScoreDetailsForBegineer();
            System.out.println("Cursur afte fetching data " + c);
            System.out.println("Cursur count afte fetching data " + c.getCount());
            if (c.getCount() <= 5) {
                this.db.insertBigneerScore(name, this.score);
                showLocalScoreDialog();
            } else if (cheackdate()) {
                this.db.updateScoreDetailForBegineer((long) this.posRow, name, this.score);
                showLocalScoreDialog();
            }
        } else if (this.level.equalsIgnoreCase("medium")) {
            Cursor c2 = this.db.fetchAllScoreDetailsForIntermediate();
            System.out.println("Cursur afte fetching data " + c2);
            System.out.println("Cursur count afte fetching data " + c2.getCount());
            if (c2.getCount() <= 5) {
                this.db.insertIntermediateScore(name, this.score);
                showLocalScoreDialog();
            } else if (cheackdate()) {
                this.db.updateScoreDetailForIntermediate((long) this.posRow, name, this.score);
                showLocalScoreDialog();
            }
        } else if (this.level.equalsIgnoreCase("expert")) {
            Cursor c3 = this.db.fetchAllScoreDetailsForExpert();
            System.out.println("Cursur afte fetching data " + c3);
            System.out.println("Cursur count afte fetching data " + c3.getCount());
            if (c3.getCount() <= 5) {
                this.db.insertExpertScore(name, this.score);
                showLocalScoreDialog();
            } else if (cheackdate()) {
                this.db.updateScoreDetailForExpert((long) this.posRow, name, this.score);
                showLocalScoreDialog();
            }
        }
    }

    public boolean cheackdate() {
        if (this.level.equalsIgnoreCase("easy")) {
            Cursor c = this.db.fetchAllScoreDetailsForBegineer();
            if (c.getCount() == 0) {
                return true;
            }
            for (int i = c.getCount(); i <= c.getCount(); i++) {
                c.moveToLast();
                int temp = Integer.parseInt(c.getString(2));
                System.out.println("temp is " + temp);
                if (temp > this.score) {
                    this.posRow = Integer.parseInt(c.getString(0));
                    return true;
                }
            }
        } else if (this.level.equalsIgnoreCase("medium")) {
            Cursor c2 = this.db.fetchAllScoreDetailsForIntermediate();
            if (c2.getCount() == 0) {
                return true;
            }
            for (int i2 = c2.getCount(); i2 <= c2.getCount(); i2++) {
                c2.moveToLast();
                int temp2 = Integer.parseInt(c2.getString(2));
                System.out.println("temp is " + temp2);
                if (temp2 > this.score) {
                    this.posRow = Integer.parseInt(c2.getString(0));
                    return true;
                }
            }
        } else if (this.level.equalsIgnoreCase("expert")) {
            Cursor c3 = this.db.fetchAllScoreDetailsForExpert();
            if (c3.getCount() == 0) {
                return true;
            }
            for (int i3 = c3.getCount(); i3 <= c3.getCount(); i3++) {
                c3.moveToLast();
                int temp3 = Integer.parseInt(c3.getString(2));
                System.out.println("temp is " + temp3);
                if (temp3 > this.score) {
                    this.posRow = Integer.parseInt(c3.getString(0));
                    return true;
                }
            }
        }
        return false;
    }

    private void showLocalScoreDialog() {
        AlertDialog prompt = new AlertDialog.Builder(this).create();
        prompt.setTitle("Note:");
        prompt.setMessage("Your Local Score submitted successfully.");
        prompt.setButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dia, int arg1) {
                dia.dismiss();
                ScoreSubmit.this.finish();
                ScoreSubmit.this.startActivity(new Intent(ScoreSubmit.this.getApplicationContext(), MainMenu.class));
            }
        });
        prompt.show();
    }

    public String getDataFrmUrl(String url) {
        long startTime = System.currentTimeMillis();
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
            conn.setDoInput(true);
            conn.setConnectTimeout(60000);
            conn.connect();
            InputStream is = conn.getInputStream();
            byte[] buffer = new byte[is.available()];
            StringBuilder sb = new StringBuilder();
            while (true) {
                int n = is.read(buffer);
                if (n == -1) {
                    return sb.toString();
                }
                sb.append(new String(buffer, 0, n));
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Processing complete in " + ((System.currentTimeMillis() - startTime) / 1000) + " sec");
            return null;
        }
    }

    public void showglobaldata(final String uname) {
        this.progressDialog = ProgressDialog.show(this, "Processing!", "Please wait..", true);
        new Thread() {
            public void run() {
                super.run();
                try {
                    Message message = new Message();
                    String url = null;
                    if (ScoreSubmit.this.level.equalsIgnoreCase("easy")) {
                        url = "http://scms.migital.com/android/score.aspx?id=1050001&score=" + ScoreSubmit.this.score + "&name=" + uname;
                    } else if (ScoreSubmit.this.level.equalsIgnoreCase("medium")) {
                        url = "http://scms.migital.com/android/score.aspx?id=1050002&score=" + ScoreSubmit.this.score + "&name=" + uname;
                    } else if (ScoreSubmit.this.level.equalsIgnoreCase("expert")) {
                        url = "http://scms.migital.com/android/score.aspx?id=10500013&score=" + ScoreSubmit.this.score + "&name=" + uname;
                    }
                    ScoreSubmit.this.setDataFrmUrl(url);
                    System.out.println("getting data");
                    ScoreSubmit.this.rank = ScoreSubmit.this.getDataFrmUrl(url);
                    System.out.println("rank is " + ScoreSubmit.this.rank);
                    ScoreSubmit.this.handler.sendMessage(message);
                } catch (Exception e) {
                    System.out.println("exception comes " + e);
                }
            }
        }.start();
    }

    public void showPrompt(String msg) {
        try {
            this.progressDialog.dismiss();
            AlertDialog prompt = new AlertDialog.Builder(this).create();
            System.out.println("control on th show promt");
            prompt.setTitle("Note:");
            if (msg != null) {
                prompt.setMessage("Your Global rank is " + msg);
            } else {
                prompt.setMessage("Unable to establish connection with the server. Please try later.");
            }
            prompt.setButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dia, int arg1) {
                    dia.dismiss();
                    ScoreSubmit.this.finish();
                    ScoreSubmit.this.startActivity(new Intent(ScoreSubmit.this.getApplicationContext(), MainMenu.class));
                }
            });
            prompt.show();
        } catch (Exception e) {
            System.out.println("exception in the show prompt page");
        }
    }

    /* access modifiers changed from: private */
    public void showPrompt1(String msg) {
        AlertDialog prompt = new AlertDialog.Builder(this).create();
        prompt.setTitle("Note:");
        prompt.setMessage(msg);
        prompt.setButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dia, int arg1) {
                dia.dismiss();
            }
        });
        prompt.show();
    }

    public void setDataFrmUrl(String url) {
        long startTime = System.currentTimeMillis();
        try {
            URL mUrl = new URL(url);
            System.out.println("mUrl = " + mUrl);
            HttpURLConnection conn = (HttpURLConnection) mUrl.openConnection();
            conn.setDoInput(true);
            conn.setConnectTimeout(60000);
            conn.connect();
            System.out.println("connecting");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Processing complete in " + ((System.currentTimeMillis() - startTime) / 1000) + " sec");
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        AddManager.activityState = "Resumed";
        if (UpdateDialog.exitApp || UpdateDialog.installUpdate) {
            finish();
        }
        this.addManager.init(11);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        AddManager.activityState = "Paused";
    }
}
