package com.mine.test_lite;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class mainFace extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        Intent postOnFacebookWallIntent = new Intent(this, ShareOnFacebook.class);
        postOnFacebookWallIntent.putExtra("facebookMessage", getIntent().getExtras().getString("facebookMessage"));
        startActivity(postOnFacebookWallIntent);
    }
}
