package com.mine.test_lite;

import android.app.Activity;
import android.content.Intent;
import android.engine.AddManager;
import android.engine.UpdateDialog;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

public class Options extends Activity {
    AddManager addManager;
    Button button_easy;
    Button button_expert;
    Button button_medium;
    String get_game_level;
    /* access modifiers changed from: private */
    public GameLevelOptions level_game;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.option);
        this.addManager = new AddManager(this);
        this.level_game = new GameLevelOptions(this);
        this.button_easy = (Button) findViewById(R.id.Button_easy);
        this.button_medium = (Button) findViewById(R.id.Button_medium);
        this.button_expert = (Button) findViewById(R.id.Button_hard);
        try {
            this.button_easy.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    Options.this.level_game.setGameLevel("1");
                    Options.this.startActivity(new Intent(Options.this.getApplicationContext(), MineSweeper_Easy.class));
                }
            });
            this.button_medium.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    Options.this.level_game.setGameLevel("2");
                    Options.this.startActivity(new Intent(Options.this.getApplicationContext(), MineSweeper_Medium.class));
                }
            });
            this.button_expert.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    Options.this.level_game.setGameLevel("3");
                    Options.this.startActivity(new Intent(Options.this.getApplicationContext(), MineSweeper_Expert.class));
                }
            });
            this.button_easy.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View arg0, MotionEvent me) {
                    switch (me.getAction()) {
                        case 0:
                            Options.this.setButtonBackGround(1);
                            return false;
                        case 1:
                            Options.this.refreshButtons();
                            return false;
                        default:
                            return false;
                    }
                }
            });
            this.button_medium.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View arg0, MotionEvent me) {
                    switch (me.getAction()) {
                        case 0:
                            Options.this.setButtonBackGround(2);
                            return false;
                        case 1:
                            Options.this.refreshButtons();
                            return false;
                        default:
                            return false;
                    }
                }
            });
            this.button_expert.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View arg0, MotionEvent me) {
                    switch (me.getAction()) {
                        case 0:
                            Options.this.setButtonBackGround(3);
                            return false;
                        case 1:
                            Options.this.refreshButtons();
                            return false;
                        default:
                            return false;
                    }
                }
            });
        } catch (Exception e) {
            System.out.println("exception in shareOnFacebook");
        }
    }

    public void setButtonBackGround(int buttonId) {
        refreshButtons();
        if (buttonId == 1) {
            this.button_easy.setBackgroundResource(R.drawable.select);
        } else if (buttonId == 2) {
            this.button_medium.setBackgroundResource(R.drawable.select);
        } else if (buttonId == 3) {
            this.button_expert.setBackgroundResource(R.drawable.select);
        }
    }

    public void refreshButtons() {
        this.button_easy.setBackgroundResource(R.drawable.unselect);
        this.button_medium.setBackgroundResource(R.drawable.unselect);
        this.button_expert.setBackgroundResource(R.drawable.unselect);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        AddManager.activityState = "Resumed";
        if (UpdateDialog.exitApp || UpdateDialog.installUpdate) {
            finish();
        }
        this.addManager.init(9);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        AddManager.activityState = "Paused";
    }
}
