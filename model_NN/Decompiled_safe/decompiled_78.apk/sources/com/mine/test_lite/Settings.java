package com.mine.test_lite;

import android.app.Activity;
import android.app.Dialog;
import android.engine.AddManager;
import android.engine.UpdateDialog;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class Settings extends Activity {
    AddManager addManager;
    AudioManager amg;
    Button cancel;
    CheckBox checkBox_timeLimit;
    CheckBox checkBox_vibr;
    boolean isDataChanged = false;
    float left = 0.0f;
    MediaPlayer mp;
    int new_progress = 0;
    RadioButton rd_time_10min;
    RadioButton rd_time_2min;
    RadioButton rd_time_5min;
    RadioButton rd_time_off;
    RadioButton rd_vibration_off;
    RadioButton rd_vibration_on;
    RadioGroup rg_time_limit;
    RadioGroup rg_vibration;
    float right = 0.0f;
    Button save;
    int seek_progress;
    int seek_ring_vol;
    String seekbar_progress;
    int sound_value;
    TimeLimit time_limit;
    int time_limit_value;
    VibrationOnOff vibr_mode;
    int vibration_value;
    int volume;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.setting);
        this.addManager = new AddManager(this);
        this.rg_time_limit = (RadioGroup) findViewById(R.id.RadioGroup_time_limit);
        this.rg_vibration = (RadioGroup) findViewById(R.id.RadioGroup_vibration_on_off);
        this.rd_vibration_on = (RadioButton) findViewById(R.id.RadioButton_vibr_on);
        this.rd_vibration_off = (RadioButton) findViewById(R.id.RadioButton_vibr_off);
        this.rd_time_off = (RadioButton) findViewById(R.id.RadioButton_off);
        this.rd_time_2min = (RadioButton) findViewById(R.id.RadioButton_2min);
        this.rd_time_5min = (RadioButton) findViewById(R.id.RadioButton_5min);
        this.rd_time_10min = (RadioButton) findViewById(R.id.RadioButton_10min);
        this.save = (Button) findViewById(R.id.Button_save);
        this.cancel = (Button) findViewById(R.id.Button_cancel);
        this.checkBox_vibr = (CheckBox) findViewById(R.id.CheckBox_vibration);
        this.checkBox_timeLimit = (CheckBox) findViewById(R.id.CheckBox_time_limit);
        this.mp = new MediaPlayer();
        this.amg = (AudioManager) getSystemService("audio");
        this.time_limit = new TimeLimit(this);
        this.vibr_mode = new VibrationOnOff(this);
        try {
            System.out.println("time_limit.getTimeLimit()   " + this.time_limit.getTimeLimit());
            if (this.time_limit.getTimeLimit().equals("0")) {
                this.time_limit_value = 0;
                this.rd_time_off.setChecked(true);
            } else if (this.time_limit.getTimeLimit().equals("2")) {
                this.time_limit_value = 1;
                this.rd_time_2min.setChecked(true);
            } else if (this.time_limit.getTimeLimit().equals("5")) {
                this.time_limit_value = 2;
                this.rd_time_5min.setChecked(true);
            } else if (this.time_limit.getTimeLimit().equals("10")) {
                this.time_limit_value = 3;
                this.rd_time_10min.setChecked(true);
            }
            System.out.println("time_limit.getTimeLimit()1   " + this.vibr_mode.getVibrationMode());
            if (this.vibr_mode.getVibrationMode().equals("1")) {
                this.rd_vibration_on.setChecked(true);
                this.vibration_value = 1;
            } else {
                this.rd_vibration_off.setChecked(true);
                this.vibration_value = 0;
            }
            this.rg_vibration.setVisibility(8);
            this.rd_vibration_on.setVisibility(8);
            this.rd_vibration_off.setVisibility(8);
            this.rg_time_limit.setVisibility(8);
            this.rd_time_off.setVisibility(8);
            this.rd_time_2min.setVisibility(8);
            this.rd_time_5min.setVisibility(8);
            this.rd_time_10min.setVisibility(8);
            this.checkBox_vibr.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
                    if (arg1) {
                        Settings.this.rg_vibration.setVisibility(0);
                        Settings.this.rd_vibration_on.setVisibility(0);
                        Settings.this.rd_vibration_off.setVisibility(0);
                        return;
                    }
                    Settings.this.rg_vibration.setVisibility(8);
                    Settings.this.rd_vibration_on.setVisibility(8);
                    Settings.this.rd_vibration_off.setVisibility(8);
                }
            });
            this.checkBox_timeLimit.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
                    if (arg1) {
                        Settings.this.rg_time_limit.setVisibility(0);
                        Settings.this.rd_time_off.setVisibility(0);
                        Settings.this.rd_time_2min.setVisibility(0);
                        Settings.this.rd_time_5min.setVisibility(0);
                        Settings.this.rd_time_10min.setVisibility(0);
                        return;
                    }
                    Settings.this.rg_time_limit.setVisibility(8);
                    Settings.this.rd_time_off.setVisibility(8);
                    Settings.this.rd_time_2min.setVisibility(8);
                    Settings.this.rd_time_5min.setVisibility(8);
                    Settings.this.rd_time_10min.setVisibility(8);
                }
            });
            this.rg_vibration.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    switch (checkedId) {
                        case R.id.RadioButton_vibr_on:
                            Settings.this.vibration_value = 1;
                            Settings.this.isDataChanged = true;
                            return;
                        case R.id.RadioButton_vibr_off:
                            Settings.this.vibration_value = 0;
                            Settings.this.isDataChanged = true;
                            return;
                        default:
                            return;
                    }
                }
            });
            this.rg_time_limit.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    switch (checkedId) {
                        case R.id.RadioButton_off:
                            Settings.this.time_limit_value = 0;
                            Settings.this.isDataChanged = true;
                            return;
                        case R.id.RadioButton_2min:
                            Settings.this.time_limit_value = 1;
                            Settings.this.isDataChanged = true;
                            return;
                        case R.id.RadioButton_5min:
                            Settings.this.time_limit_value = 2;
                            Settings.this.isDataChanged = true;
                            return;
                        case R.id.RadioButton_10min:
                            Settings.this.time_limit_value = 3;
                            Settings.this.isDataChanged = true;
                            return;
                        default:
                            return;
                    }
                }
            });
            this.save.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    switch (Settings.this.time_limit_value) {
                        case 0:
                            System.out.println("switch case time duration in case 1");
                            Settings.this.time_limit.setTimeLimit("0");
                            break;
                        case 1:
                            System.out.println("switch case time duration in case 2");
                            Settings.this.time_limit.setTimeLimit("2");
                            break;
                        case 2:
                            System.out.println("switch case time duration in case 3");
                            Settings.this.time_limit.setTimeLimit("5");
                            break;
                        case 3:
                            System.out.println("switch case time duration in case 4");
                            Settings.this.time_limit.setTimeLimit("10");
                            break;
                    }
                    switch (Settings.this.vibration_value) {
                        case 0:
                            Settings.this.vibr_mode.setVibrationMode("0");
                            System.out.println("switch case vibration  in case 0");
                            break;
                        case 1:
                            Settings.this.vibr_mode.setVibrationMode("1");
                            System.out.println("switch case vibration  in case 1");
                            break;
                    }
                    Settings.this.isDataChanged = false;
                    Toast.makeText(Settings.this.getApplicationContext(), "Settings have been saved", 0).show();
                    Settings.this.finish();
                }
            });
            this.cancel.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    Settings.this.finish();
                }
            });
        } catch (Exception e) {
            System.out.println("exception in settings");
            e.printStackTrace();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            if (this.isDataChanged) {
                System.out.println("error boolean variable " + this.isDataChanged);
                showExitDialog();
                return false;
            }
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    public void showExitDialog() {
        View v = LayoutInflater.from(this).inflate((int) R.layout.show_exit_dialog, (ViewGroup) null);
        final Dialog exitDialog = new MyDialogForBackHandling(this);
        exitDialog.setTitle("Do you really want to exit ?");
        exitDialog.setContentView(v);
        exitDialog.show();
        ((Button) v.findViewById(R.id.Button01)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Settings.this.finish();
            }
        });
        ((Button) v.findViewById(R.id.Button03)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                exitDialog.dismiss();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (UpdateDialog.exitApp || UpdateDialog.installUpdate) {
            finish();
        }
        this.addManager.init(10);
        AddManager.activityState = "Resumed";
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        AddManager.activityState = "Paused";
    }
}
