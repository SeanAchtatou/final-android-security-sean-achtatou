package com.mine.test_lite;

import android.content.Context;
import android.widget.Button;
import java.util.Vector;

public class Block_Medium extends Button {
    MineSweeper_Medium minesweeper = new MineSweeper_Medium();

    public Block_Medium(Context context) {
        super(context);
    }

    public boolean hasMine() {
        boolean minesIsThere = false;
        int size = MineSweeper_Medium.myMinesList.size();
        if (size == 0) {
            minesIsThere = false;
        } else {
            for (int i = 0; i < size; i++) {
                if (MineSweeper_Medium.myMinesList.get(i).equals(this)) {
                    return true;
                }
            }
        }
        return minesIsThere;
    }

    public boolean isClicked() {
        boolean buttonClicked = false;
        int size = MineSweeper_Medium.myClickedButtonList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Medium.myClickedButtonList.get(i).equals(this)) {
                return true;
            }
            buttonClicked = false;
        }
        return buttonClicked;
    }

    public boolean isLongClicked() {
        boolean buttonLongClicked = false;
        int size = MineSweeper_Medium.myLongClickedList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Medium.myLongClickedList.get(i).equals(this)) {
                return true;
            }
            buttonLongClicked = false;
        }
        return buttonLongClicked;
    }

    public boolean isBlank() {
        boolean buttonBlabk = false;
        int size = MineSweeper_Medium.myBlankList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Medium.myBlankList.get(i).equals(this)) {
                return true;
            }
            buttonBlabk = false;
        }
        return buttonBlabk;
    }

    public boolean hasAnyNumber() {
        boolean buttonNumber = false;
        int size = MineSweeper_Medium.myAllNumberList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Medium.myAllNumberList.get(i).equals(this)) {
                return true;
            }
            buttonNumber = false;
        }
        return buttonNumber;
    }

    public boolean hasOne() {
        boolean buttonOne = false;
        int size = MineSweeper_Medium.myOneNumberList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Medium.myOneNumberList.get(i).equals(this)) {
                return true;
            }
            buttonOne = false;
        }
        return buttonOne;
    }

    public boolean hasTwo() {
        boolean buttonTwo = false;
        int size = MineSweeper_Medium.myTwoNumberList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Medium.myTwoNumberList.get(i).equals(this)) {
                return true;
            }
            buttonTwo = false;
        }
        return buttonTwo;
    }

    public boolean hasThree() {
        boolean buttonThree = false;
        int size = MineSweeper_Medium.myThreeNumberList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Medium.myThreeNumberList.get(i).equals(this)) {
                return true;
            }
            buttonThree = false;
        }
        return buttonThree;
    }

    public boolean hasFour() {
        boolean buttonFour = false;
        int size = MineSweeper_Medium.myFourNumberList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Medium.myFourNumberList.get(i).equals(this)) {
                return true;
            }
            buttonFour = false;
        }
        return buttonFour;
    }

    public boolean hasFive() {
        boolean buttonFive = false;
        int size = MineSweeper_Medium.myFiveNumberList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Medium.myFiveNumberList.get(i).equals(this)) {
                return true;
            }
            buttonFive = false;
        }
        return buttonFive;
    }

    public boolean hasSix() {
        boolean buttonSix = false;
        int size = MineSweeper_Medium.mySixNumberList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Medium.mySixNumberList.get(i).equals(this)) {
                return true;
            }
            buttonSix = false;
        }
        return buttonSix;
    }

    public boolean hasSeven() {
        boolean buttonSeven = false;
        int size = MineSweeper_Medium.mySevenNumberList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Medium.mySevenNumberList.get(i).equals(this)) {
                return true;
            }
            buttonSeven = false;
        }
        return buttonSeven;
    }

    public boolean hasEight() {
        boolean buttonEight = false;
        int size = MineSweeper_Medium.myEightNumberList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Medium.myEightNumberList.get(i).equals(this)) {
                return true;
            }
            buttonEight = false;
        }
        return buttonEight;
    }

    public boolean isFlagged() {
        return true;
    }

    public boolean isCovered() {
        return true;
    }

    public boolean isQuestionMarked() {
        return true;
    }

    public void setBlockAsDisabled(boolean disable_block) {
    }

    public void setQuestionMarked(boolean set_ques_mark) {
    }

    public int getNumberOfMinesInSorrounding() {
        return 10;
    }

    public void setFlagIcon(boolean set_flag_icon) {
    }

    public void setQuestionMarkIcon(boolean set_ques_mark_icon) {
    }

    public void setFlagged(boolean set_flag) {
    }

    public void clearAllIcons() {
    }

    public void OpenBlock() {
    }

    public void plantMine() {
        new Vector<>().add(this);
        System.out.println("b in plant mine ");
    }
}
