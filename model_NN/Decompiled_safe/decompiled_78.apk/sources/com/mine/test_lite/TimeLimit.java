package com.mine.test_lite;

import android.content.Context;
import android.content.ContextWrapper;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class TimeLimit {
    private static String TIME_LIMIT_FILE_NAME = "time_limit.txt";
    ContextWrapper wrapper = null;

    public TimeLimit(Context ctx) {
        this.wrapper = new ContextWrapper(ctx);
        if (!this.wrapper.getFileStreamPath(TIME_LIMIT_FILE_NAME).exists()) {
            createFile();
        }
    }

    public boolean setTimeLimit(String s) {
        boolean flag = false;
        FileOutputStream outputStream = null;
        try {
            outputStream = this.wrapper.openFileOutput(TIME_LIMIT_FILE_NAME, 0);
            outputStream.write(s.getBytes());
            flag = true;
            if (outputStream != null) {
                try {
                    outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {
                }
            }
        } catch (IOException e2) {
            if (outputStream != null) {
                try {
                    outputStream.flush();
                    outputStream.close();
                } catch (IOException e3) {
                }
            }
        } catch (Throwable th) {
            if (outputStream != null) {
                try {
                    outputStream.flush();
                    outputStream.close();
                } catch (IOException e4) {
                }
            }
            throw th;
        }
        return flag;
    }

    public String getTimeLimit() {
        FileInputStream inputStream = null;
        try {
            FileInputStream inputStream2 = this.wrapper.openFileInput(TIME_LIMIT_FILE_NAME);
            byte[] reader = new byte[inputStream2.available()];
            do {
            } while (inputStream2.read(reader) != -1);
            String s = new String(reader);
            if (inputStream2 == null) {
                return s;
            }
            try {
                inputStream2.close();
                return s;
            } catch (IOException e) {
                return s;
            }
        } catch (IOException e2) {
            if (inputStream == null) {
                return null;
            }
            try {
                inputStream.close();
                return null;
            } catch (IOException e3) {
                return null;
            }
        } catch (Throwable th) {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e4) {
                }
            }
            throw th;
        }
    }

    public void createFile() {
        try {
            FileOutputStream outputStream = this.wrapper.openFileOutput(TIME_LIMIT_FILE_NAME, 0);
            outputStream.write("0".getBytes());
            outputStream.close();
        } catch (Exception e) {
        }
        System.out.println("volume file has been created");
    }
}
