package com.mine.test_lite;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.engine.AppConstants;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;

public class ShareOnFacebook extends Activity {
    private static final String APP_ID = "196694483716401";
    private static final String EXPIRES = "expires_in";
    private static final String KEY = "facebook-credentials";
    private static final String[] PERMISSIONS = {"publish_stream"};
    private static final String TOKEN = "access_token";
    /* access modifiers changed from: private */
    public Facebook facebook;
    /* access modifiers changed from: private */
    public String messageToPost;

    public boolean saveCredentials(Facebook facebook2) {
        SharedPreferences.Editor editor = getApplicationContext().getSharedPreferences(KEY, 0).edit();
        editor.putString("access_token", facebook2.getAccessToken());
        editor.putLong("expires_in", facebook2.getAccessExpires());
        return editor.commit();
    }

    public boolean restoreCredentials(Facebook facebook2) {
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(KEY, 0);
        facebook2.setAccessToken(sharedPreferences.getString("access_token", null));
        facebook2.setAccessExpires(sharedPreferences.getLong("expires_in", 0));
        return facebook2.isSessionValid();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.facebook = new Facebook(APP_ID);
        restoreCredentials(this.facebook);
        requestWindowFeature(1);
        setContentView((int) R.layout.facebook_dialog);
        String facebookMessage = getIntent().getStringExtra("facebookMessage");
        if (facebookMessage == null) {
            facebookMessage = "Test wall post for our games";
        }
        this.messageToPost = facebookMessage;
    }

    public void doNotShare(View button) {
        try {
            finish();
        } catch (Exception e) {
        }
    }

    public void showFacebookDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setIcon((int) R.drawable.facebook);
        alertDialog.setTitle("Facebook");
        alertDialog.setMessage("Do you want to publish this on facebook");
        alertDialog.setButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                if (!ShareOnFacebook.this.facebook.isSessionValid()) {
                    ShareOnFacebook.this.loginAndPostToWall();
                } else {
                    ShareOnFacebook.this.postToWall(ShareOnFacebook.this.messageToPost);
                }
            }
        });
        alertDialog.setButton2("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                ShareOnFacebook.this.finish();
            }
        });
        alertDialog.show();
    }

    public void share(View button) {
        try {
            if (!this.facebook.isSessionValid()) {
                loginAndPostToWall();
            } else {
                postToWall(this.messageToPost);
            }
        } catch (Exception e) {
        }
    }

    public void loginAndPostToWall() {
        try {
            this.facebook.authorize(this, PERMISSIONS, new LoginDialogListener());
        } catch (Exception e) {
            System.out.println("exception in shareOnFacebook");
        }
    }

    public void postToWall(String message) {
        Bundle parameters = new Bundle();
        try {
            parameters.putString("message", "");
            parameters.putString("name", getIntent().getExtras().getString("facebookMessage"));
            parameters.putString("link", AppConstants.free_apps_url);
            parameters.putString("description", "This is nice App");
            parameters.putString("picture", "http://scms.migital.com/Android/ProductFile/Minesweeper_1050/MINE_SWEEPER_SMALL_ICON.PNG");
            this.facebook.dialog(this, "stream.publish", parameters, new WallPostDialogListener());
        } catch (Exception e) {
            System.out.println("name not obtained");
        }
    }

    class LoginDialogListener implements Facebook.DialogListener {
        LoginDialogListener() {
        }

        public void onComplete(Bundle values) {
            ShareOnFacebook.this.saveCredentials(ShareOnFacebook.this.facebook);
            if (ShareOnFacebook.this.messageToPost != null) {
                ShareOnFacebook.this.postToWall(ShareOnFacebook.this.messageToPost);
            }
        }

        public void onFacebookError1(FacebookError error) {
            ShareOnFacebook.this.showToast("Authentication with Facebook failed!");
            ShareOnFacebook.this.finish();
        }

        public void onError1(DialogError error) {
            ShareOnFacebook.this.showToast("Authentication with Facebook failed!");
            ShareOnFacebook.this.finish();
        }

        public void onCancel() {
            ShareOnFacebook.this.showToast("Authentication with Facebook cancelled!");
            ShareOnFacebook.this.finish();
        }

        public void onFacebookError(FacebookError e) {
            ShareOnFacebook.this.finish();
        }

        public void onError(DialogError e) {
            ShareOnFacebook.this.finish();
        }
    }

    class WallPostDialogListener implements Facebook.DialogListener {
        WallPostDialogListener() {
        }

        public void onComplete(Bundle values) {
            if (values.getString("post_id") != null) {
                ShareOnFacebook.this.showToast("Message posted to your facebook wall!");
            } else {
                ShareOnFacebook.this.showToast("Wall post cancelled!");
            }
            ShareOnFacebook.this.finish();
        }

        public void onFacebookError(FacebookError e) {
            ShareOnFacebook.this.showToast("Failed to post to wall!");
            e.printStackTrace();
            ShareOnFacebook.this.finish();
        }

        public void onError(DialogError e) {
            ShareOnFacebook.this.showToast("Failed to post to wall!");
            e.printStackTrace();
            ShareOnFacebook.this.finish();
        }

        public void onCancel() {
            ShareOnFacebook.this.showToast("Wall post cancelled!");
            ShareOnFacebook.this.finish();
        }
    }

    /* access modifiers changed from: private */
    public void showToast(String message) {
        finish();
        Toast.makeText(getApplicationContext(), message, 0).show();
    }
}
