package com.mine.test_lite;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.engine.AddManager;
import android.engine.Config;
import android.engine.UpdateDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.util.TypedValue;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class MineSweeper_Easy extends Activity {
    static ArrayList<Block_Easy> myAllNumberList = new ArrayList<>();
    static ArrayList<Block_Easy> myBlankList = new ArrayList<>();
    static ArrayList<Block_Easy> myClickedButtonList = new ArrayList<>();
    static ArrayList<Block_Easy> myEightNumberList = new ArrayList<>();
    static ArrayList<Block_Easy> myFiveNumberList = new ArrayList<>();
    static ArrayList<Block_Easy> myFourNumberList = new ArrayList<>();
    static ArrayList<Block_Easy> myLongClickedList = new ArrayList<>();
    static ArrayList<Block_Easy> myMinesList = new ArrayList<>();
    static ArrayList<Block_Easy> myOneNumberList = new ArrayList<>();
    static ArrayList<Block_Easy> mySevenNumberList = new ArrayList<>();
    static ArrayList<Block_Easy> mySixNumberList = new ArrayList<>();
    static ArrayList<Block_Easy> myThreeNumberList = new ArrayList<>();
    static ArrayList<Block_Easy> myTwoNumberList = new ArrayList<>();
    AddManager addManager;
    AlertDialog alertDialog = null;
    boolean areMinesSet = false;
    /* access modifiers changed from: private */
    public Block_Easy[][] blocks;
    View.OnClickListener clickListener = new View.OnClickListener() {
        public void onClick(View v) {
            String new_button_id;
            String button_id = String.valueOf(v.getId());
            if (button_id.equals("0") || button_id.equals("1") || button_id.equals("2") || button_id.equals("3") || button_id.equals("4") || button_id.equals("5") || button_id.equals("6") || button_id.equals("7") || button_id.equals("8")) {
                new_button_id = "0".concat(button_id);
            } else {
                new_button_id = button_id;
            }
            System.out.println("button_id in click  =" + new_button_id);
            String row_string = new_button_id.substring(0, 1);
            String column_string = new_button_id.substring(1);
            MineSweeper_Easy.this.currentRow = Integer.parseInt(row_string);
            MineSweeper_Easy.this.currentColumn = Integer.parseInt(column_string);
            if (MineSweeper_Easy.this.for_unzoom) {
                MineSweeper_Easy.this.for_zoom = true;
                MineSweeper_Easy.this.for_unzoom = false;
                MineSweeper_Easy.this.setButtonHeightWidthFromZoom(MineSweeper_Easy.this.numberForButtonSize - 3);
                MineSweeper_Easy.this.zoom_button.setBackgroundResource(R.drawable.zoom_out);
            } else if (MineSweeper_Easy.this.isPlayButtonClicked) {
                if (!MineSweeper_Easy.this.isTimerStarted) {
                    MineSweeper_Easy.this.startTimer();
                    MineSweeper_Easy.this.isTimerStarted = true;
                }
                if (!MineSweeper_Easy.this.areMinesSet) {
                    if (MineSweeper_Easy.this.vibr_mode.getVibrationMode().equals("1")) {
                        MineSweeper_Easy.this.vibrator.vibrate(100);
                    }
                    MineSweeper_Easy.this.areMinesSet = true;
                    MineSweeper_Easy.myClickedButtonList.add(MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn]);
                    MineSweeper_Easy.this.setMines(MineSweeper_Easy.this.currentRow, MineSweeper_Easy.this.currentColumn);
                    for (int c_row = 0; c_row < MineSweeper_Easy.this.numberOfRowsInMineField; c_row++) {
                        for (int c_column = 0; c_column < MineSweeper_Easy.this.numberOfRowsInMineField; c_column++) {
                            if (!MineSweeper_Easy.this.blocks[c_row][c_column].hasMine()) {
                                int numOnButton = MineSweeper_Easy.this.setAccordingToNeighbours(c_row, c_column);
                                System.out.println("numOnButton for c_row " + c_row + " and c_column " + c_column + " is " + numOnButton);
                                if (numOnButton == 0) {
                                    MineSweeper_Easy.myBlankList.add(MineSweeper_Easy.this.blocks[c_row][c_column]);
                                } else if (MineSweeper_Easy.this.noWillSet == 1) {
                                    MineSweeper_Easy.myAllNumberList.add(MineSweeper_Easy.this.blocks[c_row][c_column]);
                                    MineSweeper_Easy.myOneNumberList.add(MineSweeper_Easy.this.blocks[c_row][c_column]);
                                } else if (MineSweeper_Easy.this.noWillSet == 2) {
                                    MineSweeper_Easy.myAllNumberList.add(MineSweeper_Easy.this.blocks[c_row][c_column]);
                                    MineSweeper_Easy.myTwoNumberList.add(MineSweeper_Easy.this.blocks[c_row][c_column]);
                                } else if (MineSweeper_Easy.this.noWillSet == 3) {
                                    MineSweeper_Easy.myAllNumberList.add(MineSweeper_Easy.this.blocks[c_row][c_column]);
                                    MineSweeper_Easy.myThreeNumberList.add(MineSweeper_Easy.this.blocks[c_row][c_column]);
                                } else if (MineSweeper_Easy.this.noWillSet == 4) {
                                    MineSweeper_Easy.myAllNumberList.add(MineSweeper_Easy.this.blocks[c_row][c_column]);
                                    MineSweeper_Easy.myFourNumberList.add(MineSweeper_Easy.this.blocks[c_row][c_column]);
                                } else if (MineSweeper_Easy.this.noWillSet == 5) {
                                    MineSweeper_Easy.myAllNumberList.add(MineSweeper_Easy.this.blocks[c_row][c_column]);
                                    MineSweeper_Easy.myFiveNumberList.add(MineSweeper_Easy.this.blocks[c_row][c_column]);
                                } else if (MineSweeper_Easy.this.noWillSet == 6) {
                                    MineSweeper_Easy.myAllNumberList.add(MineSweeper_Easy.this.blocks[c_row][c_column]);
                                    MineSweeper_Easy.mySixNumberList.add(MineSweeper_Easy.this.blocks[c_row][c_column]);
                                } else if (MineSweeper_Easy.this.noWillSet == 7) {
                                    MineSweeper_Easy.myAllNumberList.add(MineSweeper_Easy.this.blocks[c_row][c_column]);
                                    MineSweeper_Easy.mySevenNumberList.add(MineSweeper_Easy.this.blocks[c_row][c_column]);
                                } else if (MineSweeper_Easy.this.noWillSet == 8) {
                                    MineSweeper_Easy.myAllNumberList.add(MineSweeper_Easy.this.blocks[c_row][c_column]);
                                    MineSweeper_Easy.myEightNumberList.add(MineSweeper_Easy.this.blocks[c_row][c_column]);
                                }
                            }
                        }
                    }
                    if (!MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].hasAnyNumber()) {
                        MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].setBackgroundResource(R.drawable.blank1);
                        System.out.println("currentRow in first click " + MineSweeper_Easy.this.currentRow);
                        System.out.println("currentColumn in first click " + MineSweeper_Easy.this.currentColumn);
                        MineSweeper_Easy.this.checkBlankAccordingToNeighbours(MineSweeper_Easy.this.currentRow, MineSweeper_Easy.this.currentColumn);
                    } else if (MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].hasOne()) {
                        MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].setBackgroundResource(R.drawable.button_1);
                    } else if (MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].hasTwo()) {
                        MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].setBackgroundResource(R.drawable.button_2);
                    } else if (MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].hasThree()) {
                        MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].setBackgroundResource(R.drawable.button_3);
                    } else if (MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].hasFour()) {
                        MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].setBackgroundResource(R.drawable.button_4);
                    } else if (MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].hasFive()) {
                        MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].setBackgroundResource(R.drawable.button_5);
                    } else if (MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].hasSix()) {
                        MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].setBackgroundResource(R.drawable.button_6);
                    } else if (MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].hasSeven()) {
                        MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].setBackgroundResource(R.drawable.button_7);
                    } else if (MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].hasEight()) {
                        MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].setBackgroundResource(R.drawable.button_8);
                    }
                } else {
                    System.out.println("myClickedButtonList in else part of first click  " + MineSweeper_Easy.myClickedButtonList.size());
                    if (MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].isClicked()) {
                        System.out.println("in else part is clicked part");
                    } else if (MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].hasMine()) {
                        System.out.println("after second click mine found ");
                        if (MineSweeper_Easy.this.vibr_mode.getVibrationMode().equals("1")) {
                            MineSweeper_Easy.this.vibrator.vibrate(100);
                        }
                        MineSweeper_Easy.this.finishGame();
                    } else {
                        if (MineSweeper_Easy.this.vibr_mode.getVibrationMode().equals("1")) {
                            MineSweeper_Easy.this.vibrator.vibrate(100);
                        }
                        System.out.println("after second click getting neighbours  ");
                        MineSweeper_Easy.myClickedButtonList.add(MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn]);
                        if (!MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].hasAnyNumber()) {
                            if (MineSweeper_Easy.this.vibr_mode.getVibrationMode().equals("1")) {
                                MineSweeper_Easy.this.vibrator.vibrate(100);
                            }
                            MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].setBackgroundResource(R.drawable.blank1);
                            System.out.println("currentRow in second click " + MineSweeper_Easy.this.currentRow);
                            System.out.println("currentColumn in second click " + MineSweeper_Easy.this.currentColumn);
                            MineSweeper_Easy.this.checkBlankAccordingToNeighbours(MineSweeper_Easy.this.currentRow, MineSweeper_Easy.this.currentColumn);
                        } else if (MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].hasOne()) {
                            MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].setBackgroundResource(R.drawable.button_1);
                        } else if (MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].hasTwo()) {
                            MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].setBackgroundResource(R.drawable.button_2);
                        } else if (MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].hasThree()) {
                            MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].setBackgroundResource(R.drawable.button_3);
                        } else if (MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].hasFour()) {
                            MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].setBackgroundResource(R.drawable.button_4);
                        } else if (MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].hasFive()) {
                            MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].setBackgroundResource(R.drawable.button_5);
                        } else if (MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].hasSix()) {
                            MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].setBackgroundResource(R.drawable.button_6);
                        } else if (MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].hasSeven()) {
                            MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].setBackgroundResource(R.drawable.button_7);
                        } else if (MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].hasEight()) {
                            MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].setBackgroundResource(R.drawable.button_8);
                        }
                    }
                }
            } else if (MineSweeper_Easy.this.isFlagButtonClicked) {
                if (!MineSweeper_Easy.this.areMinesSet) {
                    MineSweeper_Easy.this.showPrompt("You can not start the game by Setting a flag.");
                } else if (!MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].isClicked() && MineSweeper_Easy.this.minesToFind > 0) {
                    MineSweeper_Easy.myLongClickedList.add(MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn]);
                    MineSweeper_Easy.myClickedButtonList.add(MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn]);
                    MineSweeper_Easy.this.minesToFind--;
                    MineSweeper_Easy.this.txtMineCount.setText("   ".concat(String.valueOf(MineSweeper_Easy.this.minesToFind)));
                    MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].setBackgroundResource(R.drawable.flag);
                }
            } else if (MineSweeper_Easy.this.isQuesMarkButtonClicked) {
                if (!MineSweeper_Easy.this.areMinesSet) {
                    MineSweeper_Easy.this.showPrompt("You can not start the game by Setting a question Mark.");
                } else if (MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].isClicked() && MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].isLongClicked()) {
                    if (MineSweeper_Easy.this.vibr_mode.getVibrationMode().equals("1")) {
                        MineSweeper_Easy.this.vibrator.vibrate(100);
                    }
                    MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn].setBackgroundResource(R.drawable.ques);
                    MineSweeper_Easy.this.minesToFind++;
                    MineSweeper_Easy.this.txtMineCount.setText("   ".concat(String.valueOf(MineSweeper_Easy.this.minesToFind)));
                    MineSweeper_Easy.myLongClickedList.remove(MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn]);
                    MineSweeper_Easy.myClickedButtonList.remove(MineSweeper_Easy.this.blocks[MineSweeper_Easy.this.currentRow][MineSweeper_Easy.this.currentColumn]);
                }
            }
            if (MineSweeper_Easy.this.checkGameWin()) {
                boolean isTopper = true;
                MineSweeper_Easy.this.winGameCalled = true;
                MineSweeper_Easy.this.imageButton_smily.setBackgroundResource(R.drawable.smile_happy);
                MineSweeper_Easy.this.stopTimer();
                Cursor c = MineSweeper_Easy.this.db.fetchAllScoreDetailsForBegineer();
                if (c.getCount() != 0) {
                    int i = 0;
                    while (true) {
                        if (i >= c.getCount()) {
                            break;
                        }
                        c.moveToNext();
                        if (c.getInt(2) < MineSweeper_Easy.this.secondsPassed) {
                            isTopper = false;
                            break;
                        }
                        i++;
                    }
                } else {
                    isTopper = true;
                }
                if (isTopper) {
                    MineSweeper_Easy.this.showWinDialog("You have the fastest time for beginner level.");
                } else {
                    MineSweeper_Easy.this.showWinDialog("You have won the game in " + MineSweeper_Easy.this.secondsPassed + " seconds");
                }
            }
        }
    };
    Config config;
    int currentColumn;
    int currentRow;
    DBHandler db;
    boolean finishGameCalled = false;
    boolean for_unzoom = true;
    boolean for_zoom = false;
    Handler handler;
    ImageButton imageButton_flag;
    ImageButton imageButton_ques;
    ImageButton imageButton_smily;
    boolean isFlagButtonClicked = false;
    boolean isLongClicked = false;
    boolean isPlayButtonClicked = true;
    boolean isQuesMarkButtonClicked = false;
    boolean isShowWindowButtonClicked = false;
    boolean isTimerStarted = false;
    LinearLayout lowerLayout;
    Context mContext;
    private TableLayout mineField;
    int minesToFind = 10;
    MyTimer mt;
    int noWillSet;
    int numberForButtonSize = 9;
    int numberOfColumnsInMineField = 9;
    int numberOfRowsInMineField = 9;
    boolean onPauseCalled = false;
    int secondsPassed = 0;
    int time_left_for_game;
    TimeLimit time_limit;
    Timer timer;
    int totalNumberOfMines = 10;
    View.OnTouchListener touchListener = new View.OnTouchListener() {
        public boolean onTouch(View arg0, MotionEvent me) {
            if (!MineSweeper_Easy.this.for_zoom) {
                return false;
            }
            switch (me.getAction()) {
                case 0:
                    System.out.println("in down");
                    MineSweeper_Easy.this.imageButton_smily.setBackgroundResource(R.drawable.smile_oopss);
                    return false;
                case 1:
                    System.out.println("in ACTION_UP");
                    MineSweeper_Easy.this.imageButton_smily.setBackgroundResource(R.drawable.smile_normal);
                    return false;
                case 2:
                    System.out.println("in ACTION_MOVE");
                    MineSweeper_Easy.this.imageButton_smily.setBackgroundResource(R.drawable.smile_normal);
                    return false;
                case 3:
                    System.out.println("in ACTION_CANCEL");
                    MineSweeper_Easy.this.imageButton_smily.setBackgroundResource(R.drawable.smile_normal);
                    return false;
                case 4:
                    System.out.println("in ACTION_OUTSIDE");
                    MineSweeper_Easy.this.imageButton_smily.setBackgroundResource(R.drawable.smile_normal);
                    return false;
                default:
                    return false;
            }
        }
    };
    /* access modifiers changed from: private */
    public TextView txtMineCount;
    /* access modifiers changed from: private */
    public TextView txtTimer;
    /* access modifiers changed from: private */
    public Runnable updateTimeElasped = new Runnable() {
        public void run() {
            MineSweeper_Easy.this.txtTimer.setText("   " + Integer.toString(MineSweeper_Easy.this.secondsPassed));
            if (MineSweeper_Easy.this.time_left_for_game != 0 && MineSweeper_Easy.this.secondsPassed == MineSweeper_Easy.this.time_left_for_game) {
                MineSweeper_Easy.this.stopTimer();
                MineSweeper_Easy.this.showPrompt2("Time up!!");
            }
            if (MineSweeper_Easy.this.minesToFind < 0) {
                MineSweeper_Easy.this.finishGame();
                System.out.println("in run method and in if minesToFind<0 condition");
            }
        }
    };
    LinearLayout upperLayout;
    VibrationOnOff vibr_mode;
    Vibrator vibrator;
    boolean winGameCalled = false;
    Button zoom_button;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.main_easy);
        this.noWillSet = 0;
        this.secondsPassed = 0;
        this.handler = new Handler();
        this.txtMineCount = (TextView) findViewById(R.id.MineCount);
        this.txtTimer = (TextView) findViewById(R.id.TimerTextView);
        this.zoom_button = (Button) findViewById(R.id.Button_zoom_in);
        this.imageButton_smily = (ImageButton) findViewById(R.id.ImageButton_smily_types);
        this.imageButton_flag = (ImageButton) findViewById(R.id.ImageButton_flag);
        this.imageButton_ques = (ImageButton) findViewById(R.id.ImageButton_ques);
        this.upperLayout = (LinearLayout) findViewById(R.id.LinearLayout05);
        this.lowerLayout = (LinearLayout) findViewById(R.id.LinearLayout04);
        this.timer = new Timer();
        this.mt = new MyTimer();
        clearAllList();
        this.blocks = (Block_Easy[][]) Array.newInstance(Block_Easy.class, 10, 10);
        this.vibrator = (Vibrator) getSystemService("vibrator");
        this.vibr_mode = new VibrationOnOff(this);
        this.time_limit = new TimeLimit(this);
        this.addManager = new AddManager(this);
        this.config = new Config(this);
        this.mContext = this;
        this.db = new DBHandler(this);
        this.time_left_for_game = Integer.parseInt(this.time_limit.getTimeLimit());
        this.time_left_for_game *= 60;
        for (int row = 0; row < this.numberOfRowsInMineField; row++) {
            for (int column = 0; column < this.numberOfColumnsInMineField; column++) {
                this.blocks[row][column] = new Block_Easy(this);
            }
        }
        try {
            showMineField();
            this.txtMineCount.setText("   " + String.valueOf(this.totalNumberOfMines));
            for (int row2 = 0; row2 < this.numberOfRowsInMineField; row2++) {
                for (int column2 = 0; column2 < this.numberOfColumnsInMineField; column2++) {
                    this.blocks[row2][column2].setOnClickListener(this.clickListener);
                    this.blocks[row2][column2].setOnTouchListener(this.touchListener);
                }
            }
            this.zoom_button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    if (MineSweeper_Easy.this.for_unzoom) {
                        MineSweeper_Easy.this.for_zoom = true;
                        MineSweeper_Easy.this.for_unzoom = false;
                        MineSweeper_Easy.this.setButtonHeightWidthFromZoom(MineSweeper_Easy.this.numberForButtonSize - 3);
                        MineSweeper_Easy.this.zoom_button.setBackgroundResource(R.drawable.zoom_out);
                    } else if (MineSweeper_Easy.this.for_zoom) {
                        MineSweeper_Easy.this.for_unzoom = true;
                        MineSweeper_Easy.this.for_zoom = false;
                        MineSweeper_Easy.this.setButtonHeightWidthFromZoom(MineSweeper_Easy.this.numberForButtonSize);
                        MineSweeper_Easy.this.zoom_button.setBackgroundResource(R.drawable.zoom_in);
                    }
                }
            });
            this.imageButton_smily.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    MineSweeper_Easy.this.showNewGameDialog("Do you want to start a new game?");
                }
            });
            this.imageButton_flag.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    if (MineSweeper_Easy.this.isFlagButtonClicked) {
                        MineSweeper_Easy.this.isFlagButtonClicked = false;
                        MineSweeper_Easy.this.isQuesMarkButtonClicked = false;
                        MineSweeper_Easy.this.isPlayButtonClicked = true;
                        MineSweeper_Easy.this.imageButton_ques.setBackgroundResource(R.drawable.ques_unselect);
                        MineSweeper_Easy.this.imageButton_flag.setBackgroundResource(R.drawable.flag_unselect);
                        return;
                    }
                    MineSweeper_Easy.this.isFlagButtonClicked = true;
                    MineSweeper_Easy.this.isPlayButtonClicked = false;
                    MineSweeper_Easy.this.isQuesMarkButtonClicked = false;
                    MineSweeper_Easy.this.imageButton_ques.setBackgroundResource(R.drawable.ques_unselect);
                    MineSweeper_Easy.this.imageButton_flag.setBackgroundResource(R.drawable.flag_select);
                }
            });
            this.imageButton_ques.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    if (MineSweeper_Easy.this.isQuesMarkButtonClicked) {
                        MineSweeper_Easy.this.isFlagButtonClicked = false;
                        MineSweeper_Easy.this.isQuesMarkButtonClicked = false;
                        MineSweeper_Easy.this.isPlayButtonClicked = true;
                        MineSweeper_Easy.this.imageButton_flag.setBackgroundResource(R.drawable.flag_unselect);
                        MineSweeper_Easy.this.imageButton_ques.setBackgroundResource(R.drawable.ques_unselect);
                        return;
                    }
                    MineSweeper_Easy.this.isFlagButtonClicked = false;
                    MineSweeper_Easy.this.isPlayButtonClicked = false;
                    MineSweeper_Easy.this.isQuesMarkButtonClicked = true;
                    MineSweeper_Easy.this.imageButton_flag.setBackgroundResource(R.drawable.flag_unselect);
                    MineSweeper_Easy.this.imageButton_ques.setBackgroundResource(R.drawable.ques_select);
                }
            });
        } catch (Exception e) {
            System.out.println("exception in minesweeper_easy");
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public int setAccordingToNeighbours(int currentClickedRow, int currentClickedCloumn) {
        int CR = currentClickedRow;
        int CC = currentClickedCloumn;
        this.noWillSet = 0;
        if (currentClickedRow == 8) {
            if (currentClickedCloumn == 0) {
                checkOutNeighbour(currentClickedRow - 1, currentClickedCloumn + 1);
                checkOutNeighbour(CR - 1, CC);
                checkOutNeighbour(CR, CC + 1);
            } else if (currentClickedCloumn == 8) {
                checkOutNeighbour(CR, CC - 1);
                checkOutNeighbour(CR - 1, CC);
                checkOutNeighbour(CR - 1, CC - 1);
            } else {
                checkOutNeighbour(CR - 1, CC - 1);
                checkOutNeighbour(CR - 1, CC + 1);
                checkOutNeighbour(CR - 1, CC);
                checkOutNeighbour(CR, CC - 1);
                checkOutNeighbour(CR, CC + 1);
            }
        } else if (currentClickedRow == 0) {
            if (currentClickedCloumn == 0) {
                checkOutNeighbour(CR, CC + 1);
                checkOutNeighbour(CR + 1, CC + 1);
                checkOutNeighbour(CR + 1, CC);
            } else if (currentClickedCloumn == 8) {
                checkOutNeighbour(CR, CC - 1);
                checkOutNeighbour(CR + 1, CC - 1);
                checkOutNeighbour(CR + 1, CC);
            } else {
                checkOutNeighbour(CR, CC + 1);
                checkOutNeighbour(CR, CC - 1);
                checkOutNeighbour(CR + 1, CC + 1);
                checkOutNeighbour(CR + 1, CC - 1);
                checkOutNeighbour(CR + 1, CC);
            }
        } else if (currentClickedCloumn == 0) {
            checkOutNeighbour(CR, CC + 1);
            checkOutNeighbour(CR + 1, CC + 1);
            checkOutNeighbour(CR + 1, CC);
            checkOutNeighbour(CR - 1, CC + 1);
            checkOutNeighbour(CR - 1, CC);
        } else if (currentClickedCloumn == 8) {
            checkOutNeighbour(CR, CC - 1);
            checkOutNeighbour(CR + 1, CC - 1);
            checkOutNeighbour(CR + 1, CC);
            checkOutNeighbour(CR - 1, CC - 1);
            checkOutNeighbour(CR - 1, CC);
        } else {
            checkOutNeighbour(CR, CC + 1);
            checkOutNeighbour(CR, CC - 1);
            checkOutNeighbour(CR + 1, CC + 1);
            checkOutNeighbour(CR + 1, CC - 1);
            checkOutNeighbour(CR + 1, CC);
            checkOutNeighbour(CR - 1, CC + 1);
            checkOutNeighbour(CR - 1, CC - 1);
            checkOutNeighbour(CR - 1, CC);
        }
        return this.noWillSet;
    }

    public void checkOutNeighbour(int cr, int cc) {
        if (this.blocks[cr][cc].hasMine()) {
            this.noWillSet++;
            return;
        }
        if (this.blocks[cr][cc].isClicked()) {
        }
    }

    /* access modifiers changed from: private */
    public void checkBlankAccordingToNeighbours(int currentClickedRow, int currentClickedCloumn) {
        System.out.println("currentClickedRow in checkBlankAccordingToNeighbours " + currentClickedRow);
        System.out.println("currentClickedCloumn in checkBlankAccordingToNeighbours " + currentClickedCloumn);
        int CR = currentClickedRow;
        int CC = currentClickedCloumn;
        if (currentClickedRow == 8) {
            if (currentClickedCloumn == 0) {
                System.out.println("1");
                checkBlank(CR, CC + 1);
                checkBlank(CR - 1, CC);
                checkBlank(CR - 1, CC + 1);
            } else if (currentClickedCloumn == 8) {
                System.out.println("2");
                checkBlank(CR, CC - 1);
                checkBlank(CR - 1, CC);
                checkBlank(CR - 1, CC - 1);
            } else {
                System.out.println("3");
                checkBlank(CR, CC + 1);
                checkBlank(CR, CC - 1);
                checkBlank(CR - 1, CC);
                checkBlank(CR - 1, CC + 1);
                checkBlank(CR - 1, CC - 1);
            }
        } else if (currentClickedRow == 0) {
            if (currentClickedCloumn == 0) {
                System.out.println("4");
                checkBlank(CR, CC + 1);
                checkBlank(CR + 1, CC + 1);
                checkBlank(CR + 1, CC);
            } else if (currentClickedCloumn == 8) {
                System.out.println("5");
                checkBlank(CR, CC - 1);
                checkBlank(CR + 1, CC - 1);
                checkBlank(CR + 1, CC);
            } else {
                System.out.println("6");
                checkBlank(CR, CC + 1);
                checkBlank(CR, CC - 1);
                checkBlank(CR + 1, CC + 1);
                checkBlank(CR + 1, CC - 1);
                checkBlank(CR + 1, CC);
            }
        } else if (currentClickedCloumn == 0) {
            System.out.println("7");
            checkBlank(CR, CC + 1);
            checkBlank(CR + 1, CC + 1);
            checkBlank(CR + 1, CC);
            checkBlank(CR - 1, CC + 1);
            checkBlank(CR - 1, CC);
        } else if (currentClickedCloumn == 8) {
            System.out.println("8");
            checkBlank(CR, CC - 1);
            checkBlank(CR + 1, CC - 1);
            checkBlank(CR + 1, CC);
            checkBlank(CR - 1, CC - 1);
            checkBlank(CR - 1, CC);
        } else {
            System.out.println("9");
            checkBlank(CR, CC + 1);
            checkBlank(CR, CC - 1);
            checkBlank(CR + 1, CC + 1);
            checkBlank(CR + 1, CC - 1);
            checkBlank(CR + 1, CC);
            checkBlank(CR - 1, CC + 1);
            checkBlank(CR - 1, CC - 1);
            checkBlank(CR - 1, CC);
        }
    }

    public void checkBlank(int cr, int cc) {
        int currentClickedRow = cr;
        int currentClickedCloumn = cc;
        if (this.blocks[currentClickedRow][currentClickedCloumn].isClicked()) {
            System.out.println("is clicked");
        } else if (this.blocks[currentClickedRow][currentClickedCloumn].hasAnyNumber()) {
            myClickedButtonList.add(this.blocks[currentClickedRow][currentClickedCloumn]);
            System.out.println("has number");
            if (this.blocks[currentClickedRow][currentClickedCloumn].hasOne()) {
                this.blocks[currentClickedRow][currentClickedCloumn].setBackgroundResource(R.drawable.button_1);
            } else if (this.blocks[currentClickedRow][currentClickedCloumn].hasTwo()) {
                this.blocks[currentClickedRow][currentClickedCloumn].setBackgroundResource(R.drawable.button_2);
            } else if (this.blocks[currentClickedRow][currentClickedCloumn].hasThree()) {
                this.blocks[currentClickedRow][currentClickedCloumn].setBackgroundResource(R.drawable.button_3);
            } else if (this.blocks[currentClickedRow][currentClickedCloumn].hasFour()) {
                this.blocks[currentClickedRow][currentClickedCloumn].setBackgroundResource(R.drawable.button_4);
            } else if (this.blocks[currentClickedRow][currentClickedCloumn].hasFive()) {
                this.blocks[currentClickedRow][currentClickedCloumn].setBackgroundResource(R.drawable.button_5);
            } else if (this.blocks[currentClickedRow][currentClickedCloumn].hasSix()) {
                this.blocks[currentClickedRow][currentClickedCloumn].setBackgroundResource(R.drawable.button_6);
            } else if (this.blocks[currentClickedRow][currentClickedCloumn].hasSeven()) {
                this.blocks[currentClickedRow][currentClickedCloumn].setBackgroundResource(R.drawable.button_7);
            } else if (this.blocks[currentClickedRow][currentClickedCloumn].hasEight()) {
                this.blocks[currentClickedRow][currentClickedCloumn].setBackgroundResource(R.drawable.button_8);
            }
        } else if (this.blocks[currentClickedRow][currentClickedCloumn].isBlank()) {
            System.out.println("is blank and called again checkBlankAccordingToNeighbours");
            myClickedButtonList.add(this.blocks[currentClickedRow][currentClickedCloumn]);
            this.blocks[currentClickedRow][currentClickedCloumn].setBackgroundResource(R.drawable.blank1);
            checkBlankAccordingToNeighbours(currentClickedRow, currentClickedCloumn);
        }
    }

    private void showMineField() {
        float px;
        String row_button;
        Display display = getWindowManager().getDefaultDisplay();
        int pwidth = display.getWidth();
        int phight = display.getHeight();
        Resources r = getResources();
        if (this.config.getFTYPE().equals("3")) {
            px = TypedValue.applyDimension(1, 105.0f, r.getDisplayMetrics());
        } else {
            px = TypedValue.applyDimension(1, 190.0f, r.getDisplayMetrics());
        }
        for (int row = 0; row < this.numberOfRowsInMineField; row++) {
            this.mineField = (TableLayout) findViewById(R.id.TableLayout01);
            TableRow tableRow = new TableRow(this);
            if (row == 0) {
                row_button = String.valueOf(0);
            } else {
                row_button = String.valueOf(row);
            }
            for (int column = 0; column < this.numberOfColumnsInMineField; column++) {
                String buttonId = row_button.concat(String.valueOf(column));
                System.out.println("buttonId  " + buttonId);
                this.blocks[row][column].setPadding(1, 1, 1, 1);
                this.blocks[row][column].setId(Integer.parseInt(buttonId));
                this.blocks[row][column].setBackgroundResource(R.drawable.first_set);
                tableRow.addView(this.blocks[row][column]);
                if (((float) pwidth) == ((float) phight) - px) {
                    ViewGroup.LayoutParams layoutParams = this.blocks[row][column].getLayoutParams();
                    layoutParams.height = pwidth / 9;
                    layoutParams.width = pwidth / 9;
                    this.blocks[row][column].setLayoutParams(layoutParams);
                } else {
                    int difference = (int) ((((float) phight) - px) - ((float) pwidth));
                    this.lowerLayout.layout(0, difference / 2, 0, 0);
                    this.upperLayout.layout(0, 0, 0, difference / 2);
                    ViewGroup.LayoutParams layoutParams2 = this.blocks[row][column].getLayoutParams();
                    layoutParams2.height = pwidth / 9;
                    layoutParams2.width = pwidth / 9;
                    this.blocks[row][column].setLayoutParams(layoutParams2);
                }
                System.out.println("111px");
            }
            this.mineField.addView(tableRow, new TableLayout.LayoutParams(this.numberOfColumnsInMineField * 7, 7));
        }
    }

    /* access modifiers changed from: private */
    public void setMines(int currentRow2, int currentColumn2) {
        Random rand = new Random();
        int row = 0;
        while (row < this.totalNumberOfMines) {
            int mineRow = rand.nextInt(this.numberOfColumnsInMineField);
            int mineColumn = rand.nextInt(this.numberOfRowsInMineField);
            System.out.println("mineRow  " + mineRow);
            System.out.println("mineColumn  " + mineColumn);
            if (mineRow == currentRow2 && mineColumn == currentColumn2) {
                System.out.println("button is first clicked ");
                row--;
            } else if (this.blocks[mineRow][mineColumn].isClicked()) {
                System.out.println("button is already clicked ");
                row--;
            } else if (this.blocks[mineRow][mineColumn].hasMine()) {
                System.out.println("mine is already there " + this.blocks[mineColumn][mineRow].hasMine());
                row--;
            } else {
                myMinesList.add(this.blocks[mineRow][mineColumn]);
            }
            row++;
        }
    }

    /* access modifiers changed from: private */
    public boolean checkGameWin() {
        for (int row = 0; row < this.numberOfRowsInMineField; row++) {
            for (int column = 0; column < this.numberOfColumnsInMineField; column++) {
                if (!this.blocks[row][column].isClicked()) {
                    System.out.println("In checkGameWin() return false \\\\\\\\\\\\\\\\");
                    return false;
                }
            }
        }
        if (this.minesToFind < 0) {
            return false;
        }
        System.out.println("In checkGameWin() return true \\\\\\\\\\\\\\\\");
        return true;
    }

    public void finishGame() {
        String new_mines_id;
        System.out.println("in finish game method ");
        setButtonHeightWidthFromZoom(this.numberForButtonSize);
        for (int i = 0; i < myMinesList.size(); i++) {
            String minsId = String.valueOf(myMinesList.get(i).getId());
            if (minsId.equals("0") || minsId.equals("1") || minsId.equals("2") || minsId.equals("3") || minsId.equals("4") || minsId.equals("5") || minsId.equals("6") || minsId.equals("7") || minsId.equals("8")) {
                new_mines_id = "0".concat(minsId);
            } else {
                new_mines_id = minsId;
            }
            System.out.println("button_id  =" + new_mines_id);
            String row_string = new_mines_id.substring(0, 1);
            String column_string = new_mines_id.substring(1);
            int mineRow = Integer.parseInt(row_string);
            this.blocks[mineRow][Integer.parseInt(column_string)].setBackgroundResource(R.drawable.mine);
        }
        this.finishGameCalled = true;
        stopTimer();
        this.imageButton_smily.setBackgroundResource(R.drawable.smile_badguess);
        System.out.println("in finish game method2 ");
        showPrompt2("Oops, you opened a Mine !!");
    }

    public void showPrompt2(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon((int) R.drawable.mine_sweeper_small_icon);
        builder.setTitle("Note:");
        builder.setMessage(msg);
        builder.setPositiveButton("Play Again", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                MineSweeper_Easy.this.finish();
                MineSweeper_Easy.this.startActivity(new Intent(MineSweeper_Easy.this.getApplicationContext(), MineSweeper_Easy.class));
            }
        });
        builder.setNegativeButton("Quit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                MineSweeper_Easy.this.alertDialog.dismiss();
                MineSweeper_Easy.this.finish();
            }
        });
        this.alertDialog = builder.create();
        this.alertDialog.setCancelable(false);
        this.alertDialog.show();
    }

    public void showWinDialog(String msg) {
        View v = LayoutInflater.from(this).inflate((int) R.layout.win_dialog, (ViewGroup) null);
        AlertDialog.Builder exitDialogBuilder = new AlertDialog.Builder(this);
        exitDialogBuilder.setTitle("Congratulation");
        exitDialogBuilder.setIcon((int) R.drawable.mine_sweeper_small_icon);
        exitDialogBuilder.setMessage(msg);
        exitDialogBuilder.setView(v);
        final AlertDialog winDialog = exitDialogBuilder.create();
        winDialog.show();
        ((Button) v.findViewById(R.id.Button_win_play_again)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MineSweeper_Easy.this.finish();
                MineSweeper_Easy.this.startActivity(new Intent(MineSweeper_Easy.this.getApplicationContext(), MineSweeper_Easy.class));
            }
        });
        ((Button) v.findViewById(R.id.Button_win_quit)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                winDialog.dismiss();
                MineSweeper_Easy.this.finish();
            }
        });
        ((Button) v.findViewById(R.id.Button_win_submitt_score)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                winDialog.dismiss();
                MineSweeper_Easy.this.finish();
                Intent i = new Intent(MineSweeper_Easy.this.getApplicationContext(), ScoreSubmit.class);
                i.putExtra("score", MineSweeper_Easy.this.secondsPassed);
                i.putExtra("level", "easy");
                MineSweeper_Easy.this.startActivity(i);
            }
        });
        winDialog.setCancelable(false);
    }

    /* access modifiers changed from: private */
    public void showPrompt(String msg) {
        AlertDialog prompt = new AlertDialog.Builder(this).create();
        prompt.setTitle("Note:");
        prompt.setMessage(msg);
        prompt.setButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dia, int arg1) {
                dia.dismiss();
            }
        });
        prompt.show();
    }

    public void startTimer() {
        if (this.secondsPassed == 0) {
            this.timer.schedule(this.mt, 0, 1000);
        } else if (this.onPauseCalled) {
            System.out.println("in resume timer method");
            this.timer.schedule(this.mt, 0, 1000);
        }
    }

    public void stopTimer() {
        this.timer.cancel();
        System.out.println("in stop timer");
    }

    public class MyTimer extends TimerTask {
        public MyTimer() {
        }

        public void run() {
            MineSweeper_Easy.this.secondsPassed++;
            MineSweeper_Easy.this.handler.post(MineSweeper_Easy.this.updateTimeElasped);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        showExitDialog("Do you really want to exit ?");
        return false;
    }

    public void showExitDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon((int) R.drawable.mine_sweeper_small_icon);
        builder.setTitle("Note:");
        builder.setMessage(msg);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                MineSweeper_Easy.this.finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                MineSweeper_Easy.this.alertDialog.dismiss();
            }
        });
        this.alertDialog = builder.create();
        this.alertDialog.setCancelable(false);
        this.alertDialog.show();
    }

    public void showNewGameDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon((int) R.drawable.mine_sweeper_small_icon);
        builder.setTitle("Note:");
        builder.setMessage(msg);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                MineSweeper_Easy.this.finish();
                MineSweeper_Easy.this.startActivity(new Intent(MineSweeper_Easy.this.getApplicationContext(), MineSweeper_Easy.class));
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                MineSweeper_Easy.this.alertDialog.dismiss();
            }
        });
        this.alertDialog = builder.create();
        this.alertDialog.setCancelable(false);
        this.alertDialog.show();
    }

    public void setButtonHeightWidthFromZoom(int number) {
        float px;
        Resources r = getResources();
        Display display = getWindowManager().getDefaultDisplay();
        int pwidth = display.getWidth();
        int phight = display.getHeight();
        if (this.config.getFTYPE().equals("3")) {
            px = TypedValue.applyDimension(1, 105.0f, r.getDisplayMetrics());
        } else {
            px = TypedValue.applyDimension(1, 190.0f, r.getDisplayMetrics());
        }
        for (int i = 0; i < this.numberOfRowsInMineField; i++) {
            for (int j = 0; j < this.numberOfColumnsInMineField; j++) {
                ViewGroup.LayoutParams layoutParams = this.blocks[i][j].getLayoutParams();
                if (((float) pwidth) == ((float) phight) - px) {
                    layoutParams.height = pwidth / number;
                    layoutParams.width = pwidth / number;
                    this.blocks[i][j].setLayoutParams(layoutParams);
                } else {
                    layoutParams.height = pwidth / number;
                    layoutParams.width = pwidth / number;
                    this.blocks[i][j].setLayoutParams(layoutParams);
                }
            }
        }
    }

    public void clearAllList() {
        myMinesList.removeAll(myMinesList);
        myClickedButtonList.retainAll(myClickedButtonList);
        myAllNumberList.removeAll(myAllNumberList);
        myBlankList.retainAll(myBlankList);
        myOneNumberList.removeAll(myOneNumberList);
        myTwoNumberList.retainAll(myTwoNumberList);
        myThreeNumberList.removeAll(myThreeNumberList);
        myFourNumberList.retainAll(myFourNumberList);
        myFiveNumberList.removeAll(myFiveNumberList);
        mySixNumberList.retainAll(mySixNumberList);
        mySevenNumberList.removeAll(mySevenNumberList);
        myEightNumberList.retainAll(myEightNumberList);
        myLongClickedList.retainAll(myEightNumberList);
        myMinesList.clear();
        myClickedButtonList.clear();
        myAllNumberList.clear();
        myBlankList.clear();
        myOneNumberList.clear();
        myTwoNumberList.clear();
        myThreeNumberList.clear();
        myFourNumberList.clear();
        myFiveNumberList.clear();
        mySixNumberList.clear();
        mySevenNumberList.clear();
        myEightNumberList.clear();
        myLongClickedList.clear();
        System.gc();
        System.out.println("myMinesList .size()  " + myMinesList.size());
        System.out.println("myClickedButtonList .size()  " + myClickedButtonList.size());
        System.out.println("myBlankList .size()  " + myBlankList.size());
        System.out.println("myOneNumberList .size()  " + myOneNumberList.size());
        System.out.println("myTwoNumberList .size()  " + myTwoNumberList.size());
        System.out.println("myThreeNumberList .size()  " + myThreeNumberList.size());
        System.out.println("myFourNumberList .size()  " + myFourNumberList.size());
        System.out.println("myMinesList.size()  " + myMinesList.size());
        System.out.println("myFiveNumberList .size()  " + myFiveNumberList.size());
        System.out.println("mySixNumberList .size()  " + mySixNumberList.size());
        System.out.println("mySevenNumberList .size()  " + mySevenNumberList.size());
        System.out.println("myEightNumberList .size()  " + myEightNumberList.size());
        System.out.println("myLongClickedList .size()  " + myLongClickedList.size());
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (this.onPauseCalled && this.isTimerStarted && !this.winGameCalled && !this.finishGameCalled) {
            this.timer = new Timer();
            this.mt = new MyTimer();
            startTimer();
        }
        AddManager.activityState = "Resumed";
        if (UpdateDialog.exitApp || UpdateDialog.installUpdate) {
            finish();
        }
        this.addManager.init(2);
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        AddManager.activityState = "Paused";
        if (this.isTimerStarted) {
            this.timer.cancel();
        }
        this.onPauseCalled = true;
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        clearAllList();
        myMinesList.removeAll(myMinesList);
        myClickedButtonList.retainAll(myClickedButtonList);
        myAllNumberList.removeAll(myAllNumberList);
        myBlankList.retainAll(myBlankList);
        myOneNumberList.removeAll(myOneNumberList);
        myTwoNumberList.retainAll(myTwoNumberList);
        myThreeNumberList.removeAll(myThreeNumberList);
        myFourNumberList.retainAll(myFourNumberList);
        myFiveNumberList.removeAll(myFiveNumberList);
        mySixNumberList.retainAll(mySixNumberList);
        mySevenNumberList.removeAll(mySevenNumberList);
        myEightNumberList.retainAll(myEightNumberList);
        myLongClickedList.retainAll(myEightNumberList);
        myMinesList.clear();
        myClickedButtonList.clear();
        myAllNumberList.clear();
        myBlankList.clear();
        myOneNumberList.clear();
        myTwoNumberList.clear();
        myThreeNumberList.clear();
        myFourNumberList.clear();
        myFiveNumberList.clear();
        mySixNumberList.clear();
        mySevenNumberList.clear();
        myEightNumberList.clear();
        myLongClickedList.clear();
        System.gc();
        System.out.println("myMinesList .size()  " + myMinesList.size());
        System.out.println("myClickedButtonList .size()  " + myClickedButtonList.size());
        System.out.println("myBlankList .size()  " + myBlankList.size());
        System.out.println("myOneNumberList .size()  " + myOneNumberList.size());
        System.out.println("myTwoNumberList .size()  " + myTwoNumberList.size());
        System.out.println("myThreeNumberList .size()  " + myThreeNumberList.size());
        System.out.println("myFourNumberList .size()  " + myFourNumberList.size());
        System.out.println("myMinesList.size()  " + myMinesList.size());
        System.out.println("myFiveNumberList .size()  " + myFiveNumberList.size());
        System.out.println("mySixNumberList .size()  " + mySixNumberList.size());
        System.out.println("mySevenNumberList .size()  " + mySevenNumberList.size());
        System.out.println("myEightNumberList .size()  " + myEightNumberList.size());
        System.out.println("myLongClickedList .size()  " + myLongClickedList.size());
        super.onDestroy();
    }
}
