package com.mine.test_lite;

import android.content.Context;
import android.widget.Button;
import java.util.Vector;

public class Block_Expert extends Button {
    MineSweeper_Expert minesweeper = new MineSweeper_Expert();

    public Block_Expert(Context context) {
        super(context);
    }

    public boolean hasMine() {
        boolean minesIsThere = false;
        int size = MineSweeper_Expert.myMinesList.size();
        if (size == 0) {
            minesIsThere = false;
        } else {
            for (int i = 0; i < size; i++) {
                if (MineSweeper_Expert.myMinesList.get(i).equals(this)) {
                    return true;
                }
            }
        }
        return minesIsThere;
    }

    public boolean isClicked() {
        boolean buttonClicked = false;
        int size = MineSweeper_Expert.myClickedButtonList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Expert.myClickedButtonList.get(i).equals(this)) {
                return true;
            }
            buttonClicked = false;
        }
        return buttonClicked;
    }

    public boolean isLongClicked() {
        boolean buttonLongClicked = false;
        int size = MineSweeper_Expert.myLongClickedList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Expert.myLongClickedList.get(i).equals(this)) {
                return true;
            }
            buttonLongClicked = false;
        }
        return buttonLongClicked;
    }

    public boolean isBlank() {
        boolean buttonBlabk = false;
        int size = MineSweeper_Expert.myBlankList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Expert.myBlankList.get(i).equals(this)) {
                return true;
            }
            buttonBlabk = false;
        }
        return buttonBlabk;
    }

    public boolean hasAnyNumber() {
        boolean buttonNumber = false;
        int size = MineSweeper_Expert.myAllNumberList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Expert.myAllNumberList.get(i).equals(this)) {
                return true;
            }
            buttonNumber = false;
        }
        return buttonNumber;
    }

    public boolean hasOne() {
        boolean buttonOne = false;
        int size = MineSweeper_Expert.myOneNumberList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Expert.myOneNumberList.get(i).equals(this)) {
                return true;
            }
            buttonOne = false;
        }
        return buttonOne;
    }

    public boolean hasTwo() {
        boolean buttonTwo = false;
        int size = MineSweeper_Expert.myTwoNumberList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Expert.myTwoNumberList.get(i).equals(this)) {
                return true;
            }
            buttonTwo = false;
        }
        return buttonTwo;
    }

    public boolean hasThree() {
        boolean buttonThree = false;
        int size = MineSweeper_Expert.myThreeNumberList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Expert.myThreeNumberList.get(i).equals(this)) {
                return true;
            }
            buttonThree = false;
        }
        return buttonThree;
    }

    public boolean hasFour() {
        boolean buttonFour = false;
        int size = MineSweeper_Expert.myFourNumberList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Expert.myFourNumberList.get(i).equals(this)) {
                return true;
            }
            buttonFour = false;
        }
        return buttonFour;
    }

    public boolean hasFive() {
        boolean buttonFive = false;
        int size = MineSweeper_Expert.myFiveNumberList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Expert.myFiveNumberList.get(i).equals(this)) {
                return true;
            }
            buttonFive = false;
        }
        return buttonFive;
    }

    public boolean hasSix() {
        boolean buttonSix = false;
        int size = MineSweeper_Expert.mySixNumberList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Expert.mySixNumberList.get(i).equals(this)) {
                return true;
            }
            buttonSix = false;
        }
        return buttonSix;
    }

    public boolean hasSeven() {
        boolean buttonSeven = false;
        int size = MineSweeper_Expert.mySevenNumberList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Expert.mySevenNumberList.get(i).equals(this)) {
                return true;
            }
            buttonSeven = false;
        }
        return buttonSeven;
    }

    public boolean hasEight() {
        boolean buttonEight = false;
        int size = MineSweeper_Expert.myEightNumberList.size();
        for (int i = 0; i < size; i++) {
            if (MineSweeper_Expert.myEightNumberList.get(i).equals(this)) {
                return true;
            }
            buttonEight = false;
        }
        return buttonEight;
    }

    public boolean isFlagged() {
        return true;
    }

    public boolean isCovered() {
        return true;
    }

    public boolean isQuestionMarked() {
        return true;
    }

    public void setBlockAsDisabled(boolean disable_block) {
    }

    public void setQuestionMarked(boolean set_ques_mark) {
    }

    public int getNumberOfMinesInSorrounding() {
        return 10;
    }

    public void setFlagIcon(boolean set_flag_icon) {
    }

    public void setQuestionMarkIcon(boolean set_ques_mark_icon) {
    }

    public void setFlagged(boolean set_flag) {
    }

    public void clearAllIcons() {
    }

    public void OpenBlock() {
    }

    public void plantMine() {
        new Vector<>().add(this);
        System.out.println("b in plant mine ");
    }
}
