package cmcc.location.a;

import java.io.Serializable;

public class e implements Serializable {

    /* renamed from: int  reason: not valid java name */
    private static final long f87int = 3691748769393957850L;

    /* renamed from: a  reason: collision with root package name */
    private double f196a = 0.0d;

    /* renamed from: do  reason: not valid java name */
    private float f88do = 0.0f;

    /* renamed from: for  reason: not valid java name */
    private double f89for = 0.0d;

    /* renamed from: if  reason: not valid java name */
    private double f90if = 0.0d;

    public double a() {
        return this.f89for;
    }

    public void a(double d) {
        this.f89for = d;
    }

    public void a(float f) {
        this.f88do = f;
    }

    /* renamed from: do  reason: not valid java name */
    public int m60do() {
        return (int) (this.f89for * 1000000.0d);
    }

    /* renamed from: do  reason: not valid java name */
    public void m61do(double d) {
        this.f196a = d;
    }

    /* renamed from: for  reason: not valid java name */
    public double m62for() {
        return this.f196a;
    }

    /* renamed from: if  reason: not valid java name */
    public double m63if() {
        return this.f90if;
    }

    /* renamed from: if  reason: not valid java name */
    public void m64if(double d) {
        this.f90if = d;
    }

    /* renamed from: int  reason: not valid java name */
    public float m65int() {
        return this.f88do;
    }

    /* renamed from: new  reason: not valid java name */
    public int m66new() {
        return (int) (this.f90if * 1000000.0d);
    }
}
