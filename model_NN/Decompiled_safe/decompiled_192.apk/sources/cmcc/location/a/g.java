package cmcc.location.a;

import android.location.Location;
import java.io.Serializable;

public class g implements Serializable {

    /* renamed from: if  reason: not valid java name */
    private static final long f100if = -8602137446681402706L;

    /* renamed from: a  reason: collision with root package name */
    private String f198a = "";
    private int b = 0;

    /* renamed from: byte  reason: not valid java name */
    private String f101byte = "";

    /* renamed from: case  reason: not valid java name */
    private String f102case = "";

    /* renamed from: char  reason: not valid java name */
    private String f103char = "";

    /* renamed from: do  reason: not valid java name */
    private String f104do = "";

    /* renamed from: else  reason: not valid java name */
    private String f105else = "";

    /* renamed from: for  reason: not valid java name */
    private int f106for = 0;

    /* renamed from: goto  reason: not valid java name */
    private Location f107goto = null;

    /* renamed from: int  reason: not valid java name */
    private String f108int = "";

    /* renamed from: long  reason: not valid java name */
    private String f109long = "";

    /* renamed from: new  reason: not valid java name */
    private String f110new = "";

    /* renamed from: try  reason: not valid java name */
    private int f111try = 0;

    /* renamed from: void  reason: not valid java name */
    private String f112void = "";

    public String a() {
        return this.f110new;
    }

    public void a(int i) {
        this.b = i;
    }

    public void a(Location location) {
        this.f107goto = location;
    }

    public void a(String str) {
        this.f102case = str;
    }

    /* renamed from: byte  reason: not valid java name */
    public String m84byte() {
        return this.f198a;
    }

    /* renamed from: byte  reason: not valid java name */
    public void m85byte(String str) {
        this.f101byte = str;
    }

    /* renamed from: case  reason: not valid java name */
    public String m86case() {
        return this.f112void;
    }

    /* renamed from: case  reason: not valid java name */
    public void m87case(String str) {
        this.f104do = str;
    }

    /* renamed from: char  reason: not valid java name */
    public int m88char() {
        return this.f106for;
    }

    /* renamed from: char  reason: not valid java name */
    public void m89char(String str) {
        this.f103char = str;
    }

    /* renamed from: do  reason: not valid java name */
    public int m90do() {
        return this.f111try;
    }

    /* renamed from: do  reason: not valid java name */
    public void m91do(int i) {
        this.f106for = i;
    }

    /* renamed from: do  reason: not valid java name */
    public void m92do(String str) {
        this.f109long = str;
    }

    /* renamed from: else  reason: not valid java name */
    public String m93else() {
        return this.f101byte;
    }

    /* renamed from: for  reason: not valid java name */
    public String m94for() {
        return this.f104do;
    }

    /* renamed from: for  reason: not valid java name */
    public void m95for(String str) {
        this.f105else = str;
    }

    /* renamed from: goto  reason: not valid java name */
    public Location m96goto() {
        return this.f107goto;
    }

    /* renamed from: if  reason: not valid java name */
    public String m97if() {
        return this.f108int;
    }

    /* renamed from: if  reason: not valid java name */
    public void m98if(int i) {
        this.f111try = i;
    }

    /* renamed from: if  reason: not valid java name */
    public void m99if(String str) {
        this.f198a = str;
    }

    /* renamed from: int  reason: not valid java name */
    public String m100int() {
        return this.f102case;
    }

    /* renamed from: int  reason: not valid java name */
    public void m101int(String str) {
        this.f108int = str;
    }

    /* renamed from: long  reason: not valid java name */
    public String m102long() {
        return this.f105else;
    }

    /* renamed from: new  reason: not valid java name */
    public String m103new() {
        return this.f109long;
    }

    /* renamed from: new  reason: not valid java name */
    public void m104new(String str) {
        this.f110new = str;
    }

    /* renamed from: try  reason: not valid java name */
    public int m105try() {
        return this.b;
    }

    /* renamed from: try  reason: not valid java name */
    public void m106try(String str) {
        this.f112void = str;
    }

    /* renamed from: void  reason: not valid java name */
    public String m107void() {
        return this.f103char;
    }
}
