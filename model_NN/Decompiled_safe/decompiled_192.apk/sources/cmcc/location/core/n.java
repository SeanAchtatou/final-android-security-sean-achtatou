package cmcc.location.core;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;
import cmcc.location.a.f;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class n implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private f f221a = null;

    /* renamed from: do  reason: not valid java name */
    private d f170do = null;

    /* renamed from: for  reason: not valid java name */
    private Context f171for = null;

    /* renamed from: if  reason: not valid java name */
    private h f172if = new h();

    /* renamed from: int  reason: not valid java name */
    private String f173int = "DataUploader";

    public n(Context context) {
        if (Looper.myLooper() == null) {
            Looper.prepare();
        }
        Log.d("DataUploader", "New a DataUploader Object!");
        if (context == null) {
            throw new IllegalArgumentException();
        }
        this.f171for = context;
        this.f170do = d.a(context);
        List list = this.f170do.m129new();
        if (list == null) {
            Log.e(this.f173int, "configList is null!");
        } else {
            this.f221a = this.f172if.a(list);
        }
    }

    private String a(ArrayList arrayList, Context context) throws Exception {
        if (arrayList == null || arrayList.size() <= 0) {
            return null;
        }
        if (context == null) {
            throw new IllegalArgumentException();
        }
        c cVar = new c();
        cVar.a("UpRecs");
        cVar.a("1000", cVar.a());
        cVar.m116if("1000101", cVar.a());
        cVar.a(arrayList, cVar.a(), context);
        return cVar.toString();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        return;
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(android.content.Context r5) {
        /*
            r4 = this;
            android.os.Looper r0 = android.os.Looper.myLooper()     // Catch:{ Exception -> 0x001f }
            if (r0 != 0) goto L_0x0009
            android.os.Looper.prepare()     // Catch:{ Exception -> 0x001f }
        L_0x0009:
            if (r5 != 0) goto L_0x0013
            java.lang.String r0 = r4.f173int     // Catch:{ Exception -> 0x001f }
            java.lang.String r1 = "Context is null!"
            android.util.Log.e(r0, r1)     // Catch:{ Exception -> 0x001f }
        L_0x0012:
            return
        L_0x0013:
            cmcc.location.a.f r0 = r4.f221a     // Catch:{ Exception -> 0x001f }
            if (r0 != 0) goto L_0x0028
            java.lang.String r0 = r4.f173int     // Catch:{ Exception -> 0x001f }
            java.lang.String r1 = "cb is null!"
            android.util.Log.e(r0, r1)     // Catch:{ Exception -> 0x001f }
            goto L_0x0012
        L_0x001f:
            r0 = move-exception
            java.lang.String r1 = r4.f173int
            java.lang.String r2 = "Date Upload Error!"
            android.util.Log.e(r1, r2, r0)
            goto L_0x0012
        L_0x0028:
            cmcc.location.core.d r0 = r4.f170do     // Catch:{ Exception -> 0x001f }
            if (r0 != 0) goto L_0x0034
            java.lang.String r0 = r4.f173int     // Catch:{ Exception -> 0x001f }
            java.lang.String r1 = "db is null!"
            android.util.Log.e(r0, r1)     // Catch:{ Exception -> 0x001f }
            goto L_0x0012
        L_0x0034:
            cmcc.location.a.f r0 = r4.f221a     // Catch:{ Exception -> 0x001f }
            int r0 = r0.m75if()     // Catch:{ Exception -> 0x001f }
            if (r0 > 0) goto L_0x0044
            java.lang.String r0 = r4.f173int     // Catch:{ Exception -> 0x001f }
            java.lang.String r1 = "Upload_limit <= 0!"
            android.util.Log.e(r0, r1)     // Catch:{ Exception -> 0x001f }
            goto L_0x0012
        L_0x0044:
            java.lang.String r1 = r4.f173int     // Catch:{ Exception -> 0x001f }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x001f }
            java.lang.String r3 = "Upload_limit = "
            r2.<init>(r3)     // Catch:{ Exception -> 0x001f }
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ Exception -> 0x001f }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x001f }
            android.util.Log.d(r1, r2)     // Catch:{ Exception -> 0x001f }
            java.lang.Class<cmcc.location.core.n> r1 = cmcc.location.core.n.class
            monitor-enter(r1)     // Catch:{ Exception -> 0x001f }
            cmcc.location.core.d r2 = r4.f170do     // Catch:{ all -> 0x0072 }
            java.util.ArrayList r0 = r2.a(r0)     // Catch:{ all -> 0x0072 }
            if (r0 == 0) goto L_0x0069
            boolean r2 = r0.isEmpty()     // Catch:{ all -> 0x0072 }
            if (r2 == 0) goto L_0x0075
        L_0x0069:
            java.lang.String r0 = r4.f173int     // Catch:{ all -> 0x0072 }
            java.lang.String r2 = "PluckDataBean list is empty!"
            android.util.Log.e(r0, r2)     // Catch:{ all -> 0x0072 }
            monitor-exit(r1)     // Catch:{ all -> 0x0072 }
            goto L_0x0012
        L_0x0072:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0072 }
            throw r0     // Catch:{ Exception -> 0x001f }
        L_0x0075:
            boolean r2 = r4.a(r0)     // Catch:{ all -> 0x0072 }
            if (r2 == 0) goto L_0x0080
            cmcc.location.core.d r2 = r4.f170do     // Catch:{ all -> 0x0072 }
            r2.m128if(r0)     // Catch:{ all -> 0x0072 }
        L_0x0080:
            monitor-exit(r1)     // Catch:{ all -> 0x0072 }
            goto L_0x0012
        */
        throw new UnsupportedOperationException("Method not decompiled: cmcc.location.core.n.a(android.content.Context):void");
    }

    private boolean a(ArrayList arrayList) throws Exception {
        String str;
        if (arrayList == null || arrayList.size() <= 0) {
            return false;
        }
        String a2 = a(arrayList, this.f171for);
        if (a2 == null || "".equals(a2)) {
            return false;
        }
        byte[] bytes = b.m112if(a2).getBytes();
        URL url = this.f172if.m145if(this.f221a.m70do(), this.f221a.m80new());
        ConnectivityManager connectivityManager = (ConnectivityManager) this.f171for.getSystemService("connectivity");
        if (connectivityManager == null) {
            LogUtil.getInstance().log(" 获取ConnectivityManager失败");
            return false;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            LogUtil.getInstance().log(" 获取ActiveNetworkInfo失败");
            return false;
        }
        HttpURLConnection a3 = this.f172if.a(url, activeNetworkInfo);
        if (a3 == null) {
            LogUtil.getInstance().log(" 获取网络连接失败");
            Toast.makeText(this.f171for, "获取网络连接失败,请确定网络配置", 1).show();
            return false;
        }
        a3.setDoOutput(true);
        a3.setDoInput(true);
        a3.setConnectTimeout(6000);
        a3.setReadTimeout(10000);
        a3.setRequestMethod("POST");
        a3.setRequestProperty("Content-Length", String.valueOf(bytes.length));
        a3.setRequestProperty("Content-Type", e.u);
        OutputStream outputStream = a3.getOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
        dataOutputStream.write(bytes);
        dataOutputStream.flush();
        dataOutputStream.close();
        outputStream.close();
        int responseCode = a3.getResponseCode();
        InputStream inputStream = a3.getInputStream();
        byte[] bArr = new byte[512];
        StringBuffer stringBuffer = new StringBuffer();
        if (responseCode == 200) {
            while (inputStream.read(bArr) > 0) {
                stringBuffer.append(new String(bArr).trim());
            }
            str = stringBuffer.toString();
        } else {
            str = "Web服务器异常(Response error)";
        }
        inputStream.close();
        a3.disconnect();
        return e.t.equals(this.f172if.m144if(this.f172if.m136do(str)).m58if());
    }

    public void run() {
        Log.d("DataUploader.run", "Upload Data Start!");
        long currentTimeMillis = System.currentTimeMillis();
        a(this.f171for);
        Log.d("DataUploader.run", "Upload Cost Time : " + this.f172if.a(currentTimeMillis) + " (ms)");
    }
}
