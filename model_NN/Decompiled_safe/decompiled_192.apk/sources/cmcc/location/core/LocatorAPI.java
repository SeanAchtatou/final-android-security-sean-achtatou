package cmcc.location.core;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;
import cmcc.location.a.a;
import cmcc.location.a.f;
import cmcc.location.core.a.c;
import com.mapabc.mapapi.LocationManagerProxy;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LocatorAPI {

    /* renamed from: int  reason: not valid java name */
    private static LocatorAPI f113int = null;

    /* renamed from: a  reason: collision with root package name */
    private WifiManager f200a = null;

    /* renamed from: do  reason: not valid java name */
    private int f114do = 0;

    /* renamed from: for  reason: not valid java name */
    private Intent f115for = null;

    /* renamed from: if  reason: not valid java name */
    private long f116if = 300000;

    /* renamed from: new  reason: not valid java name */
    private long f117new = 0;
    /* access modifiers changed from: private */

    /* renamed from: try  reason: not valid java name */
    public Context f118try = null;

    private LocatorAPI(Context context) {
        this.f118try = context;
    }

    public static LocatorAPI getInstance(Context context) {
        if (f113int == null) {
            f113int = new LocatorAPI(context);
        }
        return f113int;
    }

    public int finit() {
        if (this.f114do == 0) {
            this.f200a.setWifiEnabled(false);
        }
        if (this.f118try == null) {
            return -1;
        }
        if (this.f115for != null) {
            this.f118try.stopService(this.f115for);
        }
        return 0;
    }

    public Location fppLocate(String str, String str2, String str3) {
        String str4;
        try {
            if (!(this.f118try == null || this.f115for == null || str == null)) {
                if (!"".equals(str) && str2 != null && !"".equals(str2)) {
                    String str5 = (str3 == null || "".equals(str3)) ? "Point" : "area".equals(str3.toLowerCase().trim()) ? "Area" : "hybrid".equals(str3.toLowerCase().trim()) ? e.k : "Point";
                    if (Looper.myLooper() == null) {
                        Looper.prepare();
                    }
                    h hVar = new h();
                    Location location = hVar.m131byte(this.f118try);
                    if (!"point".equals(str5.toLowerCase()) || location == null) {
                        Log.d("LocationUtil.fppLocate", "*****************Do Fpp Locate Start**************");
                        List list = d.a(this.f118try).m129new();
                        if (list == null) {
                            Log.d("LocationUtil.fppLocate", "Upload Will be started!");
                            if (System.currentTimeMillis() - this.f117new >= this.f116if) {
                                this.f117new = System.currentTimeMillis();
                                new Thread(new n(this.f118try)).start();
                            }
                            return null;
                        }
                        f a2 = hVar.a(list);
                        URL url = hVar.m145if(a2.m68case(), a2.m80new());
                        ConnectivityManager connectivityManager = (ConnectivityManager) this.f118try.getSystemService("connectivity");
                        if (connectivityManager == null) {
                            LogUtil.getInstance().log(" 获取ConnectivityManager失败");
                            Log.d("LocationUtil.fppLocate", "Upload Will be started!");
                            if (System.currentTimeMillis() - this.f117new >= this.f116if) {
                                this.f117new = System.currentTimeMillis();
                                new Thread(new n(this.f118try)).start();
                            }
                            return null;
                        }
                        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                        if (activeNetworkInfo == null) {
                            LogUtil.getInstance().log(" 获取ActiveNetworkInfo失败");
                            Log.d("LocationUtil.fppLocate", "Upload Will be started!");
                            if (System.currentTimeMillis() - this.f117new >= this.f116if) {
                                this.f117new = System.currentTimeMillis();
                                new Thread(new n(this.f118try)).start();
                            }
                            return null;
                        }
                        HttpURLConnection a3 = hVar.a(url, activeNetworkInfo);
                        if (a3 == null) {
                            LogUtil.getInstance().log(" 获取网络连接失败");
                            Toast.makeText(this.f118try, "获取网络连接失败,请确定网络配置", 1).show();
                            Log.d("LocationUtil.fppLocate", "Upload Will be started!");
                            if (System.currentTimeMillis() - this.f117new >= this.f116if) {
                                this.f117new = System.currentTimeMillis();
                                new Thread(new n(this.f118try)).start();
                            }
                            return null;
                        }
                        a3.setDoOutput(true);
                        a3.setDoInput(true);
                        a3.setConnectTimeout(6000);
                        a3.setReadTimeout(10000);
                        a3.setRequestMethod("POST");
                        c cVar = new c();
                        cVar.a("PosReq_FC");
                        cVar.a(str, cVar.a());
                        cVar.m116if(str2, cVar.a());
                        cVar.a(new Date(), cVar.a());
                        cVar.m117if(cVar.a(), this.f118try);
                        cVar.a(cVar.a(), this.f118try);
                        ArrayList arrayList = hVar.m146if(this.f118try);
                        if (arrayList != null) {
                            cVar.m119if(cVar.a(), (String) arrayList.get(0));
                            cVar.a(cVar.a(), (String) arrayList.get(1));
                        }
                        cVar.m115for(cVar.a(), str5);
                        String cVar2 = cVar.toString();
                        Log.d("LocationUtil.fppLocate", "ReqStr : " + cVar2);
                        byte[] bytes = cVar2.getBytes();
                        a3.setRequestProperty("Content-Length", String.valueOf(bytes.length));
                        a3.setRequestProperty("Content-Type", e.f137do);
                        long currentTimeMillis = System.currentTimeMillis();
                        OutputStream outputStream = a3.getOutputStream();
                        DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
                        dataOutputStream.write(bytes);
                        dataOutputStream.flush();
                        dataOutputStream.close();
                        outputStream.close();
                        int responseCode = a3.getResponseCode();
                        InputStream inputStream = a3.getInputStream();
                        byte[] bArr = new byte[512];
                        StringBuffer stringBuffer = new StringBuffer();
                        if (responseCode == 200) {
                            while (inputStream.read(bArr) > 0) {
                                stringBuffer.append(new String(bArr).trim());
                            }
                            str4 = stringBuffer.toString();
                        } else {
                            str4 = "Web服务器异常(Response error)";
                        }
                        inputStream.close();
                        a3.disconnect();
                        long a4 = hVar.a(currentTimeMillis);
                        Log.d("LocationUtil.fppLocate", "Response Content is " + str4);
                        a aVar = hVar.m135do(hVar.m136do(str4));
                        if (aVar == null) {
                            Log.d("LocationUtil.fppLocate", "Upload Will be started!");
                            if (System.currentTimeMillis() - this.f117new >= this.f116if) {
                                this.f117new = System.currentTimeMillis();
                                new Thread(new n(this.f118try)).start();
                            }
                            return null;
                        } else if (!e.t.equals(aVar.m58if())) {
                            Log.d("LocationUtil.fppLocate", "Upload Will be started!");
                            if (System.currentTimeMillis() - this.f117new >= this.f116if) {
                                this.f117new = System.currentTimeMillis();
                                new Thread(new n(this.f118try)).start();
                            }
                            return null;
                        } else {
                            Location location2 = new Location(e.f143long);
                            String str6 = aVar.m44char();
                            if ("point".equals(str6.toLowerCase()) || "hybrid".equals(str6.toLowerCase())) {
                                location2.setLatitude(Double.parseDouble(aVar.m46do()));
                                location2.setLongitude(Double.parseDouble(aVar.m48for()));
                                location2.setAccuracy(Float.valueOf(aVar.m42case()).floatValue());
                                String str7 = aVar.m40byte();
                                if (str7 != null && !"".equals(str7)) {
                                    location2.setAltitude(Double.parseDouble(str7));
                                }
                            }
                            Bundle bundle = new Bundle();
                            bundle.putString(e.d, aVar.m50int());
                            bundle.putString(e.c, aVar.m58if());
                            bundle.putLong(e.f, a4);
                            bundle.putString("PosLevel", str6);
                            if ("area".equals(str6.toLowerCase()) || "hybrid".equals(str6.toLowerCase())) {
                                bundle.putString("code", aVar.m54try());
                                bundle.putString("addr", aVar.m52new());
                            }
                            location2.setExtras(bundle);
                            Log.d("LocationUtil.fppLocate", "Upload Will be started!");
                            if (System.currentTimeMillis() - this.f117new >= this.f116if) {
                                this.f117new = System.currentTimeMillis();
                                new Thread(new n(this.f118try)).start();
                            }
                            return location2;
                        }
                    } else {
                        Bundle bundle2 = new Bundle();
                        bundle2.putString(e.d, "GPS");
                        bundle2.putString(e.c, e.t);
                        bundle2.putLong(e.f, 1);
                        location.setExtras(bundle2);
                        Log.d("LocationUtil.fppLocate", "Upload Will be started!");
                        if (System.currentTimeMillis() - this.f117new < this.f116if) {
                            return location;
                        }
                        this.f117new = System.currentTimeMillis();
                        new Thread(new n(this.f118try)).start();
                        return location;
                    }
                }
            }
            Log.d("LocationUtil.fppLocate", "Upload Will be started!");
            if (System.currentTimeMillis() - this.f117new >= this.f116if) {
                this.f117new = System.currentTimeMillis();
                new Thread(new n(this.f118try)).start();
            }
            return null;
        } catch (Exception e) {
            LogUtil.getInstance().log("LocatorAPI.getlocation 获取失败：" + e.toString());
            Log.d("LocationUtil.fppLocate", "Upload Will be started!");
            if (System.currentTimeMillis() - this.f117new >= this.f116if) {
                this.f117new = System.currentTimeMillis();
                new Thread(new n(this.f118try)).start();
            }
            return null;
        } catch (Throwable th) {
            Log.d("LocationUtil.fppLocate", "Upload Will be started!");
            if (System.currentTimeMillis() - this.f117new >= this.f116if) {
                this.f117new = System.currentTimeMillis();
                new Thread(new n(this.f118try)).start();
            }
            throw th;
        }
    }

    public int init() {
        if (this.f118try == null) {
            return 0;
        }
        if (!((LocationManager) this.f118try.getSystemService(LocationManagerProxy.KEY_LOCATION_CHANGED)).isProviderEnabled(LocationManagerProxy.GPS_PROVIDER)) {
            new AlertDialog.Builder(this.f118try).setTitle("请开启GPS").setMessage("该应用需要使用GPS以便在快速的粗精度定位后，对您的位置进行修正，请开启GPS。\r\n 另外由于需要用到WIFI功能进行WIFI定位，因此系统将自动开启您的WIFI功能\r\n 点击确定按钮完成GPS设置。").setNegativeButton("取消", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            }).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    LocatorAPI.this.f118try.startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
                }
            }).show();
        }
        this.f200a = (WifiManager) this.f118try.getSystemService("wifi");
        this.f114do = this.f200a.getWifiState();
        if (this.f114do == 1 || this.f114do == 0 || this.f114do == 4) {
            this.f200a.setWifiEnabled(true);
            this.f114do = 0;
        } else {
            this.f114do = 1;
        }
        if (this.f118try == null) {
            return -1;
        }
        if (this.f115for != null) {
            return 0;
        }
        this.f115for = new Intent(this.f118try, c.class);
        return this.f118try.startService(this.f115for) != null ? 0 : -1;
    }

    public boolean isLocRunning() {
        if (this.f118try == null || this.f115for == null) {
            LogUtil.getInstance().log("Context或者S为空！");
            return false;
        }
        ArrayList arrayList = (ArrayList) ((ActivityManager) this.f118try.getSystemService("activity")).getRunningServices(50);
        for (int i = 0; i < arrayList.size(); i++) {
            if (((ActivityManager.RunningServiceInfo) arrayList.get(i)).service.getClassName().equals("cmcc.location.core.a.c")) {
                LogUtil.getInstance().log("LocationService  is Running: " + c.class);
                return true;
            }
        }
        return false;
    }
}
