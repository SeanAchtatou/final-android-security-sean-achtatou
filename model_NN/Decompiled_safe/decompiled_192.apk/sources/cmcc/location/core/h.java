package cmcc.location.core;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import android.widget.Toast;
import cmcc.location.a.a;
import cmcc.location.a.c;
import cmcc.location.a.d;
import cmcc.location.a.f;
import cmcc.location.a.g;
import cmcc.location.b.b;
import com.mapabc.mapapi.LocationManagerProxy;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Pattern;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class h {

    /* renamed from: a  reason: collision with root package name */
    private int f215a = 0;

    /* renamed from: byte  reason: not valid java name */
    private int f151byte = 80;

    /* renamed from: case  reason: not valid java name */
    private Location f152case = new Location("temp");

    /* renamed from: char  reason: not valid java name */
    public Handler f153char = new Handler() {
        public void handleMessage(Message message) {
            super.handleMessage(message);
            a.a((Context) message.obj).m111int();
        }
    };

    /* renamed from: do  reason: not valid java name */
    private int f154do = 10;

    /* renamed from: for  reason: not valid java name */
    private int f155for = 0;

    /* renamed from: if  reason: not valid java name */
    private int f156if = 10;

    /* renamed from: int  reason: not valid java name */
    private String f157int = "";

    /* renamed from: new  reason: not valid java name */
    private int f158new = 0;

    /* renamed from: try  reason: not valid java name */
    private boolean f159try = false;

    private NamedNodeMap a(Document document, String str, boolean z) throws Exception {
        if (m155try(str)) {
            throw new IllegalArgumentException();
        }
        Element documentElement = document.getDocumentElement();
        if (documentElement == null) {
            return null;
        }
        NodeList elementsByTagName = documentElement.getElementsByTagName(str);
        if (!z || (elementsByTagName != null && elementsByTagName.getLength() > 0)) {
            Node item = elementsByTagName.item(0);
            if (z) {
                if (item == null) {
                    throw new Exception(String.valueOf(str) + " Node is Null!");
                }
            } else if (item == null) {
                return null;
            }
            return item.getAttributes();
        }
        throw new Exception(String.valueOf(str) + " Node Must Be Contained!");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cmcc.location.core.h.a(org.w3c.dom.Document, java.lang.String, boolean):org.w3c.dom.NamedNodeMap
     arg types: [org.w3c.dom.Document, java.lang.String, int]
     candidates:
      cmcc.location.core.h.a(android.content.Context, java.lang.String, int):void
      cmcc.location.core.h.a(org.w3c.dom.Document, java.lang.String, boolean):org.w3c.dom.NamedNodeMap */
    private void a(Document document, d dVar) throws Exception {
        if (document == null || dVar == null) {
            throw new IllegalArgumentException();
        }
        NamedNodeMap a2 = a(document, "Result", true);
        if (a2 == null || a2.getLength() <= 0) {
            throw new Exception("Result Node Must Has Attribute");
        }
        Node namedItem = a2.getNamedItem(e.c);
        if (namedItem == null) {
            throw new Exception("The ErrCode Attribute Must Be Contain!");
        }
        dVar.m59if(namedItem.getNodeValue());
        Node namedItem2 = a2.getNamedItem("ErrDesc");
        if (namedItem2 != null) {
            dVar.a(namedItem2.getNodeValue());
        }
    }

    public int a(int i) {
        return (i * 2) - 113;
    }

    public long a(long j) {
        return System.currentTimeMillis() - j;
    }

    public Location a(Context context, String str, String str2, String str3) throws b {
        String str4;
        if (context == null || m155try(str) || m155try(str2)) {
            throw new IllegalArgumentException();
        }
        Log.d("LocationUtil.fppLocate", "*****************Do Fpp Locate Start**************");
        try {
            f a2 = a(d.a(context).m129new());
            if (Looper.myLooper() == null) {
                Looper.prepare();
            }
            URL url = m145if(a2.m68case(), a2.m80new());
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager == null) {
                Log.d("LocationUtil.fppLocate", "Upload Will be started!");
                try {
                    new Thread(new n(context)).start();
                } catch (Throwable th) {
                    LogUtil.getInstance().log("fppLocate:" + th.toString());
                    Log.e("LocationUtil.fppLocate", "Upload date failed!", th);
                }
                return null;
            }
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo == null) {
                Log.d("LocationUtil.fppLocate", "Upload Will be started!");
                try {
                    new Thread(new n(context)).start();
                } catch (Throwable th2) {
                    LogUtil.getInstance().log("fppLocate:" + th2.toString());
                    Log.e("LocationUtil.fppLocate", "Upload date failed!", th2);
                }
                return null;
            }
            HttpURLConnection a3 = a(url, activeNetworkInfo);
            if (a3 == null) {
                Log.d("LocationUtil.fppLocate", "Upload Will be started!");
                try {
                    new Thread(new n(context)).start();
                } catch (Throwable th3) {
                    LogUtil.getInstance().log("fppLocate:" + th3.toString());
                    Log.e("LocationUtil.fppLocate", "Upload date failed!", th3);
                }
                return null;
            }
            a3.setDoOutput(true);
            a3.setDoInput(true);
            a3.setConnectTimeout(6000);
            a3.setReadTimeout(10000);
            a3.setRequestMethod("POST");
            c cVar = new c();
            cVar.a(str);
            cVar.a(str2, cVar.a());
            cVar.m116if(str3, cVar.a());
            cVar.a(new Date(), cVar.a());
            cVar.m117if(cVar.a(), context);
            cVar.a(cVar.a(), context);
            ArrayList arrayList = m146if(context);
            if (arrayList != null) {
                cVar.m119if(cVar.a(), (String) arrayList.get(0));
                cVar.a(cVar.a(), (String) arrayList.get(1));
            }
            String cVar2 = cVar.toString();
            Log.d("LocationUtil.fppLocate", "ReqStr : " + cVar2);
            byte[] bytes = cVar2.getBytes();
            a3.setRequestProperty("Content-Length", String.valueOf(bytes.length));
            a3.setRequestProperty("Content-Type", e.f137do);
            long j = m143if();
            OutputStream outputStream = a3.getOutputStream();
            DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
            dataOutputStream.write(bytes);
            dataOutputStream.flush();
            dataOutputStream.close();
            outputStream.close();
            int responseCode = a3.getResponseCode();
            InputStream inputStream = a3.getInputStream();
            byte[] bArr = new byte[512];
            StringBuffer stringBuffer = new StringBuffer();
            if (responseCode == 200) {
                while (inputStream.read(bArr) > 0) {
                    stringBuffer.append(new String(bArr).trim());
                }
                str4 = stringBuffer.toString();
            } else {
                str4 = "Web服务器异常(Response error)";
            }
            inputStream.close();
            a3.disconnect();
            long currentTimeMillis = System.currentTimeMillis() - j;
            Log.d("LocationUtil.fppLocate", "Response Content is " + str4);
            a aVar = m135do(m136do(str4));
            if (aVar == null) {
                Log.d("LocationUtil.fppLocate", "Upload Will be started!");
                try {
                    new Thread(new n(context)).start();
                } catch (Throwable th4) {
                    LogUtil.getInstance().log("fppLocate:" + th4.toString());
                    Log.e("LocationUtil.fppLocate", "Upload date failed!", th4);
                }
                return null;
            } else if (!e.t.equals(aVar.m58if())) {
                Log.d("LocationUtil.fppLocate", "Upload Will be started!");
                try {
                    new Thread(new n(context)).start();
                } catch (Throwable th5) {
                    LogUtil.getInstance().log("fppLocate:" + th5.toString());
                    Log.e("LocationUtil.fppLocate", "Upload date failed!", th5);
                }
                return null;
            } else {
                Location location = new Location(e.f143long);
                location.setLatitude(Double.parseDouble(aVar.m46do()));
                location.setLongitude(Double.parseDouble(aVar.m48for()));
                location.setAccuracy(Float.valueOf(aVar.m42case()).floatValue());
                if (!m155try(aVar.m40byte())) {
                    location.setAltitude(Double.parseDouble(aVar.m40byte()));
                }
                Bundle bundle = new Bundle();
                bundle.putString(e.d, aVar.m50int());
                bundle.putString(e.c, aVar.m58if());
                bundle.putLong(e.f, currentTimeMillis);
                location.setExtras(bundle);
                Log.d("LocationUtil.fppLocate", "Upload Will be started!");
                try {
                    new Thread(new n(context)).start();
                } catch (Throwable th6) {
                    LogUtil.getInstance().log("fppLocate:" + th6.toString());
                    Log.e("LocationUtil.fppLocate", "Upload date failed!", th6);
                }
                return location;
            }
        } catch (Exception e) {
            LogUtil.getInstance().log("fppLocate:" + e.toString());
            e.printStackTrace();
            throw new b("Locate Exception!");
        } catch (Throwable th7) {
            Log.d("LocationUtil.fppLocate", "Upload Will be started!");
            try {
                new Thread(new n(context)).start();
            } catch (Throwable th8) {
                LogUtil.getInstance().log("fppLocate:" + th8.toString());
                Log.e("LocationUtil.fppLocate", "Upload date failed!", th8);
            }
            throw th7;
        }
    }

    public cmcc.location.a.b a(Document document) {
        if (document == null) {
            throw new IllegalArgumentException();
        }
        try {
            cmcc.location.a.b bVar = new cmcc.location.a.b();
            a(document, bVar);
            return bVar;
        } catch (Exception e) {
            LogUtil.getInstance().log("doc2TcPosReqBean 异常：" + e.toString());
            return null;
        }
    }

    public f a(List list) {
        if (list == null || list.size() <= 0) {
            return null;
        }
        int size = list.size();
        if (size < 2) {
            return (f) list.get(0);
        }
        int i = 0;
        f fVar = null;
        while (i < size) {
            f fVar2 = (f) list.get(i);
            if (fVar2.m67byte() == 1) {
                return fVar2;
            }
            i++;
            fVar = fVar2;
        }
        return fVar;
    }

    public String a() {
        return new SimpleDateFormat("yyyyMMdd-HHmmss").format(Calendar.getInstance(TimeZone.getTimeZone("GMT+8")).getTime());
    }

    public String a(Location location, String str) {
        if (location == null || m155try(str)) {
            return null;
        }
        Bundle extras = location.getExtras();
        if (extras == null) {
            return null;
        }
        return extras.getString(str);
    }

    public String a(String str) {
        if (m155try(str)) {
            return str;
        }
        int length = str.length() - 2;
        return length <= 0 ? "" : str.substring(0, length);
    }

    public String a(String str, int i) {
        if (str == null) {
            return null;
        }
        try {
            int indexOf = str.indexOf(47, 7);
            return String.valueOf(str.substring(0, indexOf)) + ":" + i + str.substring(indexOf, str.length());
        } catch (Exception e) {
            LogUtil.getInstance().log("config2Str:" + e.toString());
            e.printStackTrace();
            return null;
        }
    }

    public HttpURLConnection a(URL url, NetworkInfo networkInfo) throws Exception {
        if (url == null || networkInfo == null) {
            throw new IllegalArgumentException();
        }
        String typeName = networkInfo.getTypeName();
        String extraInfo = networkInfo.getExtraInfo();
        if (typeName.toUpperCase().equals("WIFI")) {
            return (HttpURLConnection) url.openConnection();
        }
        if (!typeName.toUpperCase().equals("MOBILE")) {
            return null;
        }
        if (!extraInfo.toUpperCase().equals("CMWAP")) {
            return (HttpURLConnection) url.openConnection();
        }
        LogUtil.getInstance().log("当前采用的是cmwap连接");
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.0.0.172", 80)));
        if (httpURLConnection == null) {
            return null;
        }
        LogUtil.getInstance().log("CMWAP连接成功");
        httpURLConnection.setRequestProperty("Accept", "text/javascript, text/ecmascript, application/x-javascript, */*, text/x-vcard, text/x-vcalendar, image/gif, image/vnd.wap.wbmp,textnd.wap.wml,applicationnd.wap.xhtml+xml,textml,text/css,text/vnd.wap.wml,application/vnd.wap.xhtml+xml,text/html,text/css");
        httpURLConnection.setRequestProperty("User-Agent", m139for());
        httpURLConnection.setRequestProperty("Content-Language", "en-CA");
        httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
        httpURLConnection.setRequestProperty("Cache-Control", "no-cache");
        httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        httpURLConnection.setRequestProperty("Charset", "UTF-8");
        return httpURLConnection;
    }

    public HttpURLConnection a(URL url, String[] strArr) throws Exception {
        if (url == null || strArr == null || strArr.length <= 0) {
            throw new IllegalArgumentException();
        }
        String str = strArr[2];
        if (m155try(str)) {
            return (HttpURLConnection) url.openConnection();
        }
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(str, Integer.parseInt(strArr[3]))));
        httpURLConnection.setRequestProperty("Accept", "text/javascript, text/ecmascript, application/x-javascript, */*, text/x-vcard, text/x-vcalendar, image/gif, image/vnd.wap.wbmp,textnd.wap.wml,applicationnd.wap.xhtml+xml,textml,text/css,text/vnd.wap.wml,application/vnd.wap.xhtml+xml,text/html,text/css");
        httpURLConnection.setRequestProperty("User-Agent", m139for());
        httpURLConnection.setRequestProperty("Content-Language", "en-CA");
        httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
        httpURLConnection.setRequestProperty("Cache-Control", "no-cache");
        httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        httpURLConnection.setRequestProperty("Charset", "UTF-8");
        return httpURLConnection;
    }

    public void a(long j, Activity activity) {
    }

    public void a(Activity activity, String str) {
        a(activity, str, 1);
    }

    public void a(Context context) {
        Message message = new Message();
        message.obj = context;
        this.f153char.sendMessage(message);
    }

    public void a(Context context, String str) {
        a(context, str, 0);
    }

    public void a(Context context, String str, int i) {
        Toast.makeText(context, str, i).show();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cmcc.location.core.h.a(org.w3c.dom.Document, java.lang.String, boolean):org.w3c.dom.NamedNodeMap
     arg types: [org.w3c.dom.Document, java.lang.String, int]
     candidates:
      cmcc.location.core.h.a(android.content.Context, java.lang.String, int):void
      cmcc.location.core.h.a(org.w3c.dom.Document, java.lang.String, boolean):org.w3c.dom.NamedNodeMap */
    public void a(Document document, cmcc.location.a.b bVar) throws Exception {
        if (document == null || bVar == null) {
            throw new IllegalArgumentException();
        }
        NamedNodeMap a2 = a(document, "SPID", true);
        if (a2 == null || a2.getLength() <= 0) {
            throw new Exception("SPID Node Must Has Attribute");
        }
        Node namedItem = a2.getNamedItem(c.f129do);
        if (namedItem == null) {
            throw new Exception("The SPID Attribute Must Be Contain!");
        }
        bVar.m57if(namedItem.getNodeValue());
        NamedNodeMap a3 = a(document, "ServID", true);
        if (a3 == null || a3.getLength() <= 0) {
            throw new Exception("ServID Node Must Has Attribute");
        }
        Node namedItem2 = a3.getNamedItem(c.f129do);
        if (namedItem2 == null) {
            throw new Exception("The ServID Attribute Must Be Contain!");
        }
        bVar.a(namedItem2.getNodeValue());
    }

    public void a(String[] strArr) {
        if (strArr != null && strArr.length > 0) {
            Log.d("LocationUtil.printConnectInfo", "Connection Status : " + strArr[0]);
            Log.d("LocationUtil.printConnectInfo", "Connection Proxy Type: " + strArr[1]);
            Log.d("LocationUtil.printConnectInfo", "Connection Proxy Addr: " + strArr[2]);
            Log.d("LocationUtil.printConnectInfo", "Connection Proxy Port: " + strArr[3]);
        }
    }

    public boolean a(String str, String str2) {
        if (str == null || str2 == null) {
            return false;
        }
        String[] split = str.split("\\|");
        int i = 0;
        for (String indexOf : split) {
            if (str2.indexOf(indexOf) != -1) {
                i++;
            }
        }
        return ((double) ((float) (i / split.length))) < 0.7d;
    }

    public String b(Context context) {
        String networkOperator = ((TelephonyManager) context.getSystemService("phone")).getNetworkOperator();
        return (m155try(networkOperator) || networkOperator.length() < 2) ? networkOperator : networkOperator.substring(0, networkOperator.length() - 2);
    }

    /* renamed from: byte  reason: not valid java name */
    public Location m131byte(Context context) {
        return a.a(context).m110if();
    }

    public String c(Context context) {
        String subscriberId = ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
        return m155try(subscriberId) ? "460001006020000" : subscriberId;
    }

    /* renamed from: case  reason: not valid java name */
    public GsmCellLocation m132case(Context context) {
        CellLocation cellLocation = ((TelephonyManager) context.getSystemService("phone")).getCellLocation();
        if (cellLocation instanceof GsmCellLocation) {
            return (GsmCellLocation) cellLocation;
        }
        return null;
    }

    /* renamed from: char  reason: not valid java name */
    public int m133char(Context context) {
        GsmCellLocation gsmCellLocation = m132case(context);
        if (gsmCellLocation != null) {
            return gsmCellLocation.getLac();
        }
        return 0;
    }

    public String d(Context context) {
        String line1Number = ((TelephonyManager) context.getSystemService("phone")).getLine1Number();
        return m155try(line1Number) ? "" : line1Number;
    }

    /* renamed from: do  reason: not valid java name */
    public Location m134do(Context context) {
        try {
            LocationManager locationManager = (LocationManager) context.getSystemService(LocationManagerProxy.KEY_LOCATION_CHANGED);
            if (locationManager == null) {
                return null;
            }
            return locationManager.getLastKnownLocation(LocationManagerProxy.GPS_PROVIDER);
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cmcc.location.core.h.a(org.w3c.dom.Document, java.lang.String, boolean):org.w3c.dom.NamedNodeMap
     arg types: [org.w3c.dom.Document, java.lang.String, int]
     candidates:
      cmcc.location.core.h.a(android.content.Context, java.lang.String, int):void
      cmcc.location.core.h.a(org.w3c.dom.Document, java.lang.String, boolean):org.w3c.dom.NamedNodeMap */
    /* renamed from: do  reason: not valid java name */
    public a m135do(Document document) throws Exception {
        String str;
        NamedNodeMap a2;
        if (document == null) {
            throw new IllegalArgumentException();
        }
        a aVar = new a();
        a(document, aVar);
        NamedNodeMap a3 = a(document, e.d, true);
        if (a3 != null) {
            Node namedItem = a3.getNamedItem(c.f129do);
            if (namedItem == null) {
                throw new Exception("The val Of PosType Attribute Must Be Contain!");
            }
            aVar.m45char(namedItem.getNodeValue());
        }
        NamedNodeMap a4 = a(document, "PosLevel", false);
        if (a4 != null) {
            Node namedItem2 = a4.getNamedItem(c.f129do);
            str = namedItem2 == null ? "Point" : namedItem2.getNodeValue();
            aVar.m49for(str);
        } else {
            str = "Point";
        }
        if ("point".equals(str.toLowerCase()) || "hybrid".equals(str.toLowerCase())) {
            NamedNodeMap a5 = a(document, "Coord", false);
            if (a5 != null) {
                Node namedItem3 = a5.getNamedItem("lat");
                if (namedItem3 == null) {
                    throw new Exception("The lat Attribute Must Be Contain!");
                }
                aVar.m53new(namedItem3.getNodeValue());
                Node namedItem4 = a5.getNamedItem("lon");
                if (namedItem4 == null) {
                    throw new Exception("The lon Attribute Must Be Contain!");
                }
                aVar.m41byte(namedItem4.getNodeValue());
                Node namedItem5 = a5.getNamedItem("h");
                if (namedItem5 != null) {
                    aVar.m43case(namedItem5.getNodeValue());
                }
            }
            NamedNodeMap a6 = a(document, "ErrRange", false);
            if (a6 != null) {
                Node namedItem6 = a6.getNamedItem(c.f129do);
                if (namedItem6 == null) {
                    throw new Exception("The val Of ErrRange Attribute Must Be Contain!");
                }
                aVar.m47do(namedItem6.getNodeValue());
            }
        }
        if (("area".equals(str.toLowerCase()) || "hybrid".equals(str.toLowerCase())) && (a2 = a(document, "Area", false)) != null) {
            Node namedItem7 = a2.getNamedItem("code");
            if (namedItem7 == null) {
                throw new Exception("The code Attribute Must Be Contain!");
            }
            aVar.m51int(namedItem7.getNodeValue());
            Node namedItem8 = a2.getNamedItem("addr");
            if (namedItem8 == null) {
                throw new Exception("The addr Attribute Must Be Contain!");
            }
            aVar.m55try(namedItem8.getNodeValue());
        }
        return aVar;
    }

    /* renamed from: do  reason: not valid java name */
    public Document m136do(String str) throws SAXException, IOException, ParserConfigurationException {
        if (m155try(str)) {
            throw new IllegalArgumentException();
        }
        return DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(str)));
    }

    /* renamed from: do  reason: not valid java name */
    public boolean m137do() {
        return Environment.getExternalStorageState().equals("mounted");
    }

    public List e(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getNeighboringCellInfo();
    }

    /* renamed from: else  reason: not valid java name */
    public int m138else(Context context) {
        GsmCellLocation gsmCellLocation = m132case(context);
        if (gsmCellLocation != null) {
            return gsmCellLocation.getCid();
        }
        return 0;
    }

    public g f(Context context) {
        int i = m138else(context);
        int i2 = m133char(context);
        if (i <= 0 || i2 <= 0) {
            Log.d("LocationUtil.pluckData", "Invalid data (cellId : " + i + ", lac : " + i2 + ")");
            return null;
        }
        g gVar = new g();
        ArrayList arrayList = m146if(context);
        Location location = m131byte(context);
        if ((arrayList == null || arrayList.size() <= 0) && location == null) {
            return null;
        }
        if (a.a(context).a()) {
            if (location == null) {
                this.f215a++;
                if (this.f215a >= this.f151byte) {
                    a.a(context).m109for();
                    this.f215a = 0;
                }
            } else if (location.getLatitude() == this.f152case.getLatitude() && location.getLongitude() == this.f152case.getLongitude()) {
                this.f158new++;
                if (this.f158new >= this.f156if) {
                    a.a(context).m109for();
                    this.f158new = 0;
                }
            } else {
                this.f158new = 0;
                this.f152case.setLongitude(location.getLongitude());
                this.f152case.setLatitude(location.getLatitude());
            }
        } else if (i != this.f155for) {
            if (this.f155for != 0) {
                a(context);
                this.f155for = i;
            } else {
                this.f155for = i;
            }
        } else if (arrayList != null) {
            String trim = ((String) arrayList.get(0)).trim();
            if (m155try(this.f157int)) {
                this.f157int = trim;
            } else if (!a(trim, this.f157int)) {
                return null;
            } else {
                a(context);
                this.f157int = trim;
            }
        } else if (location == null) {
            return null;
        }
        gVar.m91do(i);
        gVar.a(m152new(context));
        gVar.m85byte(m149int(context));
        gVar.a(c(context));
        gVar.m98if(i2);
        gVar.m106try(b(context));
        gVar.m99if(m151long(context));
        gVar.m104new(d(context));
        gVar.m92do(m139for());
        if (arrayList != null) {
            gVar.m87case((String) arrayList.get(0));
            gVar.m95for((String) arrayList.get(1));
        }
        if (location != null) {
            gVar.a(location);
        }
        gVar.m101int(a());
        return gVar;
    }

    /* renamed from: for  reason: not valid java name */
    public String m139for() {
        return String.valueOf(Build.BRAND) + "/" + Build.PRODUCT;
    }

    /* renamed from: for  reason: not valid java name */
    public String m140for(String str) {
        return e.t.equals(str) ? "OK" : e.r.equals(str) ? "IlligalArgs" : e.p.equals(str) ? "AccuTooBad" : e.n.equals(str) ? "AuthFail" : e.l.equals(str) ? "IntegrityFail" : e.i.equals(str) ? "Unknown" : "";
    }

    /* renamed from: for  reason: not valid java name */
    public Map m141for(Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
        if (!wifiManager.isWifiEnabled()) {
            return null;
        }
        HashMap hashMap = new HashMap();
        List<ScanResult> scanResults = wifiManager.getScanResults();
        int size = scanResults.size();
        if (size <= 0) {
            return null;
        }
        for (int i = 0; i < size; i++) {
            ScanResult scanResult = scanResults.get(i);
            if (scanResult != null) {
                hashMap.put(scanResult.BSSID, String.valueOf(scanResult.level));
            }
        }
        return hashMap;
    }

    /* renamed from: goto  reason: not valid java name */
    public void m142goto(Context context) {
        a.a(context).m109for();
    }

    /* renamed from: if  reason: not valid java name */
    public long m143if() {
        return System.currentTimeMillis();
    }

    /* renamed from: if  reason: not valid java name */
    public c m144if(Document document) {
        if (document == null) {
            throw new IllegalArgumentException();
        }
        try {
            c cVar = new c();
            a(document, cVar);
            return cVar;
        } catch (Exception e) {
            LogUtil.getInstance().log("doc2FcUpRecsRespBean 异常：" + e.toString());
            return null;
        }
    }

    /* renamed from: if  reason: not valid java name */
    public URL m145if(String str, int i) {
        if (str == null) {
            return null;
        }
        try {
            int indexOf = str.indexOf(47, 7);
            return new URL(String.valueOf(str.substring(0, indexOf)) + ":" + i + str.substring(indexOf, str.length()));
        } catch (Exception e) {
            LogUtil.getInstance().log("config2Url:" + e.toString());
            e.printStackTrace();
            return null;
        }
    }

    /* renamed from: if  reason: not valid java name */
    public ArrayList m146if(Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
        if (wifiManager == null || !wifiManager.isWifiEnabled()) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        StringBuffer stringBuffer2 = new StringBuffer();
        List<ScanResult> scanResults = wifiManager.getScanResults();
        if (scanResults == null || scanResults.size() <= 0) {
            return null;
        }
        int size = scanResults.size();
        if (size > this.f154do) {
            size = this.f154do;
        }
        for (int i = 0; i < size; i++) {
            stringBuffer.append(scanResults.get(i).BSSID);
            stringBuffer.append("|");
            stringBuffer2.append(scanResults.get(i).level);
            stringBuffer2.append("|");
        }
        stringBuffer.deleteCharAt(stringBuffer.lastIndexOf("|"));
        stringBuffer2.deleteCharAt(stringBuffer2.lastIndexOf("|"));
        ArrayList arrayList = new ArrayList();
        arrayList.add(stringBuffer.toString());
        arrayList.add(stringBuffer2.toString());
        return arrayList;
    }

    /* renamed from: if  reason: not valid java name */
    public boolean m147if(String str) {
        if (m155try(str)) {
            return false;
        }
        return Pattern.matches("[0-9]+", str);
    }

    /* renamed from: int  reason: not valid java name */
    public String m148int() {
        return new Date().toLocaleString();
    }

    /* renamed from: int  reason: not valid java name */
    public String m149int(Context context) {
        String deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        return m155try(deviceId) ? "" : deviceId;
    }

    /* renamed from: int  reason: not valid java name */
    public boolean m150int(String str) {
        return !m155try(str);
    }

    /* renamed from: long  reason: not valid java name */
    public String m151long(Context context) {
        int length;
        String networkOperator = ((TelephonyManager) context.getSystemService("phone")).getNetworkOperator();
        return (m155try(networkOperator) || (length = networkOperator.length()) < 2) ? networkOperator : networkOperator.substring(length - 2, length);
    }

    /* renamed from: new  reason: not valid java name */
    public int m152new(Context context) {
        int i = context.getSharedPreferences(e.e, 0).getInt(e.f142int, 0);
        Log.d("getCellRelex", "!!!!!!!!!!!!!! RSS : " + i);
        return i;
    }

    /* renamed from: new  reason: not valid java name */
    public boolean m153new(String str) {
        if (m155try(str)) {
            return false;
        }
        return m147if(str);
    }

    /* renamed from: try  reason: not valid java name */
    public String m154try(Context context) {
        WifiInfo connectionInfo;
        WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
        return (wifiManager == null || !wifiManager.isWifiEnabled() || (connectionInfo = wifiManager.getConnectionInfo()) == null) ? "" : connectionInfo.getBSSID();
    }

    /* renamed from: try  reason: not valid java name */
    public boolean m155try(String str) {
        return str == null || "".equals(str);
    }

    /* renamed from: void  reason: not valid java name */
    public int m156void(Context context) {
        WifiInfo connectionInfo;
        WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
        if (wifiManager == null || !wifiManager.isWifiEnabled() || (connectionInfo = wifiManager.getConnectionInfo()) == null) {
            return 0;
        }
        return connectionInfo.getRssi();
    }
}
