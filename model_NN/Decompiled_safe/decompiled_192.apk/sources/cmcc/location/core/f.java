package cmcc.location.core;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

class f implements ErrorHandler {
    f() {
    }

    public void error(SAXParseException sAXParseException) throws SAXException {
        throw new SAXException(String.valueOf(sAXParseException.getSystemId()) + " parse error at " + "line " + sAXParseException.getLineNumber() + ",column " + sAXParseException.getColumnNumber() + "," + sAXParseException.getMessage());
    }

    public void fatalError(SAXParseException sAXParseException) throws SAXException {
        throw new SAXException("line " + sAXParseException.getLineNumber() + ",column " + sAXParseException.getColumnNumber() + "," + sAXParseException.getMessage());
    }

    public void warning(SAXParseException sAXParseException) throws SAXException {
        throw new SAXException("line " + sAXParseException.getLineNumber() + ",column " + sAXParseException.getColumnNumber() + "," + sAXParseException.getMessage());
    }
}
