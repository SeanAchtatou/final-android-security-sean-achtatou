package cmcc.location.core;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import com.mapabc.mapapi.LocationManagerProxy;

public class a {

    /* renamed from: if  reason: not valid java name */
    private static a f120if = null;

    /* renamed from: a  reason: collision with root package name */
    private LocationManager f204a = null;
    /* access modifiers changed from: private */

    /* renamed from: do  reason: not valid java name */
    public Location f121do = null;

    /* renamed from: for  reason: not valid java name */
    private C0004a f122for = null;

    /* renamed from: int  reason: not valid java name */
    private Context f123int = null;

    /* renamed from: cmcc.location.core.a$a  reason: collision with other inner class name */
    class C0004a implements LocationListener {
        C0004a() {
        }

        public void onLocationChanged(Location location) {
            a.this.f121do = location;
            Log.d("MyLocationListener.onLocationChanged", "On Location Changed!");
        }

        public void onProviderDisabled(String str) {
            Log.d("MyLocationListener.onProviderDisabled", "On Provider Disabled : " + str);
            a.this.f121do = null;
        }

        public void onProviderEnabled(String str) {
            Log.d("MyLocationListener.onProviderEnabled", "On Provider Enabled : " + str);
            a.this.m109for();
            a.this.m111int();
        }

        public void onStatusChanged(String str, int i, Bundle bundle) {
            Log.d("MyLocationListener.onStatusChanged", "On StatusChanged : " + i);
        }
    }

    private a(Context context) {
        this.f123int = context;
    }

    public static a a(Context context) {
        a aVar;
        synchronized (a.class) {
            if (f120if == null) {
                f120if = new a(context);
            }
            aVar = f120if;
        }
        return aVar;
    }

    public boolean a() {
        return this.f122for != null;
    }

    /* renamed from: do  reason: not valid java name */
    public void m108do() {
        this.f204a = (LocationManager) this.f123int.getSystemService(LocationManagerProxy.KEY_LOCATION_CHANGED);
        if (this.f204a != null) {
            m111int();
        }
    }

    /* renamed from: for  reason: not valid java name */
    public void m109for() {
        if (this.f204a != null && this.f122for != null) {
            this.f204a.removeUpdates(this.f122for);
            this.f122for = null;
            this.f121do = null;
        }
    }

    /* renamed from: if  reason: not valid java name */
    public Location m110if() {
        if (this.f204a == null || this.f122for == null) {
            return null;
        }
        if (this.f121do == null) {
            this.f204a.getLastKnownLocation(LocationManagerProxy.GPS_PROVIDER);
        }
        return this.f121do;
    }

    /* renamed from: int  reason: not valid java name */
    public void m111int() {
        if (this.f204a != null && this.f122for == null) {
            Log.d("init", "Regist Location Listener Start!");
            this.f122for = new C0004a();
            this.f204a.requestLocationUpdates(LocationManagerProxy.GPS_PROVIDER, 1000, 5.0f, this.f122for);
            Log.d("init", "Regist Location Listener OK!");
        }
    }
}
