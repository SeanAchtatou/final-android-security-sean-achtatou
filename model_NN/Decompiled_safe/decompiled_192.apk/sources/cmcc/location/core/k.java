package cmcc.location.core;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import com.mapabc.mapapi.LocationManagerProxy;

public class k {
    /* access modifiers changed from: private */

    /* renamed from: for  reason: not valid java name */
    public static Location f165for = null;

    /* renamed from: if  reason: not valid java name */
    private static k f166if = null;

    /* renamed from: a  reason: collision with root package name */
    private String f219a = null;

    /* renamed from: do  reason: not valid java name */
    private LocationManager f167do = null;

    /* renamed from: int  reason: not valid java name */
    private Context f168int = null;

    /* renamed from: new  reason: not valid java name */
    private a f169new = null;

    class a implements LocationListener {
        a() {
        }

        public void onLocationChanged(Location location) {
            k.f165for = location;
            Log.d("MyLocationListener", "Location Changed!");
        }

        public void onProviderDisabled(String str) {
            Log.d("MyLocationListener", "Provider Disabled : " + str);
            k.this.m161if();
            k.f165for = (Location) null;
        }

        public void onProviderEnabled(String str) {
            Log.d("MyLocationListener", "Provider Enabled : " + str);
        }

        public void onStatusChanged(String str, int i, Bundle bundle) {
            Log.d("MyLocationListener", "onStatusChanged : " + i);
        }
    }

    private k(Context context) {
        this.f168int = context;
    }

    public static k a(Context context) {
        synchronized (k.class) {
            if (f166if == null) {
                Log.i("GpsInfoGeter", "me is Null, New a GpsInfoGeter Object!");
                f166if = new k(context);
            }
            f166if.m160do();
        }
        return f166if;
    }

    /* renamed from: do  reason: not valid java name */
    private void m160do() {
        if (this.f169new == null) {
            this.f167do = (LocationManager) this.f168int.getSystemService(LocationManagerProxy.KEY_LOCATION_CHANGED);
            if (this.f167do == null) {
                return;
            }
            if (!this.f167do.isProviderEnabled(LocationManagerProxy.GPS_PROVIDER)) {
                Log.w("GpsInfoGeter", "GPS_PROVIDER Is Disabled!");
                return;
            }
            Log.i("GpsInfoGeter", "GPS_PROVIDER Is Enabled!");
            Criteria criteria = new Criteria();
            criteria.setAccuracy(1);
            criteria.setAltitudeRequired(true);
            criteria.setBearingRequired(false);
            criteria.setCostAllowed(true);
            criteria.setPowerRequirement(0);
            this.f219a = this.f167do.getBestProvider(criteria, true);
            Log.d("GpsInfoGeter.init", "Provider Name : " + this.f219a);
            if (this.f219a == null || "".equals(this.f219a)) {
                Log.w("GpsInfoGeter", "No Provider Enabled!");
                return;
            }
            if (Looper.myLooper() == null) {
                Looper.prepare();
            }
            this.f169new = new a();
            this.f167do.requestLocationUpdates(this.f219a, 1000, 5.0f, this.f169new);
            Log.i("GpsInfoGeter", "Add Location Listener!");
        }
    }

    public Location a() {
        if (this.f219a == null || "".equals(this.f219a)) {
            return null;
        }
        if (f165for == null) {
            f165for = this.f167do.getLastKnownLocation(this.f219a);
        } else {
            Location lastKnownLocation = this.f167do.getLastKnownLocation(this.f219a);
            if (Double.compare(lastKnownLocation.getLatitude(), f165for.getLatitude()) == 0 && Double.compare(lastKnownLocation.getLongitude(), lastKnownLocation.getLongitude()) == 0) {
                return null;
            }
            f165for = lastKnownLocation;
        }
        return f165for;
    }

    /* renamed from: if  reason: not valid java name */
    public void m161if() {
        if (this.f167do != null && this.f169new != null) {
            Log.i("GpsInfoGeter", "Remove Location Listener!");
            this.f167do.removeUpdates(this.f169new);
            this.f169new = null;
        }
    }
}
