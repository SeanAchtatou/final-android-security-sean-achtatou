package cmcc.location.core.a;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import cmcc.location.a.b;
import cmcc.location.core.LogUtil;
import cmcc.location.core.c;
import cmcc.location.core.h;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.MethodNotSupportedException;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpRequestHandler;
import org.w3c.dom.Document;

public class e implements HttpRequestHandler {

    /* renamed from: a  reason: collision with root package name */
    private final Context f209a;

    public e(Context context) {
        this.f209a = context;
    }

    private String a(Location location) {
        if (location == null) {
            throw new IllegalArgumentException();
        }
        try {
            c cVar = new c();
            cVar.a("PosRes_TC");
            h hVar = new h();
            String a2 = hVar.a(location, cmcc.location.core.e.c);
            if (a2 == null || "".equals(a2)) {
                Log.e("LocationUtil.getNormalPosRspXml", "ErrCode is null!");
                return null;
            }
            cVar.a(cVar.a(), "Result", a2);
            String a3 = hVar.a(location, cmcc.location.core.e.d);
            if (a3 == null || "".equals(a3)) {
                Log.e("LocationUtil.getNormalPosRspXml", "PosType is null!");
                return null;
            }
            cVar.m114do(cVar.a(), a3);
            cVar.a(cVar.a(), location);
            cVar.m113do(cVar.a(), location);
            return cVar.toString();
        } catch (Exception e) {
            Log.e("LocationUtil.getNormalPosRspXml", "Get Normal RspXml Failed!");
            LogUtil.getInstance().log("getNormalPosRspXml 异常：" + e.toString());
            return null;
        }
    }

    private String a(String str) {
        try {
            c cVar = new c();
            cVar.a("PosRes_TC");
            cVar.a(cVar.a(), "Result", str);
            cVar.m114do(cVar.a(), cmcc.location.core.e.k);
            return cVar.toString();
        } catch (Exception e) {
            Log.e("LocateHandler.getErrReqXml", "Get Error Response Xml Failed!", e);
            LogUtil.getInstance().log("getErrRspXml 异常：" + e.toString());
            return null;
        }
    }

    private boolean a(b bVar) {
        return bVar != null;
    }

    public void handle(HttpRequest httpRequest, HttpResponse httpResponse, HttpContext httpContext) throws HttpException, IOException {
        try {
            String upperCase = httpRequest.getRequestLine().getMethod().toUpperCase(Locale.ENGLISH);
            if (!upperCase.equals("GET") && !upperCase.equals("POST")) {
                throw new MethodNotSupportedException(String.valueOf(upperCase) + " method not supported!");
            } else if (!(httpRequest instanceof HttpEntityEnclosingRequest)) {
                Log.e("LocateHandler.handle", "request instanceof HttpEntityEnclosingRequest = false");
                throw new cmcc.location.b.b(cmcc.location.core.e.i);
            } else {
                HttpEntity entity = ((HttpEntityEnclosingRequest) httpRequest).getEntity();
                if (entity == null) {
                    Log.e("LocateHandler.handle", "Entity is null!");
                    throw new cmcc.location.b.b(cmcc.location.core.e.r);
                }
                int i = 0;
                StringBuffer stringBuffer = new StringBuffer();
                InputStream content = entity.getContent();
                while (true) {
                    int read = content.read();
                    if (read == -1) {
                        break;
                    }
                    i++;
                    stringBuffer.append((char) read);
                }
                String stringBuffer2 = stringBuffer.toString();
                if (stringBuffer2 == null || "".equals(stringBuffer2)) {
                    Log.e("LocateHandler.handle", "XML is null!");
                    throw new cmcc.location.b.b(cmcc.location.core.e.r);
                }
                Log.d("LocateHandler.handle", "XML : " + stringBuffer2);
                if (Looper.myLooper() == null) {
                    Looper.prepare();
                }
                h hVar = new h();
                Document document = hVar.m136do(stringBuffer2);
                if (document == null) {
                    Log.e("LocateHandler.handle", "Document is null!");
                    throw new cmcc.location.b.b(cmcc.location.core.e.i);
                }
                b a2 = hVar.a(document);
                if (a2 == null) {
                    Log.e("LocateHandler.handle", "TcPosReqBean is null!");
                    throw new cmcc.location.b.b(cmcc.location.core.e.i);
                } else if (!a(a2)) {
                    Log.e("LocateHandler.handle", "Check Request Permissions Failed!");
                    throw new cmcc.location.b.b(cmcc.location.core.e.r);
                } else {
                    new a.b.a.c();
                    a.b.a.c a3 = a.b.a.b.a(this.f209a).a(Integer.parseInt(a2.m56if()), Integer.parseInt(a2.a()));
                    int i2 = a3.m33for();
                    if (Integer.parseInt(cmcc.location.core.e.t) != i2) {
                        throw new cmcc.location.b.b(String.valueOf(i2));
                    }
                    if (a3 == null) {
                    }
                    Location location = new Location(cmcc.location.core.e.f143long);
                    location.setLatitude(a3.a());
                    location.setLongitude(a3.m31do());
                    location.setAltitude(a3.m37int());
                    location.setAccuracy(a3.m34if());
                    Bundle bundle = new Bundle();
                    bundle.putString(cmcc.location.core.e.d, String.valueOf(a3.m38new()));
                    bundle.putString(cmcc.location.core.e.c, String.valueOf(a3.m33for()));
                    bundle.putLong(cmcc.location.core.e.f, a3.m39try());
                    location.setExtras(bundle);
                    httpResponse.setEntity(new StringEntity(a(location)));
                }
            }
        } catch (cmcc.location.b.b e) {
            httpResponse.setEntity(new StringEntity(a(e.a())));
        } catch (Throwable th) {
            Log.e("LocateHandler.handle", "LocateHandler.handle Error!", th);
        }
    }
}
