package cmcc.location.core.a;

import android.content.Context;
import android.util.Log;
import cmcc.location.core.LogUtil;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.http.impl.DefaultConnectionReuseStrategy;
import org.apache.http.impl.DefaultHttpResponseFactory;
import org.apache.http.impl.DefaultHttpServerConnection;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpProcessor;
import org.apache.http.protocol.HttpRequestHandlerRegistry;
import org.apache.http.protocol.HttpService;
import org.apache.http.protocol.ResponseConnControl;
import org.apache.http.protocol.ResponseContent;
import org.apache.http.protocol.ResponseDate;
import org.apache.http.protocol.ResponseServer;

public class d extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private final ExecutorService f208a = Executors.newFixedThreadPool(2);

    /* renamed from: do  reason: not valid java name */
    private final HttpParams f126do;

    /* renamed from: for  reason: not valid java name */
    private final HttpService f127for;

    /* renamed from: if  reason: not valid java name */
    private final ServerSocket f128if;

    public d(int i, Context context) throws IOException {
        Log.d("HttpRequestListener", "Listen port : " + i);
        this.f128if = new ServerSocket(i);
        this.f126do = new BasicHttpParams();
        this.f126do.setIntParameter("http.socket.timeout", 5000).setIntParameter("http.socket.buffer-size", 2048).setBooleanParameter("http.connection.stalecheck", false).setBooleanParameter("http.tcp.nodelay", true).setParameter("http.origin-server", "HttpComponents/1.1");
        BasicHttpProcessor basicHttpProcessor = new BasicHttpProcessor();
        basicHttpProcessor.addInterceptor(new ResponseDate());
        basicHttpProcessor.addInterceptor(new ResponseServer());
        basicHttpProcessor.addInterceptor(new ResponseContent());
        basicHttpProcessor.addInterceptor(new ResponseConnControl());
        HttpRequestHandlerRegistry httpRequestHandlerRegistry = new HttpRequestHandlerRegistry();
        httpRequestHandlerRegistry.register("*", new e(context));
        this.f127for = new HttpService(basicHttpProcessor, new DefaultConnectionReuseStrategy(), new DefaultHttpResponseFactory());
        this.f127for.setParams(this.f126do);
        this.f127for.setHandlerResolver(httpRequestHandlerRegistry);
    }

    public void run() {
        while (!Thread.interrupted()) {
            try {
                Socket accept = this.f128if.accept();
                Log.d("HttpRequestListener.run", "HttpRequestListener Received Http Request!");
                DefaultHttpServerConnection defaultHttpServerConnection = new DefaultHttpServerConnection();
                Log.d("HttpRequestListener.run", "Incoming connection from " + accept.getInetAddress());
                defaultHttpServerConnection.bind(accept, this.f126do);
                b bVar = new b(this.f127for, defaultHttpServerConnection);
                bVar.setDaemon(true);
                this.f208a.execute(bVar);
            } catch (InterruptedIOException e) {
                LogUtil.getInstance().log("HttpRequestListener:" + e.toString());
            } catch (Throwable th) {
                Log.e("HttpRequestListener.run", "Http Server Listener Error : " + th.getMessage());
                LogUtil.getInstance().log("HttpRequestListener:" + th.toString());
            }
        }
        this.f208a.shutdown();
    }
}
