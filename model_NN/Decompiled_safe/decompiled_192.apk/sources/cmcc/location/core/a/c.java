package cmcc.location.core.a;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;
import cmcc.location.a.f;
import cmcc.location.core.LogUtil;
import cmcc.location.core.a;
import cmcc.location.core.d;
import cmcc.location.core.g;
import cmcc.location.core.h;
import cmcc.location.core.j;

public class c extends Service {

    /* renamed from: a  reason: collision with root package name */
    private PowerManager.WakeLock f207a;

    private void a() {
        try {
            Log.d("LocationService.init", "************Init DB Start***********");
            d.a(this);
            Log.d("LocationService.init", "************Init DB OK***********");
            Log.d("LocationService.init", "************Get Config Start***********");
            f a2 = new h().a(d.a(this).m129new());
            if (a2 == null) {
                Log.e("LocationService.init", "Get Config Failed!");
                return;
            }
            Log.d("LocationService.init", "************Get Config OK***********");
            Log.d("LocationService.init", "************Init Network Start***********");
            String[] a3 = a.a.a.c.m16if().a(this, a2.a());
            if (a3 == null || a3.length <= 0 || !"true".equals(a3[0])) {
                Log.d("LocationService.init", "************Init Network NG***********");
            } else {
                Log.d("LocationService.init", "************Init Network OK***********");
            }
            Log.d("LocationService.init", "************Init GPS Start***********");
            a.a(this).m108do();
            Log.d("LocationService.init", "************Init GPS End***********");
            Log.d("LocationService.init", "************Init Plucker Start***********");
            g gVar = new g(this, a2);
            gVar.setDaemon(true);
            gVar.start();
            Log.d("LocationService.init", "************Init Plucker OK***********");
            Log.d("LocationService.init", "************Listen Signal Start***********");
            j.m157for().a(this);
            Log.d("LocationService.init", "************Listen Signal OK***********");
            Log.d("LocationService.init", "************Acquire Power Lock Start***********");
            this.f207a = ((PowerManager) getSystemService("power")).newWakeLock(1, "FPP_SERVICE");
            this.f207a.acquire();
            Log.d("LocationService.init", "************Acquire Power Lock OK***********");
        } catch (Exception e) {
            Log.e("LocationService.init", "Init Error, Service Exit!", e);
            LogUtil.getInstance().log("LocationService 初始化失败：" + e.toString());
            stopSelf();
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        Log.d("LocationService.onCreate", "Service is Running!");
        a();
    }

    public void onDestroy() {
        super.onDestroy();
        a.a(getApplicationContext()).m109for();
        j.m157for().m159do();
        d.a(getApplicationContext()).m125do();
        a.a.a.c.m16if().a();
        Log.d("LocationService.onDestroy", "Service is Stoped!");
        this.f207a.release();
        System.exit(0);
    }
}
