package cmcc.location.core.a;

import android.util.Log;
import cmcc.location.core.LogUtil;
import cmcc.location.core.e;
import java.io.IOException;
import org.apache.http.ConnectionClosedException;
import org.apache.http.HttpException;
import org.apache.http.HttpServerConnection;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpService;

public class b extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private final HttpService f206a;

    /* renamed from: do  reason: not valid java name */
    private long f124do;

    /* renamed from: if  reason: not valid java name */
    private final HttpServerConnection f125if;

    public b(HttpService httpService, HttpServerConnection httpServerConnection) {
        if (httpService == null || httpServerConnection == null) {
            throw new IllegalArgumentException();
        }
        this.f206a = httpService;
        this.f125if = httpServerConnection;
        Log.d("LocateThread", "LocateThread is created!");
    }

    public void run() {
        BasicHttpContext basicHttpContext = new BasicHttpContext();
        this.f124do = Thread.currentThread().getId();
        try {
            Log.d("LocateThread", "LocateThread [" + this.f124do + "] is run!");
            this.f206a.handleRequest(this.f125if, basicHttpContext);
            try {
                if (this.f125if != null && this.f125if.isOpen()) {
                    this.f125if.close();
                    this.f125if.shutdown();
                }
            } catch (Throwable th) {
                LogUtil.getInstance().log("LocateThread:" + th.toString());
                Log.e("LocateThread.run", "Close Connection Failed!");
            }
        } catch (ConnectionClosedException e) {
            LogUtil.getInstance().log("LocateThread:" + e.toString());
            Log.e("LocateThread", e.f140goto + this.f124do + "] Client closed connection", e);
            if (this.f125if != null && this.f125if.isOpen()) {
                this.f125if.close();
                this.f125if.shutdown();
            }
        } catch (IOException e2) {
            LogUtil.getInstance().log("LocateThread:" + e2.toString());
            Log.e("LocateThread", e.f140goto + this.f124do + "] I/O error: " + e2);
            if (this.f125if != null && this.f125if.isOpen()) {
                this.f125if.close();
                this.f125if.shutdown();
            }
        } catch (HttpException e3) {
            LogUtil.getInstance().log("LocateThread:" + e3.toString());
            Log.e("LocateThread", e.f140goto + this.f124do + "] Unrecoverable HTTP protocol violation: " + e3);
            if (this.f125if != null && this.f125if.isOpen()) {
                this.f125if.close();
                this.f125if.shutdown();
            }
        } catch (Throwable th2) {
            LogUtil.getInstance().log("LocateThread:" + th2.toString());
            Log.e("LocateThread.run", "Close Connection Failed!");
        }
    }
}
