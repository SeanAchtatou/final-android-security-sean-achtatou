package cmcc.location.core;

public interface e {

    /* renamed from: a  reason: collision with root package name */
    public static final String f213a = "WLAN";
    public static final String b = "Cell";

    /* renamed from: byte  reason: not valid java name */
    public static final String f134byte = "CmccLocationDB";
    public static final String c = "ErrCode";

    /* renamed from: case  reason: not valid java name */
    public static final int f135case = 0;

    /* renamed from: char  reason: not valid java name */
    public static final int f136char = 0;
    public static final String d = "PosType";

    /* renamed from: do  reason: not valid java name */
    public static final String f137do = "application/xml";
    public static final String e = "sharedFileName";

    /* renamed from: else  reason: not valid java name */
    public static final String f138else = System.getProperty("line.separator");
    public static final String f = "costTime";

    /* renamed from: for  reason: not valid java name */
    public static final String f139for = "pluckData.csv";
    public static final int g = 6;

    /* renamed from: goto  reason: not valid java name */
    public static final String f140goto = "[";
    public static final int h = 5;
    public static final String i = "105";

    /* renamed from: if  reason: not valid java name */
    public static final String f141if = "uploaded";

    /* renamed from: int  reason: not valid java name */
    public static final String f142int = "signalStrength";
    public static final int j = 4;
    public static final String k = "Hybrid";
    public static final String l = "104";

    /* renamed from: long  reason: not valid java name */
    public static final String f143long = "cmcc";
    public static final int m = 3;
    public static final String n = "103";

    /* renamed from: new  reason: not valid java name */
    public static final int f144new = -99;
    public static final int o = 2;
    public static final String p = "102";
    public static final int q = 1;
    public static final String r = "101";
    public static final int s = 0;
    public static final String t = "100";

    /* renamed from: try  reason: not valid java name */
    public static final String f145try = "unUpload";
    public static final String u = "application/zip";

    /* renamed from: void  reason: not valid java name */
    public static final String f146void = "]";
}
