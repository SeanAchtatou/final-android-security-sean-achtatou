package cmcc.location.core;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Location;
import android.util.Log;
import cmcc.location.a.f;
import cmcc.location.a.g;
import java.util.ArrayList;
import java.util.List;

public class d extends SQLiteOpenHelper {

    /* renamed from: a  reason: collision with root package name */
    private static d f212a = null;

    /* renamed from: if  reason: not valid java name */
    private static SQLiteDatabase f132if = null;

    /* renamed from: do  reason: not valid java name */
    private String f133do = "DBHandler";

    d(Context context) {
        super(context, e.f134byte, (SQLiteDatabase.CursorFactory) null, 2);
    }

    private long a(f fVar) {
        if (fVar == null) {
            return -1;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("CREATER", Integer.valueOf(fVar.m67byte()));
        contentValues.put("LOCAL_ADDR", fVar.m73for());
        contentValues.put("LOCAL_PORT", Integer.valueOf(fVar.m78int()));
        contentValues.put("FPP_POS_ADDR", fVar.m68case());
        contentValues.put("FPP_UPLOAD_ADDR", fVar.m70do());
        contentValues.put("FPP_PORT", Integer.valueOf(fVar.m80new()));
        contentValues.put("APN", Integer.valueOf(fVar.a()));
        contentValues.put("SAVE_MAX", Integer.valueOf(fVar.m69char()));
        contentValues.put("INTERVAL", Integer.valueOf(fVar.m82try()));
        contentValues.put("UPLOAD_LIMIT", Integer.valueOf(fVar.m75if()));
        long update = (long) f132if.update("ELSP_CONFIG_INFO_T", contentValues, "CREATER = ?", new String[]{"1"});
        if (update <= 0) {
            Log.v(this.f133do, "更新数据失败！");
            return update;
        }
        Log.v(this.f133do, "更新数据成功！");
        return update;
    }

    public static d a(Context context) {
        if (f212a == null || f132if == null) {
            synchronized (d.class) {
                f212a = new d(context);
                f132if = f212a.getWritableDatabase();
            }
        }
        return f212a;
    }

    /* renamed from: if  reason: not valid java name */
    private long m120if(f fVar) {
        if (fVar == null) {
            return -1;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("CREATER", Integer.valueOf(fVar.m67byte()));
        contentValues.put("LOCAL_ADDR", fVar.m73for());
        contentValues.put("LOCAL_PORT", Integer.valueOf(fVar.m78int()));
        contentValues.put("FPP_POS_ADDR", fVar.m68case());
        contentValues.put("FPP_UPLOAD_ADDR", fVar.m70do());
        contentValues.put("FPP_PORT", Integer.valueOf(fVar.m80new()));
        contentValues.put("APN", Integer.valueOf(fVar.a()));
        contentValues.put("SAVE_MAX", Integer.valueOf(fVar.m69char()));
        contentValues.put("INTERVAL", Integer.valueOf(fVar.m82try()));
        contentValues.put("UPLOAD_LIMIT", Integer.valueOf(fVar.m75if()));
        long insert = f132if.insert("ELSP_CONFIG_INFO_T", null, contentValues);
        if (insert <= 0) {
            Log.v(this.f133do, "插入数据失败！");
            return insert;
        }
        Log.v(this.f133do, "插入数据成功！");
        return insert;
    }

    /* renamed from: int  reason: not valid java name */
    private void m121int() {
        try {
            f132if.execSQL("CREATE TABLE ELSP_CONFIG_INFO_T (CREATER INTEGER PRIMARY KEY, LOCAL_ADDR TEXT NOT NULL, LOCAL_PORT INTEGER NOT NULL, FPP_POS_ADDR TEXT NOT NULL, FPP_UPLOAD_ADDR TEXT NOT NULL, FPP_PORT INTEGER NOT NULL, APN INTEGER,SAVE_MAX INTEGER NOT NULL, INTERVAL INTEGER NOT NULL, UPLOAD_LIMIT INTEGER NOT NULL);");
            f132if.execSQL("INSERT INTO ELSP_CONFIG_INFO_T VALUES(0, 'http://localhost', 1916, 'http://218.206.179.209/FPP_AD/adapter', 'http://218.206.179.209/FPP_AD/adapter', 18080, 0, 10000, 4500, 405)");
        } catch (Exception e) {
            Log.d(this.f133do, "CREATE TABLE ELSP_CONFIG_INFO_T ERROR!!!", e);
            LogUtil.getInstance().log("initDB 异常：" + e.toString());
        }
        try {
            f132if.execSQL("CREATE TABLE ELSP_COLLECTION_DATA_T (CELLID INTEGER, LAC TEXT, TIME TEXT, USERAGENT TEXT,MNC TEXT NOT NULL, MCC TEXT NOT NULL, LATITUDE TEXT, LONGITUDE TEXT, ALTITUDE TEXT, ACCURACY TEXT, IMEI TEXT, IMSI TEXT, RELEX TEXT NOT NULL, WIFI_MAC TEXT, SIGNAL_STRENGTH TEXT, UPLOAD_FLAG TEXT, CONSTRAINT pk_col_data PRIMARY KEY(CELLID, LAC, TIME));");
        } catch (Exception e2) {
            Log.d(this.f133do, "CREATE TABLE ELSP_COLLECTION_DATA_T ERROR!!!", e2);
            LogUtil.getInstance().log("initDB 异常：" + e2.toString());
        }
    }

    public int a(ArrayList arrayList) {
        int i = 0;
        if (arrayList == null || arrayList.isEmpty()) {
            return -99;
        }
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            g gVar = (g) arrayList.get(i2);
            if (gVar == null) {
                Log.d("DBHandler.markUploadData", "PluckDataBean is null");
            } else {
                f132if.execSQL("UPDATE ELSP_COLLECTION_DATA_T SET UPLOAD_FLAG = 'uploaded' WHERE CELLID = " + gVar.m88char() + " AND LAC = '" + gVar.m90do() + "' AND TIME = '" + gVar.m97if() + "'");
                i++;
            }
        }
        Log.d("DBHandler.markUploadData", "Marked Uploaded Data Count : " + i);
        return i;
    }

    public long a(g gVar, f fVar) {
        long j;
        String str = gVar.m94for();
        if (gVar == null || fVar == null) {
            return -1;
        }
        if (gVar.m96goto() == null && (str == null || "".equals(str))) {
            return -1;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("CELLID", Integer.valueOf(gVar.m88char()));
        contentValues.put("LAC", Integer.valueOf(gVar.m90do()));
        contentValues.put("TIME", gVar.m97if());
        contentValues.put("USERAGENT", gVar.m103new());
        contentValues.put("MNC", gVar.m84byte());
        contentValues.put("MCC", gVar.m86case());
        if (gVar.m96goto() != null) {
            contentValues.put("LATITUDE", Double.valueOf(gVar.m96goto().getLatitude()));
            contentValues.put("LONGITUDE", Double.valueOf(gVar.m96goto().getLongitude()));
            contentValues.put("ALTITUDE", Double.valueOf(gVar.m96goto().getAltitude()));
            contentValues.put("ACCURACY", Float.valueOf(gVar.m96goto().getAccuracy()));
        }
        contentValues.put("IMEI", gVar.m93else());
        contentValues.put("IMSI", gVar.m100int());
        contentValues.put("RELEX", Integer.valueOf(gVar.m105try()));
        contentValues.put("WIFI_MAC", gVar.m94for());
        contentValues.put("SIGNAL_STRENGTH", gVar.m102long());
        contentValues.put("UPLOAD_FLAG", e.f145try);
        synchronized (d.class) {
            f132if.insert("ELSP_COLLECTION_DATA_T", null, contentValues);
            j = (long) m122byte();
            if (j > ((long) fVar.m69char())) {
                Log.d("DBHandler.savePluckData", "count (" + j + ") > Max (" + fVar.m69char() + "),Delete Old Data!");
                a(fVar.m69char(), j);
            } else {
                Log.d("DBHandler.savePluckData", "count (" + j + ") <= Max (" + fVar.m69char() + ")");
            }
        }
        return j;
    }

    public ArrayList a() {
        Cursor query = f132if.query("ELSP_COLLECTION_DATA_T", null, null, null, null, null, null);
        if (query == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        query.moveToFirst();
        while (!query.isAfterLast()) {
            g gVar = new g();
            gVar.m91do(query.getInt(query.getColumnIndex("CELLID")));
            gVar.m98if(query.getInt(query.getColumnIndex("LAC")));
            gVar.m101int(query.getString(query.getColumnIndex("TIME")));
            gVar.m92do(query.getString(query.getColumnIndex("USERAGENT")));
            gVar.m99if(query.getString(query.getColumnIndex("MNC")));
            gVar.m106try(query.getString(query.getColumnIndex("MCC")));
            Location location = new Location(e.f143long);
            location.setLatitude(query.getDouble(query.getColumnIndex("LATITUDE")));
            location.setLongitude(query.getDouble(query.getColumnIndex("LONGITUDE")));
            location.setAltitude(query.getDouble(query.getColumnIndex("ALTITUDE")));
            location.setAccuracy((float) query.getInt(query.getColumnIndex("ACCURACY")));
            gVar.a(location);
            gVar.m85byte(query.getString(query.getColumnIndex("IMEI")));
            gVar.a(query.getString(query.getColumnIndex("IMSI")));
            gVar.a(query.getInt(query.getColumnIndex("RELEX")));
            gVar.m87case(query.getString(query.getColumnIndex("WIFI_MAC")));
            gVar.m95for(query.getString(query.getColumnIndex("SIGNAL_STRENGTH")));
            gVar.m89char(query.getString(query.getColumnIndex("UPLOAD_FLAG")));
            arrayList.add(gVar);
            query.moveToNext();
        }
        query.close();
        return arrayList;
    }

    public ArrayList a(int i) {
        Cursor query = f132if.query("ELSP_COLLECTION_DATA_T", null, "UPLOAD_FLAG = 'unUpload'", null, null, null, "TIME DESC", String.valueOf(i));
        if (query == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        query.moveToFirst();
        while (!query.isAfterLast()) {
            g gVar = new g();
            gVar.m91do(query.getInt(query.getColumnIndex("CELLID")));
            gVar.m98if(query.getInt(query.getColumnIndex("LAC")));
            gVar.m101int(query.getString(query.getColumnIndex("TIME")));
            gVar.m92do(query.getString(query.getColumnIndex("USERAGENT")));
            gVar.m99if(query.getString(query.getColumnIndex("MNC")));
            gVar.m106try(query.getString(query.getColumnIndex("MCC")));
            Location location = new Location(e.f143long);
            location.setLatitude(query.getDouble(query.getColumnIndex("LATITUDE")));
            location.setLongitude(query.getDouble(query.getColumnIndex("LONGITUDE")));
            location.setAltitude(query.getDouble(query.getColumnIndex("ALTITUDE")));
            location.setAccuracy((float) query.getInt(query.getColumnIndex("ACCURACY")));
            gVar.a(location);
            gVar.m85byte(query.getString(query.getColumnIndex("IMEI")));
            gVar.a(query.getString(query.getColumnIndex("IMSI")));
            gVar.a(query.getInt(query.getColumnIndex("RELEX")));
            gVar.m87case(query.getString(query.getColumnIndex("WIFI_MAC")));
            gVar.m95for(query.getString(query.getColumnIndex("SIGNAL_STRENGTH")));
            gVar.m89char(query.getString(query.getColumnIndex("UPLOAD_FLAG")));
            arrayList.add(gVar);
            query.moveToNext();
        }
        query.close();
        return arrayList;
    }

    public void a(int i, long j) {
        String str = "DELETE FROM ELSP_COLLECTION_DATA_T WHERE TIME IN (SELECT TIME FROM ELSP_COLLECTION_DATA_T LIMIT " + (j - ((long) i)) + ")";
        Log.d("DBHandler.deleteOldData", "Delete old sql : " + str);
        f132if.execSQL(str);
    }

    /* renamed from: byte  reason: not valid java name */
    public int m122byte() {
        Cursor query = f132if.query("ELSP_COLLECTION_DATA_T", new String[]{"COUNT(*)"}, null, null, null, null, null);
        if (query == null) {
            return -99;
        }
        query.moveToFirst();
        int i = query.getInt(0);
        query.close();
        return i;
    }

    /* renamed from: case  reason: not valid java name */
    public long m123case() {
        return (long) f132if.delete("ELSP_COLLECTION_DATA_T", null, null);
    }

    /* renamed from: do  reason: not valid java name */
    public long m124do(f fVar) {
        if (fVar == null) {
            return -1;
        }
        List list = m129new();
        return (list == null || list.size() < 2) ? m120if(fVar) : a(fVar);
    }

    /* renamed from: do  reason: not valid java name */
    public void m125do() {
        if (f132if != null && f132if.isOpen()) {
            f132if.close();
            f132if = null;
        }
        f212a = null;
    }

    /* renamed from: for  reason: not valid java name */
    public ArrayList m126for() {
        Cursor query = f132if.query("ELSP_COLLECTION_DATA_T", null, "UPLOAD_FLAG = 'unUpload'", null, null, null, "TIME DESC");
        if (query == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        query.moveToFirst();
        while (!query.isAfterLast()) {
            g gVar = new g();
            gVar.m91do(query.getInt(query.getColumnIndex("CELLID")));
            gVar.m98if(query.getInt(query.getColumnIndex("LAC")));
            gVar.m101int(query.getString(query.getColumnIndex("TIME")));
            gVar.m92do(query.getString(query.getColumnIndex("USERAGENT")));
            gVar.m99if(query.getString(query.getColumnIndex("MNC")));
            gVar.m106try(query.getString(query.getColumnIndex("MCC")));
            Location location = new Location(e.f143long);
            location.setLatitude(query.getDouble(query.getColumnIndex("LATITUDE")));
            location.setLongitude(query.getDouble(query.getColumnIndex("LONGITUDE")));
            location.setAltitude(query.getDouble(query.getColumnIndex("ALTITUDE")));
            location.setAccuracy((float) query.getInt(query.getColumnIndex("ACCURACY")));
            gVar.a(location);
            gVar.m85byte(query.getString(query.getColumnIndex("IMEI")));
            gVar.a(query.getString(query.getColumnIndex("IMSI")));
            gVar.a(query.getInt(query.getColumnIndex("RELEX")));
            gVar.m87case(query.getString(query.getColumnIndex("WIFI_MAC")));
            gVar.m95for(query.getString(query.getColumnIndex("SIGNAL_STRENGTH")));
            gVar.m89char(query.getString(query.getColumnIndex("UPLOAD_FLAG")));
            arrayList.add(gVar);
            query.moveToNext();
        }
        query.close();
        return arrayList;
    }

    /* renamed from: if  reason: not valid java name */
    public int m127if() {
        try {
            Cursor query = f132if.query("ELSP_COLLECTION_DATA_T", null, null, null, null, null, null);
            Log.d("exportCsvFile", "Export data count : " + query.getCount());
            if (query == null || query.getCount() <= 0) {
                return 0;
            }
            if (l.a(query)) {
                return query.getCount();
            }
            return -99;
        } catch (Throwable th) {
            Log.e("DBHandler.exportCsvFile", "Export CSV Failed!", th);
            return -99;
        }
    }

    /* renamed from: if  reason: not valid java name */
    public int m128if(ArrayList arrayList) {
        int i = 0;
        if (arrayList == null || arrayList.isEmpty()) {
            return -99;
        }
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            g gVar = (g) arrayList.get(i2);
            if (gVar == null) {
                Log.d("DBHandler.markUploadData", "PluckDataBean is null");
            } else {
                f132if.execSQL("DELETE FROM ELSP_COLLECTION_DATA_T WHERE CELLID = " + gVar.m88char() + " AND LAC = '" + gVar.m90do() + "' AND TIME = '" + gVar.m97if() + "'");
                i++;
            }
        }
        Log.d("DBHandler.markUploadData", "Marked Uploaded Data Count : " + i);
        return i;
    }

    /* renamed from: new  reason: not valid java name */
    public List m129new() {
        try {
            Cursor query = f132if.query("ELSP_CONFIG_INFO_T", null, null, null, null, null, null);
            if (query == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            query.moveToFirst();
            while (!query.isAfterLast()) {
                f fVar = new f();
                fVar.m79int(query.getInt(query.getColumnIndex("CREATER")));
                fVar.m77if(query.getString(query.getColumnIndex("LOCAL_ADDR")));
                fVar.m83try(query.getInt(query.getColumnIndex("LOCAL_PORT")));
                fVar.a(query.getString(query.getColumnIndex("FPP_POS_ADDR")));
                fVar.m72do(query.getString(query.getColumnIndex("FPP_UPLOAD_ADDR")));
                fVar.m71do(query.getInt(query.getColumnIndex("FPP_PORT")));
                fVar.m74for(query.getInt(query.getColumnIndex("APN")));
                fVar.m81new(query.getInt(query.getColumnIndex("SAVE_MAX")));
                fVar.m76if(query.getInt(query.getColumnIndex("INTERVAL")));
                fVar.a(query.getInt(query.getColumnIndex("UPLOAD_LIMIT")));
                arrayList.add(fVar);
                query.moveToNext();
            }
            query.close();
            return arrayList;
        } catch (Exception e) {
            Log.d(this.f133do, "取得配置数据失败！", e);
            LogUtil.getInstance().log("getConfigData 获得初始配置数据失败" + e.toString());
            return null;
        }
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        f132if = sQLiteDatabase;
        m121int();
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        f132if = sQLiteDatabase;
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS ELSP_CONFIG_INFO_T");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS ELSP_COLLECTION_DATA_T");
        m121int();
    }

    /* renamed from: try  reason: not valid java name */
    public int m130try() {
        Cursor query = f132if.query("ELSP_COLLECTION_DATA_T", new String[]{"COUNT(*)"}, "UPLOAD_FLAG = 'unUpload'", null, null, null, null);
        if (query == null) {
            return -99;
        }
        query.moveToFirst();
        int i = query.getInt(0);
        query.close();
        Log.d("DBHandler.getUnUploadDataNum", "UnUpload Data Count : " + i);
        return i;
    }
}
