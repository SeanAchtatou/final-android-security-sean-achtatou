package cmcc.location.core;

import android.content.Context;
import android.os.PowerManager;

public class o {

    /* renamed from: do  reason: not valid java name */
    private static o f174do = null;

    /* renamed from: a  reason: collision with root package name */
    private PowerManager f222a = null;

    /* renamed from: for  reason: not valid java name */
    private Context f175for = null;

    /* renamed from: if  reason: not valid java name */
    private PowerManager.WakeLock f176if = null;

    private o(Context context) {
        this.f175for = context;
        this.f222a = (PowerManager) context.getSystemService("power");
        this.f176if = this.f222a.newWakeLock(1, e.f143long);
    }

    public static synchronized o a(Context context) {
        o oVar;
        synchronized (o.class) {
            if (f174do == null) {
                f174do = new o(context);
            }
            oVar = f174do;
        }
        return oVar;
    }

    public void a() {
        if (this.f176if != null) {
            this.f176if.release();
            this.f176if = null;
            f174do = null;
            this.f222a = null;
        }
    }

    /* renamed from: do  reason: not valid java name */
    public void m163do() {
        if (this.f175for != null && this.f176if != null) {
            this.f176if.acquire();
        }
    }

    /* renamed from: if  reason: not valid java name */
    public boolean m164if() {
        if (this.f222a != null) {
            return this.f222a.isScreenOn();
        }
        return false;
    }
}
