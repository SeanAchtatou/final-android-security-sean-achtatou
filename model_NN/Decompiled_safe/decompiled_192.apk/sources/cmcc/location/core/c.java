package cmcc.location.core;

import a.a.a.d;
import android.content.Context;
import android.location.Location;
import android.util.Xml;
import cmcc.location.a.g;
import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlSerializer;

public class c {

    /* renamed from: do  reason: not valid java name */
    public static final String f129do = "val";

    /* renamed from: a  reason: collision with root package name */
    private h f211a = new h();

    /* renamed from: for  reason: not valid java name */
    private Document f130for = null;

    /* renamed from: if  reason: not valid java name */
    private Element f131if = null;

    public c() throws SAXException, ParserConfigurationException {
        DocumentBuilder newDocumentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        newDocumentBuilder.setErrorHandler(new f());
        this.f130for = newDocumentBuilder.newDocument();
    }

    private void a(Node node, XmlSerializer xmlSerializer) {
        try {
            if (node.getNodeType() == 3) {
                xmlSerializer.text(node.getNodeValue());
                return;
            }
            xmlSerializer.startTag("", node.getNodeName());
            String nodeValue = node.getNodeValue();
            if (nodeValue != null) {
                xmlSerializer.text(nodeValue);
            }
            NamedNodeMap attributes = node.getAttributes();
            int length = attributes.getLength();
            for (int i = 0; i < length; i++) {
                Node item = attributes.item(i);
                xmlSerializer.attribute("", item.getNodeName(), item.getNodeValue());
            }
            NodeList childNodes = node.getChildNodes();
            int length2 = childNodes.getLength();
            for (int i2 = 0; i2 < length2; i2++) {
                a(childNodes.item(i2), xmlSerializer);
            }
            xmlSerializer.endTag("", node.getNodeName());
        } catch (Exception e) {
            LogUtil.getInstance().log("serializNode:" + e.toString());
            e.printStackTrace();
        }
    }

    public Element a() {
        return this.f131if;
    }

    public void a(String str) {
        if (str == null || "".equals(str)) {
            throw new IllegalArgumentException();
        }
        this.f131if = this.f130for.createElement(str);
    }

    public void a(String str, Element element) {
        if (str == null || "".equals(str) || element == null) {
            throw new IllegalArgumentException();
        }
        Element createElement = this.f130for.createElement("SPID");
        createElement.setAttribute(f129do, str);
        element.appendChild(createElement);
    }

    public void a(Date date, Element element) {
        if (date == null || element == null) {
            throw new IllegalArgumentException();
        }
        String a2 = this.f211a.a();
        Element createElement = this.f130for.createElement("Time");
        createElement.setAttribute(f129do, a2);
        element.appendChild(createElement);
    }

    public void a(List list, Element element, Context context) {
        if (list == null || list.size() <= 0 || element == null || context == null) {
            throw new IllegalArgumentException();
        }
        int size = list.size();
        Element createElement = this.f130for.createElement("RecNum");
        createElement.setAttribute(f129do, String.valueOf(size));
        element.appendChild(createElement);
        for (int i = 0; i < size; i++) {
            g gVar = (g) list.get(i);
            Element createElement2 = this.f130for.createElement("UpRec");
            Element createElement3 = this.f130for.createElement("Time");
            createElement3.setAttribute(f129do, gVar.m97if());
            createElement2.appendChild(createElement3);
            m117if(createElement2, context);
            a(createElement2, gVar);
            m118if(createElement2, gVar.m96goto());
            m119if(createElement2, gVar.m94for());
            a(createElement2, gVar.m102long());
            element.appendChild(createElement2);
        }
    }

    public void a(Element element, Context context) {
        if (element == null || context == null) {
            throw new IllegalArgumentException();
        }
        Element createElement = this.f130for.createElement("CellInfo");
        createElement.setAttribute(d.a.g, this.f211a.b(context));
        createElement.setAttribute(d.a.f33goto, this.f211a.m151long(context));
        createElement.setAttribute("lac", String.valueOf(this.f211a.m133char(context)));
        createElement.setAttribute("cid", String.valueOf(this.f211a.m138else(context)));
        createElement.setAttribute("rss", String.valueOf(this.f211a.m152new(context)));
        element.appendChild(createElement);
    }

    public void a(Element element, Location location) {
        if (element == null || location == null) {
            throw new IllegalArgumentException();
        }
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        double altitude = location.getAltitude();
        Element createElement = this.f130for.createElement("Coord");
        createElement.setAttribute("lat", String.valueOf(latitude));
        createElement.setAttribute("lon", String.valueOf(longitude));
        createElement.setAttribute("h", String.valueOf(altitude));
        element.appendChild(createElement);
    }

    public void a(Element element, g gVar) {
        if (element == null || gVar == null) {
            throw new IllegalArgumentException();
        }
        Element createElement = this.f130for.createElement("CellInfo");
        createElement.setAttribute(d.a.g, gVar.m86case());
        createElement.setAttribute(d.a.f33goto, gVar.m84byte());
        createElement.setAttribute("lac", String.valueOf(gVar.m90do()));
        createElement.setAttribute("cid", String.valueOf(gVar.m88char()));
        createElement.setAttribute("rss", String.valueOf(gVar.m105try()));
        element.appendChild(createElement);
    }

    public void a(Element element, String str) {
        if (element != null && str != null && !"".equals(str)) {
            Element createElement = this.f130for.createElement("WLANMatcher");
            createElement.setAttribute(f129do, str);
            element.appendChild(createElement);
        }
    }

    public void a(Element element, String str, String str2) {
        if (element == null || str == null || "".equals(str) || str2 == null || "".equals(str2)) {
            throw new IllegalArgumentException();
        }
        Element createElement = this.f130for.createElement(str);
        createElement.setAttribute(e.c, str2);
        createElement.setAttribute("ErrDesc", this.f211a.m140for(str2));
        element.appendChild(createElement);
    }

    /* renamed from: do  reason: not valid java name */
    public void m113do(Element element, Location location) {
        if (element == null || location == null) {
            throw new IllegalArgumentException();
        }
        float accuracy = location.getAccuracy();
        Element createElement = this.f130for.createElement("ErrRange");
        createElement.setAttribute(f129do, String.valueOf(accuracy));
        element.appendChild(createElement);
    }

    /* renamed from: do  reason: not valid java name */
    public void m114do(Element element, String str) {
        if (element == null || str == null || "".equals(str)) {
            throw new IllegalArgumentException();
        }
        Element createElement = this.f130for.createElement(e.d);
        createElement.setAttribute(f129do, str);
        element.appendChild(createElement);
    }

    /* renamed from: for  reason: not valid java name */
    public void m115for(Element element, String str) {
        if (element == null || str == null || "".equals(str)) {
            throw new IllegalArgumentException();
        }
        Element createElement = this.f130for.createElement("PosLevel");
        createElement.setAttribute(f129do, str);
        element.appendChild(createElement);
    }

    /* renamed from: if  reason: not valid java name */
    public void m116if(String str, Element element) {
        if (str == null || "".equals(str) || element == null) {
            throw new IllegalArgumentException();
        }
        Element createElement = this.f130for.createElement("ServID");
        createElement.setAttribute(f129do, str);
        element.appendChild(createElement);
    }

    /* renamed from: if  reason: not valid java name */
    public void m117if(Element element, Context context) {
        if (context == null || element == null) {
            throw new IllegalArgumentException();
        }
        Element createElement = this.f130for.createElement("UserInfo");
        createElement.setAttribute("imei", this.f211a.m149int(context));
        createElement.setAttribute("imsi", this.f211a.c(context));
        createElement.setAttribute("UserAgent", this.f211a.m139for());
        element.appendChild(createElement);
    }

    /* renamed from: if  reason: not valid java name */
    public void m118if(Element element, Location location) {
        if (element == null) {
            throw new IllegalArgumentException();
        } else if (location != null) {
            Element createElement = this.f130for.createElement("LocInfo");
            createElement.setAttribute("lat", String.valueOf(location.getLatitude()));
            createElement.setAttribute("lon", String.valueOf(location.getLongitude()));
            createElement.setAttribute("h", String.valueOf(location.getAltitude()));
            element.appendChild(createElement);
        }
    }

    /* renamed from: if  reason: not valid java name */
    public void m119if(Element element, String str) {
        if (element != null && str != null && !"".equals(str)) {
            Element createElement = this.f130for.createElement("WLANIdentifier");
            createElement.setAttribute(f129do, str);
            element.appendChild(createElement);
        }
    }

    public String toString() {
        if (this.f130for == null) {
            return null;
        }
        try {
            XmlSerializer newSerializer = Xml.newSerializer();
            StringWriter stringWriter = new StringWriter();
            newSerializer.setOutput(stringWriter);
            newSerializer.startDocument("UTF-8", false);
            newSerializer.docdecl(" " + this.f131if.getNodeName() + " SYSTEM \"" + this.f131if.getNodeName() + ".dtd\"");
            a(this.f131if, newSerializer);
            newSerializer.endDocument();
            return stringWriter.toString();
        } catch (Exception e) {
            e.printStackTrace();
            LogUtil.getInstance().log("doc2TcPosReqBean 异常：" + e.toString());
            return null;
        }
    }
}
