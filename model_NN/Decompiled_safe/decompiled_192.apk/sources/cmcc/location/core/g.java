package cmcc.location.core;

import android.content.Context;
import android.util.Log;
import cmcc.location.a.f;

public class g extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private h f214a = new h();

    /* renamed from: do  reason: not valid java name */
    private final d f147do;

    /* renamed from: for  reason: not valid java name */
    private boolean f148for = true;

    /* renamed from: if  reason: not valid java name */
    private f f149if = null;

    /* renamed from: int  reason: not valid java name */
    private final Context f150int;

    public g(Context context) {
        if (context == null) {
            throw new IllegalArgumentException();
        }
        this.f150int = context;
        this.f147do = d.a(context);
        this.f149if = this.f214a.a(this.f147do.m129new());
    }

    public g(Context context, f fVar) {
        if (context == null || fVar == null) {
            throw new IllegalArgumentException();
        }
        this.f150int = context;
        this.f147do = d.a(context);
        this.f149if = fVar;
    }

    public void interrupt() {
        this.f148for = false;
    }

    public void run() {
        try {
            int i = this.f149if.m82try();
            while (this.f148for && !isInterrupted()) {
                if (!o.a(this.f150int).m164if()) {
                    Log.d("DataPlucker.run", "Screen is off.");
                    sleep((long) i);
                } else {
                    cmcc.location.a.g f = this.f214a.f(this.f150int);
                    if (f == null) {
                        sleep((long) i);
                    } else {
                        this.f147do.a(f, this.f149if);
                        Log.d("DataPlucker.run", "Save Pluck Data Success!");
                        sleep((long) i);
                    }
                }
            }
        } catch (Exception e) {
            Log.e("DataPlucker.run", "Auto Pluck Data Error!Stop Pluck Data!", e);
            LogUtil.getInstance().log("DataPulcker 线程运行异常:" + e.toString());
        }
    }
}
