package cmcc.location.core;

import android.database.Cursor;
import android.util.Log;
import java.io.File;
import java.io.RandomAccessFile;
import java.util.ArrayList;

public class l {
    private static String a(Cursor cursor, String str) throws Exception {
        String str2;
        if (cursor == null || str == null || "".equals(str)) {
            return "";
        }
        if ("SPID".equals(str)) {
            str2 = "1000";
        } else if ("SERVID".equals(str)) {
            str2 = "1000101";
        } else {
            int columnIndex = cursor.getColumnIndex(str);
            if (columnIndex >= 0) {
                str2 = cursor.getString(columnIndex);
                if (str2 == null) {
                    str2 = "";
                }
            } else {
                str2 = "";
            }
        }
        return "\"" + str2 + "\"";
    }

    private static ArrayList a() {
        ArrayList arrayList = new ArrayList();
        arrayList.add("SPID");
        arrayList.add("SERVID");
        arrayList.add("TIME");
        arrayList.add("IMSI");
        arrayList.add("IMEI");
        arrayList.add("USERAGENT");
        arrayList.add("MCC");
        arrayList.add("MNC");
        arrayList.add("LAC");
        arrayList.add("CELLID");
        arrayList.add("RELEX");
        arrayList.add("LATITUDE");
        arrayList.add("LONGITUDE");
        arrayList.add("ALTITUDE");
        arrayList.add("WIFI_MAC");
        arrayList.add("SIGNAL_STRENGTH");
        return arrayList;
    }

    protected static final boolean a(Cursor cursor) {
        RandomAccessFile randomAccessFile;
        Throwable th;
        Throwable th2;
        RandomAccessFile randomAccessFile2 = null;
        if (cursor != null) {
            try {
                if (cursor.getCount() > 0) {
                    randomAccessFile2 = m162if();
                    if (randomAccessFile2 == null) {
                        try {
                            Log.w("CsvExporter.exportData", "RandomAccessFile Create Or Open Failed!");
                        } catch (Throwable th3) {
                            Throwable th4 = th3;
                            randomAccessFile = randomAccessFile2;
                            th2 = th4;
                            cursor.close();
                            try {
                                randomAccessFile.close();
                            } catch (Throwable th5) {
                            }
                            throw th2;
                        }
                    } else {
                        a(cursor, randomAccessFile2);
                        cursor.close();
                        try {
                            randomAccessFile2.close();
                        } catch (Throwable th6) {
                        }
                        return true;
                    }
                }
            } catch (Throwable th7) {
                Throwable th8 = th7;
                randomAccessFile = null;
                th2 = th8;
                cursor.close();
                randomAccessFile.close();
                throw th2;
            }
        }
        cursor.close();
        try {
            randomAccessFile2.close();
        } catch (Throwable th9) {
        }
        return false;
        return false;
        try {
            Log.e("CsvExporter.exportData", "Export CSV Failed!", th);
            cursor.close();
            try {
                randomAccessFile.close();
            } catch (Throwable th10) {
            }
            return false;
        } catch (Throwable th11) {
            th2 = th11;
            cursor.close();
            randomAccessFile.close();
            throw th2;
        }
    }

    private static boolean a(Cursor cursor, RandomAccessFile randomAccessFile) {
        if (cursor == null || cursor.getCount() <= 0 || randomAccessFile == null) {
            return false;
        }
        try {
            ArrayList a2 = a();
            if (a2 == null || a2.size() <= 0) {
                return false;
            }
            int size = a2.size();
            StringBuffer stringBuffer = new StringBuffer();
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                for (int i = 0; i < size; i++) {
                    stringBuffer.append(a(cursor, (String) a2.get(i)));
                    stringBuffer.append(",");
                }
                stringBuffer.deleteCharAt(stringBuffer.lastIndexOf(","));
                stringBuffer.append(e.f138else);
                randomAccessFile.write(stringBuffer.toString().getBytes());
                stringBuffer.setLength(0);
                cursor.moveToNext();
            }
            return true;
        } catch (Throwable th) {
            Log.e("CsvExporter.exportData", "Export Data Error!", th);
            return false;
        }
    }

    /* renamed from: if  reason: not valid java name */
    private static RandomAccessFile m162if() throws Exception {
        if (!new h().m137do()) {
            return null;
        }
        File file = new File("sdcard/cmccLocation/");
        if (!file.exists()) {
            file.mkdirs();
        }
        File file2 = new File("sdcard/cmccLocation/pluckData.csv");
        if (file2.exists()) {
            file2.delete();
        }
        return new RandomAccessFile("sdcard/cmccLocation/pluckData.csv", "rw");
    }
}
