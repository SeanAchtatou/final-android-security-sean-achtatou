package cmcc.location.core;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    static final String f210a = "ISO-8859-1";

    public static String a(String str) throws IOException {
        if (str == null || str.length() == 0) {
            return str;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ZipInputStream zipInputStream = new ZipInputStream(new ByteArrayInputStream(str.getBytes("ISO-8859-1")));
        zipInputStream.getNextEntry();
        byte[] bArr = new byte[256];
        while (true) {
            int read = zipInputStream.read(bArr);
            if (read < 0) {
                return byteArrayOutputStream.toString("ISO-8859-1");
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }

    /* renamed from: if  reason: not valid java name */
    public static String m112if(String str) throws IOException {
        if (str == null || str.length() == 0) {
            return str;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ZipOutputStream zipOutputStream = new ZipOutputStream(byteArrayOutputStream);
        zipOutputStream.putNextEntry(new ZipEntry("0"));
        zipOutputStream.write(str.getBytes());
        zipOutputStream.closeEntry();
        zipOutputStream.close();
        return byteArrayOutputStream.toString("ISO-8859-1");
    }
}
