package cmcc.location.core;

import java.io.File;

public class LogUtil {
    public static final String DEBUG = "DEBUG";
    public static final String ERROR = "ERROR";
    public static final String INFO = "INFO";

    /* renamed from: if  reason: not valid java name */
    private static LogUtil f119if = null;

    /* renamed from: a  reason: collision with root package name */
    private File f203a = null;

    private LogUtil() {
        try {
            this.f203a = new File("/sdcard/logs.txt");
            if (!this.f203a.exists()) {
                this.f203a.createNewFile();
            }
        } catch (Exception e) {
            this.f203a = null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x003b A[SYNTHETIC, Splitter:B:15:0x003b] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0047 A[SYNTHETIC, Splitter:B:21:0x0047] */
    /* JADX WARNING: Removed duplicated region for block: B:32:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(java.io.File r5, java.lang.String... r6) {
        /*
            r4 = this;
            if (r6 == 0) goto L_0x0007
            int r0 = r6.length
            if (r0 <= 0) goto L_0x0007
            if (r5 != 0) goto L_0x0008
        L_0x0007:
            return
        L_0x0008:
            r0 = 0
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0038, all -> 0x0041 }
            r2 = 1
            r1.<init>(r5, r2)     // Catch:{ Exception -> 0x0038, all -> 0x0041 }
            java.lang.String r0 = getLog(r6)     // Catch:{ Exception -> 0x004f, all -> 0x004d }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x004f, all -> 0x004d }
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch:{ Exception -> 0x004f, all -> 0x004d }
            r2.<init>(r0)     // Catch:{ Exception -> 0x004f, all -> 0x004d }
            java.lang.String r0 = cmcc.location.core.e.f138else     // Catch:{ Exception -> 0x004f, all -> 0x004d }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Exception -> 0x004f, all -> 0x004d }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x004f, all -> 0x004d }
            byte[] r0 = r0.getBytes()     // Catch:{ Exception -> 0x004f, all -> 0x004d }
            r1.write(r0)     // Catch:{ Exception -> 0x004f, all -> 0x004d }
            r1.flush()     // Catch:{ Exception -> 0x004f, all -> 0x004d }
            if (r1 == 0) goto L_0x0007
            r1.close()     // Catch:{ Exception -> 0x0036 }
            goto L_0x0007
        L_0x0036:
            r0 = move-exception
            goto L_0x0007
        L_0x0038:
            r1 = move-exception
        L_0x0039:
            if (r0 == 0) goto L_0x0007
            r0.close()     // Catch:{ Exception -> 0x003f }
            goto L_0x0007
        L_0x003f:
            r0 = move-exception
            goto L_0x0007
        L_0x0041:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
        L_0x0045:
            if (r1 == 0) goto L_0x004a
            r1.close()     // Catch:{ Exception -> 0x004b }
        L_0x004a:
            throw r0
        L_0x004b:
            r1 = move-exception
            goto L_0x004a
        L_0x004d:
            r0 = move-exception
            goto L_0x0045
        L_0x004f:
            r0 = move-exception
            r0 = r1
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: cmcc.location.core.LogUtil.a(java.io.File, java.lang.String[]):void");
    }

    public static LogUtil getInstance() {
        synchronized (LogUtil.class) {
            if (f119if == null) {
                f119if = new LogUtil();
            }
        }
        return f119if;
    }

    public static String getLog(String... strArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (String append : strArr) {
            stringBuffer.append(e.f140goto);
            stringBuffer.append(append);
            stringBuffer.append(e.f146void);
        }
        return stringBuffer.toString();
    }

    public void log(String... strArr) {
        a(this.f203a, strArr);
    }
}
