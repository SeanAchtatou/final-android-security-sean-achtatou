package cmcc.location.core;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

public class j {

    /* renamed from: a  reason: collision with root package name */
    private static j f217a = null;

    /* renamed from: do  reason: not valid java name */
    private static a f160do = null;
    /* access modifiers changed from: private */

    /* renamed from: if  reason: not valid java name */
    public static int f161if = 0;

    /* renamed from: int  reason: not valid java name */
    private static Context f162int = null;
    /* access modifiers changed from: private */

    /* renamed from: for  reason: not valid java name */
    public SharedPreferences f163for = null;

    class a extends BroadcastReceiver {

        /* renamed from: if  reason: not valid java name */
        private h f164if = new h();

        a() {
        }

        public void onReceive(Context context, Intent intent) {
            Bundle extras = intent.getExtras();
            if (extras == null) {
                Log.w("SignalStrengthReceiver.onReceive", "Bundle is null.");
                return;
            }
            if (extras.getInt("asu") != 0) {
                j.f161if = this.f164if.a(extras.getInt("asu"));
                Log.d("SignalStrengthReceiver.onReceive", "OMS 1.5 Signal Strength." + j.f161if);
            } else {
                j.f161if = this.f164if.a(extras.getInt("GsmSignalStrength"));
                Log.d("SignalStrengthReceiver.onReceive", "OMS 2.0 Signal Strength." + j.f161if);
            }
            SharedPreferences.Editor edit = j.this.f163for.edit();
            edit.putInt(e.f142int, j.f161if);
            if (edit.commit()) {
                Log.d("Receive", "Save Signal Strength Success!");
            } else {
                Log.d("Receive", "Save Signal Strength Failed!");
            }
            Log.d("Receive", "Signal Strength : " + j.f161if);
        }
    }

    private j() {
        if (f160do == null) {
            f160do = new a();
        }
    }

    /* renamed from: for  reason: not valid java name */
    public static j m157for() {
        if (f217a == null) {
            f217a = new j();
        }
        return f217a;
    }

    public int a() {
        return f161if;
    }

    public synchronized void a(Context context) {
        if (f162int == null) {
            f162int = context;
            this.f163for = f162int.getSharedPreferences(e.e, 0);
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.SIG_STR");
            f162int.registerReceiver(f160do, intentFilter);
            Log.d("Receive", "Signal Strength : Listen");
        }
    }

    /* renamed from: do  reason: not valid java name */
    public void m159do() {
        if (f162int != null && f160do != null) {
            try {
                f162int.unregisterReceiver(f160do);
                Log.d("Receive", "Signal Strength : Remove Listener");
            } catch (Throwable th) {
                Log.e("removeListener", "Remove Signal Listener Error.", th);
            } finally {
                Log.d("SignalStrengthListener", "removeListener me = null");
                f217a = null;
                f162int = null;
                f160do = null;
            }
        }
    }
}
