package com.autonavi.mapapi;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int beta = 2130837504;
        public static final int nomap = 2130837505;
        public static final int poi_1 = 2130837506;
        public static final int poi_2 = 2130837507;
        public static final int zoom_in_true = 2130837508;
        public static final int zoom_out_true = 2130837509;
    }
}
