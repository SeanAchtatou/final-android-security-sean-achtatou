package com.mapabc.mapapi;

import android.os.Message;

final class Y {

    /* renamed from: a  reason: collision with root package name */
    private J f290a = null;
    private Message b = null;
    private Runnable c = null;
    private /* synthetic */ MapController d;

    Y(MapController mapController) {
        this.d = mapController;
    }

    public final void a(GeoPoint geoPoint, Message message, Runnable runnable) {
        b();
        this.f290a = new J(this.d.f247a.b.b(), geoPoint, this);
        this.b = message;
        this.c = runnable;
        this.f290a.e();
    }

    public final void b() {
        if (this.f290a != null) {
            this.f290a.f();
        }
    }

    public final void a(GeoPoint geoPoint) {
        this.d.setCenter(geoPoint);
    }

    public final void a() {
        if (this.b != null) {
            this.b.getTarget().sendMessage(this.b);
        }
        if (this.c != null) {
            this.c.run();
        }
        this.f290a = null;
        this.b = null;
        this.c = null;
    }
}
