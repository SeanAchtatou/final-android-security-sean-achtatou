package com.mapabc.mapapi;

import a.a.a.d;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import java.util.ArrayList;

final class G extends C0020an implements BaseColumns {

    /* renamed from: a  reason: collision with root package name */
    private ArrayList<A> f230a = new ArrayList<>();

    G() {
    }

    public final void a(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL(String.format("create TABLE %s ( %s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER, %s INTEGER, %s INTEGER, %s INTEGER, %s BLOB);", "tile", d.a.f, "line", "colomn", "zoom", "time", "image"));
    }

    public final void b(SQLiteDatabase sQLiteDatabase) {
        Cursor rawQuery = sQLiteDatabase.rawQuery(String.format("SELECT %s, %s, %s, %s FROM %s;", d.a.f, "line", "colomn", "zoom", "tile"), null);
        rawQuery.moveToFirst();
        while (!rawQuery.isAfterLast()) {
            A a2 = new A();
            rawQuery.getInt(0);
            rawQuery.getInt(1);
            rawQuery.getInt(2);
            rawQuery.getInt(3);
            this.f230a.add(a2);
            rawQuery.moveToNext();
        }
        rawQuery.close();
    }
}
