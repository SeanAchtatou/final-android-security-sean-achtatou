package com.mapabc.mapapi;

import android.graphics.Point;
import com.mapabc.mapapi.ConfigableConst;

final class C extends C0023d {
    private aD b = new aD(ConfigableConst.d[ConfigableConst.ENUM_ID.ecompassback.ordinal()], ConfigableConst.d[ConfigableConst.ENUM_ID.ecommpasspoint.ordinal()]);
    private float c = 0.0f;

    public C() {
        super(null);
        c();
    }

    /* access modifiers changed from: protected */
    public final Point a() {
        return new Point(15, 45);
    }

    public final boolean a(float f) {
        boolean z = Math.abs(f - this.c) > 3.0f;
        if (z) {
            this.c = f;
            c();
        }
        return z;
    }

    private void c() {
        this.f316a = this.b.a(this.c);
    }
}
