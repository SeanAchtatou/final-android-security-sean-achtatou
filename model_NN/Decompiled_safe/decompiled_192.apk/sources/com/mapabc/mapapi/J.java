package com.mapabc.mapapi;

final class J extends at {
    private GeoPoint c;
    private GeoPoint d;
    private int e = this.c.getLongitudeAutoNavi();
    private int f = this.c.getLatitudeAutoNavi();
    private int g;
    private int h;
    private int i;
    private int j;
    private int k;
    private int l;
    private Y m;

    public J(GeoPoint geoPoint, GeoPoint geoPoint2, Y y) {
        super(500, 10);
        this.c = geoPoint;
        this.d = geoPoint2;
        this.m = y;
        this.i = Math.abs(geoPoint2.getLongitudeAutoNavi() - this.c.getLongitudeAutoNavi());
        this.j = Math.abs(geoPoint2.getLatitudeAutoNavi() - this.c.getLatitudeAutoNavi());
        this.k = 7;
        this.g = this.i / this.k;
        this.h = this.j / this.k;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.m.a();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        int longitudeAutoNavi = this.d.getLongitudeAutoNavi();
        int latitudeAutoNavi = this.d.getLatitudeAutoNavi();
        if (!g()) {
            this.e = longitudeAutoNavi;
            this.f = latitudeAutoNavi;
            this.m.a(new GeoPoint(this.f, this.e, true));
            return;
        }
        this.l++;
        this.g += this.l * (this.l + 1);
        this.h += this.l * (this.l + 1);
        this.e = a(this.e, longitudeAutoNavi, this.g);
        this.f = a(this.f, latitudeAutoNavi, this.h);
        this.m.a(new GeoPoint(this.f, this.e, true));
    }

    private int a(int i2, int i3, int i4) {
        if (i3 > i2) {
            int i5 = i2 + i4;
            if (i5 < i3) {
                return i5;
            }
            this.l = 0;
            return i3;
        }
        int i6 = i2 - i4;
        if (i6 > i3) {
            return i6;
        }
        this.l = 0;
        return i3;
    }
}
