package com.mapabc.mapapi;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.telephony.CellLocation;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import java.util.LinkedList;

class PhoneStateManager extends PhoneStateListener implements C0007aa {

    /* renamed from: a  reason: collision with root package name */
    private boolean f267a;
    private TelephonyManager b;
    private LocationListener c;
    private LinkedList<b> d = new LinkedList<>();
    private String e;
    private String f;
    private Context g;

    public PhoneStateManager(TelephonyManager telephonyManager, LocationListener locationListener, Context context, String str, String str2) {
        this.b = telephonyManager;
        this.c = locationListener;
        this.g = context;
        this.e = str;
        this.f = str2;
        enableTrans(true);
        this.b.listen(this, 17);
    }

    public void enableTrans(boolean z) {
        this.f267a = z;
    }

    public Location getLastKnownLocation() {
        a(this.b.getCellLocation());
        return a();
    }

    public void onCellLocationChanged(CellLocation cellLocation) {
        if (a(cellLocation)) {
            b();
        }
    }

    private Location a() {
        if (this.d.size() == 0) {
            return null;
        }
        return new Location(this.d.get(0).b);
    }

    private boolean a(CellLocation cellLocation) {
        a aVar = new a(this.b, cellLocation);
        int a2 = a(aVar);
        if (a2 < 0) {
            if (this.f267a) {
                new C0037r(W.b(this.g), this.e, this.f, this).a(aVar);
            }
        } else if (a2 > 0) {
            this.d.remove(a2);
            this.d.addFirst(this.d.get(a2));
        }
        return a2 > 0;
    }

    private void b() {
        Location a2 = a();
        if (a2 != null) {
            this.c.onLocationChanged(a2);
        }
    }

    public void onServiceStateChanged(ServiceState serviceState) {
        int i;
        int state = serviceState.getState();
        if (state == 0) {
            i = 2;
        } else if (state == 1) {
            i = 1;
        } else {
            i = 0;
        }
        this.c.onStatusChanged(LocationProviderProxy.AutonaviCellProvider, i, null);
    }

    public void onTransComplete(b bVar) {
        if (a(bVar.f269a) < 0) {
            if (this.d.size() >= 256) {
                for (int i = 0; i < 128; i++) {
                    this.d.removeLast();
                }
            }
            this.d.addFirst(bVar);
            b();
        }
    }

    private int a(a aVar) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.d.size()) {
                return -1;
            }
            if (this.d.get(i2).f269a.equals(aVar)) {
                return i2;
            }
            i = i2 + 1;
        }
    }

    static final class a {

        /* renamed from: a  reason: collision with root package name */
        public int f268a = -1;
        public int b = -1;
        public int c = -1;
        public int d = -1;
        public int e = -1;
        private int f = -1;

        public a() {
        }

        public a(TelephonyManager telephonyManager, CellLocation cellLocation) {
            String networkOperator = telephonyManager.getNetworkOperator();
            if (networkOperator != null && networkOperator.length() >= 4) {
                this.f = Integer.parseInt(networkOperator.substring(0, 3));
                this.c = Integer.parseInt(networkOperator.substring(3));
                if (cellLocation instanceof GsmCellLocation) {
                    this.f268a = ((GsmCellLocation) cellLocation).getLac();
                    this.b = ((GsmCellLocation) cellLocation).getCid();
                    return;
                }
                try {
                    Class<?> cls = Class.forName("android.telephony.cdma.CdmaCellLocation");
                    if (cls != null) {
                        this.d = ((Integer) cls.newInstance().getClass().getMethod("getBaseStationLatitude", new Class[0]).invoke(cellLocation, new Object[0])).intValue();
                        this.e = ((Integer) cls.newInstance().getClass().getMethod("getBaseStationLongitude", new Class[0]).invoke(cellLocation, new Object[0])).intValue();
                        this.c = ((Integer) cls.newInstance().getClass().getMethod("getNetworkId", new Class[0]).invoke(cellLocation, new Object[0])).intValue();
                        this.b = ((Integer) cls.newInstance().getClass().getMethod("getBaseStationId", new Class[0]).invoke(cellLocation, new Object[0])).intValue();
                        this.f268a = ((Integer) cls.newInstance().getClass().getMethod("getSystemId", new Class[0]).invoke(cellLocation, new Object[0])).intValue();
                    }
                } catch (Throwable th) {
                    System.err.println(th);
                }
            }
        }

        public final boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (obj.getClass() != getClass()) {
                return false;
            }
            a aVar = (a) obj;
            return this.b == aVar.b && this.f268a == aVar.f268a && this.c == aVar.c && this.f == aVar.f;
        }

        public final String toString() {
            return String.format("%d|%d|%d|%d", Integer.valueOf(this.f), Integer.valueOf(this.c), Integer.valueOf(this.f268a), Integer.valueOf(this.b));
        }

        public final int hashCode() {
            return (this.b * 7) + (this.b * 11) + (this.c * 13) + (this.f * 17);
        }
    }

    static final class b {

        /* renamed from: a  reason: collision with root package name */
        public a f269a;
        public Location b;

        public b(a aVar, Location location) {
            this.f269a = aVar;
            this.b = location;
        }
    }
}
