package com.mapabc.mapapi;

/* renamed from: com.mapabc.mapapi.x  reason: case insensitive filesystem */
final class C0043x {

    /* renamed from: a  reason: collision with root package name */
    private Thread[] f329a;

    public C0043x(int i, Runnable runnable) {
        this.f329a = new Thread[i];
        for (int i2 = 0; i2 < i; i2++) {
            this.f329a[i2] = new Thread(runnable);
        }
    }

    public final void a() {
        for (Thread start : this.f329a) {
            start.start();
        }
    }
}
