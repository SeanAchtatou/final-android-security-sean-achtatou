package com.mapabc.mapapi;

import android.content.Context;
import android.os.Build;
import android.telephony.CellLocation;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import com.mapabc.mapapi.PhoneStateManager;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

class GlobalStore {
    static final int MaxLog = 100;

    /* renamed from: a  reason: collision with root package name */
    private static GlobalStore f235a = null;
    private static boolean c = false;
    private static int d = 0;
    private static int e = 0;
    static PhoneStateManager.a sFalseData = new PhoneStateManager.a();
    String IMEI = "unknown";
    int SDKversion = -1;
    a aPhoneStateListener = null;
    /* access modifiers changed from: private */
    public boolean b = true;
    ArrayList<b> centers = new ArrayList<>();
    String device = "unknown";
    LinkedList<C0026g> downloadInfo = new LinkedList<>();
    boolean hasInit = false;
    TelephonyManager mPhone = null;
    String manufacturer = "unknown";
    String model = "unknown";
    LinkedList<PhoneStateManager.a> radioDatas = new LinkedList<>();

    GlobalStore() {
    }

    public static boolean issIsRuning() {
        return c;
    }

    public static void setsIsRuning(boolean z) {
        c = z;
    }

    public static int getsViewWidth() {
        return d;
    }

    public static void setsViewWidth(int i) {
        d = i;
    }

    public static int getsViewHeight() {
        return e;
    }

    public static void setsViewHeight(int i) {
        e = i;
    }

    static class b {

        /* renamed from: a  reason: collision with root package name */
        GeoPoint f237a;
        int b;

        public b(GeoPoint geoPoint, int i) {
            this.f237a = geoPoint;
            this.b = i;
        }
    }

    public synchronized void addCenterIfEmpty(GeoPoint geoPoint, int i) {
        if (this.centers.size() == 0) {
            addCenter(geoPoint, i);
        }
    }

    public synchronized void addCenter(GeoPoint geoPoint, int i) {
        this.centers.add(new b(geoPoint, i));
    }

    public synchronized String centersToStringAndClear() {
        String str;
        Iterator<b> it = this.centers.iterator();
        str = "";
        while (it.hasNext()) {
            b next = it.next();
            str = (str + String.format("%s,%d", next.f237a.toString(), Integer.valueOf(next.b))) + ";";
        }
        this.centers.clear();
        return str;
    }

    public static GlobalStore getInstance() {
        if (f235a == null) {
            f235a = new GlobalStore();
        }
        return f235a;
    }

    public boolean getLogEnable() {
        return this.b;
    }

    public void setEnable(boolean z) {
        this.b = z;
        if (!z) {
            this.downloadInfo.clear();
            if (this.radioDatas.size() > 0) {
                this.radioDatas.clear();
                this.radioDatas.add(this.radioDatas.getLast());
            }
        }
    }

    public void addInfo(int i, long j, long j2, String str) {
        if (this.b && MaxLog > this.downloadInfo.size()) {
            C0026g gVar = new C0026g();
            gVar.b = str;
            gVar.f318a = i;
            gVar.c = j;
            gVar.d = (int) (j2 - j);
            gVar.e = getCurTowerIndex();
            this.downloadInfo.add(gVar);
        }
    }

    public void initDeviceInfo(Context context) {
        if (!this.hasInit) {
            this.hasInit = true;
            try {
                this.SDKversion = ((Integer) Build.VERSION.class.getField("SDK_INT").get(new Build.VERSION())).intValue();
                Class<Build> cls = Build.class;
                Field field = cls.getField("MANUFACTURER");
                if (((String) field.get(new Build())) != null) {
                    this.manufacturer = (String) field.get(new Build());
                }
                Field field2 = cls.getField("MODEL");
                if (((String) field2.get(new Build())) != null) {
                    this.model = (String) field2.get(new Build());
                }
                Field field3 = cls.getField("DEVICE");
                if (((String) field3.get(new Build())) != null) {
                    this.device = (String) field3.get(new Build());
                }
                this.mPhone = (TelephonyManager) context.getSystemService("phone");
                if (this.mPhone.getDeviceId() != null) {
                    this.IMEI = this.mPhone.getDeviceId();
                }
                if (context instanceof MapActivity) {
                    this.aPhoneStateListener = new a();
                    this.aPhoneStateListener.onCellLocationChanged(this.mPhone.getCellLocation());
                    this.mPhone.listen(this.aPhoneStateListener, 16);
                }
            } catch (Exception e2) {
            }
        }
    }

    public aK getLogParam(int i) {
        if (this.downloadInfo.size() < i) {
            return null;
        }
        LinkedList linkedList = new LinkedList();
        for (int i2 = 0; i2 < i; i2++) {
            linkedList.add(this.downloadInfo.removeFirst());
        }
        LinkedList linkedList2 = new LinkedList();
        int i3 = ((C0026g) linkedList.getLast()).e;
        if (i3 >= 0) {
            for (int i4 = 0; i4 < i3; i4++) {
                linkedList2.add(this.radioDatas.removeFirst());
            }
            linkedList2.add(this.radioDatas.getFirst());
            Iterator<C0026g> it = this.downloadInfo.iterator();
            while (it.hasNext()) {
                it.next().e -= i3;
            }
        } else {
            linkedList2.add(getCurTower());
        }
        return new aK(linkedList2, linkedList);
    }

    class a extends PhoneStateListener {
        a() {
        }

        public final void onCellLocationChanged(CellLocation cellLocation) {
            PhoneStateManager.a aVar = new PhoneStateManager.a(GlobalStore.this.mPhone, cellLocation);
            if (!GlobalStore.this.b) {
                GlobalStore.this.radioDatas.clear();
            }
            GlobalStore.this.radioDatas.add(aVar);
        }
    }

    public PhoneStateManager.a getCurTower() {
        if (this.radioDatas == null || this.radioDatas.size() == 0) {
            return sFalseData;
        }
        return this.radioDatas.getLast();
    }

    public int getCurTowerIndex() {
        return this.radioDatas.size() - 1;
    }

    public String getManufacturer() {
        return this.manufacturer;
    }

    public String getModel() {
        return this.model;
    }

    public int getSDKVersion() {
        return this.SDKversion;
    }

    public String getDevice() {
        return this.device;
    }

    public String getIMEI() {
        return this.IMEI;
    }
}
