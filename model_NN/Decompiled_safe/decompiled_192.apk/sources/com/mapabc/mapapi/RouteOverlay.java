package com.mapabc.mapapi;

import android.graphics.Canvas;
import android.graphics.Point;
import android.view.MotionEvent;
import android.view.View;
import com.mapabc.mapapi.MapView;
import com.mapabc.mapapi.Route;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RouteOverlay extends Overlay {
    public static final int OnDetail = 1;
    public static final int OnIconClick = 4;
    public static final int OnNext = 3;
    public static final int OnOverview = 0;
    public static final int OnPrev = 2;

    /* renamed from: a  reason: collision with root package name */
    a f281a = new a();
    private List<I> b = null;
    private C0013ag c = null;
    private boolean d = true;
    /* access modifiers changed from: private */
    public boolean e = true;
    /* access modifiers changed from: private */
    public List<RouteMessageHandler> f = new ArrayList();
    protected MapActivity mContext;
    protected Route mRoute = null;

    public RouteOverlay(MapActivity mapActivity, Route route) {
        aE.a(mapActivity);
        this.mContext = mapActivity;
        this.mRoute = route;
    }

    public Route getRoute() {
        return this.mRoute;
    }

    public boolean onTrackballEvent(MotionEvent motionEvent, MapView mapView) {
        return onTouchEvent(motionEvent, mapView);
    }

    /* JADX WARNING: Removed duplicated region for block: B:3:0x000d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r4, com.mapabc.mapapi.MapView r5) {
        /*
            r3 = this;
            r0 = 0
            java.util.List<com.mapabc.mapapi.I> r1 = r3.b
            java.util.Iterator r1 = r1.iterator()
        L_0x0007:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x0019
            java.lang.Object r3 = r1.next()
            com.mapabc.mapapi.I r3 = (com.mapabc.mapapi.I) r3
            boolean r0 = r3.a(r4, r5)
            if (r0 == 0) goto L_0x0007
        L_0x0019:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mapabc.mapapi.RouteOverlay.onTouchEvent(android.view.MotionEvent, com.mapabc.mapapi.MapView):boolean");
    }

    public void draw(Canvas canvas, MapView mapView, boolean z) {
        for (I a2 : this.b) {
            a2.a(canvas, mapView, z);
        }
    }

    public void enablePopup(boolean z) {
        this.d = z;
        if (!this.d) {
            closePopupWindow();
        }
    }

    public void enableDrag(boolean z) {
        this.e = z;
    }

    public void addToMap(MapView mapView) {
        this.b = new ArrayList();
        this.b.add(new aA(this, 0, this.mRoute.f276a.g(0), ItemizedOverlay.boundCenterBottom(aE.f299a), this.f281a));
        int stepCount = this.mRoute.getStepCount();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= stepCount) {
                break;
            }
            View a2 = this.mRoute.f276a.a(mapView, this.mContext, this.f281a, this, i2);
            if (a2 != null) {
                GeoPoint g = this.mRoute.f276a.g(i2);
                int i3 = i2;
                GeoPoint geoPoint = g;
                this.b.add(new M(this, i3, geoPoint, a2, aE.b(this.mContext), new MapView.LayoutParams(-2, -2, g, 0, 0, 85)));
            }
            this.b.add(new az(this, this.mRoute.getStep(i2).mShapes, this.mRoute.f276a.d(i2)));
            i = i2 + 1;
        }
        this.b.add(new aA(this, stepCount, this.mRoute.f276a.g(stepCount), ItemizedOverlay.boundCenterBottom(aE.b), this.f281a));
        mapView.getOverlays().add(this);
        for (I a3 : this.b) {
            a3.a(mapView);
        }
        mapView.invalidate();
    }

    public void registerRouteMessage(RouteMessageHandler routeMessageHandler) {
        this.f.add(routeMessageHandler);
    }

    public void unregisterRouteMessage(RouteMessageHandler routeMessageHandler) {
        this.f.remove(routeMessageHandler);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public void takeDefaultAction(MapView mapView, int i, int i2) {
        int i3;
        int i4;
        switch (i2) {
            case 0:
                closePopupWindow();
                int zoomLevel = mapView.getZoomLevel();
                D autonaviBound = mapView.getMediator().f258a.getAutonaviBound(zoomLevel);
                long j = (long) (autonaviBound.d - autonaviBound.c);
                GeoPoint lowerLeftPoint = this.mRoute.getLowerLeftPoint();
                GeoPoint upperRightPoint = this.mRoute.getUpperRightPoint();
                long longitudeAutoNavi = (long) (upperRightPoint.getLongitudeAutoNavi() - lowerLeftPoint.getLongitudeAutoNavi());
                long latitudeAutoNavi = (long) (lowerLeftPoint.getLatitudeAutoNavi() - upperRightPoint.getLatitudeAutoNavi());
                int i5 = zoomLevel;
                long j2 = j;
                long j3 = (long) (autonaviBound.b - autonaviBound.f226a);
                while (true) {
                    if (longitudeAutoNavi <= j3 && latitudeAutoNavi <= j2) {
                        if (i5 == zoomLevel) {
                            i3 = i5 - 1;
                        } else {
                            i3 = i5;
                        }
                        if (i3 < mapView.getMinZoomLevel()) {
                            i3 = mapView.getMinZoomLevel();
                        }
                        GeoPoint geoPoint = new GeoPoint((upperRightPoint.getLatitudeE6() + lowerLeftPoint.getLatitudeE6()) / 2, (lowerLeftPoint.getLongitudeE6() + upperRightPoint.getLongitudeE6()) / 2);
                        Point pixels = mapView.getProjection().toPixels(mapView.getMapCenter(), null);
                        a(mapView, pixels.x, pixels.y, i3, false);
                        mapView.getController().setCenter(geoPoint);
                        i4 = i;
                        break;
                    } else {
                        i5--;
                        j3 <<= 1;
                        j2 <<= 1;
                    }
                }
                break;
            case 1:
                closePopupWindow();
                Point a2 = a(mapView, this.mRoute.f276a.g(i));
                int zoomLevel2 = mapView.getZoomLevel();
                a(mapView, a2.x, a2.y, zoomLevel2 >= 15 ? zoomLevel2 + 1 : 15, true);
                i4 = i;
                break;
            case 2:
                i4 = this.mRoute.f276a.f(i);
                break;
            case 3:
                i4 = this.mRoute.f276a.e(i);
                break;
            default:
                i4 = i;
                break;
        }
        showPopupWindow(mapView, i4);
    }

    private static void a(MapView mapView, int i, int i2, int i3, boolean z) {
        mapView.getController().a(i, i2, i3, z);
    }

    public boolean removeFromMap(MapView mapView) {
        boolean remove = mapView.getOverlays().remove(this);
        if (remove) {
            closePopupWindow();
            for (I b2 : this.b) {
                b2.b(mapView);
            }
            mapView.invalidate();
        }
        return remove;
    }

    public boolean showPopupWindow(MapView mapView, int i) {
        if (!this.d || isStardEndMoved()) {
            return false;
        }
        View infoView = getInfoView(mapView, i);
        if (infoView == null) {
            return false;
        }
        GeoPoint g = this.mRoute.f276a.g(i);
        Point a2 = a(mapView, g);
        if (!a(mapView, a2, 30)) {
            a2.x -= mapView.getWidth() / 4;
            mapView.getController().animateTo(mapView.getProjection().fromPixels(a2.x, a2.y));
        }
        this.c = new N(mapView, infoView, g, this, i, (byte) 0);
        this.c.a();
        return true;
    }

    public void closePopupWindow() {
        if (this.c != null) {
            this.c.b();
        }
        this.c = null;
    }

    /* access modifiers changed from: protected */
    public View getInfoView(MapView mapView, int i) {
        return this.mRoute.f276a.a(this.mContext, i);
    }

    public boolean isStardEndMoved() {
        return !getStartPos().equals(this.mRoute.getStartPos()) || !getEndPos().equals(this.mRoute.getTargetPos());
    }

    public void restoreOverlay(MapView mapView) {
        removeFromMap(mapView);
        a(0).c = this.mRoute.getStartPos().Copy();
        a(this.mRoute.getStepCount()).c = this.mRoute.getTargetPos().Copy();
        addToMap(mapView);
    }

    public void renewOverlay(MapView mapView) throws IOException {
        removeFromMap(mapView);
        if (isStardEndMoved()) {
            this.mRoute = Route.calculateRoute(this.mContext, new Route.FromAndTo(getStartPos(), getEndPos(), 0), this.mRoute.getMode()).get(0);
        }
        addToMap(mapView);
    }

    public GeoPoint getStartPos() {
        return a(0).c;
    }

    public GeoPoint getEndPos() {
        return a(this.mRoute.getStepCount()).c;
    }

    static boolean a(MapView mapView, Point point, int i) {
        if (point == null) {
            return false;
        }
        return point.x > i && point.x < mapView.getWidth() - i && point.y > i && point.y < mapView.getHeight() - i;
    }

    static Point a(MapView mapView, GeoPoint geoPoint) {
        return mapView.getProjection().toPixels(geoPoint, null);
    }

    /* access modifiers changed from: private */
    public aA a(int i) {
        return i == 0 ? (aA) this.b.get(0) : (aA) this.b.get(this.b.size() - 1);
    }

    class a implements RouteMessageHandler {
        a() {
        }

        /* JADX WARNING: Removed duplicated region for block: B:3:0x0011  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final boolean onRouteEvent(com.mapabc.mapapi.MapView r4, com.mapabc.mapapi.RouteOverlay r5, int r6, int r7) {
            /*
                r3 = this;
                r0 = 0
                com.mapabc.mapapi.RouteOverlay r1 = com.mapabc.mapapi.RouteOverlay.this
                java.util.List r1 = r1.f
                java.util.Iterator r1 = r1.iterator()
            L_0x000b:
                boolean r2 = r1.hasNext()
                if (r2 == 0) goto L_0x001d
                java.lang.Object r0 = r1.next()
                com.mapabc.mapapi.RouteMessageHandler r0 = (com.mapabc.mapapi.RouteMessageHandler) r0
                boolean r0 = r0.onRouteEvent(r4, r5, r6, r7)
                if (r0 == 0) goto L_0x000b
            L_0x001d:
                if (r0 != 0) goto L_0x0024
                com.mapabc.mapapi.RouteOverlay r1 = com.mapabc.mapapi.RouteOverlay.this
                r1.takeDefaultAction(r4, r6, r7)
            L_0x0024:
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mapabc.mapapi.RouteOverlay.a.onRouteEvent(com.mapabc.mapapi.MapView, com.mapabc.mapapi.RouteOverlay, int, int):boolean");
        }

        private boolean a(MapView mapView, int i, GeoPoint geoPoint) {
            if (!RouteOverlay.this.e) {
                return false;
            }
            RouteOverlay.this.closePopupWindow();
            RouteOverlay.this.a(i).a(geoPoint);
            mapView.invalidate();
            return true;
        }

        public final void onDragBegin(MapView mapView, RouteOverlay routeOverlay, int i, GeoPoint geoPoint) {
            if (a(mapView, i, geoPoint)) {
                for (RouteMessageHandler onDragBegin : RouteOverlay.this.f) {
                    onDragBegin.onDragBegin(mapView, routeOverlay, i, geoPoint);
                }
            }
        }

        public final void onDragEnd(MapView mapView, RouteOverlay routeOverlay, int i, GeoPoint geoPoint) {
            if (a(mapView, i, geoPoint)) {
                for (RouteMessageHandler onDragEnd : RouteOverlay.this.f) {
                    onDragEnd.onDragEnd(mapView, routeOverlay, i, geoPoint);
                }
            }
        }

        public final void onDrag(MapView mapView, RouteOverlay routeOverlay, int i, GeoPoint geoPoint) {
            if (a(mapView, i, geoPoint)) {
                for (RouteMessageHandler onDrag : RouteOverlay.this.f) {
                    onDrag.onDrag(mapView, routeOverlay, i, geoPoint);
                }
            }
        }
    }
}
