package com.mapabc.mapapi;

import android.graphics.Matrix;
import android.view.animation.Animation;

final class aF extends at {
    public int c = -1;
    private Animation.AnimationListener d;
    private MapView e;
    private float f;
    private float g;
    private float h = 1.0f;
    private float i;
    private float j;
    private float k;
    private boolean l;
    private boolean m = false;

    public aF(MapView mapView, Animation.AnimationListener animationListener) {
        super(500, 50);
        this.e = mapView;
        this.d = animationListener;
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.l) {
            this.h += this.k;
        } else {
            this.h -= this.k;
        }
        Matrix matrix = new Matrix();
        matrix.setScale(this.h, this.h, this.f, this.g);
        this.e.getCanvas().onScale(this.h);
        this.e.getCanvas().onScale(matrix);
        this.e.postInvalidate();
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (!this.m) {
            this.e.getMediator().d.f263a = false;
            this.e.getController().setZoom(this.c);
            this.d.onAnimationEnd(null);
        }
    }

    private void a(float f2, boolean z, float f3, float f4) {
        this.l = z;
        this.f = f3;
        this.g = f4;
        this.i = f2;
        this.h = this.i;
        if (this.l) {
            this.k = (this.i * ((float) this.b)) / ((float) this.f311a);
            this.j = this.i * 2.0f;
            return;
        }
        this.k = ((this.i * 0.5f) * ((float) this.b)) / ((float) this.f311a);
        this.j = this.i * 0.5f;
    }

    public final void a(boolean z, float f2, float f3) {
        this.e.getZoomMgr().b();
        if (!g()) {
            a(this.e.getCanvas().a(), z, f2, f3);
            this.e.getMediator().d.g();
            this.e.getMediator().d.f263a = true;
            this.d.onAnimationStart(null);
            super.e();
            return;
        }
        this.m = true;
        f();
        a(this.j, z, f2, f3);
        this.e.getMediator().d.g();
        this.e.getMediator().d.f263a = true;
        this.d.onAnimationStart(null);
        super.e();
        this.m = false;
    }
}
