package com.mapabc.mapapi;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.Spanned;
import android.text.util.Linkify;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.mapabc.mapapi.MapView;
import java.util.ArrayList;
import java.util.List;

public class PoiOverlay extends ItemizedOverlay<PoiItem> {

    /* renamed from: a  reason: collision with root package name */
    private C0013ag f271a;
    private ArrayList<PoiItem> b;
    private boolean c;
    private MapView d;

    public void closePopupWindow() {
        if (this.f271a != null) {
            this.f271a.b();
        }
        this.f271a = null;
    }

    public PoiOverlay(Drawable drawable, List<PoiItem> list) {
        this(drawable, list, PoiTypeDef.All);
    }

    public PoiOverlay(Drawable drawable, List<PoiItem> list, String str) {
        super(drawable);
        this.f271a = null;
        this.b = new ArrayList<>();
        this.c = true;
        for (PoiItem next : list) {
            if (PoiTypeDef.isType(next.getTypeCode(), str)) {
                this.b.add(next);
            }
        }
        populate();
    }

    /* access modifiers changed from: protected */
    public PoiItem createItem(int i) {
        return this.b.get(i);
    }

    public int size() {
        return this.b.size();
    }

    public void addToMap(MapView mapView) {
        this.d = mapView;
        mapView.getOverlays().add(this);
        mapView.invalidate();
    }

    public boolean removeFromMap(MapView mapView) {
        boolean remove = mapView.getOverlays().remove(this);
        if (remove) {
            closePopupWindow();
            mapView.invalidate();
        }
        return remove;
    }

    /* access modifiers changed from: protected */
    public Drawable getPopupMarker(PoiItem poiItem) {
        Drawable marker = poiItem.getMarker(0);
        if (marker == null) {
            marker = a();
        }
        return a(marker, 24, 18);
    }

    public void enablePopup(boolean z) {
        this.c = z;
        if (!this.c) {
            closePopupWindow();
        }
    }

    public boolean showPopupWindow(MapView mapView, int i) {
        if (!this.c) {
            return false;
        }
        this.f271a = new C0013ag(this.d, getPopupView(this.b.get(i)), this.b.get(i).getPoint(), getPopupBackground(), getLayoutParam(i));
        this.f271a.a();
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean onTap(int i) {
        super.onTap(i);
        return showPopupWindow(this.d, i);
    }

    /* access modifiers changed from: protected */
    public Drawable getPopupBackground() {
        return null;
    }

    /* access modifiers changed from: protected */
    public MapView.LayoutParams getLayoutParam(int i) {
        return null;
    }

    /* access modifiers changed from: protected */
    public MapView.LayoutParams getLayoutParam() {
        return null;
    }

    /* access modifiers changed from: protected */
    public View getPopupView(PoiItem poiItem) {
        Context context = this.d.getContext();
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(1);
        linearLayout.setPadding(5, 10, 5, 20);
        LinearLayout linearLayout2 = new LinearLayout(context);
        linearLayout2.setOrientation(0);
        linearLayout2.setGravity(51);
        ImageView imageView = new ImageView(context);
        imageView.setBackgroundColor(-1);
        imageView.setImageDrawable(getPopupMarker(poiItem));
        TextView textView = new TextView(context);
        textView.setBackgroundColor(-1);
        textView.setText(W.c(W.a(poiItem.getTitle(), "#000000")));
        linearLayout2.addView(imageView, new LinearLayout.LayoutParams(-2, -2));
        linearLayout2.addView(textView, new LinearLayout.LayoutParams(-2, -2));
        linearLayout.addView(linearLayout2);
        if (a(poiItem) != null) {
            TextView textView2 = new TextView(context);
            textView2.setBackgroundColor(-1);
            textView2.setText(a(poiItem));
            linearLayout.addView(textView2, new LinearLayout.LayoutParams(-1, -2));
        }
        TextView textView3 = new TextView(context);
        textView3.setBackgroundColor(-1);
        textView3.setText(W.c(W.a("类别: " + poiItem.getTypeDes().split(" - ")[1], "#000000")));
        linearLayout.addView(textView3, new LinearLayout.LayoutParams(-1, -2));
        String tel = poiItem.getTel();
        if (!W.a(tel)) {
            TextView textView4 = new TextView(context);
            String a2 = W.a("Tel:  " + tel, "#000000");
            textView4.setBackgroundColor(-1);
            textView4.setText(W.c(a2));
            textView4.setLinksClickable(true);
            Linkify.addLinks(textView4, 4);
            linearLayout.addView(textView4, new LinearLayout.LayoutParams(-1, -2));
        }
        TextView textView5 = new TextView(context);
        textView5.setText("");
        textView5.setHeight(5);
        textView5.setWidth(1);
        linearLayout.addView(textView5);
        return linearLayout;
    }

    private static Spanned a(PoiItem poiItem) {
        String snippet = poiItem.getSnippet();
        if (W.a(snippet)) {
            return null;
        }
        return W.c(W.a("地址: " + snippet, "#000000"));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    static Drawable a(Drawable drawable, int i, int i2) {
        int intrinsicWidth = drawable.getIntrinsicWidth();
        int intrinsicHeight = drawable.getIntrinsicHeight();
        int intrinsicWidth2 = drawable.getIntrinsicWidth();
        int intrinsicHeight2 = drawable.getIntrinsicHeight();
        Bitmap createBitmap = Bitmap.createBitmap(intrinsicWidth2, intrinsicHeight2, drawable.getOpacity() != -1 ? Bitmap.Config.ARGB_4444 : Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(createBitmap);
        drawable.setBounds(0, 0, intrinsicWidth2, intrinsicHeight2);
        drawable.draw(canvas);
        Matrix matrix = new Matrix();
        matrix.postScale(((float) i) / ((float) intrinsicWidth), ((float) i2) / ((float) intrinsicHeight));
        return new BitmapDrawable(Bitmap.createBitmap(createBitmap, 0, 0, intrinsicWidth, intrinsicHeight, matrix, true));
    }
}
