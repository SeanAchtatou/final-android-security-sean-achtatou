package com.mapabc.mapapi;

import android.graphics.drawable.Drawable;

public class OverlayItem {
    public static final int ITEM_STATE_FOCUSED_MASK = 4;
    public static final int ITEM_STATE_PRESSED_MASK = 1;
    public static final int ITEM_STATE_SELECTED_MASK = 2;
    protected Drawable mMarker = null;
    protected final GeoPoint mPoint;
    protected final String mSnippet;
    protected final String mTitle;

    public OverlayItem(GeoPoint geoPoint, String str, String str2) {
        this.mPoint = geoPoint.Copy();
        this.mTitle = str;
        this.mSnippet = str2;
    }

    public void setMarker(Drawable drawable) {
        this.mMarker = drawable;
        if (this.mMarker != null) {
            this.mMarker.setState(new int[0]);
        }
    }

    public Drawable getMarker(int i) {
        if (this.mMarker == null) {
            return null;
        }
        if (i == 0) {
            return this.mMarker.getCurrent();
        }
        int[] a2 = a(i);
        int[] state = this.mMarker.getState();
        this.mMarker.setState(a2);
        Drawable current = this.mMarker.getCurrent();
        this.mMarker.setState(state);
        return current;
    }

    public static void setState(Drawable drawable, int i) {
        int[] a2 = a(i);
        if (a2.length != 0) {
            drawable.setState(a2);
        }
    }

    private static int[] a(int i) {
        int i2;
        int[] iArr = new int[3];
        if ((i & 1) != 0) {
            iArr[0] = 16842919;
            i2 = 0 + 1;
        } else {
            i2 = 0;
        }
        if ((i & 2) != 0) {
            iArr[i2] = 16842913;
            i2++;
        }
        if ((i & 4) != 0) {
            iArr[i2] = 16842908;
            i2++;
        }
        int[] iArr2 = new int[i2];
        for (int i3 = 0; i3 < i2; i3++) {
            iArr2[i3] = iArr[i3];
        }
        return iArr2;
    }

    public String getTitle() {
        return this.mTitle;
    }

    public String getSnippet() {
        return this.mSnippet;
    }

    public GeoPoint getPoint() {
        return this.mPoint;
    }

    public String routableAddress() {
        return this.mPoint.getLonLatAddr();
    }
}
