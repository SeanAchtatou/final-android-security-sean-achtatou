package com.mapabc.mapapi;

import java.io.IOException;
import java.io.InputStream;
import java.net.Proxy;
import java.util.ArrayList;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

abstract class X<T, V> extends aC<T, V> {

    /* renamed from: a  reason: collision with root package name */
    private ArrayList<String> f289a = new ArrayList<>();

    /* access modifiers changed from: protected */
    public abstract V a(NodeList nodeList);

    public X(T t, Proxy proxy, String str, String str2, String str3) {
        super(t, proxy, str, str2, str3);
    }

    /* access modifiers changed from: protected */
    public final NodeList a(InputStream inputStream) throws IOException {
        int i = 0;
        int i2 = 0;
        String i3 = i(inputStream);
        int i4 = 0;
        do {
            i2 = i3.indexOf("<![CDATA[", i2);
            if (i2 >= 0 && (i = i3.indexOf("]]>", i2 + 9)) > 0) {
                this.f289a.add(i3.substring(i2 + 9, i));
                i3 = i3.substring(0, i2) + String.format("%s%d", "ppppppppShitJava", Integer.valueOf(i4)) + i3.substring(i + 3);
                i4++;
            }
            if (i2 < 0 || i <= 0) {
                break;
            }
        } while (i > i2);
        try {
            break;
            return W.b(i3).getDocumentElement().getChildNodes();
        } catch (Exception e) {
            throw new IOException();
        }
    }

    /* access modifiers changed from: protected */
    public V e(InputStream inputStream) throws IOException {
        return a(a(inputStream));
    }

    /* access modifiers changed from: protected */
    public final String a(Node node) {
        if (node == null) {
            return null;
        }
        Node firstChild = node.getFirstChild();
        if (firstChild == null || firstChild.getNodeType() != 3) {
            return null;
        }
        String nodeValue = firstChild.getNodeValue();
        int indexOf = nodeValue.indexOf("ppppppppShitJava");
        if (indexOf < 0) {
            return nodeValue;
        }
        return this.f289a.get(Integer.parseInt(nodeValue.substring(indexOf + "ppppppppShitJava".length())));
    }
}
