package com.mapabc.mapapi;

import android.location.Criteria;
import android.location.LocationManager;

final class as extends LocationProviderProxy {
    protected as(LocationManager locationManager, String str) {
        super(locationManager, str);
    }

    public final int getAccuracy() {
        return 2;
    }

    public final String getName() {
        return LocationProviderProxy.AutonaviCellProvider;
    }

    public final int getPowerRequirement() {
        return 2;
    }

    public final boolean hasMonetaryCost() {
        return false;
    }

    public final boolean requiresCell() {
        return true;
    }

    public final boolean requiresNetwork() {
        return true;
    }

    public final boolean requiresSatellite() {
        return false;
    }

    public final boolean supportsAltitude() {
        return false;
    }

    public final boolean supportsBearing() {
        return false;
    }

    public final boolean supportsSpeed() {
        return false;
    }

    public final boolean meetsCriteria(Criteria criteria) {
        if (criteria == null) {
            return true;
        }
        return !criteria.isAltitudeRequired() && !criteria.isBearingRequired() && !criteria.isSpeedRequired() && criteria.getAccuracy() == 2 && criteria.getPowerRequirement() != 1;
    }
}
