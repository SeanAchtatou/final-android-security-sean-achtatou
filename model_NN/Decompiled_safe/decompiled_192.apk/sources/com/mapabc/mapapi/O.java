package com.mapabc.mapapi;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;

final class O implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    String f266a;
    int b;

    public O(String str) {
        this(str, C0018al.a());
    }

    private O(String str, int i) {
        this.f266a = str;
        this.b = i;
    }

    public final String toString() {
        return String.format("%d%s%s", Integer.valueOf(this.b), "--XXXDXXX--", this.f266a);
    }

    public static ArrayList<O> a(InputStream inputStream) {
        ArrayList<O> arrayList = new ArrayList<>();
        try {
            char[] cArr = new char[9216];
            int read = new InputStreamReader(inputStream).read(cArr);
            StringBuilder sb = new StringBuilder();
            sb.append(cArr, 0, read);
            for (String str : sb.toString().split("\n")) {
                if (str.length() > 0) {
                    String[] split = str.split("--XXXDXXX--");
                    arrayList.add(new O(split[1], Integer.parseInt(split[0])));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrayList;
    }
}
