package com.mapabc.mapapi;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;

/* renamed from: com.mapabc.mapapi.d  reason: case insensitive filesystem */
abstract class C0023d extends C0025f {

    /* renamed from: a  reason: collision with root package name */
    protected Bitmap f316a;

    /* access modifiers changed from: protected */
    public abstract Point a();

    public C0023d(Bitmap bitmap) {
        this.f316a = bitmap;
    }

    /* access modifiers changed from: protected */
    public final Rect b() {
        Point a2 = a();
        return new Rect(a2.x, a2.y, a2.x + this.f316a.getWidth(), a2.y + this.f316a.getHeight());
    }

    public boolean draw(Canvas canvas, MapView mapView, boolean z, long j) {
        canvas.drawBitmap(this.f316a, (float) b().left, (float) b().top, (Paint) null);
        return true;
    }
}
