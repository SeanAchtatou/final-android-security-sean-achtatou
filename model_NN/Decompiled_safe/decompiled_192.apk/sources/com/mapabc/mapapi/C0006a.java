package com.mapabc.mapapi;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.telephony.TelephonyManager;
import java.util.Iterator;
import java.util.LinkedList;

/* renamed from: com.mapabc.mapapi.a  reason: case insensitive filesystem */
final class C0006a implements LocationListener {

    /* renamed from: a  reason: collision with root package name */
    private String f292a;
    private String b;
    private Context c;
    private TelephonyManager d = null;
    private LinkedList<C0005a> e = new LinkedList<>();
    private LinkedList<b> f = new LinkedList<>();
    private PhoneStateManager g;

    /* renamed from: com.mapabc.mapapi.a$c */
    static class c {
    }

    public C0006a(Context context, String str, String str2) {
        this.c = context;
        this.f292a = str;
        this.b = str2;
        try {
            this.d = (TelephonyManager) context.getSystemService("phone");
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        if (b()) {
            W.c();
            try {
                this.g = new PhoneStateManager(this.d, this, this.c, this.f292a, this.b);
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
    }

    public final Location a() {
        if (!b()) {
            return null;
        }
        this.g.enableTrans(true);
        return this.g.getLastKnownLocation();
    }

    public final boolean b() {
        return this.d != null && this.d.getNetworkOperator() != null && this.d.getNetworkOperator().length() > 0 && !W.a(this.f292a);
    }

    private void c() {
        if (this.d == null) {
            throw new SecurityException();
        } else if (this.d.getNetworkOperator() == null || this.d.getNetworkOperator().length() <= 0) {
            throw new IllegalArgumentException();
        }
    }

    public final void a(LocationListener locationListener, Looper looper) {
        c();
        if (locationListener == null || looper == null || W.a(this.f292a)) {
            throw new IllegalArgumentException();
        }
        this.e.add(new C0005a(locationListener, looper));
    }

    public final void a(PendingIntent pendingIntent) {
        c();
        if (pendingIntent == null || W.a(this.f292a)) {
            throw new IllegalArgumentException();
        }
        this.f.add(new b(pendingIntent));
    }

    public final void a(LocationListener locationListener) {
        this.e.remove(locationListener);
    }

    public final void b(PendingIntent pendingIntent) {
        this.f.remove(pendingIntent);
    }

    /* renamed from: com.mapabc.mapapi.a$a  reason: collision with other inner class name */
    static class C0005a extends c {

        /* renamed from: a  reason: collision with root package name */
        public LocationListener f293a;
        public Looper b;

        public C0005a(LocationListener locationListener, Looper looper) {
            this.f293a = locationListener;
            this.b = looper;
        }

        public final boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (obj.getClass() != getClass()) {
                return false;
            }
            return this.f293a == ((C0005a) obj).f293a;
        }

        public final int hashCode() {
            return this.f293a.hashCode();
        }
    }

    /* renamed from: com.mapabc.mapapi.a$b */
    static class b extends c {

        /* renamed from: a  reason: collision with root package name */
        public PendingIntent f294a;

        public b(PendingIntent pendingIntent) {
            this.f294a = pendingIntent;
        }

        public final boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (obj.getClass() != getClass()) {
                return false;
            }
            return this.f294a == ((b) obj).f294a;
        }

        public final int hashCode() {
            return this.f294a.hashCode();
        }
    }

    /* renamed from: com.mapabc.mapapi.a$g */
    static abstract class g extends Handler {

        /* renamed from: a  reason: collision with root package name */
        protected LocationListener f295a = null;
        private Object b;

        public abstract int a();

        public static g a(Looper looper, LocationListener locationListener, Object obj, int i) {
            switch (i) {
                case 1:
                    return new f(looper, locationListener, obj);
                case 2:
                    return new d(looper, locationListener, obj);
                case 3:
                    return new e(looper, locationListener, obj);
                default:
                    throw new IllegalArgumentException();
            }
        }

        protected g(Looper looper, LocationListener locationListener, Object obj) {
            super(looper);
            this.f295a = locationListener;
            this.b = obj;
        }

        public final void b() {
            sendMessage(obtainMessage(a(), this.b));
        }
    }

    /* renamed from: com.mapabc.mapapi.a$f */
    static class f extends g {
        protected f(Looper looper, LocationListener locationListener, Object obj) {
            super(looper, locationListener, obj);
        }

        public final int a() {
            return 1;
        }

        public final void handleMessage(Message message) {
            if (((Integer) message.obj).intValue() == 0) {
                this.f295a.onProviderDisabled(LocationProviderProxy.AutonaviCellProvider);
            } else {
                this.f295a.onProviderEnabled(LocationProviderProxy.AutonaviCellProvider);
            }
        }
    }

    /* renamed from: com.mapabc.mapapi.a$d */
    static class d extends g {
        protected d(Looper looper, LocationListener locationListener, Object obj) {
            super(looper, locationListener, obj);
        }

        public final int a() {
            return 2;
        }

        public final void handleMessage(Message message) {
            this.f295a.onStatusChanged(LocationProviderProxy.AutonaviCellProvider, ((Integer) message.obj).intValue(), null);
        }
    }

    /* renamed from: com.mapabc.mapapi.a$e */
    static class e extends g {
        protected e(Looper looper, LocationListener locationListener, Object obj) {
            super(looper, locationListener, obj);
        }

        public final int a() {
            return 3;
        }

        public final void handleMessage(Message message) {
            this.f295a.onLocationChanged((Location) message.obj);
        }
    }

    private void a(Object obj, int i) {
        Iterator<C0005a> it = this.e.iterator();
        while (it.hasNext()) {
            C0005a next = it.next();
            g.a(next.b, next.f293a, obj, i).b();
        }
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        switch (i) {
            case 1:
                bundle.putBoolean(LocationManagerProxy.KEY_PROVIDER_ENABLED, ((Integer) obj).intValue() == 1);
                break;
            case 2:
                bundle.putInt(LocationManagerProxy.KEY_STATUS_CHANGED, ((Integer) obj).intValue());
                break;
            case 3:
                bundle.putParcelable(LocationManagerProxy.KEY_LOCATION_CHANGED, (Location) obj);
                break;
            default:
                throw new IllegalArgumentException();
        }
        intent.putExtras(bundle);
        Iterator<b> it2 = this.f.iterator();
        while (it2.hasNext()) {
            try {
                it2.next().f294a.send(this.c, 0, intent);
            } catch (PendingIntent.CanceledException e2) {
            }
        }
    }

    public final void onLocationChanged(Location location) {
        this.g.enableTrans((this.e.size() == 0 && this.f.size() == 0) ? false : true);
        a(location, 3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mapabc.mapapi.a.a(java.lang.Object, int):void
     arg types: [int, int]
     candidates:
      com.mapabc.mapapi.a.a(android.location.LocationListener, android.os.Looper):void
      com.mapabc.mapapi.a.a(java.lang.Object, int):void */
    public final void onProviderDisabled(String str) {
        a((Object) 0, 1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mapabc.mapapi.a.a(java.lang.Object, int):void
     arg types: [int, int]
     candidates:
      com.mapabc.mapapi.a.a(android.location.LocationListener, android.os.Looper):void
      com.mapabc.mapapi.a.a(java.lang.Object, int):void */
    public final void onProviderEnabled(String str) {
        a((Object) 1, 1);
    }

    public final void onStatusChanged(String str, int i, Bundle bundle) {
        a(Integer.valueOf(i), 2);
    }
}
