package com.mapabc.mapapi;

import com.mapabc.mapapi.GeoPoint;
import java.io.IOException;
import java.io.InputStream;
import java.net.Proxy;

/* renamed from: com.mapabc.mapapi.p  reason: case insensitive filesystem */
final class C0035p extends aC<GeoPoint.a, GeoPoint.a> {
    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object e(InputStream inputStream) throws IOException {
        String[] split = h(inputStream).split(",");
        return new GeoPoint.a(Double.parseDouble(split[0]), Double.parseDouble(split[1]));
    }

    public C0035p(GeoPoint.a aVar, Proxy proxy, String str, String str2, String str3) {
        super(aVar, proxy, str, str2, str3);
    }

    /* access modifiers changed from: protected */
    public final String[] e() {
        return new String[]{a("cenx", String.format("%f", Double.valueOf(((GeoPoint.a) this.d).f232a))), a("ceny", String.format("%f", Double.valueOf(((GeoPoint.a) this.d).b)))};
    }

    /* access modifiers changed from: protected */
    public final int f() {
        return 2;
    }
}
