package com.mapabc.mapapi;

import android.graphics.PointF;
import android.util.FloatMath;
import android.view.MotionEvent;
import cmcc.location.core.e;
import java.lang.reflect.InvocationTargetException;

/* renamed from: com.mapabc.mapapi.aj  reason: case insensitive filesystem */
final class C0016aj extends MultiTouchGestureDetector {
    private float k;
    private float l;
    private float m;
    private float n;

    /* synthetic */ C0016aj() {
        this((byte) 0);
    }

    private C0016aj(byte b) {
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z;
        MultiTouchGestureDetector.a(motionEvent);
        if (!MultiTouchGestureDetector.m) {
            return false;
        }
        switch (motionEvent.getAction() & 255) {
            case 0:
                this.k = motionEvent.getX();
                this.l = motionEvent.getY();
                this.d.set(this.c);
                this.e.set(this.k, this.l);
                this.b = 1;
                return false;
            case 1:
                this.j = false;
                this.b = 0;
                return false;
            case 2:
                if (this.b == 1) {
                    float x = motionEvent.getX();
                    float y = motionEvent.getY();
                    this.c.set(this.d);
                    this.c.postTranslate(motionEvent.getX() - this.e.x, motionEvent.getY() - this.e.y);
                    this.k = x;
                    this.l = y;
                    return this.f264a.onDrag(this.c) | this.f264a.onDrag(x - this.k, y - this.l) | false;
                } else if (this.b == 2) {
                    float b = b(motionEvent);
                    if (b > 10.0f) {
                        this.c.set(this.d);
                        this.i = b / this.h;
                        if (this.i < 0.5f) {
                            this.i = 0.5f;
                            a(this.g, motionEvent);
                            z = this.f264a.onDrag(this.g.x - this.m, this.g.y - this.n) | false;
                            this.m = this.g.x;
                            this.n = this.g.y;
                        } else if (this.i > 2.0f) {
                            a(this.g, motionEvent);
                            z = this.f264a.onDrag(this.g.x - this.m, this.g.y - this.n) | false;
                            this.m = this.g.x;
                            this.n = this.g.y;
                        } else {
                            z = false;
                        }
                        this.c.postScale(this.i, this.i, this.f.x, this.f.y);
                        return z | this.f264a.onScale(this.i) | this.f264a.onScale(this.c);
                    }
                }
                break;
            case 5:
                this.h = b(motionEvent);
                if (this.h > 10.0f) {
                    this.c.reset();
                    this.d.reset();
                    this.d.set(this.c);
                    a(this.f, motionEvent);
                    this.b = 2;
                    this.j = true;
                    boolean startScale = this.f264a.startScale(this.e) | false;
                    this.m = this.f.x;
                    this.n = this.f.y;
                    return startScale;
                }
                break;
            case e.g:
                if (this.j) {
                    this.j = false;
                    boolean endScale = this.f264a.endScale(this.i, this.f) | false;
                    this.b = 0;
                    return endScale;
                }
                break;
        }
        return false;
    }

    private static float b(MotionEvent motionEvent) {
        float f;
        float f2;
        try {
            f = ((Float) MultiTouchGestureDetector.k.invoke(motionEvent, 0)).floatValue() - ((Float) MultiTouchGestureDetector.k.invoke(motionEvent, 1)).floatValue();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            f = 0.0f;
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
            f = 0.0f;
        } catch (InvocationTargetException e3) {
            e3.printStackTrace();
            f = 0.0f;
        }
        try {
            f2 = ((Float) MultiTouchGestureDetector.l.invoke(motionEvent, 0)).floatValue() - ((Float) MultiTouchGestureDetector.l.invoke(motionEvent, 1)).floatValue();
        } catch (IllegalArgumentException e4) {
            e4.printStackTrace();
            f2 = 0.0f;
        } catch (IllegalAccessException e5) {
            e5.printStackTrace();
            f2 = 0.0f;
        } catch (InvocationTargetException e6) {
            e6.printStackTrace();
            f2 = 0.0f;
        }
        return FloatMath.sqrt((f2 * f2) + (f * f));
    }

    private static void a(PointF pointF, MotionEvent motionEvent) {
        float f;
        float f2;
        try {
            f = ((Float) MultiTouchGestureDetector.k.invoke(motionEvent, 1)).floatValue() + ((Float) MultiTouchGestureDetector.k.invoke(motionEvent, 0)).floatValue();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            f = 0.0f;
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
            f = 0.0f;
        } catch (InvocationTargetException e3) {
            e3.printStackTrace();
            f = 0.0f;
        }
        try {
            f2 = ((Float) MultiTouchGestureDetector.l.invoke(motionEvent, 1)).floatValue() + ((Float) MultiTouchGestureDetector.l.invoke(motionEvent, 0)).floatValue();
        } catch (IllegalArgumentException e4) {
            e4.printStackTrace();
            f2 = 0.0f;
        } catch (IllegalAccessException e5) {
            e5.printStackTrace();
            f2 = 0.0f;
        } catch (InvocationTargetException e6) {
            e6.printStackTrace();
            f2 = 0.0f;
        }
        pointF.set(f / 2.0f, f2 / 2.0f);
    }
}
