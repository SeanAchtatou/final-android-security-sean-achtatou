package com.mapabc.mapapi;

import android.content.Context;
import com.mapabc.mapapi.Tile;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ClsCachFileManager {
    public static int iTotalByteCount = 0;
    public static List<ClsIndexItem> listIndex = null;
    public boolean boWriteCach = false;
    public BufferedOutputStream bufferCachOutput = null;
    public Context objContext = null;
    public ObjectInputStream objIndexInput = null;
    public ObjectOutputStream objIndexOutput = null;
    public RandomAccessFile randomCash = null;
    public FileOutputStream streamCachOutput = null;
    public FileInputStream streamIndexInput = null;
    public FileOutputStream streamIndexOutput = null;

    public ClsCachFileManager(ClsCachIndexContainer clsCachIndexContainer, Context context) {
    }

    public ClsCachFileManager(Context context) {
        this.objContext = context;
        try {
            this.streamIndexInput = this.objContext.openFileInput("Index.dat");
            this.objIndexInput = new ObjectInputStream(this.streamIndexInput);
            this.objIndexInput.readObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        } catch (ClassNotFoundException e3) {
            e3.printStackTrace();
        }
        if (listIndex == null) {
            listIndex = new ArrayList();
        }
        try {
            this.streamCachOutput = this.objContext.openFileOutput("Data.dat", 32768);
            this.randomCash = new RandomAccessFile(this.objContext.getFileStreamPath("Data.dat"), "r");
        } catch (FileNotFoundException e4) {
            e4.printStackTrace();
        }
        this.bufferCachOutput = new BufferedOutputStream(this.streamCachOutput);
    }

    public long findIndex(long j, String str) {
        if (listIndex == null) {
            return -1;
        }
        int size = listIndex.size();
        if (size == 0) {
            return -1;
        }
        ClsIndexItem clsIndexItem = listIndex.get(0);
        if (clsIndexItem.lIndex == j) {
            return filterTheSameLocation(0, j, clsIndexItem.strKey);
        } else if (clsIndexItem.lIndex > j) {
            return -1;
        } else {
            ClsIndexItem clsIndexItem2 = listIndex.get(size - 1);
            if (clsIndexItem2.lIndex == j) {
                return filterTheSameLocation((long) (size - 1), j, clsIndexItem2.strKey);
            } else if (clsIndexItem2.lIndex < j) {
                return -1;
            } else {
                if (size == 2) {
                    return -1;
                }
                int i = (size - 1) / 2;
                ClsIndexItem clsIndexItem3 = listIndex.get(i);
                if (clsIndexItem3.lIndex == j) {
                    return filterTheSameLocation((long) i, j, clsIndexItem3.strKey);
                } else if (clsIndexItem3.lIndex < j) {
                    return findIndexDichotomy((long) i, (long) (size - 1), j);
                } else {
                    return findIndexDichotomy(0, (long) i, j);
                }
            }
        }
    }

    public long findIndexDichotomy(long j, long j2, long j3) {
        ClsIndexItem clsIndexItem = listIndex.get((int) j);
        if (clsIndexItem.lIndex == j3) {
            return filterTheSameLocation(j, j3, clsIndexItem.strKey);
        }
        ClsIndexItem clsIndexItem2 = listIndex.get((int) j2);
        if (clsIndexItem2.lIndex == j3) {
            return filterTheSameLocation(j2, j3, clsIndexItem2.strKey);
        } else if (j == j2 - 1) {
            return -1;
        } else {
            long j4 = (j + j2) / 2;
            ClsIndexItem clsIndexItem3 = listIndex.get((int) j4);
            if (clsIndexItem3.lIndex == j3) {
                return filterTheSameLocation(j4, j3, clsIndexItem3.strKey);
            } else if (clsIndexItem3.lIndex < j3) {
                return findIndexDichotomy(j4, j2, j3);
            } else {
                return findIndexDichotomy(j, j4, j3);
            }
        }
    }

    public long filterTheSameLocation(long j, long j2, String str) {
        if (listIndex.get((int) j).strKey == str) {
            return j;
        }
        long j3 = j - 1;
        long j4 = j + 1;
        long size = (long) listIndex.size();
        while (j3 >= 0) {
            ClsIndexItem clsIndexItem = listIndex.get((int) j3);
            if (clsIndexItem.lIndex != j2) {
                break;
            } else if (clsIndexItem.strKey == str) {
                return j3;
            } else {
                j3--;
            }
        }
        long j5 = j4;
        while (j5 < size) {
            ClsIndexItem clsIndexItem2 = listIndex.get((int) j5);
            if (clsIndexItem2.lIndex != j2) {
                break;
            } else if (clsIndexItem2.strKey == str) {
                return j5;
            } else {
                j5++;
            }
        }
        return -1;
    }

    public synchronized boolean addIndex(ClsIndexItem clsIndexItem) {
        boolean z;
        if (clsIndexItem != null) {
            if (clsIndexItem.lIndex >= 0 && listIndex != null) {
                int size = listIndex.size();
                if (size == 0) {
                    listIndex.add(clsIndexItem);
                    z = true;
                } else if (size == 1) {
                    if (listIndex.get(0).lIndex > clsIndexItem.lIndex) {
                        listIndex.add(0, clsIndexItem);
                    } else {
                        listIndex.add(clsIndexItem);
                    }
                    z = true;
                } else {
                    z = addIndexDichotomy(0, size - 1, clsIndexItem);
                }
            }
        }
        z = false;
        return z;
    }

    public boolean addIndexDichotomy(int i, int i2, ClsIndexItem clsIndexItem) {
        if (clsIndexItem.lIndex <= listIndex.get(i).lIndex) {
            listIndex.add(i, clsIndexItem);
            return true;
        } else if (clsIndexItem.lIndex >= listIndex.get(i2).lIndex) {
            listIndex.add(clsIndexItem);
            return true;
        } else if (i2 == i + 1) {
            listIndex.add(i2, clsIndexItem);
            return true;
        } else {
            int i3 = (i + i2) / 2;
            if (listIndex.get(i3).lIndex > clsIndexItem.lIndex) {
                return addIndexDichotomy(i, i3, clsIndexItem);
            }
            return addIndexDichotomy(i3, i2, clsIndexItem);
        }
    }

    public synchronized int addCachData(byte[] bArr) {
        int i;
        if (this.bufferCachOutput == null) {
            i = -1;
        } else {
            try {
                this.bufferCachOutput.write(bArr);
                this.bufferCachOutput.flush();
                i = bArr.length;
                iTotalByteCount += i;
                if (!this.boWriteCach) {
                    this.boWriteCach = true;
                }
            } catch (IOException e) {
                e.printStackTrace();
                i = -1;
            }
        }
        return i;
    }

    public synchronized byte[] readCachData(long j, int i) {
        byte[] bArr;
        if (j < 0 || i <= 0) {
            bArr = null;
        } else {
            bArr = new byte[i];
            try {
                this.randomCash.seek(j);
                this.randomCash.read(bArr);
            } catch (IOException e) {
                e.printStackTrace();
                bArr = null;
            }
        }
        return bArr;
    }

    public void saveCachFile() {
        if (this.randomCash != null) {
            try {
                this.randomCash.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (this.bufferCachOutput != null) {
            try {
                this.bufferCachOutput.flush();
                this.bufferCachOutput.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        if (this.streamCachOutput != null) {
            try {
                this.streamCachOutput.flush();
                this.streamCachOutput.close();
            } catch (IOException e3) {
                e3.printStackTrace();
            }
        }
        if (this.streamIndexInput != null) {
            try {
                this.streamIndexInput.close();
            } catch (IOException e4) {
                e4.printStackTrace();
            }
        }
        if (this.objIndexInput != null) {
            try {
                this.objIndexInput.close();
            } catch (IOException e5) {
                e5.printStackTrace();
            }
        }
        if (this.boWriteCach) {
            this.objContext.deleteFile("Index.dat");
            try {
                this.streamIndexOutput = this.objContext.openFileOutput("Index.dat", 32768);
                this.objIndexOutput = new ObjectOutputStream(this.streamIndexOutput);
                this.objIndexOutput.writeObject(new ClsCachIndexContainer(this));
                this.objIndexOutput.close();
                this.streamIndexOutput.flush();
                this.streamIndexOutput.close();
            } catch (FileNotFoundException e6) {
                e6.printStackTrace();
            } catch (IOException e7) {
                e7.printStackTrace();
            }
        }
    }

    public Tile getTileFromCach(Tile.TileCoordinate tileCoordinate) {
        long findIndex = findIndex(getUniqueLongKey(tileCoordinate.X, tileCoordinate.Y, tileCoordinate.Zoom), getUniqueStringKey(tileCoordinate.X, tileCoordinate.Y, tileCoordinate.Zoom));
        if (findIndex == -1) {
            return null;
        }
        ClsIndexItem clsIndexItem = listIndex.get((int) findIndex);
        if (clsIndexItem == null) {
            return null;
        }
        byte[] readCachData = readCachData(clsIndexItem.iBeginByteLocation, clsIndexItem.iByteLength);
        if (readCachData == null) {
            return null;
        }
        Tile tile = new Tile(tileCoordinate);
        tile.a(readCachData);
        return tile;
    }

    public int getUniqueIntKey(int i, int i2, int i3) {
        return new Integer("" + i3 + i + i2).intValue();
    }

    public long getUniqueLongKey(int i, int i2, int i3) {
        return Long.parseLong("" + i3 + i + i2);
    }

    public String getUniqueStringKey(int i, int i2, int i3) {
        return i3 + "-" + i + "-" + i2;
    }

    public class ClsCachIndexContainer implements Serializable {
        public static final long serialVersionUID = 1;
        public int iTotalByteCount = 0;
        public List<ClsIndexItem> listIndex = new ArrayList();

        public ClsCachIndexContainer(ClsCachFileManager clsCachFileManager) {
        }

        public void writeObject(ObjectOutputStream objectOutputStream) {
            try {
                ObjectOutputStream.PutField putFields = objectOutputStream.putFields();
                putFields.put("listIndex", ClsCachFileManager.listIndex);
                putFields.put("iTotalByteCount", ClsCachFileManager.iTotalByteCount);
                objectOutputStream.writeFields();
                objectOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void readObject(ObjectInputStream objectInputStream) {
            try {
                ObjectInputStream.GetField readFields = objectInputStream.readFields();
                Object obj = readFields.get("listIndex", (Object) null);
                if (obj != null) {
                    ClsCachFileManager.listIndex = (List) obj;
                }
                ClsCachFileManager.iTotalByteCount = readFields.get("iTotalByteCount", 0);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e2) {
                e2.printStackTrace();
            }
        }
    }
}
