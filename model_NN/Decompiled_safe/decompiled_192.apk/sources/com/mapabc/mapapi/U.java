package com.mapabc.mapapi;

import a.a.a.d;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

final class U extends C0020an implements BaseColumns {

    /* renamed from: a  reason: collision with root package name */
    private int f286a = 0;

    U() {
    }

    public final void a(SQLiteDatabase sQLiteDatabase) {
        String.format("delete from %s;", "checker");
        sQLiteDatabase.execSQL(String.format("create TABLE %s ( %s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER);", "checker", d.a.f, "checktime"));
        c(sQLiteDatabase);
    }

    public final void c(SQLiteDatabase sQLiteDatabase) {
        this.f286a = C0032m.b();
        sQLiteDatabase.execSQL(String.format("insert into %s (%s) VALUES(%d);", "checker", "checktime", Integer.valueOf(this.f286a)));
    }

    public final void b(SQLiteDatabase sQLiteDatabase) {
        Cursor rawQuery = sQLiteDatabase.rawQuery(String.format("select %s from %s;", "checktime", "checker"), null);
        rawQuery.moveToFirst();
        this.f286a = rawQuery.getInt(0);
        rawQuery.close();
    }

    public final boolean a() {
        return C0032m.b() - this.f286a > 1;
    }
}
