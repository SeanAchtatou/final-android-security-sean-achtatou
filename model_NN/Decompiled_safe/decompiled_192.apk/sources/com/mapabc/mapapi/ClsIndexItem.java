package com.mapabc.mapapi;

import java.io.Serializable;

public class ClsIndexItem implements Serializable {
    private static final long serialVersionUID = 1;
    public long iBeginByteLocation = 0;
    public int iByteLength = 0;
    public long lIndex = 0;
    public String strKey = "";
}
