package com.mapabc.mapapi;

import java.io.IOException;
import java.io.InputStream;
import java.net.Proxy;

/* renamed from: com.mapabc.mapapi.am  reason: case insensitive filesystem */
abstract class C0019am extends C0022c {
    public C0019am(aN aNVar, Proxy proxy, String str, String str2) {
        super(aNVar, proxy, str, str2);
    }

    /* access modifiers changed from: protected */
    public final Segment b(InputStream inputStream) throws IOException {
        DriveWalkSegment driveWalkSegment = new DriveWalkSegment();
        driveWalkSegment.mRoadName = h(inputStream);
        driveWalkSegment.mLength = f(inputStream);
        driveWalkSegment.mActionCode = inputStream.read();
        driveWalkSegment.mActionDes = h(inputStream);
        driveWalkSegment.mShapes = d(inputStream);
        return driveWalkSegment;
    }

    /* access modifiers changed from: protected */
    public final void a(Route route) {
        for (int size = route.mSegs.size() - 1; size > 0; size--) {
            DriveWalkSegment driveWalkSegment = (DriveWalkSegment) route.mSegs.get(size);
            DriveWalkSegment driveWalkSegment2 = (DriveWalkSegment) route.mSegs.get(size - 1);
            driveWalkSegment.mActionCode = driveWalkSegment2.mActionCode;
            driveWalkSegment.mActionDes = driveWalkSegment2.mActionDes;
        }
        DriveWalkSegment driveWalkSegment3 = (DriveWalkSegment) route.mSegs.get(0);
        driveWalkSegment3.mActionCode = -1;
        driveWalkSegment3.mActionDes = "";
    }
}
