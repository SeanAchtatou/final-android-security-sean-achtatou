package com.mapabc.mapapi;

import com.mapabc.mapapi.PoiSearch;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/* renamed from: com.mapabc.mapapi.b  reason: case insensitive filesystem */
final class C0021b extends P<Z, PoiItem> {

    /* renamed from: a  reason: collision with root package name */
    private int f313a = 1;
    private int b = 0;
    private ArrayList<String> g = new ArrayList<>();

    public C0021b(Z z, Proxy proxy, String str, String str2) {
        super(z, proxy, str, str2);
    }

    public final void a(int i) {
        this.f313a = i;
    }

    public final int a() {
        return this.b;
    }

    public final PoiSearch.Query b() {
        return ((Z) this.d).f291a;
    }

    public final PoiSearch.SearchBound c() {
        return ((Z) this.d).b;
    }

    public final List<String> d() {
        return this.g;
    }

    /* access modifiers changed from: protected */
    public final String[] e() {
        String[] strArr = new String[5];
        strArr[0] = a("keyword", ((Z) this.d).f291a.getQueryString());
        strArr[1] = a("category", ((Z) this.d).f291a.getCategory());
        if (((Z) this.d).b != null) {
            strArr[2] = a("geoobj", String.format("%f;%f;%f;%f", Double.valueOf(W.c(((Z) this.d).b.getLowerLeft().getLongitudeE6())), Double.valueOf(W.c(((Z) this.d).b.getUpperRight().getLatitudeE6())), Double.valueOf(W.c(((Z) this.d).b.getUpperRight().getLongitudeE6())), Double.valueOf(W.c(((Z) this.d).b.getLowerLeft().getLatitudeE6()))));
        } else {
            strArr[2] = a("adcode", ((Z) this.d).f291a.getCity());
        }
        strArr[3] = a("batch", String.format("%d", Integer.valueOf(this.f313a)));
        strArr[4] = a("number", String.format("%d", 25));
        return strArr;
    }

    /* access modifiers changed from: protected */
    public final int f() {
        return 20;
    }

    /* access modifiers changed from: protected */
    public final void a(ArrayList<PoiItem> arrayList) {
    }

    /* access modifiers changed from: protected */
    public final void a(Node node, ArrayList<PoiItem> arrayList) {
        if (node.getNodeType() == 1 && node.getNodeName().equals("poilist")) {
            NodeList childNodes = node.getChildNodes();
            for (int i = 0; i < childNodes.getLength(); i++) {
                Node item = childNodes.item(i);
                if (item.getNodeType() == 1) {
                    String nodeName = item.getNodeName();
                    int i2 = this.f313a;
                    if (nodeName.equals("count") && i2 == 1) {
                        this.b = Integer.parseInt(a(item));
                    } else if (nodeName.equals("pinyin") && i2 == 1) {
                        NodeList childNodes2 = item.getChildNodes();
                        for (int i3 = 0; i3 < childNodes2.getLength(); i3++) {
                            Node item2 = childNodes2.item(i3);
                            if (item2.getNodeType() == 1 && item2.getNodeName().equals("list")) {
                                NodeList childNodes3 = item2.getChildNodes();
                                for (int i4 = 0; i4 < childNodes3.getLength(); i4++) {
                                    Node item3 = childNodes3.item(i3);
                                    if (item3.getNodeType() == 1 && item3.getNodeName().equals("data")) {
                                        this.g.add(a(item3));
                                    }
                                }
                            }
                        }
                    } else if (nodeName.equals("poi")) {
                        a aVar = new a();
                        NodeList childNodes4 = item.getChildNodes();
                        for (int i5 = 0; i5 < childNodes4.getLength(); i5++) {
                            Node item4 = childNodes4.item(i5);
                            if (item4.getNodeType() == 1) {
                                try {
                                    String nodeName2 = item4.getNodeName();
                                    String a2 = a(item4);
                                    if (nodeName2.equals("name")) {
                                        aVar.b = a2;
                                    } else if (nodeName2.equals("pguid")) {
                                        aVar.f314a = a2;
                                    } else if (nodeName2.equals("newtype")) {
                                        aVar.c = a2.substring(0, 4);
                                    } else if (nodeName2.equals("type")) {
                                        String[] split = a2.split(";");
                                        aVar.d = split[0] + " - " + split[1];
                                    } else if (nodeName2.equals("address")) {
                                        aVar.e = a2;
                                    } else if (nodeName2.equals("tel")) {
                                        aVar.f = a2;
                                    } else if (nodeName2.equals("code")) {
                                        aVar.g = a2;
                                    } else if (nodeName2.equals("x")) {
                                        aVar.h = Double.parseDouble(a2);
                                    } else if (nodeName2.equals("y")) {
                                        aVar.i = Double.parseDouble(a2);
                                    }
                                } catch (Exception e) {
                                }
                            }
                        }
                        PoiItem poiItem = new PoiItem(aVar.f314a, new GeoPoint(W.a(aVar.i), W.a(aVar.h)), aVar.b, aVar.e);
                        poiItem.setAdCode(aVar.g);
                        poiItem.setTel(aVar.f);
                        poiItem.setTypeCode(aVar.c);
                        poiItem.setTypeDes(aVar.d);
                        arrayList.add(poiItem);
                    }
                }
            }
        }
    }

    /* renamed from: com.mapabc.mapapi.b$a */
    static class a {

        /* renamed from: a  reason: collision with root package name */
        String f314a;
        String b;
        String c;
        String d;
        String e;
        String f;
        String g;
        double h;
        double i;

        a() {
        }
    }
}
