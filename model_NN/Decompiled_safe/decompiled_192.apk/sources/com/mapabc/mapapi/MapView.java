package com.mapabc.mapapi;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Scroller;
import android.widget.TextView;
import com.mapabc.mapapi.ConfigableConst;
import com.mapabc.mapapi.MultiTouchGestureDetector;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MapView extends ViewGroup {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public MapActivity f249a;
    /* access modifiers changed from: private */
    public Mediator b;
    private f c;
    /* access modifiers changed from: private */
    public MapController d;
    /* access modifiers changed from: private */
    public a e;
    private boolean f;
    e mRouteCtrl;

    public enum ReticleDrawMode {
        DRAW_RETICLE_NEVER,
        DRAW_RETICLE_OVER,
        DRAW_RETICLE_UNDER
    }

    interface d {
        void a(int i);
    }

    public MapView(Context context, String str) {
        super(context);
        this.f = false;
        initEnviornment(context, str, null);
    }

    public MapView(Context context, String str, String str2) {
        super(context);
        this.f = false;
        initEnviornment(context, str, str2);
    }

    public MapView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public MapView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        String str;
        this.f = false;
        int attributeCount = attributeSet.getAttributeCount();
        String str2 = null;
        String str3 = "";
        for (int i2 = 0; i2 < attributeCount; i2++) {
            String lowerCase = attributeSet.getAttributeName(i2).toLowerCase();
            if (lowerCase.equals("amapapikey")) {
                str3 = attributeSet.getAttributeValue(i2);
            } else if (lowerCase.equals("useragent")) {
                str2 = attributeSet.getAttributeValue(i2);
            } else if (lowerCase.equals("clickable")) {
                this.f = attributeSet.getAttributeValue(i2).equals("true");
            }
        }
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, new int[]{16843281});
        if (str3.length() < 15) {
            str = obtainStyledAttributes.getString(0);
        } else {
            str = str3;
        }
        if (context instanceof MapActivity) {
            ((MapActivity) context).a(this, context, str, str2);
            return;
        }
        throw new IllegalArgumentException("MapViews can only be created inside instances of MapActivity.");
    }

    /* access modifiers changed from: package-private */
    public void initEnviornment(Context context, String str, String str2) {
        try {
            this.f249a = (MapActivity) context;
            GlobalStore.getInstance().initDeviceInfo(context);
            this.c = new f(this.f249a);
            addView(this.c, 0, new ViewGroup.LayoutParams(-1, -1));
            setBackgroundColor(Color.rgb(222, 215, 214));
            this.b = new Mediator(this.f249a, this, str, str2);
            this.f249a.a(this.b);
            this.e = new a();
            this.mRouteCtrl = new e(context);
            new c(context);
            this.d = new MapController(this.b);
            setEnabled(true);
        } catch (Exception e2) {
            throw new IllegalArgumentException("can only be created inside instances of MapActivity");
        }
    }

    /* access modifiers changed from: package-private */
    public a getZoomMgr() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public Mediator getMediator() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public f getCanvas() {
        return this.c;
    }

    public void setReticleDrawMode(ReticleDrawMode reticleDrawMode) {
        this.b.d.a(reticleDrawMode);
    }

    public GeoPoint getMapCenter() {
        return this.b.b.b();
    }

    public MapController getController() {
        return this.d;
    }

    public final List<Overlay> getOverlays() {
        return this.b.d.d();
    }

    public int getLatitudeSpan() {
        return this.b.f258a.a(this.b.b.a());
    }

    public int getLongitudeSpan() {
        return this.b.f258a.b(this.b.b.a());
    }

    public int getZoomLevel() {
        return this.b.b.a();
    }

    public int getMaxZoomLevel() {
        return 18;
    }

    /* access modifiers changed from: package-private */
    public int getMinZoomLevel() {
        return 4;
    }

    public Projection getProjection() {
        return this.b.f258a;
    }

    public String getVersion() {
        return ConfigableConst.e;
    }

    public boolean canCoverCenter() {
        return this.b.d.a(getMapCenter());
    }

    public void setBuiltInZoomControls(boolean z) {
        this.e.b(z);
    }

    public void displayZoomControls(boolean z) {
        this.e.c(z);
    }

    public void onFocusChanged(boolean z, int i, Rect rect) {
        super.onFocusChanged(z, i, rect);
    }

    /* access modifiers changed from: protected */
    public LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(-2, -2, 0, 0, 51);
    }

    public void preLoad() {
    }

    public void setSatellite(boolean z) {
    }

    public boolean isSatellite() {
        return false;
    }

    public void setTraffic(boolean z) {
        ((aq) this.b.e.a(0)).b(z);
    }

    public boolean isTraffic() {
        return ((aq) this.b.e.a(0)).e();
    }

    public void setStreetView(boolean z) {
    }

    public boolean isStreetView() {
        return false;
    }

    public void closeMapview() {
        if (C0013ag.f309a != null) {
            C0013ag.f309a.b();
        }
        Mediator mediator = this.b;
        mediator.d.b();
        mediator.d.c();
        mediator.f258a = null;
        mediator.b = null;
        mediator.c = null;
        mediator.d = null;
        mediator.e = null;
        this.d = null;
        this.c = null;
        this.b = null;
        this.mRouteCtrl = null;
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        if (-1 == indexOfChild(this.c)) {
            addView(this.c, 0, new ViewGroup.LayoutParams(-1, -1));
        }
        this.c.onDraw(canvas);
        super.dispatchDraw(canvas);
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new LayoutParams(layoutParams);
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(this.f249a, attributeSet);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        super.onRestoreInstanceState(parcelable);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        return super.onSaveInstanceState();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.f249a.a(this.b);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.f249a.a(null);
    }

    public void computeScroll() {
        if (this.c.e.computeScrollOffset()) {
            int currX = this.c.e.getCurrX() - this.c.f;
            int currY = this.c.e.getCurrY() - this.c.g;
            int unused = this.c.f = this.c.e.getCurrX();
            int unused2 = this.c.g = this.c.e.getCurrY();
            GeoPoint fromPixels = this.b.f258a.fromPixels(currX + (GlobalStore.getsViewWidth() / 2), currY + (GlobalStore.getsViewHeight() / 2));
            if (this.c.e.isFinished()) {
                this.b.b.a(false);
            } else {
                this.b.b.b(fromPixels);
            }
        } else {
            super.computeScroll();
        }
    }

    public void setClickable(boolean z) {
        this.f = z;
        super.setClickable(z);
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (this.b == null) {
            return true;
        }
        if (!this.f) {
            return false;
        }
        return this.b.d.a(i, keyEvent) || super.onKeyDown(i, keyEvent);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (this.b == null) {
            return true;
        }
        if (!this.f) {
            return false;
        }
        return this.b.d.b(i, keyEvent) || super.onKeyUp(i, keyEvent);
    }

    public boolean onTrackballEvent(MotionEvent motionEvent) {
        if (this.b == null) {
            return true;
        }
        if (!this.f) {
            return false;
        }
        if (this.b.d.a(motionEvent)) {
            return true;
        }
        return this.c.a(motionEvent);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.b == null) {
            return true;
        }
        if (!this.f) {
            return false;
        }
        if (this.b.d.b(motionEvent)) {
            return true;
        }
        return this.c.b(motionEvent);
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this.e.a(i, i2);
        this.mRouteCtrl.a(i, i2);
        this.b.b.a(i, i2);
    }

    class e {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public ImageView[] f256a = new ImageView[4];
        private Drawable[] b = new Drawable[8];
        /* access modifiers changed from: private */
        public d c = null;
        private boolean d = false;
        private int e = 130;
        private int f = 85;
        private int g = 50;
        private int h = 35;
        private int i = 30;
        private int j = 25;
        private int k = 2;
        private View.OnClickListener l = new B(this);

        public e(Context context) {
            new DisplayMetrics();
            DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
            long j2 = (long) (displayMetrics.heightPixels * displayMetrics.widthPixels);
            if (j2 > 153600) {
                this.k = 3;
                b(this.e, this.f);
            } else if (j2 < 153600) {
                this.k = 1;
                b(this.i, this.j);
            } else {
                this.k = 2;
                b(this.g, this.h);
            }
        }

        private void b(int i2, int i3) {
            String[] strArr = {"overview.png", "detail.png", "prev.png", "next.png", "overview_disable.png", "detail_disable.png", "prev_disable.png", "next_disable.png"};
            for (int i4 = 0; i4 < 8; i4++) {
                this.b[i4] = PoiOverlay.a(ConfigableConst.a(MapView.this.f249a, strArr[i4]), i2, i3);
            }
            for (int i5 = 0; i5 < 4; i5++) {
                this.f256a[i5] = new ImageView(MapView.this.f249a);
                this.f256a[i5].setImageDrawable(this.b[i5]);
                MapView.this.addView(this.f256a[i5], MapView.this.generateDefaultLayoutParams());
                this.f256a[i5].setVisibility(4);
                this.f256a[i5].setOnClickListener(this.l);
            }
        }

        public final void a(int i2, int i3) {
            int i4;
            int i5 = i3 - 10;
            switch (this.k) {
                case 1:
                    i4 = 0;
                    break;
                case 2:
                    i4 = 0;
                    break;
                case 3:
                    i4 = 20;
                    break;
                default:
                    i4 = 0;
                    break;
            }
            LayoutParams layoutParams = new LayoutParams(-2, -2, (i2 / 3) - i4, i5, 85);
            LayoutParams layoutParams2 = new LayoutParams(-2, -2, (i2 / 3) - i4, i5, 83);
            LayoutParams layoutParams3 = new LayoutParams(-2, -2, ((i2 << 1) / 3) + i4, i5, 85);
            LayoutParams layoutParams4 = new LayoutParams(-2, -2, ((i2 << 1) / 3) + i4, i5, 83);
            MapView.this.updateViewLayout(this.f256a[0], layoutParams);
            MapView.this.updateViewLayout(this.f256a[1], layoutParams2);
            MapView.this.updateViewLayout(this.f256a[2], layoutParams3);
            MapView.this.updateViewLayout(this.f256a[3], layoutParams4);
        }

        public final void a(boolean z, d dVar) {
            boolean z2;
            int i2;
            this.c = dVar;
            for (int i3 = 0; i3 < 4; i3++) {
                ImageView imageView = this.f256a[i3];
                if (z) {
                    i2 = 0;
                } else {
                    i2 = 4;
                }
                imageView.setVisibility(i2);
            }
            a access$400 = MapView.this.e;
            if (!z) {
                z2 = true;
            } else {
                z2 = false;
            }
            access$400.a(z2);
            this.d = z;
        }

        public final void a(boolean z) {
            a(2, z);
        }

        public final void b(boolean z) {
            a(3, z);
        }

        public final void c(boolean z) {
            a(1, z);
        }

        public final void d(boolean z) {
            a(0, z);
        }

        public final boolean a() {
            return this.d;
        }

        private void a(int i2, boolean z) {
            int i3 = z ? i2 : i2 + 4;
            this.f256a[i2].setClickable(z);
            this.f256a[i2].setImageDrawable(this.b[i3]);
        }
    }

    class c implements Animation.AnimationListener, C0033n {
        private LinearLayout b;
        private TextView c;
        private boolean d = false;
        private Drawable e;
        /* access modifiers changed from: private */
        public H f;
        private Animation g = new AlphaAnimation(1.0f, 0.3f);
        /* access modifiers changed from: private */
        public b h;

        public c(Context context) {
            this.g.setDuration(4000);
            this.g.setRepeatCount(-1);
            this.g.setAnimationListener(this);
            this.h = new b(context);
            this.h.getSettings().setJavaScriptEnabled(true);
            this.h.setWebViewClient(new a());
            AnonymousClass1 r0 = new View.OnClickListener() {
                public final void onClick(View view) {
                    if (c.this.h != null && c.this.f.c != null) {
                        c.this.h.loadUrl(c.this.f.c);
                        ((C0009ac) MapView.this.b.e.a(3)).a(new O(c.this.f.f238a));
                        c.this.c();
                    }
                }
            };
            this.b = new LinearLayout(context);
            this.e = ConfigableConst.a(context, "ad_panel_bg.png");
            this.b.setGravity(17);
            this.c = new TextView(context);
            this.c.setTextColor(-65536);
            this.c.setGravity(17);
            this.c.setBackgroundDrawable(this.e);
            this.c.setPadding(15, 2, 15, 2);
            this.c.setClickable(true);
            this.c.setOnClickListener(r0);
            this.b.addView(this.c, new LinearLayout.LayoutParams(-2, -2));
            if (MapView.this.b.e.a() != null) {
                b();
            }
            MapView.this.b.e.a(this);
        }

        class b extends WebView {
            public b(Context context) {
                super(context);
            }
        }

        class a extends WebViewClient {
            a() {
            }

            public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
                webView.loadUrl(str);
                return true;
            }
        }

        private void d() {
            this.f = MapView.this.b.e.a();
            this.c.setText(this.f.b);
            this.c.setLinksClickable(true);
        }

        public final void a() {
            if (this.d) {
                MapView.this.removeView(this.b);
                this.c.clearAnimation();
                this.d = false;
            }
        }

        public final void b() {
            if (!this.d) {
                MapView.this.addView(this.b, new LayoutParams(-1, 40, 0, 0, 51));
                d();
                this.c.startAnimation(this.g);
                this.d = true;
            }
        }

        public final void c() {
            this.b.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.f.c)));
        }

        public final void onAnimationEnd(Animation animation) {
        }

        public final void onAnimationRepeat(Animation animation) {
            d();
        }

        public final void onAnimationStart(Animation animation) {
        }
    }

    class a {
        private b b = new b(ConfigableConst.b);
        private b c = new b(ConfigableConst.f224a);
        private K d;
        private boolean e = true;
        /* access modifiers changed from: private */
        public Handler f = new Handler();
        /* access modifiers changed from: private */
        public long g = 0;
        /* access modifiers changed from: private */
        public Runnable h = new aB(this);

        public final boolean a(View view) {
            return view == this.b || view == this.c;
        }

        public final void a(boolean z) {
            if (!this.e) {
                return;
            }
            if (MapView.this.mRouteCtrl == null || !MapView.this.mRouteCtrl.a()) {
                if (z) {
                    this.g = W.b();
                    this.f.postDelayed(this.h, (long) ConfigableConst.c);
                }
                b();
                Mediator unused = MapView.this.b;
                int i = GlobalStore.getsViewWidth();
                Mediator unused2 = MapView.this.b;
                a(i, GlobalStore.getsViewHeight());
                this.c.a(z);
                this.b.a(z);
            }
        }

        public final void a() {
            a(true);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.mapabc.mapapi.Mediator.d.a(com.mapabc.mapapi.f, boolean):void
         arg types: [com.mapabc.mapapi.K, int]
         candidates:
          com.mapabc.mapapi.Mediator.d.a(int, android.view.KeyEvent):boolean
          com.mapabc.mapapi.Mediator.d.a(com.mapabc.mapapi.f, boolean):void */
        public a() {
            MapView.this.addView(this.c, MapView.this.generateDefaultLayoutParams());
            MapView.this.addView(this.b, MapView.this.generateDefaultLayoutParams());
            new Rect(0, 0, 0, 0);
            this.d = new K();
            MapView.this.b.d.a((C0025f) this.d, true);
            b();
            b(false);
        }

        public final void b(boolean z) {
            this.e = true;
            a(z);
            this.e = z;
            MapView.this.b.d.a(this.d, z);
        }

        public final void c(boolean z) {
            this.c.setFocusable(z);
            this.b.setFocusable(z);
        }

        public final void b() {
            this.c.setImageBitmap(ConfigableConst.d[ConfigableConst.ENUM_ID.ezoomin.ordinal()]);
            this.b.setImageBitmap(ConfigableConst.d[ConfigableConst.ENUM_ID.ezoomout.ordinal()]);
            int a2 = MapView.this.b.b.a();
            Mediator unused = MapView.this.b;
            if (a2 == 4) {
                this.b.setImageBitmap(ConfigableConst.d[ConfigableConst.ENUM_ID.ezoomoutdisable.ordinal()]);
            }
            int a3 = MapView.this.b.b.a();
            Mediator unused2 = MapView.this.b;
            if (a3 == 18) {
                this.c.setImageBitmap(ConfigableConst.d[ConfigableConst.ENUM_ID.ezoomindisable.ordinal()]);
            }
        }

        public final void a(int i, int i2) {
            LayoutParams layoutParams = new LayoutParams(-2, -2, i / 2, i2, 83);
            LayoutParams layoutParams2 = new LayoutParams(-2, -2, i / 2, i2, 85);
            if (-1 == MapView.this.indexOfChild(this.c)) {
                MapView.this.addView(this.c, layoutParams);
            } else {
                MapView.this.updateViewLayout(this.c, layoutParams);
            }
            if (-1 == MapView.this.indexOfChild(this.b)) {
                MapView.this.addView(this.b, layoutParams2);
            } else {
                MapView.this.updateViewLayout(this.b, layoutParams2);
            }
        }
    }

    class f extends ImageView implements GestureDetector.OnDoubleTapListener, GestureDetector.OnGestureListener, MultiTouchGestureDetector.OnGestureListener {

        /* renamed from: a  reason: collision with root package name */
        private Point f257a;
        private GestureDetector b;
        private MultiTouchGestureDetector c;
        private ArrayList<GestureDetector.OnGestureListener> d = new ArrayList<>();
        /* access modifiers changed from: private */
        public Scroller e;
        /* access modifiers changed from: private */
        public int f;
        /* access modifiers changed from: private */
        public int g;
        private Matrix h;
        private float i;
        private boolean j;
        private float k;
        private float l;
        private float m;
        private float n;
        private int o;
        private int p;
        private int q;
        private int r;
        /* access modifiers changed from: private */
        public boolean s;
        private Runnable t;

        public f(Context context) {
            super(context);
            new ArrayList();
            this.f = 0;
            this.g = 0;
            this.h = new Matrix();
            this.i = 1.0f;
            this.j = false;
            this.k = 0.5f;
            this.l = 2.0f;
            this.q = 0;
            this.r = 0;
            this.s = false;
            this.t = new C0029j(this);
            this.f257a = null;
            this.b = new GestureDetector(this);
            this.c = MultiTouchGestureDetector.newInstance(context, this);
            this.e = new Scroller(context);
            new DisplayMetrics();
            DisplayMetrics displayMetrics = context.getApplicationContext().getResources().getDisplayMetrics();
            this.o = displayMetrics.widthPixels;
            this.p = displayMetrics.heightPixels;
            this.f = displayMetrics.widthPixels / 2;
            this.g = displayMetrics.heightPixels / 2;
        }

        /* access modifiers changed from: protected */
        public final void onDraw(Canvas canvas) {
            MapView.this.b.d.a(canvas, this.h, this.m, this.n);
        }

        public final float a() {
            return this.i;
        }

        /* access modifiers changed from: package-private */
        public final void b() {
            this.m = 0.0f;
            this.n = 0.0f;
        }

        public final boolean a(MotionEvent motionEvent) {
            MapView.this.e.a();
            int action = motionEvent.getAction();
            int x = (int) motionEvent.getX();
            int y = (int) motionEvent.getY();
            this.f257a = null;
            switch (action) {
                case 0:
                    this.f257a = new Point(x, y);
                    return false;
                case 1:
                default:
                    return false;
                case 2:
                    MapView.this.d.scrollBy(x, y);
                    return false;
            }
        }

        /* access modifiers changed from: private */
        public void c() {
            int i2 = this.f257a.x - this.q;
            int i3 = this.f257a.y - this.r;
            this.f257a.x = this.q;
            this.f257a.y = this.r;
            MapView.this.d.scrollBy(i2, i3);
        }

        public final boolean b(MotionEvent motionEvent) {
            MapView.this.e.a();
            boolean onTouchEvent = this.c.onTouchEvent(motionEvent);
            if (!onTouchEvent) {
                return this.b.onTouchEvent(motionEvent);
            }
            return onTouchEvent;
        }

        public final boolean onDown(MotionEvent motionEvent) {
            this.f257a = new Point((int) motionEvent.getX(), (int) motionEvent.getY());
            return true;
        }

        public final boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
            this.e.fling(this.f, this.g, (((int) (-f2)) * 3) / 4, (((int) (-f3)) * 3) / 4, -this.o, this.o, -this.p, this.p);
            return true;
        }

        public final void onLongPress(MotionEvent motionEvent) {
        }

        public final boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
            int x = (int) motionEvent2.getX();
            int y = (int) motionEvent2.getY();
            if (this.f257a != null) {
                this.q = x;
                this.r = y;
                if (!this.s) {
                    this.s = true;
                    c();
                    postDelayed(this.t, 20);
                } else if (((this.f257a.x - this.q) * (this.f257a.x - this.q)) + ((this.f257a.y - this.r) * (this.f257a.y - this.r)) > 100) {
                    c();
                }
            }
            return true;
        }

        public final void onShowPress(MotionEvent motionEvent) {
        }

        public final boolean onSingleTapUp(MotionEvent motionEvent) {
            Iterator<GestureDetector.OnGestureListener> it = this.d.iterator();
            while (it.hasNext()) {
                it.next().onSingleTapUp(motionEvent);
            }
            return false;
        }

        public final void a(GestureDetector.OnGestureListener onGestureListener) {
            this.d.add(onGestureListener);
        }

        public final void b(GestureDetector.OnGestureListener onGestureListener) {
            this.d.remove(onGestureListener);
        }

        public final boolean onDrag(float f2, float f3) {
            if (this.j) {
                this.m += f2;
                this.n += f3;
            }
            return this.j;
        }

        public final boolean onScale(float f2) {
            this.i = f2;
            return false;
        }

        public final boolean onDrag(Matrix matrix) {
            return false;
        }

        public final boolean onScale(Matrix matrix) {
            this.h.set(matrix);
            postInvalidate();
            return true;
        }

        public final boolean endScale(float f2, PointF pointF) {
            boolean z;
            MapView.this.b.d.b = false;
            Mediator unused = MapView.this.b;
            int i2 = GlobalStore.getsViewWidth() / 2;
            Mediator unused2 = MapView.this.b;
            int i3 = GlobalStore.getsViewHeight() / 2;
            if (f2 >= this.l) {
                z = true;
            } else {
                if (f2 <= this.k) {
                    z = false;
                }
                this.j = false;
                return true;
            }
            int a2 = z ? MapView.this.b.b.a() + 1 : MapView.this.b.b.a() - 1;
            Mediator unused3 = MapView.this.b;
            if (a2 < 4) {
                Mediator unused4 = MapView.this.b;
                a2 = 4;
            }
            Mediator unused5 = MapView.this.b;
            if (a2 > 18) {
                Mediator unused6 = MapView.this.b;
                a2 = 18;
            }
            if (a2 != MapView.this.b.b.a()) {
                GeoPoint fromPixels = MapView.this.b.f258a.fromPixels(i2, i3);
                MapView.this.b.b.a(a2);
                MapView.this.b.b.a(fromPixels);
            }
            this.j = false;
            return true;
        }

        public final boolean startScale(PointF pointF) {
            MapView.this.b.d.g();
            MapView.this.b.d.b = true;
            this.j = true;
            return true;
        }

        public final boolean onDoubleTap(MotionEvent motionEvent) {
            MapView.this.d.zoomInFixing((int) motionEvent.getX(), (int) motionEvent.getY());
            MapView.this.b.b.a(MapView.this.b.f258a.fromPixels((int) motionEvent.getX(), (int) motionEvent.getY()));
            return true;
        }

        public final boolean onDoubleTapEvent(MotionEvent motionEvent) {
            return false;
        }

        public final boolean onSingleTapConfirmed(MotionEvent motionEvent) {
            return false;
        }
    }

    class b extends ImageView {
        /* access modifiers changed from: private */
        public int b;

        public b(int i) {
            super(MapView.this.f249a);
            this.b = i;
            Bitmap bitmap = this.b == ConfigableConst.f224a ? ConfigableConst.d[ConfigableConst.ENUM_ID.ezoomin.ordinal()] : ConfigableConst.d[ConfigableConst.ENUM_ID.ezoomout.ordinal()];
            if (bitmap != null) {
                bitmap.getWidth();
                bitmap.getHeight();
                setImageBitmap(bitmap);
            }
            setClickable(true);
            setOnClickListener(new View.OnClickListener() {
                public final void onClick(View view) {
                    if (ConfigableConst.f224a == b.this.b) {
                        MapView.this.d.zoomIn();
                    }
                    if (ConfigableConst.b == b.this.b) {
                        MapView.this.d.zoomOut();
                    }
                }
            });
        }

        public final void a(boolean z) {
            setVisibility(z ? 0 : 4);
        }
    }

    public static class LayoutParams extends ViewGroup.LayoutParams {
        public static final int BOTTOM = 80;
        public static final int BOTTOM_CENTER = 81;
        public static final int CENTER = 17;
        public static final int CENTER_HORIZONTAL = 1;
        public static final int CENTER_VERTICAL = 16;
        public static final int LEFT = 3;
        public static final int MODE_MAP = 0;
        public static final int MODE_VIEW = 1;
        public static final int RIGHT = 5;
        public static final int TOP = 48;
        public static final int TOP_LEFT = 51;
        public int alignment;
        public int mode;
        public GeoPoint point;
        public int x;
        public int y;

        public LayoutParams(int i, int i2, GeoPoint geoPoint, int i3) {
            this(i, i2, geoPoint, 0, 0, i3);
        }

        public LayoutParams(int i, int i2, GeoPoint geoPoint, int i3, int i4, int i5) {
            super(i, i2);
            this.mode = 1;
            this.point = null;
            this.x = 0;
            this.y = 0;
            this.alignment = 51;
            this.mode = 0;
            this.point = geoPoint;
            this.x = i3;
            this.y = i4;
            this.alignment = i5;
        }

        public LayoutParams(int i, int i2, int i3, int i4, int i5) {
            super(i, i2);
            this.mode = 1;
            this.point = null;
            this.x = 0;
            this.y = 0;
            this.alignment = 51;
            this.x = i3;
            this.y = i4;
            this.alignment = i5;
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.mode = 1;
            this.point = null;
            this.x = 0;
            this.y = 0;
            this.alignment = 51;
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
            this.mode = 1;
            this.point = null;
            this.x = 0;
            this.y = 0;
            this.alignment = 51;
        }
    }
}
