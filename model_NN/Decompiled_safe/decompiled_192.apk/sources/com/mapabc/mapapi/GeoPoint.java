package com.mapabc.mapapi;

public class GeoPoint {
    static final b[] BitmapOrigo = {new b(128, 128), new b(256, 256), new b(512, 512), new b(1024, 1024), new b(2048, 2048), new b(4096, 4096), new b(8192, 8192), new b(16384, 16384), new b(32768, 32768), new b(65536, 65536), new b(131072, 131072), new b(262144, 262144), new b(524288, 524288), new b(1048576, 1048576), new b(2097152, 2097152), new b(4194304, 4194304), new b(8388608, 8388608), new b(16777216, 16777216)};
    static final int EquatorLong = 40076000;
    static final int MaxDyeZoom = 17;
    static final double[] PixelsPerLatRadian = {40.74366543152521d, 81.48733086305042d, 162.97466172610083d, 325.94932345220167d, 651.8986469044033d, 1303.7972938088067d, 2607.5945876176133d, 5215.189175235227d, 10430.378350470453d, 20860.756700940907d, 41721.51340188181d, 83443.02680376363d, 166886.05360752725d, 333772.1072150545d, 667544.214430109d, 1335088.428860218d, 2670176.857720436d, 5340353.715440872d};
    static final double[] PixelsPerLonDegree = {0.7111111111111111d, 1.4222222222222223d, 2.8444444444444446d, 5.688888888888889d, 11.377777777777778d, 22.755555555555556d, 45.51111111111111d, 91.02222222222223d, 182.04444444444445d, 364.0888888888889d, 728.1777777777778d, 1456.3555555555556d, 2912.711111111111d, 5825.422222222222d, 11650.844444444445d, 23301.68888888889d, 46603.37777777778d, 93206.75555555556d};

    /* renamed from: a  reason: collision with root package name */
    private int f231a;
    private int b;
    private int c;
    private int d;

    public GeoPoint(int i, int i2) {
        a(i, i2, false);
    }

    GeoPoint(int i, int i2, boolean z) {
        a(i, i2, z);
    }

    GeoPoint(int i, int i2, int i3, int i4) {
        this.c = i;
        this.d = i2;
        this.f231a = i3;
        this.b = i4;
    }

    private void a(int i, int i2, boolean z) {
        if (!z) {
            this.f231a = i;
            this.b = i2;
            a();
            return;
        }
        this.c = i;
        this.d = i2;
        this.b = W.a(((double) (this.d - BitmapOrigo[17].f233a)) / PixelsPerLonDegree[17]);
        this.f231a = W.a(((Math.atan(Math.exp(((double) (this.c - BitmapOrigo[17].b)) / (-PixelsPerLatRadian[17]))) * 2.0d) - 1.5707963267948966d) * 57.29577951308232d);
        a();
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        GeoPoint geoPoint = (GeoPoint) obj;
        return this.c == geoPoint.c && this.d == geoPoint.d;
    }

    public int hashCode() {
        return (this.c * 7) + (this.d * 11);
    }

    public String toString() {
        return "" + this.f231a + "," + this.b;
    }

    public int getLongitudeE6() {
        return this.b;
    }

    public int getLatitudeE6() {
        return this.f231a;
    }

    /* access modifiers changed from: package-private */
    public int getLatitudeAutoNavi() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public int getLongitudeAutoNavi() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public int getLatInZoom(int i) {
        return this.c >> (18 - i);
    }

    /* access modifiers changed from: package-private */
    public int getLonInZoom(int i) {
        return this.d >> (18 - i);
    }

    /* access modifiers changed from: package-private */
    public GeoPoint Copy() {
        return new GeoPoint(this.c, this.d, this.f231a, this.b);
    }

    /* access modifiers changed from: package-private */
    public String getLonLatAddr() {
        return "" + W.c(this.f231a) + "," + W.c(this.b);
    }

    private void a() {
        double c2 = W.c(this.b);
        double c3 = W.c(this.f231a);
        this.d = (int) Math.round((c2 * PixelsPerLonDegree[17]) + ((double) BitmapOrigo[17].f233a));
        double sin = Math.sin(0.017453292519943295d * c3);
        if (sin < -0.999d) {
            sin = -0.999d;
        }
        if (sin > 0.999d) {
            sin = 0.999d;
        }
        this.c = (int) Math.round((Math.log((sin + 1.0d) / (1.0d - sin)) * 0.5d * (-PixelsPerLatRadian[17])) + ((double) BitmapOrigo[17].b));
    }

    static class b {

        /* renamed from: a  reason: collision with root package name */
        int f233a;
        int b;

        public b(int i, int i2) {
            this.f233a = i;
            this.b = i2;
        }
    }

    static class a {

        /* renamed from: a  reason: collision with root package name */
        double f232a;
        double b;

        public a(double d, double d2) {
            this.f232a = d;
            this.b = d2;
        }
    }
}
