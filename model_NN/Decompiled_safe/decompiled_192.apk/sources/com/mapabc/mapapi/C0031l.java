package com.mapabc.mapapi;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

/* renamed from: com.mapabc.mapapi.l  reason: case insensitive filesystem */
final class C0031l implements C0015ai {

    /* renamed from: a  reason: collision with root package name */
    private C0041v f321a = new C0041v(Bitmap.Config.ARGB_4444);
    private Drawable b = null;

    C0031l() {
    }

    public final Drawable a(Drawable drawable) {
        this.b = drawable;
        this.f321a.a(this.b.getIntrinsicWidth(), this.b.getIntrinsicHeight());
        this.f321a.a(this);
        this.b = null;
        return new BitmapDrawable(this.f321a.a());
    }

    public final void a(Canvas canvas) {
        this.b.setColorFilter(2130706432, PorterDuff.Mode.SRC_IN);
        canvas.skew(-0.9f, 0.0f);
        canvas.scale(1.0f, 0.5f);
        this.b.draw(canvas);
        this.b.clearColorFilter();
    }

    public static void a(Drawable drawable, Drawable drawable2) {
        Rect bounds = drawable2.getBounds();
        int height = (int) (((float) bounds.height()) * 0.5f);
        int width = (int) (((double) (((float) bounds.width()) * 0.9f)) * 0.5d);
        drawable.setBounds(bounds.left + width, bounds.top + height, width + bounds.right, bounds.bottom + height);
    }
}
