package com.mapabc.mapapi;

public class PoiTypeDef {
    public static final String Accommodation = "10|";
    public static final String All = "00|";
    public static final String AutoService = "01|02|03|04|18|";
    public static final String Education = "14|";
    public static final String Enterprise = "17|";
    public static final String Financial = "16|";
    public static final String FoodBeverages = "05|";
    public static final String LifeService = "07|20|";
    public static final String MedicalService = "09|";
    public static final String Organization = "13|";
    public static final String PlaceAndAddress = "19|";
    public static final String PublicTransportation = "15|";
    public static final String RealEstate = "12|";
    public static final String Shopping = "06|";
    public static final String SportEntertainment = "08|";
    public static final String TouristAttraction = "11|";

    public static boolean isType(String str, String str2) {
        String str3;
        if (W.a(str)) {
            return false;
        }
        if (W.a(str2)) {
            return false;
        }
        String[] split = str2.split("\\|");
        if (str.length() == 4) {
            str3 = str.substring(0, 2);
        } else {
            str3 = str;
        }
        for (String str4 : split) {
            if (!W.a(str4) && (str4.equals("00") || str4.equals(str3) || str4.equals(str))) {
                return true;
            }
        }
        return false;
    }
}
