package com.mapabc.mapapi;

import java.util.ArrayList;

class aH implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ aG f300a;

    aH(aG aGVar) {
        this.f300a = aGVar;
    }

    public final void run() {
        while (this.f300a.c) {
            if (this.f300a.f319a == null) {
                this.f300a.c = false;
            } else {
                ArrayList<T> a2 = this.f300a.e.a(this.f300a.h());
                if (this.f300a.c && a2.size() != 0) {
                    this.f300a.a((ArrayList) a2);
                    if (a2.size() != 0) {
                        Object a3 = this.f300a.a(a2, this.f300a.f319a.e.e());
                        if (this.f300a.c && a3 != null) {
                            this.f300a.d.c(a3);
                            this.f300a.h.post(this.f300a.g);
                            try {
                                if (this.f300a.c) {
                                    Thread.sleep(500);
                                }
                            } catch (Exception e) {
                            }
                        }
                    }
                }
            }
        }
    }
}
