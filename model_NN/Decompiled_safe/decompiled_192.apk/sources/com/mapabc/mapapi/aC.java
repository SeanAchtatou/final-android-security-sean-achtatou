package com.mapabc.mapapi;

import android.util.Log;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.Proxy;

abstract class aC<T, V> {

    /* renamed from: a  reason: collision with root package name */
    private Proxy f297a;
    private int b = 1;
    protected long c = 0;
    protected T d;
    protected String e;
    protected String f = "http://mapi.mapabc.com/mas/r";
    private int g = 20;
    private int h = 0;
    private int i = 0;
    private String j = "<?xml version=\"1.0\" encoding=\"GBK\" ?>\n<og><key>%s</key>\n<md5>%s</md5>\n<servcode>%d</servcode>\n<manufacturer>%s</manufacturer>\n<sdkversion>%d</sdkversion>\n<imei>%s</imei>\n<model>%s</model>\n<curcell>%s</curcell>\n<centers>%s</centers>\n<vvv>4.2</vvv>\n";

    /* access modifiers changed from: protected */
    public abstract V e(InputStream inputStream) throws IOException;

    /* access modifiers changed from: protected */
    public abstract String[] e();

    /* access modifiers changed from: protected */
    public abstract int f();

    public aC(T t, Proxy proxy, String str, String str2, String str3) {
        this.f297a = proxy;
        this.d = t;
        this.b = 1;
        this.g = 5;
        this.h = 2;
        this.e = str3;
        this.j = String.format(this.j, str.toUpperCase(), str2.toUpperCase(), Integer.valueOf(f()), GlobalStore.getInstance().getManufacturer(), Integer.valueOf(GlobalStore.getInstance().getSDKVersion()), GlobalStore.getInstance().getIMEI(), GlobalStore.getInstance().getModel(), GlobalStore.getInstance().getCurTower().toString(), GlobalStore.getInstance().centersToStringAndClear());
    }

    protected static String a(String str, boolean z) {
        return String.format(z ? "<%s>" : "</%s>", str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mapabc.mapapi.aC.a(java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      com.mapabc.mapapi.aC.a(java.io.InputStream, int):java.lang.String
      com.mapabc.mapapi.aC.a(java.lang.String, java.lang.String):java.lang.String
      com.mapabc.mapapi.aC.a(java.lang.String, boolean):java.lang.String */
    protected static String a(String str, String str2) {
        String str3;
        if (str2 == null) {
            str3 = "";
        } else {
            str3 = str2;
        }
        return (a(str, true) + str3) + a(str, false);
    }

    private String b() throws UnsupportedEncodingException {
        String str;
        String[] e2 = e();
        String str2 = this.j;
        if (e2 != null) {
            String str3 = str2;
            for (int i2 = 0; i2 < e2.length; i2++) {
                str3 = (str3 + e2[i2]) + "\n";
            }
            str = str3;
        } else {
            str = str2;
        }
        return str + "</og>";
    }

    public final V g() throws IOException {
        V h2 = h();
        if (this.i == -999) {
            throw new IOException();
        } else if (this.i == 0) {
            return h2;
        } else {
            throw new IllegalArgumentException(String.format("%d", Integer.valueOf(this.i)));
        }
    }

    public final V h() {
        if (this.d == null) {
            return null;
        }
        try {
            return c();
        } catch (Exception e2) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public V a_() {
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:52:0x00ee  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00f3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private V c() throws java.io.IOException, java.lang.InterruptedException {
        /*
            r11 = this;
            r9 = 1
            r8 = 0
            r7 = 0
            r11.i = r8
            r1 = r8
            r2 = r7
            r3 = r7
            r4 = r7
        L_0x0009:
            int r0 = r11.b
            if (r1 >= r0) goto L_0x0100
            long r5 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0109, all -> 0x0105 }
            r11.c = r5     // Catch:{ Exception -> 0x0109, all -> 0x0105 }
            java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x0109, all -> 0x0105 }
            java.lang.String r5 = r11.f     // Catch:{ Exception -> 0x0109, all -> 0x0105 }
            r0.<init>(r5)     // Catch:{ Exception -> 0x0109, all -> 0x0105 }
            java.net.Proxy r5 = r11.f297a     // Catch:{ Exception -> 0x0109, all -> 0x0105 }
            if (r5 == 0) goto L_0x00a8
            java.net.Proxy r5 = r11.f297a     // Catch:{ Exception -> 0x0109, all -> 0x0105 }
            java.net.URLConnection r0 = r0.openConnection(r5)     // Catch:{ Exception -> 0x0109, all -> 0x0105 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x0109, all -> 0x0105 }
        L_0x0026:
            int r5 = r11.g     // Catch:{ Exception -> 0x0109, all -> 0x0105 }
            int r5 = r5 * 1000
            r0.setConnectTimeout(r5)     // Catch:{ Exception -> 0x0109, all -> 0x0105 }
            int r5 = r11.g     // Catch:{ Exception -> 0x0109, all -> 0x0105 }
            int r5 = r5 * 1000
            r0.setReadTimeout(r5)     // Catch:{ Exception -> 0x0109, all -> 0x0105 }
            r5 = 1
            r0.setDoInput(r5)     // Catch:{ Exception -> 0x0109, all -> 0x0105 }
            r5 = 1
            r0.setDoOutput(r5)     // Catch:{ Exception -> 0x0109, all -> 0x0105 }
            java.lang.String r4 = "POST"
            r0.setRequestMethod(r4)     // Catch:{ Exception -> 0x00b8, all -> 0x00e8 }
            java.lang.String r4 = "Content-Type"
            java.lang.String r5 = "application/x-www-form-urlencoded"
            r0.setRequestProperty(r4, r5)     // Catch:{ Exception -> 0x00b8, all -> 0x00e8 }
            java.lang.String r4 = r11.b()     // Catch:{ Exception -> 0x00b8, all -> 0x00e8 }
            java.lang.String r5 = "UTF-8"
            byte[] r4 = r4.getBytes(r5)     // Catch:{ Exception -> 0x00b8, all -> 0x00e8 }
            java.io.OutputStream r5 = r0.getOutputStream()     // Catch:{ Exception -> 0x00b8, all -> 0x00e8 }
            r5.write(r4)     // Catch:{ Exception -> 0x00b8, all -> 0x00e8 }
            java.io.InputStream r3 = r0.getInputStream()     // Catch:{ Exception -> 0x00b8, all -> 0x00e8 }
            int r4 = f(r3)     // Catch:{ Exception -> 0x00b8, all -> 0x00e8 }
            if (r4 != 0) goto L_0x00b0
            r4 = r9
        L_0x0064:
            a(r4)     // Catch:{ Exception -> 0x00b8, all -> 0x00e8 }
            int r4 = r3.read()     // Catch:{ Exception -> 0x00b8, all -> 0x00e8 }
            if (r4 != 0) goto L_0x00b2
            r4 = r9
        L_0x006e:
            a(r4)     // Catch:{ Exception -> 0x00b8, all -> 0x00e8 }
            int r4 = r3.read()     // Catch:{ Exception -> 0x00b8, all -> 0x00e8 }
            int r5 = r11.f()     // Catch:{ Exception -> 0x00b8, all -> 0x00e8 }
            if (r4 != r5) goto L_0x00b4
            r4 = r9
        L_0x007c:
            a(r4)     // Catch:{ Exception -> 0x00b8, all -> 0x00e8 }
            int r4 = g(r3)     // Catch:{ Exception -> 0x00b8, all -> 0x00e8 }
            r11.i = r4     // Catch:{ Exception -> 0x00b8, all -> 0x00e8 }
            int r4 = r11.i     // Catch:{ Exception -> 0x00b8, all -> 0x00e8 }
            if (r4 != 0) goto L_0x00b6
            r4 = r9
        L_0x008a:
            a(r4)     // Catch:{ Exception -> 0x00b8, all -> 0x00e8 }
            f(r3)     // Catch:{ Exception -> 0x00b8, all -> 0x00e8 }
            java.io.InputStream r4 = a(r3)     // Catch:{ Exception -> 0x00b8, all -> 0x00e8 }
            java.lang.Object r2 = r11.e(r4)     // Catch:{ Exception -> 0x00b8, all -> 0x00e8 }
            int r1 = r11.b     // Catch:{ Exception -> 0x00b8, all -> 0x00e8 }
            if (r3 == 0) goto L_0x00a0
            r3.close()
            r3 = r7
        L_0x00a0:
            if (r0 == 0) goto L_0x010e
            r0.disconnect()
            r4 = r7
            goto L_0x0009
        L_0x00a8:
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x0109, all -> 0x0105 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x0109, all -> 0x0105 }
            goto L_0x0026
        L_0x00b0:
            r4 = r8
            goto L_0x0064
        L_0x00b2:
            r4 = r8
            goto L_0x006e
        L_0x00b4:
            r4 = r8
            goto L_0x007c
        L_0x00b6:
            r4 = r8
            goto L_0x008a
        L_0x00b8:
            r4 = move-exception
            r10 = r2
            r2 = r3
            r3 = r0
            r0 = r10
        L_0x00bd:
            int r1 = r1 + 1
            int r4 = r11.b     // Catch:{ all -> 0x0101 }
            if (r1 >= r4) goto L_0x00db
            int r4 = r11.h     // Catch:{ all -> 0x0101 }
            int r4 = r4 * 1000
            long r4 = (long) r4     // Catch:{ all -> 0x0101 }
            java.lang.Thread.sleep(r4)     // Catch:{ all -> 0x0101 }
        L_0x00cb:
            if (r2 == 0) goto L_0x00d1
            r2.close()
            r2 = r7
        L_0x00d1:
            if (r3 == 0) goto L_0x00f7
            r3.disconnect()
            r3 = r2
            r4 = r7
            r2 = r0
            goto L_0x0009
        L_0x00db:
            int r0 = r11.i     // Catch:{ all -> 0x0101 }
            if (r0 != 0) goto L_0x00e3
            r0 = -999(0xfffffffffffffc19, float:NaN)
            r11.i = r0     // Catch:{ all -> 0x0101 }
        L_0x00e3:
            java.lang.Object r0 = r11.a_()     // Catch:{ all -> 0x0101 }
            goto L_0x00cb
        L_0x00e8:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r3
        L_0x00ec:
            if (r1 == 0) goto L_0x00f1
            r1.close()
        L_0x00f1:
            if (r2 == 0) goto L_0x00f6
            r2.disconnect()
        L_0x00f6:
            throw r0
        L_0x00f7:
            r10 = r1
            r1 = r0
            r0 = r10
        L_0x00fa:
            r4 = r3
            r3 = r2
            r2 = r1
            r1 = r0
            goto L_0x0009
        L_0x0100:
            return r2
        L_0x0101:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x00ec
        L_0x0105:
            r0 = move-exception
            r1 = r3
            r2 = r4
            goto L_0x00ec
        L_0x0109:
            r0 = move-exception
            r0 = r2
            r2 = r3
            r3 = r4
            goto L_0x00bd
        L_0x010e:
            r10 = r1
            r1 = r2
            r2 = r3
            r3 = r0
            r0 = r10
            goto L_0x00fa
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mapabc.mapapi.aC.c():java.lang.Object");
    }

    protected static void a(boolean z) throws IOException {
        if (!z) {
            throw new IOException();
        }
    }

    private static InputStream a(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[2048];
        while (true) {
            int read = inputStream.read(bArr);
            if (read >= 0) {
                byteArrayOutputStream.write(bArr, 0, read);
                byteArrayOutputStream.flush();
            } else {
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                byteArrayOutputStream.close();
                inputStream.close();
                return new ByteArrayInputStream(byteArray);
            }
        }
    }

    protected static int f(InputStream inputStream) throws IOException {
        byte[] bArr = new byte[4];
        inputStream.read(bArr, 0, 4);
        return (bArr[0] & 255) + ((bArr[3] & 255) << 24) + ((bArr[2] & 255) << 16) + ((bArr[1] & 255) << 8);
    }

    protected static int g(InputStream inputStream) throws IOException {
        byte[] bArr = new byte[2];
        inputStream.read(bArr, 0, 2);
        return (bArr[0] & 255) + ((bArr[1] & 255) << 8);
    }

    protected static String h(InputStream inputStream) throws IOException {
        return a(inputStream, g(inputStream));
    }

    protected static String i(InputStream inputStream) throws IOException {
        return a(inputStream, f(inputStream));
    }

    protected static String j(InputStream inputStream) throws IOException {
        return a(inputStream, inputStream.read());
    }

    private static String a(InputStream inputStream, int i2) throws IOException {
        byte[] bArr = new byte[i2];
        int i3 = 0;
        while (i3 < i2) {
            try {
                i3 += inputStream.read(bArr, i3, i2 - i3);
            } catch (Exception e2) {
                Log.d("E", "Error" + e2.getMessage());
                e2.printStackTrace();
            }
        }
        return new String(bArr, "utf-8");
    }
}
