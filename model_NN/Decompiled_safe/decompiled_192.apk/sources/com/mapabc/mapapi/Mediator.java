package com.mapabc.mapapi;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Point;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import com.mapabc.mapapi.ConfigableConst;
import com.mapabc.mapapi.MapView;
import com.mapabc.mapapi.Tile;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

final class Mediator {

    /* renamed from: a  reason: collision with root package name */
    public ViewGeoProjector f258a = new ViewGeoProjector();
    public a b;
    public b c = new b();
    public d d = new d();
    public c e;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mapabc.mapapi.Mediator.d.a(com.mapabc.mapapi.f, boolean):void
     arg types: [com.mapabc.mapapi.ap, int]
     candidates:
      com.mapabc.mapapi.Mediator.d.a(int, android.view.KeyEvent):boolean
      com.mapabc.mapapi.Mediator.d.a(com.mapabc.mapapi.f, boolean):void */
    public Mediator(MapActivity mapActivity, MapView mapView, String str, String str2) {
        this.b = new a(mapView);
        this.e = new c(this, mapActivity, str, str2);
        if (Math.random() > 0.1d) {
            this.d.a((C0025f) new ap(ConfigableConst.d[ConfigableConst.ENUM_ID.ewatermark.ordinal()]), true);
        }
    }

    class c {

        /* renamed from: a  reason: collision with root package name */
        private String f262a = "";
        private String b;
        private String c = "android";
        private final MapActivity d;
        /* access modifiers changed from: private */
        public Proxy e;
        /* access modifiers changed from: private */
        public C0028i[] f;
        private SharedPreferences g;
        /* access modifiers changed from: private */
        public C0010ad h;
        /* access modifiers changed from: private */
        public ClsCachFileManager i = null;

        static /* synthetic */ void c(c cVar) {
            SharedPreferences.Editor edit = cVar.g.edit();
            GeoPoint b2 = Mediator.this.b.b();
            edit.putInt("Zoom", Mediator.this.b.a());
            edit.putInt("GeoY", b2.getLatitudeAutoNavi());
            edit.putInt("GeoX", b2.getLongitudeAutoNavi());
            edit.commit();
        }

        public final H a() {
            return ((C0009ac) this.f[3]).e();
        }

        public c(Mediator mediator, MapActivity mapActivity, String str, String str2) {
            this.d = mapActivity;
            this.b = str;
            if (str2 != null) {
                this.c = str2;
            } else {
                new DisplayMetrics();
                DisplayMetrics displayMetrics = mapActivity.getApplicationContext().getResources().getDisplayMetrics();
                long j2 = (long) (displayMetrics.heightPixels * displayMetrics.widthPixels);
                if (j2 > 153600) {
                    this.c = "androidh";
                } else if (j2 < 153600) {
                    this.c = "androidl";
                } else {
                    this.c = "android";
                }
            }
            ConfigableConst.a(this.d);
            this.g = this.d.getPreferences(0);
            int i2 = this.g.getInt("Zoom", 10);
            Mediator.this.b.a(i2);
            GeoPoint geoPoint = new GeoPoint(this.g.getInt("GeoY", 12722176), this.g.getInt("GeoX", 27631616), true);
            Mediator.this.b.a(geoPoint);
            GlobalStore.getInstance().addCenterIfEmpty(geoPoint, i2);
            this.f262a = W.a((Context) this.d);
            this.h = new ay(this.d);
            this.i = new ClsCachFileManager(this.d);
            this.f = new C0028i[]{new aq(mediator, mapActivity), new C0034o(mediator, mapActivity, this.b), new aw(mediator, mapActivity), new C0009ac(mediator, mapActivity), new ao(mediator, mapActivity), new C0014ah(mediator, mapActivity)};
        }

        public final String b() {
            return this.f262a;
        }

        public final String c() {
            return this.b;
        }

        public final String d() {
            return this.c;
        }

        public final Proxy e() {
            return this.e;
        }

        public final C0010ad f() {
            return this.h;
        }

        public final ClsCachFileManager g() {
            return this.i;
        }

        public final C0028i a(int i2) {
            return this.f[i2];
        }

        public final void a(C0033n nVar) {
            ((C0009ac) this.f[3]).a(nVar);
        }
    }

    class b {

        /* renamed from: a  reason: collision with root package name */
        int f261a = 0;
        /* access modifiers changed from: private */
        public Handler c = null;
        /* access modifiers changed from: private */
        public Runnable d = new C0036q(this);

        public b() {
            b();
        }

        public final void a() {
            for (C0028i b2 : Mediator.this.e.f) {
                b2.b();
            }
            this.c.removeCallbacks(this.d);
            this.c = null;
            Mediator.this.e.e = null;
            c.c(Mediator.this.e);
            Mediator.this.e.h.c();
            Mediator.this.e.i.saveCachFile();
        }

        public final void b() {
            for (C0028i c2 : Mediator.this.e.f) {
                c2.c();
            }
            this.c = new Handler();
            this.c.postDelayed(this.d, 3000);
            Mediator.this.e.e = W.b(Mediator.this.e.d);
        }
    }

    public class ViewGeoProjector implements Projection {
        public ViewGeoProjector() {
        }

        public Point toPixels(GeoPoint geoPoint, Point point) {
            Point point2;
            if (point == null) {
                point2 = new Point();
            } else {
                point2 = point;
            }
            int longitudeAutoNavi = geoPoint.getLongitudeAutoNavi() - Mediator.this.b.c.getLongitudeAutoNavi();
            int latitudeAutoNavi = geoPoint.getLatitudeAutoNavi() - Mediator.this.b.c.getLatitudeAutoNavi();
            int b = longitudeAutoNavi >> (18 - Mediator.this.b.f);
            int b2 = latitudeAutoNavi >> (18 - Mediator.this.b.f);
            point2.x = b + (GlobalStore.getsViewWidth() / 2);
            point2.y = (GlobalStore.getsViewHeight() / 2) + b2;
            return point2;
        }

        public GeoPoint fromPixels(int i, int i2) {
            return new GeoPoint(((i2 - (GlobalStore.getsViewHeight() / 2)) << (18 - Mediator.this.b.f)) + Mediator.this.b.c.getLatitudeAutoNavi(), ((i - (GlobalStore.getsViewWidth() / 2)) << (18 - Mediator.this.b.f)) + Mediator.this.b.c.getLongitudeAutoNavi(), true);
        }

        public float metersToEquatorPixels(float f) {
            return (((float) GeoPoint.BitmapOrigo[Mediator.this.b.a() - 1].f233a) * f) / 4.0076E7f;
        }

        /* access modifiers changed from: package-private */
        public final int a(int i) {
            return a(i, false);
        }

        /* access modifiers changed from: package-private */
        public final int b(int i) {
            return a(i, true);
        }

        private int a(int i, boolean z) {
            D autonaviBound = getAutonaviBound(i);
            GeoPoint geoPoint = new GeoPoint(autonaviBound.c, autonaviBound.f226a, true);
            GeoPoint geoPoint2 = new GeoPoint(autonaviBound.d, autonaviBound.b, true);
            return z ? geoPoint2.getLongitudeE6() - geoPoint.getLongitudeE6() : (-geoPoint2.getLatitudeE6()) + geoPoint.getLatitudeE6();
        }

        public D getAutonaviBound(int i) {
            int i2 = (GlobalStore.getsViewWidth() << (18 - i)) >> 1;
            int i3 = (GlobalStore.getsViewHeight() << (18 - i)) >> 1;
            GeoPoint b = Mediator.this.b.b();
            int longitudeAutoNavi = b.getLongitudeAutoNavi();
            int latitudeAutoNavi = b.getLatitudeAutoNavi();
            return new D(longitudeAutoNavi - i2, i2 + longitudeAutoNavi, latitudeAutoNavi - i3, i3 + latitudeAutoNavi);
        }
    }

    class d implements C0015ai {

        /* renamed from: a  reason: collision with root package name */
        boolean f263a = false;
        boolean b = false;
        private boolean c = false;
        private C0008ab<Tile> d = new C0008ab<>();
        private C0008ab<Overlay> e = new C0008ab<>();
        private C0044y<C0025f> f = new C0044y<>();
        private MapView.ReticleDrawMode g = MapView.ReticleDrawMode.DRAW_RETICLE_NEVER;
        private long h;
        /* access modifiers changed from: private */
        public boolean i = false;

        d() {
        }

        public final void a(MapView.ReticleDrawMode reticleDrawMode) {
            this.g = reticleDrawMode;
        }

        public final C0008ab<Tile> a() {
            return this.d;
        }

        public final void b() {
            this.e.clear();
        }

        public final void c() {
            Iterator<Tile> it = this.d.iterator();
            while (it.hasNext()) {
                Tile next = it.next();
                if (!next.d()) {
                    next.b.recycle();
                }
            }
        }

        public final List<Overlay> d() {
            return this.e;
        }

        public final boolean a(GeoPoint geoPoint) {
            Tile.TileCoordinate a2 = Tile.a(geoPoint, Mediator.this.b.f);
            Iterator<Tile> it = this.d.iterator();
            while (it.hasNext()) {
                Tile next = it.next();
                if (next.f284a.equals(a2)) {
                    if (!next.d()) {
                        return true;
                    }
                    return false;
                }
            }
            return false;
        }

        public final void e() {
            if (a(this.h)) {
                f();
            } else {
                this.i = true;
            }
        }

        public final void a(int i2, int i3, int i4, int i5) {
            if (a(this.h)) {
                b(i2, i3, i4, i5);
            } else {
                this.i = true;
            }
        }

        public final void f() {
            b(0, 0, GlobalStore.getsViewWidth(), GlobalStore.getsViewHeight());
        }

        public final void b(int i2, int i3, int i4, int i5) {
            Mediator.this.b.f260a.postInvalidate(i2, i3, i4, i5);
            this.h = System.currentTimeMillis();
            this.i = false;
        }

        private static boolean a(long j2) {
            return System.currentTimeMillis() - j2 > 300;
        }

        public final void g() {
            this.c = true;
        }

        public final void a(C0025f fVar, boolean z) {
            if (z) {
                this.f.a(fVar);
            } else {
                this.f.b(fVar);
            }
        }

        public final void a(Canvas canvas, Matrix matrix, float f2, float f3) {
            if (this.c) {
                canvas.save();
                canvas.translate(f2, f3);
                canvas.concat(matrix);
                b(canvas);
                canvas.restore();
                if (!this.f263a && !this.b) {
                    Iterator<Tile> it = this.d.iterator();
                    while (it.hasNext()) {
                        Tile next = it.next();
                        if (!next.d()) {
                            next.a(canvas, Mediator.this.b.f260a);
                        }
                    }
                    this.c = false;
                    Mediator.this.b.f260a.getCanvas().onScale(new Matrix());
                    Mediator.this.b.f260a.getCanvas().onScale(1.0f);
                    Mediator.this.b.f260a.getCanvas().b();
                }
                c(canvas);
                return;
            }
            Mediator.this.b.d();
            b(canvas);
            c(canvas);
        }

        private void b(Canvas canvas) {
            Iterator<Tile> it = this.d.iterator();
            while (it.hasNext()) {
                it.next().a(canvas, Mediator.this.b.f260a);
            }
        }

        private void c(Canvas canvas) {
            MyLocationOverlay myLocationOverlay = null;
            if (this.g == MapView.ReticleDrawMode.DRAW_RETICLE_UNDER) {
                d(canvas);
            }
            long b2 = W.b();
            Iterator<Overlay> it = this.e.iterator();
            while (it.hasNext()) {
                it.next().draw(canvas, Mediator.this.b.f260a, true, b2);
            }
            Iterator<Overlay> it2 = this.e.iterator();
            while (it2.hasNext()) {
                Overlay next = it2.next();
                if (next instanceof MyLocationOverlay) {
                    myLocationOverlay = (MyLocationOverlay) next;
                } else {
                    next.draw(canvas, Mediator.this.b.f260a, false, b2);
                }
            }
            if (this.g == MapView.ReticleDrawMode.DRAW_RETICLE_OVER) {
                d(canvas);
            }
            Iterator<C0025f> it3 = this.f.iterator();
            while (it3.hasNext()) {
                it3.next().draw(canvas, Mediator.this.b.f260a, false, b2);
            }
            if (myLocationOverlay != null) {
                myLocationOverlay.draw(canvas, Mediator.this.b.f260a, false, b2);
            }
        }

        private void d(Canvas canvas) {
            Iterator<Tile> it = this.d.iterator();
            while (it.hasNext()) {
                it.next().b(canvas, Mediator.this.b.f260a);
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:3:0x000d  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final boolean a(int r4, android.view.KeyEvent r5) {
            /*
                r3 = this;
                r0 = 0
                com.mapabc.mapapi.ab<com.mapabc.mapapi.Overlay> r1 = r3.e
                java.util.Iterator r1 = r1.iterator()
            L_0x0007:
                boolean r2 = r1.hasNext()
                if (r2 == 0) goto L_0x0021
                java.lang.Object r0 = r1.next()
                com.mapabc.mapapi.Overlay r0 = (com.mapabc.mapapi.Overlay) r0
                com.mapabc.mapapi.Mediator r2 = com.mapabc.mapapi.Mediator.this
                com.mapabc.mapapi.Mediator$a r2 = r2.b
                com.mapabc.mapapi.MapView r2 = r2.f260a
                boolean r0 = r0.onKeyDown(r4, r5, r2)
                if (r0 == 0) goto L_0x0007
            L_0x0021:
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mapabc.mapapi.Mediator.d.a(int, android.view.KeyEvent):boolean");
        }

        /* JADX WARNING: Removed duplicated region for block: B:3:0x000d  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final boolean b(int r4, android.view.KeyEvent r5) {
            /*
                r3 = this;
                r0 = 0
                com.mapabc.mapapi.ab<com.mapabc.mapapi.Overlay> r1 = r3.e
                java.util.Iterator r1 = r1.iterator()
            L_0x0007:
                boolean r2 = r1.hasNext()
                if (r2 == 0) goto L_0x0021
                java.lang.Object r0 = r1.next()
                com.mapabc.mapapi.Overlay r0 = (com.mapabc.mapapi.Overlay) r0
                com.mapabc.mapapi.Mediator r2 = com.mapabc.mapapi.Mediator.this
                com.mapabc.mapapi.Mediator$a r2 = r2.b
                com.mapabc.mapapi.MapView r2 = r2.f260a
                boolean r0 = r0.onKeyUp(r4, r5, r2)
                if (r0 == 0) goto L_0x0007
            L_0x0021:
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mapabc.mapapi.Mediator.d.b(int, android.view.KeyEvent):boolean");
        }

        /* JADX WARNING: Removed duplicated region for block: B:3:0x000d  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final boolean a(android.view.MotionEvent r4) {
            /*
                r3 = this;
                r0 = 0
                com.mapabc.mapapi.ab<com.mapabc.mapapi.Overlay> r1 = r3.e
                java.util.Iterator r1 = r1.iterator()
            L_0x0007:
                boolean r2 = r1.hasNext()
                if (r2 == 0) goto L_0x0021
                java.lang.Object r0 = r1.next()
                com.mapabc.mapapi.Overlay r0 = (com.mapabc.mapapi.Overlay) r0
                com.mapabc.mapapi.Mediator r2 = com.mapabc.mapapi.Mediator.this
                com.mapabc.mapapi.Mediator$a r2 = r2.b
                com.mapabc.mapapi.MapView r2 = r2.f260a
                boolean r0 = r0.onTrackballEvent(r4, r2)
                if (r0 == 0) goto L_0x0007
            L_0x0021:
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mapabc.mapapi.Mediator.d.a(android.view.MotionEvent):boolean");
        }

        /* JADX WARNING: Removed duplicated region for block: B:3:0x000f  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final boolean b(android.view.MotionEvent r6) {
            /*
                r5 = this;
                r3 = 1
                r4 = 0
                com.mapabc.mapapi.ab<com.mapabc.mapapi.Overlay> r0 = r5.e
                java.util.Iterator r1 = r0.iterator()
                r0 = r4
            L_0x0009:
                boolean r2 = r1.hasNext()
                if (r2 == 0) goto L_0x0023
                java.lang.Object r0 = r1.next()
                com.mapabc.mapapi.Overlay r0 = (com.mapabc.mapapi.Overlay) r0
                com.mapabc.mapapi.Mediator r2 = com.mapabc.mapapi.Mediator.this
                com.mapabc.mapapi.Mediator$a r2 = r2.b
                com.mapabc.mapapi.MapView r2 = r2.f260a
                boolean r0 = r0.onTouchEvent(r6, r2)
                if (r0 == 0) goto L_0x0009
            L_0x0023:
                if (r0 != 0) goto L_0x0076
                int r1 = r6.getAction()
                if (r1 != r3) goto L_0x0076
                com.mapabc.mapapi.Mediator r0 = com.mapabc.mapapi.Mediator.this
                com.mapabc.mapapi.Mediator$ViewGeoProjector r0 = r0.f258a
                float r1 = r6.getX()
                int r1 = (int) r1
                float r2 = r6.getY()
                int r2 = (int) r2
                com.mapabc.mapapi.GeoPoint r1 = r0.fromPixels(r1, r2)
                com.mapabc.mapapi.ab<com.mapabc.mapapi.Overlay> r0 = r5.e
                int r0 = r0.size()
                int r0 = r0 - r3
                r2 = r0
            L_0x0045:
                if (r2 < 0) goto L_0x0075
                com.mapabc.mapapi.ab<com.mapabc.mapapi.Overlay> r0 = r5.e
                java.lang.Object r0 = r0.get(r2)
                com.mapabc.mapapi.Overlay r0 = (com.mapabc.mapapi.Overlay) r0
                boolean r3 = r0 instanceof com.mapabc.mapapi.ItemizedOverlay
                if (r3 == 0) goto L_0x0067
                com.mapabc.mapapi.ItemizedOverlay r0 = (com.mapabc.mapapi.ItemizedOverlay) r0
                com.mapabc.mapapi.Mediator r3 = com.mapabc.mapapi.Mediator.this
                com.mapabc.mapapi.Mediator$a r3 = r3.b
                com.mapabc.mapapi.MapView r3 = r3.f260a
                boolean r0 = r0.onTap(r1, r3)
                if (r0 != 0) goto L_0x0075
            L_0x0063:
                int r0 = r2 + -1
                r2 = r0
                goto L_0x0045
            L_0x0067:
                com.mapabc.mapapi.Mediator r3 = com.mapabc.mapapi.Mediator.this
                com.mapabc.mapapi.Mediator$a r3 = r3.b
                com.mapabc.mapapi.MapView r3 = r3.f260a
                boolean r0 = r0.onTap(r1, r3)
                if (r0 == 0) goto L_0x0063
            L_0x0075:
                r0 = r4
            L_0x0076:
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mapabc.mapapi.Mediator.d.b(android.view.MotionEvent):boolean");
        }

        public final void a(Canvas canvas) {
            Iterator<Tile> it = this.d.iterator();
            while (it.hasNext()) {
                it.next().a(canvas, Mediator.this.b.f260a);
            }
        }
    }

    class a {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public MapView f260a;
        private ArrayList<C0017ak> b;
        /* access modifiers changed from: private */
        public GeoPoint c = null;
        private int d = 0;
        private int e = 0;
        /* access modifiers changed from: private */
        public int f = -1;

        public a(MapView mapView) {
            this.f260a = mapView;
            this.b = new ArrayList<>();
        }

        public final void a(int i) {
            if (i != this.f) {
                this.f = i;
                a(false);
            }
        }

        public final void a(int i, int i2) {
            if (i != this.d || i2 != this.e) {
                this.d = i;
                this.e = i2;
                GlobalStore.setsViewWidth(i);
                GlobalStore.setsViewHeight(i2);
                a(false);
            }
        }

        public final void a(GeoPoint geoPoint) {
            if (geoPoint != null && !geoPoint.equals(this.c)) {
                this.c = geoPoint.Copy();
                a(false);
            }
        }

        public final void b(GeoPoint geoPoint) {
            if (geoPoint != null && !geoPoint.equals(this.c)) {
                this.c = geoPoint.Copy();
                a(true);
            }
        }

        public final int a() {
            return this.f;
        }

        public final GeoPoint b() {
            return this.c;
        }

        public final void a(C0017ak akVar) {
            this.b.add(akVar);
        }

        public final void a(boolean z) {
            if (this.c != null && this.f >= 0 && GlobalStore.getsViewWidth() != 0 && GlobalStore.getsViewHeight() != 0) {
                Iterator<C0017ak> it = this.b.iterator();
                while (it.hasNext()) {
                    it.next().a(z);
                }
            }
        }

        public final MapView c() {
            return this.f260a;
        }

        public final void d() {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.f260a.getChildCount()) {
                    View childAt = this.f260a.getChildAt(i2);
                    if (childAt != null && (childAt.getLayoutParams() instanceof MapView.LayoutParams)) {
                        MapView.LayoutParams layoutParams = (MapView.LayoutParams) childAt.getLayoutParams();
                        if (layoutParams.mode != 0) {
                            a(childAt, layoutParams.width, layoutParams.height, layoutParams.x, layoutParams.y, layoutParams.alignment);
                        } else if (layoutParams.point != null) {
                            Point pixels = Mediator.this.f258a.toPixels(layoutParams.point, null);
                            pixels.x += layoutParams.x;
                            pixels.y += layoutParams.y;
                            a(childAt, layoutParams.width, layoutParams.height, pixels.x, pixels.y, layoutParams.alignment);
                        }
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }

        private void a(View view, int i, int i2, int i3, int i4, int i5) {
            int i6;
            int i7;
            int i8;
            int i9;
            view.measure(i, i2);
            if (i == -2) {
                i6 = view.getMeasuredWidth();
            } else if (i == -1) {
                i6 = this.f260a.getMeasuredWidth();
            } else {
                i6 = i;
            }
            if (i2 == -2) {
                i7 = view.getMeasuredHeight();
            } else if (i2 == -1) {
                i7 = this.f260a.getMeasuredHeight();
            } else {
                i7 = i2;
            }
            int i10 = i5 & 7;
            int i11 = i5 & 112;
            if (i10 == 5) {
                i8 = i3 - i6;
            } else if (i10 == 1) {
                i8 = i3 - (i6 / 2);
            } else {
                i8 = i3;
            }
            if (i11 == 80) {
                i9 = i4 - i7;
            } else {
                i9 = i11 == 16 ? i4 - (i7 / 2) : i4;
            }
            view.layout(i8, i9, i6 + i8, i7 + i9);
        }
    }
}
