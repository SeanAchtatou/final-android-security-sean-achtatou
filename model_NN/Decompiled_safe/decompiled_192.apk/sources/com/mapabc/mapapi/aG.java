package com.mapabc.mapapi;

import android.content.Context;
import android.os.Handler;
import java.net.Proxy;
import java.util.ArrayList;

abstract class aG<T, V> extends C0028i {
    protected boolean c = true;
    protected C0008ab<V> d = new C0008ab<>();
    protected aL<T> e = new aL<>();
    private Runnable f = new aH(this);
    /* access modifiers changed from: private */
    public Runnable g = new aI(this);
    /* access modifiers changed from: private */
    public final Handler h = new Handler();
    private C0043x i = new C0043x(i(), this.f);

    /* access modifiers changed from: protected */
    public abstract V a(ArrayList arrayList);

    /* access modifiers changed from: protected */
    public abstract V a(ArrayList arrayList, Proxy proxy);

    /* access modifiers changed from: protected */
    public abstract void a(Object obj);

    /* access modifiers changed from: protected */
    public abstract int h();

    /* access modifiers changed from: protected */
    public abstract int i();

    public aG(Mediator mediator, Context context) {
        super(mediator, context);
        this.i.a();
    }

    public void a() {
        this.c = false;
        this.e.a();
    }
}
