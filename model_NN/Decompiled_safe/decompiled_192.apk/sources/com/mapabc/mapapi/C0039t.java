package com.mapabc.mapapi;

/* renamed from: com.mapabc.mapapi.t  reason: case insensitive filesystem */
final class C0039t {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public RouteMessageHandler f326a;
    /* access modifiers changed from: private */
    public int b = 4;
    /* access modifiers changed from: private */
    public int c;
    /* access modifiers changed from: private */
    public MapView d;
    /* access modifiers changed from: private */
    public RouteOverlay e;

    public C0039t(MapView mapView, RouteMessageHandler routeMessageHandler, RouteOverlay routeOverlay, int i) {
        this.d = mapView;
        this.f326a = routeMessageHandler;
        this.c = i;
        this.e = routeOverlay;
    }
}
