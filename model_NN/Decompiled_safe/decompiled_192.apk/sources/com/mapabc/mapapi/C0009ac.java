package com.mapabc.mapapi;

import android.content.Context;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: com.mapabc.mapapi.ac  reason: case insensitive filesystem */
final class C0009ac extends ar<ArrayList<O>, C0024e> {
    private ArrayList<O> d = new ArrayList<>();
    private ArrayList<H> e = new ArrayList<>();
    private C0018al f = new C0018al();
    private C0033n g;

    public final /* bridge */ /* synthetic */ aC a(Object obj, Proxy proxy, String str, String str2, String str3) {
        return new C0040u((ArrayList) obj, proxy, str, str2, str3);
    }

    public final /* bridge */ /* synthetic */ void a(Object obj) {
        C0024e eVar = (C0024e) obj;
        if (eVar != null) {
            this.e = eVar.b;
            this.f = eVar.f317a;
            this.f.b = (long) C0018al.a();
            a("autonavi_api_1_store.data", this.e);
            a("autonavi_api_3_store.data", this.f);
            a("autonavi_api_2_store.data", null);
            g();
        }
    }

    public final H e() {
        int i;
        if (this.e.size() == 0 || this.f.f310a == 0) {
            return null;
        }
        double random = Math.random();
        int i2 = 0;
        Iterator<H> it = this.e.iterator();
        while (true) {
            i = i2;
            if (!it.hasNext()) {
                break;
            }
            i2 = it.next().d + i;
        }
        int i3 = (int) (((double) i) * random);
        Iterator<H> it2 = this.e.iterator();
        while (it2.hasNext()) {
            H next = it2.next();
            i3 -= next.d;
            if (i3 <= 0) {
                return next;
            }
        }
        return null;
    }

    public final void a(C0033n nVar) {
        this.g = nVar;
    }

    public C0009ac(Mediator mediator, Context context) {
        super(mediator, context);
    }

    public final void a(O o) {
        this.d.add(o);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004a, code lost:
        a((java.io.InputStream) null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x004e, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x004f, code lost:
        r6 = r1;
        r1 = null;
        r0 = r6;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0049 A[ExcHandler: Exception (e java.lang.Exception), Splitter:B:7:0x0022] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void d() {
        /*
            r7 = this;
            super.d()
            int r0 = r7.c
            r1 = 3
            if (r0 >= r1) goto L_0x0009
        L_0x0008:
            return
        L_0x0009:
            int r0 = com.mapabc.mapapi.C0018al.a()
            long r0 = (long) r0
            com.mapabc.mapapi.al r2 = r7.f
            long r2 = r2.b
            long r0 = r0 - r2
            com.mapabc.mapapi.al r2 = r7.f
            long r2 = r2.c
            r4 = 60
            long r2 = r2 * r4
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x0047
            r0 = 1
        L_0x001f:
            if (r0 == 0) goto L_0x0008
            r0 = 0
            android.content.Context r1 = r7.b     // Catch:{ Exception -> 0x0049, all -> 0x004e }
            java.lang.String r2 = "autonavi_api_2_store.data"
            java.io.FileInputStream r0 = r1.openFileInput(r2)     // Catch:{ Exception -> 0x0049, all -> 0x004e }
            java.util.ArrayList r1 = com.mapabc.mapapi.O.a(r0)     // Catch:{ Exception -> 0x0049, all -> 0x0056 }
            java.util.ArrayList<com.mapabc.mapapi.O> r2 = r7.d     // Catch:{ Exception -> 0x0049, all -> 0x0056 }
            r2.addAll(r1)     // Catch:{ Exception -> 0x0049, all -> 0x0056 }
            a(r0)
        L_0x0036:
            java.util.ArrayList<com.mapabc.mapapi.O> r0 = r7.d
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r7.d = r1
            com.mapabc.mapapi.k r1 = r7.f()
            r1.a(r0)
            goto L_0x0008
        L_0x0047:
            r0 = 0
            goto L_0x001f
        L_0x0049:
            r1 = move-exception
            a(r0)
            goto L_0x0036
        L_0x004e:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x0052:
            a(r1)
            throw r0
        L_0x0056:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0052
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mapabc.mapapi.C0009ac.d():void");
    }

    private void g() {
        if (this.g != null) {
            if (this.f.f310a <= 0 || this.e.size() <= 0) {
                this.g.a();
            } else {
                this.g.b();
            }
        }
    }

    public final void b() {
        FileOutputStream fileOutputStream;
        FileOutputStream fileOutputStream2;
        Throwable th;
        if (this.d.size() != 0) {
            fileOutputStream = null;
            try {
                fileOutputStream2 = this.b.openFileOutput("autonavi_api_2_store.data", 32768);
                try {
                    ArrayList<O> arrayList = this.d;
                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream2);
                    int i = 0;
                    while (true) {
                        int i2 = i;
                        if (i2 < arrayList.size()) {
                            try {
                                outputStreamWriter.write("\n");
                                outputStreamWriter.write(arrayList.get(i2).toString());
                                outputStreamWriter.close();
                            } catch (IOException e2) {
                                e2.printStackTrace();
                            }
                            i = i2 + 1;
                        } else {
                            this.d.clear();
                            a((OutputStream) fileOutputStream2);
                            return;
                        }
                    }
                } catch (Exception e3) {
                    fileOutputStream = fileOutputStream2;
                } catch (Throwable th2) {
                    th = th2;
                    a((OutputStream) fileOutputStream2);
                    throw th;
                }
            } catch (Exception e4) {
            } catch (Throwable th3) {
                Throwable th4 = th3;
                fileOutputStream2 = null;
                th = th4;
                a((OutputStream) fileOutputStream2);
                throw th;
            }
        } else {
            return;
        }
        a((OutputStream) fileOutputStream);
    }

    public final void c() {
        if (this.f.b == 0) {
            Object a2 = a("autonavi_api_1_store.data");
            if (a2 != null) {
                this.e = (ArrayList) a2;
            }
            Object a3 = a("autonavi_api_3_store.data");
            if (a3 != null) {
                this.f = (C0018al) a3;
            }
        }
        g();
    }

    private Object a(String str) {
        FileInputStream fileInputStream;
        ObjectInputStream objectInputStream;
        FileInputStream fileInputStream2;
        ObjectInputStream objectInputStream2;
        try {
            FileInputStream openFileInput = this.b.openFileInput(str);
            try {
                objectInputStream = new ObjectInputStream(openFileInput);
            } catch (Exception e2) {
                fileInputStream2 = openFileInput;
                objectInputStream2 = null;
                a((InputStream) objectInputStream2);
                a((InputStream) fileInputStream2);
                return null;
            } catch (Throwable th) {
                fileInputStream = openFileInput;
                th = th;
                objectInputStream = null;
                a((InputStream) objectInputStream);
                a((InputStream) fileInputStream);
                throw th;
            }
            try {
                Object readObject = objectInputStream.readObject();
                a((InputStream) objectInputStream);
                a((InputStream) openFileInput);
                return readObject;
            } catch (Exception e3) {
                ObjectInputStream objectInputStream3 = objectInputStream;
                fileInputStream2 = openFileInput;
                objectInputStream2 = objectInputStream3;
                a((InputStream) objectInputStream2);
                a((InputStream) fileInputStream2);
                return null;
            } catch (Throwable th2) {
                Throwable th3 = th2;
                fileInputStream = openFileInput;
                th = th3;
                a((InputStream) objectInputStream);
                a((InputStream) fileInputStream);
                throw th;
            }
        } catch (Exception e4) {
            objectInputStream2 = null;
            fileInputStream2 = null;
        } catch (Throwable th4) {
            th = th4;
            objectInputStream = null;
            fileInputStream = null;
            a((InputStream) objectInputStream);
            a((InputStream) fileInputStream);
            throw th;
        }
    }

    private static void a(InputStream inputStream) {
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (Exception e2) {
            }
        }
    }

    private static void a(OutputStream outputStream) {
        if (outputStream != null) {
            try {
                outputStream.close();
            } catch (Exception e2) {
            }
        }
    }

    private void a(String str, Object obj) {
        ObjectOutputStream objectOutputStream;
        FileOutputStream fileOutputStream;
        ObjectOutputStream objectOutputStream2;
        FileOutputStream fileOutputStream2 = null;
        try {
            FileOutputStream openFileOutput = this.b.openFileOutput(str, 0);
            if (obj != null) {
                try {
                    objectOutputStream = new ObjectOutputStream(openFileOutput);
                    try {
                        objectOutputStream.writeObject(obj);
                    } catch (Exception e2) {
                        ObjectOutputStream objectOutputStream3 = objectOutputStream;
                        fileOutputStream = openFileOutput;
                        objectOutputStream2 = objectOutputStream3;
                        a((OutputStream) objectOutputStream2);
                        a((OutputStream) fileOutputStream);
                        return;
                    } catch (Throwable th) {
                        Throwable th2 = th;
                        fileOutputStream2 = openFileOutput;
                        th = th2;
                        a((OutputStream) objectOutputStream);
                        a((OutputStream) fileOutputStream2);
                        throw th;
                    }
                } catch (Exception e3) {
                    fileOutputStream = openFileOutput;
                    objectOutputStream2 = null;
                    a((OutputStream) objectOutputStream2);
                    a((OutputStream) fileOutputStream);
                    return;
                } catch (Throwable th3) {
                    Throwable th4 = th3;
                    objectOutputStream = null;
                    fileOutputStream2 = openFileOutput;
                    th = th4;
                    a((OutputStream) objectOutputStream);
                    a((OutputStream) fileOutputStream2);
                    throw th;
                }
            } else {
                objectOutputStream = null;
            }
            a((OutputStream) objectOutputStream);
            a((OutputStream) openFileOutput);
        } catch (Exception e4) {
            objectOutputStream2 = null;
            fileOutputStream = null;
        } catch (Throwable th5) {
            th = th5;
            objectOutputStream = null;
            a((OutputStream) objectOutputStream);
            a((OutputStream) fileOutputStream2);
            throw th;
        }
    }
}
