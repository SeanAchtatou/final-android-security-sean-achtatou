package com.mapabc.mapapi;

import android.view.View;
import com.mapabc.mapapi.MapView;

final class N extends C0013ag implements MapView.d {
    private RouteOverlay c;
    private int d;

    public N(MapView mapView, View view, GeoPoint geoPoint, RouteOverlay routeOverlay, int i, byte b) {
        this(mapView, view, geoPoint, routeOverlay, i);
    }

    private N(MapView mapView, View view, GeoPoint geoPoint, RouteOverlay routeOverlay, int i) {
        super(mapView, view, geoPoint, null, null);
        this.c = routeOverlay;
        this.d = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mapabc.mapapi.MapView.e.a(boolean, com.mapabc.mapapi.MapView$d):void
     arg types: [int, com.mapabc.mapapi.N]
     candidates:
      com.mapabc.mapapi.MapView.e.a(int, boolean):void
      com.mapabc.mapapi.MapView.e.a(int, int):void
      com.mapabc.mapapi.MapView.e.a(boolean, com.mapabc.mapapi.MapView$d):void */
    public final void a() {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        super.a();
        this.b.mRouteCtrl.a(true, (MapView.d) this);
        MapView.e eVar = this.b.mRouteCtrl;
        if (this.d < this.c.getRoute().getStepCount()) {
            z = true;
        } else {
            z = false;
        }
        eVar.b(z);
        MapView.e eVar2 = this.b.mRouteCtrl;
        if (this.d != 0) {
            z2 = true;
        } else {
            z2 = false;
        }
        eVar2.a(z2);
        MapView.e eVar3 = this.b.mRouteCtrl;
        if (this.b.getMaxZoomLevel() != this.b.getZoomLevel()) {
            z3 = true;
        } else {
            z3 = false;
        }
        eVar3.c(z3);
        MapView.e eVar4 = this.b.mRouteCtrl;
        if (this.b.getMinZoomLevel() != this.b.getZoomLevel()) {
            z4 = true;
        } else {
            z4 = false;
        }
        eVar4.d(z4);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mapabc.mapapi.MapView.e.a(boolean, com.mapabc.mapapi.MapView$d):void
     arg types: [int, com.mapabc.mapapi.N]
     candidates:
      com.mapabc.mapapi.MapView.e.a(int, boolean):void
      com.mapabc.mapapi.MapView.e.a(int, int):void
      com.mapabc.mapapi.MapView.e.a(boolean, com.mapabc.mapapi.MapView$d):void */
    public final void b() {
        super.b();
        this.b.mRouteCtrl.a(false, (MapView.d) this);
    }

    public final void a(int i) {
        this.c.f281a.onRouteEvent(this.b, this.c, this.d, i);
    }
}
