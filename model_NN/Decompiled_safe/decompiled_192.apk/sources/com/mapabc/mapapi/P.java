package com.mapabc.mapapi;

import java.io.IOException;
import java.io.InputStream;
import java.net.Proxy;
import java.util.ArrayList;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

abstract class P<T, V> extends X<T, ArrayList<V>> {
    /* access modifiers changed from: protected */
    public abstract void a(ArrayList arrayList);

    /* access modifiers changed from: protected */
    public abstract void a(Node node, ArrayList arrayList);

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object a(NodeList nodeList) {
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object e(InputStream inputStream) throws IOException {
        NodeList a2 = a(inputStream);
        ArrayList arrayList = new ArrayList();
        int length = a2.getLength();
        for (int i = 0; i < length; i++) {
            a(a2.item(i), arrayList);
        }
        a(arrayList);
        return arrayList;
    }

    public P(T t, Proxy proxy, String str, String str2) {
        super(t, proxy, str, str2, null);
    }
}
