package com.mapabc.mapapi;

import a.a.a.d;
import android.location.Location;
import com.mapabc.mapapi.PhoneStateManager;
import java.io.IOException;
import java.io.InputStream;
import java.net.Proxy;
import java.util.Date;

/* renamed from: com.mapabc.mapapi.w  reason: case insensitive filesystem */
final class C0042w extends aC<PhoneStateManager.a, Location> {
    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object e(InputStream inputStream) throws IOException {
        Location location = new Location(LocationProviderProxy.AutonaviCellProvider);
        location.setLongitude(Double.parseDouble(h(inputStream)));
        location.setLatitude(Double.parseDouble(h(inputStream)));
        location.setAccuracy((float) Integer.parseInt(h(inputStream)));
        location.setTime(new Date().getTime());
        h(inputStream);
        return location;
    }

    public C0042w(PhoneStateManager.a aVar, Proxy proxy, String str, String str2, String str3) {
        super(aVar, proxy, str, str2, str3);
    }

    /* access modifiers changed from: protected */
    public final String[] e() {
        PhoneStateManager.a aVar = (PhoneStateManager.a) this.d;
        if (aVar.d > 0 || aVar.e > 0) {
            return new String[]{a("nid", String.format("%d", Integer.valueOf(((PhoneStateManager.a) this.d).c))), a("sid", String.format("%d", Integer.valueOf(((PhoneStateManager.a) this.d).f268a))), a("bid", String.format("%d", Integer.valueOf(((PhoneStateManager.a) this.d).b))), a("lon", String.format("%d", Integer.valueOf(((PhoneStateManager.a) this.d).e))), a("lat", String.format("%d", Integer.valueOf(((PhoneStateManager.a) this.d).d))), "<cdma>1</cdma>", "<src>androidApi</src>"};
        }
        return new String[]{a((String) d.a.f33goto, String.format("%d", Integer.valueOf(((PhoneStateManager.a) this.d).c))), a("lac", String.format("%d", Integer.valueOf(((PhoneStateManager.a) this.d).f268a))), a("cellid", String.format("%d", Integer.valueOf(((PhoneStateManager.a) this.d).b))), "<src>androidApi</src>"};
    }

    /* access modifiers changed from: protected */
    public final int f() {
        return 14;
    }
}
