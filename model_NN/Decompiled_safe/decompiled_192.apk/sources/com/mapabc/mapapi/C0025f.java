package com.mapabc.mapapi;

import android.graphics.Canvas;

/* renamed from: com.mapabc.mapapi.f  reason: case insensitive filesystem */
abstract class C0025f extends Overlay {
    public abstract boolean draw(Canvas canvas, MapView mapView, boolean z, long j);

    C0025f() {
    }
}
