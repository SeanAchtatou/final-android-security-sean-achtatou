package com.mapabc.mapapi;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.view.MotionEvent;
import java.lang.reflect.Method;

public abstract class MultiTouchGestureDetector {
    /* access modifiers changed from: private */
    public static Method k;
    /* access modifiers changed from: private */
    public static Method l;
    /* access modifiers changed from: private */
    public static boolean m = false;
    private static boolean n = false;

    /* renamed from: a  reason: collision with root package name */
    OnGestureListener f264a;
    int b = 0;
    Matrix c = new Matrix();
    Matrix d = new Matrix();
    PointF e = new PointF();
    PointF f = new PointF();
    PointF g = new PointF();
    float h = 1.0f;
    float i = 1.0f;
    boolean j = false;

    public interface OnGestureListener {
        boolean endScale(float f, PointF pointF);

        boolean onDrag(float f, float f2);

        boolean onDrag(Matrix matrix);

        boolean onScale(float f);

        boolean onScale(Matrix matrix);

        boolean startScale(PointF pointF);
    }

    public abstract boolean onTouchEvent(MotionEvent motionEvent);

    static /* synthetic */ void a(MotionEvent motionEvent) {
        if (!n) {
            n = true;
            try {
                k = motionEvent.getClass().getMethod("getX", Integer.TYPE);
                l = motionEvent.getClass().getMethod("getY", Integer.TYPE);
                if (k != null && l != null) {
                    m = true;
                }
            } catch (Exception e2) {
            }
        }
    }

    public static MultiTouchGestureDetector newInstance(Context context, OnGestureListener onGestureListener) {
        C0016aj ajVar = new C0016aj();
        ajVar.f264a = onGestureListener;
        return ajVar;
    }

    public boolean isMultiTouch() {
        return this.j;
    }
}
