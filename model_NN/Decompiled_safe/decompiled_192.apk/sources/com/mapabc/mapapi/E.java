package com.mapabc.mapapi;

import android.util.Log;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;

final class E<K, V> {
    private static boolean g = false;

    /* renamed from: a  reason: collision with root package name */
    private long f227a;
    private int b;
    private int c;
    private boolean d;
    private Hashtable<K, a<V>> e;
    private byte[] f;

    public E(long j, boolean z, byte b2) {
        this(j, z);
    }

    private E(long j, boolean z) {
        this.f = new byte[0];
        this.e = new Hashtable<>();
        this.f227a = j;
        this.b = 120;
        this.c = 10;
        this.d = z;
    }

    public final void a(K k, V v) {
        if (!g) {
            int size = this.e.size();
            if (size >= this.b) {
                synchronized (this.f) {
                    g = true;
                    Map.Entry[] a2 = a((Hashtable) this.e);
                    int i = (size - this.b) + this.c;
                    for (int i2 = 0; i2 < i; i2++) {
                        this.e.remove(a2[i2].getKey());
                    }
                    g = false;
                }
            }
            a aVar = new a();
            aVar.f228a = v;
            aVar.b = W.a();
            this.e.put(k, aVar);
        }
    }

    public final V a(K k) {
        a aVar = this.e.get(k);
        if (aVar == null) {
            return null;
        }
        if (this.d) {
            aVar.b = W.a();
        }
        return aVar.f228a;
    }

    public final long b(K k) {
        a aVar = this.e.get(k);
        if (aVar == null) {
            return 0;
        }
        return aVar.b;
    }

    public final void c(K k) {
        this.e.remove(k);
    }

    public final void a() {
        if (this.e.size() >= 2) {
            long a2 = W.a();
            Map.Entry[] a3 = a((Hashtable) this.e);
            int length = a3.length;
            int i = 0;
            while (i < length) {
                Map.Entry entry = a3[i];
                if (W.a(((a) entry.getValue()).b, a2, this.f227a)) {
                    this.e.remove(entry.getKey());
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    public final void b() {
        this.e.clear();
    }

    private static Map.Entry[] a(Hashtable hashtable) {
        Set entrySet = hashtable.entrySet();
        Map.Entry[] entryArr = (Map.Entry[]) entrySet.toArray(new Map.Entry[entrySet.size()]);
        try {
            Arrays.sort(entryArr, new Comparator() {
                public final int compare(Object obj, Object obj2) {
                    return Long.valueOf(((a) ((Map.Entry) obj).getValue()).b).compareTo((Object) Long.valueOf(((a) ((Map.Entry) obj2).getValue()).b));
                }
            });
        } catch (Exception e2) {
            Log.e("Mapabc", "in the sort cach!");
        }
        return entryArr;
    }

    private static class a<V> {

        /* renamed from: a  reason: collision with root package name */
        public V f228a;
        public long b;

        /* synthetic */ a() {
            this((byte) 0);
        }

        private a(byte b2) {
        }
    }
}
