package com.mapabc.mapapi;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.hardware.SensorListener;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import com.mapabc.mapapi.ConfigableConst;
import com.mapabc.mapapi.Overlay;

public class MyLocationOverlay extends Overlay implements SensorListener, LocationListener, Overlay.Snappable {

    /* renamed from: a  reason: collision with root package name */
    private Mediator f265a;
    private C0034o b;
    private aw c;
    private boolean d = false;
    private boolean e = false;
    private Runnable f;
    private float g = Float.NaN;
    private C h;
    private T i;

    public MyLocationOverlay(Context context, MapView mapView) {
        if (mapView == null) {
            throw new IllegalArgumentException("mapView = null");
        }
        this.f265a = mapView.getMediator();
        this.b = (C0034o) this.f265a.e.a(1);
        this.c = (aw) this.f265a.e.a(2);
        this.f = null;
        this.g = 0.0f;
        this.h = new C();
        this.i = new T(this.f265a, new Bitmap[]{ConfigableConst.d[ConfigableConst.ENUM_ID.eloc1.ordinal()], ConfigableConst.d[ConfigableConst.ENUM_ID.eloc2.ordinal()]});
        disableMyLocation();
        this.d = false;
        disableCompass();
        this.e = false;
        try {
            this.b.g();
        } catch (Exception e2) {
        }
    }

    public boolean enableMyLocation() {
        if (!this.b.a((LocationListener) this)) {
            return false;
        }
        this.d = true;
        return true;
    }

    public void disableMyLocation() {
        this.b.e();
        this.d = false;
    }

    public void onLocationChanged(Location location) {
        this.f265a.d.f();
        if (this.f != null) {
            this.b.onLocationChanged(location);
            this.f = null;
        }
    }

    public void onProviderDisabled(String str) {
        this.d = false;
    }

    public void onProviderEnabled(String str) {
        this.d = true;
    }

    public void onStatusChanged(String str, int i2, Bundle bundle) {
    }

    public Location getLastFix() {
        return this.b.f();
    }

    public GeoPoint getMyLocation() {
        return a(this.b.f());
    }

    private static GeoPoint a(Location location) {
        if (location != null) {
            return new GeoPoint(W.a(location.getLatitude()), W.a(location.getLongitude()));
        }
        return null;
    }

    public boolean runOnFirstFix(Runnable runnable) {
        Location lastFix = getLastFix();
        if (lastFix != null) {
            runnable.run();
        } else {
            this.f = runnable;
        }
        return lastFix != null;
    }

    public boolean onSnapToItem(int i2, int i3, Point point, MapView mapView) {
        return false;
    }

    public void onSensorChanged(int i2, float[] fArr) {
        this.g = fArr[0];
        this.f265a.d.a(this.h.b().left, this.h.b().top, this.h.f316a.getWidth(), this.h.f316a.getHeight());
    }

    public void onAccuracyChanged(int i2, int i3) {
    }

    public boolean draw(Canvas canvas, MapView mapView, boolean z, long j) {
        Location f2;
        if (z) {
            return false;
        }
        if (this.d && (f2 = this.b.f()) != null) {
            drawMyLocation(canvas, this.f265a.b.c(), f2, a(f2), j);
        }
        if (this.e) {
            drawCompass(canvas, this.g);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void drawMyLocation(Canvas canvas, MapView mapView, Location location, GeoPoint geoPoint, long j) {
        Point pixels = this.f265a.f258a.toPixels(geoPoint, null);
        float f2 = 500.0f;
        Paint paint = new Paint();
        paint.setColor(-16776961);
        paint.setAlpha(40);
        if (!location.equals(LocationProviderProxy.AutonaviCellProvider) && location.hasAccuracy() && location.getAccuracy() > 0.0f) {
            f2 = location.getAccuracy();
        }
        canvas.drawCircle((float) pixels.x, (float) pixels.y, (float) ((int) mapView.getProjection().metersToEquatorPixels(f2)), paint);
        paint.setAlpha(255);
        paint.setStyle(Paint.Style.STROKE);
        paint.setAntiAlias(true);
        canvas.drawCircle((float) pixels.x, (float) pixels.y, (float) ((int) mapView.getProjection().metersToEquatorPixels(f2)), paint);
        if (W.a(new Rect(0, 0, GlobalStore.getsViewWidth(), GlobalStore.getsViewHeight()), pixels)) {
            this.i.a(canvas, pixels.x, pixels.y);
        }
    }

    /* access modifiers changed from: protected */
    public void drawCompass(Canvas canvas, float f2) {
        if (this.e) {
            this.h.a(f2);
            this.h.draw(canvas, this.f265a.b.c(), false, 0);
        }
    }

    public boolean enableCompass() {
        if (!this.c.a(this)) {
            return false;
        }
        this.e = true;
        return true;
    }

    public void disableCompass() {
        this.c.e();
        this.e = false;
    }

    public boolean isMyLocationEnabled() {
        return this.d;
    }

    public boolean isCompassEnabled() {
        return this.e;
    }

    public float getOrientation() {
        return this.g;
    }

    /* access modifiers changed from: protected */
    public boolean dispatchTap() {
        return false;
    }

    public boolean onTap(GeoPoint geoPoint, MapView mapView) {
        Rect rect;
        if (!this.d) {
            return false;
        }
        GeoPoint myLocation = getMyLocation();
        if (myLocation != null) {
            int c2 = this.i.c() / 2;
            int d2 = this.i.d() / 2;
            Point pixels = this.f265a.f258a.toPixels(myLocation, null);
            rect = new Rect(pixels.x - c2, pixels.y - d2, c2 + pixels.x, pixels.y + d2);
        } else {
            rect = null;
        }
        if (rect == null) {
            return false;
        }
        Point pixels2 = this.f265a.f258a.toPixels(geoPoint, null);
        if (rect.contains(pixels2.x, pixels2.y)) {
            return dispatchTap();
        }
        return false;
    }
}
