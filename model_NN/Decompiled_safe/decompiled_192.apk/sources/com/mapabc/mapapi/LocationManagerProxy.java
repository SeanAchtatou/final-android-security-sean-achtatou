package com.mapabc.mapapi;

import android.app.PendingIntent;
import android.content.Context;
import android.location.Criteria;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class LocationManagerProxy {
    public static final String GPS_PROVIDER = "gps";
    public static final String KEY_LOCATION_CHANGED = "location";
    public static final String KEY_PROVIDER_ENABLED = "providerEnabled";
    public static final String KEY_PROXIMITY_ENTERING = "entering";
    public static final String KEY_STATUS_CHANGED = "status";
    public static final String NETWORK_PROVIDER = "network";
    public static final String PASSIVE_PROVIDER = "passive";

    /* renamed from: a  reason: collision with root package name */
    private LocationManager f244a;
    private C0006a b;
    private Hashtable<String, LocationProviderProxy> c = new Hashtable<>();

    public LocationManagerProxy(MapActivity mapActivity) {
        a(mapActivity, W.a(mapActivity), W.a((Context) mapActivity));
    }

    public LocationManagerProxy(Context context, String str) {
        a(context, str, W.a(context));
    }

    private void a(Context context, String str, String str2) {
        this.f244a = (LocationManager) context.getSystemService(KEY_LOCATION_CHANGED);
        getProvider(LocationProviderProxy.AutonaviCellProvider);
        this.b = new C0006a(context, str, str2);
    }

    public void addProximityAlert(double d, double d2, float f, long j, PendingIntent pendingIntent) {
        this.f244a.addProximityAlert(d, d2, f, j, pendingIntent);
    }

    public void removeProximityAlert(PendingIntent pendingIntent) {
        this.f244a.removeProximityAlert(pendingIntent);
    }

    public List<String> getProviders(boolean z) {
        List<String> providers = this.f244a.getProviders(z);
        if (isProviderEnabled(LocationProviderProxy.AutonaviCellProvider)) {
            if (providers == null) {
                providers = new ArrayList<>();
            }
            providers.add(LocationProviderProxy.AutonaviCellProvider);
        }
        return providers;
    }

    public List<String> getProviders(Criteria criteria, boolean z) {
        List<String> providers = this.f244a.getProviders(criteria, z);
        if (LocationProviderProxy.AutonaviCellProvider.equals(getBestProvider(criteria, z))) {
            providers.add(LocationProviderProxy.AutonaviCellProvider);
        }
        return providers;
    }

    public LocationProviderProxy getProvider(String str) {
        if (str == null) {
            throw new IllegalArgumentException("name==null");
        } else if (this.c.containsKey(str)) {
            return this.c.get(str);
        } else {
            LocationProviderProxy a2 = LocationProviderProxy.a(this.f244a, str);
            if (a2 == null) {
                return a2;
            }
            this.c.put(str, a2);
            return a2;
        }
    }

    public void requestLocationUpdates(String str, long j, float f, PendingIntent pendingIntent) {
        if (LocationProviderProxy.AutonaviCellProvider.equals(str)) {
            this.b.a(pendingIntent);
        } else {
            this.f244a.requestLocationUpdates(str, j, f, pendingIntent);
        }
    }

    public void requestLocationUpdates(String str, long j, float f, LocationListener locationListener) {
        if (LocationProviderProxy.AutonaviCellProvider.equals(str)) {
            this.b.a(locationListener, Looper.myLooper());
        } else {
            this.f244a.requestLocationUpdates(str, j, f, locationListener);
        }
    }

    public void requestLocationUpdates(String str, long j, float f, LocationListener locationListener, Looper looper) {
        if (LocationProviderProxy.AutonaviCellProvider.equals(str)) {
            this.b.a(locationListener, looper);
        } else {
            this.f244a.requestLocationUpdates(str, j, f, locationListener, looper);
        }
    }

    public boolean isProviderEnabled(String str) {
        return LocationProviderProxy.AutonaviCellProvider.equals(str) ? this.b.b() : this.f244a.isProviderEnabled(str);
    }

    public Location getLastKnownLocation(String str) {
        return LocationProviderProxy.AutonaviCellProvider.equals(str) ? this.b.a() : this.f244a.getLastKnownLocation(str);
    }

    public boolean sendExtraCommand(String str, String str2, Bundle bundle) {
        if (LocationProviderProxy.AutonaviCellProvider.equals(str)) {
            return false;
        }
        return this.f244a.sendExtraCommand(str, str2, bundle);
    }

    public String getBestProvider(Criteria criteria, boolean z) {
        if (!getProvider(LocationProviderProxy.AutonaviCellProvider).meetsCriteria(criteria)) {
            return this.f244a.getBestProvider(criteria, z);
        }
        return (!z || this.b.b()) ? LocationProviderProxy.AutonaviCellProvider : this.f244a.getBestProvider(criteria, z);
    }

    public List<String> getAllProviders() {
        List<String> allProviders = this.f244a.getAllProviders();
        allProviders.add(LocationProviderProxy.AutonaviCellProvider);
        return allProviders;
    }

    public boolean addGpsStatusListener(GpsStatus.Listener listener) {
        return this.f244a.addGpsStatusListener(listener);
    }

    public void addTestProvider(String str, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, int i, int i2) {
        this.f244a.addTestProvider(str, z, z2, z3, z4, z5, z6, z7, i, i2);
    }

    public void clearTestProviderEnabled(String str) {
        this.f244a.clearTestProviderEnabled(str);
    }

    public void clearTestProviderLocation(String str) {
        this.f244a.clearTestProviderLocation(str);
    }

    public void clearTestProviderStatus(String str) {
        this.f244a.clearTestProviderStatus(str);
    }

    public GpsStatus getGpsStatus(GpsStatus gpsStatus) {
        return this.f244a.getGpsStatus(gpsStatus);
    }

    public void removeGpsStatusListener(GpsStatus.Listener listener) {
        this.f244a.removeGpsStatusListener(listener);
    }

    public void removeUpdates(LocationListener locationListener) {
        this.f244a.removeUpdates(locationListener);
        this.b.a(locationListener);
    }

    public void removeUpdates(PendingIntent pendingIntent) {
        this.f244a.removeUpdates(pendingIntent);
        this.b.b(pendingIntent);
    }
}
