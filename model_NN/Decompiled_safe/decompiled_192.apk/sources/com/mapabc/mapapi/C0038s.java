package com.mapabc.mapapi;

import com.mapabc.mapapi.Tile;
import java.util.List;

/* renamed from: com.mapabc.mapapi.s  reason: case insensitive filesystem */
final class C0038s extends C0008ab<Tile.TileCoordinate> {
    C0038s() {
    }

    public final synchronized void a(List<Tile> list) {
        for (Tile tile : list) {
            remove(tile.f284a);
        }
    }

    public final synchronized void b(List<Tile.TileCoordinate> list) {
        for (Tile.TileCoordinate c : list) {
            c(c);
        }
    }
}
