package com.mapabc.mapapi;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

final class aD implements C0015ai {

    /* renamed from: a  reason: collision with root package name */
    private Drawable f298a;
    private Drawable b;
    private int c;
    private int d;
    private float e = 0.0f;
    private C0041v f = new C0041v(Bitmap.Config.ARGB_4444);

    public aD(Bitmap bitmap, Bitmap bitmap2) {
        this.c = bitmap.getWidth();
        this.d = bitmap.getHeight();
        this.f298a = a(bitmap);
        this.b = a(bitmap2);
    }

    private Drawable a(Bitmap bitmap) {
        BitmapDrawable bitmapDrawable = new BitmapDrawable(bitmap);
        bitmapDrawable.setBounds(0, 0, this.c, this.d);
        return bitmapDrawable;
    }

    public final Bitmap a(float f2) {
        this.e = f2;
        this.f.a(this.c, this.d);
        this.f.a(this);
        return this.f.a();
    }

    public final void a(Canvas canvas) {
        this.f298a.draw(canvas);
        canvas.rotate(-this.e, (float) (this.c / 2), (float) (this.d / 2));
        this.b.draw(canvas);
    }
}
