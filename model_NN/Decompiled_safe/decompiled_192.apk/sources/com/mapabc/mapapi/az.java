package com.mapabc.mapapi;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import java.util.ArrayList;

final class az extends I {
    private Paint b;
    private GeoPoint[] c;

    az(RouteOverlay routeOverlay, GeoPoint[] geoPointArr, Paint paint) {
        super(routeOverlay);
        this.b = paint;
        this.c = geoPointArr;
    }

    public final void a(Canvas canvas, MapView mapView, boolean z) {
        if (!z) {
            int i = 0;
            ArrayList arrayList = new ArrayList();
            while (i < this.c.length - 1) {
                i = a(mapView, i, arrayList);
                if (arrayList.size() > 0) {
                    a(canvas, arrayList);
                    arrayList.clear();
                }
            }
        }
    }

    private void a(Canvas canvas, ArrayList<Point> arrayList) {
        int size = arrayList.size();
        int i = 1;
        Point point = arrayList.get(0);
        while (i < size) {
            Point point2 = arrayList.get(i);
            canvas.drawLine((float) point.x, (float) point.y, (float) point2.x, (float) point2.y, this.b);
            i++;
            point = point2;
        }
    }

    private static boolean a(Point point, Point point2) {
        return Math.abs(point.x - point2.x) <= 2 && Math.abs(point.y - point2.y) <= 2;
    }

    private int a(MapView mapView, int i, ArrayList<Point> arrayList) {
        Point a2 = RouteOverlay.a(mapView, this.c[i]);
        int i2 = i;
        while (i2 < this.c.length - 1) {
            i2++;
            Point a3 = RouteOverlay.a(mapView, this.c[i2]);
            if (!((a2 == null || a3 == null) ? false : RouteOverlay.a(mapView, a2, 0) || RouteOverlay.a(mapView, a3, 0))) {
                break;
            }
            if (arrayList.size() == 0) {
                arrayList.add(a2);
                arrayList.add(a3);
            } else if (a(a2, a3)) {
                arrayList.set(arrayList.size() - 1, a3);
            } else {
                arrayList.add(a3);
            }
            a2 = a3;
        }
        if (arrayList.size() > 2 && a(arrayList.get(0), arrayList.get(1))) {
            arrayList.remove(1);
        }
        return i2;
    }
}
