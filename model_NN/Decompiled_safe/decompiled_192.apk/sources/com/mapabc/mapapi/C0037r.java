package com.mapabc.mapapi;

import android.location.Location;
import com.mapabc.mapapi.PhoneStateManager;
import java.net.Proxy;

/* renamed from: com.mapabc.mapapi.r  reason: case insensitive filesystem */
final class C0037r extends C0045z<PhoneStateManager.a, Location> {
    private C0007aa e;
    private PhoneStateManager.a f;

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void b(Object obj) {
        Location location = (Location) obj;
        if (location != null) {
            this.e.onTransComplete(new PhoneStateManager.b(this.f, location));
        }
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object c(Object obj) {
        PhoneStateManager.a aVar = (PhoneStateManager.a) obj;
        this.f = aVar;
        return (Location) new C0042w(aVar, this.d, this.f330a, this.b, this.c).h();
    }

    public C0037r(Proxy proxy, String str, String str2, C0007aa aaVar) {
        super(proxy, str, str2, null);
        this.e = aaVar;
    }
}
