package com.mapabc.mapapi;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.net.Proxy;

/* renamed from: com.mapabc.mapapi.z  reason: case insensitive filesystem */
abstract class C0045z<T, V> {

    /* renamed from: a  reason: collision with root package name */
    protected String f330a;
    protected String b;
    protected String c;
    protected Proxy d;
    /* access modifiers changed from: private */
    public C0045z<T, V>.b e = new b(W.c());

    /* access modifiers changed from: protected */
    public abstract void b(V v);

    /* access modifiers changed from: protected */
    public abstract V c(T t);

    public C0045z(Proxy proxy, String str, String str2, String str3) {
        this.d = proxy;
        this.f330a = str;
        this.b = str2;
        this.c = str3;
    }

    public void a(Object obj) {
        new a(obj).start();
    }

    /* renamed from: com.mapabc.mapapi.z$a */
    class a extends Thread {

        /* renamed from: a  reason: collision with root package name */
        private T f331a;

        public a(T t) {
            this.f331a = t;
        }

        public final void run() {
            C0045z.this.e.sendMessage(C0045z.this.e.obtainMessage(0, 0, 0, C0045z.this.c(this.f331a)));
        }
    }

    /* renamed from: com.mapabc.mapapi.z$b */
    class b extends Handler {
        public b(Looper looper) {
            super(looper);
        }

        public final void handleMessage(Message message) {
            C0045z.this.b(message.obj);
        }
    }
}
