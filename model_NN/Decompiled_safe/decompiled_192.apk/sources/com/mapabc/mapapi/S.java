package com.mapabc.mapapi;

import java.io.IOException;
import java.io.InputStream;
import java.net.Proxy;

final class S extends aC<Object, Boolean> {
    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object e(InputStream inputStream) throws IOException {
        String h = h(inputStream);
        if (!W.a(h)) {
            this.f = h;
        }
        return Boolean.valueOf(g(inputStream) == 1);
    }

    public S(Object obj, Proxy proxy, String str, String str2, String str3) {
        super(obj, proxy, str, str2, str3);
    }

    /* access modifiers changed from: protected */
    public final String[] e() {
        return null;
    }

    /* access modifiers changed from: protected */
    public final int f() {
        return 0;
    }
}
