package com.mapabc.mapapi;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;

final class aA extends au {
    private Drawable d;
    private RouteMessageHandler e;
    private boolean f = false;
    private boolean g = false;
    private int h;
    private int i;

    public aA(RouteOverlay routeOverlay, int i2, GeoPoint geoPoint, Drawable drawable, RouteMessageHandler routeMessageHandler) {
        super(routeOverlay, i2, geoPoint);
        this.d = drawable;
        this.e = routeMessageHandler;
        Rect bounds = this.d.getBounds();
        this.h = (int) (((double) bounds.width()) * 1.5d);
        this.i = (int) (((double) bounds.height()) * 1.5d);
    }

    public final void a(Canvas canvas, MapView mapView, boolean z) {
        if (this.d != null && !z) {
            Point a2 = a(mapView, this.c);
            if (RouteOverlay.a(mapView, a2, 0)) {
                Route route = this.f239a.getRoute();
                if (!(this.b == 0 ? route.getStartPos() : route.getTargetPos()).equals(this.c)) {
                    Paint paint = new Paint();
                    paint.setColor(Color.rgb(200, 0, 0));
                    paint.setAlpha(150);
                    paint.setStrokeWidth(3.5f);
                    paint.setStrokeCap(Paint.Cap.ROUND);
                    paint.setStrokeJoin(Paint.Join.ROUND);
                    paint.setAntiAlias(true);
                    canvas.drawLine((float) (a2.x - 5), (float) (a2.y - 5), (float) (a2.x + 5), (float) (a2.y + 5), paint);
                    canvas.drawLine((float) (a2.x - 5), (float) (a2.y + 5), (float) (a2.x + 5), (float) (a2.y - 5), paint);
                }
                Overlay.a(canvas, this.d, a2.x, a2.y);
            }
        }
    }

    private boolean b(MapView mapView, Point point) {
        Point a2 = a(mapView, this.c);
        return ((a2.y - point.y) * (a2.y - point.y)) + ((a2.x - point.x) * (a2.x - point.x)) > this.h * this.i;
    }

    public final boolean a(MotionEvent motionEvent, MapView mapView) {
        Point point = new Point((int) motionEvent.getX(), (int) motionEvent.getY());
        if (motionEvent.getAction() == 0) {
            int i2 = point.x;
            int i3 = point.y;
            Point a2 = a(mapView, this.c);
            Rect bounds = this.d.getBounds();
            if ((Math.abs(i2 - a2.x) << 1) < bounds.width() / 2 && a2.y - i3 > 0 && a2.y - i3 < bounds.height()) {
                this.f = true;
                return true;
            }
        }
        if (motionEvent.getAction() == 2) {
            if (this.g) {
                if (b(mapView, point)) {
                    this.e.onDrag(mapView, this.f239a, this.b, a(mapView, point));
                    return true;
                }
            } else if (!this.f) {
                return false;
            } else {
                if (b(mapView, point)) {
                    this.g = true;
                    this.e.onDragBegin(mapView, this.f239a, this.b, a(mapView, point));
                    return true;
                }
            }
            return true;
        } else if (motionEvent.getAction() != 1 || !this.f) {
            return false;
        } else {
            this.f = false;
            if (this.g) {
                this.g = false;
                this.e.onDragEnd(mapView, this.f239a, this.b, a(mapView, point));
                return true;
            }
            this.e.onRouteEvent(mapView, this.f239a, this.b, 4);
            return true;
        }
    }

    public final void a(GeoPoint geoPoint) {
        this.c = geoPoint;
    }
}
