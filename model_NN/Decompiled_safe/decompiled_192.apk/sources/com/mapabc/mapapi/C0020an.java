package com.mapabc.mapapi;

import android.database.sqlite.SQLiteDatabase;

/* renamed from: com.mapabc.mapapi.an  reason: case insensitive filesystem */
abstract class C0020an {
    public abstract void a(SQLiteDatabase sQLiteDatabase);

    public abstract void b(SQLiteDatabase sQLiteDatabase);

    C0020an() {
    }

    public final void d(SQLiteDatabase sQLiteDatabase) {
        try {
            b(sQLiteDatabase);
        } catch (Exception e) {
            a(sQLiteDatabase);
            b(sQLiteDatabase);
        }
    }
}
