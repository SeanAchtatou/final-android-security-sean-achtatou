package com.mapabc.mapapi;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;

final class aE {

    /* renamed from: a  reason: collision with root package name */
    static Drawable f299a;
    static Drawable b;
    static Drawable c;
    static Drawable d;
    static Drawable e;
    static Drawable f;
    static Paint g;
    static Paint h;
    static Paint i;
    private static boolean j = false;

    aE() {
    }

    static void a(Context context) {
        if (!j) {
            Paint paint = new Paint();
            h = paint;
            paint.setColor(Color.rgb(54, 114, 227));
            h.setAlpha(180);
            h.setStrokeWidth(5.5f);
            h.setStrokeJoin(Paint.Join.ROUND);
            h.setAntiAlias(true);
            Paint paint2 = new Paint();
            g = paint2;
            paint2.setColor(Color.rgb(70, 73, 70));
            g.setAlpha(150);
            g.setStrokeWidth(5.5f);
            g.setStrokeJoin(Paint.Join.ROUND);
            g.setAntiAlias(true);
            Paint paint3 = new Paint();
            i = paint3;
            paint3.setColor(Color.rgb(107, 190, 231));
            i.setAlpha(180);
            i.setStrokeWidth(5.5f);
            i.setStrokeJoin(Paint.Join.ROUND);
            i.setAntiAlias(true);
            new DisplayMetrics();
            DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
            long j2 = (long) (displayMetrics.heightPixels * displayMetrics.widthPixels);
            if (j2 > 153600) {
                a(context, 1);
            } else if (j2 < 153600) {
                a(context, 3);
            } else {
                a(context, 2);
            }
            Rect rect = new Rect(8, 4, 16, 14);
            Rect rect2 = new Rect(17, 5, 8, 12);
            ConfigableConst.a(context, "left_back.png", new byte[]{1, 2, 2, 9, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 16, 0, 0, 0, 4, 0, 0, 0, 14, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 18, 0, 0, 0, 4, 0, 0, 0, 17, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0}, rect);
            ConfigableConst.a(context, "right_back.png", new byte[]{1, 2, 2, 9, 0, 0, 0, 0, 0, 0, 0, 0, 17, 0, 0, 0, 8, 0, 0, 0, 5, 0, 0, 0, 12, 0, 0, 0, 0, 0, 0, 0, 14, 0, 0, 0, 26, 0, 0, 0, 5, 0, 0, 0, 19, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0}, rect2);
            j = true;
        }
    }

    static Drawable b(Context context) {
        return ConfigableConst.a(context, "left_back.png", new byte[]{1, 2, 2, 9, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 16, 0, 0, 0, 4, 0, 0, 0, 14, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 18, 0, 0, 0, 4, 0, 0, 0, 17, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0}, new Rect(8, 4, 16, 14));
    }

    private static void a(Context context, int i2) {
        switch (i2) {
            case 1:
                try {
                    f299a = ConfigableConst.a(context, "start_w.png");
                    b = ConfigableConst.a(context, "end_w.png");
                    c = ConfigableConst.a(context, "foot_w.png");
                    d = ConfigableConst.a(context, "car_w.png");
                    e = ConfigableConst.a(context, "starticon_w.png");
                    f = ConfigableConst.a(context, "endicon_w.png");
                    return;
                } catch (Exception e2) {
                    return;
                }
            case 2:
                try {
                    f299a = ConfigableConst.a(context, "start.png");
                    b = ConfigableConst.a(context, "end.png");
                    c = ConfigableConst.a(context, "foot.png");
                    d = ConfigableConst.a(context, "car.png");
                    e = ConfigableConst.a(context, "starticon.png");
                    f = ConfigableConst.a(context, "endicon.png");
                    return;
                } catch (Exception e3) {
                    return;
                }
            case 3:
                try {
                    f299a = ConfigableConst.a(context, "start.png");
                    b = ConfigableConst.a(context, "end.png");
                    c = ConfigableConst.a(context, "foot.png");
                    d = ConfigableConst.a(context, "car.png");
                    e = ConfigableConst.a(context, "starticon.png");
                    f = ConfigableConst.a(context, "endicon.png");
                    return;
                } catch (Exception e4) {
                    return;
                }
            default:
                try {
                    f299a = ConfigableConst.a(context, "start.png");
                    b = ConfigableConst.a(context, "end.png");
                    c = ConfigableConst.a(context, "foot.png");
                    d = ConfigableConst.a(context, "car.png");
                    e = ConfigableConst.a(context, "starticon.png");
                    f = ConfigableConst.a(context, "endicon.png");
                    return;
                } catch (Exception e5) {
                    return;
                }
        }
    }
}
