package com.mapabc.mapapi;

import android.content.Context;
import android.location.Criteria;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import com.mapabc.mapapi.GeoPoint;
import java.net.Proxy;
import java.util.ArrayList;

/* renamed from: com.mapabc.mapapi.o  reason: case insensitive filesystem */
final class C0034o extends aG<Location, Location> implements LocationListener {
    private LocationListener f = null;
    private Criteria g = new Criteria();
    /* access modifiers changed from: private */
    public LocationManagerProxy h;
    private Location i = null;
    /* access modifiers changed from: private */
    public boolean j = false;
    /* access modifiers changed from: private */
    public boolean k = false;
    /* access modifiers changed from: private */
    public long l = 0;
    private GpsStatus.Listener m;
    /* access modifiers changed from: private */
    public GpsStatus n;
    private a o;

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object a(ArrayList arrayList) {
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object a(ArrayList arrayList, Proxy proxy) {
        Location location = (Location) arrayList.get(0);
        GeoPoint.a aVar = (GeoPoint.a) new C0035p(new GeoPoint.a(location.getLongitude(), location.getLatitude()), proxy, this.f319a.e.c(), this.f319a.e.b(), this.f319a.e.d()).h();
        if (aVar != null) {
            location.setLongitude(aVar.f232a);
            location.setLatitude(aVar.b);
        }
        return location;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj) {
        Location location = (Location) obj;
        if (location != null) {
            this.i = location;
            if (this.f != null) {
                this.f.onLocationChanged(this.i);
            }
        }
    }

    public final boolean a(LocationListener locationListener) {
        j();
        this.f = locationListener;
        return k();
    }

    public final void e() {
        j();
        this.f = null;
    }

    public final void c() {
        k();
    }

    public final void b() {
        j();
    }

    public final void a() {
        j();
        if (this.m != null) {
            try {
                this.h.removeGpsStatusListener(this.m);
                this.m = null;
            } catch (Exception e) {
            }
        }
        super.a();
    }

    public final Location f() {
        return this.i;
    }

    private void j() {
        if (this.f != null) {
            try {
                this.h.removeUpdates(this);
                if (this.o != null) {
                    try {
                        this.h.removeUpdates(this.o);
                        this.o = null;
                    } catch (Exception e) {
                    }
                }
                this.l = 0;
            } catch (Exception e2) {
            }
        }
    }

    private boolean k() {
        boolean z;
        String str;
        if (this.f != null) {
            try {
                if (this.h.isProviderEnabled(LocationManagerProxy.GPS_PROVIDER)) {
                    str = LocationManagerProxy.GPS_PROVIDER;
                } else {
                    String bestProvider = this.h.getBestProvider(this.g, true);
                    if (bestProvider == null) {
                        bestProvider = this.h.getBestProvider(null, true);
                    }
                    str = bestProvider;
                }
                if (!(str == LocationProviderProxy.AutonaviCellProvider || this.f == null || this.o != null)) {
                    this.o = new a();
                    this.h.requestLocationUpdates(LocationProviderProxy.AutonaviCellProvider, 10000, 5.0f, this.o);
                }
                this.h.requestLocationUpdates(str, 10000, 5.0f, this);
                z = true;
            } catch (Exception e) {
            }
            this.j = z;
            return z;
        }
        z = false;
        this.j = z;
        return z;
    }

    public final void a(Location location) {
        if (this.f != null && location != null) {
            ArrayList arrayList = new ArrayList();
            arrayList.add(location);
            this.e.a(arrayList);
        }
    }

    public final void onLocationChanged(Location location) {
        this.l = System.currentTimeMillis();
        a(location);
    }

    public final void onProviderDisabled(String str) {
        if (this.f != null) {
            this.f.onProviderDisabled(str);
        }
    }

    public final void onProviderEnabled(String str) {
        if (this.f != null) {
            this.f.onProviderEnabled(str);
        }
    }

    public final void onStatusChanged(String str, int i2, Bundle bundle) {
        if (this.f != null) {
            this.f.onStatusChanged(str, i2, bundle);
        }
    }

    public C0034o(Mediator mediator, Context context, String str) {
        super(mediator, context);
        this.g.setAccuracy(1);
        this.g.setAltitudeRequired(false);
        this.g.setBearingRequired(false);
        this.g.setPowerRequirement(2);
        this.h = new LocationManagerProxy(context, str);
    }

    public final void g() {
        this.m = new GpsStatus.Listener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.mapabc.mapapi.o.a(com.mapabc.mapapi.o, boolean):boolean
             arg types: [com.mapabc.mapapi.o, int]
             candidates:
              com.mapabc.mapapi.o.a(com.mapabc.mapapi.o, android.location.GpsStatus):android.location.GpsStatus
              com.mapabc.mapapi.o.a(java.util.ArrayList, java.net.Proxy):java.lang.Object
              com.mapabc.mapapi.aG.a(java.util.ArrayList, java.net.Proxy):V
              com.mapabc.mapapi.o.a(com.mapabc.mapapi.o, boolean):boolean */
            public final void onGpsStatusChanged(int i) {
                int i2;
                GpsStatus unused = C0034o.this.n = C0034o.this.h.getGpsStatus(null);
                if (C0034o.this.j) {
                    switch (i) {
                        case 3:
                        default:
                            return;
                        case 4:
                            int i3 = 0;
                            for (GpsSatellite snr : C0034o.this.n.getSatellites()) {
                                if (snr.getSnr() > 10.0f) {
                                    i2 = i3 + 1;
                                } else {
                                    i2 = i3;
                                }
                                i3 = i2;
                            }
                            if (i3 < 3) {
                                boolean unused2 = C0034o.this.k = false;
                                return;
                            } else {
                                boolean unused3 = C0034o.this.k = true;
                                return;
                            }
                    }
                }
            }
        };
        this.h.addGpsStatusListener(this.m);
    }

    /* access modifiers changed from: protected */
    public final int h() {
        return 1;
    }

    /* access modifiers changed from: protected */
    public final int i() {
        return 1;
    }

    /* renamed from: com.mapabc.mapapi.o$a */
    class a implements LocationListener {
        public a() {
        }

        public final void onLocationChanged(Location location) {
            if (!C0034o.this.k) {
                if (C0034o.this.l == 0 || System.currentTimeMillis() - C0034o.this.l > 300000) {
                    location.setProvider(LocationProviderProxy.AutonaviCellProvider);
                    C0034o.this.a(location);
                }
            }
        }

        public final void onProviderDisabled(String str) {
        }

        public final void onProviderEnabled(String str) {
        }

        public final void onStatusChanged(String str, int i, Bundle bundle) {
        }
    }
}
