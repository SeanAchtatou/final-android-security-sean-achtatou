package com.mapabc.mapapi;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;

final class Tile {
    private static Paint e = null;
    private static Bitmap f = null;
    private static int g = Color.rgb(222, 215, 214);

    /* renamed from: a  reason: collision with root package name */
    public TileCoordinate f284a;
    public Bitmap b;
    public byte[] c;
    private boolean d;

    public final /* bridge */ /* synthetic */ Object clone() throws CloneNotSupportedException {
        return new Tile(this);
    }

    public Tile(TileCoordinate tileCoordinate) {
        this(tileCoordinate, false);
    }

    public Tile(TileCoordinate tileCoordinate, boolean z) {
        this.d = false;
        this.c = null;
        this.f284a = tileCoordinate;
        this.b = e();
        this.d = z;
    }

    public static int a() {
        return g;
    }

    public static Paint b() {
        if (e == null) {
            Paint paint = new Paint();
            e = paint;
            paint.setColor(-7829368);
            e.setAlpha(90);
            e.setPathEffect(new DashPathEffect(new float[]{2.0f, 2.5f}, 1.0f));
        }
        return e;
    }

    private static Bitmap e() {
        if (f == null) {
            AnonymousClass1 r0 = new C0015ai() {
                public final void a(Canvas canvas) {
                    Paint b = Tile.b();
                    canvas.drawColor(Tile.a());
                    int i = 0;
                    while (true) {
                        int i2 = i;
                        if (i2 < 107) {
                            canvas.drawLine((float) i2, 0.0f, (float) i2, 128.0f, b);
                            canvas.drawLine(0.0f, (float) i2, 128.0f, (float) i2, b);
                            i = i2 + 21;
                        } else {
                            return;
                        }
                    }
                }
            };
            C0041v vVar = new C0041v(Bitmap.Config.ARGB_4444);
            vVar.a(128, 128);
            vVar.a(r0);
            f = vVar.a();
        }
        return f;
    }

    private Tile(Tile tile) {
        this.d = false;
        this.c = null;
        this.f284a = tile.f284a;
        this.b = tile.b;
        this.d = tile.d;
    }

    public final void a(byte[] bArr, int i) {
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeByteArray(bArr, 0, i);
        } catch (Exception e2) {
        }
        if (bitmap != null) {
            this.b = bitmap;
        }
    }

    public final void a(byte[] bArr) {
        a(bArr, bArr.length);
    }

    public static void a(Tile tile, Tile tile2) {
        tile2.f284a = tile.f284a;
        tile2.b = tile.b;
        tile2.d = tile.d;
    }

    public final boolean c() {
        return this.d;
    }

    public final void a(Canvas canvas, MapView mapView) {
        Point a2 = a(mapView);
        canvas.drawBitmap(this.b, (float) a2.x, (float) a2.y, (Paint) null);
    }

    public final void b(Canvas canvas, MapView mapView) {
        Point a2 = a(mapView);
        Rect rect = new Rect(a2.x, a2.y, a2.x + 128, a2.y + 128);
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(-7829368);
        canvas.drawRect(rect, paint);
    }

    private Point a(MapView mapView) {
        return mapView.getProjection().toPixels(new GeoPoint((this.f284a.Y << 7) << (18 - this.f284a.Zoom), (this.f284a.X << 7) << (18 - this.f284a.Zoom), true), null);
    }

    public final boolean d() {
        return this.b == e();
    }

    public static class TileCoordinate {

        /* renamed from: a  reason: collision with root package name */
        private static final int[] f285a = {1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288, 1048576};
        public final int X;
        public final int Y;
        public final int Zoom;

        public TileCoordinate(int i, int i2, int i3) {
            this.X = i;
            this.Y = i2;
            this.Zoom = i3;
        }

        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (obj.getClass() != getClass()) {
                return false;
            }
            TileCoordinate tileCoordinate = (TileCoordinate) obj;
            return this.X == tileCoordinate.X && this.Y == tileCoordinate.Y && this.Zoom == tileCoordinate.Zoom;
        }

        public int hashCode() {
            return (this.X * 7) + (this.Y * 11) + (this.Zoom * 13);
        }

        public String toString() {
            int i;
            int i2;
            String str = "";
            for (int i3 = this.Zoom - 1; i3 >= 0; i3--) {
                if ((f285a[i3] & this.X) != 0) {
                    i = 1;
                } else {
                    i = 0;
                }
                if ((f285a[i3] & this.Y) != 0) {
                    i2 = 2;
                } else {
                    i2 = 0;
                }
                str = str + (i + i2);
            }
            return str;
        }
    }

    public static TileCoordinate a(GeoPoint geoPoint, int i) {
        return new TileCoordinate(geoPoint.getLonInZoom(i) / 128, geoPoint.getLatInZoom(i) / 128, i);
    }

    /*  JADX ERROR: JadxOverflowException in pass: LoopRegionVisitor
        jadx.core.utils.exceptions.JadxOverflowException: LoopRegionVisitor.assignOnlyInLoop endless recursion
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    public static com.mapabc.mapapi.Tile.TileCoordinate[] a(com.mapabc.mapapi.GeoPoint r11, int r12, int r13, int r14) {
        /*
            int r0 = r11.getLonInZoom(r12)
            int r11 = r11.getLatInZoom(r12)
            int r1 = r13 / 2
            int r1 = r0 - r1
            int r1 = r1 / 128
            int r2 = r14 / 2
            int r2 = r11 - r2
            int r2 = r2 / 128
            int r13 = r13 / 2
            int r13 = r13 + r0
            int r13 = r13 / 128
            int r14 = r14 / 2
            int r14 = r14 + r11
            int r14 = r14 / 128
            int r3 = r13 - r1
            int r3 = r3 + 1
            int r4 = r14 - r2
            int r4 = r4 + 1
            int r3 = r3 * r4
            com.mapabc.mapapi.Tile$TileCoordinate[] r3 = new com.mapabc.mapapi.Tile.TileCoordinate[r3]
            r4 = 0
            com.mapabc.mapapi.Tile$TileCoordinate r5 = new com.mapabc.mapapi.Tile$TileCoordinate
            int r0 = r0 / 128
            int r11 = r11 / 128
            r5.<init>(r0, r11, r12)
            r3[r4] = r5
            r11 = 1
            r0 = 1
            r4 = 0
            r4 = r3[r4]
            int r4 = r4.X
            r5 = 0
            r5 = r3[r5]
            int r5 = r5.Y
            r10 = r0
            r0 = r11
            r11 = r10
        L_0x0044:
            int r6 = r3.length
            if (r11 >= r6) goto L_0x00c4
            int r6 = -r0
            r10 = r6
            r6 = r11
            r11 = r10
        L_0x004b:
            if (r11 > r0) goto L_0x0082
            int r7 = r4 + r11
            boolean r7 = a(r7, r1, r13)
            if (r7 == 0) goto L_0x007f
            int r7 = r5 - r0
            boolean r7 = a(r7, r2, r14)
            if (r7 == 0) goto L_0x006a
            com.mapabc.mapapi.Tile$TileCoordinate r7 = new com.mapabc.mapapi.Tile$TileCoordinate
            int r8 = r4 + r11
            int r9 = r5 - r0
            r7.<init>(r8, r9, r12)
            r3[r6] = r7
            int r6 = r6 + 1
        L_0x006a:
            int r7 = r5 + r0
            boolean r7 = a(r7, r2, r14)
            if (r7 == 0) goto L_0x007f
            com.mapabc.mapapi.Tile$TileCoordinate r7 = new com.mapabc.mapapi.Tile$TileCoordinate
            int r8 = r4 + r11
            int r9 = r5 + r0
            r7.<init>(r8, r9, r12)
            r3[r6] = r7
            int r6 = r6 + 1
        L_0x007f:
            int r11 = r11 + 1
            goto L_0x004b
        L_0x0082:
            int r11 = -r0
            int r11 = r11 + 1
        L_0x0085:
            r7 = 1
            int r7 = r0 - r7
            if (r11 > r7) goto L_0x00bf
            int r7 = r5 + r11
            boolean r7 = a(r7, r2, r14)
            if (r7 == 0) goto L_0x00bc
            int r7 = r4 - r0
            boolean r7 = a(r7, r1, r13)
            if (r7 == 0) goto L_0x00a7
            com.mapabc.mapapi.Tile$TileCoordinate r7 = new com.mapabc.mapapi.Tile$TileCoordinate
            int r8 = r4 - r0
            int r9 = r5 + r11
            r7.<init>(r8, r9, r12)
            r3[r6] = r7
            int r6 = r6 + 1
        L_0x00a7:
            int r7 = r4 + r0
            boolean r7 = a(r7, r1, r13)
            if (r7 == 0) goto L_0x00bc
            com.mapabc.mapapi.Tile$TileCoordinate r7 = new com.mapabc.mapapi.Tile$TileCoordinate
            int r8 = r4 + r0
            int r9 = r5 + r11
            r7.<init>(r8, r9, r12)
            r3[r6] = r7
            int r6 = r6 + 1
        L_0x00bc:
            int r11 = r11 + 1
            goto L_0x0085
        L_0x00bf:
            int r11 = r0 + 1
            r0 = r11
            r11 = r6
            goto L_0x0044
        L_0x00c4:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mapabc.mapapi.Tile.a(com.mapabc.mapapi.GeoPoint, int, int, int):com.mapabc.mapapi.Tile$TileCoordinate[]");
    }

    private static boolean a(int i, int i2, int i3) {
        return i >= i2 && i <= i3;
    }
}
