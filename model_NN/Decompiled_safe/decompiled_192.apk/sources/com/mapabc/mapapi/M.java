package com.mapabc.mapapi;

import android.graphics.drawable.Drawable;
import android.view.View;
import com.mapabc.mapapi.MapView;

final class M extends au {
    private MapView.LayoutParams d;
    private View e;

    public M(RouteOverlay routeOverlay, int i, GeoPoint geoPoint, View view, Drawable drawable, MapView.LayoutParams layoutParams) {
        super(routeOverlay, i, geoPoint);
        this.e = view;
        this.e.setBackgroundDrawable(drawable);
        this.d = layoutParams;
    }

    public final void a(MapView mapView) {
        mapView.addView(this.e, this.d);
    }

    public final void b(MapView mapView) {
        mapView.removeView(this.e);
    }
}
