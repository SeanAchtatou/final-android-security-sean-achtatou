package com.mapabc.mapapi;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.location.Address;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Looper;
import android.text.Html;
import android.text.Spanned;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.Locale;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;

final class W {

    /* renamed from: a  reason: collision with root package name */
    private static String f288a = null;

    W() {
    }

    public static boolean a(String str) {
        return str == null || str.trim().length() == 0;
    }

    public static long a() {
        return System.nanoTime() / 1000000000;
    }

    public static long b() {
        return (System.nanoTime() * 1000) / 1000000000;
    }

    public static int a(int i) {
        return (int) ((1117 * ((long) i)) / 10000);
    }

    public static int b(int i) {
        return (int) ((((long) i) * 1000000) / 111700);
    }

    public static boolean a(long j, long j2, long j3) {
        return j2 - j > j3;
    }

    private static Document a(InputStream inputStream) {
        try {
            return DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inputStream);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Document b(String str) {
        return a(new ByteArrayInputStream(str.getBytes()));
    }

    public static Looper c() {
        if (Looper.myLooper() == null) {
            Looper.prepare();
            Looper.loop();
        }
        return Looper.myLooper();
    }

    public static String a(MapActivity mapActivity) {
        return mapActivity.a().e.c();
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v5, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v14, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v15, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(android.content.Context r6) {
        /*
            r5 = 0
            java.lang.String r0 = com.mapabc.mapapi.W.f288a
            if (r0 != 0) goto L_0x0089
            r0 = 16
            char[] r0 = new char[r0]
            r0 = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70} // fill-array
            java.lang.String r1 = ""
            android.content.pm.PackageManager r2 = r6.getPackageManager()     // Catch:{ NoSuchAlgorithmException -> 0x008e, NameNotFoundException -> 0x008c }
            java.lang.String r3 = r6.getPackageName()     // Catch:{ NoSuchAlgorithmException -> 0x008e, NameNotFoundException -> 0x008c }
            r4 = 64
            android.content.pm.PackageInfo r2 = r2.getPackageInfo(r3, r4)     // Catch:{ NoSuchAlgorithmException -> 0x008e, NameNotFoundException -> 0x008c }
            android.content.pm.Signature[] r2 = r2.signatures     // Catch:{ NoSuchAlgorithmException -> 0x008e, NameNotFoundException -> 0x008c }
            java.lang.String r3 = "MD5"
            java.security.MessageDigest r3 = java.security.MessageDigest.getInstance(r3)     // Catch:{ NoSuchAlgorithmException -> 0x008e, NameNotFoundException -> 0x008c }
            r4 = 0
            r2 = r2[r4]     // Catch:{ NoSuchAlgorithmException -> 0x008e, NameNotFoundException -> 0x008c }
            byte[] r2 = r2.toByteArray()     // Catch:{ NoSuchAlgorithmException -> 0x008e, NameNotFoundException -> 0x008c }
            r3.update(r2)     // Catch:{ NoSuchAlgorithmException -> 0x008e, NameNotFoundException -> 0x008c }
            byte[] r2 = r3.digest()     // Catch:{ NoSuchAlgorithmException -> 0x008e, NameNotFoundException -> 0x008c }
            r3 = r1
            r1 = r5
        L_0x0034:
            int r4 = r2.length     // Catch:{ NoSuchAlgorithmException -> 0x008e, NameNotFoundException -> 0x008c }
            if (r1 >= r4) goto L_0x0087
            byte r4 = r2[r1]     // Catch:{ NoSuchAlgorithmException -> 0x008e, NameNotFoundException -> 0x008c }
            if (r4 >= 0) goto L_0x0084
            byte r4 = r2[r1]     // Catch:{ NoSuchAlgorithmException -> 0x008e, NameNotFoundException -> 0x008c }
            int r4 = r4 + 256
        L_0x003f:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ NoSuchAlgorithmException -> 0x008e, NameNotFoundException -> 0x008c }
            r5.<init>()     // Catch:{ NoSuchAlgorithmException -> 0x008e, NameNotFoundException -> 0x008c }
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ NoSuchAlgorithmException -> 0x008e, NameNotFoundException -> 0x008c }
            int r5 = r4 / 16
            char r5 = r0[r5]     // Catch:{ NoSuchAlgorithmException -> 0x008e, NameNotFoundException -> 0x008c }
            java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ NoSuchAlgorithmException -> 0x008e, NameNotFoundException -> 0x008c }
            java.lang.String r3 = r3.toString()     // Catch:{ NoSuchAlgorithmException -> 0x008e, NameNotFoundException -> 0x008c }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ NoSuchAlgorithmException -> 0x008e, NameNotFoundException -> 0x008c }
            r5.<init>()     // Catch:{ NoSuchAlgorithmException -> 0x008e, NameNotFoundException -> 0x008c }
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ NoSuchAlgorithmException -> 0x008e, NameNotFoundException -> 0x008c }
            int r4 = r4 % 16
            char r4 = r0[r4]     // Catch:{ NoSuchAlgorithmException -> 0x008e, NameNotFoundException -> 0x008c }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ NoSuchAlgorithmException -> 0x008e, NameNotFoundException -> 0x008c }
            java.lang.String r3 = r3.toString()     // Catch:{ NoSuchAlgorithmException -> 0x008e, NameNotFoundException -> 0x008c }
            int r4 = r2.length     // Catch:{ NoSuchAlgorithmException -> 0x008e, NameNotFoundException -> 0x008c }
            r5 = 1
            int r4 = r4 - r5
            if (r1 == r4) goto L_0x0081
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ NoSuchAlgorithmException -> 0x008e, NameNotFoundException -> 0x008c }
            r4.<init>()     // Catch:{ NoSuchAlgorithmException -> 0x008e, NameNotFoundException -> 0x008c }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ NoSuchAlgorithmException -> 0x008e, NameNotFoundException -> 0x008c }
            java.lang.String r4 = ":"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ NoSuchAlgorithmException -> 0x008e, NameNotFoundException -> 0x008c }
            java.lang.String r3 = r3.toString()     // Catch:{ NoSuchAlgorithmException -> 0x008e, NameNotFoundException -> 0x008c }
        L_0x0081:
            int r1 = r1 + 1
            goto L_0x0034
        L_0x0084:
            byte r4 = r2[r1]     // Catch:{ NoSuchAlgorithmException -> 0x008e, NameNotFoundException -> 0x008c }
            goto L_0x003f
        L_0x0087:
            com.mapabc.mapapi.W.f288a = r3     // Catch:{ NoSuchAlgorithmException -> 0x008e, NameNotFoundException -> 0x008c }
        L_0x0089:
            java.lang.String r0 = com.mapabc.mapapi.W.f288a
            return r0
        L_0x008c:
            r0 = move-exception
            goto L_0x0089
        L_0x008e:
            r0 = move-exception
            goto L_0x0089
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mapabc.mapapi.W.a(android.content.Context):java.lang.String");
    }

    public static Proxy b(Context context) {
        String defaultHost;
        int defaultPort;
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null) {
            if (activeNetworkInfo.getType() == 1) {
                defaultHost = android.net.Proxy.getHost(context);
                defaultPort = android.net.Proxy.getPort(context);
            } else {
                defaultHost = android.net.Proxy.getDefaultHost();
                defaultPort = android.net.Proxy.getDefaultPort();
            }
            if (defaultHost != null) {
                return new Proxy(Proxy.Type.HTTP, new InetSocketAddress(defaultHost, defaultPort));
            }
        }
        return null;
    }

    public static int a(double d) {
        return (int) (1000000.0d * d);
    }

    public static double c(int i) {
        return ((double) i) / 1000000.0d;
    }

    public static boolean a(Rect rect, Point point) {
        return new Rect(rect.left, rect.top, rect.right, rect.bottom).contains(point.x, point.y);
    }

    public static Address d() {
        Address address = new Address(Locale.CHINA);
        address.setCountryCode("CN");
        address.setCountryName("中国");
        return address;
    }

    public static String e() {
        String str = "";
        for (int i = 0; i < 3; i++) {
            str = str + "&nbsp;";
        }
        return str;
    }

    public static String f() {
        return "<br />";
    }

    public static Spanned c(String str) {
        if (str == null) {
            return null;
        }
        return Html.fromHtml(str.replace("\n", "<br />"));
    }

    public static String a(String str, String str2) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("<font color=").append(str2).append(">").append(str).append("</font>");
        return stringBuffer.toString();
    }
}
