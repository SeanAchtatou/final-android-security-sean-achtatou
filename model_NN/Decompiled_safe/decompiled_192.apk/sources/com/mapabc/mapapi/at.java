package com.mapabc.mapapi;

import android.os.Handler;

abstract class at {

    /* renamed from: a  reason: collision with root package name */
    protected int f311a;
    protected int b;
    /* access modifiers changed from: private */
    public Handler c = null;
    private int d = 0;
    private boolean e = false;
    private Runnable f = new L(this);

    /* access modifiers changed from: protected */
    public abstract void a();

    /* access modifiers changed from: protected */
    public abstract void b();

    static /* synthetic */ void a(at atVar) {
        atVar.d += atVar.b;
        if (atVar.f311a != -1 && atVar.d > atVar.f311a) {
            atVar.e = false;
        }
    }

    public at(int i, int i2) {
        this.f311a = i;
        this.b = i2;
    }

    public final void e() {
        if (!this.e) {
            this.c = new Handler();
            this.e = true;
            this.d = 0;
        }
        h();
    }

    public final void f() {
        this.e = false;
        this.f.run();
    }

    public final boolean g() {
        return this.e;
    }

    /* access modifiers changed from: protected */
    public final void h() {
        this.c.post(this.f);
    }
}
