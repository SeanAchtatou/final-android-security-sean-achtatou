package com.mapabc.mapapi;

import android.content.Context;
import java.io.IOException;
import java.util.ArrayList;

public class PoiSearch {

    /* renamed from: a  reason: collision with root package name */
    private SearchBound f273a;
    private Query b;
    private Context c;
    private String d;

    public PoiSearch(MapActivity mapActivity, Query query) {
        this(mapActivity, W.a(mapActivity), query);
    }

    public PoiSearch(Context context, String str, Query query) {
        this.c = context;
        this.d = str;
        W.a(this.c);
        setQuery(query);
    }

    public PoiPagedResult searchPOI() throws IOException {
        C0021b bVar = new C0021b(new Z(this.b, this.f273a), W.b(this.c), this.d, W.a(this.c));
        bVar.a(1);
        return PoiPagedResult.a(bVar, (ArrayList) bVar.g());
    }

    public void setQuery(Query query) {
        this.b = query;
    }

    public void setBound(SearchBound searchBound) {
        this.f273a = searchBound;
    }

    public Query getQuery() {
        return this.b;
    }

    public SearchBound getBound() {
        return this.f273a;
    }

    public static class Query {

        /* renamed from: a  reason: collision with root package name */
        private String f274a;
        private String b;
        private String c;

        public Query(String str, String str2) {
            this(str, str2, null);
        }

        public Query(String str, String str2, String str3) {
            this.f274a = str;
            this.b = str2;
            this.c = str3;
            if (!(!W.a(this.f274a) || !W.a(this.b))) {
                throw new IllegalArgumentException("Empty  query and catagory");
            }
        }

        public String getQueryString() {
            return this.f274a;
        }

        public String getCategory() {
            if (this.b == null || this.b.equals("00") || this.b.equals(PoiTypeDef.All)) {
                return "";
            }
            return this.b;
        }

        public String getCity() {
            return this.c;
        }
    }

    public static class SearchBound {

        /* renamed from: a  reason: collision with root package name */
        private GeoPoint f275a;
        private GeoPoint b;

        public SearchBound(GeoPoint geoPoint, int i) {
            a(geoPoint, W.b(i) << 1, W.b(i) << 1);
        }

        public SearchBound(GeoPoint geoPoint, GeoPoint geoPoint2) {
            a(geoPoint, geoPoint2);
        }

        public SearchBound(MapView mapView) {
            Mediator mediator = mapView.getMediator();
            int a2 = mediator.b.a();
            a(mapView.getMapCenter(), mediator.f258a.a(a2) << 1, mediator.f258a.b(a2) << 1);
        }

        private void a(GeoPoint geoPoint, GeoPoint geoPoint2) {
            this.f275a = geoPoint;
            this.b = geoPoint2;
            a(this.f275a.getLatitudeE6());
            a(this.b.getLatitudeE6());
            b(this.f275a.getLongitudeE6());
            b(this.b.getLongitudeE6());
            if (this.f275a.getLatitudeE6() >= this.b.getLatitudeE6() || this.f275a.getLongitudeE6() >= this.b.getLongitudeE6()) {
                throw new IllegalArgumentException("invalid rect ");
            }
        }

        private static void a(int i) {
            if (i >= 65000000 || i <= 1000000) {
                throw new IllegalArgumentException("latitude == " + i);
            }
        }

        private static void b(int i) {
            if (i <= 50000000 || i >= 145000000) {
                throw new IllegalArgumentException("longitude == " + i);
            }
        }

        private void a(GeoPoint geoPoint, int i, int i2) {
            int i3 = i / 2;
            int i4 = i2 / 2;
            int latitudeE6 = geoPoint.getLatitudeE6();
            int longitudeE6 = geoPoint.getLongitudeE6();
            a(new GeoPoint(latitudeE6 - i3, longitudeE6 - i4), new GeoPoint(i3 + latitudeE6, i4 + longitudeE6));
        }

        public GeoPoint getLowerLeft() {
            return this.f275a;
        }

        public GeoPoint getUpperRight() {
            return this.b;
        }

        public GeoPoint getCenter() {
            return new GeoPoint((this.f275a.getLatitudeE6() + this.b.getLatitudeE6()) / 2, (this.f275a.getLongitudeE6() + this.b.getLongitudeE6()) / 2);
        }

        public int getLonSpanInMeter() {
            return W.a(this.b.getLongitudeE6() - this.f275a.getLongitudeE6());
        }

        public int getLatSpanInMeter() {
            return W.a(this.b.getLatitudeE6() - this.f275a.getLatitudeE6());
        }
    }
}
