package com.mapabc.mapapi;

public class PoiItem extends OverlayItem {

    /* renamed from: a  reason: collision with root package name */
    private String f270a;
    private String b;
    private String c;
    private String d;
    private String e = "";

    public PoiItem(String str, GeoPoint geoPoint, String str2, String str3) {
        super(geoPoint, str2, str3);
        this.f270a = str;
    }

    public String getTypeDes() {
        return this.e;
    }

    public void setTypeDes(String str) {
        this.e = str;
    }

    public String getTel() {
        return this.c;
    }

    public void setTel(String str) {
        this.c = str;
    }

    public String getAdCode() {
        return this.d;
    }

    public void setAdCode(String str) {
        this.d = str;
    }

    public String getPoiId() {
        return this.f270a;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        return this.f270a == ((PoiItem) obj).f270a;
    }

    public int hashCode() {
        return this.f270a.hashCode();
    }

    public String toString() {
        return this.mTitle;
    }

    public String getTypeCode() {
        return this.b;
    }

    public void setTypeCode(String str) {
        this.b = str;
    }
}
