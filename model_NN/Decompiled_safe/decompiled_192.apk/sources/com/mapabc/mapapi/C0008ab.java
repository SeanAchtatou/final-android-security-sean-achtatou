package com.mapabc.mapapi;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/* renamed from: com.mapabc.mapapi.ab  reason: case insensitive filesystem */
class C0008ab<T> implements List<T> {

    /* renamed from: a  reason: collision with root package name */
    protected LinkedList<T> f306a = new LinkedList<>();

    C0008ab() {
    }

    public final synchronized void c(T t) {
        add(t);
    }

    public final synchronized T a() {
        T t;
        t = null;
        if (this.f306a.size() != 0) {
            t = this.f306a.removeFirst();
        }
        return t;
    }

    public synchronized void add(int i, T t) {
        this.f306a.add(i, t);
    }

    public synchronized boolean addAll(Collection<? extends T> collection) {
        return this.f306a.addAll(collection);
    }

    public synchronized boolean addAll(int i, Collection<? extends T> collection) {
        return this.f306a.addAll(i, collection);
    }

    public synchronized void clear() {
        this.f306a.clear();
    }

    public synchronized boolean contains(Object obj) {
        return this.f306a.contains(obj);
    }

    public synchronized boolean containsAll(Collection<?> collection) {
        return containsAll(collection);
    }

    public synchronized T get(int i) {
        return this.f306a.get(i);
    }

    public synchronized int indexOf(Object obj) {
        return this.f306a.indexOf(obj);
    }

    public synchronized boolean isEmpty() {
        return this.f306a.isEmpty();
    }

    public synchronized Iterator<T> iterator() {
        return this.f306a.listIterator();
    }

    public synchronized int lastIndexOf(Object obj) {
        return this.f306a.lastIndexOf(obj);
    }

    public synchronized ListIterator<T> listIterator() {
        return this.f306a.listIterator();
    }

    public synchronized ListIterator<T> listIterator(int i) {
        return this.f306a.listIterator(i);
    }

    public synchronized T remove(int i) {
        return this.f306a.remove(i);
    }

    public synchronized boolean remove(Object obj) {
        return this.f306a.remove(obj);
    }

    public synchronized boolean removeAll(Collection<?> collection) {
        return this.f306a.removeAll(collection);
    }

    public synchronized boolean retainAll(Collection<?> collection) {
        return this.f306a.retainAll(collection);
    }

    public synchronized T set(int i, T t) {
        return this.f306a.set(i, t);
    }

    public synchronized int size() {
        return this.f306a.size();
    }

    public synchronized List<T> subList(int i, int i2) {
        return this.f306a.subList(i, i2);
    }

    public synchronized Object[] toArray() {
        return this.f306a.toArray();
    }

    public synchronized <V> V[] toArray(V[] vArr) {
        return this.f306a.toArray(vArr);
    }

    public boolean add(T t) {
        return this.f306a.add(t);
    }
}
