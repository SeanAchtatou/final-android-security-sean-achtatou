package com.mapabc.mapapi;

import com.mapabc.mapapi.PoiSearch;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public final class PoiPagedResult {

    /* renamed from: a  reason: collision with root package name */
    private int f272a;
    private ArrayList<ArrayList<PoiItem>> b;
    private C0021b c;

    static PoiPagedResult a(C0021b bVar, ArrayList<PoiItem> arrayList) {
        return new PoiPagedResult(bVar, arrayList);
    }

    private PoiPagedResult(C0021b bVar, ArrayList<PoiItem> arrayList) {
        this.c = bVar;
        int a2 = (bVar.a() / 25) + 1;
        this.f272a = a2 > 30 ? 30 : a2;
        a(arrayList);
    }

    private void a(ArrayList<PoiItem> arrayList) {
        this.b = new ArrayList<>();
        for (int i = 0; i <= this.f272a; i++) {
            this.b.add(null);
        }
        this.b.set(1, arrayList);
    }

    public final int getPageCount() {
        return this.f272a;
    }

    public final PoiSearch.Query getQuery() {
        return this.c.b();
    }

    public final PoiSearch.SearchBound getBound() {
        return this.c.c();
    }

    private boolean a(int i) {
        return i <= this.f272a && i > 0;
    }

    public final List<PoiItem> getPageLocal(int i) {
        if (a(i)) {
            return this.b.get(i);
        }
        throw new IllegalArgumentException("page out of range");
    }

    public final List<PoiItem> getPage(int i) throws IOException {
        if (!a(i)) {
            throw new IllegalArgumentException("page out of range");
        }
        ArrayList arrayList = (ArrayList) getPageLocal(i);
        if (arrayList != null) {
            return arrayList;
        }
        this.c.a(i);
        ArrayList arrayList2 = (ArrayList) this.c.g();
        this.b.set(i, arrayList2);
        return arrayList2;
    }

    public final List<String> getSearchSuggestions() {
        return this.c.d();
    }
}
