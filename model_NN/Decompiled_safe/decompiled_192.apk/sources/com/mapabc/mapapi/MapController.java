package com.mapabc.mapapi;

import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import com.mapabc.mapapi.MapView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

public final class MapController implements View.OnKeyListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Mediator f247a;
    private boolean b = false;
    private a c = new a();
    private Y d = new Y(this);

    MapController(Mediator mediator) {
        this.f247a = mediator;
    }

    public final boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 0) {
            return false;
        }
        switch (i) {
            case 19:
                scrollBy(0, -128);
                return true;
            case 20:
                scrollBy(0, 128);
                return true;
            case 21:
                scrollBy(-128, 0);
                return true;
            case 22:
                scrollBy(128, 0);
                return true;
            default:
                return false;
        }
    }

    public final void setCenter(GeoPoint geoPoint) {
        this.f247a.b.a(geoPoint);
    }

    public final int setZoom(int i) {
        int a2 = a(i);
        this.f247a.b.a(a2);
        return a2;
    }

    public final void zoomToSpan(int i, int i2) {
        int a2 = this.f247a.b.a();
        int b2 = this.f247a.f258a.b(a2);
        int a3 = this.f247a.f258a.a(a2);
        if (!a(b2, a3, i2, i)) {
            int i3 = a2;
            int i4 = b2;
            int i5 = a3;
            int i6 = i4;
            while (i3 >= 4) {
                i3--;
                i5 <<= 1;
                i6 <<= 1;
                if (a(i6, i5, i2, i)) {
                    break;
                }
                a2--;
            }
        } else {
            int i7 = a2;
            int i8 = b2;
            int i9 = a3;
            int i10 = i8;
            while (i7 <= 18) {
                i7++;
                i9 >>= 1;
                i10 >>= 1;
                if (!a(i10, i9, i2, i)) {
                    break;
                }
                a2++;
            }
        }
        setZoom(a2);
    }

    private static boolean a(int i, int i2, int i3, int i4) {
        return i >= i3 && i2 >= i4;
    }

    public final boolean zoomIn() {
        return a(GlobalStore.getsViewWidth() / 2, GlobalStore.getsViewHeight() / 2, true);
    }

    public final boolean zoomOut() {
        return a(GlobalStore.getsViewWidth() / 2, GlobalStore.getsViewHeight() / 2, false);
    }

    public final boolean zoomInFixing(int i, int i2) {
        return a(i, i2, true);
    }

    public final boolean zoomOutFixing(int i, int i2) {
        return a(i, i2, false);
    }

    public final void animateTo(GeoPoint geoPoint) {
        this.d.a(geoPoint, null, null);
    }

    public final void animateTo(GeoPoint geoPoint, Message message) {
        this.d.a(geoPoint, message, null);
    }

    public final void animateTo(GeoPoint geoPoint, Runnable runnable) {
        this.d.a(geoPoint, null, runnable);
    }

    public final void stopAnimation(boolean z) {
        this.c.a();
        this.d.b();
    }

    public final void scrollBy(int i, int i2) {
        if (this.b) {
            this.b = false;
        } else if (i != 0 || i2 != 0) {
            setCenter(this.f247a.f258a.fromPixels((GlobalStore.getsViewWidth() / 2) + i, (GlobalStore.getsViewHeight() / 2) + i2));
        }
    }

    public final void stopPanning() {
        this.b = true;
    }

    /* access modifiers changed from: package-private */
    public final void a(int i, int i2, int i3, boolean z) {
        this.f247a.f258a.fromPixels(i, i2);
        this.c.a(i, i2, i3, z);
    }

    private boolean a(int i, int i2, boolean z) {
        int a2 = a(z ? this.f247a.b.a() + 1 : this.f247a.b.a() - 1);
        if (a2 == this.f247a.b.a()) {
            return false;
        }
        a(i, i2, a2, z);
        return true;
    }

    private static int a(int i) {
        int i2 = 4;
        if (i >= 4) {
            i2 = i;
        }
        if (i2 > 18) {
            return 18;
        }
        return i2;
    }

    class a implements Animation.AnimationListener {

        /* renamed from: a  reason: collision with root package name */
        private LinkedList<Animation> f248a = new LinkedList<>();
        private ArrayList<View> b = new ArrayList<>();
        private aF c = null;

        a() {
        }

        public final void a() {
            this.f248a.clear();
        }

        public final void a(int i, int i2, int i3, boolean z) {
            if (!z) {
                if (this.c == null) {
                    this.c = new aF(MapController.this.f247a.b.c(), this);
                }
                this.c.c = i3;
                this.c.a(false, (float) i, (float) i2);
                return;
            }
            if (this.c == null) {
                this.c = new aF(MapController.this.f247a.b.c(), this);
            }
            this.c.c = i3;
            this.c.a(true, (float) i, (float) i2);
        }

        public final void onAnimationStart(Animation animation) {
            MapView c2 = MapController.this.f247a.b.c();
            MapView.a zoomMgr = c2.getZoomMgr();
            if (this.b.size() <= 0) {
                int childCount = c2.getChildCount();
                for (int i = 0; i < childCount; i++) {
                    View childAt = c2.getChildAt(i);
                    if (!zoomMgr.a(childAt) && childAt.getVisibility() == 0) {
                        this.b.add(childAt);
                        childAt.setVisibility(4);
                    }
                }
            }
        }

        public final void onAnimationRepeat(Animation animation) {
        }

        public final void onAnimationEnd(Animation animation) {
            MapView c2 = MapController.this.f247a.b.c();
            if (this.f248a.size() == 0) {
                MapView.a zoomMgr = c2.getZoomMgr();
                Iterator<View> it = this.b.iterator();
                while (it.hasNext()) {
                    it.next().setVisibility(0);
                }
                this.b.clear();
                zoomMgr.a(true);
                MapController.this.f247a.d.f();
                return;
            }
            c2.getCanvas().startAnimation(this.f248a.remove());
        }
    }
}
