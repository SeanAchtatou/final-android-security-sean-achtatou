package com.mapabc.mapapi;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.StatFs;

/* renamed from: com.mapabc.mapapi.m  reason: case insensitive filesystem */
abstract class C0032m {
    private static int b = 1;
    private static C0032m c = null;

    /* renamed from: a  reason: collision with root package name */
    protected Context f322a;
    private int d;
    private G e;
    private U f;
    private SQLiteDatabase g = null;

    /* access modifiers changed from: protected */
    public abstract String a();

    /* access modifiers changed from: protected */
    public abstract boolean a(String str);

    /* access modifiers changed from: protected */
    public abstract SQLiteDatabase b(String str);

    public static synchronized C0032m a(Context context, String str) {
        C0032m mVar;
        boolean z;
        SQLiteDatabase sQLiteDatabase;
        boolean z2;
        synchronized (C0032m.class) {
            if (c != null) {
                mVar = c;
            } else {
                try {
                    C0027h hVar = new C0027h(context, b);
                    c = hVar;
                    boolean a2 = hVar.a(str);
                    String a3 = hVar.a();
                    if (!W.a(a3)) {
                        StatFs statFs = new StatFs(a3);
                        z = ((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize()) > 10485760;
                    } else {
                        z = false;
                    }
                    if (z) {
                        sQLiteDatabase = hVar.b(str);
                    } else {
                        sQLiteDatabase = null;
                    }
                    hVar.g = sQLiteDatabase;
                    if (hVar.g == null) {
                        z2 = false;
                    } else {
                        if (a2) {
                            hVar.g.getVersion();
                            hVar.f.d(hVar.g);
                            if (hVar.f.a()) {
                                hVar.g.execSQL(String.format("delete FROM %s WHERE %s < %d;", "tile", "time", Integer.valueOf(b() - 15)));
                                hVar.f.c(hVar.g);
                            }
                            hVar.e.d(hVar.g);
                        } else {
                            hVar.f.a(hVar.g);
                            hVar.e.a(hVar.g);
                        }
                        hVar.g.setVersion(hVar.d);
                        z2 = true;
                    }
                    if (!z2) {
                        c = null;
                    }
                } catch (Exception e2) {
                    c = null;
                }
                mVar = c;
            }
        }
        return mVar;
    }

    public static int b() {
        return (int) ((System.currentTimeMillis() / 1000) / 86400);
    }

    protected C0032m(Context context, int i) {
        this.f322a = context;
        this.d = i;
        this.f = new U();
        this.e = new G();
    }
}
