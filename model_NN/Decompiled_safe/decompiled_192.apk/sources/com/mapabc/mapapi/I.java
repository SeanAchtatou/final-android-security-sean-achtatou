package com.mapabc.mapapi;

import android.graphics.Canvas;
import android.graphics.Point;
import android.view.MotionEvent;

class I {

    /* renamed from: a  reason: collision with root package name */
    protected RouteOverlay f239a;

    public I(RouteOverlay routeOverlay) {
        this.f239a = routeOverlay;
    }

    public void a(Canvas canvas, MapView mapView, boolean z) {
    }

    protected static Point a(MapView mapView, GeoPoint geoPoint) {
        return mapView.getProjection().toPixels(geoPoint, null);
    }

    protected static GeoPoint a(MapView mapView, Point point) {
        return mapView.getProjection().fromPixels(point.x, point.y);
    }

    public void a(MapView mapView) {
    }

    public void b(MapView mapView) {
    }

    public boolean a(MotionEvent motionEvent, MapView mapView) {
        return false;
    }
}
