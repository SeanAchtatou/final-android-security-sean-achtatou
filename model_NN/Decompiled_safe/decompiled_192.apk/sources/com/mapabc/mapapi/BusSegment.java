package com.mapabc.mapapi;

public class BusSegment extends Segment {
    protected String mFirstStation;
    protected String mLastStation;
    protected String mLine;
    protected String[] mPassStopName;
    protected GeoPoint[] mPassStopPos;

    public int getStopNumber() {
        return this.mPassStopPos.length;
    }

    public String getLineName() {
        return this.mLine;
    }

    public String getFirstStationName() {
        return this.mFirstStation;
    }

    public String getLastStationName() {
        return this.mLastStation;
    }

    public String getOnStationName() {
        return this.mPassStopName[0];
    }

    public String getOffStationName() {
        return this.mPassStopName[this.mPassStopName.length - 1];
    }
}
