package com.mapabc.mapapi;

import android.content.Context;
import android.location.Address;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public final class Geocoder {
    public static final String Cross = "Cross";
    public static final String POI = "POI";
    public static final String Street_Road = "StreetAndRoad";

    /* renamed from: a  reason: collision with root package name */
    private String f234a;
    private String b;
    private Context c;

    public Geocoder(MapActivity mapActivity) {
        a(mapActivity, W.a(mapActivity), W.a((Context) mapActivity));
    }

    public Geocoder(Context context, String str) {
        a(context, str, W.a(context));
    }

    private void a(Context context, String str, String str2) {
        this.c = context;
        this.f234a = str;
        this.b = str2;
    }

    public final List<Address> getFromLocation(double d, double d2, int i) throws IOException {
        return a(d, d2, i, false);
    }

    public final List<Address> getFromRawGpsLocation(double d, double d2, int i) throws IOException {
        return a(d, d2, i, true);
    }

    private List<Address> a(double d, double d2, int i, boolean z) throws IOException {
        if (d < W.c(1000000) || d > W.c(65000000)) {
            throw new IllegalArgumentException("latitude == " + d);
        } else if (d2 < W.c(50000000) || d2 > W.c(145000000)) {
            throw new IllegalArgumentException("longitude == " + d2);
        } else if (i <= 0) {
            return new ArrayList();
        } else {
            return (List) new ax(new V(d2, d, i, z), W.b(this.c), this.f234a, this.b).g();
        }
    }

    public final List<Address> getFromLocationName(String str, int i, double d, double d2, double d3, double d4) throws IOException {
        if (str == null) {
            throw new IllegalArgumentException("locationName == null");
        } else if (d < W.c(1000000) || d > W.c(65000000)) {
            throw new IllegalArgumentException("lowerLeftLatitude == " + d);
        } else if (d2 < W.c(50000000) || d2 > W.c(145000000)) {
            throw new IllegalArgumentException("lowerLeftLongitude == " + d2);
        } else if (d3 < W.c(1000000) || d3 > W.c(65000000)) {
            throw new IllegalArgumentException("upperRightLatitude == " + d3);
        } else if (d4 < W.c(50000000) || d4 > W.c(145000000)) {
            throw new IllegalArgumentException("upperRightLongitude == " + d4);
        } else if (i <= 0) {
            return new ArrayList();
        } else {
            return a((List) new aJ(new F(str), W.b(this.c), this.f234a, this.b).g(), d, d2, d3, d4, i);
        }
    }

    private static List<Address> a(List<Address> list, double d, double d2, double d3, double d4, int i) {
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (Address next : list) {
            double longitude = next.getLongitude();
            double latitude = next.getLatitude();
            if (longitude <= d4 && longitude >= d2 && latitude <= d3 && latitude >= d && arrayList.size() < i) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    public final List<Address> getFromLocationName(String str, int i) throws IOException {
        return getFromLocationName(str, i, W.c(1000000), W.c(50000000), W.c(65000000), W.c(145000000));
    }
}
