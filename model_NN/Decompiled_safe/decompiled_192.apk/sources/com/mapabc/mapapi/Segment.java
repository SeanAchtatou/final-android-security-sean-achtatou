package com.mapabc.mapapi;

public class Segment {

    /* renamed from: a  reason: collision with root package name */
    private GeoPoint f283a = null;
    private GeoPoint b = null;
    protected int mLength;
    protected Route mRoute;
    protected GeoPoint[] mShapes;

    private void a() {
        int i = Integer.MAX_VALUE;
        int i2 = Integer.MIN_VALUE;
        int i3 = Integer.MIN_VALUE;
        int i4 = Integer.MAX_VALUE;
        for (GeoPoint geoPoint : this.mShapes) {
            int longitudeE6 = geoPoint.getLongitudeE6();
            int latitudeE6 = geoPoint.getLatitudeE6();
            if (longitudeE6 > i3) {
                i3 = longitudeE6;
            }
            if (longitudeE6 < i4) {
                i4 = longitudeE6;
            }
            if (latitudeE6 > i2) {
                i2 = latitudeE6;
            }
            if (latitudeE6 < i) {
                i = latitudeE6;
            }
        }
        this.f283a = new GeoPoint(i, i4);
        this.b = new GeoPoint(i2, i3);
    }

    public GeoPoint getLowerLeftPoint() {
        if (this.f283a == null) {
            a();
        }
        return this.f283a;
    }

    public GeoPoint getUpperRightPoint() {
        if (this.b == null) {
            a();
        }
        return this.b;
    }

    public int getLength() {
        return this.mLength;
    }

    private int b() {
        int segmentIndex = this.mRoute.getSegmentIndex(this);
        if (segmentIndex >= 0) {
            return segmentIndex;
        }
        throw new IllegalArgumentException("this segment is not in the route !");
    }

    public Segment getPrev() {
        int b2 = b();
        if (b2 == 0) {
            return null;
        }
        return this.mRoute.getStep(b2 - 1);
    }

    public Segment getNext() {
        int b2 = b();
        if (b2 == this.mRoute.getStepCount() - 1) {
            return null;
        }
        return this.mRoute.getStep(b2 + 1);
    }

    public GeoPoint getFirstPoint() {
        return this.mShapes[0];
    }

    public GeoPoint getLastPoint() {
        return this.mShapes[this.mShapes.length - 1];
    }
}
