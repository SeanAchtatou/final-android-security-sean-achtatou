package com.mapabc.mapapi;

public class DriveWalkSegment extends Segment {
    protected int mActionCode;
    protected String mActionDes;
    protected String mRoadName;

    public String getActionDescription() {
        return this.mActionDes;
    }

    public int getActionCode() {
        return this.mActionCode;
    }

    public String getRoadName() {
        return this.mRoadName;
    }
}
