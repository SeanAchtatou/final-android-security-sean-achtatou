package com.mapabc.mapapi;

import android.content.Context;
import com.mapabc.mapapi.Tile;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.Iterator;

final class aq extends aG<Tile.TileCoordinate, ArrayList<Tile>> implements C0017ak {
    private boolean f = false;
    private E<Tile.TileCoordinate, Tile> g;
    private E<Tile.TileCoordinate, Tile> h;
    private C0038s i = new C0038s();
    private C0038s j = new C0038s();

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object a(ArrayList arrayList, Proxy proxy) {
        C0011ae aeVar = new C0011ae(arrayList, proxy, this.f319a.e.c(), this.f319a.e.b(), this.f319a.e.d(), this.f);
        if (arrayList != null && arrayList.size() > 0) {
            if (this.f) {
                this.j.b(arrayList);
            } else {
                this.i.b(arrayList);
            }
        }
        return (ArrayList) aeVar.h();
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj) {
        ArrayList arrayList = (ArrayList) obj;
        if (arrayList != null && arrayList.size() > 0) {
            if (((Tile) arrayList.get(0)).c()) {
                this.j.a(arrayList);
            } else {
                this.i.a(arrayList);
            }
        }
        C0008ab<Tile> a2 = this.f319a.d.a();
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            Tile tile = (Tile) it.next();
            if (!a(tile)) {
                Iterator<Tile> it2 = a2.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    Tile next = it2.next();
                    if (next.f284a.equals(tile.f284a) && tile.c() == this.f) {
                        Tile.a(tile, next);
                        this.f319a.d.e();
                        break;
                    }
                }
                Tile.TileCoordinate tileCoordinate = tile.f284a;
                if (tile.c()) {
                    this.h.a(tileCoordinate, tile);
                } else {
                    this.g.a(tileCoordinate, tile);
                }
                int addCachData = this.f319a.e.g().addCachData(tile.c);
                if (addCachData != -1) {
                    ClsIndexItem clsIndexItem = new ClsIndexItem();
                    clsIndexItem.strKey = this.f319a.e.g().getUniqueStringKey(tile.f284a.X, tile.f284a.Y, tile.f284a.Zoom);
                    clsIndexItem.lIndex = this.f319a.e.g().getUniqueLongKey(tile.f284a.X, tile.f284a.Y, tile.f284a.Zoom);
                    clsIndexItem.iBeginByteLocation = (long) (ClsCachFileManager.iTotalByteCount - addCachData);
                    clsIndexItem.iByteLength = addCachData;
                    this.f319a.e.g().addIndex(clsIndexItem);
                }
            }
        }
    }

    public final void b(boolean z) {
        this.f = z;
        a(false);
    }

    public final boolean e() {
        return this.f;
    }

    public final void c() {
        this.f319a.e.f().a();
    }

    public final void a() {
        super.a();
        this.g.b();
        this.h.b();
        this.i.clear();
        this.j.clear();
        this.f319a.e.f().c();
    }

    public aq(Mediator mediator, Context context) {
        super(mediator, context);
        mediator.b.a(this);
        this.g = new E<>(1200, true, (byte) 0);
        this.h = new E<>(300, false, (byte) 0);
    }

    private void b(ArrayList<Tile.TileCoordinate> arrayList) {
        if (arrayList.size() != 0) {
            this.e.a(arrayList);
        }
    }

    public final void b() {
        this.h.b();
        this.g.b();
        this.i.clear();
        this.j.clear();
    }

    public final void d() {
        if (this.f && this.f319a.b.a() > 9) {
            long a2 = W.a();
            ArrayList arrayList = new ArrayList();
            Iterator<Tile> it = this.f319a.d.a().iterator();
            while (it.hasNext()) {
                Tile next = it.next();
                long b = this.h.b(next.f284a);
                getClass();
                if (a2 - b > 300) {
                    arrayList.add(next.f284a);
                }
                if (b > 0) {
                    this.h.c(next.f284a);
                }
            }
            b(arrayList);
        }
        this.g.a();
        this.h.a();
        this.i.clear();
        this.j.clear();
        a(false);
    }

    public final void a(boolean z) {
        Tile a2;
        Tile.TileCoordinate[] a3 = Tile.a(this.f319a.b.b(), this.f319a.b.a(), GlobalStore.getsViewWidth(), GlobalStore.getsViewHeight());
        C0008ab<Tile> a4 = this.f319a.d.a();
        if (z) {
            for (Tile.TileCoordinate tileCoordinate : a3) {
                Tile tile = null;
                if (this.f) {
                    tile = this.h.a(tileCoordinate);
                }
                if (tile == null) {
                    tile = this.g.a(tileCoordinate);
                }
                if (tile == null || tile.b == null) {
                    tile = new Tile(tileCoordinate);
                }
                a4.add(tile);
            }
        } else {
            a4.clear();
            ArrayList arrayList = new ArrayList();
            for (Tile.TileCoordinate tileCoordinate2 : a3) {
                if (this.f) {
                    a2 = this.h.a(tileCoordinate2);
                    if (a2 == null) {
                        a2 = this.g.a(tileCoordinate2);
                        if (a(a2)) {
                            a2 = new Tile(tileCoordinate2);
                        }
                        if (!this.j.contains(tileCoordinate2)) {
                            arrayList.add(tileCoordinate2);
                        }
                    }
                } else {
                    a2 = this.g.a(tileCoordinate2);
                    if (a(a2)) {
                        a2 = new Tile(tileCoordinate2);
                        arrayList.add(tileCoordinate2);
                    }
                }
                a4.add(a2);
            }
            this.f319a.d.f();
            b(arrayList);
        }
        this.f319a.d.f();
    }

    private static boolean a(Tile tile) {
        return tile == null || tile.b == null || tile.d();
    }

    /* access modifiers changed from: protected */
    public final int h() {
        return 2;
    }

    /* access modifiers changed from: protected */
    public final int i() {
        return 3;
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public ArrayList<Tile> a(ArrayList<Tile.TileCoordinate> arrayList) {
        if (arrayList == null) {
            return null;
        }
        int size = arrayList.size();
        if (size == 0) {
            return null;
        }
        int i2 = 0;
        C0008ab<Tile> abVar = null;
        int i3 = size;
        while (i2 < i3) {
            Tile.TileCoordinate tileCoordinate = arrayList.get(i2);
            Tile tileFromCach = this.f319a.e.g().getTileFromCach(tileCoordinate);
            if (!a(tileFromCach)) {
                this.g.a(tileCoordinate, tileFromCach);
                arrayList.remove(i2);
                i3--;
                i2--;
                if (abVar == null) {
                    if (this.f319a == null || this.f319a.d == null) {
                        return null;
                    }
                    abVar = this.f319a.d.a();
                }
                if (abVar != null) {
                    int size2 = abVar.size();
                    int i4 = 0;
                    while (true) {
                        if (i4 >= size2) {
                            break;
                        }
                        try {
                            Tile tile = (Tile) abVar.get(i4);
                            if (tile != null) {
                                if (tile.f284a.equals(tileCoordinate) && tileFromCach.c() == this.f) {
                                    Tile.a(tileFromCach, tile);
                                    this.f319a.d.e();
                                    break;
                                }
                                i4++;
                            } else {
                                break;
                            }
                        } catch (Exception e) {
                        }
                    }
                }
            }
            abVar = abVar;
            i3 = i3;
            i2++;
        }
        return null;
    }
}
