package com.mapabc.mapapi;

import com.mapabc.mapapi.C0045z;
import java.net.Proxy;

/* renamed from: com.mapabc.mapapi.k  reason: case insensitive filesystem */
final class C0030k<T, V> extends C0045z<T, V> {
    private av<T, V> e;

    public C0030k(av<T, V> avVar, Proxy proxy, String str, String str2, String str3) {
        super(proxy, str, str2, str3);
        this.e = avVar;
    }

    public final void a(Object obj) {
        C0045z.a aVar = new C0045z.a(obj);
        aVar.setPriority(1);
        aVar.start();
    }

    /* access modifiers changed from: protected */
    public final void b(V v) {
        this.e.a(v);
    }

    /* access modifiers changed from: protected */
    public final V c(Object obj) {
        return this.e.a(obj, this.d, this.f330a, this.b, this.c).h();
    }
}
