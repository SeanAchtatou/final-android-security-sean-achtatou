package com.mapabc.mapapi;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.util.DisplayMetrics;
import java.io.InputStream;

final class ConfigableConst {

    /* renamed from: a  reason: collision with root package name */
    public static int f224a = 4097;
    public static int b = 4098;
    public static int c = 10000;
    public static final Bitmap[] d = new Bitmap[f.length];
    public static String e = "Android_MapSDKV1.3.0_1.1.0-B01";
    private static final String[] f = {"nomap.png", "beta.png", "poi_1.png", "compass_bg.png", "compass_pointer.png", "loc1.png", "loc2.png", "zoom_in_true_HVGA.9.png", "zoom_out_true_HVGA.9.png", "zoom_in_disabled_HVGA.9.png", "zoom_out_disabled_HVGA.9.png"};
    private static final String[] g = {"nomap.png", "beta.png", "poi_1_WVGA.png", "compass_bg.png", "compass_pointer.png", "loc1.png", "loc2.png", "zoom_in_true_WVGA.9.png", "zoom_out_true_WVGA.9.png", "zoom_in_disabled_WVGA.9.png", "zoom_out_disabled_WVGA.9.png"};
    private static final String[] h = {"nomap.png", "beta.png", "poi_1_QVGA.png", "compass_bg__QVGA.png", "compass_pointer_QVGA.png", "loc1_QVGA.png", "loc2_QVGA.png", "zoom_in_true_QVGA.9.png", "zoom_out_true_QVGA.9.png", "zoom_in_disabled_QVGA.9.png", "zoom_out_disabled_QVGA.9.png"};

    public enum ENUM_ID {
        enomap,
        ewatermark,
        emarker,
        ecompassback,
        ecommpasspoint,
        eloc1,
        eloc2,
        ezoomin,
        ezoomout,
        ezoomindisable,
        ezoomoutdisable
    }

    ConfigableConst() {
    }

    public static void a(Context context) {
        Bitmap b2;
        for (ENUM_ID enum_id : ENUM_ID.values()) {
            Bitmap[] bitmapArr = d;
            int ordinal = enum_id.ordinal();
            int ordinal2 = enum_id.ordinal();
            if (context == null) {
                b2 = null;
            } else {
                new DisplayMetrics();
                DisplayMetrics displayMetrics = context.getApplicationContext().getResources().getDisplayMetrics();
                long j = (long) (displayMetrics.heightPixels * displayMetrics.widthPixels);
                b2 = j > 153600 ? b(context, g[ordinal2]) : j < 153600 ? b(context, h[ordinal2]) : b(context, f[ordinal2]);
            }
            bitmapArr[ordinal] = b2;
        }
    }

    private static Bitmap b(Context context, String str) {
        Bitmap bitmap = null;
        try {
            InputStream open = context.getAssets().open(str);
            bitmap = BitmapFactory.decodeStream(open);
            open.close();
            return bitmap;
        } catch (Exception e2) {
            e2.printStackTrace();
            return bitmap;
        }
    }

    public static Drawable a(Context context, String str) {
        BitmapDrawable bitmapDrawable = new BitmapDrawable(b(context, str));
        bitmapDrawable.setBounds(0, 0, bitmapDrawable.getIntrinsicWidth(), bitmapDrawable.getIntrinsicHeight());
        return bitmapDrawable;
    }

    public static NinePatchDrawable a(Context context, String str, byte[] bArr, Rect rect) {
        return new NinePatchDrawable(b(context, str), bArr, rect, null);
    }
}
