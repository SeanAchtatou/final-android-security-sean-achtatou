package com.mapabc.mapapi;

import android.location.Address;
import java.lang.reflect.Method;
import java.net.Proxy;
import java.util.ArrayList;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

final class aJ extends P<F, Address> {

    /* renamed from: a  reason: collision with root package name */
    private int f302a = 0;

    public aJ(F f, Proxy proxy, String str, String str2) {
        super(f, proxy, str, str2);
        this.f302a = f.b;
    }

    /* access modifiers changed from: protected */
    public final String[] e() {
        return new String[]{a("address", ((F) this.d).f229a), a("start", "1"), a("count", String.format("%d", Integer.valueOf(((F) this.d).b)))};
    }

    /* access modifiers changed from: protected */
    public final int f() {
        return 16;
    }

    /* access modifiers changed from: protected */
    public final void a(Node node, ArrayList<Address> arrayList) {
        if (node.getNodeType() == 1 && node.getNodeName().equals("list")) {
            NodeList childNodes = node.getChildNodes();
            int min = Math.min(this.f302a, childNodes.getLength());
            for (int i = 0; i < min; i++) {
                Node item = childNodes.item(i);
                if (item.getNodeName().equals("poi") && item.hasChildNodes()) {
                    Address d = W.d();
                    NodeList childNodes2 = item.getChildNodes();
                    for (int i2 = 0; i2 < childNodes2.getLength(); i2++) {
                        Node item2 = childNodes2.item(i2);
                        String nodeName = item2.getNodeName();
                        String a2 = a(item2);
                        if (nodeName.equals("name")) {
                            d.setAddressLine(0, a2);
                        } else if (nodeName.equals("province")) {
                            d.setAdminArea(a2);
                        } else if (nodeName.equals("city")) {
                            d.setLocality(a2);
                        } else if (nodeName.equals("district")) {
                            try {
                                Method method = d.getClass().getMethod("setSubLocality", String.class);
                                if (method != null) {
                                    method.invoke(d, a2);
                                }
                            } catch (Exception e) {
                            }
                        } else if (nodeName.equals("address")) {
                            d.setFeatureName(a2);
                        } else if (nodeName.equals("x")) {
                            d.setLongitude(Double.parseDouble(a2));
                        } else if (nodeName.equals("y")) {
                            d.setLatitude(Double.parseDouble(a2));
                        }
                    }
                    arrayList.add(d);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(ArrayList<Address> arrayList) {
    }
}
