package com.mapabc.mapapi;

import android.location.Criteria;
import android.location.LocationManager;
import android.location.LocationProvider;

public class LocationProviderProxy {
    public static final int AVAILABLE = 2;
    public static final String AutonaviCellProvider = "AutonaviCellLocationProvider";
    public static final int OUT_OF_SERVICE = 0;
    public static final int TEMPORARILY_UNAVAILABLE = 1;

    /* renamed from: a  reason: collision with root package name */
    private LocationManager f245a;
    private String b;

    static LocationProviderProxy a(LocationManager locationManager, String str) {
        if (str == AutonaviCellProvider) {
            return new as(locationManager, str);
        }
        if (locationManager.getProvider(str) != null) {
            return new LocationProviderProxy(locationManager, str);
        }
        return null;
    }

    protected LocationProviderProxy(LocationManager locationManager, String str) {
        this.f245a = locationManager;
        this.b = str;
    }

    private LocationProvider a() {
        return this.f245a.getProvider(this.b);
    }

    public int getAccuracy() {
        return a().getAccuracy();
    }

    public String getName() {
        return a().getName();
    }

    public int getPowerRequirement() {
        return a().getPowerRequirement();
    }

    public boolean hasMonetaryCost() {
        return a().hasMonetaryCost();
    }

    public boolean meetsCriteria(Criteria criteria) {
        return a().meetsCriteria(criteria);
    }

    public boolean requiresCell() {
        return a().requiresCell();
    }

    public boolean requiresNetwork() {
        return a().requiresNetwork();
    }

    public boolean requiresSatellite() {
        return a().requiresNetwork();
    }

    public boolean supportsAltitude() {
        return a().supportsAltitude();
    }

    public boolean supportsBearing() {
        return a().supportsBearing();
    }

    public boolean supportsSpeed() {
        return a().supportsSpeed();
    }
}
