package com.mapabc.mapapi;

import android.graphics.Bitmap;
import android.graphics.Canvas;

/* renamed from: com.mapabc.mapapi.v  reason: case insensitive filesystem */
final class C0041v {

    /* renamed from: a  reason: collision with root package name */
    private Bitmap f328a = null;
    private Canvas b = null;
    private Bitmap.Config c;

    public C0041v(Bitmap.Config config) {
        this.c = config;
    }

    public final void a(int i, int i2) {
        if (this.f328a != null) {
            this.f328a.recycle();
        }
        this.f328a = null;
        this.b = null;
        this.f328a = Bitmap.createBitmap(i, i2, this.c);
        this.b = new Canvas(this.f328a);
    }

    public final void a(C0015ai aiVar) {
        this.b.save(1);
        aiVar.a(this.b);
        this.b.restore();
    }

    public final Bitmap a() {
        return this.f328a;
    }
}
