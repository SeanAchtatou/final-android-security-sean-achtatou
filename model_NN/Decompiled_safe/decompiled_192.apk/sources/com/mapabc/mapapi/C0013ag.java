package com.mapabc.mapapi;

import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import com.mapabc.mapapi.MapView;

/* renamed from: com.mapabc.mapapi.ag  reason: case insensitive filesystem */
class C0013ag implements GestureDetector.OnGestureListener {

    /* renamed from: a  reason: collision with root package name */
    static C0013ag f309a = null;
    private static Drawable c = null;
    protected MapView b;
    private View d;
    private GeoPoint e;
    private long f = -1;
    private MapView.LayoutParams g;

    public C0013ag(MapView mapView, View view, GeoPoint geoPoint, Drawable drawable, MapView.LayoutParams layoutParams) {
        Drawable drawable2;
        this.b = mapView;
        this.d = view;
        this.e = geoPoint;
        this.g = layoutParams;
        if (drawable == null) {
            if (c == null) {
                c = ConfigableConst.a(this.b.getContext(), "popup_bg.9.png", new byte[]{1, 2, 2, 9, 0, 0, 0, 0, 0, 0, 0, 0, 20, 0, 0, 0, 19, 0, 0, 0, 15, 0, 0, 0, 36, 0, 0, 0, 0, 0, 0, 0, 20, 0, 0, 0, -117, 0, 0, 0, 15, 0, 0, 0, 29, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, -1, -1, -1, -14, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0}, new Rect(20, 15, 19, 36));
            }
            drawable2 = c;
            drawable2.setAlpha(255);
        } else {
            drawable2 = drawable;
        }
        this.d.setBackgroundDrawable(drawable2);
    }

    public void a() {
        if (!c()) {
            if (f309a != null) {
                f309a.b();
            }
            f309a = this;
            this.b.getCanvas().a(this);
            if (this.g == null) {
                this.g = new MapView.LayoutParams(-2, -2, this.e, 25, 5, 85);
            }
            this.b.addView(this.d, this.g);
            this.f = W.b();
            this.b.invalidate();
        }
    }

    public void b() {
        if (c()) {
            f309a = null;
            this.b.removeView(this.d);
            this.b.getCanvas().b(this);
            this.b.invalidate();
        }
    }

    private boolean c() {
        return f309a == this;
    }

    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        return false;
    }

    public void onLongPress(MotionEvent motionEvent) {
    }

    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        return false;
    }

    public void onShowPress(MotionEvent motionEvent) {
    }

    public boolean onSingleTapUp(MotionEvent motionEvent) {
        if (W.b() - this.f <= 1000) {
            return false;
        }
        b();
        return false;
    }
}
