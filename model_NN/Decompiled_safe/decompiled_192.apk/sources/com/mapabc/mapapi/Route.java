package com.mapabc.mapapi;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.io.IOException;
import java.net.Proxy;
import java.text.DecimalFormat;
import java.util.List;

public class Route {
    public static final int BusDefault = 0;
    public static final int BusLeaseChange = 2;
    public static final int BusLeaseWalk = 3;
    public static final int BusMostComfortable = 4;
    public static final int BusSaveMoney = 1;
    public static final int DrivingDefault = 10;
    public static final int DrivingLeastDistance = 12;
    public static final int DrivingNoFastRoad = 13;
    public static final int DrivingSaveMoney = 11;
    public static final int WalkDefault = 23;

    /* renamed from: a  reason: collision with root package name */
    c f276a;
    private GeoPoint b = null;
    private GeoPoint c = null;
    private int d;
    protected List<Segment> mSegs;
    protected String mStartPlace;
    protected String mTargetPlace;

    public int getMode() {
        return this.d;
    }

    public int getLength() {
        int i = 0;
        for (Segment length : this.mSegs) {
            i += length.getLength();
        }
        return i;
    }

    private void a() {
        int i = Integer.MAX_VALUE;
        int i2 = Integer.MIN_VALUE;
        int i3 = Integer.MAX_VALUE;
        for (Segment lowerLeftPoint : this.mSegs) {
            GeoPoint lowerLeftPoint2 = lowerLeftPoint.getLowerLeftPoint();
            int longitudeE6 = lowerLeftPoint2.getLongitudeE6();
            int latitudeE6 = lowerLeftPoint2.getLatitudeE6();
            if (longitudeE6 < i3) {
                i3 = longitudeE6;
            }
            if (latitudeE6 >= i) {
                latitudeE6 = i;
            }
            i = latitudeE6;
        }
        int i4 = Integer.MIN_VALUE;
        for (Segment upperRightPoint : this.mSegs) {
            GeoPoint upperRightPoint2 = upperRightPoint.getUpperRightPoint();
            int longitudeE62 = upperRightPoint2.getLongitudeE6();
            int latitudeE62 = upperRightPoint2.getLatitudeE6();
            if (longitudeE62 > i2) {
                i2 = longitudeE62;
            }
            if (latitudeE62 <= i4) {
                latitudeE62 = i4;
            }
            i4 = latitudeE62;
        }
        this.b = new GeoPoint(i, i3);
        this.c = new GeoPoint(i4, i2);
    }

    public GeoPoint getLowerLeftPoint() {
        if (this.b == null) {
            a();
        }
        return this.b;
    }

    public GeoPoint getUpperRightPoint() {
        if (this.c == null) {
            a();
        }
        return this.c;
    }

    public static List<Route> calculateRoute(Context context, String str, FromAndTo fromAndTo, int i) throws IOException {
        C0022c r;
        aN aNVar = new aN(fromAndTo, i);
        Proxy b2 = W.b(context);
        String a2 = W.a(context);
        if (isBus(i)) {
            r = new C0012af(aNVar, b2, str, a2);
        } else if (isWalk(i)) {
            r = new Q(aNVar, b2, str, a2);
        } else {
            r = new R(aNVar, b2, str, a2);
        }
        return (List) r.g();
    }

    public static List<Route> calculateRoute(MapActivity mapActivity, FromAndTo fromAndTo, int i) throws IOException {
        return calculateRoute(mapActivity, W.a(mapActivity), fromAndTo, i);
    }

    public String getStartPlace() {
        return this.mStartPlace;
    }

    public String getTargetPlace() {
        return this.mTargetPlace;
    }

    public GeoPoint getStartPos() {
        return this.mSegs.get(0).getFirstPoint();
    }

    public GeoPoint getTargetPos() {
        return this.mSegs.get(getStepCount() - 1).getLastPoint();
    }

    public int getStepCount() {
        return this.mSegs.size();
    }

    public Segment getStep(int i) {
        return this.mSegs.get(i);
    }

    public int getSegmentIndex(Segment segment) {
        return this.mSegs.indexOf(segment);
    }

    public String getStepedDescription(int i) {
        return this.f276a.b(i);
    }

    public String getOverview() {
        return this.f276a.a();
    }

    public static class FromAndTo {
        public static final int NoTrans = 0;
        public static final int TransBothPoint = 3;
        public static final int TransFromPoint = 1;
        public static final int TransToPoint = 2;

        /* renamed from: a  reason: collision with root package name */
        GeoPoint f277a;
        GeoPoint b;
        int c;

        public FromAndTo(GeoPoint geoPoint, GeoPoint geoPoint2, int i) {
            this.f277a = geoPoint;
            this.b = geoPoint2;
            this.c = i;
        }

        public FromAndTo(GeoPoint geoPoint, GeoPoint geoPoint2) {
            this(geoPoint, geoPoint2, 0);
        }
    }

    abstract class c {
        /* access modifiers changed from: protected */
        public abstract Drawable a(int i);

        public abstract String a();

        public abstract Paint d(int i);

        c() {
        }

        public String b(int i) {
            return h(i);
        }

        private String h(int i) {
            if (i == 0) {
                return Route.this.mStartPlace;
            }
            if (i == Route.this.getStepCount()) {
                return Route.this.mTargetPlace;
            }
            return null;
        }

        public Spanned c(int i) {
            String h = h(i);
            if (h == null) {
                return null;
            }
            return W.c(W.a(h, "#000000"));
        }

        public int e(int i) {
            if (i >= Route.this.getStepCount()) {
                return -1;
            }
            return i + 1;
        }

        public int f(int i) {
            if (i <= 0) {
                return -1;
            }
            return i - 1;
        }

        public View a(MapView mapView, Context context, RouteMessageHandler routeMessageHandler, RouteOverlay routeOverlay, int i) {
            return null;
        }

        public final GeoPoint g(int i) {
            if (i == Route.this.getStepCount()) {
                return Route.this.getStep(i - 1).getLastPoint();
            }
            return Route.this.getStep(i).getFirstPoint();
        }

        public final View a(Context context, int i) {
            if (i < 0 || i > Route.this.getStepCount()) {
                return null;
            }
            LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setOrientation(1);
            linearLayout.setBackgroundColor(Color.argb(255, 255, 255, 255));
            LinearLayout linearLayout2 = new LinearLayout(context);
            linearLayout2.setOrientation(0);
            linearLayout2.setBackgroundColor(-1);
            linearLayout2.setGravity(2);
            ImageView imageView = new ImageView(context);
            imageView.setBackgroundColor(-1);
            imageView.setImageDrawable(a(i));
            imageView.setPadding(3, 3, 0, 0);
            linearLayout2.addView(imageView, new LinearLayout.LayoutParams(-2, -2));
            TextView textView = new TextView(context);
            textView.setBackgroundColor(-1);
            String[] split = c(i).toString().split("\\n", 2);
            textView.setTextColor(-16777216);
            textView.setText(W.c(split[0]));
            textView.setPadding(3, 0, 0, 3);
            linearLayout2.addView(textView, new LinearLayout.LayoutParams(-1, -2));
            TextView textView2 = new TextView(context);
            textView2.setBackgroundColor(Color.rgb(165, 166, 165));
            textView2.setLayoutParams(new LinearLayout.LayoutParams(-1, 1));
            LinearLayout linearLayout3 = new LinearLayout(context);
            linearLayout3.setOrientation(1);
            linearLayout3.setBackgroundColor(-1);
            TextView textView3 = new TextView(context);
            textView3.setBackgroundColor(-1);
            if (split.length == 2) {
                linearLayout3.addView(textView2, new LinearLayout.LayoutParams(-1, 1));
                textView3.setText(W.c(split[1]));
                textView3.setTextColor(Color.rgb(82, 85, 82));
                linearLayout3.addView(textView3, new LinearLayout.LayoutParams(-1, -2));
            }
            LinearLayout linearLayout4 = new LinearLayout(context);
            linearLayout4.setOrientation(0);
            linearLayout4.setGravity(1);
            linearLayout4.setBackgroundColor(-1);
            linearLayout.addView(linearLayout2, new LinearLayout.LayoutParams(-1, -2));
            linearLayout.addView(linearLayout3, new LinearLayout.LayoutParams(-1, 1));
            linearLayout.addView(linearLayout4, new LinearLayout.LayoutParams(-1, -2));
            new DisplayMetrics();
            DisplayMetrics displayMetrics = context.getApplicationContext().getResources().getDisplayMetrics();
            if (((long) (displayMetrics.heightPixels * displayMetrics.widthPixels)) <= 153600) {
                return linearLayout;
            }
            TextView textView4 = new TextView(context);
            textView4.setText("");
            textView4.setHeight(5);
            textView4.setWidth(1);
            linearLayout.addView(textView4);
            return linearLayout;
        }
    }

    class d extends c {
        d() {
            super();
        }

        public final Paint d(int i) {
            Paint paint = aE.g;
            if (Route.this.getStep(i) instanceof BusSegment) {
                return aE.h;
            }
            return paint;
        }

        public final String a() {
            StringBuilder sb = new StringBuilder();
            int stepCount = Route.this.getStepCount();
            int i = 1;
            int i2 = 0;
            while (i < stepCount) {
                BusSegment busSegment = (BusSegment) Route.this.getStep(i);
                if (i != 1) {
                    sb.append(" -> ");
                }
                sb.append(busSegment.mLine);
                i += 2;
                i2 = busSegment.mLength + i2;
            }
            if (i2 != 0) {
                sb.append("\n");
            }
            int i3 = 0;
            for (int i4 = 0; i4 < stepCount; i4 += 2) {
                i3 += Route.this.getStep(i4).mLength;
            }
            sb.append(String.format("%s%s  %s%s", "乘车", Route.a(i2), "步行", Route.a(i3)));
            return sb.toString();
        }

        public final String b(int i) {
            String b = super.b(i);
            if (b != null) {
                return b;
            }
            if (Route.this.getStep(i) instanceof BusSegment) {
                BusSegment busSegment = (BusSegment) Route.this.getStep(i);
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append(String.format("%s ( %s -- %s ) - %s%s\n", busSegment.mLine, busSegment.mFirstStation, busSegment.mLastStation, busSegment.mLastStation, "方向"));
                stringBuffer.append("上车 : " + busSegment.getOnStationName() + "\n");
                stringBuffer.append("下车 : " + busSegment.getOffStationName() + "\n");
                stringBuffer.append(String.format("%s%d%s (%s)", "公交", Integer.valueOf(busSegment.getStopNumber() - 1), "站", "大约" + Route.a(busSegment.mLength)));
                return stringBuffer.toString();
            }
            StringBuilder sb = new StringBuilder();
            sb.append("步行").append("去往");
            if (i == Route.this.getStepCount() - 1) {
                sb.append("目的地");
            } else {
                sb.append(((BusSegment) Route.this.getStep(i + 1)).mLine + "车站");
            }
            sb.append("\n大约" + Route.a(Route.this.getStep(i).mLength));
            return sb.toString();
        }

        public final Spanned c(int i) {
            Spanned c = super.c(i);
            if (c != null) {
                return c;
            }
            if (Route.this.getStep(i) instanceof BusSegment) {
                BusSegment busSegment = (BusSegment) Route.this.getStep(i);
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append(W.a(busSegment.mLine, "#000000"));
                stringBuffer.append(W.e());
                stringBuffer.append(W.a(busSegment.mLastStation, "#000000"));
                stringBuffer.append("方向");
                stringBuffer.append(W.f());
                stringBuffer.append("上车 : ");
                stringBuffer.append(W.a(busSegment.getOnStationName(), "#000000"));
                stringBuffer.append(W.e());
                stringBuffer.append(W.f());
                stringBuffer.append("下车 : ");
                stringBuffer.append(W.a(busSegment.getOffStationName(), "#000000"));
                stringBuffer.append(W.f());
                stringBuffer.append(String.format("%s%d%s , ", "公交", Integer.valueOf(busSegment.getStopNumber() - 1), "站"));
                stringBuffer.append("大约" + Route.a(busSegment.mLength));
                return W.c(stringBuffer.toString());
            }
            StringBuilder sb = new StringBuilder();
            sb.append("步行").append("去往");
            if (i == Route.this.getStepCount() - 1) {
                sb.append(W.a("目的地", "#808080"));
            } else {
                sb.append(W.a(((BusSegment) Route.this.getStep(i + 1)).mLine + "车站", "#808080"));
            }
            sb.append(W.f());
            sb.append("大约" + Route.a(Route.this.getStep(i).mLength));
            return W.c(sb.toString());
        }

        public final int e(int i) {
            int i2 = i;
            do {
                i2++;
                if (i2 >= Route.this.getStepCount() - 1) {
                    break;
                }
            } while (!(Route.this.getStep(i2) instanceof BusSegment));
            return i2;
        }

        public final int f(int i) {
            if (i == Route.this.getStepCount()) {
                return i - 1;
            }
            int i2 = i;
            do {
                i2--;
                if (i2 <= 0) {
                    return i2;
                }
            } while (!(Route.this.getStep(i2) instanceof BusSegment));
            return i2;
        }

        public final View a(MapView mapView, Context context, RouteMessageHandler routeMessageHandler, RouteOverlay routeOverlay, int i) {
            String str;
            Drawable a2 = a(i);
            if (i == 0 || i == Route.this.getStepCount()) {
                return null;
            }
            Segment step = Route.this.getStep(i);
            if (step instanceof BusSegment) {
                str = ((BusSegment) step).mLine;
            } else {
                str = null;
            }
            if (str == null && a2 == null) {
                return null;
            }
            LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setOrientation(0);
            ImageView imageView = new ImageView(context);
            imageView.setImageDrawable(a2);
            imageView.setPadding(3, 3, 1, 5);
            linearLayout.addView(imageView, new LinearLayout.LayoutParams(-2, -2));
            if (str != null) {
                TextView textView = new TextView(context);
                textView.setText(str);
                textView.setTextColor(-16777216);
                textView.setPadding(3, 0, 3, 3);
                linearLayout.addView(textView, new LinearLayout.LayoutParams(-2, -2));
            }
            linearLayout.setOnClickListener(new View.OnClickListener() {
                public final void onClick(View view) {
                    C0039t.this.f326a.onRouteEvent(C0039t.this.d, C0039t.this.e, C0039t.this.c, C0039t.this.b);
                }
            });
            return linearLayout;
        }

        /* access modifiers changed from: protected */
        public final Drawable a(int i) {
            if (i == Route.this.getStepCount() - 1) {
                return aE.c;
            }
            if (i < Route.this.getStepCount() && (Route.this.getStep(i) instanceof BusSegment)) {
                return aE.d;
            }
            if (i == 0) {
                return aE.e;
            }
            if (i == Route.this.getStepCount()) {
                return aE.f;
            }
            return null;
        }
    }

    abstract class e extends c {
        e() {
            super();
        }

        public final Paint d(int i) {
            return aE.i;
        }

        public final String a() {
            String str;
            String str2;
            String str3;
            String str4;
            int stepCount = Route.this.getStepCount();
            int i = 0;
            String str5 = "";
            String str6 = "";
            int i2 = 0;
            while (i2 < stepCount) {
                DriveWalkSegment driveWalkSegment = (DriveWalkSegment) Route.this.getStep(i2);
                i += driveWalkSegment.mLength;
                if (W.a(driveWalkSegment.mRoadName) || driveWalkSegment.mRoadName.equals(str5)) {
                    str2 = str5;
                    str3 = str6;
                } else {
                    if (!W.a(str6)) {
                        str4 = str6 + " -> ";
                    } else {
                        str4 = str6;
                    }
                    str3 = str4 + driveWalkSegment.mRoadName;
                    str2 = driveWalkSegment.mRoadName;
                }
                i2++;
                str6 = str3;
                str5 = str2;
            }
            if (!W.a(str6)) {
                str = str6 + "\n";
            } else {
                str = str6;
            }
            return str + String.format("%s", "大约" + Route.a(i));
        }

        public final String b(int i) {
            String b = super.b(i);
            if (b != null) {
                return b;
            }
            String str = "";
            DriveWalkSegment driveWalkSegment = (DriveWalkSegment) Route.this.getStep(i);
            if (!W.a(driveWalkSegment.mRoadName)) {
                str = driveWalkSegment.mRoadName + " ";
            }
            return (str + driveWalkSegment.mActionDes + " ") + String.format("%s%s", "大约", Route.a(driveWalkSegment.mLength));
        }

        public final Spanned c(int i) {
            String str;
            Spanned c = super.c(i);
            if (c != null) {
                return c;
            }
            DriveWalkSegment driveWalkSegment = (DriveWalkSegment) Route.this.getStep(i);
            if (W.a(driveWalkSegment.mRoadName) || W.a(driveWalkSegment.mActionDes)) {
                str = driveWalkSegment.mActionDes + driveWalkSegment.mRoadName;
            } else {
                str = driveWalkSegment.mActionDes + " --> " + driveWalkSegment.mRoadName;
            }
            return W.c((W.a(str, "#808080") + W.f()) + String.format("%s%s", "大约", Route.a(driveWalkSegment.mLength)));
        }
    }

    class b extends e {
        b(Route route) {
            super();
        }

        /* access modifiers changed from: protected */
        public final Drawable a(int i) {
            return aE.c;
        }
    }

    class a extends e {
        a(Route route) {
            super();
        }

        /* access modifiers changed from: protected */
        public final Drawable a(int i) {
            return aE.d;
        }
    }

    Route(int i) {
        this.d = i;
        if (isBus(i)) {
            this.f276a = new d();
        } else if (isDrive(i)) {
            this.f276a = new a(this);
        } else if (isWalk(i)) {
            this.f276a = new b(this);
        } else {
            throw new IllegalArgumentException("Unkown mode");
        }
    }

    public static boolean isDrive(int i) {
        return i >= 10 && i <= 13;
    }

    public static boolean isBus(int i) {
        return i >= 0 && i <= 4;
    }

    public static boolean isWalk(int i) {
        return i == 23;
    }

    static String a(int i) {
        if (i > 10000) {
            return (i / 1000) + "公里";
        } else if (i > 1000) {
            return new DecimalFormat("##0.0").format((double) (((float) i) / 1000.0f)) + "公里";
        } else if (i > 100) {
            return ((i / 50) * 50) + "米";
        } else {
            int i2 = (i / 10) * 10;
            if (i2 == 0) {
                i2 = 10;
            }
            return i2 + "米";
        }
    }
}
