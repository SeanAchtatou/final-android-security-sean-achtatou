package com.mapabc.mapapi;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;

public abstract class MapActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private Mediator f246a = null;
    private MapView b;

    /* access modifiers changed from: package-private */
    public final void a(Mediator mediator) {
        this.f246a = mediator;
    }

    /* access modifiers changed from: package-private */
    public final Mediator a() {
        return this.f246a;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        GlobalStore.setsIsRuning(true);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.f246a != null) {
            for (C0028i a2 : Mediator.this.e.f) {
                a2.a();
            }
        }
        GlobalStore.setsIsRuning(false);
        if (this.b != null) {
            this.b.closeMapview();
        }
        this.f246a = null;
        this.b = null;
        System.gc();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onPause();
        if (this.f246a != null) {
            this.f246a.c.b();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.f246a != null) {
            this.f246a.c.a();
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    /* access modifiers changed from: package-private */
    public final void a(MapView mapView, Context context, String str, String str2) {
        if (this.b != null) {
            throw new IllegalStateException("You are only allowed to have a single MapView in a MapActivity");
        }
        this.b = mapView;
        this.b.initEnviornment(context, str, str2);
    }

    /* access modifiers changed from: protected */
    public boolean isLocationDisplayed() {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean isRouteDisplayed() {
        return false;
    }
}
