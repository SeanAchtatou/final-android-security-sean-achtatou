package com.mapabc.mapapi;

import android.content.Context;
import android.hardware.SensorListener;
import android.hardware.SensorManager;

final class aw extends C0028i {
    private SensorManager c;
    private SensorListener d = null;

    public aw(Mediator mediator, Context context) {
        super(mediator, context);
        this.c = (SensorManager) context.getSystemService("sensor");
    }

    public final boolean a(SensorListener sensorListener) {
        f();
        this.d = sensorListener;
        return g();
    }

    public final void e() {
        f();
        this.d = null;
    }

    public final void c() {
        g();
    }

    public final void b() {
        f();
    }

    private void f() {
        if (this.d != null) {
            try {
                this.c.unregisterListener(this.d);
            } catch (Exception e) {
            }
        }
    }

    private boolean g() {
        if (this.d == null) {
            return false;
        }
        try {
            return this.c.registerListener(this.d, 1, 1);
        } catch (Exception e) {
            return false;
        }
    }
}
