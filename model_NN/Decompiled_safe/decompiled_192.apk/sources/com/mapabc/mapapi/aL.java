package com.mapabc.mapapi;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;

final class aL<T> {

    /* renamed from: a  reason: collision with root package name */
    private LinkedList<T> f304a = new LinkedList<>();
    private final Semaphore b = new Semaphore(0, false);
    private boolean c = true;

    aL() {
    }

    public final void a() {
        this.c = false;
        this.b.release(100);
    }

    public final synchronized void a(List list) {
        this.f304a.clear();
        this.f304a.addAll(list);
        b();
    }

    private void b() {
        if (this.f304a.size() != 0) {
            this.b.release();
        }
    }

    public final ArrayList<T> a(int i) {
        try {
            this.b.acquire();
        } catch (InterruptedException e) {
        }
        if (this.c) {
            return b(i);
        }
        return null;
    }

    private synchronized ArrayList<T> b(int i) {
        ArrayList<T> arrayList;
        synchronized (this) {
            int size = this.f304a.size();
            if (i <= size) {
                size = i;
            }
            arrayList = new ArrayList<>(size);
            for (int i2 = 0; i2 < size; i2++) {
                arrayList.add(this.f304a.get(0));
                this.f304a.removeFirst();
            }
            b();
        }
        return arrayList;
    }
}
