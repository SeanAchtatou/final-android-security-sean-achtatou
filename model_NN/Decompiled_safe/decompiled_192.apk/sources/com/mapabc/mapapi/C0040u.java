package com.mapabc.mapapi;

import java.net.Proxy;
import java.util.ArrayList;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/* renamed from: com.mapabc.mapapi.u  reason: case insensitive filesystem */
final class C0040u extends X<ArrayList<O>, C0024e> {
    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object a(NodeList nodeList) {
        int length = nodeList.getLength();
        C0024e eVar = new C0024e();
        for (int i = 0; i < length; i++) {
            Node item = nodeList.item(i);
            String nodeName = item.getNodeName();
            if (nodeName.equals("update")) {
                eVar.f317a.c = (long) Integer.parseInt(a(item));
            } else if (nodeName.equals(LocationManagerProxy.KEY_STATUS_CHANGED)) {
                eVar.f317a.f310a = Integer.parseInt(a(item));
            } else if (nodeName.equals("ads")) {
                NodeList childNodes = item.getChildNodes();
                int length2 = childNodes.getLength();
                for (int i2 = 0; i2 < length2; i2++) {
                    Node item2 = childNodes.item(i2);
                    if (item2.getNodeType() == 1 && item2.getNodeName().equals("ad")) {
                        ArrayList<H> arrayList = eVar.b;
                        H h = new H();
                        NodeList childNodes2 = item2.getChildNodes();
                        int length3 = childNodes2.getLength();
                        for (int i3 = 0; i3 < length3; i3++) {
                            Node item3 = childNodes2.item(i3);
                            String nodeName2 = item3.getNodeName();
                            String a2 = a(item3);
                            if (nodeName2.equals("clickurl")) {
                                h.c = a2;
                            } else if (nodeName2.equals("id")) {
                                h.f238a = a2;
                            } else if (nodeName2.equals("title")) {
                                h.b = a2;
                            } else if (nodeName2.equals("type")) {
                                Integer.parseInt(a2);
                            } else if (nodeName2.equals("weight")) {
                                h.d = Integer.parseInt(a2);
                            } else {
                                nodeName2.equals("content");
                            }
                        }
                        arrayList.add(h);
                    }
                }
            }
        }
        return eVar;
    }

    public C0040u(ArrayList<O> arrayList, Proxy proxy, String str, String str2, String str3) {
        super(arrayList, proxy, str, str2, str3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mapabc.mapapi.aC.a(java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      com.mapabc.mapapi.aC.a(java.io.InputStream, int):java.lang.String
      com.mapabc.mapapi.aC.a(java.lang.String, java.lang.String):java.lang.String
      com.mapabc.mapapi.aC.a(java.lang.String, boolean):java.lang.String */
    /* access modifiers changed from: protected */
    public final String[] e() {
        int size = ((ArrayList) this.d).size();
        if (size == 0) {
            return null;
        }
        String[] strArr = new String[(size + 2)];
        strArr[0] = "<![CDATA[<adlist>";
        for (int i = 1; i <= size; i++) {
            O o = (O) ((ArrayList) this.d).get(i - 1);
            strArr[i] = (String.format("%s %d ", (String.format("%s %s ", a("ad", true) + a("id", true), o.f266a) + a("id", false)) + a("time", true), Integer.valueOf(o.b)) + a("time", false)) + a("ad", false);
        }
        strArr[size + 1] = "</adlist>]]>";
        return strArr;
    }

    /* access modifiers changed from: protected */
    public final int f() {
        return 23;
    }
}
