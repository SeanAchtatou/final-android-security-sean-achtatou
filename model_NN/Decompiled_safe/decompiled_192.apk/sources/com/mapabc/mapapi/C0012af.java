package com.mapabc.mapapi;

import java.io.IOException;
import java.io.InputStream;
import java.net.Proxy;
import java.util.LinkedList;

/* renamed from: com.mapabc.mapapi.af  reason: case insensitive filesystem */
final class C0012af extends C0022c {
    public C0012af(aN aNVar, Proxy proxy, String str, String str2) {
        super(aNVar, proxy, str, str2);
    }

    /* access modifiers changed from: protected */
    public final Route a(InputStream inputStream) throws IOException {
        f(inputStream);
        f(inputStream);
        return super.a(inputStream);
    }

    private static Segment a(GeoPoint geoPoint, GeoPoint geoPoint2) {
        Segment segment = new Segment();
        segment.mShapes = new GeoPoint[2];
        segment.mShapes[0] = geoPoint;
        segment.mShapes[1] = geoPoint2;
        int latitudeE6 = geoPoint2.getLatitudeE6() - geoPoint.getLatitudeE6();
        int longitudeE6 = geoPoint2.getLongitudeE6() - geoPoint.getLongitudeE6();
        segment.mLength = W.a((int) Math.sqrt((double) ((latitudeE6 * latitudeE6) + (longitudeE6 * longitudeE6))));
        if (segment.mLength == 0) {
            segment.mLength = 10;
        }
        return segment;
    }

    /* access modifiers changed from: protected */
    public final int f() {
        return 9;
    }

    /* access modifiers changed from: protected */
    public final Segment b(InputStream inputStream) throws IOException {
        BusSegment busSegment = new BusSegment();
        String h = h(inputStream);
        busSegment.mLine = h;
        busSegment.mFirstStation = "";
        busSegment.mLastStation = "";
        int indexOf = h.indexOf("(");
        int lastIndexOf = h.lastIndexOf(")");
        if (indexOf > 0 && lastIndexOf > 0 && lastIndexOf > indexOf) {
            busSegment.mLine = h.substring(0, indexOf);
            try {
                String[] split = h.substring(indexOf + 1, lastIndexOf).split("--");
                busSegment.mFirstStation = split[0];
                busSegment.mLastStation = split[1];
            } catch (Exception e) {
            }
        }
        String h2 = h(inputStream);
        String h3 = h(inputStream);
        busSegment.mLength = f(inputStream);
        int f = f(inputStream);
        String[] strArr = new String[(f + 2)];
        GeoPoint[] geoPointArr = new GeoPoint[(f + 2)];
        int length = strArr.length - 2;
        for (int i = 1; i <= length; i++) {
            strArr[i] = h(inputStream);
            geoPointArr[i] = c(inputStream);
        }
        busSegment.mShapes = d(inputStream);
        geoPointArr[0] = busSegment.mShapes[0].Copy();
        geoPointArr[f + 1] = busSegment.mShapes[busSegment.mShapes.length - 1].Copy();
        strArr[0] = h2;
        strArr[f + 1] = h3;
        busSegment.mPassStopPos = geoPointArr;
        busSegment.mPassStopName = strArr;
        return busSegment;
    }

    /* access modifiers changed from: protected */
    public final void a(Route route) {
        LinkedList linkedList = (LinkedList) route.mSegs;
        int size = linkedList.size();
        Segment[] segmentArr = new Segment[(size - 1)];
        for (int i = 0; i <= size - 2; i++) {
            segmentArr[i] = a(((Segment) linkedList.get(i)).getLastPoint(), ((Segment) linkedList.get(i + 1)).getFirstPoint());
        }
        for (int i2 = 0; i2 <= size - 2; i2++) {
            linkedList.add((i2 << 1) + 1, segmentArr[i2]);
        }
        linkedList.addFirst(a(this.f315a, ((Segment) linkedList.getFirst()).getFirstPoint()));
        linkedList.addLast(a(((Segment) linkedList.getLast()).getLastPoint(), this.b));
        route.mSegs = linkedList;
    }
}
