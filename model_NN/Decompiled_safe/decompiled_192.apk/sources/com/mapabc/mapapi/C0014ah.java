package com.mapabc.mapapi;

import android.content.Context;
import java.net.Proxy;

/* renamed from: com.mapabc.mapapi.ah  reason: case insensitive filesystem */
final class C0014ah extends ar<Object, Boolean> {
    private boolean d = false;

    public final /* bridge */ /* synthetic */ void a(Object obj) {
        Boolean bool = (Boolean) obj;
        if (bool != null) {
            this.d = true;
            GlobalStore.getInstance().setEnable(bool.booleanValue());
        }
    }

    public C0014ah(Mediator mediator, Context context) {
        super(mediator, context);
    }

    public final void d() {
        super.d();
        if (this.c > 0 && !this.d) {
            f().a(new Object());
        }
    }

    public final aC a(Object obj, Proxy proxy, String str, String str2, String str3) {
        return new S(obj, proxy, str, str2, str3);
    }
}
