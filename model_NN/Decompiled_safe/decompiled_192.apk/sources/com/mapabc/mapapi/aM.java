package com.mapabc.mapapi;

import java.io.IOException;
import java.io.InputStream;
import java.net.Proxy;

final class aM extends aC<aK, Boolean> {
    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object e(InputStream inputStream) throws IOException {
        return true;
    }

    public aM(aK aKVar, Proxy proxy, String str, String str2, String str3) {
        super(aKVar, proxy, str, str2, str3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mapabc.mapapi.aC.a(java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      com.mapabc.mapapi.aC.a(java.io.InputStream, int):java.lang.String
      com.mapabc.mapapi.aC.a(java.lang.String, java.lang.String):java.lang.String
      com.mapabc.mapapi.aC.a(java.lang.String, boolean):java.lang.String */
    /* access modifiers changed from: protected */
    public final String[] e() {
        int size = ((aK) this.d).b.size();
        int size2 = ((aK) this.d).f303a.size();
        String[] strArr = new String[(size + 4 + size2)];
        strArr[0] = "<![CDATA[<celllist>";
        for (int i = 1; i < size2 + 1; i++) {
            strArr[i] = a("cell", true);
            strArr[i] = strArr[i] + ((aK) this.d).f303a.get(i - 1);
            strArr[i] = strArr[i] + a("cell", false);
        }
        strArr[size2 + 1] = "</celllist>]]>";
        strArr[size2 + 2] = "<![CDATA[<loglist>";
        int i2 = size2 + 3;
        while (true) {
            int i3 = i2;
            if (i3 < size + 3 + size2) {
                strArr[i3] = a("log", true);
                strArr[i3] = strArr[i3] + ((aK) this.d).b.get((i3 - size2) - 3);
                strArr[i3] = strArr[i3] + a("log", false);
                i2 = i3 + 1;
            } else {
                strArr[size2 + size + 3] = "</loglist>]]>";
                return strArr;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final int f() {
        return 100;
    }
}
