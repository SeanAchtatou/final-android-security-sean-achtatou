package com.mapabc.mapapi;

import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.KeyEvent;
import android.view.MotionEvent;
import com.mapabc.mapapi.ConfigableConst;
import com.mapabc.mapapi.GeoPoint;
import com.mapabc.mapapi.Overlay;
import com.mapabc.mapapi.OverlayItem;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

public abstract class ItemizedOverlay<Item extends OverlayItem> extends Overlay implements Overlay.Snappable {
    private static int d = -1;

    /* renamed from: a  reason: collision with root package name */
    private boolean f240a = true;
    /* access modifiers changed from: private */
    public Drawable b;
    private Drawable c;
    private ItemizedOverlay<Item>.a e = null;
    private OnFocusChangeListener f = null;
    private int g = -1;
    private int h = -1;

    public interface OnFocusChangeListener {
        void onFocusChanged(ItemizedOverlay<?> itemizedOverlay, OverlayItem overlayItem);
    }

    enum b {
        Normal,
        Center,
        CenterBottom
    }

    /* access modifiers changed from: protected */
    public abstract Item createItem(int i);

    public abstract int size();

    public ItemizedOverlay(Drawable drawable) {
        this.b = drawable;
        if (this.b == null) {
            this.b = new BitmapDrawable(ConfigableConst.d[ConfigableConst.ENUM_ID.emarker.ordinal()]);
        }
        this.b.setBounds(0, 0, this.b.getIntrinsicWidth(), this.b.getIntrinsicHeight());
        this.c = new C0031l().a(this.b);
        if (1 == d) {
            boundCenterBottom(this.b);
        } else if (2 == d) {
            boundCenter(this.b);
        } else {
            boundCenterBottom(this.b);
        }
    }

    protected static Drawable boundCenterBottom(Drawable drawable) {
        d = 1;
        return a(drawable, b.CenterBottom);
    }

    protected static Drawable boundCenter(Drawable drawable) {
        d = 2;
        return a(drawable, b.Center);
    }

    private static Drawable a(Drawable drawable, b bVar) {
        int i;
        int i2;
        if (drawable == null || b.Normal == bVar) {
            return null;
        }
        Rect bounds = drawable.getBounds();
        int width = bounds.width() / 2;
        int i3 = -bounds.height();
        if (bVar == b.Center) {
            int i4 = i3 / 2;
            i = i4;
            i2 = -i4;
        } else {
            i = i3;
            i2 = 0;
        }
        drawable.setBounds(-width, i, width, i2);
        return drawable;
    }

    /* access modifiers changed from: protected */
    public final void populate() {
        this.e = new a();
        this.g = -1;
        this.h = -1;
    }

    public GeoPoint getCenter() {
        return getItem(getIndexToDraw(0)).getPoint();
    }

    /* access modifiers changed from: protected */
    public int getIndexToDraw(int i) {
        return this.e.b(i);
    }

    public final Item getItem(int i) {
        return this.e.a(i);
    }

    public int getLatSpanE6() {
        return this.e.a(true);
    }

    public int getLonSpanE6() {
        return this.e.a(false);
    }

    public void setOnFocusChangeListener(OnFocusChangeListener onFocusChangeListener) {
        this.f = onFocusChangeListener;
    }

    public void setDrawFocusedItem(boolean z) {
        this.f240a = z;
    }

    public final int getLastFocusedIndex() {
        return this.g;
    }

    /* access modifiers changed from: protected */
    public void setLastFocusedIndex(int i) {
        this.g = i;
    }

    public Item getFocus() {
        if (this.h != -1) {
            return this.e.a(this.h);
        }
        return null;
    }

    public void setFocus(Item item) {
        if (item != null && this.h == this.e.a(item)) {
            return;
        }
        if (item != null || this.h == -1) {
            this.h = this.e.a(item);
            if (this.h != -1) {
                setLastFocusedIndex(this.h);
                if (this.f != null) {
                    this.f.onFocusChanged(this, item);
                    return;
                }
                return;
            }
            return;
        }
        if (this.f != null) {
            this.f.onFocusChanged(this, item);
        }
        this.h = -1;
    }

    public Item nextFocus(boolean z) {
        if (this.e.a() == 0) {
            return null;
        }
        if (this.g != -1) {
            int i = this.h == -1 ? this.g : this.h;
            if (z) {
                if (i == this.e.a() - 1) {
                    return null;
                }
                return this.e.a(i + 1);
            } else if (i == 0) {
                return null;
            } else {
                return this.e.a(i - 1);
            }
        } else if (this.h == -1) {
            return null;
        } else {
            return this.e.a(0);
        }
    }

    public boolean onTap(GeoPoint geoPoint, MapView mapView) {
        return this.e.a(geoPoint, mapView);
    }

    /* access modifiers changed from: protected */
    public boolean hitTest(Item item, Drawable drawable, int i, int i2) {
        return drawable.getBounds().contains(i, i2);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent, MapView mapView) {
        return false;
    }

    public boolean onTrackballEvent(MotionEvent motionEvent, MapView mapView) {
        return false;
    }

    public boolean onTouchEvent(MotionEvent motionEvent, MapView mapView) {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean onTap(int i) {
        if (i == this.h) {
            return false;
        }
        setFocus(getItem(i));
        return false;
    }

    public boolean onSnapToItem(int i, int i2, Point point, MapView mapView) {
        int i3;
        Point point2 = new Point();
        int i4 = 0;
        while (true) {
            if (i4 >= this.e.a()) {
                i3 = -1;
                break;
            }
            OverlayItem a2 = this.e.a(i4);
            mapView.getProjection().toPixels(a2.getPoint(), point2);
            if (hitTest(a2, a2.mMarker, i - point2.x, i2 - point2.y)) {
                i3 = i4;
                break;
            }
            i4++;
        }
        if (i3 == -1) {
            return false;
        }
        mapView.getProjection().toPixels(this.e.a(i3).getPoint(), point);
        return true;
    }

    public void draw(Canvas canvas, MapView mapView, boolean z) {
        for (int i = 0; i < this.e.a(); i++) {
            int indexToDraw = getIndexToDraw(i);
            if (indexToDraw != this.h) {
                a(canvas, mapView, z, getItem(indexToDraw), 0);
            }
        }
        OverlayItem focus = getFocus();
        if (this.f240a && focus != null) {
            a(canvas, mapView, true, focus, 4);
            a(canvas, mapView, false, focus, 4);
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: Item
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private void a(android.graphics.Canvas r6, com.mapabc.mapapi.MapView r7, boolean r8, Item r9, int r10) {
        /*
            r5 = this;
            android.graphics.drawable.Drawable r0 = r9.getMarker(r10)
            if (r0 != 0) goto L_0x003e
            r1 = 1
        L_0x0007:
            if (r0 == 0) goto L_0x000f
            android.graphics.drawable.Drawable r1 = r5.b
            boolean r1 = r0.equals(r1)
        L_0x000f:
            if (r1 == 0) goto L_0x0027
            if (r8 == 0) goto L_0x0040
            android.graphics.drawable.Drawable r0 = r5.c
            android.graphics.drawable.Drawable r2 = r5.c
            android.graphics.drawable.Drawable r3 = r5.b
            android.graphics.Rect r3 = r3.copyBounds()
            r2.setBounds(r3)
            android.graphics.drawable.Drawable r2 = r5.c
            android.graphics.drawable.Drawable r3 = r5.b
            com.mapabc.mapapi.C0031l.a(r2, r3)
        L_0x0027:
            com.mapabc.mapapi.Projection r2 = r7.getProjection()
            com.mapabc.mapapi.GeoPoint r3 = r9.getPoint()
            r4 = 0
            android.graphics.Point r2 = r2.toPixels(r3, r4)
            if (r1 == 0) goto L_0x0043
            int r1 = r2.x
            int r2 = r2.y
            com.mapabc.mapapi.Overlay.a(r6, r0, r1, r2)
        L_0x003d:
            return
        L_0x003e:
            r1 = 0
            goto L_0x0007
        L_0x0040:
            android.graphics.drawable.Drawable r0 = r5.b
            goto L_0x0027
        L_0x0043:
            int r1 = r2.x
            int r2 = r2.y
            com.mapabc.mapapi.Overlay.drawAt(r6, r0, r1, r2, r8)
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mapabc.mapapi.ItemizedOverlay.a(android.graphics.Canvas, com.mapabc.mapapi.MapView, boolean, com.mapabc.mapapi.OverlayItem, int):void");
    }

    class a implements Comparator<Integer> {

        /* renamed from: a  reason: collision with root package name */
        private ArrayList<Item> f241a;
        private ArrayList<Integer> b;

        public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
            GeoPoint point = ((OverlayItem) this.f241a.get(((Integer) obj).intValue())).getPoint();
            GeoPoint point2 = ((OverlayItem) this.f241a.get(((Integer) obj2).intValue())).getPoint();
            if (point.getLatitudeE6() > point2.getLatitudeE6()) {
                return -1;
            }
            if (point.getLatitudeE6() < point2.getLatitudeE6()) {
                return 1;
            }
            if (point.getLongitudeE6() < point2.getLongitudeE6()) {
                return -1;
            }
            return point.getLongitudeE6() > point2.getLongitudeE6() ? 1 : 0;
        }

        public a() {
            int size = ItemizedOverlay.this.size();
            this.f241a = new ArrayList<>(size);
            this.b = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                this.b.add(Integer.valueOf(i));
                this.f241a.add(ItemizedOverlay.this.createItem(i));
            }
            Collections.sort(this.b, this);
        }

        public final int a() {
            return this.f241a.size();
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: Item
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public final int a(Item r4) {
            /*
                r3 = this;
                r0 = -1
                if (r4 == 0) goto L_0x0019
                r1 = 0
            L_0x0004:
                java.util.ArrayList<Item> r2 = r3.f241a
                int r2 = r2.size()
                if (r1 >= r2) goto L_0x0019
                java.util.ArrayList<Item> r2 = r3.f241a
                java.lang.Object r2 = r2.get(r1)
                boolean r2 = r4.equals(r2)
                if (r2 == 0) goto L_0x001a
                r0 = r1
            L_0x0019:
                return r0
            L_0x001a:
                int r1 = r1 + 1
                goto L_0x0004
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mapabc.mapapi.ItemizedOverlay.a.a(com.mapabc.mapapi.OverlayItem):int");
        }

        public final Item a(int i) {
            return (OverlayItem) this.f241a.get(i);
        }

        public final int b(int i) {
            return this.b.get(i).intValue();
        }

        public final int a(boolean z) {
            if (this.f241a.size() == 0) {
                return 0;
            }
            Iterator<Item> it = this.f241a.iterator();
            int i = Integer.MIN_VALUE;
            int i2 = Integer.MAX_VALUE;
            while (it.hasNext()) {
                GeoPoint point = ((OverlayItem) it.next()).getPoint();
                int latitudeE6 = z ? point.getLatitudeE6() : point.getLongitudeE6();
                if (latitudeE6 > i) {
                    i = latitudeE6;
                }
                if (latitudeE6 < i2) {
                    i2 = latitudeE6;
                }
            }
            return i - i2;
        }

        public final boolean a(GeoPoint geoPoint, MapView mapView) {
            boolean z = false;
            Projection projection = mapView.getProjection();
            Point pixels = projection.toPixels(geoPoint, null);
            int i = -1;
            double d = Double.MAX_VALUE;
            for (int i2 = 0; i2 < this.f241a.size(); i2++) {
                OverlayItem overlayItem = (OverlayItem) this.f241a.get(i2);
                double d2 = -1.0d;
                GeoPoint.b a2 = a(overlayItem, projection, pixels);
                Drawable drawable = overlayItem.mMarker;
                if (drawable == null) {
                    drawable = ItemizedOverlay.this.b;
                }
                if (ItemizedOverlay.this.hitTest(overlayItem, drawable, a2.f233a, a2.b)) {
                    GeoPoint.b a3 = a(overlayItem, projection, pixels);
                    d2 = (double) ((a3.b * a3.b) + (a3.f233a * a3.f233a));
                }
                if (d2 >= 0.0d && d2 < d) {
                    d = d2;
                    i = i2;
                }
            }
            if (-1 != i) {
                z = ItemizedOverlay.this.onTap(i);
            } else {
                ItemizedOverlay.this.setFocus(null);
            }
            mapView.getMediator().d.f();
            return z;
        }

        private static GeoPoint.b a(Item item, Projection projection, Point point) {
            Point pixels = projection.toPixels(item.getPoint(), null);
            return new GeoPoint.b(point.x - pixels.x, point.y - pixels.y);
        }
    }

    static Drawable a() {
        BitmapDrawable bitmapDrawable = new BitmapDrawable(ConfigableConst.d[ConfigableConst.ENUM_ID.emarker.ordinal()]);
        bitmapDrawable.setBounds(0, 0, bitmapDrawable.getIntrinsicWidth(), bitmapDrawable.getIntrinsicHeight());
        return bitmapDrawable;
    }
}
