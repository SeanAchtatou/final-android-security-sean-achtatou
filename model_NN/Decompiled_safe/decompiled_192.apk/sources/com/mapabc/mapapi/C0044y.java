package com.mapabc.mapapi;

/* renamed from: com.mapabc.mapapi.y  reason: case insensitive filesystem */
final class C0044y<T> extends C0008ab<T> {
    C0044y() {
    }

    public final void a(T t) {
        if (!this.f306a.contains(t)) {
            this.f306a.add(t);
        }
    }

    public final void b(T t) {
        for (int i = 0; i < this.f306a.size(); i++) {
            if (t == this.f306a.get(i)) {
                this.f306a.remove(i);
                return;
            }
        }
    }
}
