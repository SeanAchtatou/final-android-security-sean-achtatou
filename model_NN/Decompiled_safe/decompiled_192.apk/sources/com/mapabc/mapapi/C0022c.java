package com.mapabc.mapapi;

import java.io.IOException;
import java.io.InputStream;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.LinkedList;

/* renamed from: com.mapabc.mapapi.c  reason: case insensitive filesystem */
abstract class C0022c extends aC<aN, ArrayList<Route>> {

    /* renamed from: a  reason: collision with root package name */
    protected GeoPoint f315a;
    protected GeoPoint b;
    private String g;
    private String h;

    /* access modifiers changed from: protected */
    public abstract void a(Route route);

    /* access modifiers changed from: protected */
    public abstract Segment b(InputStream inputStream) throws IOException;

    public C0022c(aN aNVar, Proxy proxy, String str, String str2) {
        super(aNVar, proxy, str, str2, null);
    }

    /* access modifiers changed from: private */
    /* renamed from: k */
    public ArrayList<Route> e(InputStream inputStream) throws IOException {
        ArrayList<Route> arrayList = new ArrayList<>();
        this.g = h(inputStream);
        this.f315a = c(inputStream);
        this.h = h(inputStream);
        this.b = c(inputStream);
        int f = f(inputStream);
        while (f > 0) {
            f--;
            f(inputStream);
            try {
                Route l = l(inputStream);
                if (l != null) {
                    l.mStartPlace = this.g;
                    l.mTargetPlace = this.h;
                    arrayList.add(l);
                }
            } catch (Exception e) {
            }
        }
        if (arrayList.size() != 0) {
            return arrayList;
        }
        throw new IOException();
    }

    private Route l(InputStream inputStream) throws IOException {
        int f = f(inputStream);
        f(inputStream);
        Route a2 = a(inputStream);
        LinkedList linkedList = new LinkedList();
        while (f > 0) {
            f--;
            f(inputStream);
            Segment b2 = b(inputStream);
            if (b2.mShapes.length != 0) {
                linkedList.add(b2);
            }
        }
        if (linkedList.size() == 0) {
            return null;
        }
        a2.mSegs = linkedList;
        a(a2);
        for (Segment segment : a2.mSegs) {
            segment.mRoute = a2;
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public Route a(InputStream inputStream) throws IOException {
        return new Route(((aN) this.d).b);
    }

    protected static GeoPoint c(InputStream inputStream) throws IOException {
        return new GeoPoint(f(inputStream), f(inputStream));
    }

    protected static GeoPoint[] d(InputStream inputStream) throws IOException {
        int f = f(inputStream);
        GeoPoint[] geoPointArr = new GeoPoint[f];
        for (int i = 0; i < f; i++) {
            geoPointArr[i] = c(inputStream);
        }
        return geoPointArr;
    }

    /* access modifiers changed from: protected */
    public final String[] e() {
        String[] strArr = new String[6];
        strArr[0] = a("sx", String.format("%f", Double.valueOf(W.c(((aN) this.d).f305a.f277a.getLongitudeE6()))));
        strArr[1] = a("sy", String.format("%f", Double.valueOf(W.c(((aN) this.d).f305a.f277a.getLatitudeE6()))));
        strArr[2] = a("ex", String.format("%f", Double.valueOf(W.c(((aN) this.d).f305a.b.getLongitudeE6()))));
        strArr[3] = a("ey", String.format("%f", Double.valueOf(W.c(((aN) this.d).f305a.b.getLatitudeE6()))));
        strArr[4] = a("off", String.format("%d", Integer.valueOf(((aN) this.d).f305a.c)));
        Object[] objArr = new Object[1];
        int i = ((aN) this.d).b;
        if (i >= 23) {
            i -= 23;
        } else if (i >= 10) {
            i -= 10;
        }
        objArr[0] = Integer.valueOf(i);
        strArr[5] = a("method", String.format("%d", objArr));
        return strArr;
    }
}
