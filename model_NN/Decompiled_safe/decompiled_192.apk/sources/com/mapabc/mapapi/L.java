package com.mapabc.mapapi;

import android.os.Handler;

class L implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ at f243a;

    L(at atVar) {
        this.f243a = atVar;
    }

    public final void run() {
        at.a(this.f243a);
        if (!this.f243a.g()) {
            this.f243a.c.removeCallbacks(this);
            Handler unused = this.f243a.c = null;
            this.f243a.a();
        } else if (GlobalStore.issIsRuning()) {
            long currentTimeMillis = System.currentTimeMillis();
            this.f243a.h();
            this.f243a.b();
            long currentTimeMillis2 = System.currentTimeMillis();
            if (currentTimeMillis2 - currentTimeMillis < ((long) this.f243a.b)) {
                try {
                    Thread.sleep(((long) this.f243a.b) - (currentTimeMillis2 - currentTimeMillis));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
