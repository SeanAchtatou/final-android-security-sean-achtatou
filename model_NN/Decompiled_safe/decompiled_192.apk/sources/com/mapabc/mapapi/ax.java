package com.mapabc.mapapi;

import android.location.Address;
import java.lang.reflect.Method;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.Iterator;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

final class ax extends P<V, Address> {

    /* renamed from: a  reason: collision with root package name */
    private String f312a = null;
    private String b = null;
    private String g = null;
    private int h = 0;
    private ArrayList<Address> i;
    private ArrayList<Address> j;
    private ArrayList<Address> k;
    private boolean l;
    private boolean m;

    public ax(V v, Proxy proxy, String str, String str2) {
        super(v, proxy, str, str2);
        this.h = v.c;
        this.i = new ArrayList<>();
        this.j = new ArrayList<>();
        this.k = new ArrayList<>();
    }

    /* access modifiers changed from: protected */
    public final String[] e() {
        int i2;
        String[] strArr = new String[5];
        strArr[0] = a("x", String.format("%f", Double.valueOf(((V) this.d).f287a)));
        strArr[1] = a("y", String.format("%f", Double.valueOf(((V) this.d).b)));
        strArr[2] = a("poinum", String.format("%d", Integer.valueOf(((V) this.d).c)));
        strArr[3] = a("range", String.format("%d", 500));
        Object[] objArr = new Object[1];
        if (((V) this.d).d) {
            i2 = 1;
        } else {
            i2 = 0;
        }
        objArr[0] = Integer.valueOf(i2);
        strArr[4] = a("trans", String.format("%d", objArr));
        return strArr;
    }

    /* access modifiers changed from: protected */
    public final int f() {
        return 17;
    }

    /* access modifiers changed from: protected */
    public final void a(Node node, ArrayList<Address> arrayList) {
        String nodeName = node.getNodeName();
        if (nodeName.equals("province")) {
            this.b = b(node);
        } else if (nodeName.equals("city")) {
            this.f312a = b(node);
        } else if (nodeName.equals("district")) {
            this.g = b(node);
        } else if (nodeName.equals("roadlist")) {
            a(node, "road");
        } else if (nodeName.equals("poilist")) {
            a(node, "poi");
        } else if (nodeName.equals("crosslist")) {
            a(node, "cross");
            if (this.h > 0) {
                if (this.m) {
                    arrayList.add(this.k.get(0));
                }
                a(arrayList, this.j);
                if (this.h - arrayList.size() != 0) {
                    a(arrayList, this.i);
                } else if (this.l) {
                    arrayList.set(arrayList.size() - 1, this.i.get(0));
                }
            }
        }
    }

    private String b(Node node) {
        NodeList childNodes = node.getChildNodes();
        for (int i2 = 0; i2 < childNodes.getLength(); i2++) {
            Node item = childNodes.item(i2);
            if (item.getNodeType() == 1 && item.getNodeName().equals("name")) {
                return a(item);
            }
        }
        return null;
    }

    private void a(Node node, String str) {
        Class<String> cls = String.class;
        if (this.h > 0) {
            NodeList childNodes = node.getChildNodes();
            int min = Math.min(this.h, childNodes.getLength());
            for (int i2 = 0; i2 < min; i2++) {
                Node item = childNodes.item(i2);
                if (item.getNodeType() == 1) {
                    String nodeName = item.getNodeName();
                    if (nodeName.equals(str)) {
                        Address a2 = a(item, nodeName.equals("cross"));
                        if (nodeName.equals("road")) {
                            try {
                                Method method = a2.getClass().getMethod("setPremises", String.class);
                                if (method != null) {
                                    method.invoke(a2, Geocoder.Street_Road);
                                }
                            } catch (Exception e) {
                            }
                            if (a2 != null) {
                                this.i.add(a2);
                                this.l = true;
                            }
                        } else if (nodeName.equals("poi")) {
                            try {
                                Method method2 = a2.getClass().getMethod("setPremises", String.class);
                                if (method2 != null) {
                                    method2.invoke(a2, Geocoder.POI);
                                }
                            } catch (Exception e2) {
                            }
                            if (a2 != null) {
                                this.j.add(a2);
                            }
                        } else if (nodeName.equals("cross") && a2 != null) {
                            this.k.add(a2);
                            this.m = true;
                        }
                    }
                }
            }
        }
    }

    private void a(ArrayList<Address> arrayList, ArrayList<Address> arrayList2) {
        int size = arrayList2.size();
        int size2 = this.h - arrayList.size();
        for (int i2 = 0; i2 < size2; i2++) {
            if (size > i2) {
                arrayList.add(arrayList2.get(i2));
            }
        }
    }

    private Address a(Node node, boolean z) {
        if (!node.hasChildNodes()) {
            return null;
        }
        Address d = W.d();
        NodeList childNodes = node.getChildNodes();
        for (int i2 = 0; i2 < childNodes.getLength(); i2++) {
            Node item = childNodes.item(i2);
            String nodeName = item.getNodeName();
            String a2 = a(item);
            if (item.getNodeType() == 1 && !W.a(a2)) {
                if (z) {
                    try {
                        Method method = d.getClass().getMethod("setPremises", String.class);
                        if (method != null) {
                            method.invoke(d, Geocoder.Cross);
                        }
                    } catch (Exception e) {
                    }
                    if (nodeName.equals("name1")) {
                        d.setFeatureName("交叉路口" + a2 + "  ");
                        d.setAddressLine(0, a2);
                    } else if (nodeName.equals("name2")) {
                        String featureName = d.getFeatureName();
                        if (featureName != null) {
                            d.setFeatureName(featureName + a2);
                            d.setAddressLine(1, a2);
                        }
                    } else {
                        a(nodeName, a2, d);
                    }
                } else if (nodeName.equals("name")) {
                    d.setFeatureName(a2);
                    d.setAddressLine(0, a2);
                } else {
                    a(nodeName, a2, d);
                }
            }
        }
        return d;
    }

    private static void a(String str, String str2, Address address) {
        if (str.equals("x")) {
            address.setLongitude(Double.parseDouble(str2));
        } else if (str.equals("y")) {
            address.setLatitude(Double.parseDouble(str2));
        }
    }

    /* access modifiers changed from: protected */
    public final void a(ArrayList<Address> arrayList) {
        if (this.b != null && arrayList.size() == 0) {
            arrayList.add(W.d());
        }
        Iterator<Address> it = arrayList.iterator();
        while (it.hasNext()) {
            Address next = it.next();
            next.setAdminArea(this.b);
            next.setLocality(this.f312a);
            try {
                Method method = next.getClass().getMethod("setSubLocality", String.class);
                if (method != null) {
                    method.invoke(next, this.g);
                }
            } catch (Exception e) {
            }
        }
    }
}
