package com.mapabc.mapapi;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

/* renamed from: com.mapabc.mapapi.h  reason: case insensitive filesystem */
final class C0027h extends C0032m {
    protected C0027h(Context context, int i) {
        super(context, i);
    }

    /* access modifiers changed from: protected */
    public final boolean a(String str) {
        for (String compareToIgnoreCase : this.f322a.databaseList()) {
            if (compareToIgnoreCase.compareToIgnoreCase(str) == 0) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final String a() {
        return Environment.getDataDirectory().getAbsolutePath();
    }

    /* access modifiers changed from: protected */
    public final SQLiteDatabase b(String str) {
        return this.f322a.openOrCreateDatabase(str, 0, null);
    }
}
