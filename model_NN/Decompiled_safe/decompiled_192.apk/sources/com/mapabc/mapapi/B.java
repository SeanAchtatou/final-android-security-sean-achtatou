package com.mapabc.mapapi;

import android.view.View;
import com.mapabc.mapapi.MapView;

class B implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MapView.e f223a;

    B(MapView.e eVar) {
        this.f223a = eVar;
    }

    public final void onClick(View view) {
        int i = -1;
        int i2 = 0;
        while (true) {
            if (i2 >= 4) {
                break;
            } else if (this.f223a.f256a[i2].equals(view)) {
                i = i2;
                break;
            } else {
                i2++;
            }
        }
        if (this.f223a.c != null) {
            this.f223a.c.a(i);
        }
    }
}
