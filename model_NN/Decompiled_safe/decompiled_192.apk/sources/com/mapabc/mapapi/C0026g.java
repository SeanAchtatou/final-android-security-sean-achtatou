package com.mapabc.mapapi;

/* renamed from: com.mapabc.mapapi.g  reason: case insensitive filesystem */
final class C0026g {

    /* renamed from: a  reason: collision with root package name */
    public int f318a = -1;
    public String b = "";
    public long c = -1;
    public int d = -1;
    public int e = -1;

    C0026g() {
    }

    public final String toString() {
        return String.format("%d|%d|%d|%d|%s", Integer.valueOf(this.f318a), Long.valueOf(this.c), Integer.valueOf(this.d), Integer.valueOf(this.e), this.b);
    }
}
