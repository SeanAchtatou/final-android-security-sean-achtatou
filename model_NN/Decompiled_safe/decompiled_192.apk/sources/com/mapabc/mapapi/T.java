package com.mapabc.mapapi;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

final class T extends at {
    private Bitmap[] c;
    private Rect d = new Rect(0, 0, this.c[0].getWidth(), this.c[0].getHeight());
    private int e = 0;
    private Mediator f;

    public T(Mediator mediator, Bitmap[] bitmapArr) {
        super(-1, 1000);
        this.c = bitmapArr;
        this.f = mediator;
    }

    public final int c() {
        return this.c[0].getWidth();
    }

    public final int d() {
        return this.c[0].getHeight();
    }

    public final void a(Canvas canvas, int i, int i2) {
        int width = this.d.width() / 2;
        int height = this.d.height() / 2;
        this.d.set(i - width, i2 - height, width + i, height + i2);
        this.e++;
        if (this.e >= this.c.length) {
            this.e = 0;
        }
        canvas.drawBitmap(this.c[this.e], (float) this.d.left, (float) this.d.top, (Paint) null);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.e++;
        if (this.e >= this.c.length) {
            this.e = 0;
        }
        this.f.d.b(this.d.left, this.d.top, this.d.right, this.d.bottom);
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }
}
