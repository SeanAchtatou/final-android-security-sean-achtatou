package com.mapabc.mapapi;

import android.content.Context;

/* renamed from: com.mapabc.mapapi.ad  reason: case insensitive filesystem */
abstract class C0010ad {

    /* renamed from: a  reason: collision with root package name */
    protected C0032m f307a = null;
    protected Context b;
    protected String c;
    protected boolean d = false;

    public abstract boolean a();

    public abstract void c();

    protected C0010ad(Context context) {
        this.b = context;
        this.c = this.b.getPackageName();
    }

    public final synchronized void b() {
    }
}
