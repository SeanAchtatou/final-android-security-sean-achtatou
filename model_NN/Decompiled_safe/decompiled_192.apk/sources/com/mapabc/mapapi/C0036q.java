package com.mapabc.mapapi;

import com.mapabc.mapapi.Mediator;

/* renamed from: com.mapabc.mapapi.q  reason: case insensitive filesystem */
class C0036q implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ Mediator.b f325a;

    C0036q(Mediator.b bVar) {
        this.f325a = bVar;
    }

    public final void run() {
        if (Mediator.this.f258a != null && this.f325a.c != null) {
            this.f325a.c.postDelayed(this.f325a.d, 3000);
            Mediator.b bVar = this.f325a;
            if (Mediator.this.d.i) {
                Mediator.this.d.f();
            }
            bVar.f261a++;
            if (bVar.f261a >= 20 && bVar.f261a % 20 == 0) {
                for (C0028i d : Mediator.this.e.f) {
                    d.d();
                }
            }
        }
    }
}
