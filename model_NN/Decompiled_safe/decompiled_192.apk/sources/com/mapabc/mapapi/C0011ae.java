package com.mapabc.mapapi;

import com.mapabc.mapapi.Tile;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.Proxy;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: com.mapabc.mapapi.ae  reason: case insensitive filesystem */
final class C0011ae extends aC<ArrayList<Tile.TileCoordinate>, ArrayList<Tile>> {

    /* renamed from: a  reason: collision with root package name */
    private boolean f308a;

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object a_() {
        ArrayList arrayList = new ArrayList();
        Iterator it = ((ArrayList) this.d).iterator();
        while (it.hasNext()) {
            arrayList.add(new Tile((Tile.TileCoordinate) it.next()));
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object e(InputStream inputStream) throws IOException {
        ArrayList arrayList = new ArrayList();
        a(f(inputStream) == 0);
        int g = g(inputStream);
        f(inputStream);
        int i = 0;
        for (int i2 = 0; i2 < g; i2++) {
            g(inputStream);
            g(inputStream);
            int f = f(inputStream);
            if (f > 16384) {
                return null;
            }
            byte[] bArr = new byte[f];
            i += f;
            j(inputStream);
            a(a(inputStream, bArr, f) == f);
            Tile tile = new Tile((Tile.TileCoordinate) ((ArrayList) this.d).get(i2), this.f308a);
            tile.c = bArr;
            tile.a(bArr, f);
            arrayList.add(tile);
        }
        GlobalStore.getInstance().addInfo(i, this.c, System.currentTimeMillis(), b());
        return arrayList;
    }

    public C0011ae(ArrayList<Tile.TileCoordinate> arrayList, Proxy proxy, String str, String str2, String str3, boolean z) {
        super(arrayList, proxy, str, str2, str3);
        this.f308a = z;
    }

    private static String b() {
        try {
            return InetAddress.getLocalHost().getHostAddress().toString();
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return "";
        }
    }

    private static int a(InputStream inputStream, byte[] bArr, int i) throws IOException {
        int i2 = 0;
        do {
            int read = inputStream.read(bArr, i2, i - i2);
            if (read > 0) {
                i2 += read;
            }
            if (read <= 0) {
                break;
            }
        } while (i2 < i);
        return i2;
    }

    /* access modifiers changed from: protected */
    public final String[] e() {
        String[] strArr = new String[3];
        int size = ((ArrayList) this.d).size();
        String str = "";
        for (int i = 0; i < size; i++) {
            str = str + ((Tile.TileCoordinate) ((ArrayList) this.d).get(i)).toString();
            if (i < size - 1) {
                str = str + ";";
            }
        }
        strArr[0] = a("g", str);
        strArr[1] = a("ua", this.e);
        strArr[2] = a("tr", this.f308a ? "true" : "false");
        return strArr;
    }

    /* access modifiers changed from: protected */
    public final int f() {
        return 4;
    }
}
