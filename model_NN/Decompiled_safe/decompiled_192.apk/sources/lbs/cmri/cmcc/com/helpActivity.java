package lbs.cmri.cmcc.com;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

public class helpActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.help);
        TextView textView = (TextView) findViewById(R.id.TextView01);
        String strCurrentCersion = "";
        try {
            strCurrentCersion = "当前版本：\n" + getPackageManager().getPackageInfo(getPackageName(), 0).versionName + "\n";
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        textView.setText(String.valueOf(strCurrentCersion) + getString(R.string.help));
        textView.setMovementMethod(ScrollingMovementMethod.getInstance());
        super.onCreate(savedInstanceState);
    }
}
