package lbs.cmri.cmcc.com;

public class TrackDistance {
    private float Distance = 0.0f;
    private String Time = "2011-01-01";

    public void SetTime(String strTime) {
        if (strTime != null && !"".equals(strTime)) {
            this.Time = strTime;
        }
    }

    public String GetTime() {
        return this.Time;
    }

    public void SetDistance(float fDistance) {
        this.Distance = fDistance;
    }

    public float GetDistance() {
        return this.Distance;
    }
}
