package lbs.cmri.cmcc.com;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;
import cmcc.location.core.LogUtil;
import cmcc.location.core.e;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import org.apache.commons.codec.binary.Base64;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class CommUtil {
    private static final String CDATA_SECTION_NODE_NAME = "#cdata-section";
    private static final String ENCODE = "ISO-8859-1";
    private static final String TEXT_NODE_NAME = "#text";
    private static SimpleDateFormat dateFormat;

    public static String getMIMEType(File f, boolean isOpen) {
        String type;
        String fName = f.getName();
        String end = fName.substring(fName.lastIndexOf(".") + 1, fName.length()).toLowerCase();
        if (isOpen) {
            if (end.equals("m4a") || end.equals("mp3") || end.equals("mid") || end.equals("xmf") || end.equals("ogg") || end.equals("wav")) {
                type = "audio";
            } else if (end.equals("3gp") || end.equals("mp4")) {
                type = "video";
            } else if (end.equals("jpg") || end.equals("gif") || end.equals("png") || end.equals("jpeg") || end.equals("bmp")) {
                type = "image";
            } else {
                type = "*";
            }
            return String.valueOf(type) + "/*";
        } else if (end.equals("m4a") || end.equals("mp3") || end.equals("mid") || end.equals("xmf") || end.equals("ogg") || end.equals("wav")) {
            return "audio";
        } else {
            if (end.equals("3gp") || end.equals("mp4")) {
                return "video";
            }
            if (end.equals("jpg") || end.equals("gif") || end.equals("png") || end.equals("jpeg") || end.equals("bmp")) {
                return "image";
            }
            if (end.equals("apk")) {
                return "apk";
            }
            return "";
        }
    }

    public static Bitmap fitSizePic(File f) {
        BitmapFactory.Options opts = new BitmapFactory.Options();
        if (f.length() < 20480) {
            opts.inSampleSize = 1;
        } else if (f.length() < 51200) {
            opts.inSampleSize = 2;
        } else if (f.length() < 307200) {
            opts.inSampleSize = 4;
        } else if (f.length() < 819200) {
            opts.inSampleSize = 6;
        } else if (f.length() < 1048576) {
            opts.inSampleSize = 8;
        } else {
            opts.inSampleSize = 10;
        }
        return BitmapFactory.decodeFile(f.getPath(), opts);
    }

    public static String fileSizeMsg(File f) {
        if (!f.isFile()) {
            return "";
        }
        long length = f.length();
        if (length >= 1073741824) {
            return String.valueOf((String.valueOf(((float) length) / 1.07374182E9f) + "000").substring(0, String.valueOf(((float) length) / 1.07374182E9f).indexOf(".") + 3)) + "GB";
        } else if (length >= 1048576) {
            return String.valueOf((String.valueOf(((float) length) / 1048576.0f) + "000").substring(0, String.valueOf(((float) length) / 1048576.0f).indexOf(".") + 3)) + "MB";
        } else if (length >= 1024) {
            return String.valueOf((String.valueOf(((float) length) / 1024.0f) + "000").substring(0, String.valueOf(((float) length) / 1024.0f).indexOf(".") + 3)) + "KB";
        } else if (length < 1024) {
            return String.valueOf(String.valueOf(length)) + "B";
        } else {
            return "";
        }
    }

    public static boolean checkDirPath(String newName) {
        if (newName.indexOf("\\") == -1) {
            return true;
        }
        return false;
    }

    public static boolean checkFilePath(String newName) {
        if (newName.indexOf("\\") == -1) {
            return true;
        }
        return false;
    }

    public static String getIMSI(Context ctx) {
        String str = ((TelephonyManager) ctx.getSystemService("phone")).getSubscriberId();
        if (str == null || "".equals(str)) {
            return "460001006020000";
        }
        return str;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v2, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v4, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v5, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String MD5(java.lang.String r7) {
        /*
            java.lang.String r6 = "MD5"
            java.security.MessageDigest r3 = java.security.MessageDigest.getInstance(r6)     // Catch:{ NoSuchAlgorithmException -> 0x0039 }
            byte[] r6 = r7.getBytes()     // Catch:{ NoSuchAlgorithmException -> 0x0039 }
            r3.update(r6)     // Catch:{ NoSuchAlgorithmException -> 0x0039 }
            byte[] r0 = r3.digest()     // Catch:{ NoSuchAlgorithmException -> 0x0039 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ NoSuchAlgorithmException -> 0x0039 }
            r5.<init>()     // Catch:{ NoSuchAlgorithmException -> 0x0039 }
            r4 = 0
            r2 = 0
        L_0x0018:
            int r6 = r0.length     // Catch:{ NoSuchAlgorithmException -> 0x0039 }
            if (r2 < r6) goto L_0x0020
            java.lang.String r6 = r5.toString()     // Catch:{ NoSuchAlgorithmException -> 0x0039 }
        L_0x001f:
            return r6
        L_0x0020:
            byte r4 = r0[r2]     // Catch:{ NoSuchAlgorithmException -> 0x0039 }
            if (r4 >= 0) goto L_0x0026
            int r4 = r4 + 256
        L_0x0026:
            r6 = 16
            if (r4 >= r6) goto L_0x002f
            java.lang.String r6 = "0"
            r5.append(r6)     // Catch:{ NoSuchAlgorithmException -> 0x0039 }
        L_0x002f:
            java.lang.String r6 = java.lang.Integer.toHexString(r4)     // Catch:{ NoSuchAlgorithmException -> 0x0039 }
            r5.append(r6)     // Catch:{ NoSuchAlgorithmException -> 0x0039 }
            int r2 = r2 + 1
            goto L_0x0018
        L_0x0039:
            r6 = move-exception
            r1 = r6
            r1.printStackTrace()
            r6 = 0
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: lbs.cmri.cmcc.com.CommUtil.MD5(java.lang.String):java.lang.String");
    }

    public static NodeList getNodeList(Document doc, String nodeName) {
        if (doc == null || nodeName == null || nodeName.equalsIgnoreCase("")) {
            return null;
        }
        return doc.getElementsByTagName(nodeName);
    }

    public static String getElementValue(Node node) {
        Node item;
        if (node == null) {
            return null;
        }
        String value = "";
        int count = node.getChildNodes().getLength();
        int i = 0;
        while (true) {
            if (i >= count) {
                break;
            }
            item = node.getChildNodes().item(i);
            String nodeName = item.getNodeName();
            if (nodeName.equalsIgnoreCase(TEXT_NODE_NAME) || nodeName.equals(CDATA_SECTION_NODE_NAME)) {
                value = item.getNodeValue();
            } else {
                i++;
            }
        }
        value = item.getNodeValue();
        return value;
    }

    public static Node searchNode(NodeList nl, String nodeName) {
        if (nl == null || nodeName == null || nodeName.equalsIgnoreCase("")) {
            return null;
        }
        int counter = nl.getLength();
        Node node = null;
        for (int i = 0; i < counter; i++) {
            node = nl.item(i);
            if (nodeName.equalsIgnoreCase(node.getNodeName())) {
                break;
            }
        }
        return node;
    }

    public static Node searchNode(Node node, String nodeName) {
        if (node == null || nodeName == null || nodeName.equalsIgnoreCase("")) {
            return null;
        }
        NodeList nl = node.getChildNodes();
        if (nl == null || nl.getLength() <= 0) {
            return null;
        }
        return searchNode(nl, nodeName);
    }

    public static HttpURLConnection getUrlConnectionNew(URL url, NetworkInfo mNetinfo) throws Exception {
        if (url == null || mNetinfo == null) {
            throw new IllegalArgumentException();
        }
        String netType = mNetinfo.getTypeName();
        String netSubtype = mNetinfo.getExtraInfo();
        if (netType.toUpperCase().equals("WIFI")) {
            return (HttpURLConnection) url.openConnection();
        }
        if (!netType.toUpperCase().equals("MOBILE")) {
            return null;
        }
        if (!netSubtype.toUpperCase().equals("CMWAP")) {
            return (HttpURLConnection) url.openConnection();
        }
        HttpURLConnection con = (HttpURLConnection) url.openConnection(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.0.0.172", 80)));
        con.setRequestProperty("Accept", "text/javascript, text/ecmascript, application/x-javascript, */*, text/x-vcard, text/x-vcalendar, image/gif, image/vnd.wap.wbmp,textnd.wap.wml,applicationnd.wap.xhtml+xml,textml,text/css,text/vnd.wap.wml,application/vnd.wap.xhtml+xml,text/html,text/css");
        con.setRequestProperty("User-Agent", getPhoneName());
        con.setRequestProperty("Content-Language", "en-CA");
        con.setRequestProperty("Connection", "Keep-Alive");
        con.setRequestProperty("Cache-Control", "no-cache");
        con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        con.setRequestProperty("Charset", "UTF-8");
        return con;
    }

    public static String PostRequest(HttpURLConnection con, String strBody) {
        String strResponse;
        if (con == null || con.getURL() == null) {
            return null;
        }
        try {
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setConnectTimeout(15000);
            con.setReadTimeout(30000);
            con.setRequestMethod("POST");
            byte[] data = strBody.getBytes("UTF-8");
            con.setRequestProperty("Content-Length", String.valueOf(data.length));
            con.setRequestProperty("Content-Type", e.f137do);
            OutputStream out = con.getOutputStream();
            DataOutputStream dout = new DataOutputStream(out);
            dout.write(data);
            dout.flush();
            dout.close();
            out.close();
            int state = con.getResponseCode();
            InputStream in = con.getInputStream();
            byte[] rspByte = new byte[512];
            StringBuffer sb = new StringBuffer();
            if (state == 200) {
                while (in.read(rspByte) > 0) {
                    sb.append(new String(rspByte).trim());
                }
                strResponse = sb.toString();
            } else {
                strResponse = null;
            }
            in.close();
            con.disconnect();
            return strResponse;
        } catch (Exception e) {
            Exception ex = e;
            LogUtil.getInstance().log("PostRequest 异常：" + ex.toString());
            return null;
        }
    }

    public static String GetResponse(HttpURLConnection con) {
        String strResponse;
        if (con == null || con.getURL() == null) {
            return "";
        }
        try {
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setConnectTimeout(15000);
            con.setReadTimeout(30000);
            con.setRequestMethod("GET");
            con.connect();
            int state = con.getResponseCode();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            InputStream in = con.getInputStream();
            byte[] rspByte = new byte[512];
            if (state == 200) {
                while (true) {
                    int chcount = in.read(rspByte);
                    if (chcount <= 0) {
                        break;
                    }
                    baos.write(rspByte, 0, chcount);
                }
                strResponse = new String(baos.toByteArray(), "UTF-8");
            } else {
                strResponse = null;
            }
            in.close();
            return strResponse;
        } catch (Exception e) {
            Exception ex = e;
            LogUtil.getInstance().log("GetResponse 异常：" + ex.toString());
            return null;
        }
    }

    public static String getPhoneName() {
        return String.valueOf(Build.BRAND) + "/" + Build.PRODUCT;
    }

    public static String Base64Encode(String s) {
        if (s == null) {
            return null;
        }
        new Base64();
        try {
            return new String(Base64.encodeBase64(s.getBytes("ISO-8859-1")));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String Base64Decode(String s) {
        if (s == null) {
            return null;
        }
        new Base64();
        try {
            return new String(Base64.decodeBase64(s.getBytes()), "ISO-8859-1");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String zipStr(String str) throws IOException {
        if (str == null || str.length() == 0) {
            return str;
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(out);
        zip.putNextEntry(new ZipEntry("0"));
        zip.write(str.getBytes());
        zip.closeEntry();
        zip.close();
        return out.toString("ISO-8859-1");
    }

    public static String unZipStr(String str) throws IOException {
        if (str == null || str.length() == 0) {
            return str;
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ZipInputStream gunzip = new ZipInputStream(new ByteArrayInputStream(str.getBytes("ISO-8859-1")));
        gunzip.getNextEntry();
        byte[] buffer = new byte[512];
        while (true) {
            int n = gunzip.read(buffer);
            if (n < 0) {
                return out.toString("ISO-8859-1");
            }
            out.write(buffer, 0, n);
        }
    }

    static {
        dateFormat = null;
        dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setLenient(false);
    }

    public static boolean isValidDate(String s) {
        try {
            dateFormat.parse(s);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
