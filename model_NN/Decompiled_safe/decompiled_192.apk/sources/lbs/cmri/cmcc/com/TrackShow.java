package lbs.cmri.cmcc.com;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Process;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import cmcc.location.core.LocatorAPI;
import cmcc.location.core.LogUtil;
import com.mapabc.mapapi.GeoPoint;
import com.mapabc.mapapi.ItemizedOverlay;
import com.mapabc.mapapi.MapActivity;
import com.mapabc.mapapi.MapController;
import com.mapabc.mapapi.MapView;
import com.mapabc.mapapi.Overlay;
import com.mapabc.mapapi.OverlayItem;
import com.mapabc.mapapi.Projection;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

public class TrackShow extends MapActivity {
    private static final int ACTIVITY_OPENKMLFILE = 1;
    private static final int ACTIVITY_TRACKLIST = 2;
    private static final String KML_COORDINATES = "coordinates";
    public static final int MODE_NORMAL = 0;
    private static boolean bLocServRuning = false;
    private double DEFAULT_LATITUDE = 39.895872d;
    private double DEFAULT_LONGITUDE = 116.345422d;
    private float FLATOFFSET = 0.001381939f;
    private float FLNGOFFSET = 0.006142259f;
    private int MSG_OPEN_TRACK = 4;
    /* access modifiers changed from: private */
    public int MSG_REG_OK = 5;
    /* access modifiers changed from: private */
    public int MSG_SHOW_LOCATION = 2;
    /* access modifiers changed from: private */
    public int MSG_SHOW_TOAST = 1;
    /* access modifiers changed from: private */
    public int MSG_UPDATE_SERVICE = 3;
    /* access modifiers changed from: private */
    public float accuracy = 0.0f;
    private ArrayList<Location> alLocFromKMLFile = null;
    private ArrayList<Location> alLocFromServer = null;
    private Thread checkUpdate = new Thread() {
        public void run() {
            String strApkURL;
            try {
                String strResponse = SendRequest(Globals.UpdateServiceURL, String.valueOf(String.valueOf(String.valueOf(String.valueOf("<?xml version='1.0' encoding='UTF-8'?> ") + "<ci>") + "<ua>" + CommUtil.getPhoneName() + "</ua>") + "<v>" + TrackShow.this.getPackageManager().getPackageInfo(TrackShow.this.getPackageName(), 0).versionName + "</v>") + "</ci>");
                if (strResponse != null && !"".equals(strResponse) && (strApkURL = GetApkURL(strResponse)) != null && !"".equals(strApkURL)) {
                    Message msg = new Message();
                    msg.what = TrackShow.this.MSG_UPDATE_SERVICE;
                    msg.obj = strApkURL;
                    msg.setTarget(TrackShow.this.mupdateHandler);
                    msg.sendToTarget();
                }
            } catch (Exception e) {
            }
        }

        private String SendRequest(String strURL, String strBody) {
            if (strURL == null || "".equals(strURL) || strBody == null || "".equals(strBody)) {
                return null;
            }
            try {
                URL url = new URL(strURL);
                ConnectivityManager conMan = (ConnectivityManager) TrackShow.this.getSystemService("connectivity");
                if (conMan == null) {
                    return null;
                }
                NetworkInfo info = conMan.getActiveNetworkInfo();
                if (info == null) {
                    return null;
                }
                HttpURLConnection con = CommUtil.getUrlConnectionNew(url, info);
                if (con == null) {
                    return null;
                }
                String strResponse = CommUtil.PostRequest(con, strBody);
                con.disconnect();
                return strResponse;
            } catch (Exception e) {
                Exception ex = e;
                LogUtil.getInstance().log("SendUpdateRequest Error:" + ex.toString());
                return null;
            }
        }

        private String GetApkURL(String strResponse) {
            NodeList nl;
            if (strResponse == null || "".equals(strResponse)) {
                return null;
            }
            try {
                Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(strResponse.getBytes()));
                if (doc == null || (nl = CommUtil.getNodeList(doc, "s")) == null || nl.getLength() <= 0) {
                    return null;
                }
                if (CommUtil.getElementValue(nl.item(0)).equals("0")) {
                    return null;
                }
                NodeList nlurl = CommUtil.getNodeList(doc, "url");
                String strURL = null;
                if (nlurl != null && nlurl.getLength() > 0) {
                    strURL = CommUtil.getElementValue(nlurl.item(0));
                }
                return strURL;
            } catch (Exception e) {
                LogUtil.getInstance().log("GetApkURL Error:" + e.toString());
                return null;
            }
        }
    };
    /* access modifiers changed from: private */
    public float fCurrentVelocity = 0.0f;
    /* access modifiers changed from: private */
    public float fTodayDistance = 0.0f;
    private int iZoomLevel = 17;
    public View.OnClickListener ibtConLocationClick = new View.OnClickListener() {
        public void onClick(View v) {
            TextView tvtemp = (TextView) TrackShow.this.findViewById(R.id.itv4);
            if (tvtemp.getText().toString().equals(TrackShow.this.getResources().getString(R.string.menu_StopRenewLocation))) {
                tvtemp.setText(TrackShow.this.getResources().getString(R.string.menu_RenewLocation));
                if (TrackShow.this.mContiniousLoc != null) {
                    TrackShow.this.mContiniousLoc.interrupt();
                    TrackShow.this.mContiniousLoc = null;
                    return;
                }
                return;
            }
            tvtemp.setText(TrackShow.this.getResources().getString(R.string.menu_StopRenewLocation));
            if (TrackShow.this.mContiniousLoc == null) {
                TrackShow.this.mContiniousLoc = new LocationThread(TrackShow.this, null);
                TrackShow.this.mContiniousLoc.start();
            }
        }
    };
    public View.OnClickListener ibtOpenTrackClick = new View.OnClickListener() {
        public void onClick(View v) {
            TrackShow.this.startActivityForResult(new Intent(TrackShow.this, MyFile.class), 1);
        }
    };
    public View.OnClickListener ibtShareTrackClick = new View.OnClickListener() {
        public void onClick(View v) {
            TrackShow.this.startActivity(new Intent(TrackShow.this, ShareTrackInputActivity.class));
        }
    };
    public View.OnClickListener ibtTrackListClick = new View.OnClickListener() {
        public void onClick(View v) {
            TrackShow.this.startActivityForResult(new Intent(TrackShow.this, ShareTrackListActivity.class), 2);
        }
    };
    private boolean isCalCenter = false;
    /* access modifiers changed from: private */
    public boolean isNeedZoom = true;
    /* access modifiers changed from: private */
    public boolean isShowTrack = true;
    ArrayList<Location> locListToday = null;
    ArrayList<Location> locListYesterday = null;
    private GeoPoint mCenter = null;
    /* access modifiers changed from: private */
    public Thread mContiniousLoc = new LocationThread(this, null);
    /* access modifiers changed from: private */
    public Runnable mDownloadUpdate = new Runnable() {
        public void run() {
            AlertDialog.Builder alert = new AlertDialog.Builder(TrackShow.this);
            alert.setTitle("更新").setMessage("检查到更新版本，请确认是否更新！").setPositiveButton("进行更新", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    LogUtil.getInstance().log("updateIntent");
                    Intent updateIntent = new Intent(TrackShow.this, UpdateService.class);
                    updateIntent.putExtra("URL", TrackShow.this.strApkUrl);
                    TrackShow.this.startService(updateIntent);
                }
            }).setNegativeButton("不更新", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alert.create().show();
        }
    };
    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            User usr;
            Location m_Location;
            super.handleMessage(msg);
            if (msg.what == TrackShow.this.MSG_SHOW_TOAST) {
                TrackShow.this.toastShowError((String) msg.obj);
            }
            if (msg.what == TrackShow.this.MSG_SHOW_LOCATION && (m_Location = (Location) msg.obj) != null) {
                float flon = (float) m_Location.getLongitude();
                float flat = (float) m_Location.getLatitude();
                TrackShow.this.accuracy = m_Location.getAccuracy();
                TrackShow.this.mPoint = new GeoPoint((int) (((double) flat) * 1000000.0d), (int) (((double) flon) * 1000000.0d));
                TrackShow.this.mPoint = GeoTrans.GPointOffset(TrackShow.this.mPoint);
                TrackShow.this.mHandler.post(TrackShow.this.mRunnable);
            }
            if (msg.what == TrackShow.this.MSG_REG_OK && (usr = (User) msg.obj) != null) {
                TrackData.getInstance(TrackShow.this).InsertUserInfo(usr);
            }
        }
    };
    private MapController mMapController = null;
    private MapView mMapView = null;
    /* access modifiers changed from: private */
    public GeoPoint mPoint = null;
    /* access modifiers changed from: private */
    public Runnable mRunnable = new Runnable() {
        public void run() {
            TrackShow.this.clearTrack();
            TrackShow.this.ShowCurPosition();
            TextView tv = (TextView) TrackShow.this.findViewById(R.id.tvDistance);
            tv.setText("行进距离约为：" + String.valueOf((int) TrackShow.this.fTodayDistance) + "米" + "速度约为:" + String.valueOf(((float) ((int) (TrackShow.this.fCurrentVelocity * 10.0f))) / 10.0f) + "公里/小时");
            tv.invalidate();
            if (TrackShow.this.isShowTrack) {
                TrackShow.this.showTrack();
            }
        }
    };
    public Handler mupdateHandler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == TrackShow.this.MSG_UPDATE_SERVICE) {
                TrackShow.this.strApkUrl = (String) msg.obj;
                TrackShow.this.mupdateHandler.post(TrackShow.this.mDownloadUpdate);
            }
        }
    };
    /* access modifiers changed from: private */
    public String strApkUrl = null;
    private Thread thReg = new Thread() {
        public void run() {
            User usr;
            try {
                String strResponse = SendRequest(Globals.RegisterServiceURL, String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf("<?xml version='1.0' encoding='UTF-8'?> ") + "<reg>") + "<md5>" + CommUtil.MD5(CommUtil.getIMSI(TrackShow.this)) + "</md5>") + "<email>xxxx@xxxx.com</email>") + "<pw>123456</pw>") + "</reg>");
                if (strResponse != null && !"".equals(strResponse) && (usr = GetUserInfoFromRes(strResponse)) != null && usr.getUser_id() > 0) {
                    Message msg = new Message();
                    msg.what = TrackShow.this.MSG_REG_OK;
                    msg.obj = usr;
                    msg.setTarget(TrackShow.this.mHandler);
                    msg.sendToTarget();
                }
            } catch (Exception e) {
            }
        }

        private String SendRequest(String strURL, String strBody) {
            if (strURL == null || "".equals(strURL) || strBody == null || "".equals(strBody)) {
                return null;
            }
            try {
                URL url = new URL(strURL);
                ConnectivityManager conMan = (ConnectivityManager) TrackShow.this.getSystemService("connectivity");
                if (conMan == null) {
                    return null;
                }
                NetworkInfo info = conMan.getActiveNetworkInfo();
                if (info == null) {
                    return null;
                }
                HttpURLConnection con = CommUtil.getUrlConnectionNew(url, info);
                if (con == null) {
                    return null;
                }
                String strResponse = CommUtil.PostRequest(con, strBody);
                con.disconnect();
                return strResponse;
            } catch (Exception e) {
                Exception ex = e;
                LogUtil.getInstance().log("SendUpdateRequest Error:" + ex.toString());
                return null;
            }
        }

        private User GetUserInfoFromRes(String strResponse) {
            Exception e;
            NodeList nl;
            if (strResponse == null || "".equals(strResponse)) {
                return null;
            }
            try {
                Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(strResponse.getBytes()));
                if (doc == null || (nl = CommUtil.getNodeList(doc, "s")) == null || nl.getLength() <= 0) {
                    return null;
                }
                if (CommUtil.getElementValue(nl.item(0)).equals("0")) {
                    LogUtil.getInstance().log("Error:" + CommUtil.getElementValue(CommUtil.getNodeList(doc, "err").item(0)));
                    return null;
                }
                User usr = new User();
                try {
                    NodeList nlTemp = CommUtil.getNodeList(doc, "uid");
                    if (nlTemp != null && nlTemp.getLength() > 0) {
                        usr.setUser_id(Integer.parseInt(CommUtil.getElementValue(nlTemp.item(0))));
                    }
                    NodeList nlTemp2 = CommUtil.getNodeList(doc, "pw");
                    if (nlTemp2 != null && nlTemp2.getLength() > 0) {
                        usr.setPassword(CommUtil.getElementValue(nlTemp2.item(0)));
                    }
                    NodeList nlTemp3 = CommUtil.getNodeList(doc, "imsi");
                    if (nlTemp3 != null && nlTemp3.getLength() > 0) {
                        usr.setImsi(CommUtil.getElementValue(nlTemp3.item(0)));
                    }
                    NodeList nlTemp4 = CommUtil.getNodeList(doc, "name");
                    if (nlTemp4 != null && nlTemp4.getLength() > 0) {
                        usr.setName(CommUtil.getElementValue(nlTemp4.item(0)));
                    }
                    NodeList nlTemp5 = CommUtil.getNodeList(doc, "email");
                    if (nlTemp5 != null && nlTemp5.getLength() > 0) {
                        usr.setEmail(CommUtil.getElementValue(nlTemp5.item(0)));
                    }
                    return usr;
                } catch (Exception e2) {
                    e = e2;
                    LogUtil.getInstance().log("GetUserInfoFromRes Failed " + e.toString());
                    return null;
                }
            } catch (Exception e3) {
                e = e3;
                LogUtil.getInstance().log("GetUserInfoFromRes Failed " + e.toString());
                return null;
            }
        }
    };

    private class LocationThread extends Thread {
        private boolean bcontinue;
        private int iInterval;
        private int iMaxVelocity;
        private long lLastLoc;
        private Location locLast;

        private LocationThread() {
            this.bcontinue = true;
            this.locLast = null;
            this.iInterval = 10000;
            this.iMaxVelocity = 100;
            this.lLastLoc = 0;
        }

        /* synthetic */ LocationThread(TrackShow trackShow, LocationThread locationThread) {
            this();
        }

        private boolean LocatioEqual(Location locA, Location locB) {
            if (locA == null || locB == null) {
                return false;
            }
            return locA.getLatitude() == locB.getLatitude() && locA.getLongitude() == locB.getLongitude() && locA.getAltitude() == locB.getAltitude();
        }

        public void run() {
            while (this.bcontinue) {
                try {
                    try {
                        Location m_Location = LocatorAPI.getInstance(TrackShow.this).fppLocate("1000", "1000110", "Point");
                        if (m_Location != null) {
                            if (!LocatioEqual(m_Location, this.locLast)) {
                                TrackData.getInstance(TrackShow.this).InsertLocation(m_Location);
                                if (this.locLast != null) {
                                    float fTemp = m_Location.distanceTo(this.locLast);
                                    float velocityTemp = (fTemp / 1000.0f) / ((((float) (System.currentTimeMillis() - this.lLastLoc)) / 1000.0f) / 3600.0f);
                                    TrackShow.this.fCurrentVelocity = velocityTemp;
                                    if (velocityTemp < ((float) this.iMaxVelocity)) {
                                        TrackShow trackShow = TrackShow.this;
                                        trackShow.fTodayDistance = trackShow.fTodayDistance + fTemp;
                                        this.lLastLoc = System.currentTimeMillis();
                                        this.locLast = m_Location;
                                        TrackData.getInstance(TrackShow.this).UpdateTodayDistance(TrackShow.this.fTodayDistance);
                                    } else {
                                        LogUtil.getInstance().log("Current Location deleted");
                                    }
                                } else {
                                    TrackShow.this.isNeedZoom = true;
                                    this.locLast = m_Location;
                                    this.lLastLoc = System.currentTimeMillis();
                                }
                            }
                            Message msg = new Message();
                            msg.what = TrackShow.this.MSG_SHOW_LOCATION;
                            msg.obj = m_Location;
                            msg.setTarget(TrackShow.this.mHandler);
                            msg.sendToTarget();
                        }
                        sleep((long) this.iInterval);
                    } catch (Exception e) {
                        Exception ex = e;
                        LogUtil.getInstance().log("LocationThread Get Location Failed!" + ex.toString());
                        Log.d("Exception", ex.toString());
                        return;
                    }
                } catch (Exception e2) {
                    Exception ex2 = e2;
                    LogUtil.getInstance().log("LocationThread Error" + ex2.toString());
                    Log.d("Exception", ex2.toString());
                    return;
                }
            }
        }
    }

    class MapTrack extends Overlay {
        private Drawable endMarker = null;
        private int mColor = 0;
        private List<Location> mMapTrack = null;
        private Drawable startMarker = null;

        public MapTrack(Context context, List<Location> mTrack, Drawable sMarker, Drawable eMarker) {
            if (mTrack != null && mTrack.size() > 0) {
                this.mMapTrack = mTrack;
                this.startMarker = sMarker;
                this.endMarker = eMarker;
            }
        }

        public void SetColor(int color) {
            this.mColor = color;
        }

        /* Debug info: failed to restart local var, previous not found, register: 12 */
        public void draw(Canvas canvas, MapView mapView, boolean shadow) {
            List<GeoPoint> lstPoint = new ArrayList<>(this.mMapTrack.size());
            for (int i = 0; i < this.mMapTrack.size(); i++) {
                lstPoint.add(GeoTrans.GPointOffset(new GeoPoint((int) (this.mMapTrack.get(i).getLatitude() * 1000000.0d), (int) (this.mMapTrack.get(i).getLongitude() * 1000000.0d))));
            }
            Projection projection = mapView.getProjection();
            Paint paintLine = new Paint();
            if (this.mColor != 0) {
                paintLine.setColor(this.mColor);
            } else {
                paintLine.setColor(-16776961);
            }
            paintLine.setAlpha(255);
            paintLine.setStyle(Paint.Style.STROKE);
            paintLine.setStrokeWidth(2.0f);
            for (int i2 = 0; i2 < lstPoint.size() - 1; i2++) {
                Point pointStart = projection.toPixels((GeoPoint) lstPoint.get(i2), null);
                Point pointEnd = projection.toPixels((GeoPoint) lstPoint.get(i2 + 1), null);
                canvas.drawLine((float) pointStart.x, (float) pointStart.y, (float) pointEnd.x, (float) pointEnd.y, paintLine);
            }
        }
    }

    class LocationPoint extends ItemizedOverlay<OverlayItem> {
        private List<OverlayItem> GeoList = new ArrayList();
        private float dAccuracy = 0.0f;
        private Context mContext;
        private GeoPoint mPoint = null;
        private Drawable marker;

        public LocationPoint(Context context, Drawable marker2, GeoPoint mpoint, float accuracy) {
            super(boundCenterBottom(marker2));
            this.marker = marker2;
            this.mContext = context;
            this.mPoint = mpoint;
            this.dAccuracy = accuracy;
            this.GeoList.add(new OverlayItem(this.mPoint, "定位误差" + String.valueOf((int) this.dAccuracy) + "米", ""));
            populate();
        }

        public void draw(Canvas canvas, MapView mapView, boolean shadow) {
            Projection projection = mapView.getProjection();
            for (int index = size() - 1; index >= 0; index--) {
                OverlayItem overLayItem = getItem(index);
                String title = overLayItem.getTitle();
                Point point = projection.toPixels(overLayItem.getPoint(), null);
                Paint paintCircle = new Paint();
                paintCircle.setColor(Color.rgb(0, 80, 190));
                paintCircle.setAlpha(50);
                paintCircle.setStyle(Paint.Style.FILL);
                canvas.drawCircle((float) point.x, (float) point.y, 5.0f, paintCircle);
                int iTemp = (int) projection.metersToEquatorPixels(this.dAccuracy);
                canvas.drawCircle((float) point.x, (float) point.y, (float) iTemp, paintCircle);
                paintCircle.setColor(-16776961);
                paintCircle.setAlpha(255);
                paintCircle.setStyle(Paint.Style.STROKE);
                paintCircle.setStrokeWidth(2.0f);
                canvas.drawCircle((float) point.x, (float) point.y, (float) iTemp, paintCircle);
                Paint paintText = new Paint();
                paintText.setColor(-65536);
                paintText.setTextSize(15.0f);
                canvas.drawText(title, (float) (point.x - 60), (float) (point.y + 20), paintText);
            }
            super.draw(canvas, mapView, shadow);
            boundCenterBottom(this.marker);
        }

        /* access modifiers changed from: protected */
        public OverlayItem createItem(int i) {
            return this.GeoList.get(i);
        }

        public int size() {
            return this.GeoList.size();
        }

        /* access modifiers changed from: protected */
        public boolean onTap(int i) {
            setFocus(this.GeoList.get(i));
            Toast.makeText(this.mContext, this.GeoList.get(i).getSnippet(), 0).show();
            return true;
        }
    }

    class NormalPoint extends ItemizedOverlay<OverlayItem> {
        private List<OverlayItem> GeoList = new ArrayList();
        private Context mContext;
        private GeoPoint mPoint = null;
        private Drawable marker;

        public NormalPoint(Context context, Drawable marker2, GeoPoint mpoint) {
            super(boundCenterBottom(marker2));
            this.marker = marker2;
            this.mContext = context;
            this.mPoint = mpoint;
            this.GeoList.add(new OverlayItem(this.mPoint, "", ""));
            populate();
        }

        public void draw(Canvas canvas, MapView mapView, boolean shadow) {
            boundCenterBottom(this.marker);
            super.draw(canvas, mapView, shadow);
        }

        /* access modifiers changed from: protected */
        public OverlayItem createItem(int i) {
            return this.GeoList.get(i);
        }

        public int size() {
            return this.GeoList.size();
        }
    }

    /* access modifiers changed from: private */
    public void toastShowError(String error) {
        Toast.makeText(this, error, 1).show();
    }

    public void onConfigurationChanged(Configuration newConfig) {
        setContentView((int) R.layout.main);
        super.onConfigurationChanged(newConfig);
    }

    private boolean processLocation() {
        try {
            Calendar cld_start = Calendar.getInstance(TimeZone.getTimeZone("GMT+8"));
            Location m_Location = LocatorAPI.getInstance(this).fppLocate("1000", "1000110", "Point");
            String str_Msg = String.valueOf("") + "定位耗时:" + String.valueOf(Calendar.getInstance(TimeZone.getTimeZone("GMT+8")).getTimeInMillis() - cld_start.getTimeInMillis()) + "秒" + "\r\n";
            if (m_Location != null) {
                double lng = m_Location.getLongitude();
                double lat = m_Location.getLatitude();
                double alt = m_Location.getAltitude();
                this.accuracy = m_Location.getAccuracy();
                String str_Msg2 = String.valueOf(String.valueOf(String.valueOf(String.valueOf(str_Msg) + "经度" + String.valueOf(lng) + "\n") + "纬度" + String.valueOf(lat) + "\n") + "高度" + String.valueOf(alt) + "\n") + "误差" + String.valueOf(this.accuracy) + "\n";
                LogUtil.getInstance().log(str_Msg2);
                TrackData.getInstance(this).InsertLocation(m_Location);
                Message msg = Message.obtain();
                msg.what = this.MSG_SHOW_TOAST;
                msg.obj = str_Msg2;
                this.mHandler.sendMessage(msg);
                this.mPoint = new GeoPoint((int) (((double) ((float) m_Location.getLatitude())) * 1000000.0d), (int) (((double) ((float) m_Location.getLongitude())) * 1000000.0d));
                this.mPoint = GeoTrans.GPointOffset(this.mPoint);
                this.mHandler.post(this.mRunnable);
                return true;
            }
            Message msg2 = Message.obtain();
            msg2.what = this.MSG_SHOW_TOAST;
            msg2.obj = new String("正在进行GPS定位......，请稍候！\n 首次定位需要在有GPS，请确保在室外开阔地带GPS卫星信号好的区域使用。 \n 另外阴雨天时也会影响到搜星速度。");
            this.mHandler.sendMessage(msg2);
            return false;
        } catch (Exception e) {
            Exception e2 = e;
            e2.printStackTrace();
            String error = e2.getMessage();
            Message msg3 = Message.obtain();
            msg3.what = this.MSG_SHOW_TOAST;
            msg3.obj = new String(error);
            this.mHandler.sendMessage(msg3);
            return false;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        if (!bLocServRuning && LocatorAPI.getInstance(this).init() == 0) {
            bLocServRuning = true;
        }
        ((LinearLayout) findViewById(R.id.ibtTrackList)).setOnClickListener(this.ibtTrackListClick);
        ((LinearLayout) findViewById(R.id.ibtShareTrack)).setOnClickListener(this.ibtShareTrackClick);
        ((LinearLayout) findViewById(R.id.ibtOpenTrack)).setOnClickListener(this.ibtOpenTrackClick);
        ((LinearLayout) findViewById(R.id.ibtLocation)).setOnClickListener(this.ibtConLocationClick);
        this.mMapView = (MapView) findViewById(R.id.atmapsView);
        if (this.mMapView != null) {
            this.mMapView.setBuiltInZoomControls(true);
            this.mMapController = this.mMapView.getController();
            this.mPoint = new GeoPoint((int) (this.DEFAULT_LATITUDE * 1000000.0d), (int) (this.DEFAULT_LONGITUDE * 1000000.0d));
            this.iZoomLevel = this.mMapView.getMaxZoomLevel() - 10;
            this.mMapController.setZoom(this.iZoomLevel);
            this.mMapController.setCenter(this.mPoint);
        }
        for (int i = 1; i < 99999; i++) {
        }
        this.fTodayDistance = TrackData.getInstance(this).GetTodayDistance();
        TextView tvDistance = (TextView) findViewById(R.id.tvDistance);
        tvDistance.setTextColor(-16776961);
        tvDistance.setBackgroundColor(-256);
        tvDistance.setText("行进距离约为:" + String.valueOf((int) this.fTodayDistance) + "米");
        this.locListToday = TrackData.getInstance(this).GetTodayTrackData();
        this.locListYesterday = TrackData.getInstance(this).GetYesterdayTrackData();
        if (!processLocation()) {
            this.isCalCenter = true;
            showTrack();
        } else {
            this.isNeedZoom = true;
        }
        User usr = TrackData.getInstance(this).GetUserInfo();
        if (usr == null || usr.getUser_id() <= 1000) {
            this.thReg.start();
        }
        if (bLocServRuning) {
            this.mContiniousLoc.start();
        }
        TrackData.getInstance(this).DeleteExpireTrackData();
        this.checkUpdate.start();
        super.onCreate(savedInstanceState);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        new AlertDialog.Builder(this).setTitle("退出").setMessage("你将退出我的轨迹应用，确定吗！").setNegativeButton("不退出", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        }).setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                TrackShow.this.finish();
            }
        }).show();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        LocatorAPI.getInstance(this).finit();
        Process.killProcess(Process.myPid());
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        if (inflater != null) {
            inflater.inflate(R.menu.mainmenu, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        String title = item.getTitle().toString();
        LogUtil.getInstance().log("Menu " + title + "Selected");
        switch (item.getItemId()) {
            case R.id.menu_item_SaveTrack:
                if (!TrackData.getInstance(this).SaveTrack2File()) {
                    Toast.makeText(this, "导出失败！ ", 1).show();
                    break;
                } else {
                    Toast.makeText(this, "导出成功，请到/sdcard/mytrack/目录中查看相应文件 ", 1).show();
                    break;
                }
            case R.id.menu_item_HowFar:
                startActivity(new Intent(this, ShowDistanceActivity.class));
                break;
            case R.id.menu_item_CurrentLocation:
                processLocation();
                break;
            case R.id.menu_item_DeleteTrack:
                this.alLocFromKMLFile = null;
                this.alLocFromServer = null;
                clearTrack();
                showTrack();
                ShowCurPosition();
                this.mMapView.invalidate();
                break;
            case R.id.menu_item_ShowTrackList:
                startActivityForResult(new Intent(this, ShareTrackListActivity.class), 2);
                break;
            case R.id.menu_item_ShareTrack:
                startActivity(new Intent(this, ShareTrackInputActivity.class));
                break;
            case R.id.menu_item_RenewLocation:
                if (!title.equals(getResources().getString(R.string.menu_StopRenewLocation))) {
                    item.setTitle(getResources().getString(R.string.menu_StopRenewLocation));
                    if (this.mContiniousLoc == null) {
                        this.mContiniousLoc = new LocationThread(this, null);
                        this.mContiniousLoc.start();
                        break;
                    }
                } else {
                    item.setTitle(getResources().getString(R.string.menu_RenewLocation));
                    if (this.mContiniousLoc != null) {
                        this.mContiniousLoc.interrupt();
                        this.mContiniousLoc = null;
                        break;
                    }
                }
                break;
            case R.id.menu_item_OpenTrack:
                startActivityForResult(new Intent(this, MyFile.class), 1);
                break;
            case R.id.menu_item_ShareApp:
                Intent itsend = new Intent("android.intent.action.SENDTO", Uri.parse("mmsto:"));
                itsend.putExtra("sms_body", "有一个有趣的应用，可看自己的轨迹，显示行进距离和速度，外出遛弯或者跑步时不妨试试， 还可和朋友分享你的跑步路线。下载地址： http://218.206.179.209:18080/mytrack/myTrack.apk");
                startActivity(itsend);
                break;
            case R.id.menu_item_Help:
                startActivity(new Intent(this, helpActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Throwable ex;
        if (resultCode == -1) {
            switch (requestCode) {
                case 1:
                    String strFileName = data.getStringExtra("FileName");
                    if (strFileName.toLowerCase().indexOf("kml") < 0) {
                        Toast.makeText(this, "请选择轨迹文件，扩展名为kml", 1).show();
                        return;
                    }
                    this.alLocFromKMLFile = OpenKmlFile(strFileName);
                    if (this.alLocFromKMLFile == null || this.alLocFromKMLFile.size() <= 0) {
                        Toast.makeText(this, "打开轨迹文件失败，请确认轨迹文件正确！", 1).show();
                        return;
                    }
                    showTrack(this.alLocFromKMLFile, Globals.KMLFileColor);
                    SetMapCenter(this.alLocFromKMLFile);
                    ShowStartEndPoint();
                    return;
                case 2:
                    String strTrackURL = data.getStringExtra("URL");
                    if (strTrackURL != null && !"".equals(strTrackURL)) {
                        String strKML = GetTrackFileFromServer(strTrackURL);
                        RandomAccessFile file = null;
                        try {
                            if (!TrackData.getInstance(this).hasSdcard()) {
                                Toast.makeText(this, "没有存储卡，无法保存轨迹文件", 1).show();
                                try {
                                    file.close();
                                    return;
                                } catch (Throwable th) {
                                    return;
                                }
                            } else {
                                File dir = new File("sdcard/myTrack/");
                                if (!dir.exists()) {
                                    dir.mkdirs();
                                }
                                File xmlFile = new File("sdcard/myTrack/" + ("FromServer_" + new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance(TimeZone.getTimeZone("GMT+8")).getTime()) + ".kml"));
                                if (xmlFile.exists()) {
                                    xmlFile.delete();
                                }
                                RandomAccessFile file2 = new RandomAccessFile(xmlFile, "rw");
                                try {
                                    file2.write(strKML.getBytes());
                                    file2.close();
                                    this.alLocFromServer = StrKML2Loclist(strKML);
                                    if (this.alLocFromServer == null || this.alLocFromServer.size() <= 0) {
                                        Toast.makeText(this, "打开轨迹文件失败，请确认轨迹文件正确！", 1).show();
                                    } else {
                                        showTrack(this.alLocFromServer, Globals.FromServColor);
                                        SetMapCenter(this.alLocFromServer);
                                        ShowStartEndPoint();
                                        TextView tvtemp = (TextView) findViewById(R.id.itv4);
                                        if (tvtemp.getText().toString().equals(getResources().getString(R.string.menu_StopRenewLocation))) {
                                            tvtemp.setText(getResources().getString(R.string.menu_RenewLocation));
                                            if (this.mContiniousLoc != null) {
                                                this.mContiniousLoc.interrupt();
                                                this.mContiniousLoc = null;
                                            }
                                        }
                                    }
                                    Toast.makeText(this, "从服务器下载轨迹文件成功！保存到" + xmlFile.getPath() + "\n 为了您能够看到共享的轨迹，\n 程序自动将自动位置更新功能关闭，\n 需要自动更新位置时，\n 请您手动开启位置更新功能!", 1).show();
                                    try {
                                        file2.close();
                                        return;
                                    } catch (Throwable th2) {
                                        return;
                                    }
                                } catch (Throwable th3) {
                                    th = th3;
                                    file = file2;
                                    try {
                                        file.close();
                                    } catch (Throwable th4) {
                                    }
                                    throw th;
                                }
                            }
                        } catch (Throwable th5) {
                            ex = th5;
                            try {
                                LogUtil.getInstance().log("创建文件失败" + ex.toString());
                                try {
                                    file.close();
                                    return;
                                } catch (Throwable th6) {
                                    return;
                                }
                            } catch (Throwable th7) {
                                th = th7;
                                file.close();
                                throw th;
                            }
                        }
                    } else {
                        return;
                    }
                    break;
                default:
                    return;
            }
        }
    }

    private String GetTrackFileFromServer(String strURL) {
        try {
            URL url = new URL(strURL);
            ConnectivityManager conMan = (ConnectivityManager) getSystemService("connectivity");
            if (conMan == null) {
                return null;
            }
            NetworkInfo info = conMan.getActiveNetworkInfo();
            if (info == null) {
                return null;
            }
            HttpURLConnection con = CommUtil.getUrlConnectionNew(url, info);
            if (con == null) {
                return null;
            }
            String strResponse = CommUtil.GetResponse(con);
            con.disconnect();
            return strResponse;
        } catch (Exception e) {
            Exception ex = e;
            LogUtil.getInstance().log("SendUpdateRequest Error:" + ex.toString());
            return null;
        }
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:25:0x008f=Splitter:B:25:0x008f, B:32:0x00bf=Splitter:B:32:0x00bf} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.ArrayList<android.location.Location> OpenKmlFile(java.lang.String r22) {
        /*
            r21 = this;
            if (r22 == 0) goto L_0x000e
            java.lang.String r16 = ""
            r0 = r16
            r1 = r22
            boolean r16 = r0.equals(r1)
            if (r16 == 0) goto L_0x0011
        L_0x000e:
            r16 = 0
        L_0x0010:
            return r16
        L_0x0011:
            r10 = 0
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            java.io.FileInputStream r11 = new java.io.FileInputStream     // Catch:{ IOException -> 0x008c, Exception -> 0x00bc }
            r0 = r11
            r1 = r22
            r0.<init>(r1)     // Catch:{ IOException -> 0x008c, Exception -> 0x00bc }
            javax.xml.parsers.DocumentBuilderFactory r5 = javax.xml.parsers.DocumentBuilderFactory.newInstance()     // Catch:{ IOException -> 0x0104, Exception -> 0x00ff, all -> 0x00fc }
            javax.xml.parsers.DocumentBuilder r4 = r5.newDocumentBuilder()     // Catch:{ IOException -> 0x0104, Exception -> 0x00ff, all -> 0x00fc }
            org.xml.sax.InputSource r9 = new org.xml.sax.InputSource     // Catch:{ IOException -> 0x0104, Exception -> 0x00ff, all -> 0x00fc }
            r9.<init>(r11)     // Catch:{ IOException -> 0x0104, Exception -> 0x00ff, all -> 0x00fc }
            r6 = 0
            org.w3c.dom.Document r6 = r4.parse(r9)     // Catch:{ IOException -> 0x0104, Exception -> 0x00ff, all -> 0x00fc }
            if (r6 == 0) goto L_0x0086
            java.lang.String r16 = "coordinates"
            r0 = r6
            r1 = r16
            org.w3c.dom.NodeList r14 = lbs.cmri.cmcc.com.CommUtil.getNodeList(r0, r1)     // Catch:{ IOException -> 0x0104, Exception -> 0x00ff, all -> 0x00fc }
            r8 = 0
        L_0x003d:
            int r16 = r14.getLength()     // Catch:{ IOException -> 0x0104, Exception -> 0x00ff, all -> 0x00fc }
            r0 = r8
            r1 = r16
            if (r0 < r1) goto L_0x004c
            r11.close()     // Catch:{ Throwable -> 0x00f3 }
        L_0x0049:
            r16 = r3
            goto L_0x0010
        L_0x004c:
            org.w3c.dom.Node r16 = r14.item(r8)     // Catch:{ IOException -> 0x0104, Exception -> 0x00ff, all -> 0x00fc }
            java.lang.String r15 = lbs.cmri.cmcc.com.CommUtil.getElementValue(r16)     // Catch:{ IOException -> 0x0104, Exception -> 0x00ff, all -> 0x00fc }
            java.lang.String r16 = ","
            java.lang.String[] r13 = r15.split(r16)     // Catch:{ IOException -> 0x0104, Exception -> 0x00ff, all -> 0x00fc }
            android.location.Location r12 = new android.location.Location     // Catch:{ IOException -> 0x0104, Exception -> 0x00ff, all -> 0x00fc }
            java.lang.String r16 = "kml"
            r0 = r12
            r1 = r16
            r0.<init>(r1)     // Catch:{ IOException -> 0x0104, Exception -> 0x00ff, all -> 0x00fc }
            r16 = 0
            r16 = r13[r16]     // Catch:{ IOException -> 0x0104, Exception -> 0x00ff, all -> 0x00fc }
            double r16 = java.lang.Double.parseDouble(r16)     // Catch:{ IOException -> 0x0104, Exception -> 0x00ff, all -> 0x00fc }
            r0 = r12
            r1 = r16
            r0.setLongitude(r1)     // Catch:{ IOException -> 0x0104, Exception -> 0x00ff, all -> 0x00fc }
            r16 = 1
            r16 = r13[r16]     // Catch:{ IOException -> 0x0104, Exception -> 0x00ff, all -> 0x00fc }
            double r16 = java.lang.Double.parseDouble(r16)     // Catch:{ IOException -> 0x0104, Exception -> 0x00ff, all -> 0x00fc }
            r0 = r12
            r1 = r16
            r0.setLatitude(r1)     // Catch:{ IOException -> 0x0104, Exception -> 0x00ff, all -> 0x00fc }
            r3.add(r12)     // Catch:{ IOException -> 0x0104, Exception -> 0x00ff, all -> 0x00fc }
            int r8 = r8 + 1
            goto L_0x003d
        L_0x0086:
            r11.close()     // Catch:{ Throwable -> 0x00f1 }
        L_0x0089:
            r16 = 0
            goto L_0x0010
        L_0x008c:
            r16 = move-exception
            r7 = r16
        L_0x008f:
            cmcc.location.core.LogUtil r16 = cmcc.location.core.LogUtil.getInstance()     // Catch:{ all -> 0x00ec }
            r17 = 1
            r0 = r17
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ all -> 0x00ec }
            r17 = r0
            r18 = 0
            java.lang.StringBuilder r19 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ec }
            java.lang.String r20 = "FileInputStream Error"
            r19.<init>(r20)     // Catch:{ all -> 0x00ec }
            java.lang.String r20 = r7.toString()     // Catch:{ all -> 0x00ec }
            java.lang.StringBuilder r19 = r19.append(r20)     // Catch:{ all -> 0x00ec }
            java.lang.String r19 = r19.toString()     // Catch:{ all -> 0x00ec }
            r17[r18] = r19     // Catch:{ all -> 0x00ec }
            r16.log(r17)     // Catch:{ all -> 0x00ec }
            r10.close()     // Catch:{ Throwable -> 0x00f6 }
        L_0x00b8:
            r16 = 0
            goto L_0x0010
        L_0x00bc:
            r16 = move-exception
            r7 = r16
        L_0x00bf:
            cmcc.location.core.LogUtil r16 = cmcc.location.core.LogUtil.getInstance()     // Catch:{ all -> 0x00ec }
            r17 = 1
            r0 = r17
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ all -> 0x00ec }
            r17 = r0
            r18 = 0
            java.lang.StringBuilder r19 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ec }
            java.lang.String r20 = "OpenKmlFile Error"
            r19.<init>(r20)     // Catch:{ all -> 0x00ec }
            java.lang.String r20 = r7.toString()     // Catch:{ all -> 0x00ec }
            java.lang.StringBuilder r19 = r19.append(r20)     // Catch:{ all -> 0x00ec }
            java.lang.String r19 = r19.toString()     // Catch:{ all -> 0x00ec }
            r17[r18] = r19     // Catch:{ all -> 0x00ec }
            r16.log(r17)     // Catch:{ all -> 0x00ec }
            r10.close()     // Catch:{ Throwable -> 0x00f8 }
        L_0x00e8:
            r16 = 0
            goto L_0x0010
        L_0x00ec:
            r16 = move-exception
        L_0x00ed:
            r10.close()     // Catch:{ Throwable -> 0x00fa }
        L_0x00f0:
            throw r16
        L_0x00f1:
            r16 = move-exception
            goto L_0x0089
        L_0x00f3:
            r16 = move-exception
            goto L_0x0049
        L_0x00f6:
            r16 = move-exception
            goto L_0x00b8
        L_0x00f8:
            r16 = move-exception
            goto L_0x00e8
        L_0x00fa:
            r17 = move-exception
            goto L_0x00f0
        L_0x00fc:
            r16 = move-exception
            r10 = r11
            goto L_0x00ed
        L_0x00ff:
            r16 = move-exception
            r7 = r16
            r10 = r11
            goto L_0x00bf
        L_0x0104:
            r16 = move-exception
            r7 = r16
            r10 = r11
            goto L_0x008f
        */
        throw new UnsupportedOperationException("Method not decompiled: lbs.cmri.cmcc.com.TrackShow.OpenKmlFile(java.lang.String):java.util.ArrayList");
    }

    private ArrayList<Location> StrKML2Loclist(String strKML) {
        if (strKML == null || "".equals(strKML)) {
            return null;
        }
        try {
            ArrayList<Location> alLoc = new ArrayList<>();
            Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(strKML.getBytes()));
            if (doc == null) {
                return null;
            }
            NodeList nl = CommUtil.getNodeList(doc, KML_COORDINATES);
            for (int i = 0; i < nl.getLength(); i++) {
                String[] locval = CommUtil.getElementValue(nl.item(i)).split(",");
                Location locTemp = new Location("kml");
                locTemp.setLongitude(Double.parseDouble(locval[0]));
                locTemp.setLatitude(Double.parseDouble(locval[1]));
                alLoc.add(locTemp);
            }
            return alLoc;
        } catch (Exception e) {
            Exception e2 = e;
            LogUtil.getInstance().log("StrKML2Loclist Error" + e2.toString());
            return null;
        }
    }

    /* access modifiers changed from: private */
    public void clearTrack() {
        this.mMapView.getOverlays().clear();
    }

    /* access modifiers changed from: private */
    public void ShowCurPosition() {
        Drawable marker = getResources().getDrawable(R.drawable.poi_point);
        marker.setBounds(0, 0, marker.getIntrinsicWidth(), marker.getIntrinsicHeight());
        this.mMapView.getOverlays().add(new LocationPoint(this, marker, this.mPoint, this.accuracy));
        this.mMapController.setCenter(this.mPoint);
        if (this.isNeedZoom) {
            this.isNeedZoom = false;
            this.mMapController = this.mMapView.getController();
            this.mMapController.setZoom(this.mMapView.getMaxZoomLevel() - 2);
        }
    }

    private void ShowStartEndPoint() {
        Drawable startMarker = getResources().getDrawable(R.drawable.start_32);
        Drawable endMarker = getResources().getDrawable(R.drawable.end_32);
        if (this.locListToday != null && this.locListToday.size() > 0) {
            this.mMapView.getOverlays().add(new NormalPoint(this, startMarker, GeoTrans.GPointOffset(new GeoPoint((int) (this.locListToday.get(0).getLatitude() * 1000000.0d), (int) (this.locListToday.get(0).getLongitude() * 1000000.0d)))));
        }
        if (this.locListYesterday != null && this.locListYesterday.size() > 0) {
            this.mMapView.getOverlays().add(new NormalPoint(this, startMarker, GeoTrans.GPointOffset(new GeoPoint((int) (this.locListYesterday.get(0).getLatitude() * 1000000.0d), (int) (this.locListYesterday.get(0).getLongitude() * 1000000.0d)))));
            this.mMapView.getOverlays().add(new NormalPoint(this, endMarker, GeoTrans.GPointOffset(new GeoPoint((int) (this.locListYesterday.get(this.locListYesterday.size() - 1).getLatitude() * 1000000.0d), (int) (this.locListYesterday.get(this.locListYesterday.size() - 1).getLongitude() * 1000000.0d)))));
        }
        if (this.alLocFromKMLFile != null && this.alLocFromKMLFile.size() > 0) {
            this.mMapView.getOverlays().add(new NormalPoint(this, startMarker, GeoTrans.GPointOffset(new GeoPoint((int) (this.alLocFromKMLFile.get(0).getLatitude() * 1000000.0d), (int) (this.alLocFromKMLFile.get(0).getLongitude() * 1000000.0d)))));
            this.mMapView.getOverlays().add(new NormalPoint(this, endMarker, GeoTrans.GPointOffset(new GeoPoint((int) (this.alLocFromKMLFile.get(this.alLocFromKMLFile.size() - 1).getLatitude() * 1000000.0d), (int) (this.alLocFromKMLFile.get(this.alLocFromKMLFile.size() - 1).getLongitude() * 1000000.0d)))));
        }
        if (this.alLocFromServer != null && this.alLocFromServer.size() > 0) {
            this.mMapView.getOverlays().add(new NormalPoint(this, startMarker, GeoTrans.GPointOffset(new GeoPoint((int) (this.alLocFromServer.get(0).getLatitude() * 1000000.0d), (int) (this.alLocFromServer.get(0).getLongitude() * 1000000.0d)))));
            this.mMapView.getOverlays().add(new NormalPoint(this, endMarker, GeoTrans.GPointOffset(new GeoPoint((int) (this.alLocFromServer.get(this.alLocFromServer.size() - 1).getLatitude() * 1000000.0d), (int) (this.alLocFromServer.get(this.alLocFromServer.size() - 1).getLongitude() * 1000000.0d)))));
        }
    }

    private void showTrack(ArrayList<Location> alLoc, int color) {
        if (alLoc != null && alLoc.size() != 0) {
            MapTrack mTrackTemp = new MapTrack(this, alLoc, getResources().getDrawable(R.drawable.start_32), getResources().getDrawable(R.drawable.end_32));
            mTrackTemp.SetColor(color);
            this.mMapView.getOverlays().add(mTrackTemp);
        }
    }

    /* access modifiers changed from: private */
    public void showTrack() {
        try {
            if (this.locListToday == null) {
                Toast.makeText(this, "没有今天的轨迹", 1).show();
            } else if (this.locListToday.size() > 0) {
                showTrack(this.locListToday, Globals.TodayColor);
                if (this.isCalCenter) {
                    this.isCalCenter = false;
                    SetMapCenter(this.locListToday);
                }
            }
            if (this.locListYesterday == null) {
                Toast.makeText(this, "无昨天的轨迹", 1).show();
            } else if (this.locListYesterday.size() > 0) {
                showTrack(this.locListYesterday, Globals.YesterdayColor);
                if (this.isCalCenter) {
                    this.isCalCenter = false;
                    SetMapCenter(this.locListToday);
                }
            }
            if (this.alLocFromKMLFile != null && this.alLocFromKMLFile.size() > 0) {
                showTrack(this.alLocFromKMLFile, Globals.KMLFileColor);
            }
            if (this.alLocFromServer != null && this.alLocFromServer.size() > 0) {
                showTrack(this.alLocFromServer, Globals.FromServColor);
            }
            ShowStartEndPoint();
        } catch (Exception e) {
            Exception ex = e;
            LogUtil.getInstance().log("showtrck Error" + ex.toString());
        }
    }

    private double GetMaxLat(ArrayList<Location> listLoc) {
        double dResult = -90.0d;
        if (listLoc != null && listLoc.size() > 0) {
            for (int i = 0; i < listLoc.size(); i++) {
                double dTemp = listLoc.get(i).getLatitude();
                if (dTemp > dResult) {
                    dResult = dTemp;
                }
            }
        }
        return dResult;
    }

    private double GetMinLat(ArrayList<Location> listLoc) {
        double dResult = 90.0d;
        if (listLoc != null && listLoc.size() > 0) {
            for (int i = 0; i < listLoc.size(); i++) {
                double dTemp = listLoc.get(i).getLatitude();
                if (dTemp < dResult) {
                    dResult = dTemp;
                }
            }
        }
        return dResult;
    }

    private double GetMinLng(ArrayList<Location> listLoc) {
        double dResult = 180.0d;
        if (listLoc != null && listLoc.size() > 0) {
            for (int i = 0; i < listLoc.size(); i++) {
                double dTemp = listLoc.get(i).getLongitude();
                if (dTemp < dResult) {
                    dResult = dTemp;
                }
            }
        }
        return dResult;
    }

    private int CalZoomLevel(double dMaxLat, double dMinLat, double dMaxLng, double dMinLng) {
        int iResult;
        Location location = new Location("Temp");
        location.setLatitude(dMinLat);
        location.setLongitude((dMinLng + dMaxLng) / 2.0d);
        Location location2 = new Location("Temp");
        location2.setLatitude(dMaxLat);
        location2.setLongitude((dMinLng + dMaxLng) / 2.0d);
        float fLatDistance = location2.distanceTo(location);
        Location location3 = new Location("Temp");
        location3.setLatitude((dMinLat + dMaxLat) / 2.0d);
        location3.setLongitude(dMaxLng);
        Location location4 = new Location("Temp");
        location4.setLatitude((dMinLat + dMaxLat) / 2.0d);
        location4.setLongitude(dMinLng);
        float fLngDistance = location4.distanceTo(location3);
        if (fLatDistance < 1000.0f) {
            fLatDistance = 1000.0f;
        }
        if (fLngDistance < 1000.0f) {
            fLngDistance = 1000.0f;
        }
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int iLatLevel = this.mMapView.getMaxZoomLevel();
        int iLngLevel = this.mMapView.getMaxZoomLevel();
        int i = this.mMapView.getMaxZoomLevel();
        while (true) {
            if (i <= 1) {
                break;
            }
            this.mMapController = this.mMapView.getController();
            this.mMapController.setZoom(i);
            if (this.mMapView.getProjection().metersToEquatorPixels(fLatDistance) < ((float) dm.widthPixels)) {
                iLatLevel = i;
                break;
            }
            i--;
        }
        int i2 = this.mMapView.getMaxZoomLevel();
        while (true) {
            if (i2 <= 1) {
                break;
            }
            this.mMapController = this.mMapView.getController();
            this.mMapController.setZoom(i2);
            if (this.mMapView.getProjection().metersToEquatorPixels(fLngDistance) < ((float) dm.heightPixels)) {
                iLngLevel = i2;
                break;
            }
            i2--;
        }
        int maxZoomLevel = this.mMapView.getMaxZoomLevel();
        if (iLngLevel >= iLatLevel) {
            iResult = iLatLevel;
        } else {
            iResult = iLngLevel;
        }
        return iResult - 1;
    }

    private double GetMaxLng(ArrayList<Location> listLoc) {
        double dResult = -180.0d;
        if (listLoc != null && listLoc.size() > 0) {
            for (int i = 0; i < listLoc.size(); i++) {
                double dTemp = listLoc.get(i).getLongitude();
                if (dTemp > dResult) {
                    dResult = dTemp;
                }
            }
        }
        return dResult;
    }

    private void SetMapCenter(ArrayList<Location> listLoc) {
        if (listLoc != null && listLoc.size() > 0) {
            double dMaxLat = GetMaxLat(listLoc);
            double dMinLat = GetMinLat(listLoc);
            double dMaxLng = GetMaxLng(listLoc);
            double dMinLng = GetMinLng(listLoc);
            double d = dMaxLat - dMinLat;
            double d2 = dMaxLng - dMinLng;
            this.mCenter = new GeoPoint((int) (1000000.0d * ((dMaxLat + dMinLat) / 2.0d)), (int) (1000000.0d * ((dMaxLng + dMinLng) / 2.0d)));
            this.mCenter = GeoTrans.GPointOffset(this.mCenter);
            this.mMapController = this.mMapView.getController();
            this.mMapController.setZoom(CalZoomLevel(dMaxLat, dMinLat, dMaxLng, dMinLng));
            this.mMapController.animateTo(this.mCenter);
        }
    }
}
