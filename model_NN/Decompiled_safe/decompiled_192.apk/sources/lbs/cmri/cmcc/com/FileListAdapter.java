package lbs.cmri.cmcc.com;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.io.File;
import java.util.List;

public class FileListAdapter extends BaseAdapter {
    private int isZoom = 0;
    private List<String> items;
    private Bitmap mIcon_apk;
    private Bitmap mIcon_audio;
    private Bitmap mIcon_file;
    private Bitmap mIcon_folder;
    private Bitmap mIcon_image;
    private Bitmap mIcon_video;
    private LayoutInflater mInflater;
    private List<String> paths;
    private List<String> sizes;

    public FileListAdapter(Context context, List<String> it, List<String> pa, List<String> si, int zm) {
        this.mInflater = LayoutInflater.from(context);
        this.items = it;
        this.paths = pa;
        this.sizes = si;
        this.isZoom = zm;
        this.mIcon_folder = BitmapFactory.decodeResource(context.getResources(), R.drawable.folder);
        this.mIcon_file = BitmapFactory.decodeResource(context.getResources(), R.drawable.file);
        this.mIcon_image = BitmapFactory.decodeResource(context.getResources(), R.drawable.image);
        this.mIcon_audio = BitmapFactory.decodeResource(context.getResources(), R.drawable.audio);
        this.mIcon_video = BitmapFactory.decodeResource(context.getResources(), R.drawable.video);
        this.mIcon_apk = BitmapFactory.decodeResource(context.getResources(), R.drawable.apk);
    }

    public int getCount() {
        return this.items.size();
    }

    public Object getItem(int position) {
        return this.items.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup par) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = this.mInflater.inflate((int) R.layout.list_items, (ViewGroup) null);
            holder = new ViewHolder(this, null);
            holder.f_title = (TextView) convertView.findViewById(R.id.f_title);
            holder.f_text = (TextView) convertView.findViewById(R.id.f_text);
            holder.f_icon = (ImageView) convertView.findViewById(R.id.f_icon);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        File f = new File(this.paths.get(position).toString());
        holder.f_title.setText(f.getName());
        String f_type = CommUtil.getMIMEType(f, false);
        if (f.isDirectory()) {
            holder.f_icon.setImageBitmap(this.mIcon_folder);
            holder.f_text.setText("");
        } else {
            holder.f_text.setText(this.sizes.get(position));
            if ("image".equals(f_type)) {
                if (this.isZoom == 1) {
                    Bitmap bitMap = CommUtil.fitSizePic(f);
                    if (bitMap != null) {
                        holder.f_icon.setImageBitmap(bitMap);
                    } else {
                        holder.f_icon.setImageBitmap(this.mIcon_image);
                    }
                } else {
                    holder.f_icon.setImageBitmap(this.mIcon_image);
                }
            } else if ("audio".equals(f_type)) {
                holder.f_icon.setImageBitmap(this.mIcon_audio);
            } else if ("video".equals(f_type)) {
                holder.f_icon.setImageBitmap(this.mIcon_video);
            } else if ("apk".equals(f_type)) {
                holder.f_icon.setImageBitmap(this.mIcon_apk);
            } else {
                holder.f_icon.setImageBitmap(this.mIcon_file);
            }
        }
        return convertView;
    }

    private class ViewHolder {
        ImageView f_icon;
        TextView f_text;
        TextView f_title;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(FileListAdapter fileListAdapter, ViewHolder viewHolder) {
            this();
        }
    }
}
