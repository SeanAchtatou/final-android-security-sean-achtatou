package lbs.cmri.cmcc.com;

public class User {
    private String email = "";
    private String imsi = "";
    private String name = "";
    private String password = "123456";
    private int user_id = 1000;

    public int getUser_id() {
        return this.user_id;
    }

    public void setUser_id(int iUser_id) {
        this.user_id = iUser_id;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String strPassword) {
        this.password = strPassword;
    }

    public String getImsi() {
        return this.imsi;
    }

    public void setImsi(String strImsi) {
        this.imsi = strImsi;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String strName) {
        this.name = strName;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String strEmail) {
        this.email = strEmail;
    }
}
