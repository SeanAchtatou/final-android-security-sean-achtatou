package lbs.cmri.cmcc.com;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import cmcc.location.core.LogUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;

public class UpdateService extends Service {
    private static final int DOWNLOAD_COMPLETE = 0;
    private static final int DOWNLOAD_FAIL = 1;
    String strApkURL = null;
    private String strdownloadDir = "/sdcard/myTrack";
    private int titleId = 0;
    /* access modifiers changed from: private */
    public File updateDir = null;
    /* access modifiers changed from: private */
    public File updateFile = null;
    private String updateFilePath;
    /* access modifiers changed from: private */
    public Handler updateHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    Uri uri = Uri.fromFile(UpdateService.this.updateFile);
                    Intent intent = new Intent();
                    intent.addFlags(268435456);
                    intent.setAction("android.intent.action.VIEW");
                    intent.setDataAndType(uri, "application/vnd.android.package-archive");
                    UpdateService.this.updatePendingIntent = PendingIntent.getActivity(UpdateService.this, 0, intent, 0);
                    UpdateService.this.updateNotification.defaults = 1;
                    UpdateService.this.updateNotification.setLatestEventInfo(UpdateService.this, "myTrack", "下载完成。", UpdateService.this.updatePendingIntent);
                    UpdateService.this.updateNotificationManager.notify(0, UpdateService.this.updateNotification);
                    UpdateService.this.stopService(UpdateService.this.updateIntent);
                    UpdateService.this.startActivity(intent);
                    return;
                case 1:
                    UpdateService.this.updateNotification.setLatestEventInfo(UpdateService.this, "myTrack", "下载失败", UpdateService.this.updatePendingIntent);
                    UpdateService.this.updateNotificationManager.notify(0, UpdateService.this.updateNotification);
                    return;
                default:
                    UpdateService.this.stopService(UpdateService.this.updateIntent);
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public Intent updateIntent = null;
    /* access modifiers changed from: private */
    public Notification updateNotification = null;
    /* access modifiers changed from: private */
    public NotificationManager updateNotificationManager = null;
    /* access modifiers changed from: private */
    public PendingIntent updatePendingIntent = null;

    public int onStartCommand(Intent intent, int flags, int startId) {
        LogUtil.getInstance().log("更新进程已启动");
        this.strApkURL = intent.getStringExtra("URL");
        if (this.strApkURL == null || "".equals(this.strApkURL)) {
            return 0;
        }
        if ("mounted".equals(Environment.getExternalStorageState())) {
            this.updateDir = new File("/sdcard/", "mytrack");
            this.updateFile = new File(this.updateDir, "/myTrack.apk");
            this.updateFilePath = this.updateDir + "/myTrack.apk";
        }
        this.updateNotificationManager = (NotificationManager) getSystemService("notification");
        this.updateNotification = new Notification();
        this.updateIntent = new Intent(this, TrackShow.class);
        this.updatePendingIntent = PendingIntent.getActivity(this, 0, this.updateIntent, 0);
        this.updateNotification.icon = R.drawable.arrow_down_float;
        this.updateNotification.tickerText = "开始下载";
        this.updateNotification.setLatestEventInfo(this, "myTrack", "0%", this.updatePendingIntent);
        this.updateNotificationManager.notify(0, this.updateNotification);
        new Thread(new updateRunnable()).start();
        return super.onStartCommand(intent, flags, startId);
    }

    class updateRunnable implements Runnable {
        Message message;

        updateRunnable() {
            this.message = UpdateService.this.updateHandler.obtainMessage();
        }

        public void run() {
            try {
                if (!UpdateService.this.updateDir.exists()) {
                    UpdateService.this.updateDir.mkdirs();
                }
                if (!UpdateService.this.updateFile.exists()) {
                    UpdateService.this.updateFile.createNewFile();
                }
                if (UpdateService.this.strApkURL != null && !"".equals(UpdateService.this.strApkURL)) {
                    if (UpdateService.this.downloadUpdateFile(UpdateService.this.strApkURL, UpdateService.this.updateFile) > 0) {
                        LogUtil.getInstance().log("下载已完成");
                        this.message.what = 0;
                        UpdateService.this.updateHandler.sendMessage(this.message);
                        return;
                    }
                    this.message.what = 1;
                    UpdateService.this.updateHandler.sendMessage(this.message);
                }
            } catch (Exception e) {
                Exception ex = e;
                ex.printStackTrace();
                LogUtil.getInstance().log("updateRunnable失败：" + ex.toString());
                this.message.what = 1;
                UpdateService.this.updateHandler.sendMessage(this.message);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    public long downloadUpdateFile(String downloadUrl, File saveFile) throws Exception {
        int downloadCount = 0;
        long totalSize = 0;
        HttpURLConnection httpConnection = null;
        InputStream is = null;
        FileOutputStream fos = null;
        try {
            URL url = new URL(downloadUrl);
            ConnectivityManager conMan = (ConnectivityManager) getSystemService("connectivity");
            if (conMan == null) {
                if (httpConnection != null) {
                    httpConnection.disconnect();
                }
                if (is != null) {
                    is.close();
                }
                if (fos != null) {
                    fos.close();
                }
                return 0;
            }
            NetworkInfo info = conMan.getActiveNetworkInfo();
            if (info == null) {
                if (httpConnection != null) {
                    httpConnection.disconnect();
                }
                if (is != null) {
                    is.close();
                }
                if (fos != null) {
                    fos.close();
                }
                return 0;
            }
            httpConnection = getUrlConnectionNew(url, info);
            if (httpConnection == null) {
                if (httpConnection != null) {
                    httpConnection.disconnect();
                }
                if (is != null) {
                    is.close();
                }
                if (fos != null) {
                    fos.close();
                }
                return 0;
            }
            httpConnection.setRequestProperty("User-Agent", "PacificHttpClient");
            if (0 > 0) {
                httpConnection.setRequestProperty("RANGE", "bytes=" + 0 + "-");
            }
            httpConnection.setConnectTimeout(10000);
            httpConnection.setReadTimeout(20000);
            int updateTotalSize = httpConnection.getContentLength();
            if (httpConnection.getResponseCode() == 404) {
                throw new Exception("fail!");
            }
            is = httpConnection.getInputStream();
            FileOutputStream fileOutputStream = new FileOutputStream(saveFile, false);
            try {
                byte[] buffer = new byte[4096];
                do {
                    int readsize = is.read(buffer);
                    if (readsize <= 0) {
                        break;
                    }
                    fileOutputStream.write(buffer, 0, readsize);
                    totalSize += (long) readsize;
                    if (downloadCount == 0 || ((int) ((100 * totalSize) / ((long) updateTotalSize))) - 10 > downloadCount) {
                        downloadCount += 10;
                        this.updateNotification.setLatestEventInfo(this, "正在下载", String.valueOf((((int) totalSize) * 100) / updateTotalSize) + "%", this.updatePendingIntent);
                        this.updateNotificationManager.notify(0, this.updateNotification);
                    }
                } while (totalSize != ((long) updateTotalSize));
                if (httpConnection != null) {
                    httpConnection.disconnect();
                }
                if (is != null) {
                    is.close();
                }
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
                return totalSize;
            } catch (Throwable th) {
                th = th;
                fos = fileOutputStream;
                if (httpConnection != null) {
                    httpConnection.disconnect();
                }
                if (is != null) {
                    is.close();
                }
                if (fos != null) {
                    fos.close();
                }
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    /* Debug info: failed to restart local var, previous not found, register: 8 */
    public HttpURLConnection getUrlConnectionNew(URL url, NetworkInfo mNetinfo) throws Exception {
        if (url == null || mNetinfo == null) {
            throw new IllegalArgumentException();
        }
        String netType = mNetinfo.getTypeName();
        String netSubtype = mNetinfo.getExtraInfo();
        if (netType.toUpperCase().equals("WIFI")) {
            return (HttpURLConnection) url.openConnection();
        }
        if (!netType.toUpperCase().equals("MOBILE")) {
            return null;
        }
        if (!netSubtype.toUpperCase().equals("CMWAP")) {
            return (HttpURLConnection) url.openConnection();
        }
        HttpURLConnection con = (HttpURLConnection) url.openConnection(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.0.0.172", 80)));
        con.setRequestProperty("Accept", "");
        con.setRequestProperty("User-Agent", getPhoneName());
        con.setRequestProperty("Content-Language", "en-CA");
        con.setRequestProperty("Connection", "Keep-Alive");
        con.setRequestProperty("Cache-Control", "no-cache");
        con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        con.setRequestProperty("Charset", "UTF-8");
        return con;
    }

    public String getPhoneName() {
        return String.valueOf(Build.BRAND) + "/" + Build.PRODUCT;
    }
}
