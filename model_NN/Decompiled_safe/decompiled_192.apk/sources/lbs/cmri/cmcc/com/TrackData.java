package lbs.cmri.cmcc.com;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Location;
import android.os.Environment;
import cmcc.location.core.LogUtil;
import java.io.File;
import java.io.RandomAccessFile;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

public class TrackData extends SQLiteOpenHelper {
    private static String TABLENAME = "tbTRACK";
    private static String TABLENAME_DISTANCE = "tbDISTANCE";
    private static String TABLENAME_USER = "tbUSER";
    private static Context ctx = null;
    private static SQLiteDatabase db = null;
    private static TrackData self = null;

    TrackData(Context context) {
        super(context, "myTrackDB", (SQLiteDatabase.CursorFactory) null, 2);
    }

    public static TrackData getInstance(Context c) {
        if (self == null || db == null) {
            synchronized (TrackData.class) {
                self = new TrackData(c);
                ctx = c;
                db = self.getWritableDatabase();
                if (!self.tableIsExist(TABLENAME_DISTANCE)) {
                    db.execSQL("CREATE TABLE " + TABLENAME_DISTANCE + " ( " + " TIME TEXT NOT NULL, DISTANCE FLOAT NOT NULL);");
                }
                if (!self.tableIsExist(TABLENAME_USER)) {
                    db.execSQL("CREATE TABLE " + TABLENAME_USER + " ( " + " USER_ID INTEGER NOT NULL, PASSWORD TEXT, IMSI TEXT, NAME TEXT, EMAIL TEXT );");
                }
            }
        }
        return self;
    }

    private void initDB() {
        try {
            db.execSQL("CREATE TABLE " + TABLENAME + " ( " + "TIME TEXT NOT NULL, LATITUDE FLOAT NOT NULL, " + "LONGITUDE FLOAT NOT NULL, ALTITUDE FLOAT," + " ACCURACY FLOAT);");
            db.execSQL("CREATE TABLE " + TABLENAME_DISTANCE + " ( " + " TIME TEXT NOT NULL, DISTANCE FLOAT NOT NULL);");
            db.execSQL("CREATE TABLE " + TABLENAME_USER + " ( " + " USER_ID INTEGER NOT NULL, PASSWORD TEXT, IMSI TEXT, NAME TEXT, EMAIL TEXT );");
        } catch (Exception e) {
            Exception ex = e;
            LogUtil.getInstance().log("创建表失败" + ex.toString());
        }
    }

    public boolean tableIsExist(String tableName) {
        boolean result = false;
        if (tableName == null) {
            return false;
        }
        try {
            Cursor cursor = getReadableDatabase().rawQuery("SELECT COUNT(*) AS c FROM SQLITE_MASTER WHERE TYPE ='table' and NAME ='" + tableName.trim() + "' ", null);
            if (cursor.moveToNext() && cursor.getInt(0) > 0) {
                result = true;
            }
        } catch (Exception e) {
        }
        return result;
    }

    public ArrayList<Location> GetAllTrackData() {
        ArrayList<Location> locList = new ArrayList<>();
        Cursor cs = db.query(TABLENAME, null, null, null, null, null, null);
        if (cs == null) {
            return null;
        }
        cs.moveToFirst();
        while (!cs.isAfterLast()) {
            Location locTemp = new Location("TrackData");
            locTemp.setLatitude((double) cs.getFloat(cs.getColumnIndex("LATITUDE")));
            locTemp.setLongitude((double) cs.getFloat(cs.getColumnIndex("LONGITUDE")));
            locTemp.setAltitude((double) cs.getFloat(cs.getColumnIndex("ALTITUDE")));
            locTemp.setAccuracy(cs.getFloat(cs.getColumnIndex("ACCURACY")));
            locList.add(locTemp);
            cs.moveToNext();
        }
        cs.close();
        return locList;
    }

    public ArrayList<Location> GetTrackDataByDate(String strDate) {
        if (strDate == null || strDate.trim().equals("")) {
            return null;
        }
        ArrayList<Location> locList = new ArrayList<>();
        Cursor cs = db.query(TABLENAME, null, "TIME > '" + strDate + "' and TIME <= '" + strDate + " 23:59:59'", null, null, null, null);
        if (cs == null) {
            return null;
        }
        cs.moveToFirst();
        while (!cs.isAfterLast()) {
            Location locTemp = new Location("TrackData");
            locTemp.setLatitude((double) cs.getFloat(cs.getColumnIndex("LATITUDE")));
            locTemp.setLongitude((double) cs.getFloat(cs.getColumnIndex("LONGITUDE")));
            locTemp.setAltitude((double) cs.getFloat(cs.getColumnIndex("ALTITUDE")));
            locTemp.setAccuracy(cs.getFloat(cs.getColumnIndex("ACCURACY")));
            locList.add(locTemp);
            cs.moveToNext();
        }
        cs.close();
        return locList;
    }

    public ArrayList<Location> GetYesterdayTrackData() {
        Calendar cldTemp = Calendar.getInstance(TimeZone.getTimeZone("GMT+8"));
        cldTemp.set(6, cldTemp.get(6) - 1);
        return GetTrackDataByDate(new SimpleDateFormat("yyyy-MM-dd").format(cldTemp.getTime()));
    }

    public User GetUserInfo() {
        Cursor cs = db.query(TABLENAME_USER, null, "IMSI = '" + CommUtil.MD5(CommUtil.getIMSI(ctx)) + "'", null, null, null, null);
        if (cs == null || cs.getCount() == 0) {
            return null;
        }
        cs.moveToFirst();
        User user = new User();
        user.setUser_id(cs.getInt(cs.getColumnIndex("USER_ID")));
        user.setName(cs.getString(cs.getColumnIndex("NAME")));
        user.setPassword(cs.getString(cs.getColumnIndex("PASSWORD")));
        user.setEmail(cs.getString(cs.getColumnIndex("EMAIL")));
        user.setImsi(cs.getString(cs.getColumnIndex("IMSI")));
        cs.close();
        return user;
    }

    public boolean InsertUserInfo(User usr) {
        boolean bResult = false;
        if (usr == null || usr.getUser_id() < 1000) {
            return false;
        }
        ContentValues values = new ContentValues();
        values.put("USER_ID", Integer.valueOf(usr.getUser_id()));
        values.put("PASSWORD", usr.getPassword());
        values.put("IMSI", usr.getImsi());
        values.put("NAME", usr.getName());
        values.put("EMAIL", usr.getEmail());
        if (db.insert(TABLENAME_USER, null, values) > 0) {
            bResult = true;
        }
        return bResult;
    }

    public ArrayList<Location> GetTodayTrackData() {
        return GetTrackDataByDate(new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance(TimeZone.getTimeZone("GMT+8")).getTime()));
    }

    public boolean InsertLocation(Location loc) {
        if (loc == null) {
            return false;
        }
        ContentValues values = new ContentValues();
        values.put("TIME", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance(TimeZone.getTimeZone("GMT+8")).getTime()));
        values.put("LATITUDE", Double.valueOf(loc.getLatitude()));
        values.put("LONGITUDE", Double.valueOf(loc.getLongitude()));
        values.put("ALTITUDE", Double.valueOf(loc.getAltitude()));
        values.put("ACCURACY", Float.valueOf(loc.getAccuracy()));
        if (db.insert(TABLENAME, null, values) <= 0) {
            return false;
        }
        return true;
    }

    public boolean InsertTodayDistance(float fDistance) {
        ContentValues values = new ContentValues();
        values.put("TIME", new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance(TimeZone.getTimeZone("GMT+8")).getTime()));
        values.put("DISTANCE", Float.valueOf(fDistance));
        if (db.insert(TABLENAME_DISTANCE, null, values) <= 0) {
            return false;
        }
        return true;
    }

    public ArrayList<TrackDistance> GetAllDistance(int iNum) {
        ArrayList<TrackDistance> alDistance = new ArrayList<>();
        Cursor cs = db.query(TABLENAME_DISTANCE, null, null, null, null, null, "TIME DESC", String.valueOf(iNum));
        if (cs == null || cs.getCount() == 0) {
            return null;
        }
        cs.moveToFirst();
        while (!cs.isAfterLast()) {
            String strTemp = cs.getString(cs.getColumnIndex("TIME"));
            float fTemp = cs.getFloat(cs.getColumnIndex("DISTANCE"));
            TrackDistance tdTemp = new TrackDistance();
            tdTemp.SetDistance(fTemp);
            tdTemp.SetTime(strTemp);
            alDistance.add(tdTemp);
            cs.moveToNext();
        }
        cs.close();
        return alDistance;
    }

    public boolean UpdateTodayDistance(float fDistance) {
        if (GetTodayDistance() > 0.0f) {
            db.execSQL("UPDATE " + TABLENAME_DISTANCE + " SET DISTANCE = " + String.valueOf(fDistance) + " WHERE " + ("TIME = '" + new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance(TimeZone.getTimeZone("GMT+8")).getTime()) + "'"));
            return true;
        }
        InsertTodayDistance(fDistance);
        return true;
    }

    public float GetTodayDistance() {
        Cursor cs = db.query(TABLENAME_DISTANCE, null, "TIME = '" + new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance(TimeZone.getTimeZone("GMT+8")).getTime()) + "'", null, null, null, null);
        if (cs == null || cs.getCount() == 0) {
            return 0.0f;
        }
        cs.moveToFirst();
        float fDistance = cs.getFloat(cs.getColumnIndex("DISTANCE"));
        cs.close();
        return fDistance;
    }

    public boolean DeleteTrackDataByDate(String strExpireDate) {
        if (strExpireDate == null || strExpireDate.trim().equals("")) {
            return false;
        }
        db.execSQL("DELETE FROM " + TABLENAME + " WHERE TIME <='" + strExpireDate + " 23:59:59'");
        return true;
    }

    public boolean DeleteExpireTrackData() {
        Calendar cldTemp = Calendar.getInstance(TimeZone.getTimeZone("GMT+8"));
        cldTemp.set(6, cldTemp.get(6) - 2);
        db.execSQL("DELETE FROM " + TABLENAME + " WHERE TIME <='" + new SimpleDateFormat("yyyy-MM-dd").format(cldTemp.getTime()) + " 23:59:59'");
        return true;
    }

    public void closeDB() {
        if (db != null && db.isOpen()) {
            db.close();
            db = null;
        }
        self = null;
    }

    public boolean SaveTrack2File() {
        Throwable ex;
        new ArrayList();
        ArrayList<Location> locList = GetTodayTrackData();
        if (locList == null || locList.size() == 0) {
            return false;
        }
        RandomAccessFile file = null;
        try {
            if (!hasSdcard()) {
                try {
                    file.close();
                } catch (Throwable th) {
                }
                return false;
            }
            File dir = new File("sdcard/myTrack/");
            if (!dir.exists()) {
                dir.mkdirs();
            }
            File xmlFile = new File("sdcard/myTrack/" + (String.valueOf(new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance(TimeZone.getTimeZone("GMT+8")).getTime())) + ".kml"));
            if (xmlFile.exists()) {
                xmlFile.delete();
            }
            RandomAccessFile file2 = new RandomAccessFile(xmlFile, "rw");
            try {
                file2.write(Track2KML(locList).getBytes());
                file2.close();
                LogUtil.getInstance().log("导出成xml文件成功");
                try {
                    file2.close();
                } catch (Throwable th2) {
                }
                return true;
            } catch (Throwable th3) {
                th = th3;
                file = file2;
                file.close();
                throw th;
            }
        } catch (Throwable th4) {
            ex = th4;
        }
    }

    public String SaveTrack() {
        Throwable ex;
        new ArrayList();
        ArrayList<Location> locList = GetTodayTrackData();
        if (locList == null || locList.size() == 0) {
            return null;
        }
        RandomAccessFile file = null;
        try {
            if (!hasSdcard()) {
                try {
                    file.close();
                } catch (Throwable th) {
                }
                return null;
            }
            File dir = new File("/mnt/sdcard/myTrack/");
            if (!dir.exists()) {
                dir.mkdirs();
            }
            File xmlFile = new File("/mnt/sdcard/myTrack/" + (String.valueOf(new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance(TimeZone.getTimeZone("GMT+8")).getTime())) + ".kml"));
            if (xmlFile.exists()) {
                xmlFile.delete();
            }
            RandomAccessFile file2 = new RandomAccessFile(xmlFile, "rw");
            try {
                String strResult = xmlFile.getPath();
                file2.write(Track2KML(locList).getBytes());
                file2.close();
                LogUtil.getInstance().log("导出成xml文件成功");
                try {
                    file2.close();
                } catch (Throwable th2) {
                }
                return strResult;
            } catch (Throwable th3) {
                th = th3;
                file = file2;
                file.close();
                throw th;
            }
        } catch (Throwable th4) {
            ex = th4;
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    public String Track2KML(ArrayList<Location> locList) {
        if (locList == null || locList.size() <= 0) {
            return null;
        }
        StringBuilder sbKMLBody = new StringBuilder();
        String strKMLheader = String.valueOf(String.valueOf(String.valueOf("<?xml version='1.0' encoding='UTF-8'?> \n\r") + "<kml> \n\r") + "<Document> \n\r") + "<name>KML File</name> \n\r";
        sbKMLBody.append("<Folder>");
        sbKMLBody.append("<Style id='normalPlacemark'> <IconStyle><scale>0.4</scale> <Icon> <href>d:/lbs/red-stars.png</href> </Icon>  </IconStyle></Style>");
        for (int i = 0; i < locList.size(); i++) {
            sbKMLBody.append("<Placemark> \n\r");
            sbKMLBody.append("<styleUrl>#normalPlacemark</styleUrl> \n\r");
            sbKMLBody.append("<Point> \n\r");
            sbKMLBody.append("<coordinates>");
            sbKMLBody.append(String.valueOf(locList.get(i).getLongitude()));
            sbKMLBody.append(",");
            sbKMLBody.append(String.valueOf(locList.get(i).getLatitude()));
            sbKMLBody.append(" </coordinates> \n\r");
            sbKMLBody.append("</Point> \n\r");
            sbKMLBody.append("</Placemark>\n\r");
        }
        return String.valueOf(String.valueOf(strKMLheader) + sbKMLBody.toString()) + (String.valueOf("") + " </Folder> \n\r </Document> \n\r </kml> \n\r");
    }

    public boolean hasSdcard() {
        if (Environment.getExternalStorageState().equals("mounted")) {
            return true;
        }
        return false;
    }

    private RandomAccessFile CreateOrOpenFile() throws Exception {
        if (!hasSdcard()) {
            return null;
        }
        File dir = new File("sdcard/myTrack/");
        if (!dir.exists()) {
            dir.mkdirs();
        }
        File xmlFile = new File("sdcard/myTrack/" + (String.valueOf(new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance(TimeZone.getTimeZone("GMT+8")).getTime())) + ".kml"));
        if (xmlFile.exists()) {
            xmlFile.delete();
        }
        return new RandomAccessFile(xmlFile, "rw");
    }

    public void onCreate(SQLiteDatabase arg0) {
        db = arg0;
        initDB();
    }

    public TrackData(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public void onUpgrade(SQLiteDatabase db2, int oldVersion, int newVersion) {
    }
}
