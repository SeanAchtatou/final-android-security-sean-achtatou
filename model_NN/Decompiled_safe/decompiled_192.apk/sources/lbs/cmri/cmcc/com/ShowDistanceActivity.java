package lbs.cmri.cmcc.com;

import android.app.Activity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;
import java.util.ArrayList;

public class ShowDistanceActivity extends Activity {
    private static int MAXDISTANCECOUNT = 30;

    public void onCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.distance);
        TextView textView = (TextView) findViewById(R.id.tvAllDistance);
        String strDistance = "";
        ArrayList<TrackDistance> alDistance = TrackData.getInstance(getApplicationContext()).GetAllDistance(MAXDISTANCECOUNT);
        if (alDistance != null && alDistance.size() > 0) {
            for (int i = 0; i < alDistance.size(); i++) {
                strDistance = String.valueOf(String.valueOf(String.valueOf(String.valueOf(strDistance) + alDistance.get(i).GetTime()) + " ： ") + String.valueOf((int) alDistance.get(i).GetDistance())) + " 米 \n";
            }
        }
        textView.setText(strDistance);
        textView.setMovementMethod(ScrollingMovementMethod.getInstance());
        super.onCreate(savedInstanceState);
    }
}
