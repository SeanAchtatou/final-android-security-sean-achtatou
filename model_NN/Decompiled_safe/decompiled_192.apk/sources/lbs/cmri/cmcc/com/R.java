package lbs.cmri.cmcc.com;

public final class R {

    public static final class attr {
        public static final int userAgent = 2130771968;
    }

    public static final class color {
        public static final int gray = 2130968578;
        public static final int trans_background = 2130968576;
        public static final int transparent = 2130968579;
        public static final int white = 2130968577;
    }

    public static final class dimen {
        public static final int layout_width = 2131034112;
        public static final int list_item_height = 2131034126;
        public static final int list_item_subtitlesize = 2131034125;
        public static final int list_item_titlesize = 2131034124;
        public static final int margin_hor_s = 2131034114;
        public static final int margin_hor_w = 2131034113;
        public static final int margin_ver = 2131034115;
        public static final int margin_ver_s = 2131034116;
        public static final int setting_address_width = 2131034122;
        public static final int setting_port_width = 2131034123;
        public static final int spinner_h = 2131034118;
        public static final int spinner_pading_r = 2131034127;
        public static final int spinner_w = 2131034117;
        public static final int text_view_marginBottom = 2131034120;
        public static final int text_view_marginTop = 2131034119;
        public static final int text_view_textSize = 2131034121;
    }

    public static final class drawable {
        public static final int add = 2130837504;
        public static final int alert = 2130837505;
        public static final int apk = 2130837506;
        public static final int arrow_down_float = 2130837507;
        public static final int audio = 2130837508;
        public static final int delete_48 = 2130837509;
        public static final int detail = 2130837510;
        public static final int edit = 2130837511;
        public static final int end_32 = 2130837512;
        public static final int file = 2130837513;
        public static final int folder = 2130837514;
        public static final int howfar_48 = 2130837515;
        public static final int icon = 2130837516;
        public static final int image = 2130837517;
        public static final int info = 2130837518;
        public static final int list = 2130837519;
        public static final int location_48 = 2130837520;
        public static final int mylocation_48 = 2130837521;
        public static final int network_48 = 2130837522;
        public static final int open_48 = 2130837523;
        public static final int path = 2130837524;
        public static final int poi_point = 2130837525;
        public static final int query = 2130837526;
        public static final int save_48 = 2130837527;
        public static final int set = 2130837528;
        public static final int share_48 = 2130837529;
        public static final int start_32 = 2130837530;
        public static final int translucent_background = 2130837532;
        public static final int video = 2130837531;
    }

    public static final class id {
        public static final int Share2Friend = 2131230762;
        public static final int TextView01 = 2131230725;
        public static final int atmapsView = 2131230732;
        public static final int btnShare2Server = 2131230761;
        public static final int btnext = 2131230767;
        public static final int btprev = 2131230764;
        public static final int copy_edit = 2131230721;
        public static final int copy_text = 2131230720;
        public static final int edtDiscription = 2131230758;
        public static final int f_icon = 2131230726;
        public static final int f_text = 2131230728;
        public static final int f_title = 2131230727;
        public static final int frameLayout1 = 2131230730;
        public static final int ibtLocation = 2131230745;
        public static final int ibtOpenTrack = 2131230742;
        public static final int ibtShareTrack = 2131230739;
        public static final int ibtTrackList = 2131230736;
        public static final int imv1 = 2131230737;
        public static final int imv2 = 2131230740;
        public static final int imv3 = 2131230743;
        public static final int imv4 = 2131230746;
        public static final int itv1 = 2131230738;
        public static final int itv2 = 2131230741;
        public static final int itv3 = 2131230744;
        public static final int itv4 = 2131230747;
        public static final int linearLayout1 = 2131230735;
        public static final int lvContactList = 2131230770;
        public static final int menu_item_CurrentLocation = 2131230773;
        public static final int menu_item_DeleteTrack = 2131230774;
        public static final int menu_item_Help = 2131230780;
        public static final int menu_item_HowFar = 2131230772;
        public static final int menu_item_OpenTrack = 2131230778;
        public static final int menu_item_RenewLocation = 2131230777;
        public static final int menu_item_SaveTrack = 2131230771;
        public static final int menu_item_ShareApp = 2131230779;
        public static final int menu_item_ShareTrack = 2131230776;
        public static final int menu_item_ShowTrackList = 2131230775;
        public static final int move_edit = 2131230749;
        public static final int move_text = 2131230748;
        public static final int new_edit = 2131230754;
        public static final int new_radio = 2131230751;
        public static final int new_view = 2131230750;
        public static final int newdir_radio = 2131230753;
        public static final int newfile_radio = 2131230752;
        public static final int path_edit = 2131230724;
        public static final int qry_button = 2131230723;
        public static final int relativeLayout1 = 2131230731;
        public static final int relativeLayout2 = 2131230734;
        public static final int rename_edit = 2131230756;
        public static final int rename_text = 2131230755;
        public static final int tableLayout1 = 2131230763;
        public static final int tableLayout2 = 2131230768;
        public static final int tableRow1 = 2131230760;
        public static final int textView2 = 2131230765;
        public static final int textView3 = 2131230766;
        public static final int textView4 = 2131230757;
        public static final int title = 2131230769;
        public static final int tvAllDistance = 2131230722;
        public static final int tvDistance = 2131230729;
        public static final int tvKMLURL = 2131230759;
        public static final int tvtest = 2131230733;
    }

    public static final class layout {
        public static final int copy_alert = 2130903040;
        public static final int distance = 2130903041;
        public static final int filemain = 2130903042;
        public static final int help = 2130903043;
        public static final int list_items = 2130903044;
        public static final int main = 2130903045;
        public static final int move_alert = 2130903046;
        public static final int new_alert = 2130903047;
        public static final int rename_alert = 2130903048;
        public static final int sharetrackinput = 2130903049;
        public static final int sharetracklist = 2130903050;
        public static final int title_list_item = 2130903051;
        public static final int tracksend = 2130903052;
    }

    public static final class menu {
        public static final int mainmenu = 2131165184;
    }

    public static final class string {
        public static final int TrackShare = 2131099652;
        public static final int app_name = 2131099649;
        public static final int dir = 2131099658;
        public static final int dirAboutButton = 2131099661;
        public static final int dirAddButton = 2131099659;
        public static final int dirSetButton = 2131099660;
        public static final int file = 2131099657;
        public static final int hello = 2131099648;
        public static final int help = 2131099650;
        public static final int lab_product_list_lable_name = 2131099651;
        public static final int menu_CurrentLocation = 2131099667;
        public static final int menu_DeleteTrack = 2131099673;
        public static final int menu_Help = 2131099675;
        public static final int menu_HowFar = 2131099672;
        public static final int menu_OpenTrack = 2131099666;
        public static final int menu_RenewLocation = 2131099668;
        public static final int menu_SaveTrack = 2131099671;
        public static final int menu_ShareApp = 2131099674;
        public static final int menu_ShareTrack = 2131099670;
        public static final int menu_ShowTrackList = 2131099665;
        public static final int menu_StopRenewLocation = 2131099669;
        public static final int open = 2131099662;
        public static final int openTrack = 2131099664;
        public static final int str_copy = 2131099655;
        public static final int str_move = 2131099656;
        public static final int str_new = 2131099654;
        public static final int str_rename = 2131099653;
        public static final int zoom = 2131099663;
    }

    public static final class styleable {
        public static final int[] myPhone = {R.attr.userAgent};
        public static final int myPhone_userAgent = 0;
    }
}
