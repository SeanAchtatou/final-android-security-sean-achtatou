package lbs.cmri.cmcc.com;

import com.mapabc.mapapi.GeoPoint;

public class GeoTrans {
    private double casm_f;
    private double casm_rr;
    private int casm_t1;
    private int casm_t2;
    private double casm_x1;
    private double casm_x2;
    private double casm_y1;
    private double casm_y2;
    private int wg_flag = 0;

    /* JADX INFO: Multiple debug info for r9v2 double: [D('x' double), D('tt' double)] */
    /* JADX INFO: Multiple debug info for r9v4 double: [D('x' double), D('s2' double)] */
    /* JADX INFO: Multiple debug info for r9v11 double: [D('ss' double), D('s2' double)] */
    private static double yj_sin2(double x) {
        int ff;
        int ff2;
        double tt;
        if (x < 0.0d) {
            x = -x;
            ff = 1;
        } else {
            ff = 0;
        }
        double tt2 = x - (((double) ((int) (x / 6.28318530717959d))) * 6.28318530717959d);
        if (tt2 > 3.141592653589793d) {
            double tt3 = tt2 - 3.141592653589793d;
            if (ff == 1) {
                ff2 = 0;
                tt = tt3;
            } else if (ff == 0) {
                ff2 = 1;
                tt = tt3;
            } else {
                ff2 = ff;
                tt = tt3;
            }
        } else {
            ff2 = ff;
            tt = tt2;
        }
        double x2 = tt;
        double ss = x2;
        double tt4 = tt * tt;
        double s2 = x2 * tt4;
        double s22 = s2 * tt4;
        double s23 = s22 * tt4;
        double s24 = s23 * tt4;
        double s25 = ((((ss - (0.166666666666667d * s2)) + (0.00833333333333333d * s22)) - (1.98412698412698E-4d * s23)) + (2.75573192239859E-6d * s24)) - ((s24 * tt4) * 2.50521083854417E-8d);
        if (ff2 == 1) {
            return -s25;
        }
        return s25;
    }

    private static double Transform_yj5(double x, double y) {
        return (((yj_sin2(x * 0.1047197551196598d) * 300.0d) + (150.0d * yj_sin2(0.2617993877991495d * x))) * 0.6667d) + (y * 0.1d * x) + 300.0d + (1.0d * x) + (2.0d * y) + (0.1d * x * x) + (0.1d * Math.sqrt(Math.sqrt(x * x))) + (((20.0d * yj_sin2(18.849555921538762d * x)) + (20.0d * yj_sin2(6.283185307179588d * x))) * 0.6667d) + (((20.0d * yj_sin2(3.141592653589794d * x)) + (40.0d * yj_sin2(1.047197551196598d * x))) * 0.6667d);
    }

    private static double Transform_yjy5(double x, double y) {
        return -100.0d + (2.0d * x) + (3.0d * y) + (0.2d * y * y) + (0.1d * x * y) + (Math.sqrt(Math.sqrt(x * x)) * 0.2d) + (((yj_sin2(18.849555921538762d * x) * 20.0d) + (yj_sin2(6.283185307179588d * x) * 20.0d)) * 0.6667d) + (((yj_sin2(3.141592653589794d * y) * 20.0d) + (40.0d * yj_sin2(1.047197551196598d * y))) * 0.6667d) + (((160.0d * yj_sin2(0.2617993877991495d * y)) + (320.0d * yj_sin2(0.1047197551196598d * y))) * 0.6667d);
    }

    private static double Transform_jy5(double x, double xx) {
        return (xx * 180.0d) / ((Math.cos(x * 0.0174532925199433d) * (6378245.0d / Math.sqrt(1.0d - ((0.00669342d * yj_sin2(0.0174532925199433d * x)) * yj_sin2(0.0174532925199433d * x))))) * 3.1415926d);
    }

    /* JADX INFO: Multiple debug info for r10v6 double: [D('mm' double), D('m' double)] */
    private static double Transform_jyj5(double x, double yy) {
        double mm = 1.0d - (yj_sin2(x * 0.0174532925199433d) * (yj_sin2(0.0174532925199433d * x) * 0.00669342d));
        return (yy * 180.0d) / (((6378245.0d * (1.0d - 0.00669342d)) / (mm * Math.sqrt(mm))) * 3.1415926d);
    }

    private double r_yj() {
        return 0.0d;
    }

    private double random_yj() {
        this.casm_rr = (((double) 314159269) * this.casm_rr) + ((double) 453806245);
        this.casm_rr -= (double) (((int) (this.casm_rr / 2.0d)) * 2);
        this.casm_rr /= 2.0d;
        return this.casm_rr;
    }

    /* JADX INFO: Multiple debug info for r2v2 double: [D('x_l' double), D('china_lng' double)] */
    public static GeoPoint GPointOffset(GeoPoint Pointin) {
        double x_l = ((double) Pointin.getLongitudeE6()) / 1000000.0d;
        double y_l = ((double) Pointin.getLatitudeE6()) / 1000000.0d;
        double x_add = Transform_yj5(x_l - 105.0d, y_l - 35.0d);
        return new GeoPoint((int) ((Transform_jyj5(y_l, Transform_yjy5(x_l - 105.0d, y_l - 35.0d)) + y_l) * 1000000.0d), (int) ((x_l + Transform_jy5(y_l, x_add)) * 1000000.0d));
    }
}
