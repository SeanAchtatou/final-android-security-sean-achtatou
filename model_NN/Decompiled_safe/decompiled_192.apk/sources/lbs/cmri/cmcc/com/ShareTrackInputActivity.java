package lbs.cmri.cmcc.com;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import cmcc.location.core.LocatorAPI;
import cmcc.location.core.LogUtil;
import cmcc.location.core.e;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

public class ShareTrackInputActivity extends Activity {
    private View.OnClickListener Share2Friend = new View.OnClickListener() {
        public void onClick(View v) {
            String FileName = TrackData.getInstance(ShareTrackInputActivity.this.getApplicationContext()).SaveTrack();
            if (FileName != null) {
                Intent intent = new Intent("android.intent.action.SEND", Uri.parse("mailto:yourfriendmailbox@xxxx.com"));
                intent.putExtra("android.intent.extra.TEXT", "你的朋友向你共享了他的轨迹，请查收附件后用 我的轨迹 应用可打开。如果没有安装 我的轨迹应用，请到http://218.206.179.209:18080/mytrack/myTrack.apk 下载！");
                intent.putExtra("android.intent.extra.SUBJECT", "你的朋友向你共享了他的轨迹，请查收附件");
                intent.putExtra("android.intent.extra.STREAM", Uri.fromFile(new File(FileName)));
                intent.setType("text/plain");
                ShareTrackInputActivity.this.startActivity(Intent.createChooser(intent, "Send mail..."));
            }
        }
    };
    private View.OnClickListener Share2Server = new View.OnClickListener() {
        public void onClick(View v) {
            ShareTrackInputActivity.this.SendRequest();
            ShareTrackInputActivity.this.finish();
        }
    };
    private Button btnShare2Friend = null;
    private Button btnShare2Server = null;
    private String strDescription = "";

    public void onCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.sharetrackinput);
        this.btnShare2Server = (Button) findViewById(R.id.btnShare2Server);
        this.btnShare2Server.setOnClickListener(this.Share2Server);
        this.btnShare2Friend = (Button) findViewById(R.id.Share2Friend);
        this.btnShare2Friend.setOnClickListener(this.Share2Friend);
        Location m_Location = LocatorAPI.getInstance(this).fppLocate("1000", "1000110", "Area");
        if (m_Location != null) {
            new Bundle();
            Bundle bundle = m_Location.getExtras();
            String PosLevel = bundle.getString("PosLevel");
            if ("area".equals(PosLevel.toLowerCase()) || "hybrid".equals(PosLevel.toLowerCase())) {
                String Code = bundle.getString("code");
                String Addr = e.f140goto + bundle.getString("addr") + e.f146void;
                EditText edt = (EditText) findViewById(R.id.edtDiscription);
                if (Addr == null || "".equals(Addr)) {
                    edt.setHint("请输入所在省市和轨迹描述");
                } else {
                    edt.setText(Addr);
                }
            }
        }
        super.onCreate(savedInstanceState);
    }

    public void SendRequest() {
        String strDiscription = ((EditText) findViewById(R.id.edtDiscription)).getText().toString();
        if (strDiscription == null || "".equals(strDiscription)) {
            strDiscription = "这家伙很懒，什么都没有留下";
        }
        String strKML = TrackData.getInstance(getApplicationContext()).Track2KML(TrackData.getInstance(getApplicationContext()).GetTodayTrackData());
        try {
            strKML = CommUtil.Base64Encode(CommUtil.zipStr(strKML));
        } catch (IOException e) {
            LogUtil.getInstance().log("压缩异常");
        }
        String strBody = String.valueOf("<?xml version='1.0' encoding='UTF-8'?> ") + "<uploadtrack>";
        User usr = TrackData.getInstance(getApplicationContext()).GetUserInfo();
        if (usr == null) {
            Toast.makeText(this, "没有获得用户编号", 1).show();
            return;
        }
        if (!isUploadSuccess(UploadTrack(Globals.UpLoadTrackURL, String.valueOf(String.valueOf(String.valueOf(String.valueOf(strBody) + "<uid>" + String.valueOf(usr.getUser_id()) + "</uid>") + "<disc>" + strDiscription + "</disc>") + "<kml>" + strKML + "</kml>") + "</uploadtrack>"))) {
            Toast.makeText(this, "分享轨迹失败", 1).show();
        } else {
            Toast.makeText(this, "分享成功，感谢您的分享！", 1).show();
        }
    }

    public boolean isUploadSuccess(String strResponse) {
        NodeList nl;
        boolean bResult = false;
        if (strResponse == null || "".equals(strResponse)) {
            return false;
        }
        try {
            Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(strResponse.getBytes()));
            if (!(doc == null || (nl = CommUtil.getNodeList(doc, "s")) == null || nl.getLength() <= 0)) {
                if (CommUtil.getElementValue(nl.item(0)).equals("0")) {
                    LogUtil.getInstance().log("Error:" + CommUtil.getElementValue(CommUtil.getNodeList(doc, "err").item(0)));
                    return false;
                }
                bResult = true;
            }
            return bResult;
        } catch (Exception e) {
            LogUtil.getInstance().log("isUploadSuccess Failed " + e.toString());
            return false;
        }
    }

    private String UploadTrack(String strURL, String strBody) {
        try {
            URL url = new URL(strURL);
            ConnectivityManager conMan = (ConnectivityManager) getSystemService("connectivity");
            if (conMan == null) {
                return null;
            }
            NetworkInfo info = conMan.getActiveNetworkInfo();
            if (info == null) {
                return null;
            }
            HttpURLConnection con = CommUtil.getUrlConnectionNew(url, info);
            if (con == null) {
                return null;
            }
            String strResponse = CommUtil.PostRequest(con, strBody);
            con.disconnect();
            return strResponse;
        } catch (Exception e) {
            Exception ex = e;
            LogUtil.getInstance().log("SendUpdateRequest Error:" + ex.toString());
            return null;
        }
    }
}
