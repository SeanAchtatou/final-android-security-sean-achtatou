package lbs.cmri.cmcc.com;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class MyFile extends ListActivity implements AdapterView.OnItemLongClickListener {
    protected static final int MENU_ABOUT = 3;
    protected static final int MENU_ADD = 1;
    protected static final int MENU_SET = 2;
    private int OPEN_TRACK = 4;
    private MyAdb db;
    private int id = 0;
    private int isOpen = 0;
    private int isZoom = 0;
    private List<String> items = null;
    View.OnClickListener listener_qry = new View.OnClickListener() {
        public void onClick(View arg0) {
            File file = new File(MyFile.this.path_edit.getText().toString());
            if (!file.exists()) {
                Toast.makeText(MyFile.this, "找不到该位置,请确定位置是否正确!", 0).show();
            } else if (file.isFile()) {
                MyFile.this.openFile(file);
            } else {
                MyFile.this.getFileDir(MyFile.this.path_edit.getText().toString());
            }
        }
    };
    private Cursor myCursor;
    /* access modifiers changed from: private */
    public EditText myEditText;
    private View myView;
    /* access modifiers changed from: private */
    public TextView new_textView;
    /* access modifiers changed from: private */
    public TextView path_edit;
    private List<String> paths = null;
    /* access modifiers changed from: private */
    public RadioGroup radioGroup;
    /* access modifiers changed from: private */
    public RadioButton rb_dir;
    /* access modifiers changed from: private */
    public RadioButton rb_file;
    private ImageButton rb_qry;
    private String rootPath = "/sdcard/mytrack";
    private List<String> sizes = null;

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        this.path_edit = (EditText) findViewById(R.id.path_edit);
        File file = new File(this.path_edit.getText().toString());
        if (this.rootPath.equals(this.path_edit.getText().toString())) {
            return super.onKeyDown(keyCode, event);
        }
        getFileDir(file.getParent());
        return true;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView((int) R.layout.filemain);
        this.db = new MyAdb(this);
        this.myCursor = this.db.getFileSet();
        if (this.myCursor.moveToFirst()) {
            this.id = this.myCursor.getInt(this.myCursor.getColumnIndex("_ID"));
            this.isZoom = this.myCursor.getInt(this.myCursor.getColumnIndex("ISZOOM"));
            this.isOpen = this.myCursor.getInt(this.myCursor.getColumnIndex("ISOPEN"));
        } else {
            this.db.insertFileSet(this.isZoom, this.isOpen);
            this.myCursor = this.db.getFileSet();
            this.myCursor.moveToFirst();
            this.id = this.myCursor.getInt(this.myCursor.getColumnIndex("_ID"));
        }
        this.path_edit = (EditText) findViewById(R.id.path_edit);
        this.rb_qry = (ImageButton) findViewById(R.id.qry_button);
        this.rb_qry.setOnClickListener(this.listener_qry);
        getListView().setOnItemLongClickListener(this);
        getFileDir(this.rootPath);
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id2) {
        fileOrDirHandle(new File(this.paths.get(position)), "short");
    }

    public boolean onItemLongClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
        fileOrDirHandle(new File(this.paths.get(arg2)), "long");
        return true;
    }

    private void fileOrDirHandle(final File file, String flag) {
        DialogInterface.OnClickListener listener_list = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    MyFile.this.copyFileOrDir(file);
                } else if (which == 1) {
                    MyFile.this.moveFileOrDir(file);
                } else if (which == 2) {
                    MyFile.this.modifyFileOrDir(file);
                } else if (which == 3) {
                    MyFile.this.delFileOrDir(file);
                }
            }
        };
        if (flag.equals("long")) {
            new AlertDialog.Builder(this).setTitle(file.getName()).setIcon((int) R.drawable.list).setItems(new String[]{"复制", "移动", "重命名", "删除"}, listener_list).setPositiveButton("取消", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            }).show();
        } else if (file.isDirectory()) {
            getFileDir(file.getPath());
        } else {
            openFile(file);
        }
    }

    /* access modifiers changed from: private */
    public void getFileDir(String filePath) {
        this.path_edit.setText(filePath);
        this.items = new ArrayList();
        this.paths = new ArrayList();
        this.sizes = new ArrayList();
        File[] files = new File(filePath).listFiles();
        if (files != null) {
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    this.items.add(files[i].getName());
                    this.paths.add(files[i].getPath());
                    this.sizes.add("");
                }
            }
            for (int i2 = 0; i2 < files.length; i2++) {
                if (files[i2].isFile()) {
                    this.items.add(files[i2].getName());
                    this.paths.add(files[i2].getPath());
                    this.sizes.add(CommUtil.fileSizeMsg(files[i2]));
                }
            }
        }
        setListAdapter(new FileListAdapter(this, this.items, this.paths, this.sizes, this.isZoom));
    }

    private void newDirOrFile() {
        AlertDialog nameDialog = new AlertDialog.Builder(this).create();
        this.myView = LayoutInflater.from(this).inflate((int) R.layout.new_alert, (ViewGroup) null);
        this.new_textView = (TextView) this.myView.findViewById(R.id.new_view);
        this.rb_dir = (RadioButton) this.myView.findViewById(R.id.newdir_radio);
        this.rb_file = (RadioButton) this.myView.findViewById(R.id.newfile_radio);
        this.radioGroup = (RadioGroup) this.myView.findViewById(R.id.new_radio);
        this.myEditText = (EditText) this.myView.findViewById(R.id.new_edit);
        this.path_edit = (EditText) findViewById(R.id.path_edit);
        nameDialog.setView(this.myView);
        this.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == MyFile.this.rb_file.getId()) {
                    MyFile.this.new_textView.setText("新建文件:");
                } else if (checkedId == MyFile.this.rb_dir.getId()) {
                    MyFile.this.new_textView.setText("新建文件夹:");
                }
            }
        });
        nameDialog.setButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                final int checkedId = MyFile.this.radioGroup.getCheckedRadioButtonId();
                String newName = MyFile.this.myEditText.getText().toString();
                final String newPath = String.valueOf(MyFile.this.path_edit.getText().toString()) + "/" + newName;
                final File f_new = new File(newPath);
                if (f_new.exists()) {
                    Toast.makeText(MyFile.this, "指定文件'" + newName + "'与现有文件重名,请指定另一名称!", 1).show();
                } else {
                    new AlertDialog.Builder(MyFile.this).setTitle("注意").setIcon((int) R.drawable.alert).setMessage("确定创建" + (checkedId == MyFile.this.rb_dir.getId() ? "文件夹" : "文件") + "'" + newName + "' 吗?").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            if (checkedId == MyFile.this.rb_dir.getId()) {
                                if (!CommUtil.checkDirPath(newPath)) {
                                    Toast.makeText(MyFile.this, "请输入正确的格式(不包含//)!", 0).show();
                                } else if (f_new.mkdirs()) {
                                    Toast.makeText(MyFile.this, "已创建!", 0).show();
                                    MyFile.this.getFileDir(f_new.getParent());
                                } else {
                                    Toast.makeText(MyFile.this, "出错!", 0).show();
                                }
                            } else if (!CommUtil.checkFilePath(newPath)) {
                                Toast.makeText(MyFile.this, "请输入正确的格式(不包含//)!", 0).show();
                            } else if (MyFile.this.newFile(f_new)) {
                                Toast.makeText(MyFile.this, "已创建!", 0).show();
                                MyFile.this.getFileDir(f_new.getParent());
                            } else {
                                Toast.makeText(MyFile.this, "出错!", 0).show();
                            }
                        }
                    }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).show();
                }
            }
        });
        nameDialog.setButton2("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        nameDialog.show();
    }

    /* access modifiers changed from: private */
    public void modifyFileOrDir(File f) {
        final File f_old = f;
        this.myView = LayoutInflater.from(this).inflate((int) R.layout.rename_alert, (ViewGroup) null);
        this.myEditText = (EditText) this.myView.findViewById(R.id.rename_edit);
        this.myEditText.setText(f_old.getName());
        DialogInterface.OnClickListener listenerFileEdit = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                String modName = MyFile.this.myEditText.getText().toString();
                final String pFile = String.valueOf(f_old.getParentFile().getPath()) + "/";
                final String newPath = String.valueOf(pFile) + modName;
                final File f_new = new File(newPath);
                if (!f_new.exists()) {
                    AlertDialog.Builder message = new AlertDialog.Builder(MyFile.this).setTitle("注意").setIcon((int) R.drawable.alert).setMessage("确定要修改" + (f_old.isDirectory() ? "文件夹'" : "文件'") + f_old.getName() + "'名称为'" + modName + "'吗?");
                    final File file = f_old;
                    message.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            if (file.isDirectory()) {
                                if (!CommUtil.checkDirPath(newPath)) {
                                    Toast.makeText(MyFile.this, "请输入正确的格式(不包含//)!", 0).show();
                                } else if (file.renameTo(f_new)) {
                                    Toast.makeText(MyFile.this, "已修改!", 0).show();
                                    MyFile.this.getFileDir(pFile);
                                } else {
                                    Toast.makeText(MyFile.this, "出错!", 0).show();
                                }
                            } else if (!CommUtil.checkFilePath(newPath)) {
                                Toast.makeText(MyFile.this, "请输入正确的格式(不包含//)!", 0).show();
                            } else if (file.renameTo(f_new)) {
                                Toast.makeText(MyFile.this, "已修改!", 0).show();
                                MyFile.this.getFileDir(pFile);
                            } else {
                                Toast.makeText(MyFile.this, "出错!", 0).show();
                            }
                        }
                    }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).show();
                } else if (!modName.equals(f_old.getName())) {
                    Toast.makeText(MyFile.this, "指定文件'" + modName + "'与现有文件重名,请指定另一名称!", 0).show();
                } else {
                    Toast.makeText(MyFile.this, "名称未修改!", 0).show();
                }
            }
        };
        AlertDialog renameDialog = new AlertDialog.Builder(this).create();
        renameDialog.setView(this.myView);
        renameDialog.setButton("确定", listenerFileEdit);
        renameDialog.setButton2("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        renameDialog.show();
    }

    public void copyFileOrDir(File f) {
        final File f_old = f;
        this.myView = LayoutInflater.from(this).inflate((int) R.layout.copy_alert, (ViewGroup) null);
        this.myEditText = (EditText) this.myView.findViewById(R.id.copy_edit);
        this.myEditText.setText(f.getParent());
        DialogInterface.OnClickListener listenerCopy = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                String new_path;
                String new_path2 = MyFile.this.myEditText.getText().toString();
                if (new_path2.endsWith(File.separator)) {
                    new_path = String.valueOf(new_path2) + f_old.getName();
                } else {
                    new_path = String.valueOf(new_path2) + File.separator + f_old.getName();
                }
                final File f_new = new File(new_path);
                if (f_new.exists()) {
                    Toast.makeText(MyFile.this, "指定文件'" + f_new.getName() + "'与现有文件重名,请指定另一名称!", 0).show();
                    return;
                }
                AlertDialog.Builder message = new AlertDialog.Builder(MyFile.this).setTitle("注意").setIcon((int) R.drawable.alert).setMessage("确定要把" + (f_old.isDirectory() ? "文件夹" : "文件") + "'" + f_old.getName() + "'复制到'" + f_new.getParent() + "'吗?");
                final File file = f_old;
                message.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (file.isDirectory()) {
                            if (!CommUtil.checkDirPath(f_new.getPath())) {
                                Toast.makeText(MyFile.this, "请输入正确的格式(不包含//)!", 0).show();
                            } else if (MyFile.this.copyDir(file.getPath(), f_new.getParent())) {
                                Toast.makeText(MyFile.this, "已复制!", 0).show();
                                MyFile.this.getFileDir(f_new.getParent());
                            } else {
                                Toast.makeText(MyFile.this, "出错!", 0).show();
                            }
                        } else if (!CommUtil.checkFilePath(f_new.getPath())) {
                            Toast.makeText(MyFile.this, "请输入正确的格式(不包含//)!", 0).show();
                        } else if (MyFile.this.copyFile(file.getPath(), f_new.getParent())) {
                            Toast.makeText(MyFile.this, "已复制!", 0).show();
                            MyFile.this.getFileDir(f_new.getParent());
                        } else {
                            Toast.makeText(MyFile.this, "出错!", 0).show();
                        }
                    }
                }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).show();
            }
        };
        AlertDialog copyDialog = new AlertDialog.Builder(this).create();
        copyDialog.setView(this.myView);
        copyDialog.setButton("确定", listenerCopy);
        copyDialog.setButton2("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        copyDialog.show();
    }

    public void moveFileOrDir(File f) {
        final File f_old = f;
        this.myView = LayoutInflater.from(this).inflate((int) R.layout.move_alert, (ViewGroup) null);
        this.myEditText = (EditText) this.myView.findViewById(R.id.move_edit);
        this.myEditText.setText(f_old.getParent());
        DialogInterface.OnClickListener listenerMove = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                String new_path;
                String new_path2 = MyFile.this.myEditText.getText().toString();
                if (new_path2.endsWith(File.separator)) {
                    new_path = String.valueOf(new_path2) + f_old.getName();
                } else {
                    new_path = String.valueOf(new_path2) + File.separator + f_old.getName();
                }
                final File f_new = new File(new_path);
                if (f_new.exists()) {
                    Toast.makeText(MyFile.this, "指定文件'" + f_new.getName() + "'与现有文件重名,请指定另一名称!", 0).show();
                    return;
                }
                AlertDialog.Builder message = new AlertDialog.Builder(MyFile.this).setTitle("注意").setIcon((int) R.drawable.alert).setMessage("确定要把" + (f_old.isDirectory() ? "文件夹" : "文件") + "'" + f_old.getName() + "'移动到'" + f_new.getParent() + "'吗?");
                final File file = f_old;
                message.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (file.isDirectory()) {
                            if (!CommUtil.checkDirPath(f_new.getPath())) {
                                Toast.makeText(MyFile.this, "请输入正确的格式(不包含//)!", 0).show();
                            } else if (MyFile.this.moveDir(file.getPath(), f_new.getParent())) {
                                Toast.makeText(MyFile.this, "已移动!", 0).show();
                                MyFile.this.getFileDir(f_new.getParent());
                            } else {
                                Toast.makeText(MyFile.this, "出错!", 0).show();
                            }
                        } else if (!CommUtil.checkDirPath(f_new.getPath())) {
                            Toast.makeText(MyFile.this, "请输入正确的格式(不包含//)!", 0).show();
                        } else if (MyFile.this.moveFile(file.getPath(), f_new.getParent())) {
                            Toast.makeText(MyFile.this, "已移动!", 0).show();
                            MyFile.this.getFileDir(f_new.getParent());
                        } else {
                            Toast.makeText(MyFile.this, "出错!", 0).show();
                        }
                    }
                }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).show();
            }
        };
        AlertDialog moveDialog = new AlertDialog.Builder(this).create();
        moveDialog.setView(this.myView);
        moveDialog.setButton("确定", listenerMove);
        moveDialog.setButton2("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        moveDialog.show();
    }

    /* access modifiers changed from: private */
    public void delFileOrDir(File f) {
        final File f_del = f;
        new AlertDialog.Builder(this).setTitle("注意").setIcon((int) R.drawable.alert).setMessage("确定要删除" + (f_del.isDirectory() ? "文件夹'" : "文件'") + f_del.getName() + "'吗?").setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (f_del.isDirectory()) {
                    if (MyFile.this.delDir(f_del)) {
                        Toast.makeText(MyFile.this, "已删除!", 0).show();
                        MyFile.this.getFileDir(f_del.getParent());
                        return;
                    }
                    Toast.makeText(MyFile.this, "出错!", 0).show();
                } else if (MyFile.this.delFile(f_del)) {
                    Toast.makeText(MyFile.this, "已删除!", 0).show();
                    MyFile.this.getFileDir(f_del.getParent());
                } else {
                    Toast.makeText(MyFile.this, "出错!", 0).show();
                }
            }
        }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        }).show();
    }

    public boolean newFile(File f) {
        try {
            f.createNewFile();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean copyFile(String oldPath, String newPath) {
        String f_new;
        int bytesum = 0;
        try {
            File f_old = new File(oldPath);
            if (newPath.endsWith(File.separator)) {
                f_new = String.valueOf(newPath) + f_old.getName();
            } else {
                f_new = String.valueOf(newPath) + File.separator + f_old.getName();
            }
            new File(newPath).mkdirs();
            new File(f_new).createNewFile();
            if (f_old.exists()) {
                InputStream inStream = new FileInputStream(oldPath);
                FileOutputStream fs = new FileOutputStream(f_new);
                byte[] buffer = new byte[1444];
                while (true) {
                    int byteread = inStream.read(buffer);
                    if (byteread == -1) {
                        break;
                    }
                    bytesum += byteread;
                    fs.write(buffer, 0, byteread);
                }
                inStream.close();
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean copyDir(String oldPath, String newPath) {
        try {
            File f_old = new File(oldPath);
            String d_new = String.valueOf(newPath) + File.separator + f_old.getName();
            new File(d_new).mkdirs();
            File[] files = f_old.listFiles();
            for (int i = 0; i < files.length; i++) {
                String d_old = String.valueOf(oldPath) + File.separator + files[i].getName();
                if (files[i].isFile()) {
                    copyFile(d_old, d_new);
                } else {
                    copyDir(d_old, d_new);
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean moveFile(String oldPath, String newPath) {
        boolean ret = false;
        try {
            if (copyFile(oldPath, newPath)) {
                new File(oldPath).delete();
                ret = true;
            }
            return ret;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean moveDir(String oldPath, String newPath) {
        boolean ret = false;
        try {
            if (copyDir(oldPath, newPath) && delDir(new File(oldPath))) {
                ret = true;
            }
            return ret;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean delFile(File f) {
        boolean ret = false;
        try {
            if (f.exists()) {
                f.delete();
                ret = true;
            }
            return ret;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean delDir(File f) {
        boolean ret = false;
        try {
            if (f.exists()) {
                File[] files = f.listFiles();
                for (int i = 0; i < files.length; i++) {
                    if (!files[i].isDirectory()) {
                        files[i].delete();
                    } else if (!delDir(files[i])) {
                        return false;
                    }
                }
                f.delete();
                ret = true;
            }
            return ret;
        } catch (Exception e) {
            return false;
        }
    }

    /* access modifiers changed from: private */
    public void openFile(File f) {
        String strFileName = f.getPath();
        Intent intent = getIntent();
        intent.putExtra("FileName", strFileName);
        setResult(-1, intent);
        finish();
    }
}
