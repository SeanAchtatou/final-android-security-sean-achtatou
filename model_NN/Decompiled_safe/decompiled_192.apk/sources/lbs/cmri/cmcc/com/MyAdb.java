package lbs.cmri.cmcc.com;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MyAdb extends SQLiteOpenHelper {
    public MyAdb(Context context) {
        super(context, "MYADB_FILE", (SQLiteDatabase.CursorFactory) null, 1);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE FILESET_TABLE (_ID INTEGER PRIMARY KEY AUTOINCREMENT,ISZOOM INTEGER,ISOPEN INTEGER)");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS FILESET_TABLE");
        onCreate(db);
    }

    public Cursor getFileSet() {
        return getReadableDatabase().query("FILESET_TABLE", null, null, null, null, null, null);
    }

    public long insertFileSet(int isZoom, int isOpen) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("ISZOOM", Integer.valueOf(isZoom));
        cv.put("ISOPEN", Integer.valueOf(isOpen));
        return db.insert("FILESET_TABLE", null, cv);
    }

    public long updateFileSet(int id, int isZoom, int isOpen) {
        SQLiteDatabase db = getWritableDatabase();
        String[] whereValue = {Integer.toString(id)};
        ContentValues cv = new ContentValues();
        cv.put("ISZOOM", Integer.valueOf(isZoom));
        cv.put("ISOPEN", Integer.valueOf(isOpen));
        return (long) db.update("FILESET_TABLE", cv, "_ID = ?", whereValue);
    }
}
