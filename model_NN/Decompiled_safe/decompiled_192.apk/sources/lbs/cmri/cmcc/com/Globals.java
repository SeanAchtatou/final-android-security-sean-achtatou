package lbs.cmri.cmcc.com;

import android.graphics.Color;

public class Globals {
    public static int FromServColor = Color.rgb(255, 102, 0);
    public static String GetTrackListURL = (String.valueOf(URLRoot) + "gettracklist");
    public static int KMLFileColor = -65281;
    public static String RegisterServiceURL = (String.valueOf(URLRoot) + "userregister");
    public static int TodayColor = -65536;
    public static String URLRoot = "http://218.206.179.209:18080/mytrack/";
    public static String UpLoadTrackURL = (String.valueOf(URLRoot) + "uploadtrack");
    public static String UpdateServiceURL = (String.valueOf(URLRoot) + "updateservice");
    public static int YesterdayColor = -16776961;
}
