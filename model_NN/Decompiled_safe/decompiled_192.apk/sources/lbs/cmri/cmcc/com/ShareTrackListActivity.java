package lbs.cmri.cmcc.com;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import cmcc.location.core.LogUtil;
import java.io.ByteArrayInputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ShareTrackListActivity extends Activity {
    private final int FP = -1;
    private final int WC = -2;
    /* access modifiers changed from: private */
    public ArrayList<TrackInfo> alTrackInfo = null;
    /* access modifiers changed from: private */
    public ArrayList<String> alstrPrevTime = null;
    public View.OnClickListener btNextOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            if (ShareTrackListActivity.this.alTrackInfo != null && ShareTrackListActivity.this.alTrackInfo.size() > 0) {
                String strURL = String.valueOf(Globals.GetTrackListURL) + "?time=" + ((TrackInfo) ShareTrackListActivity.this.alTrackInfo.get(ShareTrackListActivity.this.alTrackInfo.size() - 1)).gettime().replace(" ", "") + "&n=10";
                LogUtil.getInstance().log(strURL);
                ShareTrackListActivity.this.getTrackInfoList(strURL);
            }
        }
    };
    public View.OnClickListener btPrevOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            String strTime = "";
            if (ShareTrackListActivity.this.alTrackInfo != null && ShareTrackListActivity.this.alTrackInfo.size() > 0) {
                strTime = (String) ShareTrackListActivity.this.alstrPrevTime.get(0);
                for (int i = 0; i < ShareTrackListActivity.this.alstrPrevTime.size(); i++) {
                    LogUtil.getInstance().log((String) ShareTrackListActivity.this.alstrPrevTime.get(i));
                    if (((String) ShareTrackListActivity.this.alstrPrevTime.get(i)).equals(((TrackInfo) ShareTrackListActivity.this.alTrackInfo.get(0)).gettime())) {
                        if (i > 0) {
                            strTime = (String) ShareTrackListActivity.this.alstrPrevTime.get(i - 1);
                        } else {
                            strTime = (String) ShareTrackListActivity.this.alstrPrevTime.get(0);
                        }
                        LogUtil.getInstance().log(strTime);
                    }
                }
            }
            ShareTrackListActivity.this.getTrackInfoList(String.valueOf(Globals.GetTrackListURL) + "?Time=" + strTime.replace(" ", "") + "&n=10");
        }
    };
    private View.OnClickListener trackClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            int trackID = v.getId();
            if (ShareTrackListActivity.this.alTrackInfo != null && ShareTrackListActivity.this.alTrackInfo.size() > 0) {
                for (int i = 0; i < ShareTrackListActivity.this.alTrackInfo.size(); i++) {
                    if (trackID == ((TrackInfo) ShareTrackListActivity.this.alTrackInfo.get(i)).gettid()) {
                        String strURLFromServer = ((TrackInfo) ShareTrackListActivity.this.alTrackInfo.get(i)).geturl();
                        Intent intent = ShareTrackListActivity.this.getIntent();
                        intent.putExtra("URL", String.valueOf(Globals.URLRoot) + strURLFromServer.substring(1, strURLFromServer.length()));
                        ShareTrackListActivity.this.setResult(-1, intent);
                        ShareTrackListActivity.this.finish();
                        return;
                    }
                }
            }
        }
    };

    private class TrackInfo {
        private String disc;
        private int tid;
        private String time;
        private int uid;
        private String url;

        private TrackInfo() {
            this.tid = 1;
            this.uid = 1;
            this.disc = "";
            this.url = "";
            this.time = "";
        }

        /* synthetic */ TrackInfo(ShareTrackListActivity shareTrackListActivity, TrackInfo trackInfo) {
            this();
        }

        public void settid(int tid2) {
            this.tid = tid2;
        }

        public int gettid() {
            return this.tid;
        }

        public void setuid(int uid2) {
            this.uid = uid2;
        }

        public int getuid() {
            return this.uid;
        }

        public void setdisc(String disc2) {
            this.disc = disc2;
        }

        public String getdisc() {
            return this.disc;
        }

        public void seturl(String url2) {
            this.url = url2;
        }

        public String geturl() {
            return this.url;
        }

        public void settime(String time2) {
            this.time = time2;
        }

        public String gettime() {
            return this.time;
        }
    }

    private String GetResponse(String strURL) {
        try {
            URL url = new URL(strURL);
            ConnectivityManager conMan = (ConnectivityManager) getSystemService("connectivity");
            if (conMan == null) {
                return null;
            }
            NetworkInfo info = conMan.getActiveNetworkInfo();
            if (info == null) {
                return null;
            }
            HttpURLConnection con = CommUtil.getUrlConnectionNew(url, info);
            if (con == null) {
                return null;
            }
            String strResponse = CommUtil.GetResponse(con);
            con.disconnect();
            return strResponse;
        } catch (Exception e) {
            Exception ex = e;
            LogUtil.getInstance().log("SendUpdateRequest Error:" + ex.toString());
            return null;
        }
    }

    private ArrayList<TrackInfo> GetTrackInfoFromRes(String strResponse) {
        NodeList nl;
        if (strResponse == null || "".equals(strResponse)) {
            return null;
        }
        ArrayList<TrackInfo> alTrackInfoTemp = new ArrayList<>();
        try {
            Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(strResponse.getBytes()));
            if (doc == null || (nl = CommUtil.getNodeList(doc, "s")) == null || nl.getLength() <= 0) {
                return null;
            }
            if (CommUtil.getElementValue(nl.item(0)).equals("0")) {
                LogUtil.getInstance().log("Error:" + CommUtil.getElementValue(CommUtil.getNodeList(doc, "err").item(0)));
                return null;
            }
            NodeList nlTemp = CommUtil.getNodeList(doc, "track");
            if (nlTemp == null || nlTemp.getLength() <= 0) {
                return null;
            }
            for (int i = 0; i < nlTemp.getLength(); i++) {
                TrackInfo trackInfo = new TrackInfo(this, null);
                Node nodetemp = CommUtil.searchNode(nlTemp.item(i), "tid");
                if (nodetemp != null) {
                    trackInfo.settid(Integer.parseInt(CommUtil.getElementValue(nodetemp)));
                }
                Node nodetemp2 = CommUtil.searchNode(nlTemp.item(i), "uid");
                if (nodetemp2 != null) {
                    trackInfo.setuid(Integer.parseInt(CommUtil.getElementValue(nodetemp2)));
                }
                Node nodetemp3 = CommUtil.searchNode(nlTemp.item(i), "disc");
                if (nodetemp3 != null) {
                    trackInfo.setdisc(CommUtil.getElementValue(nodetemp3));
                }
                Node nodetemp4 = CommUtil.searchNode(nlTemp.item(i), "url");
                if (nodetemp4 != null) {
                    trackInfo.seturl(CommUtil.getElementValue(nodetemp4));
                }
                Node nodetemp5 = CommUtil.searchNode(nlTemp.item(i), "time");
                if (nodetemp5 != null) {
                    trackInfo.settime(CommUtil.getElementValue(nodetemp5));
                }
                alTrackInfoTemp.add(trackInfo);
            }
            return alTrackInfoTemp;
        } catch (Exception e) {
            LogUtil.getInstance().log("GetUserInfoFromRes Failed " + e.toString());
            return null;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.sharetracklist);
        getTrackInfoList(String.valueOf(Globals.GetTrackListURL) + "?n=30");
        ((Button) findViewById(R.id.btprev)).setOnClickListener(this.btPrevOnClick);
        ((Button) findViewById(R.id.btnext)).setOnClickListener(this.btNextOnClick);
        ((TableLayout) findViewById(R.id.tableLayout2)).setVerticalScrollBarEnabled(true);
        super.onCreate(savedInstanceState);
    }

    /* access modifiers changed from: private */
    public void getTrackInfoList(String strURL) {
        this.alTrackInfo = GetTrackInfoFromRes(GetResponse(strURL));
        if (this.alTrackInfo != null && this.alTrackInfo.size() > 0) {
            if (this.alstrPrevTime == null) {
                this.alstrPrevTime = new ArrayList<>();
            }
            this.alstrPrevTime.add(this.alTrackInfo.get(0).gettime());
            TableLayout TrackTable = (TableLayout) findViewById(R.id.tableLayout2);
            TrackTable.setStretchAllColumns(true);
            for (int i = 0; i < this.alTrackInfo.size(); i++) {
                TableRow tr = new TableRow(this);
                TextView tvDisc = new TextView(this);
                tvDisc.setTextSize(18.0f);
                tvDisc.setTextColor(Color.rgb(150, 150, 255));
                DisplayMetrics dm = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(dm);
                tvDisc.setWidth((int) (((double) dm.widthPixels) * 0.65d));
                tvDisc.setText(Html.fromHtml("<u>" + this.alTrackInfo.get(i).getdisc() + "</u>"));
                tvDisc.setId(this.alTrackInfo.get(i).gettid());
                tvDisc.setClickable(true);
                tvDisc.setOnClickListener(this.trackClickListener);
                tr.addView(tvDisc);
                TextView tvTime = new TextView(this);
                tvTime.setText(ChangeTimeString(this.alTrackInfo.get(i).gettime()));
                tr.addView(tvTime);
                TrackTable.addView(tr, new TableLayout.LayoutParams(-1, -2));
            }
        }
    }

    private String ChangeTimeString(String strTime) {
        String strResult = strTime.substring(1, 19);
        Calendar cldTemp = Calendar.getInstance(TimeZone.getTimeZone("GMT+8"));
        try {
            Date dateServer = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(strTime);
            Date dateNow = cldTemp.getTime();
            int year = dateNow.getYear() - dateServer.getYear();
            int month = dateNow.getMonth() - dateServer.getMonth();
            int days = dateNow.getDate() - dateServer.getDate();
            int hours = dateNow.getHours() - dateServer.getHours();
            int minutes = dateNow.getMinutes() - dateServer.getMinutes();
            if (year == 0 && month == 0) {
                strResult = days == 0 ? hours == 0 ? String.valueOf(String.valueOf(minutes)) + "分钟前" : String.valueOf(String.valueOf(hours)) + "小时前" : days == 1 ? String.valueOf(String.valueOf(hours + 24)) + "小时前" : String.valueOf(String.valueOf(days)) + "天前";
            }
            return strResult;
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }
}
