package a.b.a;

import android.os.Parcel;
import android.os.Parcelable;

public final class c implements Parcelable {

    /* renamed from: a  reason: collision with root package name */
    public static final Parcelable.Creator f189a = new Parcelable.Creator() {
        /* renamed from: a */
        public c createFromParcel(Parcel parcel) {
            return new c(parcel, null);
        }

        /* renamed from: a */
        public c[] newArray(int i) {
            return new c[i];
        }
    };

    /* renamed from: byte  reason: not valid java name */
    private double f68byte;

    /* renamed from: do  reason: not valid java name */
    private int f69do;

    /* renamed from: for  reason: not valid java name */
    private float f70for;

    /* renamed from: if  reason: not valid java name */
    private double f71if;

    /* renamed from: int  reason: not valid java name */
    private double f72int;

    /* renamed from: new  reason: not valid java name */
    private int f73new;

    /* renamed from: try  reason: not valid java name */
    private long f74try;

    public c() {
    }

    private c(Parcel parcel) {
        this.f72int = parcel.readDouble();
        this.f68byte = parcel.readDouble();
        this.f71if = parcel.readDouble();
        this.f70for = parcel.readFloat();
        this.f74try = parcel.readLong();
        this.f73new = parcel.readInt();
        this.f69do = parcel.readInt();
    }

    /* synthetic */ c(Parcel parcel, c cVar) {
        this(parcel);
    }

    public double a() {
        return this.f68byte;
    }

    public void a(double d) {
        this.f68byte = d;
    }

    public void a(float f) {
        this.f70for = f;
    }

    public void a(int i) {
        this.f73new = i;
    }

    public void a(long j) {
        this.f74try = j;
    }

    public int describeContents() {
        return 0;
    }

    /* renamed from: do  reason: not valid java name */
    public double m31do() {
        return this.f72int;
    }

    /* renamed from: do  reason: not valid java name */
    public void m32do(double d) {
        this.f71if = d;
    }

    /* renamed from: for  reason: not valid java name */
    public int m33for() {
        return this.f69do;
    }

    /* renamed from: if  reason: not valid java name */
    public float m34if() {
        return this.f70for;
    }

    /* renamed from: if  reason: not valid java name */
    public void m35if(double d) {
        this.f72int = d;
    }

    /* renamed from: if  reason: not valid java name */
    public void m36if(int i) {
        this.f69do = i;
    }

    /* renamed from: int  reason: not valid java name */
    public double m37int() {
        return this.f71if;
    }

    /* renamed from: new  reason: not valid java name */
    public int m38new() {
        return this.f73new;
    }

    /* renamed from: try  reason: not valid java name */
    public long m39try() {
        return this.f74try;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeDouble(this.f72int);
        parcel.writeDouble(this.f68byte);
        parcel.writeDouble(this.f71if);
        parcel.writeFloat(this.f70for);
        parcel.writeLong(this.f74try);
        parcel.writeInt(this.f73new);
        parcel.writeInt(this.f69do);
    }
}
