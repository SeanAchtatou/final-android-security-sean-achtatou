package a.b.a;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import cmcc.location.core.LogUtil;
import cmcc.location.core.e;
import cmcc.location.core.h;
import cmcc.location.core.n;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class b implements a {

    /* renamed from: try  reason: not valid java name */
    private static b f62try = null;

    /* renamed from: byte  reason: not valid java name */
    private ExecutorService f63byte = Executors.newCachedThreadPool();

    /* renamed from: case  reason: not valid java name */
    private final Context f64case;

    /* renamed from: for  reason: not valid java name */
    private final HashMap f65for = new HashMap();

    /* renamed from: int  reason: not valid java name */
    private h f66int = new h();

    /* renamed from: new  reason: not valid java name */
    private final n f67new;

    public b(Context context) {
        this.f64case = context;
        this.f67new = new n(context);
    }

    public static b a(Context context) {
        if (context == null) {
            throw new IllegalArgumentException();
        }
        synchronized (b.class) {
            if (f62try == null) {
                f62try = new b(context);
            }
        }
        return f62try;
    }

    public synchronized c a(int i, int i2) {
        c cVar;
        cVar = new c();
        try {
            Location a2 = this.f66int.a(this.f64case, "PosReq_FC", String.valueOf(i), String.valueOf(i2));
            if (a2 != null) {
                cVar.a(a2.getLatitude());
                cVar.m35if(a2.getLongitude());
                cVar.a(a2.getAccuracy());
                cVar.m32do(a2.getAltitude());
                new Bundle();
                Bundle extras = a2.getExtras();
                cVar.a(extras.getLong(e.f));
                String string = extras.getString(e.c);
                if (string != "") {
                    cVar.m36if(Integer.parseInt(string));
                }
                String string2 = extras.getString(e.d);
                if (string2 != "") {
                    cVar.a(Integer.parseInt(string2));
                }
            }
        } catch (Exception e) {
            LogUtil.getInstance().log("getLocation:" + e.toString());
            Log.e("Locator.getLocation", "Get Location Failed!", e);
        }
        return cVar;
    }

    public synchronized Location a(int i) {
        Location location;
        if (i == -99) {
            location = null;
        } else {
            location = (Location) this.f65for.get(Integer.valueOf(i));
            this.f65for.remove(Integer.valueOf(i));
        }
        return location;
    }

    public IBinder asBinder() {
        return null;
    }

    /* renamed from: do  reason: not valid java name */
    public int m29do(int i) {
        int i2;
        if (i == -99) {
            return i;
        }
        try {
            Location location = (Location) this.f65for.get(Integer.valueOf(i));
            if (location == null) {
                return -99;
            }
            String a2 = new h().a(location, e.c);
            if (a2 == null || "".equals(a2)) {
                this.f65for.remove(Integer.valueOf(i));
                return -99;
            }
            i2 = Integer.parseInt(a2);
            try {
                if (!e.t.equals(a2)) {
                    this.f65for.remove(Integer.valueOf(i));
                    return i2;
                }
            } catch (Exception e) {
                e = e;
                LogUtil.getInstance().log("getErrCode:" + e.toString());
                Log.e("Locator.getErrCode", "Get ErrCode Failed!");
                return i2;
            }
            return i2;
        } catch (Exception e2) {
            e = e2;
            i2 = -99;
            LogUtil.getInstance().log("getErrCode:" + e.toString());
            Log.e("Locator.getErrCode", "Get ErrCode Failed!");
            return i2;
        }
    }

    /* renamed from: if  reason: not valid java name */
    public int m30if(int i) {
        if (i == -99) {
            return i;
        }
        Location location = (Location) this.f65for.get(Integer.valueOf(i));
        if (location == null) {
            return -99;
        }
        String a2 = new h().a(location, e.d);
        if (a2 == null || "".equals(a2)) {
            return -99;
        }
        if (e.b.equals(a2)) {
            return 0;
        }
        if (e.f213a.equals(a2)) {
            return 1;
        }
        return e.k.equals(a2) ? 2 : -99;
    }
}
