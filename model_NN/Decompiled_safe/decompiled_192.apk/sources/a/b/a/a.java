package a.b.a;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface a extends IInterface {

    /* renamed from: a.b.a.a$a  reason: collision with other inner class name */
    public static abstract class C0001a extends Binder implements a {

        /* renamed from: a  reason: collision with root package name */
        private static final String f188a = "oms.fpp.service.IFppService";

        /* renamed from: if  reason: not valid java name */
        static final int f60if = 1;

        /* renamed from: a.b.a.a$a$a  reason: collision with other inner class name */
        private static class C0002a implements a {

            /* renamed from: char  reason: not valid java name */
            private IBinder f61char;

            C0002a(IBinder iBinder) {
                this.f61char = iBinder;
            }

            public c a(int i, int i2) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(C0001a.f188a);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    this.f61char.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (c) c.f189a.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public String a() {
                return C0001a.f188a;
            }

            public IBinder asBinder() {
                return this.f61char;
            }
        }

        public C0001a() {
            attachInterface(this, f188a);
        }

        public static a a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface(f188a);
            return (queryLocalInterface == null || !(queryLocalInterface instanceof a)) ? new C0002a(iBinder) : (a) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            switch (i) {
                case 1:
                    parcel.enforceInterface(f188a);
                    c a2 = a(parcel.readInt(), parcel.readInt());
                    parcel2.writeNoException();
                    if (a2 != null) {
                        parcel2.writeInt(1);
                        a2.writeToParcel(parcel2, 1);
                    } else {
                        parcel2.writeInt(0);
                    }
                    return true;
                case 1598968902:
                    parcel2.writeString(f188a);
                    return true;
                default:
                    return super.onTransact(i, parcel, parcel2, i2);
            }
        }
    }

    c a(int i, int i2) throws RemoteException;
}
