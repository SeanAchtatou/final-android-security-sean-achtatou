package a.b.b;

import a.b.a.a;
import a.b.a.c;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import cmcc.location.core.e;
import cmcc.location.core.h;

public class a extends Service {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public h f190a = new h();

    public IBinder onBind(Intent intent) {
        Log.d("AidlService.onBind", "AidlService onBind!");
        return new a.C0001a() {
            public c a(int i, int i2) throws RemoteException {
                Log.d("AidlService.onBind", "AidlService Get Location is invoked, SPID : " + i + ", SERID : " + i2);
                try {
                    Location a2 = a.this.f190a.a(a.this, "PosReq_FC", String.valueOf(i), String.valueOf(i2));
                    if (a2 == null) {
                        Log.d("AidlService.onBind", "AidlService Return location is null!");
                        return null;
                    }
                    Log.d("AidlService.onBind", "AidlService Get location Success!");
                    Log.d("AidlService.onBind", "AidlService Create FppLocation Object!");
                    c cVar = new c();
                    cVar.m32do(a2.getAltitude());
                    cVar.a(a2.getExtras().getLong(e.f));
                    cVar.m36if(Integer.parseInt(a2.getExtras().getString(e.c)));
                    cVar.a(a2.getAccuracy());
                    cVar.a(a2.getLatitude());
                    cVar.m35if(a2.getLongitude());
                    int i3 = e.b.equals(a2.getExtras().getString(e.d)) ? 0 : e.f213a.equals(a2.getExtras().getString(e.d)) ? 1 : e.k.equals(a2.getExtras().getString(e.d)) ? 2 : 0;
                    cVar.a(i3);
                    Log.d("AidlService.onBind", "AidlService Location type " + i3);
                    return cVar;
                } catch (Exception e) {
                    Log.e("AidlService.onBind", "AidlService Get Location Failed!", e);
                    return null;
                }
            }
        };
    }

    public void onDestroy() {
        super.onDestroy();
        a.a.a.c.m16if().a();
        Log.d("AidlService.onDestroy", "AidlService onDestroy!");
    }

    public boolean onUnbind(Intent intent) {
        Log.d("AidlService.onUnbind", "AidlService onUnbind!");
        return true;
    }
}
