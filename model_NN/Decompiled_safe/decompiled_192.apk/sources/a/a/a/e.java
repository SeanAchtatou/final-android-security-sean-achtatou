package a.a.a;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    public static final int f185a = 0;
    public static final String b = "apnSwitched";

    /* renamed from: byte  reason: not valid java name */
    public static final String f40byte = "psRestrictDisabled";
    public static final String c = "apnChanged";

    /* renamed from: case  reason: not valid java name */
    public static final String f41case = "dataOnBootOn";

    /* renamed from: char  reason: not valid java name */
    public static final String f42char = "simLoaded";
    public static final String d = "apnFailed";

    /* renamed from: do  reason: not valid java name */
    public static final int f43do = 3;
    public static final String e = "2GVoiceCallEnded";

    /* renamed from: else  reason: not valid java name */
    public static final String f44else = "roamingOff";
    public static final String f = "psRestrictEnabled";

    /* renamed from: for  reason: not valid java name */
    public static final String f45for = "gprsDetached";
    public static final String g = "radioTurnedOff";

    /* renamed from: goto  reason: not valid java name */
    public static final String f46goto = "wap";
    public static final String h = "restoreDefaultApn";
    public static final String i = "mms";

    /* renamed from: if  reason: not valid java name */
    public static final String f47if = "dataOnBootOff";

    /* renamed from: int  reason: not valid java name */
    public static final String f48int = "notFindFreePdp";
    public static final String j = "dataEnabled";
    public static final String k = "2GVoiceCallStarted";
    public static final String l = "roamingOn";

    /* renamed from: long  reason: not valid java name */
    public static final String f49long = "dataDisabled";
    public static final int m = 1;
    public static final int n = 2;

    /* renamed from: new  reason: not valid java name */
    public static final String f50new = "gprsAttached";
    public static final String o = "pdpReset";

    /* renamed from: try  reason: not valid java name */
    public static final String f51try = "internet";

    /* renamed from: void  reason: not valid java name */
    public static final String f52void = "deactivedByNetwork";
}
