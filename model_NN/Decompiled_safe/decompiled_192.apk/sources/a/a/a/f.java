package a.a.a;

import a.a.a.d;
import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class f {

    /* renamed from: int  reason: not valid java name */
    private static final String f53int = f.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    public String f186a;

    /* renamed from: do  reason: not valid java name */
    private Context f54do;

    /* renamed from: for  reason: not valid java name */
    private Semaphore f55for = new Semaphore(0);

    /* renamed from: if  reason: not valid java name */
    private a f56if;

    /* renamed from: new  reason: not valid java name */
    public ConnectivityManager f57new;

    public class a extends Handler {

        /* renamed from: if  reason: not valid java name */
        private static final int f58if = 1;

        /* renamed from: do  reason: not valid java name */
        private a f59do;

        public a(Looper looper, Context context) {
            super(looper);
            this.f59do = new a(context);
            this.f59do.a(this, 1);
            this.f59do.m7for();
        }

        public void a() {
            this.f59do.m11try();
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    if (this.f59do != null) {
                        NetworkInfo networkInfo = this.f59do.m9int();
                        if (networkInfo != null) {
                            String typeName = networkInfo.getTypeName();
                            NetworkInfo.State state = networkInfo.getState();
                            if (state != NetworkInfo.State.CONNECTED) {
                                if (state == NetworkInfo.State.DISCONNECTED) {
                                    NetworkInfo.DetailedState detailedState = networkInfo.getDetailedState();
                                    String reason = networkInfo.getReason();
                                    if (detailedState != NetworkInfo.DetailedState.FAILED && !TextUtils.equals(reason, e.f52void)) {
                                        TextUtils.equals(reason, e.l);
                                        break;
                                    }
                                }
                            } else if (!TextUtils.equals(e.f51try, typeName)) {
                                f.this.f186a = e.f51try;
                                break;
                            } else {
                                f.this.f186a = "";
                                break;
                            }
                        }
                    } else {
                        return;
                    }
                    break;
            }
            f.this.a();
        }
    }

    public f(Context context) {
        this.f54do = context;
        this.f57new = (ConnectivityManager) this.f54do.getSystemService("connectivity");
        this.f56if = new a(Looper.myLooper(), this.f54do);
    }

    /* renamed from: if  reason: not valid java name */
    private void m27if(long j) {
        int i = 1;
        long j2 = j <= 0 ? 300 : j <= 100 ? 1 : j / 100;
        while (i < 100 && !a(j2)) {
            if (j > 0) {
                i++;
            }
        }
    }

    public void a() {
        this.f55for.release();
    }

    public boolean a(int i, long j) {
        if (i == -1) {
            return false;
        }
        Cursor query = this.f54do.getContentResolver().query(d.a.f27byte, new String[]{"type"}, String.format("%s=\"%s\"", d.a.f, Integer.valueOf(i)), null, null);
        if (query == null) {
            return false;
        }
        return a(query.moveToNext() ? query.getString(0) : null, j);
    }

    public boolean a(long j) {
        if (j > 0) {
            try {
                return this.f55for.tryAcquire(j, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
                return false;
            }
        } else {
            this.f55for.acquire();
            return true;
        }
    }

    public boolean a(String str, long j) {
        if (str == null) {
            return false;
        }
        if (this.f56if == null) {
            return false;
        }
        return this.f57new.startUsingNetworkFeature(1, str) != -1;
    }

    /* renamed from: if  reason: not valid java name */
    public void m28if() {
        this.f56if.a();
        this.f56if = null;
    }
}
