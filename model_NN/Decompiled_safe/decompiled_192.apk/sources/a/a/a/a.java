package a.a.a;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class a {

    /* renamed from: int  reason: not valid java name */
    private static final String f0int = "NetworkConnectivityListener";

    /* renamed from: try  reason: not valid java name */
    private static final boolean f1try = true;

    /* renamed from: a  reason: collision with root package name */
    private Context f177a;

    /* renamed from: byte  reason: not valid java name */
    private b f2byte = new b(this, null);
    /* access modifiers changed from: private */

    /* renamed from: case  reason: not valid java name */
    public NetworkInfo f3case;
    /* access modifiers changed from: private */

    /* renamed from: char  reason: not valid java name */
    public String f4char;
    /* access modifiers changed from: private */

    /* renamed from: do  reason: not valid java name */
    public boolean f5do;
    /* access modifiers changed from: private */

    /* renamed from: else  reason: not valid java name */
    public NetworkInfo f6else;
    /* access modifiers changed from: private */

    /* renamed from: for  reason: not valid java name */
    public int f7for;
    /* access modifiers changed from: private */

    /* renamed from: goto  reason: not valid java name */
    public Handler f8goto;
    /* access modifiers changed from: private */

    /* renamed from: if  reason: not valid java name */
    public C0000a f9if = C0000a.UNKNOWN;
    /* access modifiers changed from: private */

    /* renamed from: new  reason: not valid java name */
    public boolean f10new;

    /* renamed from: a.a.a.a$a  reason: collision with other inner class name */
    public enum C0000a {
        UNKNOWN,
        CONNECTED,
        NOT_CONNECTED;

        public static C0000a[] a() {
            C0000a[] aVarArr = f178a;
            int length = aVarArr.length;
            C0000a[] aVarArr2 = new C0000a[length];
            System.arraycopy(aVarArr, 0, aVarArr2, 0, length);
            return aVarArr2;
        }
    }

    private class b extends BroadcastReceiver {
        private b() {
        }

        /* synthetic */ b(a aVar, b bVar) {
            this();
        }

        public void onReceive(Context context, Intent intent) {
            if (!intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE") || !a.this.f5do) {
                Log.w(a.f0int, "onReceived() called with " + a.this.f9if.toString() + " and " + intent);
                return;
            }
            boolean booleanExtra = intent.getBooleanExtra("noConnectivity", false);
            if (booleanExtra) {
                a.this.f9if = C0000a.NOT_CONNECTED;
            } else {
                a.this.f9if = C0000a.CONNECTED;
            }
            a.this.f3case = (NetworkInfo) intent.getParcelableExtra("networkInfo");
            a.this.f6else = (NetworkInfo) intent.getParcelableExtra("otherNetwork");
            a.this.f4char = intent.getStringExtra("reason");
            a.this.f10new = intent.getBooleanExtra("isFailover", false);
            Log.d(a.f0int, "onReceive(): mNetworkInfo=" + a.this.f3case + " mOtherNetworkInfo = " + (a.this.f6else == null ? "[none]" : a.this.f6else + " noConn=" + booleanExtra) + " mState=" + a.this.f9if.toString());
            a.this.f8goto.sendMessage(Message.obtain(a.this.f8goto, a.this.f7for));
        }
    }

    public a(Context context) {
        this.f177a = context;
    }

    public void a(Handler handler, int i) {
        this.f8goto = handler;
        this.f7for = i;
    }

    public boolean a() {
        return this.f10new;
    }

    /* renamed from: do  reason: not valid java name */
    public String m6do() {
        return this.f4char;
    }

    /* renamed from: for  reason: not valid java name */
    public synchronized void m7for() {
        if (!this.f5do) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            this.f177a.registerReceiver(this.f2byte, intentFilter);
            this.f5do = f1try;
        }
    }

    /* renamed from: if  reason: not valid java name */
    public C0000a m8if() {
        return this.f9if;
    }

    /* renamed from: int  reason: not valid java name */
    public NetworkInfo m9int() {
        return this.f3case;
    }

    /* renamed from: new  reason: not valid java name */
    public NetworkInfo m10new() {
        return this.f6else;
    }

    /* renamed from: try  reason: not valid java name */
    public synchronized void m11try() {
        Log.d(f0int, "stopListening... may set the mNetworkInfo to null? :" + this.f5do);
        if (this.f5do) {
            this.f177a.unregisterReceiver(this.f2byte);
            this.f177a = null;
            this.f3case = null;
            this.f6else = null;
            this.f10new = false;
            this.f4char = null;
            this.f5do = false;
        }
    }
}
