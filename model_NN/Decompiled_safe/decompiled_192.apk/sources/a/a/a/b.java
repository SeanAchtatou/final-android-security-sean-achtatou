package a.a.a;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

/*  JADX ERROR: NullPointerException in pass: ExtractFieldInit
    java.lang.NullPointerException
    	at jadx.core.utils.BlockUtils.isAllBlocksEmpty(BlockUtils.java:608)
    	at jadx.core.dex.visitors.ExtractFieldInit.getConstructorsList(ExtractFieldInit.java:241)
    	at jadx.core.dex.visitors.ExtractFieldInit.moveCommonFieldsInit(ExtractFieldInit.java:122)
    	at jadx.core.dex.visitors.ExtractFieldInit.visit(ExtractFieldInit.java:43)
    */
public class b {

    /* renamed from: a  reason: collision with root package name */
    protected String[] f180a;

    /* renamed from: do  reason: not valid java name */
    protected String[] f14do;

    /* renamed from: if  reason: not valid java name */
    protected Context f15if;

    protected static final class a {

        /* renamed from: a  reason: collision with root package name */
        public static final Uri f181a = Uri.parse("content://telephony/apgroups");

        /* renamed from: do  reason: not valid java name */
        public static final String f16do = "_id ASC";

        /* renamed from: for  reason: not valid java name */
        public static final String f17for = "name";

        /* renamed from: if  reason: not valid java name */
        public static final String f18if = "visible";

        /* renamed from: int  reason: not valid java name */
        public static final String f19int = "type";

        protected a() {
        }
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: java.lang.Object.<init>():void in method: a.a.a.b.<init>(android.content.Context):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: java.lang.Object.<init>():void
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:534)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public b(android.content.Context r1) {
        /*
            r1 = this;
            r0 = 0
            r1.<init>()
            r1.f15if = r0
            r1.f14do = r0
            r1.f180a = r0
            r1.f15if = r2
            r1.m12do()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.b.<init>(android.content.Context):void");
    }

    public String a(int i) {
        if (this.f180a == null || i >= this.f180a.length) {
            return null;
        }
        return this.f180a[i];
    }

    public String[] a() {
        return this.f14do;
    }

    /* access modifiers changed from: protected */
    /* renamed from: do  reason: not valid java name */
    public void m12do() {
        Cursor query = this.f15if.getContentResolver().query(a.f181a, new String[]{"name", "type"}, null, null, a.f16do);
        if (query != null) {
            int count = query.getCount();
            this.f14do = new String[count];
            this.f180a = new String[count];
            int i = 0;
            while (query.moveToNext()) {
                try {
                    this.f14do[i] = query.getString(0);
                    this.f180a[i] = query.getString(1);
                    i++;
                } finally {
                    query.close();
                }
            }
        }
    }

    /* renamed from: for  reason: not valid java name */
    public String[] m13for() {
        return this.f180a;
    }

    /* renamed from: if  reason: not valid java name */
    public int m14if() {
        if (this.f14do == null) {
            return 0;
        }
        return this.f14do.length;
    }

    /* renamed from: if  reason: not valid java name */
    public String m15if(int i) {
        if (this.f14do == null || i >= this.f14do.length) {
            return null;
        }
        return this.f14do[i];
    }
}
