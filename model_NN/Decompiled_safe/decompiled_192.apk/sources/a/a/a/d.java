package a.a.a;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

public class d {

    /* renamed from: a  reason: collision with root package name */
    protected String[] f183a = null;

    /* renamed from: do  reason: not valid java name */
    protected String[] f21do = null;

    /* renamed from: for  reason: not valid java name */
    protected Context f22for = null;

    /* renamed from: if  reason: not valid java name */
    protected int[] f23if = null;

    /* renamed from: int  reason: not valid java name */
    protected int[] f24int = null;

    /* renamed from: new  reason: not valid java name */
    protected String[] f25new = null;

    /* renamed from: try  reason: not valid java name */
    protected String[] f26try = null;

    protected static final class a {

        /* renamed from: a  reason: collision with root package name */
        public static final String f184a = "server";
        public static final String b = "port";

        /* renamed from: byte  reason: not valid java name */
        public static final Uri f27byte = Uri.parse("content://telephony/carriers");
        public static final String c = "mmsport";

        /* renamed from: case  reason: not valid java name */
        public static final String f28case = "type";

        /* renamed from: char  reason: not valid java name */
        public static final String f29char = "apn";
        public static final String d = "numeric";

        /* renamed from: do  reason: not valid java name */
        public static final String f30do = "name";
        public static final String e = "mmsc";

        /* renamed from: else  reason: not valid java name */
        public static final String f31else = "password";
        public static final String f = "_id";

        /* renamed from: for  reason: not valid java name */
        public static final String f32for = "user";
        public static final String g = "mcc";

        /* renamed from: goto  reason: not valid java name */
        public static final String f33goto = "mnc";

        /* renamed from: if  reason: not valid java name */
        public static final String f34if = "current";

        /* renamed from: int  reason: not valid java name */
        public static final String f35int = "proxy";

        /* renamed from: long  reason: not valid java name */
        public static final String f36long = "preloaded";

        /* renamed from: new  reason: not valid java name */
        public static final String f37new = "name ASC";

        /* renamed from: try  reason: not valid java name */
        public static final String f38try = "mmsproxy";

        /* renamed from: void  reason: not valid java name */
        public static final String f39void = "visible";

        protected a() {
        }
    }

    public d(Context context) {
        this.f22for = context;
        m24int();
    }

    public int a() {
        if (this.f21do == null) {
            return 0;
        }
        return this.f21do.length;
    }

    public String a(int i) {
        if (this.f21do == null || i >= this.f21do.length) {
            return null;
        }
        return this.f21do[i];
    }

    /* renamed from: do  reason: not valid java name */
    public int m17do(int i) {
        if (this.f23if == null || i >= this.f23if.length) {
            return -1;
        }
        return this.f23if[i];
    }

    /* renamed from: do  reason: not valid java name */
    public String[] m18do() {
        return this.f25new;
    }

    /* renamed from: for  reason: not valid java name */
    public String m19for(int i) {
        if (this.f183a == null || i >= this.f183a.length) {
            return null;
        }
        return this.f183a[i];
    }

    /* renamed from: for  reason: not valid java name */
    public String[] m20for() {
        return this.f183a;
    }

    /* renamed from: if  reason: not valid java name */
    public String m21if(int i) {
        if (this.f25new == null || i >= this.f25new.length) {
            return null;
        }
        return this.f25new[i];
    }

    /* renamed from: if  reason: not valid java name */
    public String[] m22if() {
        return this.f21do;
    }

    /* renamed from: int  reason: not valid java name */
    public String m23int(int i) {
        if (this.f26try == null || i >= this.f26try.length) {
            return null;
        }
        return this.f26try[i];
    }

    /* access modifiers changed from: protected */
    /* renamed from: int  reason: not valid java name */
    public void m24int() {
        Cursor query = this.f22for.getContentResolver().query(a.f27byte, new String[]{a.f, "name", a.f29char, "type", a.f35int, a.b}, null, null, "type");
        if (query != null) {
            int count = query.getCount();
            this.f23if = new int[count];
            this.f21do = new String[count];
            this.f183a = new String[count];
            this.f25new = new String[count];
            this.f26try = new String[count];
            this.f24int = new int[count];
            int i = 0;
            while (query.moveToNext()) {
                try {
                    this.f23if[i] = query.getInt(0);
                    this.f21do[i] = query.getString(1);
                    this.f183a[i] = query.getString(2);
                    this.f25new[i] = query.getString(3);
                    this.f26try[i] = query.getString(4);
                    this.f24int[i] = query.getInt(5);
                    Log.d("TEST", String.valueOf(this.f21do[i]) + "; " + this.f183a[i] + "; " + this.f25new[i]);
                    i++;
                } finally {
                    query.close();
                }
            }
        }
    }

    /* renamed from: new  reason: not valid java name */
    public int m25new(int i) {
        if (this.f24int == null || i >= this.f24int.length) {
            return -1;
        }
        return this.f24int[i];
    }

    /* renamed from: new  reason: not valid java name */
    public int[] m26new() {
        return this.f23if;
    }
}
