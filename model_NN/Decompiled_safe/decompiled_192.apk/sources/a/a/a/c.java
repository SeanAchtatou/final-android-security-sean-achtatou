package a.a.a;

import android.content.Context;
import cmcc.location.core.h;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private static c f182a = null;

    /* renamed from: if  reason: not valid java name */
    private f f20if = null;

    private c() {
    }

    /* renamed from: if  reason: not valid java name */
    public static synchronized c m16if() {
        c cVar;
        synchronized (c.class) {
            if (f182a == null) {
                f182a = new c();
            }
            cVar = f182a;
        }
        return cVar;
    }

    public void a() {
        if (this.f20if != null) {
            this.f20if.m28if();
            this.f20if = null;
        }
    }

    public String[] a(Context context, int i) {
        if (context == null || i < 0) {
            throw new IllegalArgumentException();
        }
        f fVar = new f(context);
        d dVar = new d(context);
        String[] strArr = new String[4];
        int i2 = 0;
        while (true) {
            if (i2 < 2) {
                i2++;
                boolean a2 = fVar.a(dVar.m17do(i), 2000);
                strArr[0] = String.valueOf(a2);
                if (a2) {
                    strArr[1] = dVar.m21if(i);
                    strArr[2] = dVar.m23int(i);
                    strArr[3] = String.valueOf(dVar.m25new(i));
                    break;
                }
            } else {
                break;
            }
        }
        new h().a(strArr);
        return strArr;
    }
}
