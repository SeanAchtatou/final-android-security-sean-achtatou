package DrWebsterApps.New.England.Patriots.Trivia;

public final class R {

    public static final class anim {
        public static final int custom_anim = 2130968576;
        public static final int fade_in = 2130968577;
        public static final int fade_in2 = 2130968578;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int Blue = 2131099656;
        public static final int Green = 2131099655;
        public static final int Red = 2131099657;
        public static final int White = 2131099658;
        public static final int logo_color = 2131099648;
        public static final int menu_color = 2131099653;
        public static final int menu_glow = 2131099654;
        public static final int title_color = 2131099651;
        public static final int title_glow = 2131099652;
        public static final int version_bkgrd = 2131099650;
        public static final int version_color = 2131099649;
    }

    public static final class dimen {
        public static final int answer_size = 2131165191;
        public static final int help_text_padding = 2131165190;
        public static final int help_text_size = 2131165189;
        public static final int logo_size = 2131165184;
        public static final int menu_item_size = 2131165188;
        public static final int question_size = 2131165192;
        public static final int screen_title_size = 2131165187;
        public static final int version_size = 2131165185;
        public static final int version_spacing = 2131165186;
    }

    public static final class drawable {
        public static final int button_text_default = 2130837512;
        public static final int button_text_disabled = 2130837513;
        public static final int button_text_focused = 2130837514;
        public static final int button_text_pressed = 2130837515;
        public static final int button_text_white_default = 2130837516;
        public static final int divider = 2130837504;
        public static final int divider_dark = 2130837517;
        public static final int divider_light = 2130837518;
        public static final int gillettestadium278 = 2130837505;
        public static final int icon = 2130837506;
        public static final int link_color = 2130837519;
        public static final int list_pressed_color = 2130837520;
        public static final int list_selected_color = 2130837521;
        public static final int list_unfocused_color = 2130837522;
        public static final int list_unselected_color = 2130837523;
        public static final int menu_row_divider_color = 2130837524;
        public static final int orange = 2130837525;
        public static final int screen_title_color = 2130837526;
        public static final int splash1 = 2130837507;
        public static final int splash2 = 2130837508;
        public static final int splash3 = 2130837509;
        public static final int splash4 = 2130837510;
        public static final int surrounded = 2130837527;
        public static final int tab_text_disabled = 2130837528;
        public static final int tab_text_enabled = 2130837529;
        public static final int tab_text_shadow_disabled = 2130837530;
        public static final int tab_text_shadow_enabled = 2130837531;
        public static final int tab_view_background = 2130837532;
        public static final int textured = 2130837511;
        public static final int vertical_divider_dark = 2130837533;
        public static final int vertical_divider_light = 2130837534;
    }

    public static final class id {
        public static final int EditText_Answer1 = 2131427349;
        public static final int EditText_Answer2 = 2131427350;
        public static final int EditText_Answer3 = 2131427351;
        public static final int EditText_Answer4 = 2131427352;
        public static final int EditText_Comment = 2131427353;
        public static final int EditText_Question = 2131427348;
        public static final int GameBackButton = 2131427335;
        public static final int ImageView2_Left = 2131427360;
        public static final int ImageView2_Right = 2131427361;
        public static final int ImageView3_Left = 2131427363;
        public static final int ImageView3_Right = 2131427364;
        public static final int ListView_Menu = 2131427342;
        public static final int QPopUp = 2131427345;
        public static final int QTextViewBottom = 2131427337;
        public static final int RadioButton01 = 2131427331;
        public static final int RadioButton02 = 2131427332;
        public static final int RadioButton03 = 2131427333;
        public static final int RadioButton04 = 2131427334;
        public static final int RadioGroup1 = 2131427330;
        public static final int RelativeLayout01 = 2131427339;
        public static final int RelativeLayout02 = 2131427341;
        public static final int SendEmailBack = 2131427355;
        public static final int SendEmailButton = 2131427356;
        public static final int Spinner_CorrectAnswer = 2131427354;
        public static final int TableLayout01 = 2131427358;
        public static final int TableRow01 = 2131427359;
        public static final int TableRow02 = 2131427362;
        public static final int TextView01 = 2131427340;
        public static final int TextViewBottomTitle = 2131427365;
        public static final int TextViewBottomVersion = 2131427366;
        public static final int TextViewQ = 2131427343;
        public static final int TextViewQ2 = 2131427329;
        public static final int TextViewTopTitle = 2131427357;
        public static final int TextView_Question = 2131427347;
        public static final int adView = 2131427338;
        public static final int button1 = 2131427336;
        public static final int help_menu_item = 2131427368;
        public static final int linearLayout1 = 2131427328;
        public static final int scrollerScores = 2131427344;
        public static final int scrolleraddq = 2131427346;
        public static final int settings_menu_item = 2131427367;
    }

    public static final class layout {
        public static final int game = 2130903040;
        public static final int help = 2130903041;
        public static final int menu = 2130903042;
        public static final int menu_item = 2130903043;
        public static final int question = 2130903044;
        public static final int scores = 2130903045;
        public static final int settings = 2130903046;
        public static final int splash = 2130903047;
    }

    public static final class menu {
        public static final int gameoptions = 2131361792;
    }

    public static final class string {
        public static final int app_logo_bottom = 2131230742;
        public static final int app_logo_top = 2131230741;
        public static final int app_name = 2131230735;
        public static final int app_version_info = 2131230743;
        public static final int button_label_saveit = 2131230730;
        public static final int center_filled = 2131230721;
        public static final int field_label_definition = 2131230734;
        public static final int field_label_firstname = 2131230731;
        public static final int field_label_lastname = 2131230732;
        public static final int field_label_term = 2131230733;
        public static final int game = 2131230738;
        public static final int gravity_center_text = 2131230725;
        public static final int hello = 2131230720;
        public static final int help = 2131230740;
        public static final int layout_gravity_and_gravity_center_text = 2131230726;
        public static final int layout_gravity_center_text = 2131230724;
        public static final int left_right = 2131230727;
        public static final int left_right_top_text = 2131230723;
        public static final int left_right_vertical_text = 2131230722;
        public static final int menu = 2131230744;
        public static final int menu_item_help = 2131230750;
        public static final int menu_item_morepps = 2131230748;
        public static final int menu_item_play = 2131230747;
        public static final int menu_item_schedule = 2131230746;
        public static final int menu_item_settings = 2131230745;
        public static final int required_field_indicator = 2131230729;
        public static final int required_instructions = 2131230728;
        public static final int scores = 2131230739;
        public static final int settings = 2131230737;
        public static final int splash = 2131230736;
        public static final int teamname = 2131230749;
    }

    public static final class style {
        public static final int button = 2131296270;
        public static final int clickablerow = 2131296262;
        public static final int field = 2131296268;
        public static final int field_textArea = 2131296269;
        public static final int instructions = 2131296271;
        public static final int label = 2131296266;
        public static final int label_noWeight = 2131296267;
        public static final int mainContainer = 2131296258;
        public static final int required_field_indicator = 2131296265;
        public static final int row = 2131296263;
        public static final int rowDivider = 2131296259;
        public static final int row_first = 2131296264;
        public static final int text = 2131296260;
        public static final int text_menu = 2131296261;
        public static final int vertical_fill = 2131296256;
        public static final int vertical_fill_light = 2131296257;
    }

    public static final class xml {
        public static final int celticstrivia = 2131034112;
    }
}
