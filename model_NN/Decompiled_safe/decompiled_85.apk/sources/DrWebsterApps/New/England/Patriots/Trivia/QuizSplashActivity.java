package DrWebsterApps.New.England.Patriots.Trivia;

import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.google.ads.AdView;

public class QuizSplashActivity extends QuizActivity {
    private static final String TEST_EMULATOR = "Celtics test";
    AdView adView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.splash);
        setTitle(getResources().getString(R.string.app_name));
        startAnimating();
    }

    private void startAnimating() {
        ((TextView) findViewById(R.id.TextViewTopTitle)).startAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_in));
        Animation fade2 = AnimationUtils.loadAnimation(this, R.anim.fade_in2);
        ((TextView) findViewById(R.id.TextViewBottomTitle)).startAnimation(fade2);
        fade2.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                QuizSplashActivity.this.startActivity(new Intent(QuizSplashActivity.this, QuizMenuActivity.class));
                QuizSplashActivity.this.finish();
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        LayoutAnimationController controller = new LayoutAnimationController(AnimationUtils.loadAnimation(this, R.anim.custom_anim));
        TableLayout table = (TableLayout) findViewById(R.id.TableLayout01);
        for (int i = 0; i < table.getChildCount(); i++) {
            ((TableRow) table.getChildAt(i)).setLayoutAnimation(controller);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        ((TextView) findViewById(R.id.TextViewTopTitle)).clearAnimation();
        ((TextView) findViewById(R.id.TextViewBottomTitle)).clearAnimation();
        TableLayout table = (TableLayout) findViewById(R.id.TableLayout01);
        for (int i = 0; i < table.getChildCount(); i++) {
            ((TableRow) table.getChildAt(i)).clearAnimation();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        startAnimating();
    }
}
