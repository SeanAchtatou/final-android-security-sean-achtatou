package DrWebsterApps.New.England.Patriots.Trivia;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

public class QuizSettingsActivity extends QuizActivity {
    static final int DATE_DIALOG_ID = 0;
    static final int PASSWORD_DIALOG_ID = 1;
    String[] AllAnswers;
    EditText Answer1Text;
    EditText Answer2Text;
    EditText Answer3Text;
    EditText Answer4Text;
    EditText CommentText;
    View.OnClickListener QBackListener = new View.OnClickListener() {
        public void onClick(View v) {
            QuizSettingsActivity.this.finish();
        }
    };
    EditText Question;
    ArrayAdapter adapter;
    AlertDialog.Builder adb;
    String correctAnswer;
    View.OnClickListener mButtonListener = new View.OnClickListener() {
        public void onClick(View v) {
            QuizSettingsActivity.this.putQuestionTogether();
        }
    };
    SharedPreferences mGameSettings;
    DialogInterface.OnClickListener myDialogOnClickListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface v, int x) {
            QuizSettingsActivity.this.sendEmail();
        }
    };
    Spinner spinner;

    public QuizSettingsActivity() {
        String[] strArr = new String[4];
        strArr[DATE_DIALOG_ID] = "Gino Cappelletti";
        strArr[PASSWORD_DIALOG_ID] = "Tom Brady";
        strArr[2] = "Bill Belichick";
        strArr[3] = "John Hannah";
        this.AllAnswers = strArr;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.settings);
        setTitle(getResources().getString(R.string.app_name));
        init();
        initCorrectAnswerSpinner();
        initALLAnswerEntry();
    }

    private void init() {
        this.Question = (EditText) findViewById(R.id.EditText_Question);
        this.Answer1Text = (EditText) findViewById(R.id.EditText_Answer1);
        this.Answer2Text = (EditText) findViewById(R.id.EditText_Answer2);
        this.Answer3Text = (EditText) findViewById(R.id.EditText_Answer3);
        this.Answer4Text = (EditText) findViewById(R.id.EditText_Answer4);
        this.CommentText = (EditText) findViewById(R.id.EditText_Comment);
        this.Answer1Text.setText(this.AllAnswers[DATE_DIALOG_ID]);
        this.Answer2Text.setText(this.AllAnswers[PASSWORD_DIALOG_ID]);
        this.Answer3Text.setText(this.AllAnswers[2]);
        this.Answer4Text.setText(this.AllAnswers[3]);
        Button SendEmailButton = (Button) findViewById(R.id.SendEmailButton);
        SendEmailButton.setOnClickListener(this.mButtonListener);
        SendEmailButton.setText("Send the Question in");
        ((Button) findViewById(R.id.SendEmailBack)).setOnClickListener(this.QBackListener);
    }

    /* access modifiers changed from: protected */
    public void sendEmail() {
        String[] mailto = new String[PASSWORD_DIALOG_ID];
        mailto[DATE_DIALOG_ID] = "dr.roger.webster@gmail.com";
        Intent sendIntent = new Intent("android.intent.action.SEND");
        sendIntent.putExtra("android.intent.extra.EMAIL", mailto);
        String s = getResources().getString(R.string.app_name);
        sendIntent.putExtra("android.intent.extra.SUBJECT", "Patriots Trivia Game App Create Your Own Question");
        sendIntent.putExtra("android.intent.extra.TEXT", "<item> \n<question>" + this.Question.getText().toString() + "</question> " + "\n" + "<answer1>" + this.Answer1Text.getText().toString() + "</answer1>" + "\n" + "<answer2>" + this.Answer2Text.getText().toString() + "</answer2>" + "\n" + "<answer3>" + this.Answer3Text.getText().toString() + "</answer3>" + "\n" + "<answer4>" + this.Answer4Text.getText().toString() + "</answer4>" + "\n" + "<correctanswer>" + this.correctAnswer + "</correctanswer>" + "\n" + "<comment>" + "Sent in by: name. " + this.CommentText.getText().toString() + "</comment>" + "</item> \n" + "\n" + s);
        sendIntent.setType("message/rfc822");
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, "Please pick your preferred email application"));
    }

    private void initALLAnswerEntry() {
        this.Answer1Text.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                QuizSettingsActivity.this.AllAnswers[QuizSettingsActivity.DATE_DIALOG_ID] = QuizSettingsActivity.this.Answer1Text.getText().toString();
                QuizSettingsActivity.this.adapter.notifyDataSetChanged();
            }
        });
        this.Answer2Text.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                QuizSettingsActivity.this.AllAnswers[QuizSettingsActivity.PASSWORD_DIALOG_ID] = QuizSettingsActivity.this.Answer2Text.getText().toString();
                QuizSettingsActivity.this.adapter.notifyDataSetChanged();
            }
        });
        this.Answer3Text.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                QuizSettingsActivity.this.AllAnswers[2] = QuizSettingsActivity.this.Answer3Text.getText().toString();
                QuizSettingsActivity.this.adapter.notifyDataSetChanged();
            }
        });
        this.Answer4Text.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                QuizSettingsActivity.this.AllAnswers[3] = QuizSettingsActivity.this.Answer4Text.getText().toString();
                QuizSettingsActivity.this.adapter.notifyDataSetChanged();
            }
        });
    }

    private void initCorrectAnswerSpinner() {
        this.spinner = (Spinner) findViewById(R.id.Spinner_CorrectAnswer);
        this.adapter = new ArrayAdapter(this, 17367048, this.AllAnswers);
        this.spinner.setAdapter((SpinnerAdapter) this.adapter);
        this.adapter.setDropDownViewResource(17367049);
        this.spinner.setSelection(DATE_DIALOG_ID);
        this.correctAnswer = this.AllAnswers[DATE_DIALOG_ID];
        this.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View itemSelected, int selectedItemPosition, long selectedId) {
                QuizSettingsActivity.this.correctAnswer = (String) parent.getItemAtPosition(selectedItemPosition);
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    public void createDialogQuestion() {
        LinearLayout linear = new LinearLayout(this);
        linear.setOrientation(PASSWORD_DIALOG_ID);
        this.adb = new AlertDialog.Builder(this);
        this.adb.setTitle("My Trivia Q");
        this.adb.setView(linear);
        View VolumeView = ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.question, linear);
        VolumeView.setBackgroundColor(getResources().getColor(R.color.Blue));
        ((TextView) VolumeView.findViewById(R.id.TextViewQ)).setText(this.Question.getText().toString());
        this.correctAnswer = (String) this.spinner.getSelectedItem();
        RadioButton rb1 = (RadioButton) VolumeView.findViewById(R.id.RadioButton01);
        rb1.setText(this.AllAnswers[DATE_DIALOG_ID]);
        if (this.correctAnswer.equals(this.AllAnswers[DATE_DIALOG_ID])) {
            rb1.setChecked(true);
        }
        RadioButton rb2 = (RadioButton) VolumeView.findViewById(R.id.RadioButton02);
        rb2.setText(this.AllAnswers[PASSWORD_DIALOG_ID]);
        if (this.correctAnswer.equals(this.AllAnswers[PASSWORD_DIALOG_ID])) {
            rb2.setChecked(true);
        }
        RadioButton rb3 = (RadioButton) VolumeView.findViewById(R.id.RadioButton03);
        rb3.setText(this.AllAnswers[2]);
        if (this.correctAnswer.equals(this.AllAnswers[2])) {
            rb3.setChecked(true);
        }
        RadioButton rb4 = (RadioButton) VolumeView.findViewById(R.id.RadioButton04);
        rb4.setText(this.AllAnswers[3]);
        if (this.correctAnswer.equals(this.AllAnswers[3])) {
            rb4.setChecked(true);
        }
        this.adb.setPositiveButton("Back", (DialogInterface.OnClickListener) null);
        this.adb.setNegativeButton("Send Q", this.myDialogOnClickListener);
        this.adb.show();
    }

    /* access modifiers changed from: private */
    public void putQuestionTogether() {
        createDialogQuestion();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }
}
