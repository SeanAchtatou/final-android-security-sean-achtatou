package DrWebsterApps.New.England.Patriots.Trivia;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class QuizMenuActivity extends QuizActivity {
    String teamname = "";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.menu);
        setTitle(getResources().getString(R.string.app_name));
        this.teamname = getResources().getString(R.string.teamname);
        ListView menuList = (ListView) findViewById(R.id.ListView_Menu);
        menuList.setAdapter((ListAdapter) new ArrayAdapter<>(this, (int) R.layout.menu_item, new String[]{getResources().getString(R.string.menu_item_play), getResources().getString(R.string.menu_item_settings), getResources().getString(R.string.menu_item_schedule), getResources().getString(R.string.menu_item_morepps), getResources().getString(R.string.menu_item_help)}));
        menuList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View itemClicked, int position, long id) {
                String strText = ((TextView) itemClicked).getText().toString();
                if (strText.equalsIgnoreCase(QuizMenuActivity.this.getResources().getString(R.string.menu_item_play))) {
                    QuizMenuActivity.this.startActivity(new Intent(QuizMenuActivity.this, QuizGameActivity.class));
                } else if (strText.equalsIgnoreCase(QuizMenuActivity.this.getResources().getString(R.string.menu_item_help))) {
                    QuizMenuActivity.this.createDialogWebAccess();
                } else if (strText.equalsIgnoreCase(QuizMenuActivity.this.getResources().getString(R.string.menu_item_settings))) {
                    QuizMenuActivity.this.startActivity(new Intent(QuizMenuActivity.this, QuizSettingsActivity.class));
                } else if (strText.equalsIgnoreCase(QuizMenuActivity.this.getResources().getString(R.string.menu_item_schedule))) {
                    QuizMenuActivity.this.createDialogSchedule();
                } else if (strText.equalsIgnoreCase(QuizMenuActivity.this.getResources().getString(R.string.menu_item_morepps))) {
                    QuizMenuActivity.this.createDialogMoreApps();
                }
            }
        });
    }

    public void createDialogMoreApps() {
        LinearLayout linear = new LinearLayout(this);
        linear.setOrientation(1);
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle("DrWebsterApps");
        WebView dialogwv = new WebView(this);
        dialogwv.loadData("<a href='" + "market://search?q=websterapp" + "'>" + "Apps by DrWebsterApps" + "</a>", "text/html", "utf-8");
        linear.addView(dialogwv);
        EditText mytext = new EditText(this);
        mytext.append("Click above to see some of my other fun Apps.\n");
        mytext.append("Thanks Dr.WebsterApps.\n GO " + this.teamname + "!!!");
        linear.addView(mytext);
        adb.setView(linear);
        adb.setPositiveButton("Ok", (DialogInterface.OnClickListener) null);
        adb.show();
    }

    public void createDialogWebAccess() {
        LinearLayout linear = new LinearLayout(this);
        linear.setOrientation(1);
        linear.setBackgroundColor(getResources().getColor(R.color.Blue));
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle("New England Patriots Trivia App");
        TextView mytext = new TextView(this);
        mytext.append("This app tests the users knowledge of the New England Patriots.");
        mytext.append(" You can also send in your own Pats Trivia question and we will put it in the database.");
        mytext.append(" We will be adding more trivia questions each week, with the help of everyone out there.");
        mytext.append(" Make sure you keep this app updated so you get all the newly added questions.");
        mytext.append(" This App developed by DrWebsterApps with help from Hannah Webster (Advertising).");
        mytext.append(" This app has no official affiliation with the NE Patriots of the NFL. Have fun and enjoy!!!\n");
        mytext.append("Cheers, Dr. Roger Webster\n");
        SharedPreferences settings = getSharedPreferences(QuizActivity.GAME_PREFERENCES, 0);
        SharedPreferences.Editor edit = settings.edit();
        if (settings.contains("totalQs")) {
            int TotalNumberofQs = settings.getInt("totalQs", 0);
            mytext.append(String.valueOf(settings.getInt("Qno", 0)) + ",");
            mytext.append(String.valueOf(TotalNumberofQs) + " ");
        }
        linear.addView(mytext);
        adb.setView(linear);
        adb.setPositiveButton("Ok", (DialogInterface.OnClickListener) null);
        adb.show();
    }

    public void createDialogSchedule() {
        LinearLayout linear = new LinearLayout(this);
        linear.setOrientation(1);
        linear.setBackgroundColor(getResources().getColor(R.color.Green));
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle("Boston Celtics Trivia App");
        TextView mytext = new TextView(this);
        mytext.append("\nClick below to Download my Boston Celtics Trivia App.");
        linear.addView(mytext);
        WebView dialogwv = new WebView(this);
        dialogwv.loadData("<a href='" + "market://details?id=DrWebsterApps.CelticsTriviaGame" + "'>" + "Download Boston Celtics Trivia App" + "</a>", "text/html", "utf-8");
        linear.addView(dialogwv);
        adb.setView(linear);
        adb.setPositiveButton("Ok", (DialogInterface.OnClickListener) null);
        adb.show();
    }
}
