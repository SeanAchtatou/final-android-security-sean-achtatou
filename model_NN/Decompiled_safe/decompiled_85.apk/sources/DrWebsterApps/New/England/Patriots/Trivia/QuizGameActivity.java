package DrWebsterApps.New.England.Patriots.Trivia;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import java.io.IOException;
import java.util.Random;
import org.xmlpull.v1.XmlPullParserException;

public class QuizGameActivity extends QuizActivity {
    private static final String TEST_EMULATOR = " ";
    String CorrectAnswer;
    LinearLayout Linerlayout2;
    Button NextQuestionButton;
    View.OnClickListener QBackListener = new View.OnClickListener() {
        public void onClick(View v) {
            QuizGameActivity.this.finish();
        }
    };
    Button QbackButton;
    TextView QuestionText;
    int TotalNumberofQs = 0;
    int TotalnumCorrect = 0;
    int TotalnumInCorrect = 0;
    AdView adView;
    AlertDialog.Builder adb;
    String comment;
    Context context;
    int correctpos = 0;
    protected int mPos;
    protected String mSelection;
    View.OnClickListener myButtonOnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            if (QuizGameActivity.this.rb1.isChecked()) {
                QuizGameActivity.this.createDialogAnswer((String) QuizGameActivity.this.rb1.getText(), QuizGameActivity.this.CorrectAnswer);
            }
            if (QuizGameActivity.this.rb2.isChecked()) {
                QuizGameActivity.this.createDialogAnswer((String) QuizGameActivity.this.rb2.getText(), QuizGameActivity.this.CorrectAnswer);
            }
            if (QuizGameActivity.this.rb3.isChecked()) {
                QuizGameActivity.this.createDialogAnswer((String) QuizGameActivity.this.rb3.getText(), QuizGameActivity.this.CorrectAnswer);
            }
            if (QuizGameActivity.this.rb4.isChecked()) {
                QuizGameActivity.this.createDialogAnswer((String) QuizGameActivity.this.rb4.getText(), QuizGameActivity.this.CorrectAnswer);
            }
        }
    };
    DialogInterface.OnClickListener myDialogOnClickListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface v, int x) {
            QuizGameActivity.this.createNextQuestion();
        }
    };
    DialogInterface.OnClickListener myDialogOnClickListenerRandom = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface v, int x) {
            QuizGameActivity.this.myQsindex = QuizGameActivity.getRandomInt(0, QuizGameActivity.this.TotalNumberofQs);
            if (QuizGameActivity.this.myQsindex == QuizGameActivity.this.TotalNumberofQs) {
                QuizGameActivity.this.myQsindex--;
            }
            QuizGameActivity.this.createNextQuestion();
        }
    };
    int myLastQsindex = 0;
    View.OnClickListener myOptionOnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
        }
    };
    myQuestion[] myQs = new myQuestion[300];
    int myQsindex = 0;
    SharedPreferences.Editor prefEditor;
    RadioButton rb1;
    RadioButton rb2;
    RadioButton rb3;
    RadioButton rb4;
    SharedPreferences settings;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.game);
        setTitle(getResources().getString(R.string.app_name));
        getAppPreferences();
        InitButtons();
        String format = String.format("%02d", Integer.valueOf(this.TotalnumCorrect));
        String format2 = String.format("%02d", Integer.valueOf(this.TotalnumInCorrect));
    }

    public void getAppPreferences() {
        this.settings = getSharedPreferences(QuizActivity.GAME_PREFERENCES, 0);
        this.prefEditor = this.settings.edit();
        this.prefEditor.putString("UserName", "Dr. Webster");
        if (this.settings.contains("Qno")) {
            this.myLastQsindex = this.settings.getInt("Qno", this.myLastQsindex);
            return;
        }
        this.prefEditor.putInt("Qno", 0);
        this.prefEditor.commit();
    }

    public void InitButtons() {
        this.Linerlayout2 = (LinearLayout) findViewById(R.id.linearLayout1);
        this.Linerlayout2.setBackgroundColor(getResources().getColor(R.color.Blue));
        this.NextQuestionButton = (Button) findViewById(R.id.button1);
        this.NextQuestionButton.setOnClickListener(this.myButtonOnClickListener);
        this.NextQuestionButton.setText("Check your Answer");
        this.QbackButton = (Button) findViewById(R.id.GameBackButton);
        this.QbackButton.setOnClickListener(this.QBackListener);
        this.QuestionText = (TextView) findViewById(R.id.TextViewQ2);
        this.rb1 = (RadioButton) findViewById(R.id.RadioButton01);
        this.rb2 = (RadioButton) findViewById(R.id.RadioButton02);
        this.rb3 = (RadioButton) findViewById(R.id.RadioButton03);
        this.rb4 = (RadioButton) findViewById(R.id.RadioButton04);
        try {
            getQuestionsfromXML();
        } catch (Exception e) {
        }
        this.prefEditor.putInt("totalQs", this.TotalNumberofQs);
        this.myQsindex = this.myLastQsindex;
        createNextQuestion();
        this.adView = (AdView) findViewById(R.id.adView);
        this.adView.loadAd(new AdRequest());
    }

    public static int getRandomInt(int min, int max) {
        return new Random(System.currentTimeMillis()).nextInt((max - min) + 1) + min;
    }

    public void createNextQuestion() {
        this.QuestionText.setText(this.myQs[this.myQsindex].Question);
        this.rb1.setText(this.myQs[this.myQsindex].Answer1);
        this.rb2.setText(this.myQs[this.myQsindex].Answer2);
        this.rb3.setText(this.myQs[this.myQsindex].Answer3);
        this.rb4.setText(this.myQs[this.myQsindex].Answer4);
        ((RadioGroup) findViewById(R.id.RadioGroup1)).clearCheck();
        this.CorrectAnswer = this.myQs[this.myQsindex].correctAnswer;
        this.comment = this.myQs[this.myQsindex].comment;
        if (this.myQsindex < this.TotalNumberofQs - 1) {
            this.myQsindex++;
        } else {
            this.myQsindex = 0;
        }
        this.myLastQsindex = this.myQsindex;
        this.prefEditor.putInt("Qno", this.myLastQsindex);
        this.prefEditor.commit();
    }

    private void VibrateALittle() {
        long[] pattern = new long[6];
        pattern[1] = 200;
        pattern[2] = 75;
        pattern[3] = 100;
        pattern[4] = 75;
        pattern[5] = 100;
        ((Vibrator) getSystemService("vibrator")).vibrate(pattern, -1);
    }

    public void createDialogAnswer(String Guess, String answer) {
        LinearLayout linear = new LinearLayout(this);
        linear.setBackgroundColor(getResources().getColor(R.color.Blue));
        linear.setOrientation(1);
        this.adb = new AlertDialog.Builder(this);
        this.adb.setTitle("Patriots Trivia App");
        this.adb.setView(linear);
        TextView mytext = (TextView) ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.scores, linear).findViewById(R.id.QPopUp);
        mytext.setTextSize(getResources().getDimension(R.dimen.answer_size));
        if (Guess == answer) {
            mytext.append(String.valueOf(Guess) + " is Correct!\n");
            mytext.append("\nYou get another GINO Point!!!!\n");
            this.TotalnumCorrect++;
        } else {
            VibrateALittle();
            mytext.append(String.valueOf(Guess) + " is NOT correct.\n");
            mytext.append(String.valueOf(answer) + " is the answer\n");
            this.TotalnumInCorrect++;
        }
        if (this.comment.length() > 1) {
            mytext.append("\n" + this.comment);
        }
        mytext.append("\nQuestions Correct: " + this.TotalnumCorrect);
        mytext.append("\nQuestions Wrong  : " + this.TotalnumInCorrect);
        this.adb.setNegativeButton("Next Question", this.myDialogOnClickListener);
        this.adb.setPositiveButton("Random Question", this.myDialogOnClickListenerRandom);
        this.adb.show();
    }

    public void setNextQuestion(String tagname, String textin) {
        if (tagname.equals("question")) {
            this.myQs[this.myQsindex] = new myQuestion();
            this.myQs[this.myQsindex].Question = textin;
        }
        if (tagname.equals("answer1")) {
            this.myQs[this.myQsindex].Answer1 = textin;
        }
        if (tagname.equals("answer2")) {
            this.myQs[this.myQsindex].Answer2 = textin;
        }
        if (tagname.equals("answer3")) {
            this.myQs[this.myQsindex].Answer3 = textin;
        }
        if (tagname.equals("answer4")) {
            this.myQs[this.myQsindex].Answer4 = textin;
        }
        if (tagname.equals("correctanswer")) {
            this.myQs[this.myQsindex].correctAnswer = textin;
        }
        if (tagname.equals("comment")) {
            this.myQs[this.myQsindex].comment = textin;
            this.TotalNumberofQs++;
            this.myQsindex++;
        }
    }

    public void getQuestionsfromXML() throws XmlPullParserException, IOException {
        String tagname = TEST_EMULATOR;
        XmlResourceParser xpp = getResources().getXml(R.xml.celticstrivia);
        if (xpp != null) {
            xpp.next();
            for (int eventType = xpp.getEventType(); eventType != 1; eventType = xpp.next()) {
                if (eventType == 2) {
                    tagname = xpp.getName();
                } else if (eventType == 4) {
                    setNextQuestion(tagname, xpp.getText());
                }
            }
        }
    }

    public class myQuestion {
        public String Answer1;
        public String Answer2;
        public String Answer3;
        public String Answer4;
        public String Question;
        public String comment;
        public String correctAnswer;

        public myQuestion() {
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.gameoptions, menu);
        menu.findItem(R.id.help_menu_item).setIntent(new Intent(this, QuizHelpActivity.class));
        menu.findItem(R.id.settings_menu_item).setIntent(new Intent(this, QuizSettingsActivity.class));
        return true;
    }
}
