package DrWebsterApps.New.England.Patriots.Trivia;

import android.os.Bundle;

public class QuizScoresActivity extends QuizActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.scores);
        setTitle(getResources().getString(R.string.app_name));
    }
}
