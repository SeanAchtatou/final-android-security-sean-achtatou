package DrWebsterApps.New.England.Patriots.Trivia;

import android.os.Bundle;

public class QuizHelpActivity extends QuizActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.help);
        setTitle(getResources().getString(R.string.app_name));
    }
}
