package au.com.bytecode.opencsv;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

public interface ResultSetHelper {
    String[] getColumnNames(ResultSet resultSet) throws SQLException;

    String[] getColumnValues(ResultSet resultSet) throws SQLException, IOException;
}
