package au.com.bytecode.opencsv.bean;

import au.com.bytecode.opencsv.CSVReader;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.IOException;

public interface MappingStrategy<T> {
    void captureHeader(CSVReader cSVReader) throws IOException;

    T createBean() throws InstantiationException, IllegalAccessException;

    PropertyDescriptor findDescriptor(int i) throws IntrospectionException;
}
