package au.com.bytecode.opencsv;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public class CSVReader implements Closeable {
    public static final int DEFAULT_SKIP_LINES = 0;
    private BufferedReader br;
    private boolean hasNext;
    private boolean linesSkiped;
    private CSVParser parser;
    private int skipLines;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: au.com.bytecode.opencsv.CSVReader.<init>(java.io.Reader, char, char, char):void
     arg types: [java.io.Reader, int, int, ?]
     candidates:
      au.com.bytecode.opencsv.CSVReader.<init>(java.io.Reader, char, char, int):void
      au.com.bytecode.opencsv.CSVReader.<init>(java.io.Reader, char, char, boolean):void
      au.com.bytecode.opencsv.CSVReader.<init>(java.io.Reader, char, char, char):void */
    public CSVReader(Reader reader) {
        this(reader, ',', '\"', (char) CSVParser.DEFAULT_ESCAPE_CHARACTER);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: au.com.bytecode.opencsv.CSVReader.<init>(java.io.Reader, char, char, char):void
     arg types: [java.io.Reader, char, int, ?]
     candidates:
      au.com.bytecode.opencsv.CSVReader.<init>(java.io.Reader, char, char, int):void
      au.com.bytecode.opencsv.CSVReader.<init>(java.io.Reader, char, char, boolean):void
      au.com.bytecode.opencsv.CSVReader.<init>(java.io.Reader, char, char, char):void */
    public CSVReader(Reader reader, char c) {
        this(reader, c, '\"', (char) CSVParser.DEFAULT_ESCAPE_CHARACTER);
    }

    public CSVReader(Reader reader, char c, char c2) {
        this(reader, c, c2, CSVParser.DEFAULT_ESCAPE_CHARACTER, 0, false);
    }

    public CSVReader(Reader reader, char c, char c2, char c3) {
        this(reader, c, c2, c3, 0, false);
    }

    public CSVReader(Reader reader, char c, char c2, char c3, int i) {
        this(reader, c, c2, c3, i, false);
    }

    public CSVReader(Reader reader, char c, char c2, char c3, int i, boolean z) {
        this(reader, c, c2, c3, i, z, true);
    }

    public CSVReader(Reader reader, char c, char c2, char c3, int i, boolean z, boolean z2) {
        this.hasNext = true;
        this.br = new BufferedReader(reader);
        this.parser = new CSVParser(c, c2, c3, z, z2);
        this.skipLines = i;
    }

    public CSVReader(Reader reader, char c, char c2, int i) {
        this(reader, c, c2, CSVParser.DEFAULT_ESCAPE_CHARACTER, i, false);
    }

    public CSVReader(Reader reader, char c, char c2, boolean z) {
        this(reader, c, c2, CSVParser.DEFAULT_ESCAPE_CHARACTER, 0, z);
    }

    private String getNextLine() throws IOException {
        if (!this.linesSkiped) {
            for (int i = 0; i < this.skipLines; i++) {
                this.br.readLine();
            }
            this.linesSkiped = true;
        }
        String readLine = this.br.readLine();
        if (readLine == null) {
            this.hasNext = false;
        }
        if (this.hasNext) {
            return readLine;
        }
        return null;
    }

    public void close() throws IOException {
        this.br.close();
    }

    public List<String[]> readAll() throws IOException {
        ArrayList arrayList = new ArrayList();
        while (this.hasNext) {
            String[] readNext = readNext();
            if (readNext != null) {
                arrayList.add(readNext);
            }
        }
        return arrayList;
    }

    public String[] readNext() throws IOException {
        String[] strArr = null;
        do {
            String nextLine = getNextLine();
            if (!this.hasNext) {
                break;
            }
            String[] parseLineMulti = this.parser.parseLineMulti(nextLine);
            if (parseLineMulti.length > 0) {
                if (strArr == null) {
                    strArr = parseLineMulti;
                } else {
                    String[] strArr2 = new String[(strArr.length + parseLineMulti.length)];
                    System.arraycopy(strArr, 0, strArr2, 0, strArr.length);
                    System.arraycopy(parseLineMulti, 0, strArr2, strArr.length, parseLineMulti.length);
                    strArr = strArr2;
                }
            }
        } while (this.parser.isPending());
        return strArr;
    }
}
