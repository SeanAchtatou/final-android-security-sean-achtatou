package au.com.bytecode.opencsv.bean;

import java.util.HashMap;
import java.util.Map;

public class HeaderColumnNameTranslateMappingStrategy<T> extends HeaderColumnNameMappingStrategy<T> {
    private Map<String, String> columnMapping = new HashMap();

    public Map<String, String> getColumnMapping() {
        return this.columnMapping;
    }

    /* access modifiers changed from: protected */
    public String getColumnName(int i) {
        return getColumnMapping().get(this.header[i]);
    }

    public void setColumnMapping(Map<String, String> map) {
        this.columnMapping = map;
    }
}
