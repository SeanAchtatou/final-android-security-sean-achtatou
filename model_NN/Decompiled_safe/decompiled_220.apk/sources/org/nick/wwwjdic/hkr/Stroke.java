package org.nick.wwwjdic.hkr;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import java.util.ArrayList;
import java.util.List;

public class Stroke {
    private static final int ANNOTATION_OFFSET = 15;
    private static final float TOUCH_TOLERANCE = 4.0f;
    private PointF lastPoint;
    private Path path = new Path();
    private List<PointF> points = new ArrayList();

    public void addPoint(PointF p) {
        if (this.points.isEmpty()) {
            this.path.reset();
            this.path.moveTo(p.x, p.y);
            this.lastPoint = new PointF(p.x, p.y);
        } else {
            float dx = Math.abs(p.x - this.lastPoint.x);
            float dy = Math.abs(p.y - this.lastPoint.y);
            if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
                this.path.quadTo(this.lastPoint.x, this.lastPoint.y, (p.x + this.lastPoint.x) / 2.0f, (p.y + this.lastPoint.y) / 2.0f);
                this.lastPoint = new PointF(p.x, p.y);
            }
        }
        this.points.add(p);
    }

    public void clear() {
        this.points.clear();
        this.path.reset();
    }

    public void draw(Canvas canvas, Paint paint) {
        this.path.lineTo(this.lastPoint.x, this.lastPoint.y);
        canvas.drawPath(this.path, paint);
    }

    /* Debug info: failed to restart local var, previous not found, register: 20 */
    public void annotate(Canvas canvas, Paint paint, int strokeNum) {
        if (!this.points.isEmpty() && this.points.size() != 1) {
            float xOffset = 15.0f;
            float yOffset = 15.0f;
            int annotationGap = 1;
            PointF firstPoint = this.points.get(0);
            if (this.points.size() > 5) {
                annotationGap = 5;
            }
            if (annotationGap <= this.points.size()) {
                canvas.drawText(Integer.toString(strokeNum), 15.0f + this.points.get(0).x, 15.0f + this.points.get(0).y, paint);
                return;
            }
            float dx = this.points.get(annotationGap).x - firstPoint.x;
            float dy = this.points.get(annotationGap).y - firstPoint.y;
            double length = Math.sqrt((double) ((dx * dx) + (dy * dy)));
            if (length > 0.0d) {
                xOffset = -15.0f * ((float) (((double) dx) / length));
                yOffset = -15.0f * ((float) (((double) dy) / length));
            }
            int height = canvas.getClipBounds().height();
            int width = canvas.getClipBounds().width();
            float x = firstPoint.x + xOffset;
            float y = firstPoint.y + yOffset;
            if (x < 0.0f) {
                x = 15.0f;
            }
            if (y < 0.0f) {
                y = 15.0f;
            }
            if (x > ((float) width)) {
                x = (float) (width - ANNOTATION_OFFSET);
            }
            if (y > ((float) height)) {
                y = (float) (height - ANNOTATION_OFFSET);
            }
            canvas.drawText(Integer.toString(strokeNum), x, y, paint);
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 20 */
    public void annotateMidway(Canvas canvas, Paint paint, int strokeNum) {
        if (!this.points.isEmpty() && this.points.size() != 1) {
            float xOffset = 15.0f;
            float yOffset = 15.0f;
            int midwayPointIdx = (this.points.size() / 2) - 1;
            if (midwayPointIdx + 2 <= this.points.size()) {
                canvas.drawText(Integer.toString(strokeNum), 15.0f + this.points.get(0).x, 15.0f + this.points.get(0).y, paint);
                return;
            }
            float x = this.points.get(midwayPointIdx).x;
            float y = this.points.get(midwayPointIdx).y;
            float dx = this.points.get(midwayPointIdx + 2).x - x;
            float dy = this.points.get(midwayPointIdx + 2).y - y;
            float length = (float) Math.sqrt((double) ((dx * dx) + (dy * dy)));
            if (((double) length) >= 1.0d) {
                xOffset = ((-dx) * 15.0f) / length;
                yOffset = (float) (((double) ((15.0f * dy) / length)) - 7.5d);
            }
            canvas.drawText(Integer.toString(strokeNum), x + xOffset, y + yOffset, paint);
        }
    }

    public List<PointF> getPoints() {
        return this.points;
    }

    public String toBase36Points() {
        StringBuffer buff = new StringBuffer();
        for (int i = 0; i < this.points.size(); i++) {
            buff.append(String.valueOf(String.valueOf("") + toBase36((int) this.points.get(i).x)) + toBase36((int) this.points.get(i).y));
        }
        return buff.toString();
    }

    public String toPoints() {
        StringBuffer buff = new StringBuffer();
        for (int i = 0; i < this.points.size(); i++) {
            String pointStr = String.valueOf("") + " " + ((int) this.points.get(i).x);
            buff.append(String.valueOf(pointStr) + " " + ((int) this.points.get(i).y));
        }
        return buff.toString();
    }

    private String toBase36(int i) {
        String result = Integer.toString(i, 36);
        if (result.length() == 1) {
            return "0" + result;
        }
        return result;
    }
}
