package org.nick.wwwjdic.hkr;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;
import java.util.Arrays;
import java.util.List;
import org.nick.kanjirecognizer.hkr.CharacterRecognizer;
import org.nick.wwwjdic.Constants;
import org.nick.wwwjdic.R;
import org.nick.wwwjdic.WebServiceBackedActivity;
import org.nick.wwwjdic.WwwjdicPreferences;
import org.nick.wwwjdic.ocr.WeOcrClient;
import org.nick.wwwjdic.utils.Analytics;
import org.nick.wwwjdic.utils.Dialogs;

public class RecognizeKanjiActivity extends WebServiceBackedActivity implements View.OnClickListener {
    private static final int HKR_RESULT = 1;
    private static final int HKR_RESULT_TYPE_KR = 2;
    private static final int HKR_RESULT_TYPE_OCR = 1;
    private static final int HKR_RESULT_TYPE_WS = 0;
    private static final String KR_USAGE_TIP_DIALOG = "kr_usage";
    private static final int NUM_KR_CANDIDATES = 10;
    private static final int NUM_OCR_CANDIDATES = 20;
    private static final int OCR_IMAGE_WIDTH = 128;
    /* access modifiers changed from: private */
    public static final String TAG = RecognizeKanjiActivity.class.getSimpleName();
    /* access modifiers changed from: private */
    public boolean bound;
    private Button clearButton;
    private ServiceConnection connection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            RecognizeKanjiActivity.this.recognizer = CharacterRecognizer.Stub.asInterface(service);
            RecognizeKanjiActivity.this.bound = true;
        }

        public void onServiceDisconnected(ComponentName className) {
            RecognizeKanjiActivity.this.recognizer = null;
            RecognizeKanjiActivity.this.bound = false;
        }
    };
    /* access modifiers changed from: private */
    public KanjiDrawView drawView;
    private CheckBox lookAheadCb;
    private Button ocrButton;
    private Button recognizeButton;
    /* access modifiers changed from: private */
    public CharacterRecognizer recognizer;
    private Button removeStrokeButton;

    /* access modifiers changed from: protected */
    public void activityOnCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.kanji_draw);
        setTitle((int) R.string.hkr);
        findViews();
        this.recognizeButton.setOnClickListener(this);
        this.ocrButton.setOnClickListener(this);
        this.removeStrokeButton.setOnClickListener(this);
        this.clearButton.setOnClickListener(this);
        this.drawView.setAnnotateStrokes(WwwjdicPreferences.isAnnoateStrokes(this));
        this.drawView.setAnnotateStrokesMidway(WwwjdicPreferences.isAnnotateStrokesMidway(this));
        this.drawView.requestFocus();
        Dialogs.showTipOnce(this, KR_USAGE_TIP_DIALOG, R.string.kr_usage_tip);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        Analytics.startSession(this);
        if (!WwwjdicPreferences.isUseKanjiRecognizer(this) || this.bound) {
            setTitle((int) R.string.online_hkr);
            return;
        }
        bindToKanjiRecognizer();
        setTitle((int) R.string.offline_hkr);
    }

    /* access modifiers changed from: package-private */
    public void bindToKanjiRecognizer() {
        if (bindService(new Intent("org.nick.kanjirecognizer.hkr.RECOGNIZE_KANJI"), this.connection, 1)) {
            Log.d(TAG, "successfully bound to KR service");
            this.lookAheadCb.setEnabled(false);
            return;
        }
        Log.d(TAG, "could not bind to KR service");
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        Analytics.endSession(this);
        if (this.bound) {
            this.bound = false;
            unbindService(this.connection);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.drawView.setAnnotateStrokes(WwwjdicPreferences.isAnnoateStrokes(this));
        this.drawView.setAnnotateStrokesMidway(WwwjdicPreferences.isAnnotateStrokesMidway(this));
    }

    private void findViews() {
        this.drawView = (KanjiDrawView) findViewById(R.id.kanji_draw_view);
        this.recognizeButton = (Button) findViewById(R.id.recognize_button);
        this.ocrButton = (Button) findViewById(R.id.ocr_button);
        this.removeStrokeButton = (Button) findViewById(R.id.remove_stroke_button);
        this.clearButton = (Button) findViewById(R.id.clear_canvas_button);
        this.lookAheadCb = (CheckBox) findViewById(R.id.lookAheadCb);
    }

    public static class RecognizeKanjiHandler extends WebServiceBackedActivity.WsResultHandler {
        public RecognizeKanjiHandler(RecognizeKanjiActivity krActivity) {
            super(krActivity);
        }

        public void handleMessage(Message msg) {
            if (this.activity == null) {
                Message newMsg = obtainMessage(msg.what, msg.arg1, msg.arg2);
                newMsg.obj = msg.obj;
                sendMessageDelayed(newMsg, 500);
                return;
            }
            RecognizeKanjiActivity krActivity = (RecognizeKanjiActivity) this.activity;
            switch (msg.what) {
                case 1:
                    krActivity.dismissProgressDialog();
                    if (msg.arg1 == 1) {
                        krActivity.sendToDictionary((String[]) msg.obj);
                        return;
                    } else if (msg.arg2 != 0) {
                        Toast.makeText(krActivity, (int) R.string.hkr_failed, 0).show();
                        return;
                    } else if (WwwjdicPreferences.isKrInstalled(krActivity, krActivity.getApplication())) {
                        showEnableKrDialog();
                        return;
                    } else {
                        showInstallKrDialog();
                        return;
                    }
                default:
                    super.handleMessage(msg);
                    return;
            }
        }

        private void showEnableKrDialog() {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.activity);
            builder.setMessage((int) R.string.wskr_unavailable_enable_kr).setCancelable(false).setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    WwwjdicPreferences.setUseKanjiRecognizer(true, RecognizeKanjiHandler.this.activity);
                    RecognizeKanjiActivity krActivity = (RecognizeKanjiActivity) RecognizeKanjiHandler.this.activity;
                    krActivity.bindToKanjiRecognizer();
                    krActivity.setTitle((int) R.string.offline_hkr);
                }
            }).setNegativeButton((int) R.string.no, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            AlertDialog dialog = builder.create();
            if (!this.activity.isFinishing()) {
                dialog.show();
            }
        }

        private void showInstallKrDialog() {
            if (!this.activity.isFinishing()) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this.activity);
                builder.setMessage((int) R.string.wskr_unavailable_install_kr).setCancelable(false).setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        RecognizeKanjiHandler.this.activity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(RecognizeKanjiHandler.this.activity.getResources().getString(R.string.kr_download_uri))));
                    }
                }).setNegativeButton((int) R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                builder.create().show();
            }
        }
    }

    /* access modifiers changed from: protected */
    public WebServiceBackedActivity.WsResultHandler createHandler() {
        return new RecognizeKanjiHandler(this);
    }

    class HkrTask implements Runnable {
        private Handler handler;
        private List<Stroke> strokes;

        public HkrTask(List<Stroke> strokes2, Handler handler2) {
            this.strokes = strokes2;
            this.handler = handler2;
        }

        public void run() {
            try {
                String[] results = new KanjiRecognizerClient(WwwjdicPreferences.getKrUrl(RecognizeKanjiActivity.this), WwwjdicPreferences.getKrTimeout(RecognizeKanjiActivity.this)).recognize(this.strokes, RecognizeKanjiActivity.this.isUseLookahead());
                Log.i(RecognizeKanjiActivity.TAG, "go KR result " + Arrays.asList(results));
                Message msg = this.handler.obtainMessage(1, 1, 0);
                msg.obj = results;
                this.handler.sendMessage(msg);
            } catch (Exception e) {
                Log.e("TAG", "Character recognition failed", e);
                this.handler.sendMessage(this.handler.obtainMessage(1, 0, 0));
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean isUseLookahead() {
        return this.lookAheadCb.isChecked();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.recognize_button:
                recognizeKanji();
                return;
            case R.id.ocr_button:
                ocrKanji();
                return;
            case R.id.remove_stroke_button:
                this.drawView.removeLastStroke();
                return;
            case R.id.clear_canvas_button:
                clear();
                return;
            default:
                return;
        }
    }

    private void ocrKanji() {
        if (hasStrokes()) {
            submitWsTask(new OcrTask(drawingToBitmap(), this.handler), getResources().getString(R.string.doing_hkr));
            Analytics.event("recognizeKanjiOcr", this);
        }
    }

    class OcrTask implements Runnable {
        private Bitmap bitmap;
        private Handler handler;

        public OcrTask(Bitmap b, Handler h) {
            this.bitmap = b;
            this.handler = h;
        }

        public void run() {
            try {
                String[] candidates = new WeOcrClient(WwwjdicPreferences.getWeocrUrl(RecognizeKanjiActivity.this), WwwjdicPreferences.getWeocrTimeout(RecognizeKanjiActivity.this)).sendCharacterOcrRequest(this.bitmap, RecognizeKanjiActivity.NUM_OCR_CANDIDATES);
                if (candidates != null) {
                    Message msg = this.handler.obtainMessage(1, 1, 1);
                    msg.obj = candidates;
                    this.handler.sendMessage(msg);
                    return;
                }
                Log.d("TAG", "OCR failed: null returned");
                this.handler.sendMessage(this.handler.obtainMessage(1, 0, 1));
            } catch (Exception e) {
                Log.e("TAG", "OCR failed", e);
                this.handler.sendMessage(this.handler.obtainMessage(1, 0, 1));
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private Bitmap drawingToBitmap() {
        Bitmap b = Bitmap.createBitmap(this.drawView.getWidth(), this.drawView.getWidth(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        boolean annotate = this.drawView.isAnnotateStrokes();
        this.drawView.setAnnotateStrokes(false);
        this.drawView.setBackgroundColor(-7829368);
        this.drawView.setStrokePaintColor(-16777216);
        this.drawView.draw(c);
        this.drawView.setAnnotateStrokes(annotate);
        this.drawView.setBackgroundColor(-16777216);
        this.drawView.setStrokePaintColor(-1);
        int width = this.drawView.getWidth();
        float scale = ((float) 128) / ((float) width);
        Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);
        c.scale(scale, scale);
        return Bitmap.createBitmap(b, 0, 0, width, width, matrix, true);
    }

    private void clear() {
        this.drawView.clear();
    }

    private void recognizeKanji() {
        if (hasStrokes()) {
            List<Stroke> strokes = this.drawView.getStrokes();
            if (!WwwjdicPreferences.isUseKanjiRecognizer(this)) {
                recognizeWs(strokes);
            } else if (this.recognizer == null) {
                Toast.makeText(this, (int) R.string.kr_not_initialized, 0).show();
                recognizeWs(strokes);
            } else {
                reconizeKanjiRecognizer(strokes);
            }
        }
    }

    private boolean hasStrokes() {
        List<Stroke> strokes = this.drawView.getStrokes();
        return strokes != null && !strokes.isEmpty();
    }

    private void recognizeWs(List<Stroke> strokes) {
        Analytics.event("recognizeKanji", this);
        submitWsTask(new HkrTask(strokes, this.handler), getResources().getString(R.string.doing_hkr));
    }

    private void reconizeKanjiRecognizer(final List<Stroke> strokes) {
        Analytics.event("recognizeKanjiKr", this);
        submitWsTask(new Runnable() {
            public void run() {
                try {
                    RecognizeKanjiActivity.this.recognizer.startRecognition(RecognizeKanjiActivity.this.drawView.getWidth(), RecognizeKanjiActivity.this.drawView.getHeight());
                    int strokeNum = 0;
                    for (Stroke s : strokes) {
                        for (PointF p : s.getPoints()) {
                            RecognizeKanjiActivity.this.recognizer.addPoint(strokeNum, (int) p.x, (int) p.y);
                        }
                        strokeNum++;
                    }
                    String[] candidates = RecognizeKanjiActivity.this.recognizer.recognize(10);
                    if (candidates != null) {
                        Message msg = RecognizeKanjiActivity.this.handler.obtainMessage(1, 1, 2);
                        msg.obj = candidates;
                        RecognizeKanjiActivity.this.handler.sendMessage(msg);
                        return;
                    }
                    RecognizeKanjiActivity.this.handler.sendMessage(RecognizeKanjiActivity.this.handler.obtainMessage(1, 0, 2));
                } catch (Exception e) {
                    Log.d(RecognizeKanjiActivity.TAG, "error calling recognizer", e);
                    RecognizeKanjiActivity.this.handler.sendMessage(RecognizeKanjiActivity.this.handler.obtainMessage(1, 0, 2));
                }
            }
        }, getResources().getString(R.string.doing_hkr));
    }

    public void sendToDictionary(String[] results) {
        Intent intent = new Intent(this, HkrCandidates.class);
        Bundle extras = new Bundle();
        extras.putStringArray(Constants.HKR_CANDIDATES_KEY, results);
        intent.putExtras(extras);
        startActivity(intent);
    }
}
