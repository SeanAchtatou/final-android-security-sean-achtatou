package org.nick.wwwjdic.hkr;

import au.com.bytecode.opencsv.CSVWriter;
import java.io.IOException;
import java.util.List;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.nick.wwwjdic.EntityBasedHttpClient;

public class KanjiRecognizerClient extends EntityBasedHttpClient {
    private static final String TAG = KanjiRecognizerClient.class.getSimpleName();

    public KanjiRecognizerClient(String endpoint, int timeout) {
        super(endpoint, timeout);
    }

    /* access modifiers changed from: protected */
    public HttpParams createHttpParams(int timeout) {
        HttpParams params = super.createHttpParams(timeout);
        HttpProtocolParams.setUseExpectContinue(params, false);
        return params;
    }

    private String createRecognizerRequest(List<Stroke> strokes, boolean useLookAhead) {
        StringBuffer buff = new StringBuffer();
        if (useLookAhead) {
            buff.append("HL ");
        } else {
            buff.append("H ");
        }
        for (Stroke s : strokes) {
            buff.append(s.toBase36Points());
            buff.append(CSVWriter.DEFAULT_LINE_END);
        }
        buff.append(CSVWriter.DEFAULT_LINE_END);
        return buff.toString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x00a2  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:19:0x00a8=Splitter:B:19:0x00a8, B:10:0x007f=Splitter:B:10:0x007f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String[] recognize(java.util.List<org.nick.wwwjdic.hkr.Stroke> r15, boolean r16) throws java.io.IOException {
        /*
            r14 = this;
            java.lang.String r9 = org.nick.wwwjdic.hkr.KanjiRecognizerClient.TAG
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            java.lang.String r11 = "Sending handwritten kanji recognition request to "
            r10.<init>(r11)
            java.lang.String r11 = r14.url
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.String r10 = r10.toString()
            android.util.Log.i(r9, r10)
            java.lang.String r1 = r14.createRecognizerRequest(r15, r16)
            java.lang.String r9 = org.nick.wwwjdic.hkr.KanjiRecognizerClient.TAG
            java.lang.String r10 = "kanji recognizer request (%d strokes): %s"
            r11 = 2
            java.lang.Object[] r11 = new java.lang.Object[r11]
            r12 = 0
            int r13 = r15.size()
            java.lang.Integer r13 = java.lang.Integer.valueOf(r13)
            r11[r12] = r13
            r12 = 1
            r11[r12] = r1
            java.lang.String r10 = java.lang.String.format(r10, r11)
            android.util.Log.d(r9, r10)
            r9 = 1
            java.lang.Object[] r9 = new java.lang.Object[r9]
            r10 = 0
            r9[r10] = r1
            org.apache.http.client.methods.HttpPost r2 = r14.createPost(r9)
            r4 = 0
            org.apache.http.impl.client.DefaultHttpClient r9 = r14.httpClient     // Catch:{ HttpResponseException -> 0x007d, JSONException -> 0x00a6 }
            org.apache.http.HttpResponse r6 = r9.execute(r2)     // Catch:{ HttpResponseException -> 0x007d, JSONException -> 0x00a6 }
            java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ HttpResponseException -> 0x007d, JSONException -> 0x00a6 }
            java.io.InputStreamReader r9 = new java.io.InputStreamReader     // Catch:{ HttpResponseException -> 0x007d, JSONException -> 0x00a6 }
            org.apache.http.HttpEntity r10 = r6.getEntity()     // Catch:{ HttpResponseException -> 0x007d, JSONException -> 0x00a6 }
            java.io.InputStream r10 = r10.getContent()     // Catch:{ HttpResponseException -> 0x007d, JSONException -> 0x00a6 }
            java.lang.String r11 = "utf-8"
            r9.<init>(r10, r11)     // Catch:{ HttpResponseException -> 0x007d, JSONException -> 0x00a6 }
            r5.<init>(r9)     // Catch:{ HttpResponseException -> 0x007d, JSONException -> 0x00a6 }
            java.lang.String r7 = r14.readAllLines(r5)     // Catch:{ HttpResponseException -> 0x00d1, JSONException -> 0x00cd, all -> 0x00ca }
            java.lang.String r9 = org.nick.wwwjdic.hkr.KanjiRecognizerClient.TAG     // Catch:{ HttpResponseException -> 0x00d1, JSONException -> 0x00cd, all -> 0x00ca }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ HttpResponseException -> 0x00d1, JSONException -> 0x00cd, all -> 0x00ca }
            java.lang.String r11 = "kanji recognizer response: "
            r10.<init>(r11)     // Catch:{ HttpResponseException -> 0x00d1, JSONException -> 0x00cd, all -> 0x00ca }
            java.lang.StringBuilder r10 = r10.append(r7)     // Catch:{ HttpResponseException -> 0x00d1, JSONException -> 0x00cd, all -> 0x00ca }
            java.lang.String r10 = r10.toString()     // Catch:{ HttpResponseException -> 0x00d1, JSONException -> 0x00cd, all -> 0x00ca }
            android.util.Log.d(r9, r10)     // Catch:{ HttpResponseException -> 0x00d1, JSONException -> 0x00cd, all -> 0x00ca }
            java.lang.String[] r8 = r14.parseResponse(r7)     // Catch:{ HttpResponseException -> 0x00d1, JSONException -> 0x00cd, all -> 0x00ca }
            if (r5 == 0) goto L_0x007c
            r5.close()
        L_0x007c:
            return r8
        L_0x007d:
            r9 = move-exception
            r3 = r9
        L_0x007f:
            java.lang.String r9 = org.nick.wwwjdic.hkr.KanjiRecognizerClient.TAG     // Catch:{ all -> 0x009f }
            java.lang.String r10 = "HTTP response exception"
            android.util.Log.e(r9, r10, r3)     // Catch:{ all -> 0x009f }
            java.lang.RuntimeException r9 = new java.lang.RuntimeException     // Catch:{ all -> 0x009f }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ all -> 0x009f }
            java.lang.String r11 = "HTTP request failed. Status: "
            r10.<init>(r11)     // Catch:{ all -> 0x009f }
            int r11 = r3.getStatusCode()     // Catch:{ all -> 0x009f }
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ all -> 0x009f }
            java.lang.String r10 = r10.toString()     // Catch:{ all -> 0x009f }
            r9.<init>(r10)     // Catch:{ all -> 0x009f }
            throw r9     // Catch:{ all -> 0x009f }
        L_0x009f:
            r9 = move-exception
        L_0x00a0:
            if (r4 == 0) goto L_0x00a5
            r4.close()
        L_0x00a5:
            throw r9
        L_0x00a6:
            r9 = move-exception
            r0 = r9
        L_0x00a8:
            java.lang.String r9 = org.nick.wwwjdic.hkr.KanjiRecognizerClient.TAG     // Catch:{ all -> 0x009f }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ all -> 0x009f }
            java.lang.String r11 = "Error parsing JSON: "
            r10.<init>(r11)     // Catch:{ all -> 0x009f }
            java.lang.String r11 = r0.getMessage()     // Catch:{ all -> 0x009f }
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ all -> 0x009f }
            java.lang.String r10 = r10.toString()     // Catch:{ all -> 0x009f }
            android.util.Log.e(r9, r10, r0)     // Catch:{ all -> 0x009f }
            java.lang.RuntimeException r9 = new java.lang.RuntimeException     // Catch:{ all -> 0x009f }
            java.lang.String r10 = r0.getMessage()     // Catch:{ all -> 0x009f }
            r9.<init>(r10)     // Catch:{ all -> 0x009f }
            throw r9     // Catch:{ all -> 0x009f }
        L_0x00ca:
            r9 = move-exception
            r4 = r5
            goto L_0x00a0
        L_0x00cd:
            r9 = move-exception
            r0 = r9
            r4 = r5
            goto L_0x00a8
        L_0x00d1:
            r9 = move-exception
            r3 = r9
            r4 = r5
            goto L_0x007f
        */
        throw new UnsupportedOperationException("Method not decompiled: org.nick.wwwjdic.hkr.KanjiRecognizerClient.recognize(java.util.List, boolean):java.lang.String[]");
    }

    private String[] parseResponse(String response) throws JSONException {
        JSONObject jsonObj = new JSONObject(response);
        String status = jsonObj.getString("status");
        if (!"OK".equals(status)) {
            throw new RuntimeException("Error calling kanji recognizer: " + status);
        }
        JSONArray jsonArr = jsonObj.getJSONArray("results");
        String[] result = new String[jsonArr.length()];
        for (int i = 0; i < result.length; i++) {
            result[i] = (String) jsonArr.get(i);
        }
        return result;
    }

    /* access modifiers changed from: protected */
    public AbstractHttpEntity createEntity(Object... params) throws IOException {
        return new KrEntity((String) params[0]);
    }
}
