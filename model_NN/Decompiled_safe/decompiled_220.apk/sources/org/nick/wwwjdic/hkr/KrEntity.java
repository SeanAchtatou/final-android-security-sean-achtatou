package org.nick.wwwjdic.hkr;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import org.apache.http.entity.AbstractHttpEntity;

public class KrEntity extends AbstractHttpEntity {
    private byte[] contentBytes;

    public KrEntity(String content) {
        try {
            this.contentBytes = content.getBytes("ASCII");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public InputStream getContent() throws IOException, IllegalStateException {
        throw new UnsupportedOperationException();
    }

    public long getContentLength() {
        return (long) this.contentBytes.length;
    }

    public boolean isRepeatable() {
        return false;
    }

    public boolean isStreaming() {
        return false;
    }

    public void writeTo(OutputStream os) throws IOException {
        os.write(this.contentBytes);
    }
}
