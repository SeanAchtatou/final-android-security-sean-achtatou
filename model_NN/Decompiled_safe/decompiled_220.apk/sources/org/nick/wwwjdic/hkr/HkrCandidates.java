package org.nick.wwwjdic.hkr;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.text.ClipboardManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import org.nick.wwwjdic.Constants;
import org.nick.wwwjdic.KanjiResultListView;
import org.nick.wwwjdic.R;
import org.nick.wwwjdic.SearchCriteria;

public class HkrCandidates extends ListActivity {
    private static final int MENU_ITEM_APPEND = 2;
    private static final int MENU_ITEM_COPY = 1;
    private static final int MENU_ITEM_DETAILS = 0;
    private static final String TAG = HkrCandidates.class.getSimpleName();
    private String[] candidates;
    protected ClipboardManager clipboard;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.candidates = getIntent().getExtras().getStringArray(Constants.HKR_CANDIDATES_KEY);
        setListAdapter(new ArrayAdapter(this, (int) R.layout.text_list_item, this.candidates));
        getListView().setOnCreateContextMenuListener(this);
        getListView().setTextFilterEnabled(true);
        setTitle(String.format(getResources().getString(R.string.candidates), Integer.valueOf(this.candidates.length)));
        this.clipboard = (ClipboardManager) getSystemService("clipboard");
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        showDetails(this.candidates[position]);
    }

    private void showDetails(String searchKey) {
        SearchCriteria criteria = SearchCriteria.createForKanjiOrReading(searchKey);
        Bundle extras = new Bundle();
        extras.putSerializable(Constants.CRITERIA_KEY, criteria);
        Intent intent = new Intent(this, KanjiResultListView.class);
        intent.putExtras(extras);
        startActivity(intent);
    }

    private void copy(String kanji) {
        this.clipboard.setText(kanji);
        Toast.makeText(this, String.format(getResources().getString(R.string.copied_to_clipboard), kanji), 0).show();
    }

    private void append(String kanji) {
        CharSequence text = this.clipboard.getText();
        this.clipboard.setText(String.valueOf(text == null ? "" : text.toString()) + kanji);
        Toast.makeText(this, String.format(getResources().getString(R.string.appended_to_clipboard), kanji), 0).show();
    }

    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        menu.add(0, 0, 0, (int) R.string.kanji_details);
        menu.add(0, 1, 1, (int) R.string.copy);
        menu.add(0, 2, 2, (int) R.string.append);
    }

    public boolean onContextItemSelected(MenuItem item) {
        try {
            String kanji = this.candidates[((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).position];
            switch (item.getItemId()) {
                case 0:
                    showDetails(kanji);
                    return true;
                case 1:
                    copy(kanji);
                    return true;
                case 2:
                    append(kanji);
                    return true;
                default:
                    return false;
            }
        } catch (ClassCastException e) {
            Log.e(TAG, "bad menuInfo", e);
            return false;
        }
    }
}
