package org.nick.wwwjdic.hkr;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import java.util.ArrayList;
import java.util.List;

public class KanjiDrawView extends View {
    private static final float OUTLINE_WIDTH = 2.0f;
    private static final float STROKE_WIDTH = 8.0f;
    private boolean annotateStrokes = true;
    private boolean annotateStrokesMidway = false;
    private Stroke currentStroke = null;
    private boolean currentStrokeDone = false;
    private OnStrokesChangedListener onStrokesChangedListener;
    private Paint outlinePaint;
    private Paint strokeAnnotationPaint;
    private Paint strokePaint;
    private List<Stroke> strokes = new ArrayList();

    public interface OnStrokesChangedListener {
        void strokesUpdated(int i);
    }

    public KanjiDrawView(Context context) {
        super(context);
        init();
    }

    public KanjiDrawView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        setFocusable(true);
        setFocusableInTouchMode(true);
        this.strokePaint = new Paint();
        this.strokePaint.setColor(-1);
        this.strokePaint.setStyle(Paint.Style.STROKE);
        this.strokePaint.setAntiAlias(true);
        this.strokePaint.setDither(true);
        this.strokePaint.setStrokeWidth(STROKE_WIDTH);
        this.strokePaint.setStrokeJoin(Paint.Join.ROUND);
        this.strokePaint.setStrokeCap(Paint.Cap.ROUND);
        this.strokeAnnotationPaint = new Paint();
        this.strokeAnnotationPaint.setColor(-16711936);
        this.strokeAnnotationPaint.setStyle(Paint.Style.FILL);
        this.strokeAnnotationPaint.setAntiAlias(true);
        this.outlinePaint = new Paint();
        this.outlinePaint.setColor(-7829368);
        this.outlinePaint.setStyle(Paint.Style.STROKE);
        this.outlinePaint.setAntiAlias(true);
        this.outlinePaint.setStrokeWidth(OUTLINE_WIDTH);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
    }

    public void setStrokePaintColor(int color) {
        this.strokePaint.setColor(color);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        Rect r = new Rect();
        getDrawingRect(r);
        canvas.drawRect(r, this.outlinePaint);
        drawStrokes(canvas);
    }

    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();
        switch (event.getAction()) {
            case 0:
                this.currentStroke = new Stroke();
                this.currentStroke.addPoint(new PointF(x, y));
                this.strokes.add(this.currentStroke);
                this.currentStrokeDone = false;
                break;
            case 1:
                this.currentStroke.addPoint(new PointF(x, y));
                if (this.onStrokesChangedListener != null) {
                    this.onStrokesChangedListener.strokesUpdated(this.strokes.size());
                }
                this.currentStrokeDone = true;
                break;
            case 2:
                this.currentStroke.addPoint(new PointF(x, y));
                break;
        }
        invalidate();
        return true;
    }

    private void drawStrokes(Canvas canvas) {
        int strokeNum = 1;
        for (int i = 0; i < this.strokes.size(); i++) {
            Stroke stroke = this.strokes.get(i);
            stroke.draw(canvas, this.strokePaint);
            if (this.annotateStrokes) {
                if (i != this.strokes.size() - 1 || this.currentStrokeDone) {
                    if (this.annotateStrokesMidway) {
                        stroke.annotateMidway(canvas, this.strokeAnnotationPaint, strokeNum);
                    } else {
                        stroke.annotate(canvas, this.strokeAnnotationPaint, strokeNum);
                    }
                    strokeNum++;
                } else {
                    return;
                }
            }
        }
    }

    public List<Stroke> getStrokes() {
        return this.strokes;
    }

    public void removeLastStroke() {
        if (!this.strokes.isEmpty()) {
            this.strokes.remove(this.strokes.size() - 1);
            invalidate();
        }
    }

    public void clear() {
        this.strokes.clear();
        invalidate();
    }

    public OnStrokesChangedListener getOnStrokesChangedListener() {
        return this.onStrokesChangedListener;
    }

    public void setOnStrokesChangedListener(OnStrokesChangedListener onStrokesChangedListener2) {
        this.onStrokesChangedListener = onStrokesChangedListener2;
    }

    public boolean isAnnotateStrokes() {
        return this.annotateStrokes;
    }

    public void setAnnotateStrokes(boolean annotateStrokes2) {
        this.annotateStrokes = annotateStrokes2;
    }

    public boolean isAnnotateStrokesMidway() {
        return this.annotateStrokesMidway;
    }

    public void setAnnotateStrokesMidway(boolean annotateStrokesMidway2) {
        this.annotateStrokesMidway = annotateStrokesMidway2;
    }
}
