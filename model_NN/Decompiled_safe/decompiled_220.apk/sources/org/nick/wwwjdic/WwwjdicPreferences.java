package org.nick.wwwjdic;

import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.util.Log;
import au.com.bytecode.opencsv.CSVWriter;
import java.util.Arrays;
import java.util.List;

public class WwwjdicPreferences extends PreferenceActivity implements Preference.OnPreferenceChangeListener {
    public static final String DEFAULT_STROKE_ANIMATION_DELAY = "150";
    public static final String DEFAULT_WWWJDIC_URL = "http://wwwjdic.mygengo.com/cgi-data/wwwjdic";
    public static final long KOD_DEFAULT_UPDATE_INTERVAL = 86400000;
    public static final String KR_DEFAULT_URL = "http://kanji.sljfaq.org/kanji-0.016.cgi";
    public static final String KR_PACKAGE = "org.nick.kanjirecognizer";
    private static final int KR_TIMEOUT_DEFAULT = 10000;
    private static final String PREF_ACCOUNT_NAME_KEY = "pref_account_name";
    public static final String PREF_AUTO_SELECT_MIRROR_KEY = "pref_auto_select_mirror";
    private static final String PREF_DEFAULT_DICT_PREF_KEY = "pref_default_dict";
    private static final String PREF_DIRECT_SEARCH_KEY = "pref_ocr_direct_search";
    public static final String PREF_DONATION_THANKS_SHOWN = "pref_donation_thanks_shown";
    private static final String PREF_DUMP_CROPPED_IMAGES_KEY = "pref_ocr_dump_cropped_images";
    private static final String PREF_ENABLE_ANALYTICS_KEY = "pref_enable_analytics";
    private static final String PREF_ENABLE_UPDATE_CHECK_KEY = "pref_enable_update_check";
    private static final String PREF_EXPORT_MEANINGS_SEPARATOR_CHAR = "pref_export_meanings_separator_char";
    private static final String PREF_KOD_JLPT_LEVEL = "prf_kod_jlpt_level";
    private static final String PREF_KOD_LEVEL1_ONLY_KEY = "pref kod_level_one_only";
    private static final String PREF_KOD_SHOW_READING_KEY = "pref_kod_show_reading";
    private static final String PREF_KOD_UPDATE_INTERAVL_KEY = "pref_kod_update_interval";
    private static final String PREF_KOD_USE_JLPT_KEY = "pref_kod_use_jlpt";
    private static final String PREF_KR_ANNOTATE = "pref_kr_annotate";
    private static final String PREF_KR_ANNOTATE_MIDWAY = "pref_kr_annotate_midway";
    private static final String PREF_KR_TIMEOUT_KEY = "pref_kr_timeout";
    public static final String PREF_KR_URL_KEY = "pref_kr_url";
    private static final String PREF_KR_USE_KANJI_RECOGNIZER_KEY = "pref_kr_use_kanji_recognizer";
    private static final String PREF_LAST_KOD_UPDATE_ERROR_KEY = "pref_last_kod_update_error";
    private static final String PREF_LAST_UPDATE_CHECK_KEY = "pref_last_update_check";
    private static final String PREF_RANDOM_EXAMPLES_KEY = "pref_random_examples";
    private static final String PREF_SOD_ANIMATION_DELAY = "pref_sod_animation_delay";
    private static final String PREF_SOD_TIMEOUT = "pref_sod_server_timeout";
    public static final String PREF_TIP_SHOWN = "pref_tip_shown";
    public static final String PREF_USE_KR_KEY = "pref_kr_use_kanji_recognizer";
    private static final String PREF_WANTS_TTS_KEY = "pref_wants_tts";
    private static final String PREF_WEOCR_TIMEOUT_KEY = "pref_weocr_timeout";
    private static final String PREF_WEOCR_URL_KEY = "pref_weocr_url";
    public static final String PREF_WHATS_NEW_SHOWN = "pref_whats_new_shown";
    public static final String PREF_WWWJDIC_MIRROR_URL_KEY = "pref_wwwjdic_mirror_url";
    public static final String PREF_WWWJDIC_TIMEOUT_KEY = "pref_wwwjdic_timeout";
    private static final int SOD_TIMEOUT_DEFAULT = 30000;
    private static final String TAG = WwwjdicPreferences.class.getSimpleName();
    public static final long UPDATE_CHECK_INTERVAL_SECS = 86400;
    private static final String WEOCR_DEFAULT_URL = "http://maggie.ocrgrid.org/cgi-bin/weocr/nhocr.cgi";
    private static final int WEOCR_TIMEOUT_DEFAULT = 10000;
    private static final int WWWJDIC_TIMEOUT_DEFAULT = 10000;
    private CheckBoxPreference autoSelectMirrorPreference;
    private ListPreference defaultDictPreference;
    private ListPreference mirrorPreference;
    private CheckBoxPreference useKrPreference;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle((int) R.string.settings);
        addPreferencesFromResource(R.xml.preferences);
        this.useKrPreference = (CheckBoxPreference) findPreference("pref_kr_use_kanji_recognizer");
        this.useKrPreference.setOnPreferenceChangeListener(this);
        this.autoSelectMirrorPreference = (CheckBoxPreference) findPreference(PREF_AUTO_SELECT_MIRROR_KEY);
        this.autoSelectMirrorPreference.setOnPreferenceChangeListener(this);
        this.mirrorPreference = (ListPreference) findPreference(PREF_WWWJDIC_MIRROR_URL_KEY);
        this.mirrorPreference.setSummary(this.mirrorPreference.getEntry());
        this.mirrorPreference.setOnPreferenceChangeListener(this);
        this.defaultDictPreference = (ListPreference) findPreference(PREF_DEFAULT_DICT_PREF_KEY);
        this.defaultDictPreference.setSummary(this.defaultDictPreference.getEntry());
        this.defaultDictPreference.setOnPreferenceChangeListener(this);
    }

    public boolean onPreferenceChange(Preference preference, Object newValue) {
        if (!"pref_kr_use_kanji_recognizer".equals(preference.getKey()) || !((Boolean) newValue).booleanValue()) {
            if (PREF_AUTO_SELECT_MIRROR_KEY.equals(preference.getKey())) {
                if (((Boolean) newValue).booleanValue()) {
                    ((WwwjdicApplication) getApplication()).setMirrorBasedOnLocation();
                    this.mirrorPreference.setSummary(getMirrorName(getWwwjdicUrl(this)));
                }
                return true;
            }
            if (PREF_WWWJDIC_MIRROR_URL_KEY.equals(preference.getKey())) {
                preference.setSummary(getMirrorName((String) newValue));
            }
            if (PREF_DEFAULT_DICT_PREF_KEY.equals(preference.getKey())) {
                preference.setSummary(getDictionaryName(Integer.valueOf((String) newValue).intValue()));
            }
            return true;
        } else if (isKrInstalled(this, getApplication())) {
            return true;
        } else {
            showInstallKrDialog();
            return false;
        }
    }

    private String getMirrorName(String url) {
        Resources r = getResources();
        List<String> mirrorUrls = Arrays.asList(r.getStringArray(R.array.wwwjdic_mirror_urls));
        String[] mirrorNames = r.getStringArray(R.array.wwwjdic_mirror_names);
        int idx = mirrorUrls.indexOf(url);
        if (idx != -1) {
            return mirrorNames[idx];
        }
        return "";
    }

    private void showInstallKrDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage((int) R.string.install_kr).setCancelable(false).setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                WwwjdicPreferences.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(WwwjdicPreferences.this.getResources().getString(R.string.kr_download_uri))));
            }
        }).setNegativeButton((int) R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        builder.create().show();
    }

    public static boolean isKrInstalled(Context context, Application application) {
        boolean result;
        Log.d(TAG, "Checking for Kanji Recognizer...");
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo pi = pm.getPackageInfo(KR_PACKAGE, 0);
            Log.d(TAG, String.format("Found KR: %s, version %s(%d)", pi.packageName, pi.versionName, Integer.valueOf(pi.versionCode)));
            if (pi.versionCode < 2) {
                Log.d(TAG, String.format("Kanji recognizer %s is installed, but we need 1.1", pi.versionName));
                return false;
            }
            String myPackageName = application.getPackageName();
            Log.d(TAG, String.format("Checking for signature match: my package = %s, KR package = %s", myPackageName, pi.packageName));
            if (pm.checkSignatures(myPackageName, pi.packageName) == 0) {
                result = true;
            } else {
                result = false;
            }
            Log.d(TAG, "signature match: " + result);
            return result;
        } catch (PackageManager.NameNotFoundException e) {
            Log.w(TAG, "Kanji Recognizer not found", e);
            return false;
        }
    }

    public static int getDefaultDictionaryIdx(Context context) {
        try {
            return Integer.parseInt(getPrefs(context).getString(PREF_DEFAULT_DICT_PREF_KEY, "0"));
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public static String getDefaultDictionary(Context context) {
        return context.getResources().getStringArray(R.array.dictionary_codes_array)[getDefaultDictionaryIdx(context)];
    }

    private String getDictionaryName(int dictIdx) {
        String[] dictionaryNames = getResources().getStringArray(R.array.dictionaries_array);
        if (dictIdx < 0 || dictIdx >= dictionaryNames.length) {
            return "";
        }
        return dictionaryNames[dictIdx];
    }

    public static String getMeaningsSeparatorCharacter(Context context) {
        return getPrefs(context).getString(PREF_EXPORT_MEANINGS_SEPARATOR_CHAR, CSVWriter.DEFAULT_LINE_END);
    }

    public static String getWwwjdicUrl(Context context) {
        return getPrefs(context).getString(PREF_WWWJDIC_MIRROR_URL_KEY, DEFAULT_WWWJDIC_URL);
    }

    public static void setWwwjdicUrl(String url, Context context) {
        SharedPreferences.Editor editor = getPrefsEditor(context);
        editor.putString(PREF_WWWJDIC_MIRROR_URL_KEY, url);
        editor.commit();
    }

    public static int getWwwjdicTimeoutSeconds(Context context) {
        try {
            return Integer.parseInt(getPrefs(context).getString(PREF_WWWJDIC_TIMEOUT_KEY, "10"));
        } catch (NumberFormatException e) {
            return 10000;
        }
    }

    public static int getKrTimeout(Context context) {
        try {
            return Integer.parseInt(getPrefs(context).getString(PREF_KR_TIMEOUT_KEY, "10")) * 1000;
        } catch (NumberFormatException e) {
            return 10000;
        }
    }

    public static String getKrUrl(Context context) {
        return getPrefs(context).getString(PREF_KR_URL_KEY, KR_DEFAULT_URL);
    }

    public static boolean isAnnotateStrokesMidway(Context context) {
        return getPrefs(context).getBoolean(PREF_KR_ANNOTATE_MIDWAY, false);
    }

    public static boolean isAnnoateStrokes(Context context) {
        return getPrefs(context).getBoolean(PREF_KR_ANNOTATE, true);
    }

    public static boolean isUseKanjiRecognizer(Context context) {
        return getPrefs(context).getBoolean("pref_kr_use_kanji_recognizer", false);
    }

    public static void setUseKanjiRecognizer(boolean useKr, Context context) {
        SharedPreferences.Editor editor = getPrefsEditor(context);
        editor.putBoolean("pref_kr_use_kanji_recognizer", useKr);
        editor.commit();
    }

    public static int getWeocrTimeout(Context context) {
        try {
            return Integer.parseInt(getPrefs(context).getString(PREF_WEOCR_TIMEOUT_KEY, "10")) * 1000;
        } catch (NumberFormatException e) {
            return 10000;
        }
    }

    public static String getWeocrUrl(Context context) {
        return getPrefs(context).getString(PREF_WEOCR_URL_KEY, WEOCR_DEFAULT_URL);
    }

    public static boolean isDumpCroppedImages(Context context) {
        return getPrefs(context).getBoolean(PREF_DUMP_CROPPED_IMAGES_KEY, false);
    }

    public static boolean isDirectSearch(Context context) {
        return getPrefs(context).getBoolean(PREF_DIRECT_SEARCH_KEY, false);
    }

    public static int getStrokeAnimationDelay(Context context) {
        try {
            return Integer.parseInt(getPrefs(context).getString(PREF_SOD_ANIMATION_DELAY, DEFAULT_STROKE_ANIMATION_DELAY));
        } catch (NumberFormatException e) {
            return Integer.parseInt(DEFAULT_STROKE_ANIMATION_DELAY);
        }
    }

    public static void setStrokeAnimationDelay(Context context, String delayMillisStr) {
        SharedPreferences.Editor editor = getPrefsEditor(context);
        editor.putString(PREF_SOD_ANIMATION_DELAY, delayMillisStr);
        editor.commit();
    }

    public static int getSodServerTimeout(Context context) {
        try {
            return Integer.parseInt(getPrefs(context).getString(PREF_SOD_TIMEOUT, "30")) * 1000;
        } catch (NumberFormatException e) {
            return SOD_TIMEOUT_DEFAULT;
        }
    }

    public static String getGoogleAcountName(Context context) {
        return getPrefs(context).getString(PREF_ACCOUNT_NAME_KEY, null);
    }

    public static synchronized void setGoogleAccountName(Context context, String accountName) {
        synchronized (WwwjdicPreferences.class) {
            SharedPreferences.Editor editor = getPrefs(context).edit();
            editor.putString(PREF_ACCOUNT_NAME_KEY, accountName);
            editor.commit();
        }
    }

    public static boolean isAnalyticsEnabled(Context ctx) {
        return getPrefs(ctx).getBoolean(PREF_ENABLE_ANALYTICS_KEY, true);
    }

    public static boolean isDonationThanksShown(Context context) {
        return getPrefs(context).getBoolean(PREF_DONATION_THANKS_SHOWN, false);
    }

    public static synchronized void setDonationThanksShown(Context context) {
        synchronized (WwwjdicPreferences.class) {
            getPrefs(context).edit().putBoolean(PREF_DONATION_THANKS_SHOWN, true).commit();
        }
    }

    public static boolean isWhatsNewShown(Context context, String versionName) {
        return getPrefs(context).getBoolean("pref_whats_new_shown_" + versionName, false);
    }

    public static synchronized void setWhantsNewShown(Context context, String versionName) {
        synchronized (WwwjdicPreferences.class) {
            SharedPreferences prefs = getPrefs(context);
            prefs.edit().putBoolean("pref_whats_new_shown_" + versionName, true).commit();
        }
    }

    public static boolean isTipShown(Context context, String tipKey) {
        return getPrefs(context).getBoolean("pref_tip_shown_" + tipKey, false);
    }

    public static void setTipShown(Context context, String tipKey) {
        SharedPreferences prefs = getPrefs(context);
        prefs.edit().putBoolean("pref_tip_shown_" + tipKey, true).commit();
    }

    private static void saveBooleanPref(Context context, String key, boolean value) {
        SharedPreferences.Editor editor = getPrefsEditor(context);
        editor.putBoolean(key, value);
        editor.commit();
    }

    private static SharedPreferences.Editor getPrefsEditor(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).edit();
    }

    private static boolean getBooleanPref(Context context, String key, boolean defValue) {
        return getPrefs(context).getBoolean(key, defValue);
    }

    private static SharedPreferences getPrefs(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static boolean isKodLevelOneOnly(Context context) {
        return getBooleanPref(context, PREF_KOD_LEVEL1_ONLY_KEY, false);
    }

    public static void setKodLevelOneOnly(Context context, boolean levelOneOnly) {
        saveBooleanPref(context, PREF_KOD_LEVEL1_ONLY_KEY, levelOneOnly);
    }

    public static boolean isKodUseJlpt(Context context) {
        return getBooleanPref(context, PREF_KOD_USE_JLPT_KEY, false);
    }

    public static void setKodUseJlpt(Context context, boolean levelOneOnly) {
        saveBooleanPref(context, PREF_KOD_USE_JLPT_KEY, levelOneOnly);
    }

    public static void setKodShowReading(Context context, boolean showReading) {
        saveBooleanPref(context, PREF_KOD_SHOW_READING_KEY, showReading);
    }

    public static boolean isKodShowReading(Context context) {
        return getBooleanPref(context, PREF_KOD_SHOW_READING_KEY, false);
    }

    public static void setKodUpdateInterval(Context context, long updateIntervalMillis) {
        SharedPreferences.Editor editor = getPrefsEditor(context);
        editor.putLong(PREF_KOD_UPDATE_INTERAVL_KEY, updateIntervalMillis);
        editor.commit();
    }

    public static long getKodUpdateInterval(Context context) {
        return getPrefs(context).getLong(PREF_KOD_UPDATE_INTERAVL_KEY, KOD_DEFAULT_UPDATE_INTERVAL);
    }

    public static void setKodJlptLevel(Context context, int jlptLevel) {
        SharedPreferences.Editor editor = getPrefsEditor(context);
        editor.putInt(PREF_KOD_JLPT_LEVEL, jlptLevel);
        editor.commit();
    }

    public static int getKodJlptLevel(Context context) {
        return getPrefs(context).getInt(PREF_KOD_JLPT_LEVEL, 1);
    }

    public static boolean wantsTts(Context context) {
        return getBooleanPref(context, PREF_WANTS_TTS_KEY, true);
    }

    public static synchronized void setWantsTts(Context context, boolean wantsTts) {
        synchronized (WwwjdicPreferences.class) {
            SharedPreferences.Editor editor = getPrefsEditor(context);
            editor.putBoolean(PREF_WANTS_TTS_KEY, wantsTts);
            editor.commit();
        }
    }

    public static boolean isUpdateCheckEnabled(Context context) {
        return getBooleanPref(context, PREF_ENABLE_UPDATE_CHECK_KEY, true);
    }

    public static synchronized void setLastUpdateCheck(Context context, long lastUpdateCheck) {
        synchronized (WwwjdicPreferences.class) {
            SharedPreferences.Editor editor = getPrefsEditor(context);
            editor.putLong(PREF_LAST_UPDATE_CHECK_KEY, lastUpdateCheck);
            editor.commit();
        }
    }

    public static long getLastUpdateCheck(Context context) {
        return getPrefs(context).getLong(PREF_LAST_UPDATE_CHECK_KEY, 0);
    }

    public static synchronized void setLastKodUpdateError(Context context, long lastUpdateError) {
        synchronized (WwwjdicPreferences.class) {
            SharedPreferences.Editor editor = getPrefsEditor(context);
            editor.putLong(PREF_LAST_KOD_UPDATE_ERROR_KEY, lastUpdateError);
            editor.commit();
        }
    }

    public static long getLastKodUpdateError(Context context) {
        return getPrefs(context).getLong(PREF_LAST_KOD_UPDATE_ERROR_KEY, 0);
    }

    public static boolean isReturnRandomExamples(Context context) {
        return getPrefs(context).getBoolean(PREF_RANDOM_EXAMPLES_KEY, false);
    }
}
