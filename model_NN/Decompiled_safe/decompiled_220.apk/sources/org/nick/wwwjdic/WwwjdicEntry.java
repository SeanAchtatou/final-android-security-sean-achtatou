package org.nick.wwwjdic;

import java.io.Serializable;

public abstract class WwwjdicEntry implements Serializable {
    private static final long serialVersionUID = 8688955395298491589L;
    protected String dictString;
    protected String dictionary;
    protected Long id;

    public abstract String getDetailString();

    public abstract String getHeadword();

    public abstract boolean isKanji();

    protected WwwjdicEntry(String dictString2) {
        this.dictString = dictString2;
    }

    protected WwwjdicEntry(String dictString2, String dictionary2) {
        this.dictString = dictString2;
        this.dictionary = dictionary2;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id2) {
        this.id = id2;
    }

    public String getDictString() {
        return this.dictString;
    }

    public boolean isSingleKanji() {
        return getHeadword().length() == 1;
    }

    public String getDictionary() {
        return this.dictionary;
    }

    public void setDictionary(String dictionary2) {
        this.dictionary = dictionary2;
    }
}
