package org.nick.wwwjdic;

import android.util.Log;
import au.com.bytecode.opencsv.CSVWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.nick.wwwjdic.utils.StringUtils;

public abstract class BackdoorSearchTask<T> extends SearchTask<T> {
    protected static final Pattern PRE_END_PATTERN = Pattern.compile("^</pre>.*$");
    protected static final String PRE_END_TAG = "</pre>";
    protected static final Pattern PRE_START_PATTERN = Pattern.compile("^<pre>.*$");
    private static final String TAG = BackdoorSearchTask.class.getSimpleName();

    /* access modifiers changed from: protected */
    public abstract String generateBackdoorCode(SearchCriteria searchCriteria);

    /* access modifiers changed from: protected */
    public abstract T parseEntry(String str);

    public BackdoorSearchTask(String url, int timeoutSeconds, ResultListView<T> resultListView, SearchCriteria criteria) {
        super(url, timeoutSeconds, resultListView, criteria);
    }

    /* access modifiers changed from: protected */
    public List<T> parseResult(String html) {
        List<T> result = new ArrayList<>();
        boolean isInPre = false;
        for (String line : html.split(CSVWriter.DEFAULT_LINE_END)) {
            if (!StringUtils.isEmpty(line)) {
                if (PRE_START_PATTERN.matcher(line).matches()) {
                    isInPre = true;
                } else if (PRE_END_PATTERN.matcher(line).matches()) {
                    break;
                } else if (isInPre) {
                    boolean hasEndPre = false;
                    if (line.contains(PRE_END_TAG)) {
                        hasEndPre = true;
                        line = line.replaceAll(PRE_END_TAG, "");
                    }
                    Log.d(TAG, "dic entry line: " + line);
                    T entry = parseEntry(line);
                    if (entry != null) {
                        result.add(entry);
                    }
                    if (hasEndPre) {
                        break;
                    }
                } else {
                    continue;
                }
            }
        }
        return result;
    }

    /* access modifiers changed from: protected */
    public String query(WwwjdicQuery query) {
        try {
            String lookupUrl = String.format("%s?%s", this.url, generateBackdoorCode((SearchCriteria) query));
            Log.d(TAG, "WWWJDIC URL: " + lookupUrl);
            return (String) this.httpclient.execute(new HttpGet(lookupUrl), this.responseHandler, this.localContext);
        } catch (ClientProtocolException e) {
            ClientProtocolException cpe = e;
            Log.e("WWWJDIC", "ClientProtocolException", cpe);
            throw new RuntimeException((Throwable) cpe);
        } catch (IOException e2) {
            IOException e3 = e2;
            Log.e("WWWJDIC", "IOException", e3);
            throw new RuntimeException(e3);
        }
    }
}
