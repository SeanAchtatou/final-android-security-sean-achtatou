package org.nick.wwwjdic;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import org.nick.wwwjdic.utils.StringUtils;

public class KanjiEntry extends WwwjdicEntry implements Serializable {
    private static final char CLASSICAL_RADICAL_CODE = 'C';
    private static final List<String> CODES = Arrays.asList("B", "C", "F", "G", "J", "H", "N", "V", "D", "P", "S", "U", "I", "Q", "M", "E", "K", "L", "O", "W", "Y", "X", "Z");
    private static final char FREQ_CODE = 'F';
    private static final char GRADE_CODE = 'G';
    private static final Pattern HIRAGANA_PATTERN = Pattern.compile("\\p{InHiragana}|-", 4);
    private static final int JISCODE_IDX = 1;
    private static final char JLTP_LEVEL_CODE = 'J';
    private static final int KANJI_IDX = 0;
    private static final Pattern KATAKANA_PATTERN = Pattern.compile("\\p{InKatakana}+", 4);
    private static final char KOREAN_READING_CODE = 'W';
    private static final String NANORI_TAG = "T1";
    private static final char PINYIN_CODE = 'Y';
    private static final char RADICAL_CODE = 'B';
    private static final String RADICAL_NAME_TAG = "T2";
    private static final char SKIP_CODE = 'P';
    private static final char STROKE_CODE = 'S';
    private static final char UNICODE_CODE = 'U';
    private static final long serialVersionUID = -2260771889935344623L;
    private Integer classicalRadicalNumber;
    private Integer frequncyeRank;
    private Integer grade;
    private String jisCode;
    private Integer jlptLevel;
    private String kanji;
    private String koreanReading;
    private String kunyomi;
    private List<String> meanings = new ArrayList();
    private String nanori;
    private String onyomi;
    private String pinyin;
    private String radicalName;
    private int radicalNumber;
    private String reading;
    private String skipCode;
    private int strokeCount;
    private String unicodeNumber;

    private KanjiEntry(String dictStr) {
        super(dictStr);
    }

    /* JADX INFO: Multiple debug info for r1v1 java.lang.String: [D('readingAndMeanings' java.lang.String), D('fields' java.lang.String[])] */
    /* JADX INFO: Multiple debug info for r7v16 java.lang.String: [D('meaningsStr' java.lang.String), D('bracketIdx' int)] */
    /* JADX INFO: Multiple debug info for r0v7 java.lang.String[]: [D('meanings' java.lang.String[]), D('reading' java.lang.String)] */
    public static KanjiEntry parseKanjidic(String kanjidicStr) {
        KanjiEntry result = new KanjiEntry(kanjidicStr);
        String[] fields = kanjidicStr.split(" ");
        result.kanji = fields[0];
        result.jisCode = fields[1];
        int i = 2;
        while (true) {
            int i2 = i;
            if (i2 < fields.length) {
                String field = fields[i2].trim();
                if (!"".equals(field)) {
                    char code = field.charAt(0);
                    if (CODES.contains(Character.toString(code))) {
                        switch (code) {
                            case 'B':
                                result.radicalNumber = parseIntCode(field).intValue();
                                continue;
                            case 'C':
                                result.classicalRadicalNumber = parseIntCode(field);
                                continue;
                            case 'F':
                                result.frequncyeRank = parseIntCode(field);
                                continue;
                            case 'G':
                                result.grade = parseIntCode(field);
                                continue;
                            case 'J':
                                result.jlptLevel = parseIntCode(field);
                                continue;
                            case 'P':
                                result.skipCode = parseStrCode(field);
                                continue;
                            case 'S':
                                if (result.strokeCount == 0) {
                                    result.strokeCount = parseIntCode(field).intValue();
                                    break;
                                } else {
                                    continue;
                                }
                            case 'U':
                                result.unicodeNumber = parseStrCode(field);
                                continue;
                            case 'W':
                                if (StringUtils.isEmpty(result.koreanReading) == 0) {
                                    result.koreanReading = String.valueOf(result.koreanReading) + " ";
                                    result.koreanReading = String.valueOf(result.koreanReading) + parseStrCode(field);
                                    break;
                                } else {
                                    result.koreanReading = parseStrCode(field);
                                    continue;
                                }
                            case 'Y':
                                if (StringUtils.isEmpty(result.pinyin) == 0) {
                                    result.pinyin = String.valueOf(result.pinyin) + " ";
                                    result.pinyin = String.valueOf(result.pinyin) + parseStrCode(field);
                                    break;
                                } else {
                                    result.pinyin = parseStrCode(field);
                                    continue;
                                }
                        }
                    } else {
                        String readingAndMeanings = StringUtils.join(fields, " ", i2);
                        int bracketIdx = readingAndMeanings.indexOf(123);
                        if (bracketIdx != -1) {
                            result.reading = readingAndMeanings.substring(0, bracketIdx).trim();
                            result.parseReading();
                            for (String meaning : readingAndMeanings.substring(bracketIdx).split("\\{")) {
                                if (!"".equals(meaning)) {
                                    result.meanings.add(meaning.replace("{", "").replace("}", "").trim());
                                }
                            }
                        } else {
                            result.reading = readingAndMeanings;
                            result.parseReading();
                        }
                    }
                }
                i = i2 + 1;
            }
        }
        return result;
    }

    private void parseReading() {
        String[] readingFields = this.reading.split(" ");
        StringBuffer onyomiBuff = new StringBuffer();
        StringBuffer kunyomiBuff = new StringBuffer();
        StringBuffer nanoriBuff = new StringBuffer();
        StringBuffer radicalNameBuff = new StringBuffer();
        boolean foundNanori = false;
        boolean foundRadicalName = false;
        for (String r : readingFields) {
            if (KATAKANA_PATTERN.matcher(r).matches()) {
                onyomiBuff.append(r.trim());
                onyomiBuff.append(" ");
            }
            if (HIRAGANA_PATTERN.matcher(Character.toString(r.charAt(0))).matches()) {
                if (foundNanori) {
                    nanoriBuff.append(r.trim());
                    nanoriBuff.append(" ");
                } else if (foundRadicalName) {
                    radicalNameBuff.append(r.trim());
                    radicalNameBuff.append(" ");
                } else {
                    kunyomiBuff.append(r.trim());
                    kunyomiBuff.append(" ");
                }
            }
            if (NANORI_TAG.equals(r)) {
                foundNanori = true;
                foundRadicalName = false;
            }
            if (RADICAL_NAME_TAG.equals(r)) {
                foundNanori = false;
                foundRadicalName = true;
            }
        }
        this.onyomi = onyomiBuff.toString().trim();
        this.kunyomi = kunyomiBuff.toString().trim();
        this.nanori = nanoriBuff.toString().trim();
        this.radicalName = radicalNameBuff.toString().trim();
        this.reading = this.reading.replaceAll(" T1", "");
        this.reading = this.reading.replaceAll(" T2", "");
        if (!StringUtils.isEmpty(this.koreanReading)) {
            this.koreanReading = this.koreanReading.trim();
        }
        if (!StringUtils.isEmpty(this.pinyin)) {
            this.pinyin = this.pinyin.trim();
        }
    }

    private static Integer parseIntCode(String field) {
        return Integer.valueOf(Integer.parseInt(field.substring(1)));
    }

    private static String parseStrCode(String field) {
        return field.substring(1);
    }

    public String getKanji() {
        return this.kanji;
    }

    public String getJisCode() {
        return this.jisCode;
    }

    public String getUnicodeNumber() {
        return this.unicodeNumber;
    }

    public int getRadicalNumber() {
        return this.radicalNumber;
    }

    public Integer getClassicalRadicalNumber() {
        return this.classicalRadicalNumber;
    }

    public Integer getFrequncyeRank() {
        return this.frequncyeRank;
    }

    public Integer getGrade() {
        return this.grade;
    }

    public int getStrokeCount() {
        return this.strokeCount;
    }

    public Integer getJlptLevel() {
        return this.jlptLevel;
    }

    public String getSkipCode() {
        return this.skipCode;
    }

    public String getKoreanReading() {
        return this.koreanReading;
    }

    public String getPinyin() {
        return this.pinyin;
    }

    public String getReading() {
        return this.reading;
    }

    public String getOnyomi() {
        return this.onyomi;
    }

    public String getKunyomi() {
        return this.kunyomi;
    }

    public String getNanori() {
        return this.nanori;
    }

    public String getRadicalName() {
        return this.radicalName;
    }

    public List<String> getMeanings() {
        return Collections.unmodifiableList(this.meanings);
    }

    public String getMeaningsAsString() {
        return StringUtils.join(getMeanings(), "/", 0);
    }

    public String getDetailString() {
        return String.valueOf(this.reading) + " " + getMeaningsAsString();
    }

    public String getHeadword() {
        return this.kanji;
    }

    public boolean isKanji() {
        return true;
    }
}
