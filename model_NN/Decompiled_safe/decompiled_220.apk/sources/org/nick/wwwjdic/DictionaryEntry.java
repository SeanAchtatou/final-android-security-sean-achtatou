package org.nick.wwwjdic;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.nick.wwwjdic.utils.StringUtils;

public class DictionaryEntry extends WwwjdicEntry implements Serializable {
    private static final int READING_IDX = 1;
    private static final int WORD_IDX = 0;
    private static final long serialVersionUID = 2057121563183659358L;
    private List<String> meanings = new ArrayList();
    private String reading;
    private String tranlsationString;
    private String word;

    private DictionaryEntry(String edictStr, String dictionary) {
        super(edictStr, dictionary);
    }

    public static DictionaryEntry parseEdict(String edictStr, String dictionary) {
        String meaningsField;
        DictionaryEntry result = new DictionaryEntry(edictStr, dictionary);
        String[] fields = edictStr.split(" ");
        result.word = fields[0];
        String translationString = edictStr.substring(edictStr.indexOf(" ") + 1);
        result.tranlsationString = translationString.replace("/", " ").trim();
        if (translationString.indexOf(91) != -1) {
            result.reading = fields[1].replaceAll("\\[", "").replaceAll("\\]", "");
            meaningsField = edictStr.substring(edictStr.indexOf(93) + 2);
        } else {
            int firstSlashIdx = translationString.indexOf(47);
            if (firstSlashIdx != -1) {
                meaningsField = translationString.substring(firstSlashIdx);
            } else {
                meaningsField = translationString.trim();
            }
        }
        String[] meaningsArr = meaningsField.split("/");
        for (String meaning : meaningsArr) {
            if (!"".equals(meaning) && !Constants.DICT_COMMON_USAGE_MARKER.equals(meaning)) {
                result.meanings.add(meaning);
            }
        }
        return result;
    }

    public String getWord() {
        return this.word;
    }

    public String getReading() {
        return this.reading;
    }

    public List<String> getMeanings() {
        return Collections.unmodifiableList(this.meanings);
    }

    public String getTranslationString() {
        return this.tranlsationString;
    }

    public String getMeaningsAsString() {
        return StringUtils.join(getMeanings(), "/", 0);
    }

    public String getDetailString() {
        return String.valueOf(this.reading == null ? "" : this.reading) + " " + (this.tranlsationString == null ? "" : this.tranlsationString);
    }

    public String getHeadword() {
        return this.word;
    }

    public boolean isKanji() {
        return false;
    }
}
