package org.nick.wwwjdic.history.gdocs;

import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.util.Key;
import java.io.IOException;
import java.util.List;

public class Feed {
    @Key("link")
    public List<Link> links;
    @Key("openSearch:totalResults")
    public int totalResults;

    static Feed executeGet(HttpTransport transport, DocsUrl url, Class<? extends Feed> feedClass) throws IOException {
        HttpRequest request = transport.buildGetRequest();
        request.url = url;
        return (Feed) request.execute().parseAs(feedClass);
    }
}
