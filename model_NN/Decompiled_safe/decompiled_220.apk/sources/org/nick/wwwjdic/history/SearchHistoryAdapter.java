package org.nick.wwwjdic.history;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;

class SearchHistoryAdapter extends CursorAdapter {
    public SearchHistoryAdapter(Context context, Cursor c) {
        super(context, c);
    }

    public void bindView(View view, Context context, Cursor cursor) {
        ((HistoryItem) view).populate(HistoryDbHelper.createCriteria(cursor));
    }

    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return new HistoryItem(context);
    }
}
