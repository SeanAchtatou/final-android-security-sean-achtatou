package org.nick.wwwjdic.history;

import org.nick.wwwjdic.DictionaryEntry;
import org.nick.wwwjdic.KanjiEntry;
import org.nick.wwwjdic.WwwjdicEntry;
import org.nick.wwwjdic.utils.StringUtils;

public class FavoritesEntryParser {
    public static final int DICTIONARY_IDX = 4;
    private static final int DICT_DETAILS_NUM_FIELDS = 3;
    public static final int DICT_STR_IDX = 2;
    public static final int HEADWORD_IDX = 1;
    private static final int KANJI_DETAILS_NUM_FIELDS = 17;
    private static final int NUM_FIELDS = 5;
    public static final int TIME_IDX = 3;
    private static final int TYPE_DICT = 0;
    public static final int TYPE_IDX = 0;
    private static final int TYPE_KANJI = 1;

    private FavoritesEntryParser() {
    }

    public static String[] toStringArray(WwwjdicEntry entry, long time) {
        String[] result = new String[5];
        result[0] = entry.isKanji() ? "1" : "0";
        result[1] = entry.getHeadword();
        result[2] = entry.getDictString();
        result[3] = Long.toString(time);
        result[4] = entry.getDictionary();
        return result;
    }

    public static String[] toParsedStringArray(WwwjdicEntry entry, String meaningsSeparatorChar) {
        if (entry.isKanji()) {
            return generateKanjiCsv((KanjiEntry) entry, meaningsSeparatorChar);
        }
        return generateDictCsv((DictionaryEntry) entry, meaningsSeparatorChar);
    }

    private static String[] generateKanjiCsv(KanjiEntry entry, String meaningsSeparatorChar) {
        String[] result = new String[KANJI_DETAILS_NUM_FIELDS];
        int idx = 0 + 1;
        result[0] = entry.getKanji();
        int idx2 = idx + 1;
        result[idx] = entry.getOnyomi();
        int idx3 = idx2 + 1;
        result[idx2] = entry.getKunyomi();
        int idx4 = idx3 + 1;
        result[idx3] = entry.getNanori();
        int idx5 = idx4 + 1;
        result[idx4] = entry.getRadicalName();
        int idx6 = idx5 + 1;
        result[idx5] = Integer.toString(entry.getRadicalNumber());
        int idx7 = idx6 + 1;
        result[idx6] = Integer.toString(entry.getStrokeCount());
        int idx8 = idx7 + 1;
        result[idx7] = toStr(entry.getClassicalRadicalNumber());
        int idx9 = idx8 + 1;
        result[idx8] = StringUtils.join(entry.getMeanings(), meaningsSeparatorChar, 0);
        int idx10 = idx9 + 1;
        result[idx9] = entry.getJisCode();
        int idx11 = idx10 + 1;
        result[idx10] = entry.getUnicodeNumber();
        int idx12 = idx11 + 1;
        result[idx11] = toStr(entry.getFrequncyeRank());
        int idx13 = idx12 + 1;
        result[idx12] = toStr(entry.getGrade());
        int idx14 = idx13 + 1;
        result[idx13] = toStr(entry.getJlptLevel());
        int idx15 = idx14 + 1;
        result[idx14] = entry.getSkipCode();
        int idx16 = idx15 + 1;
        result[idx15] = entry.getKoreanReading();
        int i = idx16 + 1;
        result[idx16] = entry.getPinyin();
        return result;
    }

    private static String toStr(Integer i) {
        if (i == null) {
            return null;
        }
        return i.toString();
    }

    private static String[] generateDictCsv(DictionaryEntry entry, String meaningsSeparatorChar) {
        String[] result = new String[3];
        int idx = 0 + 1;
        result[0] = entry.getWord();
        int idx2 = idx + 1;
        result[idx] = entry.getReading();
        int i = idx2 + 1;
        result[idx2] = StringUtils.join(entry.getMeanings(), meaningsSeparatorChar, 0);
        return result;
    }

    public static WwwjdicEntry fromStringArray(String[] record) {
        int type = Integer.parseInt(record[0]);
        switch (type) {
            case 0:
                if (record.length == 5) {
                    return DictionaryEntry.parseEdict(record[2], record[4]);
                }
                return DictionaryEntry.parseEdict(record[2], "1");
            case 1:
                return KanjiEntry.parseKanjidic(record[2]);
            default:
                throw new IllegalArgumentException("Uknown entry type " + type);
        }
    }
}
