package org.nick.wwwjdic.history;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;
import org.nick.wwwjdic.R;
import org.nick.wwwjdic.Radical;
import org.nick.wwwjdic.Radicals;
import org.nick.wwwjdic.SearchCriteria;
import org.nick.wwwjdic.utils.StringUtils;

public class HistoryItem extends LinearLayout {
    private TextView criteriaDetailsText = ((TextView) findViewById(R.id.criteria_details));
    private TextView searchKeyText = ((TextView) findViewById(R.id.search_key));
    private TextView searchTypeText = ((TextView) findViewById(R.id.search_type));

    HistoryItem(Context context) {
        super(context);
        LayoutInflater.from(context).inflate((int) R.layout.search_history_item, this);
    }

    public void populate(SearchCriteria criteria) {
        Integer radicalNumber;
        if (criteria.getType() == 0) {
            this.searchTypeText.setText((int) R.string.hiragana_a);
        } else if (criteria.getType() == 1) {
            this.searchTypeText.setText((int) R.string.kanji_kan);
        } else {
            this.searchTypeText.setText((int) R.string.kanji_bun);
        }
        String searchKey = criteria.getQueryString();
        if (criteria.isKanjiRadicalLookup() && (radicalNumber = tryParseInt(searchKey)) != null) {
            Radical radical = Radicals.getInstance().getRadicalByNumber(radicalNumber.intValue());
            searchKey = radical == null ? "" : String.valueOf(radical.getGlyph()) + " (" + radical.getNumber() + ")";
        }
        this.searchKeyText.setText(searchKey);
        String detailStr = buildDetailString(criteria);
        if (detailStr != null && !"".equals(detailStr)) {
            this.criteriaDetailsText.setText(detailStr);
        }
    }

    private Integer tryParseInt(String str) {
        try {
            return Integer.valueOf(Integer.parseInt(str));
        } catch (NumberFormatException e) {
            return null;
        }
    }

    private String buildDetailString(SearchCriteria criteria) {
        StringBuffer buff = new StringBuffer();
        if (criteria.getType() == 1) {
            buff.append(HistoryUtils.lookupKanjiSearchName(criteria.getKanjiSearchType(), criteria.getQueryString(), getContext()));
            if (criteria.hasStrokes()) {
                buff.append(String.format(" (%s: ", getStr(R.string.strokes_short)));
                if (criteria.hasMinStrokes()) {
                    buff.append(criteria.getMinStrokeCount());
                }
                buff.append("-");
                if (criteria.hasMaxStrokes()) {
                    buff.append(criteria.getMaxStrokeCount());
                }
                buff.append(")");
            }
        } else {
            if (criteria.getType() == 0) {
                buff.append(HistoryUtils.lookupDictionaryName(criteria.getDictionaryCode(), getContext()));
            }
            String dictOptStr = buildSearchOptionsString(criteria);
            if (!StringUtils.isEmpty(dictOptStr)) {
                buff.append(" ");
                buff.append(dictOptStr);
            }
        }
        String result = buff.toString();
        if (!StringUtils.isEmpty(result)) {
            return result.trim();
        }
        return result;
    }

    private String getStr(int id) {
        return getResources().getString(id);
    }

    private String buildSearchOptionsString(SearchCriteria criteria) {
        StringBuffer buff = new StringBuffer();
        if (criteria.isCommonWordsOnly()) {
            buff.append(String.format(" %s", getStr(R.string.common_short)));
        }
        if (criteria.isExactMatch()) {
            buff.append(String.format(" %s", getStr(R.string.exact_short)));
        }
        if (criteria.isRomanizedJapanese()) {
            buff.append(String.format(" %s", getStr(R.string.romanized_short)));
        }
        if (criteria.getType() == 2) {
            buff.append(String.format(" " + getStr(R.string.max_results_short), criteria.getNumMaxResults()));
        }
        String result = buff.toString().trim();
        if (!StringUtils.isEmpty(result)) {
            return "(" + result + ")";
        }
        return result;
    }
}
