package org.nick.wwwjdic.history;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import org.nick.wwwjdic.Constants;
import org.nick.wwwjdic.R;

public class FavoritesAndHistorySummaryView extends ListView implements AdapterView.OnItemClickListener {
    private static final int FAVORITES_ITEM_IDX = 0;
    private static final int FAVORITES_TAB_IDX = 0;
    private static final int HISTORY_ITEM_IDX = 1;
    private static final int HISTORY_TAB_IDX = 1;
    private HistoryFavoritesSummaryAdapter adapter;
    private Context context;
    private int favoritesFilterType;
    private int historyFilterType;

    public FavoritesAndHistorySummaryView(Context context2) {
        super(context2);
        init(context2);
    }

    public FavoritesAndHistorySummaryView(Context context2, AttributeSet attrs) {
        super(context2, attrs);
        init(context2);
    }

    private void init(Context context2) {
        this.context = context2;
        setOnItemClickListener(this);
        this.adapter = new HistoryFavoritesSummaryAdapter(context2);
        setAdapter((ListAdapter) this.adapter);
    }

    public void setRecentEntries(long numAllFavorites, List<String> recentFavorites, long numAllHistoryItems, List<String> recentHistory) {
        this.adapter.setRecentEntries(numAllFavorites, recentFavorites, numAllHistoryItems, recentHistory);
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        boolean isFavoritesTab;
        int tabIdx;
        int filterType;
        if (position == 0) {
            isFavoritesTab = true;
        } else {
            isFavoritesTab = false;
        }
        if (isFavoritesTab) {
            tabIdx = 0;
        } else {
            tabIdx = 1;
        }
        if (isFavoritesTab) {
            filterType = this.favoritesFilterType;
        } else {
            filterType = this.historyFilterType;
        }
        if (getAdapter().getCount() == 1) {
            tabIdx = 1;
            filterType = this.historyFilterType;
        }
        Intent intent = new Intent(this.context, FavoritesAndHistory.class);
        intent.putExtra(Constants.FAVORITES_HISTORY_SELECTED_TAB_IDX, tabIdx);
        intent.putExtra(Constants.FILTER_TYPE, filterType);
        this.context.startActivity(intent);
    }

    public int getFavoritesFilterType() {
        return this.favoritesFilterType;
    }

    public void setFavoritesFilterType(int favoritesFilterType2) {
        this.favoritesFilterType = favoritesFilterType2;
    }

    public int getHistoryFilterType() {
        return this.historyFilterType;
    }

    public void setHistoryFilterType(int historyFilterType2) {
        this.historyFilterType = historyFilterType2;
    }

    static class HistoryFavoritesSummaryAdapter extends BaseAdapter {
        private final Context context;
        private long numAllFavorites;
        private long numAllHistoryItems;
        private List<String> recentFavorites;
        private List<String> recentHistory;

        public HistoryFavoritesSummaryAdapter(Context context2) {
            this.context = context2;
            this.numAllFavorites = 0;
            this.recentFavorites = new ArrayList();
            this.numAllHistoryItems = 0;
            this.recentHistory = new ArrayList();
        }

        public HistoryFavoritesSummaryAdapter(Context context2, List<String> recentFavorites2, List<String> recentHistory2) {
            this.context = context2;
            this.numAllFavorites = 0;
            this.recentFavorites = recentFavorites2;
            this.numAllHistoryItems = 0;
            this.recentHistory = recentHistory2;
        }

        public void setRecentEntries(long numAllFavorites2, List<String> recentFavorites2, long numAllHistoryItems2, List<String> recentHistory2) {
            this.numAllFavorites = numAllFavorites2;
            this.recentFavorites = recentFavorites2;
            this.numAllHistoryItems = numAllHistoryItems2;
            this.recentHistory = recentHistory2;
            notifyDataSetChanged();
        }

        public int getCount() {
            if (this.recentFavorites == null) {
                return 1;
            }
            return 2;
        }

        public Object getItem(int position) {
            switch (position) {
                case 0:
                    if (this.recentFavorites == null) {
                        return "history";
                    }
                    return "favorites";
                case 1:
                    return "history";
                default:
                    throw new IllegalArgumentException("Invalid position: " + position);
            }
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            List<String> recent;
            long numAllEntries;
            boolean isFavoritesEntry = position == 0;
            if (isFavoritesEntry) {
                recent = this.recentFavorites;
            } else {
                recent = this.recentHistory;
            }
            if (isFavoritesEntry) {
                numAllEntries = this.numAllFavorites;
            } else {
                numAllEntries = this.numAllHistoryItems;
            }
            if (this.recentFavorites == null) {
                isFavoritesEntry = false;
                recent = this.recentHistory;
                numAllEntries = this.numAllHistoryItems;
            }
            if (convertView == null) {
                convertView = new SummaryView(this.context);
            }
            ((SummaryView) convertView).populate(numAllEntries, recent, isFavoritesEntry);
            return convertView;
        }

        static class SummaryView extends LinearLayout {
            private TextView itemList = ((TextView) findViewById(R.id.item_list));
            private TextView summary = ((TextView) findViewById(R.id.summary));

            SummaryView(Context context) {
                super(context);
                LayoutInflater.from(context).inflate((int) R.layout.favorites_history_summary_item, this);
            }

            /* access modifiers changed from: package-private */
            public void populate(long numAllEntries, List<String> recentEntries, boolean isFavoritesItem) {
                if (isFavoritesItem) {
                    this.summary.setText(String.format(getResources().getString(R.string.favorties_summary), Long.valueOf(numAllEntries)));
                } else {
                    this.summary.setText(String.format(getResources().getString(R.string.history_summary), Long.valueOf(numAllEntries)));
                }
                String itemsAsStr = TextUtils.join(", ", recentEntries);
                if (numAllEntries > ((long) recentEntries.size())) {
                    itemsAsStr = String.valueOf(itemsAsStr) + "...";
                }
                this.itemList.setText(itemsAsStr);
            }
        }
    }
}
