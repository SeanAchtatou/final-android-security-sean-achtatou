package org.nick.wwwjdic.history;

import org.nick.wwwjdic.SearchCriteria;
import org.nick.wwwjdic.utils.StringUtils;

public class SearchCriteriaParser {
    public static final int COMMON_WORDS_ONLY_IDX = 5;
    public static final int DICTIONARY_IDX = 6;
    public static final int EXACT_MATCH_IDX = 2;
    public static final int KANJI_LOOKUP_IDX = 3;
    public static final int KANJI_SEARCH_TYPE_IDX = 7;
    public static final int MAX_STROKE_COUNT_IDX = 9;
    public static final int MIN_STROKE_COUNT_IDX = 8;
    private static final int NUM_FILEDS = 12;
    public static final int NUM_MAX_RESULTS_IDX = 10;
    public static final int QUERY_STRING_IDX = 1;
    public static final int ROMANIZED_JAP_IDX = 4;
    public static final int TIME_IDX = 11;
    public static final int TYPE_IDX = 0;

    private SearchCriteriaParser() {
    }

    public static String[] toStringArray(SearchCriteria criteria, long time) {
        String num;
        String[] result = new String[NUM_FILEDS];
        result[0] = Integer.toString(criteria.getType());
        result[1] = criteria.getQueryString();
        result[2] = toTfInt(criteria.isExactMatch());
        result[3] = toTfInt(criteria.isKanjiLookup());
        result[4] = toTfInt(criteria.isRomanizedJapanese());
        result[5] = toTfInt(criteria.isCommonWordsOnly());
        result[6] = criteria.getDictionaryCode();
        result[7] = criteria.getKanjiSearchType();
        result[8] = toIntStr(criteria.getMinStrokeCount());
        result[9] = toIntStr(criteria.getMaxStrokeCount());
        if (criteria.getNumMaxResults() == null) {
            num = null;
        } else {
            num = Integer.toString(criteria.getNumMaxResults().intValue());
        }
        result[10] = num;
        result[11] = Long.toString(time);
        return result;
    }

    private static String toIntStr(Integer i) {
        if (i == null) {
            return null;
        }
        return i.toString();
    }

    private static String toTfInt(boolean b) {
        return b ? "1" : "0";
    }

    public static SearchCriteria fromStringArray(String[] record) {
        Integer minStrokes;
        Integer maxStrokes;
        int type = Integer.parseInt(record[0]);
        switch (type) {
            case 0:
                return SearchCriteria.createForDictionary(record[1], parseTfStr(record[2]), parseTfStr(record[4]), parseTfStr(record[5]), record[6]);
            case 1:
                if (StringUtils.isEmpty(record[8])) {
                    minStrokes = null;
                } else {
                    minStrokes = Integer.valueOf(Integer.parseInt(record[8]));
                }
                if (StringUtils.isEmpty(record[9])) {
                    maxStrokes = null;
                } else {
                    maxStrokes = Integer.valueOf(Integer.parseInt(record[9]));
                }
                return SearchCriteria.createWithStrokeCount(record[1], record[7], minStrokes, maxStrokes);
            case 2:
                return SearchCriteria.createForExampleSearch(record[1], parseTfStr(record[2]), Integer.parseInt(record[10]));
            default:
                throw new IllegalArgumentException("Unknown criteria type: " + type);
        }
    }

    private static boolean parseTfStr(String str) {
        if ("1".equals(str)) {
            return true;
        }
        return false;
    }
}
