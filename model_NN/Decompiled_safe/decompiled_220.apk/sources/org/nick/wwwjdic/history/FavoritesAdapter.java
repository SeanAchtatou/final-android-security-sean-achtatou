package org.nick.wwwjdic.history;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import org.nick.wwwjdic.history.FavoritesItem;

class FavoritesAdapter extends CursorAdapter {
    private FavoritesItem.FavoriteStatusChangedListener favoriteStatusChanged;

    public FavoritesAdapter(Context context, Cursor c, FavoritesItem.FavoriteStatusChangedListener statusChangedListener) {
        super(context, c);
        this.favoriteStatusChanged = statusChangedListener;
    }

    public void bindView(View view, Context context, Cursor cursor) {
        ((FavoritesItem) view).populate(HistoryDbHelper.createWwwjdicEntry(cursor));
    }

    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return new FavoritesItem(context, this.favoriteStatusChanged);
    }
}
