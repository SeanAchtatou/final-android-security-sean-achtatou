package org.nick.wwwjdic.history;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;
import org.nick.wwwjdic.Constants;
import org.nick.wwwjdic.R;

public class FavoritesAndHistory extends TabActivity implements TabHost.OnTabChangeListener {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(5);
        setContentView((int) R.layout.favorites_history);
        Intent intent = getIntent();
        setupTabs(intent.getIntExtra(Constants.FAVORITES_HISTORY_SELECTED_TAB_IDX, 0), intent.getIntExtra(Constants.FILTER_TYPE, -1));
    }

    private void setupTabs(int tabIdx, int filterType) {
        TabHost tabHost = getTabHost();
        Intent favoritesIntent = new Intent(this, Favorites.class);
        if (tabIdx == 0) {
            favoritesIntent.putExtra(Constants.FILTER_TYPE, filterType);
        }
        tabHost.addTab(tabHost.newTabSpec("favorites").setIndicator(getResources().getString(R.string.favorites), getResources().getDrawable(R.drawable.ic_tab_favorites)).setContent(favoritesIntent));
        Intent historyIntent = new Intent(this, SearchHistory.class);
        if (tabIdx == 1) {
            historyIntent.putExtra(Constants.FILTER_TYPE, filterType);
        }
        tabHost.addTab(tabHost.newTabSpec("history").setIndicator(getResources().getString(R.string.search_history), getResources().getDrawable(R.drawable.ic_tab_history)).setContent(historyIntent));
        tabHost.setOnTabChangedListener(this);
        tabHost.setCurrentTab(tabIdx);
    }

    public void onTabChanged(String tabId) {
        HistoryBase history = (HistoryBase) getLocalActivityManager().getActivity(tabId);
        if (history != null) {
            history.refresh();
        }
    }
}
