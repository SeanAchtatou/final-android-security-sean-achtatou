package org.nick.wwwjdic.history;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import org.nick.wwwjdic.R;
import org.nick.wwwjdic.WwwjdicEntry;

public class FavoritesItem extends LinearLayout implements CompoundButton.OnCheckedChangeListener {
    private TextView dictHeadingText = ((TextView) findViewById(R.id.dict_heading));
    private WwwjdicEntry entry;
    private TextView entryDetailsText = ((TextView) findViewById(R.id.entry_details));
    private FavoriteStatusChangedListener favoriteStatusChangedListener;
    private TextView isKanjiText = ((TextView) findViewById(R.id.is_kanji));
    private CheckBox starCb = ((CheckBox) findViewById(R.id.star));

    interface FavoriteStatusChangedListener {
        void onStatusChanged(boolean z, WwwjdicEntry wwwjdicEntry);
    }

    FavoritesItem(Context context, FavoriteStatusChangedListener statusChangedListener) {
        super(context);
        this.favoriteStatusChangedListener = statusChangedListener;
        LayoutInflater.from(context).inflate((int) R.layout.favorites_item, this);
        this.starCb.setOnCheckedChangeListener(this);
    }

    public void populate(WwwjdicEntry entry2) {
        int i;
        this.entry = entry2;
        TextView textView = this.isKanjiText;
        if (entry2.isKanji()) {
            i = R.string.kanji_kan;
        } else {
            i = R.string.hiragana_a;
        }
        textView.setText(i);
        this.dictHeadingText.setText(entry2.getHeadword());
        String detailStr = entry2.getDetailString();
        if (detailStr != null && !"".equals(detailStr)) {
            this.entryDetailsText.setText(detailStr);
        }
        this.starCb.setOnCheckedChangeListener(null);
        this.starCb.setChecked(true);
        this.starCb.setOnCheckedChangeListener(this);
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        this.favoriteStatusChangedListener.onStatusChanged(isChecked, this.entry);
    }
}
