package org.nick.wwwjdic.history;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;
import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import com.google.api.client.apache.ApacheHttpTransport;
import com.google.api.client.googleapis.GoogleHeaders;
import com.google.api.client.googleapis.GoogleTransport;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpResponseException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.InputStreamContent;
import com.google.api.client.xml.atom.AtomParser;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.nick.wwwjdic.Constants;
import org.nick.wwwjdic.DictionaryEntry;
import org.nick.wwwjdic.DictionaryEntryDetail;
import org.nick.wwwjdic.KanjiEntry;
import org.nick.wwwjdic.KanjiEntryDetail;
import org.nick.wwwjdic.R;
import org.nick.wwwjdic.WwwjdicApplication;
import org.nick.wwwjdic.WwwjdicEntry;
import org.nick.wwwjdic.WwwjdicPreferences;
import org.nick.wwwjdic.history.FavoritesItem;
import org.nick.wwwjdic.history.gdocs.DocsUrl;
import org.nick.wwwjdic.history.gdocs.Namespace;
import org.nick.wwwjdic.utils.Analytics;
import org.nick.wwwjdic.utils.Dialogs;

public class Favorites extends HistoryBase implements FavoritesItem.FavoriteStatusChangedListener {
    private static final int ACCOUNTS_DIALOG_ID = 1;
    private static final String AUTH_TOKEN_TYPE = "writely";
    private static final String CSV_EXPORT_FILENAME_EXT = "csv";
    private static final String DICT_CSV_EXPORT_FILENAME_BASE = "wwwjdic-favorites-dict";
    private static final int ECLAIR_VERSION_CODE = 5;
    private static final int EXPORT_ANKI_IDX = 3;
    private static final String EXPORT_FILENAME = "wwwjdic/favorites.csv";
    private static final int EXPORT_GDOCS_IDX = 2;
    private static final int EXPORT_LOCAL_BACKUP_IDX = 0;
    private static final int EXPORT_LOCAL_EXPORT_IDX = 1;
    private static final String FAVORITES_EXPORT_TIP_DIALOG = "tips_favorites_export";
    private static final String GDATA_VERSION = "3";
    private static final String KANJI_CSV_EXPORT_FILENAME_BASE = "wwwjdic-favorites-kanji";
    private static final int REQUEST_AUTHENTICATE = 0;
    /* access modifiers changed from: private */
    public static final String TAG = Favorites.class.getSimpleName();
    private String authToken;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog;
    /* access modifiers changed from: private */
    public HttpTransport transport;
    /* access modifiers changed from: private */
    public UploadData uploadData;

    public Favorites() {
        if (isPostEclair()) {
            initGdocsTransport();
        }
    }

    /* access modifiers changed from: private */
    public static boolean isPostEclair() {
        return Integer.parseInt(Build.VERSION.SDK) >= 5;
    }

    private void initGdocsTransport() {
        HttpTransport.setLowLevelHttpTransport(ApacheHttpTransport.INSTANCE);
        this.transport = GoogleTransport.create();
        GoogleHeaders headers = (GoogleHeaders) this.transport.defaultHeaders;
        headers.setApplicationName(WwwjdicApplication.getUserAgentString());
        headers.gdataVersion = GDATA_VERSION;
        AtomParser parser = new AtomParser();
        parser.namespaceDictionary = Namespace.DICTIONARY;
        this.transport.addParser(parser);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Dialogs.showTipOnce(this, FAVORITES_EXPORT_TIP_DIALOG, R.string.tips_favorites_export);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                return super.onCreateDialog(id);
            case 1:
                final AccountManagerWrapper manager = AccountManagerWrapper.getInstance(this);
                final String[] accountNames = manager.getGoogleAccounts();
                int size = accountNames.length;
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                if (size == 0) {
                    Toast.makeText(this, (int) R.string.no_google_accounts, 1).show();
                    return null;
                }
                builder.setTitle((int) R.string.select_google_account);
                builder.setItems(accountNames, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Favorites.this.gotAccount(manager, accountNames[which]);
                    }
                });
                return builder.create();
            default:
                return null;
        }
    }

    /* access modifiers changed from: private */
    public void gotAccount(boolean tokenExpired) {
        String accountName = WwwjdicPreferences.getGoogleAcountName(this);
        AccountManagerWrapper manager = AccountManagerWrapper.getInstance(this);
        String[] accountNames = manager.getGoogleAccounts();
        if (accountName != null) {
            int size = accountNames.length;
            for (int i = 0; i < size; i++) {
                if (accountName.equals(accountNames[i])) {
                    if (tokenExpired) {
                        manager.invalidateAuthToken(this.authToken);
                    }
                    gotAccount(manager, accountNames[i]);
                    return;
                }
            }
        }
        if (accountNames.length != 0) {
            showDialog(1);
            return;
        }
        if (this.uploadData != null) {
            if (this.uploadData.localFilename != null) {
                new File(this.uploadData.localFilename).delete();
            }
            this.uploadData = null;
        }
        Log.w(TAG, "No suitable Google accounts found");
        Toast.makeText(this, (int) R.string.no_google_accounts, 1).show();
    }

    /* access modifiers changed from: private */
    public void gotAccount(final AccountManagerWrapper manager, final String accountName) {
        WwwjdicPreferences.setGoogleAccountName(this, accountName);
        new Thread() {
            public void run() {
                try {
                    final Bundle bundle = manager.getAuthToken(accountName, Favorites.AUTH_TOKEN_TYPE);
                    Favorites.this.runOnUiThread(new Runnable() {
                        public void run() {
                            try {
                                if (bundle.containsKey(AccountManagerWrapper.KEY_INTENT)) {
                                    Intent intent = (Intent) bundle.getParcelable(AccountManagerWrapper.KEY_INTENT);
                                    intent.setFlags(intent.getFlags() & -268435457);
                                    Favorites.this.startActivityForResult(intent, 0);
                                } else if (bundle.containsKey(AccountManagerWrapper.KEY_AUTHTOKEN)) {
                                    Favorites.this.authenticatedClientLogin(bundle.getString(AccountManagerWrapper.KEY_AUTHTOKEN));
                                }
                            } catch (Exception e) {
                                Favorites.this.handleException(e);
                            }
                        }
                    });
                } catch (Exception e) {
                    Favorites.this.handleException(e);
                }
            }
        }.start();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 0:
                if (resultCode == -1) {
                    gotAccount(false);
                    return;
                } else {
                    showDialog(1);
                    return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void authenticatedClientLogin(String authToken2) {
        this.authToken = authToken2;
        ((GoogleHeaders) this.transport.defaultHeaders).setGoogleLogin(authToken2);
        authenticated();
    }

    static class UploadData {
        long contentLength;
        String contentType;
        String filename;
        String localFilename;

        UploadData() {
        }
    }

    private class GDocsExportTask extends AsyncTask<Void, Object, Boolean> {
        private Throwable error;
        private boolean tokenExpired = false;

        GDocsExportTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            if (Favorites.this.progressDialog != null && Favorites.this.progressDialog.isShowing()) {
                Favorites.this.progressDialog.dismiss();
            }
            Favorites.this.progressDialog = new ProgressDialog(Favorites.this);
            Favorites.this.progressDialog.setProgressStyle(0);
            Favorites.this.progressDialog.setMessage(Favorites.this.getString(R.string.uploading_to_gdocs));
            Favorites.this.progressDialog.setCancelable(true);
            Favorites.this.progressDialog.setButton(-3, Favorites.this.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    GDocsExportTask.this.cancel(true);
                }
            });
            Favorites.this.progressDialog.show();
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(Void... params) {
            try {
                HttpRequest request = Favorites.this.transport.buildPostRequest();
                request.url = DocsUrl.forDefaultPrivateFull();
                ((GoogleHeaders) request.headers).setSlugFromFileName(Favorites.this.uploadData.filename);
                InputStreamContent content = new InputStreamContent();
                content.inputStream = new FileInputStream(Favorites.this.uploadData.localFilename);
                content.type = Favorites.this.uploadData.contentType;
                content.length = Favorites.this.uploadData.contentLength;
                request.content = content;
                request.execute().ignore();
                deleteTempFile();
                Analytics.event("favoritesGDocsExport", Favorites.this);
                return true;
            } catch (HttpResponseException e) {
                HttpResponseException hre = e;
                Log.d(Favorites.TAG, "Error uploading to Google docs", hre);
                HttpResponse response = hre.response;
                int statusCode = response.statusCode;
                try {
                    response.ignore();
                    Log.e(Favorites.TAG, response.parseAsString());
                } catch (IOException e2) {
                    Log.w(Favorites.TAG, "error parsing response", e2);
                }
                if (statusCode == 401 || statusCode == 403) {
                    this.tokenExpired = true;
                }
                return false;
            } catch (IOException e3) {
                IOException e4 = e3;
                this.error = e4;
                Log.d(Favorites.TAG, "Error uploading to Google docs", e4);
                deleteTempFile();
                return false;
            }
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            super.onCancelled();
            deleteTempFile();
        }

        private void deleteTempFile() {
            Log.d(Favorites.TAG, "deleting temp files...");
            File f = new File(Favorites.this.uploadData.localFilename);
            if (f.delete()) {
                Log.d(Favorites.TAG, "successfully deleted " + f.getAbsolutePath());
            } else {
                Log.d(Favorites.TAG, "failed to delet " + f.getAbsolutePath());
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean result) {
            String template;
            String message;
            if (Favorites.this.progressDialog != null && Favorites.this.progressDialog.isShowing()) {
                Favorites.this.progressDialog.dismiss();
            }
            if (this.tokenExpired) {
                Favorites.this.gotAccount(true);
                return;
            }
            Resources r = Favorites.this.getResources();
            if (result.booleanValue()) {
                template = r.getString(R.string.gdocs_upload_success);
            } else {
                template = r.getString(R.string.gdocs_upload_failure);
            }
            if (result.booleanValue()) {
                message = String.format(template, Favorites.this.uploadData.filename);
            } else {
                message = String.format(template, this.error.getMessage());
            }
            Toast t = Toast.makeText(Favorites.this, message, 1);
            Favorites.this.uploadData = null;
            t.show();
        }
    }

    private void authenticated() {
        if (this.uploadData != null && this.uploadData.filename != null) {
            new GDocsExportTask().execute(new Void[0]);
        }
    }

    /* access modifiers changed from: private */
    public void handleException(Exception e) {
        Log.e(TAG, e.getMessage(), e);
        if (e instanceof HttpResponseException) {
            HttpResponse response = ((HttpResponseException) e).response;
            int statusCode = response.statusCode;
            try {
                response.ignore();
            } catch (IOException e1) {
                Log.e(TAG, e.getMessage(), e1);
            }
            if (statusCode == 401 || statusCode == 403) {
                gotAccount(true);
                return;
            }
            try {
                Log.e(TAG, response.parseAsString());
            } catch (IOException e2) {
                Log.w(TAG, e.getMessage(), e2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void setupAdapter() {
        MatrixCursor cursor = new MatrixCursor(HistoryDbHelper.FAVORITES_ALL_COLUMNS, 0);
        startManagingCursor(cursor);
        setListAdapter(new FavoritesAdapter(this, cursor, this));
        new AsyncTask<Void, Void, Cursor>() {
            /* access modifiers changed from: protected */
            public void onPreExecute() {
                Favorites.this.getParent().setProgressBarIndeterminateVisibility(true);
            }

            /* access modifiers changed from: protected */
            public Cursor doInBackground(Void... arg0) {
                return Favorites.this.filterCursor();
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(Cursor cursor) {
                Favorites.this.resetAdapter(cursor);
                Favorites.this.getParent().setProgressBarIndeterminateVisibility(false);
            }
        }.execute(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public void resetAdapter(Cursor cursor) {
        startManagingCursor(cursor);
        setListAdapter(new FavoritesAdapter(this, cursor, this));
    }

    /* access modifiers changed from: protected */
    public void deleteAll() {
        new AsyncTask<Void, Void, Void>() {
            /* access modifiers changed from: protected */
            public void onPreExecute() {
                Favorites.this.getParent().setProgressBarIndeterminateVisibility(true);
            }

            /* JADX INFO: finally extract failed */
            /* access modifiers changed from: protected */
            public Void doInBackground(Void... arg0) {
                Cursor c = Favorites.this.filterCursor();
                Favorites.this.db.beginTransaction();
                while (c.moveToNext()) {
                    try {
                        Favorites.this.db.deleteFavorite((long) c.getInt(c.getColumnIndex("_id")));
                    } catch (Throwable th) {
                        Favorites.this.db.endTransaction();
                        throw th;
                    }
                }
                Favorites.this.db.setTransactionSuccessful();
                Favorites.this.db.endTransaction();
                return null;
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(Void v) {
                Favorites.this.refresh();
                Favorites.this.getParent().setProgressBarIndeterminateVisibility(false);
            }
        }.execute(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.favorites;
    }

    /* access modifiers changed from: protected */
    public void deleteCurrentItem() {
        Cursor c = getCursor();
        this.db.deleteFavorite((long) c.getInt(c.getColumnIndex("_id")));
        refresh();
    }

    public void onStatusChanged(boolean isFavorite, WwwjdicEntry entry) {
        if (isFavorite) {
            this.db.addFavorite(entry);
            refresh();
            return;
        }
        this.db.deleteFavorite(entry.getId().longValue());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: protected */
    public void lookupCurrentItem() {
        Intent intent;
        WwwjdicEntry entry = getCurrentEntry();
        if (entry.isKanji()) {
            intent = new Intent(this, KanjiEntryDetail.class);
            intent.putExtra(Constants.KANJI_ENTRY_KEY, entry);
            intent.putExtra(Constants.IS_FAVORITE, true);
        } else {
            intent = new Intent(this, DictionaryEntryDetail.class);
            intent.putExtra(Constants.ENTRY_KEY, entry);
            intent.putExtra(Constants.IS_FAVORITE, true);
        }
        Analytics.event("lookupFromFavorites", this);
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void copyCurrentItem() {
        WwwjdicEntry entry = getCurrentEntry();
        this.clipboardManager.setText(entry.getHeadword());
        showCopiedToast(entry.getHeadword());
    }

    private WwwjdicEntry getCurrentEntry() {
        return HistoryDbHelper.createWwwjdicEntry(getCursor());
    }

    /* access modifiers changed from: protected */
    public String getImportExportFilename() {
        return String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + "/" + EXPORT_FILENAME;
    }

    /* access modifiers changed from: protected */
    public void exportItems() {
        boolean singleType;
        final boolean isKanji;
        String[] items = getResources().getStringArray(R.array.favorites_export_dialog_items);
        if (this.selectedFilter != -1) {
            singleType = true;
        } else {
            singleType = false;
        }
        if (this.selectedFilter == 1) {
            isKanji = true;
        } else {
            isKanji = false;
        }
        ExportItemsAdapter adapter = new ExportItemsAdapter(this, items, singleType);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.favorites_export_dialog_title);
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        Favorites.super.exportItems();
                        return;
                    case 1:
                        Favorites.this.exportLocalCsv(isKanji);
                        return;
                    case 2:
                        Favorites.this.exportToGDocs(isKanji);
                        return;
                    case 3:
                        Favorites.this.exportToAnkiDeckAsync(isKanji);
                        return;
                    default:
                        return;
                }
            }
        });
        builder.create().show();
    }

    /* access modifiers changed from: private */
    public void exportToAnkiDeckAsync(boolean isKanji) {
        new AnkiExportTask().execute(Boolean.valueOf(isKanji));
    }

    private class AnkiExportTask extends AsyncTask<Boolean, Object, Boolean> {
        private Throwable error;
        private String exportFilename;

        AnkiExportTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            if (Favorites.this.progressDialog != null && Favorites.this.progressDialog.isShowing()) {
                Favorites.this.progressDialog.dismiss();
            }
            Favorites.this.progressDialog = new ProgressDialog(Favorites.this);
            Favorites.this.progressDialog.setProgressStyle(0);
            Favorites.this.progressDialog.setMessage(Favorites.this.getString(R.string.exporting_to_anki));
            Favorites.this.progressDialog.setCancelable(true);
            Favorites.this.progressDialog.setButton(-3, Favorites.this.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    AnkiExportTask.this.cancel(true);
                }
            });
            Favorites.this.progressDialog.show();
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(Boolean... params) {
            try {
                this.exportFilename = Favorites.this.exportToAnkiDeck(params[0].booleanValue());
                return true;
            } catch (Exception e) {
                Exception e2 = e;
                this.error = e2;
                Log.d(Favorites.TAG, "Error exporting favorites to Anki", e2);
                deleteIncompleteFile();
                return false;
            }
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            super.onCancelled();
            deleteIncompleteFile();
        }

        private void deleteIncompleteFile() {
            Log.d(Favorites.TAG, "Anki export cancelled, deleting incomplete files...");
            if (this.exportFilename != null) {
                File f = new File(this.exportFilename);
                if (f.delete()) {
                    Log.d(Favorites.TAG, "successfully deleted " + f.getAbsolutePath());
                } else {
                    Log.d(Favorites.TAG, "failed to delet " + f.getAbsolutePath());
                }
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean result) {
            String template;
            String message;
            if (Favorites.this.progressDialog != null && Favorites.this.progressDialog.isShowing()) {
                Favorites.this.progressDialog.dismiss();
            }
            Resources r = Favorites.this.getResources();
            if (result.booleanValue()) {
                template = r.getString(R.string.anki_export_success);
            } else {
                template = r.getString(R.string.anki_export_failure);
            }
            if (result.booleanValue()) {
                message = String.format(template, this.exportFilename);
            } else {
                message = String.format(template, this.error.getMessage());
            }
            Toast.makeText(Favorites.this, message, 1).show();
        }
    }

    /* access modifiers changed from: private */
    public String exportToAnkiDeck(boolean isKanji) {
        int size;
        AnkiGenerator generator = new AnkiGenerator(this);
        File exportFile = new File(WwwjdicApplication.getWwwjdicDir(), String.valueOf(getCsvExportFilename(isKanji).replace(".csv", "")) + ".anki");
        Log.d(TAG, "exporting favorites to Anki: " + exportFile.getAbsolutePath());
        if (isKanji) {
            List<KanjiEntry> kanjis = new ArrayList<>();
            Cursor c = null;
            try {
                c = filterCursor();
                while (c.moveToNext()) {
                    kanjis.add((KanjiEntry) HistoryDbHelper.createWwwjdicEntry(c));
                }
                size = generator.createKanjiAnkiFile(exportFile.getAbsolutePath(), kanjis);
            } finally {
                if (c != null) {
                    c.close();
                }
            }
        } else {
            List<DictionaryEntry> words = new ArrayList<>();
            Cursor c2 = null;
            try {
                c2 = filterCursor();
                while (c2.moveToNext()) {
                    words.add((DictionaryEntry) HistoryDbHelper.createWwwjdicEntry(c2));
                }
                size = generator.createDictAnkiFile(exportFile.getAbsolutePath(), words);
            } finally {
                if (c2 != null) {
                    c2.close();
                }
            }
        }
        Analytics.event("favoritesAnkiExport", this);
        Log.d(TAG, String.format("Exported %d entries to %s", Integer.valueOf(size), exportFile.getAbsolutePath()));
        return exportFile.getAbsolutePath();
    }

    private static class ExportItemsAdapter extends ArrayAdapter<String> {
        private boolean isPostEclair = Favorites.isPostEclair();
        private boolean singleType;

        ExportItemsAdapter(Context context, String[] items, boolean singleType2) {
            super(context, 17367057, 16908308, items);
            this.singleType = singleType2;
        }

        public boolean areAllItemsEnabled() {
            return false;
        }

        public boolean isEnabled(int idx) {
            switch (idx) {
                case 0:
                    return true;
                case 1:
                    return this.singleType;
                case 2:
                    return this.singleType && this.isPostEclair;
                case 3:
                    return this.singleType;
                default:
                    return false;
            }
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View result = super.getView(position, convertView, parent);
            result.setEnabled(isEnabled(position));
            return result;
        }
    }

    /* access modifiers changed from: private */
    public void exportLocalCsv(final boolean isKanji) {
        new AsyncTask<Void, Void, Boolean>() {
            int count = 0;
            Exception exception;
            String exportFilename;

            /* access modifiers changed from: protected */
            public void onPreExecute() {
                Favorites.this.getParent().setProgressBarIndeterminateVisibility(true);
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
             arg types: [java.io.File, int]
             candidates:
              ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
              ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException} */
            /* access modifiers changed from: protected */
            public Boolean doInBackground(Void... params) {
                try {
                    File exportFile = new File(WwwjdicApplication.getWwwjdicDir(), Favorites.this.getCsvExportFilename(isKanji));
                    Favorites.this.writeBom(exportFile);
                    Writer writer = new FileWriter(exportFile, true);
                    this.exportFilename = exportFile.getAbsolutePath();
                    this.count = Favorites.this.exportToCsv(exportFile.getAbsolutePath(), writer, false);
                    Analytics.event("favoritesLocalCsvExport", Favorites.this);
                    return true;
                } catch (Exception e) {
                    Exception e2 = e;
                    Log.e(Favorites.TAG, "error exporting favorites", e2);
                    this.exception = e2;
                    return false;
                }
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(Boolean result) {
                String errMessage;
                if (result.booleanValue()) {
                    String message = Favorites.this.getResources().getString(R.string.favorites_exported);
                    Toast.makeText(Favorites.this, String.format(message, this.exportFilename, Integer.valueOf(this.count)), 0).show();
                } else {
                    String message2 = Favorites.this.getResources().getString(R.string.export_error);
                    if (this.exception == null) {
                        errMessage = "Error";
                    } else {
                        errMessage = this.exception.getMessage();
                    }
                    Toast.makeText(Favorites.this, String.format(message2, errMessage), 0).show();
                }
                Favorites.this.getParent().setProgressBarIndeterminateVisibility(false);
            }
        }.execute(new Void[0]);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00d8 A[Catch:{ all -> 0x0125 }] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x010b A[SYNTHETIC, Splitter:B:41:0x010b] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0110  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0128 A[SYNTHETIC, Splitter:B:50:0x0128] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x012d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int exportToCsv(java.lang.String r20, java.io.Writer r21, boolean r22) {
        /*
            r19 = this;
            r14 = 0
            r3 = 0
            android.database.Cursor r3 = r19.filterCursor()     // Catch:{ IOException -> 0x00c7 }
            au.com.bytecode.opencsv.CSVWriter r15 = new au.com.bytecode.opencsv.CSVWriter     // Catch:{ IOException -> 0x00c7 }
            r0 = r15
            r1 = r21
            r0.<init>(r1)     // Catch:{ IOException -> 0x00c7 }
            r0 = r19
            int r0 = r0.selectedFilter     // Catch:{ IOException -> 0x0142, all -> 0x013f }
            r16 = r0
            r17 = 1
            r0 = r16
            r1 = r17
            if (r0 != r1) goto L_0x0087
            r16 = 1
            r9 = r16
        L_0x0020:
            android.content.res.Resources r11 = r19.getResources()     // Catch:{ IOException -> 0x0142, all -> 0x013f }
            if (r9 == 0) goto L_0x008c
            r16 = 2131099711(0x7f06003f, float:1.7811783E38)
            r0 = r11
            r1 = r16
            java.lang.String[] r16 = r0.getStringArray(r1)     // Catch:{ IOException -> 0x0142, all -> 0x013f }
            r8 = r16
        L_0x0032:
            r15.writeNext(r8)     // Catch:{ IOException -> 0x0142, all -> 0x013f }
            r4 = 0
        L_0x0036:
            boolean r16 = r3.moveToNext()     // Catch:{ IOException -> 0x0142, all -> 0x013f }
            if (r16 != 0) goto L_0x0099
            r15.flush()     // Catch:{ IOException -> 0x0142, all -> 0x013f }
            r15.close()     // Catch:{ IOException -> 0x0142, all -> 0x013f }
            if (r22 == 0) goto L_0x0079
            android.content.res.Resources r16 = r19.getResources()     // Catch:{ IOException -> 0x0142, all -> 0x013f }
            r17 = 2131165334(0x7f070096, float:1.7944882E38)
            java.lang.String r10 = r16.getString(r17)     // Catch:{ IOException -> 0x0142, all -> 0x013f }
            r16 = 2
            r0 = r16
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ IOException -> 0x0142, all -> 0x013f }
            r16 = r0
            r17 = 0
            r16[r17] = r20     // Catch:{ IOException -> 0x0142, all -> 0x013f }
            r17 = 1
            java.lang.Integer r18 = java.lang.Integer.valueOf(r4)     // Catch:{ IOException -> 0x0142, all -> 0x013f }
            r16[r17] = r18     // Catch:{ IOException -> 0x0142, all -> 0x013f }
            r0 = r10
            r1 = r16
            java.lang.String r16 = java.lang.String.format(r0, r1)     // Catch:{ IOException -> 0x0142, all -> 0x013f }
            r17 = 0
            r0 = r19
            r1 = r16
            r2 = r17
            android.widget.Toast r13 = android.widget.Toast.makeText(r0, r1, r2)     // Catch:{ IOException -> 0x0142, all -> 0x013f }
            r13.show()     // Catch:{ IOException -> 0x0142, all -> 0x013f }
        L_0x0079:
            if (r15 == 0) goto L_0x007e
            r15.close()     // Catch:{ IOException -> 0x00b9 }
        L_0x007e:
            if (r3 == 0) goto L_0x0083
            r3.close()
        L_0x0083:
            r14 = r15
            r16 = r4
        L_0x0086:
            return r16
        L_0x0087:
            r16 = 0
            r9 = r16
            goto L_0x0020
        L_0x008c:
            r16 = 2131099710(0x7f06003e, float:1.781178E38)
            r0 = r11
            r1 = r16
            java.lang.String[] r16 = r0.getStringArray(r1)     // Catch:{ IOException -> 0x0142, all -> 0x013f }
            r8 = r16
            goto L_0x0032
        L_0x0099:
            org.nick.wwwjdic.WwwjdicEntry r6 = org.nick.wwwjdic.history.HistoryDbHelper.createWwwjdicEntry(r3)     // Catch:{ IOException -> 0x0142, all -> 0x013f }
            java.lang.String r12 = org.nick.wwwjdic.WwwjdicPreferences.getMeaningsSeparatorCharacter(r19)     // Catch:{ IOException -> 0x0142, all -> 0x013f }
            java.lang.String r16 = "space"
            r0 = r16
            r1 = r12
            boolean r16 = r0.equals(r1)     // Catch:{ IOException -> 0x0142, all -> 0x013f }
            if (r16 == 0) goto L_0x00ae
            java.lang.String r12 = " "
        L_0x00ae:
            java.lang.String[] r7 = org.nick.wwwjdic.history.FavoritesEntryParser.toParsedStringArray(r6, r12)     // Catch:{ IOException -> 0x0142, all -> 0x013f }
            r15.writeNext(r7)     // Catch:{ IOException -> 0x0142, all -> 0x013f }
            int r4 = r4 + 1
            goto L_0x0036
        L_0x00b9:
            r5 = move-exception
            java.lang.String r16 = org.nick.wwwjdic.history.Favorites.TAG
            java.lang.String r17 = "error closing CSV writer"
            r0 = r16
            r1 = r17
            r2 = r5
            android.util.Log.w(r0, r1, r2)
            goto L_0x007e
        L_0x00c7:
            r16 = move-exception
            r5 = r16
        L_0x00ca:
            java.lang.String r16 = org.nick.wwwjdic.history.Favorites.TAG     // Catch:{ all -> 0x0125 }
            java.lang.String r17 = "error exporting to CSV"
            r0 = r16
            r1 = r17
            r2 = r5
            android.util.Log.d(r0, r1, r2)     // Catch:{ all -> 0x0125 }
            if (r22 == 0) goto L_0x0109
            android.content.res.Resources r16 = r19.getResources()     // Catch:{ all -> 0x0125 }
            r17 = 2131165343(0x7f07009f, float:1.79449E38)
            java.lang.String r10 = r16.getString(r17)     // Catch:{ all -> 0x0125 }
            r16 = 1
            r0 = r16
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ all -> 0x0125 }
            r16 = r0
            r17 = 0
            java.lang.String r18 = r5.getMessage()     // Catch:{ all -> 0x0125 }
            r16[r17] = r18     // Catch:{ all -> 0x0125 }
            r0 = r10
            r1 = r16
            java.lang.String r16 = java.lang.String.format(r0, r1)     // Catch:{ all -> 0x0125 }
            r17 = 0
            r0 = r19
            r1 = r16
            r2 = r17
            android.widget.Toast r16 = android.widget.Toast.makeText(r0, r1, r2)     // Catch:{ all -> 0x0125 }
            r16.show()     // Catch:{ all -> 0x0125 }
        L_0x0109:
            if (r14 == 0) goto L_0x010e
            r14.close()     // Catch:{ IOException -> 0x0117 }
        L_0x010e:
            if (r3 == 0) goto L_0x0113
            r3.close()
        L_0x0113:
            r16 = 0
            goto L_0x0086
        L_0x0117:
            r5 = move-exception
            java.lang.String r16 = org.nick.wwwjdic.history.Favorites.TAG
            java.lang.String r17 = "error closing CSV writer"
            r0 = r16
            r1 = r17
            r2 = r5
            android.util.Log.w(r0, r1, r2)
            goto L_0x010e
        L_0x0125:
            r16 = move-exception
        L_0x0126:
            if (r14 == 0) goto L_0x012b
            r14.close()     // Catch:{ IOException -> 0x0131 }
        L_0x012b:
            if (r3 == 0) goto L_0x0130
            r3.close()
        L_0x0130:
            throw r16
        L_0x0131:
            r5 = move-exception
            java.lang.String r17 = org.nick.wwwjdic.history.Favorites.TAG
            java.lang.String r18 = "error closing CSV writer"
            r0 = r17
            r1 = r18
            r2 = r5
            android.util.Log.w(r0, r1, r2)
            goto L_0x012b
        L_0x013f:
            r16 = move-exception
            r14 = r15
            goto L_0x0126
        L_0x0142:
            r16 = move-exception
            r5 = r16
            r14 = r15
            goto L_0x00ca
        */
        throw new UnsupportedOperationException("Method not decompiled: org.nick.wwwjdic.history.Favorites.exportToCsv(java.lang.String, java.io.Writer, boolean):int");
    }

    /* access modifiers changed from: private */
    public String getCsvExportFilename(boolean isKanji) {
        String str;
        String dateStr = DateFormat.format("yyyyMMdd-kkmmss", new Date()).toString();
        Object[] objArr = new Object[3];
        if (isKanji) {
            str = KANJI_CSV_EXPORT_FILENAME_BASE;
        } else {
            str = DICT_CSV_EXPORT_FILENAME_BASE;
        }
        objArr[0] = str;
        objArr[1] = dateStr;
        objArr[2] = CSV_EXPORT_FILENAME_EXT;
        return String.format("%s-%s.%s", objArr);
    }

    /* access modifiers changed from: private */
    public void exportToGDocs(final boolean isKanji) {
        if (AccountManagerWrapper.getInstance(this).getGoogleAccounts().length == 0) {
            Log.w(TAG, "No suitable Google accounts found");
            Toast.makeText(this, (int) R.string.no_google_accounts, 1).show();
            return;
        }
        new AsyncTask<Void, Void, Boolean>() {
            Exception exception;

            /* access modifiers changed from: protected */
            public void onPreExecute() {
                Favorites.this.getParent().setProgressBarIndeterminateVisibility(true);
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
             arg types: [java.io.File, int]
             candidates:
              ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
              ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException} */
            /* access modifiers changed from: protected */
            public Boolean doInBackground(Void... params) {
                try {
                    Log.d(Favorites.TAG, "exporting to Google docs...");
                    String filename = Favorites.this.getCsvExportFilename(isKanji);
                    File tempFile = File.createTempFile("favorites-gdocs", ".csv", WwwjdicApplication.getWwwjdicDir());
                    tempFile.deleteOnExit();
                    Log.d(Favorites.TAG, "temp file: " + tempFile.getAbsolutePath());
                    Log.d(Favorites.TAG, "document filename: " + filename);
                    int unused = Favorites.this.exportToCsv(tempFile.getAbsolutePath(), new FileWriter(tempFile, true), false);
                    Favorites.this.uploadData = new UploadData();
                    Favorites.this.uploadData.contentLength = tempFile.length();
                    Favorites.this.uploadData.contentType = "text/csv";
                    Favorites.this.uploadData.filename = filename;
                    Favorites.this.uploadData.localFilename = tempFile.getAbsolutePath();
                    return true;
                } catch (Exception e) {
                    Exception e2 = e;
                    Log.e(Favorites.TAG, "error creating temporary favorites file", e2);
                    this.exception = e2;
                    return false;
                }
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(Boolean result) {
                String errMessage;
                if (result.booleanValue()) {
                    Favorites.this.gotAccount(false);
                } else {
                    String message = Favorites.this.getResources().getString(R.string.export_error);
                    if (this.exception == null) {
                        errMessage = "Error";
                    } else {
                        errMessage = this.exception.getMessage();
                    }
                    Toast.makeText(Favorites.this, String.format(message, errMessage), 0).show();
                }
                Favorites.this.getParent().setProgressBarIndeterminateVisibility(false);
            }
        }.execute(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public void doExport(final String exportFile) {
        new AsyncTask<Void, Void, Boolean>() {
            int count = 0;
            Exception exception;

            /* access modifiers changed from: protected */
            public void onPreExecute() {
                Favorites.this.getParent().setProgressBarIndeterminateVisibility(true);
            }

            /* access modifiers changed from: protected */
            public Boolean doInBackground(Void... params) {
                IOException e;
                boolean z;
                CSVWriter writer = null;
                Cursor c = null;
                try {
                    c = Favorites.this.filterCursor();
                    CSVWriter writer2 = new CSVWriter(new FileWriter(exportFile));
                    while (c.moveToNext()) {
                        try {
                            writer2.writeNext(FavoritesEntryParser.toStringArray(HistoryDbHelper.createWwwjdicEntry(c), c.getLong(c.getColumnIndex("time"))));
                            this.count++;
                        } catch (IOException e2) {
                            e = e2;
                            writer = writer2;
                        } catch (Throwable th) {
                            th = th;
                            writer = writer2;
                            if (writer != null) {
                                try {
                                    writer.close();
                                } catch (IOException e3) {
                                    Log.w(Favorites.TAG, "error closing CSV writer", e3);
                                }
                            }
                            if (c != null) {
                                c.close();
                            }
                            throw th;
                        }
                    }
                    writer2.flush();
                    writer2.close();
                    Analytics.event("favoritesExport", Favorites.this);
                    z = true;
                    if (writer2 != null) {
                        try {
                            writer2.close();
                        } catch (IOException e4) {
                            Log.w(Favorites.TAG, "error closing CSV writer", e4);
                        }
                    }
                    if (c != null) {
                        c.close();
                    }
                } catch (IOException e5) {
                    e = e5;
                    try {
                        Log.e(Favorites.TAG, "error exporting to file", e);
                        this.exception = e;
                        z = false;
                        if (writer != null) {
                            try {
                                writer.close();
                            } catch (IOException e6) {
                                Log.w(Favorites.TAG, "error closing CSV writer", e6);
                            }
                        }
                        if (c != null) {
                            c.close();
                        }
                        return z;
                    } catch (Throwable th2) {
                        th = th2;
                    }
                }
                return z;
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(Boolean result) {
                String errMessage;
                if (result.booleanValue()) {
                    String message = Favorites.this.getResources().getString(R.string.favorites_exported);
                    Toast.makeText(Favorites.this, String.format(message, exportFile, Integer.valueOf(this.count)), 0).show();
                } else {
                    String message2 = Favorites.this.getResources().getString(R.string.export_error);
                    if (this.exception == null) {
                        errMessage = "Error";
                    } else {
                        errMessage = this.exception.getMessage();
                    }
                    Toast.makeText(Favorites.this, String.format(message2, errMessage), 0).show();
                }
                Favorites.this.getParent().setProgressBarIndeterminateVisibility(false);
            }
        }.execute(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public void doImport(final String importFile) {
        new AsyncTask<Void, Void, Boolean>() {
            int count = 0;
            Exception exception;

            /* access modifiers changed from: protected */
            public void onPreExecute() {
                Favorites.this.getParent().setProgressBarIndeterminateVisibility(true);
            }

            /* access modifiers changed from: protected */
            public Boolean doInBackground(Void... params) {
                IOException e;
                boolean z;
                CSVReader reader = null;
                Favorites.this.db.beginTransaction();
                try {
                    Favorites.this.db.deleteAllFavorites();
                    CSVReader reader2 = new CSVReader(new FileReader(importFile));
                    try {
                        String[] strArr = null;
                        while (true) {
                            String[] record = reader2.readNext();
                            if (record == null) {
                                break;
                            }
                            Favorites.this.db.addFavorite(FavoritesEntryParser.fromStringArray(record), Long.parseLong(record[3]));
                            this.count++;
                        }
                        Favorites.this.db.setTransactionSuccessful();
                        Analytics.event("favoritesImport", Favorites.this);
                        z = true;
                        if (reader2 != null) {
                            try {
                                reader2.close();
                            } catch (IOException e2) {
                                Log.w(Favorites.TAG, "error closing CSV reader", e2);
                            }
                        }
                        Favorites.this.db.endTransaction();
                    } catch (IOException e3) {
                        e = e3;
                        reader = reader2;
                    } catch (Throwable th) {
                        th = th;
                        reader = reader2;
                        if (reader != null) {
                            try {
                                reader.close();
                            } catch (IOException e4) {
                                Log.w(Favorites.TAG, "error closing CSV reader", e4);
                            }
                        }
                        Favorites.this.db.endTransaction();
                        throw th;
                    }
                } catch (IOException e5) {
                    e = e5;
                    try {
                        Log.e(Favorites.TAG, "error importing favorites", e);
                        this.exception = e;
                        z = false;
                        if (reader != null) {
                            try {
                                reader.close();
                            } catch (IOException e6) {
                                Log.w(Favorites.TAG, "error closing CSV reader", e6);
                            }
                        }
                        Favorites.this.db.endTransaction();
                        return z;
                    } catch (Throwable th2) {
                        th = th2;
                    }
                }
                return z;
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(Boolean result) {
                String errMessage;
                if (result.booleanValue()) {
                    String message = Favorites.this.getResources().getString(R.string.favorites_imported);
                    Toast.makeText(Favorites.this, String.format(message, importFile, Integer.valueOf(this.count)), 0).show();
                } else {
                    String message2 = Favorites.this.getResources().getString(R.string.import_error);
                    if (this.exception == null) {
                        errMessage = "Error";
                    } else {
                        errMessage = this.exception.getMessage();
                    }
                    Toast.makeText(Favorites.this, String.format(message2, errMessage), 0).show();
                }
                Favorites.this.refresh();
                Favorites.this.getParent().setProgressBarIndeterminateVisibility(false);
            }
        }.execute(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public Cursor filterCursor() {
        if (this.selectedFilter == -1) {
            return this.db.getFavorites();
        }
        return this.db.getFavoritesByType(this.selectedFilter);
    }

    /* access modifiers changed from: protected */
    public String[] getFilterTypes() {
        return getResources().getStringArray(R.array.filter_types_favorites);
    }
}
