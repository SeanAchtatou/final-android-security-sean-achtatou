package org.nick.wwwjdic.history;

import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.SearchRecentSuggestions;
import android.util.Log;
import android.widget.Toast;
import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import java.io.FileWriter;
import java.io.IOException;
import org.nick.wwwjdic.Constants;
import org.nick.wwwjdic.DictionaryResultListView;
import org.nick.wwwjdic.ExamplesResultListView;
import org.nick.wwwjdic.KanjiResultListView;
import org.nick.wwwjdic.R;
import org.nick.wwwjdic.SearchCriteria;
import org.nick.wwwjdic.SearchSuggestionProvider;
import org.nick.wwwjdic.utils.Analytics;

public class SearchHistory extends HistoryBase {
    private static final String EXPORT_FILENAME = "wwwjdic/search-history.csv";
    /* access modifiers changed from: private */
    public static final String TAG = SearchHistory.class.getSimpleName();

    /* access modifiers changed from: protected */
    public void setupAdapter() {
        MatrixCursor cursor = new MatrixCursor(HistoryDbHelper.HISTORY_ALL_COLUMNS, 0);
        startManagingCursor(cursor);
        setListAdapter(new SearchHistoryAdapter(this, cursor));
        new AsyncTask<Void, Void, Cursor>() {
            /* access modifiers changed from: protected */
            public void onPreExecute() {
                SearchHistory.this.getParent().setProgressBarIndeterminateVisibility(true);
            }

            /* access modifiers changed from: protected */
            public Cursor doInBackground(Void... arg0) {
                return SearchHistory.this.filterCursor();
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(Cursor cursor) {
                SearchHistory.this.resetAdapter(cursor);
                SearchHistory.this.getParent().setProgressBarIndeterminateVisibility(false);
            }
        }.execute(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public void resetAdapter(Cursor c) {
        startManagingCursor(c);
        setListAdapter(new SearchHistoryAdapter(this, c));
    }

    /* access modifiers changed from: protected */
    public void deleteAll() {
        new AsyncTask<Void, Void, Boolean>() {
            Exception exception;

            /* access modifiers changed from: protected */
            public void onPreExecute() {
                SearchHistory.this.getParent().setProgressBarIndeterminateVisibility(true);
            }

            /* access modifiers changed from: protected */
            public Boolean doInBackground(Void... params) {
                boolean z;
                new SearchRecentSuggestions(SearchHistory.this, SearchSuggestionProvider.AUTHORITY, 1).clearHistory();
                Cursor c = SearchHistory.this.filterCursor();
                SearchHistory.this.db.beginTransaction();
                while (c.moveToNext()) {
                    try {
                        SearchHistory.this.db.deleteHistoryItem((long) c.getInt(c.getColumnIndex("_id")));
                    } catch (Exception e) {
                        Exception e2 = e;
                        Log.e(SearchHistory.TAG, "Error deleting history", e2);
                        this.exception = e2;
                        z = false;
                    } finally {
                        SearchHistory.this.db.endTransaction();
                    }
                }
                SearchHistory.this.db.setTransactionSuccessful();
                z = true;
                return z;
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(Boolean result) {
                SearchHistory.this.refresh();
                SearchHistory.this.getParent().setProgressBarIndeterminateVisibility(false);
            }
        }.execute(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.search_history;
    }

    /* access modifiers changed from: protected */
    public void lookupCurrentItem() {
        SearchCriteria criteria = getCurrentCriteria();
        Intent intent = null;
        switch (criteria.getType()) {
            case 0:
                intent = new Intent(this, DictionaryResultListView.class);
                break;
            case 1:
                intent = new Intent(this, KanjiResultListView.class);
                break;
            case 2:
                intent = new Intent(this, ExamplesResultListView.class);
                break;
        }
        intent.putExtra(Constants.CRITERIA_KEY, criteria);
        Analytics.event("lookupFromHistory", this);
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void deleteCurrentItem() {
        Cursor c = getCursor();
        this.db.deleteHistoryItem((long) c.getInt(c.getColumnIndex("_id")));
        refresh();
    }

    /* access modifiers changed from: protected */
    public void copyCurrentItem() {
        SearchCriteria criteria = getCurrentCriteria();
        this.clipboardManager.setText(criteria.getQueryString());
        showCopiedToast(criteria.getQueryString());
    }

    private SearchCriteria getCurrentCriteria() {
        return HistoryDbHelper.createCriteria(getCursor());
    }

    /* access modifiers changed from: protected */
    public String getImportExportFilename() {
        return String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + "/" + EXPORT_FILENAME;
    }

    /* access modifiers changed from: protected */
    public void doExport(final String filename) {
        new AsyncTask<Void, Void, Boolean>() {
            int count = 0;
            Exception exception;

            /* access modifiers changed from: protected */
            public void onPreExecute() {
                SearchHistory.this.getParent().setProgressBarIndeterminateVisibility(true);
            }

            /* access modifiers changed from: protected */
            public Boolean doInBackground(Void... params) {
                IOException e;
                boolean z;
                CSVWriter writer = null;
                Cursor c = null;
                try {
                    c = SearchHistory.this.filterCursor();
                    CSVWriter writer2 = new CSVWriter(new FileWriter(filename));
                    while (c.moveToNext()) {
                        try {
                            writer2.writeNext(SearchCriteriaParser.toStringArray(HistoryDbHelper.createCriteria(c), c.getLong(c.getColumnIndex("time"))));
                            this.count++;
                        } catch (IOException e2) {
                            e = e2;
                            writer = writer2;
                        } catch (Throwable th) {
                            th = th;
                            writer = writer2;
                            if (writer != null) {
                                try {
                                    writer.close();
                                } catch (IOException e3) {
                                    Log.w(SearchHistory.TAG, "error closing CSV writer", e3);
                                }
                            }
                            if (c != null) {
                                c.close();
                            }
                            throw th;
                        }
                    }
                    Analytics.event("historyExport", SearchHistory.this);
                    z = true;
                    if (writer2 != null) {
                        try {
                            writer2.close();
                        } catch (IOException e4) {
                            Log.w(SearchHistory.TAG, "error closing CSV writer", e4);
                        }
                    }
                    if (c != null) {
                        c.close();
                    }
                } catch (IOException e5) {
                    e = e5;
                    try {
                        Log.e(SearchHistory.TAG, "error exporting history", e);
                        this.exception = e;
                        z = false;
                        if (writer != null) {
                            try {
                                writer.close();
                            } catch (IOException e6) {
                                Log.w(SearchHistory.TAG, "error closing CSV writer", e6);
                            }
                        }
                        if (c != null) {
                            c.close();
                        }
                        return z;
                    } catch (Throwable th2) {
                        th = th2;
                    }
                }
                return z;
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(Boolean result) {
                String errMessage;
                if (result.booleanValue()) {
                    String message = SearchHistory.this.getResources().getString(R.string.history_exported);
                    Toast.makeText(SearchHistory.this, String.format(message, filename, Integer.valueOf(this.count)), 0).show();
                } else {
                    String message2 = SearchHistory.this.getResources().getString(R.string.export_error);
                    if (this.exception == null) {
                        errMessage = "Error";
                    } else {
                        errMessage = this.exception.getMessage();
                    }
                    Toast.makeText(SearchHistory.this, String.format(message2, errMessage), 0).show();
                }
                SearchHistory.this.getParent().setProgressBarIndeterminateVisibility(false);
            }
        }.execute(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public void doImport(final String importFile) {
        new AsyncTask<Void, Void, Boolean>() {
            int count = 0;
            Exception exception;

            /* access modifiers changed from: protected */
            public void onPreExecute() {
                SearchHistory.this.getParent().setProgressBarIndeterminateVisibility(true);
            }

            /* access modifiers changed from: protected */
            public Boolean doInBackground(Void... params) {
                boolean z;
                CSVReader reader = null;
                SearchHistory.this.db.beginTransaction();
                try {
                    SearchHistory.this.db.deleteAllHistory();
                    reader = SearchHistory.this.openImportFile(importFile);
                    if (reader == null) {
                        z = false;
                        if (reader != null) {
                            try {
                                reader.close();
                            } catch (IOException e) {
                                Log.w(SearchHistory.TAG, "error closing CSV reader", e);
                            }
                        }
                        SearchHistory.this.db.endTransaction();
                    } else {
                        String[] strArr = null;
                        while (true) {
                            String[] record = reader.readNext();
                            if (record == null) {
                                break;
                            }
                            SearchHistory.this.db.addSearchCriteria(SearchCriteriaParser.fromStringArray(record), Long.parseLong(record[11]));
                            this.count++;
                        }
                        SearchHistory.this.db.setTransactionSuccessful();
                        Analytics.event("historyImport", SearchHistory.this);
                        z = true;
                        if (reader != null) {
                            try {
                                reader.close();
                            } catch (IOException e2) {
                                Log.w(SearchHistory.TAG, "error closing CSV reader", e2);
                            }
                        }
                        SearchHistory.this.db.endTransaction();
                    }
                } catch (IOException e3) {
                    IOException e4 = e3;
                    Log.e(SearchHistory.TAG, "error importing history", e4);
                    this.exception = e4;
                    z = false;
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (IOException e5) {
                            Log.w(SearchHistory.TAG, "error closing CSV reader", e5);
                        }
                    }
                    SearchHistory.this.db.endTransaction();
                } catch (Throwable th) {
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (IOException e6) {
                            Log.w(SearchHistory.TAG, "error closing CSV reader", e6);
                        }
                    }
                    SearchHistory.this.db.endTransaction();
                    throw th;
                }
                return z;
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(Boolean result) {
                String errMessage;
                if (result.booleanValue()) {
                    String message = SearchHistory.this.getResources().getString(R.string.history_imported);
                    Toast.makeText(SearchHistory.this, String.format(message, importFile, Integer.valueOf(this.count)), 0).show();
                } else {
                    String message2 = SearchHistory.this.getResources().getString(R.string.import_error);
                    if (this.exception == null) {
                        errMessage = "Error";
                    } else {
                        errMessage = this.exception.getMessage();
                    }
                    Toast.makeText(SearchHistory.this, String.format(message2, errMessage), 0).show();
                }
                SearchHistory.this.refresh();
                SearchHistory.this.getParent().setProgressBarIndeterminateVisibility(false);
            }
        }.execute(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public Cursor filterCursor() {
        if (this.selectedFilter == -1) {
            return this.db.getHistory();
        }
        return this.db.getHistoryByType(this.selectedFilter);
    }

    /* access modifiers changed from: protected */
    public String[] getFilterTypes() {
        return getResources().getStringArray(R.array.filter_types_history);
    }
}
