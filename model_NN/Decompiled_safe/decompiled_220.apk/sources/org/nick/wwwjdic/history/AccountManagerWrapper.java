package org.nick.wwwjdic.history;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.content.Context;
import android.os.Bundle;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AccountManagerWrapper {
    public static final String KEY_AUTHTOKEN = "authtoken";
    public static final String KEY_INTENT = "intent";
    private static AccountManagerWrapper instance;
    private List<Account> googleAccounts = new ArrayList();
    private AccountManager manager;

    public static AccountManagerWrapper getInstance(Context context) {
        if (instance == null) {
            instance = new AccountManagerWrapper(context);
        }
        return instance;
    }

    private AccountManagerWrapper(Context context) {
        this.manager = AccountManager.get(context);
    }

    public String[] getGoogleAccounts() {
        Account[] accounts = this.manager.getAccountsByType("com.google");
        String[] result = new String[accounts.length];
        this.googleAccounts.clear();
        for (int i = 0; i < accounts.length; i++) {
            this.googleAccounts.add(accounts[i]);
            result[i] = accounts[i].name;
        }
        return result;
    }

    public Bundle getAuthToken(String accountName, String tokenType) throws OperationCanceledException, AuthenticatorException, IOException {
        return this.manager.getAuthToken(findAccount(accountName), tokenType, true, null, null).getResult();
    }

    private Account findAccount(String accountName) {
        for (Account account : this.googleAccounts) {
            if (accountName.equals(account.name)) {
                return account;
            }
        }
        return null;
    }

    public void invalidateAuthToken(String token) {
        this.manager.invalidateAuthToken("com.google", token);
    }
}
