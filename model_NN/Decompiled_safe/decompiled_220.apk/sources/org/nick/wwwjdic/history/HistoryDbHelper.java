package org.nick.wwwjdic.history;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;
import org.nick.wwwjdic.DictionaryEntry;
import org.nick.wwwjdic.KanjiEntry;
import org.nick.wwwjdic.SearchCriteria;
import org.nick.wwwjdic.WwwjdicEntry;

public class HistoryDbHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "wwwjdic_history.db";
    private static final int DATABASE_VERSION = 15;
    public static final String[] FAVORITES_ALL_COLUMNS = {ID, TIME, FAVORITES_IS_KANJI, FAVORITES_HEADWORD, FAVORITES_DICT_STR, "dictionary"};
    private static final String FAVORITES_DICTIONARY = "dictionary";
    private static final String FAVORITES_DICT_STR = "dict_str";
    private static final String FAVORITES_HEADWORD = "headword";
    private static final String FAVORITES_IS_KANJI = "is_kanji";
    private static final String FAVORITES_TABLE_CREATE = "create table favorites (_id integer primary key autoincrement, time integer not null, is_kanji integer not null, headword text not null, dict_str text not null, dictionary text);";
    private static final String FAVORITES_TABLE_NAME = "favorites";
    public static final int FAVORITES_TYPE_DICT = 0;
    public static final int FAVORITES_TYPE_KANJI = 1;
    public static final String[] HISTORY_ALL_COLUMNS = {ID, TIME, HISTORY_QUERY_STRING, HISTORY_IS_EXACT_MATCH, HISTORY_SEARCH_TYPE, HISTORY_IS_ROMANIZED_JAPANESE, HISTORY_IS_COMMON_WORDS_ONLY, "dictionary", HISTORY_KANJI_SEARCH_TYPE, HISTORY_MIN_STROKE_COUNT, HISTORY_MAX_STROKE_COUNT, HISTORY_MAX_RESULTS};
    private static final String HISTORY_BACKUP_TABLE_CREATE = "create table search_history_backup (_id integer primary key autoincrement, time integer not null, query_string text not null, is_exact_match integer, is_kanji integer not null, is_romanized_japanese integer, is_common_words_only integer, dictionary text, kanji_search_type text, min_stroke_count integer, max_stroke_count integer);";
    private static final String HISTORY_BACKUP_TABLE_NAME = "search_history_backup";
    private static final String HISTORY_DICTIONARY = "dictionary";
    private static final String HISTORY_IS_COMMON_WORDS_ONLY = "is_common_words_only";
    private static final String HISTORY_IS_EXACT_MATCH = "is_exact_match";
    private static final String HISTORY_IS_ROMANIZED_JAPANESE = "is_romanized_japanese";
    private static final String HISTORY_KANJI_SEARCH_TYPE = "kanji_search_type";
    private static final String HISTORY_MAX_RESULTS = "max_results";
    private static final String HISTORY_MAX_STROKE_COUNT = "max_stroke_count";
    private static final String HISTORY_MIN_STROKE_COUNT = "min_stroke_count";
    private static final String HISTORY_QUERY_STRING = "query_string";
    private static final String HISTORY_SEARCH_TYPE = "search_type";
    public static final int HISTORY_SEARCH_TYPE_DICT = 0;
    public static final int HISTORY_SEARCH_TYPE_EXAMPLES = 2;
    public static final int HISTORY_SEARCH_TYPE_KANJI = 1;
    private static final String HISTORY_TABLE_CREATE = "create table search_history (_id integer primary key autoincrement, time integer not null, query_string text not null, is_exact_match integer, search_type integer not null, is_romanized_japanese integer, is_common_words_only integer, dictionary text, kanji_search_type text, min_stroke_count integer, max_stroke_count integer, max_results integer);";
    private static final String HISTORY_TABLE_NAME = "search_history";
    private static final String ID = "_id";
    private static final String TAG = HistoryDbHelper.class.getSimpleName();
    private static final String TIME = "time";
    private static HistoryDbHelper instance;
    private Context context;
    private SQLiteStatement favoritesCountStatement;
    private SQLiteStatement historyCountStatement;

    public static HistoryDbHelper getInstance(Context context2) {
        if (instance == null) {
            instance = new HistoryDbHelper(context2);
        }
        return instance;
    }

    private HistoryDbHelper(Context context2) {
        super(context2, DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, (int) DATABASE_VERSION);
        this.context = context2;
    }

    public void onCreate(SQLiteDatabase db) {
        db.beginTransaction();
        try {
            db.execSQL(HISTORY_TABLE_CREATE);
            db.execSQL(FAVORITES_TABLE_CREATE);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(TAG, String.format("upgrading db from version %d to %d", Integer.valueOf(oldVersion), Integer.valueOf(newVersion)));
        if (newVersion == 14) {
            upgradeDbTov14(db);
        }
        if (newVersion == DATABASE_VERSION) {
            upgradeDbTov15(db);
        }
        Log.d(TAG, "done");
    }

    public void onOpen(SQLiteDatabase db) {
        this.favoritesCountStatement = db.compileStatement("select count(*) from favorites where is_kanji = ?");
        this.historyCountStatement = db.compileStatement("select count(*) from search_history where search_type = ?");
    }

    private void upgradeDbTov14(SQLiteDatabase db) {
        db.beginTransaction();
        try {
            db.execSQL(HISTORY_BACKUP_TABLE_CREATE);
            db.execSQL("insert into search_history_backup select * from search_history");
            db.execSQL("drop table search_history");
            db.execSQL(HISTORY_TABLE_CREATE);
            db.execSQL("insert into search_history(_id, time, query_string, is_exact_match, search_type, is_romanized_japanese, is_common_words_only, dictionary, kanji_search_type, min_stroke_count, max_stroke_count)select * from search_history_backup");
            db.execSQL("drop table search_history_backup");
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    private void upgradeDbTov15(SQLiteDatabase db) {
        db.beginTransaction();
        try {
            db.execSQL("alter table favorites add column dictionary text");
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public void addSearchCriteria(SearchCriteria criteria) {
        addSearchCriteria(criteria, System.currentTimeMillis());
    }

    public synchronized void addSearchCriteria(SearchCriteria criteria, long currentTimeMillis) {
        getWritableDatabase().insertOrThrow(HISTORY_TABLE_NAME, null, fromCriteria(criteria, currentTimeMillis));
    }

    public synchronized long addFavorite(WwwjdicEntry entry) {
        return addFavorite(entry, System.currentTimeMillis());
    }

    public synchronized long addFavorite(WwwjdicEntry entry, long currentTimeInMillis) {
        return addFavorite(getWritableDatabase(), entry, currentTimeInMillis);
    }

    private long addFavorite(SQLiteDatabase db, WwwjdicEntry entry, long currentTimeInMillis) {
        return db.insertOrThrow(FAVORITES_TABLE_NAME, null, fromEntry(entry, currentTimeInMillis));
    }

    private ContentValues fromEntry(WwwjdicEntry entry, long currentTimeInMillis) {
        int i;
        ContentValues values = new ContentValues();
        values.put(TIME, Long.valueOf(currentTimeInMillis));
        if (entry.isKanji()) {
            i = 1;
        } else {
            i = 0;
        }
        values.put(FAVORITES_IS_KANJI, Integer.valueOf(i));
        values.put(FAVORITES_HEADWORD, entry.getHeadword());
        values.put(FAVORITES_DICT_STR, entry.getDictString());
        if (entry.getDictString() != null) {
            values.put("dictionary", entry.getDictionary());
        }
        return values;
    }

    private ContentValues fromCriteria(SearchCriteria criteria, long currentTimeMillis) {
        ContentValues values = new ContentValues();
        values.put(TIME, Long.valueOf(currentTimeMillis));
        values.put(HISTORY_QUERY_STRING, criteria.getQueryString());
        values.put(HISTORY_IS_EXACT_MATCH, Boolean.valueOf(criteria.isExactMatch()));
        values.put(HISTORY_SEARCH_TYPE, Integer.valueOf(criteria.getType()));
        values.put(HISTORY_IS_ROMANIZED_JAPANESE, Boolean.valueOf(criteria.isRomanizedJapanese()));
        values.put(HISTORY_IS_COMMON_WORDS_ONLY, Boolean.valueOf(criteria.isCommonWordsOnly()));
        values.put("dictionary", criteria.getDictionaryCode());
        values.put(HISTORY_KANJI_SEARCH_TYPE, criteria.getKanjiSearchType());
        values.put(HISTORY_MIN_STROKE_COUNT, criteria.getMinStrokeCount());
        values.put(HISTORY_MAX_STROKE_COUNT, criteria.getMaxStrokeCount());
        values.put(HISTORY_MAX_RESULTS, criteria.getNumMaxResults());
        return values;
    }

    public Cursor getHistory() {
        return getReadableDatabase().query(HISTORY_TABLE_NAME, HISTORY_ALL_COLUMNS, null, null, null, null, "time desc");
    }

    public Cursor getHistoryByType(int type) {
        return getReadableDatabase().query(HISTORY_TABLE_NAME, HISTORY_ALL_COLUMNS, "search_type = ?", new String[]{Integer.toString(type)}, null, null, "time desc");
    }

    public long getHistoryCountByType(int type) {
        SQLiteDatabase db = getReadableDatabase();
        if (this.historyCountStatement == null) {
            this.historyCountStatement = db.compileStatement("select count(*) from search_history where search_type = ?");
        }
        this.historyCountStatement.bindString(1, Integer.toString(type));
        return this.historyCountStatement.simpleQueryForLong();
    }

    public long getDictHistoryCount() {
        return getHistoryCountByType(0);
    }

    public long getKanjiHistoryCount() {
        return getHistoryCountByType(1);
    }

    public long getExamplesHistoryCount() {
        return getHistoryCountByType(2);
    }

    public long getFavoritesCountByType(int type) {
        SQLiteDatabase db = getReadableDatabase();
        if (this.favoritesCountStatement == null) {
            this.favoritesCountStatement = db.compileStatement("select count(*) from favorites where is_kanji = ?");
        }
        this.favoritesCountStatement.bindString(1, Integer.toString(type));
        return this.favoritesCountStatement.simpleQueryForLong();
    }

    public long getDictFavoritesCount() {
        return getFavoritesCountByType(0);
    }

    public long getKanjiFavoritesCount() {
        return getFavoritesCountByType(1);
    }

    public List<String> getRecentHistoryByType(int type, int top) {
        List<String> result = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = null;
        try {
            c = db.query(HISTORY_TABLE_NAME, new String[]{HISTORY_QUERY_STRING, HISTORY_KANJI_SEARCH_TYPE}, "search_type = ?", new String[]{Integer.toString(type)}, null, null, "time desc", Integer.toString(top));
            while (c.moveToNext()) {
                String searchQuery = c.getString(c.getColumnIndex(HISTORY_QUERY_STRING));
                String historyStr = searchQuery;
                if (!c.isNull(c.getColumnIndex(HISTORY_KANJI_SEARCH_TYPE))) {
                    historyStr = String.format("%s(%s)", searchQuery, HistoryUtils.lookupKanjiSearchName(c.getString(c.getColumnIndex(HISTORY_KANJI_SEARCH_TYPE)), searchQuery, this.context));
                }
                result.add(historyStr);
            }
            return result;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public List<String> getRecentDictHistory(int top) {
        return getRecentHistoryByType(0, top);
    }

    public List<String> getRecentKanjiHistory(int top) {
        return getRecentHistoryByType(1, top);
    }

    public List<String> getRecentExamplesHistory(int top) {
        return getRecentHistoryByType(2, top);
    }

    public Cursor getFavorites() {
        return getReadableDatabase().query(FAVORITES_TABLE_NAME, FAVORITES_ALL_COLUMNS, null, null, null, null, "time desc");
    }

    public List<String> getRecentFavoritesByType(int type, int top) {
        List<String> result = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = null;
        try {
            c = db.query(FAVORITES_TABLE_NAME, new String[]{FAVORITES_HEADWORD}, "is_kanji = ?", new String[]{Integer.toString(type)}, null, null, "time desc", Integer.toString(top));
            while (c.moveToNext()) {
                result.add(c.getString(c.getColumnIndex(FAVORITES_HEADWORD)));
            }
            return result;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public List<String> getRecentDictFavorites(int top) {
        return getRecentFavoritesByType(0, top);
    }

    public List<String> getRecentKanjiFavorites(int top) {
        return getRecentFavoritesByType(1, top);
    }

    public Cursor getFavoritesByType(int type) {
        return getReadableDatabase().query(FAVORITES_TABLE_NAME, FAVORITES_ALL_COLUMNS, "is_kanji = ?", new String[]{Integer.toString(type)}, null, null, "time desc");
    }

    /* JADX INFO: Multiple debug info for r9v4 org.nick.wwwjdic.SearchCriteria: [D('isExactMatch' boolean), D('result' org.nick.wwwjdic.SearchCriteria)] */
    /* JADX INFO: Multiple debug info for r9v7 java.lang.String: [D('dictionary' java.lang.String), D('cursor' android.database.Cursor)] */
    /* JADX INFO: Multiple debug info for r9v8 org.nick.wwwjdic.SearchCriteria: [D('dictionary' java.lang.String), D('result' org.nick.wwwjdic.SearchCriteria)] */
    /* JADX INFO: Multiple debug info for r9v12 org.nick.wwwjdic.SearchCriteria: [D('maxStrokeCount' java.lang.Integer), D('result' org.nick.wwwjdic.SearchCriteria)] */
    /* JADX INFO: Multiple debug info for r9v16 org.nick.wwwjdic.SearchCriteria: [D('result' org.nick.wwwjdic.SearchCriteria), D('cursor' android.database.Cursor)] */
    public static SearchCriteria createCriteria(Cursor cursor) {
        Integer minStrokeCount;
        Integer maxStrokeCount;
        int type = cursor.getInt(cursor.getColumnIndex(HISTORY_SEARCH_TYPE));
        String queryString = cursor.getString(cursor.getColumnIndex(HISTORY_QUERY_STRING));
        long id = cursor.getLong(cursor.getColumnIndex(ID));
        switch (type) {
            case 0:
                SearchCriteria result = SearchCriteria.createForDictionary(queryString, cursor.getInt(cursor.getColumnIndex(HISTORY_IS_EXACT_MATCH)) == 1, cursor.getInt(cursor.getColumnIndex(HISTORY_IS_ROMANIZED_JAPANESE)) == 1, cursor.getInt(cursor.getColumnIndex(HISTORY_IS_COMMON_WORDS_ONLY)) == 1, cursor.getString(cursor.getColumnIndex("dictionary")));
                result.setId(Long.valueOf(id));
                return result;
            case 1:
                String searchType = cursor.getString(cursor.getColumnIndex(HISTORY_KANJI_SEARCH_TYPE));
                int minStrokexIdx = cursor.getColumnIndex(HISTORY_MIN_STROKE_COUNT);
                int maxStrokesIdx = cursor.getColumnIndex(HISTORY_MAX_STROKE_COUNT);
                if (!cursor.isNull(minStrokexIdx) || !cursor.isNull(maxStrokesIdx)) {
                    if (!cursor.isNull(minStrokexIdx)) {
                        minStrokeCount = Integer.valueOf(cursor.getInt(minStrokexIdx));
                    } else {
                        minStrokeCount = null;
                    }
                    if (!cursor.isNull(maxStrokesIdx)) {
                        maxStrokeCount = Integer.valueOf(cursor.getInt(maxStrokesIdx));
                    } else {
                        maxStrokeCount = null;
                    }
                    SearchCriteria result2 = SearchCriteria.createWithStrokeCount(queryString, searchType, minStrokeCount, maxStrokeCount);
                    result2.setId(Long.valueOf(id));
                    return result2;
                }
                SearchCriteria result3 = SearchCriteria.createForKanji(queryString, searchType);
                result3.setId(Long.valueOf(id));
                return result3;
            case 2:
                SearchCriteria result4 = SearchCriteria.createForExampleSearch(queryString, cursor.getInt(cursor.getColumnIndex(HISTORY_IS_EXACT_MATCH)) == 1, cursor.getInt(cursor.getColumnIndex(HISTORY_MAX_RESULTS)));
                result4.setId(Long.valueOf(id));
                return result4;
            default:
                throw new IllegalStateException("Unknown criteria type " + type);
        }
    }

    public static WwwjdicEntry createWwwjdicEntry(Cursor cursor) {
        boolean isKanji;
        WwwjdicEntry result;
        if (cursor.getInt(cursor.getColumnIndex(FAVORITES_IS_KANJI)) == 1) {
            isKanji = true;
        } else {
            isKanji = false;
        }
        String dictStr = cursor.getString(cursor.getColumnIndex(FAVORITES_DICT_STR));
        long id = cursor.getLong(cursor.getColumnIndex(ID));
        String dictionary = null;
        int idx = cursor.getColumnIndexOrThrow("dictionary");
        if (!cursor.isNull(idx)) {
            dictionary = cursor.getString(idx);
        }
        if (isKanji) {
            result = KanjiEntry.parseKanjidic(dictStr);
        } else {
            result = DictionaryEntry.parseEdict(dictStr, dictionary);
        }
        result.setId(Long.valueOf(id));
        return result;
    }

    public synchronized void deleteHistoryItem(long id) {
        getWritableDatabase().delete(HISTORY_TABLE_NAME, "_id = " + id, null);
    }

    public synchronized void deleteFavorite(long id) {
        getWritableDatabase().delete(FAVORITES_TABLE_NAME, "_id = " + id, null);
    }

    public synchronized void deleteAllHistory() {
        getWritableDatabase().delete(HISTORY_TABLE_NAME, null, null);
    }

    public synchronized void deleteAllFavorites() {
        getWritableDatabase().delete(FAVORITES_TABLE_NAME, null, null);
    }

    public Long getFavoriteId(String headword) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = null;
        try {
            cursor = db.query(FAVORITES_TABLE_NAME, new String[]{ID}, "headword = ?", new String[]{headword}, null, null, "time desc");
            if (cursor.getCount() == 0) {
                return null;
            }
            int idx = cursor.getColumnIndex(ID);
            cursor.moveToFirst();
            Long valueOf = Long.valueOf(cursor.getLong(idx));
            if (cursor == null) {
                return valueOf;
            }
            cursor.close();
            return valueOf;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public void beginTransaction() {
        getWritableDatabase().beginTransaction();
    }

    public void setTransactionSuccessful() {
        getWritableDatabase().setTransactionSuccessful();
    }

    public void endTransaction() {
        getWritableDatabase().endTransaction();
    }
}
