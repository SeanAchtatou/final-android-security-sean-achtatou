package org.nick.wwwjdic.history;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import org.nick.wwwjdic.Constants;
import org.nick.wwwjdic.DictionaryEntry;
import org.nick.wwwjdic.KanjiEntry;

public class AnkiGenerator {
    private static final int CARD_PRIORITY_NORMAL = 2;
    private static final int CARD_TYPE_NEW = 2;
    private static final long DECK_ID = 1;
    private static final long DICT_FWD_CARD_MODEL_ID = -4952737841158526416L;
    private static final long DICT_HEADWORD_FIELD_ID = 7468722094757145135L;
    private static final int DICT_HEADWORD_ORD = 0;
    private static final long DICT_MEANING_FIELD_ID = 8937385955465940432L;
    private static final int DICT_MEANING_ORD = 2;
    private static final long DICT_MODEL_ID = -1051435290320934353L;
    private static final long DICT_READING_FIELD_ID = -6512467652926215633L;
    private static final int DICT_READING_ORD = 1;
    private static final int FWD_CARD_ORDINAL = 0;
    private static final long KANJI_FIELD_ID = -4886934269285393224L;
    private static final int KANJI_FIELD_ORD = 0;
    private static final long KANJI_FWD_CARD_MODEL_ID = 8257311625381387448L;
    private static final long KANJI_MODEL_ID = 4998196432932412600L;
    private static final long KUNYOMI_FIELD_ID = 760313581623303912L;
    private static final int KUNYOMI_FIELD_ORD = 2;
    private static final long MEANING_FIELD_ID = -3128184056797208296L;
    private static final int MEANING_FIELD_ORD = 4;
    private static final long NANORI_FIELD_ID = -7227726355099557880L;
    private static final int NANORI_FIELD_ORD = 3;
    private static final long ONYOMI_FIELD_ID = 4393069213469613240L;
    private static final int ONYOMI_FIELD_ORD = 1;
    private static final String QA_TEMPLATE = "<span class=\"fm3cf7512c968f9cb8\">%s</span><br>";
    private static final String TAG = AnkiGenerator.class.getSimpleName();
    private Context context;
    private Random random;

    public AnkiGenerator(Context context2) {
        this.context = context2;
        try {
            this.random = SecureRandom.getInstance("SHA1PRNG");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public int createKanjiAnkiFile(String path, List<KanjiEntry> kanjis) {
        SQLiteDatabase db = null;
        try {
            db = SQLiteDatabase.openDatabase(path, null, 268435456);
            db.beginTransaction();
            execSqlFromFile(db, "anki-create-tables.sql");
            execSqlFromFile(db, "anki-kanji-model.sql");
            addKanjis(kanjis, db);
            setUtcOffset(db);
            db.setTransactionSuccessful();
            return kanjis.size();
        } finally {
            if (db != null) {
                db.endTransaction();
                db.close();
            }
        }
    }

    private void addKanjis(List<KanjiEntry> kanjis, SQLiteDatabase db) {
        for (KanjiEntry k : kanjis) {
            addKanji(db, k);
        }
        updateDeckCounts(db, kanjis.size());
    }

    private void setUtcOffset(SQLiteDatabase db) {
        ContentValues values = new ContentValues();
        values.put("utcOffset", Integer.valueOf(((Calendar.getInstance().get(15) * -1) / 1000) + 14400));
        db.update("decks", values, "id = ?", new String[]{Long.toString(DECK_ID)});
    }

    private void updateDeckCounts(SQLiteDatabase db, int count) {
        ContentValues values = new ContentValues();
        values.put("cardCount", Integer.valueOf(count));
        values.put("factCount", Integer.valueOf(count));
        values.put("newCount", Integer.valueOf(count));
        db.update("decks", values, "id = ?", new String[]{Long.toString(DECK_ID)});
    }

    private void addKanji(SQLiteDatabase db, KanjiEntry k) {
        long factId = insertFact(db, KANJI_MODEL_ID);
        insertCard(db, factId, KANJI_FWD_CARD_MODEL_ID, generateQuestion(k.getHeadword()), generateKanjiAnswer(k));
        insertField(db, k.getKanji(), factId, KANJI_FIELD_ID, 0);
        if (k.getOnyomi() != null) {
            insertField(db, k.getOnyomi(), factId, ONYOMI_FIELD_ID, 1);
        }
        if (k.getKunyomi() != null) {
            insertField(db, k.getKunyomi(), factId, KUNYOMI_FIELD_ID, 2);
        }
        if (k.getNanori() != null) {
            insertField(db, k.getNanori(), factId, NANORI_FIELD_ID, 3);
        }
        if (k.getMeaningsAsString() != null) {
            insertField(db, k.getMeaningsAsString(), factId, MEANING_FIELD_ID, 4);
        }
    }

    private void execSqlFromFile(SQLiteDatabase db, String resourceName) {
        for (String s : readSchema(resourceName).split(Constants.DICT_VARIATION_DELIMITER)) {
            if (s != null) {
                String s2 = s.trim();
                Log.d(TAG, "SQL: " + s2);
                if (!TextUtils.isEmpty(s2) && !s2.startsWith("--")) {
                    db.execSQL(s2);
                }
            }
        }
    }

    public int createDictAnkiFile(String path, List<DictionaryEntry> words) {
        SQLiteDatabase db = null;
        try {
            db = SQLiteDatabase.openDatabase(path, null, 268435456);
            db.beginTransaction();
            execSqlFromFile(db, "anki-create-tables.sql");
            execSqlFromFile(db, "anki-dict-model.sql");
            addWords(words, db);
            setUtcOffset(db);
            db.setTransactionSuccessful();
            return words.size();
        } finally {
            if (db != null) {
                db.endTransaction();
                db.close();
            }
        }
    }

    private void addWords(List<DictionaryEntry> words, SQLiteDatabase db) {
        for (DictionaryEntry w : words) {
            addWord(db, w);
        }
        updateDeckCounts(db, words.size());
    }

    private void addWord(SQLiteDatabase db, DictionaryEntry d) {
        long factId = insertFact(db, DICT_MODEL_ID);
        insertCard(db, factId, DICT_FWD_CARD_MODEL_ID, generateQuestion(d.getHeadword()), generateDictAnswer(d));
        insertField(db, d.getHeadword(), factId, DICT_HEADWORD_FIELD_ID, 0);
        if (d.getReading() != null) {
            insertField(db, d.getReading(), factId, DICT_READING_FIELD_ID, 1);
        }
        if (d.getMeaningsAsString() != null) {
            insertField(db, d.getMeaningsAsString(), factId, DICT_MEANING_FIELD_ID, 2);
        }
    }

    private long insertFact(SQLiteDatabase db, long modelId) {
        ContentValues fact = new ContentValues();
        long factId = generateId();
        fact.put("id", Long.valueOf(factId));
        fact.put("modelId", Long.valueOf(modelId));
        double now = now();
        fact.put("created", Double.valueOf(now));
        fact.put("modified", Double.valueOf(now));
        fact.put("tags", "");
        fact.put("spaceUntil", Double.valueOf(0.0d));
        db.insert("facts", null, fact);
        return factId;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    private long insertCard(SQLiteDatabase db, long factId, long cardModelId, String question, String answer) {
        ContentValues card = new ContentValues();
        long cardId = generateId();
        double now = now();
        card.put("id", Long.valueOf(cardId));
        card.put("factId", Long.valueOf(factId));
        card.put("cardModelId", Long.valueOf(cardModelId));
        card.put("created", Double.valueOf(now));
        card.put("modified", Double.valueOf(now));
        card.put("tags", "");
        card.put("ordinal", (Integer) 0);
        card.put("question", question);
        card.put("answer", answer);
        card.put("priority", (Integer) 2);
        card.put("interval", Double.valueOf(0.0d));
        card.put("lastInterval", Double.valueOf(0.0d));
        card.put("due", Double.valueOf(now));
        card.put("lastDue", (Integer) 0);
        card.put("factor", Double.valueOf(2.5d));
        card.put("lastFactor", Double.valueOf(2.5d));
        card.put("firstAnswered", (Integer) 0);
        card.put("reps", (Integer) 0);
        card.put("successive", (Integer) 0);
        card.put("averageTime", (Integer) 0);
        card.put("reviewTime", (Integer) 0);
        card.put("youngEase0", (Integer) 0);
        card.put("youngEase1", (Integer) 0);
        card.put("youngEase2", (Integer) 0);
        card.put("youngEase3", (Integer) 0);
        card.put("youngEase4", (Integer) 0);
        card.put("matureEase0", (Integer) 0);
        card.put("matureEase1", (Integer) 0);
        card.put("matureEase2", (Integer) 0);
        card.put("matureEase3", (Integer) 0);
        card.put("matureEase4", (Integer) 0);
        card.put("yesCount", (Integer) 0);
        card.put("noCount", (Integer) 0);
        card.put("spaceUntil", (Integer) 0);
        card.put("relativeDelay", (Integer) 0);
        card.put("isDue", (Boolean) true);
        card.put("type", (Integer) 2);
        card.put("combinedDue", Double.valueOf(now));
        db.insert("cards", null, card);
        return cardId;
    }

    private void insertField(SQLiteDatabase db, String value, long factId, long fieldModelId, int ordinal) {
        ContentValues field = new ContentValues();
        field.put("id", Long.valueOf(generateId()));
        field.put("factId", Long.valueOf(factId));
        field.put("fieldModelId", Long.valueOf(fieldModelId));
        field.put("ordinal", Integer.valueOf(ordinal));
        field.put("value", value);
        db.insert("fields", null, field);
    }

    private String generateKanjiAnswer(KanjiEntry k) {
        StringBuffer buff = new StringBuffer();
        if (k.getOnyomi() != null) {
            buff.append(String.format(QA_TEMPLATE, k.getOnyomi()));
        }
        if (k.getKunyomi() != null) {
            buff.append(String.format(QA_TEMPLATE, k.getKunyomi()));
        }
        if (k.getNanori() != null) {
            buff.append(String.format(QA_TEMPLATE, k.getNanori()));
        }
        if (k.getMeaningsAsString() != null) {
            buff.append(String.format(QA_TEMPLATE, k.getMeaningsAsString()));
        }
        return buff.toString();
    }

    private String generateDictAnswer(DictionaryEntry d) {
        StringBuffer buff = new StringBuffer();
        if (d.getReading() != null) {
            buff.append(String.format(QA_TEMPLATE, d.getReading()));
        }
        if (d.getMeaningsAsString() != null) {
            buff.append(String.format(QA_TEMPLATE, d.getMeaningsAsString()));
        }
        return buff.toString();
    }

    private String generateQuestion(String question) {
        return String.format(QA_TEMPLATE, question);
    }

    private long generateId() {
        return ((long) (this.random.nextInt(21) << 41)) | System.currentTimeMillis();
    }

    private String readSchema(String name) {
        InputStream in = null;
        try {
            in = this.context.getAssets().open(name);
            String readTextFile = readTextFile(in);
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                }
            }
            return readTextFile;
        } catch (IOException e2) {
            throw new RuntimeException(e2);
        } catch (Throwable th) {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e3) {
                }
            }
            throw th;
        }
    }

    private String readTextFile(InputStream in) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buff = new byte[1024];
        while (true) {
            int len = in.read(buff);
            if (len == -1) {
                return baos.toString("ASCII");
            }
            baos.write(buff, 0, len);
        }
    }

    private static double now() {
        return ((double) System.currentTimeMillis()) / 1000.0d;
    }
}
