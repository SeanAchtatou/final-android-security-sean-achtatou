package org.nick.wwwjdic.history;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Environment;
import android.text.ClipboardManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.Toast;
import au.com.bytecode.opencsv.CSVReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import org.nick.wwwjdic.Constants;
import org.nick.wwwjdic.R;
import org.nick.wwwjdic.utils.Analytics;

public abstract class HistoryBase extends ListActivity {
    protected static final int CONFIRM_DELETE_DIALOG_ID = 0;
    private static final int DELETE_ALL_ITEM_IDX = 3;
    private static final int EXPORT_ITEM_IDX = 1;
    public static final int FILTER_ALL = -1;
    public static final int FILTER_DICT = 0;
    public static final int FILTER_EXAMPLES = 2;
    private static final int FILTER_ITEM_IDX = 2;
    public static final int FILTER_KANJI = 1;
    private static final int IMPORT_ITEM_IDX = 0;
    private static final int MENU_ITEM_COPY = 2;
    private static final int MENU_ITEM_DELETE = 3;
    private static final int MENU_ITEM_DELETE_ALL = 0;
    private static final int MENU_ITEM_EXPORT = 4;
    private static final int MENU_ITEM_FILTER = 6;
    private static final int MENU_ITEM_IMPORT = 5;
    private static final int MENU_ITEM_LOOKUP = 1;
    private static final String TAG = HistoryBase.class.getSimpleName();
    private static final byte[] UTF8_BOM = {-17, -69, -65};
    protected ClipboardManager clipboardManager;
    protected HistoryDbHelper db = HistoryDbHelper.getInstance(this);
    protected int selectedFilter = -1;

    /* access modifiers changed from: protected */
    public abstract void copyCurrentItem();

    /* access modifiers changed from: protected */
    public abstract void deleteAll();

    /* access modifiers changed from: protected */
    public abstract void deleteCurrentItem();

    /* access modifiers changed from: protected */
    public abstract void doExport(String str);

    /* access modifiers changed from: protected */
    public abstract void doImport(String str);

    /* access modifiers changed from: protected */
    public abstract Cursor filterCursor();

    /* access modifiers changed from: protected */
    public abstract int getContentView();

    /* access modifiers changed from: protected */
    public abstract String[] getFilterTypes();

    /* access modifiers changed from: protected */
    public abstract String getImportExportFilename();

    /* access modifiers changed from: protected */
    public abstract void lookupCurrentItem();

    /* access modifiers changed from: protected */
    public abstract void resetAdapter(Cursor cursor);

    /* access modifiers changed from: protected */
    public abstract void setupAdapter();

    protected HistoryBase() {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(5);
        this.clipboardManager = (ClipboardManager) getSystemService("clipboard");
        setContentView(getContentView());
        getListView().setOnCreateContextMenuListener(this);
        this.selectedFilter = getIntent().getIntExtra(Constants.FILTER_TYPE, -1);
        setupAdapter();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        Analytics.startSession(this);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        Analytics.endSession(this);
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        lookupCurrentItem();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 5, 0, (int) R.string.import_items).setIcon((int) R.drawable.ic_menu_import);
        menu.add(0, 4, 1, (int) R.string.export_items).setIcon((int) R.drawable.ic_menu_export);
        menu.add(0, 6, 2, (int) R.string.filter).setIcon((int) R.drawable.ic_menu_filter);
        menu.add(0, 0, 3, (int) R.string.delete_all).setIcon(17301564);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean hasItems;
        super.onPrepareOptionsMenu(menu);
        if (getListAdapter().getCount() > 0) {
            hasItems = true;
        } else {
            hasItems = false;
        }
        menu.getItem(0).setEnabled(new File(getImportExportFilename()).exists());
        menu.getItem(1).setEnabled(hasItems);
        menu.getItem(3).setEnabled(hasItems);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                showDialog(0);
                return true;
            case 4:
                exportItems();
                break;
            case 5:
                importItems();
                break;
            case 6:
                showFilterDialog();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showFilterDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.select_filter_type);
        builder.setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setSingleChoiceItems(getFilterTypes(), this.selectedFilter + 1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                HistoryBase.this.selectedFilter = item - 1;
                HistoryBase.this.filter();
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    /* access modifiers changed from: protected */
    public void importItems() {
        confirmOverwriteAndImport(getImportExportFilename());
        showAll();
    }

    private void confirmOverwriteAndImport(final String filename) {
        if (getListAdapter().isEmpty()) {
            doImport(filename);
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage((int) R.string.import_and_overwrite).setCancelable(false).setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                HistoryBase.this.doImport(filename);
            }
        }).setNegativeButton((int) R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        builder.create().show();
    }

    /* access modifiers changed from: protected */
    public void exportItems() {
        createWwwjdicDirIfNecessary();
        confirmOverwriteAndExport(getImportExportFilename());
    }

    /* access modifiers changed from: protected */
    public void confirmOverwriteAndExport(final String filename) {
        if (!new File(filename).exists()) {
            doExport(filename);
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(String.format(getResources().getString(R.string.overwrite_file), filename)).setCancelable(false).setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                HistoryBase.this.doExport(filename);
            }
        }).setNegativeButton((int) R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        builder.create().show();
    }

    /* access modifiers changed from: private */
    public void filter() {
        Cursor c = filterCursor();
        ((CursorAdapter) getListAdapter()).changeCursor(c);
        startManagingCursor(c);
        refresh();
    }

    /* access modifiers changed from: protected */
    public void showAll() {
        this.selectedFilter = -1;
        filter();
    }

    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        try {
            if (((Cursor) getListAdapter().getItem(((AdapterView.AdapterContextMenuInfo) menuInfo).position)) != null) {
                menu.add(0, 1, 0, (int) R.string.look_up);
                menu.add(0, 2, 1, (int) R.string.copy);
                menu.add(0, 3, 2, (int) R.string.delete);
            }
        } catch (ClassCastException e) {
            Log.e(TAG, "bad menuInfo", e);
        }
    }

    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                lookupCurrentItem();
                return true;
            case 2:
                copyCurrentItem();
                return true;
            case 3:
                deleteCurrentItem();
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public Cursor getCursor() {
        return ((CursorAdapter) getListAdapter()).getCursor();
    }

    public void onResume() {
        super.onResume();
        refresh();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                return createConfirmDeleteDialog();
            default:
                return null;
        }
    }

    private Dialog createConfirmDeleteDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage((int) R.string.delete_all_iteims).setCancelable(false).setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                HistoryBase.this.deleteAll();
            }
        }).setNegativeButton((int) R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        return builder.create();
    }

    /* access modifiers changed from: protected */
    public void refresh() {
        getCursor().requery();
        ((CursorAdapter) getListAdapter()).notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public void createWwwjdicDirIfNecessary() {
        File wwwjdicDir = new File(String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + "/wwwjdic");
        if (!wwwjdicDir.exists()) {
            wwwjdicDir.mkdir();
        }
        if (!wwwjdicDir.canWrite()) {
        }
    }

    /* access modifiers changed from: protected */
    public CSVReader openImportFile(String importFile) throws FileNotFoundException {
        if (new File(importFile).exists()) {
            return new CSVReader(new FileReader(importFile));
        }
        Toast.makeText(this, String.format(getResources().getString(R.string.file_not_found), importFile), 0).show();
        return null;
    }

    /* access modifiers changed from: protected */
    public void writeBom(File exportFile) throws FileNotFoundException, IOException {
        OutputStream out = new FileOutputStream(exportFile);
        out.write(UTF8_BOM);
        out.flush();
        out.close();
    }

    /* access modifiers changed from: protected */
    public void showCopiedToast(String headword) {
        Toast.makeText(this, String.format(getResources().getString(R.string.copied_to_clipboard), headword), 0).show();
    }
}
