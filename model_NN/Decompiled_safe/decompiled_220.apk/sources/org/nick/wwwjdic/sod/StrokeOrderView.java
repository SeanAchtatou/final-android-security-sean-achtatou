package org.nick.wwwjdic.sod;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import java.util.List;

public class StrokeOrderView extends View {
    private static final float OUTLINE_WIDTH = 2.0f;
    private static final float SEGMENT_LENGTH = 20.0f;
    private boolean animate = false;
    private int animationDelayMillis;
    private boolean annotateStrokes = true;
    private StrokedCharacter character;
    private long lastTick = 0;
    private Paint outlinePaint;

    public StrokeOrderView(Context context) {
        super(context);
        init();
    }

    public StrokeOrderView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        this.outlinePaint = new Paint();
        this.outlinePaint.setColor(-7829368);
        this.outlinePaint.setStyle(Paint.Style.STROKE);
        this.outlinePaint.setAntiAlias(true);
        this.outlinePaint.setStrokeWidth(OUTLINE_WIDTH);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        drawOutline(canvas);
        if (this.character != null && this.character.hasStrokes()) {
            drawStrokePaths(canvas, this.annotateStrokes);
        }
    }

    private void drawOutline(Canvas canvas) {
        Rect r = new Rect();
        getDrawingRect(r);
        r.top += 2;
        this.outlinePaint.setPathEffect(null);
        canvas.drawRect(r, this.outlinePaint);
    }

    private void drawStrokePaths(Canvas canvas, boolean annotate) {
        int strokeNum = 1;
        float scale = 1.0f;
        float dx = 0.0f;
        float dy = 0.0f;
        int width = getWidth();
        int height = getHeight();
        int dimension = Math.min(width, height);
        if (!this.character.isTransformed()) {
            scale = ((float) dimension) / this.character.getDimension();
            RectF scaledBounds = this.character.getScaledBounds(scale);
            RectF r = new RectF(0.0f, 0.0f, (float) width, (float) height);
            dx = Math.abs(r.centerX() - scaledBounds.centerX());
            dy = Math.abs(r.centerY() - scaledBounds.centerY());
            if (r.centerX() < scaledBounds.centerX()) {
                dx *= -1.0f;
            }
            if (r.centerY() < scaledBounds.centerY()) {
                dy *= -1.0f;
            }
            this.character.setCanvasWidth(Float.valueOf((float) width));
            this.character.setCanvasHeight(Float.valueOf((float) height));
            this.character.setTransformed(true);
        }
        if (!this.animate) {
            for (StrokePath sp : this.character.getStrokes()) {
                sp.draw(canvas, scale, dx, dy, strokeNum, annotate);
                strokeNum++;
            }
            return;
        }
        this.character.segmentStrokes(scale, dx, dy, SEGMENT_LENGTH);
        boolean advance = false;
        if (System.currentTimeMillis() - this.lastTick >= ((long) this.animationDelayMillis)) {
            this.lastTick = System.currentTimeMillis();
            advance = true;
        }
        List<StrokePath> strokePaths = this.character.getStrokes();
        int i = 0;
        while (true) {
            if (i >= strokePaths.size()) {
                break;
            }
            StrokePath sp2 = strokePaths.get(i);
            if (sp2.isFullyDrawn()) {
                sp2.draw(canvas, scale, dx, dy, strokeNum, annotate);
                strokeNum++;
                if (i == strokePaths.size() - 1) {
                    this.animate = false;
                }
                i++;
            } else {
                if (advance) {
                    sp2.advanceSegment();
                }
                sp2.drawSegments(canvas);
            }
        }
        postInvalidate();
    }

    public void clear() {
        if (this.character != null) {
            this.character = null;
        }
        this.lastTick = 0;
        this.animate = false;
        invalidate();
    }

    public boolean isAnnotateStrokes() {
        return this.annotateStrokes;
    }

    public void setAnnotateStrokes(boolean annotateStrokes2) {
        this.annotateStrokes = annotateStrokes2;
    }

    public StrokedCharacter getCharacter() {
        return this.character;
    }

    public void setCharacter(StrokedCharacter character2) {
        this.character = character2;
    }

    public List<StrokePath> getStrokePaths() {
        if (this.character == null) {
            return null;
        }
        return this.character.getStrokes();
    }

    public int getAnimationDelayMillis() {
        return this.animationDelayMillis;
    }

    public void setAnimationDelayMillis(int animationDelayMillis2) {
        this.animationDelayMillis = animationDelayMillis2;
    }

    public void startAnimation() {
        this.animate = true;
        this.lastTick = 0;
        if (this.character != null) {
            this.character.resetSegments();
        }
        invalidate();
    }
}
