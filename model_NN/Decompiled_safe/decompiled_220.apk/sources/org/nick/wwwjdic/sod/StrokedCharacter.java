package org.nick.wwwjdic.sod;

import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.RectF;
import java.util.ArrayList;
import java.util.List;

public class StrokedCharacter {
    private RectF bounds;
    private Float canvasHeight;
    private Float canvasWidth;
    private boolean needsPaddding;
    private boolean segemented;
    private List<StrokePath> strokes;
    private boolean transformed;

    public StrokedCharacter() {
        this.strokes = new ArrayList();
    }

    public StrokedCharacter(List<StrokePath> strokes2, float canvasWidth2, float canvasHeight2) {
        this.strokes = strokes2;
        this.canvasHeight = Float.valueOf(canvasHeight2);
        this.canvasWidth = Float.valueOf(canvasWidth2);
    }

    public List<StrokePath> getStrokes() {
        return this.strokes;
    }

    public float getCanvasWidth() {
        if (this.canvasWidth == null) {
            return getBounds().width();
        }
        return this.canvasWidth.floatValue();
    }

    public void setCanvasWidth(Float canvasWidth2) {
        this.canvasWidth = canvasWidth2;
    }

    public float getCanvasHeight() {
        if (this.canvasHeight == null) {
            return getBounds().height();
        }
        return this.canvasHeight.floatValue();
    }

    public void setCanvasHeight(Float canvasHeight2) {
        this.canvasHeight = canvasHeight2;
    }

    public RectF getBounds() {
        if (this.bounds != null) {
            return this.bounds;
        }
        this.bounds = new RectF();
        for (StrokePath s : this.strokes) {
            RectF strokeBounds = new RectF();
            s.getStrokePath().computeBounds(strokeBounds, true);
            this.bounds.union(strokeBounds);
        }
        return this.bounds;
    }

    public RectF getScaledBounds(float scale) {
        RectF scaledBounds = new RectF();
        Matrix m = new Matrix();
        m.postScale(scale, scale);
        for (StrokePath sp : this.strokes) {
            Path p = new Path(sp.getStrokePath());
            sp.getStrokePath().transform(m, p);
            RectF b = new RectF();
            p.computeBounds(b, true);
            scaledBounds.union(b);
        }
        return scaledBounds;
    }

    public void addStroke(StrokePath stroke) {
        this.strokes.add(stroke);
    }

    public boolean hasStrokes() {
        return this.strokes != null && !this.strokes.isEmpty();
    }

    public boolean isTransformed() {
        return this.transformed;
    }

    public void setTransformed(boolean transformed2) {
        this.transformed = transformed2;
    }

    public float getDimension() {
        return Math.max(getCanvasWidth(), getCanvasHeight());
    }

    public void segmentStrokes(float scale, float dx, float dy, float segmentLength) {
        if (!this.segemented) {
            for (StrokePath sp : this.strokes) {
                sp.segmentStroke(segmentLength, scale, dx, dy);
            }
            this.segemented = true;
        }
    }

    public boolean isSegemented() {
        return this.segemented;
    }

    public void clear() {
        if (this.strokes != null) {
            this.strokes.clear();
        }
        this.canvasWidth = null;
        this.canvasHeight = null;
        this.bounds = null;
        this.transformed = false;
        this.segemented = false;
    }

    public void resetSegments() {
        for (StrokePath sp : this.strokes) {
            sp.resetSegments();
        }
        this.segemented = false;
    }

    public boolean isNeedsPaddding() {
        return this.needsPaddding;
    }

    public void setNeedsPaddding(boolean needsPaddding2) {
        this.needsPaddding = needsPaddding2;
    }
}
