package org.nick.wwwjdic.sod;

import android.graphics.PointF;

public class Curve {
    private final PointF p1;
    private final PointF p2;
    private final PointF p3;
    private final boolean relative;
    private final boolean smooth;

    public Curve(PointF p12, PointF p22, PointF p32, boolean relative2, boolean smooth2) {
        this.p1 = p12;
        this.p2 = p22;
        this.p3 = p32;
        this.relative = relative2;
        this.smooth = smooth2;
    }

    public PointF getP1() {
        return this.p1;
    }

    public PointF getP2() {
        return this.p2;
    }

    public PointF getP3() {
        return this.p3;
    }

    public boolean isRelative() {
        return this.relative;
    }

    public boolean isSmooth() {
        return this.smooth;
    }
}
