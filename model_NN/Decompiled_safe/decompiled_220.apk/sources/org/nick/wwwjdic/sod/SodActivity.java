package org.nick.wwwjdic.sod;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import au.com.bytecode.opencsv.CSVWriter;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.nick.wwwjdic.Constants;
import org.nick.wwwjdic.HttpClientFactory;
import org.nick.wwwjdic.R;
import org.nick.wwwjdic.WwwjdicPreferences;
import org.nick.wwwjdic.utils.Analytics;

public class SodActivity extends Activity implements View.OnClickListener {
    private static final String EXTRA_RIGHT_STROKE_PATHS_STRING = "org.nick.recognizer.quiz.RIGHT_STROKE_PATHS_STRING";
    private static final float KANJIVG_SIZE = 109.0f;
    private static final String NOT_FOUND_STATUS = "not found";
    private static final String STROKE_PATH_LOOKUP_URL = "http://wwwjdic-android.appspot.com/kanji/";
    /* access modifiers changed from: private */
    public static final String TAG = SodActivity.class.getSimpleName();
    private Button animateButton;
    private StrokedCharacter character;
    private Button clearButton;
    private Button drawButton;
    private GetSodTask getSodTask;
    private boolean isRotating = false;
    private String kanji;
    private ProgressDialog progressDialog;
    private StrokeOrderView strokeOrderView;
    private String strokePathsStr;
    private String unicodeNumber;

    static class GetSodTask extends AsyncTask<String, Void, String> {
        private boolean animate;
        private HttpClient httpClient = createHttpClient();
        private ResponseHandler<String> responseHandler = HttpClientFactory.createWwwjdicResponseHandler();
        private SodActivity sodActivity;

        GetSodTask(SodActivity sodActivity2, boolean animate2) {
            this.sodActivity = sodActivity2;
            this.animate = animate2;
        }

        private HttpClient createHttpClient() {
            return HttpClientFactory.createSodHttpClient(WwwjdicPreferences.getSodServerTimeout(this.sodActivity));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            if (this.sodActivity != null) {
                this.sodActivity.showProgressDialog(this.sodActivity.getResources().getString(R.string.getting_sod_info));
            }
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            try {
                String responseStr = (String) this.httpClient.execute(new HttpGet(SodActivity.STROKE_PATH_LOOKUP_URL + params[0]), this.responseHandler);
                Log.d(SodActivity.TAG, "got SOD response: " + responseStr);
                return responseStr;
            } catch (Exception e) {
                Exception e2 = e;
                Log.e(SodActivity.TAG, e2.getMessage(), e2);
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            if (this.sodActivity != null) {
                this.sodActivity.dismissProgressDialog();
                if (result != null) {
                    this.sodActivity.setStrokePathsStr(result);
                    StrokedCharacter character = SodActivity.parseWsReply(result);
                    if (character == null) {
                        Toast.makeText(this.sodActivity, String.format(this.sodActivity.getString(R.string.no_sod_data), this.sodActivity.getKanji()), 0).show();
                    } else if (this.animate) {
                        this.sodActivity.animate(character);
                    } else {
                        this.sodActivity.drawSod(character);
                    }
                } else {
                    Toast.makeText(this.sodActivity, (int) R.string.getting_sod_data_failed, 0).show();
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void attach(SodActivity sodActivity2) {
            this.sodActivity = sodActivity2;
        }

        /* access modifiers changed from: package-private */
        public void detach() {
            this.sodActivity = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.sod);
        findViews();
        this.drawButton.setOnClickListener(this);
        this.clearButton.setOnClickListener(this);
        this.animateButton.setOnClickListener(this);
        this.unicodeNumber = getIntent().getExtras().getString(Constants.KANJI_UNICODE_NUMBER);
        this.kanji = getIntent().getExtras().getString(Constants.KANJI_GLYPH);
        setTitle(String.format(getResources().getString(R.string.sod_for), this.kanji));
        this.getSodTask = (GetSodTask) getLastNonConfigurationInstance();
        if (this.getSodTask != null) {
            this.getSodTask.attach(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.getSodTask != null && !this.isRotating) {
            this.getSodTask.cancel(true);
            this.getSodTask = null;
        }
    }

    public Object onRetainNonConfigurationInstance() {
        this.isRotating = true;
        if (this.getSodTask != null) {
            this.getSodTask.detach();
        }
        return this.getSodTask;
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        Analytics.startSession(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        drawSod();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        Analytics.endSession(this);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            this.strokePathsStr = savedInstanceState.getString(EXTRA_RIGHT_STROKE_PATHS_STRING);
            this.character = parseWsReply(this.strokePathsStr);
            if (this.character != null) {
                this.strokeOrderView.setCharacter(this.character);
                this.strokeOrderView.invalidate();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(EXTRA_RIGHT_STROKE_PATHS_STRING, this.strokePathsStr);
    }

    /* access modifiers changed from: package-private */
    public void drawSod(StrokedCharacter character2) {
        this.character = character2;
        this.strokeOrderView.setCharacter(character2);
        this.strokeOrderView.setAnnotateStrokes(true);
        this.strokeOrderView.invalidate();
    }

    /* access modifiers changed from: package-private */
    public void animate(StrokedCharacter character2) {
        this.character = character2;
        this.strokeOrderView.setAnimationDelayMillis(WwwjdicPreferences.getStrokeAnimationDelay(this));
        this.strokeOrderView.setCharacter(character2);
        this.strokeOrderView.setAnnotateStrokes(true);
        this.strokeOrderView.startAnimation();
    }

    private void findViews() {
        this.drawButton = (Button) findViewById(R.id.draw_sod_button);
        this.clearButton = (Button) findViewById(R.id.clear_sod_button);
        this.animateButton = (Button) findViewById(R.id.animate_button);
        this.strokeOrderView = (StrokeOrderView) findViewById(R.id.sod_draw_view);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.draw_sod_button:
                drawSod();
                return;
            case R.id.animate_button:
                animate();
                return;
            case R.id.clear_sod_button:
                this.strokeOrderView.clear();
                this.strokeOrderView.invalidate();
                return;
            default:
                return;
        }
    }

    private void drawSod() {
        Analytics.event("drawSod", this);
        if (this.character == null) {
            getStrokes();
        } else {
            drawSod(this.character);
        }
    }

    private void getStrokes() {
        if (this.getSodTask != null) {
            this.getSodTask.cancel(true);
        }
        this.getSodTask = new GetSodTask(this, false);
        this.getSodTask.execute(this.unicodeNumber);
    }

    private void animate() {
        Analytics.event("animateSod", this);
        if (this.character == null) {
            getStrokes();
        } else {
            animate(this.character);
        }
    }

    private static List<StrokePath> parseWsReplyStrokes(String reply) {
        if (reply == null || "".equals(reply)) {
            return null;
        }
        if (reply.startsWith(NOT_FOUND_STATUS)) {
            return null;
        }
        String[] lines = reply.split(CSVWriter.DEFAULT_LINE_END);
        List<StrokePath> result = new ArrayList<>();
        for (int i = 1; i < lines.length; i++) {
            String line = lines[i];
            if (line != null && !"".equals(line)) {
                result.add(StrokePath.parsePath(line.trim()));
            }
        }
        return result;
    }

    /* access modifiers changed from: private */
    public static StrokedCharacter parseWsReply(String reply) {
        List<StrokePath> strokes = parseWsReplyStrokes(reply);
        if (strokes == null) {
            return null;
        }
        return new StrokedCharacter(strokes, KANJIVG_SIZE, KANJIVG_SIZE);
    }

    /* access modifiers changed from: package-private */
    public String getKanji() {
        return this.kanji;
    }

    /* access modifiers changed from: package-private */
    public String getStrokePathsStr() {
        return this.strokePathsStr;
    }

    /* access modifiers changed from: package-private */
    public void setStrokePathsStr(String strokePathsStr2) {
        this.strokePathsStr = strokePathsStr2;
    }

    /* access modifiers changed from: package-private */
    public void showProgressDialog(String message) {
        this.progressDialog = ProgressDialog.show(this, "", message, true);
    }

    /* access modifiers changed from: package-private */
    public void dismissProgressDialog() {
        if (this.progressDialog != null && this.progressDialog.isShowing() && !isFinishing()) {
            this.progressDialog.dismiss();
            this.progressDialog = null;
        }
    }
}
