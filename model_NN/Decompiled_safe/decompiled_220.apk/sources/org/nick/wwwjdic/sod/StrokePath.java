package org.nick.wwwjdic.sod;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PointF;
import android.util.Log;
import android.util.Xml;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import org.acra.ErrorReporter;
import org.xmlpull.v1.XmlPullParser;

public class StrokePath {
    private static final float STROKE_WIDTH = 6.0f;
    private static final String TAG = StrokePath.class.getSimpleName();
    private int currentSegment;
    private List<Curve> curves = new ArrayList();
    private PointF moveTo;
    private boolean pathScaled = false;
    private List<Path> segments;
    private Paint strokeAnnotationPaint;
    private Paint strokePaint;
    private Path strokePath;
    private float translationDx = -1.0f;
    private float translationDy = -1.0f;

    public StrokePath(PointF moveTo2) {
        this.moveTo = moveTo2;
        initPaints();
    }

    private void initPaints() {
        this.strokePaint = new Paint();
        this.strokePaint.setColor(-1);
        this.strokePaint.setStyle(Paint.Style.STROKE);
        this.strokePaint.setAntiAlias(true);
        this.strokePaint.setStrokeWidth(STROKE_WIDTH);
        this.strokeAnnotationPaint = new Paint();
        this.strokeAnnotationPaint.setColor(-16711936);
        this.strokeAnnotationPaint.setStyle(Paint.Style.FILL);
        this.strokeAnnotationPaint.setAntiAlias(true);
        this.strokeAnnotationPaint.setStrokeWidth(4.0f);
    }

    public void addCurve(Curve curve) {
        this.curves.add(curve);
    }

    public PointF getMoveTo() {
        return this.moveTo;
    }

    public List<Curve> getCurves() {
        return this.curves;
    }

    public void draw(Canvas canvas, float scale, float dx, float dy, int strokeNum, boolean annotate) {
        Matrix matrix = new Matrix();
        if (this.strokePath != null) {
            if (!this.pathScaled) {
                matrix.postScale(scale, scale);
                this.pathScaled = true;
            }
            if (needsTranslation(dx, dy)) {
                matrix.postTranslate(dx, dy);
                this.translationDx = dx;
                this.translationDy = dy;
            }
            transformMoveTo(matrix);
            this.strokePath.transform(matrix);
            if (annotate) {
                annotate(canvas, strokeNum);
            }
        } else {
            this.strokePath = curvesToPath();
            matrix.postScale(scale, scale);
            matrix.postTranslate(dx, dy);
            transformMoveTo(matrix);
            this.strokePath.transform(matrix);
            this.pathScaled = true;
            this.translationDx = dx;
            this.translationDy = dy;
            if (annotate) {
                annotate(canvas, strokeNum);
            }
        }
        canvas.drawPath(this.strokePath, this.strokePaint);
    }

    private void transformMoveTo(Matrix matrix) {
        float[] cs = {this.moveTo.x, this.moveTo.y};
        matrix.mapPoints(cs);
        this.moveTo.x = cs[0];
        this.moveTo.y = cs[1];
    }

    private boolean needsTranslation(float dx, float dy) {
        return ((dx == 0.0f || dy == 0.0f || dx == this.translationDx) && dy == this.translationDy) ? false : true;
    }

    private Path curvesToPath() {
        PointF p1;
        PointF p2;
        PointF p3;
        Path path = new Path();
        path.moveTo(this.moveTo.x, this.moveTo.y);
        path.setFillType(Path.FillType.WINDING);
        PointF lastPoint = this.moveTo;
        PointF lastP2 = null;
        int idx = 0;
        for (Curve c : this.curves) {
            if (c.isRelative()) {
                p1 = calcAbsolute(lastPoint, c.getP1());
                p2 = calcAbsolute(lastPoint, c.getP2());
                p3 = calcAbsolute(lastPoint, c.getP3());
            } else {
                p1 = c.getP1();
                p2 = c.getP2();
                p3 = c.getP3();
            }
            if (c.isSmooth()) {
                p1 = calcReflectionRelToCurrent(lastP2, lastPoint);
            }
            path.cubicTo(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y);
            if (c.isRelative()) {
                lastP2 = new PointF(lastPoint.x + c.getP2().x, lastPoint.y + c.getP2().y);
                lastPoint = new PointF(lastPoint.x + c.getP3().x, lastPoint.y + c.getP3().y);
            } else {
                lastPoint = c.getP3();
                lastP2 = c.getP2();
            }
            idx++;
        }
        return path;
    }

    private void annotate(Canvas canvas, int strokeNum) {
        canvas.drawText(Integer.toString(strokeNum), 7.0f + this.moveTo.x, this.moveTo.y - 9.0f, this.strokeAnnotationPaint);
    }

    private Path getStrokePath(float scale) {
        Matrix matrix = new Matrix();
        if (this.strokePath == null) {
            this.strokePath = curvesToPath();
            matrix.postScale(scale, scale);
            this.strokePath.transform(matrix);
            this.pathScaled = true;
        } else if (!this.pathScaled) {
            matrix.postScale(scale, scale);
            this.strokePath.transform(matrix);
            this.pathScaled = true;
        }
        return this.strokePath;
    }

    public void segmentStroke(float segmentLength, float scale, float dx, float dy) {
        this.segments = segmentPath(getStrokePath(scale), segmentLength, scale, dx, dy);
        this.currentSegment = 0;
    }

    private List<Path> segmentPath(Path path, float segmentLength, float scale, float dx, float dy) {
        PathMeasure pm = new PathMeasure(path, false);
        float length = pm.getLength();
        float delta = segmentLength;
        List<Path> segments2 = new ArrayList<>();
        for (float start = 0.0f; start <= length; start += delta) {
            float end = start + delta;
            if (end > length) {
                end = length;
            }
            Path segment = new Path();
            pm.getSegment(start, end, segment, true);
            if (needsTranslation(dx, dy)) {
                Matrix matrix = new Matrix();
                matrix.postTranslate(dx, dy);
                segment.transform(matrix);
                this.translationDx = dx;
                this.translationDy = dy;
            }
            segments2.add(segment);
        }
        return segments2;
    }

    private PointF calcAbsolute(PointF currentPoint, PointF p) {
        if (p == null) {
            return null;
        }
        return new PointF(p.x + currentPoint.x, p.y + currentPoint.y);
    }

    private PointF calcReflectionRelToCurrent(PointF p, PointF currentPoint) {
        return new PointF((float) ((((double) currentPoint.x) * 2.0d) - ((double) p.x)), (float) ((((double) currentPoint.y) * 2.0d) - ((double) p.y)));
    }

    /* JADX INFO: Multiple debug info for r12v2 java.lang.Float: [D('x' java.lang.Float), D('y' java.lang.Float)] */
    /* JADX INFO: Multiple debug info for r4v11 org.acra.ErrorReporter: [D('p1' android.graphics.PointF), D('er' org.acra.ErrorReporter)] */
    /* JADX INFO: Multiple debug info for r9v22 java.lang.Float: [D('f' float), D('y' java.lang.Float)] */
    public static StrokePath parsePath(String path) {
        StringBuffer buff;
        Float x;
        Float x2;
        Float y;
        Float x3;
        StrokePath result;
        Log.d(TAG, "parsing " + path);
        String floatStr = null;
        StringBuffer buff2 = new StringBuffer();
        Float x4 = null;
        Float y2 = null;
        PointF p1 = null;
        PointF p2 = null;
        PointF p3 = null;
        StrokePath result2 = null;
        boolean relative = false;
        boolean smooth = false;
        int i = 0;
        while (true) {
            int i2 = i;
            StrokePath result3 = result2;
            String isInMoveTo = floatStr;
            Float x5 = x4;
            Float y3 = y2;
            if (i2 >= path.length()) {
                return result3;
            }
            char c = path.charAt(i2);
            if (c == 'M' || c == 'm') {
                y2 = y3;
                floatStr = 1;
                x4 = x5;
                result2 = result3;
            } else {
                if (Character.isDigit(c) || c == '.') {
                    buff2.append(Character.toString(c));
                }
                if (c == ',' || c == '-' || c == 'c' || c == 'C' || c == 's' || c == 'S' || i2 == path.length() - 1) {
                    String floatStr2 = buff2.toString();
                    buff2 = new StringBuffer();
                    if (c == '-') {
                        buff2.append(c);
                    }
                    if ("".equals(floatStr2)) {
                        y2 = y3;
                        floatStr = isInMoveTo;
                        x4 = x5;
                        result2 = result3;
                    } else {
                        try {
                            float f = Float.parseFloat(floatStr2);
                            if (x5 == null) {
                                x2 = Float.valueOf(f);
                                x = y3;
                                buff = buff2;
                            } else {
                                x = Float.valueOf(f);
                                buff = buff2;
                                x2 = x5;
                            }
                        } catch (NumberFormatException e) {
                            ErrorReporter er = ErrorReporter.getInstance();
                            er.putCustomData("pathStr", path);
                            er.putCustomData("floatStr", floatStr2);
                            er.handleSilentException(e);
                            throw e;
                        }
                    }
                } else {
                    x = y3;
                    x2 = x5;
                    buff = buff2;
                }
                if (x2 == null || x == null) {
                    y = x;
                    x3 = x2;
                    result = result3;
                } else {
                    PointF p = new PointF(x2.floatValue(), x.floatValue());
                    if (isInMoveTo != null) {
                        result = new StrokePath(p);
                        y = null;
                        x3 = null;
                    } else {
                        if (p1 == null) {
                            p1 = p;
                        } else if (p1 != null && p2 == null) {
                            p2 = p;
                        } else if (!(p1 == null || p2 == null || p3 != null)) {
                            p3 = p;
                        }
                        if (!smooth) {
                            if (!(p1 == null || p2 == null || p3 == null)) {
                                result3.addCurve(new Curve(p1, p2, p3, relative, smooth));
                                p1 = null;
                                p2 = null;
                                p3 = null;
                                result = result3;
                                y = null;
                                x3 = null;
                            }
                        } else if (!(p1 == null || p2 == null)) {
                            result3.addCurve(new Curve(null, p1, p2, relative, smooth));
                            p1 = null;
                            p2 = null;
                            p3 = null;
                            result = result3;
                            y = null;
                            x3 = null;
                        }
                        result = result3;
                        y = null;
                        x3 = null;
                    }
                }
                if (c == 'c' || c == 'C' || c == 's' || c == 'S') {
                    relative = c == 'c' || c == 's';
                    smooth = c == 's' || c == 'S';
                    y2 = y;
                    x4 = x3;
                    result2 = result;
                    floatStr = null;
                    buff2 = buff;
                } else {
                    y2 = y;
                    x4 = x3;
                    buff2 = buff;
                    result2 = result;
                    floatStr = isInMoveTo;
                }
            }
            i = i2 + 1;
        }
    }

    public static List<StrokePath> parseKangiVgXml(File f) {
        List<StrokePath> strokes = new ArrayList<>();
        parseKangiVgXml(f, strokes);
        return strokes;
    }

    public static void tryParseKangiVgXml(File f) {
        parseKangiVgXml(f, null);
    }

    private static void parseKangiVgXml(File f, List<StrokePath> strokePaths) {
        XmlPullParser parser = Xml.newPullParser();
        try {
            parser.setInput(new FileInputStream(f), null);
            for (int eventType = parser.getEventType(); eventType != 1 && 0 == 0; eventType = parser.next()) {
                switch (eventType) {
                    case 2:
                        String name = parser.getName();
                        if (name.equalsIgnoreCase("stroke")) {
                            String path = parser.getAttributeValue(null, "path");
                            Log.d(TAG, "parsing " + path);
                            if (path != null && !"".equals(path)) {
                                StrokePath strokePath2 = parsePath(path);
                                if (strokePaths != null) {
                                    strokePaths.add(strokePath2);
                                }
                            }
                        }
                        if (!name.equalsIgnoreCase("kanji")) {
                            break;
                        } else {
                            Log.d(TAG, parser.getAttributeValue(null, "id"));
                            break;
                        }
                    case 3:
                        String name2 = parser.getName();
                        break;
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public List<Path> getSegments() {
        return this.segments;
    }

    public void advanceSegment() {
        this.currentSegment++;
    }

    public void resetSegments() {
        if (this.segments != null) {
            this.segments.clear();
        }
        this.currentSegment = 0;
    }

    public boolean isFullyDrawn() {
        if (this.segments == null) {
            return true;
        }
        return this.currentSegment == this.segments.size() - 1;
    }

    public boolean isSegmented() {
        return this.segments != null && !this.segments.isEmpty();
    }

    public void drawSegments(Canvas canvas) {
        if (!this.segments.isEmpty()) {
            Path linkedPath = new Path();
            for (int i = 0; i <= this.currentSegment; i++) {
                linkedPath.addPath(this.segments.get(i));
            }
            canvas.drawPath(linkedPath, this.strokePaint);
        }
    }

    public Path getStrokePath() {
        if (this.strokePath == null) {
            this.strokePath = curvesToPath();
        }
        return this.strokePath;
    }

    public void setStrokePath(Path strokePath2) {
        this.strokePath = strokePath2;
    }

    public void reset() {
        this.strokePath = null;
        this.pathScaled = false;
        this.translationDx = -1.0f;
        this.translationDy = -1.0f;
    }

    public boolean isScaled() {
        return this.pathScaled;
    }
}
