package org.nick.wwwjdic;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import org.nick.wwwjdic.utils.Analytics;
import org.nick.wwwjdic.utils.Pair;

public class KanjiEntryDetail extends DetailActivity implements View.OnClickListener {
    private static final List<String> ELEMENTARY_GRADES = Arrays.asList("1", "2", "3", "4", "5", "6");
    private static final List<String> JINMEIYOU_GRADES = Arrays.asList("9", "10");
    private static final List<String> SECONDARY_GRADES = Arrays.asList("8");
    private static final String TAG = KanjiEntryDetail.class.getSimpleName();
    private KanjiEntry entry;
    private LinearLayout translationsCodesLayout;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kanji_entry_details);
        checkTtsAvailability();
        this.entry = (KanjiEntry) getIntent().getSerializableExtra(Constants.KANJI_ENTRY_KEY);
        this.wwwjdicEntry = this.entry;
        this.isFavorite = getIntent().getBooleanExtra(Constants.IS_FAVORITE, false);
        if (getIntent().getBooleanExtra(Constants.KOD_WIDGET_CLICK, false)) {
            Analytics.startSession(this);
            Analytics.event("kodWidgetClicked", this);
        }
        setTitle(String.format(getResources().getString(R.string.details_for), this.entry.getKanji()));
        TextView entryView = (TextView) findViewById(R.id.kanjiText);
        entryView.setText(this.entry.getKanji());
        entryView.setOnLongClickListener(this);
        TextView radicalGlyphText = (TextView) findViewById(R.id.radicalGlyphText);
        Radical radical = Radicals.getInstance().getRadicalByNumber(this.entry.getRadicalNumber());
        if (radical != null) {
            radicalGlyphText.setText(radical.getGlyph().substring(0, 1));
        }
        ((TextView) findViewById(R.id.radicalNumberText)).setText(Integer.toString(this.entry.getRadicalNumber()));
        ((TextView) findViewById(R.id.strokeCountText)).setText(Integer.toString(this.entry.getStrokeCount()));
        Button sodButton = (Button) findViewById(R.id.sod_button);
        sodButton.setOnClickListener(this);
        sodButton.setNextFocusDownId(R.id.compound_link_starting);
        TextView compoundsLinkStarting = (TextView) findViewById(R.id.compound_link_starting);
        compoundsLinkStarting.setNextFocusDownId(R.id.compound_link_any);
        compoundsLinkStarting.setNextFocusUpId(R.id.sod_button);
        makeClickable(compoundsLinkStarting, createCompoundSearchIntent(1, false));
        TextView compoundsLinkAny = (TextView) findViewById(R.id.compound_link_any);
        compoundsLinkAny.setNextFocusDownId(R.id.compound_link_common);
        compoundsLinkAny.setNextFocusUpId(R.id.compound_link_starting);
        makeClickable(compoundsLinkAny, createCompoundSearchIntent(2, false));
        TextView compoundsLinkCommon = (TextView) findViewById(R.id.compound_link_common);
        compoundsLinkCommon.setNextFocusUpId(R.id.compound_link_any);
        makeClickable(compoundsLinkCommon, createCompoundSearchIntent(0, true));
        ((ScrollView) findViewById(R.id.meaningsScroll)).setNextFocusUpId(R.id.compound_link_common);
        LinearLayout readingLayout = (LinearLayout) findViewById(R.id.readingLayout);
        if (this.entry.getReading() != null) {
            TextView textView = new TextView(this, null, R.style.dict_detail_reading);
            textView.setText(this.entry.getOnyomi());
            readingLayout.addView(textView);
            TextView textView2 = new TextView(this, null, R.style.dict_detail_reading);
            textView2.setText(this.entry.getKunyomi());
            readingLayout.addView(textView2);
        }
        if (!TextUtils.isEmpty(this.entry.getNanori())) {
            LinearLayout linearLayout = new LinearLayout(this);
            linearLayout.setOrientation(0);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
            layoutParams.setMargins(0, 0, 5, 0);
            TextView textView3 = new TextView(this);
            textView3.setText(R.string.nanori_label);
            textView3.setTextSize(10.0f);
            textView3.setGravity(17);
            linearLayout.addView(textView3, layoutParams);
            TextView textView4 = new TextView(this, null, R.style.dict_detail_reading);
            textView4.setText(this.entry.getNanori());
            linearLayout.addView(textView4, layoutParams);
            readingLayout.addView(linearLayout);
        }
        if (!TextUtils.isEmpty(this.entry.getRadicalName())) {
            LinearLayout linearLayout2 = new LinearLayout(this);
            linearLayout2.setOrientation(0);
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
            layoutParams2.setMargins(0, 0, 5, 0);
            TextView textView5 = new TextView(this);
            textView5.setText(R.string.radical_name_label);
            textView5.setTextSize(10.0f);
            textView5.setGravity(17);
            linearLayout2.addView(textView5, layoutParams2);
            TextView textView6 = new TextView(this, null, R.style.dict_detail_reading);
            textView6.setText(this.entry.getRadicalName());
            linearLayout2.addView(textView6, layoutParams2);
            readingLayout.addView(linearLayout2);
        }
        this.translationsCodesLayout = (LinearLayout) findViewById(R.id.translations_layout);
        if (this.entry.getMeanings().isEmpty()) {
            this.translationsCodesLayout.addView(new TextView(this, null, R.style.dict_detail_meaning));
        } else {
            for (String meaning : this.entry.getMeanings()) {
                Pair<LinearLayout, TextView> translationViews = createMeaningTextView(this, meaning);
                Matcher m = CROSS_REF_PATTERN.matcher(meaning);
                if (m.matches()) {
                    Intent crossRefIntent = createCrossRefIntent(m.group(1));
                    makeClickable(translationViews.getSecond(), m.start(1), m.end(1), crossRefIntent);
                }
                this.translationsCodesLayout.addView(translationViews.getFirst());
            }
        }
        TextView textView7 = new TextView(this);
        textView7.setText(R.string.codes_more);
        textView7.setTextColor(-1);
        textView7.setBackgroundColor(-7829368);
        this.translationsCodesLayout.addView(textView7);
        addCodesTable(this.translationsCodesLayout, createCodesData(this.entry));
        CheckBox starCb = (CheckBox) findViewById(R.id.star_kanji);
        starCb.setOnCheckedChangeListener(null);
        starCb.setChecked(this.isFavorite);
        starCb.setOnCheckedChangeListener(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.tts != null) {
            this.tts.shutdown();
        }
    }

    private void addCodesTable(LinearLayout meaningsCodesLayout, List<Pair<String, String>> codesData) {
        TableLayout table = new TableLayout(this);
        for (Pair<String, String> codesEntry : codesData) {
            TableRow row = new TableRow(this);
            TextView labelView = new TextView(this);
            labelView.setText((CharSequence) codesEntry.getFirst());
            labelView.setGravity(3);
            row.addView(labelView);
            TextView textView = new TextView(this);
            textView.setText((CharSequence) codesEntry.getSecond());
            textView.setGravity(3);
            textView.setPadding(10, 0, 0, 0);
            row.addView(textView);
            table.addView(row);
        }
        meaningsCodesLayout.addView(table, new LinearLayout.LayoutParams(-1, -1));
    }

    private Intent createCrossRefIntent(String kanji) {
        SearchCriteria criteria = SearchCriteria.createForKanjiOrReading(kanji);
        Intent intent = new Intent(this, KanjiResultListView.class);
        intent.putExtra(Constants.CRITERIA_KEY, criteria);
        return intent;
    }

    private Intent createCompoundSearchIntent(int searchType, boolean commonWordsOnly) {
        String dictionary = getApp().getCurrentDictionary();
        Log.d(TAG, String.format("Will look for compounds in dictionary: %s(%s)", getApp().getCurrentDictionaryName(), dictionary));
        SearchCriteria criteria = SearchCriteria.createForKanjiCompounds(this.entry.getKanji(), searchType, commonWordsOnly, dictionary);
        Intent intent = new Intent(this, DictionaryResultListView.class);
        intent.putExtra(Constants.CRITERIA_KEY, criteria);
        return intent;
    }

    private void makeClickable(TextView textView, Intent intent) {
        makeClickable(textView, 0, textView.getText().length(), intent);
    }

    private List<Pair<String, String>> createCodesData(KanjiEntry entry2) {
        ArrayList<Pair<String, String>> data = new ArrayList<>();
        if (entry2.getUnicodeNumber() != null) {
            data.add(new Pair(getStr(R.string.unicode_number), entry2.getUnicodeNumber().toUpperCase()));
        }
        if (entry2.getJisCode() != null) {
            data.add(new Pair(getStr(R.string.jis_code), entry2.getJisCode().toUpperCase()));
        }
        String kanji = entry2.getHeadword();
        try {
            byte[] sjis = kanji.getBytes("SJIS");
            if (sjis.length < 2) {
                Log.w(TAG, "Unable to encode " + kanji + " as SJIS");
                data.add(new Pair(getStr(R.string.sjis_code), "N/A"));
            } else {
                data.add(new Pair(getStr(R.string.sjis_code), String.format("%02x%02x", Byte.valueOf(sjis[0]), Byte.valueOf(sjis[1])).toUpperCase()));
            }
        } catch (UnsupportedEncodingException e) {
            Log.w(TAG, "SJIS conversion not supported", e);
        }
        if (entry2.getClassicalRadicalNumber() != null) {
            data.add(new Pair(getStr(R.string.classical_radical), entry2.getClassicalRadicalNumber().toString()));
        }
        if (entry2.getFrequncyeRank() != null) {
            data.add(new Pair(getStr(R.string.freq_rank), entry2.getFrequncyeRank().toString()));
        }
        if (entry2.getGrade() != null) {
            String rawGrade = entry2.getGrade().toString();
            String gradeStr = rawGrade;
            if (ELEMENTARY_GRADES.contains(rawGrade)) {
                gradeStr = getResources().getString(R.string.grade_elementary, rawGrade);
            } else if (SECONDARY_GRADES.contains(rawGrade)) {
                gradeStr = getResources().getString(R.string.grade_secondary);
            } else if (JINMEIYOU_GRADES.contains(rawGrade)) {
                gradeStr = getResources().getString(R.string.grade_jinmeiyou);
            }
            data.add(new Pair(getStr(R.string.grade), gradeStr));
        }
        if (entry2.getJlptLevel() != null) {
            String newLevel = JlptLevels.getInstance().getLevel(entry2.getKanji());
            String levelStr = entry2.getJlptLevel().toString();
            if (newLevel != null) {
                levelStr = String.valueOf(levelStr) + " (" + newLevel + ")";
            }
            data.add(new Pair(getStr(R.string.jlpt_level), levelStr));
        }
        if (entry2.getSkipCode() != null) {
            data.add(new Pair(getStr(R.string.skip_code), entry2.getSkipCode()));
        }
        if (entry2.getKoreanReading() != null) {
            data.add(new Pair(getStr(R.string.korean_reading), entry2.getKoreanReading()));
        }
        if (entry2.getPinyin() != null) {
            data.add(new Pair(getStr(R.string.pinyn), entry2.getPinyin()));
        }
        return data;
    }

    private String getStr(int id) {
        return getResources().getText(id).toString();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sod_button /*2131296337*/:
                Activities.showStrokeOrder(this, this.entry);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void setHomeActivityExtras(Intent homeActivityIntent) {
        homeActivityIntent.putExtra(Constants.SELECTED_TAB_IDX, 1);
    }

    /* access modifiers changed from: protected */
    public Locale getSpeechLocale() {
        return Locale.ENGLISH;
    }

    /* access modifiers changed from: protected */
    public void showTtsButtons() {
        toggleTtsButtons(true);
    }

    /* access modifiers changed from: protected */
    public void hideTtsButtons() {
        toggleTtsButtons(false);
    }

    private void toggleTtsButtons(boolean show) {
        int childCount = this.translationsCodesLayout.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View view = this.translationsCodesLayout.getChildAt(i);
            if (view instanceof Button) {
                if (show) {
                    view.setVisibility(0);
                } else {
                    view.setVisibility(4);
                }
            } else if (view instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) view;
                int count = vg.getChildCount();
                for (int j = 0; j < count; j++) {
                    View view2 = vg.getChildAt(j);
                    if (view2 instanceof Button) {
                        if (show) {
                            view2.setVisibility(0);
                        } else {
                            view2.setVisibility(4);
                        }
                    }
                }
            }
        }
    }
}
