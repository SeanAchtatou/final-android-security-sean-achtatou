package org.nick.wwwjdic;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TabActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;
import org.nick.wwwjdic.utils.Analytics;

public class Wwwjdic extends TabActivity {
    private static final String DICTIONARY_TAB = "dictionaryTab";
    private static final String DONATE_VERSION_PACKAGE = "org.nick.wwwjdic.donate";
    private static final int DONATION_THANKS_DIALOG_ID = 2;
    private static final String EXAMPLE_SEARCH_TAB = "exampleSearchTab";
    private static final String KANJI_TAB = "kanjiTab";
    private static final int WHATS_NEW_DIALOG_ID = 1;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.main);
        setupTabs();
        if (!isDonateVersion() || WwwjdicPreferences.isDonationThanksShown(this)) {
            showWhatsNew();
        }
        showDonationThanks();
    }

    private void showDonationThanks() {
        if (isDonateVersion() && !WwwjdicPreferences.isDonationThanksShown(this)) {
            WwwjdicPreferences.setDonationThanksShown(this);
            showDialog(2);
        }
    }

    private boolean isDonateVersion() {
        return DONATE_VERSION_PACKAGE.equals(getApplication().getPackageName());
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        Analytics.startSession(this);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        Analytics.endSession(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    public void showWhatsNew() {
        if (!WwwjdicPreferences.isWhatsNewShown(this, getVersionName())) {
            WwwjdicPreferences.setWhantsNewShown(this, getVersionName());
            showDialog(1);
        }
    }

    private void setupTabs() {
        TabHost tabHost = getTabHost();
        Bundle extras = getIntent().getExtras();
        Intent dictionaryIntent = new Intent(this, Dictionary.class);
        if (extras != null) {
            dictionaryIntent.putExtras(extras);
        }
        tabHost.addTab(tabHost.newTabSpec(DICTIONARY_TAB).setIndicator(getResources().getText(R.string.dictionary), getResources().getDrawable(R.drawable.ic_tab_dict)).setContent(dictionaryIntent));
        Intent kanjiLookup = new Intent(this, KanjiLookup.class);
        if (extras != null) {
            kanjiLookup.putExtras(extras);
        }
        tabHost.addTab(tabHost.newTabSpec(KANJI_TAB).setIndicator(getResources().getText(R.string.kanji_lookup), getResources().getDrawable(R.drawable.ic_tab_kanji)).setContent(kanjiLookup));
        Intent exampleSearch = new Intent(this, ExampleSearch.class);
        if (extras != null) {
            exampleSearch.putExtras(extras);
        }
        tabHost.addTab(tabHost.newTabSpec(EXAMPLE_SEARCH_TAB).setIndicator(getResources().getText(R.string.example_search), getResources().getDrawable(R.drawable.ic_tab_example)).setContent(exampleSearch));
        tabHost.setCurrentTab(0);
        if (extras != null) {
            int selectedTab = extras.getInt(Constants.SELECTED_TAB_IDX, -1);
            if (selectedTab != -1) {
                tabHost.setCurrentTab(selectedTab);
            }
            String searchKey = extras.getString(Constants.SEARCH_TEXT_KEY);
            int searchType = extras.getInt(Constants.SEARCH_TYPE);
            if (searchKey != null) {
                switch (searchType) {
                    case 0:
                        tabHost.setCurrentTab(0);
                        return;
                    case 1:
                        tabHost.setCurrentTab(1);
                        return;
                    case 2:
                        tabHost.setCurrentTab(2);
                        return;
                    default:
                        return;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return createWhatsNewDialog();
            case 2:
                return createDonationThanksDialog();
            default:
                return null;
        }
    }

    private Dialog createDonationThanksDialog() {
        return createInfoDialog(R.string.donation_thanks_title, R.string.donation_thanks, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Wwwjdic.this.showWhatsNew();
            }
        });
    }

    private Dialog createWhatsNewDialog() {
        return createInfoDialog(R.string.whats_new_title, R.string.whats_new, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
    }

    private Dialog createInfoDialog(int titleId, int messageId, DialogInterface.OnClickListener okAction) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(String.format(getResources().getString(titleId), getVersionName()));
        builder.setIcon(17301659);
        builder.setMessage(messageId);
        builder.setPositiveButton((int) R.string.ok, okAction);
        return builder.create();
    }

    private String getVersionName() {
        return WwwjdicApplication.getVersion();
    }
}
