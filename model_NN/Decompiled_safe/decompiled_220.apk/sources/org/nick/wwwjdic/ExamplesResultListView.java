package org.nick.wwwjdic;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.ClipboardManager;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import java.util.List;
import org.nick.wwwjdic.utils.Analytics;

public class ExamplesResultListView extends ResultListViewBase<ExampleSentence> {
    private static final String EXAMPLE_SEARCH_QUERY_STR = "?11";
    public static String EXTRA_EXAMPLES_BACKDOOR_SEARCH = "orgk.nick.wwwjdic.EXAMPLES_BACKDOOR_SEARCH";
    private static final int MENU_ITEM_BREAK_DOWN = 0;
    private static final int MENU_ITEM_COPY_ENG = 3;
    private static final int MENU_ITEM_COPY_JP = 2;
    private static final int MENU_ITEM_LOOKUP_ALL_KANJI = 1;
    private static final String TAG = ExamplesResultListView.class.getSimpleName();
    private ClipboardManager clipboardManager;
    /* access modifiers changed from: private */
    public List<ExampleSentence> sentences;

    static class ExampleSentenceAdapter extends BaseAdapter {
        private final Context context;
        private final List<ExampleSentence> entries;
        private final String queryString;

        public ExampleSentenceAdapter(Context context2, List<ExampleSentence> entries2, String queryString2) {
            this.context = context2;
            this.entries = entries2;
            this.queryString = queryString2;
        }

        public int getCount() {
            if (this.entries == null) {
                return 0;
            }
            return this.entries.size();
        }

        /* Debug info: failed to restart local var, previous not found, register: 1 */
        public Object getItem(int position) {
            if (this.entries == null) {
                return null;
            }
            return this.entries.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ExampleSentence entry = this.entries.get(position);
            if (convertView == null) {
                convertView = new ExampleSentenceView(this.context);
            }
            ((ExampleSentenceView) convertView).populate(entry, this.queryString);
            return convertView;
        }

        static class ExampleSentenceView extends LinearLayout {
            private static final int HILIGHT_COLOR = -12420393;
            private TextView englishSentenceText = ((TextView) findViewById(R.id.englishSentenceText));
            private TextView japaneseSentenceText = ((TextView) findViewById(R.id.japaneseSentenceText));

            ExampleSentenceView(Context context) {
                super(context);
                LayoutInflater.from(context).inflate((int) R.layout.example_sentence_item, this);
            }

            /* access modifiers changed from: package-private */
            public void populate(ExampleSentence sentence, String queryString) {
                SpannableString japanese;
                SpannableString english = markQueryString(sentence.getEnglish(), queryString, true);
                if (sentence.getMatches().isEmpty()) {
                    japanese = markQueryString(sentence.getJapanese(), queryString, false);
                } else {
                    japanese = new SpannableString(sentence.getJapanese());
                    for (String match : sentence.getMatches()) {
                        markQueryString(japanese, sentence.getJapanese(), match, false);
                    }
                }
                this.japaneseSentenceText.setText(japanese);
                this.englishSentenceText.setText(english);
            }

            private SpannableString markQueryString(String sentenceStr, String queryString, boolean italicize) {
                SpannableString result = new SpannableString(sentenceStr);
                markQueryString(result, sentenceStr, queryString, italicize);
                return result;
            }

            private void markQueryString(SpannableString result, String sentenceStr, String queryString, boolean italicize) {
                String sentenceUpper = sentenceStr.toUpperCase();
                String queryUpper = queryString.toUpperCase();
                int idx = sentenceUpper.indexOf(queryUpper);
                while (idx != -1) {
                    result.setSpan(new ForegroundColorSpan((int) HILIGHT_COLOR), idx, queryString.length() + idx, 0);
                    if (italicize) {
                        result.setSpan(new StyleSpan(2), idx, queryString.length() + idx, 0);
                    }
                    if (queryString.length() + idx + 1 <= sentenceStr.length() - 1) {
                        idx = sentenceUpper.indexOf(queryUpper, idx + 1);
                    } else {
                        return;
                    }
                }
            }
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        SearchTask<ExampleSentence> searchTask;
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.search_results);
        this.clipboardManager = (ClipboardManager) getSystemService("clipboard");
        getListView().setOnCreateContextMenuListener(this);
        extractSearchCriteria();
        if (getIntent().getBooleanExtra(EXTRA_EXAMPLES_BACKDOOR_SEARCH, false)) {
            searchTask = new ExampleSearchTaskBackdoor(getWwwjdicUrl(), getHttpTimeoutSeconds(), this, this.criteria, WwwjdicPreferences.isReturnRandomExamples(this));
        } else {
            searchTask = new ExampleSearchTask(String.valueOf(getWwwjdicUrl()) + EXAMPLE_SEARCH_QUERY_STR, getHttpTimeoutSeconds(), this, this.criteria, this.criteria.getNumMaxResults().intValue());
        }
        submitSearchTask(searchTask);
    }

    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        menu.add(0, 0, 0, (int) R.string.break_down_jap);
        menu.add(0, 1, 1, (int) R.string.look_up_all_kanji);
        menu.add(0, 2, 2, (int) R.string.copy_jp);
        menu.add(0, 3, 3, (int) R.string.copy_eng);
    }

    public boolean onContextItemSelected(MenuItem item) {
        try {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
            switch (item.getItemId()) {
                case 0:
                    breakDown(info.id);
                    return true;
                case 1:
                    lookupAllKanji(info.id);
                    return true;
                case 2:
                    copyJapanese(info.id);
                    return true;
                case 3:
                    copyEnglish(info.id);
                    return true;
                default:
                    return false;
            }
        } catch (ClassCastException e) {
            Log.e(TAG, "bad menuInfo", e);
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        breakDown(id);
    }

    private void breakDown(long id) {
        Analytics.event("sentenceBreakdown", this);
        Intent intent = new Intent(this, SentenceBreakdown.class);
        ExampleSentence sentence = getCurrentSentence(id);
        intent.putExtra(SentenceBreakdown.EXTRA_SENTENCE, sentence.getJapanese());
        intent.putExtra(SentenceBreakdown.EXTRA_SENTENCE_TRANSLATION, sentence.getEnglish());
        startActivity(intent);
    }

    private void copyEnglish(long id) {
        this.clipboardManager.setText(getCurrentSentence(id).getEnglish());
    }

    private ExampleSentence getCurrentSentence(long id) {
        return this.sentences.get((int) id);
    }

    private void copyJapanese(long id) {
        this.clipboardManager.setText(getCurrentSentence(id).getJapanese());
    }

    private void lookupAllKanji(long id) {
        SearchCriteria criteria = SearchCriteria.createForKanjiOrReading(getCurrentSentence(id).getJapanese());
        Intent intent = new Intent(this, KanjiResultListView.class);
        intent.putExtra(Constants.CRITERIA_KEY, criteria);
        startActivity(intent);
    }

    public void setResult(final List<ExampleSentence> result) {
        this.guiThread.post(new Runnable() {
            public void run() {
                ExamplesResultListView.this.sentences = result;
                ExamplesResultListView.this.setListAdapter(new ExampleSentenceAdapter(ExamplesResultListView.this, ExamplesResultListView.this.sentences, ExamplesResultListView.this.criteria.getQueryString()));
                ExamplesResultListView.this.getListView().setTextFilterEnabled(true);
                String message = ExamplesResultListView.this.getResources().getString(R.string.examples_for);
                ExamplesResultListView.this.setTitle(String.format(message, Integer.valueOf(ExamplesResultListView.this.sentences.size()), ExamplesResultListView.this.criteria.getQueryString()));
                ExamplesResultListView.this.dismissProgressDialog();
            }
        });
    }
}
