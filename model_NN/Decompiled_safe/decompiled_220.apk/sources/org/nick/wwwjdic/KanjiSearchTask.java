package org.nick.wwwjdic;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class KanjiSearchTask extends BackdoorSearchTask<KanjiEntry> {
    public KanjiSearchTask(String url, int timeoutSeconds, ResultListView<KanjiEntry> resultListView, SearchCriteria criteria) {
        super(url, timeoutSeconds, resultListView, criteria);
    }

    /* access modifiers changed from: protected */
    public KanjiEntry parseEntry(String entryStr) {
        return KanjiEntry.parseKanjidic(entryStr.trim());
    }

    /* access modifiers changed from: protected */
    public String generateBackdoorCode(SearchCriteria criteria) {
        StringBuffer buff = new StringBuffer();
        buff.append("1");
        buff.append("Z");
        if (criteria.isKanjiCodeLookup()) {
            buff.append("K");
        } else {
            buff.append("M");
        }
        buff.append(criteria.getKanjiSearchType());
        try {
            buff.append(URLEncoder.encode(criteria.getQueryString(), "UTF-8"));
            if (criteria.hasStrokes()) {
                buff.append("=");
            }
            if (criteria.hasMinStrokes()) {
                buff.append(criteria.getMinStrokeCount().toString());
            }
            if (criteria.hasMaxStrokes()) {
                buff.append("-");
                buff.append(criteria.getMaxStrokeCount().toString());
            }
            if (criteria.hasStrokes()) {
                buff.append("=");
            }
            return buff.toString();
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}
