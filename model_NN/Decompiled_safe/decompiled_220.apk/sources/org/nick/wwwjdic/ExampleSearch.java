package org.nick.wwwjdic;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import java.util.List;
import org.nick.wwwjdic.history.FavoritesAndHistorySummaryView;
import org.nick.wwwjdic.history.HistoryDbHelper;
import org.nick.wwwjdic.utils.Analytics;
import org.nick.wwwjdic.utils.StringUtils;

public class ExampleSearch extends WwwjdicActivityBase implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    private HistoryDbHelper dbHelper;
    private CheckBox exampleExactMatchCb;
    private Button exampleSearchButton;
    private EditText exampleSearchInputText;
    private FavoritesAndHistorySummaryView examplesHistorySummary;
    private EditText maxNumExamplesText;
    private Spinner sentenceModeSpinner;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.example_search);
        findViews();
        setupListeners();
        setupSpinners();
        this.exampleSearchInputText.requestFocus();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String searchKey = extras.getString(Constants.SEARCH_TEXT_KEY);
            int searchType = extras.getInt(Constants.SEARCH_TYPE);
            if (searchKey != null) {
                switch (searchType) {
                    case 2:
                        this.exampleSearchInputText.setText(searchKey);
                        break;
                }
                this.inputTextFromBundle = true;
            }
        }
        this.dbHelper = HistoryDbHelper.getInstance(this);
        setupExamplesSummary();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        setupExamplesSummary();
    }

    private void setupExamplesSummary() {
        this.dbHelper.beginTransaction();
        try {
            long numAllHistory = this.dbHelper.getExamplesHistoryCount();
            List<String> recentHistory = this.dbHelper.getRecentExamplesHistory(5);
            this.examplesHistorySummary.setHistoryFilterType(2);
            this.examplesHistorySummary.setRecentEntries(0, null, numAllHistory, recentHistory);
            this.dbHelper.setTransactionSuccessful();
        } finally {
            this.dbHelper.endTransaction();
        }
    }

    private void setupListeners() {
        findViewById(R.id.exampleSearchButton).setOnClickListener(this);
    }

    private void setupSpinners() {
        ArrayAdapter<CharSequence> sentenceModeAdapter = ArrayAdapter.createFromResource(this, R.array.sentence_modes, R.layout.spinner_text);
        sentenceModeAdapter.setDropDownViewResource(17367049);
        this.sentenceModeSpinner.setAdapter((SpinnerAdapter) sentenceModeAdapter);
        this.sentenceModeSpinner.setOnItemSelectedListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.exampleSearchButton /*2131296307*/:
                String queryString = this.exampleSearchInputText.getText().toString();
                if (TextUtils.isEmpty(queryString)) {
                    return;
                }
                if (this.sentenceModeSpinner.getSelectedItemPosition() == 0) {
                    int numMaxResults = 20;
                    try {
                        numMaxResults = Integer.parseInt(this.maxNumExamplesText.getText().toString());
                    } catch (NumberFormatException e) {
                    }
                    SearchCriteria criteria = SearchCriteria.createForExampleSearch(queryString.trim(), this.exampleExactMatchCb.isChecked(), numMaxResults);
                    Intent intent = new Intent(this, ExamplesResultListView.class);
                    intent.putExtra(Constants.CRITERIA_KEY, criteria);
                    if (!StringUtils.isEmpty(criteria.getQueryString())) {
                        this.dbHelper.addSearchCriteria(criteria);
                    }
                    Analytics.event("exampleSearch", this);
                    startActivity(intent);
                    return;
                }
                Intent intent2 = new Intent(this, SentenceBreakdown.class);
                intent2.putExtra(SentenceBreakdown.EXTRA_SENTENCE, queryString);
                Analytics.event("sentenceTranslation", this);
                startActivity(intent2);
                return;
            default:
                return;
        }
    }

    private void findViews() {
        this.exampleSearchInputText = (EditText) findViewById(R.id.exampleInputText);
        this.maxNumExamplesText = (EditText) findViewById(R.id.maxExamplesInput);
        this.exampleExactMatchCb = (CheckBox) findViewById(R.id.exampleExactMatchCb);
        this.sentenceModeSpinner = (Spinner) findViewById(R.id.modeSpinner);
        this.exampleSearchButton = (Button) findViewById(R.id.exampleSearchButton);
        this.examplesHistorySummary = (FavoritesAndHistorySummaryView) findViewById(R.id.examples_history_summary);
    }

    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (this.inputTextFromBundle) {
            this.inputTextFromBundle = false;
            return;
        }
        switch (parent.getId()) {
            case R.id.modeSpinner /*2131296303*/:
                toggleExampleOptions(position == 0);
                return;
            default:
                return;
        }
    }

    private void toggleExampleOptions(boolean isEnabled) {
        this.maxNumExamplesText.setEnabled(isEnabled);
        this.exampleExactMatchCb.setEnabled(isEnabled);
        this.maxNumExamplesText.setFocusableInTouchMode(isEnabled);
        this.exampleSearchInputText.setText("");
        this.exampleSearchInputText.requestFocus();
        if (!isEnabled) {
            this.exampleSearchInputText.setHint((int) R.string.enter_japanese_text);
            this.exampleSearchButton.setText((int) R.string.translate);
            return;
        }
        this.exampleSearchInputText.setHint((int) R.string.enter_eng_or_jap);
        this.exampleSearchButton.setText((int) R.string.search);
    }

    public void onNothingSelected(AdapterView<?> adapterView) {
    }
}
