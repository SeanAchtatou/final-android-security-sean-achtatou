package org.nick.wwwjdic;

import android.content.SearchRecentSuggestionsProvider;

public class SearchSuggestionProvider extends SearchRecentSuggestionsProvider {
    public static final String AUTHORITY = "org.nick.wwwjdic.SearchSuggestionProvider";
    public static final int MODE = 1;

    public SearchSuggestionProvider() {
        setupSuggestions(AUTHORITY, 1);
    }
}
