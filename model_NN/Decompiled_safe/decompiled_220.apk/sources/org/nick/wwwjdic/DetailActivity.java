package org.nick.wwwjdic;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.text.ClipboardManager;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Locale;
import java.util.regex.Pattern;
import org.nick.wwwjdic.history.HistoryDbHelper;
import org.nick.wwwjdic.utils.DictUtils;
import org.nick.wwwjdic.utils.IntentSpan;
import org.nick.wwwjdic.utils.Pair;

public abstract class DetailActivity extends Activity implements CompoundButton.OnCheckedChangeListener, View.OnLongClickListener, TextToSpeech.OnInitListener {
    protected static final Pattern CROSS_REF_PATTERN = Pattern.compile("^.*\\(See (\\S+)\\).*$");
    protected static final int ITEM_ID_HOME = 0;
    private static final String TAG = DetailActivity.class.getSimpleName();
    private static final int TTS_DATA_CHECK_CODE = 0;
    protected HistoryDbHelper db = HistoryDbHelper.getInstance(this);
    protected boolean isFavorite;
    protected TextToSpeech tts;
    protected WwwjdicEntry wwwjdicEntry;

    /* access modifiers changed from: protected */
    public abstract Locale getSpeechLocale();

    /* access modifiers changed from: protected */
    public abstract void hideTtsButtons();

    /* access modifiers changed from: protected */
    public abstract void setHomeActivityExtras(Intent intent);

    /* access modifiers changed from: protected */
    public abstract void showTtsButtons();

    protected DetailActivity() {
    }

    /* access modifiers changed from: protected */
    public void addToFavorites() {
        this.wwwjdicEntry.setId(Long.valueOf(this.db.addFavorite(this.wwwjdicEntry)));
    }

    /* access modifiers changed from: protected */
    public void removeFromFavorites() {
        this.db.deleteFavorite(this.wwwjdicEntry.getId().longValue());
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            addToFavorites();
        } else {
            removeFromFavorites();
        }
    }

    public boolean onLongClick(View v) {
        ((ClipboardManager) getSystemService("clipboard")).setText(this.wwwjdicEntry.getHeadword());
        Toast.makeText(this, String.format(getResources().getString(R.string.copied_to_clipboard), this.wwwjdicEntry.getHeadword()), 0).show();
        return false;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 0, 0, (int) R.string.home).setIcon(17301561);
        return true;
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                Intent intent = new Intent(this, Wwwjdic.class);
                intent.setFlags(67108864);
                setHomeActivityExtras(intent);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onMenuItemSelected(featureId, item);
        }
    }

    /* access modifiers changed from: protected */
    public WwwjdicApplication getApp() {
        return (WwwjdicApplication) getApplication();
    }

    /* access modifiers changed from: protected */
    public void makeClickable(TextView textView, int start, int end, Intent intent) {
        SpannableString str = SpannableString.valueOf(textView.getText());
        str.setSpan(new IntentSpan(this, intent), start, end, 33);
        textView.setText(str);
        textView.setLinkTextColor(-1);
        MovementMethod m = textView.getMovementMethod();
        if ((m == null || !(m instanceof LinkMovementMethod)) && textView.getLinksClickable()) {
            textView.setMovementMethod(LinkMovementMethod.getInstance());
        }
    }

    public void onInit(int status) {
        if (status != 0) {
            hideTtsButtons();
            return;
        }
        Locale locale = getSpeechLocale();
        if (locale == null) {
            Log.w(TAG, "TTS locale " + locale + "not recognized");
            hideTtsButtons();
        } else if (this.tts.isLanguageAvailable(locale) == -1 || this.tts.isLanguageAvailable(locale) == -2) {
            Log.w(TAG, "TTS locale " + locale + " not available");
            hideTtsButtons();
        } else {
            this.tts.setLanguage(getSpeechLocale());
            showTtsButtons();
        }
    }

    /* access modifiers changed from: protected */
    public Pair<LinearLayout, TextView> createMeaningTextView(final Context ctx, String meaning) {
        LinearLayout translationLayout = (LinearLayout) LayoutInflater.from(ctx).inflate((int) R.layout.translation_item, (ViewGroup) null);
        final TextView translationText = (TextView) translationLayout.findViewById(R.id.translation_text);
        translationText.setText(meaning);
        ((Button) translationLayout.findViewById(R.id.speak_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String toSpeak = DictUtils.stripWwwjdicTags(ctx, translationText.getText().toString());
                if (DetailActivity.this.tts != null) {
                    DetailActivity.this.tts.speak(toSpeak, 1, null);
                }
            }
        });
        return new Pair<>(translationLayout, translationText);
    }

    /* access modifiers changed from: protected */
    public void checkTtsAvailability() {
        if (isIntentAvailable("android.speech.tts.engine.CHECK_TTS_DATA")) {
            startActivityForResult(new Intent("android.speech.tts.engine.CHECK_TTS_DATA"), 0);
        }
    }

    private boolean isIntentAvailable(String action) {
        return getPackageManager().queryIntentActivities(new Intent(action), 65536).size() > 0;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode != 0) {
            return;
        }
        if (resultCode == 1) {
            this.tts = new TextToSpeech(this, this);
        } else if (WwwjdicPreferences.wantsTts(this)) {
            createInstallTtsDataDialog().show();
        } else {
            hideTtsButtons();
        }
    }

    public Dialog createInstallTtsDataDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage((int) R.string.install_tts_data_message).setTitle((int) R.string.install_tts_data_title).setIcon(17301543).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                WwwjdicPreferences.setWantsTts(DetailActivity.this, true);
                Intent installIntent = new Intent("android.speech.tts.engine.INSTALL_TTS_DATA");
                dialog.dismiss();
                DetailActivity.this.startActivity(installIntent);
            }
        }).setNegativeButton((int) R.string.not_now, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                DetailActivity.this.hideTtsButtons();
                dialog.dismiss();
            }
        }).setNeutralButton((int) R.string.dont_ask_again, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                DetailActivity.this.hideTtsButtons();
                WwwjdicPreferences.setWantsTts(DetailActivity.this, false);
                dialog.dismiss();
            }
        });
        return builder.create();
    }
}
