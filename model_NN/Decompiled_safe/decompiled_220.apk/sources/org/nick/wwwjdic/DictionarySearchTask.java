package org.nick.wwwjdic;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class DictionarySearchTask extends BackdoorSearchTask<DictionaryEntry> {
    public DictionarySearchTask(String url, int timeoutSeconds, ResultListViewBase<DictionaryEntry> resultListView, SearchCriteria criteria) {
        super(url, timeoutSeconds, resultListView, criteria);
    }

    /* access modifiers changed from: protected */
    public DictionaryEntry parseEntry(String entryStr) {
        return DictionaryEntry.parseEdict(entryStr.trim(), ((SearchCriteria) this.query).getDictionaryCode());
    }

    /* access modifiers changed from: protected */
    public String generateBackdoorCode(SearchCriteria criteria) {
        StringBuffer buff = new StringBuffer();
        buff.append(criteria.getDictionaryCode());
        buff.append("Z");
        if (criteria.isRomanizedJapanese()) {
            buff.append("D");
        } else {
            buff.append("U");
        }
        if (criteria.isKanjiCompoundSearch()) {
            if (criteria.isCommonWordsOnly()) {
                buff.append("P");
            } else if (criteria.isStartingKanjiCompoundSearch()) {
                buff.append("K");
            } else {
                buff.append("L");
            }
        } else if (criteria.isExactMatch() && !criteria.isCommonWordsOnly()) {
            buff.append("Q");
        } else if (criteria.isExactMatch() && criteria.isCommonWordsOnly()) {
            buff.append("R");
        } else if (!criteria.isExactMatch() && criteria.isCommonWordsOnly()) {
            buff.append("P");
        } else if (criteria.isRomanizedJapanese()) {
            buff.append("J");
        } else {
            buff.append("E");
        }
        try {
            buff.append(URLEncoder.encode(criteria.getQueryString(), "UTF-8"));
            return buff.toString();
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}
