package org.nick.wwwjdic;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.List;
import org.nick.wwwjdic.utils.StringUtils;

public class SentenceBreakdown extends ResultListViewBase<SentenceBreakdownEntry> {
    public static final String EXTRA_SENTENCE = "org.nick.wwwjdic.SENTENCE";
    public static final String EXTRA_SENTENCE_TRANSLATION = "org.nick.wwwjdic.SENTENCE_TRANSLATION";
    private static final int HILIGHT_COLOR1 = -12420393;
    private static final int HILIGHT_COLOR2 = -428544;
    private static final int ITEM_ID_HOME = 0;
    /* access modifiers changed from: private */
    public List<SentenceBreakdownEntry> entries;
    /* access modifiers changed from: private */
    public SpannableString markedSentence;
    /* access modifiers changed from: private */
    public TextView sentenceView;

    static class SentenceBreakdownAdapter extends BaseAdapter {
        private final Context context;
        private final List<SentenceBreakdownEntry> entries;

        public SentenceBreakdownAdapter(Context context2, List<SentenceBreakdownEntry> entries2) {
            this.context = context2;
            this.entries = entries2;
        }

        public int getCount() {
            if (this.entries == null) {
                return 0;
            }
            return this.entries.size();
        }

        /* Debug info: failed to restart local var, previous not found, register: 1 */
        public Object getItem(int position) {
            if (this.entries == null) {
                return null;
            }
            return this.entries.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            SentenceBreakdownEntry entry = this.entries.get(position);
            if (convertView == null) {
                convertView = new SentenceBreakdownEntryView(this.context);
            }
            ((SentenceBreakdownEntryView) convertView).populate(entry);
            return convertView;
        }

        static class SentenceBreakdownEntryView extends LinearLayout {
            private TextView explanationText = ((TextView) findViewById(R.id.explanationText));
            private TextView readingText = ((TextView) findViewById(R.id.readingText));
            private TextView translationText = ((TextView) findViewById(R.id.translationText));
            private TextView wordText = ((TextView) findViewById(R.id.wordText));

            SentenceBreakdownEntryView(Context context) {
                super(context);
                LayoutInflater.from(context).inflate((int) R.layout.breakdown_item, this);
            }

            /* access modifiers changed from: package-private */
            public void populate(SentenceBreakdownEntry entry) {
                if (!StringUtils.isEmpty(entry.getExplanation())) {
                    this.explanationText.setText(entry.getExplanation());
                } else {
                    this.explanationText.setVisibility(8);
                }
                this.wordText.setText(entry.getWord());
                if (!StringUtils.isEmpty(entry.getReading())) {
                    this.readingText.setText(entry.getReading());
                } else {
                    this.readingText.setVisibility(8);
                }
                this.translationText.setText(entry.getTranslation());
            }
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.sentence_breakdown);
        Bundle extras = getIntent().getExtras();
        String sentenceStr = extras.getString(EXTRA_SENTENCE);
        String sentenceTranslation = extras.getString(EXTRA_SENTENCE_TRANSLATION);
        this.markedSentence = new SpannableString(sentenceStr);
        this.sentenceView = (TextView) findViewById(R.id.sentence);
        this.sentenceView.setText(this.markedSentence);
        TextView englishSentenceText = (TextView) findViewById(R.id.englishSentence);
        if (!StringUtils.isEmpty(sentenceTranslation)) {
            englishSentenceText.setText(sentenceTranslation);
        } else {
            englishSentenceText.setVisibility(8);
        }
        submitSearchTask(new SentenceBreakdownTask(getWwwjdicUrl(), getHttpTimeoutSeconds(), this, new WwwjdicQuery(sentenceStr)));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 0, 0, (int) R.string.home).setIcon(17301561);
        return true;
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                Intent intent = new Intent(this, Wwwjdic.class);
                intent.setFlags(67108864);
                intent.putExtra(Constants.SELECTED_TAB_IDX, 2);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onMenuItemSelected(featureId, item);
        }
    }

    /* access modifiers changed from: private */
    public void markString(SpannableString spannable, String term) {
        String sentence = spannable.toString();
        String[] terms = term.split(" ");
        if (terms.length > 1) {
            term = terms[0];
        }
        int idx = sentence.indexOf(term);
        while (idx != -1) {
            int color = HILIGHT_COLOR1;
            ForegroundColorSpan[] spans = (ForegroundColorSpan[]) spannable.getSpans(0, idx, ForegroundColorSpan.class);
            if (spans.length > 0) {
                if (spans[spans.length - 1].getForegroundColor() == HILIGHT_COLOR1) {
                    color = HILIGHT_COLOR2;
                } else {
                    color = HILIGHT_COLOR1;
                }
            }
            int spanStart = idx;
            int spanEnd = idx + term.length();
            if (((ForegroundColorSpan[]) spannable.getSpans(spanStart, spanEnd, ForegroundColorSpan.class)).length == 0) {
                spannable.setSpan(new ForegroundColorSpan(color), spanStart, spanEnd, 0);
                return;
            } else if (term.length() + idx + 1 <= spannable.length() - 1) {
                idx = sentence.indexOf(term, idx + 1);
            }
        }
    }

    public void setResult(final List<SentenceBreakdownEntry> result) {
        this.guiThread.post(new Runnable() {
            public void run() {
                SentenceBreakdown.this.entries = result;
                SentenceBreakdown.this.setListAdapter(new SentenceBreakdownAdapter(SentenceBreakdown.this, SentenceBreakdown.this.entries));
                SentenceBreakdown.this.getListView().setTextFilterEnabled(true);
                SentenceBreakdown.this.setTitle((int) R.string.sentence_breakdown);
                for (SentenceBreakdownEntry entry : SentenceBreakdown.this.entries) {
                    SentenceBreakdown.this.markString(SentenceBreakdown.this.markedSentence, entry.getInflectedForm());
                }
                SentenceBreakdown.this.sentenceView.setText(SentenceBreakdown.this.markedSentence);
                SentenceBreakdown.this.dismissProgressDialog();
            }
        });
    }
}
