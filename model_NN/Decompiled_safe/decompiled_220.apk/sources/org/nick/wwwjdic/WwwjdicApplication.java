package org.nick.wwwjdic;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import com.flurry.android.FlurryAgent;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;
import org.nick.wwwjdic.updates.UpdateCheckService;
import org.nick.wwwjdic.utils.FileUtils;

@ReportsCrashes(formKey = "dDA0N2Z5TWdReHdCaC1vOVB4WEpCZEE6MQ", mode = ReportingInteractionMode.TOAST, resToastText = R.string.crash_toast_text)
public class WwwjdicApplication extends Application {
    private static final String NEW_JAPAN_MIRROR = "http://wwwjdic.mygengo.com/cgi-data/wwwjdic";
    private static final String OLD_JAPAN_MIRROR = "http://www.aa.tufs.ac.jp/~jwb/cgi-bin/wwwjdic.cgi";
    private static final String TAG = WwwjdicApplication.class.getSimpleName();
    private static final String WWWJDIC_DIR = "wwwjdic";
    private static String flurryKey;
    private static String version;
    private String currentDictionary = "1";
    private String currentDictionaryName = "General";
    private ExecutorService executorService = Executors.newSingleThreadExecutor();
    private LocationManager locationManager;

    public void onCreate() {
        version = getVersionName();
        flurryKey = readKey();
        FlurryAgent.setCaptureUncaughtExceptions(false);
        ACRA.init(this);
        createWwwjdicDirIfNecessary();
        updateJapanMirror();
        initRadicals();
        if (isAutoSelectMirror()) {
            setMirrorBasedOnLocation();
        }
        updateKanjiRecognizerUrl();
        WwwjdicPreferences.setStrokeAnimationDelay(this, WwwjdicPreferences.DEFAULT_STROKE_ANIMATION_DELAY);
        startCheckUpdateService();
    }

    private void updateJapanMirror() {
        if (OLD_JAPAN_MIRROR.equals(WwwjdicPreferences.getWwwjdicUrl(this))) {
            WwwjdicPreferences.setWwwjdicUrl("http://wwwjdic.mygengo.com/cgi-data/wwwjdic", this);
        }
    }

    private void startCheckUpdateService() {
        if (WwwjdicPreferences.isUpdateCheckEnabled(this) && new Date().getTime() - WwwjdicPreferences.getLastUpdateCheck(this) >= WwwjdicPreferences.KOD_DEFAULT_UPDATE_INTERVAL) {
            startService(UpdateCheckService.createStartIntent(this, getResources().getString(R.string.versions_url), getResources().getString(R.string.market_name)));
        }
    }

    private void updateKanjiRecognizerUrl() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if (prefs.getString(WwwjdicPreferences.PREF_KR_URL_KEY, WwwjdicPreferences.KR_DEFAULT_URL).contains("kanji.cgi")) {
            Log.d(TAG, "found old KR URL, will overwrite with http://kanji.sljfaq.org/kanji-0.016.cgi");
            prefs.edit().putString(WwwjdicPreferences.PREF_KR_URL_KEY, WwwjdicPreferences.KR_DEFAULT_URL).commit();
        }
    }

    private boolean isAutoSelectMirror() {
        return PreferenceManager.getDefaultSharedPreferences(this).getBoolean(WwwjdicPreferences.PREF_AUTO_SELECT_MIRROR_KEY, true);
    }

    public synchronized void setMirrorBasedOnLocation() {
        Log.d(TAG, "auto selecting mirror...");
        this.locationManager = (LocationManager) getSystemService("location");
        try {
            boolean isEnabled = this.locationManager.isProviderEnabled("network");
            Log.d(TAG, "network enabled: " + isEnabled);
            if (!isEnabled) {
                Log.d(TAG, "provider not enabled, giving up");
            } else {
                Location myLocation = this.locationManager.getLastKnownLocation("network");
                if (myLocation == null) {
                    Log.w(TAG, "failed to get cached location, giving up");
                } else {
                    Log.d(TAG, "my location: " + myLocation);
                    String[] mirrorCoords = getResources().getStringArray(R.array.wwwjdic_mirror_coords);
                    String[] mirrorNames = getResources().getStringArray(R.array.wwwjdic_mirror_names);
                    String[] mirrorUrls = getResources().getStringArray(R.array.wwwjdic_mirror_urls);
                    List<Float> distanceToMirrors = new ArrayList<>(mirrorCoords.length);
                    for (int i = 0; i < mirrorCoords.length; i++) {
                        String[] latlng = mirrorCoords[i].split("/");
                        float[] distance = new float[1];
                        Location.distanceBetween(myLocation.getLatitude(), myLocation.getLongitude(), Location.convert(latlng[0]), Location.convert(latlng[1]), distance);
                        distanceToMirrors.add(Float.valueOf(distance[0]));
                        Log.d(TAG, String.format("distance to %s: %f km", mirrorNames[i], Float.valueOf(distance[0] / 1000.0f)));
                    }
                    float minDistance = ((Float) Collections.min(distanceToMirrors)).floatValue();
                    int mirrorIdx = distanceToMirrors.indexOf(Float.valueOf(minDistance));
                    Log.d(TAG, String.format("found closest mirror: %s (%s) (distance: %f km)", mirrorUrls[mirrorIdx], mirrorNames[mirrorIdx], Float.valueOf(minDistance / 1000.0f)));
                    PreferenceManager.getDefaultSharedPreferences(this).edit().putString(WwwjdicPreferences.PREF_WWWJDIC_MIRROR_URL_KEY, mirrorUrls[mirrorIdx]).commit();
                }
            }
        } catch (Exception e) {
            Log.w(TAG, "error getting location, giving up", e);
        }
        return;
    }

    private void createWwwjdicDirIfNecessary() {
        File wwwjdicDir = getWwwjdicDir();
        if (wwwjdicDir.exists()) {
            return;
        }
        if (wwwjdicDir.mkdir()) {
            Log.d(TAG, "successfully created " + wwwjdicDir.getAbsolutePath());
        } else {
            Log.d(TAG, "failed to create " + wwwjdicDir.getAbsolutePath());
        }
    }

    public static File getWwwjdicDir() {
        return new File(Environment.getExternalStorageDirectory(), WWWJDIC_DIR);
    }

    private String getVersionName() {
        try {
            return getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return "";
        }
    }

    private String readKey() {
        InputStream in = null;
        try {
            in = getAssets().open("keys");
            String trim = FileUtils.readTextFile(in).trim();
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                }
            }
            return trim;
        } catch (IOException e2) {
            throw new RuntimeException(e2);
        } catch (Throwable th) {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e3) {
                }
            }
            throw th;
        }
    }

    public ExecutorService getExecutorService() {
        return this.executorService;
    }

    public static String getVersion() {
        return version;
    }

    public static String getUserAgentString() {
        return "Android-WWWJDIC/" + getVersion();
    }

    public static String getFlurryKey() {
        return flurryKey;
    }

    private void initRadicals() {
        Radicals radicals = Radicals.getInstance();
        if (!radicals.isInitialized()) {
            radicals.addRadicals(1, getIntArray(R.array.one_stroke_radical_numbers), getStrArray(R.array.one_stroke_radicals));
            radicals.addRadicals(2, getIntArray(R.array.two_stroke_radical_numbers), getStrArray(R.array.two_stroke_radicals));
            radicals.addRadicals(3, getIntArray(R.array.three_stroke_radical_numbers), getStrArray(R.array.three_stroke_radicals));
            radicals.addRadicals(4, getIntArray(R.array.four_stroke_radical_numbers), getStrArray(R.array.four_stroke_radicals));
            radicals.addRadicals(5, getIntArray(R.array.five_stroke_radical_numbers), getStrArray(R.array.five_stroke_radicals));
            radicals.addRadicals(6, getIntArray(R.array.six_stroke_radical_numbers), getStrArray(R.array.six_stroke_radicals));
            radicals.addRadicals(7, getIntArray(R.array.seven_stroke_radical_numbers), getStrArray(R.array.seven_stroke_radicals));
            radicals.addRadicals(8, getIntArray(R.array.eight_stroke_radical_numbers), getStrArray(R.array.eight_stroke_radicals));
            radicals.addRadicals(9, getIntArray(R.array.nine_stroke_radical_numbers), getStrArray(R.array.nine_stroke_radicals));
            radicals.addRadicals(10, getIntArray(R.array.ten_stroke_radical_numbers), getStrArray(R.array.ten_stroke_radicals));
            radicals.addRadicals(11, getIntArray(R.array.eleven_stroke_radical_numbers), getStrArray(R.array.eleven_stroke_radicals));
            radicals.addRadicals(12, getIntArray(R.array.twelve_stroke_radical_numbers), getStrArray(R.array.twelve_stroke_radicals));
            radicals.addRadicals(13, getIntArray(R.array.thirteen_stroke_radical_numbers), getStrArray(R.array.thirteen_stroke_radicals));
            radicals.addRadicals(14, getIntArray(R.array.fourteen_stroke_radical_numbers), getStrArray(R.array.fourteen_stroke_radicals));
            radicals.addRadicals(15, getIntArray(R.array.fivteen_stroke_radical_numbers), getStrArray(R.array.fivteen_stroke_radicals));
            radicals.addRadicals(16, getIntArray(R.array.sixteen_stroke_radical_numbers), getStrArray(R.array.sixteen_stroke_radicals));
            radicals.addRadicals(17, getIntArray(R.array.seventeen_stroke_radical_numbers), getStrArray(R.array.seventeen_stroke_radicals));
        }
    }

    private int[] getIntArray(int id) {
        return getResources().getIntArray(id);
    }

    private String[] getStrArray(int id) {
        return getResources().getStringArray(id);
    }

    public synchronized String getCurrentDictionary() {
        return this.currentDictionary;
    }

    public synchronized void setCurrentDictionary(String currentDictionary2) {
        this.currentDictionary = currentDictionary2;
    }

    public synchronized String getCurrentDictionaryName() {
        return this.currentDictionaryName;
    }

    public synchronized void setCurrentDictionaryName(String currentDictionaryName2) {
        this.currentDictionaryName = currentDictionaryName2;
    }
}
