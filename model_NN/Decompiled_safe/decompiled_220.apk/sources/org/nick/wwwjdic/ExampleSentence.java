package org.nick.wwwjdic;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ExampleSentence implements Serializable {
    private static final long serialVersionUID = 1017016303787281524L;
    private String english;
    private String japanese;
    private List<String> matches = new ArrayList();

    public ExampleSentence(String japanese2, String english2) {
        this.japanese = japanese2;
        this.english = english2;
    }

    public String getJapanese() {
        return this.japanese;
    }

    public String getEnglish() {
        return this.english;
    }

    public List<String> getMatches() {
        return this.matches;
    }

    public void addMatch(String match) {
        this.matches.add(match);
    }
}
