package org.nick.wwwjdic;

import android.content.Intent;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import java.util.List;
import org.nick.wwwjdic.history.HistoryUtils;
import org.nick.wwwjdic.utils.Analytics;
import org.nick.wwwjdic.utils.DictUtils;
import org.nick.wwwjdic.utils.StringUtils;

public class DictionaryResultListView extends ResultListViewBase<DictionaryEntry> {
    private static final int MENU_ITEM_ADD_TO_FAVORITES = 3;
    private static final int MENU_ITEM_COPY = 1;
    private static final int MENU_ITEM_DETAILS = 0;
    private static final int MENU_ITEM_EXAMPLES = 4;
    private static final int MENU_ITEM_LOOKUP_KANJI = 2;
    private static final int NUM_EXAMPLE_RESULTS = 20;
    private static final String TAG = DictionaryResultListView.class.getSimpleName();
    /* access modifiers changed from: private */
    public List<DictionaryEntry> entries;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.search_results);
        getListView().setOnCreateContextMenuListener(this);
        Intent intent = getIntent();
        if ("android.intent.action.SEARCH".equals(intent.getAction())) {
            String query = intent.getStringExtra("query");
            String dictionary = WwwjdicPreferences.getDefaultDictionary(this);
            new SearchRecentSuggestions(this, SearchSuggestionProvider.AUTHORITY, 1).saveRecentQuery(query, null);
            this.criteria = SearchCriteria.createForDictionary(query, false, false, false, dictionary);
        } else {
            extractSearchCriteria();
        }
        submitSearchTask(new DictionarySearchTask(getWwwjdicUrl(), getHttpTimeoutSeconds(), this, this.criteria));
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        showDetails(this.entries.get(position));
    }

    private void showDetails(DictionaryEntry entry) {
        Intent intent = new Intent(this, DictionaryEntryDetail.class);
        intent.putExtra(Constants.ENTRY_KEY, entry);
        setFavoriteId(intent, entry);
        startActivity(intent);
    }

    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        menu.add(0, 0, 0, (int) R.string.details);
        menu.add(0, 1, 1, (int) R.string.copy);
        menu.add(0, 2, 2, (int) R.string.lookup_kanji);
        menu.add(0, 3, 3, (int) R.string.add_to_favorites);
        menu.add(0, 4, 4, (int) R.string.examples);
    }

    public boolean onContextItemSelected(MenuItem item) {
        try {
            DictionaryEntry entry = this.entries.get(((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).position);
            switch (item.getItemId()) {
                case 0:
                    showDetails(entry);
                    return true;
                case 1:
                    copy(entry);
                    return true;
                case 2:
                    Activities.lookupKanji(this, this.db, entry.getHeadword());
                    return true;
                case 3:
                    addToFavorites(entry);
                    return true;
                case 4:
                    searchExamples(entry);
                    return true;
                default:
                    return false;
            }
        } catch (ClassCastException e) {
            Log.e(TAG, "bad menuInfo", e);
            return false;
        }
    }

    private void searchExamples(DictionaryEntry entry) {
        SearchCriteria criteria = SearchCriteria.createForExampleSearch(DictUtils.extractSearchKey(entry), false, NUM_EXAMPLE_RESULTS);
        Intent intent = new Intent(this, ExamplesResultListView.class);
        intent.putExtra(Constants.CRITERIA_KEY, criteria);
        if (!StringUtils.isEmpty(criteria.getQueryString())) {
            this.db.addSearchCriteria(criteria);
        }
        Analytics.event("exampleSearch", this);
        startActivity(intent);
    }

    public void setResult(final List<DictionaryEntry> result) {
        this.guiThread.post(new Runnable() {
            public void run() {
                DictionaryResultListView.this.entries = result;
                DictionaryResultListView.this.setListAdapter(new DictionaryEntryAdapter(DictionaryResultListView.this, DictionaryResultListView.this.entries));
                DictionaryResultListView.this.getListView().setTextFilterEnabled(true);
                DictionaryResultListView.this.setTitle(DictionaryResultListView.this.getResources().getString(R.string.results_for_in_dict, Integer.valueOf(DictionaryResultListView.this.entries.size()), DictionaryResultListView.this.criteria.getQueryString(), HistoryUtils.lookupDictionaryName(DictionaryResultListView.this.criteria.getDictionaryCode(), DictionaryResultListView.this)));
                DictionaryResultListView.this.dismissProgressDialog();
            }
        });
    }
}
