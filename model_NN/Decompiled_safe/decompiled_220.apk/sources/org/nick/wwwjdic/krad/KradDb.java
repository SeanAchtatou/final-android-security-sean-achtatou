package org.nick.wwwjdic.krad;

import android.util.Log;
import au.com.bytecode.opencsv.CSVWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.nick.wwwjdic.utils.FileUtils;

public class KradDb {
    private static final String TAG = KradDb.class.getSimpleName();
    private Map<String, Set<String>> kanjiToRadicals = new HashMap();
    private Map<String, Set<String>> radicalToKanjis = new HashMap();

    public boolean isInitialized() {
        return !this.radicalToKanjis.isEmpty();
    }

    public void readFromStream(InputStream in) {
        if (!isInitialized()) {
            try {
                Log.d(TAG, "loading radkfile...");
                long start = System.currentTimeMillis();
                String[] lines = FileUtils.readTextFile(in, "UTF-8").trim().split(CSVWriter.DEFAULT_LINE_END);
                int length = lines.length;
                for (int i = 0; i < length; i++) {
                    String[] fields = lines[i].split(":");
                    if (fields.length >= 3) {
                        String radical = fields[0].trim();
                        HashSet hashSet = new HashSet();
                        String[] kanjiChars = fields[2].trim().split("");
                        int length2 = kanjiChars.length;
                        for (int i2 = 0; i2 < length2; i2++) {
                            String c = kanjiChars[i2];
                            if (!"".equals(c)) {
                                hashSet.add(c);
                                Set<String> radicals = this.kanjiToRadicals.get(c);
                                if (radicals == null) {
                                    radicals = new HashSet<>();
                                    this.kanjiToRadicals.put(c, radicals);
                                }
                                radicals.add(radical);
                            }
                        }
                        this.radicalToKanjis.put(radical, hashSet);
                    }
                }
                long time = System.currentTimeMillis() - start;
                Log.d(TAG, String.format("loaded %d radicals, %d kanji in %d [ms]", Integer.valueOf(this.radicalToKanjis.size()), Integer.valueOf(this.kanjiToRadicals.size()), Long.valueOf(time)));
                if (in != null) {
                    try {
                        in.close();
                    } catch (IOException e) {
                    }
                }
            } catch (IOException e2) {
                throw new RuntimeException(e2);
            } catch (Throwable th) {
                if (in != null) {
                    try {
                        in.close();
                    } catch (IOException e3) {
                    }
                }
                throw th;
            }
        }
    }

    public Set<String> getKanjiForRadical(String radical) {
        return this.radicalToKanjis.get(radical);
    }

    public Set<String> getKanjisForRadicals(Set<String> radicals) {
        Set<String> result = new HashSet<>();
        for (String radical : radicals) {
            Set<String> kanjis = getKanjiForRadical(radical);
            if (result.isEmpty()) {
                result.addAll(kanjis);
            } else {
                result.retainAll(kanjis);
            }
        }
        return result;
    }

    public Set<String> getRadicalsForKanji(String kanji) {
        return this.kanjiToRadicals.get(kanji);
    }

    public Set<String> getRadicalsForKanjis(Set<String> kanjis) {
        Set<String> result = new HashSet<>();
        for (String kanji : kanjis) {
            result.addAll(getRadicalsForKanji(kanji));
        }
        return result;
    }
}
