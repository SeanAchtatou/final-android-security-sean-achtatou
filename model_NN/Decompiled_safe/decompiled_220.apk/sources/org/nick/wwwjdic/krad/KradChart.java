package org.nick.wwwjdic.krad;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.nick.wwwjdic.Constants;
import org.nick.wwwjdic.KanjiResultListView;
import org.nick.wwwjdic.R;
import org.nick.wwwjdic.SearchCriteria;
import org.nick.wwwjdic.hkr.HkrCandidates;
import org.nick.wwwjdic.utils.Analytics;
import org.nick.wwwjdic.utils.Dialogs;
import org.nick.wwwjdic.utils.IntentSpan;

public class KradChart extends Activity implements View.OnClickListener, AdapterView.OnItemClickListener {
    private static final Map<String, String> KRAD_TO_DISPLAY = new HashMap();
    private static final String MULTI_RADICAL_TIP = "multi_radical_tip";
    /* access modifiers changed from: private */
    public static final List<String> NUM_STROKES = Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "17");
    private static final int NUM_SUMMARY_CHARS = 10;
    /* access modifiers changed from: private */
    public static final List<String> REPLACED_CHARS = Arrays.asList("邦", "阡", "尚", "个");
    private static final String STATE_KEY = "org.nick.wwwjdic.kradChartState";
    /* access modifiers changed from: private */
    public static final String TAG = KradChart.class.getSimpleName();
    /* access modifiers changed from: private */
    public KradAdapter adapter;
    private Button clearButton;
    private KradDb kradDb = new KradDb();
    private TextView matchedKanjiText;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog;
    /* access modifiers changed from: private */
    public GridView radicalChartGrid;
    /* access modifiers changed from: private */
    public List<String> radicals = new ArrayList();
    private Button showAllButton;
    private State state = new State();
    private TextView totalMatchesText;

    static {
        KRAD_TO_DISPLAY.put("⺅", "亻");
        KRAD_TO_DISPLAY.put("⺾", "艹");
        KRAD_TO_DISPLAY.put("辶", "辶");
        KRAD_TO_DISPLAY.put("⻏", "邦");
        KRAD_TO_DISPLAY.put("⻖", "阡");
        KRAD_TO_DISPLAY.put("⺌", "尚");
        KRAD_TO_DISPLAY.put("𠆢", "个");
        KRAD_TO_DISPLAY.put("⺹", "耂");
    }

    static class State implements Serializable {
        private static final long serialVersionUID = -6074503793592867534L;
        Set<String> enabledRadicals = new HashSet();
        Set<String> matchingKanjis = new HashSet();
        Set<String> selectedRadicals = new HashSet();

        State() {
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.krad_chart);
        this.matchedKanjiText = (TextView) findViewById(R.id.matched_kanji);
        this.totalMatchesText = (TextView) findViewById(R.id.total_matches);
        displayTotalMatches();
        this.showAllButton = (Button) findViewById(R.id.show_all_button);
        this.showAllButton.setOnClickListener(this);
        this.clearButton = (Button) findViewById(R.id.clear_button);
        this.clearButton.setOnClickListener(this);
        toggleButtons();
        this.radicalChartGrid = (GridView) findViewById(R.id.kradChartGrid);
        this.radicalChartGrid.setOnItemClickListener(this);
        setTitle((int) R.string.kanji_multi_radical_search);
        new AsyncTask<Void, Void, Boolean>() {
            private Throwable error;

            /* access modifiers changed from: protected */
            public void onPreExecute() {
                if (KradChart.this.progressDialog != null && KradChart.this.progressDialog.isShowing()) {
                    KradChart.this.progressDialog.dismiss();
                }
                KradChart.this.progressDialog = new ProgressDialog(KradChart.this);
                KradChart.this.progressDialog.setProgressStyle(0);
                KradChart.this.progressDialog.setMessage(KradChart.this.getString(R.string.loading));
                KradChart.this.progressDialog.setCancelable(false);
                KradChart.this.progressDialog.show();
            }

            /* access modifiers changed from: protected */
            public Boolean doInBackground(Void... params) {
                try {
                    KradChart.this.initKradDb();
                    for (String numStrokesStr : KradChart.NUM_STROKES) {
                        KradChart.this.radicals.add(new String(numStrokesStr));
                        KradChart.this.radicals.addAll(Arrays.asList(KradChart.this.getResources().getStringArray(((Integer) R.array.class.getField("_" + numStrokesStr + "_stroke").get(null)).intValue())));
                    }
                    return true;
                } catch (NoSuchFieldException e) {
                    throw new RuntimeException(e);
                } catch (IllegalAccessException e2) {
                    throw new RuntimeException(e2);
                } catch (Exception e3) {
                    Exception e4 = e3;
                    this.error = e4;
                    Log.d(KradChart.TAG, "Error loading radkfile-u", e4);
                    return false;
                }
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(Boolean result) {
                if (KradChart.this.progressDialog != null && KradChart.this.progressDialog.isShowing()) {
                    KradChart.this.progressDialog.dismiss();
                    if (result.booleanValue()) {
                        KradChart.this.enableAllRadicals();
                        KradChart.this.adapter = new KradAdapter(KradChart.this, R.layout.krad_item, KradChart.this.radicals);
                        KradChart.this.radicalChartGrid.setAdapter((ListAdapter) KradChart.this.adapter);
                        Dialogs.showTipOnce(KradChart.this, KradChart.MULTI_RADICAL_TIP, R.string.multi_radical_search_tip);
                        return;
                    }
                    Toast.makeText(KradChart.this, "error loading radkfile-u " + this.error.getMessage(), 0).show();
                }
            }
        }.execute(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        this.state = (State) savedInstanceState.getSerializable(STATE_KEY);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(STATE_KEY, this.state);
    }

    private void displayTotalMatches() {
        String totalMatchesTemplate = getResources().getString(R.string.total_matches);
        this.totalMatchesText.setText(String.format(totalMatchesTemplate, Integer.valueOf(this.state.matchingKanjis.size())));
    }

    /* access modifiers changed from: private */
    public void initKradDb() {
        if (!this.kradDb.isInitialized()) {
            try {
                this.kradDb.readFromStream(getAssets().open("radkfile-u-jis208.txt"));
            } catch (IOException e) {
                IOException e2 = e;
                Log.e(TAG, "error reading radkfile-u", e2);
                throw new RuntimeException(e2);
            }
        }
    }

    /* access modifiers changed from: private */
    public void enableAllRadicals() {
        for (String radical : this.radicals) {
            if (!isStrokeNumLabel(radical)) {
                this.state.enabledRadicals.add(radical.trim());
            }
        }
    }

    public class KradAdapter extends ArrayAdapter<String> {
        public KradAdapter(Context context, int textViewResourceId, List<String> objects) {
            super(context, textViewResourceId, objects);
        }

        public View getView(int position, View convertView, ViewGroup viewGroup) {
            TextView result = (TextView) super.getView(position, convertView, viewGroup);
            result.setTextColor(-1);
            result.setBackgroundColor(0);
            result.setTextSize(24.0f);
            String modelStr = (String) getItem(position);
            if (KradChart.isStrokeNumLabel(modelStr)) {
                result.setBackgroundColor(-7829368);
            } else {
                String displayStr = KradChart.toDisplayStr(modelStr.trim());
                result.setText(displayStr);
                if (KradChart.REPLACED_CHARS.contains(displayStr)) {
                    result.setTextColor(-3355444);
                }
            }
            if (KradChart.this.isSelected(modelStr)) {
                result.setBackgroundColor(-16711936);
            }
            if (KradChart.this.isDisabled(modelStr)) {
                result.setBackgroundColor(-12303292);
            }
            return result;
        }

        public boolean areAllItemsEnabled() {
            return false;
        }

        public boolean isEnabled(int position) {
            String text = (String) getItem(position);
            return !KradChart.isStrokeNumLabel(text) && !KradChart.this.isDisabled(text);
        }
    }

    /* access modifiers changed from: private */
    public static boolean isStrokeNumLabel(String str) {
        return NUM_STROKES.contains(str.trim());
    }

    /* access modifiers changed from: private */
    public boolean isSelected(String radical) {
        return this.state.selectedRadicals.contains(radical);
    }

    /* access modifiers changed from: private */
    public boolean isDisabled(String radical) {
        return !isStrokeNumLabel(radical) && !this.state.enabledRadicals.contains(radical);
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Analytics.event("multiradicalSelect", this);
        String radical = this.radicals.get(position).trim();
        if (this.state.selectedRadicals.contains(radical)) {
            this.state.selectedRadicals.remove(radical);
        } else {
            this.state.selectedRadicals.add(radical);
        }
        if (this.state.selectedRadicals.isEmpty()) {
            enableAllRadicals();
            this.state.matchingKanjis.clear();
            this.matchedKanjiText.setText((int) R.string.no_matches);
        } else {
            this.state.matchingKanjis = this.kradDb.getKanjisForRadicals(this.state.selectedRadicals);
            addClickableKanji(this.matchedKanjiText);
            Log.d(TAG, "matching kanjis: " + this.state.matchingKanjis);
            this.state.enabledRadicals = this.kradDb.getRadicalsForKanjis(this.state.matchingKanjis);
            Log.d(TAG, "enabled radicals: " + this.state.enabledRadicals);
        }
        toggleButtons();
        displayTotalMatches();
        this.adapter.notifyDataSetChanged();
    }

    private void addClickableKanji(TextView textView) {
        if (!this.state.matchingKanjis.isEmpty()) {
            String[] matchingChars = (String[]) this.state.matchingKanjis.toArray(new String[this.state.matchingKanjis.size()]);
            Arrays.sort(matchingChars);
            String[] charsToDisplay = new String[10];
            if (matchingChars.length < charsToDisplay.length) {
                charsToDisplay = new String[matchingChars.length];
            }
            System.arraycopy(matchingChars, 0, charsToDisplay, 0, charsToDisplay.length);
            String text = TextUtils.join(" ", charsToDisplay);
            if (matchingChars.length > charsToDisplay.length) {
                text = String.valueOf(text) + " " + "...";
            }
            SpannableString str = new SpannableString(text);
            int length = charsToDisplay.length;
            for (int i = 0; i < length; i++) {
                String c = charsToDisplay[i];
                int idx = text.indexOf(c);
                if (idx != -1) {
                    Intent intent = createCharDetailsIntent(c);
                    if (idx + 1 > str.length() - 1) {
                        int end = str.length();
                    }
                    str.setSpan(new IntentSpan(this, intent), idx, idx + 1, 34);
                }
            }
            int idx2 = text.indexOf("...");
            if (idx2 != -1) {
                str.setSpan(new IntentSpan(this, createShowAllIntent()), idx2, "...".length() + idx2, 34);
            }
            textView.setText(str);
            textView.setLinkTextColor(-1);
            MovementMethod m = textView.getMovementMethod();
            if ((m == null || !(m instanceof LinkMovementMethod)) && textView.getLinksClickable()) {
                textView.setMovementMethod(LinkMovementMethod.getInstance());
            }
        }
    }

    private Intent createCharDetailsIntent(String kanji) {
        Intent intent = new Intent(this, KanjiResultListView.class);
        intent.putExtra(Constants.CRITERIA_KEY, SearchCriteria.createForKanjiOrReading(kanji));
        return intent;
    }

    private void toggleButtons() {
        boolean matchesFound = !this.state.matchingKanjis.isEmpty();
        this.showAllButton.setEnabled(matchesFound);
        this.clearButton.setEnabled(matchesFound);
    }

    /* access modifiers changed from: private */
    public static String toDisplayStr(String radical) {
        String displayChar = KRAD_TO_DISPLAY.get(radical);
        if (displayChar == null) {
            displayChar = radical;
        }
        if (displayChar != radical) {
            Log.d(TAG, String.format("%s %s", radical, displayChar));
        }
        return displayChar;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.show_all_button:
                showCandidates();
                return;
            case R.id.clear_button:
                clearSelection();
                return;
            default:
                return;
        }
    }

    private void clearSelection() {
        this.state.selectedRadicals.clear();
        this.state.matchingKanjis.clear();
        enableAllRadicals();
        this.matchedKanjiText.setText((int) R.string.no_matches);
        displayTotalMatches();
        toggleButtons();
        this.adapter.notifyDataSetChanged();
    }

    private void showCandidates() {
        Analytics.event("multiradicalShowAll", this);
        startActivity(createShowAllIntent());
    }

    private Intent createShowAllIntent() {
        String[] matchingChars = (String[]) this.state.matchingKanjis.toArray(new String[this.state.matchingKanjis.size()]);
        Arrays.sort(matchingChars);
        Intent intent = new Intent(this, HkrCandidates.class);
        intent.putExtra(Constants.HKR_CANDIDATES_KEY, matchingChars);
        return intent;
    }
}
