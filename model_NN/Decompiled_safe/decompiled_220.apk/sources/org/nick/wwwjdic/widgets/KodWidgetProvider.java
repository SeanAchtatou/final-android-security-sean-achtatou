package org.nick.wwwjdic.widgets;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.RemoteViews;
import java.util.Date;
import java.util.List;
import org.nick.wwwjdic.Constants;
import org.nick.wwwjdic.KanjiEntry;
import org.nick.wwwjdic.KanjiEntryDetail;
import org.nick.wwwjdic.R;
import org.nick.wwwjdic.WwwjdicPreferences;

public class KodWidgetProvider extends AppWidgetProvider {
    private static final String TAG = KodWidgetProvider.class.getSimpleName();

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        Log.d(TAG, "KOD widget udpate");
        context.startService(new Intent(context, GetKanjiService.class));
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Log.d(TAG, "KOD widget onReceive: " + action);
        if ("android.appwidget.action.APPWIDGET_DELETED".equals(action)) {
            int appWidgetId = intent.getExtras().getInt("appWidgetId", 0);
            if (appWidgetId != 0) {
                onDeleted(context, new int[]{appWidgetId});
            }
        } else if ("android.net.conn.CONNECTIVITY_CHANGE".equals(action)) {
            Bundle extras = intent.getExtras();
            boolean noConnectivity = extras.getBoolean("noConnectivity", false);
            Log.d(TAG, "CONNECTIVITY_ACTION::noConnectivity : " + noConnectivity);
            if (!noConnectivity) {
                NetworkInfo ni = (NetworkInfo) extras.getParcelable("networkInfo");
                if (ni.isConnected()) {
                    Log.d(TAG, String.valueOf(ni.getTypeName()) + " is connecting");
                    if (WwwjdicPreferences.getLastKodUpdateError(context) != 0) {
                        Log.d(TAG, "KOD widget is in error state, trying to update...");
                        context.startService(new Intent(context, GetKanjiService.class));
                    }
                }
            }
        } else {
            super.onReceive(context, intent);
        }
    }

    public void onDeleted(Context context, int[] appWidgetIds) {
        Log.d(TAG, "onDeleted");
        int[] thisWidgetIds = AppWidgetManager.getInstance(context).getAppWidgetIds(new ComponentName(context, KodWidgetProvider.class));
        Log.d(TAG, "widget IDs: " + thisWidgetIds.length);
        if (thisWidgetIds.length == 0) {
            Log.d(TAG, "we are the last widget, cleaning up");
            Log.d(TAG, "stopping update service...");
            Log.d(TAG, "stopped: " + context.stopService(new Intent(context, GetKanjiService.class)));
            Log.d(TAG, "cancelling update timer...");
            ((AlarmManager) context.getSystemService("alarm")).cancel(PendingIntent.getService(context, 0, new Intent(context, GetKanjiService.class), 0));
            Log.d(TAG, "done");
        }
    }

    public static void showError(Context context, RemoteViews views) {
        views.setViewVisibility(R.id.kod_message_text, 0);
        views.setTextViewText(R.id.kod_message_text, context.getResources().getString(R.string.error));
        views.setViewVisibility(R.id.widget, 8);
        views.setOnClickPendingIntent(R.id.kod_message_text, PendingIntent.getService(context, 0, new Intent(context, GetKanjiService.class), 134217728));
    }

    public static void showLoading(Context context, RemoteViews views) {
        views.setTextViewText(R.id.kod_message_text, context.getResources().getString(R.string.widget_loading));
        views.setViewVisibility(R.id.kod_message_text, 0);
        views.setViewVisibility(R.id.widget, 8);
    }

    public static void clearLoading(Context context, RemoteViews views) {
        views.setViewVisibility(R.id.kod_message_text, 8);
        views.setViewVisibility(R.id.widget, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void showKanji(Context context, RemoteViews views, boolean showReadingAndMeaning, List<KanjiEntry> entries) {
        KanjiEntry entry = entries.get(0);
        String kod = entry.getHeadword();
        Log.d(TAG, "KOD: " + kod);
        Intent intent = new Intent(context, KanjiEntryDetail.class);
        intent.putExtra(Constants.KANJI_ENTRY_KEY, entries.get(0));
        intent.putExtra(Constants.KOD_WIDGET_CLICK, true);
        intent.setFlags(335544320);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 268435456);
        views.setTextViewText(R.id.kod_date_text, DateFormat.getDateFormat(context).format(new Date()));
        views.setTextViewText(R.id.kod_text, kod);
        if (showReadingAndMeaning) {
            views.setTextViewText(R.id.kod_reading, entry.getReading());
            views.setTextViewText(R.id.kod_meaning, entry.getMeaningsAsString());
        }
        views.setOnClickPendingIntent(R.id.widget, pendingIntent);
        clearLoading(context, views);
    }
}
