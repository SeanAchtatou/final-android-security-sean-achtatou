package org.nick.wwwjdic.widgets;

import java.io.UnsupportedEncodingException;
import java.util.Random;

public class RandomJisGenerator {
    private static final int JIS_GRID_SIZE = 94;
    private static final int KANJI_END_LINE = 84;
    private static final int KANJI_START_LINE = 16;
    private static final int LEVEL1_END_LINE = 47;
    private static final int LEVEL1_END_LINE_NUM_KANJI = 51;
    private static final int LEVEL1_START_LINE = 16;
    private static final int NUM_KANJI_LAST_LINE = 6;
    private static final int OFFSET = 32;
    private Random random = new Random();

    public String generateRawJis() {
        return generateRawJis(false);
    }

    public String generateRawJis(boolean limitToLevelOne) {
        int column;
        int startLine = 16;
        int endLine = KANJI_END_LINE;
        if (limitToLevelOne) {
            startLine = 16;
            endLine = LEVEL1_END_LINE;
        }
        int line = this.random.nextInt((endLine - startLine) + 1) + 16;
        if (limitToLevelOne && line == LEVEL1_END_LINE) {
            column = this.random.nextInt(LEVEL1_END_LINE_NUM_KANJI) + 1;
        } else if (limitToLevelOne || line != KANJI_END_LINE) {
            column = this.random.nextInt(JIS_GRID_SIZE);
        } else {
            column = this.random.nextInt(6) + 1;
        }
        return String.valueOf(Integer.toString(line + 32, 16)) + Integer.toString(column + 32, 16);
    }

    public String generateAsUnicodeCp(boolean limitToLevelOne) {
        return rawJisToUnicodeCp(generateRawJis(limitToLevelOne));
    }

    private static String rawJisToUnicodeCp(String rawJis) {
        if (rawJis.length() != 4) {
            throw new IllegalArgumentException("Invalid raw JIS code");
        }
        try {
            return Integer.toString(new String(new byte[]{(byte) (Byte.parseByte(rawJis.substring(0, 2), 16) + 128), (byte) (Byte.parseByte(rawJis.substring(2), 16) + 128)}, "EUC-JP").toCharArray()[0], 16);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}
