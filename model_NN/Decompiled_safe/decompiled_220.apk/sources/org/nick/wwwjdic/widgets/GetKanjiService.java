package org.nick.wwwjdic.widgets;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.text.format.Time;
import android.util.Log;
import android.widget.RemoteViews;
import au.com.bytecode.opencsv.CSVWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.regex.Pattern;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.nick.wwwjdic.HttpClientFactory;
import org.nick.wwwjdic.JlptLevels;
import org.nick.wwwjdic.KanjiEntry;
import org.nick.wwwjdic.R;
import org.nick.wwwjdic.WwwjdicPreferences;
import org.nick.wwwjdic.utils.StringUtils;

public class GetKanjiService extends Service {
    private static final int NUM_RETRIES = 5;
    private static final Pattern PRE_END_PATTERN = Pattern.compile("^</pre>.*$");
    private static final String PRE_END_TAG = "</pre>";
    private static final Pattern PRE_START_PATTERN = Pattern.compile("^<pre>.*$");
    private static final int RETRY_INTERVAL = 15000;
    private static final String TAG = GetKanjiService.class.getSimpleName();
    private final Executor executor = Executors.newSingleThreadExecutor();
    private HttpClient httpclient;
    private final RandomJisGenerator jisGenerator = new RandomJisGenerator();
    private ResponseHandler<String> responseHandler;

    public void onStart(Intent intent, int startId) {
        this.httpclient = HttpClientFactory.createWwwjdicHttpClient(WwwjdicPreferences.getWwwjdicTimeoutSeconds(this) * 1000);
        this.responseHandler = HttpClientFactory.createWwwjdicResponseHandler();
        this.executor.execute(new GetKanjiTask(this, null));
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    private class GetKanjiTask implements Runnable {
        private GetKanjiTask() {
        }

        /* synthetic */ GetKanjiTask(GetKanjiService getKanjiService, GetKanjiTask getKanjiTask) {
            this();
        }

        public void run() {
            try {
                GetKanjiService.this.updateKodWidgets(GetKanjiService.this, GetKanjiService.this.buildUpdate(GetKanjiService.this));
            } finally {
                GetKanjiService.this.scheduleNextUpdate();
                GetKanjiService.this.stopSelf();
            }
        }
    }

    /* access modifiers changed from: private */
    public void updateKodWidgets(Context context, RemoteViews updateViews) {
        AppWidgetManager.getInstance(context).updateAppWidget(new ComponentName(context, KodWidgetProvider.class), updateViews);
    }

    /* access modifiers changed from: private */
    public void scheduleNextUpdate() {
        Time time = new Time();
        time.set(System.currentTimeMillis() + WwwjdicPreferences.getKodUpdateInterval(this));
        long nextUpdate = time.toMillis(false);
        Log.d(TAG, "Requesting next update at " + time + ", in " + ((nextUpdate - System.currentTimeMillis()) / 60000) + " min");
        ((AlarmManager) getSystemService("alarm")).set(1, nextUpdate, PendingIntent.getService(this, 0, new Intent(this, GetKanjiService.class), 0));
    }

    /* access modifiers changed from: private */
    public RemoteViews buildUpdate(Context context) {
        RemoteViews views;
        try {
            boolean showReadingAndMeaning = WwwjdicPreferences.isKodShowReading(this);
            if (showReadingAndMeaning) {
                views = new RemoteViews(context.getPackageName(), (int) R.layout.kod_widget_details);
            } else {
                views = new RemoteViews(context.getPackageName(), (int) R.layout.kod_widget);
            }
            KodWidgetProvider.showLoading(this, views);
            updateKodWidgets(this, views);
            String unicodeCp = selectKanji(context);
            Log.d(TAG, "KOD Unicode CP: " + unicodeCp);
            String backdoorCode = generateBackdoorCode(unicodeCp);
            Log.d(TAG, "backdoor code: " + backdoorCode);
            String wwwjdicResponse = null;
            int i = 0;
            while (i < 5) {
                try {
                    wwwjdicResponse = query(WwwjdicPreferences.getWwwjdicUrl(this), backdoorCode);
                    if (wwwjdicResponse != null) {
                        break;
                    }
                    i++;
                } catch (Exception e) {
                    Exception e2 = e;
                    if (i < 4) {
                        Log.w(TAG, String.format("Couldn't contact WWWJDIC, will retry after %d ms.", Integer.valueOf((int) RETRY_INTERVAL)), e2);
                        Thread.sleep((long) ((i + 1) * RETRY_INTERVAL));
                    } else {
                        Log.e(TAG, "Couldn't contact WWWJDIC.", e2);
                    }
                }
            }
            if (wwwjdicResponse == null) {
                Log.e(TAG, String.format("Failed to get WWWJDIC response after %d tries, giving up.", 5));
                WwwjdicPreferences.setLastKodUpdateError(context, System.currentTimeMillis());
                KodWidgetProvider.showError(this, views);
                return views;
            }
            Log.d(TAG, "WWWJDIC response " + wwwjdicResponse);
            List<KanjiEntry> entries = parseResult(wwwjdicResponse);
            if (entries.isEmpty()) {
                WwwjdicPreferences.setLastKodUpdateError(context, System.currentTimeMillis());
                KodWidgetProvider.showError(this, views);
                return views;
            }
            KodWidgetProvider.showKanji(context, views, showReadingAndMeaning, entries);
            WwwjdicPreferences.setLastKodUpdateError(context, 0);
            return views;
        } catch (Exception e3) {
            Log.e(TAG, "Couldn't contact WWWJDIC", e3);
            RemoteViews views2 = new RemoteViews(context.getPackageName(), (int) R.layout.kod_widget);
            WwwjdicPreferences.setLastKodUpdateError(context, System.currentTimeMillis());
            KodWidgetProvider.showError(this, views2);
            return views2;
        }
    }

    private String selectKanji(Context context) {
        if (!WwwjdicPreferences.isKodUseJlpt(this) || WwwjdicPreferences.isKodLevelOneOnly(this)) {
            return this.jisGenerator.generateAsUnicodeCp(WwwjdicPreferences.isKodLevelOneOnly(context));
        }
        String[] kanjis = JlptLevels.getKanjiForLevel(WwwjdicPreferences.getKodJlptLevel(this));
        return Integer.toString(kanjis[new Random().nextInt(kanjis.length)].toCharArray()[0], 16);
    }

    private String query(String url, String backdoorCode) {
        try {
            return (String) this.httpclient.execute(new HttpGet(String.format("%s?%s", url, backdoorCode)), this.responseHandler);
        } catch (ClientProtocolException e) {
            ClientProtocolException cpe = e;
            Log.e("WWWJDIC", "ClientProtocolException", cpe);
            throw new RuntimeException((Throwable) cpe);
        } catch (IOException e2) {
            IOException e3 = e2;
            Log.e("WWWJDIC", "IOException", e3);
            throw new RuntimeException(e3);
        }
    }

    /* access modifiers changed from: protected */
    public List<KanjiEntry> parseResult(String html) {
        List<KanjiEntry> result = new ArrayList<>();
        boolean isInPre = false;
        for (String line : html.split(CSVWriter.DEFAULT_LINE_END)) {
            if (!StringUtils.isEmpty(line)) {
                if (PRE_START_PATTERN.matcher(line).matches()) {
                    isInPre = true;
                } else if (PRE_END_PATTERN.matcher(line).matches()) {
                    break;
                } else if (isInPre) {
                    boolean hasEndPre = false;
                    if (line.contains(PRE_END_TAG)) {
                        hasEndPre = true;
                        line = line.replaceAll(PRE_END_TAG, "");
                    }
                    Log.d(TAG, "dic entry line: " + line);
                    result.add(KanjiEntry.parseKanjidic(line));
                    if (hasEndPre) {
                        break;
                    }
                } else {
                    continue;
                }
            }
        }
        return result;
    }

    private String generateBackdoorCode(String jisCode) {
        StringBuffer buff = new StringBuffer();
        buff.append("1");
        buff.append("Z");
        buff.append("K");
        buff.append("U");
        try {
            buff.append(URLEncoder.encode(jisCode, "UTF-8"));
            return buff.toString();
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}
