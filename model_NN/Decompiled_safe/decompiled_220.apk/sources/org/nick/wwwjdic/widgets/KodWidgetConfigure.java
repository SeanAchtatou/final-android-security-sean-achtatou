package org.nick.wwwjdic.widgets;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.TextView;
import org.nick.wwwjdic.R;
import org.nick.wwwjdic.WwwjdicPreferences;

public class KodWidgetConfigure extends Activity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private static final int ONE_DAY_IDX = 0;
    private static final long ONE_DAY_MILLIS = 86400000;
    private static final int SIX_HOURS_IDX = 2;
    private static final long SIX_HOURS_MILLIS = 21600000;
    private static final int TWELVE_HOURS_IDX = 1;
    private static final long TWELVE_HOURS_MILLIS = 43200000;
    private static final int TWO_MINUTES_IDX = 3;
    private static final long TWO_MINUTES_MILLIS = 120000;
    private int appWidgetId = 0;
    private TextView jlptLevelLabel;
    private Spinner jlptLevelSpinner;
    private CheckBox levelOneCb;
    private CheckBox showReadingCb;
    private Spinner updateIntervalSpinner;
    private CheckBox useJlptCb;

    public void onCreate(Bundle icicle) {
        boolean z;
        boolean z2;
        super.onCreate(icicle);
        setResult(0);
        setContentView((int) R.layout.kod_widget_configure);
        findViews();
        boolean isJisLevel1 = WwwjdicPreferences.isKodLevelOneOnly(this);
        boolean isUseJlpt = WwwjdicPreferences.isKodUseJlpt(this);
        this.levelOneCb.setChecked(isJisLevel1);
        this.useJlptCb.setChecked(isUseJlpt);
        CheckBox checkBox = this.levelOneCb;
        if (isUseJlpt) {
            z = false;
        } else {
            z = true;
        }
        checkBox.setEnabled(z);
        CheckBox checkBox2 = this.useJlptCb;
        if (isJisLevel1) {
            z2 = false;
        } else {
            z2 = true;
        }
        checkBox2.setEnabled(z2);
        this.jlptLevelSpinner.setEnabled(isUseJlpt);
        this.jlptLevelLabel.setEnabled(isUseJlpt);
        this.showReadingCb.setChecked(WwwjdicPreferences.isKodShowReading(this));
        long updateInterval = WwwjdicPreferences.getKodUpdateInterval(this);
        if (updateInterval == TWELVE_HOURS_MILLIS) {
            this.updateIntervalSpinner.setSelection(1);
        } else if (updateInterval == SIX_HOURS_MILLIS) {
            this.updateIntervalSpinner.setSelection(2);
        } else if (updateInterval == TWO_MINUTES_MILLIS) {
            this.updateIntervalSpinner.setSelection(3);
        } else {
            this.updateIntervalSpinner.setSelection(0);
        }
        this.jlptLevelSpinner.setSelection(WwwjdicPreferences.getKodJlptLevel(this) - 1);
        findViewById(R.id.kod_configure_ok_button).setOnClickListener(this);
        findViewById(R.id.kod_configure_cancel_button).setOnClickListener(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.appWidgetId = extras.getInt("appWidgetId", 0);
        }
        if (this.appWidgetId == 0) {
            finish();
        }
    }

    private void findViews() {
        this.levelOneCb = (CheckBox) findViewById(R.id.kod_level1_only_cb);
        this.levelOneCb.setOnCheckedChangeListener(this);
        this.useJlptCb = (CheckBox) findViewById(R.id.kod_use_jlpt_cb);
        this.useJlptCb.setOnCheckedChangeListener(this);
        this.jlptLevelLabel = (TextView) findViewById(R.id.jlpt_level_label);
        this.jlptLevelSpinner = (Spinner) findViewById(R.id.kod_jlpt_level_spinner);
        this.showReadingCb = (CheckBox) findViewById(R.id.kod_show_reading_cb);
        this.updateIntervalSpinner = (Spinner) findViewById(R.id.kod_update_interval_spinner);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.kod_configure_ok_button:
                WwwjdicPreferences.setKodLevelOneOnly(this, this.levelOneCb.isChecked());
                WwwjdicPreferences.setKodUseJlpt(this, this.useJlptCb.isChecked());
                WwwjdicPreferences.setKodJlptLevel(this, this.jlptLevelSpinner.getSelectedItemPosition() + 1);
                WwwjdicPreferences.setKodShowReading(this, this.showReadingCb.isChecked());
                long updateInterval = 86400000;
                switch (this.updateIntervalSpinner.getSelectedItemPosition()) {
                    case 0:
                        updateInterval = 86400000;
                        break;
                    case 1:
                        updateInterval = TWELVE_HOURS_MILLIS;
                        break;
                    case 2:
                        updateInterval = SIX_HOURS_MILLIS;
                        break;
                    case 3:
                        updateInterval = TWO_MINUTES_MILLIS;
                        break;
                }
                WwwjdicPreferences.setKodUpdateInterval(this, updateInterval);
                startService(new Intent(this, GetKanjiService.class));
                Intent resultValue = new Intent();
                resultValue.putExtra("appWidgetId", this.appWidgetId);
                setResult(-1, resultValue);
                finish();
                return;
            case R.id.kod_configure_cancel_button:
                finish();
                return;
            default:
                return;
        }
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        switch (buttonView.getId()) {
            case R.id.kod_level1_only_cb:
                CheckBox checkBox = this.useJlptCb;
                if (isChecked) {
                    z2 = false;
                } else {
                    z2 = true;
                }
                checkBox.setEnabled(z2);
                Spinner spinner = this.jlptLevelSpinner;
                if (isChecked) {
                    z3 = false;
                } else {
                    z3 = true;
                }
                spinner.setEnabled(z3);
                TextView textView = this.jlptLevelLabel;
                if (isChecked) {
                    z4 = false;
                } else {
                    z4 = true;
                }
                textView.setEnabled(z4);
                return;
            case R.id.kod_use_jlpt_cb:
                CheckBox checkBox2 = this.levelOneCb;
                if (isChecked) {
                    z = false;
                } else {
                    z = true;
                }
                checkBox2.setEnabled(z);
                this.jlptLevelLabel.setEnabled(isChecked);
                this.jlptLevelSpinner.setEnabled(isChecked);
                return;
            default:
                return;
        }
    }
}
