package org.nick.wwwjdic;

import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.RejectedExecutionException;
import org.nick.wwwjdic.history.FavoritesAndHistorySummaryView;
import org.nick.wwwjdic.history.HistoryDbHelper;
import org.nick.wwwjdic.hkr.RecognizeKanjiActivity;
import org.nick.wwwjdic.krad.KradChart;
import org.nick.wwwjdic.ocr.OcrActivity;
import org.nick.wwwjdic.utils.Analytics;
import org.nick.wwwjdic.utils.IntentSpan;
import org.nick.wwwjdic.utils.StringUtils;

public class KanjiLookup extends WwwjdicActivityBase implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    private static final Map<Integer, String> IDX_TO_CODE = new HashMap();
    private static final int NUM_RECENT_HISTORY_ENTRIES = 5;
    private static final String TAG = KanjiLookup.class.getSimpleName();
    private HistoryDbHelper dbHelper;
    private FavoritesAndHistorySummaryView kanjiHistorySummary;
    private EditText kanjiInputText;
    private Spinner kanjiSearchTypeSpinner;
    private EditText radicalEditText;
    private Button selectRadicalButton;
    private EditText strokeCountMaxInput;
    private EditText strokeCountMinInput;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.kanji_lookup);
        populateIdxToCode();
        findViews();
        setupListeners();
        setupSpinners();
        setupTabOrder();
        toggleRadicalStrokeCountPanel(false);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String searchKey = extras.getString(Constants.SEARCH_TEXT_KEY);
            int searchType = extras.getInt(Constants.SEARCH_TYPE);
            if (searchKey != null) {
                switch (searchType) {
                    case 1:
                        this.kanjiInputText.setText(searchKey);
                        break;
                }
                this.inputTextFromBundle = true;
            }
        }
        this.dbHelper = HistoryDbHelper.getInstance(this);
        setupKanjiSummary();
        setupClickableLinks();
    }

    private void populateIdxToCode() {
        if (IDX_TO_CODE.isEmpty()) {
            String[] kanjiSearchCodesArray = getResources().getStringArray(R.array.kanji_search_codes_array);
            for (int i = 0; i < kanjiSearchCodesArray.length; i++) {
                IDX_TO_CODE.put(Integer.valueOf(i), kanjiSearchCodesArray[i]);
            }
        }
    }

    private void setupClickableLinks() {
        findViewById(R.id.kanji_history_summary).setNextFocusDownId(R.id.hwrSearchLink);
        TextView textView = (TextView) findViewById(R.id.hwrSearchLink);
        makeClickable(textView, new Intent(this, RecognizeKanjiActivity.class));
        textView.setNextFocusUpId(R.id.kanji_history_summary);
        textView.setNextFocusDownId(R.id.ocrSearchLink);
        TextView textView2 = (TextView) findViewById(R.id.ocrSearchLink);
        makeClickable(textView2, new Intent(this, OcrActivity.class));
        textView2.setNextFocusUpId(R.id.hwrSearchLink);
        textView2.setNextFocusDownId(R.id.multiRadicalSearchLink);
        TextView textView3 = (TextView) findViewById(R.id.multiRadicalSearchLink);
        makeClickable(textView3, new Intent(this, KradChart.class));
        textView3.setNextFocusUpId(R.id.ocrSearchLink);
    }

    private void makeClickable(TextView textView, Intent intent) {
        String text = textView.getText().toString();
        SpannableString str = new SpannableString(text);
        str.setSpan(new IntentSpan(this, intent), 0, text.length(), 34);
        textView.setText(str);
        textView.setLinkTextColor(-1);
        MovementMethod m = textView.getMovementMethod();
        if ((m == null || !(m instanceof LinkMovementMethod)) && textView.getLinksClickable()) {
            textView.setMovementMethod(LinkMovementMethod.getInstance());
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        setupKanjiSummary();
        setupClickableLinks();
    }

    private void setupKanjiSummary() {
        this.dbHelper.beginTransaction();
        try {
            long numAllFavorites = this.dbHelper.getKanjiFavoritesCount();
            List<String> recentFavorites = this.dbHelper.getRecentKanjiFavorites(5);
            long numAllHistory = this.dbHelper.getKanjiHistoryCount();
            List<String> recentHistory = this.dbHelper.getRecentKanjiHistory(5);
            this.kanjiHistorySummary.setFavoritesFilterType(1);
            this.kanjiHistorySummary.setHistoryFilterType(1);
            this.kanjiHistorySummary.setRecentEntries(numAllFavorites, recentFavorites, numAllHistory, recentHistory);
            this.dbHelper.setTransactionSuccessful();
        } finally {
            this.dbHelper.endTransaction();
        }
    }

    private void setupListeners() {
        findViewById(R.id.kanjiSearchButton).setOnClickListener(this);
        this.selectRadicalButton.setOnClickListener(this);
    }

    private void setupSpinners() {
        ArrayAdapter<CharSequence> kajiSearchTypeAdapter = ArrayAdapter.createFromResource(this, R.array.kanji_search_types_array, R.layout.spinner_text);
        kajiSearchTypeAdapter.setDropDownViewResource(17367049);
        this.kanjiSearchTypeSpinner.setAdapter((SpinnerAdapter) kajiSearchTypeAdapter);
        this.kanjiSearchTypeSpinner.setOnItemSelectedListener(this);
    }

    private void setupTabOrder() {
        this.strokeCountMinInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case 5:
                        EditText v1 = (EditText) v.focusSearch(66);
                        if (v1 == null || v1.requestFocus(66)) {
                            return true;
                        }
                        throw new IllegalStateException("unfocucsable view");
                    default:
                        return true;
                }
            }
        });
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.selectRadicalButton /*2131296351*/:
                startActivityForResult(new Intent(this, RadicalChart.class), 0);
                return;
            case R.id.strokeCountMinInput /*2131296352*/:
            case R.id.strokeCountMaxInput /*2131296353*/:
            default:
                return;
            case R.id.kanjiSearchButton /*2131296354*/:
                String kanjiInput = this.kanjiInputText.getText().toString();
                if (!TextUtils.isEmpty(kanjiInput)) {
                    try {
                        int searchTypeIdx = this.kanjiSearchTypeSpinner.getSelectedItemPosition();
                        String searchType = IDX_TO_CODE.get(Integer.valueOf(searchTypeIdx));
                        Log.i(TAG, Integer.toString(searchTypeIdx));
                        Log.i(TAG, "kanji search type: " + searchType);
                        if (searchType == null) {
                            searchType = "J";
                        }
                        String minStr = this.strokeCountMinInput.getText().toString();
                        String maxStr = this.strokeCountMaxInput.getText().toString();
                        SearchCriteria criteria = SearchCriteria.createWithStrokeCount(kanjiInput.trim(), searchType, tryParseInt(minStr), tryParseInt(maxStr));
                        Intent intent = new Intent(this, KanjiResultListView.class);
                        intent.putExtra(Constants.CRITERIA_KEY, criteria);
                        if (!StringUtils.isEmpty(criteria.getQueryString())) {
                            this.dbHelper.addSearchCriteria(criteria);
                        }
                        Analytics.event("kanjiSearch", this);
                        startActivity(intent);
                        return;
                    } catch (RejectedExecutionException e) {
                        Log.e(TAG, "RejectedExecutionException", e);
                        return;
                    }
                } else {
                    return;
                }
        }
    }

    private Integer tryParseInt(String str) {
        try {
            return Integer.valueOf(Integer.parseInt(str));
        } catch (Exception e) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 0 && resultCode == -1) {
            Radical radical = (Radical) intent.getExtras().getSerializable(Constants.RADICAL_KEY);
            this.kanjiInputText.setText(Integer.toString(radical.getNumber()));
            this.radicalEditText.setText(radical.getGlyph().substring(0, 1));
        }
    }

    private void findViews() {
        this.kanjiInputText = (EditText) findViewById(R.id.kanjiInputText);
        this.kanjiSearchTypeSpinner = (Spinner) findViewById(R.id.kanjiSearchTypeSpinner);
        this.radicalEditText = (EditText) findViewById(R.id.radicalInputText);
        this.strokeCountMinInput = (EditText) findViewById(R.id.strokeCountMinInput);
        this.strokeCountMaxInput = (EditText) findViewById(R.id.strokeCountMaxInput);
        this.selectRadicalButton = (Button) findViewById(R.id.selectRadicalButton);
        this.kanjiHistorySummary = (FavoritesAndHistorySummaryView) findViewById(R.id.kanji_history_summary);
    }

    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (this.inputTextFromBundle) {
            this.inputTextFromBundle = false;
            return;
        }
        switch (parent.getId()) {
            case R.id.kanjiSearchTypeSpinner /*2131296347*/:
                this.kanjiInputText.setText("");
                this.kanjiInputText.requestFocus();
                if (position == 1 || position == 2) {
                    this.kanjiInputText.setInputType(2);
                } else {
                    this.kanjiInputText.setInputType(1);
                }
                if (position != 2) {
                    toggleRadicalStrokeCountPanel(false);
                    return;
                } else {
                    toggleRadicalStrokeCountPanel(true);
                    return;
                }
            default:
                return;
        }
    }

    private void toggleRadicalStrokeCountPanel(boolean isEnabled) {
        this.selectRadicalButton.setEnabled(isEnabled);
        this.strokeCountMinInput.setEnabled(isEnabled);
        this.strokeCountMinInput.setFocusableInTouchMode(isEnabled);
        this.strokeCountMaxInput.setEnabled(isEnabled);
        this.strokeCountMaxInput.setFocusableInTouchMode(isEnabled);
        if (!isEnabled) {
            this.strokeCountMinInput.setText("");
            this.strokeCountMaxInput.setText("");
            this.radicalEditText.setText("");
            this.kanjiInputText.requestFocus();
        }
    }

    public void onNothingSelected(AdapterView<?> adapterView) {
    }
}
