package org.nick.wwwjdic;

import java.io.Serializable;

public class Radical implements Serializable {
    private static final long serialVersionUID = 1;
    private String glyph;
    private int numStrokes;
    private int number;

    public Radical(int number2, String glyph2, int numStrokes2) {
        this.number = number2;
        this.glyph = glyph2;
        this.numStrokes = numStrokes2;
    }

    public int getNumber() {
        return this.number;
    }

    public String getGlyph() {
        return this.glyph;
    }

    public int getNumStrokes() {
        return this.numStrokes;
    }
}
