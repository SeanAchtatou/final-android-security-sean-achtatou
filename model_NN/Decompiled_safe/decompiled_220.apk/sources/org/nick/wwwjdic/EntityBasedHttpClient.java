package org.nick.wwwjdic;

import java.io.BufferedReader;
import java.io.IOException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

public abstract class EntityBasedHttpClient {
    protected DefaultHttpClient httpClient;
    protected String url;

    /* access modifiers changed from: protected */
    public abstract AbstractHttpEntity createEntity(Object... objArr) throws IOException;

    public EntityBasedHttpClient(String endpoint, int timeout) {
        this.url = endpoint;
        this.httpClient = new DefaultHttpClient(createHttpParams(timeout));
    }

    /* access modifiers changed from: protected */
    public HttpParams createHttpParams(int timeout) {
        HttpParams params = new BasicHttpParams();
        HttpProtocolParams.setContentCharset(params, "ISO-8859-1");
        HttpProtocolParams.setUseExpectContinue(params, true);
        HttpConnectionParams.setConnectionTimeout(params, timeout);
        HttpProtocolParams.setUserAgent(params, WwwjdicApplication.getUserAgentString());
        return params;
    }

    /* access modifiers changed from: protected */
    public String readAllLines(BufferedReader reader) throws IOException {
        StringBuffer buff = new StringBuffer();
        while (true) {
            String line = reader.readLine();
            if (line == null) {
                return buff.toString().trim();
            }
            buff.append(line);
            buff.append(10);
        }
    }

    /* access modifiers changed from: protected */
    public HttpPost createPost(Object... params) throws IOException {
        HttpPost post = new HttpPost(this.url);
        post.setEntity(createEntity(params));
        return post;
    }
}
