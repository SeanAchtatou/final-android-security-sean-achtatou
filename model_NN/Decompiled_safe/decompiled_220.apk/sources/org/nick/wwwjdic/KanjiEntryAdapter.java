package org.nick.wwwjdic;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.List;

public class KanjiEntryAdapter extends BaseAdapter {
    private final Context context;
    private final List<KanjiEntry> entries;

    public KanjiEntryAdapter(Context context2, List<KanjiEntry> entries2) {
        this.context = context2;
        this.entries = entries2;
    }

    public int getCount() {
        if (this.entries == null) {
            return 0;
        }
        return this.entries.size();
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public Object getItem(int position) {
        if (this.entries == null) {
            return null;
        }
        return this.entries.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        KanjiEntry entry = this.entries.get(position);
        if (convertView == null) {
            return new KanjiEntryView(this.context, entry);
        }
        KanjiEntryView entryView = (KanjiEntryView) convertView;
        entryView.populate(entry);
        return entryView;
    }

    private static final class KanjiEntryView extends LinearLayout {
        private TextView entryText;
        private TextView kunyomiText;
        private TextView onyomiText;
        private TextView translationText;

        public KanjiEntryView(Context context, KanjiEntry entry) {
            super(context);
            setOrientation(0);
            new LinearLayout.LayoutParams(-2, -2).setMargins(5, 3, 5, 0);
            LinearLayout readingMeaningsLayout = new LinearLayout(context);
            readingMeaningsLayout.setOrientation(1);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(-2, -2);
            params.setMargins(5, 3, 5, 0);
            LinearLayout.LayoutParams entryParams = new LinearLayout.LayoutParams((ViewGroup.MarginLayoutParams) params);
            entryParams.gravity = 17;
            this.entryText = createTextView(context, R.style.kanji_list_heading, entry.getKanji());
            this.entryText.setGravity(17);
            this.entryText.setTextSize(42.0f);
            addView(this.entryText, entryParams);
            this.onyomiText = createTextView(context, R.style.kanji_list_reading, "");
            readingMeaningsLayout.addView(this.onyomiText, params);
            if (entry.getOnyomi() != null) {
                this.onyomiText.setText(entry.getOnyomi());
            }
            this.kunyomiText = createTextView(context, R.style.kanji_list_reading, "");
            readingMeaningsLayout.addView(this.kunyomiText, params);
            if (entry.getKunyomi() != null) {
                this.kunyomiText.setText(entry.getKunyomi());
            }
            this.translationText = createTextView(context, R.style.kanji_list_translation, entry.getMeaningsAsString());
            readingMeaningsLayout.addView(this.translationText, params);
            addView(readingMeaningsLayout);
        }

        private TextView createTextView(Context context, int style, String text) {
            TextView result = new TextView(context, null, style);
            result.setSingleLine(true);
            result.setEllipsize(TextUtils.TruncateAt.END);
            result.setText(text);
            return result;
        }

        /* access modifiers changed from: package-private */
        public void populate(KanjiEntry entry) {
            this.entryText.setText(entry.getKanji());
            if (entry.getOnyomi() != null) {
                this.onyomiText.setText(entry.getOnyomi());
            } else {
                this.onyomiText.setText("");
            }
            if (entry.getKunyomi() != null) {
                this.kunyomiText.setText(entry.getKunyomi());
            } else {
                this.kunyomiText.setText("");
            }
            this.translationText.setText(entry.getMeaningsAsString());
        }
    }
}
