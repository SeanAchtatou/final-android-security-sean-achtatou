package org.nick.wwwjdic;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import java.util.List;

public class KanjiResultListView extends ResultListViewBase<KanjiEntry> {
    private static final int MENU_ITEM_ADD_TO_FAVORITES = 2;
    private static final int MENU_ITEM_COMPOUNDS = 4;
    private static final int MENU_ITEM_COPY = 1;
    private static final int MENU_ITEM_DETAILS = 0;
    private static final int MENU_ITEM_STROKE_ORDER = 3;
    private static final String TAG = KanjiResultListView.class.getSimpleName();
    /* access modifiers changed from: private */
    public List<KanjiEntry> entries;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.search_results);
        getListView().setOnCreateContextMenuListener(this);
        extractSearchCriteria();
        submitSearchTask(new KanjiSearchTask(getWwwjdicUrl(), getHttpTimeoutSeconds(), this, this.criteria));
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        showDetails(this.entries.get(position));
    }

    private void showDetails(KanjiEntry entry) {
        Intent intent = new Intent(this, KanjiEntryDetail.class);
        intent.putExtra(Constants.KANJI_ENTRY_KEY, entry);
        setFavoriteId(intent, entry);
        startActivity(intent);
    }

    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        menu.add(0, 0, 0, (int) R.string.details);
        menu.add(0, 1, 1, (int) R.string.copy);
        menu.add(0, 2, 2, (int) R.string.add_to_favorites);
        menu.add(0, 3, 3, (int) R.string.stroke_order);
        menu.add(0, 4, 4, (int) R.string.compounds);
    }

    public boolean onContextItemSelected(MenuItem item) {
        try {
            KanjiEntry entry = this.entries.get(((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).position);
            switch (item.getItemId()) {
                case 0:
                    showDetails(entry);
                    return true;
                case 1:
                    copy(entry);
                    return true;
                case 2:
                    addToFavorites(entry);
                    return true;
                case 3:
                    Activities.showStrokeOrder(this, entry);
                    return true;
                case 4:
                    showCompounds(entry);
                    return true;
                default:
                    return false;
            }
        } catch (ClassCastException e) {
            Log.e(TAG, "bad menuInfo", e);
            return false;
        }
    }

    private void showCompounds(KanjiEntry entry) {
        String dictionary = getApp().getCurrentDictionary();
        Log.d(TAG, String.format("Will look for compounds in dictionary: %s(%s)", getApp().getCurrentDictionaryName(), dictionary));
        SearchCriteria criteria = SearchCriteria.createForKanjiCompounds(entry.getKanji(), 2, false, dictionary);
        Intent intent = new Intent(this, DictionaryResultListView.class);
        intent.putExtra(Constants.CRITERIA_KEY, criteria);
        startActivity(intent);
    }

    public void setResult(final List<KanjiEntry> result) {
        this.guiThread.post(new Runnable() {
            public void run() {
                KanjiResultListView.this.entries = result;
                KanjiResultListView.this.setListAdapter(new KanjiEntryAdapter(KanjiResultListView.this, KanjiResultListView.this.entries));
                KanjiResultListView.this.getListView().setTextFilterEnabled(true);
                KanjiResultListView.this.setTitle(KanjiResultListView.this.getResources().getString(R.string.results_for, Integer.valueOf(KanjiResultListView.this.entries.size()), KanjiResultListView.this.criteria.getQueryString()));
                KanjiResultListView.this.dismissProgressDialog();
            }
        });
    }
}
