package org.nick.wwwjdic;

import android.util.Log;
import java.util.List;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.protocol.HttpContext;

public abstract class SearchTask<T> implements Runnable {
    private static final String TAG = SearchTask.class.getSimpleName();
    protected HttpClient httpclient = HttpClientFactory.createWwwjdicHttpClient(this.timeoutMillis);
    protected HttpContext localContext;
    protected WwwjdicQuery query;
    protected ResponseHandler<String> responseHandler = HttpClientFactory.createWwwjdicResponseHandler();
    protected ResultListView<T> resultListView;
    protected int timeoutMillis;
    protected String url;

    /* access modifiers changed from: protected */
    public abstract List<T> parseResult(String str);

    /* access modifiers changed from: protected */
    public abstract String query(WwwjdicQuery wwwjdicQuery);

    public SearchTask(String url2, int timeoutSeconds, ResultListView<T> resultView, WwwjdicQuery query2) {
        this.url = url2;
        this.timeoutMillis = timeoutSeconds * 1000;
        this.resultListView = resultView;
        this.query = query2;
    }

    public void run() {
        try {
            List<T> result = fetchResult(this.query);
            if (this.resultListView != null) {
                this.resultListView.setResult(result);
            }
        } catch (Exception e) {
            Exception e2 = e;
            Log.e(TAG, e2.getMessage(), e2);
            this.resultListView.setError(e2);
        }
    }

    private List<T> fetchResult(WwwjdicQuery query2) {
        return parseResult(query(query2));
    }

    public ResultListView<T> getResultListView() {
        return this.resultListView;
    }

    public void setResultListView(ResultListView<T> resultListView2) {
        this.resultListView = resultListView2;
    }
}
