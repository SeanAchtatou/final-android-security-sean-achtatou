package org.nick.wwwjdic;

import android.util.Log;
import au.com.bytecode.opencsv.CSVWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;

public class SentenceBreakdownTask extends SearchTask<SentenceBreakdownEntry> {
    private static final Pattern BR_PATTERN = Pattern.compile("^<br>$");
    private static final Pattern ENTRY_PATTERN = Pattern.compile("^.*<li>\\s*(.+)\\s【(.+)】\\s+(.+)\\s+(<font.*>(.+)</font>)?</li>.*$");
    private static final Pattern ENTRY_WITH_EXPLANATION_PATTERN = Pattern.compile("^.*<li>\\s*(.+)<br>\\s*(.+)\\s【(.+)】\\s+(.+)</li>.*$");
    private static final Pattern INFLECTED_FORM_PATTERN = Pattern.compile("<font color=\"\\S+\">(\\S+)</font>", 2);
    private static final Pattern NO_READING_ENTRY_PATTERN = Pattern.compile("^.*<li>\\s*(\\S+)\\s+(.+)\\s+(<font.*>(.+)</font>)?</li>.*$");
    private static final Pattern SENTENCE_PART_PATTERN = Pattern.compile("^.*<font color=\"\\S+\">\\S+</font>.*<br>$", 2);

    public SentenceBreakdownTask(String url, int timeoutSeconds, ResultListViewBase<SentenceBreakdownEntry> resultListView, WwwjdicQuery query) {
        super(url, timeoutSeconds, resultListView, query);
    }

    /* access modifiers changed from: protected */
    public List<SentenceBreakdownEntry> parseResult(String html) {
        SentenceBreakdownEntry entry;
        SentenceBreakdownEntry entry2;
        List<SentenceBreakdownEntry> result = new ArrayList<>();
        ArrayList arrayList = new ArrayList();
        String[] lines = html.split(CSVWriter.DEFAULT_LINE_END);
        boolean exampleFollows = false;
        int wordIdx = 0;
        int length = lines.length;
        for (int i = 0; i < length; i++) {
            String line = lines[i];
            if (BR_PATTERN.matcher(line).matches()) {
                exampleFollows = true;
            } else if (exampleFollows) {
                Matcher m = INFLECTED_FORM_PATTERN.matcher(line);
                while (m.find()) {
                    arrayList.add(m.group(1));
                }
                exampleFollows = false;
            } else if (SENTENCE_PART_PATTERN.matcher(line).matches()) {
                Matcher m2 = INFLECTED_FORM_PATTERN.matcher(line);
                while (m2.find()) {
                    arrayList.add(m2.group(1));
                }
            } else {
                Matcher m3 = ENTRY_WITH_EXPLANATION_PATTERN.matcher(line);
                if (m3.matches()) {
                    String explanation = m3.group(1).trim();
                    result.add(SentenceBreakdownEntry.createWithExplanation((String) arrayList.get(wordIdx), m3.group(2).trim(), m3.group(3).trim(), m3.group(4).trim(), explanation));
                    wordIdx++;
                } else {
                    Matcher m4 = ENTRY_PATTERN.matcher(line);
                    if (m4.matches()) {
                        String word = m4.group(1).trim();
                        String reading = m4.group(2).trim();
                        String translation = m4.group(3).trim();
                        if (m4.groupCount() <= 4 || m4.group(5) == null) {
                            entry2 = SentenceBreakdownEntry.create((String) arrayList.get(wordIdx), word, reading, translation);
                        } else {
                            entry2 = SentenceBreakdownEntry.createWithExplanation((String) arrayList.get(wordIdx), word, reading, translation, m4.group(5).trim());
                        }
                        result.add(entry2);
                        wordIdx++;
                    } else {
                        Matcher m5 = NO_READING_ENTRY_PATTERN.matcher(line);
                        if (m5.matches()) {
                            String word2 = m5.group(1).trim();
                            String translation2 = m5.group(2).trim();
                            if (m5.groupCount() <= 3 || m5.group(4) == null) {
                                entry = SentenceBreakdownEntry.createNoReading((String) arrayList.get(wordIdx), word2, translation2);
                            } else {
                                entry = SentenceBreakdownEntry.createWithExplanation((String) arrayList.get(wordIdx), word2, null, translation2, m5.group(4).trim());
                            }
                            result.add(entry);
                            wordIdx++;
                        }
                    }
                }
            }
        }
        return result;
    }

    /* access modifiers changed from: protected */
    public String query(WwwjdicQuery query) {
        try {
            return (String) this.httpclient.execute(new HttpGet(String.format("%s?%s", this.url, generateBackdoorCode(query))), this.responseHandler, this.localContext);
        } catch (ClientProtocolException e) {
            ClientProtocolException cpe = e;
            Log.e("WWWJDIC", "ClientProtocolException", cpe);
            throw new RuntimeException((Throwable) cpe);
        } catch (IOException e2) {
            IOException e3 = e2;
            Log.e("WWWJDIC", "IOException", e3);
            throw new RuntimeException(e3);
        }
    }

    private String generateBackdoorCode(WwwjdicQuery query) {
        StringBuffer buff = new StringBuffer();
        buff.append("9ZIG");
        try {
            buff.append(URLEncoder.encode(query.getQueryString(), "UTF-8"));
            return buff.toString();
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}
