package org.nick.wwwjdic;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class AboutActivity extends Activity implements View.OnClickListener {
    private static final String DONATE_VERSION_PACKAGE = "org.nick.wwwjdic.donate";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.about_dialog);
        ((TextView) findViewById(R.id.versionText)).setText("version " + WwwjdicApplication.getVersion());
        ((TextView) findViewById(R.id.faqText)).setMovementMethod(LinkMovementMethod.getInstance());
        ((TextView) findViewById(R.id.kradfile_attribution_text)).setMovementMethod(LinkMovementMethod.getInstance());
        Button buyDonateButton = (Button) findViewById(R.id.buy_donate);
        if (!isDonateVersion()) {
            buyDonateButton.setOnClickListener(this);
        } else {
            buyDonateButton.setVisibility(8);
        }
    }

    public void onClick(View view) {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=org.nick.wwwjdic.donate")));
    }

    private boolean isDonateVersion() {
        return DONATE_VERSION_PACKAGE.equals(getApplication().getPackageName());
    }
}
