package org.nick.wwwjdic;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.Arrays;
import java.util.Locale;
import java.util.regex.Matcher;
import org.nick.wwwjdic.utils.DictUtils;
import org.nick.wwwjdic.utils.Pair;

public class DictionaryEntryDetail extends DetailActivity implements View.OnClickListener {
    private static final int DEFAULT_MAX_NUM_EXAMPLES = 20;
    private static final int ITEM_ID_LOOKUP_KANJI = 1;
    private static final String TAG = DictionaryEntryDetail.class.getSimpleName();
    private DictionaryEntry entry;
    private TextView entryView;
    private Button exampleSearchButton;
    private String exampleSearchKey;
    private CheckBox starCb;
    private LinearLayout translationsLayout;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.entry_details);
        checkTtsAvailability();
        this.entry = (DictionaryEntry) getIntent().getSerializableExtra(Constants.ENTRY_KEY);
        this.wwwjdicEntry = this.entry;
        this.isFavorite = getIntent().getBooleanExtra(Constants.IS_FAVORITE, false);
        setTitle(String.format(getResources().getString(R.string.details_for), this.entry.getWord()));
        LinearLayout wordReadingLayout = (LinearLayout) findViewById(R.id.word_reading_layout);
        this.entryView = (TextView) findViewById(R.id.wordText);
        this.entryView.setText(this.entry.getWord());
        this.entryView.setOnLongClickListener(this);
        if (this.entry.getReading() != null) {
            TextView readingView = new TextView(this, null, R.style.dict_detail_reading);
            readingView.setText(this.entry.getReading());
            wordReadingLayout.addView(readingView);
        }
        this.translationsLayout = (LinearLayout) findViewById(R.id.translations_layout);
        for (String meaning : this.entry.getMeanings()) {
            Pair<LinearLayout, TextView> translationViews = createMeaningTextView(this, meaning);
            Matcher m = CROSS_REF_PATTERN.matcher(meaning);
            if (m.matches()) {
                Intent intent = createCrossRefIntent(m.group(1));
                makeClickable(translationViews.getSecond(), m.start(1), m.end(1), intent);
            }
            this.translationsLayout.addView(translationViews.getFirst());
        }
        this.starCb = (CheckBox) findViewById(R.id.star_word);
        this.starCb.setOnCheckedChangeListener(null);
        this.starCb.setChecked(this.isFavorite);
        this.starCb.setOnCheckedChangeListener(this);
        this.exampleSearchButton = (Button) findViewById(R.id.examples_button);
        this.exampleSearchButton.setOnClickListener(this);
        this.exampleSearchKey = DictUtils.extractSearchKey(this.wwwjdicEntry);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.tts != null) {
            this.tts.shutdown();
        }
    }

    private Intent createCrossRefIntent(String word) {
        String dictionary = getApp().getCurrentDictionary();
        Log.d(TAG, String.format("Will look for compounds in dictionary: %s(%s)", getApp().getCurrentDictionaryName(), dictionary));
        SearchCriteria criteria = SearchCriteria.createForDictionary(word, true, false, false, dictionary);
        Intent intent = new Intent(this, DictionaryResultListView.class);
        intent.putExtra(Constants.CRITERIA_KEY, criteria);
        return intent;
    }

    private void disableExampleSearchIfSingleKanji() {
        if (this.exampleSearchKey.length() == 1) {
            this.exampleSearchButton.setEnabled(false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.examples_button /*2131296297*/:
                Intent intent = new Intent(this, ExamplesResultListView.class);
                intent.putExtra(Constants.CRITERIA_KEY, SearchCriteria.createForExampleSearch(this.exampleSearchKey, false, DEFAULT_MAX_NUM_EXAMPLES));
                intent.putExtra(ExamplesResultListView.EXTRA_EXAMPLES_BACKDOOR_SEARCH, true);
                startActivity(intent);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void setHomeActivityExtras(Intent homeActivityIntent) {
        homeActivityIntent.putExtra(Constants.SELECTED_TAB_IDX, 0);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 1, 0, (int) R.string.lookup_kanji).setIcon(17301583);
        return true;
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                Intent intent = new Intent(this, Wwwjdic.class);
                intent.setFlags(67108864);
                setHomeActivityExtras(intent);
                startActivity(intent);
                finish();
                return true;
            case 1:
                Activities.lookupKanji(this, this.db, this.wwwjdicEntry.getHeadword());
                return true;
            default:
                return super.onMenuItemSelected(featureId, item);
        }
    }

    /* access modifiers changed from: protected */
    public Locale getSpeechLocale() {
        String entryDictionary = this.entry.getDictionary();
        if (entryDictionary == null) {
            return Locale.ENGLISH;
        }
        if (Arrays.asList("1", "3", "4", "5", "6", "7", "8", "A", "B", "C", "D").contains(entryDictionary)) {
            return Locale.ENGLISH;
        }
        if ("G".equals(entryDictionary)) {
            return Locale.GERMAN;
        }
        if ("H".equals(entryDictionary)) {
            return Locale.FRENCH;
        }
        if ("I".equals(entryDictionary)) {
            return new Locale("RU");
        }
        if ("J".equals(entryDictionary)) {
            return new Locale("SE");
        }
        if ("K".equals(entryDictionary)) {
            return new Locale("HU");
        }
        if ("L".equals(entryDictionary)) {
            return new Locale("ES");
        }
        if ("M".equals(entryDictionary)) {
            return new Locale("NL");
        }
        if ("N".equals(entryDictionary)) {
            return new Locale("SL");
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void showTtsButtons() {
        toggleTtsButtons(true);
    }

    /* access modifiers changed from: protected */
    public void hideTtsButtons() {
        toggleTtsButtons(false);
    }

    private void toggleTtsButtons(boolean show) {
        int childCount = this.translationsLayout.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View view = this.translationsLayout.getChildAt(i);
            if (view instanceof Button) {
                if (show) {
                    view.setVisibility(0);
                } else {
                    view.setVisibility(4);
                }
            } else if (view instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) view;
                int count = vg.getChildCount();
                for (int j = 0; j < count; j++) {
                    View view2 = vg.getChildAt(j);
                    if (view2 instanceof Button) {
                        if (show) {
                            view2.setVisibility(0);
                        } else {
                            view2.setVisibility(4);
                        }
                    }
                }
            }
        }
    }
}
