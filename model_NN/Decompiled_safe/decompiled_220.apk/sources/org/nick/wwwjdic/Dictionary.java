package org.nick.wwwjdic;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.RejectedExecutionException;
import org.nick.wwwjdic.history.FavoritesAndHistorySummaryView;
import org.nick.wwwjdic.history.HistoryDbHelper;
import org.nick.wwwjdic.utils.Analytics;
import org.nick.wwwjdic.utils.StringUtils;

public class Dictionary extends WwwjdicActivityBase implements View.OnClickListener, View.OnFocusChangeListener, CompoundButton.OnCheckedChangeListener, AdapterView.OnItemSelectedListener {
    private static final Map<Integer, String> IDX_TO_DICT_CODE = new HashMap();
    private static final String TAG = Dictionary.class.getSimpleName();
    private CheckBox commonWordsCb;
    private HistoryDbHelper dbHelper;
    private FavoritesAndHistorySummaryView dictHistorySummary;
    private Spinner dictSpinner;
    private CheckBox exactMatchCb;
    /* access modifiers changed from: private */
    public EditText inputText;
    private CheckBox romanizedJapaneseCb;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.dict_lookup);
        populateIdxToDictCode();
        findViews();
        setupListeners();
        setupSpinners();
        this.inputText.requestFocus();
        selectDictionary(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String searchKey = extras.getString(Constants.SEARCH_TEXT_KEY);
            int searchType = extras.getInt(Constants.SEARCH_TYPE);
            if (searchKey != null) {
                switch (searchType) {
                    case 0:
                        this.inputText.setText(searchKey);
                        break;
                }
                this.inputTextFromBundle = true;
            }
        }
        this.dbHelper = HistoryDbHelper.getInstance(this);
        setupDictSummary();
        this.inputText.post(new Runnable() {
            public void run() {
                Dictionary.this.inputText.requestFocus();
            }
        });
    }

    private void populateIdxToDictCode() {
        if (IDX_TO_DICT_CODE.isEmpty()) {
            String[] dictionaryIdxs = getResources().getStringArray(R.array.dictionary_idxs_array);
            String[] dictionaryCodes = getResources().getStringArray(R.array.dictionary_codes_array);
            for (int i = 0; i < dictionaryIdxs.length; i++) {
                IDX_TO_DICT_CODE.put(Integer.valueOf(Integer.parseInt(dictionaryIdxs[i])), dictionaryCodes[i]);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    private void selectDictionary(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            this.dictSpinner.setSelection(savedInstanceState.getInt(Constants.SELECTED_DICTIONARY_IDX, 0));
            return;
        }
        this.dictSpinner.setSelection(WwwjdicPreferences.getDefaultDictionaryIdx(this));
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        setupDictSummary();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(Constants.SELECTED_DICTIONARY_IDX, this.dictSpinner.getSelectedItemPosition());
    }

    private void findViews() {
        this.inputText = (EditText) findViewById(R.id.inputText);
        this.exactMatchCb = (CheckBox) findViewById(R.id.exactMatchCb);
        this.commonWordsCb = (CheckBox) findViewById(R.id.commonWordsCb);
        this.romanizedJapaneseCb = (CheckBox) findViewById(R.id.romanizedCb);
        this.dictSpinner = (Spinner) findViewById(R.id.dictionarySpinner);
        this.dictHistorySummary = (FavoritesAndHistorySummaryView) findViewById(R.id.dict_history_summary);
    }

    private void setupListeners() {
        findViewById(R.id.translateButton).setOnClickListener(this);
        this.romanizedJapaneseCb.setOnCheckedChangeListener(this);
        this.exactMatchCb.setOnCheckedChangeListener(this);
        this.commonWordsCb.setOnCheckedChangeListener(this);
    }

    private void setupSpinners() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.dictionaries_array, R.layout.spinner_text);
        adapter.setDropDownViewResource(17367049);
        this.dictSpinner.setAdapter((SpinnerAdapter) adapter);
        this.dictSpinner.setOnItemSelectedListener(this);
    }

    private void setupDictSummary() {
        this.dbHelper.beginTransaction();
        try {
            long numAllFavorites = this.dbHelper.getDictFavoritesCount();
            List<String> recentFavorites = this.dbHelper.getRecentDictFavorites(5);
            long numAllHistory = this.dbHelper.getDictHistoryCount();
            List<String> recentHistory = this.dbHelper.getRecentDictHistory(5);
            this.dictHistorySummary.setFavoritesFilterType(0);
            this.dictHistorySummary.setHistoryFilterType(0);
            this.dictHistorySummary.setRecentEntries(numAllFavorites, recentFavorites, numAllHistory, recentHistory);
            this.dbHelper.setTransactionSuccessful();
        } finally {
            this.dbHelper.endTransaction();
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.translateButton /*2131296291*/:
                hideKeyboard();
                String input = this.inputText.getText().toString();
                if (!TextUtils.isEmpty(input)) {
                    try {
                        SearchCriteria criteria = SearchCriteria.createForDictionary(input.trim(), this.exactMatchCb.isChecked(), this.romanizedJapaneseCb.isChecked(), this.commonWordsCb.isChecked(), getDictionaryFromSelection(this.dictSpinner.getSelectedItemPosition()));
                        Intent intent = new Intent(this, DictionaryResultListView.class);
                        intent.putExtra(Constants.CRITERIA_KEY, criteria);
                        if (!StringUtils.isEmpty(criteria.getQueryString())) {
                            this.dbHelper.addSearchCriteria(criteria);
                        }
                        Analytics.event("dictSearch", this);
                        startActivity(intent);
                        return;
                    } catch (RejectedExecutionException e) {
                        RejectedExecutionException e2 = e;
                        Log.e(TAG, e2.getMessage(), e2);
                        return;
                    }
                } else {
                    return;
                }
            default:
                return;
        }
    }

    private String getDictionaryFromSelection(int dictIdx) {
        String dict = IDX_TO_DICT_CODE.get(Integer.valueOf(dictIdx));
        Log.i(TAG, "dictionary idx: " + Integer.toString(dictIdx));
        Log.i(TAG, "dictionary: " + dict);
        if (dict == null) {
            return "1";
        }
        return dict;
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.commonWordsCb /*2131296288*/:
                toggleRomanizedCb(isChecked);
                return;
            case R.id.exactMatchCb /*2131296289*/:
                toggleRomanizedCb(isChecked);
                return;
            case R.id.romanizedCb /*2131296290*/:
                toggleExactCommonCbs(isChecked);
                return;
            default:
                return;
        }
    }

    private void toggleExactCommonCbs(boolean isChecked) {
        if (isChecked) {
            this.exactMatchCb.setEnabled(false);
            this.commonWordsCb.setEnabled(false);
            return;
        }
        this.exactMatchCb.setEnabled(true);
        this.commonWordsCb.setEnabled(true);
    }

    private void toggleRomanizedCb(boolean isChecked) {
        if (isChecked) {
            this.romanizedJapaneseCb.setEnabled(false);
        } else if (!this.exactMatchCb.isChecked() && !this.commonWordsCb.isChecked()) {
            this.romanizedJapaneseCb.setEnabled(true);
        }
    }

    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.inputText /*2131296283*/:
                if (hasFocus) {
                    showKeyboard();
                    return;
                } else {
                    hideKeyboard();
                    return;
                }
            default:
                return;
        }
    }

    private void hideKeyboard() {
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.inputText.getWindowToken(), 0);
    }

    private void showKeyboard() {
        EditText editText = (EditText) findViewById(R.id.inputText);
        editText.requestFocus();
        ((InputMethodManager) getSystemService("input_method")).showSoftInput(editText, 1);
    }

    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
        String dict = getDictionaryFromSelection(pos);
        String dictName = (String) parent.getSelectedItem();
        getApp().setCurrentDictionary(dict);
        getApp().setCurrentDictionaryName(dictName);
        Log.d(TAG, String.format("current dictionary: %s(%s)", dictName, dict));
    }

    public void onNothingSelected(AdapterView<?> adapterView) {
    }
}
