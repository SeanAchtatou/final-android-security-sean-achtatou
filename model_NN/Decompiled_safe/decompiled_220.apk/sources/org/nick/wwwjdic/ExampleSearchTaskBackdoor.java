package org.nick.wwwjdic;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExampleSearchTaskBackdoor extends BackdoorSearchTask<ExampleSentence> {
    private static final Pattern BREAKDOWN_PATTERN = Pattern.compile("^B:\\s.+\\{(\\S+)\\}.*$");
    private static final Pattern SENTENCE_PATTERN = Pattern.compile("^A:\\s(\\S+)\t(.+)#.*$");
    private static final Pattern WORD_FORM_PATTERN = Pattern.compile("(\\S+?)(?:\\(\\p{InHiragana}+\\))?(?:\\[\\d+\\])?(?:\\{(\\S+)\\})?~?", 4);
    private ExampleSentence lastSentence;
    private boolean randomExamples;

    public ExampleSearchTaskBackdoor(String url, int timeoutSeconds, ResultListView<ExampleSentence> resultView, SearchCriteria searchCriteria, boolean randomExamples2) {
        super(url, timeoutSeconds, resultView, searchCriteria);
        this.randomExamples = randomExamples2;
    }

    /* access modifiers changed from: protected */
    public ExampleSentence parseEntry(String entryStr) {
        Matcher m = SENTENCE_PATTERN.matcher(entryStr);
        if (m.matches()) {
            ExampleSentence result = new ExampleSentence(m.group(1), m.group(2));
            this.lastSentence = result;
            return result;
        }
        if (this.lastSentence != null && BREAKDOWN_PATTERN.matcher(entryStr).matches()) {
            String[] words = entryStr.substring(3).split(" ");
            String queryForm = this.query.getQueryString();
            for (String word : words) {
                Matcher formMatcher = WORD_FORM_PATTERN.matcher(word);
                if (formMatcher.matches()) {
                    String basicForm = formMatcher.group(1);
                    String formInSentence = formMatcher.group(2);
                    if (formInSentence != null) {
                        if (queryForm.equals(basicForm) || queryForm.equals(formInSentence)) {
                            this.lastSentence.addMatch(formInSentence);
                        }
                    } else if (queryForm.equals(basicForm)) {
                        this.lastSentence.addMatch(basicForm);
                    }
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public String generateBackdoorCode(SearchCriteria criteria) {
        StringBuffer buff = new StringBuffer();
        buff.append("1");
        buff.append("Z");
        buff.append("E");
        buff.append("U");
        try {
            buff.append(URLEncoder.encode(criteria.getQueryString(), "UTF-8"));
            if (this.randomExamples) {
                buff.append("=1=");
            } else {
                buff.append("=0=");
            }
            return buff.toString();
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}
