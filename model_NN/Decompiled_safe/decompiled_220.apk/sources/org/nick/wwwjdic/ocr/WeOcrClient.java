package org.nick.wwwjdic.ocr;

import android.graphics.Bitmap;
import au.com.bytecode.opencsv.CSVWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.entity.AbstractHttpEntity;
import org.nick.wwwjdic.EntityBasedHttpClient;

public class WeOcrClient extends EntityBasedHttpClient {
    private static final String TAG = WeOcrClient.class.getSimpleName();

    public WeOcrClient(String endpoint, int timeout) {
        super(endpoint, timeout);
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0095  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String sendLineOcrRequest(android.graphics.Bitmap r10) throws java.io.IOException {
        /*
            r9 = this;
            java.lang.String r6 = org.nick.wwwjdic.ocr.WeOcrClient.TAG
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            java.lang.String r8 = "Sending OCR request to "
            r7.<init>(r8)
            java.lang.String r8 = r9.url
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r7 = r7.toString()
            android.util.Log.i(r6, r7)
            r6 = 1
            java.lang.Object[] r6 = new java.lang.Object[r6]
            r7 = 0
            r6[r7] = r10
            org.apache.http.client.methods.HttpPost r0 = r9.createPost(r6)
            r2 = 0
            org.apache.http.impl.client.DefaultHttpClient r6 = r9.httpClient     // Catch:{ HttpResponseException -> 0x00a6 }
            org.apache.http.HttpResponse r4 = r6.execute(r0)     // Catch:{ HttpResponseException -> 0x00a6 }
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ HttpResponseException -> 0x00a6 }
            java.io.InputStreamReader r6 = new java.io.InputStreamReader     // Catch:{ HttpResponseException -> 0x00a6 }
            org.apache.http.HttpEntity r7 = r4.getEntity()     // Catch:{ HttpResponseException -> 0x00a6 }
            java.io.InputStream r7 = r7.getContent()     // Catch:{ HttpResponseException -> 0x00a6 }
            java.lang.String r8 = "utf-8"
            r6.<init>(r7, r8)     // Catch:{ HttpResponseException -> 0x00a6 }
            r3.<init>(r6)     // Catch:{ HttpResponseException -> 0x00a6 }
            java.lang.String r5 = r3.readLine()     // Catch:{ HttpResponseException -> 0x006f, all -> 0x00a3 }
            int r6 = r5.length()     // Catch:{ HttpResponseException -> 0x006f, all -> 0x00a3 }
            if (r6 == 0) goto L_0x0099
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ HttpResponseException -> 0x006f, all -> 0x00a3 }
            java.lang.String r7 = java.lang.String.valueOf(r5)     // Catch:{ HttpResponseException -> 0x006f, all -> 0x00a3 }
            r6.<init>(r7)     // Catch:{ HttpResponseException -> 0x006f, all -> 0x00a3 }
            java.lang.String r7 = r9.readAllLines(r3)     // Catch:{ HttpResponseException -> 0x006f, all -> 0x00a3 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ HttpResponseException -> 0x006f, all -> 0x00a3 }
            java.lang.String r5 = r6.toString()     // Catch:{ HttpResponseException -> 0x006f, all -> 0x00a3 }
            java.lang.RuntimeException r6 = new java.lang.RuntimeException     // Catch:{ HttpResponseException -> 0x006f, all -> 0x00a3 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ HttpResponseException -> 0x006f, all -> 0x00a3 }
            java.lang.String r8 = "WeOCR failed. Status: "
            r7.<init>(r8)     // Catch:{ HttpResponseException -> 0x006f, all -> 0x00a3 }
            java.lang.StringBuilder r7 = r7.append(r5)     // Catch:{ HttpResponseException -> 0x006f, all -> 0x00a3 }
            java.lang.String r7 = r7.toString()     // Catch:{ HttpResponseException -> 0x006f, all -> 0x00a3 }
            r6.<init>(r7)     // Catch:{ HttpResponseException -> 0x006f, all -> 0x00a3 }
            throw r6     // Catch:{ HttpResponseException -> 0x006f, all -> 0x00a3 }
        L_0x006f:
            r6 = move-exception
            r1 = r6
            r2 = r3
        L_0x0072:
            java.lang.String r6 = org.nick.wwwjdic.ocr.WeOcrClient.TAG     // Catch:{ all -> 0x0092 }
            java.lang.String r7 = "HTTP response exception"
            android.util.Log.e(r6, r7, r1)     // Catch:{ all -> 0x0092 }
            java.lang.RuntimeException r6 = new java.lang.RuntimeException     // Catch:{ all -> 0x0092 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x0092 }
            java.lang.String r8 = "HTTP request failed. Status: "
            r7.<init>(r8)     // Catch:{ all -> 0x0092 }
            int r8 = r1.getStatusCode()     // Catch:{ all -> 0x0092 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x0092 }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x0092 }
            r6.<init>(r7)     // Catch:{ all -> 0x0092 }
            throw r6     // Catch:{ all -> 0x0092 }
        L_0x0092:
            r6 = move-exception
        L_0x0093:
            if (r2 == 0) goto L_0x0098
            r2.close()
        L_0x0098:
            throw r6
        L_0x0099:
            java.lang.String r6 = r9.readAllLines(r3)     // Catch:{ HttpResponseException -> 0x006f, all -> 0x00a3 }
            if (r3 == 0) goto L_0x00a2
            r3.close()
        L_0x00a2:
            return r6
        L_0x00a3:
            r6 = move-exception
            r2 = r3
            goto L_0x0093
        L_0x00a6:
            r6 = move-exception
            r1 = r6
            goto L_0x0072
        */
        throw new UnsupportedOperationException("Method not decompiled: org.nick.wwwjdic.ocr.WeOcrClient.sendLineOcrRequest(android.graphics.Bitmap):java.lang.String");
    }

    /* Debug info: failed to restart local var, previous not found, register: 11 */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x00a1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String[] sendCharacterOcrRequest(android.graphics.Bitmap r12, int r13) throws java.io.IOException {
        /*
            r11 = this;
            java.lang.String r8 = org.nick.wwwjdic.ocr.WeOcrClient.TAG
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            java.lang.String r10 = "Sending OCR request to "
            r9.<init>(r10)
            java.lang.String r10 = r11.url
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r9 = r9.toString()
            android.util.Log.i(r8, r9)
            r8 = 3
            java.lang.Object[] r8 = new java.lang.Object[r8]
            r9 = 0
            r8[r9] = r12
            r9 = 1
            java.lang.String r10 = "character"
            r8[r9] = r10
            r9 = 2
            java.lang.Integer r10 = java.lang.Integer.valueOf(r13)
            r8[r9] = r10
            org.apache.http.client.methods.HttpPost r0 = r11.createPost(r8)
            r2 = 0
            org.apache.http.impl.client.DefaultHttpClient r8 = r11.httpClient     // Catch:{ HttpResponseException -> 0x00d6 }
            org.apache.http.HttpResponse r4 = r8.execute(r0)     // Catch:{ HttpResponseException -> 0x00d6 }
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ HttpResponseException -> 0x00d6 }
            java.io.InputStreamReader r8 = new java.io.InputStreamReader     // Catch:{ HttpResponseException -> 0x00d6 }
            org.apache.http.HttpEntity r9 = r4.getEntity()     // Catch:{ HttpResponseException -> 0x00d6 }
            java.io.InputStream r9 = r9.getContent()     // Catch:{ HttpResponseException -> 0x00d6 }
            java.lang.String r10 = "utf-8"
            r8.<init>(r9, r10)     // Catch:{ HttpResponseException -> 0x00d6 }
            r3.<init>(r8)     // Catch:{ HttpResponseException -> 0x00d6 }
            java.lang.String r7 = r3.readLine()     // Catch:{ HttpResponseException -> 0x007b, all -> 0x00d3 }
            int r8 = r7.length()     // Catch:{ HttpResponseException -> 0x007b, all -> 0x00d3 }
            if (r8 == 0) goto L_0x00a5
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ HttpResponseException -> 0x007b, all -> 0x00d3 }
            java.lang.String r9 = java.lang.String.valueOf(r7)     // Catch:{ HttpResponseException -> 0x007b, all -> 0x00d3 }
            r8.<init>(r9)     // Catch:{ HttpResponseException -> 0x007b, all -> 0x00d3 }
            java.lang.String r9 = r11.readAllLines(r3)     // Catch:{ HttpResponseException -> 0x007b, all -> 0x00d3 }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ HttpResponseException -> 0x007b, all -> 0x00d3 }
            java.lang.String r7 = r8.toString()     // Catch:{ HttpResponseException -> 0x007b, all -> 0x00d3 }
            java.lang.RuntimeException r8 = new java.lang.RuntimeException     // Catch:{ HttpResponseException -> 0x007b, all -> 0x00d3 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ HttpResponseException -> 0x007b, all -> 0x00d3 }
            java.lang.String r10 = "WeOCR failed. Status: "
            r9.<init>(r10)     // Catch:{ HttpResponseException -> 0x007b, all -> 0x00d3 }
            java.lang.StringBuilder r9 = r9.append(r7)     // Catch:{ HttpResponseException -> 0x007b, all -> 0x00d3 }
            java.lang.String r9 = r9.toString()     // Catch:{ HttpResponseException -> 0x007b, all -> 0x00d3 }
            r8.<init>(r9)     // Catch:{ HttpResponseException -> 0x007b, all -> 0x00d3 }
            throw r8     // Catch:{ HttpResponseException -> 0x007b, all -> 0x00d3 }
        L_0x007b:
            r8 = move-exception
            r1 = r8
            r2 = r3
        L_0x007e:
            java.lang.String r8 = org.nick.wwwjdic.ocr.WeOcrClient.TAG     // Catch:{ all -> 0x009e }
            java.lang.String r9 = "HTTP response exception"
            android.util.Log.e(r8, r9, r1)     // Catch:{ all -> 0x009e }
            java.lang.RuntimeException r8 = new java.lang.RuntimeException     // Catch:{ all -> 0x009e }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x009e }
            java.lang.String r10 = "HTTP request failed. Status: "
            r9.<init>(r10)     // Catch:{ all -> 0x009e }
            int r10 = r1.getStatusCode()     // Catch:{ all -> 0x009e }
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ all -> 0x009e }
            java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x009e }
            r8.<init>(r9)     // Catch:{ all -> 0x009e }
            throw r8     // Catch:{ all -> 0x009e }
        L_0x009e:
            r8 = move-exception
        L_0x009f:
            if (r2 == 0) goto L_0x00a4
            r2.close()
        L_0x00a4:
            throw r8
        L_0x00a5:
            java.lang.String r5 = r11.readAllLines(r3)     // Catch:{ HttpResponseException -> 0x007b, all -> 0x00d3 }
            java.lang.String r8 = org.nick.wwwjdic.ocr.WeOcrClient.TAG     // Catch:{ HttpResponseException -> 0x007b, all -> 0x00d3 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ HttpResponseException -> 0x007b, all -> 0x00d3 }
            java.lang.String r10 = "WeOCR response: "
            r9.<init>(r10)     // Catch:{ HttpResponseException -> 0x007b, all -> 0x00d3 }
            java.lang.StringBuilder r9 = r9.append(r5)     // Catch:{ HttpResponseException -> 0x007b, all -> 0x00d3 }
            java.lang.String r9 = r9.toString()     // Catch:{ HttpResponseException -> 0x007b, all -> 0x00d3 }
            android.util.Log.d(r8, r9)     // Catch:{ HttpResponseException -> 0x007b, all -> 0x00d3 }
            java.util.List r6 = r11.extractCandidates(r5)     // Catch:{ HttpResponseException -> 0x007b, all -> 0x00d3 }
            int r8 = r6.size()     // Catch:{ HttpResponseException -> 0x007b, all -> 0x00d3 }
            java.lang.String[] r8 = new java.lang.String[r8]     // Catch:{ HttpResponseException -> 0x007b, all -> 0x00d3 }
            java.lang.Object[] r11 = r6.toArray(r8)     // Catch:{ HttpResponseException -> 0x007b, all -> 0x00d3 }
            java.lang.String[] r11 = (java.lang.String[]) r11     // Catch:{ HttpResponseException -> 0x007b, all -> 0x00d3 }
            if (r3 == 0) goto L_0x00d2
            r3.close()
        L_0x00d2:
            return r11
        L_0x00d3:
            r8 = move-exception
            r2 = r3
            goto L_0x009f
        L_0x00d6:
            r8 = move-exception
            r1 = r8
            goto L_0x007e
        */
        throw new UnsupportedOperationException("Method not decompiled: org.nick.wwwjdic.ocr.WeOcrClient.sendCharacterOcrRequest(android.graphics.Bitmap, int):java.lang.String[]");
    }

    private List<String> extractCandidates(String responseStr) {
        List<String> result = new ArrayList<>();
        for (String l : responseStr.split(CSVWriter.DEFAULT_LINE_END)) {
            String line = l.trim();
            if (!line.startsWith("#") && line.startsWith("R")) {
                result.add(line.split("\t")[2]);
            }
        }
        return result;
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    /* access modifiers changed from: protected */
    public AbstractHttpEntity createEntity(Object... params) throws IOException {
        if (params.length == 1) {
            return new OcrFormEntity((Bitmap) params[0]);
        }
        return new OcrFormEntity((Bitmap) params[0], (String) params[1], (Integer) params[2]);
    }
}
