package org.nick.wwwjdic.ocr;

import android.hardware.Camera;
import android.os.Handler;
import android.util.Log;
import java.io.OutputStream;
import org.acra.ErrorReporter;

public class ImageCaptureCallback implements Camera.PictureCallback {
    private static final String TAG = ImageCaptureCallback.class.getSimpleName();
    private OutputStream fileoutputStream;
    private Handler handler;

    public ImageCaptureCallback(OutputStream os, Handler handler2) {
        this.fileoutputStream = os;
        this.handler = handler2;
    }

    public void onPictureTaken(byte[] data, Camera camera) {
        try {
            this.fileoutputStream.write(data);
            this.fileoutputStream.flush();
            this.fileoutputStream.close();
            this.handler.sendMessage(this.handler.obtainMessage(3, -1, -1));
        } catch (Exception e) {
            Exception ex = e;
            Log.e(TAG, "onPictureTaken error: " + ex.getMessage(), ex);
            ErrorReporter.getInstance().handleException(ex);
        }
    }
}
