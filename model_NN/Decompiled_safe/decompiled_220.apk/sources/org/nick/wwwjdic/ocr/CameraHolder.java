package org.nick.wwwjdic.ocr;

import android.hardware.Camera;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

public class CameraHolder {
    private static final String TAG = CameraHolder.class.getSimpleName();
    private static CameraHolder instance;
    private Camera camera;

    private CameraHolder() {
    }

    public static CameraHolder getInstance() {
        if (instance == null) {
            instance = new CameraHolder();
        }
        return instance;
    }

    public synchronized Camera open() {
        if (this.camera == null) {
            this.camera = Camera.open();
        }
        return this.camera;
    }

    public synchronized Camera tryOpen() {
        Camera camera2;
        try {
            camera2 = open();
        } catch (RuntimeException e) {
            Log.e(TAG, "error opening camera: " + e.getMessage(), e);
            camera2 = null;
        }
        return camera2;
    }

    public List<Camera.Size> getSupportedPictureSizes(Camera.Parameters params) {
        if (this.camera == null) {
            return null;
        }
        List<Camera.Size> supportedPictureSizes = ReflectionUtils.getSupportedPictureSizes(params);
        if (supportedPictureSizes == null) {
            String supportedSizesStr = params.get("picture-size-values");
            if (supportedSizesStr == null) {
                supportedSizesStr = params.get("picture-size-value");
            }
            Log.d(TAG, "picture sizes: " + supportedSizesStr);
            if (supportedSizesStr != null) {
                supportedPictureSizes = parseSizeListStr(supportedSizesStr);
            }
        }
        return supportedPictureSizes;
    }

    private List<Camera.Size> parseSizeListStr(String previewSizesStr) {
        List<Camera.Size> result = new ArrayList<>();
        for (String s : previewSizesStr.split(",")) {
            String s2 = s.trim();
            int idx = s2.indexOf(120);
            if (idx < 0) {
                Log.w(TAG, "Bad preview-size: " + s2);
            } else {
                try {
                    int width = Integer.parseInt(s2.substring(0, idx));
                    int height = Integer.parseInt(s2.substring(idx + 1));
                    Camera camera2 = this.camera;
                    camera2.getClass();
                    result.add(new Camera.Size(camera2, width, height));
                } catch (NumberFormatException e) {
                    Log.w(TAG, "Bad preview-size: " + s2);
                }
            }
        }
        return result;
    }

    public List<Camera.Size> getSupportedPreviewSizes(Camera.Parameters params) {
        if (this.camera == null) {
            return null;
        }
        List<Camera.Size> supportedPreviewSizes = ReflectionUtils.getSupportedPreviewSizes(params);
        if (supportedPreviewSizes == null) {
            String previewSizesStr = params.get("preview-size-values");
            if (previewSizesStr == null) {
                previewSizesStr = params.get("preview-size-value");
            }
            Log.d(TAG, "preview sizes: " + previewSizesStr);
            if (previewSizesStr != null) {
                supportedPreviewSizes = parseSizeListStr(previewSizesStr);
            }
        }
        return supportedPreviewSizes;
    }

    public boolean supportsFlash(Camera.Parameters params) {
        return ReflectionUtils.getFlashMode(params) != null;
    }

    public synchronized void toggleFlash(boolean useFlash, Camera.Parameters params) {
        String flashMode;
        if (this.camera != null) {
            if (useFlash) {
                flashMode = "on";
            } else {
                flashMode = "off";
            }
            ReflectionUtils.setFlashMode(params, flashMode);
            this.camera.setParameters(params);
        }
    }

    public synchronized void release() {
        if (this.camera != null) {
            this.camera.stopPreview();
            this.camera.release();
            this.camera = null;
        }
    }
}
