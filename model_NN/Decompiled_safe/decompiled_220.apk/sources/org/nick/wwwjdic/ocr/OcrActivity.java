package org.nick.wwwjdic.ocr;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import org.acra.ErrorReporter;
import org.nick.wwwjdic.Constants;
import org.nick.wwwjdic.DictionaryResultListView;
import org.nick.wwwjdic.ExamplesResultListView;
import org.nick.wwwjdic.KanjiResultListView;
import org.nick.wwwjdic.R;
import org.nick.wwwjdic.SearchCriteria;
import org.nick.wwwjdic.WebServiceBackedActivity;
import org.nick.wwwjdic.Wwwjdic;
import org.nick.wwwjdic.WwwjdicPreferences;
import org.nick.wwwjdic.ocr.crop.CropImage;
import org.nick.wwwjdic.utils.Analytics;
import org.nick.wwwjdic.utils.Dialogs;

public class OcrActivity extends WebServiceBackedActivity implements SurfaceHolder.Callback, View.OnClickListener, View.OnTouchListener, CompoundButton.OnCheckedChangeListener {
    private static final int AUTO_FOCUS = 1;
    private static final int CROP_REQUEST_CODE = 1;
    private static final String IMAGE_CAPTURE_URI_KEY = "ocr.imageCaptureUri";
    private static final int MAX_PIXELS = 307200;
    private static final int MIN_PIXELS = 153600;
    protected static final int OCRRED_TEXT = 2;
    public static final int PICTURE_TAKEN = 3;
    private static final int SELECT_IMAGE_REQUEST_CODE = 2;
    /* access modifiers changed from: private */
    public static final String TAG = OcrActivity.class.getSimpleName();
    private boolean autoFocusInProgress = false;
    /* access modifiers changed from: private */
    public Camera camera;
    private Button dictSearchButton;
    private Button exampleSearchButton;
    private ToggleButton flashToggle;
    private Uri imageCaptureUri;
    private boolean isPreviewRunning = false;
    private Button kanjidictSearchButton;
    private TextView ocrredTextView;
    private Button pickImageButton;
    Camera.PictureCallback pictureCallbackRaw = new Camera.PictureCallback() {
        public void onPictureTaken(byte[] data, Camera c) {
            OcrActivity.this.camera.startPreview();
        }
    };
    private Camera.Size pictureSize;
    private Camera.Size previewSize;
    private boolean supportsFlash = false;
    private SurfaceHolder surfaceHolder;
    private SurfaceView surfaceView;

    /* access modifiers changed from: protected */
    public void activityOnCreate(Bundle icicle) {
        Window window = getWindow();
        window.addFlags(1152);
        requestWindowFeature(1);
        window.setFormat(-3);
        setRequestedOrientation(0);
        setContentView((int) R.layout.ocr);
        this.surfaceView = (SurfaceView) findViewById(R.id.capture_surface);
        this.camera = CameraHolder.getInstance().tryOpen();
        if (this.camera == null) {
            Dialogs.createFinishActivityAlertDialog(this, R.string.camera_in_use_title, R.string.camera_in_use_message).show();
            return;
        }
        this.surfaceView.setOnTouchListener(this);
        this.surfaceHolder = this.surfaceView.getHolder();
        this.surfaceHolder.addCallback(this);
        this.surfaceHolder.setType(3);
        this.ocrredTextView = (TextView) findViewById(R.id.ocrredText);
        this.ocrredTextView.setTextSize(30.0f);
        this.dictSearchButton = (Button) findViewById(R.id.send_to_dict);
        this.dictSearchButton.setOnClickListener(this);
        this.kanjidictSearchButton = (Button) findViewById(R.id.send_to_kanjidict);
        this.kanjidictSearchButton.setOnClickListener(this);
        this.exampleSearchButton = (Button) findViewById(R.id.send_to_example_search);
        this.exampleSearchButton.setOnClickListener(this);
        toggleSearchButtons(false);
        this.pickImageButton = (Button) findViewById(R.id.pick_image);
        this.pickImageButton.setOnClickListener(this);
        this.flashToggle = (ToggleButton) findViewById(R.id.auto_flash_toggle);
        this.flashToggle.setOnCheckedChangeListener(this);
        if (icicle != null) {
            this.imageCaptureUri = (Uri) icicle.getParcelable(IMAGE_CAPTURE_URI_KEY);
        }
        this.surfaceView.requestFocus();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        closeCamera();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(IMAGE_CAPTURE_URI_KEY, this.imageCaptureUri);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        Analytics.startSession(this);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        Analytics.endSession(this);
    }

    private void toggleSearchButtons(boolean enabled) {
        this.dictSearchButton.setEnabled(enabled);
        this.kanjidictSearchButton.setEnabled(enabled);
        this.exampleSearchButton.setEnabled(enabled);
    }

    /* access modifiers changed from: package-private */
    public void autoFocus() {
        try {
            this.autoFocusInProgress = false;
            if (this.camera != null) {
                this.imageCaptureUri = createTempFile();
                if (this.imageCaptureUri == null) {
                    Toast.makeText(this, (int) R.string.sd_file_create_failed, 0).show();
                    return;
                }
                this.camera.takePicture(null, null, new ImageCaptureCallback(getContentResolver().openOutputStream(this.imageCaptureUri), this.handler));
            }
        } catch (Exception e) {
            Exception e2 = e;
            Log.e(TAG, e2.getMessage(), e2);
            deleteTempFile();
            ErrorReporter.getInstance().handleException(e2);
            Toast.makeText(this, (int) R.string.image_capture_failed, 0).show();
        }
    }

    /* access modifiers changed from: package-private */
    public void autoFocusFailed() {
        this.autoFocusInProgress = false;
        Toast.makeText(this, (int) R.string.af_failed, 0).show();
    }

    /* access modifiers changed from: package-private */
    public void ocrSuccess(String ocrredText) {
        this.ocrredTextView.setTextSize(30.0f);
        this.ocrredTextView.setText(ocrredText);
        toggleSearchButtons(true);
    }

    /* access modifiers changed from: package-private */
    public void ocrFailed() {
        Toast.makeText(this, (int) R.string.ocr_failed, 0).show();
    }

    public static class OcrHandler extends WebServiceBackedActivity.WsResultHandler {
        public OcrHandler(OcrActivity ocrActivity) {
            super(ocrActivity);
        }

        public void handleMessage(Message msg) {
            OcrActivity ocrActivity = (OcrActivity) this.activity;
            if (this.activity != null) {
                switch (msg.what) {
                    case 1:
                        if (msg.arg1 == 1) {
                            ocrActivity.autoFocus();
                            return;
                        } else {
                            ocrActivity.autoFocusFailed();
                            return;
                        }
                    case 2:
                        ocrActivity.dismissProgressDialog();
                        if (msg.arg1 == 1) {
                            ocrActivity.ocrSuccess((String) msg.obj);
                            return;
                        } else {
                            ocrActivity.ocrFailed();
                            return;
                        }
                    case 3:
                        ocrActivity.crop();
                        return;
                    default:
                        super.handleMessage(msg);
                        return;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public WebServiceBackedActivity.WsResultHandler createHandler() {
        return new OcrHandler(this);
    }

    class OcrTask implements Runnable {
        private Bitmap bitmap;
        private Handler handler;

        public OcrTask(Bitmap b, Handler h) {
            this.bitmap = b;
            this.handler = h;
        }

        public void run() {
            try {
                String ocredText = new WeOcrClient(WwwjdicPreferences.getWeocrUrl(OcrActivity.this), WwwjdicPreferences.getWeocrTimeout(OcrActivity.this)).sendLineOcrRequest(this.bitmap);
                Log.d(OcrActivity.TAG, "OCR result: " + ocredText);
                if (ocredText == null || "".equals(ocredText)) {
                    Log.d("TAG", "OCR failed: empty string returned");
                    this.handler.sendMessage(this.handler.obtainMessage(2, 0, 0));
                    return;
                }
                Message msg = this.handler.obtainMessage(2, 1, 0);
                msg.obj = ocredText;
                this.handler.sendMessage(msg);
            } catch (Exception e) {
                Log.e("TAG", "OCR failed", e);
                this.handler.sendMessage(this.handler.obtainMessage(2, 0, 0));
            }
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            return super.onKeyDown(keyCode, event);
        }
        if (keyCode == 23) {
            Analytics.event("ocrTake", this);
            requestAutoFocus();
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void crop() {
        try {
            if (!tempFileExists()) {
                String path = "";
                if (this.imageCaptureUri != null) {
                    path = new File(this.imageCaptureUri.getPath()).getAbsolutePath();
                }
                Log.w(TAG, "temp file does not exist: " + path);
                Toast.makeText(this, getResources().getString(R.string.read_picture_error, path), 0).show();
            }
            callCropper(this.imageCaptureUri);
        } catch (Exception e) {
            Exception e2 = e;
            Log.e(TAG, e2.getMessage(), e2);
            Toast.makeText(this, (int) R.string.cant_start_cropper, 0).show();
        }
    }

    private void callCropper(Uri imageUri) {
        Intent intent = new Intent(this, CropImage.class);
        Bundle extras = new Bundle();
        intent.setDataAndType(imageUri, "image/jpeg");
        intent.putExtras(extras);
        startActivityForResult(intent, 1);
    }

    private void requestAutoFocus() {
        String str;
        if (!this.autoFocusInProgress) {
            this.autoFocusInProgress = true;
            try {
                this.camera.autoFocus(new Camera.AutoFocusCallback() {
                    public void onAutoFocus(boolean success, Camera camera) {
                        OcrActivity.this.sendAutoFocusResultMessage(success);
                    }
                });
            } catch (RuntimeException e) {
                RuntimeException e2 = e;
                Log.e(TAG, "auto focusFailed: " + e2.getMessage(), e2);
                sendAutoFocusResultMessage(false);
            } finally {
                toggleSearchButtons(false);
                str = "";
                this.ocrredTextView.setText(str);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            deleteTempFile();
            if (resultCode == -1) {
                Bitmap cropped = (Bitmap) data.getExtras().getParcelable("data");
                try {
                    if (WwwjdicPreferences.isDumpCroppedImages(this)) {
                        dumpBitmap(cropped, "cropped-color.jpg");
                    }
                    Bitmap blackAndWhiteBitmap = convertToGrayscale(cropped);
                    if (WwwjdicPreferences.isDumpCroppedImages(this)) {
                        dumpBitmap(blackAndWhiteBitmap, "cropped.jpg");
                    }
                    Analytics.event("ocr", this);
                    submitWsTask(new OcrTask(blackAndWhiteBitmap, this.handler), getResources().getString(R.string.doing_ocr));
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            } else if (resultCode == 0) {
                Toast.makeText(this, (int) R.string.cancelled, 0).show();
            }
        } else if (requestCode == 2 && resultCode == -1) {
            callCropper(data.getData());
        }
    }

    private void deleteTempFile() {
        if (this.imageCaptureUri != null) {
            File f = new File(this.imageCaptureUri.getPath());
            if (f.exists()) {
                Log.d(TAG, "deleted: " + f.delete());
            }
        }
    }

    private boolean tempFileExists() {
        if (this.imageCaptureUri == null) {
            return false;
        }
        return new File(this.imageCaptureUri.getPath()).exists();
    }

    private Bitmap convertToGrayscale(Bitmap bitmap) {
        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(0.0f);
        Paint paint = new Paint();
        paint.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
        Bitmap result = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.RGB_565);
        Canvas drawingCanvas = new Canvas(result);
        Rect src = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        drawingCanvas.drawBitmap(bitmap, src, new Rect(src), paint);
        return result;
    }

    private void dumpBitmap(Bitmap bitmap, String filename) {
        try {
            File wwwjdicDir = new File(String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + "/wwwjdic");
            if (!wwwjdicDir.exists()) {
                wwwjdicDir.mkdir();
            }
            if (wwwjdicDir.canWrite()) {
                FileOutputStream out = new FileOutputStream(new File(wwwjdicDir, filename).getAbsolutePath());
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.flush();
                out.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        Log.d(TAG, String.format("surface changed: w=%d; h=%d)", Integer.valueOf(w), Integer.valueOf(h)));
        if (holder.getSurface() == null) {
            Log.d(TAG, "holder.getSurface() == null");
        } else if (holder == this.surfaceHolder && this.camera != null && w >= h) {
            if (this.isPreviewRunning) {
                stopPreview();
            }
            try {
                Camera.Parameters p = this.camera.getParameters();
                if (this.previewSize != null) {
                    Log.d(TAG, String.format("previewSize: w=%d; h=%d", Integer.valueOf(this.previewSize.width), Integer.valueOf(this.previewSize.height)));
                    p.setPreviewSize(this.previewSize.width, this.previewSize.height);
                } else {
                    int previewWidth = (w >> 3) << 3;
                    int previewHeight = (h >> 3) << 3;
                    Log.d(TAG, String.format("previewSize: w=%d; h=%d", Integer.valueOf(previewWidth), Integer.valueOf(previewHeight)));
                    p.setPreviewSize(previewWidth, previewHeight);
                }
                if (this.pictureSize != null) {
                    Log.d(TAG, String.format("pictureSize: w=%d; h=%d", Integer.valueOf(this.pictureSize.width), Integer.valueOf(this.pictureSize.height)));
                    p.setPictureSize(this.pictureSize.width, this.pictureSize.height);
                } else {
                    int pictureWidth = (w >> 3) << 3;
                    int pictureHeight = (h >> 3) << 3;
                    Log.d(TAG, String.format("pictureSize: w=%d; h=%d", Integer.valueOf(pictureWidth), Integer.valueOf(pictureHeight)));
                    p.setPictureSize(pictureWidth, pictureHeight);
                }
                if (this.supportsFlash) {
                    CameraHolder.getInstance().toggleFlash(this.flashToggle.isChecked(), p);
                }
                this.camera.setParameters(p);
                this.camera.setPreviewDisplay(holder);
                startPreview();
            } catch (Exception e) {
                Exception e2 = e;
                Log.e(TAG, "error initializing camera: " + e2.getMessage(), e2);
                ErrorReporter.getInstance().handleException(e2);
                Dialogs.createErrorDialog(this, R.string.ocr_error).show();
            }
        }
    }

    private void startPreview() {
        if (this.isPreviewRunning) {
            stopPreview();
        }
        try {
            Log.v(TAG, "startPreview");
            this.camera.startPreview();
            this.isPreviewRunning = true;
        } catch (Throwable th) {
            closeCamera();
            throw new RuntimeException("startPreview failed", th);
        }
    }

    public void surfaceCreated(SurfaceHolder holder) {
        Log.d(TAG, "surfaceCreated");
        if (this.camera == null) {
            this.camera = CameraHolder.getInstance().tryOpen();
            if (this.camera == null) {
                Dialogs.createFinishActivityAlertDialog(this, R.string.camera_in_use_title, R.string.camera_in_use_message).show();
                return;
            }
        }
        Camera.Parameters params = this.camera.getParameters();
        List<Camera.Size> supportedPreviewSizes = CameraHolder.getInstance().getSupportedPreviewSizes(params);
        if (supportedPreviewSizes != null) {
            Log.d(TAG, "supported preview sizes");
            for (Camera.Size s : supportedPreviewSizes) {
                Log.d(TAG, String.format("%dx%d", Integer.valueOf(s.width), Integer.valueOf(s.height)));
            }
        }
        List<Camera.Size> supportedPictueSizes = CameraHolder.getInstance().getSupportedPictureSizes(params);
        if (supportedPictueSizes != null) {
            Log.d(TAG, "supported picture sizes:");
            for (Camera.Size s2 : supportedPictueSizes) {
                Log.d(TAG, String.format("%dx%d", Integer.valueOf(s2.width), Integer.valueOf(s2.height)));
            }
        }
        this.supportsFlash = CameraHolder.getInstance().supportsFlash(params);
        if (supportedPreviewSizes != null) {
            try {
                if (!supportedPreviewSizes.isEmpty()) {
                    this.previewSize = getOptimalPreviewSize(supportedPreviewSizes);
                    Log.d(TAG, String.format("preview width: %d; height: %d", Integer.valueOf(this.previewSize.width), Integer.valueOf(this.previewSize.height)));
                    params.setPreviewSize(this.previewSize.width, this.previewSize.height);
                    this.camera.setParameters(params);
                }
            } catch (Exception e) {
                Exception e2 = e;
                Log.e(TAG, "error initializing camera: " + e2.getMessage(), e2);
                ErrorReporter.getInstance().handleException(e2);
                Dialogs.createErrorDialog(this, R.string.ocr_error).show();
                return;
            }
        }
        if (supportedPictueSizes != null && !supportedPictueSizes.isEmpty()) {
            this.pictureSize = getOptimalPictureSize(supportedPictueSizes);
            Log.d(TAG, String.format("picture width: %d; height: %d", Integer.valueOf(this.pictureSize.width), Integer.valueOf(this.pictureSize.height)));
        }
        this.flashToggle.setEnabled(this.supportsFlash);
        this.camera.setPreviewDisplay(holder);
    }

    private Camera.Size getOptimalPictureSize(List<Camera.Size> supportedPictueSizes) {
        Camera.Size result = supportedPictueSizes.get(supportedPictueSizes.size() - 1);
        for (Camera.Size s : supportedPictueSizes) {
            int pixels = s.width * s.height;
            if (pixels >= MIN_PIXELS && pixels <= MAX_PIXELS) {
                return s;
            }
        }
        return result;
    }

    private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes) {
        WindowManager windowManager = (WindowManager) getSystemService("window");
        int targetHeight = windowManager.getDefaultDisplay().getHeight();
        int targetWidth = windowManager.getDefaultDisplay().getWidth();
        Camera.Size result = null;
        double diff = Double.MAX_VALUE;
        for (Camera.Size size : sizes) {
            double newDiff = (double) (Math.abs(size.width - targetWidth) + Math.abs(size.height - targetHeight));
            if (newDiff == 0.0d) {
                return size;
            }
            if (newDiff < diff) {
                diff = newDiff;
                result = size;
            }
        }
        return result;
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        stopPreview();
        closeCamera();
    }

    private void stopPreview() {
        if (this.camera != null && this.isPreviewRunning) {
            this.camera.stopPreview();
        }
        this.isPreviewRunning = false;
    }

    private void closeCamera() {
        if (this.camera != null) {
            CameraHolder.getInstance().release();
            this.camera = null;
            this.isPreviewRunning = false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View v) {
        String key = ((TextView) findViewById(R.id.ocrredText)).getText().toString();
        boolean isDirectSearch = WwwjdicPreferences.isDirectSearch(this);
        SearchCriteria criteria = null;
        Intent intent = new Intent(this, Wwwjdic.class);
        Bundle extras = new Bundle();
        switch (v.getId()) {
            case R.id.pick_image:
                Intent intent2 = new Intent("android.intent.action.GET_CONTENT");
                intent2.addCategory("android.intent.category.OPENABLE");
                intent2.setType("image/*");
                intent2.putExtra("android.intent.extra.LOCAL_ONLY", true);
                startActivityForResult(intent2, 2);
                return;
            case R.id.send_to_dict:
                if (!isDirectSearch) {
                    extras.putInt(Constants.SEARCH_TYPE, 0);
                    break;
                } else {
                    criteria = SearchCriteria.createForDictionaryDefault(key);
                    intent = new Intent(this, DictionaryResultListView.class);
                    break;
                }
            case R.id.send_to_kanjidict:
                if (!isDirectSearch) {
                    extras.putInt(Constants.SEARCH_TYPE, 1);
                    break;
                } else {
                    criteria = SearchCriteria.createForKanjiOrReading(key);
                    intent = new Intent(this, KanjiResultListView.class);
                    break;
                }
            case R.id.send_to_example_search:
                if (!isDirectSearch) {
                    extras.putInt(Constants.SEARCH_TYPE, 2);
                    break;
                } else {
                    criteria = SearchCriteria.createForExampleSearchDefault(key);
                    intent = new Intent(this, ExamplesResultListView.class);
                    break;
                }
        }
        if (isDirectSearch) {
            extras.putSerializable(Constants.CRITERIA_KEY, criteria);
        } else {
            extras.putString(Constants.SEARCH_TEXT_KEY, key);
        }
        intent.putExtras(extras);
        startActivity(intent);
    }

    public boolean onTouch(View v, MotionEvent event) {
        Analytics.event("ocrTouch", this);
        requestAutoFocus();
        return false;
    }

    private Uri createTempFile() {
        File sdDir = Environment.getExternalStorageDirectory();
        if (isUseInternalStorage()) {
            sdDir = new File("/emmc");
        }
        File wwwjdicDir = new File(sdDir, "wwwjdic");
        if (!wwwjdicDir.exists()) {
            wwwjdicDir.mkdir();
        }
        if (!wwwjdicDir.exists() || !wwwjdicDir.canWrite()) {
            return null;
        }
        return Uri.fromFile(new File(wwwjdicDir, "tmp_ocr_" + String.valueOf(System.currentTimeMillis()) + ".jpg"));
    }

    private boolean isUseInternalStorage() {
        return PreferenceManager.getDefaultSharedPreferences(this).getBoolean("pref_ocr_use_internal_storage", false);
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (this.supportsFlash) {
            CameraHolder.getInstance().toggleFlash(isChecked, this.camera.getParameters());
        }
    }

    /* access modifiers changed from: private */
    public void sendAutoFocusResultMessage(boolean success) {
        this.handler.sendMessage(this.handler.obtainMessage(1, success ? 1 : 0, -1));
    }
}
