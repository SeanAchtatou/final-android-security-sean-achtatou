package org.nick.wwwjdic.ocr;

import android.graphics.Bitmap;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Locale;
import org.apache.http.entity.AbstractHttpEntity;

public class OcrFormEntity extends AbstractHttpEntity {
    private static final String BOUNDARY = "--------------abcdefghijklmnopqrstu";
    private static final int BUFF_SIZE = 2048;
    private static final String CONTENT_TYPE = "multipart/form-data; boundary=--------------abcdefghijklmnopqrstu";
    public static final String ECLASS_AUTO = "auto";
    public static final String ECLASS_CHAR = "character";
    public static final String ECLASS_PAGE = "page";
    public static final String ECLASS_TEXT_BLOCK = "text_block";
    public static final String ECLASS_TEXT_LINE = "text_line";
    public static final String ECLASS_WORD = "word";
    private static final String HEADER = "----------------abcdefghijklmnopqrstu\r\nContent-Disposition: form-data; name=\"userfile\"; filename=\"file.jpg\"\r\nContent-Type: image/jpeg\r\nContent-Transfer-Encoding: binary\r\n\r\n";
    private static final int IMG_QUALITY = 90;
    private static final String TRAILER = "\r\n----------------abcdefghijklmnopqrstu\r\nContent-Disposition: form-data; name=\"outputformat\"\r\n\r\ntxt\r\n----------------abcdefghijklmnopqrstu\r\nContent-Disposition: form-data; name=\"eclass\"\r\n\r\n%s\r\n----------------abcdefghijklmnopqrstu\r\nContent-Disposition: form-data; name=\"ntop\"\r\n\r\n%d\r\n----------------abcdefghijklmnopqrstu\r\nContent-Disposition: form-data; name=\"outputencoding\"\r\n\r\nutf-8\r\n----------------abcdefghijklmnopqrstu--\r\n";
    private ByteArrayOutputStream buff;
    private String trailer;

    public OcrFormEntity(Bitmap bitmap, int quality, String eclass, Integer numberOfTopCandidates) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream(2048);
        bitmap.compress(Bitmap.CompressFormat.JPEG, quality, baos);
        baos.close();
        this.buff = baos;
        this.trailer = String.format(Locale.US, TRAILER, eclass, Integer.valueOf(numberOfTopCandidates != null ? numberOfTopCandidates.intValue() : 1));
        setContentType(CONTENT_TYPE);
        setChunked(false);
    }

    public OcrFormEntity(Bitmap img, String eclass, Integer numberOfTopCandidates) throws IOException {
        this(img, IMG_QUALITY, eclass, numberOfTopCandidates);
    }

    public OcrFormEntity(Bitmap img) throws IOException {
        this(img, IMG_QUALITY, ECLASS_AUTO, null);
    }

    public InputStream getContent() throws IOException, IllegalStateException {
        throw new UnsupportedOperationException();
    }

    public long getContentLength() {
        return (long) (this.buff.size() + HEADER.length() + this.trailer.length());
    }

    public boolean isRepeatable() {
        return true;
    }

    public boolean isStreaming() {
        return false;
    }

    public void writeTo(OutputStream os) throws IOException {
        if (os == null) {
            throw new IllegalArgumentException("Output stream can't be null.");
        }
        os.write(HEADER.getBytes("ascii"));
        this.buff.writeTo(os);
        os.write(this.trailer.getBytes("ascii"));
        os.flush();
    }
}
