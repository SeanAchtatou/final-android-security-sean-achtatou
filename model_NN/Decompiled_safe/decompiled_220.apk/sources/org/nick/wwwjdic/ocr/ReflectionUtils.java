package org.nick.wwwjdic.ocr;

import android.hardware.Camera;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

public class ReflectionUtils {
    private static Method Parameters_getFlashMode;
    private static Method Parameters_getSupportedPictureSizes;
    private static Method Parameters_getSupportedPreviewSizes;
    private static Method Parameters_setFlashMode;

    static {
        initCompatibility();
    }

    private ReflectionUtils() {
    }

    private static void initCompatibility() {
        try {
            Parameters_getSupportedPreviewSizes = Camera.Parameters.class.getMethod("getSupportedPreviewSizes", new Class[0]);
            Parameters_getSupportedPictureSizes = Camera.Parameters.class.getMethod("getSupportedPictureSizes", new Class[0]);
            Parameters_getFlashMode = Camera.Parameters.class.getMethod("getFlashMode", new Class[0]);
            Parameters_setFlashMode = Camera.Parameters.class.getMethod("setFlashMode", String.class);
        } catch (NoSuchMethodException e) {
        }
    }

    public static List<Camera.Size> getSupportedPreviewSizes(Camera.Parameters p) {
        try {
            if (Parameters_getSupportedPreviewSizes != null) {
                return (List) Parameters_getSupportedPreviewSizes.invoke(p, new Object[0]);
            }
            return null;
        } catch (InvocationTargetException e) {
            InvocationTargetException ite = e;
            Throwable cause = ite.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException(ite);
            }
        } catch (IllegalAccessException e2) {
            return null;
        }
    }

    public static List<Camera.Size> getSupportedPictureSizes(Camera.Parameters p) {
        try {
            if (Parameters_getSupportedPictureSizes != null) {
                return (List) Parameters_getSupportedPictureSizes.invoke(p, new Object[0]);
            }
            return null;
        } catch (InvocationTargetException e) {
            InvocationTargetException ite = e;
            Throwable cause = ite.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException(ite);
            }
        } catch (IllegalAccessException e2) {
            return null;
        }
    }

    public static String getFlashMode(Camera.Parameters p) {
        try {
            if (Parameters_getFlashMode != null) {
                return (String) Parameters_getFlashMode.invoke(p, new Object[0]);
            }
            return null;
        } catch (InvocationTargetException e) {
            InvocationTargetException ite = e;
            Throwable cause = ite.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException(ite);
            }
        } catch (IllegalAccessException e2) {
            return null;
        }
    }

    public static void setFlashMode(Camera.Parameters p, String flashMode) {
        try {
            if (Parameters_setFlashMode != null) {
                Parameters_setFlashMode.invoke(p, flashMode);
            }
        } catch (InvocationTargetException e) {
            InvocationTargetException ite = e;
            Throwable cause = ite.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException(ite);
            }
        } catch (IllegalAccessException e2) {
        }
    }
}
