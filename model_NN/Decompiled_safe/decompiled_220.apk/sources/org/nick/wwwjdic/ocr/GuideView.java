package org.nick.wwwjdic.ocr;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

public class GuideView extends View {
    private static int FIRST_GUIDE_Y = 50;
    private static int GUIDE_HEIGHT = 50;

    public GuideView(Context context) {
        super(context);
    }

    public GuideView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        Paint paint = new Paint();
        Rect rect = new Rect();
        int width = canvas.getWidth();
        int numGuides = (canvas.getHeight() - FIRST_GUIDE_Y) / GUIDE_HEIGHT;
        int top = 0;
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(-3355444);
        for (int i = 0; i < numGuides; i++) {
            if (i == 0) {
                top = FIRST_GUIDE_Y;
            } else {
                top += GUIDE_HEIGHT;
            }
            rect.set(0, top, width, GUIDE_HEIGHT + top);
            canvas.drawRect(rect, paint);
        }
    }
}
