package org.nick.wwwjdic.ocr.crop;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.CountDownLatch;
import org.acra.ErrorReporter;
import org.nick.wwwjdic.R;
import org.nick.wwwjdic.ocr.crop.MonitoredActivity;

public class CropImage extends MonitoredActivity {
    private static final boolean RECYCLE_INPUT = true;
    private static final String TAG = CropImage.class.getSimpleName();
    /* access modifiers changed from: private */
    public int mAspectX;
    /* access modifiers changed from: private */
    public int mAspectY;
    /* access modifiers changed from: private */
    public Bitmap mBitmap;
    /* access modifiers changed from: private */
    public boolean mCircleCrop = false;
    HighlightView mCrop;
    /* access modifiers changed from: private */
    public final Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    public CropImageView mImageView;
    private int mOutputX;
    private int mOutputY;
    Runnable mRunFaceDetection = new Runnable() {
        Matrix mImageMatrix;
        float mScale = 1.0f;

        /* access modifiers changed from: private */
        public void makeDefault() {
            boolean z;
            HighlightView hv = new HighlightView(CropImage.this.mImageView);
            int width = CropImage.this.mBitmap.getWidth();
            int height = CropImage.this.mBitmap.getHeight();
            Rect imageRect = new Rect(0, 0, width, height);
            int cropWidth = (Math.min(width, height) * 4) / 5;
            int cropHeight = cropWidth;
            if (!(CropImage.this.mAspectX == 0 || CropImage.this.mAspectY == 0)) {
                if (CropImage.this.mAspectX > CropImage.this.mAspectY) {
                    cropHeight = (CropImage.this.mAspectY * cropWidth) / CropImage.this.mAspectX;
                } else {
                    cropWidth = (CropImage.this.mAspectX * cropHeight) / CropImage.this.mAspectY;
                }
            }
            int x = (width - cropWidth) / 2;
            int y = (height - cropHeight) / 2;
            RectF cropRect = new RectF((float) x, (float) y, (float) (x + cropWidth), (float) (y + cropHeight));
            Matrix matrix = this.mImageMatrix;
            boolean access$4 = CropImage.this.mCircleCrop;
            if (CropImage.this.mAspectX == 0 || CropImage.this.mAspectY == 0) {
                z = false;
            } else {
                z = true;
            }
            hv.setup(matrix, imageRect, cropRect, access$4, z);
            CropImage.this.mImageView.add(hv);
        }

        public void run() {
            this.mImageMatrix = CropImage.this.mImageView.getImageMatrix();
            this.mScale = 1.0f / this.mScale;
            CropImage.this.mHandler.post(new Runnable() {
                public void run() {
                    AnonymousClass1.this.makeDefault();
                    CropImage.this.mImageView.invalidate();
                    if (CropImage.this.mImageView.mHighlightViews.size() == 1) {
                        CropImage.this.mCrop = CropImage.this.mImageView.mHighlightViews.get(0);
                        CropImage.this.mCrop.setFocus(true);
                    }
                }
            });
        }
    };
    boolean mSaving;
    private boolean mScale;
    private boolean mScaleUp = true;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        requestWindowFeature(1);
        setContentView((int) R.layout.cropimage);
        this.mImageView = (CropImageView) findViewById(R.id.image);
        this.mImageView.mContext = this;
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            this.mBitmap = (Bitmap) extras.getParcelable("data");
            this.mAspectX = extras.getInt("aspectX");
            this.mAspectY = extras.getInt("aspectY");
            this.mOutputX = extras.getInt("outputX");
            this.mOutputY = extras.getInt("outputY");
            this.mScale = extras.getBoolean("scale", true);
            this.mScaleUp = extras.getBoolean("scaleUpIfNeeded", true);
        }
        if (this.mBitmap == null) {
            InputStream is = null;
            try {
                is = getContentResolver().openInputStream(intent.getData());
                this.mBitmap = BitmapFactory.decodeStream(is);
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) {
                    }
                }
            } catch (IOException e2) {
                IOException e3 = e2;
                Log.e(TAG, "error reading picture: " + e3.getMessage(), e3);
                ErrorReporter.getInstance().handleException(e3);
                Toast.makeText(this, getResources().getString(R.string.read_picture_error, e3.getMessage()), 0).show();
                finish();
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e4) {
                    }
                }
            } catch (Throwable th) {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e5) {
                    }
                }
                throw th;
            }
        }
        if (this.mBitmap == null) {
            finish();
            return;
        }
        getWindow().addFlags(1024);
        findViewById(R.id.discard).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CropImage.this.setResult(0);
                CropImage.this.finish();
            }
        });
        findViewById(R.id.save).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CropImage.this.onSaveClicked();
            }
        });
        startFaceDetection();
    }

    private void startFaceDetection() {
        if (!isFinishing()) {
            this.mImageView.setImageBitmapResetBase(this.mBitmap, true);
            startBackgroundJob(this, null, getResources().getString(R.string.runningFaceDetection), new Runnable() {
                public void run() {
                    final CountDownLatch latch = new CountDownLatch(1);
                    final Bitmap b = CropImage.this.mBitmap;
                    CropImage.this.mHandler.post(new Runnable() {
                        public void run() {
                            if (!(b == CropImage.this.mBitmap || b == null)) {
                                CropImage.this.mImageView.setImageBitmapResetBase(b, true);
                                CropImage.this.mBitmap.recycle();
                                CropImage.this.mBitmap = b;
                            }
                            if (CropImage.this.mImageView.getScale() == 1.0f) {
                                CropImage.this.mImageView.center(true, true);
                            }
                            latch.countDown();
                        }
                    });
                    try {
                        latch.await();
                        CropImage.this.mRunFaceDetection.run();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }, this.mHandler);
        }
    }

    private static class BackgroundJob extends MonitoredActivity.LifeCycleAdapter implements Runnable {
        /* access modifiers changed from: private */
        public final MonitoredActivity mActivity;
        private final Runnable mCleanupRunner = new Runnable() {
            public void run() {
                BackgroundJob.this.mActivity.removeLifeCycleListener(BackgroundJob.this);
                if (BackgroundJob.this.mDialog.getWindow() != null) {
                    BackgroundJob.this.mDialog.dismiss();
                }
            }
        };
        /* access modifiers changed from: private */
        public final ProgressDialog mDialog;
        private final Handler mHandler;
        private final Runnable mJob;

        public BackgroundJob(MonitoredActivity activity, Runnable job, ProgressDialog dialog, Handler handler) {
            this.mActivity = activity;
            this.mDialog = dialog;
            this.mJob = job;
            this.mActivity.addLifeCycleListener(this);
            this.mHandler = handler;
        }

        public void run() {
            try {
                this.mJob.run();
            } finally {
                this.mHandler.post(this.mCleanupRunner);
            }
        }

        public void onActivityDestroyed(MonitoredActivity activity) {
            this.mCleanupRunner.run();
            this.mHandler.removeCallbacks(this.mCleanupRunner);
        }

        public void onActivityStopped(MonitoredActivity activity) {
            this.mDialog.hide();
        }

        public void onActivityStarted(MonitoredActivity activity) {
            this.mDialog.show();
        }
    }

    private static void startBackgroundJob(MonitoredActivity activity, String title, String message, Runnable job, Handler handler) {
        new Thread(new BackgroundJob(activity, job, ProgressDialog.show(activity, title, message, true, false), handler)).start();
    }

    /* access modifiers changed from: private */
    public void onSaveClicked() {
        Bitmap croppedImage;
        if (this.mCrop != null && !this.mSaving) {
            this.mSaving = true;
            if (this.mOutputX == 0 || this.mOutputY == 0 || this.mScale) {
                Rect r = this.mCrop.getCropRect();
                int width = r.width();
                int height = r.height();
                croppedImage = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
                new Canvas(croppedImage).drawBitmap(this.mBitmap, r, new Rect(0, 0, width, height), (Paint) null);
                this.mImageView.clear();
                this.mBitmap.recycle();
                if (!(this.mOutputX == 0 || this.mOutputY == 0 || !this.mScale)) {
                    croppedImage = transform(new Matrix(), croppedImage, this.mOutputX, this.mOutputY, this.mScaleUp, true);
                }
            } else {
                croppedImage = Bitmap.createBitmap(this.mOutputX, this.mOutputY, Bitmap.Config.RGB_565);
                Canvas canvas = new Canvas(croppedImage);
                Rect srcRect = this.mCrop.getCropRect();
                Rect dstRect = new Rect(0, 0, this.mOutputX, this.mOutputY);
                int dx = (srcRect.width() - dstRect.width()) / 2;
                int dy = (srcRect.height() - dstRect.height()) / 2;
                srcRect.inset(Math.max(0, dx), Math.max(0, dy));
                dstRect.inset(Math.max(0, -dx), Math.max(0, -dy));
                canvas.drawBitmap(this.mBitmap, srcRect, dstRect, (Paint) null);
                this.mImageView.clear();
                this.mBitmap.recycle();
            }
            this.mImageView.setImageBitmapResetBase(croppedImage, true);
            this.mImageView.center(true, true);
            this.mImageView.mHighlightViews.clear();
            Bundle extras = new Bundle();
            extras.putParcelable("data", croppedImage);
            setResult(-1, new Intent().setAction("inline-data").putExtras(extras));
            finish();
        }
    }

    /* JADX INFO: Multiple debug info for r1v2 float: [D('deltaY' int), D('bitmapWidthF' float)] */
    /* JADX INFO: Multiple debug info for r0v2 float: [D('deltaX' int), D('bitmapHeightF' float)] */
    /* JADX INFO: Multiple debug info for r9v1 android.graphics.Bitmap: [D('targetWidth' int), D('b2' android.graphics.Bitmap)] */
    /* JADX INFO: Multiple debug info for r7v5 android.graphics.Bitmap: [D('b1' android.graphics.Bitmap), D('b2' android.graphics.Bitmap)] */
    /* JADX INFO: Multiple debug info for r7v9 android.graphics.Bitmap: [D('scaler' android.graphics.Matrix), D('b2' android.graphics.Bitmap)] */
    /* JADX INFO: Multiple debug info for r11v12 android.graphics.Canvas: [D('scaleUp' boolean), D('c' android.graphics.Canvas)] */
    /* JADX INFO: Multiple debug info for r0v18 int: [D('deltaX' int), D('deltaXHalf' int)] */
    /* JADX INFO: Multiple debug info for r1v7 int: [D('deltaY' int), D('deltaYHalf' int)] */
    /* JADX INFO: Multiple debug info for r1v8 int: [D('deltaYHalf' int), D('dstX' int)] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private static Bitmap transform(Matrix scaler, Bitmap source, int targetWidth, int targetHeight, boolean scaleUp, boolean recycle) {
        Bitmap b1;
        int deltaX = source.getWidth() - targetWidth;
        int deltaY = source.getHeight() - targetHeight;
        if (scaleUp || (deltaX >= 0 && deltaY >= 0)) {
            float bitmapWidthF = (float) source.getWidth();
            float bitmapHeightF = (float) source.getHeight();
            if (bitmapWidthF / bitmapHeightF > ((float) targetWidth) / ((float) targetHeight)) {
                float scale = ((float) targetHeight) / bitmapHeightF;
                if (scale < 0.9f || scale > 1.0f) {
                    scaler.setScale(scale, scale);
                } else {
                    scaler = null;
                }
            } else {
                float scale2 = ((float) targetWidth) / bitmapWidthF;
                if (scale2 < 0.9f || scale2 > 1.0f) {
                    scaler.setScale(scale2, scale2);
                } else {
                    scaler = null;
                }
            }
            if (scaler != null) {
                b1 = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), scaler, true);
            } else {
                b1 = source;
            }
            if (recycle && b1 != source) {
                source.recycle();
            }
            Bitmap b2 = Bitmap.createBitmap(b1, Math.max(0, b1.getWidth() - targetWidth) / 2, Math.max(0, b1.getHeight() - targetHeight) / 2, targetWidth, targetHeight);
            if (b2 != b1 && (recycle || b1 != source)) {
                b1.recycle();
            }
            return b2;
        }
        Bitmap b22 = Bitmap.createBitmap(targetWidth, targetHeight, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b22);
        int deltaXHalf = Math.max(0, deltaX / 2);
        int deltaYHalf = Math.max(0, deltaY / 2);
        Rect src = new Rect(deltaXHalf, deltaYHalf, Math.min(targetWidth, source.getWidth()) + deltaXHalf, Math.min(targetHeight, source.getHeight()) + deltaYHalf);
        int dstX = (targetWidth - src.width()) / 2;
        int dstY = (targetHeight - src.height()) / 2;
        c.drawBitmap(source, src, new Rect(dstX, dstY, targetWidth - dstX, targetHeight - dstY), (Paint) null);
        if (!recycle) {
            return b22;
        }
        source.recycle();
        return b22;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }
}
