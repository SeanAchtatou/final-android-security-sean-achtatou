package org.nick.wwwjdic;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import java.util.concurrent.Future;

public abstract class WebServiceBackedActivity extends Activity {
    protected Future<?> activeWsRequest;
    protected WsResultHandler handler;
    protected ProgressDialog progressDialog;

    /* access modifiers changed from: protected */
    public abstract void activityOnCreate(Bundle bundle);

    /* access modifiers changed from: protected */
    public abstract WsResultHandler createHandler();

    public static abstract class WsResultHandler extends Handler {
        protected Activity activity;

        public WsResultHandler(Activity krActivity) {
            this.activity = krActivity;
        }

        public Activity getActivity() {
            return this.activity;
        }

        public void setActivity(Activity activity2) {
            this.activity = activity2;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.handler = createHandler();
        activityOnCreate(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.activeWsRequest != null) {
            this.activeWsRequest.cancel(true);
        }
        activityOnDestroy();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void activityOnDestroy() {
    }

    /* access modifiers changed from: protected */
    public void submitWsTask(Runnable task, String message) {
        showProgressDialog(message);
        this.activeWsRequest = getApp().getExecutorService().submit(task);
    }

    public void showProgressDialog(String message) {
        this.progressDialog = ProgressDialog.show(this, "", message, true);
    }

    public void dismissProgressDialog() {
        if (this.progressDialog != null && this.progressDialog.isShowing() && !isFinishing()) {
            this.progressDialog.dismiss();
            this.progressDialog = null;
        }
    }

    /* access modifiers changed from: protected */
    public WwwjdicApplication getApp() {
        return (WwwjdicApplication) getApplication();
    }
}
