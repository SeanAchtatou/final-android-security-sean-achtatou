package org.nick.wwwjdic;

import java.util.ArrayList;
import java.util.List;

public class Radicals {
    private static final Radicals instance = new Radicals();
    private final List<Radical> radicals = new ArrayList();

    private Radicals() {
    }

    public static Radicals getInstance() {
        return instance;
    }

    public void addRadicals(int strokeCount, int[] radicalNumbers, String[] radicalStrs) {
        if (radicalNumbers.length != radicalStrs.length) {
            throw new IllegalArgumentException("arrays length does not match");
        }
        for (int i = 0; i < radicalNumbers.length; i++) {
            this.radicals.add(new Radical(radicalNumbers[i], radicalStrs[i], strokeCount));
        }
    }

    public boolean isInitialized() {
        return !this.radicals.isEmpty();
    }

    public List<Radical> getRadicals() {
        return this.radicals;
    }

    public Radical getRadical(int index) {
        return this.radicals.get(index);
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public Radical getRadicalByNumber(int radicalNumber) {
        if (radicalNumber > this.radicals.size()) {
            return null;
        }
        return this.radicals.get(radicalNumber - 1);
    }
}
