package org.nick.wwwjdic;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.List;
import org.nick.wwwjdic.utils.StringUtils;

public class DictionaryEntryAdapter extends BaseAdapter {
    private final Context context;
    private final List<DictionaryEntry> entries;

    public DictionaryEntryAdapter(Context context2, List<DictionaryEntry> entries2) {
        this.context = context2;
        this.entries = entries2;
    }

    public int getCount() {
        if (this.entries == null) {
            return 0;
        }
        return this.entries.size();
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public Object getItem(int position) {
        if (this.entries == null) {
            return null;
        }
        return this.entries.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        DictionaryEntry entry = this.entries.get(position);
        if (convertView == null) {
            return new DictionaryEntryView(this.context, entry);
        }
        DictionaryEntryView entryView = (DictionaryEntryView) convertView;
        entryView.populate(entry);
        return entryView;
    }

    private final class DictionaryEntryView extends LinearLayout {
        private TextView entryText;
        private TextView readingText;
        private TextView translationText;

        public DictionaryEntryView(Context context, DictionaryEntry entry) {
            super(context);
            setOrientation(1);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(-2, -2);
            params.setMargins(5, 3, 5, 0);
            this.entryText = createTextView(context, R.style.dict_list_heading, entry.getWord());
            addView(this.entryText, params);
            this.readingText = createTextView(context, R.style.dict_list_reading, "");
            addView(this.readingText, params);
            if (entry.getReading() != null) {
                this.readingText.setText(entry.getReading());
            }
            this.translationText = createTextView(context, R.style.dict_list_translation, StringUtils.join(entry.getMeanings(), "/", 0));
            addView(this.translationText, params);
        }

        private TextView createTextView(Context context, int style, String text) {
            TextView result = new TextView(context, null, style);
            result.setSingleLine(true);
            result.setEllipsize(TextUtils.TruncateAt.END);
            result.setText(text);
            return result;
        }

        /* access modifiers changed from: package-private */
        public void populate(DictionaryEntry entry) {
            this.entryText.setText(entry.getWord());
            if (entry.getReading() != null) {
                this.readingText.setText(entry.getReading());
            } else {
                this.readingText.setText("");
            }
            this.translationText.setText(StringUtils.join(entry.getMeanings(), "/", 0));
        }
    }
}
