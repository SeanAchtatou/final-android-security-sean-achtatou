package org.nick.wwwjdic;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.text.ClipboardManager;
import android.util.Log;
import android.widget.Toast;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.List;
import java.util.concurrent.Future;
import org.nick.wwwjdic.history.HistoryDbHelper;
import org.nick.wwwjdic.utils.Analytics;

public abstract class ResultListViewBase<T> extends ListActivity implements ResultListView<T> {
    private static final String TAG = ResultListViewBase.class.getSimpleName();
    protected SearchCriteria criteria;
    protected HistoryDbHelper db = HistoryDbHelper.getInstance(this);
    protected Handler guiThread = new Handler();
    protected ProgressDialog progressDialog;
    protected Future<?> transPending;

    public abstract void setResult(List<T> list);

    protected ResultListViewBase() {
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        Analytics.startSession(this);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        Analytics.endSession(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.transPending != null) {
            this.transPending.cancel(true);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void submitSearchTask(SearchTask<T> searchTask) {
        this.progressDialog = ProgressDialog.show(this, "", getResources().getText(R.string.loading).toString(), true);
        this.transPending = getApp().getExecutorService().submit(searchTask);
    }

    /* access modifiers changed from: protected */
    public WwwjdicApplication getApp() {
        return (WwwjdicApplication) getApplication();
    }

    public void setError(final Exception ex) {
        if (!isFinishing()) {
            this.guiThread.post(new Runnable() {
                public void run() {
                    ResultListViewBase.this.setTitle(ResultListViewBase.this.getResources().getText(R.string.error));
                    ResultListViewBase.this.dismissProgressDialog();
                    AlertDialog.Builder alert = new AlertDialog.Builder(ResultListViewBase.this);
                    alert.setTitle((int) R.string.error);
                    if ((ex instanceof SocketTimeoutException) || (ex.getCause() instanceof SocketTimeoutException)) {
                        alert.setMessage(ResultListViewBase.this.getResources().getString(R.string.timeout_error_message));
                    } else if ((ex instanceof SocketException) || (ex.getCause() instanceof SocketException)) {
                        alert.setMessage(ResultListViewBase.this.getResources().getString(R.string.socket_error_message));
                    } else {
                        alert.setMessage(String.valueOf(ResultListViewBase.this.getResources().getString(R.string.generic_error_message)) + "(" + ex.getMessage() + ")");
                    }
                    alert.setPositiveButton(ResultListViewBase.this.getResources().getText(R.string.ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.dismiss();
                            ResultListViewBase.this.finish();
                        }
                    });
                    alert.show();
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void extractSearchCriteria() {
        this.criteria = (SearchCriteria) getIntent().getSerializableExtra(Constants.CRITERIA_KEY);
        if (this.criteria != null) {
            Log.d(TAG, "query string: " + this.criteria.getQueryString());
        }
    }

    /* access modifiers changed from: protected */
    public String getWwwjdicUrl() {
        return WwwjdicPreferences.getWwwjdicUrl(this);
    }

    /* access modifiers changed from: protected */
    public int getHttpTimeoutSeconds() {
        return WwwjdicPreferences.getWwwjdicTimeoutSeconds(this);
    }

    /* access modifiers changed from: protected */
    public void dismissProgressDialog() {
        try {
            if (this.progressDialog != null && this.progressDialog.isShowing()) {
                this.progressDialog.dismiss();
                this.progressDialog = null;
            }
        } catch (Throwable th) {
            Throwable t = th;
            Log.w(TAG, "error dismissing progress dialog: " + t.getMessage(), t);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: protected */
    public void setFavoriteId(Intent intent, WwwjdicEntry entry) {
        Long favoriteId = this.db.getFavoriteId(entry.getHeadword());
        if (favoriteId != null) {
            intent.putExtra(Constants.IS_FAVORITE, true);
            entry.setId(favoriteId);
        }
    }

    /* access modifiers changed from: protected */
    public void copy(WwwjdicEntry entry) {
        String headword = entry.getHeadword();
        ((ClipboardManager) getSystemService("clipboard")).setText(headword);
        Toast.makeText(this, getResources().getString(R.string.copied_to_clipboard, headword), 0).show();
    }

    /* access modifiers changed from: protected */
    public void addToFavorites(WwwjdicEntry entry) {
        this.db.addFavorite(entry);
        Toast.makeText(this, getResources().getString(R.string.added_to_favorites, entry.getHeadword()), 0).show();
    }
}
