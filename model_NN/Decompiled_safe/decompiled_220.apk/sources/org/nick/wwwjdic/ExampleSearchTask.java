package org.nick.wwwjdic;

import au.com.bytecode.opencsv.CSVWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;

public class ExampleSearchTask extends SearchTask<ExampleSentence> {
    private static final Pattern CLOSING_UL_PATTERN = Pattern.compile("^.*</ul>.*$");
    private static final int EXAMPLES_FINISHED = 3;
    private static final int EXAMPLE_FOLLOWS = 1;
    private static final Pattern INPUT_PATTERN = Pattern.compile("^.*<INPUT.*$");
    private static final int IN_EXAMPLES_BLOCK = 0;
    private static final Pattern LI_PATTERN = Pattern.compile("^.*<li>.*$");
    private static final int TRANSLATION_FOLLOWS = 2;
    private static final Pattern UL_PATTERN = Pattern.compile("^.*<ul>.*$");
    private int maxNumExamples;

    public ExampleSearchTask(String url, int timeoutSeconds, ResultListView<ExampleSentence> resultView, SearchCriteria searchCriteria, int maxNumExamples2) {
        super(url, timeoutSeconds, resultView, searchCriteria);
        this.maxNumExamples = maxNumExamples2;
    }

    /* access modifiers changed from: protected */
    public List<ExampleSentence> parseResult(String html) {
        List<ExampleSentence> result = new ArrayList<>();
        String[] lines = html.split(CSVWriter.DEFAULT_LINE_END);
        int state = -1;
        String japaneseSentence = null;
        int length = lines.length;
        int i = 0;
        while (true) {
            if (i < length) {
                String line = lines[i];
                if (UL_PATTERN.matcher(line).matches()) {
                    state = 0;
                } else if (LI_PATTERN.matcher(line).matches()) {
                    state = 1;
                } else if (INPUT_PATTERN.matcher(line).matches()) {
                    state = 2;
                } else if (!CLOSING_UL_PATTERN.matcher(line).matches()) {
                    switch (state) {
                        case 1:
                            japaneseSentence = line.trim();
                            continue;
                        case 2:
                            if (japaneseSentence != null) {
                                result.add(new ExampleSentence(japaneseSentence, line.trim()));
                                break;
                            } else {
                                continue;
                            }
                    }
                }
                i++;
            }
        }
        return result;
    }

    /* access modifiers changed from: protected */
    public String query(WwwjdicQuery query) {
        try {
            SearchCriteria criteria = (SearchCriteria) query;
            HttpPost post = new HttpPost(this.url);
            List<NameValuePair> pairs = new ArrayList<>();
            String searchString = criteria.getQueryString();
            if (criteria.isExactMatch()) {
                searchString = "\\<" + searchString + "\\>";
            }
            pairs.add(new BasicNameValuePair("exsrchstr", searchString));
            pairs.add(new BasicNameValuePair("exsrchnum", Integer.toString(this.maxNumExamples)));
            post.setEntity(new UrlEncodedFormEntity(pairs, "EUC-JP"));
            return (String) this.httpclient.execute(post, this.responseHandler);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
