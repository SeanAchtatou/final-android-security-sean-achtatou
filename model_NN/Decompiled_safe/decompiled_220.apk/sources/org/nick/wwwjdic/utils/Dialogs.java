package org.nick.wwwjdic.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import org.nick.wwwjdic.R;
import org.nick.wwwjdic.WwwjdicPreferences;

public class Dialogs {
    private Dialogs() {
    }

    public static Dialog createTipDialog(Context context, int messageId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(messageId);
        builder.setTitle((int) R.string.tip);
        builder.setIcon(17301659);
        return builder.create();
    }

    public static Dialog createErrorDialog(Context context, int messageId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(messageId).setTitle((int) R.string.error).setIcon(17301543).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        return builder.create();
    }

    public static void showTipOnce(Activity activity, String tipKey, int messageId) {
        if (!WwwjdicPreferences.isTipShown(activity, tipKey)) {
            WwwjdicPreferences.setTipShown(activity, tipKey);
            createTipDialog(activity, messageId).show();
        }
    }

    public static Dialog createFinishActivityAlertDialog(final Activity activity, int titleId, int messageId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(messageId);
        builder.setTitle(titleId);
        builder.setIcon(17301543);
        builder.setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                activity.finish();
            }
        });
        builder.setCancelable(false);
        return builder.create();
    }
}
