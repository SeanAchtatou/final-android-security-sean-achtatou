package org.nick.wwwjdic.utils;

public class Pair<T, U> {
    private T first;
    private U second;

    public Pair(T first2, U second2) {
        this.first = first2;
        this.second = second2;
    }

    public T getFirst() {
        return this.first;
    }

    public U getSecond() {
        return this.second;
    }
}
