package org.nick.wwwjdic.utils;

import android.content.Context;
import android.content.Intent;
import android.text.style.ClickableSpan;
import android.view.View;

public class IntentSpan extends ClickableSpan {
    private Context context;
    private Intent intent;

    public IntentSpan(Context context2, Intent intent2) {
        this.context = context2;
        this.intent = intent2;
    }

    public void onClick(View widget) {
        this.context.startActivity(this.intent);
    }
}
