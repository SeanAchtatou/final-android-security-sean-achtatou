package org.nick.wwwjdic.utils;

import android.content.Context;
import org.nick.wwwjdic.Constants;
import org.nick.wwwjdic.R;
import org.nick.wwwjdic.WwwjdicEntry;

public class DictUtils {
    private DictUtils() {
    }

    public static String extractSearchKey(WwwjdicEntry wwwjdicEntry) {
        String searchKey = wwwjdicEntry.getHeadword();
        if (searchKey.indexOf(Constants.DICT_VARIATION_DELIMITER) != -1) {
            return searchKey.split(Constants.DICT_VARIATION_DELIMITER)[0].replace(Constants.DICT_COMMON_USAGE_MARKER, "");
        }
        return searchKey;
    }

    public static String stripWwwjdicTags(Context ctx, String meaning) {
        return stripLangTags(ctx, stripCrossrefTags(ctx, stripTags(ctx, R.array.wwwjdic_dialect_tags, stripTags(ctx, R.array.wwwjdic_misc_tags, stripTags(ctx, R.array.wwwjdic_fields_tags, stripTags(ctx, R.array.wwwjdic_pos_tags, stripCommonWordTag(ctx, stripNumberTags(ctx, meaning))))))));
    }

    public static String stripCrossrefTags(Context ctx, String meaning) {
        return new String(meaning).replaceAll("\\(See\\s\\S+\\)", "").trim();
    }

    public static String stripNumberTags(Context ctx, String meaning) {
        return new String(meaning).replaceAll("\\(\\d+\\)", "").trim();
    }

    public static String stripLangTags(Context ctx, String meaning) {
        return new String(meaning).replaceAll("\\(\\w{3}: \\S+\\)", "").trim();
    }

    public static String stripCommonWordTag(Context ctx, String meaning) {
        return new String(meaning).replaceAll("\\(P\\)", "").trim().replaceAll("\\(p\\)", "").trim();
    }

    public static String stripPosTags(Context ctx, String meaning) {
        return stripTags(ctx, R.array.wwwjdic_pos_tags, meaning);
    }

    public static String stripTags(Context ctx, int arrayId, String meaning) {
        String[] tags = ctx.getResources().getStringArray(arrayId);
        String result = new String(meaning);
        for (String tag : tags) {
            result = result.replaceAll("\\(" + tag + "\\)", "").trim().replaceAll("\\(" + tag + "\\S+\\)", "").trim().replaceAll("\\{" + tag + "\\}", "").trim().replaceAll("\\{" + tag + "\\S+\\}", "").trim();
        }
        return result;
    }
}
