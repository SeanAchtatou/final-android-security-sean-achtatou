package org.nick.wwwjdic;

import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import org.nick.wwwjdic.history.FavoritesAndHistory;
import org.nick.wwwjdic.hkr.RecognizeKanjiActivity;
import org.nick.wwwjdic.ocr.OcrActivity;

public class WwwjdicActivityBase extends Activity {
    private static final int ITEM_ID_ABOUT = 1;
    private static final int ITEM_ID_DRAW = 4;
    private static final int ITEM_ID_HISTORY = 5;
    private static final int ITEM_ID_OCR = 2;
    private static final int ITEM_ID_SETTINGS = 3;
    protected static final int NUM_RECENT_HISTORY_ENTRIES = 5;
    protected boolean inputTextFromBundle;

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 2, 0, (int) R.string.ocr).setIcon(17301559);
        menu.add(0, 4, 1, (int) R.string.write_kanji).setIcon(17301566);
        menu.add(0, 5, 2, (int) R.string.favorites_hist).setIcon(17301578);
        menu.add(0, 3, 3, (int) R.string.settings).setIcon(17301577);
        menu.add(0, 1, 4, (int) R.string.about).setIcon(17301569);
        return true;
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                startActivity(new Intent(this, AboutActivity.class));
                return true;
            case 2:
                startActivity(new Intent(this, OcrActivity.class));
                return true;
            case 3:
                startActivity(new Intent(this, WwwjdicPreferences.class));
                return true;
            case 4:
                startActivity(new Intent(this, RecognizeKanjiActivity.class));
                return true;
            case 5:
                startActivity(new Intent(this, FavoritesAndHistory.class));
                return true;
            default:
                return super.onMenuItemSelected(featureId, item);
        }
    }

    /* access modifiers changed from: protected */
    public WwwjdicApplication getApp() {
        return (WwwjdicApplication) getApplication();
    }
}
