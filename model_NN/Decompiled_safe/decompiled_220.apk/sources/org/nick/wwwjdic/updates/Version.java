package org.nick.wwwjdic.updates;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Version implements Comparable<Version> {
    public static final String MARKET_AMAZON = "AmazonAppStore";
    public static final String MARKET_ANDROID = "AndroidMarket";
    private static final String TAG = Version.class.getSimpleName();
    private String downloadUrl;
    private String marketName;
    private String packageName;
    private int versionCode;
    private String versionName;

    public static Version getAppVersion(Context context, String marketName2) {
        Version result = new Version();
        result.marketName = marketName2;
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            result.packageName = info.packageName;
            result.versionCode = info.versionCode;
            result.versionName = info.versionName;
            return result;
        } catch (PackageManager.NameNotFoundException e) {
            Log.w(TAG, "Unable to obtain package info: ", e);
            return null;
        }
    }

    public String getPackageName() {
        return this.packageName;
    }

    public void setPackageName(String packageName2) {
        this.packageName = packageName2;
    }

    public String getMarketName() {
        return this.marketName;
    }

    public void setMarketName(String marketName2) {
        this.marketName = marketName2;
    }

    public int getVersionCode() {
        return this.versionCode;
    }

    public void setVersionCode(int versionCode2) {
        this.versionCode = versionCode2;
    }

    public String getVersionName() {
        return this.versionName;
    }

    public void setVersionName(String versionName2) {
        this.versionName = versionName2;
    }

    public String getDownloadUrl() {
        return this.downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl2) {
        this.downloadUrl = downloadUrl2;
    }

    public static Version fromJson(String jsonStr) {
        try {
            return fromJsonObj(new JSONObject(jsonStr));
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    private static Version fromJsonObj(JSONObject jsonObj) throws JSONException {
        Version result = new Version();
        result.packageName = jsonObj.getString("packageName");
        result.marketName = jsonObj.getString("marketName");
        result.versionCode = jsonObj.getInt("versionCode");
        result.versionName = jsonObj.getString("versionName");
        result.downloadUrl = jsonObj.getString("downloadUrl");
        return result;
    }

    public static List<Version> listFromJson(String jsonStr) {
        List<Version> result = new ArrayList<>();
        try {
            JSONArray jsonArr = new JSONArray(jsonStr);
            for (int i = 0; i < jsonArr.length(); i++) {
                result.add(fromJsonObj(jsonArr.getJSONObject(i)));
            }
            return result;
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public static List<Version> findMatching(Version version, List<Version> versions) {
        List<Version> result = new ArrayList<>();
        for (Version v : versions) {
            if (version.getMarketName().equals(v.getMarketName()) && version.getPackageName().equals(v.getPackageName())) {
                result.add(v);
            }
        }
        Collections.sort(result, Collections.reverseOrder());
        return result;
    }

    public int compareTo(Version rhs) {
        return this.versionCode - rhs.versionCode;
    }

    public String toString() {
        return String.format("version[%s: %s(%d)]", this.packageName, this.versionName, Integer.valueOf(this.versionCode));
    }
}
