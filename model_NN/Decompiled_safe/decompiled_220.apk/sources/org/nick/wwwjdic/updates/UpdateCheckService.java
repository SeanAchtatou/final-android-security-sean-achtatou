package org.nick.wwwjdic.updates;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.nick.wwwjdic.HttpClientFactory;
import org.nick.wwwjdic.R;
import org.nick.wwwjdic.WwwjdicPreferences;

public class UpdateCheckService extends IntentService {
    private static final String MARKET_NAME_KEY = "market-name";
    private static final String TAG = UpdateCheckService.class.getSimpleName();
    private static final String VERSIONS_URL_KEY = "versions-url";
    private HttpClient httpClient;
    private String marketName;
    private String versionsUrl;

    public static Intent createStartIntent(Context ctx, String versionsUrl2, String marketName2) {
        Intent intent = new Intent(ctx, UpdateCheckService.class);
        intent.putExtra(VERSIONS_URL_KEY, versionsUrl2);
        intent.putExtra(MARKET_NAME_KEY, marketName2);
        return intent;
    }

    public UpdateCheckService() {
        super("UpdateCheckService");
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        this.versionsUrl = extras.getString(VERSIONS_URL_KEY);
        this.marketName = extras.getString(MARKET_NAME_KEY);
        this.httpClient = HttpClientFactory.createWwwjdicHttpClient(WwwjdicPreferences.getWwwjdicTimeoutSeconds(this) * 1000);
        HttpGet get = new HttpGet(this.versionsUrl);
        try {
            Log.d(TAG, "getting latests versions info from " + this.versionsUrl);
            HttpResponse response = this.httpClient.execute(get);
            if (response.getStatusLine().getStatusCode() != 200) {
                Log.e(TAG, "error getting current versions: " + response.getStatusLine());
                if (response.getEntity() != null) {
                    response.getEntity().consumeContent();
                    return;
                }
                return;
            }
            List<Version> allVersions = Version.listFromJson(EntityUtils.toString(response.getEntity()));
            Log.d(TAG, String.format("got %d versions", Integer.valueOf(allVersions.size())));
            Version thisAppsVersion = Version.getAppVersion(this, this.marketName);
            if (thisAppsVersion != null) {
                List<Version> matchingVersions = Version.findMatching(thisAppsVersion, allVersions);
                if (matchingVersions.isEmpty()) {
                    Log.w(TAG, "no versions matching current market : " + this.marketName);
                    return;
                }
                Version latest = matchingVersions.get(0);
                if (latest.getVersionCode() > thisAppsVersion.getVersionCode()) {
                    showNotification(latest);
                } else {
                    Log.i(TAG, "already using latest version: " + thisAppsVersion);
                }
                WwwjdicPreferences.setLastUpdateCheck(this, System.currentTimeMillis());
            }
        } catch (Exception e) {
            Exception e2 = e;
            Log.e(TAG, "error checking for current versions: " + e2.getMessage(), e2);
        }
    }

    private void showNotification(Version latest) {
        CharSequence message = getResources().getString(R.string.update_available, latest.getVersionName());
        Notification notification = new Notification(R.drawable.icon_update, message, System.currentTimeMillis());
        notification.flags |= 16;
        notification.flags |= 8;
        notification.setLatestEventInfo(this, getResources().getString(R.string.app_name), message, PendingIntent.getActivity(this, 0, new Intent("android.intent.action.VIEW", Uri.parse(latest.getDownloadUrl())), 1073741824));
        ((NotificationManager) getSystemService("notification")).notify(latest.getPackageName().hashCode(), notification);
    }
}
