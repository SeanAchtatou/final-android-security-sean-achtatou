package org.nick.wwwjdic;

import java.io.Serializable;

public class WwwjdicQuery implements Serializable {
    private static final long serialVersionUID = -5227778690344003552L;
    protected String queryString;

    public WwwjdicQuery(String queryString2) {
        this.queryString = queryString2;
    }

    public String getQueryString() {
        return this.queryString;
    }
}
