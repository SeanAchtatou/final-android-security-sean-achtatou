package org.nick.wwwjdic;

import java.io.Serializable;

public class SearchCriteria extends WwwjdicQuery implements Serializable {
    public static final int CRITERIA_TYPE_DICT = 0;
    public static final int CRITERIA_TYPE_EXAMPLES = 2;
    public static final int CRITERIA_TYPE_KANJI = 1;
    private static final int DEFAULT_MAX_RESULTS = 20;
    private static final String DICTIONARY_CODE_GENERAL = "1";
    private static final String ENGLISH_MEANING_LOOKUP_CODE = "E";
    public static final int KANJI_COMPOUND_SEARCH_TYPE_ANY = 2;
    public static final int KANJI_COMPOUND_SEARCH_TYPE_NONE = 0;
    public static final int KANJI_COMPOUND_SEARCH_TYPE_STARTING = 1;
    private static final String KANJI_RADICAL_LOOKUP_CODE = "B";
    private static final String KANJI_TEXT_LOOKUP_CODE = "J";
    private static final long serialVersionUID = -6703864987202245997L;
    private String dictionaryCode;
    private Long id;
    private boolean isCommonWordsOnly;
    private boolean isExactMatch;
    private boolean isKanjiLookup;
    private boolean isRomanizedJapanese;
    private int kanjiCompoundSearchType;
    private String kanjiSearchType;
    private Integer maxStrokeCount;
    private Integer minStrokeCount;
    private Integer numMaxResults;
    private final int type;

    public static SearchCriteria createForDictionary(String queryString, boolean isExactMatch2, boolean isRomanized, boolean isCommonWordsOnly2, String dictionary) {
        return new SearchCriteria(0, queryString, isExactMatch2, false, isRomanized, isCommonWordsOnly2, 0, dictionary, null, null, null, null);
    }

    public static SearchCriteria createForDictionaryDefault(String queryString) {
        return createForDictionary(queryString, false, false, false, DICTIONARY_CODE_GENERAL);
    }

    public static SearchCriteria createForKanjiCompounds(String queryString, int compoundSearchType, boolean isCommonWordsOnly2, String dictionary) {
        return new SearchCriteria(0, queryString, false, false, false, isCommonWordsOnly2, compoundSearchType, dictionary, null, null, null, null);
    }

    public static SearchCriteria createForKanji(String queryString, String searchType) {
        return new SearchCriteria(1, queryString, false, true, true, false, 0, null, searchType, null, null, null);
    }

    public static SearchCriteria createForKanjiOrReading(String queryString) {
        return new SearchCriteria(1, queryString, false, true, true, false, 0, null, KANJI_TEXT_LOOKUP_CODE, null, null, null);
    }

    public static SearchCriteria createWithStrokeCount(String queryString, String searchType, Integer minStrokeCount2, Integer maxStrokeCount2) {
        return new SearchCriteria(1, queryString, false, true, true, false, 0, null, searchType, minStrokeCount2, maxStrokeCount2, null);
    }

    public static SearchCriteria createForExampleSearch(String queryString, boolean isExactMatch2, int numMaxResults2) {
        return new SearchCriteria(2, queryString, isExactMatch2, false, false, false, 0, null, null, null, null, Integer.valueOf(numMaxResults2));
    }

    public static SearchCriteria createForExampleSearchDefault(String queryString) {
        return createForExampleSearch(queryString, false, DEFAULT_MAX_RESULTS);
    }

    private SearchCriteria(int type2, String queryString, boolean isExactMatch2, boolean isKanjiLookup2, boolean isRomanizedJapanese2, boolean isCommonWordsOnly2, int kanjiCompoundSearchType2, String dictionary, String kanjiSearchType2, Integer minStrokeCount2, Integer maxStrokeCount2, Integer numMaxResults2) {
        super(queryString);
        this.type = type2;
        this.queryString = queryString;
        this.isExactMatch = isExactMatch2;
        this.isKanjiLookup = isKanjiLookup2;
        this.isRomanizedJapanese = isRomanizedJapanese2;
        this.isCommonWordsOnly = isCommonWordsOnly2;
        this.kanjiCompoundSearchType = kanjiCompoundSearchType2;
        this.dictionaryCode = dictionary;
        this.kanjiSearchType = kanjiSearchType2;
        this.minStrokeCount = minStrokeCount2;
        this.maxStrokeCount = maxStrokeCount2;
        this.numMaxResults = numMaxResults2;
    }

    public boolean isExactMatch() {
        return this.isExactMatch;
    }

    public boolean isKanjiLookup() {
        return this.isKanjiLookup;
    }

    public boolean isRomanizedJapanese() {
        return this.isRomanizedJapanese;
    }

    public boolean isCommonWordsOnly() {
        return this.isCommonWordsOnly;
    }

    public String getDictionaryCode() {
        return this.dictionaryCode;
    }

    public String getKanjiSearchType() {
        return this.kanjiSearchType;
    }

    public boolean isKanjiCodeLookup() {
        return !KANJI_TEXT_LOOKUP_CODE.equals(this.kanjiSearchType) && !ENGLISH_MEANING_LOOKUP_CODE.equals(this.kanjiSearchType);
    }

    public boolean isKanjiRadicalLookup() {
        return KANJI_RADICAL_LOOKUP_CODE.equals(this.kanjiSearchType);
    }

    public Integer getMinStrokeCount() {
        return this.minStrokeCount;
    }

    public Integer getMaxStrokeCount() {
        return this.maxStrokeCount;
    }

    public boolean hasStrokes() {
        return (this.minStrokeCount == null && this.maxStrokeCount == null) ? false : true;
    }

    public boolean hasMinStrokes() {
        return this.minStrokeCount != null;
    }

    public boolean hasMaxStrokes() {
        return this.maxStrokeCount != null;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id2) {
        this.id = id2;
    }

    public boolean isNarrowedDown() {
        return this.isExactMatch || this.isCommonWordsOnly;
    }

    public Integer getNumMaxResults() {
        return this.numMaxResults;
    }

    public int getType() {
        return this.type;
    }

    public boolean isKanjiCompoundSearch() {
        return this.kanjiCompoundSearchType != 0;
    }

    public int getKanjiCompoundSearchType() {
        return this.kanjiCompoundSearchType;
    }

    public boolean isStartingKanjiCompoundSearch() {
        return this.kanjiCompoundSearchType == 1;
    }
}
