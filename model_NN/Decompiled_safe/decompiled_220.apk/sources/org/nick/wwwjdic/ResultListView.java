package org.nick.wwwjdic;

import java.util.List;

public interface ResultListView<T> {
    void setError(Exception exc);

    void setResult(List<T> list);
}
