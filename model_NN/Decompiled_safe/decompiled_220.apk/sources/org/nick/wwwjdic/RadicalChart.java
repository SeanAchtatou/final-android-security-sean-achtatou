package org.nick.wwwjdic;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

public class RadicalChart extends Activity implements AdapterView.OnItemClickListener {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.radical_chart);
        ((GridView) findViewById(R.id.radicalChartGrid)).setOnItemClickListener(this);
        ((GridView) findViewById(R.id.radicalChartGrid)).setAdapter((ListAdapter) new RadicalAdapter(this, Radicals.getInstance()));
        setTitle((int) R.string.select_radical);
    }

    private static class RadicalAdapter extends BaseAdapter {
        private Context context;
        private Radicals radicals;

        public RadicalAdapter(Context context2, Radicals radicals2) {
            this.context = context2;
            this.radicals = radicals2;
        }

        public int getCount() {
            return this.radicals.getRadicals().size();
        }

        public Object getItem(int position) {
            return this.radicals.getRadicals().get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            Radical radical = this.radicals.getRadical(position);
            LinearLayout radicalLayout = new LinearLayout(this.context);
            radicalLayout.setOrientation(0);
            LinearLayout numberStrokesLayout = new LinearLayout(this.context);
            numberStrokesLayout.setOrientation(1);
            numberStrokesLayout.setLayoutParams(new ViewGroup.LayoutParams(-2, -1));
            TextView numberText = new TextView(this.context);
            numberText.setText(Integer.toString(radical.getNumber()));
            numberText.setTextSize(12.0f);
            numberStrokesLayout.addView(numberText);
            TextView numStrokesText = new TextView(this.context);
            numStrokesText.setText(Integer.toString(radical.getNumStrokes()));
            numStrokesText.setTextSize(12.0f);
            numberStrokesLayout.addView(numStrokesText);
            radicalLayout.addView(numberStrokesLayout);
            TextView glyphText = new TextView(this.context);
            glyphText.setText(radical.getGlyph());
            glyphText.setTextSize(34.0f);
            glyphText.setTextColor(-1);
            radicalLayout.addView(glyphText);
            return radicalLayout;
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Radical radical = Radicals.getInstance().getRadical(position);
        Intent resultIntent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.RADICAL_KEY, radical);
        resultIntent.putExtras(bundle);
        setResult(-1, resultIntent);
        finish();
    }
}
