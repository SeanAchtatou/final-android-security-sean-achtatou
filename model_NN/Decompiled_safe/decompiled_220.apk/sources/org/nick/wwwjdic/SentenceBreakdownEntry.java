package org.nick.wwwjdic;

public class SentenceBreakdownEntry {
    private String explanation;
    private String inflectedForm;
    private String reading;
    private String translation;
    private String word;

    public static SentenceBreakdownEntry create(String inflectedForm2, String word2, String reading2, String translation2) {
        return new SentenceBreakdownEntry(inflectedForm2, word2, reading2, translation2, null);
    }

    public static SentenceBreakdownEntry createNoReading(String inflectedForm2, String word2, String translation2) {
        return new SentenceBreakdownEntry(inflectedForm2, word2, null, translation2, null);
    }

    public static SentenceBreakdownEntry createWithExplanation(String inflectedForm2, String word2, String reading2, String translation2, String explanation2) {
        return new SentenceBreakdownEntry(inflectedForm2, word2, reading2, translation2, explanation2);
    }

    private SentenceBreakdownEntry(String inflectedForm2, String word2, String reading2, String translation2, String explanation2) {
        this.inflectedForm = inflectedForm2;
        this.word = word2;
        this.reading = reading2;
        this.translation = translation2;
        this.explanation = explanation2;
    }

    public String getInflectedForm() {
        return this.inflectedForm;
    }

    public String getWord() {
        return this.word;
    }

    public String getReading() {
        return this.reading;
    }

    public String getTranslation() {
        return this.translation;
    }

    public String getExplanation() {
        return this.explanation;
    }
}
