package org.acra.sender;

import android.content.Context;
import android.content.Intent;
import org.acra.ACRA;
import org.acra.CrashReportData;
import org.acra.ReportField;

public class EmailIntentSender implements ReportSender {
    Context mContext = null;

    public EmailIntentSender(Context ctx) {
        this.mContext = ctx;
    }

    public void send(CrashReportData errorContent) throws ReportSenderException {
        Intent emailIntent = new Intent("android.intent.action.SEND");
        emailIntent.addFlags(268435456);
        emailIntent.setType("text/plain");
        String body = buildBody(errorContent);
        emailIntent.putExtra("android.intent.extra.SUBJECT", ((String) errorContent.get(ReportField.PACKAGE_NAME)) + " Crash Report");
        emailIntent.putExtra("android.intent.extra.TEXT", body);
        emailIntent.putExtra("android.intent.extra.EMAIL", new String[]{ACRA.getConfig().mailTo()});
        this.mContext.startActivity(emailIntent);
    }

    private String buildBody(CrashReportData errorContent) {
        StringBuilder builder = new StringBuilder();
        ReportField[] fields = ACRA.getConfig().customReportContent();
        if (fields.length == 0) {
            fields = ACRA.DEFAULT_MAIL_REPORT_FIELDS;
        }
        for (ReportField field : fields) {
            builder.append(field.toString()).append("=");
            builder.append((String) errorContent.get(field));
            builder.append(10);
        }
        return builder.toString();
    }
}
