package org.acra.sender;

import org.acra.CrashReportData;

public interface ReportSender {
    void send(CrashReportData crashReportData) throws ReportSenderException;
}
