package org.acra.sender;

import android.net.Uri;
import android.util.Log;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import org.acra.ACRA;
import org.acra.CrashReportData;
import org.acra.ReportField;
import org.acra.util.HttpUtils;

public class HttpPostSender implements ReportSender {
    private Uri mFormUri = null;
    private Map<ReportField, String> mMapping = null;

    public HttpPostSender(String formUri, Map<ReportField, String> mapping) {
        this.mFormUri = Uri.parse(formUri);
        this.mMapping = mapping;
    }

    public void send(CrashReportData report) throws ReportSenderException {
        try {
            Map<String, String> finalReport = remap(report);
            URL reportUrl = new URL(this.mFormUri.toString());
            Log.d(ACRA.LOG_TAG, "Connect to " + reportUrl.toString());
            HttpUtils.doPost(finalReport, reportUrl, ACRA.getConfig().formUriBasicAuthLogin(), ACRA.getConfig().formUriBasicAuthPassword());
        } catch (Exception e) {
            throw new ReportSenderException("Error while sending report to Http Post Form.", e);
        }
    }

    private Map<String, String> remap(Map<ReportField, String> report) {
        Map<String, String> finalReport = new HashMap<>(report.size());
        ReportField[] fields = ACRA.getConfig().customReportContent();
        if (fields.length == 0) {
            fields = ACRA.DEFAULT_REPORT_FIELDS;
        }
        for (ReportField field : fields) {
            if (this.mMapping == null || this.mMapping.get(field) == null) {
                finalReport.put(field.toString(), report.get(field));
            } else {
                finalReport.put(this.mMapping.get(field), report.get(field));
            }
        }
        return finalReport;
    }
}
