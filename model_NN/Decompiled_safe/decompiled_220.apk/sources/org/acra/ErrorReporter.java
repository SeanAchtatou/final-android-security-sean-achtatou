package org.acra;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.os.Looper;
import android.os.PowerManager;
import android.os.Process;
import android.os.StatFs;
import android.telephony.TelephonyManager;
import android.text.format.Time;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.widget.Toast;
import au.com.bytecode.opencsv.CSVWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.acra.annotation.ReportsCrashes;
import org.acra.sender.ReportSender;
import org.acra.sender.ReportSenderException;
import org.acra.util.Installation;

public class ErrorReporter implements Thread.UncaughtExceptionHandler {
    static final String APPROVED_SUFFIX = "-approved";
    static final String EXTRA_REPORT_FILE_NAME = "REPORT_FILE_NAME";
    private static final int MAX_SEND_REPORTS = 5;
    public static final String REPORTFILE_EXTENSION = ".stacktrace";
    static final String SILENT_SUFFIX = ("-" + ReportField.IS_SILENT);
    private static boolean enabled = false;
    /* access modifiers changed from: private */
    public static Context mContext;
    private static CrashReportData mCrashProperties = new CrashReportData();
    private static ErrorReporter mInstanceSingleton;
    private static ArrayList<ReportSender> mReportSenders = new ArrayList<>();
    Map<String, String> mCustomParameters = new HashMap();
    private Thread.UncaughtExceptionHandler mDfltExceptionHandler;
    private String mInitialConfiguration;
    private ReportingInteractionMode mReportingInteractionMode = ReportingInteractionMode.SILENT;

    final class ReportsSenderWorker extends Thread {
        private boolean mApprovePendingReports = false;
        private String mCommentedReportFileName = null;
        private boolean mSendOnlySilentReports = false;
        private String mUserComment = null;
        private String mUserEmail = null;

        public ReportsSenderWorker(boolean sendOnlySilentReports) {
            this.mSendOnlySilentReports = sendOnlySilentReports;
        }

        public ReportsSenderWorker() {
        }

        public void run() {
            PowerManager.WakeLock wakeLock = acquireWakeLock();
            try {
                if (this.mApprovePendingReports) {
                    ErrorReporter.this.approvePendingReports();
                    this.mCommentedReportFileName = this.mCommentedReportFileName.replace(ErrorReporter.REPORTFILE_EXTENSION, "-approved.stacktrace");
                }
                ErrorReporter.addUserDataToReport(ErrorReporter.mContext, this.mCommentedReportFileName, this.mUserComment, this.mUserEmail);
                ErrorReporter.this.checkAndSendReports(ErrorReporter.mContext, this.mSendOnlySilentReports);
            } finally {
                if (wakeLock != null) {
                    wakeLock.release();
                }
            }
        }

        private PowerManager.WakeLock acquireWakeLock() {
            PackageManager pm = ErrorReporter.mContext.getPackageManager();
            if (!(pm != null ? pm.checkPermission("android.permission.WAKE_LOCK", ErrorReporter.mContext.getPackageName()) == 0 : false)) {
                return null;
            }
            PowerManager.WakeLock wakeLock = ((PowerManager) ErrorReporter.mContext.getSystemService("power")).newWakeLock(1, "ACRA wakelock");
            wakeLock.acquire();
            return wakeLock;
        }

        /* access modifiers changed from: package-private */
        public void setUserComment(String reportFileName, String userComment) {
            this.mCommentedReportFileName = reportFileName;
            this.mUserComment = userComment;
        }

        /* access modifiers changed from: package-private */
        public void setUserEmail(String reportFileName, String userEmail) {
            this.mCommentedReportFileName = reportFileName;
            this.mUserEmail = userEmail;
        }

        public void setApprovePendingReports() {
            this.mApprovePendingReports = true;
        }
    }

    public void approvePendingReports() {
        Log.d(ACRA.LOG_TAG, "Mark all pending reports as approved.");
        for (String reportFileName : getCrashReportFilesList()) {
            if (!isApproved(reportFileName)) {
                new File(mContext.getFilesDir(), reportFileName).renameTo(new File(mContext.getFilesDir(), reportFileName.replace(REPORTFILE_EXTENSION, "-approved.stacktrace")));
            }
        }
    }

    @Deprecated
    public void addCustomData(String key, String value) {
        this.mCustomParameters.put(key, value);
    }

    public String putCustomData(String key, String value) {
        return this.mCustomParameters.put(key, value);
    }

    public String removeCustomData(String key) {
        return this.mCustomParameters.remove(key);
    }

    public String getCustomData(String key) {
        return this.mCustomParameters.get(key);
    }

    private String createCustomInfoString() {
        String CustomInfo = "";
        for (String CurrentKey : this.mCustomParameters.keySet()) {
            CustomInfo = CustomInfo + CurrentKey + " = " + this.mCustomParameters.get(CurrentKey) + CSVWriter.DEFAULT_LINE_END;
        }
        return CustomInfo;
    }

    public static ErrorReporter getInstance() {
        if (mInstanceSingleton == null) {
            mInstanceSingleton = new ErrorReporter();
        }
        return mInstanceSingleton;
    }

    public void init(Context context) {
        if (this.mDfltExceptionHandler == null) {
            this.mDfltExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
            enabled = true;
            Thread.setDefaultUncaughtExceptionHandler(this);
            mContext = context;
            this.mInitialConfiguration = ConfigurationInspector.toString(mContext.getResources().getConfiguration());
        }
    }

    private static long getAvailableInternalMemorySize() {
        StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
        return ((long) stat.getAvailableBlocks()) * ((long) stat.getBlockSize());
    }

    private static long getTotalInternalMemorySize() {
        StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
        return ((long) stat.getBlockCount()) * ((long) stat.getBlockSize());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
     arg types: [org.acra.ReportField, java.lang.String]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Object, java.lang.Object):java.lang.Object}
      ClspMth{java.util.AbstractMap.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V} */
    private void retrieveCrashData(Context context) {
        String deviceId;
        try {
            ReportsCrashes config = ACRA.getConfig();
            ReportField[] fields = config.customReportContent();
            if (fields.length == 0) {
                if (config.mailTo() == null || "".equals(config.mailTo())) {
                    fields = ACRA.DEFAULT_REPORT_FIELDS;
                } else if (!"".equals(config.mailTo())) {
                    fields = ACRA.DEFAULT_MAIL_REPORT_FIELDS;
                }
            }
            List<ReportField> fieldsList = Arrays.asList(fields);
            SharedPreferences prefs = ACRA.getACRASharedPreferences();
            if (fieldsList.contains(ReportField.REPORT_ID)) {
                mCrashProperties.put((Enum) ReportField.REPORT_ID, (Object) UUID.randomUUID().toString());
            }
            if (fieldsList.contains(ReportField.DUMPSYS_MEMINFO)) {
                mCrashProperties.put((Enum) ReportField.DUMPSYS_MEMINFO, (Object) DumpSysCollector.collectMemInfo());
            }
            PackageManager pm = context.getPackageManager();
            if (pm != null) {
                if (!prefs.getBoolean(ACRA.PREF_ENABLE_SYSTEM_LOGS, true) || pm.checkPermission("android.permission.READ_LOGS", context.getPackageName()) != 0) {
                    Log.i(ACRA.LOG_TAG, "READ_LOGS not allowed. ACRA will not include LogCat and DropBox data.");
                } else {
                    Log.i(ACRA.LOG_TAG, "READ_LOGS granted! ACRA can include LogCat and DropBox data.");
                    if (fieldsList.contains(ReportField.LOGCAT)) {
                        mCrashProperties.put((Enum) ReportField.LOGCAT, (Object) LogCatCollector.collectLogCat(null).toString());
                    }
                    if (fieldsList.contains(ReportField.EVENTSLOG)) {
                        mCrashProperties.put((Enum) ReportField.EVENTSLOG, (Object) LogCatCollector.collectLogCat("events").toString());
                    }
                    if (fieldsList.contains(ReportField.RADIOLOG)) {
                        mCrashProperties.put((Enum) ReportField.RADIOLOG, (Object) LogCatCollector.collectLogCat("radio").toString());
                    }
                    if (fieldsList.contains(ReportField.DROPBOX)) {
                        mCrashProperties.put((Enum) ReportField.DROPBOX, (Object) DropBoxCollector.read(mContext, ACRA.getConfig().additionalDropBoxTags()));
                    }
                }
                if (fieldsList.contains(ReportField.DEVICE_ID) && prefs.getBoolean(ACRA.PREF_ENABLE_DEVICE_ID, true) && pm.checkPermission("android.permission.READ_PHONE_STATE", context.getPackageName()) == 0 && (deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId()) != null) {
                    mCrashProperties.put((Enum) ReportField.DEVICE_ID, (Object) deviceId);
                }
            }
            if (fieldsList.contains(ReportField.INSTALLATION_ID)) {
                mCrashProperties.put((Enum) ReportField.INSTALLATION_ID, (Object) Installation.id(mContext));
            }
            if (fieldsList.contains(ReportField.INITIAL_CONFIGURATION)) {
                mCrashProperties.put((Enum) ReportField.INITIAL_CONFIGURATION, (Object) this.mInitialConfiguration);
            }
            if (fieldsList.contains(ReportField.CRASH_CONFIGURATION)) {
                mCrashProperties.put((Enum) ReportField.CRASH_CONFIGURATION, (Object) ConfigurationInspector.toString(context.getResources().getConfiguration()));
            }
            PackageInfo pi = pm.getPackageInfo(context.getPackageName(), 0);
            if (pi != null) {
                if (fieldsList.contains(ReportField.APP_VERSION_CODE)) {
                    mCrashProperties.put((Enum) ReportField.APP_VERSION_CODE, (Object) Integer.toString(pi.versionCode));
                }
                if (fieldsList.contains(ReportField.APP_VERSION_NAME)) {
                    mCrashProperties.put((Enum) ReportField.APP_VERSION_NAME, (Object) (pi.versionName != null ? pi.versionName : "not set"));
                }
            } else {
                mCrashProperties.put((Enum) ReportField.APP_VERSION_NAME, (Object) "Package info unavailable");
            }
            if (fieldsList.contains(ReportField.PACKAGE_NAME)) {
                mCrashProperties.put((Enum) ReportField.PACKAGE_NAME, (Object) context.getPackageName());
            }
            if (fieldsList.contains(ReportField.BUILD)) {
                mCrashProperties.put((Enum) ReportField.BUILD, (Object) ReflectionCollector.collectConstants(Build.class));
            }
            if (fieldsList.contains(ReportField.PHONE_MODEL)) {
                mCrashProperties.put((Enum) ReportField.PHONE_MODEL, (Object) Build.MODEL);
            }
            if (fieldsList.contains(ReportField.ANDROID_VERSION)) {
                mCrashProperties.put((Enum) ReportField.ANDROID_VERSION, (Object) Build.VERSION.RELEASE);
            }
            if (fieldsList.contains(ReportField.BRAND)) {
                mCrashProperties.put((Enum) ReportField.BRAND, (Object) Build.BRAND);
            }
            if (fieldsList.contains(ReportField.PRODUCT)) {
                mCrashProperties.put((Enum) ReportField.PRODUCT, (Object) Build.PRODUCT);
            }
            if (fieldsList.contains(ReportField.TOTAL_MEM_SIZE)) {
                mCrashProperties.put((Enum) ReportField.TOTAL_MEM_SIZE, (Object) Long.toString(getTotalInternalMemorySize()));
            }
            if (fieldsList.contains(ReportField.AVAILABLE_MEM_SIZE)) {
                mCrashProperties.put((Enum) ReportField.AVAILABLE_MEM_SIZE, (Object) Long.toString(getAvailableInternalMemorySize()));
            }
            if (fieldsList.contains(ReportField.FILE_PATH)) {
                mCrashProperties.put((Enum) ReportField.FILE_PATH, (Object) context.getFilesDir().getAbsolutePath());
            }
            if (fieldsList.contains(ReportField.DISPLAY)) {
                mCrashProperties.put((Enum) ReportField.DISPLAY, (Object) toString(((WindowManager) context.getSystemService("window")).getDefaultDisplay()));
            }
            if (fieldsList.contains(ReportField.USER_CRASH_DATE)) {
                Time curDate = new Time();
                curDate.setToNow();
                mCrashProperties.put((Enum) ReportField.USER_CRASH_DATE, (Object) curDate.format3339(false));
            }
            if (fieldsList.contains(ReportField.CUSTOM_DATA)) {
                mCrashProperties.put((Enum) ReportField.CUSTOM_DATA, (Object) createCustomInfoString());
            }
            if (fieldsList.contains(ReportField.USER_EMAIL)) {
                mCrashProperties.put((Enum) ReportField.USER_EMAIL, (Object) prefs.getString(ACRA.PREF_USER_EMAIL_ADDRESS, "N/A"));
            }
            if (fieldsList.contains(ReportField.DEVICE_FEATURES)) {
                mCrashProperties.put((Enum) ReportField.DEVICE_FEATURES, (Object) DeviceFeaturesCollector.getFeatures(context));
            }
            if (fieldsList.contains(ReportField.ENVIRONMENT)) {
                mCrashProperties.put((Enum) ReportField.ENVIRONMENT, (Object) ReflectionCollector.collectStaticGettersResults(Environment.class));
            }
            if (fieldsList.contains(ReportField.SETTINGS_SYSTEM)) {
                mCrashProperties.put((Enum) ReportField.SETTINGS_SYSTEM, (Object) SettingsCollector.collectSystemSettings(mContext));
            }
            if (fieldsList.contains(ReportField.SETTINGS_SECURE)) {
                mCrashProperties.put((Enum) ReportField.SETTINGS_SECURE, (Object) SettingsCollector.collectSecureSettings(mContext));
            }
            if (fieldsList.contains(ReportField.SHARED_PREFERENCES)) {
                mCrashProperties.put((Enum) ReportField.SHARED_PREFERENCES, (Object) SharedPreferencesCollector.collect(mContext));
            }
        } catch (Exception e) {
            Log.e(ACRA.LOG_TAG, "Error while retrieving crash data", e);
        }
    }

    private static String toString(Display display) {
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        StringBuilder result = new StringBuilder();
        result.append("width=").append(display.getWidth()).append(10).append("height=").append(display.getHeight()).append(10).append("pixelFormat=").append(display.getPixelFormat()).append(10).append("refreshRate=").append(display.getRefreshRate()).append("fps").append(10).append("metrics.density=x").append(metrics.density).append(10).append("metrics.scaledDensity=x").append(metrics.scaledDensity).append(10).append("metrics.widthPixels=").append(metrics.widthPixels).append(10).append("metrics.heightPixels=").append(metrics.heightPixels).append(10).append("metrics.xdpi=").append(metrics.xdpi).append(10).append("metrics.ydpi=").append(metrics.ydpi);
        return result.toString();
    }

    public void uncaughtException(Thread t, Throwable e) {
        Log.e(ACRA.LOG_TAG, "ACRA caught a " + e.getClass().getSimpleName() + " exception for " + mContext.getPackageName() + ". Building report.");
        mCrashProperties.remove(ReportField.IS_SILENT);
        ReportsSenderWorker worker = handleException(e);
        if (this.mReportingInteractionMode == ReportingInteractionMode.TOAST) {
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e1) {
                Log.e(ACRA.LOG_TAG, "Error : ", e1);
            }
        }
        if (worker != null) {
            while (worker.isAlive()) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e12) {
                    Log.e(ACRA.LOG_TAG, "Error : ", e12);
                }
            }
        }
        if (this.mReportingInteractionMode == ReportingInteractionMode.SILENT || (this.mReportingInteractionMode == ReportingInteractionMode.TOAST && ACRA.getConfig().forceCloseDialogAfterToast())) {
            this.mDfltExceptionHandler.uncaughtException(t, e);
            return;
        }
        try {
            Log.e(ACRA.LOG_TAG, ((Object) mContext.getPackageManager().getApplicationInfo(mContext.getPackageName(), 0).loadLabel(mContext.getPackageManager())) + " fatal error : " + e.getMessage(), e);
        } catch (PackageManager.NameNotFoundException e2) {
            Log.e(ACRA.LOG_TAG, "Error : ", e2);
        } finally {
            Process.killProcess(Process.myPid());
            System.exit(10);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
     arg types: [org.acra.ReportField, java.lang.String]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Object, java.lang.Object):java.lang.Object}
      ClspMth{java.util.AbstractMap.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V} */
    /* access modifiers changed from: package-private */
    public ReportsSenderWorker handleException(Throwable e, ReportingInteractionMode reportingInteractionMode) {
        boolean sendOnlySilentReports = false;
        if (reportingInteractionMode == null) {
            reportingInteractionMode = this.mReportingInteractionMode;
        } else if (reportingInteractionMode == ReportingInteractionMode.SILENT && this.mReportingInteractionMode != ReportingInteractionMode.SILENT) {
            sendOnlySilentReports = true;
        }
        if (e == null) {
            e = new Exception("Report requested by developer");
        }
        if (reportingInteractionMode == ReportingInteractionMode.TOAST || (reportingInteractionMode == ReportingInteractionMode.NOTIFICATION && ACRA.getConfig().resToastText() != 0)) {
            new Thread() {
                public void run() {
                    Looper.prepare();
                    Toast.makeText(ErrorReporter.mContext, ACRA.getConfig().resToastText(), 1).show();
                    Looper.loop();
                }
            }.start();
        }
        retrieveCrashData(mContext);
        Writer result = new StringWriter();
        PrintWriter printWriter = new PrintWriter(result);
        e.printStackTrace(printWriter);
        Log.getStackTraceString(e);
        for (Throwable cause = e.getCause(); cause != null; cause = cause.getCause()) {
            cause.printStackTrace(printWriter);
        }
        mCrashProperties.put((Enum) ReportField.STACK_TRACE, (Object) result.toString());
        printWriter.close();
        String reportFileName = saveCrashReportFile(null, null);
        mCrashProperties.remove(ReportField.IS_SILENT);
        mCrashProperties.remove(ReportField.USER_COMMENT);
        if (reportingInteractionMode == ReportingInteractionMode.SILENT || reportingInteractionMode == ReportingInteractionMode.TOAST || ACRA.getACRASharedPreferences().getBoolean(ACRA.PREF_ALWAYS_ACCEPT, false)) {
            approvePendingReports();
            ReportsSenderWorker wk = new ReportsSenderWorker(sendOnlySilentReports);
            Log.v(ACRA.LOG_TAG, "About to start ReportSenderWorker from #handleException");
            wk.start();
            return wk;
        }
        if (reportingInteractionMode == ReportingInteractionMode.NOTIFICATION) {
            notifySendReport(reportFileName);
        }
        return null;
    }

    public ReportsSenderWorker handleException(Throwable e) {
        return handleException(e, this.mReportingInteractionMode);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
     arg types: [org.acra.ReportField, java.lang.String]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Object, java.lang.Object):java.lang.Object}
      ClspMth{java.util.AbstractMap.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V} */
    public Thread handleSilentException(Throwable e) {
        if (enabled) {
            mCrashProperties.put((Enum) ReportField.IS_SILENT, (Object) "true");
            return handleException(e, ReportingInteractionMode.SILENT);
        }
        Log.d(ACRA.LOG_TAG, "ACRA is disabled. Silent report not sent.");
        return null;
    }

    /* access modifiers changed from: package-private */
    public void notifySendReport(String reportFileName) {
        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService("notification");
        ReportsCrashes conf = ACRA.getConfig();
        Notification notification = new Notification(conf.resNotifIcon(), mContext.getText(conf.resNotifTickerText()), System.currentTimeMillis());
        CharSequence contentTitle = mContext.getText(conf.resNotifTitle());
        CharSequence contentText = mContext.getText(conf.resNotifText());
        Intent notificationIntent = new Intent(mContext, CrashReportDialog.class);
        Log.d(ACRA.LOG_TAG, "Creating Notification for " + reportFileName);
        notificationIntent.putExtra(EXTRA_REPORT_FILE_NAME, reportFileName);
        notification.setLatestEventInfo(mContext, contentTitle, contentText, PendingIntent.getActivity(mContext, 0, notificationIntent, 134217728));
        notificationManager.cancelAll();
        notificationManager.notify(666, notification);
    }

    private static void sendCrashReport(Context context, CrashReportData errorContent) throws ReportSenderException {
        boolean sentAtLeastOnce = false;
        Iterator i$ = mReportSenders.iterator();
        while (i$.hasNext()) {
            ReportSender sender = i$.next();
            try {
                sender.send(errorContent);
                sentAtLeastOnce = true;
            } catch (ReportSenderException e) {
                if (!sentAtLeastOnce) {
                    throw e;
                }
                Log.w(ACRA.LOG_TAG, "ReportSender of class " + sender.getClass().getName() + " failed but other senders completed their task. ACRA will not send this report again.");
            }
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String saveCrashReportFile(java.lang.String r8, org.acra.CrashReportData r9) {
        /*
            java.lang.String r6 = org.acra.ACRA.LOG_TAG     // Catch:{ Exception -> 0x005a }
            java.lang.String r7 = "Writing crash report file."
            android.util.Log.d(r6, r7)     // Catch:{ Exception -> 0x005a }
            if (r9 != 0) goto L_0x000b
            org.acra.CrashReportData r9 = org.acra.ErrorReporter.mCrashProperties     // Catch:{ Exception -> 0x005a }
        L_0x000b:
            if (r8 != 0) goto L_0x0041
            android.text.format.Time r2 = new android.text.format.Time     // Catch:{ Exception -> 0x005a }
            r2.<init>()     // Catch:{ Exception -> 0x005a }
            r2.setToNow()     // Catch:{ Exception -> 0x005a }
            r6 = 0
            long r4 = r2.toMillis(r6)     // Catch:{ Exception -> 0x005a }
            org.acra.ReportField r6 = org.acra.ReportField.IS_SILENT     // Catch:{ Exception -> 0x005a }
            java.lang.String r1 = r9.getProperty(r6)     // Catch:{ Exception -> 0x005a }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x005a }
            r6.<init>()     // Catch:{ Exception -> 0x005a }
            java.lang.String r7 = ""
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x005a }
            java.lang.StringBuilder r6 = r6.append(r4)     // Catch:{ Exception -> 0x005a }
            if (r1 == 0) goto L_0x0052
            java.lang.String r7 = org.acra.ErrorReporter.SILENT_SUFFIX     // Catch:{ Exception -> 0x005a }
        L_0x0033:
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x005a }
            java.lang.String r7 = ".stacktrace"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x005a }
            java.lang.String r8 = r6.toString()     // Catch:{ Exception -> 0x005a }
        L_0x0041:
            android.content.Context r6 = org.acra.ErrorReporter.mContext     // Catch:{ Exception -> 0x005a }
            r7 = 0
            java.io.FileOutputStream r3 = r6.openFileOutput(r8, r7)     // Catch:{ Exception -> 0x005a }
            java.lang.String r6 = ""
            r9.store(r3, r6)     // Catch:{ all -> 0x0055 }
            r3.close()     // Catch:{ Exception -> 0x005a }
            r6 = r8
        L_0x0051:
            return r6
        L_0x0052:
            java.lang.String r7 = ""
            goto L_0x0033
        L_0x0055:
            r6 = move-exception
            r3.close()     // Catch:{ Exception -> 0x005a }
            throw r6     // Catch:{ Exception -> 0x005a }
        L_0x005a:
            r6 = move-exception
            r0 = r6
            java.lang.String r6 = org.acra.ACRA.LOG_TAG
            java.lang.String r7 = "An error occured while writing the report file..."
            android.util.Log.e(r6, r7, r0)
            r6 = 0
            goto L_0x0051
        */
        throw new UnsupportedOperationException("Method not decompiled: org.acra.ErrorReporter.saveCrashReportFile(java.lang.String, org.acra.CrashReportData):java.lang.String");
    }

    /* access modifiers changed from: package-private */
    public String[] getCrashReportFilesList() {
        if (mContext == null) {
            Log.e(ACRA.LOG_TAG, "Trying to get ACRA reports but ACRA is not initialized.");
            return new String[0];
        }
        File dir = mContext.getFilesDir();
        if (dir != null) {
            Log.d(ACRA.LOG_TAG, "Looking for error files in " + dir.getAbsolutePath());
            String[] result = dir.list(new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return name.endsWith(ErrorReporter.REPORTFILE_EXTENSION);
                }
            });
            return result == null ? new String[0] : result;
        }
        Log.w(ACRA.LOG_TAG, "Application files directory does not exist! The application may not be installed correctly. Please try reinstalling.");
        return new String[0];
    }

    /* access modifiers changed from: package-private */
    public synchronized void checkAndSendReports(Context context, boolean sendOnlySilentReports) {
        Log.d(ACRA.LOG_TAG, "#checkAndSendReports - start");
        String[] reportFiles = getCrashReportFilesList();
        Arrays.sort(reportFiles);
        int reportsSentCount = 0;
        for (String curFileName : reportFiles) {
            if (!sendOnlySilentReports || isSilent(curFileName)) {
                if (reportsSentCount >= 5) {
                    break;
                }
                Log.i(ACRA.LOG_TAG, "Sending file " + curFileName);
                try {
                    sendCrashReport(context, loadCrashReport(context, curFileName));
                    deleteFile(context, curFileName);
                    reportsSentCount++;
                } catch (RuntimeException e) {
                    Log.e(ACRA.LOG_TAG, "Failed to send crash reports", e);
                    deleteFile(context, curFileName);
                } catch (IOException e2) {
                    Log.e(ACRA.LOG_TAG, "Failed to load crash report for " + curFileName, e2);
                    deleteFile(context, curFileName);
                } catch (ReportSenderException e3) {
                    Log.e(ACRA.LOG_TAG, "Failed to send crash report for " + curFileName, e3);
                }
            }
        }
        Log.d(ACRA.LOG_TAG, "#checkAndSendReports - finish");
        return;
    }

    private CrashReportData loadCrashReport(Context context, String fileName) throws IOException {
        CrashReportData crashReport = new CrashReportData();
        FileInputStream input = context.openFileInput(fileName);
        try {
            crashReport.load(input);
            return crashReport;
        } finally {
            input.close();
        }
    }

    private void deleteFile(Context context, String fileName) {
        if (!context.deleteFile(fileName)) {
            Log.w(ACRA.LOG_TAG, "Could not deleted error report : " + fileName);
        }
    }

    /* access modifiers changed from: package-private */
    public void setReportingInteractionMode(ReportingInteractionMode reportingInteractionMode) {
        this.mReportingInteractionMode = reportingInteractionMode;
    }

    public void checkReportsOnApplicationStart() {
        String[] filesList = getCrashReportFilesList();
        if (filesList != null && filesList.length > 0) {
            boolean onlySilentOrApprovedReports = containsOnlySilentOrApprovedReports(filesList);
            if (this.mReportingInteractionMode == ReportingInteractionMode.SILENT || this.mReportingInteractionMode == ReportingInteractionMode.TOAST || (this.mReportingInteractionMode == ReportingInteractionMode.NOTIFICATION && onlySilentOrApprovedReports)) {
                if (this.mReportingInteractionMode == ReportingInteractionMode.TOAST && !onlySilentOrApprovedReports) {
                    Toast.makeText(mContext, ACRA.getConfig().resToastText(), 1).show();
                }
                Log.v(ACRA.LOG_TAG, "About to start ReportSenderWorker from #checkReportOnApplicationStart");
                new ReportsSenderWorker().start();
            } else if (ACRA.getConfig().deleteUnapprovedReportsOnApplicationStart()) {
                getInstance().deletePendingNonApprovedReports();
            } else {
                getInstance().notifySendReport(getLatestNonSilentReport(filesList));
            }
        }
    }

    private String getLatestNonSilentReport(String[] filesList) {
        if (filesList == null || filesList.length <= 0) {
            return null;
        }
        for (int i = filesList.length - 1; i >= 0; i--) {
            if (!isSilent(filesList[i])) {
                return filesList[i];
            }
        }
        return filesList[filesList.length - 1];
    }

    public void deletePendingReports() {
        deletePendingReports(true, true, 0);
    }

    public void deletePendingSilentReports() {
        deletePendingReports(true, false, 0);
    }

    public void deletePendingNonApprovedReports() {
        int nbReportsToKeep;
        if (this.mReportingInteractionMode == ReportingInteractionMode.NOTIFICATION) {
            nbReportsToKeep = 1;
        } else {
            nbReportsToKeep = 0;
        }
        deletePendingReports(false, true, nbReportsToKeep);
    }

    private void deletePendingReports(boolean deleteApprovedReports, boolean deleteNonApprovedReports, int nbOfLatestToKeep) {
        String[] filesList = getCrashReportFilesList();
        Arrays.sort(filesList);
        if (filesList != null) {
            for (int iFile = 0; iFile < filesList.length - nbOfLatestToKeep; iFile++) {
                String fileName = filesList[iFile];
                boolean isReportApproved = isApproved(fileName);
                if ((isReportApproved && deleteApprovedReports) || (!isReportApproved && deleteNonApprovedReports)) {
                    new File(mContext.getFilesDir(), fileName).delete();
                }
            }
        }
    }

    public void disable() {
        if (mContext != null) {
            Log.d(ACRA.LOG_TAG, "ACRA is disabled for " + mContext.getPackageName());
        } else {
            Log.d(ACRA.LOG_TAG, "ACRA is disabled.");
        }
        if (this.mDfltExceptionHandler != null) {
            Thread.setDefaultUncaughtExceptionHandler(this.mDfltExceptionHandler);
            enabled = false;
        }
    }

    private boolean containsOnlySilentOrApprovedReports(String[] reportFileNames) {
        for (String reportFileName : reportFileNames) {
            if (!isApproved(reportFileName)) {
                return false;
            }
        }
        return true;
    }

    private boolean isSilent(String reportFileName) {
        return reportFileName.contains(SILENT_SUFFIX);
    }

    private boolean isApproved(String reportFileName) {
        return isSilent(reportFileName) || reportFileName.contains(APPROVED_SUFFIX);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
     arg types: [org.acra.ReportField, java.lang.String]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Object, java.lang.Object):java.lang.Object}
      ClspMth{java.util.AbstractMap.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V} */
    /* access modifiers changed from: private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void addUserDataToReport(android.content.Context r6, java.lang.String r7, java.lang.String r8, java.lang.String r9) {
        /*
            java.lang.String r3 = org.acra.ACRA.LOG_TAG
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Add user comment to "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r7)
            java.lang.String r4 = r4.toString()
            android.util.Log.d(r3, r4)
            if (r7 == 0) goto L_0x0043
            if (r8 == 0) goto L_0x0043
            java.io.FileInputStream r2 = r6.openFileInput(r7)     // Catch:{ FileNotFoundException -> 0x0049, InvalidPropertiesFormatException -> 0x0055, IOException -> 0x005f }
            org.acra.CrashReportData r0 = new org.acra.CrashReportData     // Catch:{ FileNotFoundException -> 0x0049, InvalidPropertiesFormatException -> 0x0055, IOException -> 0x005f }
            r0.<init>()     // Catch:{ FileNotFoundException -> 0x0049, InvalidPropertiesFormatException -> 0x0055, IOException -> 0x005f }
            java.lang.String r3 = org.acra.ACRA.LOG_TAG     // Catch:{ all -> 0x0044 }
            java.lang.String r4 = "Loading Properties report to insert user comment."
            android.util.Log.d(r3, r4)     // Catch:{ all -> 0x0044 }
            r0.load(r2)     // Catch:{ all -> 0x0044 }
            r2.close()     // Catch:{ FileNotFoundException -> 0x0049, InvalidPropertiesFormatException -> 0x0055, IOException -> 0x005f }
            org.acra.ReportField r3 = org.acra.ReportField.USER_COMMENT     // Catch:{ FileNotFoundException -> 0x0049, InvalidPropertiesFormatException -> 0x0055, IOException -> 0x005f }
            r0.put(r3, r8)     // Catch:{ FileNotFoundException -> 0x0049, InvalidPropertiesFormatException -> 0x0055, IOException -> 0x005f }
            org.acra.ReportField r3 = org.acra.ReportField.USER_EMAIL     // Catch:{ FileNotFoundException -> 0x0049, InvalidPropertiesFormatException -> 0x0055, IOException -> 0x005f }
            if (r9 != 0) goto L_0x0053
            java.lang.String r4 = ""
        L_0x003d:
            r0.put(r3, r4)     // Catch:{ FileNotFoundException -> 0x0049, InvalidPropertiesFormatException -> 0x0055, IOException -> 0x005f }
            saveCrashReportFile(r7, r0)     // Catch:{ FileNotFoundException -> 0x0049, InvalidPropertiesFormatException -> 0x0055, IOException -> 0x005f }
        L_0x0043:
            return
        L_0x0044:
            r3 = move-exception
            r2.close()     // Catch:{ FileNotFoundException -> 0x0049, InvalidPropertiesFormatException -> 0x0055, IOException -> 0x005f }
            throw r3     // Catch:{ FileNotFoundException -> 0x0049, InvalidPropertiesFormatException -> 0x0055, IOException -> 0x005f }
        L_0x0049:
            r3 = move-exception
            r1 = r3
            java.lang.String r3 = org.acra.ACRA.LOG_TAG
            java.lang.String r4 = "User comment not added: "
            android.util.Log.w(r3, r4, r1)
            goto L_0x0043
        L_0x0053:
            r4 = r9
            goto L_0x003d
        L_0x0055:
            r3 = move-exception
            r1 = r3
            java.lang.String r3 = org.acra.ACRA.LOG_TAG
            java.lang.String r4 = "User comment not added: "
            android.util.Log.w(r3, r4, r1)
            goto L_0x0043
        L_0x005f:
            r3 = move-exception
            r1 = r3
            java.lang.String r3 = org.acra.ACRA.LOG_TAG
            java.lang.String r4 = "User comment not added: "
            android.util.Log.w(r3, r4, r1)
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: org.acra.ErrorReporter.addUserDataToReport(android.content.Context, java.lang.String, java.lang.String, java.lang.String):void");
    }

    public void addReportSender(ReportSender sender) {
        mReportSenders.add(sender);
    }

    public void removeReportSender(ReportSender sender) {
        mReportSenders.remove(sender);
    }

    public void removeReportSenders(Class<?> senderClass) {
        if (ReportSender.class.isAssignableFrom(senderClass)) {
            Iterator i$ = mReportSenders.iterator();
            while (i$.hasNext()) {
                ReportSender sender = i$.next();
                if (senderClass.isInstance(sender)) {
                    mReportSenders.remove(sender);
                }
            }
        }
    }

    public void removeAllReportSenders() {
        mReportSenders.clear();
    }

    public void setReportSender(ReportSender sender) {
        removeAllReportSenders();
        addReportSender(sender);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
     arg types: [org.acra.ReportField, java.lang.String]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Object, java.lang.Object):java.lang.Object}
      ClspMth{java.util.AbstractMap.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V} */
    public void setAppStartDate(Time appStartDate) {
        mCrashProperties.put((Enum) ReportField.USER_APP_START_DATE, (Object) appStartDate.format3339(false));
    }
}
