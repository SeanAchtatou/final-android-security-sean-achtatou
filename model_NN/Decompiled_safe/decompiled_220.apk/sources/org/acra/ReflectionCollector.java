package org.acra;

import au.com.bytecode.opencsv.CSVWriter;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectionCollector {
    public static String collectConstants(Class<? extends Object> someClass) {
        StringBuilder result = new StringBuilder();
        for (Field field : someClass.getFields()) {
            result.append(field.getName()).append("=");
            try {
                result.append(field.get(null).toString());
            } catch (IllegalArgumentException e) {
                result.append("N/A");
            } catch (IllegalAccessException e2) {
                result.append("N/A");
            }
            result.append(CSVWriter.DEFAULT_LINE_END);
        }
        return result.toString();
    }

    public static String collectStaticGettersResults(Class<? extends Object> someClass) {
        StringBuilder result = new StringBuilder();
        for (Method method : someClass.getMethods()) {
            if (method.getParameterTypes().length == 0 && ((method.getName().startsWith("get") || method.getName().startsWith("is")) && !method.getName().equals("getClass"))) {
                try {
                    result.append(method.getName()).append('=').append(method.invoke(null, null)).append(CSVWriter.DEFAULT_LINE_END);
                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                }
            }
        }
        return result.toString();
    }
}
