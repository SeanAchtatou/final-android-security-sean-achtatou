package org.acra;

import android.content.Context;
import android.text.format.Time;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

class DropBoxCollector {
    private static final String[] SYSTEM_TAGS = {"system_app_anr", "system_app_wtf", "system_app_crash", "system_server_anr", "system_server_wtf", "system_server_crash", "BATTERY_DISCHARGE_INFO", "SYSTEM_RECOVERY_LOG", "SYSTEM_BOOT", "SYSTEM_LAST_KMSG", "APANIC_CONSOLE", "APANIC_THREADS", "SYSTEM_RESTART", "SYSTEM_TOMBSTONE", "data_app_strictmode"};

    DropBoxCollector() {
    }

    /* JADX INFO: Multiple debug info for r3v1 java.lang.Object: [D('serviceName' java.lang.String), D('dropbox' java.lang.Object)] */
    public static String read(Context context, String[] additionalTags) {
        ArrayList<String> tags;
        try {
            String serviceName = Compatibility.getDropBoxServiceName();
            if (serviceName != null) {
                StringBuilder dropboxContent = new StringBuilder();
                Object dropbox = context.getSystemService(serviceName);
                Method getNextEntry = dropbox.getClass().getMethod("getNextEntry", String.class, Long.TYPE);
                if (getNextEntry != null) {
                    Time timer = new Time();
                    timer.setToNow();
                    timer.minute -= ACRA.getConfig().dropboxCollectionMinutes();
                    timer.normalize(false);
                    long time = timer.toMillis(false);
                    if (ACRA.getConfig().includeDropBoxSystemTags()) {
                        tags = new ArrayList<>(Arrays.asList(SYSTEM_TAGS));
                    } else {
                        tags = new ArrayList<>();
                    }
                    if (additionalTags != null && additionalTags.length > 0) {
                        tags.addAll(Arrays.asList(additionalTags));
                    }
                    String entry = null;
                    if (tags.size() > 0) {
                        Iterator i$ = tags.iterator();
                        while (true) {
                            String text = entry;
                            if (!i$.hasNext()) {
                                break;
                            }
                            String tag = (String) i$.next();
                            long msec = time;
                            dropboxContent.append("Tag: ").append(tag).append(10);
                            Object entry2 = getNextEntry.invoke(dropbox, tag, Long.valueOf(msec));
                            if (entry2 != null) {
                                Method getText = entry2.getClass().getMethod("getText", Integer.TYPE);
                                Method getTimeMillis = entry2.getClass().getMethod("getTimeMillis", null);
                                Method close = entry2.getClass().getMethod("close", null);
                                while (entry2 != null) {
                                    msec = ((Long) getTimeMillis.invoke(entry2, null)).longValue();
                                    timer.set(msec);
                                    dropboxContent.append("@").append(timer.format2445()).append(10);
                                    text = (String) getText.invoke(entry2, 500);
                                    if (text != null) {
                                        dropboxContent.append("Text: ").append(text).append(10);
                                    } else {
                                        dropboxContent.append("Not Text!").append(10);
                                    }
                                    close.invoke(entry2, null);
                                    entry2 = getNextEntry.invoke(dropbox, tag, Long.valueOf(msec));
                                }
                                entry = text;
                            } else {
                                dropboxContent.append("Nothing.").append(10);
                                long j = msec;
                                Object obj = entry2;
                                entry = text;
                            }
                        }
                    } else {
                        dropboxContent.append("No tag configured for collection.");
                    }
                }
                return dropboxContent.toString();
            }
        } catch (SecurityException e) {
            Log.i(ACRA.LOG_TAG, "DropBoxManager not available.");
        } catch (NoSuchMethodException e2) {
            Log.i(ACRA.LOG_TAG, "DropBoxManager not available.");
        } catch (IllegalArgumentException e3) {
            Log.i(ACRA.LOG_TAG, "DropBoxManager not available.");
        } catch (IllegalAccessException e4) {
            Log.i(ACRA.LOG_TAG, "DropBoxManager not available.");
        } catch (InvocationTargetException e5) {
            Log.i(ACRA.LOG_TAG, "DropBoxManager not available.");
        } catch (NoSuchFieldException e6) {
            Log.i(ACRA.LOG_TAG, "DropBoxManager not available.");
        }
        return "N/A";
    }
}
