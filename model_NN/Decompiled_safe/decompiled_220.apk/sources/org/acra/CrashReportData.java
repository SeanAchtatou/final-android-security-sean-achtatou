package org.acra;

import au.com.bytecode.opencsv.CSVParser;
import au.com.bytecode.opencsv.CSVWriter;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Collections;
import java.util.EnumMap;
import java.util.Enumeration;
import java.util.Map;
import org.nick.wwwjdic.history.SearchCriteriaParser;

public class CrashReportData extends EnumMap<ReportField, String> {
    private static final int CONTINUE = 3;
    private static final int IGNORE = 5;
    private static final int KEY_DONE = 4;
    private static final int NONE = 0;
    private static final String PROP_DTD_NAME = "http://java.sun.com/dtd/properties.dtd";
    private static final int SLASH = 1;
    private static final int UNICODE = 2;
    private static String lineSeparator = CSVWriter.DEFAULT_LINE_END;
    private static final long serialVersionUID = 4112578634029874840L;
    protected CrashReportData defaults;

    public CrashReportData() {
        super(ReportField.class);
    }

    public CrashReportData(CrashReportData properties) {
        super(ReportField.class);
        this.defaults = properties;
    }

    private void dumpString(StringBuilder buffer, String string, boolean key) {
        int i = 0;
        if (!key && 0 < string.length() && string.charAt(0) == ' ') {
            buffer.append("\\ ");
            i = 0 + 1;
        }
        while (i < string.length()) {
            char ch = string.charAt(i);
            switch (ch) {
                case SearchCriteriaParser.MAX_STROKE_COUNT_IDX /*9*/:
                    buffer.append("\\t");
                    break;
                case SearchCriteriaParser.NUM_MAX_RESULTS_IDX /*10*/:
                    buffer.append("\\n");
                    break;
                case SearchCriteriaParser.TIME_IDX /*11*/:
                default:
                    if ("\\#!=:".indexOf(ch) >= 0 || (key && ch == ' ')) {
                        buffer.append((char) CSVParser.DEFAULT_ESCAPE_CHARACTER);
                    }
                    if (ch >= ' ' && ch <= '~') {
                        buffer.append(ch);
                        break;
                    } else {
                        String hex = Integer.toHexString(ch);
                        buffer.append("\\u");
                        for (int j = 0; j < 4 - hex.length(); j++) {
                            buffer.append("0");
                        }
                        buffer.append(hex);
                        break;
                    }
                    break;
                case 12:
                    buffer.append("\\f");
                    break;
                case 13:
                    buffer.append("\\r");
                    break;
            }
            i++;
        }
    }

    public String getProperty(ReportField key) {
        String result = (String) super.get(key);
        if (result != null || this.defaults == null) {
            return result;
        }
        return this.defaults.getProperty(key);
    }

    public String getProperty(ReportField key, String defaultValue) {
        String property = (String) super.get(key);
        if (property == null && this.defaults != null) {
            property = this.defaults.getProperty(key);
        }
        if (property == null) {
            return defaultValue;
        }
        return property;
    }

    public void list(PrintStream out) {
        if (out == null) {
            throw new NullPointerException();
        }
        StringBuilder buffer = new StringBuilder(80);
        Enumeration<ReportField> keys = keys();
        while (keys.hasMoreElements()) {
            ReportField key = keys.nextElement();
            buffer.append(key);
            buffer.append('=');
            String property = (String) super.get(key);
            CrashReportData def = this.defaults;
            while (property == null) {
                property = (String) def.get(key);
                def = def.defaults;
            }
            if (property.length() > 40) {
                buffer.append(property.substring(0, 37));
                buffer.append("...");
            } else {
                buffer.append(property);
            }
            out.println(buffer.toString());
            buffer.setLength(0);
        }
    }

    public void list(PrintWriter writer) {
        if (writer == null) {
            throw new NullPointerException();
        }
        StringBuilder buffer = new StringBuilder(80);
        Enumeration<ReportField> keys = keys();
        while (keys.hasMoreElements()) {
            ReportField key = keys.nextElement();
            buffer.append(key);
            buffer.append('=');
            String property = (String) super.get(key);
            CrashReportData def = this.defaults;
            while (property == null) {
                property = (String) def.get(key);
                def = def.defaults;
            }
            if (property.length() > 40) {
                buffer.append(property.substring(0, 37));
                buffer.append("...");
            } else {
                buffer.append(property);
            }
            writer.println(buffer.toString());
            buffer.setLength(0);
        }
    }

    public synchronized void load(InputStream in) throws IOException {
        if (in == null) {
            throw new NullPointerException();
        }
        BufferedInputStream bis = new BufferedInputStream(in);
        bis.mark(Integer.MAX_VALUE);
        boolean isEbcdic = isEbcdic(bis);
        bis.reset();
        if (!isEbcdic) {
            load(new InputStreamReader(bis, "ISO8859-1"));
        } else {
            load(new InputStreamReader(bis));
        }
    }

    private boolean isEbcdic(BufferedInputStream in) throws IOException {
        byte b;
        do {
            b = (byte) in.read();
            if (b == -1) {
                return false;
            }
            if (b == 35 || b == 10 || b == 61) {
                return false;
            }
        } while (b != 21);
        return true;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
     arg types: [java.lang.Enum, java.lang.String]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Object, java.lang.Object):java.lang.Object}
      ClspMth{java.util.AbstractMap.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
     arg types: [org.acra.ReportField, java.lang.String]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Object, java.lang.Object):java.lang.Object}
      ClspMth{java.util.AbstractMap.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V} */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00f2  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0150  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void load(java.io.Reader r24) throws java.io.IOException {
        /*
            r23 = this;
            monitor-enter(r23)
            r13 = 0
            r19 = 0
            r7 = 0
            r21 = 40
            r0 = r21
            char[] r0 = new char[r0]     // Catch:{ all -> 0x003b }
            r6 = r0
            r16 = 0
            r12 = -1
            r9 = 1
            java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ all -> 0x003b }
            r0 = r5
            r1 = r24
            r0.<init>(r1)     // Catch:{ all -> 0x003b }
            r17 = r16
        L_0x001a:
            int r10 = r5.read()     // Catch:{ all -> 0x003b }
            r21 = -1
            r0 = r10
            r1 = r21
            if (r0 != r1) goto L_0x003e
            r21 = 2
            r0 = r13
            r1 = r21
            if (r0 != r1) goto L_0x01b1
            r21 = 4
            r0 = r7
            r1 = r21
            if (r0 > r1) goto L_0x01b1
            java.lang.IllegalArgumentException r21 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x003b }
            java.lang.String r22 = "luni.08"
            r21.<init>(r22)     // Catch:{ all -> 0x003b }
            throw r21     // Catch:{ all -> 0x003b }
        L_0x003b:
            r21 = move-exception
            monitor-exit(r23)
            throw r21
        L_0x003e:
            char r15 = (char) r10
            r0 = r6
            int r0 = r0.length     // Catch:{ all -> 0x003b }
            r21 = r0
            r0 = r17
            r1 = r21
            if (r0 != r1) goto L_0x0064
            r0 = r6
            int r0 = r0.length     // Catch:{ all -> 0x003b }
            r21 = r0
            int r21 = r21 * 2
            r0 = r21
            char[] r0 = new char[r0]     // Catch:{ all -> 0x003b }
            r14 = r0
            r21 = 0
            r22 = 0
            r0 = r6
            r1 = r21
            r2 = r14
            r3 = r22
            r4 = r17
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)     // Catch:{ all -> 0x003b }
            r6 = r14
        L_0x0064:
            r21 = 2
            r0 = r13
            r1 = r21
            if (r0 != r1) goto L_0x00b0
            r21 = 16
            r0 = r15
            r1 = r21
            int r8 = java.lang.Character.digit(r0, r1)     // Catch:{ all -> 0x003b }
            if (r8 < 0) goto L_0x009f
            int r21 = r19 << 4
            int r19 = r21 + r8
            int r7 = r7 + 1
            r21 = 4
            r0 = r7
            r1 = r21
            if (r0 < r1) goto L_0x001a
        L_0x0083:
            r13 = 0
            int r16 = r17 + 1
            r0 = r19
            char r0 = (char) r0     // Catch:{ all -> 0x003b }
            r21 = r0
            r6[r17] = r21     // Catch:{ all -> 0x003b }
            r21 = 10
            r0 = r15
            r1 = r21
            if (r0 == r1) goto L_0x00ae
            r21 = 133(0x85, float:1.86E-43)
            r0 = r15
            r1 = r21
            if (r0 == r1) goto L_0x00ae
            r17 = r16
            goto L_0x001a
        L_0x009f:
            r21 = 4
            r0 = r7
            r1 = r21
            if (r0 > r1) goto L_0x0083
            java.lang.IllegalArgumentException r21 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x003b }
            java.lang.String r22 = "luni.09"
            r21.<init>(r22)     // Catch:{ all -> 0x003b }
            throw r21     // Catch:{ all -> 0x003b }
        L_0x00ae:
            r17 = r16
        L_0x00b0:
            r21 = 1
            r0 = r13
            r1 = r21
            if (r0 != r1) goto L_0x00e9
            r13 = 0
            switch(r15) {
                case 10: goto L_0x00d1;
                case 13: goto L_0x00ce;
                case 98: goto L_0x00d4;
                case 102: goto L_0x00d7;
                case 110: goto L_0x00da;
                case 114: goto L_0x00dd;
                case 116: goto L_0x00e0;
                case 117: goto L_0x00e3;
                case 133: goto L_0x00d1;
                default: goto L_0x00bb;
            }     // Catch:{ all -> 0x003b }
        L_0x00bb:
            r9 = 0
            r21 = 4
            r0 = r13
            r1 = r21
            if (r0 != r1) goto L_0x00c6
            r12 = r17
            r13 = 0
        L_0x00c6:
            int r16 = r17 + 1
            r6[r17] = r15     // Catch:{ all -> 0x003b }
            r17 = r16
            goto L_0x001a
        L_0x00ce:
            r13 = 3
            goto L_0x001a
        L_0x00d1:
            r13 = 5
            goto L_0x001a
        L_0x00d4:
            r15 = 8
            goto L_0x00bb
        L_0x00d7:
            r15 = 12
            goto L_0x00bb
        L_0x00da:
            r15 = 10
            goto L_0x00bb
        L_0x00dd:
            r15 = 13
            goto L_0x00bb
        L_0x00e0:
            r15 = 9
            goto L_0x00bb
        L_0x00e3:
            r13 = 2
            r7 = 0
            r19 = r7
            goto L_0x001a
        L_0x00e9:
            switch(r15) {
                case 10: goto L_0x0137;
                case 13: goto L_0x0141;
                case 33: goto L_0x0112;
                case 35: goto L_0x0112;
                case 58: goto L_0x0194;
                case 61: goto L_0x0194;
                case 92: goto L_0x0188;
                case 133: goto L_0x0141;
                default: goto L_0x00ec;
            }     // Catch:{ all -> 0x003b }
        L_0x00ec:
            boolean r21 = java.lang.Character.isWhitespace(r15)     // Catch:{ all -> 0x003b }
            if (r21 == 0) goto L_0x01a0
            r21 = 3
            r0 = r13
            r1 = r21
            if (r0 != r1) goto L_0x00fa
            r13 = 5
        L_0x00fa:
            if (r17 == 0) goto L_0x001a
            r0 = r17
            r1 = r12
            if (r0 == r1) goto L_0x001a
            r21 = 5
            r0 = r13
            r1 = r21
            if (r0 == r1) goto L_0x001a
            r21 = -1
            r0 = r12
            r1 = r21
            if (r0 != r1) goto L_0x01a0
            r13 = 4
            goto L_0x001a
        L_0x0112:
            if (r9 == 0) goto L_0x00ec
        L_0x0114:
            int r10 = r5.read()     // Catch:{ all -> 0x003b }
            r21 = -1
            r0 = r10
            r1 = r21
            if (r0 == r1) goto L_0x001a
            char r15 = (char) r10     // Catch:{ all -> 0x003b }
            r21 = 13
            r0 = r15
            r1 = r21
            if (r0 == r1) goto L_0x001a
            r21 = 10
            r0 = r15
            r1 = r21
            if (r0 == r1) goto L_0x001a
            r21 = 133(0x85, float:1.86E-43)
            r0 = r15
            r1 = r21
            if (r0 != r1) goto L_0x0114
            goto L_0x001a
        L_0x0137:
            r21 = 3
            r0 = r13
            r1 = r21
            if (r0 != r1) goto L_0x0141
            r13 = 5
            goto L_0x001a
        L_0x0141:
            r13 = 0
            r9 = 1
            if (r17 > 0) goto L_0x0149
            if (r17 != 0) goto L_0x0181
            if (r12 != 0) goto L_0x0181
        L_0x0149:
            r21 = -1
            r0 = r12
            r1 = r21
            if (r0 != r1) goto L_0x0152
            r12 = r17
        L_0x0152:
            java.lang.String r18 = new java.lang.String     // Catch:{ all -> 0x003b }
            r21 = 0
            r0 = r18
            r1 = r6
            r2 = r21
            r3 = r17
            r0.<init>(r1, r2, r3)     // Catch:{ all -> 0x003b }
            java.lang.Class<org.acra.ReportField> r21 = org.acra.ReportField.class
            r22 = 0
            r0 = r18
            r1 = r22
            r2 = r12
            java.lang.String r22 = r0.substring(r1, r2)     // Catch:{ all -> 0x003b }
            java.lang.Enum r21 = java.lang.Enum.valueOf(r21, r22)     // Catch:{ all -> 0x003b }
            r0 = r18
            r1 = r12
            java.lang.String r22 = r0.substring(r1)     // Catch:{ all -> 0x003b }
            r0 = r23
            r1 = r21
            r2 = r22
            r0.put(r1, r2)     // Catch:{ all -> 0x003b }
        L_0x0181:
            r12 = -1
            r16 = 0
            r17 = r16
            goto L_0x001a
        L_0x0188:
            r21 = 4
            r0 = r13
            r1 = r21
            if (r0 != r1) goto L_0x0191
            r12 = r17
        L_0x0191:
            r13 = 1
            goto L_0x001a
        L_0x0194:
            r21 = -1
            r0 = r12
            r1 = r21
            if (r0 != r1) goto L_0x00ec
            r13 = 0
            r12 = r17
            goto L_0x001a
        L_0x01a0:
            r21 = 5
            r0 = r13
            r1 = r21
            if (r0 == r1) goto L_0x01ae
            r21 = 3
            r0 = r13
            r1 = r21
            if (r0 != r1) goto L_0x00bb
        L_0x01ae:
            r13 = 0
            goto L_0x00bb
        L_0x01b1:
            r21 = -1
            r0 = r12
            r1 = r21
            if (r0 != r1) goto L_0x01bc
            if (r17 <= 0) goto L_0x01bc
            r12 = r17
        L_0x01bc:
            if (r12 < 0) goto L_0x020c
            java.lang.String r18 = new java.lang.String     // Catch:{ all -> 0x003b }
            r21 = 0
            r0 = r18
            r1 = r6
            r2 = r21
            r3 = r17
            r0.<init>(r1, r2, r3)     // Catch:{ all -> 0x003b }
            java.lang.Class<org.acra.ReportField> r21 = org.acra.ReportField.class
            r22 = 0
            r0 = r18
            r1 = r22
            r2 = r12
            java.lang.String r22 = r0.substring(r1, r2)     // Catch:{ all -> 0x003b }
            java.lang.Enum r11 = java.lang.Enum.valueOf(r21, r22)     // Catch:{ all -> 0x003b }
            org.acra.ReportField r11 = (org.acra.ReportField) r11     // Catch:{ all -> 0x003b }
            r0 = r18
            r1 = r12
            java.lang.String r20 = r0.substring(r1)     // Catch:{ all -> 0x003b }
            r21 = 1
            r0 = r13
            r1 = r21
            if (r0 != r1) goto L_0x0204
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ all -> 0x003b }
            r21.<init>()     // Catch:{ all -> 0x003b }
            r0 = r21
            r1 = r20
            java.lang.StringBuilder r21 = r0.append(r1)     // Catch:{ all -> 0x003b }
            java.lang.String r22 = "\u0000"
            java.lang.StringBuilder r21 = r21.append(r22)     // Catch:{ all -> 0x003b }
            java.lang.String r20 = r21.toString()     // Catch:{ all -> 0x003b }
        L_0x0204:
            r0 = r23
            r1 = r11
            r2 = r20
            r0.put(r1, r2)     // Catch:{ all -> 0x003b }
        L_0x020c:
            monitor-exit(r23)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.acra.CrashReportData.load(java.io.Reader):void");
    }

    private Enumeration<ReportField> keys() {
        return Collections.enumeration(keySet());
    }

    @Deprecated
    public void save(OutputStream out, String comment) {
        try {
            store(out, comment);
        } catch (IOException e) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
     arg types: [org.acra.ReportField, java.lang.String]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Object, java.lang.Object):java.lang.Object}
      ClspMth{java.util.AbstractMap.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V} */
    public Object setProperty(ReportField key, String value) {
        return put((Enum) key, (Object) value);
    }

    public synchronized void store(OutputStream out, String comment) throws IOException {
        StringBuilder buffer = new StringBuilder(200);
        OutputStreamWriter writer = new OutputStreamWriter(out, "ISO8859_1");
        if (comment != null) {
            writer.write("#");
            writer.write(comment);
            writer.write(lineSeparator);
        }
        for (Map.Entry<ReportField, String> entry : entrySet()) {
            dumpString(buffer, ((ReportField) entry.getKey()).toString(), true);
            buffer.append('=');
            dumpString(buffer, (String) entry.getValue(), false);
            buffer.append(lineSeparator);
            writer.write(buffer.toString());
            buffer.setLength(0);
        }
        writer.flush();
    }

    public synchronized void store(Writer writer, String comment) throws IOException {
        StringBuilder buffer = new StringBuilder(200);
        if (comment != null) {
            writer.write("#");
            writer.write(comment);
            writer.write(lineSeparator);
        }
        for (Map.Entry<ReportField, String> entry : entrySet()) {
            dumpString(buffer, ((ReportField) entry.getKey()).toString(), true);
            buffer.append('=');
            dumpString(buffer, (String) entry.getValue(), false);
            buffer.append(lineSeparator);
            writer.write(buffer.toString());
            buffer.setLength(0);
        }
        writer.flush();
    }

    public void storeToXML(OutputStream os, String comment) throws IOException {
        storeToXML(os, comment, "UTF-8");
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:3:0x0005=Splitter:B:3:0x0005, B:11:0x0016=Splitter:B:11:0x0016} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void storeToXML(java.io.OutputStream r11, java.lang.String r12, java.lang.String r13) throws java.io.IOException {
        /*
            r10 = this;
            monitor-enter(r10)
            if (r11 == 0) goto L_0x0005
            if (r13 != 0) goto L_0x000e
        L_0x0005:
            java.lang.NullPointerException r7 = new java.lang.NullPointerException     // Catch:{ all -> 0x000b }
            r7.<init>()     // Catch:{ all -> 0x000b }
            throw r7     // Catch:{ all -> 0x000b }
        L_0x000b:
            r7 = move-exception
            monitor-exit(r10)
            throw r7
        L_0x000e:
            java.nio.charset.Charset r7 = java.nio.charset.Charset.forName(r13)     // Catch:{ IllegalCharsetNameException -> 0x0092, UnsupportedCharsetException -> 0x00b6 }
            java.lang.String r1 = r7.name()     // Catch:{ IllegalCharsetNameException -> 0x0092, UnsupportedCharsetException -> 0x00b6 }
        L_0x0016:
            java.io.PrintStream r6 = new java.io.PrintStream     // Catch:{ all -> 0x000b }
            r7 = 0
            r6.<init>(r11, r7, r1)     // Catch:{ all -> 0x000b }
            java.lang.String r7 = "<?xml version=\"1.0\" encoding=\""
            r6.print(r7)     // Catch:{ all -> 0x000b }
            r6.print(r1)     // Catch:{ all -> 0x000b }
            java.lang.String r7 = "\"?>"
            r6.println(r7)     // Catch:{ all -> 0x000b }
            java.lang.String r7 = "<!DOCTYPE properties SYSTEM \""
            r6.print(r7)     // Catch:{ all -> 0x000b }
            java.lang.String r7 = "http://java.sun.com/dtd/properties.dtd"
            r6.print(r7)     // Catch:{ all -> 0x000b }
            java.lang.String r7 = "\">"
            r6.println(r7)     // Catch:{ all -> 0x000b }
            java.lang.String r7 = "<properties>"
            r6.println(r7)     // Catch:{ all -> 0x000b }
            if (r12 == 0) goto L_0x0050
            java.lang.String r7 = "<comment>"
            r6.print(r7)     // Catch:{ all -> 0x000b }
            java.lang.String r7 = r10.substitutePredefinedEntries(r12)     // Catch:{ all -> 0x000b }
            r6.print(r7)     // Catch:{ all -> 0x000b }
            java.lang.String r7 = "</comment>"
            r6.println(r7)     // Catch:{ all -> 0x000b }
        L_0x0050:
            java.util.Set r7 = r10.entrySet()     // Catch:{ all -> 0x000b }
            java.util.Iterator r4 = r7.iterator()     // Catch:{ all -> 0x000b }
        L_0x0058:
            boolean r7 = r4.hasNext()     // Catch:{ all -> 0x000b }
            if (r7 == 0) goto L_0x00da
            java.lang.Object r2 = r4.next()     // Catch:{ all -> 0x000b }
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2     // Catch:{ all -> 0x000b }
            java.lang.Object r7 = r2.getKey()     // Catch:{ all -> 0x000b }
            org.acra.ReportField r7 = (org.acra.ReportField) r7     // Catch:{ all -> 0x000b }
            java.lang.String r5 = r7.toString()     // Catch:{ all -> 0x000b }
            java.lang.Object r3 = r2.getValue()     // Catch:{ all -> 0x000b }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ all -> 0x000b }
            java.lang.String r7 = "<entry key=\""
            r6.print(r7)     // Catch:{ all -> 0x000b }
            java.lang.String r7 = r10.substitutePredefinedEntries(r5)     // Catch:{ all -> 0x000b }
            r6.print(r7)     // Catch:{ all -> 0x000b }
            java.lang.String r7 = "\">"
            r6.print(r7)     // Catch:{ all -> 0x000b }
            java.lang.String r7 = r10.substitutePredefinedEntries(r3)     // Catch:{ all -> 0x000b }
            r6.print(r7)     // Catch:{ all -> 0x000b }
            java.lang.String r7 = "</entry>"
            r6.println(r7)     // Catch:{ all -> 0x000b }
            goto L_0x0058
        L_0x0092:
            r7 = move-exception
            r0 = r7
            java.io.PrintStream r7 = java.lang.System.out     // Catch:{ all -> 0x000b }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x000b }
            r8.<init>()     // Catch:{ all -> 0x000b }
            java.lang.String r9 = "Warning: encoding name "
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x000b }
            java.lang.StringBuilder r8 = r8.append(r13)     // Catch:{ all -> 0x000b }
            java.lang.String r9 = " is illegal, using UTF-8 as default encoding"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x000b }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x000b }
            r7.println(r8)     // Catch:{ all -> 0x000b }
            java.lang.String r1 = "UTF-8"
            goto L_0x0016
        L_0x00b6:
            r7 = move-exception
            r0 = r7
            java.io.PrintStream r7 = java.lang.System.out     // Catch:{ all -> 0x000b }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x000b }
            r8.<init>()     // Catch:{ all -> 0x000b }
            java.lang.String r9 = "Warning: encoding "
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x000b }
            java.lang.StringBuilder r8 = r8.append(r13)     // Catch:{ all -> 0x000b }
            java.lang.String r9 = " is not supported, using UTF-8 as default encoding"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x000b }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x000b }
            r7.println(r8)     // Catch:{ all -> 0x000b }
            java.lang.String r1 = "UTF-8"
            goto L_0x0016
        L_0x00da:
            java.lang.String r7 = "</properties>"
            r6.println(r7)     // Catch:{ all -> 0x000b }
            r6.flush()     // Catch:{ all -> 0x000b }
            monitor-exit(r10)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.acra.CrashReportData.storeToXML(java.io.OutputStream, java.lang.String, java.lang.String):void");
    }

    private String substitutePredefinedEntries(String s) {
        return s.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("'", "&apos;").replaceAll("\"", "&quot;");
    }
}
