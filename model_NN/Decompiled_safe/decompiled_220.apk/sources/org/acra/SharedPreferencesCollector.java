package org.acra;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import au.com.bytecode.opencsv.CSVWriter;
import java.util.Map;
import java.util.TreeMap;

public class SharedPreferencesCollector {
    public static String collect(Context context) {
        StringBuilder result = new StringBuilder();
        Map<String, SharedPreferences> shrdPrefs = new TreeMap<>();
        shrdPrefs.put("default", PreferenceManager.getDefaultSharedPreferences(context));
        String[] shrdPrefsIds = ACRA.getConfig().additionalSharedPreferences();
        if (shrdPrefsIds != null) {
            for (String shrdPrefId : shrdPrefsIds) {
                shrdPrefs.put(shrdPrefId, context.getSharedPreferences(shrdPrefId, 0));
            }
        }
        for (String prefsId : shrdPrefs.keySet()) {
            result.append(prefsId).append(CSVWriter.DEFAULT_LINE_END);
            SharedPreferences prefs = shrdPrefs.get(prefsId);
            if (prefs != null) {
                Map<String, ?> kv = prefs.getAll();
                if (kv == null || kv.size() <= 0) {
                    result.append("empty\n");
                } else {
                    for (String key : kv.keySet()) {
                        result.append(key).append("=").append(kv.get(key).toString()).append(CSVWriter.DEFAULT_LINE_END);
                    }
                }
            } else {
                result.append("null\n");
            }
            result.append(CSVWriter.DEFAULT_LINE_END);
        }
        return result.toString();
    }
}
