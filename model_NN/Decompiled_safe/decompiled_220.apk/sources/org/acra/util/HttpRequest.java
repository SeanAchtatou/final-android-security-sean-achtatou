package org.acra.util;

import android.util.Log;
import com.google.api.client.http.UrlEncodedParser;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import org.acra.ACRA;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

public class HttpRequest {
    UsernamePasswordCredentials creds = null;
    DefaultHttpClient httpClient;
    HttpGet httpGet = null;
    HttpPost httpPost = null;
    HttpContext localContext;

    public HttpRequest(String login, String password) {
        if (!(login == null && password == null)) {
            this.creds = new UsernamePasswordCredentials(login, password);
        }
        HttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, ACRA.getConfig().socketTimeout());
        HttpConnectionParams.setSoTimeout(httpParams, ACRA.getConfig().socketTimeout());
        HttpConnectionParams.setSocketBufferSize(httpParams, 8192);
        SchemeRegistry registry = new SchemeRegistry();
        registry.register(new Scheme("http", new PlainSocketFactory(), 80));
        registry.register(new Scheme("https", new FakeSocketFactory(), 443));
        this.httpClient = new DefaultHttpClient(new ThreadSafeClientConnManager(httpParams, registry), httpParams);
        this.localContext = new BasicHttpContext();
    }

    public void clearCookies() {
        this.httpClient.getCookieStore().clear();
    }

    public void abort() {
        try {
            if (this.httpClient != null) {
                Log.d(ACRA.LOG_TAG, "Abort HttpClient request.");
                this.httpPost.abort();
            }
        } catch (Exception e) {
            Log.e(ACRA.LOG_TAG, "Error while aborting HttpClient request", e);
        }
    }

    public String sendPost(String url, String data) throws ClientProtocolException, IOException {
        return sendPost(url, data, null);
    }

    public String sendPost(String url, String data, String contentType) throws ClientProtocolException, IOException {
        this.httpClient.getParams().setParameter("http.protocol.cookie-policy", "rfc2109");
        this.httpPost = new HttpPost(url);
        Log.d(ACRA.LOG_TAG, "Setting httpPost headers");
        if (this.creds != null) {
            this.httpPost.addHeader(BasicScheme.authenticate(this.creds, "UTF-8", false));
        }
        this.httpPost.setHeader("User-Agent", "Android");
        this.httpPost.setHeader("Accept", "text/html,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5");
        if (contentType != null) {
            this.httpPost.setHeader("Content-Type", contentType);
        } else {
            this.httpPost.setHeader("Content-Type", UrlEncodedParser.CONTENT_TYPE);
        }
        this.httpPost.setEntity(new StringEntity(data, "UTF-8"));
        Log.d(ACRA.LOG_TAG, "Sending request to " + url);
        HttpResponse response = this.httpClient.execute(this.httpPost, this.localContext);
        if (response == null) {
            return null;
        }
        if (response.getStatusLine() != null) {
            String statusCode = Integer.toString(response.getStatusLine().getStatusCode());
            if (statusCode.startsWith("4") || statusCode.startsWith("5")) {
                throw new IOException("Host returned error code " + statusCode);
            }
        }
        return EntityUtils.toString(response.getEntity());
    }

    public String sendGet(String url) throws ClientProtocolException, IOException {
        this.httpGet = new HttpGet(url);
        return EntityUtils.toString(this.httpClient.execute(this.httpGet).getEntity());
    }

    public InputStream getHttpStream(String urlString) throws IOException {
        URLConnection conn = new URL(urlString).openConnection();
        if (!(conn instanceof HttpURLConnection)) {
            throw new IOException("Not an HTTP connection");
        }
        try {
            HttpURLConnection httpConn = (HttpURLConnection) conn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect();
            if (httpConn.getResponseCode() == 200) {
                return httpConn.getInputStream();
            }
            return null;
        } catch (Exception e) {
            throw new IOException("Error connecting");
        }
    }
}
