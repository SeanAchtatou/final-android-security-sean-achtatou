package org.acra;

import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;
import au.com.bytecode.opencsv.CSVWriter;

public class DeviceFeaturesCollector {
    public static String getFeatures(Context ctx) {
        if (Compatibility.getAPILevel() < 5) {
            return "Data available only with API Level > 5";
        }
        StringBuffer result = new StringBuffer();
        try {
            for (Object feature : (Object[]) PackageManager.class.getMethod("getSystemAvailableFeatures", null).invoke(ctx.getPackageManager(), new Object[0])) {
                String featureName = (String) feature.getClass().getField("name").get(feature);
                if (featureName != null) {
                    result.append(featureName);
                } else {
                    result.append("glEsVersion = ");
                    result.append((String) feature.getClass().getMethod("getGlEsVersion", null).invoke(feature, new Object[0]));
                }
                result.append(CSVWriter.DEFAULT_LINE_END);
            }
        } catch (Throwable th) {
            Throwable e = th;
            Log.w(ACRA.LOG_TAG, "Error : ", e);
            result.append("Could not retrieve data: ");
            result.append(e.getMessage());
        }
        return result.toString();
    }
}
