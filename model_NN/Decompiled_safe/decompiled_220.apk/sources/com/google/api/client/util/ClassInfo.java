package com.google.api.client.util;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.WeakHashMap;

public final class ClassInfo {
    private static final ThreadLocal<WeakHashMap<Class<?>, ClassInfo>> CACHE = new ThreadLocal<WeakHashMap<Class<?>, ClassInfo>>() {
        /* access modifiers changed from: protected */
        public WeakHashMap<Class<?>, ClassInfo> initialValue() {
            return new WeakHashMap<>();
        }
    };
    public final Class<?> clazz;
    private final IdentityHashMap<String, FieldInfo> keyNameToFieldInfoMap;

    public static ClassInfo of(Class<?> clazz2) {
        if (clazz2 == null) {
            return null;
        }
        WeakHashMap<Class<?>, ClassInfo> cache = CACHE.get();
        ClassInfo classInfo = (ClassInfo) cache.get(clazz2);
        if (classInfo == null) {
            classInfo = new ClassInfo(clazz2);
            cache.put(clazz2, classInfo);
        }
        return classInfo;
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public FieldInfo getFieldInfo(String keyName) {
        IdentityHashMap<String, FieldInfo> keyNameToFieldInfoMap2;
        if (keyName == null || (keyNameToFieldInfoMap2 = this.keyNameToFieldInfoMap) == null) {
            return null;
        }
        return keyNameToFieldInfoMap2.get(keyName.intern());
    }

    public Field getField(String keyName) {
        FieldInfo fieldInfo = getFieldInfo(keyName);
        if (fieldInfo == null) {
            return null;
        }
        return fieldInfo.field;
    }

    public int getKeyCount() {
        IdentityHashMap<String, FieldInfo> keyNameToFieldInfoMap2 = this.keyNameToFieldInfoMap;
        if (keyNameToFieldInfoMap2 == null) {
            return 0;
        }
        return keyNameToFieldInfoMap2.size();
    }

    public Collection<String> getKeyNames() {
        IdentityHashMap<String, FieldInfo> keyNameToFieldInfoMap2 = this.keyNameToFieldInfoMap;
        if (keyNameToFieldInfoMap2 == null) {
            return Collections.emptySet();
        }
        return Collections.unmodifiableSet(keyNameToFieldInfoMap2.keySet());
    }

    public static <T> T newInstance(Class<T> clazz2) {
        try {
            return clazz2.newInstance();
        } catch (IllegalAccessException e) {
            throw handleExceptionForNewInstance(e, clazz2);
        } catch (InstantiationException e2) {
            throw handleExceptionForNewInstance(e2, clazz2);
        }
    }

    private static IllegalArgumentException handleExceptionForNewInstance(Exception e, Class<?> clazz2) {
        StringBuilder buf = new StringBuilder("unable to create new instance of class ").append(clazz2.getName());
        if (Modifier.isAbstract(clazz2.getModifiers())) {
            buf.append(" (and) because it is abstract");
        }
        if (clazz2.getEnclosingClass() != null && !Modifier.isStatic(clazz2.getModifiers())) {
            buf.append(" (and) because it is not static");
        }
        if (!Modifier.isPublic(clazz2.getModifiers())) {
            buf.append(" (and) because it is not public");
        } else {
            try {
                clazz2.getConstructor(new Class[0]);
            } catch (NoSuchMethodException e2) {
                buf.append(" (and) because it has no public default constructor");
            }
        }
        throw new IllegalArgumentException(buf.toString(), e);
    }

    public static Collection<Object> newCollectionInstance(Class<?> collectionClass) {
        if (collectionClass == null || collectionClass.isAssignableFrom(ArrayList.class)) {
            return new ArrayList();
        }
        if ((collectionClass.getModifiers() & 1536) == 0) {
            return (Collection) newInstance(collectionClass);
        }
        if (collectionClass.isAssignableFrom(HashSet.class)) {
            return new HashSet();
        }
        if (collectionClass.isAssignableFrom(TreeSet.class)) {
            return new TreeSet();
        }
        throw new IllegalArgumentException("no default collection class defined for class: " + collectionClass.getName());
    }

    public static Map<String, Object> newMapInstance(Class<?> mapClass) {
        if (mapClass != null && (mapClass.getModifiers() & 1536) == 0) {
            return (Map) newInstance(mapClass);
        }
        if (mapClass == null || mapClass.isAssignableFrom(ArrayMap.class)) {
            return ArrayMap.create();
        }
        if (mapClass.isAssignableFrom(TreeMap.class)) {
            return new TreeMap();
        }
        throw new IllegalArgumentException("no default map class defined for class: " + mapClass.getName());
    }

    public static Class<?> getCollectionParameter(Field field) {
        if (field != null) {
            Type genericType = field.getGenericType();
            if (genericType instanceof ParameterizedType) {
                Type[] typeArgs = ((ParameterizedType) genericType).getActualTypeArguments();
                if (typeArgs.length == 1 && (typeArgs[0] instanceof Class)) {
                    return (Class) typeArgs[0];
                }
            }
        }
        return null;
    }

    public static Class<?> getMapValueParameter(Field field) {
        if (field != null) {
            return getMapValueParameter(field.getGenericType());
        }
        return null;
    }

    public static Class<?> getMapValueParameter(Type genericType) {
        if (genericType instanceof ParameterizedType) {
            Type[] typeArgs = ((ParameterizedType) genericType).getActualTypeArguments();
            if (typeArgs.length == 2 && (typeArgs[1] instanceof Class)) {
                return (Class) typeArgs[1];
            }
        }
        return null;
    }

    private ClassInfo(Class<?> clazz2) {
        IdentityHashMap<String, FieldInfo> superKeyNameToFieldInfoMap;
        this.clazz = clazz2;
        Class<?> superClass = clazz2.getSuperclass();
        IdentityHashMap<String, FieldInfo> keyNameToFieldInfoMap2 = new IdentityHashMap<>();
        if (!(superClass == null || (superKeyNameToFieldInfoMap = of(superClass).keyNameToFieldInfoMap) == null)) {
            keyNameToFieldInfoMap2.putAll(superKeyNameToFieldInfoMap);
        }
        for (Field field : clazz2.getDeclaredFields()) {
            FieldInfo fieldInfo = FieldInfo.of(field);
            if (fieldInfo != null) {
                String fieldName = fieldInfo.name;
                FieldInfo conflictingFieldInfo = keyNameToFieldInfoMap2.get(fieldName);
                if (conflictingFieldInfo != null) {
                    throw new IllegalArgumentException("two fields have the same data key name: " + field + " and " + conflictingFieldInfo.field);
                }
                keyNameToFieldInfoMap2.put(fieldName, fieldInfo);
            }
        }
        if (keyNameToFieldInfoMap2.isEmpty()) {
            this.keyNameToFieldInfoMap = null;
        } else {
            this.keyNameToFieldInfoMap = keyNameToFieldInfoMap2;
        }
    }
}
