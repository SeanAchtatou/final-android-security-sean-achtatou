package com.google.api.client.http;

import com.google.api.client.escape.CharEscapers;
import com.google.api.client.escape.Escaper;
import com.google.api.client.escape.PercentEscaper;
import com.google.api.client.util.GenericData;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class GenericUrl extends GenericData {
    private static final Escaper URI_FRAGMENT_ESCAPER = new PercentEscaper("=&-_.!~*'()@:$,;/?:", false);
    public String fragment;
    public String host;
    public List<String> pathParts;
    public int port = -1;
    public String scheme;

    public GenericUrl() {
    }

    public GenericUrl(String encodedUrl) {
        try {
            URI uri = new URI(encodedUrl);
            this.scheme = uri.getScheme().toLowerCase();
            this.host = uri.getHost();
            this.port = uri.getPort();
            this.pathParts = toPathParts(uri.getRawPath());
            this.fragment = uri.getFragment();
            String query = uri.getRawQuery();
            if (query != null) {
                UrlEncodedParser.parse(query, this);
            }
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public int hashCode() {
        return build().hashCode();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj) || !(obj instanceof GenericUrl)) {
            return false;
        }
        return build().equals(((GenericUrl) obj).toString());
    }

    public String toString() {
        return build();
    }

    public GenericUrl clone() {
        GenericUrl result = (GenericUrl) super.clone();
        result.pathParts = new ArrayList(this.pathParts);
        return result;
    }

    public final String build() {
        StringBuilder buf = new StringBuilder();
        buf.append(this.scheme).append("://").append(this.host);
        int port2 = this.port;
        if (port2 != -1) {
            buf.append(':').append(port2);
        }
        if (this.pathParts != null) {
            appendRawPathFromParts(buf);
        }
        boolean first = true;
        for (Map.Entry<String, Object> nameValueEntry : entrySet()) {
            Object value = nameValueEntry.getValue();
            if (value != null) {
                String name = CharEscapers.escapeUriQuery((String) nameValueEntry.getKey());
                if (value instanceof Collection) {
                    for (Object repeatedValue : (Collection) value) {
                        first = appendParam(first, buf, name, repeatedValue);
                    }
                } else {
                    first = appendParam(first, buf, name, value);
                }
            }
        }
        String fragment2 = this.fragment;
        if (fragment2 != null) {
            buf.append('#').append(URI_FRAGMENT_ESCAPER.escape(fragment2));
        }
        return buf.toString();
    }

    public Object getFirst(String name) {
        Object value = get(name);
        if (!(value instanceof Collection)) {
            return value;
        }
        Iterator<Object> iterator = ((Collection) value).iterator();
        if (iterator.hasNext()) {
            return iterator.next();
        }
        return null;
    }

    public Collection<Object> getAll(String name) {
        Object value = get(name);
        if (value == null) {
            return Collections.emptySet();
        }
        if (value instanceof Collection) {
            return Collections.unmodifiableCollection((Collection) value);
        }
        return Collections.singleton(value);
    }

    public String getRawPath() {
        if (this.pathParts == null) {
            return null;
        }
        StringBuilder buf = new StringBuilder();
        appendRawPathFromParts(buf);
        return buf.toString();
    }

    public void setRawPath(String encodedPath) {
        this.pathParts = toPathParts(encodedPath);
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    public void appendRawPath(String encodedPath) {
        if (encodedPath != null && encodedPath.length() != 0) {
            List<String> pathParts2 = this.pathParts;
            List<String> appendedPathParts = toPathParts(encodedPath);
            if (pathParts2 == null || pathParts2.isEmpty()) {
                this.pathParts = appendedPathParts;
                return;
            }
            int size = pathParts2.size();
            pathParts2.set(size - 1, pathParts2.get(size - 1) + appendedPathParts.get(0));
            pathParts2.addAll(appendedPathParts.subList(1, appendedPathParts.size()));
        }
    }

    public static List<String> toPathParts(String encodedPath) {
        String sub;
        if (encodedPath == null || encodedPath.length() == 0) {
            return null;
        }
        List<String> result = new ArrayList<>();
        int cur = 0;
        boolean notDone = true;
        while (notDone) {
            int slash = encodedPath.indexOf(47, cur);
            notDone = slash != -1;
            if (notDone) {
                sub = encodedPath.substring(cur, slash);
            } else {
                sub = encodedPath.substring(cur);
            }
            result.add(CharEscapers.decodeUri(sub));
            cur = slash + 1;
        }
        return result;
    }

    private void appendRawPathFromParts(StringBuilder buf) {
        List<String> pathParts2 = this.pathParts;
        int size = pathParts2.size();
        for (int i = 0; i < size; i++) {
            String pathPart = pathParts2.get(i);
            if (i != 0) {
                buf.append('/');
            }
            if (pathPart.length() != 0) {
                buf.append(CharEscapers.escapeUriPath(pathPart));
            }
        }
    }

    private static boolean appendParam(boolean first, StringBuilder buf, String name, Object value) {
        if (first) {
            first = false;
            buf.append('?');
        } else {
            buf.append('&');
        }
        buf.append(name);
        String stringValue = CharEscapers.escapeUriQuery(value.toString());
        if (stringValue.length() != 0) {
            buf.append('=').append(stringValue);
        }
        return first;
    }
}
