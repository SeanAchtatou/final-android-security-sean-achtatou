package com.google.api.client.xml;

import com.google.api.client.util.DataUtil;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.FieldInfo;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import org.xmlpull.v1.XmlSerializer;

public final class XmlNamespaceDictionary {
    public final HashMap<String, String> namespaceAliasToUriMap = new HashMap<>();

    public void addNamespace(String alias, String uri) {
        if (alias == null || uri == null) {
            throw new NullPointerException();
        }
        HashMap<String, String> namespaceAliasToUriMap2 = this.namespaceAliasToUriMap;
        String knownUri = namespaceAliasToUriMap2.get(alias);
        if (uri.equals(knownUri)) {
            return;
        }
        if (knownUri != null) {
            throw new IllegalArgumentException("expected namespace alias <" + alias + "> to be <" + knownUri + "> but encountered <" + uri + ">");
        }
        namespaceAliasToUriMap2.put(alias, uri);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.xml.XmlNamespaceDictionary.serialize(org.xmlpull.v1.XmlSerializer, java.lang.String, java.lang.Object, boolean):void
     arg types: [org.xmlpull.v1.XmlSerializer, java.lang.String, java.lang.Object, int]
     candidates:
      com.google.api.client.xml.XmlNamespaceDictionary.serialize(org.xmlpull.v1.XmlSerializer, java.lang.String, java.lang.String, java.lang.Object):void
      com.google.api.client.xml.XmlNamespaceDictionary.serialize(org.xmlpull.v1.XmlSerializer, java.lang.String, java.lang.Object, boolean):void */
    public String toStringOf(String elementName, Object element) {
        try {
            StringWriter writer = new StringWriter();
            XmlSerializer serializer = Xml.createSerializer();
            serializer.setOutput(writer);
            serialize(serializer, elementName, element, false);
            return writer.toString();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public void serialize(XmlSerializer serializer, String elementNamespaceUri, String elementLocalName, Object element) throws IOException {
        serialize(serializer, elementNamespaceUri, elementLocalName, element, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.xml.XmlNamespaceDictionary.serialize(org.xmlpull.v1.XmlSerializer, java.lang.String, java.lang.Object, boolean):void
     arg types: [org.xmlpull.v1.XmlSerializer, java.lang.String, java.lang.Object, int]
     candidates:
      com.google.api.client.xml.XmlNamespaceDictionary.serialize(org.xmlpull.v1.XmlSerializer, java.lang.String, java.lang.String, java.lang.Object):void
      com.google.api.client.xml.XmlNamespaceDictionary.serialize(org.xmlpull.v1.XmlSerializer, java.lang.String, java.lang.Object, boolean):void */
    public void serialize(XmlSerializer serializer, String elementName, Object element) throws IOException {
        serialize(serializer, elementName, element, true);
    }

    private void serialize(XmlSerializer serializer, String elementNamespaceUri, String elementLocalName, Object element, boolean errorOnUnknown) throws IOException {
        startDoc(serializer, element, errorOnUnknown, elementNamespaceUri).serialize(serializer, elementNamespaceUri, elementLocalName);
        serializer.endDocument();
    }

    private void serialize(XmlSerializer serializer, String elementName, Object element, boolean errorOnUnknown) throws IOException {
        startDoc(serializer, element, errorOnUnknown, null).serialize(serializer, elementName);
        serializer.endDocument();
    }

    private ElementSerializer startDoc(XmlSerializer serializer, Object element, boolean errorOnUnknown, String extraNamespace) throws IOException {
        serializer.startDocument(null, null);
        SortedSet<String> aliases = new TreeSet<>();
        computeAliases(element, aliases);
        HashMap<String, String> namespaceAliasToUriMap2 = this.namespaceAliasToUriMap;
        boolean foundExtra = extraNamespace == null;
        for (String alias : aliases) {
            String uri = namespaceAliasToUriMap2.get(alias);
            serializer.setPrefix(alias, uri);
            if (!foundExtra && uri.equals(extraNamespace)) {
                foundExtra = true;
            }
        }
        if (!foundExtra) {
            Iterator i$ = namespaceAliasToUriMap2.entrySet().iterator();
            while (true) {
                if (!i$.hasNext()) {
                    break;
                }
                Map.Entry<String, String> entry = i$.next();
                if (extraNamespace.equals(entry.getValue())) {
                    serializer.setPrefix((String) entry.getKey(), extraNamespace);
                    break;
                }
            }
        }
        return new ElementSerializer(element, errorOnUnknown);
    }

    private void computeAliases(Object element, SortedSet<String> aliases) {
        String alias;
        for (Map.Entry<String, Object> entry : DataUtil.mapOf(element).entrySet()) {
            Object value = entry.getValue();
            if (value != null) {
                String name = (String) entry.getKey();
                if (!"text()".equals(name)) {
                    int colon = name.indexOf(58);
                    boolean isAttribute = name.charAt(0) == '@';
                    if (colon != -1 || !isAttribute) {
                        if (colon == -1) {
                            alias = "";
                        } else {
                            alias = name.substring(name.charAt(0) == '@' ? 1 : 0, colon);
                        }
                        aliases.add(alias);
                    }
                    if (!isAttribute && !FieldInfo.isPrimitive(value)) {
                        if (value instanceof Collection) {
                            for (Object subValue : (Collection) value) {
                                computeAliases(subValue, aliases);
                            }
                        } else {
                            computeAliases(value, aliases);
                        }
                    }
                }
            }
        }
    }

    class ElementSerializer {
        final List<String> attributeNames = new ArrayList();
        final List<Object> attributeValues = new ArrayList();
        private final boolean errorOnUnknown;
        final List<String> subElementNames = new ArrayList();
        final List<Object> subElementValues = new ArrayList();
        Object textValue = null;

        ElementSerializer(Object elementValue, boolean errorOnUnknown2) {
            this.errorOnUnknown = errorOnUnknown2;
            if (FieldInfo.isPrimitive(elementValue.getClass())) {
                this.textValue = elementValue;
                return;
            }
            for (Map.Entry<String, Object> entry : DataUtil.mapOf(elementValue).entrySet()) {
                Object fieldValue = entry.getValue();
                if (fieldValue != null) {
                    String fieldName = (String) entry.getKey();
                    if ("text()".equals(fieldName)) {
                        this.textValue = fieldValue;
                    } else if (fieldName.charAt(0) == '@') {
                        this.attributeNames.add(fieldName.substring(1));
                        this.attributeValues.add(fieldValue);
                    } else {
                        this.subElementNames.add(fieldName);
                        this.subElementValues.add(fieldValue);
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        public String getNamespaceUriForAlias(String alias) {
            String result = XmlNamespaceDictionary.this.namespaceAliasToUriMap.get(alias);
            if (result != null) {
                return result;
            }
            if (!this.errorOnUnknown) {
                return "http://unknown/" + alias;
            }
            throw new IllegalArgumentException("unrecognized alias: " + (alias.length() == 0 ? "(default)" : alias));
        }

        /* access modifiers changed from: package-private */
        public void serialize(XmlSerializer serializer, String elementName) throws IOException {
            String elementLocalName = null;
            String elementNamespaceUri = null;
            if (elementName != null) {
                int colon = elementName.indexOf(58);
                elementLocalName = elementName.substring(colon + 1);
                String alias = colon == -1 ? "" : elementName.substring(0, colon);
                elementNamespaceUri = getNamespaceUriForAlias(alias);
                if (elementNamespaceUri == null) {
                    elementNamespaceUri = "http://unknown/" + alias;
                }
            }
            serialize(serializer, elementNamespaceUri, elementLocalName);
        }

        /* access modifiers changed from: package-private */
        public void serialize(XmlSerializer serializer, String elementNamespaceUri, String elementLocalName) throws IOException {
            boolean errorOnUnknown2 = this.errorOnUnknown;
            if (elementLocalName == null) {
                if (errorOnUnknown2) {
                    throw new IllegalArgumentException("XML name not specified");
                }
                elementLocalName = "unknownName";
            }
            serializer.startTag(elementNamespaceUri, elementLocalName);
            List<String> attributeNames2 = this.attributeNames;
            List<Object> attributeValues2 = this.attributeValues;
            int num = attributeNames2.size();
            for (int i = 0; i < num; i++) {
                String attributeName = attributeNames2.get(i);
                int colon = attributeName.indexOf(58);
                serializer.attribute(colon == -1 ? null : getNamespaceUriForAlias(attributeName.substring(0, colon)), attributeName.substring(colon + 1), XmlNamespaceDictionary.toSerializedValue(attributeValues2.get(i)));
            }
            Object textValue2 = this.textValue;
            if (textValue2 != null) {
                serializer.text(XmlNamespaceDictionary.toSerializedValue(textValue2));
            }
            List<String> subElementNames2 = this.subElementNames;
            List<Object> subElementValues2 = this.subElementValues;
            int num2 = subElementNames2.size();
            for (int i2 = 0; i2 < num2; i2++) {
                Object subElementValue = subElementValues2.get(i2);
                String subElementName = subElementNames2.get(i2);
                if (subElementValue instanceof Collection) {
                    for (Object subElement : (Collection) subElementValue) {
                        new ElementSerializer(subElement, errorOnUnknown2).serialize(serializer, subElementName);
                    }
                } else {
                    new ElementSerializer(subElementValue, errorOnUnknown2).serialize(serializer, subElementName);
                }
            }
            serializer.endTag(elementNamespaceUri, elementLocalName);
        }
    }

    static String toSerializedValue(Object value) {
        if (value instanceof Float) {
            Float f = (Float) value;
            if (f.floatValue() == Float.POSITIVE_INFINITY) {
                return "INF";
            }
            if (f.floatValue() == Float.NEGATIVE_INFINITY) {
                return "-INF";
            }
        }
        if (value instanceof Double) {
            Double d = (Double) value;
            if (d.doubleValue() == Double.POSITIVE_INFINITY) {
                return "INF";
            }
            if (d.doubleValue() == Double.NEGATIVE_INFINITY) {
                return "-INF";
            }
        }
        if ((value instanceof String) || (value instanceof Number) || (value instanceof Boolean)) {
            return value.toString();
        }
        if (value instanceof DateTime) {
            return ((DateTime) value).toStringRfc3339();
        }
        throw new IllegalArgumentException("unrecognized value type: " + value.getClass());
    }
}
