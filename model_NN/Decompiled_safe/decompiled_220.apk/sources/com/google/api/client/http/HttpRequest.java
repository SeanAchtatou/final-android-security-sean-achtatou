package com.google.api.client.http;

import com.google.api.client.util.Strings;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class HttpRequest {
    private static final String USER_AGENT_SUFFIX = "Google-API-Java-Client/1.2.1-alpha";
    public HttpContent content;
    public boolean disableContentLogging;
    public HttpHeaders headers;
    public String method;
    public final HttpTransport transport;
    public GenericUrl url;

    HttpRequest(HttpTransport transport2, String method2) {
        this.transport = transport2;
        this.headers = transport2.defaultHeaders.clone();
        this.method = method2;
    }

    public void setUrl(String encodedUrl) {
        this.url = new GenericUrl(encodedUrl);
    }

    /* JADX WARNING: Removed duplicated region for block: B:72:0x0252  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.api.client.http.HttpResponse execute() throws java.io.IOException {
        /*
            r34 = this;
            r0 = r34
            java.lang.String r0 = r0.method
            r31 = r0
            com.google.api.client.repackaged.com.google.common.base.Preconditions.checkNotNull(r31)
            r0 = r34
            com.google.api.client.http.GenericUrl r0 = r0.url
            r31 = r0
            com.google.api.client.repackaged.com.google.common.base.Preconditions.checkNotNull(r31)
            r0 = r34
            com.google.api.client.http.HttpTransport r0 = r0.transport
            r27 = r0
            r0 = r27
            java.util.List<com.google.api.client.http.HttpExecuteIntercepter> r0 = r0.intercepters
            r31 = r0
            java.util.Iterator r14 = r31.iterator()
        L_0x0022:
            boolean r31 = r14.hasNext()
            if (r31 == 0) goto L_0x0036
            java.lang.Object r16 = r14.next()
            com.google.api.client.http.HttpExecuteIntercepter r16 = (com.google.api.client.http.HttpExecuteIntercepter) r16
            r0 = r16
            r1 = r34
            r0.intercept(r1)
            goto L_0x0022
        L_0x0036:
            com.google.api.client.http.LowLevelHttpTransport r21 = com.google.api.client.http.HttpTransport.useLowLevelHttpTransport()
            r0 = r34
            java.lang.String r0 = r0.method
            r23 = r0
            r0 = r34
            com.google.api.client.http.GenericUrl r0 = r0.url
            r28 = r0
            java.lang.String r29 = r28.build()
            java.lang.String r31 = "DELETE"
            r0 = r23
            r1 = r31
            boolean r31 = r0.equals(r1)
            if (r31 == 0) goto L_0x00ff
            r0 = r21
            r1 = r29
            com.google.api.client.http.LowLevelHttpRequest r20 = r0.buildDeleteRequest(r1)
        L_0x005e:
            java.util.logging.Logger r19 = com.google.api.client.http.HttpTransport.LOGGER
            java.util.logging.Level r31 = java.util.logging.Level.CONFIG
            r0 = r19
            r1 = r31
            boolean r18 = r0.isLoggable(r1)
            r17 = 0
            if (r18 == 0) goto L_0x009d
            java.lang.StringBuilder r17 = new java.lang.StringBuilder
            r17.<init>()
            java.lang.String r31 = "-------------- REQUEST  --------------"
            r0 = r17
            r1 = r31
            java.lang.StringBuilder r31 = r0.append(r1)
            java.lang.String r32 = com.google.api.client.util.Strings.LINE_SEPARATOR
            r31.append(r32)
            r0 = r17
            r1 = r23
            java.lang.StringBuilder r31 = r0.append(r1)
            r32 = 32
            java.lang.StringBuilder r31 = r31.append(r32)
            r0 = r31
            r1 = r29
            java.lang.StringBuilder r31 = r0.append(r1)
            java.lang.String r32 = com.google.api.client.util.Strings.LINE_SEPARATOR
            r31.append(r32)
        L_0x009d:
            r0 = r34
            com.google.api.client.http.HttpHeaders r0 = r0.headers
            r13 = r0
            r0 = r13
            java.lang.String r0 = r0.userAgent
            r31 = r0
            if (r31 != 0) goto L_0x01a6
            java.lang.String r31 = "Google-API-Java-Client/1.2.1-alpha"
            r0 = r31
            r1 = r13
            r1.userAgent = r0
        L_0x00b0:
            java.util.HashSet r12 = new java.util.HashSet
            r12.<init>()
            r0 = r34
            com.google.api.client.http.HttpHeaders r0 = r0.headers
            r31 = r0
            java.util.Set r31 = r31.entrySet()
            java.util.Iterator r14 = r31.iterator()
        L_0x00c3:
            boolean r31 = r14.hasNext()
            if (r31 == 0) goto L_0x0200
            java.lang.Object r11 = r14.next()
            java.util.Map$Entry r11 = (java.util.Map.Entry) r11
            java.lang.Object r24 = r11.getKey()
            java.lang.String r24 = (java.lang.String) r24
            java.lang.String r22 = r24.toLowerCase()
            r0 = r12
            r1 = r22
            boolean r31 = r0.add(r1)
            if (r31 != 0) goto L_0x01c5
            java.lang.IllegalArgumentException r31 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r32 = new java.lang.StringBuilder
            r32.<init>()
            java.lang.String r33 = "multiple headers of the same name (headers are case insensitive): "
            java.lang.StringBuilder r32 = r32.append(r33)
            r0 = r32
            r1 = r22
            java.lang.StringBuilder r32 = r0.append(r1)
            java.lang.String r32 = r32.toString()
            r31.<init>(r32)
            throw r31
        L_0x00ff:
            java.lang.String r31 = "GET"
            r0 = r23
            r1 = r31
            boolean r31 = r0.equals(r1)
            if (r31 == 0) goto L_0x0115
            r0 = r21
            r1 = r29
            com.google.api.client.http.LowLevelHttpRequest r20 = r0.buildGetRequest(r1)
            goto L_0x005e
        L_0x0115:
            java.lang.String r31 = "HEAD"
            r0 = r23
            r1 = r31
            boolean r31 = r0.equals(r1)
            if (r31 == 0) goto L_0x0139
            boolean r31 = r21.supportsHead()
            if (r31 != 0) goto L_0x012f
            java.lang.IllegalArgumentException r31 = new java.lang.IllegalArgumentException
            java.lang.String r32 = "HTTP transport doesn't support HEAD"
            r31.<init>(r32)
            throw r31
        L_0x012f:
            r0 = r21
            r1 = r29
            com.google.api.client.http.LowLevelHttpRequest r20 = r0.buildHeadRequest(r1)
            goto L_0x005e
        L_0x0139:
            java.lang.String r31 = "PATCH"
            r0 = r23
            r1 = r31
            boolean r31 = r0.equals(r1)
            if (r31 == 0) goto L_0x015d
            boolean r31 = r21.supportsPatch()
            if (r31 != 0) goto L_0x0153
            java.lang.IllegalArgumentException r31 = new java.lang.IllegalArgumentException
            java.lang.String r32 = "HTTP transport doesn't support PATCH"
            r31.<init>(r32)
            throw r31
        L_0x0153:
            r0 = r21
            r1 = r29
            com.google.api.client.http.LowLevelHttpRequest r20 = r0.buildPatchRequest(r1)
            goto L_0x005e
        L_0x015d:
            java.lang.String r31 = "POST"
            r0 = r23
            r1 = r31
            boolean r31 = r0.equals(r1)
            if (r31 == 0) goto L_0x0173
            r0 = r21
            r1 = r29
            com.google.api.client.http.LowLevelHttpRequest r20 = r0.buildPostRequest(r1)
            goto L_0x005e
        L_0x0173:
            java.lang.String r31 = "PUT"
            r0 = r23
            r1 = r31
            boolean r31 = r0.equals(r1)
            if (r31 == 0) goto L_0x0189
            r0 = r21
            r1 = r29
            com.google.api.client.http.LowLevelHttpRequest r20 = r0.buildPutRequest(r1)
            goto L_0x005e
        L_0x0189:
            java.lang.IllegalArgumentException r31 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r32 = new java.lang.StringBuilder
            r32.<init>()
            java.lang.String r33 = "illegal method: "
            java.lang.StringBuilder r32 = r32.append(r33)
            r0 = r32
            r1 = r23
            java.lang.StringBuilder r32 = r0.append(r1)
            java.lang.String r32 = r32.toString()
            r31.<init>(r32)
            throw r31
        L_0x01a6:
            java.lang.StringBuilder r31 = new java.lang.StringBuilder
            r31.<init>()
            r0 = r13
            java.lang.String r0 = r0.userAgent
            r32 = r0
            java.lang.StringBuilder r31 = r31.append(r32)
            java.lang.String r32 = " Google-API-Java-Client/1.2.1-alpha"
            java.lang.StringBuilder r31 = r31.append(r32)
            java.lang.String r31 = r31.toString()
            r0 = r31
            r1 = r13
            r1.userAgent = r0
            goto L_0x00b0
        L_0x01c5:
            java.lang.Object r30 = r11.getValue()
            if (r30 == 0) goto L_0x00c3
            r0 = r30
            boolean r0 = r0 instanceof java.util.Collection
            r31 = r0
            if (r31 == 0) goto L_0x01f1
            java.util.Collection r30 = (java.util.Collection) r30
            java.util.Iterator r15 = r30.iterator()
        L_0x01d9:
            boolean r31 = r15.hasNext()
            if (r31 == 0) goto L_0x00c3
            java.lang.Object r25 = r15.next()
            r0 = r19
            r1 = r17
            r2 = r20
            r3 = r24
            r4 = r25
            addHeader(r0, r1, r2, r3, r4)
            goto L_0x01d9
        L_0x01f1:
            r0 = r19
            r1 = r17
            r2 = r20
            r3 = r24
            r4 = r30
            addHeader(r0, r1, r2, r3, r4)
            goto L_0x00c3
        L_0x0200:
            r0 = r34
            com.google.api.client.http.HttpContent r0 = r0.content
            r6 = r0
            if (r6 == 0) goto L_0x02fb
            java.lang.String r8 = r6.getEncoding()
            long r9 = r6.getLength()
            java.lang.String r7 = r6.getType()
            r31 = 0
            int r31 = (r9 > r31 ? 1 : (r9 == r31 ? 0 : -1))
            if (r31 == 0) goto L_0x02f8
            if (r8 != 0) goto L_0x02f8
            boolean r31 = com.google.api.client.http.LogContent.isTextBasedContentType(r7)
            if (r31 == 0) goto L_0x02f8
            if (r18 == 0) goto L_0x022b
            r0 = r34
            boolean r0 = r0.disableContentLogging
            r31 = r0
            if (r31 == 0) goto L_0x0237
        L_0x022b:
            java.util.logging.Level r31 = java.util.logging.Level.ALL
            r0 = r19
            r1 = r31
            boolean r31 = r0.isLoggable(r1)
            if (r31 == 0) goto L_0x023d
        L_0x0237:
            com.google.api.client.http.LogContent r5 = new com.google.api.client.http.LogContent
            r5.<init>(r6, r7, r8, r9)
            r6 = r5
        L_0x023d:
            r31 = 256(0x100, double:1.265E-321)
            int r31 = (r9 > r31 ? 1 : (r9 == r31 ? 0 : -1))
            if (r31 < 0) goto L_0x02f8
            com.google.api.client.http.GZipContent r5 = new com.google.api.client.http.GZipContent
            r5.<init>(r6, r7)
            java.lang.String r8 = r5.getEncoding()
            long r9 = r5.getLength()
        L_0x0250:
            if (r18 == 0) goto L_0x02c3
            java.lang.StringBuilder r31 = new java.lang.StringBuilder
            r31.<init>()
            java.lang.String r32 = "Content-Type: "
            java.lang.StringBuilder r31 = r31.append(r32)
            r0 = r31
            r1 = r7
            java.lang.StringBuilder r31 = r0.append(r1)
            java.lang.String r31 = r31.toString()
            r0 = r17
            r1 = r31
            java.lang.StringBuilder r31 = r0.append(r1)
            java.lang.String r32 = com.google.api.client.util.Strings.LINE_SEPARATOR
            r31.append(r32)
            if (r8 == 0) goto L_0x029a
            java.lang.StringBuilder r31 = new java.lang.StringBuilder
            r31.<init>()
            java.lang.String r32 = "Content-Encoding: "
            java.lang.StringBuilder r31 = r31.append(r32)
            r0 = r31
            r1 = r8
            java.lang.StringBuilder r31 = r0.append(r1)
            java.lang.String r31 = r31.toString()
            r0 = r17
            r1 = r31
            java.lang.StringBuilder r31 = r0.append(r1)
            java.lang.String r32 = com.google.api.client.util.Strings.LINE_SEPARATOR
            r31.append(r32)
        L_0x029a:
            r31 = 0
            int r31 = (r9 > r31 ? 1 : (r9 == r31 ? 0 : -1))
            if (r31 < 0) goto L_0x02c3
            java.lang.StringBuilder r31 = new java.lang.StringBuilder
            r31.<init>()
            java.lang.String r32 = "Content-Length: "
            java.lang.StringBuilder r31 = r31.append(r32)
            r0 = r31
            r1 = r9
            java.lang.StringBuilder r31 = r0.append(r1)
            java.lang.String r31 = r31.toString()
            r0 = r17
            r1 = r31
            java.lang.StringBuilder r31 = r0.append(r1)
            java.lang.String r32 = com.google.api.client.util.Strings.LINE_SEPARATOR
            r31.append(r32)
        L_0x02c3:
            r0 = r20
            r1 = r5
            r0.setContent(r1)
        L_0x02c9:
            if (r18 == 0) goto L_0x02d6
            java.lang.String r31 = r17.toString()
            r0 = r19
            r1 = r31
            r0.config(r1)
        L_0x02d6:
            com.google.api.client.http.HttpResponse r26 = new com.google.api.client.http.HttpResponse
            com.google.api.client.http.LowLevelHttpResponse r31 = r20.execute()
            r0 = r26
            r1 = r27
            r2 = r31
            r0.<init>(r1, r2)
            r0 = r26
            boolean r0 = r0.isSuccessStatusCode
            r31 = r0
            if (r31 != 0) goto L_0x02f7
            com.google.api.client.http.HttpResponseException r31 = new com.google.api.client.http.HttpResponseException
            r0 = r31
            r1 = r26
            r0.<init>(r1)
            throw r31
        L_0x02f7:
            return r26
        L_0x02f8:
            r5 = r6
            goto L_0x0250
        L_0x02fb:
            r5 = r6
            goto L_0x02c9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.api.client.http.HttpRequest.execute():com.google.api.client.http.HttpResponse");
    }

    private static void addHeader(Logger logger, StringBuilder logbuf, LowLevelHttpRequest lowLevelHttpRequest, String name, Object value) {
        String stringValue = value.toString();
        if (logbuf != null) {
            logbuf.append(name).append(": ");
            if (!"Authorization".equals(name) || logger.isLoggable(Level.ALL)) {
                logbuf.append(stringValue);
            } else {
                logbuf.append("<Not Logged>");
            }
            logbuf.append(Strings.LINE_SEPARATOR);
        }
        lowLevelHttpRequest.addHeader(name, stringValue);
    }
}
