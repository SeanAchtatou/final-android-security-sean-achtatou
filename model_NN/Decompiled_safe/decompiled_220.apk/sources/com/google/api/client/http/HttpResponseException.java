package com.google.api.client.http;

import java.io.IOException;

public final class HttpResponseException extends IOException {
    static final long serialVersionUID = 1;
    public final HttpResponse response;

    public HttpResponseException(HttpResponse response2) {
        super(computeMessage(response2));
        this.response = response2;
    }

    public static String computeMessage(HttpResponse response2) {
        String statusMessage = response2.statusMessage;
        int statusCode = response2.statusCode;
        if (statusMessage == null) {
            return String.valueOf(statusCode);
        }
        return new StringBuilder(statusMessage.length() + 4).append(statusCode).append(' ').append(statusMessage).toString();
    }
}
