package com.google.api.client.http;

import com.google.api.client.util.ArrayMap;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

public final class HttpTransport {
    static final Logger LOGGER = Logger.getLogger(HttpTransport.class.getName());
    private static LowLevelHttpTransport lowLevelHttpTransport;
    private final ArrayMap<String, HttpParser> contentTypeToParserMap = ArrayMap.create();
    public HttpHeaders defaultHeaders = new HttpHeaders();
    public List<HttpExecuteIntercepter> intercepters = new ArrayList(1);

    public static void setLowLevelHttpTransport(LowLevelHttpTransport lowLevelHttpTransport2) {
        lowLevelHttpTransport = lowLevelHttpTransport2;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v9, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: com.google.api.client.http.LowLevelHttpTransport} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v13, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: com.google.api.client.http.LowLevelHttpTransport} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v20, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v4, resolved type: com.google.api.client.http.LowLevelHttpTransport} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.google.api.client.http.LowLevelHttpTransport useLowLevelHttpTransport() {
        /*
            com.google.api.client.http.LowLevelHttpTransport r4 = com.google.api.client.http.HttpTransport.lowLevelHttpTransport
            if (r4 != 0) goto L_0x004a
            java.lang.String r5 = "com.google.appengine.api.urlfetch.URLFetchServiceFactory"
            java.lang.Class.forName(r5)     // Catch:{ Exception -> 0x004b }
            java.lang.String r5 = "com.google.api.client.appengine.UrlFetchTransport"
            java.lang.Class r5 = java.lang.Class.forName(r5)     // Catch:{ Exception -> 0x004b }
            java.lang.String r6 = "INSTANCE"
            java.lang.reflect.Field r5 = r5.getField(r6)     // Catch:{ Exception -> 0x004b }
            r6 = 0
            java.lang.Object r5 = r5.get(r6)     // Catch:{ Exception -> 0x004b }
            r0 = r5
            com.google.api.client.http.LowLevelHttpTransport r0 = (com.google.api.client.http.LowLevelHttpTransport) r0     // Catch:{ Exception -> 0x004b }
            r4 = r0
            com.google.api.client.http.HttpTransport.lowLevelHttpTransport = r4     // Catch:{ Exception -> 0x004b }
        L_0x0020:
            java.util.logging.Logger r5 = com.google.api.client.http.HttpTransport.LOGGER
            java.util.logging.Level r6 = java.util.logging.Level.CONFIG
            boolean r5 = r5.isLoggable(r6)
            if (r5 == 0) goto L_0x004a
            java.util.logging.Logger r5 = com.google.api.client.http.HttpTransport.LOGGER
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "Using low-level HTTP transport: "
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.Class r7 = r4.getClass()
            java.lang.String r7 = r7.getName()
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r6 = r6.toString()
            r5.config(r6)
        L_0x004a:
            return r4
        L_0x004b:
            r5 = move-exception
            r1 = r5
            java.lang.String r5 = "org.apache.http.client.HttpClient"
            java.lang.Class.forName(r5)     // Catch:{ Exception -> 0x006a }
            java.lang.String r5 = "com.google.api.client.apache.ApacheHttpTransport"
            java.lang.Class r5 = java.lang.Class.forName(r5)     // Catch:{ Exception -> 0x006a }
            java.lang.String r6 = "INSTANCE"
            java.lang.reflect.Field r5 = r5.getField(r6)     // Catch:{ Exception -> 0x006a }
            r6 = 0
            java.lang.Object r5 = r5.get(r6)     // Catch:{ Exception -> 0x006a }
            r0 = r5
            com.google.api.client.http.LowLevelHttpTransport r0 = (com.google.api.client.http.LowLevelHttpTransport) r0     // Catch:{ Exception -> 0x006a }
            r4 = r0
            com.google.api.client.http.HttpTransport.lowLevelHttpTransport = r4     // Catch:{ Exception -> 0x006a }
            goto L_0x0020
        L_0x006a:
            r5 = move-exception
            r2 = r5
            java.lang.String r5 = "com.google.api.client.javanet.NetHttpTransport"
            java.lang.Class r5 = java.lang.Class.forName(r5)     // Catch:{ Exception -> 0x0084 }
            java.lang.String r6 = "INSTANCE"
            java.lang.reflect.Field r5 = r5.getField(r6)     // Catch:{ Exception -> 0x0084 }
            r6 = 0
            java.lang.Object r5 = r5.get(r6)     // Catch:{ Exception -> 0x0084 }
            r0 = r5
            com.google.api.client.http.LowLevelHttpTransport r0 = (com.google.api.client.http.LowLevelHttpTransport) r0     // Catch:{ Exception -> 0x0084 }
            r4 = r0
            com.google.api.client.http.HttpTransport.lowLevelHttpTransport = r4     // Catch:{ Exception -> 0x0084 }
            goto L_0x0020
        L_0x0084:
            r5 = move-exception
            r3 = r5
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r6 = "unable to load NetHttpTrasnport"
            r5.<init>(r6)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.api.client.http.HttpTransport.useLowLevelHttpTransport():com.google.api.client.http.LowLevelHttpTransport");
    }

    public void addParser(HttpParser parser) {
        this.contentTypeToParserMap.put(getNormalizedContentType(parser.getContentType()), parser);
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public HttpParser getParser(String contentType) {
        if (contentType == null) {
            return null;
        }
        return this.contentTypeToParserMap.get(getNormalizedContentType(contentType));
    }

    private String getNormalizedContentType(String contentType) {
        int semicolon = contentType.indexOf(59);
        return semicolon == -1 ? contentType : contentType.substring(0, semicolon);
    }

    public HttpTransport() {
        useLowLevelHttpTransport();
    }

    public HttpRequest buildRequest() {
        return new HttpRequest(this, null);
    }

    public HttpRequest buildDeleteRequest() {
        return new HttpRequest(this, "DELETE");
    }

    public HttpRequest buildGetRequest() {
        return new HttpRequest(this, "GET");
    }

    public HttpRequest buildPostRequest() {
        return new HttpRequest(this, "POST");
    }

    public HttpRequest buildPutRequest() {
        return new HttpRequest(this, "PUT");
    }

    public HttpRequest buildPatchRequest() {
        return new HttpRequest(this, "PATCH");
    }

    public HttpRequest buildHeadRequest() {
        return new HttpRequest(this, "HEAD");
    }

    public void removeIntercepters(Class<?> intercepterClass) {
        Iterator<HttpExecuteIntercepter> iterable = this.intercepters.iterator();
        while (iterable.hasNext()) {
            if (intercepterClass.isAssignableFrom(iterable.next().getClass())) {
                iterable.remove();
            }
        }
    }
}
