package com.google.api.client.googleapis;

import com.google.api.client.http.HttpExecuteIntercepter;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpTransport;
import java.util.HashSet;

public class MethodOverrideIntercepter implements HttpExecuteIntercepter {
    public static final HashSet<String> overriddenMethods = new HashSet<>();

    static {
        if (!HttpTransport.useLowLevelHttpTransport().supportsPatch()) {
            overriddenMethods.add("PATCH");
        }
        if (!HttpTransport.useLowLevelHttpTransport().supportsHead()) {
            overriddenMethods.add("HEAD");
        }
    }

    public void intercept(HttpRequest request) {
        String method = request.method;
        if (overriddenMethods.contains(method)) {
            request.method = "POST";
            request.headers.set("X-HTTP-Method-Override", method);
        }
    }

    public static void setAsFirstFor(HttpTransport transport) {
        transport.removeIntercepters(MethodOverrideIntercepter.class);
        transport.intercepters.add(0, new MethodOverrideIntercepter());
    }
}
