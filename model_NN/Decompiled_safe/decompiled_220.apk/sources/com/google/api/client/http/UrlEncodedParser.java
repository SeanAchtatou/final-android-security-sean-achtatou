package com.google.api.client.http;

import com.google.api.client.escape.CharEscapers;
import com.google.api.client.util.ClassInfo;
import com.google.api.client.util.FieldInfo;
import com.google.api.client.util.GenericData;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

public final class UrlEncodedParser implements HttpParser {
    public static final String CONTENT_TYPE = "application/x-www-form-urlencoded";
    public String contentType = CONTENT_TYPE;
    public boolean disableContentLogging;

    public String getContentType() {
        return this.contentType;
    }

    public <T> T parse(HttpResponse response, Class cls) throws IOException {
        if (this.disableContentLogging) {
            response.disableContentLogging = true;
        }
        T newInstance = ClassInfo.newInstance(cls);
        parse(response.parseAsString(), newInstance);
        return newInstance;
    }

    /* JADX INFO: Multiple debug info for r1v7 int: [D('amp' int), D('cur' int)] */
    /* JADX INFO: Multiple debug info for r8v4 java.lang.Class<?>: [D('type' java.lang.Class<?>), D('name' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r4v5 java.lang.Class<?>: [D('fieldInfo' com.google.api.client.util.FieldInfo), D('subFieldClass' java.lang.Class<?>)] */
    public static void parse(String content, Object data) {
        Map<Object, Object> map;
        int amp;
        String name;
        String stringValue;
        int nextEquals;
        Class<?> clazz = data.getClass();
        ClassInfo classInfo = ClassInfo.of(clazz);
        GenericData genericData = GenericData.class.isAssignableFrom(clazz) ? (GenericData) data : null;
        if (Map.class.isAssignableFrom(clazz)) {
            map = (Map) data;
        } else {
            map = null;
        }
        int length = content.length();
        int nextEquals2 = content.indexOf(61);
        for (int cur = 0; cur < length; cur = amp + 1) {
            amp = content.indexOf(38, cur);
            if (amp == -1) {
                amp = length;
            }
            if (nextEquals2 == -1 || nextEquals2 >= amp) {
                name = content.substring(cur, amp);
                stringValue = "";
                nextEquals = nextEquals2;
            } else {
                name = content.substring(cur, nextEquals2);
                stringValue = CharEscapers.decodeUri(content.substring(nextEquals2 + 1, amp));
                nextEquals = content.indexOf(61, amp + 1);
            }
            String name2 = CharEscapers.decodeUri(name);
            FieldInfo fieldInfo = classInfo.getFieldInfo(name2);
            if (fieldInfo != null) {
                Class<?> type = fieldInfo.type;
                if (Collection.class.isAssignableFrom(type)) {
                    Collection<Object> collection = (Collection) fieldInfo.getValue(data);
                    if (collection == null) {
                        collection = ClassInfo.newCollectionInstance(type);
                        fieldInfo.setValue(data, collection);
                    }
                    collection.add(FieldInfo.parsePrimitiveValue(ClassInfo.getCollectionParameter(fieldInfo.field), stringValue));
                } else {
                    fieldInfo.setValue(data, FieldInfo.parsePrimitiveValue(type, stringValue));
                }
            } else {
                ArrayList<String> listValue = (ArrayList) map.get(name2);
                if (listValue == null) {
                    listValue = new ArrayList<>();
                    if (genericData != null) {
                        genericData.set(name2, listValue);
                    } else {
                        map.put(name2, listValue);
                    }
                }
                listValue.add(stringValue);
            }
            nextEquals2 = nextEquals;
        }
    }
}
