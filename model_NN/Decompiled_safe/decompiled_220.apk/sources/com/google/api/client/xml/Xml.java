package com.google.api.client.xml;

import com.google.api.client.util.ClassInfo;
import com.google.api.client.util.FieldInfo;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public class Xml {
    public static XmlParserFactory parserFactory;

    private static XmlParserFactory getParserFactory() throws XmlPullParserException {
        XmlParserFactory parserFactory2 = parserFactory;
        if (parserFactory2 != null) {
            return parserFactory2;
        }
        XmlParserFactory parserFactory3 = DefaultXmlParserFactory.getInstance();
        parserFactory = parserFactory3;
        return parserFactory3;
    }

    public static XmlSerializer createSerializer() {
        try {
            return getParserFactory().createSerializer();
        } catch (XmlPullParserException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static XmlPullParser createParser() throws XmlPullParserException {
        XmlPullParser result = getParserFactory().createParser();
        if (result.getFeature("http://xmlpull.org/v1/doc/features.html#process-namespaces")) {
            return result;
        }
        throw new IllegalStateException("XML pull parser must have namespace-aware feature");
    }

    public static String toStringOf(Object element) {
        return new XmlNamespaceDictionary().toStringOf(null, element);
    }

    private static void parseValue(String stringValue, Field field, Object destination, GenericXml genericXml, Map<String, Object> destinationMap, String name) {
        if (field != null) {
            Class<?> fieldClass = field.getType();
            if (!Modifier.isFinal(field.getModifiers()) || FieldInfo.isPrimitive(fieldClass)) {
                FieldInfo.setFieldValue(field, destination, parseValue(stringValue, fieldClass));
                return;
            }
            throw new IllegalArgumentException("final sub-element fields are not supported");
        } else if (genericXml != null) {
            genericXml.set(name, parseValue(stringValue, null));
        } else if (destinationMap != null) {
            destinationMap.put(name, parseValue(stringValue, null));
        }
    }

    public static class CustomizeParser {
        public boolean stopBeforeStartTag(String namespace, String localName) {
            return false;
        }

        public boolean stopAfterEndTag(String namespace, String localName) {
            return false;
        }
    }

    public static void parseElement(XmlPullParser parser, Object destination, XmlNamespaceDictionary namespaceDictionary, CustomizeParser customizeParser) throws IOException, XmlPullParserException {
        parseElementInternal(parser, destination, namespaceDictionary, customizeParser);
    }

    /* JADX INFO: Multiple debug info for r4v6 int: [D('attributeName' java.lang.String), D('event' int)] */
    /* JADX INFO: Multiple debug info for r10v1 java.util.Map<java.lang.String, java.lang.Object>: [D('isStopped' boolean), D('mapValue' java.util.Map<java.lang.String, java.lang.Object>)] */
    /* JADX INFO: Multiple debug info for r6v5 boolean: [D('fieldClass' java.lang.Class<?>), D('isStopped' boolean)] */
    /* JADX INFO: Multiple debug info for r5v11 java.lang.Object: [D('field' java.lang.reflect.Field), D('list' java.util.Collection<java.lang.Object>)] */
    /* JADX INFO: Multiple debug info for r4v21 java.util.Collection<java.lang.Object>: [D('atom' com.google.api.client.xml.GenericXml), D('list' java.util.Collection<java.lang.Object>)] */
    /* JADX INFO: Multiple debug info for r6v6 java.lang.Object: [D('fieldClass' java.lang.Class<?>), D('value' java.lang.Object)] */
    /* JADX INFO: Multiple debug info for r9v2 java.lang.Class<?>: [D('subFieldClass' java.lang.Class<?>), D('fieldName' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r5v35 java.lang.reflect.Field: [D('attributeNamespace' java.lang.String), D('field' java.lang.reflect.Field)] */
    /* JADX INFO: Multiple debug info for r4v57 java.lang.String: [D('alias' java.lang.String), D('name' java.lang.String)] */
    private static boolean parseElementInternal(XmlPullParser parser, Object destination, XmlNamespaceDictionary namespaceDictionary, CustomizeParser customizeParser) throws IOException, XmlPullParserException {
        String alias;
        boolean isStopped;
        int level;
        Object obj;
        boolean isStopped2;
        Object parseValue;
        int level2;
        Field field;
        Class<?> destinationClass = destination == null ? null : destination.getClass();
        GenericXml genericXml = destination instanceof GenericXml ? (GenericXml) destination : null;
        boolean isMap = genericXml == null && (destination instanceof Map);
        Map<String, Object> destinationMap = isMap ? (Map) destination : null;
        ClassInfo classInfo = (isMap || destination == null) ? null : ClassInfo.of(destinationClass);
        int eventType = parser.getEventType();
        if (parser.getEventType() == 0) {
            eventType = parser.next();
        }
        if (eventType == 2) {
            String prefix = parser.getPrefix();
            if (prefix == null) {
                alias = "";
            } else {
                alias = prefix;
            }
            namespaceDictionary.addNamespace(alias, parser.getNamespace());
            if (genericXml != null) {
                genericXml.namespaceDictionary = namespaceDictionary;
                String name = parser.getName();
                if (prefix != null) {
                    name = prefix + ":" + name;
                }
                genericXml.name = name;
            }
            if (destination != null) {
                int attributeCount = parser.getAttributeCount();
                for (int i = 0; i < attributeCount; i++) {
                    String attributeName = parser.getAttributeName(i);
                    String attributePrefix = parser.getAttributePrefix(i);
                    String attributeNamespace = parser.getAttributeNamespace(i);
                    if (attributePrefix != null) {
                        namespaceDictionary.addNamespace(attributePrefix, attributeNamespace);
                    }
                    String fieldName = getFieldName(true, attributePrefix, attributeNamespace, attributeName);
                    if (isMap) {
                        field = null;
                    } else {
                        field = classInfo.getField(fieldName);
                    }
                    parseValue(parser.getAttributeValue(i), field, destination, genericXml, destinationMap, fieldName);
                }
            }
            while (true) {
                switch (parser.next()) {
                    case 1:
                        return true;
                    case 2:
                        if (customizeParser != null) {
                            if (customizeParser.stopBeforeStartTag(parser.getNamespace(), parser.getName())) {
                                return true;
                            }
                        }
                        if (destination != null) {
                            String fieldName2 = getFieldName(false, parser.getPrefix(), parser.getNamespace(), parser.getName());
                            Field field2 = isMap ? null : classInfo.getField(fieldName2);
                            Class<?> fieldClass = field2 == null ? null : field2.getType();
                            if ((field2 == null && !isMap && genericXml == null) || (field2 != null && FieldInfo.isPrimitive(fieldClass))) {
                                int level3 = 1;
                                while (level3 != 0) {
                                    switch (parser.next()) {
                                        case 1:
                                            return true;
                                        case 2:
                                            level = level3 + 1;
                                            break;
                                        case 3:
                                            level = level3 - 1;
                                            break;
                                        case 4:
                                            if (level3 == 1) {
                                                parseValue(parser.getText(), field2, destination, genericXml, destinationMap, fieldName2);
                                            }
                                        default:
                                            level = level3;
                                            break;
                                    }
                                    level3 = level;
                                }
                                isStopped = false;
                            } else if (field2 == null || Map.class.isAssignableFrom(fieldClass)) {
                                Map<String, Object> mapValue = ClassInfo.newMapInstance(fieldClass);
                                boolean isStopped3 = parseElementInternal(parser, mapValue, namespaceDictionary, customizeParser);
                                if (isMap) {
                                    Collection<Object> list = (Collection) destinationMap.get(fieldName2);
                                    if (list == null) {
                                        list = new ArrayList<>(1);
                                        destinationMap.put(fieldName2, list);
                                    }
                                    list.add(mapValue);
                                } else if (field2 != null) {
                                    FieldInfo.setFieldValue(field2, destination, mapValue);
                                } else {
                                    GenericXml atom = (GenericXml) destination;
                                    Collection<Object> list2 = (Collection) atom.get(fieldName2);
                                    if (list2 == null) {
                                        list2 = new ArrayList<>(1);
                                        atom.set(fieldName2, list2);
                                    }
                                    list2.add(mapValue);
                                }
                                isStopped = isStopped3;
                            } else if (Collection.class.isAssignableFrom(fieldClass)) {
                                Collection<Object> collectionValue = (Collection) FieldInfo.getFieldValue(field2, destination);
                                if (collectionValue == null) {
                                    collectionValue = ClassInfo.newCollectionInstance(fieldClass);
                                    FieldInfo.setFieldValue(field2, destination, collectionValue);
                                }
                                Class<?> subFieldClass = ClassInfo.getCollectionParameter(field2);
                                if (subFieldClass == null || FieldInfo.isPrimitive(subFieldClass)) {
                                    Object obj2 = null;
                                    int level4 = 1;
                                    while (level4 != 0) {
                                        switch (parser.next()) {
                                            case 1:
                                                return true;
                                            case 2:
                                                level2 = level4 + 1;
                                                parseValue = obj2;
                                                break;
                                            case 3:
                                                level2 = level4 - 1;
                                                parseValue = obj2;
                                                break;
                                            case 4:
                                                if (level4 == 1 && subFieldClass != null) {
                                                    int level5 = level4;
                                                    parseValue = parseValue(parser.getText(), subFieldClass);
                                                    level2 = level5;
                                                    break;
                                                }
                                            default:
                                                level2 = level4;
                                                parseValue = obj2;
                                                break;
                                        }
                                        obj2 = parseValue;
                                        level4 = level2;
                                    }
                                    obj = obj2;
                                    isStopped2 = false;
                                } else {
                                    Object elementValue = ClassInfo.newInstance(subFieldClass);
                                    Object obj3 = elementValue;
                                    isStopped2 = parseElementInternal(parser, elementValue, namespaceDictionary, customizeParser);
                                    obj = obj3;
                                }
                                collectionValue.add(obj);
                                isStopped = isStopped2;
                            } else {
                                Object value = ClassInfo.newInstance(fieldClass);
                                isStopped = parseElementInternal(parser, value, namespaceDictionary, customizeParser);
                                FieldInfo.setFieldValue(field2, destination, value);
                            }
                            if (!isStopped) {
                                break;
                            } else {
                                return true;
                            }
                        } else {
                            int level6 = 1;
                            while (level6 != 0) {
                                switch (parser.next()) {
                                    case 1:
                                        return true;
                                    case 2:
                                        level6++;
                                        break;
                                    case 3:
                                        level6--;
                                        break;
                                }
                            }
                            continue;
                        }
                        break;
                    case 3:
                        if (customizeParser != null) {
                            if (customizeParser.stopAfterEndTag(parser.getNamespace(), parser.getName())) {
                                return true;
                            }
                        }
                        return false;
                    case 4:
                        if (destination != null) {
                            parseValue(parser.getText(), isMap ? null : classInfo.getField("text()"), destination, genericXml, destinationMap, "text()");
                            break;
                        } else {
                            break;
                        }
                }
            }
        } else {
            throw new IllegalArgumentException("expected start of XML element, but got something else (event type " + eventType + ")");
        }
    }

    private static String getFieldName(boolean isAttribute, String alias, String namespace, String name) {
        if (alias == null) {
            alias = "";
        }
        StringBuilder buf = new StringBuilder(alias.length() + 2 + name.length());
        if (isAttribute) {
            buf.append('@');
        }
        if (alias != "") {
            buf.append(alias).append(':');
        }
        return buf.append(name).toString();
    }

    private static Object parseValue(String stringValue, Class<?> fieldClass) {
        if (fieldClass == Double.class || fieldClass == Double.TYPE) {
            if (stringValue.equals("INF")) {
                return new Double(Double.POSITIVE_INFINITY);
            }
            if (stringValue.equals("-INF")) {
                return new Double(Double.NEGATIVE_INFINITY);
            }
        }
        if (fieldClass == Float.class || fieldClass == Float.TYPE) {
            if (stringValue.equals("INF")) {
                return Float.valueOf(Float.POSITIVE_INFINITY);
            }
            if (stringValue.equals("-INF")) {
                return Float.valueOf(Float.NEGATIVE_INFINITY);
            }
        }
        return FieldInfo.parsePrimitiveValue(fieldClass, stringValue);
    }

    private Xml() {
    }
}
