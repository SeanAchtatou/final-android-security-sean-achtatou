package com.google.api.client.http;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

final class LogContent implements HttpContent {
    private final String contentEncoding;
    private final long contentLength;
    private final String contentType;
    private final HttpContent httpContent;

    LogContent(HttpContent httpContent2, String contentType2, String contentEncoding2, long contentLength2) {
        this.httpContent = httpContent2;
        this.contentType = contentType2;
        this.contentLength = contentLength2;
        this.contentEncoding = contentEncoding2;
    }

    public void writeTo(OutputStream out) throws IOException {
        ByteArrayOutputStream debugStream = new ByteArrayOutputStream();
        this.httpContent.writeTo(debugStream);
        byte[] debugContent = debugStream.toByteArray();
        HttpTransport.LOGGER.config(new String(debugContent));
        out.write(debugContent);
    }

    public String getEncoding() {
        return this.contentEncoding;
    }

    public long getLength() {
        return this.contentLength;
    }

    public String getType() {
        return this.contentType;
    }

    public static boolean isTextBasedContentType(String contentType2) {
        return contentType2 != null && (contentType2.startsWith("text/") || contentType2.startsWith("application/"));
    }
}
