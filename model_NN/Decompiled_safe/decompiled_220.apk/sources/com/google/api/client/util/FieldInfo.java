package com.google.api.client.util;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.WeakHashMap;

public class FieldInfo {
    private static final ThreadLocal<WeakHashMap<Field, FieldInfo>> CACHE = new ThreadLocal<WeakHashMap<Field, FieldInfo>>() {
        /* access modifiers changed from: protected */
        public WeakHashMap<Field, FieldInfo> initialValue() {
            return new WeakHashMap<>();
        }
    };
    public final Field field;
    public final boolean isFinal;
    public final boolean isPrimitive;
    public final String name;
    public final Class<?> type;

    public static FieldInfo of(Field field2) {
        if (field2 == null) {
            return null;
        }
        WeakHashMap<Field, FieldInfo> cache = CACHE.get();
        FieldInfo fieldInfo = (FieldInfo) cache.get(field2);
        if (fieldInfo == null && !Modifier.isStatic(field2.getModifiers())) {
            Key key = (Key) field2.getAnnotation(Key.class);
            if (key == null) {
                return null;
            }
            String fieldName = key.value();
            if ("##default".equals(fieldName)) {
                fieldName = field2.getName();
            }
            fieldInfo = new FieldInfo(field2, fieldName);
            cache.put(field2, fieldInfo);
            field2.setAccessible(true);
        }
        return fieldInfo;
    }

    FieldInfo(Field field2, String name2) {
        this.field = field2;
        this.name = name2.intern();
        this.isFinal = Modifier.isFinal(field2.getModifiers());
        Class<?> type2 = field2.getType();
        this.type = type2;
        this.isPrimitive = isPrimitive(type2);
    }

    public Object getValue(Object obj) {
        return getFieldValue(this.field, obj);
    }

    public void setValue(Object obj, Object value) {
        setFieldValue(this.field, obj, value);
    }

    public ClassInfo getClassInfo() {
        return ClassInfo.of(this.field.getDeclaringClass());
    }

    public static boolean isPrimitive(Class<?> fieldClass) {
        return fieldClass.isPrimitive() || fieldClass == Character.class || fieldClass == String.class || fieldClass == Integer.class || fieldClass == Long.class || fieldClass == Short.class || fieldClass == Byte.class || fieldClass == Float.class || fieldClass == Double.class || fieldClass == BigInteger.class || fieldClass == BigDecimal.class || fieldClass == DateTime.class || fieldClass == Boolean.class;
    }

    public static boolean isPrimitive(Object fieldValue) {
        return fieldValue == null || isPrimitive(fieldValue.getClass());
    }

    public static Object getFieldValue(Field field2, Object obj) {
        try {
            return field2.get(obj);
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static void setFieldValue(Field field2, Object obj, Object value) {
        if (Modifier.isFinal(field2.getModifiers())) {
            Object finalValue = getFieldValue(field2, obj);
            if (value == null) {
                if (finalValue == null) {
                    return;
                }
            } else if (value.equals(finalValue)) {
                return;
            }
            throw new IllegalArgumentException("expected final value <" + finalValue + "> but was <" + value + "> on " + field2.getName() + " field in " + obj.getClass().getName());
        }
        try {
            field2.set(obj, value);
        } catch (SecurityException e) {
            throw new IllegalArgumentException(e);
        } catch (IllegalAccessException e2) {
            throw new IllegalArgumentException(e2);
        }
    }

    public static Object parsePrimitiveValue(Class<?> primitiveClass, String stringValue) {
        if (stringValue == null || primitiveClass == null || primitiveClass == String.class) {
            return stringValue;
        }
        if (primitiveClass == Character.class || primitiveClass == Character.TYPE) {
            if (stringValue.length() == 1) {
                return Character.valueOf(stringValue.charAt(0));
            }
            throw new IllegalArgumentException("expected type Character/char but got " + primitiveClass);
        } else if (primitiveClass == Boolean.class || primitiveClass == Boolean.TYPE) {
            return Boolean.valueOf(stringValue);
        } else {
            if (primitiveClass == Byte.class || primitiveClass == Byte.TYPE) {
                return Byte.valueOf(stringValue);
            }
            if (primitiveClass == Short.class || primitiveClass == Short.TYPE) {
                return Short.valueOf(stringValue);
            }
            if (primitiveClass == Integer.class || primitiveClass == Integer.TYPE) {
                return Integer.valueOf(stringValue);
            }
            if (primitiveClass == Long.class || primitiveClass == Long.TYPE) {
                return Long.valueOf(stringValue);
            }
            if (primitiveClass == Float.class || primitiveClass == Float.TYPE) {
                return Float.valueOf(stringValue);
            }
            if (primitiveClass == Double.class || primitiveClass == Double.TYPE) {
                return Double.valueOf(stringValue);
            }
            if (primitiveClass == DateTime.class) {
                return DateTime.parseRfc3339(stringValue);
            }
            if (primitiveClass == BigInteger.class) {
                return new BigInteger(stringValue);
            }
            if (primitiveClass == BigDecimal.class) {
                return new BigDecimal(stringValue);
            }
            throw new IllegalArgumentException("expected primitive class, but got: " + primitiveClass);
        }
    }
}
