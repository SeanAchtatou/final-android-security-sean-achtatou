package com.google.api.client.http;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public final class InputStreamContent implements HttpContent {
    private static final int BUFFER_SIZE = 2048;
    public String encoding;
    public InputStream inputStream;
    public long length = -1;
    public String type;

    public void setFileInput(File file) throws FileNotFoundException {
        this.inputStream = new FileInputStream(file);
        this.length = file.length();
    }

    public void setByteArrayInput(byte[] content) {
        this.inputStream = new ByteArrayInputStream(content);
        this.length = (long) content.length;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public void writeTo(OutputStream out) throws IOException {
        InputStream inputStream2 = this.inputStream;
        long contentLength = this.length;
        if (contentLength < 0) {
            copy(inputStream2, out);
            return;
        }
        byte[] buffer = new byte[2048];
        long remaining = contentLength;
        while (remaining > 0) {
            try {
                int read = inputStream2.read(buffer, 0, (int) Math.min(2048L, remaining));
                if (read == -1) {
                    break;
                }
                out.write(buffer, 0, read);
                remaining -= (long) read;
            } catch (Throwable th) {
                inputStream2.close();
                throw th;
            }
        }
        inputStream2.close();
    }

    public String getEncoding() {
        return this.encoding;
    }

    public long getLength() {
        return this.length;
    }

    public String getType() {
        return this.type;
    }

    public static void copy(InputStream inputStream2, OutputStream outputStream) throws IOException {
        try {
            byte[] tmp = new byte[2048];
            while (true) {
                int bytesRead = inputStream2.read(tmp);
                if (bytesRead != -1) {
                    outputStream.write(tmp, 0, bytesRead);
                } else {
                    return;
                }
            }
        } finally {
            inputStream2.close();
        }
    }
}
