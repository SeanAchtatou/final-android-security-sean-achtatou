package com.google.api.client.util;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class DateTime {
    private static final TimeZone GMT = TimeZone.getTimeZone("GMT");
    public final boolean dateOnly;
    public final Integer tzShift;
    public final long value;

    public DateTime(Date date, TimeZone zone) {
        long value2 = date.getTime();
        this.dateOnly = false;
        this.value = value2;
        this.tzShift = Integer.valueOf(zone.getOffset(value2) / 60000);
    }

    public DateTime(long value2) {
        this(false, value2, null);
    }

    public DateTime(Date value2) {
        this(value2.getTime());
    }

    public DateTime(long value2, Integer tzShift2) {
        this(false, value2, tzShift2);
    }

    public DateTime(boolean dateOnly2, long value2, Integer tzShift2) {
        this.dateOnly = dateOnly2;
        this.value = value2;
        this.tzShift = tzShift2;
    }

    public String toStringRfc3339() {
        StringBuilder sb = new StringBuilder();
        Calendar dateTime = new GregorianCalendar(GMT);
        long localTime = this.value;
        Integer tzShift2 = this.tzShift;
        if (tzShift2 != null) {
            localTime += tzShift2.longValue() * 60000;
        }
        dateTime.setTimeInMillis(localTime);
        appendInt(sb, dateTime.get(1), 4);
        sb.append('-');
        appendInt(sb, dateTime.get(2) + 1, 2);
        sb.append('-');
        appendInt(sb, dateTime.get(5), 2);
        if (!this.dateOnly) {
            sb.append('T');
            appendInt(sb, dateTime.get(11), 2);
            sb.append(':');
            appendInt(sb, dateTime.get(12), 2);
            sb.append(':');
            appendInt(sb, dateTime.get(13), 2);
            if (dateTime.isSet(14)) {
                sb.append('.');
                appendInt(sb, dateTime.get(14), 3);
            }
        }
        if (tzShift2 != null) {
            if (tzShift2.intValue() == 0) {
                sb.append('Z');
            } else {
                int absTzShift = tzShift2.intValue();
                if (tzShift2.intValue() > 0) {
                    sb.append('+');
                } else {
                    sb.append('-');
                    absTzShift = -absTzShift;
                }
                appendInt(sb, absTzShift / 60, 2);
                sb.append(':');
                appendInt(sb, absTzShift % 60, 2);
            }
        }
        return sb.toString();
    }

    public String toString() {
        return toStringRfc3339();
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof DateTime)) {
            return false;
        }
        DateTime other = (DateTime) o;
        return this.dateOnly == other.dateOnly && this.value == other.value;
    }

    /* JADX INFO: Multiple debug info for r3v5 long: [D('value' long), D('day' int)] */
    public static DateTime parseRfc3339(String str) throws NumberFormatException {
        int year;
        long value2;
        Integer tzShiftInteger;
        int tzShift2;
        try {
            Calendar dateTime = new GregorianCalendar(GMT);
            int year2 = Integer.parseInt(str.substring(0, 4));
            int month = Integer.parseInt(str.substring(5, 7)) - 1;
            int day = Integer.parseInt(str.substring(8, 10));
            int length = str.length();
            boolean dateOnly2 = length <= 10 || Character.toUpperCase(str.charAt(10)) != 'T';
            if (dateOnly2) {
                dateTime.set(year2, month, day);
                year = 10;
            } else {
                dateTime.set(year2, month, day, Integer.parseInt(str.substring(11, 13)), Integer.parseInt(str.substring(14, 16)), Integer.parseInt(str.substring(17, 19)));
                if (str.charAt(19) == 46) {
                    dateTime.set(14, Integer.parseInt(str.substring(20, 23)));
                    year = 23;
                } else {
                    year = 19;
                }
            }
            long value3 = dateTime.getTimeInMillis();
            if (length > year) {
                if (Character.toUpperCase(str.charAt(year)) == 'Z') {
                    tzShift2 = 0;
                    value2 = value3;
                } else {
                    int tzShift3 = (Integer.parseInt(str.substring(year + 1, year + 3)) * 60) + Integer.parseInt(str.substring(year + 4, year + 6));
                    if (str.charAt(year) == '-') {
                        tzShift2 = -tzShift3;
                    } else {
                        tzShift2 = tzShift3;
                    }
                    value2 = value3 - ((long) (60000 * tzShift2));
                }
                tzShiftInteger = Integer.valueOf(tzShift2);
            } else {
                value2 = value3;
                tzShiftInteger = null;
            }
            return new DateTime(dateOnly2, value2, tzShiftInteger);
        } catch (StringIndexOutOfBoundsException e) {
            throw new NumberFormatException("Invalid date/time format.");
        }
    }

    private static void appendInt(StringBuilder sb, int num, int numDigits) {
        if (num < 0) {
            sb.append('-');
            num = -num;
        }
        int x = num;
        while (x > 0) {
            x /= 10;
            numDigits--;
        }
        for (int i = 0; i < numDigits; i++) {
            sb.append('0');
        }
        if (num != 0) {
            sb.append(num);
        }
    }
}
