package com.google.api.client.xml;

import com.google.api.client.http.HttpContent;
import java.io.IOException;
import java.io.OutputStream;
import org.xmlpull.v1.XmlSerializer;

public abstract class AbstractXmlHttpContent implements HttpContent {
    public String contentType = XmlHttpParser.CONTENT_TYPE;
    public XmlNamespaceDictionary namespaceDictionary;

    /* access modifiers changed from: protected */
    public abstract void writeTo(XmlSerializer xmlSerializer) throws IOException;

    public String getEncoding() {
        return null;
    }

    public long getLength() {
        return -1;
    }

    public final String getType() {
        return this.contentType;
    }

    public final void writeTo(OutputStream out) throws IOException {
        XmlSerializer serializer = Xml.createSerializer();
        serializer.setOutput(out, "UTF-8");
        writeTo(serializer);
    }
}
