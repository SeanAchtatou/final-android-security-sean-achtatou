package com.google.api.client.util;

import java.io.UnsupportedEncodingException;

public class Strings {
    public static final String LINE_SEPARATOR = System.getProperty("line.separator");

    public static byte[] toBytesUtf8(String string) {
        try {
            return string.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String fromBytesUtf8(byte[] bytes) {
        try {
            return new String(bytes, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private Strings() {
    }
}
