package com.wigball.android.games.dotsandboxes;

public final class R {

    public static final class array {
        public static final int boardTypeKeys = 2131099651;
        public static final int boardTypeValues = 2131099650;
        public static final int fieldSizesKeys = 2131099649;
        public static final int fieldSizesValues = 2131099648;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int game_display = 2130837504;
        public static final int icon = 2130837505;
        public static final int kasten = 2130837506;
        public static final int kasten_dots = 2130837507;
        public static final int kasten_player1 = 2130837508;
        public static final int kasten_player2 = 2130837509;
        public static final int menu_exit = 2130837510;
        public static final int menu_restart = 2130837511;
        public static final int number0 = 2130837512;
        public static final int number1 = 2130837513;
        public static final int number2 = 2130837514;
        public static final int number3 = 2130837515;
        public static final int number4 = 2130837516;
        public static final int number5 = 2130837517;
        public static final int number6 = 2130837518;
        public static final int number7 = 2130837519;
        public static final int number8 = 2130837520;
        public static final int number9 = 2130837521;
        public static final int rounded_rectangle = 2130837522;
        public static final int rounded_rectangle_selected = 2130837523;
        public static final int start_btn_layout = 2130837524;
        public static final int title_logo = 2130837525;
        public static final int winner_background = 2130837526;
    }

    public static final class id {
        public static final int GameCanvas = 2131296256;
        public static final int StartBackground = 2131296261;
        public static final int btnExit = 2131296267;
        public static final int btnPlayer1 = 2131296264;
        public static final int btnPlayer2 = 2131296265;
        public static final int btnQuitGame = 2131296260;
        public static final int btnRestartGame = 2131296259;
        public static final int btnSettings = 2131296266;
        public static final int btnStartGame = 2131296263;
        public static final int masterLayout = 2131296262;
        public static final int menu_exit_id = 2131296269;
        public static final int menu_restart_id = 2131296268;
        public static final int winnerLayout = 2131296257;
        public static final int winnerText = 2131296258;
    }

    public static final class layout {
        public static final int game = 2130903040;
        public static final int main = 2130903041;
    }

    public static final class menu {
        public static final int game_menu = 2131230720;
    }

    public static final class string {
        public static final int app_name = 2131034112;
        public static final int boardType = 2131034130;
        public static final int boardTypeDetail = 2131034131;
        public static final int btnExit = 2131034120;
        public static final int btnPlayer1 = 2131034117;
        public static final int btnPlayer2 = 2131034118;
        public static final int btnSettings = 2131034119;
        public static final int btnStartGame = 2131034116;
        public static final int choosePlayer = 2131034134;
        public static final int copyright = 2131034113;
        public static final int demo_question = 2131034144;
        public static final int draw = 2131034123;
        public static final int droid_easy = 2131034136;
        public static final int droid_hard = 2131034138;
        public static final int droid_medium = 2131034137;
        public static final int fieldSizes = 2131034128;
        public static final int fieldSizesDetail = 2131034129;
        public static final int hard_player_not_available_in_demo = 2131034143;
        public static final int human = 2131034135;
        public static final int licenseCheckFailed = 2131034142;
        public static final int licenseProgress = 2131034141;
        public static final int menu_quit_game = 2131034140;
        public static final int menu_restart_game = 2131034139;
        public static final int no = 2131034115;
        public static final int player = 2131034121;
        public static final int prefillOuterLines = 2131034132;
        public static final int prefillOuterLinesDetail = 2131034133;
        public static final int question_exit = 2131034126;
        public static final int question_restart = 2131034127;
        public static final int quit = 2131034124;
        public static final int restart = 2131034125;
        public static final int wins = 2131034122;
        public static final int yes = 2131034114;
    }

    public static final class style {
        public static final int btnStart = 2131165184;
        public static final int winnerText = 2131165185;
    }

    public static final class xml {
        public static final int preferences = 2130968576;
    }
}
