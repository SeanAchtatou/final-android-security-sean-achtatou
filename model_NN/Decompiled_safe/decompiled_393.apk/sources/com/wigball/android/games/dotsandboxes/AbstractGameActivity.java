package com.wigball.android.games.dotsandboxes;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import com.wigball.android.games.dotsandboxes.canvas.GameCanvas;
import com.wigball.android.games.dotsandboxes.control.GameControl;
import com.wigball.android.games.dotsandboxes.demo.R;
import com.wigball.android.games.dotsandboxes.intents.IntentExtras;

public abstract class AbstractGameActivity extends AbstractActivity implements IntentExtras {
    /* access modifiers changed from: private */
    public GameControl gameControl = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.game);
        this.gameControl = (GameControl) getIntent().getSerializableExtra(IntentExtras.EXTRA_GAME_CONTROL);
        final GameCanvas canvas = (GameCanvas) findViewById(R.id.GameCanvas);
        canvas.setGameControl(this.gameControl);
        ((Button) findViewById(R.id.btnRestartGame)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                AbstractGameActivity.this.gameControl.reset(AbstractGameActivity.this);
                canvas.repaint();
                ((LinearLayout) AbstractGameActivity.this.findViewById(R.id.winnerLayout)).setVisibility(4);
            }
        });
        ((Button) findViewById(R.id.btnQuitGame)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                AbstractGameActivity.this.finish();
            }
        });
        if (this.gameControl.getOwnerModel().existFreeBoxes()) {
            ((LinearLayout) findViewById(R.id.winnerLayout)).setVisibility(4);
        }
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (event.getRepeatCount() > 0) {
            return false;
        }
        switch (keyCode) {
            case 82:
                return false;
            default:
                return true;
        }
    }

    private void askToExit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage((int) R.string.question_exit);
        builder.setCancelable(false);
        builder.setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                AbstractGameActivity.this.finish();
            }
        });
        builder.setNegativeButton((int) R.string.no, new DialogInterface.OnClickListener() {
            /*  JADX ERROR: Method load error
                jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: android.content.DialogInterface.cancel():void in method: com.wigball.android.games.dotsandboxes.AbstractGameActivity.4.onClick(android.content.DialogInterface, int):void, dex: classes.dex
                	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
                	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
                	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:312)
                	at jadx.core.ProcessClass.process(ProcessClass.java:36)
                	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
                	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
                	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
                Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: android.content.DialogInterface.cancel():void
                	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
                	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
                	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
                	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:536)
                	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
                	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
                	... 6 more
                */
            public void onClick(android.content.DialogInterface r1, int r2) {
                /*
                    r0 = this;
                    r1.cancel()
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.wigball.android.games.dotsandboxes.AbstractGameActivity.AnonymousClass4.onClick(android.content.DialogInterface, int):void");
            }
        });
        builder.create().show();
    }

    private void askToRestart() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage((int) R.string.question_restart);
        builder.setCancelable(false);
        builder.setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                AbstractGameActivity.this.gameControl.reset(AbstractGameActivity.this);
                ((GameCanvas) AbstractGameActivity.this.findViewById(R.id.GameCanvas)).repaint();
                ((LinearLayout) AbstractGameActivity.this.findViewById(R.id.winnerLayout)).setVisibility(4);
            }
        });
        builder.setNegativeButton((int) R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        builder.create().show();
    }

    public void onBackPressed() {
        askToExit();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.game_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_restart_id /*2131296268*/:
                askToRestart();
                return true;
            case R.id.menu_exit_id /*2131296269*/:
                askToExit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
