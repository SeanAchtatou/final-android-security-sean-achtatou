package com.wigball.android.games.dotsandboxes.util;

public interface License {
    public static final String LICENSE_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAm4gw0q0otNbH4CZoUB15yClRL0Li4S5tib8peg28EXMFWRUEqRKkcYZuhEVDS1wfCi0W1/U+yKq5PfZvagVnGnXK0zhH0s22j8rZKK5Ns6rGu0SzN00cWzmq2pUrgB2LrJau3PwtLT6mto3JGcjLx/hthZNK24etccTye6D2/ysHN5wa4Y83uJTXRuUsoCgeCOKv63ZelE2KqrZPvoapK6G4bt3ifKwZEA3Dy+Eft5SOMEQD5aCLjw6c1hLl26bq9QmJYTR8KIcSEvrL6qzzoO70w2TX7j1Ac+BI8IFgS5crEgkzPFX7rBSFc4QokKb+kCpbTb1+SB68OuwA8+MhmwIDAQAB";
    public static final byte[] LICENSE_SALT = {10, -74, -123, -30, 101, 76, -52, 109, 4, -92, -83, 29, -67, 49, 110, 71, -41, -61, 69, 6};
}
