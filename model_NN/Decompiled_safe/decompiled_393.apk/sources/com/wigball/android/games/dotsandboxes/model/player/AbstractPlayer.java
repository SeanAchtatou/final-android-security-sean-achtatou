package com.wigball.android.games.dotsandboxes.model.player;

import java.io.Serializable;

public abstract class AbstractPlayer implements Player, Serializable {
    private static final long serialVersionUID = 1180345535811012635L;
}
