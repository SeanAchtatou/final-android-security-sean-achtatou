package com.wigball.android.games.dotsandboxes.demo;

import com.wigball.android.games.dotsandboxes.AbstractGameActivity;

public class GameActivity extends AbstractGameActivity {
    public boolean isDemo() {
        return true;
    }

    public Class<?> getStartActivityClass() {
        return DotsAndBoxesStartActivity.class;
    }

    public Class<?> getPreferencesClass() {
        return Preferences.class;
    }

    public Class<?> getGameActivityClass() {
        return GameActivity.class;
    }
}
