package com.wigball.android.games.dotsandboxes.preferences;

public interface PreferencesKeys {
    public static final String KEY_BOARD_TYPE = "boardtype";
    public static final String KEY_FIELD_SIZE = "fieldsize";
    public static final String KEY_PLAYER_CHOISE_PLAYER_1 = "playerchoise1";
    public static final String KEY_PLAYER_CHOISE_PLAYER_2 = "playerchoise2";
    public static final String KEY_PREFILL_OUTER_LINES = "prefillOuterLines";
    public static final String VALUE_BOARD_TYPE_BOXES = "b";
    public static final String VALUE_BOARD_TYPE_DOTS = "d";
    public static final String VALUE_PLAYER_CHOISE_DROID_EASY = "de";
    public static final String VALUE_PLAYER_CHOISE_DROID_HARD = "dh";
    public static final String VALUE_PLAYER_CHOISE_DROID_MEDIUM = "dm";
    public static final String VALUE_PLAYER_CHOISE_HUMAN = "h";
}
