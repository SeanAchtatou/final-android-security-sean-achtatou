package com.wigball.android.games.dotsandboxes.canvas;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.wigball.android.games.dotsandboxes.demo.R;

public class StartBackground extends SurfaceView implements SurfaceHolder.Callback {
    private int height;
    private SurfaceHolder holder = getHolder();
    private int width;

    public StartBackground(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.holder.addCallback(this);
        this.width = 0;
        this.height = 0;
    }

    public void surfaceChanged(SurfaceHolder arg0, int format, int width2, int height2) {
        this.width = width2;
        this.height = height2;
        Canvas c = this.holder.lockCanvas();
        drawBackground(c);
        this.holder.unlockCanvasAndPost(c);
    }

    private void drawBackground(Canvas c) {
        Bitmap boxImg = BitmapFactory.decodeResource(getResources(), R.drawable.kasten);
        c.drawColor(-16777216);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        int newBoxSize = this.width / 6;
        int offsetX = (this.width % newBoxSize) / 2;
        int offsetY = (this.height % newBoxSize) / 2;
        for (int y = offsetY; y < this.height - offsetY; y += newBoxSize) {
            for (int x = offsetX; x < this.width - offsetX; x += newBoxSize) {
                c.drawBitmap(boxImg, (Rect) null, new RectF((float) x, (float) y, (float) (x + newBoxSize), (float) (y + newBoxSize)), paint);
            }
        }
        String copyright = getResources().getString(R.string.copyright);
        paint.setTypeface(Typeface.create(Typeface.SANS_SERIF, 1));
        paint.setColor(-1);
        float txtWidth = 0.0f;
        int fontSize = 25;
        for (int i = 25; i >= 0; i--) {
            txtWidth = 0.0f;
            fontSize = i;
            paint.setTextSize((float) fontSize);
            float[] widths = new float[copyright.length()];
            paint.getTextWidths(copyright, widths);
            for (int j = 0; j < widths.length; j++) {
                txtWidth += widths[j];
            }
            if (txtWidth < ((float) this.width)) {
                break;
            }
        }
        float txtX = (((float) this.width) - txtWidth) / 2.0f;
        float txtY = (float) ((this.height - offsetY) - ((newBoxSize - fontSize) / 2));
        c.drawText(copyright, txtX, txtY, paint);
        paint.setColor(-16777216);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(1.0f);
        c.drawText(copyright, txtX, txtY, paint);
        Bitmap titleImg = BitmapFactory.decodeResource(getResources(), R.drawable.title_logo);
        c.drawBitmap(titleImg, (Rect) null, new Rect(offsetX, offsetY, this.width - (offsetX * 2), ((this.width - (offsetX * 2)) * titleImg.getHeight()) / titleImg.getWidth()), paint);
    }

    public void surfaceCreated(SurfaceHolder arg0) {
    }

    public void surfaceDestroyed(SurfaceHolder arg0) {
    }
}
