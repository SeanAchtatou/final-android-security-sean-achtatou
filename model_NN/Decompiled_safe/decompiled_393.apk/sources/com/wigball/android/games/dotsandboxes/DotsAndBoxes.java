package com.wigball.android.games.dotsandboxes;

public interface DotsAndBoxes {
    Class<?> getGameActivityClass();

    Class<?> getPreferencesClass();

    Class<?> getStartActivityClass();

    boolean isDemo();
}
