package com.wigball.android.games.dotsandboxes.model.player;

import com.wigball.android.games.dotsandboxes.control.GameControl;

public interface Player {
    public static final int MOVE_COMPLETE = 1;
    public static final int NO_MOVE = 0;

    int doMove(GameControl gameControl);
}
