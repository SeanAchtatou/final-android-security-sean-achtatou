package com.wigball.android.games.dotsandboxes.model;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.HashSet;
import java.util.Set;

public class OwnerModel implements Serializable {
    public static final int NOBODY = 0;
    public static final int PLAYER1 = 1;
    public static final int PLAYER2 = 2;
    private static final long serialVersionUID = 3086472130798588813L;
    private int boxCount;
    private int[][] boxOwner;

    public OwnerModel(int boxCount2) {
        this.boxOwner = (int[][]) Array.newInstance(Integer.TYPE, boxCount2, boxCount2);
        this.boxCount = boxCount2;
    }

    public void setOwner(int box, int owner) {
        int boxX = box % this.boxCount;
        this.boxOwner[boxX][box / this.boxCount] = owner;
    }

    public int getOwner(int box) {
        int boxX = box % this.boxCount;
        return this.boxOwner[boxX][box / this.boxCount];
    }

    public int getPlayer1FieldCount() {
        return getPlayerCount(1);
    }

    public int getPlayer2FieldCount() {
        return getPlayerCount(2);
    }

    private int getPlayerCount(int player) {
        int count = 0;
        for (int i = 0; i < this.boxOwner.length; i++) {
            for (int i2 : this.boxOwner[i]) {
                if (i2 == player) {
                    count++;
                }
            }
        }
        return count;
    }

    public boolean existFreeBoxes() {
        if (getPlayerCount(0) != 0) {
            return true;
        }
        return false;
    }

    public int getRandomFreeBox() {
        Set<Integer> usedPairs = new HashSet<>();
        int boxNo = -1;
        while (boxNo == -1) {
            boolean rndFound = false;
            int boxX = -1;
            int boxY = -1;
            while (!rndFound) {
                boxX = (int) Math.floor(Math.random() * ((double) this.boxCount));
                if (boxX == this.boxCount) {
                    boxX--;
                }
                boxY = (int) Math.floor(Math.random() * ((double) this.boxCount));
                if (boxY == this.boxCount) {
                    boxY--;
                }
                rndFound = !usedPairs.contains(Integer.valueOf((this.boxCount * boxY) + boxX));
            }
            if (this.boxOwner[boxX][boxY] == 0) {
                boxNo = (this.boxCount * boxY) + boxX;
            } else {
                usedPairs.add(Integer.valueOf((this.boxCount * boxY) + boxX));
                if (usedPairs.size() == this.boxCount * this.boxCount) {
                    break;
                }
            }
        }
        return boxNo;
    }
}
