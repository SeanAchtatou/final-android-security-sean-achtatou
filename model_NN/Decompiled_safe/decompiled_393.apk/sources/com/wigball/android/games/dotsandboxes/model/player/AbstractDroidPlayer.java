package com.wigball.android.games.dotsandboxes.model.player;

import com.wigball.android.games.dotsandboxes.control.GameControl;

public abstract class AbstractDroidPlayer extends AbstractPlayer {
    private static final long serialVersionUID = -4162027361385573649L;

    /* access modifiers changed from: protected */
    public int getFreeLine(GameControl gc, int boxId) {
        if (!gc.getLineModel().isLineDrawn(boxId, 0)) {
            return 0;
        }
        if (!gc.getLineModel().isLineDrawn(boxId, 1)) {
            return 1;
        }
        if (!gc.getLineModel().isLineDrawn(boxId, 2)) {
            return 2;
        }
        return 3;
    }
}
