package com.wigball.android.games.dotsandboxes.control;

import android.content.Context;
import android.preference.PreferenceManager;
import com.wigball.android.games.dotsandboxes.model.LineModel;
import com.wigball.android.games.dotsandboxes.model.OwnerModel;
import com.wigball.android.games.dotsandboxes.model.player.EasyDroidPlayer;
import com.wigball.android.games.dotsandboxes.model.player.HardDroidPlayer;
import com.wigball.android.games.dotsandboxes.model.player.HumanPlayer;
import com.wigball.android.games.dotsandboxes.model.player.MediumDroidPlayer;
import com.wigball.android.games.dotsandboxes.model.player.Player;
import com.wigball.android.games.dotsandboxes.preferences.PreferencesKeys;
import java.io.Serializable;

public class GameControl implements Serializable, PreferencesKeys {
    private static final long serialVersionUID = -3454508155225486415L;
    private int NO_BOX = -1;
    private int boxCount;
    private LineModel lineModel;
    private OwnerModel ownerModel;
    private Player player1;
    private Player player2;
    private int playerTurn;

    public GameControl(Context ctx) {
        reset(ctx);
    }

    public void reset(Context ctx) {
        this.boxCount = readBoxCount(ctx);
        this.ownerModel = new OwnerModel(this.boxCount);
        this.lineModel = new LineModel(this.boxCount);
        this.playerTurn = 1;
        this.player1 = getPlayer(ctx, 1);
        this.player2 = getPlayer(ctx, 2);
        if (prefillOuterLines(ctx)) {
            this.lineModel.prefillOuterLines();
        }
        nextMove();
    }

    private Player getPlayer(Context ctx, int no) {
        String playerValue = PreferenceManager.getDefaultSharedPreferences(ctx).getString(no == 1 ? PreferencesKeys.KEY_PLAYER_CHOISE_PLAYER_1 : PreferencesKeys.KEY_PLAYER_CHOISE_PLAYER_2, no == 1 ? PreferencesKeys.VALUE_PLAYER_CHOISE_HUMAN : PreferencesKeys.VALUE_PLAYER_CHOISE_DROID_EASY);
        if (PreferencesKeys.VALUE_PLAYER_CHOISE_HUMAN.equals(playerValue)) {
            return new HumanPlayer();
        }
        if (PreferencesKeys.VALUE_PLAYER_CHOISE_DROID_EASY.equals(playerValue)) {
            return new EasyDroidPlayer();
        }
        if (PreferencesKeys.VALUE_PLAYER_CHOISE_DROID_MEDIUM.equals(playerValue)) {
            return new MediumDroidPlayer();
        }
        if (PreferencesKeys.VALUE_PLAYER_CHOISE_DROID_HARD.equals(playerValue)) {
            return new HardDroidPlayer();
        }
        return null;
    }

    private boolean prefillOuterLines(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx).getBoolean(PreferencesKeys.KEY_PREFILL_OUTER_LINES, false);
    }

    private int readBoxCount(Context ctx) {
        return Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(ctx).getString(PreferencesKeys.KEY_FIELD_SIZE, "6"));
    }

    public int getBoxCount() {
        return this.boxCount;
    }

    public LineModel getLineModel() {
        return this.lineModel;
    }

    public OwnerModel getOwnerModel() {
        return this.ownerModel;
    }

    public void addLine(int box, int line) {
        int i;
        this.lineModel.addLine(box, line);
        boolean moreBoxesFound = true;
        boolean foundBoxes = false;
        while (moreBoxesFound) {
            int newOwnedBox = checkNewOwner(box);
            if (newOwnedBox != this.NO_BOX) {
                this.ownerModel.setOwner(newOwnedBox, this.playerTurn);
                foundBoxes = true;
            } else {
                moreBoxesFound = false;
            }
        }
        if (!foundBoxes) {
            if (this.playerTurn == 1) {
                i = 2;
            } else {
                i = 1;
            }
            this.playerTurn = i;
        }
        if (this.ownerModel.existFreeBoxes()) {
            nextMove();
        }
    }

    private void nextMove() {
        (this.playerTurn == 1 ? this.player1 : this.player2).doMove(this);
    }

    private int checkNewOwner(int boxId) {
        if (isUnowned(boxId)) {
            return boxId;
        }
        if (boxId >= this.boxCount) {
            int aboveBox = boxId - this.boxCount;
            if (isUnowned(aboveBox)) {
                return aboveBox;
            }
        }
        if (boxId % this.boxCount != this.boxCount - 1) {
            int rightBox = boxId + 1;
            if (isUnowned(rightBox)) {
                return rightBox;
            }
        }
        if (boxId / this.boxCount != this.boxCount - 1) {
            int beneathBox = boxId + this.boxCount;
            if (isUnowned(beneathBox)) {
                return beneathBox;
            }
        }
        if (boxId % this.boxCount != 0) {
            int leftBox = boxId - 1;
            if (isUnowned(leftBox)) {
                return leftBox;
            }
        }
        return this.NO_BOX;
    }

    private boolean isUnowned(int boxId) {
        return this.lineModel.isBoxClosed(boxId) && this.ownerModel.getOwner(boxId) == 0;
    }

    public int getNextPlayer() {
        return this.playerTurn;
    }
}
