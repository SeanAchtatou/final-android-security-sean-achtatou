package com.wigball.android.games.dotsandboxes.model.player;

import com.wigball.android.games.dotsandboxes.control.GameControl;

public class EasyDroidPlayer extends AbstractDroidPlayer {
    private static final long serialVersionUID = 8729792782655878702L;

    public int doMove(GameControl gc) {
        int boxNo = gc.getOwnerModel().getRandomFreeBox();
        gc.addLine(boxNo, gc.getLineModel().getFreeLine(boxNo));
        return 1;
    }
}
