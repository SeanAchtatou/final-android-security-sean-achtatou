package com.wigball.android.games.dotsandboxes.model.player;

import com.wigball.android.games.dotsandboxes.control.GameControl;

public class MediumDroidPlayer extends AbstractDroidPlayer {
    private static final long serialVersionUID = -1260679181317115483L;

    public int doMove(GameControl gc) {
        int boxId = gc.getLineModel().getRandomBoxWithOpenSides(1);
        if (boxId == -1) {
            boxId = gc.getLineModel().getRandomBoxWithOpenSides(3);
        }
        if (boxId == -1) {
            boxId = gc.getLineModel().getRandomBoxWithOpenSides(4);
        }
        if (boxId == -1) {
            boxId = gc.getOwnerModel().getRandomFreeBox();
        }
        gc.addLine(boxId, getFreeLine(gc, boxId));
        return 1;
    }
}
