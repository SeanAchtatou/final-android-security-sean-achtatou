package com.wigball.android.games.dotsandboxes.canvas;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wigball.android.games.dotsandboxes.control.GameControl;
import com.wigball.android.games.dotsandboxes.demo.R;
import com.wigball.android.games.dotsandboxes.model.LineModel;
import com.wigball.android.games.dotsandboxes.preferences.PreferencesKeys;

public class GameCanvas extends SurfaceView implements SurfaceHolder.Callback, PreferencesKeys {
    private static final int COLOR_BLACK_LINE = 1711276032;
    private static final int COLOR_RED_LINE = 1727987712;
    private static final int NO_BOX = -1;
    private Bitmap boxImage;
    private Bitmap dotsImage;
    private GameControl gameControl;
    private int height;
    private SurfaceHolder holder = getHolder();
    private float innerFieldHeight;
    private float innerFieldWidth;
    private float newBoxSize;
    private Bitmap number0;
    private Bitmap number1;
    private Bitmap number2;
    private Bitmap number3;
    private Bitmap number4;
    private Bitmap number5;
    private Bitmap number6;
    private Bitmap number7;
    private Bitmap number8;
    private Bitmap number9;
    private float outerFieldHeight;
    private Bitmap player1BoxImg;
    private Bitmap player2BoxImg;
    private int width;

    public GameCanvas(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.holder.addCallback(this);
        this.boxImage = BitmapFactory.decodeResource(getResources(), R.drawable.kasten);
        this.dotsImage = BitmapFactory.decodeResource(getResources(), R.drawable.kasten_dots);
        this.player1BoxImg = BitmapFactory.decodeResource(getResources(), R.drawable.kasten_player1);
        this.player2BoxImg = BitmapFactory.decodeResource(getResources(), R.drawable.kasten_player2);
        this.number0 = BitmapFactory.decodeResource(getResources(), R.drawable.number0);
        this.number1 = BitmapFactory.decodeResource(getResources(), R.drawable.number1);
        this.number2 = BitmapFactory.decodeResource(getResources(), R.drawable.number2);
        this.number3 = BitmapFactory.decodeResource(getResources(), R.drawable.number3);
        this.number4 = BitmapFactory.decodeResource(getResources(), R.drawable.number4);
        this.number5 = BitmapFactory.decodeResource(getResources(), R.drawable.number5);
        this.number6 = BitmapFactory.decodeResource(getResources(), R.drawable.number6);
        this.number7 = BitmapFactory.decodeResource(getResources(), R.drawable.number7);
        this.number8 = BitmapFactory.decodeResource(getResources(), R.drawable.number8);
        this.number9 = BitmapFactory.decodeResource(getResources(), R.drawable.number9);
    }

    public void setGameControl(GameControl gc) {
        this.gameControl = gc;
    }

    public void repaint() {
        Canvas c = this.holder.lockCanvas();
        c.drawColor(-2236963);
        drawField(c);
        drawModel(c);
        drawGameDisplay(c);
        if (!this.gameControl.getOwnerModel().existFreeBoxes()) {
            TextView winnerText = (TextView) getRootView().findViewById(R.id.winnerText);
            StringBuffer buf = new StringBuffer();
            int player1Fields = this.gameControl.getOwnerModel().getPlayer1FieldCount();
            int player2Fields = this.gameControl.getOwnerModel().getPlayer2FieldCount();
            if (player1Fields != player2Fields) {
                buf.append(getContext().getString(R.string.player));
                if (player1Fields > player2Fields) {
                    buf.append(" 1 ");
                } else {
                    buf.append(" 2 ");
                }
                buf.append(getContext().getString(R.string.wins));
            } else {
                buf.append(getContext().getString(R.string.draw));
            }
            winnerText.setText(buf.toString());
            ((LinearLayout) getRootView().findViewById(R.id.winnerLayout)).setVisibility(0);
        }
        this.holder.unlockCanvasAndPost(c);
    }

    private void drawField(Canvas c) {
        Bitmap boxImg = PreferencesKeys.VALUE_BOARD_TYPE_BOXES.equals(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(PreferencesKeys.KEY_BOARD_TYPE, PreferencesKeys.VALUE_BOARD_TYPE_BOXES)) ? this.boxImage : this.dotsImage;
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        this.innerFieldWidth = (float) (this.width - 10);
        this.newBoxSize = this.innerFieldWidth / ((float) this.gameControl.getBoxCount());
        this.innerFieldHeight = this.newBoxSize * ((float) this.gameControl.getBoxCount());
        this.outerFieldHeight = this.innerFieldHeight + 10.0f;
        for (int y = 0; y < this.gameControl.getBoxCount(); y++) {
            for (int x = 0; x < this.gameControl.getBoxCount(); x++) {
                c.drawBitmap(boxImg, (Rect) null, new RectF((((float) x) * this.newBoxSize) + 5.0f, (((float) y) * this.newBoxSize) + 5.0f, (((float) x) * this.newBoxSize) + this.newBoxSize + 5.0f, (((float) y) * this.newBoxSize) + this.newBoxSize + 5.0f), paint);
            }
        }
    }

    private void drawModel(Canvas c) {
        drawGrid(c);
        drawUserFields(c);
    }

    private void drawGameDisplay(Canvas c) {
        float playerDisplayBoxSize;
        int player1Color;
        int player2Color;
        float player1StrokeWidth;
        float player2StrokeWidth;
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        Drawable d = getResources().getDrawable(R.drawable.game_display);
        d.setBounds(0, (int) Math.floor((double) this.outerFieldHeight), this.width, this.height);
        d.draw(c);
        float playerDisplayWidth = (float) (this.width / 2);
        float playerDisplayHalfWidth = playerDisplayWidth / 2.0f;
        float playerDisplayHalfHeight = (((float) this.height) - this.outerFieldHeight) / 2.0f;
        if (playerDisplayHalfWidth < playerDisplayHalfHeight) {
            playerDisplayBoxSize = playerDisplayHalfWidth * 0.8f;
        } else {
            playerDisplayBoxSize = playerDisplayHalfHeight * 0.8f;
        }
        float x1 = playerDisplayHalfWidth - playerDisplayBoxSize;
        float y1 = (this.outerFieldHeight + playerDisplayHalfHeight) - playerDisplayBoxSize;
        float x2 = playerDisplayHalfWidth;
        float y2 = y1 + playerDisplayBoxSize;
        RectF rectF = new RectF(x1, y1, x2, y2);
        c.drawBitmap(this.boxImage, (Rect) null, rectF, paint);
        RectF rectF2 = new RectF(x1 + playerDisplayBoxSize, y1, x2 + playerDisplayBoxSize, y2);
        c.drawBitmap(this.boxImage, (Rect) null, rectF2, paint);
        float x12 = playerDisplayHalfWidth - playerDisplayBoxSize;
        float y12 = y1 + playerDisplayBoxSize;
        float x22 = playerDisplayHalfWidth;
        float y22 = y2 + playerDisplayBoxSize;
        RectF rectF3 = new RectF(x12, y12, x22, y22);
        c.drawBitmap(this.boxImage, (Rect) null, rectF3, paint);
        RectF rectF4 = new RectF(x12 + playerDisplayBoxSize, y12, x22 + playerDisplayBoxSize, y22);
        c.drawBitmap(this.boxImage, (Rect) null, rectF4, paint);
        float x13 = (playerDisplayWidth + playerDisplayHalfWidth) - playerDisplayBoxSize;
        float y13 = (this.outerFieldHeight + playerDisplayHalfHeight) - playerDisplayBoxSize;
        float x23 = x13 + playerDisplayBoxSize;
        float y23 = y13 + playerDisplayBoxSize;
        RectF rectF5 = new RectF(x13, y13, x23, y23);
        c.drawBitmap(this.boxImage, (Rect) null, rectF5, paint);
        RectF rectF6 = new RectF(x13 + playerDisplayBoxSize, y13, x23 + playerDisplayBoxSize, y23);
        c.drawBitmap(this.boxImage, (Rect) null, rectF6, paint);
        float x14 = (playerDisplayWidth + playerDisplayHalfWidth) - playerDisplayBoxSize;
        float y14 = y13 + playerDisplayBoxSize;
        float x24 = x14 + playerDisplayBoxSize;
        float y24 = y23 + playerDisplayBoxSize;
        RectF rectF7 = new RectF(x14, y14, x24, y24);
        c.drawBitmap(this.boxImage, (Rect) null, rectF7, paint);
        RectF rectF8 = new RectF(x14 + playerDisplayBoxSize, y14, x24 + playerDisplayBoxSize, y24);
        c.drawBitmap(this.boxImage, (Rect) null, rectF8, paint);
        c.drawBitmap(this.player1BoxImg, (Rect) null, rectF, paint);
        c.drawBitmap(this.player2BoxImg, (Rect) null, rectF6, paint);
        int player1Count = this.gameControl.getOwnerModel().getPlayer1FieldCount();
        int player2Count = this.gameControl.getOwnerModel().getPlayer2FieldCount();
        c.drawBitmap(getNumberBitmap(player1Count / 10), (Rect) null, rectF3, paint);
        c.drawBitmap(getNumberBitmap(player1Count % 10), (Rect) null, rectF4, paint);
        c.drawBitmap(getNumberBitmap(player2Count / 10), (Rect) null, rectF7, paint);
        c.drawBitmap(getNumberBitmap(player2Count % 10), (Rect) null, rectF8, paint);
        if (this.gameControl.getNextPlayer() == 1) {
            player1Color = COLOR_RED_LINE;
            player2Color = COLOR_BLACK_LINE;
            player1StrokeWidth = 6.0f;
            player2StrokeWidth = 3.0f;
        } else if (this.gameControl.getNextPlayer() == 2) {
            player1Color = COLOR_BLACK_LINE;
            player2Color = COLOR_RED_LINE;
            player1StrokeWidth = 3.0f;
            player2StrokeWidth = 6.0f;
        } else {
            player1Color = 0;
            player2Color = 0;
            player1StrokeWidth = 0.0f;
            player2StrokeWidth = 0.0f;
        }
        paint.setStyle(Paint.Style.STROKE);
        RectF rectF9 = new RectF(rectF.left, rectF.top, rectF4.right, rectF4.bottom);
        paint.setColor(player1Color);
        paint.setStrokeWidth(player1StrokeWidth);
        c.drawRect(rectF9, paint);
        RectF rectF10 = new RectF(rectF5.left, rectF5.top, rectF8.right, rectF8.bottom);
        paint.setColor(player2Color);
        paint.setStrokeWidth(player2StrokeWidth);
        c.drawRect(rectF10, paint);
    }

    private Bitmap getNumberBitmap(int nr) {
        switch (nr) {
            case 0:
                return this.number0;
            case 1:
                return this.number1;
            case 2:
                return this.number2;
            case LineModel.LINE_LEFT /*3*/:
                return this.number3;
            case 4:
                return this.number4;
            case 5:
                return this.number5;
            case 6:
                return this.number6;
            case 7:
                return this.number7;
            case 8:
                return this.number8;
            case 9:
                return this.number9;
            default:
                return null;
        }
    }

    private void drawGrid(Canvas c) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
        for (int y = 0; y < this.gameControl.getBoxCount(); y++) {
            for (int x = 0; x < this.gameControl.getBoxCount(); x++) {
                int boxId = (this.gameControl.getBoxCount() * y) + x;
                if (x == 0 && this.gameControl.getLineModel().isLineDrawn(boxId, 3)) {
                    drawLine(c, paint, boxId, 3);
                }
                if (y == 0 && this.gameControl.getLineModel().isLineDrawn(boxId, 0)) {
                    drawLine(c, paint, boxId, 0);
                }
                if (this.gameControl.getLineModel().isLineDrawn(boxId, 1)) {
                    drawLine(c, paint, boxId, 1);
                }
                if (this.gameControl.getLineModel().isLineDrawn(boxId, 2)) {
                    drawLine(c, paint, boxId, 2);
                }
            }
        }
    }

    private void drawLine(Canvas c, Paint paint, int box, int line) {
        RectF rect;
        int x = box % this.gameControl.getBoxCount();
        int y = box / this.gameControl.getBoxCount();
        float x1 = (((float) x) * this.newBoxSize) + 5.0f;
        float y1 = (((float) y) * this.newBoxSize) + 5.0f;
        float x2 = (((float) x) * this.newBoxSize) + this.newBoxSize + 5.0f;
        float y2 = (((float) y) * this.newBoxSize) + this.newBoxSize + 5.0f;
        if (this.gameControl.getLineModel().isLastDrawn(box, line)) {
            paint.setColor((int) COLOR_RED_LINE);
        } else {
            paint.setColor((int) COLOR_BLACK_LINE);
        }
        switch (line) {
            case 0:
                rect = new RectF(x1, y1 - 3.0f, x2, y1 + 3.0f);
                break;
            case 1:
                rect = new RectF(x2 - 3.0f, y1, x2 + 3.0f, y2);
                break;
            case 2:
                rect = new RectF(x1, y2 - 3.0f, x2, y2 + 3.0f);
                break;
            case LineModel.LINE_LEFT /*3*/:
                rect = new RectF(x1 - 3.0f, y1, x1 + 3.0f, y2);
                break;
            default:
                rect = null;
                break;
        }
        if (rect != null) {
            c.drawRect(rect, paint);
        }
    }

    private void drawUserFields(Canvas c) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        for (int y = 0; y < this.gameControl.getBoxCount(); y++) {
            for (int x = 0; x < this.gameControl.getBoxCount(); x++) {
                int owner = this.gameControl.getOwnerModel().getOwner((this.gameControl.getBoxCount() * y) + x);
                Bitmap ownerImage = null;
                if (owner == 1) {
                    ownerImage = this.player1BoxImg;
                } else if (owner == 2) {
                    ownerImage = this.player2BoxImg;
                }
                if (ownerImage != null) {
                    c.drawBitmap(ownerImage, (Rect) null, new RectF((((float) x) * this.newBoxSize) + 5.0f, (((float) y) * this.newBoxSize) + 5.0f, (((float) x) * this.newBoxSize) + this.newBoxSize + 5.0f, (((float) y) * this.newBoxSize) + this.newBoxSize + 5.0f), paint);
                }
            }
        }
    }

    public void surfaceChanged(SurfaceHolder arg0, int format, int width2, int height2) {
        this.width = width2;
        this.height = height2;
        repaint();
    }

    public void surfaceCreated(SurfaceHolder arg0) {
    }

    public void surfaceDestroyed(SurfaceHolder arg0) {
    }

    private int identifyBox(int x, int y) {
        if (((float) y) > this.outerFieldHeight) {
            return -1;
        }
        int row = y / ((int) Math.floor((double) this.newBoxSize));
        int col = x / ((int) Math.floor((double) this.newBoxSize));
        if (row >= this.gameControl.getBoxCount()) {
            row = this.gameControl.getBoxCount() - 1;
        }
        if (col >= this.gameControl.getBoxCount()) {
            col = this.gameControl.getBoxCount() - 1;
        }
        return (this.gameControl.getBoxCount() * row) + col;
    }

    private int identifyLine(int boxId, int x, int y) {
        int boxX = boxId % this.gameControl.getBoxCount();
        int boxY = boxId / this.gameControl.getBoxCount();
        float relativeX = ((float) x) - (5.0f + (((float) boxX) * this.newBoxSize));
        float relativeY = ((float) y) - (5.0f + (((float) boxY) * this.newBoxSize));
        float distanceToTop = relativeY;
        float distanceToRight = this.newBoxSize - relativeX;
        float distanceToBottom = this.newBoxSize - relativeY;
        float minimumDistance = relativeX;
        int line = 3;
        if (distanceToTop < minimumDistance) {
            minimumDistance = distanceToTop;
            line = 0;
        }
        if (distanceToRight < minimumDistance) {
            minimumDistance = distanceToRight;
            line = 1;
        }
        if (distanceToBottom < minimumDistance) {
            return 2;
        }
        return line;
    }

    public boolean onTouchEvent(MotionEvent me) {
        if (me.getAction() != 0) {
            return super.onTouchEvent(me);
        }
        int x = (int) Math.floor((double) me.getX());
        int y = (int) Math.floor((double) me.getY());
        if (x <= 5) {
            x = 6;
        } else if (((float) x) > this.innerFieldWidth) {
            x = ((int) Math.floor((double) this.innerFieldWidth)) - 1;
        }
        if (y <= 5) {
            y = 6;
        } else if (((float) y) > this.innerFieldHeight && ((float) y) <= this.outerFieldHeight) {
            y = ((int) Math.floor((double) this.innerFieldHeight)) - 1;
        }
        int boxId = identifyBox(x, y);
        if (boxId == -1) {
            return super.onTouchEvent(me);
        }
        int line = identifyLine(boxId, x, y);
        if (this.gameControl.getLineModel().isLineDrawn(boxId, line)) {
            return super.onTouchEvent(me);
        }
        this.gameControl.addLine(boxId, line);
        repaint();
        return true;
    }
}
