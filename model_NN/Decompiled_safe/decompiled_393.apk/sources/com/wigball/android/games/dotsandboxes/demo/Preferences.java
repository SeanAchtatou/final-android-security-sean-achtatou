package com.wigball.android.games.dotsandboxes.demo;

import com.wigball.android.games.dotsandboxes.AbstractPreferences;

public class Preferences extends AbstractPreferences {
    public boolean isDemo() {
        return true;
    }

    public Class<?> getStartActivityClass() {
        return DotsAndBoxesStartActivity.class;
    }

    public Class<?> getPreferencesClass() {
        return Preferences.class;
    }

    public Class<?> getGameActivityClass() {
        return GameActivity.class;
    }
}
