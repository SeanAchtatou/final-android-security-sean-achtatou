package com.wigball.android.games.dotsandboxes.model.player;

import com.wigball.android.games.dotsandboxes.control.GameControl;
import com.wigball.android.games.dotsandboxes.model.LineModel;
import com.wigball.android.games.dotsandboxes.model.OwnerModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class HardDroidPlayer extends AbstractDroidPlayer {
    private static final int NO_NEIGHBOR = -1;
    private static final long serialVersionUID = -3866221272021774575L;

    /* Debug info: failed to restart local var, previous not found, register: 36 */
    public int doMove(GameControl gc) {
        int boxId;
        int line;
        int lineValue;
        int boxCount = gc.getBoxCount();
        int noBoxes = boxCount * boxCount;
        OwnerModel ownerModel = gc.getOwnerModel();
        LineModel lineModel = gc.getLineModel();
        int boxWith3Lines = lineModel.getRandomBoxWithOpenSides(1);
        if (boxWith3Lines != -1) {
            gc.addLine(boxWith3Lines, getFreeLine(gc, boxWith3Lines));
            return 1;
        }
        HashSet<Integer> hashSet = new HashSet<>();
        for (int i = 0; i < noBoxes; i++) {
            if (ownerModel.getOwner(i) == 0) {
                hashSet.add(Integer.valueOf(i));
            }
        }
        HashMap hashMap = new HashMap();
        for (Integer intValue : hashSet) {
            int boxId2 = intValue.intValue();
            HashSet hashSet2 = new HashSet();
            if (!lineModel.isLineDrawn(boxId2, 0)) {
                hashSet2.add(null);
            }
            if (!lineModel.isLineDrawn(boxId2, 1)) {
                hashSet2.add(1);
            }
            if (!lineModel.isLineDrawn(boxId2, 2)) {
                hashSet2.add(2);
            }
            if (!lineModel.isLineDrawn(boxId2, 3)) {
                hashSet2.add(3);
            }
            hashMap.put(Integer.valueOf(boxId2), hashSet2);
        }
        HashMap hashMap2 = new HashMap();
        for (Integer intValue2 : hashMap.keySet()) {
            int boxId3 = intValue2.intValue();
            HashMap hashMap3 = new HashMap();
            for (Integer intValue3 : (Set) hashMap.get(Integer.valueOf(boxId3))) {
                int lineNo = intValue3.intValue();
                if (lineModel.getLineCountForBox(boxId3) == 3) {
                    lineValue = 0 + 20;
                } else if (lineModel.getLineCountForBox(boxId3) == 2) {
                    lineValue = 0 - 20;
                } else {
                    lineValue = 0 + getValueForNeighbor(getTopNeighbor(boxId3, boxCount), lineModel) + getValueForNeighbor(getRightNeighbor(boxId3, boxCount), lineModel) + getValueForNeighbor(getBottomNeighbor(boxId3, boxCount), lineModel) + getValueForNeighbor(getLeftNeighbor(boxId3, boxCount), lineModel);
                }
                hashMap3.put(Integer.valueOf(lineNo), Integer.valueOf(lineValue));
            }
            hashMap2.put(Integer.valueOf(boxId3), hashMap3);
        }
        int highestValueSoFar = Integer.MIN_VALUE;
        HashMap hashMap4 = new HashMap();
        for (Integer intValue4 : hashMap2.keySet()) {
            int boxId4 = intValue4.intValue();
            Map<Integer, Integer> actualValues = (Map) hashMap2.get(Integer.valueOf(boxId4));
            HashSet hashSet3 = new HashSet();
            for (Integer intValue5 : actualValues.keySet()) {
                int line2 = intValue5.intValue();
                int value = ((Integer) actualValues.get(Integer.valueOf(line2))).intValue();
                if (value > highestValueSoFar) {
                    hashSet3.clear();
                    hashMap4.clear();
                    hashSet3.add(Integer.valueOf(line2));
                    highestValueSoFar = value;
                } else if (value == highestValueSoFar) {
                    hashSet3.add(Integer.valueOf(line2));
                }
            }
            if (!hashSet3.isEmpty()) {
                hashMap4.put(Integer.valueOf(boxId4), hashSet3);
            }
        }
        Iterator<Integer> boxIterator = hashMap4.keySet().iterator();
        if (hashMap4.keySet().size() == 1) {
            boxId = ((Integer) boxIterator.next()).intValue();
        } else {
            ArrayList arrayList = new ArrayList();
            while (boxIterator.hasNext()) {
                arrayList.add((Integer) boxIterator.next());
            }
            int rndIdx = (int) Math.floor(Math.random() * ((double) arrayList.size()));
            if (rndIdx == arrayList.size()) {
                rndIdx--;
            }
            boxId = ((Integer) arrayList.get(rndIdx)).intValue();
        }
        Set<Integer> lineSet = (Set) hashMap4.get(Integer.valueOf(boxId));
        if (lineSet.size() == 1) {
            line = ((Integer) lineSet.iterator().next()).intValue();
        } else {
            ArrayList arrayList2 = new ArrayList();
            for (Integer add : lineSet) {
                arrayList2.add(add);
            }
            int rndIdx2 = (int) Math.floor(Math.random() * ((double) arrayList2.size()));
            if (rndIdx2 == arrayList2.size()) {
                rndIdx2--;
            }
            line = ((Integer) arrayList2.get(rndIdx2)).intValue();
        }
        gc.addLine(boxId, line);
        return 1;
    }

    private int getValueForNeighbor(int neighborBoxId, LineModel lm) {
        if (neighborBoxId == -1) {
            return 0;
        }
        if (lm.getLineCountForBox(neighborBoxId) == 2) {
            return -5;
        }
        return lm.getLineCountForBox(neighborBoxId) == 3 ? 5 : 0;
    }

    private int getTopNeighbor(int boxId, int boxCount) {
        if (boxId / boxCount > 0) {
            return boxId - boxCount;
        }
        return -1;
    }

    private int getRightNeighbor(int boxId, int boxCount) {
        if (boxId % boxCount != boxCount - 1) {
            return boxId + 1;
        }
        return -1;
    }

    private int getBottomNeighbor(int boxId, int boxCount) {
        if (boxId / boxCount != boxCount - 1) {
            return boxId + boxCount;
        }
        return -1;
    }

    private int getLeftNeighbor(int boxId, int boxCount) {
        if (boxId % boxCount != 0) {
            return boxId - 1;
        }
        return -1;
    }
}
