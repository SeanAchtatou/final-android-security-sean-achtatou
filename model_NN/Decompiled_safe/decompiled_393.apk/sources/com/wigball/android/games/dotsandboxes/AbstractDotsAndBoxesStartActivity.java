package com.wigball.android.games.dotsandboxes;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.android.vending.licensing.AESObfuscator;
import com.android.vending.licensing.LicenseChecker;
import com.android.vending.licensing.LicenseCheckerCallback;
import com.android.vending.licensing.ServerManagedPolicy;
import com.wigball.android.games.dotsandboxes.control.GameControl;
import com.wigball.android.games.dotsandboxes.demo.R;
import com.wigball.android.games.dotsandboxes.intents.IntentExtras;
import com.wigball.android.games.dotsandboxes.preferences.PreferencesKeys;
import com.wigball.android.games.dotsandboxes.util.License;

public abstract class AbstractDotsAndBoxesStartActivity extends AbstractActivity implements IntentExtras, PreferencesKeys, License {
    private static final int LICENSE_PROGRESS_DIALOG = 0;
    /* access modifiers changed from: private */
    public Button btnStartGame;
    /* access modifiers changed from: private */
    public GameControl gameControl;
    private Handler licenseCheckedHandler;
    private LicenseChecker licenseChecker;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        this.gameControl = null;
        this.btnStartGame = (Button) findViewById(R.id.btnStartGame);
        Button btnPlayer1 = (Button) findViewById(R.id.btnPlayer1);
        Button btnPlayer2 = (Button) findViewById(R.id.btnPlayer2);
        Button btnSettings = (Button) findViewById(R.id.btnSettings);
        Button btnExit = (Button) findViewById(R.id.btnExit);
        this.btnStartGame.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (AbstractDotsAndBoxesStartActivity.this.gameControl == null) {
                    AbstractDotsAndBoxesStartActivity.this.gameControl = new GameControl(AbstractDotsAndBoxesStartActivity.this);
                } else {
                    AbstractDotsAndBoxesStartActivity.this.gameControl.reset(AbstractDotsAndBoxesStartActivity.this);
                }
                Intent i = new Intent(AbstractDotsAndBoxesStartActivity.this, AbstractDotsAndBoxesStartActivity.this.getGameActivityClass());
                i.putExtra(IntentExtras.EXTRA_GAME_CONTROL, AbstractDotsAndBoxesStartActivity.this.gameControl);
                AbstractDotsAndBoxesStartActivity.this.startActivity(i);
            }
        });
        if (!isDemo()) {
            this.btnStartGame.setEnabled(false);
            this.licenseCheckedHandler = new Handler();
            this.licenseChecker = new LicenseChecker(this, new ServerManagedPolicy(this, new AESObfuscator(LICENSE_SALT, getPackageName(), ((TelephonyManager) getSystemService("phone")).getDeviceId())), License.LICENSE_KEY);
            showDialog(0);
            this.licenseChecker.checkAccess(new LicenseCheckerCallback() {
                public void allow() {
                    if (!AbstractDotsAndBoxesStartActivity.this.isFinishing()) {
                        AbstractDotsAndBoxesStartActivity.this.licenseResult(true);
                    }
                }

                public void dontAllow() {
                    if (!AbstractDotsAndBoxesStartActivity.this.isFinishing()) {
                        AbstractDotsAndBoxesStartActivity.this.licenseResult(false);
                    }
                }

                public void applicationError(LicenseCheckerCallback.ApplicationErrorCode errorCode) {
                    AbstractDotsAndBoxesStartActivity.this.licenseResult(false);
                }
            });
        }
        CharSequence[] choises = {getString(R.string.human), getString(R.string.droid_easy), getString(R.string.droid_medium), getString(R.string.droid_hard)};
        CharSequence[] choiseValues = {PreferencesKeys.VALUE_PLAYER_CHOISE_HUMAN, PreferencesKeys.VALUE_PLAYER_CHOISE_DROID_EASY, PreferencesKeys.VALUE_PLAYER_CHOISE_DROID_MEDIUM, PreferencesKeys.VALUE_PLAYER_CHOISE_DROID_HARD};
        final CharSequence[] charSequenceArr = choises;
        final CharSequence[] charSequenceArr2 = choiseValues;
        btnPlayer1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                String title = String.format(AbstractDotsAndBoxesStartActivity.this.getString(R.string.choosePlayer), String.valueOf(1));
                AlertDialog.Builder builder = new AlertDialog.Builder(AbstractDotsAndBoxesStartActivity.this);
                builder.setTitle(title);
                CharSequence[] charSequenceArr = charSequenceArr;
                final CharSequence[] charSequenceArr2 = charSequenceArr2;
                builder.setItems(charSequenceArr, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        if (AbstractDotsAndBoxesStartActivity.this.isDemo() && item == 3) {
                            Toast.makeText(AbstractDotsAndBoxesStartActivity.this, (int) R.string.hard_player_not_available_in_demo, 1).show();
                            item = 2;
                        }
                        AbstractDotsAndBoxesStartActivity.this.savePlayerSettings(1, charSequenceArr2[item]);
                        if (item != 0) {
                            AbstractDotsAndBoxesStartActivity.this.savePlayerSettings(2, charSequenceArr2[0]);
                        }
                        AbstractDotsAndBoxesStartActivity.this.refreshButtonText();
                    }
                });
                builder.create().show();
            }
        });
        final CharSequence[] charSequenceArr3 = choises;
        final CharSequence[] charSequenceArr4 = choiseValues;
        btnPlayer2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                String title = String.format(AbstractDotsAndBoxesStartActivity.this.getString(R.string.choosePlayer), String.valueOf(2));
                AlertDialog.Builder builder = new AlertDialog.Builder(AbstractDotsAndBoxesStartActivity.this);
                builder.setTitle(title);
                CharSequence[] charSequenceArr = charSequenceArr3;
                final CharSequence[] charSequenceArr2 = charSequenceArr4;
                builder.setItems(charSequenceArr, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        if (AbstractDotsAndBoxesStartActivity.this.isDemo() && item == 3) {
                            Toast.makeText(AbstractDotsAndBoxesStartActivity.this, (int) R.string.hard_player_not_available_in_demo, 1).show();
                            item = 2;
                        }
                        AbstractDotsAndBoxesStartActivity.this.savePlayerSettings(2, charSequenceArr2[item]);
                        if (item != 0) {
                            AbstractDotsAndBoxesStartActivity.this.savePlayerSettings(1, charSequenceArr2[0]);
                        }
                        AbstractDotsAndBoxesStartActivity.this.refreshButtonText();
                    }
                });
                builder.create().show();
            }
        });
        btnSettings.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                AbstractDotsAndBoxesStartActivity.this.startActivity(new Intent(AbstractDotsAndBoxesStartActivity.this, AbstractDotsAndBoxesStartActivity.this.getPreferencesClass()));
            }
        });
        btnExit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                AbstractDotsAndBoxesStartActivity.this.finish();
            }
        });
        refreshButtonText();
        if (isDemo()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.demo_question);
            builder.setCancelable(false);
            builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    AbstractDotsAndBoxesStartActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.wigball.android.games.dotsandboxes")));
                    dialog.cancel();
                }
            });
            builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            builder.create().show();
        }
    }

    /* access modifiers changed from: private */
    public void savePlayerSettings(int player, CharSequence choise) {
        String key = player == 1 ? PreferencesKeys.KEY_PLAYER_CHOISE_PLAYER_1 : PreferencesKeys.KEY_PLAYER_CHOISE_PLAYER_2;
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.putString(key, choise.toString());
        editor.commit();
    }

    /* access modifiers changed from: private */
    public void refreshButtonText() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String player1Value = prefs.getString(PreferencesKeys.KEY_PLAYER_CHOISE_PLAYER_1, PreferencesKeys.VALUE_PLAYER_CHOISE_HUMAN);
        String player2Value = prefs.getString(PreferencesKeys.KEY_PLAYER_CHOISE_PLAYER_2, PreferencesKeys.VALUE_PLAYER_CHOISE_DROID_EASY);
        String player = getString(R.string.player);
        ((Button) findViewById(R.id.btnPlayer1)).setText(String.valueOf(player) + " 1: " + translateChoiseString(player1Value));
        ((Button) findViewById(R.id.btnPlayer2)).setText(String.valueOf(player) + " 2: " + translateChoiseString(player2Value));
    }

    private String translateChoiseString(String choise) {
        if (PreferencesKeys.VALUE_PLAYER_CHOISE_HUMAN.equals(choise)) {
            return getString(R.string.human);
        }
        if (PreferencesKeys.VALUE_PLAYER_CHOISE_DROID_EASY.equals(choise)) {
            return getString(R.string.droid_easy);
        }
        if (PreferencesKeys.VALUE_PLAYER_CHOISE_DROID_MEDIUM.equals(choise)) {
            return getString(R.string.droid_medium);
        }
        if (PreferencesKeys.VALUE_PLAYER_CHOISE_DROID_HARD.equals(choise)) {
            return getString(R.string.droid_hard);
        }
        return "";
    }

    /* access modifiers changed from: private */
    public void licenseResult(final boolean ok) {
        this.licenseCheckedHandler.post(new Runnable() {
            public void run() {
                AbstractDotsAndBoxesStartActivity.this.dismissDialog(0);
                if (!ok) {
                    Toast.makeText(AbstractDotsAndBoxesStartActivity.this, (int) R.string.licenseCheckFailed, 1).show();
                    AbstractDotsAndBoxesStartActivity.this.btnStartGame.setEnabled(false);
                    return;
                }
                AbstractDotsAndBoxesStartActivity.this.btnStartGame.setEnabled(true);
            }
        });
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                return ProgressDialog.show(this, "", getString(R.string.licenseProgress), false);
            default:
                return null;
        }
    }
}
