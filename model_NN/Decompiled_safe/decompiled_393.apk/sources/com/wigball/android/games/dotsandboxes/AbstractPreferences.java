package com.wigball.android.games.dotsandboxes;

import android.os.Bundle;
import com.wigball.android.games.dotsandboxes.demo.R;

public abstract class AbstractPreferences extends AbstractPreferenceActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }
}
