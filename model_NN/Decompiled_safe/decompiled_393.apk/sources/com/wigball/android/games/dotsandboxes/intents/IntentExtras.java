package com.wigball.android.games.dotsandboxes.intents;

public interface IntentExtras {
    public static final String EXTRA_GAME_CONTROL = "dotsandboxes.gamecontrol";
}
