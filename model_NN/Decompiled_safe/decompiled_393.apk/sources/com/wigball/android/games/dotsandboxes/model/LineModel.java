package com.wigball.android.games.dotsandboxes.model;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.HashSet;
import java.util.Set;

public class LineModel implements Serializable {
    public static final int LINE_BOTTOM = 2;
    public static final int LINE_LEFT = 3;
    public static final int LINE_RIGHT = 1;
    public static final int LINE_TOP = 0;
    public static final int NO_BOX = -1;
    private static final long serialVersionUID = 712365808829659426L;
    private int boxCount;
    private boolean[][] horizontalLineStatus;
    boolean lastDrawnWasHorizontal = false;
    private int lastDrawnX;
    private int lastDrawnY;
    private boolean[][] verticalLineStatus;

    public LineModel(int boxCount2) {
        this.boxCount = boxCount2;
        this.verticalLineStatus = (boolean[][]) Array.newInstance(Boolean.TYPE, boxCount2 + 1, boxCount2);
        this.horizontalLineStatus = (boolean[][]) Array.newInstance(Boolean.TYPE, boxCount2, boxCount2 + 1);
        this.lastDrawnX = -1;
        this.lastDrawnY = -1;
    }

    public void addLine(int box, int line) {
        int boxY = box / this.boxCount;
        int boxX = box % this.boxCount;
        switch (line) {
            case 0:
                this.horizontalLineStatus[boxX][boxY] = true;
                this.lastDrawnX = boxX;
                this.lastDrawnY = boxY;
                this.lastDrawnWasHorizontal = true;
                return;
            case 1:
                this.verticalLineStatus[boxX + 1][boxY] = true;
                this.lastDrawnX = boxX + 1;
                this.lastDrawnY = boxY;
                this.lastDrawnWasHorizontal = false;
                return;
            case 2:
                this.horizontalLineStatus[boxX][boxY + 1] = true;
                this.lastDrawnX = boxX;
                this.lastDrawnY = boxY + 1;
                this.lastDrawnWasHorizontal = true;
                return;
            case LINE_LEFT /*3*/:
                this.verticalLineStatus[boxX][boxY] = true;
                this.lastDrawnX = boxX;
                this.lastDrawnY = boxY;
                this.lastDrawnWasHorizontal = false;
                return;
            default:
                return;
        }
    }

    public boolean isLastDrawn(int box, int line) {
        int boxY = box / this.boxCount;
        int boxX = box % this.boxCount;
        switch (line) {
            case 0:
                return this.lastDrawnWasHorizontal && boxX == this.lastDrawnX && boxY == this.lastDrawnY;
            case 1:
                return !this.lastDrawnWasHorizontal && boxX + 1 == this.lastDrawnX && boxY == this.lastDrawnY;
            case 2:
                return this.lastDrawnWasHorizontal && boxX == this.lastDrawnX && this.lastDrawnY == boxY + 1;
            case LINE_LEFT /*3*/:
                return !this.lastDrawnWasHorizontal && boxX == this.lastDrawnX && boxY == this.lastDrawnY;
            default:
                return false;
        }
    }

    public boolean isBoxClosed(int box) {
        return isLineDrawn(box, 0) && isLineDrawn(box, 1) && isLineDrawn(box, 2) && isLineDrawn(box, 3);
    }

    public int getLineCountForBox(int box) {
        int count = 0;
        if (isLineDrawn(box, 0)) {
            count = 0 + 1;
        }
        if (isLineDrawn(box, 1)) {
            count++;
        }
        if (isLineDrawn(box, 2)) {
            count++;
        }
        if (isLineDrawn(box, 3)) {
            return count + 1;
        }
        return count;
    }

    public int getRandomBoxWithOpenSides(int openSideCount) {
        int closedSides = 4 - openSideCount;
        Set<Integer> usedBoxes = new HashSet<>();
        int maxBoxes = this.boxCount * this.boxCount;
        while (usedBoxes.size() != maxBoxes) {
            int boxX = (int) Math.floor(Math.random() * ((double) this.boxCount));
            if (boxX == this.boxCount) {
                boxX--;
            }
            int boxY = (int) Math.floor(Math.random() * ((double) this.boxCount));
            if (boxY == this.boxCount) {
                boxY--;
            }
            int boxId = (this.boxCount * boxY) + boxX;
            if (getLineCountForBox(boxId) == closedSides) {
                return boxId;
            }
            usedBoxes.add(Integer.valueOf(boxId));
        }
        return -1;
    }

    public boolean isLineDrawn(int box, int line) {
        int boxY = box / this.boxCount;
        int boxX = box % this.boxCount;
        switch (line) {
            case 0:
                return this.horizontalLineStatus[boxX][boxY];
            case 1:
                return this.verticalLineStatus[boxX + 1][boxY];
            case 2:
                return this.horizontalLineStatus[boxX][boxY + 1];
            case LINE_LEFT /*3*/:
                return this.verticalLineStatus[boxX][boxY];
            default:
                return false;
        }
    }

    public int getFreeLine(int box) {
        int boxY = box / this.boxCount;
        int boxX = box % this.boxCount;
        if (!this.horizontalLineStatus[boxX][boxY]) {
            return 0;
        }
        if (!this.verticalLineStatus[boxX + 1][boxY]) {
            return 1;
        }
        if (!this.horizontalLineStatus[boxX][boxY + 1]) {
            return 2;
        }
        if (!this.verticalLineStatus[boxX][boxY]) {
            return 3;
        }
        return -1;
    }

    public void prefillOuterLines() {
        for (int i = 0; i < this.boxCount; i++) {
            this.verticalLineStatus[0][i] = true;
            this.verticalLineStatus[this.verticalLineStatus.length - 1][i] = true;
        }
        for (int i2 = 0; i2 < this.horizontalLineStatus.length; i2++) {
            this.horizontalLineStatus[i2][0] = true;
            this.horizontalLineStatus[i2][this.horizontalLineStatus[i2].length - 1] = true;
        }
    }
}
