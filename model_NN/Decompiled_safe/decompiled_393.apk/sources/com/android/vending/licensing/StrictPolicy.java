package com.android.vending.licensing;

import com.android.vending.licensing.Policy;

public class StrictPolicy implements Policy {
    private Policy.LicenseResponse mLastResponse = Policy.LicenseResponse.RETRY;

    public void processServerResponse(Policy.LicenseResponse response, ResponseData rawData) {
        this.mLastResponse = response;
    }

    public boolean allowAccess() {
        return this.mLastResponse == Policy.LicenseResponse.LICENSED;
    }
}
