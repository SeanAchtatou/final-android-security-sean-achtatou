package com.android.vending.licensing;

import com.android.vending.licensing.Policy;

public class NullDeviceLimiter implements DeviceLimiter {
    public Policy.LicenseResponse isDeviceAllowed(String userId) {
        return Policy.LicenseResponse.LICENSED;
    }
}
