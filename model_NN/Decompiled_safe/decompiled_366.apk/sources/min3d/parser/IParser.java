package min3d.parser;

import min3d.animation.AnimationObject3d;
import min3d.core.Object3dContainer;

public interface IParser {
    AnimationObject3d getParsedAnimationObject();

    Object3dContainer getParsedObject();

    void parse();
}
