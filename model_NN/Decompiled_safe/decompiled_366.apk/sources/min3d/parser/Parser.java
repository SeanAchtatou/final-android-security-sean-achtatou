package min3d.parser;

import android.content.res.Resources;

public class Parser {
    private static /* synthetic */ int[] $SWITCH_TABLE$min3d$parser$Parser$Type;

    public enum Type {
        OBJ,
        MAX_3DS,
        MD2
    }

    static /* synthetic */ int[] $SWITCH_TABLE$min3d$parser$Parser$Type() {
        int[] iArr = $SWITCH_TABLE$min3d$parser$Parser$Type;
        if (iArr == null) {
            iArr = new int[Type.values().length];
            try {
                iArr[Type.MAX_3DS.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[Type.MD2.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[Type.OBJ.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$min3d$parser$Parser$Type = iArr;
        }
        return iArr;
    }

    public static IParser createParser(Type type, Resources resources, String resourceID, boolean generateMipMap) {
        switch ($SWITCH_TABLE$min3d$parser$Parser$Type()[type.ordinal()]) {
            case 1:
                return new ObjParser(resources, resourceID, generateMipMap);
            case 2:
                return new Max3DSParser(resources, resourceID, generateMipMap);
            case 3:
                return new MD2Parser(resources, resourceID, generateMipMap);
            default:
                return null;
        }
    }
}
