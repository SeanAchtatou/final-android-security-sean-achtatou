package min3d.vos;

import min3d.interfaces.IDirtyManaged;
import min3d.interfaces.IDirtyParent;

public abstract class AbstractDirtyManaged implements IDirtyManaged {
    protected boolean _dirty;
    protected IDirtyParent _parent;

    public AbstractDirtyManaged(IDirtyParent $parent) {
        this._parent = $parent;
    }

    public boolean isDirty() {
        return this._dirty;
    }

    public void setDirtyFlag() {
        this._dirty = true;
        if (this._parent != null) {
            this._parent.onDirty();
        }
    }

    public void clearDirtyFlag() {
        this._dirty = false;
    }
}
