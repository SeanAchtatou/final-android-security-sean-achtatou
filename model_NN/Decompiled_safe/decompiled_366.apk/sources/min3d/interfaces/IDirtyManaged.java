package min3d.interfaces;

public interface IDirtyManaged {
    void clearDirtyFlag();

    boolean isDirty();

    void setDirtyFlag();
}
