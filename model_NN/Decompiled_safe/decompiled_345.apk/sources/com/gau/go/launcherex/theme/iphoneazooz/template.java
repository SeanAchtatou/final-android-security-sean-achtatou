package com.gau.go.launcherex.theme.iphoneazooz;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Process;
import android.view.KeyEvent;

public class template extends Activity {
    private CustomAlertDialog mDialog;

    class CustomAlertDialog extends AlertDialog {
        public CustomAlertDialog(Context context) {
            super(context);
        }

        public boolean onKeyDown(int i, KeyEvent keyEvent) {
            boolean onKeyDown = super.onKeyDown(i, keyEvent);
            template.this.finish();
            return onKeyDown;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.main);
        if (isExistSkin("com.gau.go.launcherex")) {
            startGOLauncher("com.gau.go.launcherex");
            finish();
            return;
        }
        this.mDialog = new CustomAlertDialog(this);
        this.mDialog.setTitle((int) R.string.dialogtitle);
        this.mDialog.setMessage(getResources().getString(R.string.dialogcontent));
        this.mDialog.setButton(-1, getResources().getString(R.string.dialogok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pname:com.gau.go.launcherex"));
                intent.setPackage("com.android.vending");
                intent.setFlags(268435456);
                try {
                    template.this.startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Intent intent2 = new Intent("android.intent.action.VIEW", Uri.parse("http://61.145.124.93/soft/3GHeart/com.gau.go.launcherex.apk"));
                    intent2.setFlags(268435456);
                    try {
                        template.this.startActivity(intent2);
                    } catch (ActivityNotFoundException e2) {
                        e2.printStackTrace();
                    }
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
                template.this.finish();
            }
        });
        this.mDialog.show();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        Process.killProcess(Process.myPid());
    }

    private boolean isExistSkin(String str) {
        try {
            createPackageContext(str, 2);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    private void startGOLauncher(String str) {
        Intent launchIntentForPackage = getPackageManager().getLaunchIntentForPackage(str);
        if (launchIntentForPackage != null) {
            try {
                startActivity(launchIntentForPackage);
            } catch (ActivityNotFoundException e) {
            }
        }
    }
}
