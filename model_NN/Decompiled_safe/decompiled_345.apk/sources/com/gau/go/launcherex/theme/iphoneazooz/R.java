package com.gau.go.launcherex.theme.iphoneazooz;

public final class R {

    public static final class array {
        public static final int wallpaperlist = 2130968576;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int allapp = 2130837504;
        public static final int allapp_selected = 2130837505;

        /* renamed from: appfunc_folder_close_up */
        public static final int androidsysteminfo = 2130837506;

        /* renamed from: appfunc_folder_open */
        public static final int angrybirds = 2130837507;

        /* renamed from: appfunc_folderback */
        public static final int appfunc_folder_close_up = 2130837508;

        /* renamed from: appfunc_movetodesk */
        public static final int appfunc_folder_open = 2130837509;

        /* renamed from: appfunc_rename */
        public static final int appfunc_folderback = 2130837510;

        /* renamed from: appfunc_screennow */
        public static final int appfunc_movetodesk = 2130837511;

        /* renamed from: appfunc_screenother */
        public static final int appfunc_rename = 2130837512;

        /* renamed from: appfunchome */
        public static final int appfunc_screennow = 2130837513;

        /* renamed from: browser */
        public static final int appfunc_screenother = 2130837514;

        /* renamed from: button_h */
        public static final int appfunchome = 2130837515;

        /* renamed from: button_h2 */
        public static final int browser = 2130837516;

        /* renamed from: button_v */
        public static final int button_h = 2130837517;

        /* renamed from: button_v2 */
        public static final int button_h2 = 2130837518;

        /* renamed from: calculator */
        public static final int button_v = 2130837519;

        /* renamed from: calendar */
        public static final int button_v2 = 2130837520;

        /* renamed from: camera */
        public static final int calculator = 2130837521;

        /* renamed from: clock */
        public static final int calendar = 2130837522;

        /* renamed from: contacts */
        public static final int camera = 2130837523;

        /* renamed from: d_add */
        public static final int clock = 2130837524;

        /* renamed from: d_home */
        public static final int contacts = 2130837525;

        /* renamed from: default_wallpaper */
        public static final int d_home = 2130837526;

        /* renamed from: default_wallpaper2 */
        public static final int default_wallpaper = 2130837527;

        /* renamed from: default_wallpaper2_thumb */
        public static final int default_wallpaper2 = 2130837528;

        /* renamed from: default_wallpaper_thumb */
        public static final int default_wallpaper2_thumb = 2130837529;

        /* renamed from: dock */
        public static final int default_wallpaper3 = 2130837530;

        /* renamed from: email */
        public static final int default_wallpaper3_thumb = 2130837531;

        /* renamed from: es */
        public static final int default_wallpaper_thumb = 2130837532;

        /* renamed from: evernote */
        public static final int dock = 2130837533;

        /* renamed from: facebook */
        public static final int dropbox = 2130837534;

        /* renamed from: folder_close_light */
        public static final int email = 2130837535;

        /* renamed from: folder_close_up */
        public static final int es = 2130837536;

        /* renamed from: folder_open */
        public static final int estaskmanager = 2130837537;

        /* renamed from: folderback */
        public static final int evernote = 2130837538;

        /* renamed from: folderclose */
        public static final int facebook = 2130837539;

        /* renamed from: folderopenback */
        public static final int folder_close_light = 2130837540;

        /* renamed from: funbg */
        public static final int folder_close_up = 2130837541;

        /* renamed from: gallery */
        public static final int folder_open = 2130837542;

        /* renamed from: gmail */
        public static final int folderback = 2130837543;

        /* renamed from: googlesearch */
        public static final int folderclose = 2130837544;

        /* renamed from: gtalk */
        public static final int folderopenback = 2130837545;

        /* renamed from: handcent */
        public static final int funbg = 2130837546;

        /* renamed from: history */
        public static final int gallery = 2130837547;

        /* renamed from: history_selected */
        public static final int gmail = 2130837548;

        /* renamed from: homebg */
        public static final int golauncher = 2130837549;

        /* renamed from: icon */
        public static final int googlesearch = 2130837550;

        /* renamed from: iconback */
        public static final int gtalk = 2130837551;

        /* renamed from: iconback2 */
        public static final int history = 2130837552;

        /* renamed from: iconback3 */
        public static final int history_selected = 2130837553;

        /* renamed from: iconback4 */
        public static final int homebg = 2130837554;

        /* renamed from: iconback5 */
        public static final int icon = 2130837555;

        /* renamed from: kikmessenger */
        public static final int iconback = 2130837556;

        /* renamed from: lab_rab_empty_bg */
        public static final int iconback2 = 2130837557;

        /* renamed from: maps */
        public static final int iconback3 = 2130837558;

        /* renamed from: market */
        public static final int iconback4 = 2130837559;

        /* renamed from: meno */
        public static final int iconupon = 2130837560;

        /* renamed from: messaging */
        public static final int kikmessenger = 2130837561;

        /* renamed from: music */
        public static final int lab_rab_empty_bg = 2130837562;

        /* renamed from: opera */
        public static final int maps = 2130837563;

        /* renamed from: operamini */
        public static final int market = 2130837564;

        /* renamed from: phone */
        public static final int meno = 2130837565;

        /* renamed from: picplz */
        public static final int messaging = 2130837566;

        /* renamed from: renren */
        public static final int music = 2130837567;

        /* renamed from: run */
        public static final int navigation = 2130837568;

        /* renamed from: run_selected */
        public static final int phone = 2130837569;

        /* renamed from: screennow */
        public static final int places = 2130837570;

        /* renamed from: screenother */
        public static final int run = 2130837571;

        /* renamed from: settings */
        public static final int run_selected = 2130837572;

        /* renamed from: shortcut_light_iconbg */
        public static final int screennow = 2130837573;

        /* renamed from: t01 */
        public static final int screenother = 2130837574;

        /* renamed from: t02 */
        public static final int settings = 2130837575;

        /* renamed from: tab_h */
        public static final int shortcut_light_iconbg = 2130837576;

        /* renamed from: tab_v */
        public static final int t01 = 2130837577;

        /* renamed from: tabfocused_h */
        public static final int tab_h = 2130837578;

        /* renamed from: tabfocused_v */
        public static final int tab_v = 2130837579;

        /* renamed from: tablight_h */
        public static final int tabfocused_h = 2130837580;

        /* renamed from: tablight_v */
        public static final int tabfocused_v = 2130837581;

        /* renamed from: taskmanager */
        public static final int tablight_h = 2130837582;

        /* renamed from: themepreview */
        public static final int tablight_v = 2130837583;

        /* renamed from: tray_handle_normal */
        public static final int themepreview = 2130837584;

        /* renamed from: twitter */
        public static final int tray_handle_normal = 2130837585;

        /* renamed from: voicesearch */
        public static final int twitter = 2130837586;

        /* renamed from: weibo */
        public static final int voicerecorder = 2130837587;

        /* renamed from: youtube */
        public static final int voicesearch = 2130837588;
        /* added by JADX */

        /* renamed from: androidsysteminfo  reason: collision with other field name */
        public static final int f0androidsysteminfo = 2130837506;
        /* added by JADX */

        /* renamed from: angrybirds  reason: collision with other field name */
        public static final int f1angrybirds = 2130837507;
        /* added by JADX */

        /* renamed from: default_wallpaper3  reason: collision with other field name */
        public static final int f2default_wallpaper3 = 2130837530;
        /* added by JADX */

        /* renamed from: default_wallpaper3_thumb  reason: collision with other field name */
        public static final int f3default_wallpaper3_thumb = 2130837531;
        /* added by JADX */

        /* renamed from: dropbox  reason: collision with other field name */
        public static final int f4dropbox = 2130837534;
        /* added by JADX */

        /* renamed from: estaskmanager  reason: collision with other field name */
        public static final int f5estaskmanager = 2130837537;
        /* added by JADX */

        /* renamed from: golauncher  reason: collision with other field name */
        public static final int f6golauncher = 2130837549;
        /* added by JADX */

        /* renamed from: iconupon  reason: collision with other field name */
        public static final int f7iconupon = 2130837560;
        /* added by JADX */

        /* renamed from: navigation  reason: collision with other field name */
        public static final int f8navigation = 2130837568;
        /* added by JADX */

        /* renamed from: places  reason: collision with other field name */
        public static final int f9places = 2130837570;
        /* added by JADX */

        /* renamed from: voicerecorder  reason: collision with other field name */
        public static final int f10voicerecorder = 2130837587;
        /* added by JADX */
        public static final int yahoomail = 2130837589;
        /* added by JADX */
        public static final int yahoomessenger = 2130837590;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class string {
        public static final int app_name = 2131034113;
        public static final int dialogcontent = 2131034115;
        public static final int dialogok = 2131034116;
        public static final int dialogtitle = 2131034114;
        public static final int hello = 2131034112;
        public static final int theme_info = 2131034118;
        public static final int theme_title = 2131034117;
    }
}
