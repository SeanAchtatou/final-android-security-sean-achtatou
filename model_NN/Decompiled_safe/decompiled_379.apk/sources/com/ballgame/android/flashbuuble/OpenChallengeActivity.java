package com.ballgame.android.flashbuuble;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.scoreloop.client.android.core.model.Challenge;
import com.scoreloop.client.android.core.model.Session;

public class OpenChallengeActivity extends NewChallengeActivity {
    /* access modifiers changed from: protected */
    public void initButtons() {
        this.playChallengeButton = (Button) findViewById(R.id.new_challenge_play_button);
        this.playChallengeButton.setEnabled(false);
        this.playChallengeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Challenge challenge = new Challenge(OpenChallengeActivity.this.getSelectedStake());
                challenge.setContender(Session.getCurrentSession().getUser());
                ScoreloopManager.setCurrentChallenge(challenge);
                OpenChallengeActivity.this.startActivity(new Intent(OpenChallengeActivity.this, GamePlayActivity.class));
                OpenChallengeActivity.this.finish();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void initChallengeInfo() {
        ((TextView) findViewById(R.id.challenge_info)).setText((int) R.string.new_open_challenge);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.open_challenge);
    }
}
