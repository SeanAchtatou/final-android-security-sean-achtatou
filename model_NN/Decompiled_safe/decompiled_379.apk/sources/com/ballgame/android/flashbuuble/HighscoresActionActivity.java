package com.ballgame.android.flashbuuble;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class HighscoresActionActivity extends BaseActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.highscores_action);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        ((TextView) findViewById(R.id.user_info)).setText(String.format(getString(R.string.user_info_format), ScoreloopManager.getPossibleOpponent().getLogin()));
        ((Button) findViewById(R.id.challenge_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HighscoresActionActivity.this.startActivity(new Intent(HighscoresActionActivity.this, DirectChallengeActivity.class));
            }
        });
    }
}
