package com.ballgame.android.flashbuuble;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import com.ballgame.android.flashbuuble.BaseActivity;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.UserController;
import com.scoreloop.client.android.core.model.Money;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import java.util.List;

public abstract class NewChallengeActivity extends BaseActivity {
    private View balanceTooLowView;
    protected User opponent;
    protected Button playChallengeButton;
    protected int scoreResult;
    private TextView stakeDisplay;
    private SeekBar stakeSlider;
    private List<Money> stakeValues;

    /* access modifiers changed from: protected */
    public abstract void initButtons();

    /* access modifiers changed from: protected */
    public abstract void initChallengeInfo();

    private class UserUpdateObserver extends BaseActivity.UserGenericObserver {
        private UserUpdateObserver() {
            super();
        }

        /* synthetic */ UserUpdateObserver(NewChallengeActivity newChallengeActivity, UserUpdateObserver userUpdateObserver) {
            this();
        }

        public void requestControllerDidReceiveResponse(RequestController aRequestController) {
            NewChallengeActivity.this.hideProgressIndicator();
            NewChallengeActivity.this.populateStakeValues();
            NewChallengeActivity.this.updateBalanceView();
        }
    }

    private void initBalanceInfo() {
        findViewById(R.id.new_challenge_balance).setVisibility(4);
    }

    private void initBalanceTooLowInfo() {
        this.balanceTooLowView = findViewById(R.id.new_challenge_balance_too_low);
    }

    private void initStakeControls() {
        this.stakeDisplay = (TextView) findViewById(R.id.new_challenge_stake_display);
        this.stakeDisplay.setVisibility(8);
        this.stakeSlider = (SeekBar) findViewById(R.id.new_challenge_stake_slider);
        this.stakeSlider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                NewChallengeActivity.this.onStakeChanged();
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        this.stakeSlider.setEnabled(false);
    }

    private void initStakeHeader() {
        ((TextView) findViewById(R.id.new_challenge_stake_header)).setText((int) R.string.new_challenge_stake_header);
    }

    /* access modifiers changed from: private */
    public void onStakeChanged() {
        boolean enoughFunds;
        int i;
        Money stake = getSelectedStake();
        this.stakeDisplay.setText(formatMoney(stake));
        this.stakeDisplay.setVisibility(0);
        if (stake.compareTo(Session.getCurrentSession().getBalance()) <= 0) {
            enoughFunds = true;
        } else {
            enoughFunds = false;
        }
        this.playChallengeButton.setEnabled(isPlayChallengeButtonEnabled(enoughFunds));
        View view = this.balanceTooLowView;
        if (enoughFunds) {
            i = 4;
        } else {
            i = 0;
        }
        view.setVisibility(i);
    }

    /* access modifiers changed from: private */
    public void populateStakeValues() {
        this.stakeValues = Session.getCurrentSession().getChallengeStakes();
        this.stakeSlider.setMax(this.stakeValues.size() - 1);
        this.stakeSlider.setProgress(0);
        this.stakeSlider.setEnabled(true);
        onStakeChanged();
    }

    /* access modifiers changed from: private */
    public void updateBalanceView() {
        TextView balanceView = (TextView) findViewById(R.id.new_challenge_balance);
        String balanceString = formatMoney(Session.getCurrentSession().getBalance());
        balanceView.setText(String.format(getString(R.string.new_challenge_balance), balanceString));
        balanceView.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public Money getSelectedStake() {
        int aStakeIndex = this.stakeSlider.getProgress();
        if (aStakeIndex >= this.stakeValues.size()) {
            aStakeIndex = 0;
        }
        return this.stakeValues.get(aStakeIndex);
    }

    /* access modifiers changed from: protected */
    public boolean isPlayChallengeButtonEnabled(boolean suggestion) {
        return suggestion;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.opponent = ScoreloopManager.getPossibleOpponent();
        initChallengeInfo();
        initStakeHeader();
        initBalanceInfo();
        initBalanceTooLowInfo();
        initStakeControls();
        initButtons();
        new UserController(new UserUpdateObserver(this, null)).updateUser();
        showProgressIndicator();
    }
}
