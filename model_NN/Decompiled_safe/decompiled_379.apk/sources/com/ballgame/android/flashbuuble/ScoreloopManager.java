package com.ballgame.android.flashbuuble;

import android.content.Context;
import com.scoreloop.client.android.core.model.Challenge;
import com.scoreloop.client.android.core.model.Client;
import com.scoreloop.client.android.core.model.Range;
import com.scoreloop.client.android.core.model.User;

abstract class ScoreloopManager {
    static final int GAME_MODE_MIN = 0;
    private static Challenge challenge;
    private static Client client;
    private static User possibleOpponent;

    ScoreloopManager() {
    }

    static Challenge getCurrentChallenge() {
        return challenge;
    }

    static User getPossibleOpponent() {
        return possibleOpponent;
    }

    static boolean hasCurrentChallenge() {
        return getCurrentChallenge() != null;
    }

    static void init(Context context, String gameID, String gameSecret) {
        if (client == null) {
            client = new Client(context, gameID, gameSecret, null);
            client.setGameModes(new Range(0, 1));
        }
    }

    static void resetCurrentChallenge() {
        possibleOpponent = null;
        setCurrentChallenge(null);
    }

    static void setCurrentChallenge(Challenge challenge2) {
        challenge = challenge2;
    }

    static void setPossibleOpponent(User opponent) {
        possibleOpponent = opponent;
    }

    public static void setNumberOfModes(int modeCount) {
        if (client != null) {
            client.setGameModes(new Range(0, modeCount));
            return;
        }
        throw new IllegalStateException("client object is null. has ScoreloopManager.init() been called?");
    }
}
