package com.ballgame.android.flashbuuble;

import java.lang.reflect.Array;

public class BBMatrix {
    public int[][] mBubbleGrid = ((int[][]) Array.newInstance(Integer.TYPE, this.mXBubbleCount, this.mYBubbleCount));
    public boolean[][] mBubbleMarked = ((boolean[][]) Array.newInstance(Boolean.TYPE, this.mXBubbleCount, this.mYBubbleCount));
    public int mSameBubbleCount;
    public int mXBubbleCount;
    public int mYBubbleCount;

    public BBMatrix(int pViewWidth, int pViewHeight) {
        this.mXBubbleCount = (int) Math.floor((double) (pViewWidth / 32));
        this.mYBubbleCount = (int) Math.floor((double) (pViewHeight / 32));
        clearBubbles();
        fillBBMatrix();
    }

    public void fillBBMatrix() {
        for (int i = 0; i < this.mXBubbleCount; i++) {
            for (int j = 0; j < this.mYBubbleCount; j++) {
                newBubble(i, j, (int) (Math.random() * 5.0d));
            }
        }
    }

    public void newBubble(int x, int y, int pBubbleType) {
        this.mBubbleGrid[x][y] = pBubbleType;
        this.mBubbleMarked[x][y] = false;
    }

    public void findSameBubble(int x, int y) {
        if (x >= 0 && y >= 0 && x < this.mXBubbleCount && y < this.mYBubbleCount && this.mBubbleGrid[x][y] != -1) {
            this.mSameBubbleCount++;
            this.mBubbleMarked[x][y] = true;
            int color = this.mBubbleGrid[x][y];
            if (getColor(x, y + 1) == color && !isMarked(x, y + 1)) {
                findSameBubble(x, y + 1);
            }
            if (getColor(x, y - 1) == color && !isMarked(x, y - 1)) {
                findSameBubble(x, y - 1);
            }
            if (getColor(x + 1, y) == color && !isMarked(x + 1, y)) {
                findSameBubble(x + 1, y);
            }
            if (getColor(x - 1, y) == color && !isMarked(x - 1, y)) {
                findSameBubble(x - 1, y);
            }
        }
    }

    public boolean isEmptyColumn(int x) {
        return isNullBubble(x, this.mYBubbleCount - 1);
    }

    public void removeMarkedBubbles() {
        for (int y = 0; y < this.mYBubbleCount; y++) {
            for (int x = 0; x < this.mXBubbleCount; x++) {
                if (isMarked(x, y)) {
                    removeBubble(x, y);
                    for (int i = y; i > 0; i--) {
                        moveBubble(x, i - 1, x, i);
                    }
                }
            }
        }
        for (int x2 = 0; x2 < this.mXBubbleCount; x2++) {
            if (isEmptyColumn(x2)) {
                int i2 = x2 + 1;
                while (i2 < this.mXBubbleCount && isEmptyColumn(i2)) {
                    i2++;
                }
                if (i2 < this.mXBubbleCount) {
                    for (int y2 = 0; y2 < this.mYBubbleCount; y2++) {
                        moveBubble(i2, y2, x2, y2);
                    }
                }
            }
        }
    }

    public void removeBubble(int x, int y) {
        if (!isNullBubble(x, y)) {
            this.mBubbleGrid[x][y] = -1;
        }
    }

    public void removeMark() {
        this.mSameBubbleCount = 0;
        for (int i = 0; i < this.mXBubbleCount; i++) {
            for (int j = 0; j < this.mYBubbleCount; j++) {
                this.mBubbleMarked[i][j] = false;
            }
        }
    }

    public void moveBubble(int x, int y, int toX, int toY) {
        if (x != toX || y != toY) {
            this.mBubbleGrid[toX][toY] = this.mBubbleGrid[x][y];
            this.mBubbleGrid[x][y] = -1;
        }
    }

    public boolean isNullBubble(int x, int y) {
        if (x < 0 || y < 0 || x >= this.mXBubbleCount || y >= this.mYBubbleCount || -1 == this.mBubbleGrid[x][y]) {
            return true;
        }
        return false;
    }

    public int getColor(int x, int y) {
        if (isNullBubble(x, y)) {
            return -1;
        }
        return this.mBubbleGrid[x][y];
    }

    public boolean isMarked(int x, int y) {
        if (isNullBubble(x, y)) {
            return false;
        }
        return this.mBubbleMarked[x][y];
    }

    public void clearBubbles() {
        for (int x = 0; x < this.mXBubbleCount; x++) {
            for (int y = 0; y < this.mYBubbleCount; y++) {
                setBubble(x, y, -1);
            }
        }
    }

    public void setBubble(int x, int y, int bubbleIndex) {
        this.mBubbleGrid[x][y] = bubbleIndex;
    }

    public boolean isBBMatrixSolvable() {
        for (int y = this.mYBubbleCount - 1; y > -1; y--) {
            for (int x = 0; x < this.mXBubbleCount; x++) {
                if (!isNullBubble(x, y)) {
                    if (!isNullBubble(x + 1, y) && this.mBubbleGrid[x][y] == this.mBubbleGrid[x + 1][y]) {
                        return true;
                    }
                    if (!isNullBubble(x, y - 1) && this.mBubbleGrid[x][y] == this.mBubbleGrid[x][y - 1]) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
