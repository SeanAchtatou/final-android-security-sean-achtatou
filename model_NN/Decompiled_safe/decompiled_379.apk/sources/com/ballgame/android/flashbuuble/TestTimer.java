package com.ballgame.android.flashbuuble;

import com.ballgame.android.flashbuuble.MyTimer;

public class TestTimer implements MyTimer.CallBack {
    private BBView mBBView;
    MyTimer mTimer = new MyTimer(10, this);

    public TestTimer(BBView bv) {
        this.mBBView = bv;
        this.mTimer.startTimer();
    }

    public void timerCallBack() {
        this.mBBView.RefeshDrawTime();
    }
}
