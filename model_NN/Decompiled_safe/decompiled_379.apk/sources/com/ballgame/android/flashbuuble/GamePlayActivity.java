package com.ballgame.android.flashbuuble;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import com.scoreloop.client.android.core.controller.RankingController;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.model.Challenge;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.SearchList;
import com.scoreloop.client.android.core.model.Session;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class GamePlayActivity extends BaseActivity {
    static final String KEY_MODE = "GAME_MODE";
    static final String KEY_RESULT = "GAME_RESULT";
    private static final int MAX_SCORE_VALUE = 999999;
    /* access modifiers changed from: private */
    public Spinner gameModeSpinner;
    /* access modifiers changed from: private */
    public Random random;
    /* access modifiers changed from: private */
    public TextView rankingCheckText;
    /* access modifiers changed from: private */
    public RankingController rankingController;
    /* access modifiers changed from: private */
    public EditText scoreText;
    /* access modifiers changed from: private */
    public int scoreValue;

    private class RankingControllerObserver implements RequestControllerObserver {
        private RankingControllerObserver() {
        }

        /* synthetic */ RankingControllerObserver(GamePlayActivity gamePlayActivity, RankingControllerObserver rankingControllerObserver) {
            this();
        }

        public void requestControllerDidFail(RequestController requestController, Exception exception) {
            GamePlayActivity.this.hideProgressIndicator();
            if (!GamePlayActivity.this.isRequestCancellation(exception)) {
                GamePlayActivity.this.rankingCheckText.setText((int) R.string.ranking_check_failed);
            }
        }

        public void requestControllerDidReceiveResponse(RequestController aRequestController) {
            GamePlayActivity.this.rankingCheckText.setText(String.format(GamePlayActivity.this.getString(R.string.ranking_check_label), GamePlayActivity.this.rankingController.getRanking().getRank()));
            GamePlayActivity.this.hideProgressIndicator();
        }
    }

    /* access modifiers changed from: package-private */
    public void onSearchListsAvailable() {
        initSearchListChooser(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapter, View view, int position, long id) {
                GamePlayActivity.this.rankingController.setSearchList((SearchList) adapter.getItemAtPosition(position));
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.game_play);
        this.random = new Random(System.currentTimeMillis());
        this.rankingController = new RankingController(new RankingControllerObserver(this, null));
        if (!Session.getCurrentSession().isAuthenticated()) {
            requestSearchLists();
        } else {
            onSearchListsAvailable();
        }
        this.rankingCheckText = (TextView) findViewById(R.id.rank_info);
        this.scoreText = (EditText) findViewById(R.id.score_edit);
        this.scoreText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    GamePlayActivity.this.scoreValue = Integer.parseInt(s.toString());
                    if (GamePlayActivity.this.scoreValue > GamePlayActivity.MAX_SCORE_VALUE) {
                        GamePlayActivity.this.scoreValue = GamePlayActivity.MAX_SCORE_VALUE;
                        GamePlayActivity.this.scoreText.setText(new StringBuilder().append(GamePlayActivity.this.scoreValue).toString());
                    }
                } catch (NumberFormatException e) {
                    GamePlayActivity.this.scoreValue = 0;
                }
            }
        });
        ((Button) findViewById(R.id.rank_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Map<String, Object> context = new HashMap<>();
                context.put(Game.CONTEXT_KEY_MODE, Integer.valueOf(GamePlayActivity.this.gameModeSpinner.getSelectedItemPosition()));
                GamePlayActivity.this.rankingController.requestRankingForScoreResult(new Double((double) GamePlayActivity.this.scoreValue), context);
                GamePlayActivity.this.showProgressIndicator();
            }
        });
        ((Button) findViewById(R.id.random_score_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GamePlayActivity.this.scoreValue = GamePlayActivity.this.random.nextInt(GamePlayActivity.MAX_SCORE_VALUE);
                GamePlayActivity.this.scoreText.setText(new StringBuilder().append(GamePlayActivity.this.scoreValue).toString());
            }
        });
        ((Button) findViewById(R.id.game_over_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(GamePlayActivity.this, GameResultActivity.class);
                intent.putExtra(GamePlayActivity.KEY_RESULT, GamePlayActivity.this.scoreValue);
                intent.putExtra(GamePlayActivity.KEY_MODE, GamePlayActivity.this.gameModeSpinner.getSelectedItemPosition());
                GamePlayActivity.this.startActivity(intent);
                GamePlayActivity.this.finish();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        boolean z;
        super.onStart();
        if (ScoreloopManager.hasCurrentChallenge()) {
            Challenge challenge = ScoreloopManager.getCurrentChallenge();
            Integer mode = challenge.getMode();
            if (challenge.isAccepted()) {
                z = false;
            } else {
                z = true;
            }
            this.gameModeSpinner = getGameModeChooser(mode, z);
        } else {
            this.gameModeSpinner = getGameModeChooser(null, true);
        }
        this.scoreValue = this.random.nextInt(MAX_SCORE_VALUE);
        this.scoreText.setText(new StringBuilder().append(this.scoreValue).toString());
    }
}
