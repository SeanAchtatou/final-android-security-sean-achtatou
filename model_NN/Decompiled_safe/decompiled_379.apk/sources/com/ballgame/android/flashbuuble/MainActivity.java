package com.ballgame.android.flashbuuble;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import com.ballgame.android.flashbuuble.utils.ExecutableItem;

public class MainActivity extends BaseActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.menu);
        initMenu(new ExecutableItem[]{new ExecutableItem((int) R.string.menu_item_play, new View.OnClickListener() {
            public void onClick(View v) {
                ScoreloopManager.resetCurrentChallenge();
                MainActivity.this.startActivity(new Intent(MainActivity.this, Bubble.class));
            }
        }), new ExecutableItem((int) R.string.menu_item_highscores, HighscoresActivity.class), new ExecutableItem((int) R.string.menu_item_profile, ProfileActivity.class), new ExecutableItem((int) R.string.mExit, new View.OnClickListener() {
            public void onClick(View v) {
                MainActivity.this.exitConfirm();
            }
        })});
    }

    /* access modifiers changed from: private */
    public void exitConfirm() {
        new AlertDialog.Builder(this).setMessage("Do you want to exit now?").setTitle("Exit").setIcon((int) R.drawable.icon).setPositiveButton("More Game", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                MainActivity.this.showMore();
            }
        }).setNeutralButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                MainActivity.this.finish();
            }
        }).create().show();
    }

    /* access modifiers changed from: private */
    public void showMore() {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://market.android.com/search?q=pub:\"PLAY FISH\"")));
    }
}
