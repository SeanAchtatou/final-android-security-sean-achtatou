package com.ballgame.android.flashbuuble;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.ballgame.android.flashbuuble.BaseActivity;
import com.scoreloop.client.android.core.controller.ChallengeController;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.UserController;
import com.scoreloop.client.android.core.model.Challenge;

public class ChallengeConfirmActivity extends BaseActivity {
    /* access modifiers changed from: private */
    public Challenge challenge;
    private Button rejectButton;

    private class ChallengeUpdateObserver extends BaseActivity.ChallengeGenericObserver {
        private ChallengeUpdateObserver() {
            super();
        }

        /* synthetic */ ChallengeUpdateObserver(ChallengeConfirmActivity challengeConfirmActivity, ChallengeUpdateObserver challengeUpdateObserver) {
            this();
        }

        public void requestControllerDidReceiveResponse(RequestController aRequestController) {
            ChallengeConfirmActivity.this.hideProgressIndicator();
            if (ScoreloopManager.getCurrentChallenge().isAccepted()) {
                ChallengeConfirmActivity.this.startActivity(new Intent(ChallengeConfirmActivity.this, GamePlayActivity.class));
            }
            ChallengeConfirmActivity.this.finish();
        }
    }

    private class UserDetailObserver extends BaseActivity.UserGenericObserver {
        private UserDetailObserver() {
            super();
        }

        /* synthetic */ UserDetailObserver(ChallengeConfirmActivity challengeConfirmActivity, UserDetailObserver userDetailObserver) {
            this();
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            ChallengeConfirmActivity.this.hideProgressIndicator();
            ((TextView) ChallengeConfirmActivity.this.findViewById(R.id.opponent_info)).setText(String.format(ChallengeConfirmActivity.this.getString(R.string.challenge_opponent_format), Double.valueOf(((UserController) requestController).getUser().getDetail().getWinningProbability().doubleValue() * 100.0d), ((UserController) requestController).getUser().getDetail().getChallengesWon(), ((UserController) requestController).getUser().getDetail().getChallengesLost()));
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.challenge_confirm);
        ((Button) findViewById(R.id.accept_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ChallengeController challengeController = new ChallengeController(new ChallengeUpdateObserver(ChallengeConfirmActivity.this, null));
                challengeController.setChallenge(ChallengeConfirmActivity.this.challenge);
                challengeController.acceptChallenge();
                ChallengeConfirmActivity.this.showProgressIndicator();
            }
        });
        this.rejectButton = (Button) findViewById(R.id.reject_button);
        this.rejectButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ChallengeController challengeController = new ChallengeController(new ChallengeUpdateObserver(ChallengeConfirmActivity.this, null));
                challengeController.setChallenge(ChallengeConfirmActivity.this.challenge);
                challengeController.rejectChallenge();
                ChallengeConfirmActivity.this.showProgressIndicator();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        Object contenderSkill;
        super.onStart();
        this.challenge = ScoreloopManager.getCurrentChallenge();
        String string = getString(R.string.challenge_confirmation_format);
        Object[] objArr = new Object[6];
        objArr[0] = this.challenge.getContender().getLogin();
        objArr[1] = formatMoney(this.challenge.getStake());
        objArr[2] = DEFAULT_DATE_FORMAT.format(this.challenge.getCreatedAt());
        objArr[3] = isEmpty(this.challenge.getContestantSkill()) ? "---" : this.challenge.getContestantSkill();
        objArr[4] = this.challenge.getContender().getLogin();
        if (isEmpty(this.challenge.getContenderSkill())) {
            contenderSkill = "---";
        } else {
            contenderSkill = this.challenge.getContenderSkill();
        }
        objArr[5] = contenderSkill;
        ((TextView) findViewById(R.id.challenge_info)).setText(String.format(string, objArr));
        if (!this.challenge.isAssigned()) {
            this.rejectButton.setVisibility(8);
        }
        UserController userController = new UserController(new UserDetailObserver(this, null));
        userController.setUser(this.challenge.getContender());
        userController.requestUserDetail();
        showProgressIndicator();
    }
}
