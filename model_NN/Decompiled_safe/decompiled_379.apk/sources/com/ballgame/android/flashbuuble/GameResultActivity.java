package com.ballgame.android.flashbuuble;

import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.ballgame.android.flashbuuble.BaseActivity;
import com.scoreloop.client.android.core.controller.ChallengeController;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.controller.ScoreController;
import com.scoreloop.client.android.core.model.Challenge;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.Session;

public class GameResultActivity extends BaseActivity {
    /* access modifiers changed from: private */
    public LinearLayout challenge1Controls;
    /* access modifiers changed from: private */
    public LinearLayout challenge2Controls;
    /* access modifiers changed from: private */
    public LinearLayout normalControls;

    private class ScoreSubmitObserver implements RequestControllerObserver {
        private ScoreSubmitObserver() {
        }

        /* synthetic */ ScoreSubmitObserver(GameResultActivity gameResultActivity, ScoreSubmitObserver scoreSubmitObserver) {
            this();
        }

        public void requestControllerDidFail(RequestController requestController, Exception exception) {
            GameResultActivity.this.hideProgressIndicator();
            if (!GameResultActivity.this.isRequestCancellation(exception)) {
                ((TextView) GameResultActivity.this.findViewById(R.id.submitted_score)).setText((int) R.string.score_submit_failed);
                GameResultActivity.this.normalControls.setVisibility(0);
            }
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            GameResultActivity.this.hideProgressIndicator();
            ((TextView) GameResultActivity.this.findViewById(R.id.submitted_score)).setText(String.valueOf(GameResultActivity.this.getString(R.string.submitted_score_label)) + " " + ((ScoreController) requestController).getScore().getResult());
            GameResultActivity.this.normalControls.setVisibility(0);
        }
    }

    private class ChallengeSubmitObserver extends BaseActivity.ChallengeGenericObserver {
        private ChallengeSubmitObserver() {
            super();
        }

        /* synthetic */ ChallengeSubmitObserver(GameResultActivity gameResultActivity, ChallengeSubmitObserver challengeSubmitObserver) {
            this();
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            int myScore;
            int opponentScore;
            String opponentLogin;
            GameResultActivity.this.hideProgressIndicator();
            Challenge challenge = ((ChallengeController) requestController).getChallenge();
            if (challenge.isOpen() || challenge.isAssigned()) {
                ((TextView) GameResultActivity.this.findViewById(R.id.submitted_challenge_score)).setText(String.valueOf(GameResultActivity.this.getString(R.string.submitted_challenge_score_label)) + " " + challenge.getContenderScore().getResult());
                GameResultActivity.this.challenge1Controls.setVisibility(0);
            }
            if (challenge.isComplete()) {
                boolean iAmWinner = challenge.isWinner(Session.getCurrentSession().getUser());
                TextView wonLostText = (TextView) GameResultActivity.this.findViewById(R.id.won_lost);
                String string = GameResultActivity.this.getString(R.string.challenge_won_lost_format);
                Object[] objArr = new Object[1];
                objArr[0] = GameResultActivity.this.getString(iAmWinner ? R.string.won : R.string.lost);
                wonLostText.setText(String.format(string, objArr));
                TextView priceText = (TextView) GameResultActivity.this.findViewById(R.id.price);
                if (iAmWinner) {
                    priceText.setText(String.format(GameResultActivity.this.getString(R.string.challenge_stake_won_format), GameResultActivity.this.formatMoney(challenge.getStake()), GameResultActivity.this.formatMoney(challenge.getPrice())));
                } else {
                    priceText.setText(String.format(GameResultActivity.this.getString(R.string.challenge_stake_lost_format), GameResultActivity.this.formatMoney(challenge.getStake())));
                }
                TextView scoreResultsText = (TextView) GameResultActivity.this.findViewById(R.id.score_results);
                if (Session.getCurrentSession().getUser().equals(challenge.getContender())) {
                    myScore = challenge.getContenderScore().getResult().intValue();
                    opponentScore = challenge.getContestantScore().getResult().intValue();
                    opponentLogin = challenge.getContestant().getLogin();
                } else {
                    myScore = challenge.getContestantScore().getResult().intValue();
                    opponentScore = challenge.getContenderScore().getResult().intValue();
                    opponentLogin = challenge.getContender().getLogin();
                }
                scoreResultsText.setText(String.format(GameResultActivity.this.getString(R.string.challenge_score_result_format), Integer.valueOf(myScore), opponentLogin, Integer.valueOf(opponentScore)));
                GameResultActivity.this.challenge2Controls.setVisibility(0);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.game_result);
        this.normalControls = (LinearLayout) findViewById(R.id.normal_controls);
        this.challenge1Controls = (LinearLayout) findViewById(R.id.challenge1_controls);
        this.challenge2Controls = (LinearLayout) findViewById(R.id.challenge2_controls);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.normalControls.setVisibility(8);
        this.challenge1Controls.setVisibility(8);
        this.challenge2Controls.setVisibility(8);
        int scoreValue = getIntent().getIntExtra("GAME_RESULT", 0);
        int gameMode = getIntent().getIntExtra("GAME_MODE", 0);
        Score score = new Score(Double.valueOf((double) scoreValue), null, Session.getCurrentSession().getUser());
        score.setMode(Integer.valueOf(gameMode));
        if (ScoreloopManager.hasCurrentChallenge()) {
            Challenge challenge = ScoreloopManager.getCurrentChallenge();
            if (challenge.isCreated()) {
                challenge.setContenderScore(score);
            }
            if (challenge.isAccepted()) {
                challenge.setContestantScore(score);
            }
            ChallengeController challengeController = new ChallengeController(new ChallengeSubmitObserver(this, null));
            challengeController.setChallenge(challenge);
            challengeController.submitChallenge();
            showProgressIndicator();
            return;
        }
        new ScoreController(new ScoreSubmitObserver(this, null)).submitScore(score);
        showProgressIndicator();
    }
}
