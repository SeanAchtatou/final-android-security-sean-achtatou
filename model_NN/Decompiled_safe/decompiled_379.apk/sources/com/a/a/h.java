package com.a.a;

import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;

class h implements e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f13a;

    private h(l lVar) {
        this.f13a = lVar;
    }

    public void a(a aVar) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.q.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.a.a.l.a(com.a.a.l, com.a.a.a):com.a.a.a
      com.a.a.l.a(com.a.a.l, boolean):boolean
      com.a.a.q.a(java.lang.String, java.util.Map):java.net.URL
      com.a.a.q.a(com.a.a.q, boolean):void
      com.a.a.q.a(java.lang.Throwable, boolean):void
      com.a.a.q.a(boolean, boolean):void */
    public void a(a aVar, Object obj) {
        Date date = null;
        try {
            JSONObject jSONObject = (JSONObject) obj;
            Long valueOf = Long.valueOf(jSONObject.getLong("uid"));
            String string = jSONObject.getString("session_key");
            String string2 = jSONObject.getString("secret");
            Long valueOf2 = Long.valueOf(jSONObject.getLong("expires"));
            if (valueOf2 != null) {
                date = new Date(valueOf2.longValue() * 1000);
            }
            a unused = this.f13a.e = (a) null;
            this.f13a.f17a.a(this.f13a.c, valueOf, string, string2, date);
            this.f13a.f17a.b(this.f13a.c);
            this.f13a.a(true, true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.l.a(com.a.a.l, boolean):boolean
     arg types: [com.a.a.l, int]
     candidates:
      com.a.a.l.a(com.a.a.l, com.a.a.a):com.a.a.a
      com.a.a.q.a(java.lang.String, java.util.Map):java.net.URL
      com.a.a.q.a(com.a.a.q, boolean):void
      com.a.a.q.a(java.lang.Throwable, boolean):void
      com.a.a.q.a(boolean, boolean):void
      com.a.a.l.a(com.a.a.l, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.q.a(java.lang.Throwable, boolean):void
     arg types: [java.lang.Throwable, int]
     candidates:
      com.a.a.l.a(com.a.a.l, com.a.a.a):com.a.a.a
      com.a.a.l.a(com.a.a.l, boolean):boolean
      com.a.a.q.a(java.lang.String, java.util.Map):java.net.URL
      com.a.a.q.a(com.a.a.q, boolean):void
      com.a.a.q.a(boolean, boolean):void
      com.a.a.q.a(java.lang.Throwable, boolean):void */
    public void a(a aVar, Throwable th) {
        a unused = this.f13a.e = (a) null;
        if (!(th instanceof m)) {
            this.f13a.a(th, true);
        } else if (this.f13a.g && ((m) th).a() == 100) {
            boolean unused2 = this.f13a.g = false;
            this.f13a.g();
        }
    }

    public void b(a aVar) {
    }
}
