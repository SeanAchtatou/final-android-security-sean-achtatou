package com.scoreloop.client.android.core.model;

public class Range {

    /* renamed from: a  reason: collision with root package name */
    private int f81a;
    private int b;

    public Range(int i, int i2) {
        if (i < 0 || i2 < 0) {
            throw new IllegalArgumentException("length & location must be >= 0");
        }
        this.b = i;
        this.f81a = i2;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.b;
    }

    public void a(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("length must be >= 0");
        }
        this.f81a = i;
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return this.b + this.f81a;
    }

    public void b(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("location must be >= 0");
        }
        this.b = i;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Range)) {
            return super.equals(obj);
        }
        Range range = (Range) obj;
        return a() == range.a() && b() == range.b();
    }

    public int getLength() {
        return this.f81a;
    }

    public int getLocation() {
        return this.b;
    }

    public int hashCode() {
        return new Integer(a()).hashCode() ^ new Integer(b()).hashCode();
    }

    public String toString() {
        return " [" + a() + ", " + b() + "] ";
    }
}
