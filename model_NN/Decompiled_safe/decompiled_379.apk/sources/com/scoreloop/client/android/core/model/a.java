package com.scoreloop.client.android.core.model;

import org.json.JSONObject;

class a {

    /* renamed from: a  reason: collision with root package name */
    private final String f92a;
    private final int b;
    private final int c;

    public a(JSONObject jSONObject) {
        this.f92a = jSONObject.getString("decoration");
        this.b = jSONObject.getInt("threshold");
        this.c = jSONObject.getInt("value");
    }
}
