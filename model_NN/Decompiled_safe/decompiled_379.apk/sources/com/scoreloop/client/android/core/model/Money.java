package com.scoreloop.client.android.core.model;

import java.math.BigDecimal;
import org.json.JSONException;
import org.json.JSONObject;

public class Money implements Cloneable, Comparable {
    private static String c;
    private static String d;
    private static String e;
    private static String f;

    /* renamed from: a  reason: collision with root package name */
    private BigDecimal f78a;
    private String b;

    public Money(String str, BigDecimal bigDecimal) {
        this.b = str;
        this.f78a = bigDecimal;
    }

    public Money(BigDecimal bigDecimal) {
        this.f78a = bigDecimal;
        this.b = a();
    }

    public Money(JSONObject jSONObject) {
        a(jSONObject);
    }

    public static String a() {
        return c != null ? c : "SLD";
    }

    static void a(String str) {
        c = str;
    }

    public static void b(String str) {
        d = str;
    }

    public static void c(String str) {
        e = str;
    }

    public static void d(String str) {
        f = str;
    }

    public void a(JSONObject jSONObject) {
        this.b = jSONObject.getString("currency");
        try {
            this.f78a = new BigDecimal(jSONObject.getString("amount"));
            this.f78a.divide(new BigDecimal(100));
        } catch (NumberFormatException e2) {
            throw new JSONException("Invalid format of money amount");
        }
    }

    /* renamed from: b */
    public Money clone() {
        return new Money(this.b, this.f78a);
    }

    public JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("amount", this.f78a);
        jSONObject.put("currency", this.b);
        return jSONObject;
    }

    public int compareTo(Money money) {
        if (money == null) {
            throw new IllegalArgumentException();
        } else if (d().equalsIgnoreCase(money.d())) {
            return getAmount().compareTo(money.getAmount());
        } else {
            throw new IllegalArgumentException();
        }
    }

    public String d() {
        return this.b;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Money)) {
            return super.equals(obj);
        }
        Money money = (Money) obj;
        return d().equalsIgnoreCase(money.d()) && getAmount().equals(money.getAmount());
    }

    public BigDecimal getAmount() {
        return this.f78a;
    }

    public boolean hasAmount() {
        return getAmount().compareTo(new BigDecimal(0)) > 0;
    }

    public int hashCode() {
        return d().hashCode() ^ getAmount().hashCode();
    }

    public String toString() {
        return this.b.equalsIgnoreCase("SLD") ? this.f78a.toString() + " " + "Coins" : this.f78a.toString() + " " + this.b;
    }
}
