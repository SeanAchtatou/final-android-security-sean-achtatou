package com.scoreloop.client.android.core.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public abstract class JSONUtils {
    private static Object a(Object obj) {
        if (JSONObject.NULL.equals(obj)) {
            return null;
        }
        return obj instanceof JSONArray ? a((JSONArray) obj) : obj instanceof JSONObject ? b((JSONObject) obj) : obj;
    }

    private static List a(JSONArray jSONArray) {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(a(jSONArray.opt(i)));
        }
        return arrayList;
    }

    public static Map a(JSONObject jSONObject) {
        return b(jSONObject);
    }

    public static JSONObject a(Map map) {
        return new JSONObject(map);
    }

    public static boolean a(JSONObject jSONObject, String str) {
        if (!jSONObject.has(str)) {
            return false;
        }
        Object opt = jSONObject.opt(str);
        if (opt == null) {
            return false;
        }
        if (JSONObject.NULL.equals(opt)) {
            return false;
        }
        return !(opt instanceof String) || !"".equalsIgnoreCase((String) opt);
    }

    private static Map b(JSONObject jSONObject) {
        HashMap hashMap = new HashMap();
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            hashMap.put(next, a(jSONObject.opt(next)));
        }
        return hashMap;
    }
}
