package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Ranking;

/* renamed from: com.scoreloop.client.android.core.controller.k  reason: case insensitive filesystem */
class C0012k implements RequestControllerObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ScoresController f59a;

    private C0012k(ScoresController scoresController) {
        this.f59a = scoresController;
    }

    public void requestControllerDidFail(RequestController requestController, Exception exc) {
        this.f59a.f44a.requestControllerDidFail(this.f59a, exc);
    }

    public void requestControllerDidReceiveResponse(RequestController requestController) {
        Ranking ranking = ((RankingController) requestController).getRanking();
        if (this.f59a.i != null && this.f59a.i.getRank() == null) {
            this.f59a.i.a(ranking.getRank());
        }
        this.f59a.a(ranking.getRank());
    }
}
