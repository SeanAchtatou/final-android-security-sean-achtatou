package com.scoreloop.client.android.core.ui;

import com.a.a.d;
import com.a.a.q;

final class g extends d {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FacebookAuthViewController f122a;

    private g(FacebookAuthViewController facebookAuthViewController) {
        this.f122a = facebookAuthViewController;
    }

    /* synthetic */ g(FacebookAuthViewController facebookAuthViewController, b bVar) {
        this(facebookAuthViewController);
    }

    public void a(q qVar, Throwable th) {
        this.f122a.a(false);
        this.f122a.a().didFail(th);
    }

    public void b(q qVar) {
        this.f122a.a(false);
        this.f122a.a().userDidCancel();
    }
}
