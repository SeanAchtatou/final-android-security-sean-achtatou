package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import org.json.JSONObject;

class L extends K {
    public L(RequestCompletionCallback requestCompletionCallback, User user, User user2) {
        super(requestCompletionCallback, null, user, user2);
    }

    public JSONObject a() {
        return null;
    }

    public RequestMethod b() {
        return RequestMethod.DELETE;
    }

    public String c() {
        return String.format("/service/users/%s/buddies/%s", this.c.getIdentifier(), this.b.getIdentifier());
    }
}
