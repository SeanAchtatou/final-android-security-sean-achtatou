package com.scoreloop.client.android.core.controller;

enum J {
    CREATE(21),
    RESET(22),
    UPDATE(23),
    VERIFY(20);
    
    private final int e;

    private J(int i) {
        this.e = i;
    }

    public static J[] a() {
        return (J[]) f.clone();
    }
}
