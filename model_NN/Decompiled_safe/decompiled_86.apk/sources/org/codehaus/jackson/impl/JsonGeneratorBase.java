package org.codehaus.jackson.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.ObjectCodec;

public abstract class JsonGeneratorBase extends JsonGenerator {
    private static /* synthetic */ int[] $SWITCH_TABLE$org$codehaus$jackson$JsonParser$NumberType;
    private static /* synthetic */ int[] $SWITCH_TABLE$org$codehaus$jackson$JsonToken;
    protected boolean _cfgNumbersAsStrings;
    protected boolean _closed;
    protected int _features;
    protected ObjectCodec _objectCodec;
    protected JsonWriteContext _writeContext = JsonWriteContext.createRootContext();

    /* access modifiers changed from: protected */
    public abstract void _releaseBuffers();

    /* access modifiers changed from: protected */
    public abstract void _verifyValueWrite(String str) throws IOException, JsonGenerationException;

    /* access modifiers changed from: protected */
    public abstract void _writeEndArray() throws IOException, JsonGenerationException;

    /* access modifiers changed from: protected */
    public abstract void _writeEndObject() throws IOException, JsonGenerationException;

    /* access modifiers changed from: protected */
    public abstract void _writeFieldName(String str, boolean z) throws IOException, JsonGenerationException;

    /* access modifiers changed from: protected */
    public abstract void _writeStartArray() throws IOException, JsonGenerationException;

    /* access modifiers changed from: protected */
    public abstract void _writeStartObject() throws IOException, JsonGenerationException;

    public abstract void flush() throws IOException;

    public abstract void writeBoolean(boolean z) throws IOException, JsonGenerationException;

    public abstract void writeNull() throws IOException, JsonGenerationException;

    public abstract void writeNumber(double d) throws IOException, JsonGenerationException;

    public abstract void writeNumber(float f) throws IOException, JsonGenerationException;

    public abstract void writeNumber(int i) throws IOException, JsonGenerationException;

    public abstract void writeNumber(long j) throws IOException, JsonGenerationException;

    public abstract void writeNumber(BigDecimal bigDecimal) throws IOException, JsonGenerationException;

    static /* synthetic */ int[] $SWITCH_TABLE$org$codehaus$jackson$JsonParser$NumberType() {
        int[] iArr = $SWITCH_TABLE$org$codehaus$jackson$JsonParser$NumberType;
        if (iArr == null) {
            iArr = new int[JsonParser.NumberType.values().length];
            try {
                iArr[JsonParser.NumberType.BIG_DECIMAL.ordinal()] = 6;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[JsonParser.NumberType.BIG_INTEGER.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[JsonParser.NumberType.DOUBLE.ordinal()] = 5;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[JsonParser.NumberType.FLOAT.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[JsonParser.NumberType.INT.ordinal()] = 1;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[JsonParser.NumberType.LONG.ordinal()] = 2;
            } catch (NoSuchFieldError e6) {
            }
            $SWITCH_TABLE$org$codehaus$jackson$JsonParser$NumberType = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] $SWITCH_TABLE$org$codehaus$jackson$JsonToken() {
        int[] iArr = $SWITCH_TABLE$org$codehaus$jackson$JsonToken;
        if (iArr == null) {
            iArr = new int[JsonToken.values().length];
            try {
                iArr[JsonToken.END_ARRAY.ordinal()] = 5;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[JsonToken.END_OBJECT.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[JsonToken.FIELD_NAME.ordinal()] = 6;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[JsonToken.NOT_AVAILABLE.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[JsonToken.START_ARRAY.ordinal()] = 4;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[JsonToken.START_OBJECT.ordinal()] = 2;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[JsonToken.VALUE_EMBEDDED_OBJECT.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[JsonToken.VALUE_FALSE.ordinal()] = 12;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[JsonToken.VALUE_NULL.ordinal()] = 13;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[JsonToken.VALUE_NUMBER_FLOAT.ordinal()] = 10;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[JsonToken.VALUE_NUMBER_INT.ordinal()] = 9;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[JsonToken.VALUE_STRING.ordinal()] = 8;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[JsonToken.VALUE_TRUE.ordinal()] = 11;
            } catch (NoSuchFieldError e13) {
            }
            $SWITCH_TABLE$org$codehaus$jackson$JsonToken = iArr;
        }
        return iArr;
    }

    protected JsonGeneratorBase(int features, ObjectCodec codec) {
        this._features = features;
        this._objectCodec = codec;
        this._cfgNumbersAsStrings = isEnabled(JsonGenerator.Feature.WRITE_NUMBERS_AS_STRINGS);
    }

    public JsonGenerator enable(JsonGenerator.Feature f) {
        this._features |= f.getMask();
        if (f == JsonGenerator.Feature.WRITE_NUMBERS_AS_STRINGS) {
            this._cfgNumbersAsStrings = true;
        }
        return this;
    }

    public JsonGenerator disable(JsonGenerator.Feature f) {
        this._features &= f.getMask() ^ -1;
        if (f == JsonGenerator.Feature.WRITE_NUMBERS_AS_STRINGS) {
            this._cfgNumbersAsStrings = false;
        }
        return this;
    }

    public final boolean isEnabled(JsonGenerator.Feature f) {
        return (this._features & f.getMask()) != 0;
    }

    public final JsonGenerator useDefaultPrettyPrinter() {
        return setPrettyPrinter(new DefaultPrettyPrinter());
    }

    public final JsonGenerator setCodec(ObjectCodec oc) {
        this._objectCodec = oc;
        return this;
    }

    public final ObjectCodec getCodec() {
        return this._objectCodec;
    }

    public final JsonWriteContext getOutputContext() {
        return this._writeContext;
    }

    public final void writeStartArray() throws IOException, JsonGenerationException {
        _verifyValueWrite("start an array");
        this._writeContext = this._writeContext.createChildArrayContext();
        if (this._cfgPrettyPrinter != null) {
            this._cfgPrettyPrinter.writeStartArray(this);
        } else {
            _writeStartArray();
        }
    }

    public final void writeEndArray() throws IOException, JsonGenerationException {
        if (!this._writeContext.inArray()) {
            _reportError("Current context not an ARRAY but " + this._writeContext.getTypeDesc());
        }
        if (this._cfgPrettyPrinter != null) {
            this._cfgPrettyPrinter.writeEndArray(this, this._writeContext.getEntryCount());
        } else {
            _writeEndArray();
        }
        this._writeContext = this._writeContext.getParent();
    }

    public final void writeStartObject() throws IOException, JsonGenerationException {
        _verifyValueWrite("start an object");
        this._writeContext = this._writeContext.createChildObjectContext();
        if (this._cfgPrettyPrinter != null) {
            this._cfgPrettyPrinter.writeStartObject(this);
        } else {
            _writeStartObject();
        }
    }

    public final void writeEndObject() throws IOException, JsonGenerationException {
        if (!this._writeContext.inObject()) {
            _reportError("Current context not an object but " + this._writeContext.getTypeDesc());
        }
        this._writeContext = this._writeContext.getParent();
        if (this._cfgPrettyPrinter != null) {
            this._cfgPrettyPrinter.writeEndObject(this, this._writeContext.getEntryCount());
        } else {
            _writeEndObject();
        }
    }

    public final void writeFieldName(String name) throws IOException, JsonGenerationException {
        int status = this._writeContext.writeFieldName(name);
        if (status == 4) {
            _reportError("Can not write a field name, expecting a value");
        }
        _writeFieldName(name, status == 1);
    }

    public void writeObject(Object value) throws IOException, JsonProcessingException {
        if (value == null) {
            writeNull();
        } else if (this._objectCodec != null) {
            this._objectCodec.writeValue(this, value);
        } else {
            _writeSimpleObject(value);
        }
    }

    public void writeTree(JsonNode rootNode) throws IOException, JsonProcessingException {
        if (rootNode == null) {
            writeNull();
        } else if (this._objectCodec == null) {
            throw new IllegalStateException("No ObjectCodec defined for the generator, can not serialize JsonNode-based trees");
        } else {
            this._objectCodec.writeTree(this, rootNode);
        }
    }

    public void close() throws IOException {
        this._closed = true;
    }

    public boolean isClosed() {
        return this._closed;
    }

    public final void copyCurrentEvent(JsonParser jp) throws IOException, JsonProcessingException {
        switch ($SWITCH_TABLE$org$codehaus$jackson$JsonToken()[jp.getCurrentToken().ordinal()]) {
            case 2:
                writeStartObject();
                return;
            case 3:
                writeEndObject();
                return;
            case 4:
                writeStartArray();
                return;
            case JsonWriteContext.STATUS_EXPECT_NAME /*5*/:
                writeEndArray();
                return;
            case 6:
                writeFieldName(jp.getCurrentName());
                return;
            case 7:
                writeObject(jp.getEmbeddedObject());
                return;
            case 8:
                writeString(jp.getTextCharacters(), jp.getTextOffset(), jp.getTextLength());
                return;
            case 9:
                switch ($SWITCH_TABLE$org$codehaus$jackson$JsonParser$NumberType()[jp.getNumberType().ordinal()]) {
                    case 1:
                        writeNumber(jp.getIntValue());
                        return;
                    case 2:
                    default:
                        writeNumber(jp.getLongValue());
                        return;
                    case 3:
                        writeNumber(jp.getBigIntegerValue());
                        return;
                }
            case 10:
                switch ($SWITCH_TABLE$org$codehaus$jackson$JsonParser$NumberType()[jp.getNumberType().ordinal()]) {
                    case 4:
                        writeNumber(jp.getFloatValue());
                        return;
                    case JsonWriteContext.STATUS_EXPECT_NAME /*5*/:
                    default:
                        writeNumber(jp.getDoubleValue());
                        return;
                    case 6:
                        writeNumber(jp.getDecimalValue());
                        return;
                }
            case 11:
                writeBoolean(true);
                return;
            case 12:
                writeBoolean(false);
                return;
            case 13:
                writeNull();
                return;
            default:
                _cantHappen();
                return;
        }
    }

    public final void copyCurrentStructure(JsonParser jp) throws IOException, JsonProcessingException {
        JsonToken t = jp.getCurrentToken();
        if (t == JsonToken.FIELD_NAME) {
            writeFieldName(jp.getCurrentName());
            t = jp.nextToken();
        }
        switch ($SWITCH_TABLE$org$codehaus$jackson$JsonToken()[t.ordinal()]) {
            case 2:
                writeStartObject();
                while (jp.nextToken() != JsonToken.END_OBJECT) {
                    copyCurrentStructure(jp);
                }
                writeEndObject();
                return;
            case 3:
            default:
                copyCurrentEvent(jp);
                return;
            case 4:
                writeStartArray();
                while (jp.nextToken() != JsonToken.END_ARRAY) {
                    copyCurrentStructure(jp);
                }
                writeEndArray();
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void _reportError(String msg) throws JsonGenerationException {
        throw new JsonGenerationException(msg);
    }

    /* access modifiers changed from: protected */
    public void _cantHappen() {
        throw new RuntimeException("Internal error: should never end up through this code path");
    }

    /* access modifiers changed from: protected */
    public void _writeSimpleObject(Object value) throws IOException, JsonGenerationException {
        if (value == null) {
            writeNull();
        } else if (value instanceof String) {
            writeString((String) value);
        } else {
            if (value instanceof Number) {
                Number n = (Number) value;
                if (n instanceof Integer) {
                    writeNumber(n.intValue());
                    return;
                } else if (n instanceof Long) {
                    writeNumber(n.longValue());
                    return;
                } else if (n instanceof Double) {
                    writeNumber(n.doubleValue());
                    return;
                } else if (n instanceof Float) {
                    writeNumber(n.floatValue());
                    return;
                } else if (n instanceof Short) {
                    writeNumber((int) n.shortValue());
                    return;
                } else if (n instanceof Byte) {
                    writeNumber((int) n.byteValue());
                    return;
                } else if (n instanceof BigInteger) {
                    writeNumber((BigInteger) n);
                    return;
                } else if (n instanceof BigDecimal) {
                    writeNumber((BigDecimal) n);
                    return;
                } else if (n instanceof AtomicInteger) {
                    writeNumber(((AtomicInteger) n).get());
                    return;
                } else if (n instanceof AtomicLong) {
                    writeNumber(((AtomicLong) n).get());
                    return;
                }
            } else if (value instanceof byte[]) {
                writeBinary((byte[]) value);
                return;
            } else if (value instanceof Boolean) {
                writeBoolean(((Boolean) value).booleanValue());
                return;
            } else if (value instanceof AtomicBoolean) {
                writeBoolean(((AtomicBoolean) value).get());
                return;
            }
            throw new IllegalStateException("No ObjectCodec defined for the generator, can only serialize simple wrapper types (type passed " + value.getClass().getName() + ")");
        }
    }
}
