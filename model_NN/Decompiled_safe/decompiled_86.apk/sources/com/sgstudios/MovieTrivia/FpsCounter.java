package com.sgstudios.MovieTrivia;

import android.os.SystemClock;

public class FpsCounter {
    static int mFPS;
    static int mFramecount;
    static long mLastTime;

    public static int GetFps() {
        long time = SystemClock.uptimeMillis();
        if (mLastTime == 0) {
            mLastTime = time;
            return -1;
        } else if (time - mLastTime < 1000) {
            mFramecount++;
            return -1;
        } else if (time - mLastTime < 1000) {
            return 0;
        } else {
            mLastTime = time;
            int res = mFramecount + 1;
            mFramecount = res;
            mFramecount = 0;
            return res;
        }
    }
}
