package com.sgstudios.MovieTrivia;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.openfeint.api.OpenFeint;
import com.openfeint.api.OpenFeintDelegate;
import com.openfeint.api.OpenFeintSettings;
import com.openfeint.api.resource.CurrentUser;
import com.openfeint.api.resource.Leaderboard;
import com.openfeint.api.resource.Score;
import com.openfeint.api.resource.User;
import java.util.HashMap;
import java.util.Map;

public class levelSelect extends Activity {
    static int[] ITEMS = {R.drawable.startgame, R.drawable.leaderboard, R.drawable.more};
    static int[] mScores;
    final boolean FREEVERSION = true;
    GameOptionsDialog mGameOptionsDlg;
    S3HighScore mHighScore;
    Animation mItemAnim;
    private AdapterView.OnItemClickListener mMessageClickedHandler = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView parent, View v, int position, long id) {
            levelSelect.this.m_playerSelect.Play();
            levelSelect.this.mSelectedItem = position;
            levelSelect.this.madapt.notifyDataSetChanged();
        }
    };
    S3Openfeint mOFeint;
    long[] mRank = new long[3];
    int mSelectedItem = -1;
    PowerManager.WakeLock mWakeLock;
    S3ScrollingView m_View;
    Player m_playerMusic;
    Player m_playerSelect;
    EfficientAdapter madapt;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        this.m_View = new S3ScrollingView(this);
        setContentView(this.m_View);
        this.m_View.setSelector((int) R.drawable.selector);
        this.madapt = new EfficientAdapter(this);
        this.m_View.setAdapter((ListAdapter) this.madapt);
        this.m_View.mCustomDraw = true;
        this.m_View.TouchEnabled(true);
        this.m_View.ScrollEnabled(true);
        this.m_View.setCacheColorHint(0);
        this.m_View.setOnItemClickListener(this.mMessageClickedHandler);
        this.mWakeLock = ((PowerManager) getSystemService("power")).newWakeLock(10, "tag");
        this.mWakeLock.acquire();
        this.mItemAnim = AnimationUtils.loadAnimation(getBaseContext(), R.anim.iconanimation);
        this.m_playerSelect = new Player(this, R.raw.beepfalling);
        this.m_playerMusic = new Player(this, R.raw.brunner);
        this.m_playerMusic.mMediaPlayer.setVolume(0.6f, 0.6f);
        this.m_playerMusic.InfiniteLoop();
        this.m_playerMusic.Play();
        this.mHighScore = new S3HighScore(this);
        mScores = new int[ITEMS.length];
        this.mGameOptionsDlg = new GameOptionsDialog(this);
        this.mGameOptionsDlg.requestWindowFeature(1);
        Map<String, Object> options = new HashMap<>();
        options.put(OpenFeintSettings.SettingCloudStorageCompressionStrategy, OpenFeintSettings.CloudStorageCompressionStrategyDefault);
        OpenFeint.initialize(this, new OpenFeintSettings("MovieTrivia", "ea7Uw9WIKGSKfjJpiKfqA", "1HQFFfEadPvcrjuRM6Xi7IUuD4IPrB2iBwJGWPcwyg0", "261293", options), new OpenFeintDelegate() {
            public void userLoggedIn(CurrentUser user) {
                levelSelect.this.UpdateOnlineRanking();
            }
        });
        this.mOFeint = new S3Openfeint(getBaseContext());
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        FreeVersionManager.Reset();
        this.m_playerSelect.Release();
        this.m_playerMusic.Release();
        this.mWakeLock.release();
    }

    public void onResume() {
        super.onResume();
        this.m_playerMusic.Silent(false);
        this.m_playerMusic.Stop();
        this.m_playerMusic.RePlay();
        this.mWakeLock.acquire();
        RefreshHighScores();
        UpdateOnlineRanking();
        this.madapt.notifyDataSetChanged();
    }

    public void onPause() {
        super.onPause();
        this.m_playerMusic.Silent(true);
        this.mWakeLock.release();
    }

    public void onBackPressed() {
        FreeVersionManager.Reset();
        finish();
    }

    public void RefreshHighScores() {
        for (int i = 0; i < mScores.length; i++) {
            this.mHighScore.GetScore(i);
            mScores[i] = this.mHighScore.mScore;
        }
    }

    private void ShowLevelLocked() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Level LOCKED");
        builder.setMessage("This mode will be available only on our upcoming Geotrivia PRO!!!").setCancelable(false).setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        builder.create().show();
    }

    /* access modifiers changed from: private */
    public void LaunchGame(int level, boolean multiplayer, boolean multiansw) {
        if (level >= 0 && level < ITEMS.length) {
            Intent mainIntent = new Intent(this, MovieTrivia.class);
            Bundle bundle = new Bundle();
            bundle.putInt("gametype", level);
            boolean finallvl = false;
            if (level == ITEMS.length - 1) {
                finallvl = true;
            }
            bundle.putBoolean("final", finallvl);
            bundle.putBoolean("multiplayer", multiplayer);
            bundle.putBoolean("multianswer", multiansw);
            mainIntent.putExtras(bundle);
            this.m_View.ReleaseCustomDraw();
            this.mSelectedItem = -1;
            startActivityForResult(mainIntent, 0);
            this.m_playerMusic.Stop();
            this.m_View.mStopped = true;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int a, int b, Intent it) {
        if (b == 666) {
            finish();
            return;
        }
        this.m_View.mStopped = false;
        this.m_playerMusic.RePlay();
    }

    private class EfficientAdapter extends BaseAdapter {
        ViewHolder holder;
        Animation mHyperspaceJump;
        private LayoutInflater mInflater;
        Bitmap m_bmpfalse;
        Bitmap m_bmpquest;
        Bitmap m_bmptrue;

        public EfficientAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
            this.m_bmpquest = BitmapFactory.decodeResource(levelSelect.this.getResources(), R.drawable.iconquestion);
            this.m_bmpfalse = BitmapFactory.decodeResource(levelSelect.this.getResources(), R.drawable.iconfalse);
            this.m_bmptrue = BitmapFactory.decodeResource(levelSelect.this.getResources(), R.drawable.icontrue);
            this.mHyperspaceJump = AnimationUtils.loadAnimation(levelSelect.this.getBaseContext(), R.anim.iconanimation);
        }

        public int getCount() {
            return levelSelect.ITEMS.length;
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = this.mInflater.inflate((int) R.layout.itemlevel, (ViewGroup) null);
                this.holder = new ViewHolder();
                this.holder.score = (TextView) convertView.findViewById(R.id.Score);
                this.holder.icon = (ImageView) convertView.findViewById(R.id.image);
                this.holder.rank = (TextView) convertView.findViewById(R.id.Rank);
            } else {
                this.holder = (ViewHolder) convertView.getTag();
            }
            this.holder.score.setTextColor(-1);
            this.holder.rank.setTextColor(-1);
            if (position == 0) {
                this.holder.score.setText("High Score: " + Integer.toString(levelSelect.mScores[position]));
                if (levelSelect.this.mRank[position] == 0) {
                    this.holder.rank.setText("World Rank: ? ");
                } else {
                    this.holder.rank.setText("World Rank: " + levelSelect.this.mRank[position] + " ");
                }
            } else {
                this.holder.score.setText("");
                this.holder.rank.setText("");
            }
            this.holder.icon.setImageResource(levelSelect.ITEMS[position]);
            if (position == levelSelect.this.mSelectedItem && levelSelect.this.mSelectedItem != -1) {
                levelSelect.this.mItemAnim.setAnimationListener(new Animation.AnimationListener() {
                    public void onAnimationEnd(Animation animation) {
                        if (levelSelect.this.mSelectedItem == 2) {
                            levelSelect.this.m_View.setSelection(0);
                            Intent intent = new Intent("android.intent.action.VIEW");
                            intent.setData(Uri.parse("market://search?q=pub:JumpingSheeps"));
                            levelSelect.this.startActivity(intent);
                            levelSelect.this.mSelectedItem = -1;
                        } else if (levelSelect.this.mSelectedItem == 1) {
                            levelSelect.this.mOFeint.ShowLeaderboard();
                            levelSelect.this.mSelectedItem = -1;
                        } else {
                            levelSelect.this.mGameOptionsDlg.mLevel = levelSelect.this.mSelectedItem;
                            levelSelect.this.mSelectedItem = -1;
                            levelSelect.this.mGameOptionsDlg.show();
                        }
                    }

                    public void onAnimationStart(Animation animation) {
                    }

                    public void onAnimationRepeat(Animation animation) {
                    }
                });
                this.holder.icon.startAnimation(levelSelect.this.mItemAnim);
            }
            convertView.setTag(this.holder);
            return convertView;
        }

        class ViewHolder {
            ImageView icon;
            TextView rank;
            TextView score;

            ViewHolder() {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void UpdateOnlineRanking() {
        Leaderboard lb1 = new Leaderboard(S3HighScore.FeintLeaderboards[0]);
        User me = OpenFeint.getCurrentUser();
        if (me != null) {
            lb1.getUserScore(me, new Leaderboard.GetUserScoreCB() {
                public void onSuccess(Score score) {
                    if (score != null) {
                        levelSelect.this.mRank[0] = (long) score.rank;
                    }
                    levelSelect.this.madapt.notifyDataSetChanged();
                }
            });
        }
    }

    /* access modifiers changed from: package-private */
    public void ShowFreeVersionAllert() {
        new FreeVersionManager(this).ShowAllert(false);
    }

    public class GameOptionsDialog extends Dialog implements View.OnClickListener {
        int mLevel = 0;

        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView((int) R.layout.playoptions_dialog);
            Typeface createFromAsset = Typeface.createFromAsset(levelSelect.this.getAssets(), "fonts/Clockopia.ttf");
            ((ImageButton) findViewById(R.id.triviamodebutton)).setOnClickListener(this);
            ((ImageButton) findViewById(R.id.booleanmodebutton)).setOnClickListener(this);
        }

        public GameOptionsDialog(Context c) {
            super(c);
        }

        /* access modifiers changed from: protected */
        public void onStart() {
        }

        public void onClick(View view) {
            levelSelect.this.m_playerSelect.Play();
            switch (view.getId()) {
                case R.id.triviamodebutton:
                    levelSelect.this.LaunchGame(this.mLevel, false, true);
                    dismiss();
                    return;
                case R.id.booleanmodebutton:
                    levelSelect.this.LaunchGame(this.mLevel, true, true);
                    dismiss();
                    return;
                default:
                    return;
            }
        }

        public void onBackPressed() {
            dismiss();
        }
    }
}
