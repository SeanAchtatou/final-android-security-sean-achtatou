package com.sgstudios.MovieTrivia;

import com.sgstudios.MovieTrivia.MovieFact;
import java.util.Random;

class Query {
    static Random m_random;
    Query[] Childs;
    public int CorrectAnsw;
    public int answer = -1;
    public boolean isTrue;
    public int mBonus = 0;
    public String mFake;
    public String mHint;
    public int mImage;
    public String mString;
    public QUERYTYPE mType;
    public int player;

    public enum QUERYTYPE {
        PARENT,
        CHILD,
        FAKECHILD
    }

    private void Prepare() {
        if (m_random == null) {
            m_random = new Random();
        }
    }

    Query(MovieFact.MOVIETYPE gametype) {
        Prepare();
        LoadMulti(gametype);
    }

    /* access modifiers changed from: package-private */
    public void LoadMulti(MovieFact.MOVIETYPE gametype) {
        Load(gametype);
        int valid = m_random.nextInt(3);
        if (valid > 2) {
            valid--;
        }
        this.Childs = new Query[3];
        int i = 0;
        while (i < 3) {
            this.Childs[i] = new Query(gametype, QUERYTYPE.CHILD);
            if (this.mFake != null && m_random.nextInt(3) == 2) {
                this.Childs[i].mString = this.mFake;
            }
            if (this.Childs[i].mString == this.mString) {
                i--;
            } else {
                int k = 0;
                while (true) {
                    if (k >= i) {
                        break;
                    } else if (this.Childs[i].mString == this.Childs[k].mString) {
                        i--;
                        break;
                    } else {
                        k++;
                    }
                }
            }
            i++;
        }
        this.Childs[valid].mString = this.mString;
        this.Childs[valid].isTrue = true;
    }

    Query(MovieFact.MOVIETYPE gametype, QUERYTYPE type) {
        Load(gametype);
        this.isTrue = false;
    }

    private void Load(MovieFact.MOVIETYPE gametype) {
        MovieFact mv = new MovieFact(gametype);
        this.mImage = mv.mImgId;
        this.mString = mv.mDescription;
        this.mHint = mv.mHint;
        this.mFake = mv.mFake;
        this.isTrue = true;
    }
}
