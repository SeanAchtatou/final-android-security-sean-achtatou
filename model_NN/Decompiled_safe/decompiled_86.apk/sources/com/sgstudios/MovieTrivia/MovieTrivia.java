package com.sgstudios.MovieTrivia;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.PowerManager;
import android.os.SystemClock;
import android.os.Vibrator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mobfox.sdk.MobFoxView;
import com.sgstudios.MovieTrivia.MovieFact;
import com.sgstudios.MovieTrivia.S3ScoreSprite;
import java.util.Random;
import org.codehaus.jackson.util.BufferRecycler;

public class MovieTrivia extends Activity {
    static Query[] ITEMS;
    static int gamecounter = 0;
    final int CDTIME = 20000;
    final int CORRECTPOINTS = 100;
    final int CORRECTPOINTSMULTIANSW = 250;
    final int ERRORPOINTS = -100;
    int[] EU_SCOREAVATRS = {R.drawable.final_monkey, R.drawable.final_keepout, R.drawable.final_cinemaenthusiast, R.drawable.final_cinemaguy, R.drawable.final_assistantdirector, R.drawable.final_director};
    String[] EU_SCOREAVATRSTXT = {"YOU ARE A MONKEY", "BANNED FROM CINEMAS", "CINEMA ENTHUSIAST", "CINEMA OPERATOR", "ASSISTANT DIRECTOR", "DIRECTOR"};
    final boolean FREEVERSION = true;
    final int MAXFREEGAMES = 2;
    final int MINSCORE = 100;
    final int POINTS10INAROW = 200;
    final int POINTS3INAROW = 50;
    final int POINTS3INAROWPOINTS = 50;
    final int POINTS5INAROW = 100;
    String PackageURI = "android.resource://com.sgstudios.GeoTrivia/";
    final int TIMEATTACKLIMIT = 3000;
    final int UNLOCKSCORE = 1000;
    boolean mBlinkCorrect = false;
    ImageButton mButtonFalse1P;
    ImageButton mButtonFalse2P;
    ImageButton mButtonTrue1P;
    ImageButton mButtonTrue2P;
    CountDownTimer mCountDownTimer;
    int mDiffLevel;
    boolean mFinal = false;
    boolean mGameOver = false;
    MovieFact.MOVIETYPE mGameType = MovieFact.MOVIETYPE.ACTORNAMES;
    GameoverDialog mGameoverDialog;
    int mGlobalPoints1P;
    int mGlobalPoints2P;
    S3HighScore mHighScore;
    ImageView mImageTimer;
    int mItemHeight = 0;
    S3ScrollingView mLv;
    boolean mMultiAnswer = false;
    boolean mMultiplayer = false;
    boolean mMultiplayerguy = false;
    int mPoints1P;
    int mPoints2P;
    long mTimeRemaining;
    boolean mTimeattack = false;
    AnimationDrawableTimer mTimerAnim;
    LinearLayout mTransparentPanel;
    Vibrator mVibrator;
    boolean mWaitingForScroll = false;
    PowerManager.WakeLock mWakeLock;
    int mXinARowP1 = 0;
    int mXinARowP2 = 0;
    Player m_beeptimer;
    Player m_playerFail;
    Player m_playerMusic;
    Player m_playerWin;
    Random m_randomGenerator;
    EfficientAdapter madapt;
    long mlastClick;
    long mlastClickTick;
    public int mpos = 0;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView(R.layout.main);
        try {
            this.m_playerWin = new Player(this, R.raw.beepfalling);
            this.m_playerFail = new Player(this, R.raw.beepdown);
            this.m_playerMusic = new Player(this, R.raw.axel);
            this.mVibrator = (Vibrator) getSystemService("vibrator");
            this.m_beeptimer = new Player(this, R.raw.timerbeep);
        } catch (Exception e) {
            Exception e2 = e;
            Log.e("TAG", "error: " + e2.getMessage(), e2);
        }
        if (getIntent().getExtras() != null) {
            Bundle bundle = getIntent().getExtras();
            this.mMultiplayer = bundle.getBoolean("multiplayer");
            this.mMultiAnswer = bundle.getBoolean("multianswer");
            this.mDiffLevel = bundle.getInt("gametype");
        }
        if (this.mDiffLevel == 0) {
            this.mGameType = MovieFact.MOVIETYPE.ACTORNAMES;
        } else if (this.mDiffLevel == 1) {
            this.mGameType = MovieFact.MOVIETYPE.ACTORFILMS;
        } else if (this.mDiffLevel == 2) {
            this.mGameType = MovieFact.MOVIETYPE.ACTORFILMS;
        } else if (this.mDiffLevel == 3) {
            this.mGameType = MovieFact.MOVIETYPE.ACTORCHARACTER;
        }
        ITEMS = new Query[50];
        if (!this.mMultiAnswer) {
            ButtonBarInit();
        }
        this.mCountDownTimer = new CountDownTimer(20000, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                MovieTrivia.this.GameOver();
            }
        };
        this.mLv = (S3ScrollingView) findViewById(R.id.list);
        this.madapt = new EfficientAdapter(this);
        this.mLv.setAdapter((ListAdapter) this.madapt);
        this.mLv.mActivity = this;
        this.mLv.TouchEnabled(true);
        this.mLv.ScrollEnabled(false);
        this.mLv.setDividerHeight(0);
        this.m_randomGenerator = new Random();
        this.mWakeLock = ((PowerManager) getSystemService("power")).newWakeLock(10, "tag");
        this.mWakeLock.acquire();
        this.mHighScore = new S3HighScore(this);
        this.mGameoverDialog = new GameoverDialog(this);
        this.mGameoverDialog.requestWindowFeature(1);
        this.mGameoverDialog.mAdView1 = new MobFoxView(this, "65286ad9509fda4f8786e58b0d0d9c3c", true, true);
        ((LinearLayout) findViewById(R.id.Linear)).addView(new MobFoxView(this, "65286ad9509fda4f8786e58b0d0d9c3c", true, true));
        this.mImageTimer = (ImageView) findViewById(R.id.ImageTimer);
        this.mTimerAnim = new AnimationDrawableTimer();
        this.mImageTimer.setImageDrawable(this.mTimerAnim);
        this.mTransparentPanel = (LinearLayout) findViewById(R.id.transparent_panel);
        StartGame();
    }

    class Starter implements Runnable {
        public AnimationDrawable mAnimation;
        public Player mPlayer;
        public ImageView mView;

        Starter() {
        }

        public void run() {
            if (this.mAnimation != null) {
                if (this.mView != null) {
                    this.mView.setImageDrawable(this.mAnimation);
                }
                if (this.mPlayer != null) {
                    this.mPlayer.Play();
                }
                this.mAnimation.stop();
                this.mAnimation.start();
            }
        }
    }

    class AnimationDrawableTimer extends AnimationDrawable {
        public Starter mStarter;

        AnimationDrawableTimer() {
            addFrame(MovieTrivia.this.getResources().getDrawable(R.drawable.n_19), 1000);
            addFrame(MovieTrivia.this.getResources().getDrawable(R.drawable.n_18), 1000);
            addFrame(MovieTrivia.this.getResources().getDrawable(R.drawable.n_17), 1000);
            addFrame(MovieTrivia.this.getResources().getDrawable(R.drawable.n_16), 1000);
            addFrame(MovieTrivia.this.getResources().getDrawable(R.drawable.n_15), 1000);
            addFrame(MovieTrivia.this.getResources().getDrawable(R.drawable.n_14), 1000);
            addFrame(MovieTrivia.this.getResources().getDrawable(R.drawable.n_13), 1000);
            addFrame(MovieTrivia.this.getResources().getDrawable(R.drawable.n_12), 1000);
            addFrame(MovieTrivia.this.getResources().getDrawable(R.drawable.n_11), 1000);
            addFrame(MovieTrivia.this.getResources().getDrawable(R.drawable.n_10), 1000);
            addFrame(MovieTrivia.this.getResources().getDrawable(R.drawable.n_9), 1000);
            addFrame(MovieTrivia.this.getResources().getDrawable(R.drawable.n_8), 1000);
            addFrame(MovieTrivia.this.getResources().getDrawable(R.drawable.n_7), 1000);
            addFrame(MovieTrivia.this.getResources().getDrawable(R.drawable.n_6), 1000);
            addFrame(MovieTrivia.this.getResources().getDrawable(R.drawable.n_5), 1000);
            addFrame(MovieTrivia.this.getResources().getDrawable(R.drawable.n_4), 1000);
            addFrame(MovieTrivia.this.getResources().getDrawable(R.drawable.n_3), 1000);
            addFrame(MovieTrivia.this.getResources().getDrawable(R.drawable.n_2), 1000);
            addFrame(MovieTrivia.this.getResources().getDrawable(R.drawable.n_1), 1000);
            addFrame(MovieTrivia.this.getResources().getDrawable(R.drawable.n_0), 1000);
            setOneShot(true);
            setVisible(true, true);
            this.mStarter = new Starter();
            this.mStarter.mAnimation = this;
        }
    }

    /* access modifiers changed from: package-private */
    public void ButtonBarInit() {
    }

    public void onBackPressed() {
        this.mCountDownTimer.cancel();
        this.mLv.mStopped = true;
        this.mCountDownTimer.cancel();
        this.m_playerWin.Release();
        this.m_playerFail.Release();
        this.m_playerMusic.Release();
        this.m_beeptimer.Release();
        if (this.mGameoverDialog != null) {
            this.mGameoverDialog.Recycle();
        }
        System.gc();
        finish();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.mCountDownTimer.cancel();
        this.m_playerWin.Release();
        this.m_playerFail.Release();
        this.m_playerMusic.Release();
        this.m_beeptimer.Release();
        this.mCountDownTimer.cancel();
        this.m_playerWin = null;
        this.m_playerFail = null;
        this.m_playerMusic = null;
        this.m_beeptimer = null;
        if (this.mGameoverDialog != null) {
            this.mGameoverDialog.Recycle();
        }
        System.gc();
        this.mWakeLock.release();
    }

    public void onResume() {
        super.onResume();
        this.m_playerMusic.Silent(false);
        this.mWakeLock.acquire();
    }

    public void onStart() {
        super.onStart();
    }

    public void onPause() {
        super.onPause();
        this.m_playerMusic.Silent(true);
        this.mWakeLock.release();
    }

    /* access modifiers changed from: package-private */
    public void ShowFreeVersionAllert() {
        FreeVersionDialog dialog = new FreeVersionDialog(this);
        dialog.requestWindowFeature(1);
        dialog.mParent = this;
        dialog.mTerminate = true;
        gamecounter = 0;
        dialog.show();
    }

    public void SetRandomBG() {
    }

    /* access modifiers changed from: package-private */
    public void SuperScroll(int nitems, int delay) {
        this.mLv.postDelayed(new Runnable() {
            public void run() {
                MovieTrivia.this.mpos += 4;
                MovieTrivia.this.madapt.notifyDataSetChanged();
                MovieTrivia.this.mBlinkCorrect = false;
                MovieTrivia.this.mLv.setSelectionFromTop(MovieTrivia.this.mpos, 0);
                MovieTrivia.this.mWaitingForScroll = false;
                MovieTrivia.this.mLv.requestFocusFromTouch();
                MovieTrivia.this.mLv.setSelection(MovieTrivia.this.mpos);
            }
        }, (long) delay);
    }

    public void Touch(boolean timeout, int itemidx) {
        int delay;
        if (!this.mWaitingForScroll) {
            int querypos = itemidx / 4;
            int answerpos = itemidx % 4;
            if (itemidx <= this.mpos + 3 && itemidx > this.mpos) {
                int answerpos2 = answerpos - 1;
                ITEMS[querypos].Childs[answerpos2].answer = 1;
                int player = 1;
                if (!this.mMultiplayer) {
                    ITEMS[querypos].Childs[answerpos2].player = 1;
                } else {
                    if (this.mLv.mLastTouchX > this.mLv.getWidth() / 2) {
                        player = 2;
                    }
                    ITEMS[querypos].Childs[answerpos2].player = player;
                }
                if (!ITEMS[querypos].Childs[answerpos2].isTrue) {
                    this.m_playerFail.Play();
                    this.mVibrator.vibrate(100);
                    this.mLv.AddScore(-100, S3ScoreSprite.BONUSTYPE.NONE);
                    if (player == 1) {
                        this.mXinARowP1 = 0;
                    } else if (player == 2) {
                        this.mXinARowP2 = 0;
                    }
                } else {
                    this.m_playerWin.Play();
                    if (player == 1) {
                        this.mXinARowP1++;
                    } else if (player == 2) {
                        this.mXinARowP2++;
                    }
                    int bonuspoints = 0;
                    S3ScoreSprite.BONUSTYPE btype = S3ScoreSprite.BONUSTYPE.NONE;
                    if (player == 1) {
                        if (this.mXinARowP1 == 3) {
                            bonuspoints = 50;
                            btype = S3ScoreSprite.BONUSTYPE.BONUS3INROW;
                        } else if (this.mXinARowP1 == 5) {
                            bonuspoints = 100;
                            btype = S3ScoreSprite.BONUSTYPE.BONUS5INROW;
                        } else if (this.mXinARowP1 == 10) {
                            bonuspoints = 200;
                            btype = S3ScoreSprite.BONUSTYPE.BONUS10INROW;
                            this.mXinARowP1 = 0;
                        }
                    } else if (player == 2) {
                        if (this.mXinARowP2 == 3) {
                            bonuspoints = 50;
                            btype = S3ScoreSprite.BONUSTYPE.BONUS3INROW;
                        } else if (this.mXinARowP2 == 5) {
                            bonuspoints = 100;
                            btype = S3ScoreSprite.BONUSTYPE.BONUS5INROW;
                        } else if (this.mXinARowP2 == 10) {
                            bonuspoints = 200;
                            btype = S3ScoreSprite.BONUSTYPE.BONUS10INROW;
                            this.mXinARowP2 = 0;
                        }
                    }
                    ITEMS[querypos].Childs[answerpos2].mBonus = bonuspoints;
                    this.mLv.AddScore(bonuspoints + 250, btype);
                }
                if (!ITEMS[querypos].Childs[answerpos2].isTrue) {
                    delay = BufferRecycler.DEFAULT_WRITE_CONCAT_BUFFER_LEN;
                } else {
                    delay = 1000;
                }
                BlinkCorrectAnswer();
                this.mWaitingForScroll = true;
                this.mLv.requestFocus();
                SuperScroll(-1, delay);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void BlinkCorrectAnswer() {
        this.mBlinkCorrect = true;
        this.madapt.notifyDataSetChanged();
    }

    public void Click(boolean value, int player) {
        int i;
        boolean answ;
        this.mlastClick = this.mTimeRemaining;
        long now = SystemClock.uptimeMillis();
        if (now - this.mlastClickTick < 400) {
            this.mLv.requestFocusFromTouch();
            this.mLv.setSelection(this.mpos);
            this.madapt.notifyDataSetChanged();
            if (Integer.valueOf(Build.VERSION.SDK).intValue() < 8) {
                this.mLv.setSelectionFromTop(this.mpos, 80);
            } else {
                this.mLv.smoothScrollToPosition(this.mpos + 3);
            }
        } else {
            this.mlastClickTick = now;
            if (this.mpos >= ITEMS.length) {
                GameOver();
                return;
            }
            Query query = ITEMS[this.mpos];
            if (value) {
                i = 1;
            } else {
                i = 0;
            }
            query.answer = i;
            ITEMS[this.mpos].player = player;
            if (value) {
                answ = true;
            } else {
                answ = false;
            }
            if (answ != ITEMS[this.mpos].isTrue) {
                this.m_playerFail.Play();
                this.mVibrator.vibrate(100);
            } else {
                this.m_playerWin.Play();
            }
            this.mpos++;
            this.mLv.requestFocusFromTouch();
            this.mLv.setSelection(this.mpos);
            this.madapt.notifyDataSetChanged();
            if (Integer.valueOf(Build.VERSION.SDK).intValue() < 8) {
                this.mLv.setSelectionFromTop(this.mpos, 80);
            } else {
                this.mLv.smoothScrollToPosition(this.mpos + 3);
            }
            UpdateScore();
        }
    }

    public void GameOver() {
        UpdateScore();
        if (!this.mGameOver) {
            this.mGameOver = true;
            this.mGlobalPoints1P += this.mPoints1P;
            this.mGlobalPoints2P += this.mPoints2P;
            boolean newrec = false;
            this.mHighScore.GetScore(this.mDiffLevel);
            if (this.mGlobalPoints1P > this.mHighScore.mScore) {
                newrec = true;
            }
            this.mGameoverDialog.mNewRecord = newrec;
            this.mGameoverDialog.mscore1P = this.mPoints1P;
            this.mGameoverDialog.mscore2P = this.mPoints2P;
            this.mGameoverDialog.mGlobalscore1P = this.mGlobalPoints1P;
            this.mGameoverDialog.mGlobalscore2P = this.mGlobalPoints2P;
            this.mGameoverDialog.mMultiplayer = this.mMultiplayer;
            this.mLv.mStopped = true;
            this.mGameoverDialog.show();
        }
    }

    public void StartGame() {
        this.mLv.mStopped = false;
        this.mXinARowP1 = 0;
        this.mXinARowP2 = 0;
        this.mlastClick = 20000;
        ITEMS = new Query[15];
        int i = 0;
        while (i < ITEMS.length) {
            ITEMS[i] = new Query(this.mGameType);
            int k = 0;
            while (true) {
                if (k >= i) {
                    break;
                } else if (ITEMS[i].mImage == ITEMS[k].mImage) {
                    i--;
                    break;
                } else {
                    k++;
                }
            }
            i++;
        }
        this.madapt.notifyDataSetChanged();
        this.mLv.SetRandomBG();
        if (!this.mMultiAnswer) {
            this.mLv.requestFocusFromTouch();
        }
        this.mpos = 0;
        UpdateScore();
        this.mGameOver = false;
        this.mLv.setSelection(0);
        int delay = 0;
        this.mTransparentPanel.setVisibility(0);
        ImageView guy = (ImageView) findViewById(R.id.ImageViewActor);
        S3FrameAnimation animGuy = new S3FrameAnimation(this);
        if (this.mMultiplayer && !this.mMultiplayerguy) {
            animGuy.AddFrame(R.drawable.multi1, 1500);
            animGuy.AddFrame(R.drawable.multi2, 1500);
            delay = 3000;
            this.mMultiplayerguy = true;
        }
        if (this.mGameType == MovieFact.MOVIETYPE.ACTORNAMES) {
            animGuy.AddFrame(R.drawable.actornames, 1000);
            animGuy.AddFrame(R.drawable.actornames1, 500);
            animGuy.AddFrame(R.drawable.actornames2, 500);
            animGuy.AddFrame(R.drawable.actornames1, 500);
        } else if (this.mGameType == MovieFact.MOVIETYPE.ACTORFILMS) {
            animGuy.AddFrame(R.drawable.movienames, 1000);
            animGuy.AddFrame(R.drawable.movienames1, 500);
            animGuy.AddFrame(R.drawable.movienames2, 500);
            animGuy.AddFrame(R.drawable.movienames1, 500);
        } else if (this.mGameType == MovieFact.MOVIETYPE.ACTORCHARACTER) {
            animGuy.AddFrame(R.drawable.charnames, 1000);
            animGuy.AddFrame(R.drawable.charnames1, 500);
            animGuy.AddFrame(R.drawable.charnames2, 500);
            animGuy.AddFrame(R.drawable.charnames1, 500);
        }
        animGuy.SetView(guy);
        animGuy.Start();
        Runnable Timerbeeper = new Runnable() {
            public void run() {
                MovieTrivia.this.m_beeptimer.Play();
            }
        };
        Runnable GameStarter = new Runnable() {
            public void run() {
                MovieTrivia.this.mTransparentPanel.setVisibility(8);
                MovieTrivia.this.mCountDownTimer.start();
                MovieTrivia.this.mImageTimer.postDelayed(MovieTrivia.this.mTimerAnim.mStarter, 0);
            }
        };
        S3FrameAnimation animTimer = new S3FrameAnimation(this);
        animTimer.AddFrame(R.drawable.cd3, delay + 1000);
        animTimer.AddFrame(R.drawable.cd2, 1000);
        animTimer.AddFrame(R.drawable.cd1, 1000);
        animTimer.AddFrame(R.drawable.cd0, 1000);
        animTimer.SetView((ImageView) findViewById(R.id.cdTimer));
        animTimer.SetEndEvent(GameStarter);
        animTimer.SetChangeEvent(Timerbeeper);
        animTimer.Start();
    }

    class CountDownStart extends AnimationDrawable {
        public Starter mStarter;

        CountDownStart() {
            addFrame(MovieTrivia.this.getResources().getDrawable(R.drawable.cd3), 1000);
            addFrame(MovieTrivia.this.getResources().getDrawable(R.drawable.cd2), 1000);
            addFrame(MovieTrivia.this.getResources().getDrawable(R.drawable.cd1), 1000);
            addFrame(MovieTrivia.this.getResources().getDrawable(R.drawable.cd0), 1000);
            setOneShot(true);
            setVisible(true, true);
            this.mStarter = new Starter();
            this.mStarter.mAnimation = this;
        }
    }

    class GuyAskingActorsNames extends AnimationDrawable {
        public Starter mStarter;

        GuyAskingActorsNames() {
            Drawable act = MovieTrivia.this.getResources().getDrawable(R.drawable.actornames);
            Drawable act1 = MovieTrivia.this.getResources().getDrawable(R.drawable.actornames1);
            Drawable act2 = MovieTrivia.this.getResources().getDrawable(R.drawable.actornames2);
            addFrame(act, 300);
            addFrame(act1, 300);
            addFrame(act, 300);
            addFrame(act1, 300);
            addFrame(act1, 300);
            addFrame(act2, 300);
            addFrame(act1, 300);
            addFrame(act2, 300);
            setOneShot(true);
            setVisible(true, true);
            this.mStarter = new Starter();
            this.mStarter.mAnimation = this;
        }
    }

    class GuyAskingCharacterNames extends AnimationDrawable {
        public Starter mStarter;

        GuyAskingCharacterNames() {
            Drawable charact = MovieTrivia.this.getResources().getDrawable(R.drawable.charnames);
            Drawable charact1 = MovieTrivia.this.getResources().getDrawable(R.drawable.charnames1);
            Drawable charact2 = MovieTrivia.this.getResources().getDrawable(R.drawable.charnames2);
            addFrame(charact, 300);
            addFrame(charact1, 300);
            addFrame(charact, 300);
            addFrame(charact1, 300);
            addFrame(charact1, 300);
            addFrame(charact2, 300);
            addFrame(charact1, 300);
            addFrame(charact2, 300);
            setOneShot(true);
            setVisible(true, true);
            this.mStarter = new Starter();
            this.mStarter.mAnimation = this;
        }
    }

    class GuyAskingActorsFilms extends AnimationDrawable {
        public Starter mStarter;

        GuyAskingActorsFilms() {
            Drawable mvn = MovieTrivia.this.getResources().getDrawable(R.drawable.movienames);
            Drawable mvn1 = MovieTrivia.this.getResources().getDrawable(R.drawable.movienames1);
            Drawable mvn2 = MovieTrivia.this.getResources().getDrawable(R.drawable.movienames2);
            addFrame(mvn, 300);
            addFrame(mvn1, 300);
            addFrame(mvn, 300);
            addFrame(mvn1, 300);
            addFrame(mvn1, 300);
            addFrame(mvn2, 300);
            addFrame(mvn1, 300);
            addFrame(mvn2, 300);
            setOneShot(true);
            setVisible(true, true);
            this.mStarter = new Starter();
            this.mStarter.mAnimation = this;
        }
    }

    class GuyMultiPlayer extends AnimationDrawable {
        public Starter mStarter;

        GuyMultiPlayer() {
            Drawable m1 = MovieTrivia.this.getResources().getDrawable(R.drawable.multi1);
            Drawable m2 = MovieTrivia.this.getResources().getDrawable(R.drawable.multi2);
            addFrame(MovieTrivia.this.getResources().getDrawable(R.drawable.multi), 1000);
            addFrame(m1, 1000);
            addFrame(m2, 1000);
            addFrame(m1, 1000);
            addFrame(m2, 1000);
            setOneShot(true);
            setVisible(true, true);
            this.mStarter = new Starter();
            this.mStarter.mAnimation = this;
        }
    }

    public class GameoverDialog extends Dialog implements View.OnClickListener {
        final int SCORECOUNTERMAXDELTA = 25;
        Typeface btf;
        MobFoxView mAdView1;
        /* access modifiers changed from: private */
        public final Runnable mBlinkNewRecord = new Runnable() {
            public void run() {
                if (GameoverDialog.this.mNewRec.isShown()) {
                    GameoverDialog.this.mScoretot.setTextColor(-1);
                    GameoverDialog.this.mNewRec.setVisibility(4);
                } else {
                    GameoverDialog.this.mNewRec.setVisibility(0);
                    GameoverDialog.this.mScoretot.setTextColor(-65536);
                }
                GameoverDialog.this.mHandler.postDelayed(GameoverDialog.this.mBlinkNewRecord, 300);
            }
        };
        ImageButton mContinueBtn;
        DrawView mDw;
        ImageButton mExitBtn;
        int mGlobalscore1P;
        int mGlobalscore2P;
        /* access modifiers changed from: private */
        public Handler mHandler;
        ImageView mImage;
        boolean mMultiplayer;
        ImageView mNewRec;
        boolean mNewRecord;
        boolean mReadyToClose = false;
        TextView mScore;
        int mScoreCounterIncrease = 0;
        TextView mScoretot;
        TextView mStage;
        /* access modifiers changed from: private */
        public final Runnable mUpdateScoreTxt = new Runnable() {
            public void run() {
                GameoverDialog.this.mExitBtn.setEnabled(false);
                GameoverDialog.this.mContinueBtn.setEnabled(false);
                if (GameoverDialog.this.mScoreCounterIncrease < 25) {
                    GameoverDialog.this.mScoreCounterIncrease++;
                }
                if (GameoverDialog.this.mscore1P < 0) {
                    if (GameoverDialog.this.p1scrcounter - GameoverDialog.this.mScoreCounterIncrease > GameoverDialog.this.mscore1P) {
                        GameoverDialog.this.p1scrcounter -= GameoverDialog.this.mScoreCounterIncrease;
                    } else {
                        GameoverDialog.this.p1scrcounter = GameoverDialog.this.mscore1P;
                    }
                }
                if (GameoverDialog.this.mscore1P > 0) {
                    if (GameoverDialog.this.p1scrcounter + GameoverDialog.this.mScoreCounterIncrease < GameoverDialog.this.mscore1P) {
                        GameoverDialog.this.p1scrcounter += GameoverDialog.this.mScoreCounterIncrease;
                    } else {
                        GameoverDialog.this.p1scrcounter = GameoverDialog.this.mscore1P;
                    }
                }
                if (GameoverDialog.this.mscore2P < 0) {
                    if (GameoverDialog.this.p2scrcounter - GameoverDialog.this.mScoreCounterIncrease > GameoverDialog.this.mscore2P) {
                        GameoverDialog.this.p2scrcounter -= GameoverDialog.this.mScoreCounterIncrease;
                    } else {
                        GameoverDialog.this.p2scrcounter = GameoverDialog.this.mscore2P;
                    }
                }
                if (GameoverDialog.this.mscore2P > 0) {
                    if (GameoverDialog.this.p2scrcounter + GameoverDialog.this.mScoreCounterIncrease < GameoverDialog.this.mscore2P) {
                        GameoverDialog.this.p2scrcounter += GameoverDialog.this.mScoreCounterIncrease;
                    } else {
                        GameoverDialog.this.p2scrcounter = GameoverDialog.this.mscore2P;
                    }
                }
                if (!GameoverDialog.this.mMultiplayer) {
                    GameoverDialog.this.mScore.setText("Points: " + GameoverDialog.this.p1scrcounter);
                    GameoverDialog.this.mScoretot.setText("Score: " + ((GameoverDialog.this.mGlobalscore1P - GameoverDialog.this.mscore1P) + GameoverDialog.this.p1scrcounter));
                } else {
                    GameoverDialog.this.mScore.setText("P1: " + GameoverDialog.this.p1scrcounter + "    P2: " + GameoverDialog.this.p2scrcounter);
                    GameoverDialog.this.mScoretot.setText("Tot: " + ((GameoverDialog.this.mGlobalscore1P - GameoverDialog.this.mscore1P) + GameoverDialog.this.p1scrcounter) + "    Tot: " + ((GameoverDialog.this.mGlobalscore2P - GameoverDialog.this.mscore2P) + GameoverDialog.this.p2scrcounter));
                }
                if (GameoverDialog.this.p1scrcounter == GameoverDialog.this.mscore1P && GameoverDialog.this.p2scrcounter == GameoverDialog.this.mscore2P) {
                    GameoverDialog.this.mExitBtn.setEnabled(true);
                    GameoverDialog.this.mContinueBtn.setEnabled(true);
                    if (!GameoverDialog.this.mMultiplayer && MovieTrivia.this.mGameType == MovieFact.MOVIETYPE.ACTORCHARACTER && GameoverDialog.this.mNewRecord) {
                        GameoverDialog.this.mHandler.postDelayed(GameoverDialog.this.mBlinkNewRecord, 300);
                        return;
                    }
                    return;
                }
                GameoverDialog.this.mHandler.postDelayed(GameoverDialog.this.mUpdateScoreTxt, 30);
            }
        };
        Context mctx;
        int mscore1P;
        int mscore2P;
        int p1scrcounter = 0;
        int p2scrcounter = 0;

        public void Recycle() {
            if (this.mDw != null) {
                this.mDw.Recycle();
                this.mDw = null;
            }
        }

        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            this.mDw = new DrawView(getContext());
            setContentView(this.mDw);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(14);
            ((LinearLayout) findViewById(R.id.layout_root)).addView(this.mAdView1, layoutParams);
            this.mDw.invalidate();
            MovieTrivia.this.m_playerMusic.Play();
            this.mScore = (TextView) findViewById(R.id.score);
            this.mScore.setTypeface(this.btf);
            this.mScoretot = (TextView) findViewById(R.id.scoretot);
            this.mScoretot.setTypeface(this.btf);
            this.mStage = (TextView) findViewById(R.id.stage);
            this.mStage.setTypeface(this.btf);
            this.mContinueBtn = (ImageButton) findViewById(R.id.Ok);
            this.mContinueBtn.setOnClickListener(this);
            this.mExitBtn = (ImageButton) findViewById(R.id.Canc);
            this.mExitBtn.setOnClickListener(this);
            this.mImage = (ImageView) findViewById(R.id.image);
            this.mNewRec = (ImageView) findViewById(R.id.newrec);
            this.mHandler = new Handler();
        }

        public GameoverDialog(Context c) {
            super(c, R.style.Dialog_Fullscreen);
            this.btf = Typeface.createFromAsset(MovieTrivia.this.getAssets(), "fonts/MrBubbleFont.ttf");
            this.mctx = c;
        }

        /* access modifiers changed from: protected */
        public void onStart() {
            if (MovieTrivia.this.m_playerMusic.mMediaPlayer != null && !MovieTrivia.this.m_playerMusic.mMediaPlayer.isPlaying()) {
                MovieTrivia.this.m_playerMusic.RePlay();
            }
            if (!this.mMultiplayer) {
                this.mScore.setTextSize(30.0f);
                this.mScoretot.setTextSize(40.0f);
            } else {
                this.mScore.setTextSize(30.0f);
                this.mScoretot.setTextSize(30.0f);
            }
            if (MovieTrivia.this.mGameType == MovieFact.MOVIETYPE.ACTORNAMES) {
                this.mStage.setText("Stage 1:");
            } else if (MovieTrivia.this.mGameType == MovieFact.MOVIETYPE.ACTORFILMS) {
                this.mStage.setText("Stage 2:");
            } else if (MovieTrivia.this.mGameType == MovieFact.MOVIETYPE.ACTORCHARACTER) {
                this.mStage.setText("Stage 3:");
            }
            this.p1scrcounter = 0;
            this.p2scrcounter = 0;
            this.mHandler.postDelayed(this.mUpdateScoreTxt, 100);
            this.mImage.setImageResource(R.drawable.oscar);
            this.mImage.startAnimation(AnimationUtils.loadAnimation(MovieTrivia.this.getBaseContext(), R.anim.airplane));
            if (!MovieTrivia.this.hasWindowFocus()) {
            }
        }

        public void onBackPressed() {
            CloseGameFromDialog();
        }

        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.Ok:
                    if (MovieTrivia.this.mGameType == MovieFact.MOVIETYPE.ACTORNAMES) {
                        MovieTrivia.this.mGameType = MovieFact.MOVIETYPE.ACTORFILMS;
                    } else if (MovieTrivia.this.mGameType == MovieFact.MOVIETYPE.ACTORFILMS) {
                        MovieTrivia.this.mGameType = MovieFact.MOVIETYPE.ACTORCHARACTER;
                    } else if (MovieTrivia.this.mGameType == MovieFact.MOVIETYPE.ACTORCHARACTER) {
                        if (!this.mReadyToClose) {
                            LaunchFinalAnimation();
                            return;
                        }
                        MovieTrivia.this.mHighScore.SetScore(MovieTrivia.this.mDiffLevel, MovieTrivia.this.mGlobalPoints1P);
                        CloseGameFromDialog();
                        return;
                    }
                    MovieTrivia.this.m_playerMusic.Stop();
                    MovieTrivia.this.StartGame();
                    dismiss();
                    return;
                case R.id.Canc:
                    CloseGameFromDialog();
                    return;
                default:
                    return;
            }
        }

        private void CloseGameFromDialog() {
            MovieTrivia.this.m_playerMusic.Stop();
            this.mReadyToClose = false;
            this.mDw.mScoreAvatar1 = -1;
            this.mDw.mScoreAvatar2 = -1;
            MovieTrivia.this.mLv.Release();
            MovieTrivia.this.m_playerWin.Release();
            MovieTrivia.this.m_playerFail.Release();
            MovieTrivia.this.m_playerMusic.Release();
            MovieTrivia.this.m_beeptimer.Release();
            Recycle();
            System.gc();
            dismiss();
            MovieTrivia.this.finish();
        }

        /* access modifiers changed from: package-private */
        public void LaunchFinalAnimation() {
            this.mReadyToClose = true;
            Animation airplaneanim2 = AnimationUtils.loadAnimation(MovieTrivia.this.getBaseContext(), R.anim.oscarfadeout);
            airplaneanim2.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationEnd(Animation animation) {
                    GameoverDialog.this.mScore.setText("");
                    GameoverDialog.this.mStage.setText("");
                    if (GameoverDialog.this.mGlobalscore1P < 1000) {
                        GameoverDialog.this.mDw.mScoreAvatar1 = 0;
                    } else if (GameoverDialog.this.mGlobalscore1P < 2000) {
                        GameoverDialog.this.mDw.mScoreAvatar1 = 1;
                    } else if (GameoverDialog.this.mGlobalscore1P < 3000) {
                        GameoverDialog.this.mDw.mScoreAvatar1 = 2;
                    } else if (GameoverDialog.this.mGlobalscore1P < 4000) {
                        GameoverDialog.this.mDw.mScoreAvatar1 = 3;
                    } else if (GameoverDialog.this.mGlobalscore1P < 5000) {
                        GameoverDialog.this.mDw.mScoreAvatar1 = 4;
                    } else {
                        GameoverDialog.this.mDw.mScoreAvatar1 = 5;
                    }
                    if (GameoverDialog.this.mGlobalscore2P < 1000) {
                        GameoverDialog.this.mDw.mScoreAvatar2 = 0;
                    } else if (GameoverDialog.this.mGlobalscore2P < 2000) {
                        GameoverDialog.this.mDw.mScoreAvatar2 = 1;
                    } else if (GameoverDialog.this.mGlobalscore2P < 3000) {
                        GameoverDialog.this.mDw.mScoreAvatar2 = 2;
                    } else if (GameoverDialog.this.mGlobalscore2P < 4000) {
                        GameoverDialog.this.mDw.mScoreAvatar2 = 3;
                    } else if (GameoverDialog.this.mGlobalscore2P < 5000) {
                        GameoverDialog.this.mDw.mScoreAvatar2 = 4;
                    } else {
                        GameoverDialog.this.mDw.mScoreAvatar2 = 5;
                    }
                    if (!GameoverDialog.this.mMultiplayer) {
                        GameoverDialog.this.mScore.setText(MovieTrivia.this.EU_SCOREAVATRSTXT[GameoverDialog.this.mDw.mScoreAvatar1]);
                        GameoverDialog.this.mImage.setImageResource(MovieTrivia.this.EU_SCOREAVATRS[GameoverDialog.this.mDw.mScoreAvatar1]);
                        GameoverDialog.this.mImage.setVisibility(0);
                        return;
                    }
                    GameoverDialog.this.mImage.setVisibility(4);
                }

                public void onAnimationStart(Animation animation) {
                }

                public void onAnimationRepeat(Animation animation) {
                }
            });
            this.mImage.startAnimation(airplaneanim2);
        }

        public class DrawView extends LinearLayout {
            private Paint mBgPaint;
            private Shader mBgShader;
            Rect mBounds;
            private Matrix mMatrix0;
            private Matrix mMatrix1;
            private Matrix mMatrix2;
            Paint mPaint;
            private Paint mParallax1Paint;
            private Paint mParallax2Paint;
            private Shader mPx1Shader;
            private Shader mPx2Shader;
            public int mScoreAvatar1 = -1;
            public int mScoreAvatar2 = -1;
            private float mXtVel0;
            private float mXtVel1;
            private float mXtVel2;
            Bitmap m_scorebmp1;
            Bitmap m_scorebmp2;
            private Bitmap mbg;
            private Bitmap mpx1;
            private Bitmap mpx2;

            public DrawView(Context context) {
                super(context);
                LayoutInflater inflater = (LayoutInflater) context.getSystemService("layout_inflater");
                if (inflater != null) {
                    inflater.inflate((int) R.layout.score_dialog, this);
                    setWillNotDraw(false);
                    this.mPaint = new Paint();
                    this.mPaint.setTextSize(20.0f);
                    this.mPaint.setColor(-1);
                    this.mBounds = new Rect();
                }
                this.mbg = BitmapFactory.decodeResource(getResources(), R.drawable.bg0);
                if (this.mbg != null) {
                    this.mBgShader = new BitmapShader(this.mbg, Shader.TileMode.REPEAT, Shader.TileMode.MIRROR);
                    this.mBgPaint = new Paint();
                    this.mBgPaint.setShader(this.mBgShader);
                }
                BitmapFactory.Options opt = new BitmapFactory.Options();
                opt.inSampleSize = 2;
                this.mpx1 = BitmapFactory.decodeResource(getResources(), R.drawable.bg1, opt);
                if (this.mpx1 != null) {
                    this.mPx1Shader = new BitmapShader(this.mpx1, Shader.TileMode.REPEAT, Shader.TileMode.MIRROR);
                    this.mParallax1Paint = new Paint();
                    this.mParallax1Paint.setShader(this.mPx1Shader);
                }
                this.mpx2 = BitmapFactory.decodeResource(getResources(), R.drawable.bg2, opt);
                if (this.mpx2 != null) {
                    this.mPx2Shader = new BitmapShader(this.mpx2, Shader.TileMode.REPEAT, Shader.TileMode.MIRROR);
                    this.mParallax2Paint = new Paint();
                    this.mParallax2Paint.setShader(this.mPx2Shader);
                }
                this.mMatrix0 = new Matrix();
                this.mMatrix1 = new Matrix();
                this.mMatrix2 = new Matrix();
                invalidate();
            }

            public void Recycle() {
                if (this.mbg != null) {
                    this.mbg.recycle();
                    this.mbg = null;
                }
                if (this.mpx1 != null) {
                    this.mpx1.recycle();
                    this.mpx1 = null;
                }
                if (this.mpx2 != null) {
                    this.mpx2.recycle();
                    this.mpx2 = null;
                }
            }

            /* access modifiers changed from: protected */
            public void onDraw(Canvas canvas) {
                if (!(this.mBgPaint == null || this.mBgShader == null)) {
                    this.mXtVel0 -= 0.025f;
                    this.mMatrix0.postTranslate(-0.5f, 0.0f);
                    this.mBgShader.setLocalMatrix(this.mMatrix0);
                    canvas.drawPaint(this.mBgPaint);
                }
                if (!(this.mParallax1Paint == null || this.mPx1Shader == null)) {
                    this.mXtVel1 += 0.05f;
                    this.mMatrix1.postTranslate(-1.0f, 0.0f);
                    this.mPx1Shader.setLocalMatrix(this.mMatrix1);
                    canvas.drawPaint(this.mParallax1Paint);
                }
                if (!(this.mParallax2Paint == null || this.mPx2Shader == null)) {
                    this.mXtVel2 += 0.1f;
                    this.mMatrix2.postTranslate(-3.0f, 0.0f);
                    this.mPx2Shader.setLocalMatrix(this.mMatrix2);
                    canvas.drawPaint(this.mParallax2Paint);
                }
                if (GameoverDialog.this.mMultiplayer) {
                    if (this.mScoreAvatar1 == -1 || this.mScoreAvatar1 >= MovieTrivia.this.EU_SCOREAVATRS.length) {
                        this.m_scorebmp1 = null;
                    } else {
                        if (this.m_scorebmp1 == null) {
                            this.m_scorebmp1 = BitmapFactory.decodeResource(getResources(), MovieTrivia.this.EU_SCOREAVATRS[this.mScoreAvatar1]);
                        }
                        canvas.drawBitmap(this.m_scorebmp1, (float) 0, (float) 40, new Paint());
                        String txtmsg = MovieTrivia.this.EU_SCOREAVATRSTXT[this.mScoreAvatar1];
                        this.mPaint.getTextBounds(txtmsg, 0, txtmsg.length(), this.mBounds);
                        canvas.drawText(txtmsg, (float) ((this.m_scorebmp1.getWidth() - this.mBounds.width()) / 2), (float) (40 - this.mBounds.height()), this.mPaint);
                    }
                    if (this.mScoreAvatar2 == -1 || this.mScoreAvatar2 >= MovieTrivia.this.EU_SCOREAVATRS.length) {
                        this.m_scorebmp2 = null;
                    } else {
                        if (this.m_scorebmp2 == null) {
                            this.m_scorebmp2 = BitmapFactory.decodeResource(getResources(), MovieTrivia.this.EU_SCOREAVATRS[this.mScoreAvatar2]);
                        }
                        canvas.drawBitmap(this.m_scorebmp2, (float) (((canvas.getWidth() / 4) * 3) - (this.m_scorebmp2.getWidth() / 2)), (float) 40, new Paint());
                        String txtmsg2 = MovieTrivia.this.EU_SCOREAVATRSTXT[this.mScoreAvatar2];
                        this.mPaint.getTextBounds(txtmsg2, 0, txtmsg2.length(), this.mBounds);
                        canvas.drawText(txtmsg2, (float) (((canvas.getWidth() / 4) * 3) - (this.mBounds.width() / 2)), (float) (40 - this.mBounds.height()), this.mPaint);
                    }
                }
                invalidate();
            }
        }
    }

    public void UpdateScore() {
        if (this.mMultiAnswer) {
            UpdateScoreMultiAnsw();
        } else {
            UpdateScoreSingleAnsw();
        }
    }

    public void UpdateScoreSingleAnsw() {
        int score1 = 0;
        int score2 = 0;
        for (int i = 0; i < ITEMS.length; i++) {
            if (ITEMS[i].answer != -1) {
                if ((ITEMS[i].answer == 1) == ITEMS[i].isTrue) {
                    if (ITEMS[i].player == 1) {
                        score1 += 100;
                    } else if (ITEMS[i].player == 2) {
                        score2 += 100;
                    }
                } else if (ITEMS[i].player == 1) {
                    score1 -= 100;
                } else if (ITEMS[i].player == 2) {
                    score2 -= 100;
                } else if (ITEMS[i].player == 0) {
                    score1 -= 100;
                    score2 -= 100;
                }
            }
        }
        this.mPoints1P = score1;
        this.mPoints2P = score2;
    }

    public void UpdateScoreMultiAnsw() {
        int score1 = 0;
        int score2 = 0;
        for (int i = 0; i < ITEMS.length; i++) {
            for (int k = 0; k < 3; k++) {
                if (ITEMS[i].Childs[k].answer != -1) {
                    boolean userans = ITEMS[i].Childs[k].answer == 1;
                    boolean aspectedansw = ITEMS[i].Childs[k].isTrue;
                    int playerid = ITEMS[i].Childs[k].player;
                    int bonuspoints = ITEMS[i].Childs[k].mBonus;
                    if (userans == aspectedansw) {
                        if (playerid == 1) {
                            score1 += bonuspoints + 250;
                        } else if (playerid == 2) {
                            score2 += bonuspoints + 250;
                        }
                    } else if (playerid == 1) {
                        score1 -= 100;
                    } else if (playerid == 2) {
                        score2 -= 100;
                    } else if (playerid == 0) {
                        score1 -= 100;
                        score2 -= 100;
                    }
                }
            }
        }
        this.mPoints1P = score1;
        this.mPoints2P = score2;
    }

    private class EfficientAdapter extends BaseAdapter {
        ViewHolder holder;
        Drawable mA1img;
        Drawable mAimg;
        Drawable mB1img;
        Drawable mBimg;
        Drawable mC1img;
        Drawable mCimg;
        AnimationDrawable mFrameAnim;
        Animation mHyperspaceJump;
        private LayoutInflater mInflater;
        Animation mTextAnimation;
        Drawable m_bmpFrame;
        Drawable m_bmpFrame1P;
        Drawable m_bmpFrame2P;
        Drawable m_bmpFrameGREEN;
        Drawable m_bmpFramePhoto;
        Drawable m_bmpFrameRED;
        Drawable m_bmpFrameSelectLeft;
        Drawable m_bmpFrameSelectRight;
        Bitmap m_bmpfalse;
        Bitmap m_bmpone;
        Bitmap m_bmpquest;
        Bitmap m_bmpthree;
        Bitmap m_bmptrue;
        Bitmap m_bmptwo;

        public EfficientAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
            this.m_bmpquest = BitmapFactory.decodeResource(MovieTrivia.this.getResources(), R.drawable.iconquestion);
            this.m_bmpfalse = BitmapFactory.decodeResource(MovieTrivia.this.getResources(), R.drawable.iconfalse);
            this.m_bmptrue = BitmapFactory.decodeResource(MovieTrivia.this.getResources(), R.drawable.icontrue);
            this.m_bmpone = BitmapFactory.decodeResource(MovieTrivia.this.getResources(), R.drawable.one);
            this.m_bmptwo = BitmapFactory.decodeResource(MovieTrivia.this.getResources(), R.drawable.two);
            this.m_bmpthree = BitmapFactory.decodeResource(MovieTrivia.this.getResources(), R.drawable.three);
            this.m_bmpFrame = MovieTrivia.this.getResources().getDrawable(R.drawable.calcframe);
            this.m_bmpFrame1P = MovieTrivia.this.getResources().getDrawable(R.drawable.calcframeyellow);
            this.m_bmpFrame2P = MovieTrivia.this.getResources().getDrawable(R.drawable.calcframeblue);
            this.m_bmpFrameRED = MovieTrivia.this.getResources().getDrawable(R.drawable.calcframered);
            this.m_bmpFrameGREEN = MovieTrivia.this.getResources().getDrawable(R.drawable.calcframegreen);
            this.m_bmpFramePhoto = MovieTrivia.this.getResources().getDrawable(R.drawable.photobg);
            this.m_bmpFrameSelectRight = MovieTrivia.this.getResources().getDrawable(R.drawable.calcframebluerightyellow);
            this.m_bmpFrameSelectLeft = MovieTrivia.this.getResources().getDrawable(R.drawable.calcframeblueleftyellow);
            this.mHyperspaceJump = AnimationUtils.loadAnimation(MovieTrivia.this.getBaseContext(), R.anim.iconanimation);
            this.mTextAnimation = AnimationUtils.loadAnimation(MovieTrivia.this.getBaseContext(), R.anim.textanimation);
            this.mAimg = MovieTrivia.this.getResources().getDrawable(R.drawable.a);
            this.mA1img = MovieTrivia.this.getResources().getDrawable(R.drawable.a1);
            this.mBimg = MovieTrivia.this.getResources().getDrawable(R.drawable.b);
            this.mB1img = MovieTrivia.this.getResources().getDrawable(R.drawable.b1);
            this.mCimg = MovieTrivia.this.getResources().getDrawable(R.drawable.c);
            this.mC1img = MovieTrivia.this.getResources().getDrawable(R.drawable.c1);
        }

        public int getCount() {
            return MovieTrivia.ITEMS.length * 4;
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = this.mInflater.inflate((int) R.layout.item, (ViewGroup) null);
                this.holder = new ViewHolder();
                this.holder.text = (TextView) convertView.findViewById(R.id.text);
                this.holder.hint = (TextView) convertView.findViewById(R.id.hint);
                Typeface tf = Typeface.createFromAsset(MovieTrivia.this.getAssets(), "fonts/MrBubbleFont.ttf");
                this.holder.text.setTypeface(tf);
                this.holder.hint.setTypeface(tf);
                this.holder.hint.setTextSize(30.0f);
                this.holder.icon = (ImageView) convertView.findViewById(R.id.image1);
                this.holder.firsticon = (ImageView) convertView.findViewById(R.id.image0);
                this.holder.secondicon = (ImageView) convertView.findViewById(R.id.image2);
            } else {
                this.holder = (ViewHolder) convertView.getTag();
            }
            this.holder.text.setTextColor(-1);
            this.holder.hint.setTextColor(-1);
            if (!MovieTrivia.this.mMultiAnswer) {
                DrawSingleAnswer(position, this.holder, convertView);
            } else {
                DrawMultiAnswer(position, this.holder, convertView);
            }
            convertView.setTag(this.holder);
            if (MovieTrivia.this.mItemHeight == 0) {
                MovieTrivia.this.mItemHeight = 70;
            }
            if (convertView.getHeight() > 0) {
                MovieTrivia.this.mItemHeight = convertView.getHeight();
            }
            return convertView;
        }

        class ViewHolder {
            ImageView firsticon;
            TextView hint;
            ImageView icon;
            ImageView secondicon;
            TextView text;

            ViewHolder() {
            }
        }

        /* access modifiers changed from: package-private */
        public void DrawMultiAnswer(int position, ViewHolder holder2, View convertView) {
            boolean useransw;
            AnimationDrawableLetters Animation;
            int querypos = position / 4;
            int answerpos = position % 4;
            boolean isout = false;
            if (position > MovieTrivia.this.mpos + 3) {
                isout = true;
            }
            holder2.firsticon.setImageBitmap(null);
            holder2.icon.setImageBitmap(null);
            holder2.secondicon.setImageBitmap(null);
            holder2.firsticon.setBackgroundResource(0);
            holder2.secondicon.setBackgroundResource(0);
            holder2.text.setText("");
            holder2.hint.setText("");
            holder2.hint.setVisibility(8);
            convertView.setBackgroundDrawable(null);
            convertView.setMinimumHeight(0);
            if (!isout) {
                if (answerpos == 0) {
                    if (MovieTrivia.this.mLv.getWidth() > 240) {
                        convertView.setMinimumHeight(200);
                    }
                    if (MovieTrivia.this.mLv.getWidth() <= 240) {
                        holder2.icon.setMaxHeight(150);
                    }
                    convertView.setBackgroundDrawable(this.m_bmpFramePhoto);
                    holder2.text.setTextSize(14.0f);
                    holder2.text.setText("");
                    holder2.icon.setImageResource(MovieTrivia.ITEMS[querypos].mImage);
                    if (MovieTrivia.ITEMS[querypos].mHint != null) {
                        if (MovieTrivia.this.mLv.getWidth() > 240) {
                            holder2.hint.setTextSize(30.0f);
                        } else {
                            holder2.hint.setTextSize(20.0f);
                        }
                        holder2.hint.setText(MovieTrivia.ITEMS[querypos].mHint);
                        holder2.hint.setBackgroundColor(-16777216);
                        holder2.hint.setVisibility(4);
                        if (!MovieTrivia.this.mBlinkCorrect) {
                            convertView.post(new AlphaAnim(holder2.hint).mStarter);
                            return;
                        }
                        return;
                    }
                    return;
                }
                holder2.firsticon.setVisibility(0);
                holder2.text.setVisibility(0);
                holder2.hint.setAnimation(null);
                holder2.hint.setVisibility(8);
                if (!isout) {
                    if (answerpos == 1) {
                        Animation = new AnimationDrawableLetters(0);
                    } else if (answerpos == 2) {
                        Animation = new AnimationDrawableLetters(1);
                    } else {
                        Animation = new AnimationDrawableLetters(2);
                    }
                    holder2.firsticon.setImageDrawable(Animation);
                    convertView.post(Animation.mStarter);
                    if (MovieTrivia.this.mMultiplayer) {
                        holder2.secondicon.setImageDrawable(Animation);
                        holder2.secondicon.post(Animation.mStarter);
                    }
                }
                convertView.setMinimumHeight(0);
                if (MovieTrivia.ITEMS[querypos].Childs[answerpos - 1].answer == 1) {
                    useransw = true;
                } else {
                    useransw = false;
                }
                int userIntAnsw = MovieTrivia.ITEMS[querypos].Childs[answerpos - 1].answer;
                boolean expectedansw = MovieTrivia.ITEMS[querypos].Childs[answerpos - 1].isTrue;
                holder2.text.setText(MovieTrivia.ITEMS[querypos].Childs[answerpos - 1].mString);
                holder2.text.setTextSize(30.0f);
                if (MovieTrivia.this.mBlinkCorrect && MovieTrivia.ITEMS[querypos].Childs[answerpos - 1].isTrue) {
                    AnimationBlinkingAnswer anim = new AnimationBlinkingAnswer();
                    convertView.setBackgroundDrawable(anim);
                    convertView.post(anim.mStarter);
                } else if (userIntAnsw == -1) {
                    convertView.setBackgroundDrawable(this.m_bmpFrame2P);
                } else if (!MovieTrivia.this.mMultiplayer) {
                    if (useransw == expectedansw) {
                        convertView.setBackgroundDrawable(this.m_bmpFrameGREEN);
                    } else {
                        convertView.setBackgroundDrawable(this.m_bmpFrameRED);
                    }
                } else if (MovieTrivia.ITEMS[querypos].Childs[answerpos - 1].player == 2) {
                    convertView.setBackgroundDrawable(this.m_bmpFrameSelectRight);
                } else {
                    convertView.setBackgroundDrawable(this.m_bmpFrameSelectLeft);
                }
            }
        }

        class AlphaAnim {
            Animation animation = new AlphaAnimation(0.0f, 1.0f);
            public AlphaStarter mStarter;
            View mview;

            public AlphaAnim(View vw) {
                this.animation.setDuration(4000);
                this.animation.setFillEnabled(true);
                this.animation.setFillBefore(false);
                this.animation.setFillAfter(true);
                this.mStarter = new AlphaStarter();
                this.mStarter.mAnimation = this.animation;
                this.mview = vw;
            }

            class AlphaStarter implements Runnable {
                public Animation mAnimation;

                AlphaStarter() {
                }

                public void run() {
                    if (AlphaAnim.this.mview != null) {
                        AlphaAnim.this.mview.startAnimation(this.mAnimation);
                    }
                }
            }
        }

        class AnimationDrawableLetters extends AnimationDrawable {
            public Starter mStarter;

            AnimationDrawableLetters(int type) {
                if (type == 0) {
                    addFrame(EfficientAdapter.this.mAimg, 500);
                    addFrame(EfficientAdapter.this.mA1img, 200);
                } else if (type == 1) {
                    addFrame(EfficientAdapter.this.mBimg, 500);
                    addFrame(EfficientAdapter.this.mB1img, 200);
                } else if (type == 2) {
                    addFrame(EfficientAdapter.this.mCimg, 500);
                    addFrame(EfficientAdapter.this.mC1img, 200);
                }
                setOneShot(false);
                setVisible(true, true);
                this.mStarter = new Starter();
                this.mStarter.mAnimation = this;
            }
        }

        class AnimationBlinkingAnswer extends AnimationDrawable {
            public Starter mStarter;

            AnimationBlinkingAnswer() {
                addFrame(MovieTrivia.this.getResources().getDrawable(R.drawable.calcframeblue), 200);
                addFrame(MovieTrivia.this.getResources().getDrawable(R.drawable.calcframegreen), 200);
                addFrame(MovieTrivia.this.getResources().getDrawable(R.drawable.calcframeblue), 200);
                addFrame(MovieTrivia.this.getResources().getDrawable(R.drawable.calcframegreen), 200);
                setOneShot(true);
                setVisible(true, true);
                this.mStarter = new Starter();
                this.mStarter.mAnimation = this;
            }
        }

        class Starter implements Runnable {
            public AnimationDrawable mAnimation;

            Starter() {
            }

            public void run() {
                if (this.mAnimation != null) {
                    this.mAnimation.start();
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void DrawSingleAnswer(int position, ViewHolder holder2, View convertView) {
            boolean useransw;
            holder2.text.setText(MovieTrivia.ITEMS[position].mString);
            if (MovieTrivia.ITEMS[position].answer == 0) {
                useransw = false;
            } else {
                useransw = true;
            }
            if (MovieTrivia.ITEMS[position].answer == -1) {
                if (!MovieTrivia.this.mTimeattack || position != MovieTrivia.this.mpos) {
                    holder2.icon.setImageBitmap(this.m_bmpquest);
                } else if (position == MovieTrivia.this.mpos) {
                    if (MovieTrivia.this.mlastClick - MovieTrivia.this.mTimeRemaining <= 1000) {
                        holder2.icon.setImageBitmap(this.m_bmpthree);
                    } else if (MovieTrivia.this.mlastClick - MovieTrivia.this.mTimeRemaining <= 2000) {
                        holder2.icon.setImageBitmap(this.m_bmptwo);
                    } else if (MovieTrivia.this.mlastClick - MovieTrivia.this.mTimeRemaining <= 3000) {
                        holder2.icon.setImageBitmap(this.m_bmpone);
                    }
                }
            } else if (useransw == MovieTrivia.ITEMS[position].isTrue) {
                holder2.icon.setImageBitmap(this.m_bmptrue);
            } else {
                holder2.icon.setImageBitmap(this.m_bmpfalse);
            }
            if (!MovieTrivia.this.mTimeattack) {
                if (position != MovieTrivia.this.mpos - 1 || MovieTrivia.ITEMS[position].answer == -1) {
                    holder2.icon.clearAnimation();
                    holder2.text.clearAnimation();
                } else {
                    holder2.icon.startAnimation(this.mHyperspaceJump);
                    holder2.text.startAnimation(this.mTextAnimation);
                }
            }
            if (MovieTrivia.this.mTimeattack) {
                if (position == MovieTrivia.this.mpos && MovieTrivia.ITEMS[position].answer == -1) {
                    holder2.icon.startAnimation(this.mHyperspaceJump);
                } else {
                    holder2.icon.clearAnimation();
                }
            }
            if (MovieTrivia.this.mMultiplayer) {
                if (MovieTrivia.ITEMS[position].answer != -1 && MovieTrivia.ITEMS[position].player == 1) {
                    convertView.setBackgroundDrawable(this.m_bmpFrame1P);
                } else if (MovieTrivia.ITEMS[position].answer == -1 || MovieTrivia.ITEMS[position].player != 2) {
                    convertView.setBackgroundDrawable(this.m_bmpFrame);
                } else {
                    convertView.setBackgroundDrawable(this.m_bmpFrame2P);
                }
            } else if (MovieTrivia.ITEMS[position].answer == -1) {
                convertView.setBackgroundDrawable(this.m_bmpFrame);
            } else if (useransw == MovieTrivia.ITEMS[position].isTrue) {
                convertView.setBackgroundDrawable(this.m_bmpFrameGREEN);
            } else {
                convertView.setBackgroundDrawable(this.m_bmpFrameRED);
            }
        }
    }
}
