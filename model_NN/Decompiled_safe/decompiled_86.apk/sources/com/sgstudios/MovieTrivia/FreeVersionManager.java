package com.sgstudios.MovieTrivia;

import android.app.Activity;
import android.content.Context;

public class FreeVersionManager {
    static final boolean FREEVERSION = false;
    static int mGamecounter = 0;
    final int MAXFREEGAMES = 2;
    private Context mContext;

    static void Reset() {
        mGamecounter = 0;
    }

    public FreeVersionManager(Context ctx) {
        this.mContext = ctx;
    }

    /* access modifiers changed from: package-private */
    public boolean Test(boolean destroy) {
        if (mGamecounter <= 1) {
            return false;
        }
        if (destroy) {
            ShowAllertAndTerminate(true);
        } else {
            ShowAllert(true);
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean CountAndTest(boolean destroy) {
        int i = mGamecounter;
        mGamecounter = i + 1;
        if (i <= 1) {
            return false;
        }
        if (destroy) {
            ShowAllertAndTerminate(true);
        } else {
            ShowAllert(true);
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void ShowAllert(boolean showmarquee) {
        FreeVersionDialog dialog = new FreeVersionDialog(this.mContext);
        dialog.requestWindowFeature(1);
        if (showmarquee) {
            dialog.mShowaddTxt = true;
        }
        dialog.show();
    }

    private void ShowAllertAndTerminate(boolean showmarquee) {
        FreeVersionDialog dialog = new FreeVersionDialog(this.mContext);
        dialog.requestWindowFeature(1);
        dialog.mParent = (Activity) this.mContext;
        dialog.mTerminate = true;
        if (showmarquee) {
            dialog.mShowaddTxt = true;
        }
        dialog.show();
    }
}
