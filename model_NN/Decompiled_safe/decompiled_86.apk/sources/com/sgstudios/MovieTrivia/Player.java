package com.sgstudios.MovieTrivia;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;
import java.io.IOException;

public class Player {
    public MediaPlayer mMediaPlayer;

    public Player(Context ctx, int res) {
        this.mMediaPlayer = MediaPlayer.create(ctx, res);
        if (this.mMediaPlayer != null) {
            this.mMediaPlayer.setVolume(6.0f, 6.0f);
        } else {
            this.mMediaPlayer = null;
        }
    }

    public void InfiniteLoop() {
        this.mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                mp.start();
            }
        });
    }

    public Player() {
        this.mMediaPlayer = new MediaPlayer();
        if (this.mMediaPlayer != null) {
            this.mMediaPlayer.setVolume(6.0f, 6.0f);
        }
    }

    /* access modifiers changed from: package-private */
    public void Silent(boolean silent) {
        if (this.mMediaPlayer != null) {
            if (silent) {
                try {
                    this.mMediaPlayer.setVolume(0.0f, 0.0f);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                this.mMediaPlayer.setVolume(6.0f, 6.0f);
            }
        }
    }

    public void RePlay() {
        try {
            if (this.mMediaPlayer != null) {
                this.mMediaPlayer.prepare();
                this.mMediaPlayer.start();
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    public void Pause() {
        if (this.mMediaPlayer != null && this.mMediaPlayer.isPlaying()) {
            this.mMediaPlayer.pause();
        }
    }

    public void Play() {
        try {
            if (this.mMediaPlayer != null) {
                this.mMediaPlayer.start();
            }
        } catch (Exception e) {
            Exception e2 = e;
            Log.e("TAG", "error: " + e2.getMessage(), e2);
        }
    }

    public void Load(Context context, Uri uri) throws IllegalArgumentException, SecurityException, IllegalStateException, IOException {
        try {
            if (this.mMediaPlayer != null) {
                this.mMediaPlayer.reset();
                this.mMediaPlayer.setDataSource(context, uri);
                this.mMediaPlayer.prepare();
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    public void Stop() {
        try {
            if (this.mMediaPlayer != null && this.mMediaPlayer.isPlaying()) {
                this.mMediaPlayer.stop();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Release() {
        if (this.mMediaPlayer != null) {
            try {
                this.mMediaPlayer.release();
                this.mMediaPlayer = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
