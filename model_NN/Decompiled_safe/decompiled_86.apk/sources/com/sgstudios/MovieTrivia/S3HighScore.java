package com.sgstudios.MovieTrivia;

import android.app.Activity;
import android.content.SharedPreferences;

public class S3HighScore {
    public static final String[] FeintLeaderboards = {"681936", "681936", "681936"};
    public static final String PREFS_NAME = "ScoreFile";
    SharedPreferences.Editor editor = this.settings.edit();
    S3Openfeint mFeint = new S3Openfeint(this.m_Activity);
    public int mScore;
    private Activity m_Activity;
    SharedPreferences settings = this.m_Activity.getSharedPreferences(PREFS_NAME, 0);

    public S3HighScore(Activity activity) {
        this.m_Activity = activity;
    }

    public void SetScore(int level, int score) {
        GetScore(level);
        if (this.mFeint != null) {
            this.mFeint.SaveScore((long) (score < this.mScore ? this.mScore : score), FeintLeaderboards[level]);
        }
        if (score > this.mScore) {
            this.editor.putInt(Integer.toString(level), score);
            this.editor.commit();
        }
    }

    public void GetScore(int level) {
        this.mScore = this.settings.getInt(Integer.toString(level), 0);
    }
}
