package com.sgstudios.MovieTrivia;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class FreeVersionDialog extends Dialog implements View.OnClickListener {
    TextView mMarquee;
    Activity mParent;
    boolean mShowaddTxt = false;
    boolean mTerminate = false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.freeversion_dialog);
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/term_cyr.ttf");
        ((ImageButton) findViewById(R.id.trygeo)).setOnClickListener(this);
        ((ImageButton) findViewById(R.id.Ok)).setOnClickListener(this);
        TextView txtv = (TextView) findViewById(R.id.TextView01);
        txtv.setTypeface(tf);
        txtv.setTextSize(10.0f);
    }

    public FreeVersionDialog(Context c) {
        super(c);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.trygeo /*2131361799*/:
                ((ImageView) findViewById(R.id.ImageView04)).requestFocus();
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setData(Uri.parse("market://details?id=com.sgstudios.GeoTriviaPro"));
                getContext().startActivity(intent);
                dismiss();
                return;
            case R.id.ImageView02 /*2131361800*/:
            default:
                return;
            case R.id.Ok /*2131361801*/:
                if (!this.mTerminate) {
                    dismiss();
                    return;
                } else {
                    dismiss();
                    return;
                }
        }
    }

    public void onBackPressed() {
        if (!this.mTerminate) {
            dismiss();
            return;
        }
        this.mParent.finish();
        dismiss();
    }
}
