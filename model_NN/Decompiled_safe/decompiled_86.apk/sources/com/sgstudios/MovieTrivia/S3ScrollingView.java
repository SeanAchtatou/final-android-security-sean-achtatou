package com.sgstudios.MovieTrivia;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Shader;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ListView;
import com.sgstudios.MovieTrivia.S3ScoreSprite;
import java.util.ArrayList;
import java.util.Random;

public class S3ScrollingView extends ListView {
    final float MAXVEL;
    final float MAXZOOM;
    final float MINVEL;
    final float MINZOOM;
    final int TOUCHTIMELAG;
    int[] bgs;
    MovieTrivia mActivity;
    private Paint mBgPaint;
    private Shader mBgShader;
    Context mContext;
    boolean mCustomDraw;
    int mLastAction;
    long mLastTick;
    int mLastTouchX;
    int mLastTouchY;
    Matrix mMatrix;
    private Matrix mMatrix0;
    private Matrix mMatrix1;
    private Matrix mMatrix2;
    Paint mPaint;
    private Paint mParallax1Paint;
    private Paint mParallax2Paint;
    private Shader mPx1Shader;
    private Shader mPx2Shader;
    Shader mShader;
    ArrayList mSpritesScoreList;
    boolean mStopped;
    float mXtVel;
    boolean mXvelIncrease;
    float mYtVel;
    boolean mYvelIncrease;
    boolean mZoomIncrease;
    float mZoomVel;
    Bitmap m_Bg;
    Canvas m_Cavas;
    Random m_randomGenerator;
    private Bitmap mbg;
    long mlastTouchTick;
    private Bitmap mpx1;
    private Bitmap mpx2;
    boolean mscrollenabled;
    boolean mtouchenabled;

    /* access modifiers changed from: package-private */
    public void AddScore(int score, S3ScoreSprite.BONUSTYPE btype) {
        this.mSpritesScoreList.add(new S3ScoreSprite(getContext(), this.mLastTouchX, this.mLastTouchY, score, btype));
    }

    /* access modifiers changed from: package-private */
    public void DrawScores(Canvas canvas) {
        for (int i = 0; i < this.mSpritesScoreList.size(); i++) {
            S3ScoreSprite tmp = (S3ScoreSprite) this.mSpritesScoreList.get(i);
            tmp.Draw(canvas);
            if (tmp.mTimetolife == 0) {
                this.mSpritesScoreList.remove(i);
            }
        }
    }

    private void init() {
        this.m_randomGenerator = new Random();
        SetRandomBG();
        this.mShader = new BitmapShader(this.m_Bg, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        this.mMatrix = new Matrix();
        this.mPaint = new Paint();
        this.mPaint.setShader(this.mShader);
    }

    public void SetRandomBG() {
        this.m_Bg = BitmapFactory.decodeResource(getResources(), this.bgs[this.m_randomGenerator.nextInt(this.bgs.length)]);
        this.mShader = new BitmapShader(this.m_Bg, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        if (this.mPaint == null) {
            this.mPaint = new Paint();
        }
        this.mPaint.setShader(this.mShader);
    }

    public void SetBG(int bgresource) {
        this.m_Bg = BitmapFactory.decodeResource(getResources(), bgresource);
        this.mShader = new BitmapShader(this.m_Bg, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        if (this.mPaint == null) {
            this.mPaint = new Paint();
        }
        this.mPaint.setShader(this.mShader);
    }

    public S3ScrollingView(Context context) {
        super(context);
        this.mXtVel = 0.0f;
        this.mYtVel = 0.0f;
        this.mZoomVel = 1.0f;
        this.MAXZOOM = 1.003f;
        this.MINZOOM = 0.997f;
        this.MAXVEL = 5.5f;
        this.MINVEL = -5.5f;
        this.mXvelIncrease = false;
        this.mYvelIncrease = false;
        this.mZoomIncrease = true;
        this.bgs = new int[]{R.drawable.bg256q, R.drawable.bg256s};
        this.mtouchenabled = false;
        this.mscrollenabled = true;
        this.mStopped = false;
        this.mCustomDraw = false;
        this.mLastAction = 0;
        this.mlastTouchTick = 0;
        this.TOUCHTIMELAG = 400;
        this.mLastTouchX = -1;
        this.mLastTouchY = -1;
        this.mSpritesScoreList = new ArrayList(5);
        this.m_randomGenerator = new Random();
        this.mContext = context;
        SetRandomBG();
        this.mShader = new BitmapShader(this.m_Bg, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        this.mMatrix = new Matrix();
        this.mPaint = new Paint();
        this.mPaint.setShader(this.mShader);
    }

    public S3ScrollingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mXtVel = 0.0f;
        this.mYtVel = 0.0f;
        this.mZoomVel = 1.0f;
        this.MAXZOOM = 1.003f;
        this.MINZOOM = 0.997f;
        this.MAXVEL = 5.5f;
        this.MINVEL = -5.5f;
        this.mXvelIncrease = false;
        this.mYvelIncrease = false;
        this.mZoomIncrease = true;
        this.bgs = new int[]{R.drawable.bg256q, R.drawable.bg256s};
        this.mtouchenabled = false;
        this.mscrollenabled = true;
        this.mStopped = false;
        this.mCustomDraw = false;
        this.mLastAction = 0;
        this.mlastTouchTick = 0;
        this.TOUCHTIMELAG = 400;
        this.mLastTouchX = -1;
        this.mLastTouchY = -1;
        this.mSpritesScoreList = new ArrayList(5);
        this.mContext = context;
        init();
    }

    public S3ScrollingView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mXtVel = 0.0f;
        this.mYtVel = 0.0f;
        this.mZoomVel = 1.0f;
        this.MAXZOOM = 1.003f;
        this.MINZOOM = 0.997f;
        this.MAXVEL = 5.5f;
        this.MINVEL = -5.5f;
        this.mXvelIncrease = false;
        this.mYvelIncrease = false;
        this.mZoomIncrease = true;
        this.bgs = new int[]{R.drawable.bg256q, R.drawable.bg256s};
        this.mtouchenabled = false;
        this.mscrollenabled = true;
        this.mStopped = false;
        this.mCustomDraw = false;
        this.mLastAction = 0;
        this.mlastTouchTick = 0;
        this.TOUCHTIMELAG = 400;
        this.mLastTouchX = -1;
        this.mLastTouchY = -1;
        this.mSpritesScoreList = new ArrayList(5);
        this.mContext = context;
        init();
    }

    public void onDraw(Canvas canvas) {
        if (!this.mStopped) {
            if (this.mCustomDraw) {
                CustomDraw(canvas);
                invalidate();
                return;
            }
            UpdateVelocity(SystemClock.uptimeMillis());
            canvas.drawPaint(this.mPaint);
            DrawScores(canvas);
            invalidate();
        }
    }

    public void CustomDraw(Canvas canvas) {
        if (!this.mStopped) {
            if (this.mBgPaint == null) {
                InitCustomDraw();
            }
            this.mMatrix0.postTranslate(-3.5f, 0.0f);
            this.mBgShader.setLocalMatrix(this.mMatrix0);
            canvas.drawPaint(this.mBgPaint);
            this.mMatrix1.postTranslate(0.5f, 1.0f);
            this.mPx1Shader.setLocalMatrix(this.mMatrix1);
            canvas.drawPaint(this.mParallax1Paint);
            this.mMatrix2.postTranslate(-6.0f, 0.0f);
            this.mPx2Shader.setLocalMatrix(this.mMatrix2);
            canvas.drawPaint(this.mParallax2Paint);
        }
    }

    public void InitCustomDraw() {
        this.mbg = BitmapFactory.decodeResource(getResources(), R.drawable.bg0);
        this.mBgShader = new BitmapShader(this.mbg, Shader.TileMode.REPEAT, Shader.TileMode.MIRROR);
        this.mBgPaint = new Paint();
        this.mBgPaint.setShader(this.mBgShader);
        this.mpx1 = BitmapFactory.decodeResource(getResources(), R.drawable.bgoscar);
        this.mPx1Shader = new BitmapShader(this.mpx1, Shader.TileMode.REPEAT, Shader.TileMode.MIRROR);
        this.mParallax1Paint = new Paint();
        this.mParallax1Paint.setShader(this.mPx1Shader);
        this.mpx2 = BitmapFactory.decodeResource(getResources(), R.drawable.bg2);
        this.mPx2Shader = new BitmapShader(this.mpx2, Shader.TileMode.REPEAT, Shader.TileMode.MIRROR);
        this.mParallax2Paint = new Paint();
        this.mParallax2Paint.setShader(this.mPx2Shader);
        this.mMatrix0 = new Matrix();
        this.mMatrix1 = new Matrix();
        this.mMatrix2 = new Matrix();
    }

    public void ReleaseCustomDraw() {
        if (this.mbg != null) {
            this.mbg.recycle();
            this.mbg = null;
            this.mBgShader = null;
            this.mBgPaint = null;
        }
        if (this.mpx1 != null) {
            this.mpx1.recycle();
            this.mpx1 = null;
            this.mPx1Shader = null;
            this.mParallax1Paint = null;
        }
        if (this.mpx2 != null) {
            this.mpx2.recycle();
            this.mpx2 = null;
            this.mPx2Shader = null;
            this.mParallax2Paint = null;
        }
    }

    public void TouchEnabled(boolean value) {
        this.mtouchenabled = value;
    }

    public void ScrollEnabled(boolean value) {
        this.mscrollenabled = value;
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (this.mtouchenabled) {
            if (this.mscrollenabled) {
                super.onTouchEvent(event);
            } else {
                long now = SystemClock.uptimeMillis();
                if (now - this.mlastTouchTick < 400) {
                    return true;
                }
                if (event.getAction() == 0) {
                    this.mlastTouchTick = now;
                }
                this.mLastTouchX = (int) event.getX();
                this.mLastTouchY = (int) event.getY();
                if (event.getAction() == 0) {
                    this.mActivity.Touch(false, pointToPosition(this.mLastTouchX, this.mLastTouchY));
                }
                if (event.getAction() == 2) {
                    return true;
                }
                super.onTouchEvent(event);
            }
        }
        return true;
    }

    public void UpdateVelocity(long tick) {
        float velmultfactor;
        if (this.mXtVel > 5.5f) {
            this.mXvelIncrease = false;
        } else if (this.mXtVel < -5.5f) {
            this.mXvelIncrease = true;
        }
        if (this.mYtVel > 5.5f) {
            this.mYvelIncrease = false;
        } else if (this.mYtVel < -5.5f) {
            this.mYvelIncrease = true;
        }
        if (this.mZoomVel > 1.003f) {
            this.mZoomIncrease = false;
        } else if (this.mZoomVel < 0.997f) {
            this.mZoomIncrease = true;
        }
        long elapsedtime = tick - this.mLastTick;
        if (((float) elapsedtime) == 0.0f || elapsedtime >= 200) {
            velmultfactor = 1.0f;
        } else {
            velmultfactor = ((float) elapsedtime) / 30.0f;
        }
        if (this.mXvelIncrease) {
            this.mXtVel += 0.01f * velmultfactor;
        } else {
            this.mXtVel -= 0.01f * velmultfactor;
        }
        if (this.mYvelIncrease) {
            this.mYtVel += 0.03f * velmultfactor;
        } else {
            this.mYtVel -= 0.03f * velmultfactor;
        }
        if (this.mZoomIncrease) {
            this.mZoomVel += 1.0E-5f * velmultfactor;
        } else {
            this.mZoomVel -= 1.0E-5f * velmultfactor;
        }
        this.mMatrix.postScale(this.mZoomVel, this.mZoomVel);
        this.mMatrix.postTranslate(this.mXtVel, this.mYtVel);
        this.mShader.setLocalMatrix(this.mMatrix);
        this.mLastTick = tick;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int ow, int oh) {
        super.onSizeChanged(w, h, ow, oh);
    }

    public void Release() {
        if (this.m_Bg != null) {
            this.m_Bg.recycle();
            this.m_Bg = null;
        } else if (this.mbg != null) {
            this.mbg.recycle();
            this.mbg = null;
        } else if (this.mpx1 != null) {
            this.mpx1.recycle();
            this.mpx1 = null;
        } else if (this.mpx2 != null) {
            this.mpx2.recycle();
            this.mpx2 = null;
        }
    }
}
