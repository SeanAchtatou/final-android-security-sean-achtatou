package com.sgstudios.MovieTrivia;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class splashScreen extends Activity {
    private final int SPLASH_DISPLAY_LENGTH = 13000;
    Animation mAnim1;
    Animation mAnim2;
    Animation mAnim3;
    /* access modifiers changed from: private */
    public ImageView mImg;
    /* access modifiers changed from: private */
    public ImageView mImg2;
    /* access modifiers changed from: private */
    public ImageView mImg3;
    PowerManager.WakeLock mWakeLock;
    Player m_playerMusic;

    public void onCreate(Bundle savedInstanceState) {
        this.mWakeLock = ((PowerManager) getSystemService("power")).newWakeLock(10, "tag");
        this.mWakeLock.acquire();
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.splash);
        this.mAnim1 = AnimationUtils.loadAnimation(getBaseContext(), R.anim.slowlogo0);
        this.mAnim2 = AnimationUtils.loadAnimation(getBaseContext(), R.anim.slowlogo1);
        this.mAnim3 = AnimationUtils.loadAnimation(getBaseContext(), R.anim.slowlogo3);
        this.mImg = (ImageView) findViewById(R.id.ImageView);
        this.mImg2 = (ImageView) findViewById(R.id.ImageViewbottom);
        this.mImg3 = (ImageView) findViewById(R.id.ImageViewTrailer);
        this.mImg.setImageResource(R.drawable.shlogo);
        this.mImg.startAnimation(this.mAnim1);
        Runnable runnable0 = new Runnable() {
            public void run() {
                splashScreen.this.mImg2.setImageResource(R.drawable.shlogo2);
                splashScreen.this.mImg2.startAnimation(splashScreen.this.mAnim2);
            }
        };
        Runnable runnable1 = new Runnable() {
            public void run() {
                splashScreen.this.mImg.setImageResource(R.drawable.featureoptions);
                splashScreen.this.mImg.startAnimation(splashScreen.this.mAnim1);
                splashScreen.this.mImg2.setImageResource(R.drawable.logo2);
                splashScreen.this.mImg2.startAnimation(splashScreen.this.mAnim2);
            }
        };
        Runnable runnable2 = new Runnable() {
            public void run() {
                splashScreen.this.mImg3.setImageResource(R.drawable.jumpingsheepsgame);
                splashScreen.this.mImg3.startAnimation(splashScreen.this.mAnim3);
            }
        };
        Runnable runnable3 = new Runnable() {
            public void run() {
                splashScreen.this.startActivity(new Intent(splashScreen.this, levelSelect.class));
                splashScreen.this.finish();
            }
        };
        new Handler().postDelayed(runnable0, 2000);
        new Handler().postDelayed(runnable1, 6500);
        new Handler().postDelayed(runnable2, 7500);
        new Handler().postDelayed(runnable3, 13000);
        this.m_playerMusic = new Player(this, R.raw.intro);
        this.m_playerMusic.mMediaPlayer.setVolume(0.6f, 0.6f);
        this.m_playerMusic.Play();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.m_playerMusic.Release();
        this.mWakeLock.release();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mWakeLock.acquire();
        this.m_playerMusic.Play();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.mWakeLock.release();
        this.m_playerMusic.Pause();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.m_playerMusic.Pause();
    }
}
