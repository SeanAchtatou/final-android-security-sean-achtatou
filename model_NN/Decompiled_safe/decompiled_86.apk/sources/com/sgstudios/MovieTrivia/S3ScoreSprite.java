package com.sgstudios.MovieTrivia;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;

public class S3ScoreSprite {
    static Bitmap mBmp10Row;
    static Bitmap mBmp3Row;
    static Bitmap mBmp5Row;
    public int mAlpha;
    int mBmpHeight;
    int mBmpWidth;
    Rect mBounds;
    BONUSTYPE mBtype;
    Context mContext;
    Paint mPaint;
    public int mPosX;
    public int mPosY;
    public int mScore;
    public int mTimetolife;

    public enum BONUSTYPE {
        NONE,
        BONUS3INROW,
        BONUS5INROW,
        BONUS10INROW
    }

    S3ScoreSprite(Context context, int x, int y, int score, BONUSTYPE btype) {
        if (mBmp5Row == null || mBmp10Row == null) {
            mBmp3Row = BitmapFactory.decodeResource(context.getResources(), R.drawable.bonus3inrow);
            mBmp5Row = BitmapFactory.decodeResource(context.getResources(), R.drawable.bonus5inrow);
            mBmp10Row = BitmapFactory.decodeResource(context.getResources(), R.drawable.bonus10inrow);
        }
        this.mBtype = btype;
        this.mContext = context;
        this.mTimetolife = 250;
        this.mPosX = x;
        this.mPosY = y;
        this.mAlpha = 255;
        this.mScore = score;
        this.mPaint = new Paint();
        this.mPaint.setTextSize(30.0f);
        this.mPaint.setTypeface(Typeface.createFromAsset(this.mContext.getAssets(), "fonts/term_cyr.ttf"));
        this.mBounds = new Rect();
    }

    public void Draw(Canvas canvas) {
        String txscore = "";
        if (this.mScore > 0) {
            this.mPaint.setColor(65280);
            txscore = "+" + this.mScore;
        } else if (this.mScore < 0) {
            this.mPaint.setColor(16719904);
            txscore = new StringBuilder().append(this.mScore).toString();
        }
        this.mPaint.getTextBounds(txscore, 0, txscore.length(), this.mBounds);
        if (this.mPosX > canvas.getWidth() - this.mBounds.width()) {
            this.mPosX = canvas.getWidth() - this.mBounds.width();
        }
        this.mPaint.setAlpha(this.mAlpha);
        canvas.drawText(txscore, (float) this.mPosX, (float) this.mPosY, this.mPaint);
        if (this.mBtype != BONUSTYPE.NONE) {
            int leftposbmp = (this.mPosX + (this.mBounds.width() / 2)) - (mBmp5Row.getWidth() / 2);
            if (this.mBtype == BONUSTYPE.BONUS3INROW) {
                canvas.drawBitmap(mBmp3Row, (float) leftposbmp, (float) this.mPosY, this.mPaint);
            } else if (this.mBtype == BONUSTYPE.BONUS5INROW) {
                canvas.drawBitmap(mBmp5Row, (float) leftposbmp, (float) this.mPosY, this.mPaint);
            } else if (this.mBtype == BONUSTYPE.BONUS10INROW) {
                canvas.drawBitmap(mBmp10Row, (float) leftposbmp, (float) this.mPosY, this.mPaint);
            }
        }
        this.mPosY--;
        this.mAlpha -= 5;
        this.mTimetolife -= 5;
    }
}
