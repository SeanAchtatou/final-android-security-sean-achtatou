package com.sgstudios.MovieTrivia;

public final class R {

    public static final class anim {
        public static final int airplane = 2130968576;
        public static final int airplane2 = 2130968577;
        public static final int aletteranim = 2130968578;
        public static final int alphanimation = 2130968579;
        public static final int correctblink = 2130968580;
        public static final int iconanimation = 2130968581;
        public static final int oscarfadeout = 2130968582;
        public static final int slowlogo0 = 2130968583;
        public static final int slowlogo1 = 2130968584;
        public static final int slowlogo3 = 2130968585;
        public static final int textanimation = 2130968586;
    }

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771971;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771972;
        public static final int secondaryTextColor = 2130771970;
    }

    public static final class color {
        public static final int of_transparent = 2131099648;
    }

    public static final class drawable {
        public static final int a = 2130837504;
        public static final int a1 = 2130837505;
        public static final int actor_aaroneckhart = 2130837506;
        public static final int actor_adrienbrody = 2130837507;
        public static final int actor_albertosordi = 2130837508;
        public static final int actor_alpacinoscarface = 2130837509;
        public static final int actor_angelinajoliemrmrssmith = 2130837510;
        public static final int actor_annehathawayloveandotherdrugs = 2130837511;
        public static final int actor_antoniobanderasspykidstwo = 2130837512;
        public static final int actor_arnoldschwarzeneggerterminator = 2130837513;
        public static final int actor_audreyhepburnmyfairlady = 2130837514;
        public static final int actor_benaffleckstateofplay = 2130837515;
        public static final int actor_benstillernightatthemuseum = 2130837516;
        public static final int actor_bobhoskins = 2130837517;
        public static final int actor_bradpittinglouriousbasterds = 2130837518;
        public static final int actor_brendanfraser = 2130837519;
        public static final int actor_brigittebardot = 2130837520;
        public static final int actor_brucewillisunbreakable = 2130837521;
        public static final int actor_camerondiaztheholiday = 2130837522;
        public static final int actor_catherinezetajonesnoreservations = 2130837523;
        public static final int actor_charlizetheron = 2130837524;
        public static final int actor_christopherlloyd = 2130837525;
        public static final int actor_clinteastwoodforafistfulofdollars = 2130837526;
        public static final int actor_cliveowenduplicity = 2130837527;
        public static final int actor_colinfarrellmiamivice = 2130837528;
        public static final int actor_colinfirth = 2130837529;
        public static final int actor_denzelwashingtonbookofeli = 2130837530;
        public static final int actor_dustinhoffmanlittlefockers = 2130837531;
        public static final int actor_eddiemurphybeverlyhillscop = 2130837532;
        public static final int actor_elizabethtaylorcleopatra = 2130837533;
        public static final int actor_evamendes = 2130837534;
        public static final int actor_ewanmcgregortrainspotting = 2130837535;
        public static final int actor_georgeclooneyupintheair = 2130837536;
        public static final int actor_gerarddepardieu = 2130837537;
        public static final int actor_harrisonfordstarwars = 2130837538;
        public static final int actor_heathledgerthepatriot = 2130837539;
        public static final int actor_jackblackgulliverstravels = 2130837540;
        public static final int actor_jacknicholsonaboutschmidt = 2130837541;
        public static final int actor_jakegyllenhaalprinceofpersia = 2130837542;
        public static final int actor_jamesdeanrebelwithoutacause = 2130837543;
        public static final int actor_jamesfranco = 2130837544;
        public static final int actor_jamesmarsdensupermanreturns = 2130837545;
        public static final int actor_jeanreno = 2130837546;
        public static final int actor_jenniferanistonlovehappens = 2130837547;
        public static final int actor_jessicaalba = 2130837548;
        public static final int actor_jimcarreymemyselfandirene = 2130837549;
        public static final int actor_joaquinphoenix = 2130837550;
        public static final int actor_jodiefosterthesilenceofthelambs = 2130837551;
        public static final int actor_johnnydepp = 2130837552;
        public static final int actor_johntravoltafrompariswithlove = 2130837553;
        public static final int actor_johnwaynestagecoach = 2130837554;
        public static final int actor_judelawalfie = 2130837555;
        public static final int actor_juliarobertseatpraylove = 2130837556;
        public static final int actor_katewinslettitanic = 2130837557;
        public static final int actor_keanureeveshenryscrime = 2130837558;
        public static final int actor_keiraknightley = 2130837559;
        public static final int actor_kirstendunst = 2130837560;
        public static final int actor_kurtrusselthething = 2130837561;
        public static final int actor_leonardodicaprioinception = 2130837562;
        public static final int actor_livtyler = 2130837563;
        public static final int actor_marilynmonroesomelikeithot = 2130837564;
        public static final int actor_markwahlberg = 2130837565;
        public static final int actor_marlonbrandothegodfather = 2130837566;
        public static final int actor_marylstreepthedevilwearsprada = 2130837567;
        public static final int actor_mattdamongreenzone = 2130837568;
        public static final int actor_mattdillon = 2130837569;
        public static final int actor_melgibsonbraveheart = 2130837570;
        public static final int actor_michaeldouglaswallstreet = 2130837571;
        public static final int actor_michaeljfox = 2130837572;
        public static final int actor_michellepfeifferdangerousliaisons = 2130837573;
        public static final int actor_monicabellucci = 2130837574;
        public static final int actor_morganfreemaninvictus = 2130837575;
        public static final int actor_natalieportmanloveandotherimpossiblepursuits = 2130837576;
        public static final int actor_nicolekidnamnine = 2130837577;
        public static final int actor_orlandobloom = 2130837578;
        public static final int actor_owenwilson = 2130837579;
        public static final int actor_patrickswayzepointbreak = 2130837580;
        public static final int actor_paulnewmanthelonghotsummer = 2130837581;
        public static final int actor_rachelweisz = 2130837582;
        public static final int actor_reesewitherspoon = 2130837583;
        public static final int actor_reneezellweger = 2130837584;
        public static final int actor_richardgereamelia = 2130837585;
        public static final int actor_robertdeniroronin = 2130837586;
        public static final int actor_robertdowneyjrduedate = 2130837587;
        public static final int actor_robertobenigni = 2130837588;
        public static final int actor_robinwilliamshook = 2130837589;
        public static final int actor_russellcrowe = 2130837590;
        public static final int actor_samuelljacksonironmantwo = 2130837591;
        public static final int actor_sandrabullocktheproposal = 2130837592;
        public static final int actor_scarletjohanssonironmantwo = 2130837593;
        public static final int actor_seanconneryindianajonesandthelastcrusade = 2130837594;
        public static final int actor_sophialoren = 2130837595;
        public static final int actor_sylvesterstallonerambo = 2130837596;
        public static final int actor_tobeymaguire = 2130837597;
        public static final int actor_tomcruiseknightandday = 2130837598;
        public static final int actor_tomhanksangelsanddemons = 2130837599;
        public static final int actor_umathurmantheaccidentalhusband = 2130837600;
        public static final int actor_valkilmer = 2130837601;
        public static final int actor_vincentcassel = 2130837602;
        public static final int actor_whoopigoldbergjumpinjackflash = 2130837603;
        public static final int actor_willsmithindependenceday = 2130837604;
        public static final int actor_zacefron = 2130837605;
        public static final int actornames = 2130837606;
        public static final int actornames1 = 2130837607;
        public static final int actornames2 = 2130837608;
        public static final int b = 2130837609;
        public static final int b1 = 2130837610;
        public static final int bg0 = 2130837611;
        public static final int bg1 = 2130837612;
        public static final int bg2 = 2130837613;
        public static final int bg256q = 2130837614;
        public static final int bg256s = 2130837615;
        public static final int bg3 = 2130837616;
        public static final int bg4 = 2130837617;
        public static final int bg5 = 2130837618;
        public static final int bgitem = 2130837619;
        public static final int bgoscar = 2130837620;
        public static final int bonus10inrow = 2130837621;
        public static final int bonus3inrow = 2130837622;
        public static final int bonus5inrow = 2130837623;
        public static final int c = 2130837624;
        public static final int c1 = 2130837625;
        public static final int calcframe = 2130837626;
        public static final int calcframeblue = 2130837627;
        public static final int calcframeblueleftyellow = 2130837628;
        public static final int calcframebluerightyellow = 2130837629;
        public static final int calcframegreen = 2130837630;
        public static final int calcframered = 2130837631;
        public static final int calcframeyellow = 2130837632;
        public static final int cd0 = 2130837633;
        public static final int cd1 = 2130837634;
        public static final int cd2 = 2130837635;
        public static final int cd3 = 2130837636;
        public static final int charnames = 2130837637;
        public static final int charnames1 = 2130837638;
        public static final int charnames2 = 2130837639;
        public static final int continu = 2130837640;
        public static final int continudisabled = 2130837641;
        public static final int continuselected = 2130837642;
        public static final int exit = 2130837643;
        public static final int exitdisabled = 2130837644;
        public static final int exitselected = 2130837645;
        public static final int falsebig = 2130837646;
        public static final int falsesmall = 2130837647;
        public static final int featureoptions = 2130837648;
        public static final int final_assistantdirector = 2130837649;
        public static final int final_cinemaenthusiast = 2130837650;
        public static final int final_cinemaguy = 2130837651;
        public static final int final_director = 2130837652;
        public static final int final_keepout = 2130837653;
        public static final int final_monkey = 2130837654;
        public static final int frame_layout_shape = 2130837655;
        public static final int ic_launcher = 2130837656;
        public static final int iconfalse = 2130837657;
        public static final int iconquestion = 2130837658;
        public static final int icontrue = 2130837659;
        public static final int imagebuttonclassicgame = 2130837660;
        public static final int imagebuttoncontinue = 2130837661;
        public static final int imagebuttonexit = 2130837662;
        public static final int imagebuttonfalsebig = 2130837663;
        public static final int imagebuttonfalsesmall = 2130837664;
        public static final int imagebuttontriviagame = 2130837665;
        public static final int imagebuttontruebig = 2130837666;
        public static final int imagebuttontruesmall = 2130837667;
        public static final int imagebuttontryit = 2130837668;
        public static final int jumpingsheepsgame = 2130837669;
        public static final int leaderboard = 2130837670;
        public static final int logo2 = 2130837671;
        public static final int more = 2130837672;
        public static final int movienames = 2130837673;
        public static final int movienames1 = 2130837674;
        public static final int movienames2 = 2130837675;
        public static final int multi = 2130837676;
        public static final int multi1 = 2130837677;
        public static final int multi2 = 2130837678;
        public static final int n_0 = 2130837679;
        public static final int n_1 = 2130837680;
        public static final int n_10 = 2130837681;
        public static final int n_11 = 2130837682;
        public static final int n_12 = 2130837683;
        public static final int n_13 = 2130837684;
        public static final int n_14 = 2130837685;
        public static final int n_15 = 2130837686;
        public static final int n_16 = 2130837687;
        public static final int n_17 = 2130837688;
        public static final int n_18 = 2130837689;
        public static final int n_19 = 2130837690;
        public static final int n_2 = 2130837691;
        public static final int n_20 = 2130837692;
        public static final int n_3 = 2130837693;
        public static final int n_4 = 2130837694;
        public static final int n_5 = 2130837695;
        public static final int n_6 = 2130837696;
        public static final int n_7 = 2130837697;
        public static final int n_8 = 2130837698;
        public static final int n_9 = 2130837699;
        public static final int newrec = 2130837700;
        public static final int of_achievement_icon_frame = 2130837701;
        public static final int of_achievement_icon_locked = 2130837702;
        public static final int of_achievement_icon_unlocked = 2130837703;
        public static final int of_achievement_notification_bkg = 2130837704;
        public static final int of_achievement_notification_locked = 2130837705;
        public static final int of_feint_points_white = 2130837706;
        public static final int of_icon_dashboard_exit = 2130837707;
        public static final int of_icon_dashboard_home = 2130837708;
        public static final int of_icon_dashboard_settings = 2130837709;
        public static final int of_icon_highscore_notification = 2130837710;
        public static final int of_ll_logo = 2130837711;
        public static final int of_native_loader = 2130837712;
        public static final int of_native_loader_frame = 2130837713;
        public static final int of_native_loader_leaf = 2130837714;
        public static final int of_native_loader_progress = 2130837715;
        public static final int of_native_loader_progress_01 = 2130837716;
        public static final int of_native_loader_progress_02 = 2130837717;
        public static final int of_native_loader_progress_03 = 2130837718;
        public static final int of_native_loader_progress_04 = 2130837719;
        public static final int of_native_loader_progress_05 = 2130837720;
        public static final int of_native_loader_progress_06 = 2130837721;
        public static final int of_native_loader_progress_07 = 2130837722;
        public static final int of_native_loader_progress_08 = 2130837723;
        public static final int of_native_loader_progress_09 = 2130837724;
        public static final int of_native_loader_progress_10 = 2130837725;
        public static final int of_native_loader_progress_11 = 2130837726;
        public static final int of_native_loader_progress_12 = 2130837727;
        public static final int of_notification_bkg = 2130837728;
        public static final int one = 2130837729;
        public static final int oscar = 2130837730;
        public static final int photobg = 2130837731;
        public static final int play = 2130837732;
        public static final int playmulti = 2130837733;
        public static final int playmultiselected = 2130837734;
        public static final int playselected = 2130837735;
        public static final int selectedbig = 2130837736;
        public static final int selectedsmall = 2130837737;
        public static final int selector = 2130837738;
        public static final int sheepsgroup1 = 2130837739;
        public static final int sheepsgroup2 = 2130837740;
        public static final int shlogo = 2130837741;
        public static final int shlogo2 = 2130837742;
        public static final int startgame = 2130837743;
        public static final int stub = 2130837744;
        public static final int three = 2130837745;
        public static final int truebig = 2130837746;
        public static final int truesmall = 2130837747;
        public static final int tryit = 2130837748;
        public static final int tryitenabled = 2130837749;
        public static final int two = 2130837750;
    }

    public static final class id {
        public static final int ButtonBar = 2131361841;
        public static final int Canc = 2131361840;
        public static final int False = 2131361842;
        public static final int ImageTimer = 2131361814;
        public static final int ImageView = 2131361844;
        public static final int ImageView01 = 2131361796;
        public static final int ImageView02 = 2131361800;
        public static final int ImageView04 = 2131361798;
        public static final int ImageView05 = 2131361795;
        public static final int ImageViewActor = 2131361816;
        public static final int ImageViewTrailer = 2131361846;
        public static final int ImageViewbottom = 2131361845;
        public static final int Linear = 2131361811;
        public static final int LinearLayout01 = 2131361792;
        public static final int Ok = 2131361801;
        public static final int Rank = 2131361808;
        public static final int Score = 2131361809;
        public static final int ScrollView01 = 2131361793;
        public static final int TextView01 = 2131361797;
        public static final int True = 2131361843;
        public static final int booleanmodebutton = 2131361835;
        public static final int cdTimer = 2131361817;
        public static final int exit_feint = 2131361849;
        public static final int frameLayout = 2131361827;
        public static final int hint = 2131361802;
        public static final int home = 2131361847;
        public static final int home_container = 2131361810;
        public static final int image = 2131361807;
        public static final int image0 = 2131361803;
        public static final int image1 = 2131361805;
        public static final int image2 = 2131361806;
        public static final int layout_root = 2131361794;
        public static final int list = 2131361812;
        public static final int nested_window_root = 2131361826;
        public static final int newrec = 2131361838;
        public static final int of_achievement_icon = 2131361819;
        public static final int of_achievement_icon_frame = 2131361820;
        public static final int of_achievement_notification = 2131361818;
        public static final int of_achievement_progress_icon = 2131361824;
        public static final int of_achievement_score = 2131361822;
        public static final int of_achievement_score_icon = 2131361823;
        public static final int of_achievement_text = 2131361821;
        public static final int of_icon = 2131361830;
        public static final int of_ll_logo_image = 2131361829;
        public static final int of_text = 2131361831;
        public static final int of_text1 = 2131361832;
        public static final int of_text2 = 2131361833;
        public static final int progress = 2131361825;
        public static final int score = 2131361837;
        public static final int scoretot = 2131361839;
        public static final int settings = 2131361848;
        public static final int stage = 2131361836;
        public static final int text = 2131361804;
        public static final int transparent_panel = 2131361815;
        public static final int transparent_panel2 = 2131361813;
        public static final int triviamodebutton = 2131361834;
        public static final int trygeo = 2131361799;
        public static final int web_view = 2131361828;
    }

    public static final class layout {
        public static final int freeversion_dialog = 2130903040;
        public static final int item = 2130903041;
        public static final int itemlevel = 2130903042;
        public static final int itemparent = 2130903043;
        public static final int main = 2130903044;
        public static final int of_achievement_notification = 2130903045;
        public static final int of_native_loader = 2130903046;
        public static final int of_nested_window = 2130903047;
        public static final int of_simple_notification = 2130903048;
        public static final int of_two_line_notification = 2130903049;
        public static final int playoptions_dialog = 2130903050;
        public static final int score_dialog = 2130903051;
        public static final int singleplayerbuttons = 2130903052;
        public static final int splash = 2130903053;
    }

    public static final class menu {
        public static final int of_dashboard = 2131296256;
    }

    public static final class raw {
        public static final int axel = 2131034112;
        public static final int beepdown = 2131034113;
        public static final int beepfalling = 2131034114;
        public static final int brunner = 2131034115;
        public static final int intro = 2131034116;
        public static final int menu = 2131034117;
        public static final int timerbeep = 2131034118;
    }

    public static final class string {
        public static final int app_name = 2131165228;
        public static final int hello = 2131165227;
        public static final int of_achievement_load_null = 2131165192;
        public static final int of_achievement_unlock_null = 2131165191;
        public static final int of_achievement_unlocked = 2131165207;
        public static final int of_banned_dialog = 2131165220;
        public static final int of_bitmap_decode_error = 2131165209;
        public static final int of_cancel = 2131165203;
        public static final int of_cant_compress_blob = 2131165205;
        public static final int of_crash_report_query = 2131165217;
        public static final int of_device = 2131165198;
        public static final int of_error_parsing_error_message = 2131165211;
        public static final int of_exit_feint = 2131165222;
        public static final int of_file_not_found = 2131165210;
        public static final int of_home = 2131165219;
        public static final int of_id_cannot_be_null = 2131165186;
        public static final int of_io_exception_on_download = 2131165204;
        public static final int of_ioexception_reading_body = 2131165213;
        public static final int of_key_cannot_be_null = 2131165184;
        public static final int of_loading_feint = 2131165199;
        public static final int of_low_memory_profile_pic = 2131165194;
        public static final int of_malformed_request_error = 2131165226;
        public static final int of_name_cannot_be_null = 2131165187;
        public static final int of_no = 2131165200;
        public static final int of_no_blob = 2131165206;
        public static final int of_no_video = 2131165218;
        public static final int of_nodisk = 2131165196;
        public static final int of_now_logged_in_as_format = 2131165215;
        public static final int of_null_icon_url = 2131165190;
        public static final int of_offline_notification = 2131165223;
        public static final int of_offline_notification_line2 = 2131165224;
        public static final int of_ok = 2131165202;
        public static final int of_profile_pic_changed = 2131165216;
        public static final int of_profile_picture_download_failed = 2131165195;
        public static final int of_profile_url_null = 2131165193;
        public static final int of_score_submitted_notification = 2131165225;
        public static final int of_sdcard = 2131165197;
        public static final int of_secret_cannot_be_null = 2131165185;
        public static final int of_server_error_code_format = 2131165212;
        public static final int of_settings = 2131165221;
        public static final int of_switched_accounts = 2131165214;
        public static final int of_timeout = 2131165208;
        public static final int of_unexpected_response_format = 2131165188;
        public static final int of_unknown_server_error = 2131165189;
        public static final int of_yes = 2131165201;
    }

    public static final class style {
        public static final int Dialog_Fullscreen = 2131230722;
        public static final int OFLoading = 2131230720;
        public static final int OFNestedWindow = 2131230721;
        public static final int Theme_NoBackground = 2131230723;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
    }
}
