package com.sgstudios.MovieTrivia;

import android.content.Context;
import com.openfeint.api.OpenFeint;
import com.openfeint.api.resource.Leaderboard;
import com.openfeint.api.resource.Score;
import com.openfeint.api.resource.User;
import com.openfeint.api.ui.Dashboard;

public class S3Openfeint {
    static Context mContext;
    static boolean mOFeintInit;

    S3Openfeint(Context Context) {
        mContext = Context;
        OpenFeintInit();
    }

    private void OpenFeintInit() {
    }

    /* access modifiers changed from: package-private */
    public void ShowLeaderboard() {
        Dashboard.openLeaderboards();
    }

    /* access modifiers changed from: package-private */
    public void SaveScore(long scoreValue, String leaderboradID) {
        new Score(scoreValue, null).submitTo(new Leaderboard(leaderboradID), null);
    }

    /* access modifiers changed from: package-private */
    public void UpdateOnlineMaxScore(long localmaxscore, String leaderboradID) {
        final long local = localmaxscore;
        final Leaderboard l = new Leaderboard(leaderboradID);
        User me = OpenFeint.getCurrentUser();
        if (me != null) {
            l.getUserScore(me, new Leaderboard.GetUserScoreCB() {
                public void onSuccess(Score score) {
                    if (score.score < local) {
                        new Score(local, null).submitTo(l, null);
                    }
                }
            });
        }
    }
}
