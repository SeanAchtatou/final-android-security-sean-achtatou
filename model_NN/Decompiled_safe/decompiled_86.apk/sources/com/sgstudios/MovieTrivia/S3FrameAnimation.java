package com.sgstudios.MovieTrivia;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.widget.ImageView;
import java.util.ArrayList;
import java.util.List;

public class S3FrameAnimation {
    Runnable mChange;
    Context mContext;
    /* access modifiers changed from: private */
    public int mCurrentFrame = 0;
    Runnable mEnd;
    /* access modifiers changed from: private */
    public List<S3Frame> mFrames = new ArrayList();
    Runnable mRunner;
    Bitmap mTmpBitmap;
    /* access modifiers changed from: private */
    public ImageView mView;

    class S3Frame {
        int mDuration;
        int mImg;

        public S3Frame(int Img, int Duration) {
            this.mImg = Img;
            this.mDuration = Duration;
        }
    }

    public S3FrameAnimation(Context ctx) {
        this.mContext = ctx;
    }

    public void SetView(ImageView view) {
        this.mView = view;
    }

    public void AddFrame(int image, int duration) {
        this.mFrames.add(new S3Frame(image, duration));
    }

    public void SetEndEvent(Runnable end) {
        this.mEnd = end;
    }

    public void SetChangeEvent(Runnable change) {
        this.mChange = change;
    }

    public void Start() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                if (S3FrameAnimation.this.mCurrentFrame < S3FrameAnimation.this.mFrames.size()) {
                    S3Frame currframe = (S3Frame) S3FrameAnimation.this.mFrames.get(S3FrameAnimation.this.mCurrentFrame);
                    if (S3FrameAnimation.this.mTmpBitmap != null) {
                        S3FrameAnimation.this.mTmpBitmap.recycle();
                        S3FrameAnimation.this.mTmpBitmap = null;
                    }
                    S3FrameAnimation.this.mTmpBitmap = BitmapFactory.decodeResource(S3FrameAnimation.this.mContext.getResources(), currframe.mImg);
                    S3FrameAnimation.this.mView.setImageBitmap(S3FrameAnimation.this.mTmpBitmap);
                    new Handler().postDelayed(this, (long) currframe.mDuration);
                    S3FrameAnimation s3FrameAnimation = S3FrameAnimation.this;
                    s3FrameAnimation.mCurrentFrame = s3FrameAnimation.mCurrentFrame + 1;
                    if (S3FrameAnimation.this.mChange != null) {
                        S3FrameAnimation.this.mChange.run();
                    }
                } else if (S3FrameAnimation.this.mEnd != null) {
                    S3FrameAnimation.this.mEnd.run();
                }
            }
        }, 0);
    }
}
