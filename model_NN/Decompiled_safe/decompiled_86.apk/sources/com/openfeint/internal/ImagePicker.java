package com.openfeint.internal;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.provider.MediaStore;
import android.widget.Toast;
import com.openfeint.internal.OpenFeintInternal;
import com.sgstudios.MovieTrivia.R;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class ImagePicker {
    public static final int IMAGE_PICKER_REQ_ID = 10009;
    protected static final String TAG = "ImagePicker";

    public static void show(Activity currentActivity) {
        ((ActivityManager) currentActivity.getSystemService("activity")).getMemoryInfo(new ActivityManager.MemoryInfo());
        Intent intent = new Intent("android.intent.action.PICK", MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        intent.setType("image/*");
        currentActivity.startActivityForResult(intent, IMAGE_PICKER_REQ_ID);
    }

    public static boolean isImagePickerActivityResult(int requestCode) {
        return requestCode == 10009;
    }

    /* JADX INFO: Multiple debug info for r6v3 java.lang.String: [D('currentActivity' android.app.Activity), D('msg' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r6v7 int: [D('currentActivity' android.app.Activity), D('columnIndex' int)] */
    /* JADX INFO: Multiple debug info for r6v8 java.lang.String: [D('columnIndex' int), D('filePath' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r6v9 android.graphics.Bitmap: [D('filePath' java.lang.String), D('image' android.graphics.Bitmap)] */
    public static Bitmap onImagePickerActivityResult(Activity currentActivity, int resultCode, int maxLength, Intent returnedIntent) {
        if (resultCode == -1) {
            String[] columns = {"_data", "orientation"};
            Cursor cursor = currentActivity.getContentResolver().query(returnedIntent.getData(), columns, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                String filePath = cursor.getString(cursor.getColumnIndex(columns[0]));
                int rotation = cursor.getInt(cursor.getColumnIndex(columns[1]));
                cursor.close();
                Bitmap image = resize(filePath, maxLength, rotation);
                OpenFeintInternal.log(TAG, "image! " + image.getWidth() + "x" + image.getHeight());
                return image;
            }
            Toast.makeText(OpenFeintInternal.getInstance().getContext(), OpenFeintInternal.getRString(R.string.of_profile_picture_download_failed), 1).show();
        }
        return null;
    }

    public static void compressAndUpload(Bitmap image, String apiPath, OpenFeintInternal.IUploadDelegate delegate) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, out);
        upload(apiPath, out, delegate);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private static Bitmap resize(String filePath, int maxLength, int rotation) {
        boolean tall;
        int _y;
        int _length;
        Bitmap image = preScaleImage(filePath, maxLength);
        int width = image.getWidth();
        int height = image.getHeight();
        if (height > width) {
            tall = true;
        } else {
            tall = false;
        }
        int _x = tall ? 0 : (width - height) / 2;
        if (tall) {
            _y = (height - width) / 2;
        } else {
            _y = 0;
        }
        if (tall) {
            _length = width;
        } else {
            _length = height;
        }
        float scale = ((float) maxLength) / ((float) _length);
        Matrix transform = new Matrix();
        transform.postScale(scale, scale);
        transform.postRotate((float) rotation);
        return Bitmap.createBitmap(image, _x, _y, _length, _length, transform, false);
    }

    private static Bitmap preScaleImage(String filePath, int maxLength) {
        File f = new File(filePath);
        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);
            int minDim = Math.min(o.outWidth, o.outHeight);
            int scale = 1;
            while (minDim / 2 > maxLength) {
                minDim /= 2;
                scale++;
            }
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
            OpenFeintInternal.log(TAG, e.toString());
            return null;
        }
    }

    private static void upload(String apiPath, ByteArrayOutputStream stream, OpenFeintInternal.IUploadDelegate delegate) {
        OpenFeintInternal.getInstance().uploadFile(apiPath, "profile.png", stream.toByteArray(), "image/png", delegate);
    }
}
