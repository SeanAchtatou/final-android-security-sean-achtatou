package com.openfeint.internal.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

public class PadLogoImageView extends ImageView {
    private final String tag = "ImageView2";

    public PadLogoImageView(Context context) {
        super(context);
    }

    public PadLogoImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PadLogoImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        Log.e("ImageView2", String.valueOf(changed));
        Log.e("ImageView2", String.valueOf(top));
        if (!changed) {
            return;
        }
        if (top < 350) {
            setVisibility(4);
        } else {
            setVisibility(0);
        }
    }
}
