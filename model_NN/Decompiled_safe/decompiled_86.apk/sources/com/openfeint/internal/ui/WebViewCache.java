package com.openfeint.internal.ui;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDiskIOException;
import android.database.sqlite.SQLiteStatement;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import com.openfeint.api.ui.Dashboard;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.Util;
import com.openfeint.internal.db.DB;
import com.openfeint.internal.request.BaseRequest;
import com.openfeint.internal.request.CacheRequest;
import com.openfeint.internal.request.OrderedArgList;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class WebViewCache {
    private static final String ManifestRequestKey = "manifest";
    private static final String OPENFEINT_ROOT = "openfeint";
    static final String TAG = "WebViewCache";
    private static final String WEBUI = "webui";
    private static boolean diskError = false;
    static final int kBatchLoaded = 2;
    static final long kBatchRetryDelayMillis = 5000;
    static final int kClientManifestReady = 3;
    static final int kDataLoaded = 1;
    static final int kNumBatchRetries = 3;
    static final int kServerManifestReady = 0;
    public static String manifestProductOverride;
    /* access modifiers changed from: private */
    public static String rootPath;
    private static String rootUri;
    static WebViewCache sInstance;
    public static URI serverOverride;
    Context appContext;
    boolean batchesAreBroken = false;
    Map<String, String> clientManifest;
    WebViewCacheCallback delegate;
    boolean globalsFinished = false;
    boolean loadingFinished = false;
    Handler mDelayHandler;
    Handler mHandler;
    Set<String> pathsToLoad;
    Set<String> prioritizedPaths;
    ManifestData serverManifest;
    final URI serverURI = getServerURI();
    Map<String, ItemAndCallback> trackedItems;
    Set<PathAndCallback> trackedPaths;

    public static WebViewCache initialize(Context context) {
        if (sInstance != null) {
            sInstance.finishWithoutLoading();
        }
        sInstance = new WebViewCache(context);
        return sInstance;
    }

    public static void prioritize(String path) {
        sInstance.prioritizeInner(path);
    }

    public static boolean trackPath(String path, WebViewCacheCallback cb) {
        return sInstance.trackPathInner(path, cb);
    }

    public static boolean isLoaded(String path) {
        return sInstance.isLoadedInner(path);
    }

    public final void setRootUriSdcard(File path) {
        boolean copyDefault;
        final File webui = new File(path, WEBUI);
        if (webui.exists()) {
            copyDefault = false;
        } else {
            copyDefault = true;
        }
        if (copyDefault) {
            try {
                new File(path, ".nomedia").createNewFile();
            } catch (IOException e) {
            }
            if (!webui.mkdirs()) {
                setRootUriInternal();
                return;
            }
        }
        rootPath = String.valueOf(webui.getAbsolutePath()) + "/";
        rootUri = "file://" + rootPath;
        if (copyDefault) {
            final File baseDir = this.appContext.getFilesDir();
            final File inPhoneWebui = new File(baseDir, WEBUI);
            if (inPhoneWebui.isDirectory()) {
                try {
                    Util.copyFile(this.appContext.getDatabasePath(DB.DBNAME), new File(webui, DB.DBNAME));
                } catch (IOException e2) {
                }
            }
            new Thread(new Runnable() {
                public void run() {
                    try {
                        if (inPhoneWebui.isDirectory()) {
                            Util.copyDirectory(inPhoneWebui, webui);
                            WebViewCache.this.deleteAll();
                            OpenFeintInternal.log(WebViewCache.TAG, "copy in phone data finish");
                            WebViewCache.this.clientManifestReady();
                            return;
                        }
                        OpenFeintInternal.log(WebViewCache.TAG, "copy from asset");
                        WebViewCache.this.copyDefaultBackground(baseDir);
                    } catch (IOException e) {
                        OpenFeintInternal.log(WebViewCache.TAG, e.getMessage());
                        WebViewCache.this.setRootUriInternal();
                    }
                }
            }).start();
            return;
        }
        clientManifestReady();
        deleteAll();
    }

    public final void setRootUriInternal() {
        OpenFeintInternal.log(TAG, "can't use sdcard");
        final File baseDir = this.appContext.getFilesDir();
        rootPath = String.valueOf(new File(baseDir, WEBUI).getAbsolutePath()) + "/";
        rootUri = "file://" + rootPath;
        if (!new File(baseDir, WEBUI).isDirectory()) {
            new Thread(new Runnable() {
                public void run() {
                    WebViewCache.this.copyDefaultBackground(baseDir);
                }
            }).start();
        } else {
            clientManifestReady();
        }
    }

    public static final String getItemUri(String itemPath) {
        return String.valueOf(rootUri) + itemPath;
    }

    public static void start() {
        sInstance.updateExternalStorageState();
        sInstance.sync();
    }

    private static class ManifestItem {
        public Set<String> dependentObjects;
        public String hash;
        public String path;

        ManifestItem(String _path, String _hash) {
            this.path = _path;
            this.hash = _hash;
            this.dependentObjects = new HashSet();
        }

        ManifestItem(ManifestItem item) {
            this.path = item.path;
            this.dependentObjects = new HashSet(item.dependentObjects);
        }
    }

    public static void diskError() {
        diskError = true;
        for (PathAndCallback pathAndCb : sInstance.trackedPaths) {
            pathAndCb.callback.failLoaded();
        }
        sInstance.trackedPaths.clear();
        sInstance.finishWithoutLoading();
    }

    private static class ManifestData {
        Set<String> globals = new HashSet();
        Map<String, ManifestItem> objects = new HashMap();

        ManifestData(SQLiteDatabase db) {
            ManifestItem manifestItem;
            boolean isGlobal;
            Cursor result = null;
            try {
                result = db.rawQuery("SELECT path, hash, is_global FROM server_manifest", null);
                if (result.getCount() > 0) {
                    result.moveToFirst();
                    do {
                        String path = result.getString(0);
                        String hash = result.getString(1);
                        if (result.getInt(2) != 0) {
                            isGlobal = true;
                        } else {
                            isGlobal = false;
                        }
                        this.objects.put(path, new ManifestItem(path, hash));
                        if (isGlobal) {
                            this.globals.add(path);
                        }
                    } while (result.moveToNext());
                }
                result.close();
                for (String path2 : this.objects.keySet()) {
                    result = db.rawQuery("SELECT has_dependency FROM dependencies WHERE path = ?", new String[]{path2});
                    if (result.getCount() <= 0 || (manifestItem = this.objects.get(path2)) == null) {
                        result.close();
                    } else {
                        Set<String> deps = manifestItem.dependentObjects;
                        result.moveToFirst();
                        do {
                            deps.add(result.getString(0));
                        } while (result.moveToNext());
                        result.close();
                    }
                }
                try {
                    result.close();
                } catch (Exception e) {
                }
            } catch (SQLiteDiskIOException e2) {
                WebViewCache.diskError();
                try {
                    result.close();
                } catch (Exception e3) {
                }
            } catch (Exception e4) {
                OpenFeintInternal.log(WebViewCache.TAG, "SQLite exception. " + e4.toString());
                try {
                    result.close();
                } catch (Exception e5) {
                }
            } catch (Throwable th) {
                try {
                    result.close();
                } catch (Exception e6) {
                }
                throw th;
            }
        }

        /* access modifiers changed from: package-private */
        public void saveTo(SQLiteDatabase db) {
            try {
                db.beginTransaction();
                db.execSQL("DELETE FROM server_manifest;");
                db.execSQL("DELETE FROM dependencies;");
                SQLiteStatement insertIntoManifest = db.compileStatement("INSERT INTO server_manifest(path, hash, is_global) VALUES(?, ?, ?)");
                SQLiteStatement insertIntoDependencies = db.compileStatement("INSERT INTO dependencies(path, has_dependency) VALUES(?, ?)");
                for (String path : this.objects.keySet()) {
                    ManifestItem item = this.objects.get(path);
                    insertIntoManifest.bindString(1, path);
                    insertIntoManifest.bindString(2, item.hash);
                    insertIntoManifest.bindLong(3, (long) (this.globals.contains(path) ? 1 : 0));
                    insertIntoManifest.execute();
                    insertIntoDependencies.bindString(1, path);
                    for (String dep : item.dependentObjects) {
                        insertIntoDependencies.bindString(2, dep);
                        insertIntoDependencies.execute();
                    }
                }
                db.setTransactionSuccessful();
                try {
                    db.endTransaction();
                } catch (Exception e) {
                }
            } catch (SQLiteDiskIOException e2) {
                WebViewCache.diskError();
                try {
                    db.endTransaction();
                } catch (Exception e3) {
                }
            } catch (Exception e4) {
                OpenFeintInternal.log(WebViewCache.TAG, "SQLite exception. " + e4.toString());
                try {
                    db.endTransaction();
                } catch (Exception e5) {
                }
            } catch (Throwable th) {
                try {
                    db.endTransaction();
                } catch (Exception e6) {
                }
                throw th;
            }
        }

        ManifestData(byte[] stm) throws Exception {
            Exception e;
            String path;
            try {
                BufferedReader buffered = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(stm)), 8192);
                ManifestItem item = null;
                while (true) {
                    try {
                        String line = buffered.readLine();
                        if (line != null) {
                            String line2 = line.trim();
                            if (line2.length() != 0) {
                                switch (line2.charAt(0)) {
                                    case '#':
                                        continue;
                                    case '-':
                                        if (item != null) {
                                            item.dependentObjects.add(line2.substring(1).trim());
                                            continue;
                                        } else {
                                            throw new Exception("Manifest Syntax Error: Dependency without an item");
                                        }
                                    default:
                                        String[] pieces = line2.split(" ");
                                        if (pieces.length >= 2) {
                                            if (pieces[0].charAt(0) == '@') {
                                                path = pieces[0].substring(1);
                                                this.globals.add(path);
                                            } else {
                                                path = pieces[0];
                                            }
                                            ManifestItem item2 = new ManifestItem(path, pieces[1]);
                                            this.objects.put(path, item2);
                                            item = item2;
                                            continue;
                                        } else {
                                            throw new Exception("Manifest Syntax Error: Extra items in line");
                                        }
                                }
                            }
                        } else {
                            return;
                        }
                    } catch (Exception e2) {
                        e = e2;
                    }
                }
            } catch (Exception e3) {
                e = e3;
                throw new Exception(e);
            }
        }
    }

    private static class ItemAndCallback {
        public final WebViewCacheCallback callback;
        public final ManifestItem item;

        public ItemAndCallback(ManifestItem _item, WebViewCacheCallback _cb) {
            this.item = _item;
            this.callback = _cb;
        }
    }

    private static class PathAndCallback {
        public final WebViewCacheCallback callback;
        public final String path;

        public PathAndCallback(String _path, WebViewCacheCallback _cb) {
            this.path = _path;
            this.callback = _cb;
        }
    }

    private boolean trackPathInner(String path, WebViewCacheCallback cb) {
        if (this.loadingFinished) {
            cb.pathLoaded(path);
            return false;
        } else if (this.serverManifest == null) {
            cb.onTrackingNeeded();
            this.trackedPaths.add(new PathAndCallback(path, cb));
            return true;
        } else {
            ManifestItem loadedItem = this.serverManifest.objects.get(path);
            if (loadedItem != null) {
                cb.onTrackingNeeded();
                ManifestItem newItem = new ManifestItem(loadedItem);
                newItem.dependentObjects.retainAll(this.pathsToLoad);
                this.trackedItems.put(path, new ItemAndCallback(newItem, cb));
                return true;
            }
            cb.pathLoaded(path);
            return false;
        }
    }

    private boolean isLoadedInner(String path) {
        if (this.serverManifest == null) {
            return this.loadingFinished;
        }
        if (this.pathsToLoad.contains(path)) {
            return false;
        }
        return true;
    }

    private WebViewCache(Context _appContext) {
        this.appContext = _appContext;
        this.trackedPaths = new HashSet();
        this.trackedItems = new HashMap();
        this.pathsToLoad = new HashSet();
        this.prioritizedPaths = new HashSet();
        this.mDelayHandler = new Handler();
        this.mHandler = new Handler() {
            /* Debug info: failed to restart local var, previous not found, register: 4 */
            public void dispatchMessage(Message msg) {
                switch (msg.what) {
                    case 0:
                        OpenFeintInternal.log(WebViewCache.TAG, "kServerManifestReady");
                        WebViewCache.this.serverManifest = (ManifestData) msg.obj;
                        WebViewCache.this.triggerUpdates();
                        return;
                    case 1:
                        WebViewCache.this.finishItem((String) msg.obj, msg.arg1 > 0);
                        return;
                    case 2:
                        WebViewCache.this.finishItems((Set) msg.obj, msg.arg1 > 0);
                        return;
                    case 3:
                        WebViewCache.this.clientManifest = (Map) msg.obj;
                        WebViewCache.this.triggerUpdates();
                        return;
                    default:
                        return;
                }
            }
        };
    }

    private void updateExternalStorageState() {
        if (Util.noSdcardPermission()) {
            OpenFeintInternal.log(TAG, "no sdcard permission");
            setRootUriInternal();
            return;
        }
        String state = Environment.getExternalStorageState();
        if ("mounted".equals(state)) {
            setRootUriSdcard(new File(Environment.getExternalStorageDirectory(), OPENFEINT_ROOT));
            return;
        }
        OpenFeintInternal.log(TAG, state);
        setRootUriInternal();
    }

    /* access modifiers changed from: private */
    public void sync() {
        OpenFeintInternal.log(TAG, "--- WebViewCache Sync ---");
        new ManifestRequest(ManifestRequestKey).launch();
    }

    private class ManifestRequest extends CacheRequest {
        /* access modifiers changed from: private */
        public ManifestData data = null;

        public ManifestRequest(String key) {
            super(key);
        }

        public boolean signed() {
            return false;
        }

        public String path() {
            return WebViewCache.getManifestPath(WebViewCache.this.appContext);
        }

        public void onResponse(int responseCode, byte[] body) {
        }

        public void onResponseOffMainThread(int responseCode, byte[] body) {
            if (responseCode == 200) {
                try {
                    this.data = new ManifestData(body);
                } catch (Exception e) {
                    OpenFeintInternal.log(WebViewCache.TAG, e.toString());
                }
            } else {
                try {
                    this.data = new ManifestData(DB.storeHelper.getReadableDatabase());
                } catch (Exception e2) {
                    OpenFeintInternal.log(WebViewCache.TAG, e2.toString());
                }
            }
            if (this.data == null || this.data.objects.isEmpty()) {
                this.data = null;
                new BaseRequest() {
                    public String method() {
                        return "GET";
                    }

                    public String path() {
                        return ManifestRequest.this.path();
                    }

                    public void onResponse(int responseCode, byte[] body) {
                    }

                    public void onResponseOffMainThread(int responseCode, byte[] body) {
                        if (200 == responseCode) {
                            try {
                                ManifestRequest.this.data = new ManifestData(body);
                                ManifestRequest.this.finishManifest();
                                ManifestRequest.this.updateLastModifiedFromResponse(getResponse());
                            } catch (Exception e) {
                            }
                        } else {
                            OpenFeintInternal.log(TAG, "finishWithoutLoading " + responseCode);
                            WebViewCache.this.finishWithoutLoading();
                        }
                    }
                }.launch();
                return;
            }
            finishManifest();
            updateLastModifiedFromResponse(getResponse());
        }

        /* access modifiers changed from: private */
        public void finishManifest() {
            if (this.data != null) {
                try {
                    this.data.saveTo(DB.storeHelper.getWritableDatabase());
                } catch (Exception e) {
                    OpenFeintInternal.log(WebViewCache.TAG, e.toString());
                }
                Message.obtain(WebViewCache.this.mHandler, 0, this.data).sendToTarget();
                return;
            }
            WebViewCache.this.finishWithoutLoading();
        }
    }

    /* access modifiers changed from: private */
    public void deleteAll() {
        Util.deleteFiles(new File(this.appContext.getFilesDir(), WEBUI));
        this.appContext.getDatabasePath(DB.DBNAME).delete();
    }

    private void gatherDefaultItems(String path, Set<String> items) {
        try {
            String[] stuff = this.appContext.getAssets().list(path);
            int length = stuff.length;
            for (int i = 0; i < length; i++) {
                String fullpath = String.valueOf(path) + "/" + stuff[i];
                try {
                    InputStream check = this.appContext.getAssets().open(fullpath);
                    items.add(fullpath);
                    check.close();
                } catch (IOException e) {
                    gatherDefaultItems(fullpath, items);
                }
            }
        } catch (IOException e2) {
            OpenFeintInternal.log(TAG, e2.toString());
        }
    }

    private void copySingleItem(File baseDir, String path) {
        try {
            File filePath = new File(baseDir, path);
            DataInputStream reader = new DataInputStream(this.appContext.getAssets().open(path));
            filePath.getParentFile().mkdirs();
            Util.copyStream(reader, new DataOutputStream(new FileOutputStream(filePath)));
        } catch (Exception e) {
            OpenFeintInternal.log(TAG, e.toString());
        }
    }

    private Set<String> stripUnused(Set<String> table) {
        String test = Util.getDpiName(this.appContext).equals("mdpi") ? ".hdpi." : ".mdpi.";
        Set<String> reducedSet = new HashSet<>();
        for (String path : table) {
            if (!path.contains(test)) {
                reducedSet.add(path);
            }
        }
        return reducedSet;
    }

    private void copySpecific(File baseDir, String path, Set<String> items) {
        if (items.contains(path)) {
            copySingleItem(baseDir, path);
            items.remove(path);
        }
    }

    private void copyDirectory(File baseDir, String root, Set<String> items) {
        Set<String> dirItems = new HashSet<>();
        for (String path : items) {
            if (path.startsWith(root)) {
                dirItems.add(path);
            }
        }
        for (String path2 : dirItems) {
            copySpecific(baseDir, path2, items);
        }
    }

    /* access modifiers changed from: private */
    public void copyDefaultBackground(File baseDir) {
        Set<String> defaultItems = new HashSet<>();
        gatherDefaultItems(WEBUI, defaultItems);
        Set<String> defaultItems2 = stripUnused(defaultItems);
        copySpecific(baseDir, "webui/manifest.plist", defaultItems2);
        copyDirectory(baseDir, "webui/javascripts/", defaultItems2);
        copyDirectory(baseDir, "webui/stylesheets/", defaultItems2);
        copyDirectory(baseDir, "webui/intro/", defaultItems2);
        if (Util.getDpiName(this.appContext).equals("mdpi")) {
            copySpecific(baseDir, "webui/images/space.grid.mdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/button.gray.mdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/button.gray.hit.mdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/button.green.mdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/button.green.hit.mdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/logo.small.mdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/header_bg.mdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/loading.spinner.mdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/input.text.mdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/frame.small.mdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/icon.leaf.gray.mdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/tab.divider.mdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/tab.active_indicator.mdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/logo.mdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/header_bg.mdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/loading.spinner.mdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/icon.user.male.mdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/intro.leaderboards.mdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/intro.friends.mdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/intro.achievements.mdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/intro.games.mdpi.png", defaultItems2);
        } else {
            copySpecific(baseDir, "webui/images/space.grid.hdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/button.gray.hdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/button.gray.hit.hdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/button.green.hdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/button.green.hit.hdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/logo.small.hdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/header_bg.hdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/loading.spinner.hdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/input.text.hdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/frame.small.hdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/icon.leaf.gray.hdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/tab.divider.hdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/tab.active_indicator.hdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/logo.hdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/header_bg.hdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/loading.spinner.hdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/icon.user.male.hdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/intro.leaderboards.hdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/intro.friends.hdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/intro.achievements.hdpi.png", defaultItems2);
            copySpecific(baseDir, "webui/images/intro.games.hdpi.png", defaultItems2);
        }
        clientManifestReady();
        for (String path : defaultItems2) {
            copySingleItem(baseDir, path);
        }
    }

    /* access modifiers changed from: private */
    public void clientManifestReady() {
        Map obj = getDefaultClientManifest();
        Message msg = Message.obtain(this.mHandler, 3);
        msg.obj = obj;
        msg.sendToTarget();
    }

    private class SaxHandler extends DefaultHandler {
        String key;
        String loadingString;
        Map<String, String> outputMap;

        private SaxHandler() {
            this.outputMap = new HashMap();
        }

        /* synthetic */ SaxHandler(WebViewCache webViewCache, SaxHandler saxHandler) {
            this();
        }

        public Map<String, String> getOutputMap() {
            return this.outputMap;
        }

        public void startElement(String uri, String name, String qName, Attributes attr) {
            this.loadingString = "";
        }

        public void endElement(String uri, String name, String qName) {
            String clipped = name.trim();
            if (clipped.equals("key")) {
                this.key = this.loadingString;
            } else if (clipped.equals("string")) {
                this.outputMap.put(this.key, this.loadingString);
                DB.setClientManifest(this.key, this.loadingString);
            }
        }

        public void characters(char[] ch, int start, int length) {
            this.loadingString = new String(ch).substring(start, start + length);
        }
    }

    public static boolean isDiskError() {
        return diskError;
    }

    public static boolean recover() {
        if (diskError) {
            return false;
        }
        return sInstance.recoverInternal();
    }

    /* access modifiers changed from: package-private */
    public void markSyncRequired() {
        this.loadingFinished = false;
        this.globalsFinished = false;
    }

    /* access modifiers changed from: package-private */
    public boolean recoverInternal() {
        boolean success = DB.recover(this.appContext);
        this.serverManifest = null;
        if (success) {
            this.clientManifest = getDefaultClientManifestFromAsset();
            success = !this.clientManifest.isEmpty();
        }
        markSyncRequired();
        sync();
        return success;
    }

    private Map<String, String> getDefaultClientManifest() {
        Cursor result = null;
        try {
            Cursor result2 = DB.storeHelper.getReadableDatabase().rawQuery("SELECT * FROM manifest", null);
            if (result2.getCount() > 0) {
                Map<String, String> outManifest = new HashMap<>();
                result2.moveToFirst();
                do {
                    outManifest.put(result2.getString(0), result2.getString(1));
                } while (result2.moveToNext());
                result2.close();
                OpenFeintInternal.log(TAG, "create client Manifest from db");
                try {
                    result2.close();
                } catch (Exception e) {
                }
                return outManifest;
            }
            try {
                result2.close();
            } catch (Exception e2) {
            }
            return getDefaultClientManifestFromAsset();
        } catch (SQLiteDiskIOException e3) {
            diskError();
            try {
                result.close();
            } catch (Exception e4) {
            }
        } catch (Exception e5) {
            OpenFeintInternal.log(TAG, "SQLite exception. " + e5.toString());
            try {
                result.close();
            } catch (Exception e6) {
            }
        } catch (Throwable th) {
            try {
                result.close();
            } catch (Exception e7) {
            }
            throw th;
        }
    }

    private Map<String, String> getDefaultClientManifestFromAsset() {
        File manifestFile = new File(rootPath, "manifest.plist");
        if (manifestFile.isFile()) {
            try {
                XMLReader xr = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
                SaxHandler handler = new SaxHandler(this, null);
                xr.setContentHandler(handler);
                xr.parse(new InputSource(new FileInputStream(manifestFile.getPath())));
                return handler.getOutputMap();
            } catch (Exception e) {
                OpenFeintInternal.log(TAG, e.toString());
            }
        }
        return new HashMap();
    }

    private static final URI getServerURI() {
        try {
            if (serverOverride != null) {
                return serverOverride;
            }
            return new URI(OpenFeintInternal.getInstance().getServerUrl());
        } catch (Exception e) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public static final String getManifestPath(Context ctx) {
        return String.format("/webui/manifest/%s.%s.%s", "android", manifestProductOverride != null ? manifestProductOverride : "embed", Util.getDpiName(ctx));
    }

    /* access modifiers changed from: private */
    public void triggerUpdates() {
        OpenFeintInternal.log(TAG, "loadedManifest");
        if (this.serverManifest != null && this.clientManifest != null) {
            for (ManifestItem item : this.serverManifest.objects.values()) {
                if (!item.hash.equals(this.clientManifest.get(item.path))) {
                    this.pathsToLoad.add(item.path);
                }
            }
            loadNextItem();
        }
    }

    /* access modifiers changed from: private */
    public void finishWithoutLoading() {
        OpenFeintInternal.log(TAG, "finishWithoutLoading");
        for (PathAndCallback pathAndCb : this.trackedPaths) {
            pathAndCb.callback.pathLoaded(pathAndCb.path);
        }
        this.trackedPaths.clear();
        this.prioritizedPaths.clear();
        this.serverManifest.globals.clear();
        this.pathsToLoad.clear();
        finishLoading();
    }

    private void finishLoading() {
        DB.storeHelper.close();
        this.loadingFinished = true;
    }

    /* access modifiers changed from: private */
    public void batchFetch(String originalUrl, String currentUrl, int retriesLeft, Set<String> paths) {
        final String str = originalUrl;
        final String str2 = currentUrl;
        final int i = retriesLeft;
        final Set<String> set = paths;
        new BaseRequest() {
            public boolean signed() {
                return false;
            }

            public String method() {
                return "GET";
            }

            public String path() {
                return "";
            }

            public String url() {
                return str;
            }

            public void onResponse(int responseCode, byte[] body) {
            }

            public void onResponseOffMainThread(int responseCode, byte[] body) {
                WebViewCache.this.handleBatchBody(responseCode, body, str, str2, i, set);
            }
        }.launch();
    }

    private void batchRequest(Set<String> paths) {
        batchRequest(paths, 3);
    }

    /* access modifiers changed from: private */
    public void batchRequest(final Set<String> paths, final int numRetries) {
        OpenFeintInternal.log(TAG, String.format("Syncing %d items", Integer.valueOf(paths.size())));
        OrderedArgList oal = new OrderedArgList();
        for (String s : paths) {
            ManifestItem manifestItem = this.serverManifest.objects.get(s);
            oal.put("files[][path]", manifestItem.path);
            oal.put("files[][hash]", manifestItem.hash);
        }
        new BaseRequest(oal) {
            public boolean signed() {
                return false;
            }

            public String method() {
                return "POST";
            }

            public String path() {
                return "/webui/assets";
            }

            /* access modifiers changed from: protected */
            public void onResponseOffMainThread(int responseCode, byte[] body) {
                WebViewCache.this.handleBatchBody(responseCode, body, url(), currentURL(), numRetries, paths);
            }

            public void onResponse(int responseCode, byte[] body) {
            }
        }.launch();
    }

    /* access modifiers changed from: private */
    public void handleBatchBody(int responseCode, byte[] body, String originalUrl, String currentUrl, int retriesLeft, Set<String> paths) {
        if (200 <= responseCode && responseCode < 300) {
            processBatch(paths, body);
        } else if (302 == responseCode || 303 == responseCode) {
            batchFetch(originalUrl, currentUrl, retriesLeft, paths);
        } else if (responseCode != 0 && (400 > responseCode || responseCode >= 500)) {
            Message.obtain(this.mHandler, 2, 0, 0, paths).sendToTarget();
        } else if (retriesLeft > 0) {
            final String str = originalUrl;
            final String str2 = currentUrl;
            final Set<String> set = paths;
            final int i = retriesLeft;
            this.mDelayHandler.postDelayed(new Runnable() {
                public void run() {
                    if (str.equals(str2)) {
                        WebViewCache.this.batchRequest(set, i - 1);
                    } else {
                        WebViewCache.this.batchFetch(str, str2, i - 1, set);
                    }
                }
            }, kBatchRetryDelayMillis);
        } else {
            Message.obtain(this.mHandler, 2, 0, 0, paths).sendToTarget();
        }
    }

    private void finishGlobals() {
        ManifestItem item;
        for (PathAndCallback pathAndCb : this.trackedPaths) {
            if (!this.pathsToLoad.contains(pathAndCb.path)) {
                pathAndCb.callback.pathLoaded(pathAndCb.path);
            } else {
                ManifestItem newItem = new ManifestItem(this.serverManifest.objects.get(pathAndCb.path));
                newItem.dependentObjects.retainAll(this.pathsToLoad);
                this.trackedItems.put(pathAndCb.path, new ItemAndCallback(newItem, pathAndCb.callback));
            }
        }
        this.trackedPaths.clear();
        Set<String> priorityDependents = new HashSet<>();
        for (String path : this.prioritizedPaths) {
            if (this.pathsToLoad.contains(path) && (item = this.serverManifest.objects.get(path)) != null) {
                priorityDependents.addAll(item.dependentObjects);
            }
        }
        priorityDependents.retainAll(this.pathsToLoad);
        this.prioritizedPaths.addAll(priorityDependents);
        this.globalsFinished = true;
    }

    private void loadNextItem() {
        OpenFeintInternal.log(TAG, "loadNextItem");
        this.serverManifest.globals.retainAll(this.pathsToLoad);
        if (!this.globalsFinished && this.serverManifest.globals.isEmpty()) {
            finishGlobals();
        }
        this.prioritizedPaths.retainAll(this.pathsToLoad);
        int numGlobalsAndPrioritized = this.serverManifest.globals.size() + this.prioritizedPaths.size();
        if (!this.batchesAreBroken && numGlobalsAndPrioritized > 1) {
            Set<String> combinedGlobalsAndPrio = new HashSet<>();
            combinedGlobalsAndPrio.addAll(this.serverManifest.globals);
            combinedGlobalsAndPrio.addAll(this.prioritizedPaths);
            batchRequest(combinedGlobalsAndPrio);
        } else if (this.serverManifest.globals.size() > 0) {
            singleRequest(this.serverManifest.globals.iterator().next());
        } else if (this.prioritizedPaths.size() > 0) {
            singleRequest(this.prioritizedPaths.iterator().next());
        } else if (!this.batchesAreBroken && this.pathsToLoad.size() > 1) {
            batchRequest(this.pathsToLoad);
        } else if (this.pathsToLoad.size() > 0) {
            singleRequest(this.pathsToLoad.iterator().next());
        } else {
            finishLoading();
        }
    }

    private final void singleRequest(final String finalPath) {
        OpenFeintInternal.log(TAG, "Syncing item: " + finalPath);
        new BaseRequest() {
            public boolean signed() {
                return false;
            }

            public String method() {
                return "GET";
            }

            public String path() {
                return "/webui/" + finalPath;
            }

            public void onResponse(int responseCode, byte[] body) {
                if (responseCode != 200) {
                    Message.obtain(WebViewCache.this.mHandler, 1, 0, 0, finalPath).sendToTarget();
                    return;
                }
                try {
                    Util.saveFile(body, String.valueOf(WebViewCache.rootPath) + finalPath);
                    Message.obtain(WebViewCache.this.mHandler, 1, 1, 0, finalPath).sendToTarget();
                } catch (Exception e) {
                    Message.obtain(WebViewCache.this.mHandler, 1, 0, 0, finalPath).sendToTarget();
                }
            }
        }.launch();
    }

    /* access modifiers changed from: private */
    public void finishItem(String path, boolean succeeded) {
        HashSet<String> tiny = new HashSet<>(1);
        tiny.add(path);
        finishItems(tiny, succeeded, true);
    }

    /* access modifiers changed from: private */
    public void finishItems(Set<String> paths, boolean succeeded) {
        finishItems(paths, succeeded, false);
    }

    private void finishItems(Set<String> paths, boolean succeeded, boolean wasSingular) {
        if (this.serverManifest != null) {
            if (succeeded || wasSingular) {
                for (ItemAndCallback itemAndCb : this.trackedItems.values()) {
                    itemAndCb.item.dependentObjects.removeAll(paths);
                }
                this.pathsToLoad.removeAll(paths);
                this.serverManifest.globals.removeAll(paths);
                this.prioritizedPaths.removeAll(paths);
                if (this.globalsFinished) {
                    HashSet<String> pathsToRemove = new HashSet<>();
                    for (ItemAndCallback itemAndCb2 : this.trackedItems.values()) {
                        if (!this.pathsToLoad.contains(itemAndCb2.item.path) && itemAndCb2.item.dependentObjects.size() == 0) {
                            pathsToRemove.add(itemAndCb2.item.path);
                            itemAndCb2.callback.pathLoaded(itemAndCb2.item.path);
                        }
                    }
                    Iterator it = pathsToRemove.iterator();
                    while (it.hasNext()) {
                        this.trackedItems.remove((String) it.next());
                    }
                }
                String[] pathsArray = new String[paths.size()];
                String[] hashArray = new String[pathsArray.length];
                int i = 0;
                for (String path : paths) {
                    String hashValue = succeeded ? this.serverManifest.objects.get(path).hash : "INVALID";
                    pathsArray[i] = path;
                    hashArray[i] = hashValue;
                    i++;
                    this.clientManifest.put(path, hashValue);
                }
                DB.setClientManifestBatch(pathsArray, hashArray);
            } else {
                this.batchesAreBroken = true;
            }
            loadNextItem();
        }
    }

    private void prioritizeInner(String path) {
        ManifestItem item;
        if (!this.loadingFinished) {
            this.prioritizedPaths.add(path);
            if (this.serverManifest != null && (item = this.serverManifest.objects.get(path)) != null) {
                Set<String> loadingDependents = new HashSet<>(item.dependentObjects);
                loadingDependents.retainAll(this.pathsToLoad);
                this.prioritizedPaths.addAll(loadingDependents);
                OpenFeintInternal.log(TAG, "Prioritizing " + path + " deps:" + loadingDependents.toString());
            }
        }
    }

    private void processBatch(Set<String> paths, byte[] body) {
        HashSet<String> fetchedPaths = new HashSet<>();
        ZipInputStream zis = new ZipInputStream(new ByteArrayInputStream(body));
        while (true) {
            try {
                ZipEntry ze = zis.getNextEntry();
                if (ze == null) {
                    break;
                } else if (!ze.isDirectory()) {
                    String finalPath = ze.getName();
                    Util.saveStreamAndLeaveInputOpen(zis, String.valueOf(rootPath) + finalPath);
                    fetchedPaths.add(finalPath);
                }
            } catch (Exception e) {
                OpenFeintInternal.log(TAG, e.getMessage());
            }
        }
        if (!fetchedPaths.isEmpty()) {
            Message.obtain(this.mHandler, 2, 1, 0, fetchedPaths).sendToTarget();
        } else {
            Message.obtain(this.mHandler, 2, 0, 0, paths).sendToTarget();
        }
    }

    public static class TestOnlyManifestItem {
        public String clientHash;
        public String path;
        public String serverHash;

        public enum Status {
            NotYetDownloaded,
            NotOnServer,
            UpToDate,
            OutOfDate
        }

        public TestOnlyManifestItem(String _path, String _clientHash, String _serverHash) {
            this.path = _path;
            this.clientHash = _clientHash;
            this.serverHash = _serverHash;
        }

        public Status status() {
            if (this.clientHash == null) {
                return Status.NotYetDownloaded;
            }
            if (this.serverHash == null) {
                return Status.NotOnServer;
            }
            if (this.serverHash.equals(this.clientHash)) {
                return Status.UpToDate;
            }
            return Status.OutOfDate;
        }

        public void invalidate() {
            DB.setClientManifest(this.path, "INVALID");
            WebViewCache.sInstance.clientManifest.put(this.path, "INVALID");
            Util.deleteFiles(new File(String.valueOf(WebViewCache.rootPath) + this.path));
            WebViewCache.sInstance.markSyncRequired();
        }

        public static void syncAndOpenDashboard() {
            if (!WebViewCache.sInstance.loadingFinished) {
                WebViewCache.sInstance.serverManifest = null;
                WebViewCache.sInstance.sync();
            }
            Dashboard.open();
        }
    }

    private static String fullOuterJoin(String fields, String table1, String table2, String condition) {
        return String.format("%s UNION %s;", String.format("SELECT %s from %s LEFT OUTER JOIN %s ON %s", fields, table1, table2, condition), String.format("SELECT %s from %s LEFT OUTER JOIN %s ON %s", fields, table2, table1, condition));
    }

    public static TestOnlyManifestItem[] testOnlyManifestItems() {
        SQLiteDatabase db = DB.storeHelper.getReadableDatabase();
        Cursor result = null;
        ArrayList<TestOnlyManifestItem> items = new ArrayList<>();
        try {
            Cursor result2 = db.rawQuery(fullOuterJoin("server_manifest.path, server_manifest.hash, manifest.hash", "server_manifest", ManifestRequestKey, "server_manifest.path = manifest.path"), null);
            if (result2.getCount() > 0) {
                result2.moveToFirst();
                do {
                    String path = result2.getString(0);
                    if (path != null) {
                        items.add(new TestOnlyManifestItem(path, result2.getString(2), result2.getString(1)));
                    }
                } while (result2.moveToNext());
            }
            try {
                result2.close();
            } catch (Exception e) {
            }
        } catch (Exception e2) {
            try {
                result.close();
            } catch (Exception e3) {
            }
        } catch (Throwable th) {
            try {
                result.close();
            } catch (Exception e4) {
            }
            throw th;
        }
        TestOnlyManifestItem[] rv = (TestOnlyManifestItem[]) items.toArray(new TestOnlyManifestItem[0]);
        Arrays.sort(rv, new Comparator<TestOnlyManifestItem>() {
            public int compare(TestOnlyManifestItem lhs, TestOnlyManifestItem rhs) {
                return lhs.path.compareTo(rhs.path);
            }
        });
        return rv;
    }
}
