package com.openfeint.internal.request;

import com.openfeint.internal.OpenFeintInternal;
import com.sgstudios.MovieTrivia.R;
import java.io.IOException;

public abstract class CompressedBlobDownloadRequest extends DownloadRequest {
    /* access modifiers changed from: protected */
    public abstract void onSuccessDecompress(byte[] bArr);

    /* access modifiers changed from: protected */
    public final void onSuccess(byte[] body) {
        try {
            onSuccessDecompress(Compression.decompress(body));
        } catch (IOException e) {
            onFailure(OpenFeintInternal.getRString(R.string.of_io_exception_on_download));
        }
    }
}
