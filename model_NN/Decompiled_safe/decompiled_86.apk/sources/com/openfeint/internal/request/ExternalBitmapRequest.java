package com.openfeint.internal.request;

public class ExternalBitmapRequest extends BitmapRequest {
    private String mURL;

    public ExternalBitmapRequest(String url) {
        this.mURL = url;
    }

    public boolean signed() {
        return false;
    }

    public String url() {
        return this.mURL;
    }

    public String path() {
        return "";
    }
}
