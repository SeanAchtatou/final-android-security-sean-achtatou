package com.openfeint.internal.request;

public interface IRawRequestDelegate {
    void onResponse(int i, String str);
}
