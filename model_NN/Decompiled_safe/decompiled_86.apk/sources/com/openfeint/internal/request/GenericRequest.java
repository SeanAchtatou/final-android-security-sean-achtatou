package com.openfeint.internal.request;

import java.util.Map;
import org.apache.http.params.HttpConnectionParams;

public class GenericRequest extends JSONContentRequest {
    private IRawRequestDelegate mDelegate;
    final String mMethod;
    final String mPath;
    private int mRetries = super.numRetries();
    private long mTimeout = super.timeout();

    public long timeout() {
        return this.mTimeout;
    }

    public int numRetries() {
        return this.mRetries;
    }

    public GenericRequest(String path, String method, Map<String, Object> args, Map<String, Object> httpParams, IRawRequestDelegate delegate) {
        if (httpParams != null) {
            for (Map.Entry<String, Object> e : httpParams.entrySet()) {
                String k = (String) e.getKey();
                int i = Integer.parseInt(e.getValue().toString());
                if (k.equals("connectionTimeout")) {
                    HttpConnectionParams.setConnectionTimeout(getHttpParams(), i);
                } else if (k.equals("socketTimeout")) {
                    HttpConnectionParams.setSoTimeout(getHttpParams(), i);
                } else if (k.equals("lingerTimeout")) {
                    HttpConnectionParams.setLinger(getHttpParams(), i);
                } else if (k.equals("timeout")) {
                    this.mTimeout = (long) i;
                } else if (k.equals("retries")) {
                    this.mRetries = i;
                }
            }
        }
        OrderedArgList argList = new OrderedArgList(args);
        argList.put("format", "json");
        setArgs(argList);
        this.mMethod = method;
        this.mPath = path;
        setDelegate(delegate);
    }

    public String method() {
        return this.mMethod;
    }

    public String path() {
        return this.mPath;
    }

    public void onResponse(int responseCode, byte[] body) {
        String respText;
        try {
            if (!isResponseJSON()) {
                respText = notJSONError(responseCode).generate();
            } else {
                respText = body != null ? new String(body) : "";
            }
            if (this.mDelegate != null) {
                this.mDelegate.onResponse(responseCode, respText);
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    public void setDelegate(IRawRequestDelegate mDelegate2) {
        this.mDelegate = mDelegate2;
    }
}
