package com.openfeint.internal;

import android.content.Context;
import android.content.res.Configuration;

public class Util11 {
    public static boolean isPad(Context ctx) {
        try {
            int mask = Configuration.class.getField("SCREENLAYOUT_SIZE_XLARGE").getInt(null);
            if ((ctx.getResources().getConfiguration().screenLayout & mask) == mask) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }
}
