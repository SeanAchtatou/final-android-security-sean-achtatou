package android.support.v4.app;

import android.view.animation.Animation;

class r implements Animation.AnimationListener {
    final /* synthetic */ Fragment a;
    final /* synthetic */ p b;

    r(p pVar, Fragment fragment) {
        this.b = pVar;
        this.a = fragment;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.p.a(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.p.a(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.p.a(android.support.v4.app.Fragment, int, int, int, boolean):void */
    public void onAnimationEnd(Animation animation) {
        if (this.a.b != null) {
            this.a.b = null;
            this.b.a(this.a, this.a.c, 0, 0, false);
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }
}
