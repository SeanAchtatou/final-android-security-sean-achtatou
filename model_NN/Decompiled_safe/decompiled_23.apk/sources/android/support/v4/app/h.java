package android.support.v4.app;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

public class h extends Activity {
    final Handler a = new i(this);
    final p b = new p();
    final m c = new j(this);
    boolean d;
    boolean e;
    boolean f;
    boolean g;
    boolean h;
    boolean i;
    boolean j;
    boolean k;
    HashMap l;
    aa m;

    private static String a(View view) {
        String str;
        char c2 = 'F';
        char c3 = '.';
        StringBuilder sb = new StringBuilder(128);
        sb.append(view.getClass().getName());
        sb.append('{');
        sb.append(Integer.toHexString(System.identityHashCode(view)));
        sb.append(' ');
        switch (view.getVisibility()) {
            case 0:
                sb.append('V');
                break;
            case 4:
                sb.append('I');
                break;
            case 8:
                sb.append('G');
                break;
            default:
                sb.append('.');
                break;
        }
        sb.append(view.isFocusable() ? 'F' : '.');
        sb.append(view.isEnabled() ? 'E' : '.');
        sb.append(view.willNotDraw() ? '.' : 'D');
        sb.append(view.isHorizontalScrollBarEnabled() ? 'H' : '.');
        sb.append(view.isVerticalScrollBarEnabled() ? 'V' : '.');
        sb.append(view.isClickable() ? 'C' : '.');
        sb.append(view.isLongClickable() ? 'L' : '.');
        sb.append(' ');
        if (!view.isFocused()) {
            c2 = '.';
        }
        sb.append(c2);
        sb.append(view.isSelected() ? 'S' : '.');
        if (view.isPressed()) {
            c3 = 'P';
        }
        sb.append(c3);
        sb.append(' ');
        sb.append(view.getLeft());
        sb.append(',');
        sb.append(view.getTop());
        sb.append('-');
        sb.append(view.getRight());
        sb.append(',');
        sb.append(view.getBottom());
        int id = view.getId();
        if (id != -1) {
            sb.append(" #");
            sb.append(Integer.toHexString(id));
            Resources resources = view.getResources();
            if (!(id == 0 || resources == null)) {
                switch (-16777216 & id) {
                    case 16777216:
                        str = "android";
                        String resourceTypeName = resources.getResourceTypeName(id);
                        String resourceEntryName = resources.getResourceEntryName(id);
                        sb.append(" ");
                        sb.append(str);
                        sb.append(":");
                        sb.append(resourceTypeName);
                        sb.append("/");
                        sb.append(resourceEntryName);
                        break;
                    case 2130706432:
                        str = "app";
                        String resourceTypeName2 = resources.getResourceTypeName(id);
                        String resourceEntryName2 = resources.getResourceEntryName(id);
                        sb.append(" ");
                        sb.append(str);
                        sb.append(":");
                        sb.append(resourceTypeName2);
                        sb.append("/");
                        sb.append(resourceEntryName2);
                        break;
                    default:
                        try {
                            str = resources.getResourcePackageName(id);
                            String resourceTypeName22 = resources.getResourceTypeName(id);
                            String resourceEntryName22 = resources.getResourceEntryName(id);
                            sb.append(" ");
                            sb.append(str);
                            sb.append(":");
                            sb.append(resourceTypeName22);
                            sb.append("/");
                            sb.append(resourceEntryName22);
                            break;
                        } catch (Resources.NotFoundException e2) {
                            break;
                        }
                }
            }
        }
        sb.append("}");
        return sb.toString();
    }

    private void a(String str, PrintWriter printWriter, View view) {
        ViewGroup viewGroup;
        int childCount;
        printWriter.print(str);
        if (view == null) {
            printWriter.println("null");
            return;
        }
        printWriter.println(a(view));
        if ((view instanceof ViewGroup) && (childCount = (viewGroup = (ViewGroup) view).getChildCount()) > 0) {
            String str2 = str + "  ";
            for (int i2 = 0; i2 < childCount; i2++) {
                a(str2, printWriter, viewGroup.getChildAt(i2));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public aa a(String str, boolean z, boolean z2) {
        if (this.l == null) {
            this.l = new HashMap();
        }
        aa aaVar = (aa) this.l.get(str);
        if (aaVar != null) {
            aaVar.a(this);
            return aaVar;
        } else if (!z2) {
            return aaVar;
        } else {
            aa aaVar2 = new aa(str, this, z);
            this.l.put(str, aaVar2);
            return aaVar2;
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.b.m();
    }

    public void a(Fragment fragment) {
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        aa aaVar;
        if (this.l != null && (aaVar = (aa) this.l.get(str)) != null && !aaVar.g) {
            aaVar.h();
            this.l.remove(str);
        }
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        if (Build.VERSION.SDK_INT >= 11) {
        }
        printWriter.print(str);
        printWriter.print("Local FragmentActivity ");
        printWriter.print(Integer.toHexString(System.identityHashCode(this)));
        printWriter.println(" State:");
        String str2 = str + "  ";
        printWriter.print(str2);
        printWriter.print("mCreated=");
        printWriter.print(this.d);
        printWriter.print("mResumed=");
        printWriter.print(this.e);
        printWriter.print(" mStopped=");
        printWriter.print(this.f);
        printWriter.print(" mReallyStopped=");
        printWriter.println(this.g);
        printWriter.print(str2);
        printWriter.print("mLoadersStarted=");
        printWriter.println(this.k);
        if (this.m != null) {
            printWriter.print(str);
            printWriter.print("Loader Manager ");
            printWriter.print(Integer.toHexString(System.identityHashCode(this.m)));
            printWriter.println(":");
            this.m.a(str + "  ", fileDescriptor, printWriter, strArr);
        }
        this.b.a(str, fileDescriptor, printWriter, strArr);
        printWriter.print(str);
        printWriter.println("View Hierarchy:");
        a(str + "  ", printWriter, getWindow().getDecorView());
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        if (!this.g) {
            this.g = true;
            this.h = z;
            this.a.removeMessages(1);
            d();
        }
    }

    public Object b() {
        return null;
    }

    public void c() {
        if (Build.VERSION.SDK_INT >= 11) {
            a.a(this);
        } else {
            this.i = true;
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        if (this.k) {
            this.k = false;
            if (this.m != null) {
                if (!this.h) {
                    this.m.c();
                } else {
                    this.m.d();
                }
            }
        }
        this.b.p();
    }

    public n e() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        this.b.i();
        int i4 = i2 >> 16;
        if (i4 != 0) {
            int i5 = i4 - 1;
            if (this.b.f == null || i5 < 0 || i5 >= this.b.f.size()) {
                Log.w("FragmentActivity", "Activity result fragment index out of range: 0x" + Integer.toHexString(i2));
                return;
            }
            Fragment fragment = (Fragment) this.b.f.get(i5);
            if (fragment == null) {
                Log.w("FragmentActivity", "Activity result no fragment exists for index: 0x" + Integer.toHexString(i2));
            } else {
                fragment.a(65535 & i2, i3, intent);
            }
        } else {
            super.onActivityResult(i2, i3, intent);
        }
    }

    public void onBackPressed() {
        if (!this.b.c()) {
            finish();
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.b.a(configuration);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        this.b.a(this, this.c, (Fragment) null);
        if (getLayoutInflater().getFactory() == null) {
            getLayoutInflater().setFactory(this);
        }
        super.onCreate(bundle);
        l lVar = (l) getLastNonConfigurationInstance();
        if (lVar != null) {
            this.l = lVar.e;
        }
        if (bundle != null) {
            this.b.a(bundle.getParcelable("android:support:fragments"), lVar != null ? lVar.d : null);
        }
        this.b.j();
    }

    public boolean onCreatePanelMenu(int i2, Menu menu) {
        if (i2 != 0) {
            return super.onCreatePanelMenu(i2, menu);
        }
        boolean onCreatePanelMenu = super.onCreatePanelMenu(i2, menu) | this.b.a(menu, getMenuInflater());
        if (Build.VERSION.SDK_INT >= 11) {
            return onCreatePanelMenu;
        }
        return true;
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:16:0x0064 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:19:0x006e */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r1v0, types: [android.view.View, java.lang.String] */
    /* JADX WARN: Type inference failed for: r1v1 */
    /* JADX WARN: Type inference failed for: r1v2 */
    /* JADX WARN: Type inference failed for: r1v3, types: [android.support.v4.app.Fragment, java.lang.Object] */
    /* JADX WARN: Type inference failed for: r1v24 */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.p.a(android.support.v4.app.Fragment, boolean):void
     arg types: [android.support.v4.app.Fragment, int]
     candidates:
      android.support.v4.app.p.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.p.a(int, android.support.v4.app.b):void
      android.support.v4.app.p.a(int, boolean):void
      android.support.v4.app.p.a(android.os.Parcelable, java.util.ArrayList):void
      android.support.v4.app.p.a(java.lang.Runnable, boolean):void
      android.support.v4.app.p.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.p.a(android.support.v4.app.Fragment, boolean):void */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Multi-variable type inference failed */
    public android.view.View onCreateView(java.lang.String r10, android.content.Context r11, android.util.AttributeSet r12) {
        /*
            r9 = this;
            r3 = 0
            r1 = 0
            r8 = 1
            r6 = -1
            java.lang.String r0 = "fragment"
            boolean r0 = r0.equals(r10)
            if (r0 != 0) goto L_0x0011
            android.view.View r0 = super.onCreateView(r10, r11, r12)
        L_0x0010:
            return r0
        L_0x0011:
            java.lang.String r0 = "class"
            java.lang.String r0 = r12.getAttributeValue(r1, r0)
            int[] r2 = android.support.v4.app.k.a
            android.content.res.TypedArray r4 = r11.obtainStyledAttributes(r12, r2)
            if (r0 != 0) goto L_0x0023
            java.lang.String r0 = r4.getString(r3)
        L_0x0023:
            int r2 = r4.getResourceId(r8, r6)
            r5 = 2
            java.lang.String r5 = r4.getString(r5)
            r4.recycle()
            if (r1 == 0) goto L_0x0035
            int r3 = r1.getId()
        L_0x0035:
            if (r3 != r6) goto L_0x005c
            if (r2 != r6) goto L_0x005c
            if (r5 != 0) goto L_0x005c
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = r12.getPositionDescription()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = ": Must specify unique android:id, android:tag, or have a parent with an id for "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        L_0x005c:
            if (r2 == r6) goto L_0x0064
            android.support.v4.app.p r1 = r9.b
            android.support.v4.app.Fragment r1 = r1.a(r2)
        L_0x0064:
            if (r1 != 0) goto L_0x006e
            if (r5 == 0) goto L_0x006e
            android.support.v4.app.p r1 = r9.b
            android.support.v4.app.Fragment r1 = r1.a(r5)
        L_0x006e:
            if (r1 != 0) goto L_0x0078
            if (r3 == r6) goto L_0x0078
            android.support.v4.app.p r1 = r9.b
            android.support.v4.app.Fragment r1 = r1.a(r3)
        L_0x0078:
            boolean r4 = android.support.v4.app.p.a
            if (r4 == 0) goto L_0x00ac
            java.lang.String r4 = "FragmentActivity"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "onCreateView: id=0x"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = java.lang.Integer.toHexString(r2)
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = " fname="
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.StringBuilder r6 = r6.append(r0)
            java.lang.String r7 = " existing="
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.StringBuilder r6 = r6.append(r1)
            java.lang.String r6 = r6.toString()
            android.util.Log.v(r4, r6)
        L_0x00ac:
            if (r1 != 0) goto L_0x00f3
            android.support.v4.app.Fragment r4 = android.support.v4.app.Fragment.a(r9, r0)
            r4.o = r8
            if (r2 == 0) goto L_0x00f1
            r1 = r2
        L_0x00b7:
            r4.w = r1
            r4.x = r3
            r4.y = r5
            r4.p = r8
            android.support.v4.app.p r1 = r9.b
            r4.s = r1
            android.os.Bundle r1 = r4.d
            r4.a(r9, r12, r1)
            android.support.v4.app.p r1 = r9.b
            r1.a(r4, r8)
            r1 = r4
        L_0x00ce:
            android.view.View r3 = r1.I
            if (r3 != 0) goto L_0x014f
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Fragment "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r2 = " did not create a view."
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        L_0x00f1:
            r1 = r3
            goto L_0x00b7
        L_0x00f3:
            boolean r4 = r1.p
            if (r4 == 0) goto L_0x013e
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r6 = r12.getPositionDescription()
            java.lang.StringBuilder r4 = r4.append(r6)
            java.lang.String r6 = ": Duplicate id 0x"
            java.lang.StringBuilder r4 = r4.append(r6)
            java.lang.String r2 = java.lang.Integer.toHexString(r2)
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r4 = ", tag "
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.String r4 = ", or parent id 0x"
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r3 = java.lang.Integer.toHexString(r3)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = " with another fragment for "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        L_0x013e:
            r1.p = r8
            boolean r3 = r1.C
            if (r3 != 0) goto L_0x0149
            android.os.Bundle r3 = r1.d
            r1.a(r9, r12, r3)
        L_0x0149:
            android.support.v4.app.p r3 = r9.b
            r3.b(r1)
            goto L_0x00ce
        L_0x014f:
            if (r2 == 0) goto L_0x0156
            android.view.View r0 = r1.I
            r0.setId(r2)
        L_0x0156:
            android.view.View r0 = r1.I
            java.lang.Object r0 = r0.getTag()
            if (r0 != 0) goto L_0x0163
            android.view.View r0 = r1.I
            r0.setTag(r5)
        L_0x0163:
            android.view.View r0 = r1.I
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.app.h.onCreateView(java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View");
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        a(false);
        this.b.r();
        if (this.m != null) {
            this.m.h();
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (Build.VERSION.SDK_INT >= 5 || i2 != 4 || keyEvent.getRepeatCount() != 0) {
            return super.onKeyDown(i2, keyEvent);
        }
        onBackPressed();
        return true;
    }

    public void onLowMemory() {
        super.onLowMemory();
        this.b.s();
    }

    public boolean onMenuItemSelected(int i2, MenuItem menuItem) {
        if (super.onMenuItemSelected(i2, menuItem)) {
            return true;
        }
        switch (i2) {
            case 0:
                return this.b.a(menuItem);
            case 6:
                return this.b.b(menuItem);
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.b.i();
    }

    public void onPanelClosed(int i2, Menu menu) {
        switch (i2) {
            case 0:
                this.b.b(menu);
                break;
        }
        super.onPanelClosed(i2, menu);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.e = false;
        if (this.a.hasMessages(2)) {
            this.a.removeMessages(2);
            a();
        }
        this.b.n();
    }

    /* access modifiers changed from: protected */
    public void onPostResume() {
        super.onPostResume();
        this.a.removeMessages(2);
        a();
        this.b.e();
    }

    public boolean onPreparePanel(int i2, View view, Menu menu) {
        if (i2 != 0 || menu == null) {
            return super.onPreparePanel(i2, view, menu);
        }
        if (this.i) {
            this.i = false;
            menu.clear();
            onCreatePanelMenu(i2, menu);
        }
        return (super.onPreparePanel(i2, view, menu) || this.b.a(menu)) && menu.hasVisibleItems();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.a.sendEmptyMessage(2);
        this.e = true;
        this.b.e();
    }

    public final Object onRetainNonConfigurationInstance() {
        int i2 = 0;
        if (this.f) {
            a(true);
        }
        Object b2 = b();
        ArrayList g2 = this.b.g();
        if (this.l != null) {
            aa[] aaVarArr = new aa[this.l.size()];
            this.l.values().toArray(aaVarArr);
            if (aaVarArr != null) {
                int i3 = 0;
                while (i2 < aaVarArr.length) {
                    aa aaVar = aaVarArr[i2];
                    if (aaVar.g) {
                        i3 = 1;
                    } else {
                        aaVar.h();
                        this.l.remove(aaVar.d);
                    }
                    i2++;
                }
                i2 = i3;
            }
        }
        if (g2 == null && i2 == 0 && b2 == null) {
            return null;
        }
        l lVar = new l();
        lVar.a = null;
        lVar.b = b2;
        lVar.c = null;
        lVar.d = g2;
        lVar.e = this.l;
        return lVar;
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        Parcelable h2 = this.b.h();
        if (h2 != null) {
            bundle.putParcelable("android:support:fragments", h2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.h.a(java.lang.String, boolean, boolean):android.support.v4.app.aa
     arg types: [?[OBJECT, ARRAY], boolean, int]
     candidates:
      android.support.v4.app.h.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.h.a(java.lang.String, boolean, boolean):android.support.v4.app.aa */
    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.f = false;
        this.g = false;
        this.a.removeMessages(1);
        if (!this.d) {
            this.d = true;
            this.b.k();
        }
        this.b.i();
        this.b.e();
        if (!this.k) {
            this.k = true;
            if (this.m != null) {
                this.m.b();
            } else if (!this.j) {
                this.m = a((String) null, this.k, false);
                if (this.m != null && !this.m.f) {
                    this.m.b();
                }
            }
            this.j = true;
        }
        this.b.l();
        if (this.l != null) {
            aa[] aaVarArr = new aa[this.l.size()];
            this.l.values().toArray(aaVarArr);
            if (aaVarArr != null) {
                for (aa aaVar : aaVarArr) {
                    aaVar.e();
                    aaVar.g();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.f = true;
        this.a.sendEmptyMessage(1);
        this.b.o();
    }

    public void startActivityForResult(Intent intent, int i2) {
        if (i2 == -1 || (-65536 & i2) == 0) {
            super.startActivityForResult(intent, i2);
            return;
        }
        throw new IllegalArgumentException("Can only use lower 16 bits for requestCode");
    }
}
