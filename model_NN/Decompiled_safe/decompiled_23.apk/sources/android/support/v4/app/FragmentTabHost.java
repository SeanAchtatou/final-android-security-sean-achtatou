package android.support.v4.app;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import android.widget.TabHost;
import java.util.ArrayList;

public class FragmentTabHost extends TabHost implements TabHost.OnTabChangeListener {
    private final ArrayList a;
    private Context b;
    private n c;
    private int d;
    private TabHost.OnTabChangeListener e;
    private w f;
    private boolean g;

    class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = new v();
        String a;

        private SavedState(Parcel parcel) {
            super(parcel);
            this.a = parcel.readString();
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public String toString() {
            return "FragmentTabHost.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " curTab=" + this.a + "}";
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeString(this.a);
        }
    }

    private x a(String str, x xVar) {
        w wVar = null;
        int i = 0;
        while (i < this.a.size()) {
            w wVar2 = (w) this.a.get(i);
            if (!wVar2.a.equals(str)) {
                wVar2 = wVar;
            }
            i++;
            wVar = wVar2;
        }
        if (wVar == null) {
            throw new IllegalStateException("No tab known for tag " + str);
        }
        if (this.f != wVar) {
            if (xVar == null) {
                xVar = this.c.a();
            }
            if (!(this.f == null || this.f.d == null)) {
                xVar.b(this.f.d);
            }
            if (wVar != null) {
                if (wVar.d == null) {
                    Fragment unused = wVar.d = Fragment.a(this.b, wVar.b.getName(), wVar.c);
                    xVar.a(this.d, wVar.d, wVar.a);
                } else {
                    xVar.c(wVar.d);
                }
            }
            this.f = wVar;
        }
        return xVar;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        String currentTabTag = getCurrentTabTag();
        x xVar = null;
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.a.size()) {
                break;
            }
            w wVar = (w) this.a.get(i2);
            Fragment unused = wVar.d = this.c.a(wVar.a);
            if (wVar.d != null && !wVar.d.f()) {
                if (wVar.a.equals(currentTabTag)) {
                    this.f = wVar;
                } else {
                    if (xVar == null) {
                        xVar = this.c.a();
                    }
                    xVar.b(wVar.d);
                }
            }
            i = i2 + 1;
        }
        this.g = true;
        x a2 = a(currentTabTag, xVar);
        if (a2 != null) {
            a2.a();
            this.c.b();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.g = false;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        setCurrentTabByTag(savedState.a);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.a = getCurrentTabTag();
        return savedState;
    }

    public void onTabChanged(String str) {
        x a2;
        if (this.g && (a2 = a(str, null)) != null) {
            a2.a();
        }
        if (this.e != null) {
            this.e.onTabChanged(str);
        }
    }

    public void setOnTabChangedListener(TabHost.OnTabChangeListener onTabChangeListener) {
        this.e = onTabChangeListener;
    }

    @Deprecated
    public void setup() {
        throw new IllegalStateException("Must call setup() that takes a Context and FragmentManager");
    }
}
