package android.support.v4.app;

import android.support.v4.c.a;
import android.support.v4.c.c;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.PrintWriter;

class aa extends y {
    static boolean a = false;
    final c b = new c();
    final c c = new c();
    final String d;
    h e;
    boolean f;
    boolean g;

    aa(String str, h hVar, boolean z) {
        this.d = str;
        this.e = hVar;
        this.f = z;
    }

    /* access modifiers changed from: package-private */
    public void a(h hVar) {
        this.e = hVar;
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        if (this.b.a() > 0) {
            printWriter.print(str);
            printWriter.println("Active Loaders:");
            String str2 = str + "    ";
            for (int i = 0; i < this.b.a(); i++) {
                ab abVar = (ab) this.b.b(i);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(this.b.a(i));
                printWriter.print(": ");
                printWriter.println(abVar.toString());
                abVar.a(str2, fileDescriptor, printWriter, strArr);
            }
        }
        if (this.c.a() > 0) {
            printWriter.print(str);
            printWriter.println("Inactive Loaders:");
            String str3 = str + "    ";
            for (int i2 = 0; i2 < this.c.a(); i2++) {
                ab abVar2 = (ab) this.c.b(i2);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(this.c.a(i2));
                printWriter.print(": ");
                printWriter.println(abVar2.toString());
                abVar2.a(str3, fileDescriptor, printWriter, strArr);
            }
        }
    }

    public boolean a() {
        int a2 = this.b.a();
        boolean z = false;
        for (int i = 0; i < a2; i++) {
            ab abVar = (ab) this.b.b(i);
            z |= abVar.h && !abVar.f;
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (a) {
            Log.v("LoaderManager", "Starting in " + this);
        }
        if (this.f) {
            RuntimeException runtimeException = new RuntimeException("here");
            runtimeException.fillInStackTrace();
            Log.w("LoaderManager", "Called doStart when already started: " + this, runtimeException);
            return;
        }
        this.f = true;
        for (int a2 = this.b.a() - 1; a2 >= 0; a2--) {
            ((ab) this.b.b(a2)).a();
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (a) {
            Log.v("LoaderManager", "Stopping in " + this);
        }
        if (!this.f) {
            RuntimeException runtimeException = new RuntimeException("here");
            runtimeException.fillInStackTrace();
            Log.w("LoaderManager", "Called doStop when not started: " + this, runtimeException);
            return;
        }
        for (int a2 = this.b.a() - 1; a2 >= 0; a2--) {
            ((ab) this.b.b(a2)).e();
        }
        this.f = false;
    }

    /* access modifiers changed from: package-private */
    public void d() {
        if (a) {
            Log.v("LoaderManager", "Retaining in " + this);
        }
        if (!this.f) {
            RuntimeException runtimeException = new RuntimeException("here");
            runtimeException.fillInStackTrace();
            Log.w("LoaderManager", "Called doRetain when not started: " + this, runtimeException);
            return;
        }
        this.g = true;
        this.f = false;
        for (int a2 = this.b.a() - 1; a2 >= 0; a2--) {
            ((ab) this.b.b(a2)).b();
        }
    }

    /* access modifiers changed from: package-private */
    public void e() {
        if (this.g) {
            if (a) {
                Log.v("LoaderManager", "Finished Retaining in " + this);
            }
            this.g = false;
            for (int a2 = this.b.a() - 1; a2 >= 0; a2--) {
                ((ab) this.b.b(a2)).c();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void f() {
        for (int a2 = this.b.a() - 1; a2 >= 0; a2--) {
            ((ab) this.b.b(a2)).k = true;
        }
    }

    /* access modifiers changed from: package-private */
    public void g() {
        for (int a2 = this.b.a() - 1; a2 >= 0; a2--) {
            ((ab) this.b.b(a2)).d();
        }
    }

    /* access modifiers changed from: package-private */
    public void h() {
        if (!this.g) {
            if (a) {
                Log.v("LoaderManager", "Destroying Active in " + this);
            }
            for (int a2 = this.b.a() - 1; a2 >= 0; a2--) {
                ((ab) this.b.b(a2)).f();
            }
        }
        if (a) {
            Log.v("LoaderManager", "Destroying Inactive in " + this);
        }
        for (int a3 = this.c.a() - 1; a3 >= 0; a3--) {
            ((ab) this.c.b(a3)).f();
        }
        this.c.b();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("LoaderManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        a.a(this.e, sb);
        sb.append("}}");
        return sb.toString();
    }
}
