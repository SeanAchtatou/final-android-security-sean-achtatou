package android.support.v4.view;

import android.view.View;
import java.util.Comparator;

class am implements Comparator {
    am() {
    }

    /* renamed from: a */
    public int compare(View view, View view2) {
        ag agVar = (ag) view.getLayoutParams();
        ag agVar2 = (ag) view2.getLayoutParams();
        return agVar.a != agVar2.a ? agVar.a ? 1 : -1 : agVar.e - agVar2.e;
    }
}
