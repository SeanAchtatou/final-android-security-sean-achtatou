package android.support.v4.view;

import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.d.a;
import android.util.AttributeSet;
import android.util.Log;
import android.view.FocusFinder;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.Interpolator;
import android.widget.Scroller;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ViewPager extends ViewGroup {
    /* access modifiers changed from: private */
    public static final int[] a = {16842931};
    private static final am ac = new am();
    private static final Comparator b = new ac();
    private static final Interpolator c = new ad();
    private boolean A;
    private int B;
    private int C;
    private int D;
    private float E;
    private float F;
    private float G;
    private float H;
    private int I;
    private VelocityTracker J;
    private int K;
    private int L;
    private int M;
    private int N;
    private boolean O;
    private a P;
    private a Q;
    private boolean R;
    private boolean S;
    private boolean T;
    private int U;
    private ai V;
    private ai W;
    private ah X;
    private aj Y;
    private Method Z;
    private int aa;
    private ArrayList ab;
    private final Runnable ad;
    private int ae;
    private final ArrayList d;
    private final af e;
    private final Rect f;
    private k g;
    private int h;
    private int i;
    private Parcelable j;
    private ClassLoader k;
    private Scroller l;
    private ak m;
    private int n;
    private Drawable o;
    private int p;
    private int q;
    private float r;
    private float s;
    private int t;
    private int u;
    private boolean v;
    private boolean w;
    private boolean x;
    private int y;
    private boolean z;

    public class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = android.support.v4.b.a.a(new al());
        int a;
        Parcelable b;
        ClassLoader c;

        SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel);
            classLoader = classLoader == null ? getClass().getClassLoader() : classLoader;
            this.a = parcel.readInt();
            this.b = parcel.readParcelable(classLoader);
            this.c = classLoader;
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public String toString() {
            return "FragmentPager.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " position=" + this.a + "}";
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.a);
            parcel.writeParcelable(this.b, i);
        }
    }

    private int a(int i2, float f2, int i3, int i4) {
        if (Math.abs(i4) <= this.M || Math.abs(i3) <= this.K) {
            i2 = (int) ((i2 >= this.h ? 0.4f : 0.6f) + ((float) i2) + f2);
        } else if (i3 <= 0) {
            i2++;
        }
        return this.d.size() > 0 ? Math.max(((af) this.d.get(0)).b, Math.min(i2, ((af) this.d.get(this.d.size() - 1)).b)) : i2;
    }

    private Rect a(Rect rect, View view) {
        Rect rect2 = rect == null ? new Rect() : rect;
        if (view == null) {
            rect2.set(0, 0, 0, 0);
            return rect2;
        }
        rect2.left = view.getLeft();
        rect2.right = view.getRight();
        rect2.top = view.getTop();
        rect2.bottom = view.getBottom();
        ViewParent parent = view.getParent();
        while ((parent instanceof ViewGroup) && parent != this) {
            ViewGroup viewGroup = (ViewGroup) parent;
            rect2.left += viewGroup.getLeft();
            rect2.right += viewGroup.getRight();
            rect2.top += viewGroup.getTop();
            rect2.bottom += viewGroup.getBottom();
            parent = viewGroup.getParent();
        }
        return rect2;
    }

    private void a(int i2, int i3, int i4, int i5) {
        if (i3 <= 0 || this.d.isEmpty()) {
            af b2 = b(this.h);
            int min = (int) ((b2 != null ? Math.min(b2.e, this.s) : 0.0f) * ((float) i2));
            if (min != getScrollX()) {
                a(false);
                scrollTo(min, getScrollY());
                return;
            }
            return;
        }
        int scrollX = (int) (((float) (i2 + i4)) * (((float) getScrollX()) / ((float) (i3 + i5))));
        scrollTo(scrollX, getScrollY());
        if (!this.l.isFinished()) {
            this.l.startScroll(scrollX, 0, (int) (b(this.h).e * ((float) i2)), 0, this.l.getDuration() - this.l.timePassed());
        }
    }

    private void a(int i2, boolean z2, int i3, boolean z3) {
        int i4;
        af b2 = b(i2);
        if (b2 != null) {
            i4 = (int) (Math.max(this.r, Math.min(b2.e, this.s)) * ((float) getWidth()));
        } else {
            i4 = 0;
        }
        if (z2) {
            a(i4, 0, i3);
            if (z3 && this.V != null) {
                this.V.a(i2);
            }
            if (z3 && this.W != null) {
                this.W.a(i2);
                return;
            }
            return;
        }
        if (z3 && this.V != null) {
            this.V.a(i2);
        }
        if (z3 && this.W != null) {
            this.W.a(i2);
        }
        a(false);
        scrollTo(i4, 0);
    }

    private void a(af afVar, int i2, af afVar2) {
        af afVar3;
        af afVar4;
        int a2 = this.g.a();
        int width = getWidth();
        float f2 = width > 0 ? ((float) this.n) / ((float) width) : 0.0f;
        if (afVar2 != null) {
            int i3 = afVar2.b;
            if (i3 < afVar.b) {
                float f3 = afVar2.e + afVar2.d + f2;
                int i4 = i3 + 1;
                int i5 = 0;
                while (i4 <= afVar.b && i5 < this.d.size()) {
                    Object obj = this.d.get(i5);
                    while (true) {
                        afVar4 = (af) obj;
                        if (i4 > afVar4.b && i5 < this.d.size() - 1) {
                            i5++;
                            obj = this.d.get(i5);
                        }
                    }
                    while (i4 < afVar4.b) {
                        f3 += this.g.a(i4) + f2;
                        i4++;
                    }
                    afVar4.e = f3;
                    f3 += afVar4.d + f2;
                    i4++;
                }
            } else if (i3 > afVar.b) {
                int size = this.d.size() - 1;
                float f4 = afVar2.e;
                int i6 = i3 - 1;
                while (i6 >= afVar.b && size >= 0) {
                    Object obj2 = this.d.get(size);
                    while (true) {
                        afVar3 = (af) obj2;
                        if (i6 < afVar3.b && size > 0) {
                            size--;
                            obj2 = this.d.get(size);
                        }
                    }
                    while (i6 > afVar3.b) {
                        f4 -= this.g.a(i6) + f2;
                        i6--;
                    }
                    f4 -= afVar3.d + f2;
                    afVar3.e = f4;
                    i6--;
                }
            }
        }
        int size2 = this.d.size();
        float f5 = afVar.e;
        int i7 = afVar.b - 1;
        this.r = afVar.b == 0 ? afVar.e : -3.4028235E38f;
        this.s = afVar.b == a2 + -1 ? (afVar.e + afVar.d) - 1.0f : Float.MAX_VALUE;
        for (int i8 = i2 - 1; i8 >= 0; i8--) {
            af afVar5 = (af) this.d.get(i8);
            float f6 = f5;
            while (i7 > afVar5.b) {
                f6 -= this.g.a(i7) + f2;
                i7--;
            }
            f5 = f6 - (afVar5.d + f2);
            afVar5.e = f5;
            if (afVar5.b == 0) {
                this.r = f5;
            }
            i7--;
        }
        float f7 = afVar.e + afVar.d + f2;
        int i9 = afVar.b + 1;
        for (int i10 = i2 + 1; i10 < size2; i10++) {
            af afVar6 = (af) this.d.get(i10);
            float f8 = f7;
            while (i9 < afVar6.b) {
                f8 = this.g.a(i9) + f2 + f8;
                i9++;
            }
            if (afVar6.b == a2 - 1) {
                this.s = (afVar6.d + f8) - 1.0f;
            }
            afVar6.e = f8;
            f7 = f8 + afVar6.d + f2;
            i9++;
        }
        this.S = false;
    }

    private void a(MotionEvent motionEvent) {
        int a2 = f.a(motionEvent);
        if (f.b(motionEvent, a2) == this.I) {
            int i2 = a2 == 0 ? 1 : 0;
            this.E = f.c(motionEvent, i2);
            this.I = f.b(motionEvent, i2);
            if (this.J != null) {
                this.J.clear();
            }
        }
    }

    private void a(boolean z2) {
        boolean z3 = this.ae == 2;
        if (z3) {
            setScrollingCacheEnabled(false);
            this.l.abortAnimation();
            int scrollX = getScrollX();
            int scrollY = getScrollY();
            int currX = this.l.getCurrX();
            int currY = this.l.getCurrY();
            if (!(scrollX == currX && scrollY == currY)) {
                scrollTo(currX, currY);
            }
        }
        this.x = false;
        boolean z4 = z3;
        for (int i2 = 0; i2 < this.d.size(); i2++) {
            af afVar = (af) this.d.get(i2);
            if (afVar.c) {
                afVar.c = false;
                z4 = true;
            }
        }
        if (!z4) {
            return;
        }
        if (z2) {
            q.a(this, this.ad);
        } else {
            this.ad.run();
        }
    }

    private boolean a(float f2, float f3) {
        return (f2 < ((float) this.C) && f3 > 0.0f) || (f2 > ((float) (getWidth() - this.C)) && f3 < 0.0f);
    }

    private void b(boolean z2) {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            q.a(getChildAt(i2), z2 ? 2 : 0, null);
        }
    }

    private boolean b(float f2) {
        boolean z2;
        float f3;
        boolean z3 = true;
        boolean z4 = false;
        this.E = f2;
        float scrollX = ((float) getScrollX()) + (this.E - f2);
        int width = getWidth();
        float f4 = ((float) width) * this.r;
        float f5 = ((float) width) * this.s;
        af afVar = (af) this.d.get(0);
        af afVar2 = (af) this.d.get(this.d.size() - 1);
        if (afVar.b != 0) {
            f4 = afVar.e * ((float) width);
            z2 = false;
        } else {
            z2 = true;
        }
        if (afVar2.b != this.g.a() - 1) {
            f3 = afVar2.e * ((float) width);
            z3 = false;
        } else {
            f3 = f5;
        }
        if (scrollX < f4) {
            if (z2) {
                z4 = this.P.a(Math.abs(f4 - scrollX) / ((float) width));
            }
        } else if (scrollX > f3) {
            if (z3) {
                z4 = this.Q.a(Math.abs(scrollX - f3) / ((float) width));
            }
            f4 = f3;
        } else {
            f4 = scrollX;
        }
        this.E += f4 - ((float) ((int) f4));
        scrollTo((int) f4, getScrollY());
        d((int) f4);
        return z4;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, float, int):void
     arg types: [int, int, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.support.v4.view.af, int, android.support.v4.view.af):void
      android.support.v4.view.ViewPager.a(int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean):void
      android.support.v4.view.ViewPager.a(int, float, int):void */
    private boolean d(int i2) {
        if (this.d.size() == 0) {
            this.T = false;
            a(0, 0.0f, 0);
            if (this.T) {
                return false;
            }
            throw new IllegalStateException("onPageScrolled did not call superclass implementation");
        }
        af g2 = g();
        int width = getWidth();
        int i3 = this.n + width;
        float f2 = ((float) this.n) / ((float) width);
        int i4 = g2.b;
        float f3 = ((((float) i2) / ((float) width)) - g2.e) / (g2.d + f2);
        this.T = false;
        a(i4, f3, (int) (((float) i3) * f3));
        if (this.T) {
            return true;
        }
        throw new IllegalStateException("onPageScrolled did not call superclass implementation");
    }

    private void f() {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < getChildCount()) {
                if (!((ag) getChildAt(i3).getLayoutParams()).a) {
                    removeViewAt(i3);
                    i3--;
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    private af g() {
        int i2;
        af afVar;
        int width = getWidth();
        float scrollX = width > 0 ? ((float) getScrollX()) / ((float) width) : 0.0f;
        float f2 = width > 0 ? ((float) this.n) / ((float) width) : 0.0f;
        float f3 = 0.0f;
        float f4 = 0.0f;
        int i3 = -1;
        int i4 = 0;
        boolean z2 = true;
        af afVar2 = null;
        while (i4 < this.d.size()) {
            af afVar3 = (af) this.d.get(i4);
            if (z2 || afVar3.b == i3 + 1) {
                af afVar4 = afVar3;
                i2 = i4;
                afVar = afVar4;
            } else {
                af afVar5 = this.e;
                afVar5.e = f3 + f4 + f2;
                afVar5.b = i3 + 1;
                afVar5.d = this.g.a(afVar5.b);
                af afVar6 = afVar5;
                i2 = i4 - 1;
                afVar = afVar6;
            }
            float f5 = afVar.e;
            float f6 = afVar.d + f5 + f2;
            if (!z2 && scrollX < f5) {
                return afVar2;
            }
            if (scrollX < f6 || i2 == this.d.size() - 1) {
                return afVar;
            }
            f4 = f5;
            i3 = afVar.b;
            z2 = false;
            f3 = afVar.d;
            afVar2 = afVar;
            i4 = i2 + 1;
        }
        return afVar2;
    }

    private void h() {
        this.z = false;
        this.A = false;
        if (this.J != null) {
            this.J.recycle();
            this.J = null;
        }
    }

    private void setScrollState(int i2) {
        if (this.ae != i2) {
            this.ae = i2;
            if (this.Y != null) {
                b(i2 != 0);
            }
            if (this.V != null) {
                this.V.b(i2);
            }
        }
    }

    private void setScrollingCacheEnabled(boolean z2) {
        if (this.w != z2) {
            this.w = z2;
        }
    }

    /* access modifiers changed from: package-private */
    public float a(float f2) {
        return (float) Math.sin((double) ((float) (((double) (f2 - 0.5f)) * 0.4712389167638204d)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.k.a(android.view.ViewGroup, int):java.lang.Object
     arg types: [android.support.v4.view.ViewPager, int]
     candidates:
      android.support.v4.view.k.a(android.view.View, int):java.lang.Object
      android.support.v4.view.k.a(android.os.Parcelable, java.lang.ClassLoader):void
      android.support.v4.view.k.a(android.view.View, java.lang.Object):boolean
      android.support.v4.view.k.a(android.view.ViewGroup, int):java.lang.Object */
    /* access modifiers changed from: package-private */
    public af a(int i2, int i3) {
        af afVar = new af();
        afVar.b = i2;
        afVar.a = this.g.a((ViewGroup) this, i2);
        afVar.d = this.g.a(i2);
        if (i3 < 0 || i3 >= this.d.size()) {
            this.d.add(afVar);
        } else {
            this.d.add(i3, afVar);
        }
        return afVar;
    }

    /* access modifiers changed from: package-private */
    public af a(View view) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.d.size()) {
                return null;
            }
            af afVar = (af) this.d.get(i3);
            if (this.g.a(view, afVar.a)) {
                return afVar;
            }
            i2 = i3 + 1;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.k.a(android.view.ViewGroup, int, java.lang.Object):void
     arg types: [android.support.v4.view.ViewPager, int, java.lang.Object]
     candidates:
      android.support.v4.view.k.a(android.view.View, int, java.lang.Object):void
      android.support.v4.view.k.a(android.view.ViewGroup, int, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, boolean):void
     arg types: [int, int, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.support.v4.view.af, int, android.support.v4.view.af):void
      android.support.v4.view.ViewPager.a(int, float, int):void
      android.support.v4.view.ViewPager.a(int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean):void */
    /* access modifiers changed from: package-private */
    public void a() {
        int i2;
        boolean z2;
        int i3;
        boolean z3;
        boolean z4 = this.d.size() < (this.y * 2) + 1 && this.d.size() < this.g.a();
        boolean z5 = false;
        int i4 = this.h;
        boolean z6 = z4;
        int i5 = 0;
        while (i5 < this.d.size()) {
            af afVar = (af) this.d.get(i5);
            int a2 = this.g.a(afVar.a);
            if (a2 == -1) {
                i2 = i5;
                z2 = z5;
                i3 = i4;
                z3 = z6;
            } else if (a2 == -2) {
                this.d.remove(i5);
                int i6 = i5 - 1;
                if (!z5) {
                    this.g.a((ViewGroup) this);
                    z5 = true;
                }
                this.g.a((ViewGroup) this, afVar.b, afVar.a);
                if (this.h == afVar.b) {
                    i2 = i6;
                    z2 = z5;
                    i3 = Math.max(0, Math.min(this.h, this.g.a() - 1));
                    z3 = true;
                } else {
                    i2 = i6;
                    z2 = z5;
                    i3 = i4;
                    z3 = true;
                }
            } else if (afVar.b != a2) {
                if (afVar.b == this.h) {
                    i4 = a2;
                }
                afVar.b = a2;
                i2 = i5;
                z2 = z5;
                i3 = i4;
                z3 = true;
            } else {
                i2 = i5;
                z2 = z5;
                i3 = i4;
                z3 = z6;
            }
            z6 = z3;
            i4 = i3;
            z5 = z2;
            i5 = i2 + 1;
        }
        if (z5) {
            this.g.b((ViewGroup) this);
        }
        Collections.sort(this.d, b);
        if (z6) {
            int childCount = getChildCount();
            for (int i7 = 0; i7 < childCount; i7++) {
                ag agVar = (ag) getChildAt(i7).getLayoutParams();
                if (!agVar.a) {
                    agVar.c = 0.0f;
                }
            }
            a(i4, false, true);
            requestLayout();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.k.b(android.view.ViewGroup, int, java.lang.Object):void
     arg types: [android.support.v4.view.ViewPager, int, java.lang.Object]
     candidates:
      android.support.v4.view.k.b(android.view.View, int, java.lang.Object):void
      android.support.v4.view.k.b(android.view.ViewGroup, int, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.k.a(android.view.ViewGroup, int, java.lang.Object):void
     arg types: [android.support.v4.view.ViewPager, int, java.lang.Object]
     candidates:
      android.support.v4.view.k.a(android.view.View, int, java.lang.Object):void
      android.support.v4.view.k.a(android.view.ViewGroup, int, java.lang.Object):void */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0058, code lost:
        if (r0.b == r14.h) goto L_0x005a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(int r15) {
        /*
            r14 = this;
            r0 = 0
            int r1 = r14.h
            if (r1 == r15) goto L_0x024a
            int r0 = r14.h
            android.support.v4.view.af r0 = r14.b(r0)
            r14.h = r15
            r1 = r0
        L_0x000e:
            android.support.v4.view.k r0 = r14.g
            if (r0 != 0) goto L_0x0013
        L_0x0012:
            return
        L_0x0013:
            boolean r0 = r14.x
            if (r0 != 0) goto L_0x0012
            android.os.IBinder r0 = r14.getWindowToken()
            if (r0 == 0) goto L_0x0012
            android.support.v4.view.k r0 = r14.g
            r0.a(r14)
            int r0 = r14.y
            r2 = 0
            int r3 = r14.h
            int r3 = r3 - r0
            int r7 = java.lang.Math.max(r2, r3)
            android.support.v4.view.k r2 = r14.g
            int r8 = r2.a()
            int r2 = r8 + -1
            int r3 = r14.h
            int r0 = r0 + r3
            int r9 = java.lang.Math.min(r2, r0)
            r3 = 0
            r0 = 0
            r2 = r0
        L_0x003e:
            java.util.ArrayList r0 = r14.d
            int r0 = r0.size()
            if (r2 >= r0) goto L_0x0247
            java.util.ArrayList r0 = r14.d
            java.lang.Object r0 = r0.get(r2)
            android.support.v4.view.af r0 = (android.support.v4.view.af) r0
            int r4 = r0.b
            int r5 = r14.h
            if (r4 < r5) goto L_0x011a
            int r4 = r0.b
            int r5 = r14.h
            if (r4 != r5) goto L_0x0247
        L_0x005a:
            if (r0 != 0) goto L_0x0244
            if (r8 <= 0) goto L_0x0244
            int r0 = r14.h
            android.support.v4.view.af r0 = r14.a(r0, r2)
            r6 = r0
        L_0x0065:
            if (r6 == 0) goto L_0x00bf
            r5 = 0
            int r4 = r2 + -1
            if (r4 < 0) goto L_0x011f
            java.util.ArrayList r0 = r14.d
            java.lang.Object r0 = r0.get(r4)
            android.support.v4.view.af r0 = (android.support.v4.view.af) r0
        L_0x0074:
            r3 = 1073741824(0x40000000, float:2.0)
            float r10 = r6.d
            float r10 = r3 - r10
            int r3 = r14.h
            int r3 = r3 + -1
            r12 = r3
            r3 = r5
            r5 = r12
            r13 = r4
            r4 = r2
            r2 = r13
        L_0x0084:
            if (r5 < 0) goto L_0x008e
            int r11 = (r3 > r10 ? 1 : (r3 == r10 ? 0 : -1))
            if (r11 < 0) goto L_0x014a
            if (r5 >= r7) goto L_0x014a
            if (r0 != 0) goto L_0x0122
        L_0x008e:
            float r3 = r6.d
            int r5 = r4 + 1
            r0 = 1073741824(0x40000000, float:2.0)
            int r0 = (r3 > r0 ? 1 : (r3 == r0 ? 0 : -1))
            if (r0 >= 0) goto L_0x00bc
            java.util.ArrayList r0 = r14.d
            int r0 = r0.size()
            if (r5 >= r0) goto L_0x017a
            java.util.ArrayList r0 = r14.d
            java.lang.Object r0 = r0.get(r5)
            android.support.v4.view.af r0 = (android.support.v4.view.af) r0
        L_0x00a8:
            int r2 = r14.h
            int r2 = r2 + 1
            r12 = r2
            r2 = r3
            r3 = r5
            r5 = r12
        L_0x00b0:
            if (r5 >= r8) goto L_0x00bc
            r7 = 1073741824(0x40000000, float:2.0)
            int r7 = (r2 > r7 ? 1 : (r2 == r7 ? 0 : -1))
            if (r7 < 0) goto L_0x01ad
            if (r5 <= r9) goto L_0x01ad
            if (r0 != 0) goto L_0x017d
        L_0x00bc:
            r14.a(r6, r4, r1)
        L_0x00bf:
            android.support.v4.view.k r1 = r14.g
            int r2 = r14.h
            if (r6 == 0) goto L_0x01ed
            java.lang.Object r0 = r6.a
        L_0x00c7:
            r1.b(r14, r2, r0)
            android.support.v4.view.k r0 = r14.g
            r0.b(r14)
            int r0 = r14.aa
            if (r0 == 0) goto L_0x01f0
            r0 = 1
            r2 = r0
        L_0x00d5:
            if (r2 == 0) goto L_0x00e2
            java.util.ArrayList r0 = r14.ab
            if (r0 != 0) goto L_0x01f4
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r14.ab = r0
        L_0x00e2:
            int r3 = r14.getChildCount()
            r0 = 0
            r1 = r0
        L_0x00e8:
            if (r1 >= r3) goto L_0x01fb
            android.view.View r4 = r14.getChildAt(r1)
            android.view.ViewGroup$LayoutParams r0 = r4.getLayoutParams()
            android.support.v4.view.ag r0 = (android.support.v4.view.ag) r0
            r0.f = r1
            boolean r5 = r0.a
            if (r5 != 0) goto L_0x010f
            float r5 = r0.c
            r6 = 0
            int r5 = (r5 > r6 ? 1 : (r5 == r6 ? 0 : -1))
            if (r5 != 0) goto L_0x010f
            android.support.v4.view.af r5 = r14.a(r4)
            if (r5 == 0) goto L_0x010f
            float r6 = r5.d
            r0.c = r6
            int r5 = r5.b
            r0.e = r5
        L_0x010f:
            if (r2 == 0) goto L_0x0116
            java.util.ArrayList r0 = r14.ab
            r0.add(r4)
        L_0x0116:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x00e8
        L_0x011a:
            int r0 = r2 + 1
            r2 = r0
            goto L_0x003e
        L_0x011f:
            r0 = 0
            goto L_0x0074
        L_0x0122:
            int r11 = r0.b
            if (r5 != r11) goto L_0x0144
            boolean r11 = r0.c
            if (r11 != 0) goto L_0x0144
            java.util.ArrayList r11 = r14.d
            r11.remove(r2)
            android.support.v4.view.k r11 = r14.g
            java.lang.Object r0 = r0.a
            r11.a(r14, r5, r0)
            int r2 = r2 + -1
            int r4 = r4 + -1
            if (r2 < 0) goto L_0x0148
            java.util.ArrayList r0 = r14.d
            java.lang.Object r0 = r0.get(r2)
            android.support.v4.view.af r0 = (android.support.v4.view.af) r0
        L_0x0144:
            int r5 = r5 + -1
            goto L_0x0084
        L_0x0148:
            r0 = 0
            goto L_0x0144
        L_0x014a:
            if (r0 == 0) goto L_0x0162
            int r11 = r0.b
            if (r5 != r11) goto L_0x0162
            float r0 = r0.d
            float r3 = r3 + r0
            int r2 = r2 + -1
            if (r2 < 0) goto L_0x0160
            java.util.ArrayList r0 = r14.d
            java.lang.Object r0 = r0.get(r2)
            android.support.v4.view.af r0 = (android.support.v4.view.af) r0
            goto L_0x0144
        L_0x0160:
            r0 = 0
            goto L_0x0144
        L_0x0162:
            int r0 = r2 + 1
            android.support.v4.view.af r0 = r14.a(r5, r0)
            float r0 = r0.d
            float r3 = r3 + r0
            int r4 = r4 + 1
            if (r2 < 0) goto L_0x0178
            java.util.ArrayList r0 = r14.d
            java.lang.Object r0 = r0.get(r2)
            android.support.v4.view.af r0 = (android.support.v4.view.af) r0
            goto L_0x0144
        L_0x0178:
            r0 = 0
            goto L_0x0144
        L_0x017a:
            r0 = 0
            goto L_0x00a8
        L_0x017d:
            int r7 = r0.b
            if (r5 != r7) goto L_0x023f
            boolean r7 = r0.c
            if (r7 != 0) goto L_0x023f
            java.util.ArrayList r7 = r14.d
            r7.remove(r3)
            android.support.v4.view.k r7 = r14.g
            java.lang.Object r0 = r0.a
            r7.a(r14, r5, r0)
            java.util.ArrayList r0 = r14.d
            int r0 = r0.size()
            if (r3 >= r0) goto L_0x01ab
            java.util.ArrayList r0 = r14.d
            java.lang.Object r0 = r0.get(r3)
            android.support.v4.view.af r0 = (android.support.v4.view.af) r0
        L_0x01a1:
            r12 = r2
            r2 = r0
            r0 = r12
        L_0x01a4:
            int r5 = r5 + 1
            r12 = r0
            r0 = r2
            r2 = r12
            goto L_0x00b0
        L_0x01ab:
            r0 = 0
            goto L_0x01a1
        L_0x01ad:
            if (r0 == 0) goto L_0x01ce
            int r7 = r0.b
            if (r5 != r7) goto L_0x01ce
            float r0 = r0.d
            float r2 = r2 + r0
            int r3 = r3 + 1
            java.util.ArrayList r0 = r14.d
            int r0 = r0.size()
            if (r3 >= r0) goto L_0x01cc
            java.util.ArrayList r0 = r14.d
            java.lang.Object r0 = r0.get(r3)
            android.support.v4.view.af r0 = (android.support.v4.view.af) r0
        L_0x01c8:
            r12 = r2
            r2 = r0
            r0 = r12
            goto L_0x01a4
        L_0x01cc:
            r0 = 0
            goto L_0x01c8
        L_0x01ce:
            android.support.v4.view.af r0 = r14.a(r5, r3)
            int r3 = r3 + 1
            float r0 = r0.d
            float r2 = r2 + r0
            java.util.ArrayList r0 = r14.d
            int r0 = r0.size()
            if (r3 >= r0) goto L_0x01eb
            java.util.ArrayList r0 = r14.d
            java.lang.Object r0 = r0.get(r3)
            android.support.v4.view.af r0 = (android.support.v4.view.af) r0
        L_0x01e7:
            r12 = r2
            r2 = r0
            r0 = r12
            goto L_0x01a4
        L_0x01eb:
            r0 = 0
            goto L_0x01e7
        L_0x01ed:
            r0 = 0
            goto L_0x00c7
        L_0x01f0:
            r0 = 0
            r2 = r0
            goto L_0x00d5
        L_0x01f4:
            java.util.ArrayList r0 = r14.ab
            r0.clear()
            goto L_0x00e2
        L_0x01fb:
            if (r2 == 0) goto L_0x0204
            java.util.ArrayList r0 = r14.ab
            android.support.v4.view.am r1 = android.support.v4.view.ViewPager.ac
            java.util.Collections.sort(r0, r1)
        L_0x0204:
            boolean r0 = r14.hasFocus()
            if (r0 == 0) goto L_0x0012
            android.view.View r0 = r14.findFocus()
            if (r0 == 0) goto L_0x023d
            android.support.v4.view.af r0 = r14.b(r0)
        L_0x0214:
            if (r0 == 0) goto L_0x021c
            int r0 = r0.b
            int r1 = r14.h
            if (r0 == r1) goto L_0x0012
        L_0x021c:
            r0 = 0
        L_0x021d:
            int r1 = r14.getChildCount()
            if (r0 >= r1) goto L_0x0012
            android.view.View r1 = r14.getChildAt(r0)
            android.support.v4.view.af r2 = r14.a(r1)
            if (r2 == 0) goto L_0x023a
            int r2 = r2.b
            int r3 = r14.h
            if (r2 != r3) goto L_0x023a
            r2 = 2
            boolean r1 = r1.requestFocus(r2)
            if (r1 != 0) goto L_0x0012
        L_0x023a:
            int r0 = r0 + 1
            goto L_0x021d
        L_0x023d:
            r0 = 0
            goto L_0x0214
        L_0x023f:
            r12 = r2
            r2 = r0
            r0 = r12
            goto L_0x01a4
        L_0x0244:
            r6 = r0
            goto L_0x0065
        L_0x0247:
            r0 = r3
            goto L_0x005a
        L_0x024a:
            r1 = r0
            goto L_0x000e
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.view.ViewPager.a(int):void");
    }

    /* access modifiers changed from: protected */
    public void a(int i2, float f2, int i3) {
        int measuredWidth;
        int i4;
        int i5;
        if (this.U > 0) {
            int scrollX = getScrollX();
            int paddingLeft = getPaddingLeft();
            int paddingRight = getPaddingRight();
            int width = getWidth();
            int childCount = getChildCount();
            int i6 = 0;
            while (i6 < childCount) {
                View childAt = getChildAt(i6);
                ag agVar = (ag) childAt.getLayoutParams();
                if (!agVar.a) {
                    int i7 = paddingRight;
                    i4 = paddingLeft;
                    i5 = i7;
                } else {
                    switch (agVar.b & 7) {
                        case 1:
                            measuredWidth = Math.max((width - childAt.getMeasuredWidth()) / 2, paddingLeft);
                            int i8 = paddingRight;
                            i4 = paddingLeft;
                            i5 = i8;
                            break;
                        case 2:
                        case 4:
                        default:
                            measuredWidth = paddingLeft;
                            int i9 = paddingRight;
                            i4 = paddingLeft;
                            i5 = i9;
                            break;
                        case 3:
                            int width2 = childAt.getWidth() + paddingLeft;
                            int i10 = paddingLeft;
                            i5 = paddingRight;
                            i4 = width2;
                            measuredWidth = i10;
                            break;
                        case 5:
                            measuredWidth = (width - paddingRight) - childAt.getMeasuredWidth();
                            int measuredWidth2 = paddingRight + childAt.getMeasuredWidth();
                            i4 = paddingLeft;
                            i5 = measuredWidth2;
                            break;
                    }
                    int left = (measuredWidth + scrollX) - childAt.getLeft();
                    if (left != 0) {
                        childAt.offsetLeftAndRight(left);
                    }
                }
                i6++;
                int i11 = i5;
                paddingLeft = i4;
                paddingRight = i11;
            }
        }
        if (this.V != null) {
            this.V.a(i2, f2, i3);
        }
        if (this.W != null) {
            this.W.a(i2, f2, i3);
        }
        if (this.Y != null) {
            int scrollX2 = getScrollX();
            int childCount2 = getChildCount();
            for (int i12 = 0; i12 < childCount2; i12++) {
                View childAt2 = getChildAt(i12);
                if (!((ag) childAt2.getLayoutParams()).a) {
                    this.Y.a(childAt2, ((float) (childAt2.getLeft() - scrollX2)) / ((float) getWidth()));
                }
            }
        }
        this.T = true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* access modifiers changed from: package-private */
    public void a(int i2, int i3, int i4) {
        int abs;
        if (getChildCount() == 0) {
            setScrollingCacheEnabled(false);
            return;
        }
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        int i5 = i2 - scrollX;
        int i6 = i3 - scrollY;
        if (i5 == 0 && i6 == 0) {
            a(false);
            b();
            setScrollState(0);
            return;
        }
        setScrollingCacheEnabled(true);
        setScrollState(2);
        int width = getWidth();
        int i7 = width / 2;
        float a2 = (((float) i7) * a(Math.min(1.0f, (((float) Math.abs(i5)) * 1.0f) / ((float) width)))) + ((float) i7);
        int abs2 = Math.abs(i4);
        if (abs2 > 0) {
            abs = Math.round(1000.0f * Math.abs(a2 / ((float) abs2))) * 4;
        } else {
            abs = (int) (((((float) Math.abs(i5)) / ((((float) width) * this.g.a(this.h)) + ((float) this.n))) + 1.0f) * 100.0f);
        }
        this.l.startScroll(scrollX, scrollY, i5, i6, Math.min(abs, 600));
        q.b(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, boolean):void
     arg types: [int, boolean, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.support.v4.view.af, int, android.support.v4.view.af):void
      android.support.v4.view.ViewPager.a(int, float, int):void
      android.support.v4.view.ViewPager.a(int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean):void */
    public void a(int i2, boolean z2) {
        this.x = false;
        a(i2, z2, false);
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, boolean z2, boolean z3) {
        a(i2, z2, z3, 0);
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, boolean z2, boolean z3, int i3) {
        boolean z4 = false;
        if (this.g == null || this.g.a() <= 0) {
            setScrollingCacheEnabled(false);
        } else if (z3 || this.h != i2 || this.d.size() == 0) {
            if (i2 < 0) {
                i2 = 0;
            } else if (i2 >= this.g.a()) {
                i2 = this.g.a() - 1;
            }
            int i4 = this.y;
            if (i2 > this.h + i4 || i2 < this.h - i4) {
                for (int i5 = 0; i5 < this.d.size(); i5++) {
                    ((af) this.d.get(i5)).c = true;
                }
            }
            if (this.h != i2) {
                z4 = true;
            }
            a(i2);
            a(i2, z2, i3, z4);
        } else {
            setScrollingCacheEnabled(false);
        }
    }

    public boolean a(KeyEvent keyEvent) {
        if (keyEvent.getAction() != 0) {
            return false;
        }
        switch (keyEvent.getKeyCode()) {
            case 21:
                return c(17);
            case 22:
                return c(66);
            case 61:
                if (Build.VERSION.SDK_INT < 11) {
                    return false;
                }
                if (a.a(keyEvent)) {
                    return c(2);
                }
                if (a.a(keyEvent, 1)) {
                    return c(1);
                }
                return false;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(View view, boolean z2, int i2, int i3, int i4) {
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int scrollX = view.getScrollX();
            int scrollY = view.getScrollY();
            for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                View childAt = viewGroup.getChildAt(childCount);
                if (i3 + scrollX >= childAt.getLeft() && i3 + scrollX < childAt.getRight() && i4 + scrollY >= childAt.getTop() && i4 + scrollY < childAt.getBottom()) {
                    if (a(childAt, true, i2, (i3 + scrollX) - childAt.getLeft(), (i4 + scrollY) - childAt.getTop())) {
                        return true;
                    }
                }
            }
        }
        return z2 && q.a(view, -i2);
    }

    public void addFocusables(ArrayList arrayList, int i2, int i3) {
        af a2;
        int size = arrayList.size();
        int descendantFocusability = getDescendantFocusability();
        if (descendantFocusability != 393216) {
            for (int i4 = 0; i4 < getChildCount(); i4++) {
                View childAt = getChildAt(i4);
                if (childAt.getVisibility() == 0 && (a2 = a(childAt)) != null && a2.b == this.h) {
                    childAt.addFocusables(arrayList, i2, i3);
                }
            }
        }
        if ((descendantFocusability == 262144 && size != arrayList.size()) || !isFocusable()) {
            return;
        }
        if (((i3 & 1) != 1 || !isInTouchMode() || isFocusableInTouchMode()) && arrayList != null) {
            arrayList.add(this);
        }
    }

    public void addTouchables(ArrayList arrayList) {
        af a2;
        for (int i2 = 0; i2 < getChildCount(); i2++) {
            View childAt = getChildAt(i2);
            if (childAt.getVisibility() == 0 && (a2 = a(childAt)) != null && a2.b == this.h) {
                childAt.addTouchables(arrayList);
            }
        }
    }

    public void addView(View view, int i2, ViewGroup.LayoutParams layoutParams) {
        ViewGroup.LayoutParams generateLayoutParams = !checkLayoutParams(layoutParams) ? generateLayoutParams(layoutParams) : layoutParams;
        ag agVar = (ag) generateLayoutParams;
        agVar.a |= view instanceof ae;
        if (!this.v) {
            super.addView(view, i2, generateLayoutParams);
        } else if (agVar == null || !agVar.a) {
            agVar.d = true;
            addViewInLayout(view, i2, generateLayoutParams);
        } else {
            throw new IllegalStateException("Cannot add pager decor view during layout");
        }
    }

    /* access modifiers changed from: package-private */
    public af b(int i2) {
        int i3 = 0;
        while (true) {
            int i4 = i3;
            if (i4 >= this.d.size()) {
                return null;
            }
            af afVar = (af) this.d.get(i4);
            if (afVar.b == i2) {
                return afVar;
            }
            i3 = i4 + 1;
        }
    }

    /* access modifiers changed from: package-private */
    public af b(View view) {
        while (true) {
            ViewParent parent = view.getParent();
            if (parent == this) {
                return a(view);
            }
            if (parent == null || !(parent instanceof View)) {
                return null;
            }
            view = (View) parent;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        a(this.h);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.graphics.Rect, android.view.View):android.graphics.Rect
      android.support.v4.view.ViewPager.a(float, float):boolean
      android.support.v4.view.ViewPager.a(int, int):android.support.v4.view.af
      android.support.v4.view.ViewPager.a(int, boolean):void */
    /* access modifiers changed from: package-private */
    public boolean c() {
        if (this.h <= 0) {
            return false;
        }
        a(this.h - 1, true);
        return true;
    }

    public boolean c(int i2) {
        boolean z2;
        View findFocus = findFocus();
        if (findFocus == this) {
            findFocus = null;
        }
        View findNextFocus = FocusFinder.getInstance().findNextFocus(this, findFocus, i2);
        if (findNextFocus == null || findNextFocus == findFocus) {
            if (i2 == 17 || i2 == 1) {
                z2 = c();
            } else {
                if (i2 == 66 || i2 == 2) {
                    z2 = d();
                }
                z2 = false;
            }
        } else if (i2 == 17) {
            z2 = (findFocus == null || a(this.f, findNextFocus).left < a(this.f, findFocus).left) ? findNextFocus.requestFocus() : c();
        } else {
            if (i2 == 66) {
                z2 = (findFocus == null || a(this.f, findNextFocus).left > a(this.f, findFocus).left) ? findNextFocus.requestFocus() : d();
            }
            z2 = false;
        }
        if (z2) {
            playSoundEffect(SoundEffectConstants.getContantForFocusDirection(i2));
        }
        return z2;
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof ag) && super.checkLayoutParams(layoutParams);
    }

    public void computeScroll() {
        if (this.l.isFinished() || !this.l.computeScrollOffset()) {
            a(true);
            return;
        }
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        int currX = this.l.getCurrX();
        int currY = this.l.getCurrY();
        if (!(scrollX == currX && scrollY == currY)) {
            scrollTo(currX, currY);
            if (!d(currX)) {
                this.l.abortAnimation();
                scrollTo(0, currY);
            }
        }
        q.b(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.graphics.Rect, android.view.View):android.graphics.Rect
      android.support.v4.view.ViewPager.a(float, float):boolean
      android.support.v4.view.ViewPager.a(int, int):android.support.v4.view.af
      android.support.v4.view.ViewPager.a(int, boolean):void */
    /* access modifiers changed from: package-private */
    public boolean d() {
        if (this.g == null || this.h >= this.g.a() - 1) {
            return false;
        }
        a(this.h + 1, true);
        return true;
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent) || a(keyEvent);
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        af a2;
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (childAt.getVisibility() == 0 && (a2 = a(childAt)) != null && a2.b == this.h && childAt.dispatchPopulateAccessibilityEvent(accessibilityEvent)) {
                return true;
            }
        }
        return false;
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        boolean z2 = false;
        int a2 = q.a(this);
        if (a2 == 0 || (a2 == 1 && this.g != null && this.g.a() > 1)) {
            if (!this.P.a()) {
                int save = canvas.save();
                int height = (getHeight() - getPaddingTop()) - getPaddingBottom();
                int width = getWidth();
                canvas.rotate(270.0f);
                canvas.translate((float) ((-height) + getPaddingTop()), this.r * ((float) width));
                this.P.a(height, width);
                z2 = false | this.P.a(canvas);
                canvas.restoreToCount(save);
            }
            if (!this.Q.a()) {
                int save2 = canvas.save();
                int width2 = getWidth();
                int height2 = (getHeight() - getPaddingTop()) - getPaddingBottom();
                canvas.rotate(90.0f);
                canvas.translate((float) (-getPaddingTop()), (-(this.s + 1.0f)) * ((float) width2));
                this.Q.a(height2, width2);
                z2 |= this.Q.a(canvas);
                canvas.restoreToCount(save2);
            }
        } else {
            this.P.b();
            this.Q.b();
        }
        if (z2) {
            q.b(this);
        }
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        Drawable drawable = this.o;
        if (drawable != null && drawable.isStateful()) {
            drawable.setState(getDrawableState());
        }
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new ag();
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new ag(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return generateDefaultLayoutParams();
    }

    public k getAdapter() {
        return this.g;
    }

    /* access modifiers changed from: protected */
    public int getChildDrawingOrder(int i2, int i3) {
        if (this.aa == 2) {
            i3 = (i2 - 1) - i3;
        }
        return ((ag) ((View) this.ab.get(i3)).getLayoutParams()).f;
    }

    public int getCurrentItem() {
        return this.h;
    }

    public int getOffscreenPageLimit() {
        return this.y;
    }

    public int getPageMargin() {
        return this.n;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.R = true;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        removeCallbacks(this.ad);
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        float f2;
        super.onDraw(canvas);
        if (this.n > 0 && this.o != null && this.d.size() > 0 && this.g != null) {
            int scrollX = getScrollX();
            int width = getWidth();
            float f3 = ((float) this.n) / ((float) width);
            af afVar = (af) this.d.get(0);
            float f4 = afVar.e;
            int size = this.d.size();
            int i2 = afVar.b;
            int i3 = ((af) this.d.get(size - 1)).b;
            int i4 = 0;
            int i5 = i2;
            while (i5 < i3) {
                while (i5 > afVar.b && i4 < size) {
                    i4++;
                    afVar = (af) this.d.get(i4);
                }
                if (i5 == afVar.b) {
                    f2 = (afVar.e + afVar.d) * ((float) width);
                    f4 = afVar.e + afVar.d + f3;
                } else {
                    float a2 = this.g.a(i5);
                    f2 = (f4 + a2) * ((float) width);
                    f4 += a2 + f3;
                }
                if (((float) this.n) + f2 > ((float) scrollX)) {
                    this.o.setBounds((int) f2, this.p, (int) (((float) this.n) + f2 + 0.5f), this.q);
                    this.o.draw(canvas);
                }
                if (f2 <= ((float) (scrollX + width))) {
                    i5++;
                } else {
                    return;
                }
            }
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction() & 255;
        if (action == 3 || action == 1) {
            this.z = false;
            this.A = false;
            this.I = -1;
            if (this.J == null) {
                return false;
            }
            this.J.recycle();
            this.J = null;
            return false;
        }
        if (action != 0) {
            if (this.z) {
                return true;
            }
            if (this.A) {
                return false;
            }
        }
        switch (action) {
            case 0:
                float x2 = motionEvent.getX();
                this.G = x2;
                this.E = x2;
                float y2 = motionEvent.getY();
                this.H = y2;
                this.F = y2;
                this.I = f.b(motionEvent, 0);
                this.A = false;
                this.l.computeScrollOffset();
                if (this.ae == 2 && Math.abs(this.l.getFinalX() - this.l.getCurrX()) > this.N) {
                    this.l.abortAnimation();
                    this.x = false;
                    b();
                    this.z = true;
                    setScrollState(1);
                    break;
                } else {
                    a(false);
                    this.z = false;
                    break;
                }
                break;
            case 2:
                int i2 = this.I;
                if (i2 != -1) {
                    int a2 = f.a(motionEvent, i2);
                    float c2 = f.c(motionEvent, a2);
                    float f2 = c2 - this.E;
                    float abs = Math.abs(f2);
                    float d2 = f.d(motionEvent, a2);
                    float abs2 = Math.abs(d2 - this.H);
                    if (f2 == 0.0f || a(this.E, f2) || !a(this, false, (int) f2, (int) c2, (int) d2)) {
                        if (abs > ((float) this.D) && 0.5f * abs > abs2) {
                            this.z = true;
                            setScrollState(1);
                            this.E = f2 > 0.0f ? this.G + ((float) this.D) : this.G - ((float) this.D);
                            this.F = d2;
                            setScrollingCacheEnabled(true);
                        } else if (abs2 > ((float) this.D)) {
                            this.A = true;
                        }
                        if (this.z && b(c2)) {
                            q.b(this);
                            break;
                        }
                    } else {
                        this.E = c2;
                        this.F = d2;
                        this.A = true;
                        return false;
                    }
                }
                break;
            case 6:
                a(motionEvent);
                break;
        }
        if (this.J == null) {
            this.J = VelocityTracker.obtain();
        }
        this.J.addMovement(motionEvent);
        return this.z;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        af a2;
        int i6;
        int i7;
        int i8;
        int measuredHeight;
        int i9;
        int i10;
        this.v = true;
        b();
        this.v = false;
        int childCount = getChildCount();
        int i11 = i4 - i2;
        int i12 = i5 - i3;
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();
        int scrollX = getScrollX();
        int i13 = 0;
        int i14 = 0;
        while (i14 < childCount) {
            View childAt = getChildAt(i14);
            if (childAt.getVisibility() != 8) {
                ag agVar = (ag) childAt.getLayoutParams();
                if (agVar.a) {
                    int i15 = agVar.b & 7;
                    int i16 = agVar.b & 112;
                    switch (i15) {
                        case 1:
                            i8 = Math.max((i11 - childAt.getMeasuredWidth()) / 2, paddingLeft);
                            break;
                        case 2:
                        case 4:
                        default:
                            i8 = paddingLeft;
                            break;
                        case 3:
                            i8 = paddingLeft;
                            paddingLeft = childAt.getMeasuredWidth() + paddingLeft;
                            break;
                        case 5:
                            int measuredWidth = (i11 - paddingRight) - childAt.getMeasuredWidth();
                            paddingRight += childAt.getMeasuredWidth();
                            i8 = measuredWidth;
                            break;
                    }
                    switch (i16) {
                        case 16:
                            measuredHeight = Math.max((i12 - childAt.getMeasuredHeight()) / 2, paddingTop);
                            int i17 = paddingBottom;
                            i9 = paddingTop;
                            i10 = i17;
                            break;
                        case 48:
                            int measuredHeight2 = childAt.getMeasuredHeight() + paddingTop;
                            int i18 = paddingTop;
                            i10 = paddingBottom;
                            i9 = measuredHeight2;
                            measuredHeight = i18;
                            break;
                        case 80:
                            measuredHeight = (i12 - paddingBottom) - childAt.getMeasuredHeight();
                            int measuredHeight3 = paddingBottom + childAt.getMeasuredHeight();
                            i9 = paddingTop;
                            i10 = measuredHeight3;
                            break;
                        default:
                            measuredHeight = paddingTop;
                            int i19 = paddingBottom;
                            i9 = paddingTop;
                            i10 = i19;
                            break;
                    }
                    int i20 = i8 + scrollX;
                    childAt.layout(i20, measuredHeight, childAt.getMeasuredWidth() + i20, childAt.getMeasuredHeight() + measuredHeight);
                    i6 = i13 + 1;
                    i7 = i9;
                    paddingBottom = i10;
                    i14++;
                    paddingLeft = paddingLeft;
                    paddingRight = paddingRight;
                    paddingTop = i7;
                    i13 = i6;
                }
            }
            i6 = i13;
            i7 = paddingTop;
            i14++;
            paddingLeft = paddingLeft;
            paddingRight = paddingRight;
            paddingTop = i7;
            i13 = i6;
        }
        for (int i21 = 0; i21 < childCount; i21++) {
            View childAt2 = getChildAt(i21);
            if (childAt2.getVisibility() != 8) {
                ag agVar2 = (ag) childAt2.getLayoutParams();
                if (!agVar2.a && (a2 = a(childAt2)) != null) {
                    int i22 = ((int) (a2.e * ((float) i11))) + paddingLeft;
                    if (agVar2.d) {
                        agVar2.d = false;
                        childAt2.measure(View.MeasureSpec.makeMeasureSpec((int) (agVar2.c * ((float) ((i11 - paddingLeft) - paddingRight))), 1073741824), View.MeasureSpec.makeMeasureSpec((i12 - paddingTop) - paddingBottom, 1073741824));
                    }
                    childAt2.layout(i22, paddingTop, childAt2.getMeasuredWidth() + i22, childAt2.getMeasuredHeight() + paddingTop);
                }
            }
        }
        this.p = paddingTop;
        this.q = i12 - paddingBottom;
        this.U = i13;
        this.R = false;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r14, int r15) {
        /*
            r13 = this;
            r0 = 0
            int r0 = getDefaultSize(r0, r14)
            r1 = 0
            int r1 = getDefaultSize(r1, r15)
            r13.setMeasuredDimension(r0, r1)
            int r0 = r13.getMeasuredWidth()
            int r1 = r0 / 10
            int r2 = r13.B
            int r1 = java.lang.Math.min(r1, r2)
            r13.C = r1
            int r1 = r13.getPaddingLeft()
            int r0 = r0 - r1
            int r1 = r13.getPaddingRight()
            int r3 = r0 - r1
            int r0 = r13.getMeasuredHeight()
            int r1 = r13.getPaddingTop()
            int r0 = r0 - r1
            int r1 = r13.getPaddingBottom()
            int r5 = r0 - r1
            int r9 = r13.getChildCount()
            r0 = 0
            r8 = r0
        L_0x003b:
            if (r8 >= r9) goto L_0x00bc
            android.view.View r10 = r13.getChildAt(r8)
            int r0 = r10.getVisibility()
            r1 = 8
            if (r0 == r1) goto L_0x00a5
            android.view.ViewGroup$LayoutParams r0 = r10.getLayoutParams()
            android.support.v4.view.ag r0 = (android.support.v4.view.ag) r0
            if (r0 == 0) goto L_0x00a5
            boolean r1 = r0.a
            if (r1 == 0) goto L_0x00a5
            int r1 = r0.b
            r6 = r1 & 7
            int r1 = r0.b
            r4 = r1 & 112(0x70, float:1.57E-43)
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            r7 = 48
            if (r4 == r7) goto L_0x0069
            r7 = 80
            if (r4 != r7) goto L_0x00a9
        L_0x0069:
            r4 = 1
            r7 = r4
        L_0x006b:
            r4 = 3
            if (r6 == r4) goto L_0x0071
            r4 = 5
            if (r6 != r4) goto L_0x00ac
        L_0x0071:
            r4 = 1
            r6 = r4
        L_0x0073:
            if (r7 == 0) goto L_0x00af
            r2 = 1073741824(0x40000000, float:2.0)
        L_0x0077:
            int r4 = r0.width
            r11 = -2
            if (r4 == r11) goto L_0x010f
            r4 = 1073741824(0x40000000, float:2.0)
            int r2 = r0.width
            r11 = -1
            if (r2 == r11) goto L_0x010c
            int r2 = r0.width
        L_0x0085:
            int r11 = r0.height
            r12 = -2
            if (r11 == r12) goto L_0x010a
            r1 = 1073741824(0x40000000, float:2.0)
            int r11 = r0.height
            r12 = -1
            if (r11 == r12) goto L_0x010a
            int r0 = r0.height
        L_0x0093:
            int r2 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r4)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r1)
            r10.measure(r2, r0)
            if (r7 == 0) goto L_0x00b4
            int r0 = r10.getMeasuredHeight()
            int r5 = r5 - r0
        L_0x00a5:
            int r0 = r8 + 1
            r8 = r0
            goto L_0x003b
        L_0x00a9:
            r4 = 0
            r7 = r4
            goto L_0x006b
        L_0x00ac:
            r4 = 0
            r6 = r4
            goto L_0x0073
        L_0x00af:
            if (r6 == 0) goto L_0x0077
            r1 = 1073741824(0x40000000, float:2.0)
            goto L_0x0077
        L_0x00b4:
            if (r6 == 0) goto L_0x00a5
            int r0 = r10.getMeasuredWidth()
            int r3 = r3 - r0
            goto L_0x00a5
        L_0x00bc:
            r0 = 1073741824(0x40000000, float:2.0)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r3, r0)
            r13.t = r0
            r0 = 1073741824(0x40000000, float:2.0)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r5, r0)
            r13.u = r0
            r0 = 1
            r13.v = r0
            r13.b()
            r0 = 0
            r13.v = r0
            int r2 = r13.getChildCount()
            r0 = 0
            r1 = r0
        L_0x00db:
            if (r1 >= r2) goto L_0x0109
            android.view.View r4 = r13.getChildAt(r1)
            int r0 = r4.getVisibility()
            r5 = 8
            if (r0 == r5) goto L_0x0105
            android.view.ViewGroup$LayoutParams r0 = r4.getLayoutParams()
            android.support.v4.view.ag r0 = (android.support.v4.view.ag) r0
            if (r0 == 0) goto L_0x00f5
            boolean r5 = r0.a
            if (r5 != 0) goto L_0x0105
        L_0x00f5:
            float r5 = (float) r3
            float r0 = r0.c
            float r0 = r0 * r5
            int r0 = (int) r0
            r5 = 1073741824(0x40000000, float:2.0)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r5)
            int r5 = r13.u
            r4.measure(r0, r5)
        L_0x0105:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x00db
        L_0x0109:
            return
        L_0x010a:
            r0 = r5
            goto L_0x0093
        L_0x010c:
            r2 = r3
            goto L_0x0085
        L_0x010f:
            r4 = r2
            r2 = r3
            goto L_0x0085
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.view.ViewPager.onMeasure(int, int):void");
    }

    /* access modifiers changed from: protected */
    public boolean onRequestFocusInDescendants(int i2, Rect rect) {
        int i3;
        af a2;
        int i4 = -1;
        int childCount = getChildCount();
        if ((i2 & 2) != 0) {
            i4 = 1;
            i3 = 0;
        } else {
            i3 = childCount - 1;
            childCount = -1;
        }
        while (i3 != childCount) {
            View childAt = getChildAt(i3);
            if (childAt.getVisibility() == 0 && (a2 = a(childAt)) != null && a2.b == this.h && childAt.requestFocus(i2, rect)) {
                return true;
            }
            i3 += i4;
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, boolean):void
     arg types: [int, int, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.support.v4.view.af, int, android.support.v4.view.af):void
      android.support.v4.view.ViewPager.a(int, float, int):void
      android.support.v4.view.ViewPager.a(int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean):void */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (this.g != null) {
            this.g.a(savedState.b, savedState.c);
            a(savedState.a, false, true);
            return;
        }
        this.i = savedState.a;
        this.j = savedState.b;
        this.k = savedState.c;
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.a = this.h;
        if (this.g != null) {
            savedState.b = this.g.b();
        }
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (i2 != i4) {
            a(i2, i4, this.n, this.n);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, boolean, int):void
     arg types: [int, int, int, int]
     candidates:
      android.support.v4.view.ViewPager.a(int, float, int, int):int
      android.support.v4.view.ViewPager.a(int, int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, int, boolean):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, int, boolean):void
     arg types: [int, int, int, int]
     candidates:
      android.support.v4.view.ViewPager.a(int, float, int, int):int
      android.support.v4.view.ViewPager.a(int, int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean, int):void
      android.support.v4.view.ViewPager.a(int, boolean, int, boolean):void */
    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z2 = false;
        if (this.O) {
            return true;
        }
        if (motionEvent.getAction() == 0 && motionEvent.getEdgeFlags() != 0) {
            return false;
        }
        if (this.g == null || this.g.a() == 0) {
            return false;
        }
        if (this.J == null) {
            this.J = VelocityTracker.obtain();
        }
        this.J.addMovement(motionEvent);
        switch (motionEvent.getAction() & 255) {
            case 0:
                this.l.abortAnimation();
                this.x = false;
                b();
                this.z = true;
                setScrollState(1);
                float x2 = motionEvent.getX();
                this.G = x2;
                this.E = x2;
                float y2 = motionEvent.getY();
                this.H = y2;
                this.F = y2;
                this.I = f.b(motionEvent, 0);
                break;
            case 1:
                if (this.z) {
                    VelocityTracker velocityTracker = this.J;
                    velocityTracker.computeCurrentVelocity(1000, (float) this.L);
                    int a2 = (int) l.a(velocityTracker, this.I);
                    this.x = true;
                    int width = getWidth();
                    int scrollX = getScrollX();
                    af g2 = g();
                    a(a(g2.b, ((((float) scrollX) / ((float) width)) - g2.e) / g2.d, a2, (int) (f.c(motionEvent, f.a(motionEvent, this.I)) - this.G)), true, true, a2);
                    this.I = -1;
                    h();
                    z2 = this.Q.c() | this.P.c();
                    break;
                }
                break;
            case 2:
                if (!this.z) {
                    int a3 = f.a(motionEvent, this.I);
                    float c2 = f.c(motionEvent, a3);
                    float abs = Math.abs(c2 - this.E);
                    float d2 = f.d(motionEvent, a3);
                    float abs2 = Math.abs(d2 - this.F);
                    if (abs > ((float) this.D) && abs > abs2) {
                        this.z = true;
                        this.E = c2 - this.G > 0.0f ? this.G + ((float) this.D) : this.G - ((float) this.D);
                        this.F = d2;
                        setScrollState(1);
                        setScrollingCacheEnabled(true);
                    }
                }
                if (this.z) {
                    z2 = false | b(f.c(motionEvent, f.a(motionEvent, this.I)));
                    break;
                }
                break;
            case 3:
                if (this.z) {
                    a(this.h, true, 0, false);
                    this.I = -1;
                    h();
                    z2 = this.Q.c() | this.P.c();
                    break;
                }
                break;
            case 5:
                int a4 = f.a(motionEvent);
                this.E = f.c(motionEvent, a4);
                this.I = f.b(motionEvent, a4);
                break;
            case 6:
                a(motionEvent);
                this.E = f.c(motionEvent, f.a(motionEvent, this.I));
                break;
        }
        if (z2) {
            q.b(this);
        }
        return true;
    }

    public void removeView(View view) {
        if (this.v) {
            removeViewInLayout(view);
        } else {
            super.removeView(view);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.k.a(android.view.ViewGroup, int, java.lang.Object):void
     arg types: [android.support.v4.view.ViewPager, int, java.lang.Object]
     candidates:
      android.support.v4.view.k.a(android.view.View, int, java.lang.Object):void
      android.support.v4.view.k.a(android.view.ViewGroup, int, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, boolean):void
     arg types: [int, int, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.support.v4.view.af, int, android.support.v4.view.af):void
      android.support.v4.view.ViewPager.a(int, float, int):void
      android.support.v4.view.ViewPager.a(int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean):void */
    public void setAdapter(k kVar) {
        if (this.g != null) {
            this.g.b(this.m);
            this.g.a((ViewGroup) this);
            for (int i2 = 0; i2 < this.d.size(); i2++) {
                af afVar = (af) this.d.get(i2);
                this.g.a((ViewGroup) this, afVar.b, afVar.a);
            }
            this.g.b((ViewGroup) this);
            this.d.clear();
            f();
            this.h = 0;
            scrollTo(0, 0);
        }
        k kVar2 = this.g;
        this.g = kVar;
        if (this.g != null) {
            if (this.m == null) {
                this.m = new ak(this, null);
            }
            this.g.a((DataSetObserver) this.m);
            this.x = false;
            this.R = true;
            if (this.i >= 0) {
                this.g.a(this.j, this.k);
                a(this.i, false, true);
                this.i = -1;
                this.j = null;
                this.k = null;
            } else {
                b();
            }
        }
        if (this.X != null && kVar2 != kVar) {
            this.X.a(kVar2, kVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void setChildrenDrawingOrderEnabledCompat(boolean z2) {
        if (this.Z == null) {
            Class<ViewGroup> cls = ViewGroup.class;
            try {
                this.Z = cls.getDeclaredMethod("setChildrenDrawingOrderEnabled", Boolean.TYPE);
            } catch (NoSuchMethodException e2) {
                Log.e("ViewPager", "Can't find setChildrenDrawingOrderEnabled", e2);
            }
        }
        try {
            this.Z.invoke(this, Boolean.valueOf(z2));
        } catch (Exception e3) {
            Log.e("ViewPager", "Error changing children drawing order", e3);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.a(int, boolean, boolean):void
     arg types: [int, boolean, int]
     candidates:
      android.support.v4.view.ViewPager.a(android.support.v4.view.af, int, android.support.v4.view.af):void
      android.support.v4.view.ViewPager.a(int, float, int):void
      android.support.v4.view.ViewPager.a(int, int, int):void
      android.support.v4.view.ViewPager.a(int, boolean, boolean):void */
    public void setCurrentItem(int i2) {
        this.x = false;
        a(i2, !this.R, false);
    }

    public void setOffscreenPageLimit(int i2) {
        if (i2 < 1) {
            Log.w("ViewPager", "Requested offscreen page limit " + i2 + " too small; defaulting to " + 1);
            i2 = 1;
        }
        if (i2 != this.y) {
            this.y = i2;
            b();
        }
    }

    /* access modifiers changed from: package-private */
    public void setOnAdapterChangeListener(ah ahVar) {
        this.X = ahVar;
    }

    public void setOnPageChangeListener(ai aiVar) {
        this.V = aiVar;
    }

    public void setPageMargin(int i2) {
        int i3 = this.n;
        this.n = i2;
        int width = getWidth();
        a(width, width, i2, i3);
        requestLayout();
    }

    public void setPageMarginDrawable(int i2) {
        setPageMarginDrawable(getContext().getResources().getDrawable(i2));
    }

    public void setPageMarginDrawable(Drawable drawable) {
        this.o = drawable;
        if (drawable != null) {
            refreshDrawableState();
        }
        setWillNotDraw(drawable == null);
        invalidate();
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.o;
    }
}
