package android.support.v4.view;

import android.graphics.Paint;
import android.os.Build;
import android.view.View;

public class q {
    static final x a;

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 17) {
            a = new w();
        } else if (i >= 16) {
            a = new v();
        } else if (i >= 14) {
            a = new u();
        } else if (i >= 11) {
            a = new t();
        } else if (i >= 9) {
            a = new s();
        } else {
            a = new r();
        }
    }

    public static int a(View view) {
        return a.a(view);
    }

    public static void a(View view, int i, Paint paint) {
        a.a(view, i, paint);
    }

    public static void a(View view, Runnable runnable) {
        a.a(view, runnable);
    }

    public static boolean a(View view, int i) {
        return a.a(view, i);
    }

    public static void b(View view) {
        a.b(view);
    }
}
