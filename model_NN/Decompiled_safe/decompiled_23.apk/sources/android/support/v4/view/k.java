package android.support.v4.view;

import android.database.DataSetObservable;
import android.database.DataSetObserver;
import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;

public abstract class k {
    private DataSetObservable a;

    public float a(int i) {
        return 1.0f;
    }

    public abstract int a();

    public int a(Object obj) {
        return -1;
    }

    public Object a(View view, int i) {
        throw new UnsupportedOperationException("Required method instantiateItem was not overridden");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.k.a(android.view.View, int):java.lang.Object
     arg types: [android.view.ViewGroup, int]
     candidates:
      android.support.v4.view.k.a(android.view.ViewGroup, int):java.lang.Object
      android.support.v4.view.k.a(android.os.Parcelable, java.lang.ClassLoader):void
      android.support.v4.view.k.a(android.view.View, java.lang.Object):boolean
      android.support.v4.view.k.a(android.view.View, int):java.lang.Object */
    public Object a(ViewGroup viewGroup, int i) {
        return a((View) viewGroup, i);
    }

    /* access modifiers changed from: package-private */
    public void a(DataSetObserver dataSetObserver) {
        this.a.registerObserver(dataSetObserver);
    }

    public void a(Parcelable parcelable, ClassLoader classLoader) {
    }

    public void a(View view) {
    }

    public void a(View view, int i, Object obj) {
        throw new UnsupportedOperationException("Required method destroyItem was not overridden");
    }

    public void a(ViewGroup viewGroup) {
        a((View) viewGroup);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.k.a(android.view.View, int, java.lang.Object):void
     arg types: [android.view.ViewGroup, int, java.lang.Object]
     candidates:
      android.support.v4.view.k.a(android.view.ViewGroup, int, java.lang.Object):void
      android.support.v4.view.k.a(android.view.View, int, java.lang.Object):void */
    public void a(ViewGroup viewGroup, int i, Object obj) {
        a((View) viewGroup, i, obj);
    }

    public abstract boolean a(View view, Object obj);

    public Parcelable b() {
        return null;
    }

    /* access modifiers changed from: package-private */
    public void b(DataSetObserver dataSetObserver) {
        this.a.unregisterObserver(dataSetObserver);
    }

    public void b(View view) {
    }

    public void b(View view, int i, Object obj) {
    }

    public void b(ViewGroup viewGroup) {
        b((View) viewGroup);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.k.b(android.view.View, int, java.lang.Object):void
     arg types: [android.view.ViewGroup, int, java.lang.Object]
     candidates:
      android.support.v4.view.k.b(android.view.ViewGroup, int, java.lang.Object):void
      android.support.v4.view.k.b(android.view.View, int, java.lang.Object):void */
    public void b(ViewGroup viewGroup, int i, Object obj) {
        b((View) viewGroup, i, obj);
    }
}
