package com.avidia.b;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class c {
    private final Map a = new HashMap();

    public void a(List list) {
        HashMap hashMap = new HashMap(this.a);
        this.a.clear();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            b bVar = (b) it.next();
            this.a.put(Integer.valueOf(bVar.d), Boolean.valueOf(hashMap.containsKey(Integer.valueOf(bVar.d)) ? ((Boolean) hashMap.get(Integer.valueOf(bVar.d))).booleanValue() : false));
        }
    }

    public boolean a(int i) {
        if (this.a.containsKey(Integer.valueOf(i))) {
            return ((Boolean) this.a.get(Integer.valueOf(i))).booleanValue();
        }
        return false;
    }

    public void b(int i) {
        this.a.put(Integer.valueOf(i), true);
    }
}
