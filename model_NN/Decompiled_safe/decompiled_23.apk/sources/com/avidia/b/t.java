package com.avidia.b;

import org.json.JSONObject;

public class t {
    private String a;
    private String b;
    private String c;
    private String d;
    private int e;
    private String f;

    public static JSONObject c(String str) {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("FileFormat", 4096);
        jSONObject.put("FileName", "image_" + Math.abs(str.hashCode()) + ".jpg");
        jSONObject.put("ContentLength", str.length());
        jSONObject.put("Base64", str);
        return jSONObject;
    }

    public String a() {
        return this.d;
    }

    public void a(String str) {
        this.d = str;
    }

    public int b() {
        return this.e;
    }

    public void b(String str) {
        JSONObject jSONObject = new JSONObject(str);
        this.d = jSONObject.optString("UploadDate");
        this.c = jSONObject.optString("DocDisplayName");
        this.b = jSONObject.optString("DocId");
        this.a = jSONObject.optString("TpaId");
        this.e = jSONObject.optInt("FileKey", -1);
        this.f = jSONObject.optString("OriginalFileName");
    }

    public JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("UploadDate", this.d);
        jSONObject.put("DocDisplayName", this.c);
        jSONObject.put("DocId", this.b);
        jSONObject.put("TpaId", this.a);
        jSONObject.put("OriginalFileName", this.f);
        if (this.e != -1) {
            jSONObject.put("FileKey", this.e);
        }
        return jSONObject;
    }
}
