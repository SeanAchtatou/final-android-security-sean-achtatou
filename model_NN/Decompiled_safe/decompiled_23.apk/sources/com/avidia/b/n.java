package com.avidia.b;

import org.json.JSONObject;

public class n {
    private int a;
    private int b;
    private int c;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;
    private String i;
    private String j;

    private n() {
    }

    public static n a(String str) {
        return a(new JSONObject(str));
    }

    public static n a(JSONObject jSONObject) {
        n nVar = new n();
        nVar.b(jSONObject.getString("BankAccountName"));
        nVar.c(jSONObject.getString("BankAccountNumber"));
        nVar.a(jSONObject.optInt("BankAccountStatusCdeEnumeration"));
        nVar.d(jSONObject.getString("BankAcctNickName"));
        nVar.b(jSONObject.optInt("BankAcctTypeCdeEnumeration"));
        nVar.e(jSONObject.getString("BankRoutingNumber"));
        nVar.f(jSONObject.getString("ConfirmDte"));
        nVar.g(jSONObject.getString("DataPartnerId"));
        nVar.c(jSONObject.optInt("ExtBankAcctKey"));
        nVar.h(jSONObject.getString("TpaId"));
        return nVar;
    }

    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("BankAccountName", b());
        jSONObject.put("BankAccountNumber", c());
        jSONObject.put("BankAccountStatusCdeEnumeration", d());
        jSONObject.put("BankAcctNickName", e());
        jSONObject.put("BankAcctTypeCdeEnumeration", f());
        jSONObject.put("BankRoutingNumber", g());
        jSONObject.put("ConfirmDte", h());
        jSONObject.put("DataPartnerId", i());
        jSONObject.put("ExtBankAcctKey", j());
        jSONObject.put("TpaId", k());
        return jSONObject;
    }

    public void a(int i2) {
        this.a = i2;
    }

    public String b() {
        return this.g;
    }

    public void b(int i2) {
        this.b = i2;
    }

    public void b(String str) {
        this.g = str;
    }

    public String c() {
        return this.d;
    }

    public void c(int i2) {
        this.c = i2;
    }

    public void c(String str) {
        this.d = str;
    }

    public int d() {
        return this.a;
    }

    public void d(String str) {
        this.f = str;
    }

    public String e() {
        return this.f;
    }

    public void e(String str) {
        this.e = str;
    }

    public int f() {
        return this.b;
    }

    public void f(String str) {
        this.i = str;
    }

    public String g() {
        return this.e;
    }

    public void g(String str) {
        this.h = str;
    }

    public String h() {
        return this.i;
    }

    public void h(String str) {
        this.j = str;
    }

    public String i() {
        return this.h;
    }

    public int j() {
        return this.c;
    }

    public String k() {
        return this.j;
    }
}
