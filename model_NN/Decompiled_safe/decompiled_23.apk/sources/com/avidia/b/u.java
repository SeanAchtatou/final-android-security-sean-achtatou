package com.avidia.b;

import android.text.TextUtils;
import com.avidia.e.ag;
import java.util.Calendar;
import org.json.JSONObject;

public class u extends t {
    private int a;
    private String b;

    public u() {
        a(ag.b(Calendar.getInstance()));
    }

    public void b(String str) {
        super.b(str);
        JSONObject jSONObject = new JSONObject(str);
        if (!TextUtils.isEmpty(str)) {
            d(jSONObject.optString("___LOCAL_PATH___"));
        }
    }

    public JSONObject c() {
        JSONObject c = super.c();
        c.put("___LOCAL_PATH___", this.b);
        return c;
    }

    public String d() {
        return this.b;
    }

    public void d(String str) {
        this.b = str;
        if (!TextUtils.isEmpty(str)) {
            this.a = str.hashCode();
        } else {
            this.a = 0;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.a == ((u) obj).a;
    }

    public int hashCode() {
        return this.a;
    }
}
