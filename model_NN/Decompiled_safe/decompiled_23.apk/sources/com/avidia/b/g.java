package com.avidia.b;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public final class g {
    private int a;
    private String b;
    private String c;
    private String d;
    private String e;
    private int f;
    private String g;
    private String h;
    private int i;
    private List j = new ArrayList();
    private r k;
    private int l;

    private void a(JSONArray jSONArray) {
        this.j.clear();
        if (jSONArray != null) {
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                JSONObject optJSONObject = jSONArray.optJSONObject(i2);
                if (optJSONObject != null) {
                    this.j.add(new l(optJSONObject));
                }
            }
        }
    }

    private boolean c(String str) {
        for (l lVar : this.j) {
            if (lVar.d().equalsIgnoreCase(str)) {
                return lVar.e();
            }
        }
        return false;
    }

    public void a(String str) {
        JSONObject jSONObject = new JSONObject(str);
        this.l = jSONObject.optInt("FlexAccountKey", -1);
        this.a = jSONObject.optInt("BarCodeSymbology");
        this.b = jSONObject.optString("CertificationText");
        this.c = jSONObject.optString("ConfirmationText");
        this.d = jSONObject.optString(this.d);
        this.e = jSONObject.optString(this.e);
        this.f = jSONObject.optInt("Key");
        this.g = jSONObject.optString("Notes");
        this.h = jSONObject.optString(this.h);
        this.i = jSONObject.optInt("ReceiptsFileType");
        this.k = new r();
        this.k.a(jSONObject.optJSONObject("ParticipantClaimEntryTemplateData"));
        a(jSONObject.optJSONArray("DisplayableFields"));
    }

    public boolean a() {
        return b("reimbursement_method");
    }

    public boolean b() {
        return b("account_type");
    }

    public boolean b(String str) {
        for (l lVar : this.j) {
            if (lVar.d().equalsIgnoreCase(str)) {
                return lVar.f();
            }
        }
        return false;
    }

    public boolean c() {
        return b("service_category_code");
    }

    public boolean d() {
        return b("notes");
    }

    public boolean e() {
        return c("reimbursement_method");
    }

    public boolean f() {
        return c("account_type");
    }

    public boolean g() {
        return c("service_category_code");
    }

    public boolean h() {
        return c("notes");
    }

    public List i() {
        return this.k.a();
    }

    public List j() {
        return this.k.b();
    }

    public List k() {
        return this.k.d();
    }

    public List l() {
        return this.k.c();
    }

    public String m() {
        return this.b == null ? "" : this.b;
    }

    public int n() {
        return this.l;
    }
}
