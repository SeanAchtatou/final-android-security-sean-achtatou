package com.avidia.b;

import com.avidia.e.ag;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

public class a extends d {
    public String a;
    public String b;
    public String c;
    public String d;
    public String e;
    public boolean f;
    private final List m = new ArrayList();

    private void a(List list) {
        if (list != null) {
            this.m.clear();
            this.m.addAll(list);
        }
    }

    public List a() {
        return this.m;
    }

    public void a(String str) {
        a(new JSONObject(str));
    }

    public void a(JSONObject jSONObject) {
        super.a(jSONObject);
        this.a = jSONObject.optString("Balance");
        this.c = jSONObject.optString("AccountDisplayHeader");
        this.b = jSONObject.optString("AccountType");
        this.f = jSONObject.optBoolean("OptInStatus");
        this.d = jSONObject.optString("ExternalBankAccounts");
        this.e = jSONObject.optString("SubmitClaimsLastDate");
        a(ag.a(jSONObject.getJSONArray("DisplayableFields")));
    }
}
