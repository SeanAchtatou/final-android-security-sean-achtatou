package com.avidia.b;

import android.text.TextUtils;
import com.avidia.e.ag;
import java.io.Serializable;
import java.util.Calendar;
import org.json.JSONObject;

public class j implements Serializable {
    private static int t = 1;
    private boolean a;
    private boolean b;
    private String c = "";
    private double d;
    private double e;
    private int f;
    private String g = "";
    private boolean h;
    private String i = "";
    private String j = "";
    private int k;
    private int l;
    private String m = "";
    private int n = t;
    private int o;
    private int p = 2;
    private String q = "";
    private long r;
    private n s;

    private j() {
    }

    public j(Calendar calendar, int i2) {
        String c2 = ag.c(calendar);
        c(c2);
        d(c2);
        c(i2);
    }

    public static j a(String str) {
        return a(new JSONObject(str));
    }

    public static j a(JSONObject jSONObject) {
        j jVar = new j();
        jVar.a(ag.a(jSONObject.optInt("AccessRolloverBucketsBoolean")));
        jVar.b(ag.a(jSONObject.optInt("CardholderDisplayBoolean")));
        jVar.b(jSONObject.optString("ConfirmationNumber"));
        jVar.a(ag.a(jSONObject, "DepositAmtEmpe"));
        jVar.b(ag.a(jSONObject, "DepositAmtTotal"));
        jVar.a(jSONObject.optInt("DepositTypeCdeEnumeration"));
        jVar.c(jSONObject.optString("DisplayDte"));
        jVar.c(ag.a(jSONObject.optInt("DuplicateDepCheckBoolean")));
        jVar.d(jSONObject.optString("EffectiveDte"));
        jVar.e(jSONObject.getString("ExternalAccountDebitDte"));
        String optString = jSONObject.optString("ExternalBankAccount");
        if (optString == null || TextUtils.isEmpty(optString)) {
            jVar.a((n) null);
        } else {
            jVar.a(n.a(jSONObject.optString("ExternalBankAccount")));
        }
        jVar.b(jSONObject.optInt("ExternalBankAcctKey"));
        jVar.c(jSONObject.optInt("FlexAcctKey"));
        jVar.f(jSONObject.optString("Notes"));
        jVar.d(jSONObject.optInt("PendingContribStatusEnumeration"));
        jVar.e(jSONObject.optInt("PendingContributionKey"));
        jVar.f(jSONObject.optInt("PendingContributionYearEnumeration"));
        jVar.g(jSONObject.optString("TpaId"));
        jVar.a(jSONObject.optLong("TxnOptionsEnumeration"));
        return jVar;
    }

    public String a() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("DepositAmtTotal", this.e);
        jSONObject.put("DisplayDte", this.g);
        jSONObject.put("EffectiveDte", this.i);
        jSONObject.put("ExternalAccountDebitDte", ag.b(Calendar.getInstance()));
        jSONObject.put("ExternalBankAcctKey", this.k);
        jSONObject.put("FlexAcctKey", this.l);
        jSONObject.put("PendingContribStatusEnumeration", this.n);
        jSONObject.put("PendingContributionYearEnumeration", this.p);
        jSONObject.put("TpaId", this.q);
        return jSONObject.toString();
    }

    public void a(double d2) {
        this.d = d2;
    }

    public void a(int i2) {
        this.f = i2;
    }

    public void a(long j2) {
        this.r = j2;
    }

    public void a(n nVar) {
        this.s = nVar;
    }

    public void a(boolean z) {
        this.a = z;
    }

    public String b() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("AccessRolloverBucketsBoolean", ag.a(this.a));
        jSONObject.put("CardholderDisplayBoolean", ag.a(this.b));
        jSONObject.put("ConfirmationNumber", this.c);
        jSONObject.put("DepositAmtEmpe", this.d);
        jSONObject.put("DepositAmtTotal", this.e);
        jSONObject.put("DepositTypeCdeEnumeration", this.f);
        jSONObject.put("DisplayDte", this.g);
        jSONObject.put("DuplicateDepCheckBoolean", ag.a(this.h));
        jSONObject.put("EffectiveDte", this.i);
        jSONObject.put("ExternalAccountDebitDte", this.j);
        if (this.s != null) {
            jSONObject.put("ExternalBankAccount", this.s.a());
        } else {
            jSONObject.put("ExternalBankAccount", "");
        }
        jSONObject.put("ExternalBankAcctKey", this.k);
        jSONObject.put("FlexAcctKey", this.l);
        jSONObject.put("Notes", this.m);
        jSONObject.put("PendingContribStatusEnumeration", this.n);
        jSONObject.put("PendingContributionKey", this.o);
        jSONObject.put("PendingContributionYearEnumeration", this.p);
        jSONObject.put("TpaId", this.q);
        jSONObject.put("TxnOptionsEnumeration", this.r);
        return jSONObject.toString();
    }

    public void b(double d2) {
        this.e = d2;
    }

    public void b(int i2) {
        this.k = i2;
    }

    public void b(String str) {
        this.c = str;
    }

    public void b(boolean z) {
        this.b = z;
    }

    public String c() {
        return this.c;
    }

    public void c(int i2) {
        this.l = i2;
    }

    public void c(String str) {
        this.g = str;
    }

    public void c(boolean z) {
        this.h = z;
    }

    public double d() {
        return this.e;
    }

    public void d(int i2) {
        this.n = i2;
    }

    public void d(String str) {
        this.i = str;
    }

    public void d(boolean z) {
        this.p = z ? 2 : 1;
    }

    public String e() {
        return this.g;
    }

    public void e(int i2) {
        this.o = i2;
    }

    public void e(String str) {
        this.j = str;
    }

    public String f() {
        return this.j;
    }

    public void f(int i2) {
        this.p = i2;
    }

    public void f(String str) {
        this.m = str;
    }

    public void g(String str) {
        this.q = str;
    }

    public boolean g() {
        return this.p == 2;
    }

    public n h() {
        return this.s;
    }
}
