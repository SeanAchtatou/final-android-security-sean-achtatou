package com.avidia.b;

import android.text.TextUtils;
import com.avidia.e.ag;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class r {
    private static final String e = r.class.getSimpleName();
    private List a = new ArrayList();
    private List b = new ArrayList();
    private List c = new ArrayList();
    private List d = new ArrayList();

    private void a(String str) {
        this.d.clear();
        if (!TextUtils.isEmpty(str) && !"null".equalsIgnoreCase(str)) {
            String trim = str.trim();
            if (trim.length() < 2) {
                ag.b(e, "ServiceCategoryCodes == EMPTY");
                return;
            }
            if (trim.startsWith("(") && trim.endsWith(")")) {
                trim = trim.replaceFirst("\\(", "[").substring(0, trim.length() - 1) + "]";
            }
            try {
                JSONArray jSONArray = new JSONArray(trim);
                for (int i = 0; i < jSONArray.length(); i++) {
                    w wVar = new w();
                    wVar.a(jSONArray.getString(i));
                    this.d.add(wVar);
                }
            } catch (JSONException e2) {
                ag.a(e, "Error wgile parsing ServiceCategoryCode", e2);
            }
        }
    }

    private void a(JSONArray jSONArray, List list) {
        list.clear();
        if (jSONArray != null) {
            for (int i = 0; i < jSONArray.length(); i++) {
                String optString = jSONArray.optString(i);
                if (!TextUtils.isEmpty(optString)) {
                    list.add(optString);
                }
            }
        }
    }

    public List a() {
        return Collections.unmodifiableList(this.b);
    }

    public void a(JSONObject jSONObject) {
        a(jSONObject.optJSONArray("ReimbursementMethod"), this.c);
        a(jSONObject.optString("ServiceCategoryCodes"));
        JSONArray optJSONArray = jSONObject.optJSONArray("AccountsInfo");
        JSONArray optJSONArray2 = jSONObject.optJSONArray("Claimants");
        if (optJSONArray != null) {
            this.a.clear();
            for (int i = 0; i < optJSONArray.length(); i++) {
                try {
                    this.a.add(new d(optJSONArray.optJSONObject(i)));
                } catch (JSONException e2) {
                    ag.b(e, "Error while parsing the account info");
                }
            }
        }
        if (optJSONArray2 != null) {
            this.b.clear();
            for (int i2 = 0; i2 < optJSONArray2.length(); i2++) {
                this.b.add(new h(optJSONArray2.optJSONObject(i2)));
            }
        }
    }

    public List b() {
        return Collections.unmodifiableList(this.c);
    }

    public List c() {
        return Collections.unmodifiableList(this.d);
    }

    public List d() {
        ArrayList arrayList = new ArrayList();
        for (d dVar : this.a) {
            if (!TextUtils.isEmpty(dVar.h)) {
                arrayList.add(dVar.h);
            }
        }
        return arrayList;
    }
}
