package com.avidia.b;

import org.json.JSONObject;

public class l {
    private int a;
    private int b;
    private String c;
    private String d;

    public l() {
    }

    public l(JSONObject jSONObject) {
        a(jSONObject);
    }

    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("DisplayOptions", this.a);
        jSONObject.put("OriginalFieldName", this.d);
        jSONObject.put("DisplaySpecifications", this.b);
        jSONObject.put("AlternativeFieldName", this.c);
        return jSONObject;
    }

    public void a(String str) {
        this.c = str;
    }

    public void a(JSONObject jSONObject) {
        this.c = jSONObject.optString("AlternativeFieldName");
        this.a = jSONObject.optInt("DisplayOptions");
        this.b = jSONObject.optInt("DisplaySpecifications");
        this.d = jSONObject.optString("OriginalFieldName");
    }

    public int b() {
        return this.a;
    }

    public void b(String str) {
        this.d = str;
    }

    public String c() {
        return this.c;
    }

    public String d() {
        return this.d;
    }

    public boolean e() {
        return (this.b == k.DisplaySpecificationsNone.a() || (this.b & k.DisplaySpecificationsIsRequired.a()) == 0) ? false : true;
    }

    public boolean f() {
        return (this.b == k.DisplaySpecificationsNone.a() || (this.b & k.DisplaySpecificationsIsVisible.a()) == 0) ? false : true;
    }
}
