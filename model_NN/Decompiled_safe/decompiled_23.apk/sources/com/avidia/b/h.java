package com.avidia.b;

import org.json.JSONObject;

public class h {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;
    private String i;
    private String j;

    public h(String str) {
        this(new JSONObject(str));
    }

    public h(JSONObject jSONObject) {
        a(jSONObject);
    }

    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("CardNum", this.d);
        jSONObject.put("CardholderKey", this.i);
        jSONObject.put("CardholderTypeCde", this.j);
        jSONObject.put("EmprId", this.b);
        jSONObject.put("FirstName", this.f);
        jSONObject.put("Initial", this.c);
        jSONObject.put("LastName", this.e);
        jSONObject.put("NamePrefix", this.h);
        jSONObject.put("SocSecNum", this.g);
        jSONObject.put("TpaId", this.a);
        return jSONObject;
    }

    public void a(JSONObject jSONObject) {
        this.d = jSONObject.optString("CardNum", null);
        this.i = jSONObject.optString("CardholderKey");
        this.j = jSONObject.optString("CardholderTypeCde");
        this.b = jSONObject.optString("EmprId");
        this.f = jSONObject.optString("FirstName");
        this.c = jSONObject.optString("Initial");
        this.e = jSONObject.optString("LastName");
        this.h = jSONObject.optString("NamePrefix");
        this.g = jSONObject.optString("SocSecNum", null);
        this.a = jSONObject.optString("TpaId");
    }

    public String b() {
        return String.format("%s %s", this.f, this.e).trim();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        h hVar = (h) obj;
        if (this.d == null ? hVar.d != null : !this.d.equals(hVar.d)) {
            return false;
        }
        if (this.i == null ? hVar.i != null : !this.i.equals(hVar.i)) {
            return false;
        }
        if (this.j == null ? hVar.j != null : !this.j.equals(hVar.j)) {
            return false;
        }
        if (this.b == null ? hVar.b != null : !this.b.equals(hVar.b)) {
            return false;
        }
        if (this.f == null ? hVar.f != null : !this.f.equals(hVar.f)) {
            return false;
        }
        if (this.c == null ? hVar.c != null : !this.c.equals(hVar.c)) {
            return false;
        }
        if (this.e == null ? hVar.e != null : !this.e.equals(hVar.e)) {
            return false;
        }
        if (this.h == null ? hVar.h != null : !this.h.equals(hVar.h)) {
            return false;
        }
        if (this.g == null ? hVar.g != null : !this.g.equals(hVar.g)) {
            return false;
        }
        if (this.a != null) {
            if (this.a.equals(hVar.a)) {
                return true;
            }
        } else if (hVar.a == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i2 = 0;
        int hashCode = ((this.i != null ? this.i.hashCode() : 0) + (((this.h != null ? this.h.hashCode() : 0) + (((this.g != null ? this.g.hashCode() : 0) + (((this.f != null ? this.f.hashCode() : 0) + (((this.e != null ? this.e.hashCode() : 0) + (((this.d != null ? this.d.hashCode() : 0) + (((this.c != null ? this.c.hashCode() : 0) + (((this.b != null ? this.b.hashCode() : 0) + ((this.a != null ? this.a.hashCode() : 0) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31;
        if (this.j != null) {
            i2 = this.j.hashCode();
        }
        return hashCode + i2;
    }
}
