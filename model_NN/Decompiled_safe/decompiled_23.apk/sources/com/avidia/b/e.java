package com.avidia.b;

import android.text.TextUtils;
import com.avidia.a.a;
import com.avidia.e.ag;
import com.avidia.fismobile.FisApplication;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class e {
    private String a;
    private String b;
    private String c;
    private double d;
    private String e;
    private String f;
    private h g;
    private String h;
    private w i;
    private List j;
    private String k;
    private String l;
    private String m;
    private f n;
    private int o;
    private List p;
    private Map q;

    public e() {
        this.e = "";
        this.o = -1;
        this.p = new ArrayList();
        this.q = new HashMap();
        Calendar instance = Calendar.getInstance();
        this.a = ag.b(instance);
        this.b = ag.b(instance);
        instance.set(instance.get(1), instance.get(2), instance.get(5) + 1);
        this.c = "NONE";
    }

    public e(String str, boolean z) {
        this();
        a(str, z);
    }

    private String a(int i2) {
        switch (i2) {
            case 1:
                return "Check";
            case 2:
                return "Direct Deposit";
            case 3:
                return "External Check";
            case 4:
                return "External Direct Deposit";
            default:
                return "None";
        }
    }

    private void a(String str, String str2) {
        if (str != null && !TextUtils.isEmpty(str2)) {
            this.q.put(str, str2);
        }
    }

    private void a(JSONArray jSONArray) {
        this.j = null;
        if (jSONArray != null) {
            this.j = new ArrayList();
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                String optString = jSONArray.optString(i2);
                if (optString != null) {
                    this.j.add(new l(new JSONObject(optString)));
                }
            }
        }
    }

    private void a(JSONArray jSONArray, boolean z) {
        this.p.clear();
        if (jSONArray != null) {
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                t uVar = z ? new u() : new t();
                uVar.b(jSONArray.getJSONObject(i2).toString());
                this.p.add(uVar);
            }
        }
    }

    private void a(JSONObject jSONObject, String str, String str2) {
        if (!TextUtils.isEmpty(str2)) {
            jSONObject.put(str, str2);
        }
    }

    private JSONArray b(boolean z) {
        if (this.p == null || this.p.isEmpty()) {
            return null;
        }
        JSONArray jSONArray = new JSONArray();
        for (t tVar : this.p) {
            if (!(tVar instanceof u) || z) {
                jSONArray.put(tVar.c());
            } else {
                String m2 = ag.m(((u) tVar).d());
                if (m2 != null) {
                    jSONArray.put(u.c(m2));
                }
            }
        }
        return jSONArray;
    }

    private int h(String str) {
        if (TextUtils.isEmpty(str)) {
            return 0;
        }
        if ("Check".equalsIgnoreCase(str)) {
            return 1;
        }
        if ("Direct Deposit".equalsIgnoreCase(str)) {
            return 2;
        }
        if ("External Check".equalsIgnoreCase(str)) {
            return 3;
        }
        return "External Direct Deposit".equalsIgnoreCase(str) ? 4 : 0;
    }

    private void i(String str) {
        g h2 = FisApplication.k().h();
        this.i = null;
        for (w wVar : h2.l()) {
            if (wVar.b().equalsIgnoreCase(str)) {
                this.i = wVar;
            }
        }
    }

    private f j(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        String[] split = str.split(":");
        return split[0].equalsIgnoreCase("CardTransaction") ? f.ClaimTypeCardTransaction : split[0].equalsIgnoreCase("ManualClaim") ? f.ClaimTypeManualClaim : f.ClaimTypeUnknown;
    }

    private String q() {
        return this.g == null ? "" : this.g.b();
    }

    public String a() {
        return this.m;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    public String a(boolean z) {
        g h2;
        JSONObject jSONObject = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        JSONObject jSONObject2 = new JSONObject();
        a(jSONObject2, "AcctTypeCde", this.h);
        if (!TextUtils.isEmpty(this.h) && (h2 = FisApplication.k().h()) != null) {
            jSONObject2.put("FlexAcctKey", h2.n());
        }
        if ("NONE".equalsIgnoreCase(this.c)) {
            jSONObject2.put("ServiceEndDate", (Object) null);
        } else {
            a(jSONObject2, "ServiceEndDate", this.c);
        }
        a(jSONObject2, "ServiceStartDate", this.b);
        a(jSONObject2, "TpaId", a.b);
        jSONObject2.put("ReimbModeCde", h(this.f));
        a(jSONObject2, "TransactionDate", this.a);
        if (this.i != null) {
            a(jSONObject2, "ScCde", this.i.b());
        }
        if (this.g != null) {
            jSONObject2.put("Claimant", this.g.a());
        }
        a(jSONObject2, "TrackingNum", this.k);
        a(jSONObject2, "Notes", this.e);
        a(jSONObject2, "Type", this.m);
        jSONObject2.put("TxnAmt", this.d);
        jSONArray.put(jSONObject2);
        jSONObject.put("Claims", jSONArray);
        JSONArray b2 = b(z);
        if (b2 == null) {
            jSONObject2.put("HasReceipt", false);
            jSONObject.put("SingleReceipt", false);
        } else {
            jSONObject2.put("ReceiptsInfo", b2);
            jSONObject2.put("HasReceipt", true);
            jSONObject.put("SingleReceipt", true);
        }
        return jSONObject.toString();
    }

    public void a(double d2) {
        this.d = d2;
    }

    public void a(h hVar) {
        this.g = hVar;
    }

    public void a(w wVar) {
        this.i = wVar;
    }

    public void a(String str) {
        if (str.startsWith("/Date(")) {
            this.b = str;
        } else {
            this.b = ag.b(ag.h(str));
        }
    }

    public void a(String str, boolean z) {
        JSONArray jSONArray;
        this.q.clear();
        JSONArray jSONArray2 = null;
        try {
            jSONArray2 = new JSONObject(str).optJSONArray("Claims");
        } catch (Exception e2) {
        }
        if (jSONArray2 == null) {
            try {
                jSONArray = new JSONArray(str);
            } catch (Exception e3) {
                jSONArray = jSONArray2;
            }
        } else {
            jSONArray = jSONArray2;
        }
        a(jSONArray == null ? new JSONObject(str) : jSONArray.getJSONObject(0), z);
    }

    public void a(List list) {
        this.p.clear();
        if (list != null && !list.isEmpty()) {
            this.p.addAll(list);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avidia.b.e.a(org.json.JSONObject, boolean):void
     arg types: [org.json.JSONObject, int]
     candidates:
      com.avidia.b.e.a(java.lang.String, java.lang.String):void
      com.avidia.b.e.a(org.json.JSONArray, boolean):void
      com.avidia.b.e.a(java.lang.String, boolean):void
      com.avidia.b.e.a(org.json.JSONObject, boolean):void */
    public void a(JSONObject jSONObject) {
        a(jSONObject, false);
    }

    public void a(JSONObject jSONObject, boolean z) {
        d(jSONObject.optString("Notes"));
        this.o = jSONObject.optInt("ClaimKey", -1);
        this.h = jSONObject.optString("AcctTypeCde");
        b(jSONObject.optString("ServiceEndDate"));
        this.b = jSONObject.optString("ServiceStartDate");
        this.d = jSONObject.optDouble("TxnAmt");
        i(jSONObject.optString("ScCde"));
        this.f = a(jSONObject.optInt("ReimbModeCde"));
        String optString = jSONObject.optString("Claimant");
        if (TextUtils.isEmpty(optString)) {
            this.g = null;
        } else {
            this.g = new h(optString);
        }
        this.k = jSONObject.optString("TrackingNum");
        this.l = jSONObject.optString("Status");
        this.m = jSONObject.optString("Type");
        i(jSONObject.optString("ScCde"));
        a(jSONObject.optJSONArray("DisplayableFields"));
        String optString2 = jSONObject.optString("TransactionDate");
        if (!TextUtils.isEmpty(optString2)) {
            this.a = optString2;
        }
        this.l = jSONObject.optString("Status");
        this.n = j(jSONObject.optString("__type"));
        a("InsertDate", jSONObject.optString("InsertDate"));
        a("PlanEndDate", jSONObject.optString("PlanEndDate"));
        a("PlanStartDate", jSONObject.optString("PlanStartDate"));
        a("SettlementDate", jSONObject.optString("SettlementDate"));
        a("TransactionId", jSONObject.optString("TransactionId"));
        a("UpdateDte", jSONObject.optString("UpdateDte"));
        a(jSONObject.optJSONArray("ReceiptsInfo"), z);
    }

    public String b() {
        return this.f;
    }

    public void b(String str) {
        try {
            if (TextUtils.isEmpty(str) || str.equalsIgnoreCase("NONE")) {
                this.c = "NONE";
            } else if (str.startsWith("/Date(")) {
                this.c = str;
            } else {
                this.c = ag.b(ag.h(str));
            }
        } catch (Exception e2) {
            this.c = "NONE";
        }
    }

    public h c() {
        return this.g;
    }

    public void c(String str) {
        this.f = str;
    }

    public String d() {
        return this.h;
    }

    public void d(String str) {
        this.e = str;
    }

    public String e() {
        return this.b;
    }

    public void e(String str) {
        this.h = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avidia.b.e.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.avidia.b.e.a(java.lang.String, java.lang.String):void
      com.avidia.b.e.a(org.json.JSONArray, boolean):void
      com.avidia.b.e.a(org.json.JSONObject, boolean):void
      com.avidia.b.e.a(java.lang.String, boolean):void */
    public void f(String str) {
        a(str, false);
    }

    public boolean f() {
        return TextUtils.isEmpty(this.c) || this.c.equalsIgnoreCase("NONE");
    }

    public String g() {
        return this.c;
    }

    public String g(String str) {
        if ("TrackingNum".equalsIgnoreCase(str)) {
            return this.k;
        }
        if ("TransactionDate".equalsIgnoreCase(str)) {
            return ag.f(j());
        }
        if ("ServiceStartDate".equalsIgnoreCase(str)) {
            return ag.f(e());
        }
        if ("ServiceEndDate".equalsIgnoreCase(str)) {
            return ag.f(g());
        }
        if ("AcctTypeCde".equalsIgnoreCase(str)) {
            return this.h;
        }
        if ("Type".equalsIgnoreCase(str)) {
            return a();
        }
        if ("Claimant".equalsIgnoreCase(str)) {
            return q();
        }
        if ("Status".equalsIgnoreCase(str)) {
            return this.l;
        }
        if ("TxnAmt".equalsIgnoreCase(str)) {
            return ag.a(Double.valueOf(this.d));
        }
        if ("ScCde".equalsIgnoreCase(str)) {
            return this.i != null ? this.i.b() : "";
        }
        String str2 = (String) this.q.get(str);
        return str2 == null ? "" : str2;
    }

    public double h() {
        return this.d;
    }

    public String i() {
        return this.e;
    }

    public String j() {
        return this.a;
    }

    public void k() {
        this.a = ag.b(Calendar.getInstance());
    }

    public List l() {
        return this.j;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    public String m() {
        JSONObject jSONObject = new JSONObject();
        JSONArray b2 = b(true);
        if (b2 == null) {
            jSONObject.put("HasReceipt", false);
        } else {
            jSONObject.put("ReceiptsInfo", b2);
            jSONObject.put("HasReceipt", true);
        }
        jSONObject.put("ClaimKey", this.o);
        a(jSONObject, "AcctTypeCde", this.h);
        a(jSONObject, "ServiceEndDate", this.c);
        a(jSONObject, "ServiceStartDate", this.b);
        a(jSONObject, "TpaId", a.b);
        jSONObject.put("ReimbModeCde", h(this.f));
        a(jSONObject, "TransactionDate", this.a);
        a(jSONObject, "TrackingNum", this.k);
        a(jSONObject, "Notes", this.e);
        a(jSONObject, "Type", this.m);
        jSONObject.put("TxnAmt", this.d);
        if (this.j != null && !this.j.isEmpty()) {
            JSONArray jSONArray = new JSONArray();
            for (l a2 : this.j) {
                jSONArray.put(a2.a());
            }
            jSONObject.put("DisplayableFields", jSONArray);
        }
        a(jSONObject, "Status", this.l);
        if (this.n != null) {
            a(jSONObject, "__type", this.n.name());
        }
        a(jSONObject, "InsertDate", (String) this.q.get("InsertDate"));
        a(jSONObject, "PlanEndDate", (String) this.q.get("PlanEndDate"));
        a(jSONObject, "PlanStartDate", (String) this.q.get("PlanStartDate"));
        a(jSONObject, "SettlementDate", (String) this.q.get("SettlementDate"));
        a(jSONObject, "TransactionId", (String) this.q.get("TransactionId"));
        a(jSONObject, "UpdateDte", (String) this.q.get("UpdateDte"));
        if (this.i != null) {
            a(jSONObject, "ScCde", this.i.b());
        }
        if (this.g != null) {
            jSONObject.put("Claimant", this.g.a());
        }
        if (this.p != null && !this.p.isEmpty()) {
            JSONArray jSONArray2 = new JSONArray();
            for (t c2 : this.p) {
                jSONArray2.put(c2.c());
            }
            jSONObject.put("ReceiptsInfo", jSONArray2);
        }
        return jSONObject.toString();
    }

    public String n() {
        return this.i != null ? this.i.a() : "";
    }

    public List o() {
        return Collections.unmodifiableList(this.p);
    }

    public int p() {
        return this.o;
    }
}
