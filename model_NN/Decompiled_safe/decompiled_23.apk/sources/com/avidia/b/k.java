package com.avidia.b;

public enum k {
    DisplaySpecificationsNone(0),
    DisplaySpecificationsIsVisible(1),
    DisplaySpecificationsIsRequired(2),
    DisplaySpecificationsIsOptional(4),
    DisplaySpecificationsIsReadOnly(8);
    
    private int f;

    private k(int i) {
        this.f = i;
    }

    public int a() {
        return this.f;
    }
}
