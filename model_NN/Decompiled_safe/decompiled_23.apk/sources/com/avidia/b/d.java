package com.avidia.b;

import org.json.JSONObject;

public class d {
    private JSONObject a;
    public int g;
    public String h;
    public int i;
    public String j;
    public String k;
    public String l;

    public d() {
    }

    public d(JSONObject jSONObject) {
        a(jSONObject);
    }

    public void a(JSONObject jSONObject) {
        this.a = jSONObject;
        this.j = jSONObject.optString("PlanEndDate");
        this.k = jSONObject.optString("PlanStartDate");
        this.i = jSONObject.optInt("FlexAccountKey");
        this.h = jSONObject.optString("AccountType");
        this.l = jSONObject.optString("ProductPartnerId");
        this.g = jSONObject.optInt("AccountTypeOptions");
    }

    public String b() {
        return this.a != null ? this.a.toString() : "";
    }
}
