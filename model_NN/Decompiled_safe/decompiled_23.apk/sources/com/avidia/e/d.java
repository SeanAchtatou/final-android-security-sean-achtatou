package com.avidia.e;

import android.text.TextUtils;
import com.avidia.b.v;
import org.json.JSONObject;

public class d extends h implements q {
    private String a;
    private String b;
    private final String c;
    private final String d;

    public d(String str, String str2, String str3, String str4) {
        this.a = str;
        this.c = str2;
        this.d = str3;
        this.b = str4;
    }

    public d(JSONObject jSONObject) {
        this.a = jSONObject.optString("SeqNumber");
        if (TextUtils.isEmpty(this.a)) {
            this.a = jSONObject.optString("ExternalSeqNumber");
        }
        this.c = jSONObject.optString("TransactionId");
        this.d = jSONObject.optString("SettlementDate");
        try {
            this.b = jSONObject.optJSONArray("ReceiptsInfo").getJSONObject(0).toString();
        } catch (Exception e) {
        }
    }

    public v a() {
        v a2 = a("https://m.wealthcareadmin.com/ServiceProvider/services/participant/receipts/pos/", String.format("?transactionid=%s&setldate=%s&seqnum=%s%s", this.c, this.d, this.a, "&decrypt=0"), p.PUT, this.b);
        if (!(a2 == null || a2.b == null)) {
            ag.c(getClass().getSimpleName(), a2.b);
        }
        return a2;
    }
}
