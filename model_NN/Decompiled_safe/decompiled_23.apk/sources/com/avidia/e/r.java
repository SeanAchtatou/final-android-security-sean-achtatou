package com.avidia.e;

import android.content.SharedPreferences;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import com.avidia.b.i;
import com.avidia.c.b;
import com.avidia.c.c;
import com.avidia.c.d;
import com.avidia.c.e;
import com.avidia.d.a;
import com.avidia.fismobile.FisApplication;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.net.ssl.TrustManager;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.InputSource;

public class r {
    public static boolean a;
    static final HttpClient b;
    static TrustManager[] c = {new s()};
    private static final String d = r.class.getSimpleName();
    private static CookieStore e = new BasicCookieStore();
    private static final HttpContext f = new BasicHttpContext();

    static {
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        schemeRegistry.register(new Scheme("https", new a(), 443));
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        basicHttpParams.setParameter("http.conn-manager.max-total", 30);
        basicHttpParams.setParameter("http.conn-manager.max-per-route", new ConnPerRouteBean(30));
        basicHttpParams.setParameter("http.protocol.expect-continue", true);
        HttpProtocolParams.setVersion(basicHttpParams, HttpVersion.HTTP_1_1);
        ThreadSafeClientConnManager threadSafeClientConnManager = new ThreadSafeClientConnManager(basicHttpParams, schemeRegistry);
        f.setAttribute("http.cookie-store", e);
        b = new DefaultHttpClient(threadSafeClientConnManager, basicHttpParams);
    }

    static String a(String str) {
        if (str.startsWith("\"")) {
            str = str.substring(1);
        }
        if (str.endsWith("\"")) {
            str = str.substring(0, str.length() - 1);
        }
        return str.replace("\\", "");
    }

    public static String a(String str, String str2) {
        HttpPost httpPost = new HttpPost(str);
        httpPost.setEntity(new StringEntity(str2));
        ag.c(d, "Hitting URL:" + str);
        HttpClient a2 = a();
        try {
            String handleResponse = new BasicResponseHandler().handleResponse(a2.execute(httpPost, f));
            a2.getConnectionManager().closeExpiredConnections();
            return handleResponse;
        } catch (IOException e2) {
            ag.a(d, "Error when hitting the url:" + str, e2);
            throw e2;
        } catch (Throwable th) {
            a2.getConnectionManager().closeExpiredConnections();
            throw th;
        }
    }

    public static String a(String str, String str2, p pVar, String str3, i iVar) {
        HttpPost httpGet;
        URL url = new URL(str);
        ag.c(d, "Hitting URL:" + str);
        switch (t.a[pVar.ordinal()]) {
            case 1:
                httpGet = new HttpPost(str);
                if (str3 != null) {
                    if (com.avidia.a.a.e) {
                        Log.d(d, "POST body:" + str3);
                    }
                    httpGet.setEntity(new StringEntity(str3, "UTF-8"));
                    break;
                }
                break;
            case 2:
                httpGet = new HttpPut(str);
                if (str3 != null) {
                    ag.c(d, "PUT body:" + str3);
                    httpGet.setEntity(new StringEntity(str3, "UTF-8"));
                    break;
                }
                break;
            case 3:
                httpGet = new HttpGet(str);
                break;
            default:
                throw new IllegalArgumentException("Invalid method");
        }
        httpGet.setHeader("Host", url.getHost());
        httpGet.setHeader("Authorization", str2);
        httpGet.setHeader("Content-Type", iVar.a());
        HttpClient a2 = a();
        try {
            ag.c(d, "Request type:" + httpGet.getMethod());
            ag.c(d, "Headers");
            for (Header header : httpGet.getAllHeaders()) {
                ag.c(d, header.getName() + ":" + header.getValue());
            }
            HttpResponse execute = a2.execute(httpGet, f);
            if (FisApplication.k().o()) {
                FisApplication.k().n();
            }
            int statusCode = execute.getStatusLine().getStatusCode();
            ag.c(d, "Response status:" + statusCode);
            if (statusCode != 200) {
                String a3 = a(execute);
                ag.c(d, "Content:" + a3);
                a(statusCode, a3);
            }
            String handleResponse = new BasicResponseHandler().handleResponse(execute);
            if (com.avidia.a.a.e) {
                Log.d(d, "Response:" + handleResponse);
            }
            return handleResponse;
        } finally {
            a2.getConnectionManager().closeExpiredConnections();
        }
    }

    public static String a(String str, List list) {
        Throwable th;
        String str2;
        HttpClient a2 = a();
        HttpPost httpPost = new HttpPost(str);
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(list));
            if (com.avidia.a.a.e) {
                Log.d(d, "Hitting url:" + str);
            }
            c();
            HttpResponse execute = FisApplication.k().o() ? a2.execute(httpPost, f) : a2.execute(httpPost);
            if (execute.getStatusLine().getStatusCode() == 200) {
                if (FisApplication.k().o()) {
                    FisApplication.k().n();
                }
                str2 = new BasicResponseHandler().handleResponse(execute);
                try {
                    if (com.avidia.a.a.e) {
                        Log.d(d, "Response:" + str2);
                    }
                    String decode = URLDecoder.decode(str2, "utf-8");
                    if (com.avidia.a.a.e) {
                        Log.d(d, "Decoded response:" + decode);
                    }
                    String decode2 = URLDecoder.decode(decode);
                    if (com.avidia.a.a.e) {
                        Log.d(d, "Double decoded response:" + decode2);
                    }
                    a2.getConnectionManager().closeExpiredConnections();
                } catch (Throwable th2) {
                    th = th2;
                    try {
                        a(str, list, th);
                        return str2;
                    } finally {
                        a2.getConnectionManager().closeExpiredConnections();
                    }
                }
                return str2;
            }
            if (com.avidia.a.a.e) {
                Log.e(d, "Error:" + execute.getStatusLine().getReasonPhrase());
            }
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(execute.getEntity().getContent()));
            StringBuilder sb = new StringBuilder();
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    sb.append(readLine);
                } else {
                    throw new u(sb.toString());
                }
            }
        } catch (Throwable th3) {
            Throwable th4 = th3;
            str2 = "";
            th = th4;
            a(str, list, th);
            return str2;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0047 A[SYNTHETIC, Splitter:B:22:0x0047] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0058 A[SYNTHETIC, Splitter:B:29:0x0058] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String a(org.apache.http.HttpResponse r5) {
        /*
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r2 = 0
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ Throwable -> 0x003c, all -> 0x0054 }
            java.io.InputStreamReader r0 = new java.io.InputStreamReader     // Catch:{ Throwable -> 0x003c, all -> 0x0054 }
            org.apache.http.HttpEntity r4 = r5.getEntity()     // Catch:{ Throwable -> 0x003c, all -> 0x0054 }
            java.io.InputStream r4 = r4.getContent()     // Catch:{ Throwable -> 0x003c, all -> 0x0054 }
            r0.<init>(r4)     // Catch:{ Throwable -> 0x003c, all -> 0x0054 }
            r1.<init>(r0)     // Catch:{ Throwable -> 0x003c, all -> 0x0054 }
        L_0x0018:
            java.lang.String r0 = r1.readLine()     // Catch:{ Throwable -> 0x0067 }
            if (r0 == 0) goto L_0x0021
            r3.append(r0)     // Catch:{ Throwable -> 0x0067 }
        L_0x0021:
            if (r0 == 0) goto L_0x0029
            int r0 = r0.length()     // Catch:{ Throwable -> 0x0067 }
            if (r0 != 0) goto L_0x0018
        L_0x0029:
            if (r1 == 0) goto L_0x002e
            r1.close()     // Catch:{ Throwable -> 0x0033 }
        L_0x002e:
            java.lang.String r0 = r3.toString()
            return r0
        L_0x0033:
            r0 = move-exception
            java.lang.String r1 = com.avidia.e.r.d
            java.lang.String r2 = ""
            com.avidia.e.ag.a(r1, r2, r0)
            goto L_0x002e
        L_0x003c:
            r0 = move-exception
            r1 = r2
        L_0x003e:
            java.lang.String r2 = com.avidia.e.r.d     // Catch:{ all -> 0x0065 }
            java.lang.String r4 = ""
            com.avidia.e.ag.a(r2, r4, r0)     // Catch:{ all -> 0x0065 }
            if (r1 == 0) goto L_0x002e
            r1.close()     // Catch:{ Throwable -> 0x004b }
            goto L_0x002e
        L_0x004b:
            r0 = move-exception
            java.lang.String r1 = com.avidia.e.r.d
            java.lang.String r2 = ""
            com.avidia.e.ag.a(r1, r2, r0)
            goto L_0x002e
        L_0x0054:
            r0 = move-exception
            r1 = r2
        L_0x0056:
            if (r1 == 0) goto L_0x005b
            r1.close()     // Catch:{ Throwable -> 0x005c }
        L_0x005b:
            throw r0
        L_0x005c:
            r1 = move-exception
            java.lang.String r2 = com.avidia.e.r.d
            java.lang.String r3 = ""
            com.avidia.e.ag.a(r2, r3, r1)
            goto L_0x005b
        L_0x0065:
            r0 = move-exception
            goto L_0x0056
        L_0x0067:
            r0 = move-exception
            goto L_0x003e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avidia.e.r.a(org.apache.http.HttpResponse):java.lang.String");
    }

    public static HttpClient a() {
        return b;
    }

    static void a(int i, String str) {
        Throwable th;
        if (i == 503) {
            try {
                ag.b(d, "Error:statusCode=" + i + ", " + str);
                th = new com.avidia.c.a(str);
            } catch (Throwable th2) {
                ag.a(d, "", th2);
                th = null;
            }
        } else if (i == 400 || i == 404 || i == 500) {
            ag.b(d, "Error:statusCode=" + i + ", " + str);
            th = new RuntimeException("Server error");
        } else {
            d c2 = str.startsWith("<HttpError") ? c(str) : d(str);
            th = "12049".equals(c2.b()) ? new c() : c2.d() ? new b() : new e(c2);
        }
        if (th != null) {
            throw th;
        }
    }

    public static void a(SharedPreferences.Editor editor) {
        ArrayList arrayList = new ArrayList();
        for (Cookie cookie : e.getCookies()) {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("name", cookie.getName());
                jSONObject.put("value", cookie.getValue());
                jSONObject.put("domain", cookie.getDomain());
                jSONObject.put("path", cookie.getPath());
                arrayList.add(jSONObject);
            } catch (JSONException e2) {
                if (com.avidia.a.a.e) {
                    Log.e(d, "", e2);
                }
            }
            editor.putString("cookies", new JSONArray((Collection) arrayList).toString());
        }
    }

    public static void a(SharedPreferences sharedPreferences) {
        String string = sharedPreferences.getString("cookies", "");
        if (!TextUtils.isEmpty(string)) {
            try {
                JSONArray jSONArray = new JSONArray(string);
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    BasicClientCookie basicClientCookie = new BasicClientCookie(jSONObject.getString("name"), jSONObject.getString("value"));
                    basicClientCookie.setPath(jSONObject.getString("path"));
                    basicClientCookie.setDomain(jSONObject.getString("domain"));
                    e.addCookie(basicClientCookie);
                }
            } catch (JSONException e2) {
                if (com.avidia.a.a.e) {
                    Log.e(d, "", e2);
                }
            }
        }
    }

    private static void a(String str, List list, Throwable th) {
        StringBuilder append = new StringBuilder("Error when posting data to url:").append(str);
        if (list != null) {
            append.append(", parameters:");
            Iterator it = list.iterator();
            while (it.hasNext()) {
                NameValuePair nameValuePair = (NameValuePair) it.next();
                append.append("\nname=").append(nameValuePair.getName()).append(",value=").append(nameValuePair.getValue());
            }
        }
        ag.a(d, append.toString(), th);
        throw th;
    }

    public static void b() {
        e.clear();
    }

    public static void b(String str) {
        Cookie cookie;
        String a2 = ag.a(Settings.Secure.getString(FisApplication.k().getContentResolver(), "android_id"));
        String format = String.format("device_%s", str);
        String format2 = String.format("userid=%s&deviceid=%s", str, a2);
        ArrayList<Cookie> arrayList = new ArrayList<>(e.getCookies());
        Iterator it = arrayList.iterator();
        while (true) {
            if (!it.hasNext()) {
                cookie = null;
                break;
            }
            cookie = (Cookie) it.next();
            if (cookie.getDomain().equals("m.wealthcareadmin.com") && cookie.getName().equals(format) && cookie.getValue().startsWith(format2)) {
                break;
            }
        }
        if (cookie != null) {
            arrayList.remove(cookie);
            e.clear();
            for (Cookie addCookie : arrayList) {
                e.addCookie(addCookie);
            }
            FisApplication.k().n();
        }
    }

    private static d c(String str) {
        Throwable th;
        d dVar;
        d dVar2 = new d();
        try {
            SAXParser newSAXParser = SAXParserFactory.newInstance().newSAXParser();
            x xVar = new x();
            newSAXParser.parse(new InputSource(new StringReader(str)), xVar);
            dVar = xVar.a();
            try {
                if (dVar.c().equals("1000") && dVar.a().toLowerCase().contains("expired")) {
                    dVar.a(true);
                }
            } catch (Throwable th2) {
                th = th2;
                ag.a(d, "", th);
                return dVar;
            }
        } catch (Throwable th3) {
            Throwable th4 = th3;
            dVar = dVar2;
            th = th4;
        }
        return dVar;
    }

    public static void c() {
        for (Cookie cookie : e.getCookies()) {
            ag.c(d, "Cookie:" + cookie.getDomain() + "|" + cookie.getPath() + "|" + cookie.getName() + "|" + cookie.getValue());
        }
    }

    private static d d(String str) {
        String a2 = a(str);
        if (com.avidia.a.a.e) {
            Log.d(d, "JSON value:" + a2);
        }
        JSONObject jSONObject = new JSONObject(a2.trim());
        d dVar = new d();
        dVar.b(jSONObject.getString("Code"));
        dVar.a(jSONObject.getString("Description"));
        dVar.d(jSONObject.getString("Module"));
        dVar.c(jSONObject.getString("Id"));
        return dVar;
    }
}
