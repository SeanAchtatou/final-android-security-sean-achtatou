package com.avidia.e;

import android.support.v4.app.h;
import android.widget.ArrayAdapter;
import com.avidia.fismobile.C0000R;
import com.avidia.fismobile.w;
import java.util.List;

public class j {
    public static int a(boolean z) {
        return z ? 0 : 8;
    }

    public static String a(String str) {
        return str.replaceAll("\\$", "").replaceAll(",", "");
    }

    public static void a(h hVar, w wVar, List list, int i) {
        if (wVar.a()) {
            wVar.b();
        }
        wVar.a(new ArrayAdapter(hVar, (int) C0000R.layout.spinner_item, list));
        if (i >= 0) {
            wVar.a(i);
        }
        wVar.c();
    }

    public static double b(String str) {
        try {
            return Double.valueOf(a(str)).doubleValue();
        } catch (Exception e) {
            return 0.0d;
        }
    }
}
