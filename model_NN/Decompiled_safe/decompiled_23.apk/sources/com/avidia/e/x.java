package com.avidia.e;

import com.avidia.c.d;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class x extends DefaultHandler {
    public static final String a = aa.class.getSimpleName();
    private final d b = new d();
    private StringBuilder c = new StringBuilder();

    public d a() {
        return this.b;
    }

    public void characters(char[] cArr, int i, int i2) {
        super.characters(cArr, i, i2);
        this.c.append(cArr, i, i2);
    }

    public void endElement(String str, String str2, String str3) {
        super.endElement(str, str2, str3);
        if ("Code".equalsIgnoreCase(str2)) {
            this.b.b(this.c.toString());
        } else if ("Description".equalsIgnoreCase(str2)) {
            this.b.a(this.c.toString());
        } else if ("Id".equalsIgnoreCase(str2)) {
            this.b.c(this.c.toString());
        } else if ("Module".equalsIgnoreCase(str2)) {
            this.b.d(this.c.toString());
        }
        this.c.setLength(0);
    }

    public void startElement(String str, String str2, String str3, Attributes attributes) {
        super.startElement(str, str2, str3, attributes);
    }
}
