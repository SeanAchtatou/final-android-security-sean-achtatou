package com.avidia.e;

import com.avidia.b.i;
import com.avidia.b.m;
import com.avidia.b.v;
import com.avidia.c.a;
import com.avidia.fismobile.FisApplication;
import java.net.URLEncoder;
import java.util.Map;

public class h {
    /* access modifiers changed from: protected */
    public v a(String str, String str2) {
        return a(str, str2, p.GET, (String) null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avidia.e.h.a(java.lang.StringBuilder, java.util.Map, java.lang.String, boolean):void
     arg types: [java.lang.StringBuilder, java.util.Map, java.lang.String, int]
     candidates:
      com.avidia.e.h.a(java.lang.String, java.lang.String, com.avidia.e.p, java.lang.String):com.avidia.b.v
      com.avidia.e.h.a(java.lang.StringBuilder, java.util.Map, java.lang.String, boolean):void */
    /* access modifiers changed from: protected */
    public v a(String str, String str2, p pVar, String str3) {
        v vVar = new v();
        String str4 = "WRAP access_token=" + FisApplication.k().q();
        StringBuilder sb = new StringBuilder(str);
        Map r = FisApplication.k().r();
        try {
            a(sb, r, "http://schema.bensoft.metavante.com/identity/claims/tpaid", true);
            a(sb, r, "http://schema.bensoft.metavante.com/identity/claims/employerid", false);
            a(sb, r, "http://schema.bensoft.metavante.com/identity/claims/employeeid", false);
            sb.append(str2);
            String a = r.a(sb.toString().replace("%", "%25"), str4, pVar, str3, i.JSON);
            vVar.a = true;
            vVar.b = a;
        } catch (Throwable th) {
            vVar.a = false;
            vVar.c = new m(th);
            if (th instanceof a) {
                vVar.c.b = th.getMessage();
            }
        }
        return vVar;
    }

    /* access modifiers changed from: package-private */
    public void a(StringBuilder sb, Map map, String str, boolean z) {
        String str2 = (String) map.get(str);
        if (str2 != null) {
            if (!z) {
                sb.append("/");
            }
            ag.c(y.a, "Param name:" + str);
            ag.c(y.a, "Param value:" + str2);
            for (int i = 0; i < str2.length(); i++) {
                char charAt = str2.charAt(i);
                if (charAt == '/' || charAt == '&') {
                    sb.append(URLEncoder.encode(String.valueOf(charAt), "UTF-8").toLowerCase());
                } else {
                    sb.append(charAt);
                }
            }
            String encode = URLEncoder.encode(str2, "UTF-8");
            ag.c(y.a, "Param encoded value:" + encode);
            ag.c(y.a, "Param double encoded value:" + URLEncoder.encode(encode, "UTF-8"));
            return;
        }
        ag.b(y.a, "Parameter not found:" + str);
    }
}
