package com.avidia.e;

import com.avidia.b.e;
import com.avidia.b.v;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class b extends h implements q {
    private final String a;

    public b(String str) {
        this.a = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avidia.b.e.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.avidia.b.e.a(java.lang.String, java.lang.String):void
      com.avidia.b.e.a(org.json.JSONArray, boolean):void
      com.avidia.b.e.a(org.json.JSONObject, boolean):void
      com.avidia.b.e.a(java.lang.String, boolean):void */
    private String a(String str) {
        e eVar = new e();
        eVar.a(str, true);
        return eVar.a(false);
    }

    private String a(String str, List list) {
        JSONObject jSONObject = new JSONObject(str);
        JSONObject jSONObject2 = jSONObject.optJSONArray("Claims").getJSONObject(0);
        JSONArray optJSONArray = jSONObject2.optJSONArray("ReceiptsInfo");
        if (optJSONArray == null) {
            return str;
        }
        jSONObject2.remove("ReceiptsInfo");
        jSONObject2.put("Receipt", optJSONArray.getJSONObject(0));
        JSONArray jSONArray = new JSONArray();
        jSONArray.put(jSONObject2);
        jSONObject.remove("Claims");
        jSONObject.put("Claims", jSONArray);
        for (int i = 1; i < optJSONArray.length(); i++) {
            JSONObject optJSONObject = optJSONArray.optJSONObject(i);
            if (optJSONObject != null) {
                list.add(optJSONObject);
            }
        }
        return jSONObject.toString();
    }

    public v a() {
        ArrayList<JSONObject> arrayList = new ArrayList<>();
        v a2 = a("https://m.wealthcareadmin.com/ServiceProvider/services/participant/claims/", "?decrypt=0", p.POST, a(a(this.a), arrayList));
        if (!arrayList.isEmpty() && a2.a) {
            int i = new JSONArray(a2.b).getJSONObject(0).getInt("ClaimKey");
            for (JSONObject jSONObject : arrayList) {
                a2 = a("https://m.wealthcareadmin.com/ServiceProvider/services/participant/receipts/submitted/", String.format("?claimkey=%d%s", Integer.valueOf(i), "&decrypt=0"), p.PUT, jSONObject.toString());
                if (!(a2 == null || a2.b == null)) {
                    ag.c(getClass().getSimpleName(), a2.b);
                }
            }
        }
        return a2;
    }
}
