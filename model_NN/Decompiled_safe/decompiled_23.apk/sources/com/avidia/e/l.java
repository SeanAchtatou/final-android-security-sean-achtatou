package com.avidia.e;

import android.util.Log;
import com.avidia.a.a;
import com.avidia.b.v;

public class l extends h implements q {
    public static final String a = l.class.getSimpleName();
    private final int b;

    public l(int i) {
        this.b = i;
    }

    public v a() {
        String str = "?flexaccountkey=" + String.valueOf(this.b) + "&decrypt=0";
        if (a.e) {
            Log.d(a, "Contributions request with params: " + str);
        }
        return a("https://m.wealthcareadmin.com/ServiceProvider/services/participant/hsa/contributions/", str);
    }
}
