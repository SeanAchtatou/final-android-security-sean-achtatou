package com.avidia.e;

import android.util.Log;
import com.avidia.a.a;
import com.avidia.b.v;

public class n extends h implements q {
    public static final String a = n.class.getSimpleName();
    private String b;
    private p c;

    public n(String str, boolean z) {
        this.b = str;
        this.c = z ? p.POST : p.PUT;
    }

    public v a() {
        if (a.e) {
            Log.d(a, "Contributions request with params: ?decrypt=0");
        }
        return a("https://m.wealthcareadmin.com/ServiceProvider/services/participant/hsa/contributions/", "/?decrypt=0", this.c, this.b);
    }
}
