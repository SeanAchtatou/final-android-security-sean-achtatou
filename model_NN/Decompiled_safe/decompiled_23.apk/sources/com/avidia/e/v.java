package com.avidia.e;

import android.util.Log;
import com.avidia.a.a;
import com.avidia.b.i;
import com.avidia.c.f;
import com.avidia.fismobile.FisApplication;
import java.util.ArrayList;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

public class v implements q {
    private static final String a = v.class.getSimpleName();
    private String b;
    private String c;

    public v(String str, String str2) {
        this.b = str;
        this.c = str2;
    }

    private boolean a(String str) {
        try {
            return "1.0".compareTo(new JSONObject(str).getString("CurrentVersion")) == -1;
        } catch (Throwable th) {
            if (!a.e) {
                return false;
            }
            Log.e(a, "", th);
            return false;
        }
    }

    public com.avidia.b.v a() {
        String a2 = r.a("https://m.wealthcareadmin.com/issuer/handshake/bootstrap/", a.c);
        ag.c(a, "Verification result: " + a2);
        com.avidia.b.v vVar = new com.avidia.b.v();
        if (a(a2)) {
            throw new f();
        }
        ag.c(a, "Device id:" + this.c);
        String a3 = ag.a(this.c);
        ag.c(a, "Device id(MD5):" + a3);
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("wrap_name", this.b));
        arrayList.add(new BasicNameValuePair("deviceid", a3));
        arrayList.add(new BasicNameValuePair("tpaid", a.b));
        String a4 = r.a("https://m.wealthcareadmin.com/issuer/handshake/", arrayList);
        FisApplication.k().f(this.b);
        FisApplication.k().e(a4);
        if (a.e) {
            Log.d(a, a4);
        }
        vVar.b = r.a("https://m.wealthcareadmin.com/issuer/challenge/" + this.b, ag.a(), p.POST, null, i.JSON);
        vVar.a = true;
        return vVar;
    }
}
