package com.avidia.e;

import android.app.Activity;
import android.view.View;
import com.avidia.fismobile.view.bm;

final class ah implements View.OnClickListener {
    final /* synthetic */ bm a;
    final /* synthetic */ Activity b;

    ah(bm bmVar, Activity activity) {
        this.a = bmVar;
        this.b = activity;
    }

    public void onClick(View view) {
        this.a.dismiss();
        this.b.finish();
        ag.a(this.b);
    }
}
