package com.avidia.e;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.View;
import com.avidia.fismobile.C0000R;
import com.avidia.fismobile.FisApplication;
import com.avidia.fismobile.h;

public class ac {
    public static Dialog a(Context context, int i, int i2) {
        Dialog dialog = new Dialog(context, i2);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(i);
        if (context instanceof Activity) {
            dialog.setOwnerActivity((Activity) context);
        }
        return dialog;
    }

    public static Dialog a(h hVar, int i, String str, int i2) {
        Dialog a = a(hVar, (int) C0000R.layout.choose_photo_dlg, 2131296302);
        a.findViewById(C0000R.id.cancel).setOnClickListener(new ad(a));
        View findViewById = a.findViewById(C0000R.id.take_photo);
        if (ag.a(FisApplication.k().getPackageManager())) {
            findViewById.setOnClickListener(new ae(hVar, i, str, a));
        } else {
            findViewById.setVisibility(8);
        }
        a.findViewById(C0000R.id.choose_photo).setOnClickListener(new af(hVar, i2, a));
        return a;
    }

    public static void a(h hVar, int i) {
        hVar.l();
        Intent intent = new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        hVar.startActivityForResult(Intent.createChooser(intent, hVar.getString(C0000R.string.choose_picture)), i);
    }

    public static void a(h hVar, int i, String str) {
        hVar.l();
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        intent.putExtra("output", Uri.parse(str));
        hVar.startActivityForResult(intent, i);
    }
}
