package com.avidia.e;

import android.util.Log;
import com.avidia.a.a;
import com.avidia.b.i;
import com.avidia.b.m;
import com.avidia.b.v;
import com.avidia.fismobile.FisApplication;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.message.BasicNameValuePair;

public class y extends h implements q {
    public static final String a = y.class.getSimpleName();
    private String b;

    public y(String str) {
        this.b = str;
    }

    private void a(String str) {
        String substring = str.substring(18);
        if (a.e) {
            Log.d(a, "Data received after logon:" + substring);
        }
        String decode = URLDecoder.decode(substring, "UTF-8");
        if (a.e) {
            Log.d(a, "Data after decoding:" + decode);
        }
        FisApplication.k().g(decode);
        String decode2 = URLDecoder.decode(decode, "UTF-8");
        if (a.e) {
            Log.d(a, "Data after double decoding:" + decode2);
        }
        FisApplication.k().b(b(decode2));
    }

    private List b(String str) {
        ArrayList arrayList = new ArrayList();
        String substring = str.substring(str.lastIndexOf(47, str.indexOf(61)) + 1);
        if (a.e) {
            Log.d(a, "Parameters string:" + substring);
        }
        for (String str2 : substring.split("&")) {
            int indexOf = str2.indexOf("=");
            arrayList.add(new BasicNameValuePair(str2.substring(0, indexOf), str2.substring(indexOf + 1)));
        }
        return arrayList;
    }

    public v a() {
        try {
            String a2 = r.a("https://m.wealthcareadmin.com/ServiceProvider/services/logon/", ag.a(), p.POST, ag.b(this.b), i.JSON);
            ag.c(a, "Logon result:" + a2);
            a(a2);
            return a("https://m.wealthcareadmin.com/ServiceProvider/services/participant/accounts/summary/", "?planyear=" + a.d[1] + "&decrypt=1");
        } catch (Throwable th) {
            Throwable th2 = th;
            v vVar = new v();
            vVar.a = false;
            vVar.c = new m(th2);
            return vVar;
        }
    }
}
