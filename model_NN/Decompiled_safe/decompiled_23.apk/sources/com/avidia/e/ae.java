package com.avidia.e;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.view.View;
import com.avidia.fismobile.C0000R;
import com.avidia.fismobile.h;

final class ae implements View.OnClickListener {
    final /* synthetic */ h a;
    final /* synthetic */ int b;
    final /* synthetic */ String c;
    final /* synthetic */ Dialog d;

    ae(h hVar, int i, String str, Dialog dialog) {
        this.a = hVar;
        this.b = i;
        this.c = str;
        this.d = dialog;
    }

    public void onClick(View view) {
        try {
            ac.a(this.a, this.b, this.c);
        } catch (ActivityNotFoundException e) {
            ag.a(this.a.getClass().getSimpleName(), "Photo Activity not found", e);
            this.a.a((int) C0000R.string.open_camera_error);
            this.a.m();
        }
        this.d.dismiss();
    }
}
