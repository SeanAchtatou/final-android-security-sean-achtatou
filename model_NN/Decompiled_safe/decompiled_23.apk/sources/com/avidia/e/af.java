package com.avidia.e;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.view.View;
import com.avidia.fismobile.C0000R;
import com.avidia.fismobile.h;

final class af implements View.OnClickListener {
    final /* synthetic */ h a;
    final /* synthetic */ int b;
    final /* synthetic */ Dialog c;

    af(h hVar, int i, Dialog dialog) {
        this.a = hVar;
        this.b = i;
        this.c = dialog;
    }

    public void onClick(View view) {
        try {
            ac.a(this.a, this.b);
        } catch (ActivityNotFoundException e) {
            ag.a(this.a.getClass().getSimpleName(), "Media Activity not found", e);
            this.a.a((int) C0000R.string.open_media_error);
            this.a.m();
        }
        this.c.dismiss();
    }
}
