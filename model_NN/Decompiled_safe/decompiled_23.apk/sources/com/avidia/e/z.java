package com.avidia.e;

import android.util.Log;
import com.avidia.a.a;
import com.avidia.b.i;
import com.avidia.b.v;
import com.avidia.fismobile.FisApplication;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

public class z extends h implements q {
    public static final String a = z.class.getSimpleName();

    private List a(String str) {
        ArrayList arrayList = new ArrayList();
        try {
            JSONObject jSONObject = new JSONObject(str);
            arrayList.add(new BasicNameValuePair("http://schema.bensoft.metavante.com/identity/claims/employeeid", String.valueOf(jSONObject.get("EmployeeId"))));
            arrayList.add(new BasicNameValuePair("http://schema.bensoft.metavante.com/identity/claims/employerid", String.valueOf(jSONObject.get("EmployerId"))));
            arrayList.add(new BasicNameValuePair("http://schema.bensoft.metavante.com/identity/claims/tpaid", String.valueOf(jSONObject.get("TpaId"))));
            arrayList.add(new BasicNameValuePair("http://schema.bensoft.metavante.com/identity/claims/userid", String.valueOf(jSONObject.get("UserId"))));
            arrayList.add(new BasicNameValuePair("ParticipantAccessRights", String.valueOf(jSONObject.optInt("ParticipantAccessRights"))));
        } catch (Exception e) {
            if (a.e) {
                Log.d(a, "JSON parsing error: " + e.getMessage());
            }
        }
        return arrayList;
    }

    public v a() {
        ag.c(a, "UserInfo URL: https://m.wealthcareadmin.com/ServiceProvider/services/userinfo/?accessattributes=3");
        try {
            String a2 = r.a("https://m.wealthcareadmin.com/ServiceProvider/services/userinfo/?accessattributes=3", "WRAP access_token=" + FisApplication.k().q(), p.GET, null, i.JSON);
            ag.c(a, "Data received after logon:" + a2);
            FisApplication.k().a(a(a2));
        } catch (Throwable th) {
            ag.c(a, "User Info not loaded. " + th.getMessage());
        }
        return a("https://m.wealthcareadmin.com/ServiceProvider/services/participant/accounts/summary/", "?planyear=" + a.d[1] + "&decrypt=0");
    }
}
