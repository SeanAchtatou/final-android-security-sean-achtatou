package com.avidia.e;

import com.avidia.b.v;

public class c extends h implements q {
    private final String a;
    private final int b;

    public c(int i, String str) {
        this.b = i;
        this.a = str;
    }

    public v a() {
        v a2 = a("https://m.wealthcareadmin.com/ServiceProvider/services/participant/receipts/submitted/", String.format("?claimkey=%d%s", Integer.valueOf(this.b), "&decrypt=0"), p.PUT, this.a);
        if (!(a2 == null || a2.b == null)) {
            ag.c(getClass().getSimpleName(), a2.b);
        }
        return a2;
    }
}
