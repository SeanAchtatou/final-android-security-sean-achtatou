package com.avidia.e;

import android.util.Log;
import com.avidia.a.a;
import com.avidia.b.y;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class aa extends DefaultHandler {
    public static final String a = aa.class.getSimpleName();
    private final y b = new y();
    private StringBuilder c = new StringBuilder();

    public y a() {
        return this.b;
    }

    public void characters(char[] cArr, int i, int i2) {
        super.characters(cArr, i, i2);
        this.c.append(cArr, i, i2);
    }

    public void endElement(String str, String str2, String str3) {
        super.endElement(str, str2, str3);
        if ("Phrase".equalsIgnoreCase(str2)) {
            this.b.a = this.c.toString();
            if (a.e) {
                Log.d(a, "Phrase:" + this.b.a);
            }
        } else if ("SiteImage".equalsIgnoreCase(str2)) {
            this.b.b = ag.c(this.c.toString());
        }
        this.c.setLength(0);
    }

    public void startElement(String str, String str2, String str3, Attributes attributes) {
        super.startElement(str, str2, str3, attributes);
    }
}
