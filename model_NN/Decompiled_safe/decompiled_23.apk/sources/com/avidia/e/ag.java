package com.avidia.e;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v4.app.h;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import com.avidia.a.a;
import com.avidia.b.l;
import com.avidia.b.s;
import com.avidia.b.v;
import com.avidia.c.b;
import com.avidia.c.e;
import com.avidia.c.f;
import com.avidia.fismobile.C0000R;
import com.avidia.fismobile.FisApplication;
import com.avidia.fismobile.HtmlViewActivity;
import com.avidia.fismobile.SignInActivity;
import com.avidia.fismobile.view.AccountsFragmentActivity;
import com.avidia.fismobile.view.ClaimsFragmentActivity;
import com.avidia.fismobile.view.MoreActionsFragmentActivity;
import com.avidia.fismobile.view.MyAlertsFragmentActivity;
import com.avidia.fismobile.view.bm;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.regex.Pattern;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.SSLException;
import org.apache.http.client.HttpResponseException;
import org.json.JSONArray;
import org.json.JSONObject;

public class ag {
    private static final String a = ag.class.getSimpleName();
    private static final SimpleDateFormat b = new SimpleDateFormat("MM/dd/yyyy");
    private static final SimpleDateFormat c = new SimpleDateFormat("yyyyMMdd");

    public static double a(JSONObject jSONObject, String str) {
        try {
            String string = jSONObject.getString(str);
            if (string == null) {
                return 0.0d;
            }
            return Double.parseDouble(string.trim().replaceAll(",", "."));
        } catch (Exception e) {
            return 0.0d;
        }
    }

    private static int a(BitmapFactory.Options options, int i) {
        int i2 = options.outHeight;
        int i3 = options.outWidth;
        if (i2 <= i && i3 <= i) {
            return 1;
        }
        int round = Math.round(((float) i2) / ((float) i));
        int round2 = Math.round(((float) i3) / ((float) i));
        return round > round2 ? round : round2;
    }

    public static int a(boolean z) {
        return z ? 1 : 0;
    }

    public static Bitmap a(Bitmap bitmap) {
        if (bitmap.getWidth() <= 1280 && bitmap.getHeight() <= 1280) {
            return bitmap;
        }
        double max = 1280.0d / ((double) Math.max(bitmap.getWidth(), bitmap.getHeight()));
        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(bitmap, (int) (((double) bitmap.getWidth()) * max), (int) (max * ((double) bitmap.getHeight())), true);
        if (bitmap != createScaledBitmap) {
            bitmap.recycle();
        }
        return createScaledBitmap;
    }

    public static Bitmap a(Uri uri, h hVar) {
        if (!b(uri, hVar)) {
            return null;
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(c(uri, hVar), options);
        int a2 = a(options, 1280);
        System.gc();
        BitmapFactory.Options options2 = new BitmapFactory.Options();
        options2.inSampleSize = a2;
        InputStream openInputStream = hVar.getContentResolver().openInputStream(uri);
        Bitmap decodeStream = BitmapFactory.decodeStream(openInputStream, null, options2);
        a(openInputStream);
        return decodeStream;
    }

    public static Bitmap a(h hVar, String str) {
        try {
            File d = d(str, c().getName());
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(d.getAbsolutePath(), options);
            int a2 = a(options, 1280);
            System.gc();
            BitmapFactory.Options options2 = new BitmapFactory.Options();
            options2.inSampleSize = a2;
            InputStream openInputStream = hVar.getContentResolver().openInputStream(Uri.fromFile(d));
            Bitmap decodeStream = BitmapFactory.decodeStream(openInputStream, null, options2);
            System.gc();
            a(openInputStream);
            d.delete();
            return decodeStream;
        } catch (Exception e) {
            return null;
        }
    }

    private static File a(Context context, String str) {
        File externalCacheDir = context.getExternalCacheDir();
        if (externalCacheDir == null) {
            externalCacheDir = context.getCacheDir();
        }
        File file = new File(externalCacheDir, str);
        if (file.isDirectory() || file.mkdirs()) {
            return file;
        }
        return null;
    }

    public static String a() {
        String str = a.a;
        String l = FisApplication.k().l();
        String a2 = a(l, str);
        if (a.e) {
            Log.d(a, "hmac-sha256:" + a2);
        }
        return "handshakestart handshakeseed:" + l + ",handshakehmac:" + a2.toUpperCase() + " handshakeend";
    }

    public static String a(Bitmap bitmap, boolean z) {
        try {
            bitmap = a(bitmap);
            return b(bitmap, z);
        } catch (Exception e) {
            if (bitmap != null && z) {
                bitmap.recycle();
            }
            return "";
        }
    }

    public static String a(v vVar) {
        return (vVar == null || vVar.c == null || vVar.c.a == null) ? "Server error" : a(vVar.c.a);
    }

    public static String a(Number number) {
        return e(new DecimalFormat("0.00").format(number));
    }

    public static String a(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            byte[] digest = instance.digest();
            StringBuilder sb = new StringBuilder();
            int i = 0;
            while (i < digest.length && i <= 8) {
                sb.append(Integer.toHexString(digest[i] & 255));
                i++;
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            if (!a.e) {
                return "";
            }
            Log.e(a, "", e);
            return "";
        }
    }

    public static String a(String str, String str2) {
        Mac instance = Mac.getInstance("HmacSHA256");
        instance.init(new SecretKeySpec(str2.getBytes(), instance.getAlgorithm()));
        byte[] doFinal = instance.doFinal(str.getBytes());
        StringBuilder sb = new StringBuilder();
        int length = doFinal.length;
        for (int i = 0; i < length; i++) {
            sb.append(String.format("%02x", Byte.valueOf(doFinal[i])));
        }
        return sb.toString();
    }

    public static String a(Throwable th) {
        a(a, "", th);
        StringBuilder sb = new StringBuilder();
        if (th instanceof b) {
            sb.append("Your session is expired, please log in again");
        } else if (th instanceof u) {
            sb.append("Server is currently unavailable, please try later");
        } else if (th instanceof e) {
            e eVar = (e) th;
            if (eVar.a() != null) {
                sb.append(eVar.a());
            } else {
                sb.append(eVar.getMessage());
            }
        } else if (th instanceof f) {
            sb.append(FisApplication.k().getString(C0000R.string.unsupported_version));
        } else if (th instanceof SSLException) {
            sb.append(FisApplication.k().getString(C0000R.string.unsupported_untrusted_certificate));
        } else if ((th instanceof HttpResponseException) && (((HttpResponseException) th).getStatusCode() == 500 || ((HttpResponseException) th).getStatusCode() == 400)) {
            sb.append(FisApplication.k().getString(C0000R.string.internal_server_error));
        } else if ((th instanceof HttpResponseException) && ((HttpResponseException) th).getStatusCode() == 503) {
            sb.append(th.getMessage());
        } else if (th instanceof IOException) {
            sb.append(FisApplication.k().getString(C0000R.string.internet_connection_error));
        } else {
            sb.append("Server error");
        }
        return sb.toString();
    }

    public static String a(Calendar calendar) {
        return b.format(Long.valueOf(calendar.getTimeInMillis()));
    }

    public static String a(List list) {
        StringBuilder sb = new StringBuilder("<UserQuestionAnswers xmlns=\"http://schema.wealthcareadmin.com/payload/mobile/2012/06\">");
        Iterator it = list.iterator();
        while (it.hasNext()) {
            s sVar = (s) it.next();
            sb.append(e("UserQuestionAnswer", e("Answer", e("Content", sVar.b)) + e("Question", e("Content", sVar.a))));
        }
        sb.append("</UserQuestionAnswers>");
        return sb.toString();
    }

    public static List a(JSONArray jSONArray) {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject jSONObject = jSONArray.getJSONObject(i);
            l lVar = new l();
            lVar.a(jSONObject.getString("AlternativeFieldName"));
            lVar.b(jSONObject.getString("OriginalFieldName"));
            arrayList.add(lVar);
        }
        return arrayList;
    }

    public static void a(Context context) {
        if (context == null) {
            context = FisApplication.k();
        }
        FisApplication k = FisApplication.k();
        k.a(false);
        k.f();
        Intent intent = new Intent(context, SignInActivity.class);
        intent.addFlags(67108864);
        intent.addFlags(268435456);
        context.startActivity(intent);
        d();
    }

    private static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Exception e) {
            }
        }
    }

    public static void a(String str, String str2, Throwable th) {
        if (a.e) {
            Log.e(str, str2, th);
        }
    }

    public static boolean a(int i) {
        return i != 0;
    }

    public static boolean a(int i, Context context) {
        switch (i) {
            case C0000R.id.menu_accounts /*2131427498*/:
                Intent intent = new Intent(context, AccountsFragmentActivity.class);
                intent.putExtra("accountData", FisApplication.k().u());
                intent.putExtra("filter_name", FisApplication.k().i());
                context.startActivity(intent);
                break;
            case C0000R.id.menu_claim /*2131427499*/:
                context.startActivity(new Intent(context, ClaimsFragmentActivity.class));
                break;
            case C0000R.id.menu_alerts /*2131427500*/:
                context.startActivity(new Intent(context, MyAlertsFragmentActivity.class));
                break;
            case C0000R.id.menu_contactus /*2131427501*/:
                Intent intent2 = new Intent(context, HtmlViewActivity.class);
                intent2.putExtra("html_text", FisApplication.k().getString(C0000R.string.contact_us_message));
                intent2.putExtra("HEADER_ID", (int) C0000R.string.contact_us);
                context.startActivity(intent2);
                break;
            case C0000R.id.menu_more /*2131427502*/:
                context.startActivity(new Intent(context, MoreActionsFragmentActivity.class));
                break;
            default:
                return false;
        }
        return true;
    }

    public static boolean a(Activity activity, v vVar) {
        if (vVar.a || vVar.c == null || !(vVar.c.a instanceof b)) {
            return false;
        }
        bm bmVar = new bm(activity);
        bmVar.a("Token expired");
        bmVar.b("Your session is expired, please log in again");
        bmVar.setCancelable(false);
        bmVar.a(new ah(bmVar, activity));
        bmVar.show();
        return true;
    }

    public static boolean a(PackageManager packageManager) {
        try {
            return packageManager.queryIntentActivities(new Intent("android.media.action.IMAGE_CAPTURE"), 65536).size() > 0;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean a(MenuItem menuItem, Context context) {
        return a(menuItem.getItemId(), context);
    }

    public static String b(Bitmap bitmap) {
        if (bitmap == null || bitmap.isRecycled()) {
            return "";
        }
        Bitmap a2 = a(bitmap);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        a2.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
        String encodeToString = Base64.encodeToString(byteArrayOutputStream.toByteArray(), 0);
        a2.recycle();
        System.gc();
        return encodeToString;
    }

    /* JADX WARNING: Removed duplicated region for block: B:41:0x00ac A[SYNTHETIC, Splitter:B:41:0x00ac] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String b(android.graphics.Bitmap r7, boolean r8) {
        /*
            r0 = 0
            java.io.File r3 = c()
            r1 = 0
            boolean r2 = r3.createNewFile()     // Catch:{ Exception -> 0x006a, all -> 0x00a7 }
            if (r2 != 0) goto L_0x0041
            java.lang.String r2 = com.avidia.e.ag.a     // Catch:{ Exception -> 0x006a, all -> 0x00a7 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x006a, all -> 0x00a7 }
            r4.<init>()     // Catch:{ Exception -> 0x006a, all -> 0x00a7 }
            java.lang.String r5 = "Impossible to create the file: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x006a, all -> 0x00a7 }
            java.lang.String r5 = r3.getAbsolutePath()     // Catch:{ Exception -> 0x006a, all -> 0x00a7 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x006a, all -> 0x00a7 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x006a, all -> 0x00a7 }
            b(r2, r4)     // Catch:{ Exception -> 0x006a, all -> 0x00a7 }
            if (r0 == 0) goto L_0x0030
            r1.flush()     // Catch:{ Exception -> 0x0038 }
            r1.close()     // Catch:{ Exception -> 0x0038 }
        L_0x0030:
            if (r8 == 0) goto L_0x0037
            if (r7 == 0) goto L_0x0037
            r7.recycle()
        L_0x0037:
            return r0
        L_0x0038:
            r1 = move-exception
            java.lang.String r2 = com.avidia.e.ag.a
            java.lang.String r3 = ""
            android.util.Log.e(r2, r3, r1)
            goto L_0x0030
        L_0x0041:
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x006a, all -> 0x00a7 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x006a, all -> 0x00a7 }
            android.graphics.Bitmap$CompressFormat r1 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ Exception -> 0x00c5 }
            r4 = 90
            r7.compress(r1, r4, r2)     // Catch:{ Exception -> 0x00c5 }
            java.lang.String r0 = r3.getAbsolutePath()     // Catch:{ Exception -> 0x00c5 }
            if (r2 == 0) goto L_0x0059
            r2.flush()     // Catch:{ Exception -> 0x0061 }
            r2.close()     // Catch:{ Exception -> 0x0061 }
        L_0x0059:
            if (r8 == 0) goto L_0x0037
            if (r7 == 0) goto L_0x0037
            r7.recycle()
            goto L_0x0037
        L_0x0061:
            r1 = move-exception
            java.lang.String r2 = com.avidia.e.ag.a
            java.lang.String r3 = ""
            android.util.Log.e(r2, r3, r1)
            goto L_0x0059
        L_0x006a:
            r1 = move-exception
            r2 = r0
        L_0x006c:
            java.lang.String r4 = com.avidia.e.ag.a     // Catch:{ all -> 0x00c3 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c3 }
            r5.<init>()     // Catch:{ all -> 0x00c3 }
            java.lang.String r6 = "Error while saving bitmap: "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x00c3 }
            java.lang.String r3 = r3.getAbsolutePath()     // Catch:{ all -> 0x00c3 }
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ all -> 0x00c3 }
            java.lang.String r5 = " . Error:  "
            java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ all -> 0x00c3 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x00c3 }
            a(r4, r3, r1)     // Catch:{ all -> 0x00c3 }
            if (r2 == 0) goto L_0x0096
            r2.flush()     // Catch:{ Exception -> 0x009e }
            r2.close()     // Catch:{ Exception -> 0x009e }
        L_0x0096:
            if (r8 == 0) goto L_0x0037
            if (r7 == 0) goto L_0x0037
            r7.recycle()
            goto L_0x0037
        L_0x009e:
            r1 = move-exception
            java.lang.String r2 = com.avidia.e.ag.a
            java.lang.String r3 = ""
            android.util.Log.e(r2, r3, r1)
            goto L_0x0096
        L_0x00a7:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x00aa:
            if (r2 == 0) goto L_0x00b2
            r2.flush()     // Catch:{ Exception -> 0x00ba }
            r2.close()     // Catch:{ Exception -> 0x00ba }
        L_0x00b2:
            if (r8 == 0) goto L_0x00b9
            if (r7 == 0) goto L_0x00b9
            r7.recycle()
        L_0x00b9:
            throw r0
        L_0x00ba:
            r1 = move-exception
            java.lang.String r2 = com.avidia.e.ag.a
            java.lang.String r3 = ""
            android.util.Log.e(r2, r3, r1)
            goto L_0x00b2
        L_0x00c3:
            r0 = move-exception
            goto L_0x00aa
        L_0x00c5:
            r1 = move-exception
            goto L_0x006c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avidia.e.ag.b(android.graphics.Bitmap, boolean):java.lang.String");
    }

    public static String b(v vVar) {
        String string = (vVar == null || vVar.c == null || TextUtils.isEmpty(vVar.c.b)) ? FisApplication.k().getString(C0000R.string.server_error) : vVar.c.b;
        if (!(vVar == null || vVar.c == null || vVar.c.a == null)) {
            a(a, string, vVar.c.a);
        }
        return string;
    }

    public static String b(String str) {
        return "wrap_name=" + FisApplication.k().m() + "&wrap_password=" + URLEncoder.encode(str) + "&wrap_scope=" + "https://m.wealthcareadmin.com/ServiceProvider/services/";
    }

    public static String b(Calendar calendar) {
        return String.format("/Date(%d-0400)/", Long.valueOf(calendar.getTimeInMillis()));
    }

    public static Calendar b() {
        Calendar instance = Calendar.getInstance();
        instance.add(5, 2);
        return instance;
    }

    public static void b(String str, String str2) {
        if (a.e) {
            Log.e(str, str2);
        }
    }

    private static boolean b(Uri uri, h hVar) {
        String c2 = c(uri, hVar);
        if (TextUtils.isEmpty(c2)) {
            return false;
        }
        if (c2.equals("PATH_POISON")) {
            return true;
        }
        return new File(c2).isFile();
    }

    public static Bitmap c(String str) {
        if (a.e) {
            Log.d(a, "Decoding base64:" + str);
        }
        byte[] decode = Base64.decode(str, 0);
        return BitmapFactory.decodeByteArray(decode, 0, decode.length);
    }

    public static File c() {
        File a2 = a(FisApplication.k().getApplicationContext(), "RCD");
        File file = null;
        while (true) {
            if (file != null && !file.exists()) {
                return file;
            }
            file = new File(a2, UUID.randomUUID().toString() + "REC");
        }
    }

    private static String c(Uri uri, h hVar) {
        if (uri.toString().startsWith("file:///")) {
            try {
                File file = new File(new URI(uri.toString()));
                if (file.isFile()) {
                    return file.getAbsolutePath();
                }
                throw new FileNotFoundException("File not found " + file.getAbsolutePath());
            } catch (Exception e) {
                a(a, "", e);
                return null;
            }
        } else {
            Cursor query = hVar.getContentResolver().query(uri, new String[]{"_data", "_display_name"}, null, null, null);
            if (uri.toString().startsWith("content://com.android.gallery3d.provider")) {
                uri = Uri.parse(uri.toString().replace("com.android.gallery3d", "com.google.android.gallery3d"));
            }
            if (query == null) {
                return null;
            }
            query.moveToFirst();
            int columnIndex = query.getColumnIndex("_data");
            if (!uri.toString().startsWith("content://com.google.android.gallery3d")) {
                return query.getString(columnIndex);
            }
            if (query.getColumnIndex("_display_name") != -1) {
                return "PATH_POISON";
            }
            return null;
        }
    }

    public static String c(Calendar calendar) {
        return c.format(calendar.getTime());
    }

    public static void c(String str, String str2) {
        if (a.e) {
            if (str2 == null) {
                str2 = "";
            }
            Log.d(str, str2);
        }
    }

    public static File d(String str, String str2) {
        FileOutputStream fileOutputStream;
        byte[] decode = Base64.decode(str, 0);
        File file = new File(a(FisApplication.k().getApplicationContext(), "RCD"), str2);
        file.delete();
        file.createNewFile();
        try {
            fileOutputStream = new FileOutputStream(file);
            try {
                fileOutputStream.write(decode);
                a(fileOutputStream);
                return file;
            } catch (Throwable th) {
                th = th;
                a(fileOutputStream);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fileOutputStream = null;
            a(fileOutputStream);
            throw th;
        }
    }

    public static void d() {
        File a2 = a(FisApplication.k().getApplicationContext(), "RCD");
        if (a2 != null && a2.isDirectory()) {
            for (File file : a2.listFiles()) {
                if (file.isFile()) {
                    file.delete();
                }
            }
        }
    }

    public static boolean d(String str) {
        return str == null || TextUtils.isEmpty(str) || TextUtils.isEmpty(str.replaceAll("\\[", "").replaceAll("\\]", ""));
    }

    public static String e() {
        return Uri.fromFile(c()).toString();
    }

    public static String e(String str) {
        boolean z;
        String str2;
        String replaceAll = str.replaceAll(",", ".");
        try {
            if (replaceAll.contains(".")) {
                int indexOf = replaceAll.indexOf(".");
                if (indexOf >= replaceAll.length() - 2) {
                    int length = 2 - ((replaceAll.length() - indexOf) - 1);
                    for (int i = 0; i < length; i++) {
                        replaceAll = replaceAll + "0";
                    }
                }
                z = true;
            } else {
                z = false;
            }
            String str3 = "";
            if (z) {
                String[] split = replaceAll.split("\\.");
                str2 = split[0];
                str3 = split[1];
            } else {
                str2 = replaceAll;
            }
            String format = NumberFormat.getInstance(Locale.US).format(Long.parseLong(str2));
            String str4 = z ? format + "." + str3 : format + ".00";
            return !str4.startsWith("-") ? "$" + str4 : "($" + str4.substring(1) + ")";
        } catch (Exception e) {
            return replaceAll;
        }
    }

    private static String e(String str, String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append("<").append(str).append(">").append(str2).append("</").append(str).append(">");
        return sb.toString();
    }

    public static String f(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        if (str.length() == 8 && TextUtils.isDigitsOnly(str)) {
            StringBuilder sb = new StringBuilder();
            sb.append(str.substring(4, 6)).append("/");
            sb.append(str.substring(6, 8)).append("/");
            sb.append(str.substring(0, 4));
            return sb.toString();
        } else if (str.length() <= 6) {
            return "";
        } else {
            if (str.startsWith("/Date(-")) {
                str = str.replaceFirst("\\/Date\\(-", "/Date(");
            }
            String substring = str.substring(6);
            String[] split = substring.split("-");
            if (split.length != 2) {
                return substring;
            }
            return b.format(new Date(Long.parseLong(split[0])));
        }
    }

    public static int[] g(String str) {
        if (str == null || str.length() != "MM/DD/YYYY".length()) {
            if (a.e) {
                Log.d(a, "parseYYYYMMDDToIntArray: Invalid date format");
            }
            return null;
        }
        String[] split = str.split("/");
        return new int[]{Integer.valueOf(split[2]).intValue(), Integer.valueOf(split[0]).intValue() - 1, Integer.valueOf(split[1]).intValue()};
    }

    public static Calendar h(String str) {
        int[] g = g(str);
        if (g.length != 3) {
            return null;
        }
        Calendar instance = Calendar.getInstance();
        instance.set(g[0], g[1], g[2]);
        instance.set(10, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        return instance;
    }

    public static String i(String str) {
        if (str.length() != 10 || !TextUtils.isDigitsOnly(str.replaceAll("/", ""))) {
            if (a.e) {
                Log.d(a, "Invalid date format");
            }
            return "";
        }
        String[] split = str.split("/");
        if (split.length == 3) {
            return split[2] + split[0] + split[1];
        }
        if (a.e) {
            Log.d(a, "Invalid date format");
        }
        return "";
    }

    public static String j(String str) {
        return str + " ";
    }

    public static boolean k(String str) {
        return Pattern.compile("\\<.+\\>").matcher(str).find();
    }

    public static Bitmap l(String str) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        return BitmapFactory.decodeFile(str, options);
    }

    public static String m(String str) {
        Bitmap decodeFile = BitmapFactory.decodeFile(str);
        if (decodeFile == null) {
            return null;
        }
        return b(decodeFile);
    }
}
