package com.avidia.c;

public class e extends RuntimeException {
    private final d a;

    public e(d dVar) {
        super("Unauthorized");
        this.a = dVar;
    }

    public String a() {
        return this.a.a();
    }

    public String b() {
        return this.a.c();
    }
}
