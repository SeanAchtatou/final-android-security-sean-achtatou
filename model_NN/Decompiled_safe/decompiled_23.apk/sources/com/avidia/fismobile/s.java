package com.avidia.fismobile;

import android.util.Log;
import android.widget.Toast;
import com.avidia.a.a;
import com.avidia.b.v;

class s implements Runnable {
    final /* synthetic */ v a;
    final /* synthetic */ ContributionPreview b;

    s(ContributionPreview contributionPreview, v vVar) {
        this.b = contributionPreview;
        this.a = vVar;
    }

    public void run() {
        this.b.m();
        if (this.a.a) {
            String unused = this.b.o = "TYPE_SUCCESS";
            String unused2 = this.b.p = this.a.b;
            this.b.a("TYPE_SUCCESS", this.a.b);
            return;
        }
        if (a.e) {
            Log.d(ContributionPreview.s, "Failed: " + this.a.c);
        }
        Toast.makeText(this.b, (int) C0000R.string.internal_error, 1).show();
    }
}
