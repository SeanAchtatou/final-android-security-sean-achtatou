package com.avidia.fismobile;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.avidia.b.a;
import com.avidia.b.j;
import com.avidia.b.n;
import com.avidia.e.ag;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class EditContributionActivity extends h implements ai {
    public static final String o = EditContributionActivity.class.getSimpleName();
    private TextView p;
    /* access modifiers changed from: private */
    public w q;
    private EditText r;
    private RadioGroup s;
    private a t;
    private Calendar u;
    private af v;
    private j w;
    private List x;

    private void a(int i, int i2, int i3) {
        this.v = new af(this, this, i, i2, i3);
        this.v.show();
    }

    /* access modifiers changed from: private */
    public void c(boolean z) {
        if (r()) {
            Intent intent = new Intent(this, ContributionPreview.class);
            intent.putExtra("CONTRIBUTION", this.w.b());
            intent.putExtra("account_info", this.t.b());
            intent.putExtra("PREVIEW_ACTIVITY_TYPE", z ? "TYPE_PREVIEW_EDIT" : "TYPE_PREVIEW_ADD");
            j();
            startActivity(intent);
        }
    }

    private List d(String str) {
        ArrayList arrayList = new ArrayList();
        try {
            JSONArray jSONArray = new JSONArray(str);
            for (int i = 0; i < jSONArray.length(); i++) {
                n a = n.a(jSONArray.getJSONObject(i));
                if (a.d() == 1) {
                    arrayList.add(a);
                }
            }
        } catch (JSONException e) {
            if (com.avidia.a.a.e) {
                Log.d(o, "Unable to parse ExternalBankAccounts");
            }
        }
        return arrayList;
    }

    private ArrayAdapter q() {
        ArrayList arrayList = new ArrayList();
        for (n e : this.x) {
            arrayList.add(e.e());
        }
        return new ArrayAdapter(this, (int) C0000R.layout.spinner_item, arrayList);
    }

    private boolean r() {
        if (this.w == null) {
            return false;
        }
        try {
            this.w.b(Double.valueOf(this.r.getText().toString()).doubleValue());
            String i = ag.i(this.p.getText().toString());
            this.w.c(i);
            this.w.d(i);
            n nVar = (n) this.x.get(this.q.d());
            this.w.b(nVar.j());
            this.w.g(nVar.k());
            this.w.a(nVar);
            if (this.s.getCheckedRadioButtonId() == C0000R.id.contribution_year_previos) {
                this.w.d(false);
            } else {
                this.w.d(true);
            }
            return true;
        } catch (Exception e) {
            Toast.makeText(this, (int) C0000R.string.enter_valid_contribution_amount, 1).show();
            return false;
        }
    }

    /* access modifiers changed from: private */
    public void s() {
        a(this.u.get(1), this.u.get(2), this.u.get(5));
    }

    public void a(Calendar calendar) {
        Toast.makeText(this, getString(C0000R.string.date_picker_err_message, new Object[]{ag.a(calendar)}), 1).show();
    }

    public void b(Calendar calendar) {
        this.p.setText(ag.a(calendar));
    }

    public boolean c(Calendar calendar) {
        return calendar.getTimeInMillis() >= this.u.getTimeInMillis();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.edit_contribution);
        Bundle extras = getIntent().getExtras();
        this.t = new a();
        String string = extras.getString("account_info");
        this.u = ag.b();
        boolean z = extras.getBoolean("EDIT_CONTRIBUTION");
        try {
            this.t.a(new JSONObject(string));
            this.x = d(this.t.d);
            if (z) {
                this.w = j.a(extras.getString("CONTRIBUTION"));
            } else {
                this.w = new j(ag.b(), this.t.i);
            }
            this.s = (RadioGroup) findViewById(C0000R.id.radioGroup1);
            if (this.w.g()) {
                this.s.check(C0000R.id.contribution_year_current);
            } else {
                this.s.check(C0000R.id.contribution_year_previos);
            }
            this.r = (EditText) findViewById(C0000R.id.edt_contribution_amount);
            TextView textView = (TextView) findViewById(C0000R.id.header_caption);
            if (z) {
                textView.setText((int) C0000R.string.edit_contribution);
                ((TextView) findViewById(C0000R.id.add_contribution_text)).setVisibility(8);
                int d = (int) this.w.d();
                if (((double) d) == this.w.d()) {
                    this.r.setText(String.valueOf(d));
                } else {
                    this.r.setText(String.valueOf(this.w.d()));
                }
            } else {
                textView.setText((int) C0000R.string.fund_my_hsa);
            }
            ((TextView) findViewById(C0000R.id.contribution_account_type)).setText(this.t.h);
            x xVar = new x(this);
            this.p = (TextView) findViewById(C0000R.id.contribution_date);
            this.p.setOnClickListener(xVar);
            this.p.setText(ag.f(this.w.e()));
            Button button = (Button) findViewById(C0000R.id.edit_contribution_accounts);
            this.q = new w(this, button);
            button.setOnClickListener(new y(this));
            findViewById(C0000R.id.edit_contribution_accounts_layout).setOnClickListener(new z(this, button));
            TextView textView2 = (TextView) findViewById(C0000R.id.withdrawal_bank_account_labl);
            if (!this.x.isEmpty()) {
                button.setVisibility(0);
                textView2.setVisibility(0);
                ArrayAdapter q2 = q();
                q2.setDropDownViewResource(C0000R.layout.spinner_item);
                this.q.a(q2);
                n h = this.w.h();
                if (h != null) {
                    int position = q2.getPosition(h.e());
                    if (position >= 0) {
                        this.q.a(position);
                    }
                } else if (q2.getCount() > 0) {
                    this.q.a(0);
                }
            } else {
                button.setVisibility(8);
                textView2.setVisibility(8);
            }
            findViewById(C0000R.id.btn_preview).setOnClickListener(new aa(this, z));
        } catch (Exception e) {
            if (com.avidia.a.a.e) {
                Log.d(o, "Unable to parse contribution" + e.getMessage());
            }
            Toast.makeText(this, (int) C0000R.string.internal_error, 1).show();
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        this.s.check(bundle.getInt("CHECKED_RB"));
        this.r.setText(bundle.getString("AMOUNT_TEXT"));
        this.p.setText(bundle.getString("CDATE"));
        this.q.a(bundle.getInt("SELECTED_SPINNER"));
        if (bundle.getBoolean("SPINNER_STATE")) {
            this.q.c();
        }
        if (bundle.containsKey("PICKER_Y")) {
            if (this.v != null && this.v.isShowing()) {
                this.v.dismiss();
            }
            a(bundle.getInt("PICKER_Y"), bundle.getInt("PICKER_M"), bundle.getInt("PICKER_D"));
        }
        super.onRestoreInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putInt("CHECKED_RB", this.s.getCheckedRadioButtonId());
        bundle.putString("AMOUNT_TEXT", this.r.getText().toString());
        bundle.putString("CDATE", this.p.getText().toString());
        bundle.putInt("SELECTED_SPINNER", this.q.d());
        bundle.putBoolean("SPINNER_STATE", this.q.a());
        this.q.b();
        if (this.v != null && this.v.isShowing()) {
            Calendar a = this.v.a();
            this.v.dismiss();
            bundle.putInt("PICKER_Y", a.get(1));
            bundle.putInt("PICKER_M", a.get(2));
            bundle.putInt("PICKER_D", a.get(5));
        }
        super.onSaveInstanceState(bundle);
    }
}
