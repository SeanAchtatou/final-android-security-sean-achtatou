package com.avidia.fismobile;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.avidia.a.a;
import com.avidia.b.q;
import com.avidia.c.e;
import com.avidia.e.ag;
import com.avidia.e.r;
import com.avidia.e.v;
import com.avidia.e.w;
import com.avidia.fismobile.view.bo;

public class SignInActivity extends l implements View.OnClickListener, View.OnTouchListener, w {
    public static final String o = SignInActivity.class.getSimpleName();
    /* access modifiers changed from: private */
    public View p;
    private TextView q;
    private final int r = 1;
    /* access modifiers changed from: private */
    public CheckBox s;
    /* access modifiers changed from: private */
    public String t;
    private View.OnClickListener u = new ao(this);

    private void a(View view, boolean z) {
        if (view != null) {
            view.setEnabled(z);
        }
    }

    private void a(q qVar) {
        ((ViewGroup) findViewById(C0000R.id.signin)).addView(getLayoutInflater().inflate((int) C0000R.layout.signin_prefilled_signin, (ViewGroup) null));
        ViewGroup viewGroup = (ViewGroup) findViewById(C0000R.id.prefilled_users_area);
        for (String str : qVar.a) {
            TextView textView = (TextView) getLayoutInflater().inflate((int) C0000R.layout.prefilled_user_item, (ViewGroup) null);
            textView.setText(str);
            textView.setOnClickListener(this.u);
            viewGroup.addView(textView);
            if (a.e) {
                Log.d(o, "Prefilled online id [" + str + "]added");
            }
        }
        findViewById(C0000R.id.signin_use_differenr_user_id).setOnClickListener(new aq(this));
    }

    /* access modifiers changed from: private */
    public void a(String str, boolean z) {
        if (z) {
            this.t = str;
        } else {
            this.t = "";
        }
        j();
        v();
        String trim = str.trim();
        if (!FisApplication.k().c(trim)) {
            FisApplication.k().j(trim);
        }
        if (!TextUtils.isEmpty(trim)) {
            if (a.e) {
                Log.d(o, "Cookies available:" + FisApplication.k().o());
            }
            r.c();
            if (!FisApplication.k().o()) {
                r.b();
            }
            f().B();
            f().a(new v(trim, Settings.Secure.getString(getContentResolver(), "android_id")), this);
            l();
            return;
        }
        j();
        Toast.makeText(this, (int) C0000R.string.enter_non_empty_id, 1).show();
    }

    /* access modifiers changed from: private */
    public boolean b(com.avidia.b.v vVar) {
        if (vVar.c.a instanceof e) {
            String b = ((e) vVar.c.a).b();
            if ("1000".equals(b) || "120490".equals(b)) {
                return true;
            }
        }
        return false;
    }

    private void c(boolean z) {
        a(findViewById(C0000R.id.btn_signin), z);
        a(findViewById(C0000R.id.et_onlineid), z);
        a(this.s, z);
    }

    /* access modifiers changed from: private */
    public boolean d(String str) {
        return str.contains("Phrase") && str.contains("SiteImage");
    }

    private boolean q() {
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        defaultDisplay.getMetrics(displayMetrics);
        int i = displayMetrics.widthPixels;
        int i2 = displayMetrics.heightPixels;
        if (Math.max(i, i2) < 1020 && Math.min(i, i2) < 580) {
            return false;
        }
        int i3 = i / displayMetrics.densityDpi;
        int i4 = i2 / displayMetrics.densityDpi;
        return Math.sqrt((double) ((i3 * i3) + (i4 * i4))) >= 5.5d;
    }

    /* access modifiers changed from: private */
    public void r() {
        startActivity(new Intent(this, VersionNotSupportedActivity.class));
    }

    /* access modifiers changed from: private */
    public void s() {
        String string = getString(C0000R.string.signin_error_id_doesnot_exist);
        if (a.e) {
            Log.d(o, string);
        }
        this.q.setText(Html.fromHtml(string));
        this.q.setVisibility(0);
        this.q.setEnabled(true);
    }

    private EditText t() {
        return (EditText) findViewById(C0000R.id.et_onlineid);
    }

    /* access modifiers changed from: private */
    public void u() {
        ViewGroup viewGroup = (ViewGroup) findViewById(C0000R.id.signin);
        View findViewById = findViewById(C0000R.id.signin_prefilled);
        if (findViewById != null) {
            viewGroup.removeView(findViewById);
        }
        viewGroup.addView(getLayoutInflater().inflate((int) C0000R.layout.signin_new_user, (ViewGroup) null));
        EditText editText = (EditText) viewGroup.findViewById(C0000R.id.et_onlineid);
        a(editText, this.p.getVisibility() != 0);
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(editText.getWindowToken(), 0);
        getWindow().setSoftInputMode(3);
        editText.setOnEditorActionListener(new ar(this, editText));
        findViewById(C0000R.id.btn_signin).setOnClickListener(this);
        this.s = (CheckBox) findViewById(C0000R.id.cb_save_online_id);
    }

    private void v() {
        this.q.setVisibility(8);
        this.q.setEnabled(false);
    }

    public void a(com.avidia.b.v vVar) {
        runOnUiThread(new ap(this, vVar));
    }

    public void c(String str) {
        j();
        this.q.setText(Html.fromHtml(str));
        this.q.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 1 && i2 == -1) {
            String stringExtra = intent.getStringExtra("answer");
            if (stringExtra == null) {
                stringExtra = "No answer";
            }
            Toast.makeText(this, stringExtra, 1).show();
        }
    }

    public void onClick(View view) {
        if (this.p != null && this.p.getVisibility() != 0) {
            a(t().getText().toString(), this.s.isChecked());
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        boolean z = true;
        FisApplication.b(!q());
        super.onCreate(bundle);
        setContentView(bo.d());
        this.p = findViewById(C0000R.id.pb_progress_bar);
        View findViewById = findViewById(C0000R.id.signin_view);
        if (findViewById != null) {
            findViewById.setOnTouchListener(this);
        }
        h();
        q k = k();
        if (k.a.size() == 0) {
            u();
        } else {
            a(k);
        }
        this.q = (TextView) findViewById(C0000R.id.signin_error);
        this.q.setMovementMethod(LinkMovementMethod.getInstance());
        getWindow().setSoftInputMode(3);
        if (this.p.getVisibility() == 0) {
            z = false;
        }
        c(z);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        m();
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (SignInQuestionActivity.q()) {
            SignInQuestionActivity.r();
            ag.b(o, "Wrong attempts limit for security questions is exhausted");
            finish();
        }
        k();
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() != 0) {
            return false;
        }
        i();
        return false;
    }
}
