package com.avidia.fismobile;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import com.avidia.a.a;
import com.avidia.e.ag;

class ak extends WebViewClient {
    final /* synthetic */ HtmlViewActivity a;

    private ak(HtmlViewActivity htmlViewActivity) {
        this.a = htmlViewActivity;
    }

    private void a(String str) {
        try {
            String str2 = str.split(":")[1];
            if (a.e) {
                Log.d(HtmlViewActivity.o, "Sending email to:" + str2);
            }
            Intent intent = new Intent("android.intent.action.SEND");
            intent.setType("text/plain");
            intent.putExtra("android.intent.extra.EMAIL", new String[]{str2});
            intent.putExtra("android.intent.extra.TEXT", new String[]{" "});
            this.a.startActivityForResult(intent, 811);
        } catch (Exception e) {
            Log.e(HtmlViewActivity.o, "Error when sending email to url:" + str, e);
            this.a.b(true);
        }
    }

    private void b(String str) {
        try {
            ag.c(HtmlViewActivity.o, "Try to call to phone number:" + str);
            this.a.startActivityForResult(new Intent("android.intent.action.DIAL", Uri.parse(str)), 811);
        } catch (Exception e) {
            this.a.b(true);
            ag.a(HtmlViewActivity.o, "Error when calling to tel number:" + str, e);
            Toast.makeText(this.a, (int) C0000R.string.unable_to_call, 1).show();
        }
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (str.contains("mailto:")) {
            a(str);
            return true;
        } else if (!str.contains("tel:")) {
            return super.shouldOverrideUrlLoading(webView, str);
        } else {
            b(str);
            return true;
        }
    }
}
