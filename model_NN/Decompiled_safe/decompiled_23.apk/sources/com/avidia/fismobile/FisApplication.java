package com.avidia.fismobile;

import android.app.Application;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.a.a.j;
import com.avidia.b.c;
import com.avidia.b.g;
import com.avidia.b.q;
import com.avidia.e.ag;
import com.avidia.e.r;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.http.NameValuePair;
import org.json.JSONException;

public class FisApplication extends Application {
    private static FisApplication a;
    private static final String b = FisApplication.class.getSimpleName();
    private static boolean c;
    private j d;
    private String e;
    private String f;
    private String g;
    private final Map h = new ConcurrentHashMap();
    private String i;
    private float j = 0.5f;
    private String k = null;
    private g l;
    private String m;
    private boolean n = false;
    private long o = 0;
    private boolean p = false;

    private void a(List list, boolean z) {
        if (list != null) {
            if (z) {
                this.h.clear();
            }
            Iterator it = list.iterator();
            while (it.hasNext()) {
                NameValuePair nameValuePair = (NameValuePair) it.next();
                this.h.put(nameValuePair.getName(), nameValuePair.getValue());
            }
        }
    }

    public static void b(boolean z) {
        c = z;
    }

    public static boolean e() {
        return c;
    }

    public static FisApplication k() {
        return a;
    }

    private void k(String str) {
        getSharedPreferences("alerts_marks", 0).edit().putString(s(), str).commit();
    }

    private void w() {
        r.a(getSharedPreferences("auth_cookies", 0));
    }

    private void x() {
        this.p = getSharedPreferences("auth_cookies", 0).getBoolean("cookies_available", false);
    }

    public void a() {
        this.o = Calendar.getInstance().getTimeInMillis();
    }

    public void a(float f2) {
        this.j = f2;
    }

    public void a(c cVar) {
        k(this.d.a(cVar));
    }

    public void a(String str) {
        this.k = str;
    }

    public void a(List list) {
        a(list, false);
    }

    public void a(boolean z) {
        this.n = z;
    }

    public void b(String str) {
        if (str != null && !TextUtils.isEmpty(str.trim())) {
            SharedPreferences sharedPreferences = getSharedPreferences("user_preferences", 0);
            String string = sharedPreferences.getString("user_ud", "");
            q qVar = TextUtils.isEmpty(string) ? new q() : (q) this.d.a(string, q.class);
            qVar.a.add(str);
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putString("user_ud", this.d.a(qVar));
            edit.commit();
        }
    }

    public void b(List list) {
        a(list, true);
    }

    public boolean b() {
        return this.o != 0 && Math.abs(this.o - Calendar.getInstance().getTimeInMillis()) >= 900000;
    }

    public void c() {
        this.o = 0;
    }

    public void c(boolean z) {
        this.p = z;
        p();
    }

    public boolean c(String str) {
        if (str == null || TextUtils.isEmpty(str)) {
            return false;
        }
        String string = getSharedPreferences("user_preferences", 0).getString("user_ud", "");
        return (TextUtils.isEmpty(string) ? new q() : (q) this.d.a(string, q.class)).a.contains(str);
    }

    public void d(String str) {
        this.m = str;
    }

    public boolean d() {
        return this.n;
    }

    public void e(String str) {
        this.e = str;
    }

    public void f() {
        this.k = null;
        this.l = null;
    }

    public void f(String str) {
        if (this.f != null && !this.f.equals(str)) {
            this.g = "";
        }
        this.f = str;
        this.h.clear();
    }

    public float g() {
        return this.j;
    }

    public void g(String str) {
        this.i = str;
    }

    public g h() {
        g gVar;
        if (this.l != null) {
            return this.l;
        }
        if (TextUtils.isEmpty(this.k)) {
            return null;
        }
        try {
            gVar = new g();
            try {
                gVar.a(this.k);
            } catch (JSONException e2) {
                ag.b(b, "unable to parse template");
                this.l = gVar;
                return this.l;
            }
        } catch (JSONException e3) {
            gVar = null;
            ag.b(b, "unable to parse template");
            this.l = gVar;
            return this.l;
        }
        this.l = gVar;
        return this.l;
    }

    public void h(String str) {
        this.g = str;
    }

    public String i() {
        return this.m;
    }

    public String i(String str) {
        return (String) this.h.get(str);
    }

    public int j() {
        try {
            return Integer.valueOf(i("ParticipantAccessRights")).intValue();
        } catch (Exception e2) {
            return 0;
        }
    }

    public void j(String str) {
        if (!TextUtils.isEmpty(str)) {
            SharedPreferences sharedPreferences = getSharedPreferences("user_preferences", 0);
            String string = sharedPreferences.getString("user_ud", "");
            q qVar = TextUtils.isEmpty(string) ? new q() : (q) this.d.a(string, q.class);
            if (qVar.a.remove(str)) {
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putString("user_ud", this.d.a(qVar));
                edit.commit();
            }
            r.b(str);
            if (qVar.a.isEmpty()) {
                c(false);
            }
        }
    }

    public String l() {
        return this.e;
    }

    public String m() {
        return this.f;
    }

    public void n() {
        SharedPreferences.Editor edit = getSharedPreferences("auth_cookies", 0).edit();
        r.a(edit);
        edit.commit();
    }

    public boolean o() {
        return this.p;
    }

    public void onCreate() {
        super.onCreate();
        a = this;
        this.d = new j();
        x();
        if (o()) {
            w();
        }
    }

    public void onTerminate() {
        if (o()) {
            n();
        }
        ag.d();
        super.onTerminate();
    }

    public void p() {
        SharedPreferences.Editor edit = getSharedPreferences("auth_cookies", 0).edit();
        edit.putBoolean("cookies_available", this.p);
        edit.commit();
    }

    public String q() {
        return this.i;
    }

    public Map r() {
        return this.h;
    }

    public String s() {
        return this.f + "_alerts_marks";
    }

    public c t() {
        if (TextUtils.isEmpty(this.f)) {
            return null;
        }
        String string = getSharedPreferences("alerts_marks", 0).getString(s(), "");
        if (!TextUtils.isEmpty(string)) {
            return (c) this.d.a(string, c.class);
        }
        return null;
    }

    public String u() {
        return this.g;
    }

    public void v() {
        j(m());
    }
}
