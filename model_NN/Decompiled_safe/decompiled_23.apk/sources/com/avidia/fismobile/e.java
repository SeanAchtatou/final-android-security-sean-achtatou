package com.avidia.fismobile;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.avidia.b.b;
import com.avidia.b.c;
import com.avidia.fismobile.view.MyAlertsFragmentActivity;
import com.avidia.fismobile.view.cd;
import java.util.ArrayList;
import java.util.Iterator;

public class e extends BaseAdapter {
    private static LayoutInflater f = null;
    private MyAlertsFragmentActivity a;
    private final int b;
    private int c = -1;
    private final ArrayList d;
    private final ArrayList e;
    private c g;
    /* access modifiers changed from: private */
    public final cd h;

    public e(cd cdVar, MyAlertsFragmentActivity myAlertsFragmentActivity) {
        this.h = cdVar;
        this.a = myAlertsFragmentActivity;
        this.d = new ArrayList();
        this.e = new ArrayList();
        f = (LayoutInflater) this.a.getSystemService("layout_inflater");
        if (FisApplication.e()) {
            this.b = C0000R.layout.alert_item;
        } else {
            this.b = C0000R.layout.alert_item_tablet;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avidia.fismobile.e.a(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      com.avidia.fismobile.e.a(boolean, boolean):void
      com.avidia.fismobile.e.a(android.view.View, boolean):void */
    private void a(View view) {
        if (!FisApplication.e()) {
            view.setBackgroundResource(C0000R.color.listitem_background_selected_tablet);
            a(view, true);
        }
    }

    private void a(View view, boolean z) {
        View findViewById = view.findViewById(C0000R.id.selection_marker);
        if (findViewById != null) {
            if (z) {
                findViewById.setVisibility(0);
            } else {
                findViewById.setVisibility(4);
            }
        }
    }

    private boolean b(int i) {
        if (this.g != null) {
            return !this.g.a(((b) this.d.get(i)).d);
        }
        return true;
    }

    public c a() {
        return this.g;
    }

    public void a(int i) {
        this.c = i;
        notifyDataSetChanged();
    }

    public void a(b bVar) {
        this.d.add(bVar);
        this.e.add(bVar);
    }

    public void a(c cVar) {
        this.g = cVar;
    }

    public void a(boolean z, boolean z2) {
        this.d.clear();
        Iterator it = this.e.iterator();
        while (it.hasNext()) {
            b bVar = (b) it.next();
            if ((z && bVar.e == 1) || (z2 && bVar.e == 0)) {
                this.d.add(bVar);
            }
        }
        notifyDataSetChanged();
    }

    public void b() {
        this.d.clear();
        this.e.clear();
    }

    public ArrayList c() {
        return this.d;
    }

    public int getCount() {
        return this.d.size();
    }

    public Object getItem(int i) {
        return this.d.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = f.inflate(this.b, (ViewGroup) null);
        TextView textView = (TextView) inflate.findViewById(C0000R.id.alert_date);
        TextView textView2 = (TextView) inflate.findViewById(C0000R.id.alert_subject);
        if (b(i)) {
            textView.setTextAppearance(this.a, 2131296266);
            textView2.setTextAppearance(this.a, 2131296266);
        } else {
            textView.setTextAppearance(this.a, 2131296267);
            textView2.setTextAppearance(this.a, 2131296267);
        }
        b bVar = (b) this.d.get(i);
        textView.setText(bVar.b);
        textView2.setText(bVar.c);
        ImageView imageView = (ImageView) inflate.findViewById(C0000R.id.alert_type);
        ImageView imageView2 = (ImageView) inflate.findViewById(C0000R.id.alert_type1);
        if (bVar.e == 0) {
            imageView.setImageResource(C0000R.drawable.mail_icon);
            imageView2.setVisibility(4);
        } else if (bVar.e == 1) {
            imageView.setImageResource(C0000R.drawable.phone_icon);
            imageView2.setVisibility(4);
        }
        inflate.setOnClickListener(new f(this, i));
        if (i == this.c) {
            a(inflate);
        }
        return inflate;
    }
}
