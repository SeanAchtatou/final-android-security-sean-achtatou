package com.avidia.fismobile;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

public class SignInErrorActivity extends Activity implements View.OnClickListener {
    public void onClick(View view) {
        TextView textView = (TextView) findViewById(C0000R.id.signin_error_text);
        textView.setVisibility(0);
        textView.setText(Html.fromHtml(getString(C0000R.string.signin_error_password_change_required)));
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.signin_error);
        findViewById(C0000R.id.signin_error_text).setOnClickListener(this);
    }
}
