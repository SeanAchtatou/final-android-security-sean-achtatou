package com.avidia.fismobile;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.avidia.a.a;
import com.avidia.b.j;
import com.avidia.b.v;
import com.avidia.e.ag;
import com.avidia.e.l;
import com.avidia.e.w;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ContributionsActivity extends h implements w {
    private static final String t = ContributionsActivity.class.getSimpleName();
    private View o;
    private View p;
    private o q;
    private String r;
    private String s;

    private void b(int i) {
        an f = f();
        if (f != null) {
            f.B();
            l();
            f.a(new l(i), this);
        } else if (a.e) {
            Log.e(t, "Unable to load contributions");
        }
    }

    private void c(boolean z) {
        if (z) {
            this.p.setVisibility(8);
            this.o.setVisibility(0);
            return;
        }
        this.o.setVisibility(8);
        this.p.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void d(String str) {
        if (this.q == null || str == null || ag.d(str)) {
            c(false);
            return;
        }
        if (a.e) {
            Log.d(t, "Contributions data:" + str);
        }
        c(true);
        try {
            JSONArray jSONArray = new JSONArray(str);
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < jSONArray.length(); i++) {
                arrayList.add(j.a(jSONArray.getJSONObject(i)));
            }
            this.r = str;
            this.q.a(arrayList);
        } catch (Exception e) {
            if (a.e) {
                Log.d(t, "Contributions JSON parsing error: " + e.getMessage());
            }
            c(false);
        }
    }

    /* access modifiers changed from: private */
    public void q() {
        a((j) null);
    }

    public void a(j jVar) {
        boolean z = jVar != null;
        Intent intent = new Intent(this, EditContributionActivity.class);
        intent.putExtra("EDIT_CONTRIBUTION", z);
        intent.putExtra("account_info", this.s);
        if (z) {
            try {
                intent.putExtra("CONTRIBUTION", jVar.b());
            } catch (Exception e) {
                if (a.e) {
                    Log.d(t, "Unable to parse contribution into json");
                }
                Toast.makeText(this, (int) C0000R.string.internal_error, 1).show();
                return;
            }
        }
        j();
        startActivity(intent);
    }

    public void a(v vVar) {
        runOnUiThread(new u(this, vVar));
    }

    public void b(j jVar) {
        try {
            Intent intent = new Intent(this, ContributionPreview.class);
            intent.putExtra("CONTRIBUTION", jVar.b());
            intent.putExtra("PREVIEW_ACTIVITY_TYPE", "TYPE_DETAILS");
            j();
            startActivity(intent);
        } catch (Exception e) {
            if (a.e) {
                Log.d(t, e.getMessage());
            }
            Toast.makeText(this, (int) C0000R.string.internal_error, 1).show();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.contributions);
        this.o = findViewById(C0000R.id.listview_container);
        this.p = findViewById(C0000R.id.add_contribution_hint);
        ((TextView) findViewById(C0000R.id.header_caption)).setText((int) C0000R.string.contributions);
        findViewById(C0000R.id.btn_header_left).setVisibility(4);
        Button button = (Button) findViewById(C0000R.id.btn_header_right);
        button.setText((int) C0000R.string.add);
        a((int) C0000R.id.btn_header_right_layout, button);
        this.q = new o(this);
        ((ListView) findViewById(C0000R.id.contributions_listview)).setAdapter((ListAdapter) this.q);
        this.s = getIntent().getExtras().getString("account_info");
        com.avidia.b.a aVar = new com.avidia.b.a();
        try {
            aVar.a(new JSONObject(this.s));
            if (bundle == null) {
                b(aVar.i);
            }
            v vVar = new v(this, null);
            this.p.setOnClickListener(vVar);
            button.setOnClickListener(vVar);
        } catch (JSONException e) {
            if (a.e) {
                Log.d(t, "Unable to parse account info");
            }
            Toast.makeText(this, (int) C0000R.string.internal_error, 1).show();
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        d(bundle.getString("JSON_KEY"));
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putString("JSON_KEY", this.r);
    }
}
