package com.avidia.fismobile;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import com.avidia.a.a;

class t implements View.OnClickListener {
    final /* synthetic */ ContributionPreview a;

    private t(ContributionPreview contributionPreview) {
        this.a = contributionPreview;
    }

    /* synthetic */ t(ContributionPreview contributionPreview, r rVar) {
        this(contributionPreview);
    }

    public void onClick(View view) {
        if (this.a.q == null) {
            if (a.e) {
                Log.d(ContributionPreview.s, "accountInfo == null");
            }
            Toast.makeText(this.a, (int) C0000R.string.internal_error, 1).show();
            return;
        }
        Intent intent = new Intent(this.a, ContributionsActivity.class);
        intent.putExtra("account_info", this.a.q.b());
        this.a.startActivity(intent);
        this.a.finish();
    }
}
