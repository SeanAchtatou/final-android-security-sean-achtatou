package com.avidia.fismobile;

import android.view.View;
import java.util.Calendar;

class ah implements View.OnClickListener {
    final /* synthetic */ af a;

    ah(af afVar) {
        this.a = afVar;
    }

    public void onClick(View view) {
        Calendar instance = Calendar.getInstance();
        instance.set(this.a.a.getYear(), this.a.a.getMonth(), this.a.a.getDayOfMonth());
        if (this.a.b.c(instance)) {
            this.a.b.b(instance);
            this.a.cancel();
            return;
        }
        this.a.b.a(instance);
    }
}
