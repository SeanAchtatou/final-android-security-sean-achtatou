package com.avidia.fismobile;

import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import com.avidia.a.a;
import com.avidia.b.v;
import com.avidia.c.f;
import com.avidia.e.ag;

class ap implements Runnable {
    final /* synthetic */ v a;
    final /* synthetic */ SignInActivity b;

    ap(SignInActivity signInActivity, v vVar) {
        this.b = signInActivity;
        this.a = vVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void run() {
        this.b.m();
        StringBuilder sb = new StringBuilder("Result:");
        sb.append(this.a.a);
        if (!(this.a.c == null || this.a.c.a == null)) {
            sb.append(", errorInfo:").append(this.a.c.a);
        }
        if (a.e) {
            Log.i(SignInActivity.o, "Result of login :" + sb.toString());
        }
        if (this.a.a) {
            if (!TextUtils.isEmpty(this.a.b)) {
                sb.append(", message:").append(this.a.b);
            }
            if (this.b.d(this.a.b)) {
                Intent intent = new Intent(this.b, SignInKeyActivity.class);
                intent.putExtra("jsonUsed", true);
                intent.putExtra("siteKey", this.a.b);
                this.b.startActivity(intent);
                return;
            }
            Intent intent2 = new Intent(this.b, SignInQuestionActivity.class);
            intent2.putExtra("questions", this.a.b);
            intent2.putExtra("online_id", this.b.t);
            this.b.startActivityForResult(intent2, 1);
            return;
        }
        String str = "";
        if (this.a.c == null) {
            str = this.b.getString(C0000R.string.internal_error);
        } else if (this.b.b(this.a)) {
            this.b.s();
        } else if (this.a.c.a == null) {
            str = this.b.getString(C0000R.string.server_error);
        } else if (this.a.c.a instanceof f) {
            this.b.r();
            this.b.finish();
            return;
        } else {
            str = ag.a(this.a.c.a);
        }
        if (!TextUtils.isEmpty(str)) {
            this.b.c(str);
        }
    }
}
