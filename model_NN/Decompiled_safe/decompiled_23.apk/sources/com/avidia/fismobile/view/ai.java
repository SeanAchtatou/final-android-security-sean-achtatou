package com.avidia.fismobile.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.avidia.a.a;
import com.avidia.b.e;
import com.avidia.e.ag;
import com.avidia.fismobile.C0000R;
import com.avidia.fismobile.FisApplication;

public abstract class ai extends aj {
    protected String P;
    protected e Q;
    protected String R;
    protected MultiLineReceiptsView S;
    protected final String T = getClass().getSimpleName();
    private LayoutInflater V;

    private void L() {
        int j = FisApplication.k().j();
        this.S.a(getClass(), I(), this.V, ((ae) I()).getWindowManager().getDefaultDisplay(), j == 2 || j == 3 ? F() : null, this.Q);
    }

    /* access modifiers changed from: protected */
    public int C() {
        return C0000R.layout.window_caption;
    }

    /* access modifiers changed from: protected */
    public abstract int D();

    /* access modifiers changed from: protected */
    public int E() {
        return C0000R.layout.calim_preview_layout;
    }

    /* access modifiers changed from: protected */
    public View.OnClickListener F() {
        return null;
    }

    /* access modifiers changed from: protected */
    public boolean G() {
        return false;
    }

    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.V = layoutInflater;
        View inflate = layoutInflater.inflate(E(), (ViewGroup) null);
        a(inflate, C(), (int) C0000R.layout.window_caption_tablet);
        a(inflate, D());
        try {
            this.R = b().getString("CLAIM").trim();
            a(inflate, layoutInflater);
        } catch (Exception e) {
            ag.a(this.T, "Error while parsing claim", e);
            H();
        }
        return inflate;
    }

    /* access modifiers changed from: protected */
    public abstract void a(View view);

    /* access modifiers changed from: protected */
    public void a(View view, LayoutInflater layoutInflater) {
        this.S = (MultiLineReceiptsView) view.findViewById(C0000R.id.photo_container);
        a(view);
        try {
            this.Q = new e(this.R, G());
            b(view, layoutInflater);
        } catch (Exception e) {
            if (a.e) {
                Log.e(this.T, "Error while parsing the claim", e);
            }
            H();
        }
    }

    public void a_() {
        L();
    }

    /* access modifiers changed from: protected */
    public void b(View view, LayoutInflater layoutInflater) {
        LinearLayout linearLayout = (LinearLayout) view.findViewById(C0000R.id.list_view);
        linearLayout.removeAllViews();
        bg bgVar = new bg(I(), layoutInflater);
        bgVar.a(this.R);
        linearLayout.addView(bgVar);
        L();
        linearLayout.invalidate();
    }
}
