package com.avidia.fismobile.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Spanned;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.avidia.fismobile.C0000R;

public class bm extends Dialog {
    private String a;
    private String b;
    private String c;
    private CharSequence d;
    private View.OnClickListener e;
    private View.OnClickListener f;

    public bm(Context context) {
        super(context, 2131296302);
    }

    public void a(Spanned spanned) {
        this.d = spanned;
    }

    public void a(View.OnClickListener onClickListener) {
        this.e = onClickListener;
    }

    public void a(String str) {
        this.c = str;
    }

    public void a(String str, View.OnClickListener onClickListener) {
        a(onClickListener);
        this.a = str;
    }

    public void b(View.OnClickListener onClickListener) {
        this.f = onClickListener;
    }

    public void b(String str) {
        this.d = str;
    }

    public void b(String str, View.OnClickListener onClickListener) {
        b(onClickListener);
        this.b = str;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.buttoned_dialog_layout);
        if (this.c == null) {
            findViewById(C0000R.id.header_wrapper).setVisibility(8);
        } else {
            ((TextView) findViewById(C0000R.id.header)).setText(this.c);
        }
        ((TextView) findViewById(C0000R.id.message)).setText(this.d);
        Button button = (Button) findViewById(C0000R.id.ok);
        button.setOnClickListener(this.e);
        if (this.a != null) {
            button.setText(this.a);
        }
        if (this.f != null) {
            Button button2 = (Button) findViewById(C0000R.id.cancel);
            button2.setVisibility(0);
            button2.setOnClickListener(this.f);
            if (this.b != null) {
                button2.setText(this.b);
            }
        }
    }

    public void show() {
        if (this.e == null) {
            this.e = new bn(this);
        }
        super.show();
    }
}
