package com.avidia.fismobile.view;

import android.widget.TextView;
import com.avidia.e.ag;
import com.avidia.fismobile.ai;
import java.util.Calendar;

class ac implements ai {
    final /* synthetic */ o a;
    private boolean b;
    private int c;
    private TextView d;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avidia.fismobile.view.ac.<init>(com.avidia.fismobile.view.o, android.widget.TextView, int, boolean):void
     arg types: [com.avidia.fismobile.view.o, android.widget.TextView, int, int]
     candidates:
      com.avidia.fismobile.view.ac.<init>(com.avidia.fismobile.view.o, android.widget.TextView, int, com.avidia.fismobile.view.p):void
      com.avidia.fismobile.view.ac.<init>(com.avidia.fismobile.view.o, android.widget.TextView, int, boolean):void */
    private ac(o oVar, TextView textView, int i) {
        this(oVar, textView, i, false);
    }

    /* synthetic */ ac(o oVar, TextView textView, int i, p pVar) {
        this(oVar, textView, i);
    }

    public ac(o oVar, TextView textView, int i, boolean z) {
        this.a = oVar;
        this.b = z;
        this.d = textView;
        this.c = i;
    }

    public void a(Calendar calendar) {
        this.a.a(this.a.a(this.c, ag.a(calendar)));
    }

    public void b(Calendar calendar) {
        this.d.setText(ag.a(calendar));
    }

    public boolean c(Calendar calendar) {
        Calendar instance = Calendar.getInstance();
        instance.set(10, 0);
        instance.set(12, 0);
        int i = instance.get(1) + 20;
        int i2 = instance.get(1) - 15;
        if (calendar.get(1) > i && calendar.get(1) < i2) {
            return false;
        }
        if (this.b) {
            return true;
        }
        try {
            Calendar h = ag.h(this.a.V.getText().toString());
            return (h.get(1) == calendar.get(1) && h.get(2) == calendar.get(2) && h.get(5) == calendar.get(5)) || calendar.compareTo(h) == 1;
        } catch (Exception e) {
            return true;
        }
    }
}
