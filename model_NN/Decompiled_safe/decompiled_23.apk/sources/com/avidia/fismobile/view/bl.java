package com.avidia.fismobile.view;

import android.view.View;
import android.widget.Button;

class bl implements View.OnClickListener {
    final /* synthetic */ Button a;
    final /* synthetic */ bh b;

    bl(bh bhVar, Button button) {
        this.b = bhVar;
        this.a = button;
    }

    public void onClick(View view) {
        if (this.a.isEnabled() && this.a.getVisibility() == 0 && this.a.isClickable()) {
            this.a.performClick();
        }
    }
}
