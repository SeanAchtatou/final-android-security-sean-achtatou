package com.avidia.fismobile.view;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.avidia.e.ag;
import com.avidia.fismobile.C0000R;
import com.avidia.fismobile.FisApplication;
import com.avidia.fismobile.HtmlViewActivity;
import com.avidia.fismobile.h;
import java.util.HashMap;
import java.util.Map;

public abstract class al extends h {
    private Map o;
    protected final String p = getClass().getSimpleName();

    protected al() {
    }

    private ImageView a(Class cls) {
        ImageView imageView = new ImageView(this);
        imageView.setImageResource(c(cls));
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        layoutParams.setMargins(5, 5, 5, 5);
        imageView.setLayoutParams(layoutParams);
        imageView.setId(cls.getSimpleName().hashCode());
        imageView.setOnClickListener(new an(this, cls));
        return imageView;
    }

    private void a(View view, float f) {
        if (!FisApplication.e() && view != null) {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, -1);
            layoutParams.weight = f;
            view.setLayoutParams(layoutParams);
            view.invalidate();
        }
    }

    private void a(Class cls, ViewGroup viewGroup) {
        if (viewGroup != null) {
            ImageView imageView = null;
            int hashCode = cls.getSimpleName().hashCode();
            int i = 0;
            while (true) {
                if (i >= viewGroup.getChildCount()) {
                    break;
                } else if (hashCode == viewGroup.getChildAt(i).getId()) {
                    imageView = (ImageView) viewGroup.getChildAt(i);
                    break;
                } else {
                    i++;
                }
            }
            if (imageView != null) {
                int i2 = cls == AccountsFragmentActivity.class ? C0000R.drawable.tablet_menu_accounts_grey : cls == MoreActionsFragmentActivity.class ? C0000R.drawable.tablet_menu_more_grey : cls == ClaimsFragmentActivity.class ? C0000R.drawable.tablet_menu_claims_grey : cls == MyAlertsFragmentActivity.class ? C0000R.drawable.tablet_menu_alerts_grey : cls == HtmlViewActivity.class ? C0000R.drawable.tablet_menu_contact_us_grey : -1;
                if (i2 != -1) {
                    imageView.setImageResource(i2);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public int b(Class cls) {
        if (cls == AccountsFragmentActivity.class) {
            return C0000R.id.menu_accounts;
        }
        if (cls == MoreActionsFragmentActivity.class) {
            return C0000R.id.menu_more;
        }
        if (cls == ClaimsFragmentActivity.class) {
            return C0000R.id.menu_claim;
        }
        if (cls == MyAlertsFragmentActivity.class) {
            return C0000R.id.menu_alerts;
        }
        if (cls == HtmlViewActivity.class) {
            return C0000R.id.menu_contactus;
        }
        return 0;
    }

    private int c(Class cls) {
        if (cls == AccountsFragmentActivity.class) {
            return C0000R.drawable.tablet_menu_accounts_normal;
        }
        if (cls == MoreActionsFragmentActivity.class) {
            return C0000R.drawable.tablet_menu_more_normal;
        }
        if (cls == ClaimsFragmentActivity.class) {
            return C0000R.drawable.tablet_menu_claims_normal;
        }
        if (cls == MyAlertsFragmentActivity.class) {
            return C0000R.drawable.tablet_menu_alerts_normal;
        }
        if (cls == HtmlViewActivity.class) {
            return C0000R.drawable.tablet_menu_contact_us_normal;
        }
        return 0;
    }

    private int d(Class cls) {
        if (cls == AccountsFragmentActivity.class) {
            return C0000R.string.accounts;
        }
        if (cls == MoreActionsFragmentActivity.class) {
            return C0000R.string.more;
        }
        if (cls == ClaimsFragmentActivity.class) {
            return C0000R.string.accounts_menu_claim;
        }
        if (cls == MyAlertsFragmentActivity.class) {
            return C0000R.string.myalerts;
        }
        if (cls == HtmlViewActivity.class) {
            return C0000R.string.about_us;
        }
        return 0;
    }

    private void r() {
        if (!FisApplication.e()) {
            float f = getResources().getConfiguration().orientation == 2 ? 0.35f : 0.5f;
            FisApplication.k().a(f);
            a(findViewById(C0000R.id.frame_left), f);
            a(findViewById(C0000R.id.frame_right), 1.0f - f);
        }
    }

    /* access modifiers changed from: protected */
    public abstract aj a(String str, Bundle bundle, boolean z);

    /* access modifiers changed from: protected */
    public aj a(String str, Class cls, Bundle bundle) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        aj ajVar = (aj) Fragment.a(this, cls.getName(), bundle);
        a(ajVar, str);
        return ajVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avidia.fismobile.view.al.a(android.support.v4.app.Fragment, java.lang.String):void
     arg types: [com.avidia.fismobile.view.aj, java.lang.String]
     candidates:
      com.avidia.fismobile.view.al.a(com.avidia.fismobile.view.al, java.lang.Class):int
      com.avidia.fismobile.view.al.a(android.view.View, float):void
      com.avidia.fismobile.view.al.a(java.lang.Class, android.view.ViewGroup):void
      com.avidia.fismobile.view.al.a(java.lang.String, java.lang.String):com.avidia.fismobile.view.aj
      com.avidia.fismobile.view.al.a(com.avidia.fismobile.view.aj, java.lang.String):void
      com.avidia.fismobile.view.al.a(java.lang.String, android.os.Bundle):void
      com.avidia.fismobile.h.a(android.content.ComponentName, android.content.Context):boolean
      com.avidia.fismobile.h.a(int, android.widget.Button):void
      com.avidia.fismobile.view.al.a(android.support.v4.app.Fragment, java.lang.String):void */
    /* access modifiers changed from: protected */
    public aj a(String str, String str2) {
        aj ajVar = (aj) e().a(str);
        if (h(str2)) {
            return ajVar;
        }
        if (ajVar == null || !ajVar.g()) {
            return null;
        }
        this.o.remove(str);
        aj i = i(str2);
        if (i == null) {
            i = e(str2);
            this.o.put(str2, i);
        }
        a((Fragment) i, str2);
        ag.c(this.p, "switched to " + str2);
        return i;
    }

    /* access modifiers changed from: protected */
    public void a(Fragment fragment, String str) {
        if (fragment == null) {
            ag.b(this.p, "Fragment == null");
            return;
        }
        Fragment a = e().a(str);
        if (a == null) {
            a = (Fragment) this.o.get(str);
        }
        if (a == null || !a.g() || a != fragment) {
            e().a().b(b(fragment), fragment, str).b();
        }
    }

    /* access modifiers changed from: protected */
    public void a(aj ajVar, String str) {
        this.o.put(str, ajVar);
    }

    public void a(String str, Bundle bundle) {
        b(str, bundle, false);
    }

    /* access modifiers changed from: protected */
    public int b(Fragment fragment) {
        return bo.b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avidia.fismobile.view.al.a(android.support.v4.app.Fragment, java.lang.String):void
     arg types: [com.avidia.fismobile.view.aj, java.lang.String]
     candidates:
      com.avidia.fismobile.view.al.a(com.avidia.fismobile.view.al, java.lang.Class):int
      com.avidia.fismobile.view.al.a(android.view.View, float):void
      com.avidia.fismobile.view.al.a(java.lang.Class, android.view.ViewGroup):void
      com.avidia.fismobile.view.al.a(java.lang.String, java.lang.String):com.avidia.fismobile.view.aj
      com.avidia.fismobile.view.al.a(com.avidia.fismobile.view.aj, java.lang.String):void
      com.avidia.fismobile.view.al.a(java.lang.String, android.os.Bundle):void
      com.avidia.fismobile.h.a(android.content.ComponentName, android.content.Context):boolean
      com.avidia.fismobile.h.a(int, android.widget.Button):void
      com.avidia.fismobile.view.al.a(android.support.v4.app.Fragment, java.lang.String):void */
    public void b(String str, Bundle bundle, boolean z) {
        aj a = a(str, bundle, z);
        if (a != null) {
            a((Fragment) a, str);
        }
    }

    /* access modifiers changed from: protected */
    public boolean d(String str) {
        return false;
    }

    /* access modifiers changed from: protected */
    public aj e(String str) {
        return null;
    }

    /* access modifiers changed from: protected */
    public boolean h(String str) {
        Fragment a = e().a(str);
        return a != null && a.g();
    }

    /* access modifiers changed from: protected */
    public aj i(String str) {
        aj ajVar;
        if (((aj) this.o.get(str)) == null && (ajVar = (aj) e().a(str)) != null) {
            this.o.put(str, ajVar);
        }
        return (aj) this.o.get(str);
    }

    public void j() {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
            View currentFocus = getCurrentFocus();
            if (currentFocus != null) {
                inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
                inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
            }
            getWindow().setSoftInputMode(3);
        } catch (Exception e) {
            ag.a(this.p, "", e);
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        r();
        super.onConfigurationChanged(configuration);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(q());
        v();
        r();
        this.o = new HashMap();
    }

    /* access modifiers changed from: protected */
    public int q() {
        return bo.a();
    }

    /* access modifiers changed from: protected */
    public void v() {
        LinearLayout linearLayout = (LinearLayout) findViewById(C0000R.id.icons_layout);
        if (!FisApplication.e() && linearLayout != null) {
            linearLayout.addView(a(AccountsFragmentActivity.class));
            if (FisApplication.k().j() != 0) {
                linearLayout.addView(a(ClaimsFragmentActivity.class));
            }
            linearLayout.addView(a(MyAlertsFragmentActivity.class));
            linearLayout.addView(a(HtmlViewActivity.class));
            linearLayout.addView(a(MoreActionsFragmentActivity.class));
            ImageView imageView = new ImageView(this);
            imageView.setImageResource(C0000R.drawable.log_out);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
            layoutParams.setMargins(10, 5, 5, 5);
            imageView.setLayoutParams(layoutParams);
            imageView.setOnClickListener(new am(this));
            linearLayout.addView(imageView);
            ((TextView) findViewById(C0000R.id.header_caption)).setText(d(getClass()));
            a(getClass(), linearLayout);
        }
    }

    /* access modifiers changed from: protected */
    public void w() {
        for (String str : this.o.keySet()) {
            aj i = i(str);
            if (i != null && d(str) && i.g()) {
                e().a().a(i).b();
            }
        }
    }

    public int x() {
        return this.o.keySet().size();
    }
}
