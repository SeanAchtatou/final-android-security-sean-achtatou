package com.avidia.fismobile.view;

import android.app.Activity;
import android.view.View;

class cb implements View.OnClickListener {
    final /* synthetic */ Activity a;
    final /* synthetic */ int b;
    final /* synthetic */ Class c;
    final /* synthetic */ MultiLineReceiptsView d;

    cb(MultiLineReceiptsView multiLineReceiptsView, Activity activity, int i, Class cls) {
        this.d = multiLineReceiptsView;
        this.a = activity;
        this.b = i;
        this.c = cls;
    }

    public void onClick(View view) {
        if (this.a instanceof ae) {
            ((ae) this.a).a(this.b, this.c);
        }
    }
}
