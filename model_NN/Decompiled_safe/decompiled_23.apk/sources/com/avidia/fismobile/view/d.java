package com.avidia.fismobile.view;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.avidia.fismobile.C0000R;
import com.avidia.fismobile.FisApplication;
import com.avidia.fismobile.a;
import java.util.ArrayList;
import org.json.JSONArray;

public class d extends aj {
    public static final String P = AccountsFragmentActivity.class.getSimpleName();
    private final ArrayList Q = new ArrayList();
    private TextView R;
    private a S;
    private String T;
    private String V;
    private ListView W;
    private View X;

    /* access modifiers changed from: package-private */
    public com.avidia.b.a B() {
        if (this.S == null) {
            return null;
        }
        return this.S.a();
    }

    public void C() {
        a(i());
    }

    /* access modifiers changed from: protected */
    public void D() {
        b(i());
    }

    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(FisApplication.e() ? C0000R.layout.accounts : C0000R.layout.accounts_tablet, (ViewGroup) null);
        a(inflate, (int) C0000R.layout.header_with_btns, 0);
        c((int) C0000R.string.accounts);
        this.X = inflate.findViewById(C0000R.id.error_message);
        a(inflate);
        return inflate;
    }

    public void a(View view) {
        if (view != null || i() != null) {
            if (view == null) {
                view = i();
            }
            this.T = c().getIntent().getStringExtra("accountData");
            this.V = c().getIntent().getStringExtra("filter_name");
            this.R = (TextView) view.findViewById(C0000R.id.accounts_filter_value);
            this.W = (ListView) view.findViewById(C0000R.id.accounts_listview);
            this.S = new a((AccountsFragmentActivity) I(), this.Q, FisApplication.k().j());
            this.W.setAdapter((ListAdapter) this.S);
            AccountsFragmentActivity accountsFragmentActivity = (AccountsFragmentActivity) I();
            if (accountsFragmentActivity != null) {
                this.S.a(accountsFragmentActivity.t());
            }
            b(view);
            if (this.Q.isEmpty()) {
                a((com.avidia.b.a) null);
            }
            View findViewById = view.findViewById(C0000R.id.btn_header_right);
            if (FisApplication.e()) {
                findViewById.setOnClickListener(new e(this));
                View findViewById2 = view.findViewById(C0000R.id.btn_header_left);
                findViewById2.setOnClickListener(new f(this));
                a((int) C0000R.id.btn_header_right_layout, findViewById, view);
                a((int) C0000R.id.btn_header_left_layout, findViewById2, view);
                return;
            }
            View findViewById3 = view.findViewById(C0000R.id.filter_common);
            findViewById3.setClickable(true);
            findViewById3.findViewById(C0000R.id.filters_header_line).setVisibility(0);
            findViewById3.setBackgroundColor(d().getColor(C0000R.color.filter_layout_backgroung_tablet));
            findViewById3.setOnClickListener(new g(this, findViewById3));
            findViewById.setOnClickListener(new h(this, findViewById3));
            findViewById3.setVisibility(8);
            if (accountsFragmentActivity == null) {
                return;
            }
            if (!this.Q.isEmpty()) {
                com.avidia.b.a B = B();
                if (B == null) {
                    B = (com.avidia.b.a) this.S.getItem(0);
                }
                accountsFragmentActivity.a(B);
                this.S.b(B);
                this.S.notifyDataSetChanged();
            } else if (this.Q.isEmpty()) {
                accountsFragmentActivity.w();
            }
        }
    }

    public void a(com.avidia.b.a aVar) {
        if (this.S != null && aVar != null) {
            this.S.a(aVar);
        }
    }

    /* access modifiers changed from: protected */
    public void b(View view) {
        View findViewById;
        this.Q.clear();
        if (!TextUtils.isEmpty(this.V)) {
            this.R.setText(this.V + " accounts");
        }
        try {
            ArrayList arrayList = new ArrayList();
            JSONArray jSONArray = new JSONArray(this.T);
            for (int i = 0; i < jSONArray.length(); i++) {
                com.avidia.b.a aVar = new com.avidia.b.a();
                aVar.a(jSONArray.getJSONObject(i));
                arrayList.add(aVar);
            }
            if (arrayList.size() > 0) {
                this.Q.addAll(arrayList);
                this.W.setVisibility(0);
                this.X.setVisibility(8);
            } else {
                this.Q.clear();
                this.W.setVisibility(8);
                this.X.setVisibility(0);
            }
        } catch (Exception e) {
            if (com.avidia.a.a.e) {
                Log.e(P, "Error when parsing data:" + this.T, e);
            }
        }
        this.S.notifyDataSetChanged();
        if (view != null && (findViewById = view.findViewById(C0000R.id.filter_common)) != null) {
            findViewById.setVisibility(8);
        }
    }
}
