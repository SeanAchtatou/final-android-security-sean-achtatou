package com.avidia.fismobile.view;

import android.view.View;
import com.avidia.fismobile.C0000R;
import java.util.ArrayList;

class h implements View.OnClickListener {
    final /* synthetic */ View a;
    final /* synthetic */ d b;

    h(d dVar, View view) {
        this.b = dVar;
        this.a = view;
    }

    public void onClick(View view) {
        if (this.a.getVisibility() == 0) {
            this.a.setVisibility(8);
        } else if (this.b.I() != null) {
            ((AccountsFragmentActivity) this.b.I()).a(this.a, new ArrayList(), this.b.d().getColor(C0000R.color.filter_adapter_background_layout));
            this.a.setVisibility(0);
        }
    }
}
