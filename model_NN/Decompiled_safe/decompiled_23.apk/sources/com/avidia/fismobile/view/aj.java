package com.avidia.fismobile.view;

import android.content.res.Configuration;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.avidia.e.ag;
import com.avidia.fismobile.C0000R;
import com.avidia.fismobile.FisApplication;

public abstract class aj extends Fragment {
    protected final String U = getClass().getSimpleName();

    private void b(View view, int i) {
        TextView textView = (TextView) view.findViewById(C0000R.id.header_caption);
        if (textView != null) {
            textView.setText(i);
        }
    }

    /* access modifiers changed from: protected */
    public void H() {
        if (I() != null) {
            I().o();
        }
    }

    /* access modifiers changed from: protected */
    public al I() {
        try {
            return (al) c();
        } catch (ClassCastException e) {
            ag.a(this.U, "Activity cast exception", e);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void J() {
        al alVar = (al) c();
        if (alVar != null) {
            alVar.m();
        }
    }

    /* access modifiers changed from: protected */
    public void K() {
        al alVar = (al) c();
        if (alVar != null) {
            alVar.l();
        }
    }

    /* access modifiers changed from: protected */
    public void a(int i, View view, View view2) {
        view2.findViewById(i).setOnClickListener(new ak(this, view));
    }

    public void a(View view, int i) {
        if (view != null && view.findViewById(C0000R.id.window_title_layout) != null) {
            b(view, i);
        }
    }

    /* access modifiers changed from: protected */
    public void a(View view, int i, int i2) {
        if (FisApplication.e() || i2 != 0) {
            ViewGroup viewGroup = (ViewGroup) view.findViewById(C0000R.id.fragment_header);
            if (viewGroup == null) {
                throw new RuntimeException("Header layout not found");
            }
            viewGroup.removeAllViews();
            if (I() == null) {
                return;
            }
            if (FisApplication.e()) {
                I().getLayoutInflater().inflate(i, viewGroup);
            } else {
                I().getLayoutInflater().inflate(i2, viewGroup);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        if (I() != null) {
            I().b(str);
        }
    }

    public void a_() {
    }

    public void b(String str) {
        if (I() != null) {
            I().c(str);
        }
    }

    /* access modifiers changed from: protected */
    public void c(int i) {
        if (!FisApplication.e()) {
            try {
                ((TextView) c().findViewById(C0000R.id.window_title_layout).findViewById(C0000R.id.header_caption)).setText(i);
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public void d(int i) {
        if (I() != null) {
            I().a(i);
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        a_();
    }
}
