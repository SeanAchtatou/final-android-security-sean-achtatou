package com.avidia.fismobile.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.avidia.fismobile.C0000R;

public class a extends aj {
    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate((int) C0000R.layout.about_us, (ViewGroup) null);
        a(inflate, (int) C0000R.layout.window_caption, (int) C0000R.layout.window_caption_tablet);
        a(inflate, (int) C0000R.string.about_us);
        WebView webView = (WebView) inflate.findViewById(C0000R.id.web_view);
        webView.setBackgroundColor(0);
        webView.loadDataWithBaseURL(null, a((int) C0000R.string.about_us_message), "text/html", "UTF-8", null);
        return inflate;
    }
}
