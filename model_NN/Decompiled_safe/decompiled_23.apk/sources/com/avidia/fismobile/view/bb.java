package com.avidia.fismobile.view;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.avidia.b.e;
import com.avidia.b.v;
import com.avidia.e.ag;
import com.avidia.e.g;
import com.avidia.e.w;
import com.avidia.fismobile.C0000R;
import com.avidia.fismobile.FisApplication;
import com.avidia.fismobile.an;
import com.avidia.fismobile.m;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;

public class bb extends aj implements w {
    private static final String R = bb.class.getSimpleName();
    /* access modifiers changed from: private */
    public g P;
    /* access modifiers changed from: private */
    public m Q;

    /* access modifiers changed from: private */
    public void E() {
        ((ae) I()).g(null);
    }

    /* access modifiers changed from: private */
    public void a(m mVar) {
        ae aeVar;
        if (!FisApplication.e() && (aeVar = (ae) I()) != null && aeVar.x() == 1) {
            mVar.a();
        }
    }

    /* access modifiers changed from: private */
    public void a(m mVar, int i) {
        if (!FisApplication.e() && ((ae) I()) != null) {
            mVar.a(i);
        }
    }

    /* access modifiers changed from: private */
    public void a(List list) {
        Collections.sort(list, new be(this));
    }

    private void c(String str) {
        View i = i();
        if (i == null) {
            ag.b(R, "fillListview(): View == null");
            return;
        }
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        if (!TextUtils.isEmpty(str)) {
            try {
                JSONArray jSONArray = new JSONArray(str);
                for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                    e eVar = new e();
                    String jSONObject = jSONArray.getJSONObject(i2).toString();
                    eVar.a(jSONArray.getJSONObject(i2));
                    arrayList.add(eVar);
                    arrayList2.add(jSONObject);
                }
            } catch (Exception e) {
                ag.b(R, "Error while parsing claims");
                arrayList.clear();
            }
            if (arrayList.isEmpty()) {
                c(false);
                return;
            }
            a(arrayList);
            this.Q = new m(this, arrayList, arrayList2);
            ((ListView) i.findViewById(C0000R.id.claims_listview)).setAdapter((ListAdapter) this.Q);
            this.Q.notifyDataSetChanged();
            c(true);
        }
    }

    /* access modifiers changed from: private */
    public void c(boolean z) {
        View i = i();
        if (i != null) {
            View findViewById = i.findViewById(C0000R.id.claims_listview);
            View findViewById2 = i.findViewById(C0000R.id.no_claims_available);
            if (z) {
                findViewById.setVisibility(0);
                findViewById2.setVisibility(8);
                return;
            }
            findViewById.setVisibility(8);
            findViewById2.setVisibility(0);
        }
    }

    /* access modifiers changed from: package-private */
    public void C() {
        b().putString("claimsJson", "");
        D();
    }

    /* access modifiers changed from: package-private */
    public void D() {
        if (this.P == null) {
            String string = b().getString("claimsJson");
            if (TextUtils.isEmpty(string)) {
                an f = ((ae) I()).f();
                if (f != null) {
                    f.B();
                    K();
                    this.P = new g();
                    f.a(this.P, this);
                    return;
                }
                ag.b(R, "Unable to load claims");
                return;
            }
            c(string);
        }
    }

    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Button button;
        View inflate = layoutInflater.inflate((int) C0000R.layout.claims_layout, (ViewGroup) null);
        a(inflate, (int) C0000R.layout.header_with_btns, 0);
        a(inflate, (int) C0000R.string.accounts_menu_claim);
        if (FisApplication.e()) {
            ((Button) inflate.findViewById(C0000R.id.btn_header_left)).setOnClickListener(new bc(this));
            button = (Button) inflate.findViewById(C0000R.id.btn_header_right);
            button.setText((int) C0000R.string.add);
        } else {
            button = (Button) inflate.findViewById(C0000R.id.add_claim_button);
            button.setText((int) C0000R.string.add_claim);
        }
        int j = FisApplication.k().j();
        if (j == 1 || j == 3) {
            if (FisApplication.e()) {
                button.setVisibility(0);
            } else {
                inflate.findViewById(C0000R.id.add_claim_background).setVisibility(0);
            }
            button.setOnClickListener(new bd(this));
        } else if (FisApplication.e()) {
            button.setVisibility(8);
        } else {
            inflate.findViewById(C0000R.id.add_claim_background).setVisibility(8);
        }
        if (bundle == null) {
            b().putString("claimsJson", "");
            D();
        } else if (b().getString("claimsJson") != null) {
            C();
        }
        return inflate;
    }

    public void a(e eVar, String str) {
        try {
            String m = eVar.m();
            Bundle bundle = new Bundle();
            bundle.putString("CLAIM", m);
            bundle.putString("CLAIM_ORIGINAL", str);
            ((ae) I()).d(bundle);
        } catch (JSONException e) {
            ag.a(R, "Claim json error", e);
            H();
        }
    }

    public void a(v vVar) {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        if (vVar.a) {
            try {
                JSONArray jSONArray = new JSONArray(vVar.b);
                for (int i = 0; i < jSONArray.length(); i++) {
                    e eVar = new e();
                    String jSONObject = jSONArray.getJSONObject(i).toString();
                    eVar.f(jSONObject);
                    arrayList.add(eVar);
                    arrayList2.add(jSONObject);
                }
            } catch (Exception e) {
                ag.b(R, "Error while parsing claims");
                arrayList.clear();
            }
        }
        ((ae) I()).runOnUiThread(new bf(this, vVar, arrayList, arrayList2));
    }

    public void b(e eVar, String str) {
        a(eVar, str);
    }
}
