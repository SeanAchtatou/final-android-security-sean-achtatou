package com.avidia.fismobile.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.avidia.b.v;
import com.avidia.e.ag;
import com.avidia.e.e;
import com.avidia.e.f;
import com.avidia.e.w;
import com.avidia.fismobile.FisApplication;
import com.avidia.fismobile.an;

public class MyAlertsFragmentActivity extends al implements w {
    /* access modifiers changed from: private */
    public String o;
    private w q = new cj(this);

    /* access modifiers changed from: private */
    public void f(String str) {
        this.o = str;
        cd cdVar = (cd) i("ALERTS");
        boolean z = false;
        if (!FisApplication.e() && i("ALERT_DETAILS") == null) {
            z = true;
        }
        if (cdVar != null) {
            cdVar.a(this.o, z);
        }
    }

    private void r() {
        Bundle bundle = new Bundle();
        bundle.putString("ACCOUNTS_JSON", this.o);
        b("ALERTS", bundle, true);
    }

    private void s() {
        l();
        f().a(new f(), this);
    }

    /* access modifiers changed from: protected */
    public aj a(String str, Bundle bundle, boolean z) {
        aj i = z ? null : i(str);
        return i == null ? str.equals("ALERTS") ? a("ALERTS", cd.class, bundle) : str.equals("ALERT_DETAILS") ? a("ALERT_DETAILS", cc.class, bundle) : i : i;
    }

    public void a(v vVar) {
        runOnUiThread(new ci(this, vVar));
    }

    /* access modifiers changed from: protected */
    public int b(Fragment fragment) {
        return fragment instanceof cd ? bo.c() : super.b(fragment);
    }

    public void b(int i) {
        l();
        an f = f();
        f.B();
        f.a(new e(i), this.q);
    }

    public void onBackPressed() {
        if (p() || a("ALERT_DETAILS", "ALERTS") != null) {
            return;
        }
        if (!h("ALERTS") || p()) {
            ag.c(this.p, "onBackPressed(): fragment == null");
        } else if (FisApplication.e()) {
            super.onBackPressed();
            m();
        } else {
            m();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.o = getIntent().getStringExtra("ACCOUNTS_JSON");
        if (this.o == null) {
            s();
        }
        if (bundle == null) {
            r();
        }
    }
}
