package com.avidia.fismobile.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.avidia.b.a;
import com.avidia.b.l;
import com.avidia.b.o;
import com.avidia.e.ag;
import com.avidia.fismobile.C0000R;
import com.avidia.fismobile.al;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

public class b extends aj {
    public static final String P = b.class.getSimpleName();
    private static final List V = new ArrayList();
    private ArrayList Q;
    private al R;
    private TextView S;
    private TextView T;

    static {
        V.add("AnnualElection");
        V.add("TotalContributions");
        V.add("Payments");
        V.add("AdditionalDeposits");
        V.add("Balance");
        V.add("HSABalance");
        V.add("PortfolioBalance");
        V.add("TotalHSABalance");
    }

    private boolean a(l lVar) {
        return V.contains(lVar.d());
    }

    private boolean b(l lVar) {
        return "SubmitClaimsLastDate".equals(lVar.d()) || "PlanStartDate".equals(lVar.d()) || "PlanEndDate".equals(lVar.d());
    }

    private void c(String str) {
        try {
            this.Q.clear();
            a aVar = new a();
            JSONObject jSONObject = new JSONObject(str);
            aVar.a(jSONObject);
            this.S.setText(aVar.c);
            this.T.setText(ag.e(aVar.a));
            for (l lVar : aVar.a()) {
                if (!"AccountDisplayHeader".equals(lVar.d())) {
                    String string = jSONObject.getString(lVar.d());
                    if (b(lVar)) {
                        string = ag.f(string);
                    }
                    if (a(lVar)) {
                        string = ag.e(string);
                    }
                    if (string != null && !string.toLowerCase().equals("null")) {
                        this.Q.add(new o(lVar.c(), string));
                    }
                }
            }
            this.R.notifyDataSetChanged();
        } catch (Throwable th) {
            ag.a(P, "Error when parsing json:" + str, th);
        }
    }

    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate((int) C0000R.layout.account_details, (ViewGroup) null);
        a(inflate, (int) C0000R.layout.window_caption, (int) C0000R.layout.window_caption_tablet);
        a(inflate, (int) C0000R.string.account_details);
        this.S = (TextView) inflate.findViewById(C0000R.id.account_fullname);
        this.T = (TextView) inflate.findViewById(C0000R.id.account_balance);
        this.Q = new ArrayList();
        this.R = new al(c(), this.Q);
        ((ListView) inflate.findViewById(C0000R.id.account_details_listview)).setAdapter((ListAdapter) this.R);
        c(b().getString("account_info"));
        return inflate;
    }
}
