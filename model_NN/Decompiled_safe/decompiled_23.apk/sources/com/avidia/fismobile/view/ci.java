package com.avidia.fismobile.view;

import com.avidia.b.v;
import com.avidia.e.ag;

class ci implements Runnable {
    final /* synthetic */ v a;
    final /* synthetic */ MyAlertsFragmentActivity b;

    ci(MyAlertsFragmentActivity myAlertsFragmentActivity, v vVar) {
        this.b = myAlertsFragmentActivity;
        this.a = vVar;
    }

    public void run() {
        this.b.m();
        if (!ag.a(this.b, this.a)) {
            if (this.a.a) {
                this.b.getIntent().putExtra("ACCOUNTS_JSON", this.b.o);
                this.b.f(this.a.b);
                return;
            }
            this.b.c(ag.a(this.a));
        }
    }
}
