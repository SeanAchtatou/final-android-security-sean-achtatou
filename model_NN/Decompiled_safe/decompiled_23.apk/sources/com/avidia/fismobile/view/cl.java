package com.avidia.fismobile.view;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import com.avidia.b.u;
import com.avidia.e.ac;
import com.avidia.e.ag;
import com.avidia.fismobile.C0000R;
import java.io.File;
import java.net.URI;

public class cl extends aj {
    /* access modifiers changed from: private */
    public int P;
    private String Q;
    private Bitmap R;
    private TextView S;
    private Dialog T;
    /* access modifiers changed from: private */
    public bm V;
    private ImageViewTouchBase W;

    /* access modifiers changed from: private */
    public void C() {
        if (this.V != null) {
            this.V.dismiss();
        }
        this.V = new bm(I());
        this.V.setCancelable(true);
        this.V.a(a((int) C0000R.string.delete_image));
        this.V.b(a((int) C0000R.string.delete_image_dlg_message));
        this.V.a(a((int) C0000R.string.YES), new co(this));
        this.V.b(a((int) C0000R.string.NO), new cp(this));
        this.V.show();
    }

    /* access modifiers changed from: private */
    public void D() {
        if (this.T != null) {
            this.T.dismiss();
            this.T = null;
        }
        this.Q = ag.e();
        this.T = ac.a(I(), 140, this.Q, 150);
        this.T.show();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avidia.e.ag.a(android.graphics.Bitmap, boolean):java.lang.String
     arg types: [android.graphics.Bitmap, int]
     candidates:
      com.avidia.e.ag.a(org.json.JSONObject, java.lang.String):double
      com.avidia.e.ag.a(android.graphics.BitmapFactory$Options, int):int
      com.avidia.e.ag.a(android.net.Uri, android.support.v4.app.h):android.graphics.Bitmap
      com.avidia.e.ag.a(android.support.v4.app.h, java.lang.String):android.graphics.Bitmap
      com.avidia.e.ag.a(android.content.Context, java.lang.String):java.io.File
      com.avidia.e.ag.a(java.lang.String, java.lang.String):java.lang.String
      com.avidia.e.ag.a(int, android.content.Context):boolean
      com.avidia.e.ag.a(android.app.Activity, com.avidia.b.v):boolean
      com.avidia.e.ag.a(android.view.MenuItem, android.content.Context):boolean
      com.avidia.e.ag.a(android.graphics.Bitmap, boolean):java.lang.String */
    private void a(Bitmap bitmap) {
        a(ag.a(bitmap, false), bitmap);
    }

    private void a(Uri uri) {
        try {
            a(new File(new URI(uri.toString())).getAbsolutePath(), ag.a(uri, I()));
        } catch (Exception e) {
            H();
            ag.a(this.U, "error while adding the image", e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avidia.fismobile.view.ImageViewTouchBase.a(android.graphics.Bitmap, boolean):void
     arg types: [android.graphics.Bitmap, int]
     candidates:
      com.avidia.fismobile.view.ImageViewTouchBase.a(android.graphics.Bitmap, android.graphics.Matrix):void
      com.avidia.fismobile.view.ImageViewTouchBase.a(android.graphics.Matrix, int):float
      com.avidia.fismobile.view.ImageViewTouchBase.a(float, float):void
      com.avidia.fismobile.view.ImageViewTouchBase.a(boolean, boolean):void
      com.avidia.fismobile.view.ImageViewTouchBase.a(android.graphics.Bitmap, boolean):void */
    private void a(View view) {
        u uVar = new u();
        try {
            uVar.b(b().getString("RECEIPT"));
            this.R = ag.l(uVar.d());
        } catch (Exception e) {
            this.R = null;
        }
        if (this.R == null) {
            this.W.setVisibility(8);
            this.S.setVisibility(0);
            view.findViewById(C0000R.id.btn_header_left).setVisibility(8);
            view.findViewById(C0000R.id.btn_header_right).setVisibility(8);
        } else {
            this.W.setVisibility(0);
            this.S.setVisibility(8);
        }
        this.W.a(this.R, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avidia.fismobile.view.ImageViewTouchBase.a(android.graphics.Bitmap, boolean):void
     arg types: [android.graphics.Bitmap, int]
     candidates:
      com.avidia.fismobile.view.ImageViewTouchBase.a(android.graphics.Bitmap, android.graphics.Matrix):void
      com.avidia.fismobile.view.ImageViewTouchBase.a(android.graphics.Matrix, int):float
      com.avidia.fismobile.view.ImageViewTouchBase.a(float, float):void
      com.avidia.fismobile.view.ImageViewTouchBase.a(boolean, boolean):void
      com.avidia.fismobile.view.ImageViewTouchBase.a(android.graphics.Bitmap, boolean):void */
    private void a(String str, Bitmap bitmap) {
        if (this.R == null) {
            this.S.setVisibility(0);
            this.W.setVisibility(8);
            return;
        }
        this.W.a(this.R, true);
        this.S.setVisibility(8);
        this.W.setVisibility(0);
        ae aeVar = (ae) I();
        if (aeVar == null) {
            H();
        } else {
            aeVar.a(str, this.P);
        }
    }

    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate((int) C0000R.layout.photo_view_layout, (ViewGroup) null);
        a(inflate, (int) C0000R.layout.header_with_image_buttons, (int) C0000R.layout.header_with_image_buttons_tablet);
        a(inflate, (int) C0000R.string.receipt);
        this.W = (ImageViewTouchBase) inflate.findViewById(C0000R.id.photo_image_view);
        this.S = (TextView) inflate.findViewById(C0000R.id.error_message);
        ImageButton imageButton = (ImageButton) inflate.findViewById(C0000R.id.btn_header_left);
        ImageButton imageButton2 = (ImageButton) inflate.findViewById(C0000R.id.btn_header_right);
        imageButton.setImageResource(C0000R.drawable.delete_icon42);
        imageButton2.setImageResource(C0000R.drawable.redo_icon42);
        this.P = b().getInt("INDEX", -1);
        imageButton.setOnClickListener(new cm(this));
        imageButton2.setOnClickListener(new cn(this));
        if (bundle == null) {
            a(inflate);
        }
        return inflate;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avidia.e.ag.a(android.graphics.Bitmap, boolean):java.lang.String
     arg types: [android.graphics.Bitmap, int]
     candidates:
      com.avidia.e.ag.a(org.json.JSONObject, java.lang.String):double
      com.avidia.e.ag.a(android.graphics.BitmapFactory$Options, int):int
      com.avidia.e.ag.a(android.net.Uri, android.support.v4.app.h):android.graphics.Bitmap
      com.avidia.e.ag.a(android.support.v4.app.h, java.lang.String):android.graphics.Bitmap
      com.avidia.e.ag.a(android.content.Context, java.lang.String):java.io.File
      com.avidia.e.ag.a(java.lang.String, java.lang.String):java.lang.String
      com.avidia.e.ag.a(int, android.content.Context):boolean
      com.avidia.e.ag.a(android.app.Activity, com.avidia.b.v):boolean
      com.avidia.e.ag.a(android.view.MenuItem, android.content.Context):boolean
      com.avidia.e.ag.a(android.graphics.Bitmap, boolean):java.lang.String */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0024 A[Catch:{ Exception -> 0x0048 }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x007b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(int r6, android.content.Intent r7) {
        /*
            r5 = this;
            r3 = 140(0x8c, float:1.96E-43)
            r2 = 1
            if (r6 == r3) goto L_0x0009
            r0 = 150(0x96, float:2.1E-43)
            if (r6 != r0) goto L_0x0053
        L_0x0009:
            java.lang.String r1 = ""
            if (r6 != r3) goto L_0x0064
            if (r7 == 0) goto L_0x005a
            java.lang.String r0 = "data"
            android.os.Parcelable r0 = r7.getParcelableExtra(r0)     // Catch:{ Exception -> 0x0048 }
            if (r0 == 0) goto L_0x005a
            java.lang.String r0 = "data"
            android.os.Parcelable r0 = r7.getParcelableExtra(r0)     // Catch:{ Exception -> 0x0048 }
            android.graphics.Bitmap r0 = (android.graphics.Bitmap) r0     // Catch:{ Exception -> 0x0048 }
            if (r0 == 0) goto L_0x0057
            r0 = 0
        L_0x0022:
            if (r0 == 0) goto L_0x007b
            java.lang.String r0 = r5.Q     // Catch:{ Exception -> 0x0048 }
            android.net.Uri r0 = android.net.Uri.parse(r0)     // Catch:{ Exception -> 0x0048 }
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x0048 }
            java.net.URI r3 = new java.net.URI     // Catch:{ Exception -> 0x0048 }
            java.lang.String r4 = r0.toString()     // Catch:{ Exception -> 0x0048 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x0048 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0048 }
            boolean r3 = r2.isFile()     // Catch:{ Exception -> 0x0048 }
            if (r3 != 0) goto L_0x005c
            java.io.FileNotFoundException r0 = new java.io.FileNotFoundException     // Catch:{ Exception -> 0x0048 }
            java.lang.String r2 = r2.getAbsolutePath()     // Catch:{ Exception -> 0x0048 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x0048 }
            throw r0     // Catch:{ Exception -> 0x0048 }
        L_0x0048:
            r0 = move-exception
            r0 = r1
        L_0x004a:
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 == 0) goto L_0x0053
            r5.H()
        L_0x0053:
            r5.J()
            return
        L_0x0057:
            r5.a(r0)     // Catch:{ Exception -> 0x0048 }
        L_0x005a:
            r0 = r2
            goto L_0x0022
        L_0x005c:
            r5.a(r0)     // Catch:{ Exception -> 0x0048 }
            java.lang.String r0 = r2.getAbsolutePath()     // Catch:{ Exception -> 0x0048 }
            goto L_0x004a
        L_0x0064:
            android.net.Uri r0 = r7.getData()     // Catch:{ Exception -> 0x0048 }
            android.support.v4.app.h r2 = r5.c()     // Catch:{ Exception -> 0x0048 }
            android.graphics.Bitmap r2 = com.avidia.e.ag.a(r0, r2)     // Catch:{ Exception -> 0x0048 }
            r0 = 1
            java.lang.String r0 = com.avidia.e.ag.a(r2, r0)     // Catch:{ Exception -> 0x0048 }
            r5.a(r0, r2)     // Catch:{ Exception -> 0x0079 }
            goto L_0x004a
        L_0x0079:
            r1 = move-exception
            goto L_0x004a
        L_0x007b:
            r0 = r1
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avidia.fismobile.view.cl.a(int, android.content.Intent):void");
    }
}
