package com.avidia.fismobile.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.avidia.e.ag;
import com.avidia.fismobile.FisApplication;

public class ClaimsFragmentActivity extends ae {
    /* access modifiers changed from: protected */
    public int b(Fragment fragment) {
        return fragment instanceof bb ? bo.c() : super.b(fragment);
    }

    public void onBackPressed() {
        if (p() || f("CLAIM_DETAILS_FRAGMENT") != null || a("CLAIM_DETAILS_FRAGMENT", "CLAIMS_FRAGMENT") != null || a("CLAIM_PREVIEW_FRAGMENT", "ADD_CLAIM_FRAGMENT") != null || a("CLAIM_SUCCESS_FRAGMENT", "CLAIMS_FRAGMENT") != null || a("ADD_CLAIM_FRAGMENT", "CLAIMS_FRAGMENT") != null) {
            return;
        }
        if (!FisApplication.e() || p()) {
            ag.c(this.p, "onBackPressed(): fragment == null");
            return;
        }
        super.onBackPressed();
        m();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle == null) {
            u();
        }
    }

    /* access modifiers changed from: package-private */
    public void r() {
        bb bbVar = (bb) i("CLAIMS_FRAGMENT");
        if (bbVar != null) {
            bbVar.C();
        }
    }

    /* access modifiers changed from: package-private */
    public void s() {
        f("CLAIM_DETAILS_FRAGMENT");
    }
}
