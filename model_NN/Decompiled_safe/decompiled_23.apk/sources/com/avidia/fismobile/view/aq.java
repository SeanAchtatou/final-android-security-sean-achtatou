package com.avidia.fismobile.view;

import com.avidia.b.v;
import com.avidia.e.ag;
import com.avidia.fismobile.FisApplication;

class aq implements Runnable {
    final /* synthetic */ v a;
    final /* synthetic */ ao b;

    aq(ao aoVar, v vVar) {
        this.b = aoVar;
        this.a = vVar;
    }

    public void run() {
        this.b.J();
        if (!ag.a(this.b.I(), this.a)) {
            if (this.a.a) {
                ag.c(this.b.T, this.a.b);
                this.b.R = this.a.b;
                String unused = this.b.V = this.b.R;
                this.b.b().putString("CLAIM", this.b.R);
                this.b.b().putString("CLAIM_ORIGINAL", this.b.V);
                this.b.a(this.b.i(), ((ae) this.b.I()).getLayoutInflater());
                if (!FisApplication.e()) {
                    try {
                        bb bbVar = (bb) ((ae) this.b.I()).i("CLAIMS_FRAGMENT");
                        if (bbVar != null) {
                            bbVar.C();
                        } else {
                            ag.b(this.b.T, "Error while refreshing the claims");
                        }
                    } catch (Exception e) {
                        ag.b(this.b.T, "Error while refreshing the claims");
                    }
                }
            } else {
                this.b.b(ag.b(this.a));
            }
        }
    }
}
