package com.avidia.fismobile.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.ImageView;

public class ImageViewTouchBase extends ImageView {
    protected Matrix a = new Matrix();
    protected Matrix b = new Matrix();
    protected Bitmap c;
    int d = -1;
    int e = -1;
    float f;
    protected Handler g = new Handler();
    private final Matrix h = new Matrix();
    private final float[] i = new float[9];
    private Runnable j = null;

    public ImageViewTouchBase(Context context) {
        super(context);
        b();
    }

    public ImageViewTouchBase(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    private void a(Bitmap bitmap, Matrix matrix) {
        float width = (float) getWidth();
        float height = (float) getHeight();
        float width2 = (float) bitmap.getWidth();
        float height2 = (float) bitmap.getHeight();
        matrix.reset();
        float min = Math.min(Math.min(width / width2, 8.0f), Math.min(height / height2, 8.0f));
        matrix.postScale(min, min);
        matrix.postTranslate((width - (width2 * min)) / 2.0f, (height - (height2 * min)) / 2.0f);
    }

    private void b() {
        setScaleType(ImageView.ScaleType.MATRIX);
    }

    /* access modifiers changed from: private */
    public void b(Bitmap bitmap, boolean z) {
        if (getWidth() <= 0) {
            this.j = new bt(this, bitmap, z);
            return;
        }
        if (bitmap != null) {
            a(bitmap, this.a);
            setImageBitmapInternal(bitmap);
        } else {
            this.a.reset();
            setImageBitmap(null);
        }
        if (z) {
            this.b.reset();
        }
        setImageMatrix(getImageViewMatrix());
        this.f = a();
    }

    private void setImageBitmapInternal(Bitmap bitmap) {
        super.setImageBitmap(bitmap);
        Drawable drawable = getDrawable();
        if (drawable != null) {
            drawable.setDither(true);
        }
        if (this.c != null && !this.c.isRecycled()) {
            this.c.recycle();
        }
        this.c = bitmap;
    }

    /* access modifiers changed from: protected */
    public float a() {
        if (this.c == null) {
            return 1.0f;
        }
        return Math.max(((float) this.c.getWidth()) / ((float) this.d), ((float) this.c.getHeight()) / ((float) this.e)) * 8.0f;
    }

    /* access modifiers changed from: protected */
    public float a(Matrix matrix) {
        return a(matrix, 0);
    }

    /* access modifiers changed from: protected */
    public float a(Matrix matrix, int i2) {
        matrix.getValues(this.i);
        return this.i[i2];
    }

    /* access modifiers changed from: protected */
    public void a(float f2) {
        a(f2, ((float) getWidth()) / 2.0f, ((float) getHeight()) / 2.0f);
    }

    /* access modifiers changed from: protected */
    public void a(float f2, float f3) {
        a(f2, ((float) getWidth()) / 2.0f, ((float) getHeight()) / 2.0f, f3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avidia.fismobile.view.ImageViewTouchBase.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.avidia.fismobile.view.ImageViewTouchBase.a(android.graphics.Bitmap, android.graphics.Matrix):void
      com.avidia.fismobile.view.ImageViewTouchBase.a(android.graphics.Matrix, int):float
      com.avidia.fismobile.view.ImageViewTouchBase.a(float, float):void
      com.avidia.fismobile.view.ImageViewTouchBase.a(android.graphics.Bitmap, boolean):void
      com.avidia.fismobile.view.ImageViewTouchBase.a(boolean, boolean):void */
    public void a(float f2, float f3, float f4) {
        if (f2 > this.f) {
            f2 = this.f;
        }
        float scale = f2 / getScale();
        this.b.postScale(scale, scale, f3, f4);
        setImageMatrix(getImageViewMatrix());
        a(true, true);
    }

    public void a(float f2, float f3, float f4, float f5) {
        float scale = getScale();
        float f6 = f5;
        this.g.post(new bu(this, f6, System.currentTimeMillis(), scale, (f2 - getScale()) / f5, f3, f4));
    }

    public void a(Bitmap bitmap, boolean z) {
        b(bitmap, z);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x003e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(boolean r8, boolean r9) {
        /*
            r7 = this;
            r6 = 1073741824(0x40000000, float:2.0)
            r0 = 0
            android.graphics.Bitmap r1 = r7.c
            if (r1 != 0) goto L_0x0008
        L_0x0007:
            return
        L_0x0008:
            android.graphics.Matrix r1 = r7.getImageViewMatrix()
            android.graphics.RectF r2 = new android.graphics.RectF
            android.graphics.Bitmap r3 = r7.c
            int r3 = r3.getWidth()
            float r3 = (float) r3
            android.graphics.Bitmap r4 = r7.c
            int r4 = r4.getHeight()
            float r4 = (float) r4
            r2.<init>(r0, r0, r3, r4)
            r1.mapRect(r2)
            float r1 = r2.height()
            float r3 = r2.width()
            if (r9 == 0) goto L_0x0088
            int r4 = r7.getHeight()
            float r5 = (float) r4
            int r5 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r5 >= 0) goto L_0x0058
            float r4 = (float) r4
            float r1 = r4 - r1
            float r1 = r1 / r6
            float r4 = r2.top
            float r1 = r1 - r4
        L_0x003c:
            if (r8 == 0) goto L_0x004d
            int r4 = r7.getWidth()
            float r5 = (float) r4
            int r5 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r5 >= 0) goto L_0x0072
            float r0 = (float) r4
            float r0 = r0 - r3
            float r0 = r0 / r6
            float r2 = r2.left
            float r0 = r0 - r2
        L_0x004d:
            r7.b(r0, r1)
            android.graphics.Matrix r0 = r7.getImageViewMatrix()
            r7.setImageMatrix(r0)
            goto L_0x0007
        L_0x0058:
            float r1 = r2.top
            int r1 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
            if (r1 <= 0) goto L_0x0062
            float r1 = r2.top
            float r1 = -r1
            goto L_0x003c
        L_0x0062:
            float r1 = r2.bottom
            float r4 = (float) r4
            int r1 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r1 >= 0) goto L_0x0088
            int r1 = r7.getHeight()
            float r1 = (float) r1
            float r4 = r2.bottom
            float r1 = r1 - r4
            goto L_0x003c
        L_0x0072:
            float r3 = r2.left
            int r3 = (r3 > r0 ? 1 : (r3 == r0 ? 0 : -1))
            if (r3 <= 0) goto L_0x007c
            float r0 = r2.left
            float r0 = -r0
            goto L_0x004d
        L_0x007c:
            float r3 = r2.right
            float r5 = (float) r4
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 >= 0) goto L_0x004d
            float r0 = (float) r4
            float r2 = r2.right
            float r0 = r0 - r2
            goto L_0x004d
        L_0x0088:
            r1 = r0
            goto L_0x003c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avidia.fismobile.view.ImageViewTouchBase.a(boolean, boolean):void");
    }

    /* access modifiers changed from: protected */
    public void b(float f2, float f3) {
        this.b.postTranslate(f2, f3);
    }

    public void b(float f2, float f3, float f4) {
        float a2 = a(this.a, 0);
        if (a2 < 2.0f) {
            a(2.0f / a2, f2, f3, f4);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avidia.fismobile.view.ImageViewTouchBase.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.avidia.fismobile.view.ImageViewTouchBase.a(android.graphics.Bitmap, android.graphics.Matrix):void
      com.avidia.fismobile.view.ImageViewTouchBase.a(android.graphics.Matrix, int):float
      com.avidia.fismobile.view.ImageViewTouchBase.a(float, float):void
      com.avidia.fismobile.view.ImageViewTouchBase.a(android.graphics.Bitmap, boolean):void
      com.avidia.fismobile.view.ImageViewTouchBase.a(boolean, boolean):void */
    /* access modifiers changed from: protected */
    public void c(float f2, float f3) {
        b(f2, f3);
        a(true, true);
    }

    /* access modifiers changed from: protected */
    public Matrix getImageViewMatrix() {
        this.h.set(this.a);
        this.h.postConcat(this.b);
        return this.h;
    }

    /* access modifiers changed from: protected */
    public float getScale() {
        return a(this.b);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4 || keyEvent.getRepeatCount() != 0) {
            return super.onKeyDown(i2, keyEvent);
        }
        keyEvent.startTracking();
        return true;
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (i2 != 4 || !keyEvent.isTracking() || keyEvent.isCanceled() || getScale() <= 1.0f) {
            return super.onKeyUp(i2, keyEvent);
        }
        a(1.0f);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
        this.d = i4 - i2;
        this.e = i5 - i3;
        Runnable runnable = this.j;
        if (runnable != null) {
            this.j = null;
            runnable.run();
        }
        if (this.c != null) {
            a(this.c, this.a);
            setImageMatrix(getImageViewMatrix());
        }
    }

    public void setImageBitmap(Bitmap bitmap) {
        setImageBitmapInternal(bitmap);
    }
}
