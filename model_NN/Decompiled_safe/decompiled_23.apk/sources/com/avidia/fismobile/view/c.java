package com.avidia.fismobile.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.avidia.fismobile.C0000R;
import com.avidia.fismobile.ac;
import java.util.ArrayList;

public class c extends aj {
    private ac P;
    private String[] Q;

    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate((int) C0000R.layout.filter, (ViewGroup) null);
        a(inflate, (int) C0000R.layout.window_caption, (int) C0000R.layout.window_caption_tablet);
        a(inflate, (int) C0000R.string.filter_filter);
        AccountsFragmentActivity accountsFragmentActivity = (AccountsFragmentActivity) I();
        if (accountsFragmentActivity == null) {
            this.Q = d().getStringArray(C0000R.array.filter_values);
            this.P = new ac(accountsFragmentActivity, this.Q);
            ((ListView) inflate.findViewById(C0000R.id.filter_listview)).setAdapter((ListAdapter) this.P);
        } else {
            ArrayList arrayList = new ArrayList();
            this.P = accountsFragmentActivity.a(inflate, arrayList);
            this.Q = (String[]) arrayList.toArray(new String[arrayList.size()]);
        }
        return inflate;
    }
}
