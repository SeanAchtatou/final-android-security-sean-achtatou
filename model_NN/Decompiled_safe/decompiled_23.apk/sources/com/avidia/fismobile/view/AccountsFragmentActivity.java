package com.avidia.fismobile.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.a.a.j;
import com.avidia.b.a;
import com.avidia.b.v;
import com.avidia.b.x;
import com.avidia.e.ab;
import com.avidia.e.ag;
import com.avidia.e.d;
import com.avidia.e.r;
import com.avidia.e.w;
import com.avidia.fismobile.C0000R;
import com.avidia.fismobile.FisApplication;
import com.avidia.fismobile.ac;
import com.avidia.fismobile.an;
import java.util.Arrays;
import java.util.List;
import org.json.JSONObject;

public class AccountsFragmentActivity extends ae {
    private String o;
    private w q = new i(this);
    private w r = new k(this);
    private w s = new m(this);
    /* access modifiers changed from: private */
    public a t;

    /* access modifiers changed from: private */
    public void a(v vVar) {
        m();
        if (!ag.a(this, vVar)) {
            if (vVar.a) {
                Bundle bundle = new Bundle();
                bundle.putString("recent_transactions", vVar.b);
                bundle.putString("account_info", this.o);
                if (vVar.d instanceof x) {
                    bundle.putString("short_account_info", new j().a((x) vVar.d));
                }
                b("RECENTTRANSACTIONS", bundle, true);
                return;
            }
            c(ag.b(vVar));
        }
    }

    private a y() {
        d dVar = (d) i("ACCOUNTS");
        if (dVar == null) {
            return null;
        }
        if (dVar.B() != null) {
            this.t = dVar.B();
        }
        return this.t;
    }

    /* access modifiers changed from: private */
    public void z() {
        d dVar;
        if (!FisApplication.e() && (dVar = (d) i("ACCOUNTS")) != null) {
            dVar.a(this.t);
        }
    }

    /* access modifiers changed from: package-private */
    public ac a(View view, List list) {
        return a(view, list, 0);
    }

    /* access modifiers changed from: package-private */
    public ac a(View view, List list, int i) {
        list.addAll(Arrays.asList(getResources().getStringArray(C0000R.array.filter_values)));
        ac acVar = new ac(this, (String[]) list.toArray(new String[list.size()]), i);
        ((ListView) view.findViewById(C0000R.id.filter_listview)).setAdapter((ListAdapter) acVar);
        return acVar;
    }

    /* access modifiers changed from: protected */
    public aj a(String str, Bundle bundle, boolean z) {
        aj i = z ? null : i(str);
        return i == null ? str.equals("ACCOUNTS") ? a("ACCOUNTS", d.class, bundle) : str.equals("RECENTTRANSACTIONS") ? a("RECENTTRANSACTIONS", cr.class, bundle) : str.equals("ACCOUNT_INFO") ? a("ACCOUNT_INFO", b.class, bundle) : str.equals("TRANSACTION_DETAILS_FRAGMENT") ? a("TRANSACTION_DETAILS_FRAGMENT", cs.class, bundle) : str.equals("FILTER_ACTIVITY") ? a("FILTER_ACTIVITY", c.class, bundle) : super.a(str, bundle, z) : i;
    }

    /* access modifiers changed from: package-private */
    public void a(Bundle bundle) {
        b("TRANSACTION_DETAILS_FRAGMENT", bundle, true);
    }

    public void a(a aVar) {
        if (aVar != null) {
            String b = aVar.b();
            Bundle bundle = new Bundle();
            bundle.putString("account_info", b);
            b("ACCOUNT_INFO", bundle, true);
        }
    }

    public void a(JSONObject jSONObject) {
        an f = f();
        if (f != null) {
            f.B();
            l();
            f.a(new d(jSONObject), this.r);
            return;
        }
        ag.b(this.p, "Can not get sender fragment");
    }

    /* access modifiers changed from: protected */
    public int b(Fragment fragment) {
        return ((fragment instanceof d) || (fragment instanceof cr)) ? bo.c() : super.b(fragment);
    }

    public void b(int i) {
        this.t = y();
        if (i >= 0) {
            l();
            an f = f();
            if (f != null) {
                f.B();
                l();
                f.a(new com.avidia.e.a(i), this.q);
            }
        }
    }

    public void b(a aVar) {
        this.t = y();
        c(aVar);
    }

    /* access modifiers changed from: package-private */
    public void c(a aVar) {
        this.o = aVar.b();
        d(aVar);
    }

    /* access modifiers changed from: package-private */
    public void d(a aVar) {
        r.a = true;
        if (aVar != null) {
            an f = f();
            if (f != null) {
                f.B();
                l();
                f.a(new ab(aVar.i, aVar.c, aVar.a), this.s);
                return;
            }
            ag.b(this.p, "Can not get sender fragment when loading transactions for flex key:" + aVar.i);
        }
    }

    /* access modifiers changed from: protected */
    public boolean d(String str) {
        return !FisApplication.e() && !"ACCOUNTS".equalsIgnoreCase(str) && !"RECENTTRANSACTIONS".equalsIgnoreCase(str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avidia.fismobile.view.AccountsFragmentActivity.a(java.lang.String, android.os.Bundle, boolean):com.avidia.fismobile.view.aj
     arg types: [java.lang.String, ?[OBJECT, ARRAY], int]
     candidates:
      com.avidia.fismobile.view.AccountsFragmentActivity.a(android.view.View, java.util.List, int):com.avidia.fismobile.ac
      com.avidia.fismobile.view.ae.a(com.avidia.fismobile.view.aj, java.lang.String, java.lang.String):android.support.v4.app.Fragment
      com.avidia.fismobile.view.al.a(java.lang.String, java.lang.Class, android.os.Bundle):com.avidia.fismobile.view.aj
      android.support.v4.app.h.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.h.a(java.lang.String, boolean, boolean):android.support.v4.app.aa
      com.avidia.fismobile.view.AccountsFragmentActivity.a(java.lang.String, android.os.Bundle, boolean):com.avidia.fismobile.view.aj */
    /* access modifiers changed from: protected */
    public aj e(String str) {
        return str.equals("ACCOUNTS") ? a("ACCOUNTS", (Bundle) null, true) : super.e(str);
    }

    public void onBackPressed() {
        if (p() || f("TRANSACTION_DETAILS_FRAGMENT") != null || a("RECENTTRANSACTIONS", "ACCOUNTS") != null || a("TRANSACTION_DETAILS_FRAGMENT", "RECENTTRANSACTIONS") != null || a("ACCOUNT_INFO", "ACCOUNTS") != null || a("FILTER_ACTIVITY", "ACCOUNTS") != null || a("ADD_CLAIM_FRAGMENT", "ACCOUNTS") != null || a("RECEIPT_EDIT_FRAGMENT", "ADD_CLAIM_FRAGMENT") != null || a("CLAIM_PREVIEW_FRAGMENT", "ADD_CLAIM_FRAGMENT") != null || a("CLAIM_SUCCESS_FRAGMENT", "ACCOUNTS") != null) {
            return;
        }
        if (!FisApplication.e() || p()) {
            ag.c(this.p, "onBackPressed(): fragment == null");
            return;
        }
        super.onBackPressed();
        m();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle == null) {
            Bundle bundle2 = new Bundle();
            getIntent().putExtra("accountData", FisApplication.k().u());
            getIntent().putExtra("filter_name", FisApplication.k().i());
            a("ACCOUNTS", bundle2);
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        if (bundle.containsKey("currentAccount")) {
            this.o = bundle.getString("currentAccount");
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
        super.onSaveInstanceState(bundle);
        if (this.o != null) {
            bundle.putString("currentAccount", this.o);
        }
    }

    public void r() {
        a("FILTER_ACTIVITY", (Bundle) null);
    }

    /* access modifiers changed from: package-private */
    public void s() {
        f("TRANSACTION_DETAILS_FRAGMENT");
    }

    public a t() {
        return this.t;
    }
}
