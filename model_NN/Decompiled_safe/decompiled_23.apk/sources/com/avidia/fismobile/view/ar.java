package com.avidia.fismobile.view;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.avidia.b.v;
import com.avidia.e.ag;
import com.avidia.e.b;
import com.avidia.e.w;
import com.avidia.fismobile.C0000R;
import com.avidia.fismobile.FisApplication;
import com.avidia.fismobile.an;

public class ar extends ai implements w {
    private String V;
    /* access modifiers changed from: private */
    public bm W;
    /* access modifiers changed from: private */
    public bm X;

    /* access modifiers changed from: private */
    public void L() {
        this.X = new bm(I());
        this.X.setCancelable(false);
        this.X.a(a((int) C0000R.string.accept), new at(this));
        this.X.b(a((int) C0000R.string.decline), new au(this));
        this.X.a(a((int) C0000R.string.certification));
        this.X.a(Html.fromHtml(this.V));
        this.X.show();
    }

    /* access modifiers changed from: private */
    public void M() {
        this.W = new bm(I());
        this.W.setCancelable(false);
        this.W.a(a((int) C0000R.string.YES), new av(this));
        this.W.b(a((int) C0000R.string.NO), new aw(this));
        this.W.b(a((int) C0000R.string.abandon_dlg_message));
        this.W.show();
    }

    /* access modifiers changed from: private */
    public void N() {
        if (this.R == null) {
            ag.b(this.T, "on submit: Claim == null");
            H();
            return;
        }
        try {
            an f = ((ae) I()).f();
            if (f != null) {
                f.B();
                K();
                f.a(new b(this.R), this);
                return;
            }
            ag.b(this.T, "Sender fragment == null");
        } catch (Exception e) {
            ag.b(this.T, "Sender fragment == null");
            H();
        }
    }

    /* access modifiers changed from: protected */
    public int C() {
        return C0000R.layout.header_with_btns;
    }

    /* access modifiers changed from: protected */
    public int D() {
        return C0000R.string.claim_preview;
    }

    /* access modifiers changed from: protected */
    public boolean G() {
        return true;
    }

    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.V = FisApplication.k().h().m();
        return super.a(layoutInflater, viewGroup, bundle);
    }

    /* access modifiers changed from: protected */
    public void a(View view) {
        Button button;
        if (FisApplication.e()) {
            view.findViewById(C0000R.id.btn_header_left).setVisibility(8);
            button = (Button) view.findViewById(C0000R.id.btn_header_right);
        } else {
            button = (Button) view.findViewById(C0000R.id.submit);
            button.setVisibility(0);
        }
        button.setText((int) C0000R.string.submit);
        button.setOnClickListener(new as(this));
    }

    public void a(v vVar) {
        ((ae) I()).runOnUiThread(new ax(this, vVar));
    }
}
