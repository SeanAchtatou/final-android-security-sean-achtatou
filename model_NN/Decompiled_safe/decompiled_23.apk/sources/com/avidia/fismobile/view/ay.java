package com.avidia.fismobile.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.avidia.e.k;
import com.avidia.fismobile.C0000R;
import com.avidia.fismobile.FisApplication;

public class ay extends ai {
    /* access modifiers changed from: private */
    public void L() {
        if (FisApplication.e()) {
            ((ae) I()).u();
        }
    }

    /* access modifiers changed from: protected */
    public int C() {
        return C0000R.layout.header_with_btns;
    }

    /* access modifiers changed from: protected */
    public int D() {
        return C0000R.string.success;
    }

    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ae aeVar;
        new k().execute(new Void[0]);
        if (!FisApplication.e() && (aeVar = (ae) I()) != null && (aeVar instanceof ClaimsFragmentActivity)) {
            ((ClaimsFragmentActivity) aeVar).r();
        }
        return super.a(layoutInflater, viewGroup, bundle);
    }

    /* access modifiers changed from: protected */
    public void a(View view) {
        ViewGroup viewGroup = (ViewGroup) view.findViewById(C0000R.id.other_content);
        if (FisApplication.e()) {
            view.findViewById(C0000R.id.btn_header_left).setVisibility(8);
            Button button = (Button) view.findViewById(C0000R.id.btn_header_right);
            button.setText((int) C0000R.string.back_to_claims);
            button.setOnClickListener(new az(this));
        }
        Button button2 = new Button(I());
        button2.setText((int) C0000R.string.add_another_claim);
        button2.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        viewGroup.setPadding(10, 20, 10, 10);
        button2.setOnClickListener(new ba(this));
        viewGroup.addView(button2);
    }
}
