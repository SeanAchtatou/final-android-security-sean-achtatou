package com.avidia.fismobile.view;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.a.a.j;
import com.avidia.b.o;
import com.avidia.b.t;
import com.avidia.b.x;
import com.avidia.e.ac;
import com.avidia.e.ag;
import com.avidia.fismobile.C0000R;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class cs extends aj {
    public static final String Q = cs.class.getSimpleName();
    boolean P = false;
    private MultiLineReceiptsView R;
    private List S;
    private j T;
    private String V;
    private LinearLayout W;
    private TextView X;
    private Dialog Y;
    private TextView Z;
    private ArrayList aa;

    /* access modifiers changed from: private */
    public void D() {
        if (this.Y != null) {
            this.Y.dismiss();
        }
        AccountsFragmentActivity accountsFragmentActivity = (AccountsFragmentActivity) I();
        if (accountsFragmentActivity != null) {
            this.V = ag.e();
            this.Y = ac.a(accountsFragmentActivity, 120, this.V, 130);
            this.Y.show();
        }
    }

    private JSONObject a(String str, int i) {
        return new JSONArray(str).getJSONObject(i);
    }

    private void a(View view) {
        JSONArray jSONArray;
        x xVar = (x) this.T.a(b().getString("short_account_info"), x.class);
        try {
            String string = b().getString("UPLOADED_RECEIPTS");
            if (TextUtils.isEmpty(string) || (jSONArray = new JSONObject(string).optJSONArray("ReceiptsInfo")) == null || jSONArray.length() <= 0) {
                jSONArray = null;
            }
        } catch (Exception e) {
            jSONArray = null;
        }
        this.Z.setText(xVar.a);
        this.X.setText(ag.e(xVar.b));
        String string2 = b().getString("transactions_info");
        int i = b().getInt("position", -1);
        if (i == -1) {
            ag.b(Q, "Wrong position (-1)");
        } else {
            a(string2, i, view, jSONArray);
        }
    }

    private void a(String str, int i, View view, JSONArray jSONArray) {
        try {
            JSONObject a = a(str, i);
            this.aa.clear();
            a("Date of Service", ag.f(a.getString("TransactionDate")));
            a("Amount", ag.e(a.getString("TransactionAmt")));
            a("Description", a.getString("Description"));
            a("Type", a.getString("TransactionType"));
            a("Status", a.getString("TransactionStatus"));
            this.W.removeAllViews();
            Iterator it = this.aa.iterator();
            while (it.hasNext()) {
                o oVar = (o) it.next();
                View inflate = c().getLayoutInflater().inflate((int) C0000R.layout.key_value_item, (ViewGroup) null);
                ImageView imageView = new ImageView(c());
                imageView.setBackgroundResource(C0000R.drawable.listview_divider);
                TextView textView = (TextView) inflate.findViewById(C0000R.id.textview_value);
                ((TextView) inflate.findViewById(C0000R.id.textview_key)).setText(oVar.a);
                String str2 = oVar.b;
                if (ag.k(str2)) {
                    textView.setText(Html.fromHtml(str2));
                } else {
                    textView.setText(str2);
                }
                this.W.addView(inflate);
                this.W.addView(imageView, -1, -2);
            }
            this.P = a.optBoolean("CanUploadReceipt", false);
            String optString = a.optString("ReceiptsInfo");
            if (jSONArray == null) {
                a(this.P, optString);
            } else {
                a(this.P, jSONArray.toString());
            }
        } catch (Throwable th) {
            ag.a(Q, "Error when parsing json:" + str, th);
            H();
            this.W.removeAllViews();
        }
    }

    private void a(String str, String str2) {
        if (str2 == null || str2.equalsIgnoreCase("null")) {
            str2 = "";
        }
        this.aa.add(new o(str, str2));
    }

    private void a(boolean z, String str) {
        JSONArray jSONArray;
        View.OnClickListener onClickListener = null;
        try {
            jSONArray = new JSONArray(str);
        } catch (Exception e) {
            ag.c(Q, "Error while loading receipts");
            jSONArray = null;
        }
        this.S = new ArrayList();
        if (jSONArray == null) {
            jSONArray = new JSONArray();
        }
        int i = 0;
        while (i < jSONArray.length()) {
            try {
                t tVar = new t();
                tVar.b(jSONArray.optJSONObject(i).toString());
                this.S.add(tVar);
                i++;
            } catch (Exception e2) {
                ag.c(Q, "Error while filling receipts");
                return;
            }
        }
        if (this.R != null) {
            MultiLineReceiptsView multiLineReceiptsView = this.R;
            Class<?> cls = getClass();
            ae aeVar = (ae) c();
            LayoutInflater layoutInflater = c().getLayoutInflater();
            Display defaultDisplay = c().getWindowManager().getDefaultDisplay();
            if (z) {
                onClickListener = C();
            }
            multiLineReceiptsView.a(cls, aeVar, layoutInflater, defaultDisplay, onClickListener, this.S);
        }
    }

    private void d(String str) {
        if (TextUtils.isEmpty(str)) {
            H();
            return;
        }
        String string = b().getString("transactions_info");
        JSONObject jSONObject = new JSONArray(string).getJSONObject(b().getInt("position", -1));
        jSONObject.remove("ReceiptsInfo");
        JSONArray jSONArray = new JSONArray();
        jSONArray.put(t.c(str));
        jSONObject.put("ReceiptsInfo", jSONArray);
        AccountsFragmentActivity accountsFragmentActivity = (AccountsFragmentActivity) I();
        if (accountsFragmentActivity == null) {
            ag.c(Q, "Error while adding receipt: activity == null");
        } else {
            accountsFragmentActivity.a(jSONObject);
        }
    }

    /* access modifiers changed from: protected */
    public View.OnClickListener C() {
        return new ct(this);
    }

    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate((int) C0000R.layout.transaction_details, (ViewGroup) null);
        a(inflate, (int) C0000R.layout.window_caption, (int) C0000R.layout.window_caption_tablet);
        a(inflate, (int) C0000R.string.transactions_details);
        this.Z = (TextView) inflate.findViewById(C0000R.id.account_fullname);
        this.X = (TextView) inflate.findViewById(C0000R.id.account_balance);
        this.R = (MultiLineReceiptsView) inflate.findViewById(C0000R.id.photo_container);
        this.W = (LinearLayout) inflate.findViewById(C0000R.id.list_view);
        this.aa = new ArrayList();
        this.T = new j();
        a(inflate);
        return inflate;
    }

    public void a_() {
        this.R.a(getClass(), (ae) c(), c().getLayoutInflater(), c().getWindowManager().getDefaultDisplay(), this.P ? C() : null, this.S);
    }

    public void b(int i, int i2, Intent intent) {
        if ((i == 120 || i == 130) && i2 == -1) {
            Bitmap bitmap = null;
            if (i == 120) {
                boolean z = true;
                if (intent != null) {
                    try {
                        if (intent.getExtras().getParcelable("data") != null) {
                            Bitmap a = ag.a((Bitmap) intent.getExtras().getParcelable("data"));
                            if (a != null) {
                                z = false;
                                bitmap = a;
                            } else {
                                bitmap = a;
                            }
                        }
                    } catch (Exception e) {
                        H();
                        J();
                        return;
                    }
                }
                if (z) {
                    Uri parse = Uri.parse(this.V);
                    File file = new File(new URI(parse.toString()));
                    if (!file.isFile()) {
                        throw new FileNotFoundException(file.getAbsolutePath());
                    }
                    bitmap = ag.a(parse, c());
                }
            } else {
                bitmap = ag.a(intent.getData(), c());
            }
            String b = ag.b(bitmap);
            if (TextUtils.isEmpty(b)) {
                J();
                H();
                return;
            }
            d(b);
            return;
        }
        J();
    }

    public void c(String str) {
        b().putString("UPLOADED_RECEIPTS", str);
        a(i());
    }
}
