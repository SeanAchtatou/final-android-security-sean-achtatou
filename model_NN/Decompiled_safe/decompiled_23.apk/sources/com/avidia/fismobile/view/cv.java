package com.avidia.fismobile.view;

import android.view.GestureDetector;
import android.view.MotionEvent;

class cv extends GestureDetector.SimpleOnGestureListener {
    final /* synthetic */ ZoomableView a;

    private cv(ZoomableView zoomableView) {
        this.a = zoomableView;
    }

    public boolean onDoubleTap(MotionEvent motionEvent) {
        if (this.a.a()) {
            ImageViewTouchBase a2 = this.a.a;
            if (a2.getScale() > 1.0f) {
                a2.a(1.0f, 100.0f);
            } else {
                a2.b(motionEvent.getX(), motionEvent.getY(), 100.0f);
            }
        }
        return true;
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        return false;
    }

    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        ImageViewTouchBase a2 = this.a.a;
        if (a2.getScale() <= 1.0f) {
            return true;
        }
        a2.c(-f, -f2);
        return true;
    }

    public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
        return false;
    }

    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }
}
