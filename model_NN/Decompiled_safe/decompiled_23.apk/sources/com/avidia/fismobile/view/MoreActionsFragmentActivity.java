package com.avidia.fismobile.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.avidia.e.ag;
import com.avidia.fismobile.C0000R;
import com.avidia.fismobile.FisApplication;

public class MoreActionsFragmentActivity extends al {
    /* access modifiers changed from: protected */
    public aj a(String str, Bundle bundle, boolean z) {
        aj i = z ? null : i(str);
        return i == null ? str.equals("MORE") ? a("MORE", bv.class, bundle) : str.equals("PRIVACY_AND_SECURITY") ? a("PRIVACY_AND_SECURITY", bp.class, bundle) : str.equals("ABOUT_US") ? a("ABOUT_US", a.class, bundle) : str.equals("CLEAR_ID") ? a("CLEAR_ID", bh.class, bundle) : i : i;
    }

    /* access modifiers changed from: protected */
    public int b(Fragment fragment) {
        return fragment instanceof bv ? bo.c() : super.b(fragment);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avidia.fismobile.view.MoreActionsFragmentActivity.a(java.lang.String, android.os.Bundle, boolean):com.avidia.fismobile.view.aj
     arg types: [java.lang.String, ?[OBJECT, ARRAY], int]
     candidates:
      com.avidia.fismobile.view.al.a(java.lang.String, java.lang.Class, android.os.Bundle):com.avidia.fismobile.view.aj
      android.support.v4.app.h.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.h.a(java.lang.String, boolean, boolean):android.support.v4.app.aa
      com.avidia.fismobile.view.MoreActionsFragmentActivity.a(java.lang.String, android.os.Bundle, boolean):com.avidia.fismobile.view.aj */
    /* access modifiers changed from: protected */
    public aj e(String str) {
        return str.equals("MORE") ? a("MORE", (Bundle) null, true) : super.e(str);
    }

    public void onBackPressed() {
        if (p() || a("ABOUT_US", "MORE") != null || a("CLEAR_ID", "MORE") != null || a("PRIVACY_AND_SECURITY", "MORE") != null) {
            return;
        }
        if (!FisApplication.e() || p()) {
            ag.c(this.p, "onBackPressed(): fragment == null");
            m();
            return;
        }
        super.onBackPressed();
        m();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle == null) {
            a("MORE", (Bundle) null);
            if (!FisApplication.e()) {
                a("ABOUT_US", (Bundle) null);
            }
        }
    }

    public void r() {
        try {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.avidia.fismobile")));
        } catch (Exception e) {
            b("Unsupported action");
        }
    }

    public void s() {
        Bundle bundle = new Bundle();
        bundle.putString("html_text", getString(C0000R.string.privacy_and_security_message));
        bundle.putInt("HEADER_ID", C0000R.string.privacy_and_security);
        a("PRIVACY_AND_SECURITY", bundle);
    }
}
