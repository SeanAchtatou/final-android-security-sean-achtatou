package com.avidia.fismobile.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.a.a.j;
import com.avidia.a.a;
import com.avidia.b.b;
import com.avidia.b.o;
import com.avidia.e.ag;
import com.avidia.fismobile.C0000R;
import com.avidia.fismobile.g;
import java.util.ArrayList;
import org.json.JSONObject;

public class cc extends aj {
    public static final String P = cc.class.getSimpleName();
    private int Q;
    private b R;
    private ArrayList S;
    private g T;

    private void b(int i) {
        if (i != -1) {
            if (this.R.e == 1) {
                this.S.clear();
                this.S.add(new o("Date", ag.f(this.R.b)));
                this.S.add(new o("Sent via", e(this.R.e)));
                this.S.add(new o("Subject", this.R.c));
                this.S.add(new o("Message", this.R.f));
                this.T.notifyDataSetChanged();
            } else if (this.Q != i) {
                this.Q = i;
                ((MyAlertsFragmentActivity) I()).b(this.Q);
            }
        }
    }

    private String e(int i) {
        switch (i) {
            case 0:
                return "Email";
            case 1:
                return "SMS";
            default:
                return "";
        }
    }

    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate((int) C0000R.layout.alert_details, (ViewGroup) null);
        a(inflate, (int) C0000R.layout.window_caption, (int) C0000R.layout.window_caption_tablet);
        a(inflate, (int) C0000R.string.alert_details);
        this.S = new ArrayList();
        this.T = new g(I(), this.S);
        ((ListView) inflate.findViewById(C0000R.id.alert_details_listview)).setAdapter((ListAdapter) this.T);
        int i = b().getInt("comm_hist_key", -1);
        this.R = (b) new j().a(b().getString("alert_info"), b.class);
        b(i);
        return inflate;
    }

    /* access modifiers changed from: package-private */
    public void c(String str) {
        if (a.e) {
            Log.d(P, "AlertDetailData:" + str);
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            this.S.clear();
            this.S.add(new o("Date", this.R.b));
            this.S.add(new o("Sent via", e(this.R.e)));
            this.S.add(new o("Subject", this.R.c));
            this.S.add(new o("Message", jSONObject.getString("Body")));
        } catch (Throwable th) {
            if (a.e) {
                Log.e(P, "", th);
            }
        }
        this.T.notifyDataSetChanged();
    }
}
