package com.avidia.fismobile.view;

import com.avidia.b.e;
import com.avidia.e.ag;
import java.util.Comparator;

class be implements Comparator {
    final /* synthetic */ bb a;

    be(bb bbVar) {
        this.a = bbVar;
    }

    /* renamed from: a */
    public int compare(e eVar, e eVar2) {
        String f = ag.f(eVar.e());
        String f2 = ag.f(eVar2.e());
        try {
            return Long.valueOf(ag.h(f2).getTimeInMillis()).compareTo(Long.valueOf(ag.h(f).getTimeInMillis()));
        } catch (Exception e) {
            return 1;
        }
    }
}
