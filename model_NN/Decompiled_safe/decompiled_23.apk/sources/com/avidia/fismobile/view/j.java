package com.avidia.fismobile.view;

import com.avidia.b.a;
import com.avidia.b.v;
import com.avidia.e.ag;
import com.avidia.fismobile.FisApplication;

class j implements Runnable {
    final /* synthetic */ v a;
    final /* synthetic */ i b;

    j(i iVar, v vVar) {
        this.b = iVar;
        this.a = vVar;
    }

    public void run() {
        d dVar;
        if (!ag.a(this.b.a, this.a)) {
            this.b.a.m();
            if (this.a.a) {
                if (FisApplication.e()) {
                    d dVar2 = (d) this.b.a.a("FILTER_ACTIVITY", "ACCOUNTS");
                    if (dVar2 == null) {
                        dVar2 = (d) this.b.a.i("ACCOUNTS");
                    }
                    this.b.a.z();
                    a unused = this.b.a.t = (a) null;
                    dVar = dVar2;
                } else {
                    dVar = (d) this.b.a.i("ACCOUNTS");
                }
                if (dVar == null) {
                    this.b.a.m();
                    return;
                }
                if (this.a.d instanceof Integer) {
                    String str = com.avidia.a.a.d[((Integer) this.a.d).intValue()];
                    FisApplication.k().h(this.a.b);
                    FisApplication.k().d(str);
                    this.b.a.getIntent().putExtra("accountData", this.a.b);
                    this.b.a.getIntent().putExtra("filter_name", str);
                    dVar.C();
                }
                dVar.D();
            } else {
                this.b.a.c(ag.b(this.a));
            }
            this.b.a.m();
        }
    }
}
