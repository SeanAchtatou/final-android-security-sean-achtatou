package com.avidia.fismobile.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.a.a.j;
import com.avidia.a.a;
import com.avidia.b.b;
import com.avidia.b.c;
import com.avidia.e.ag;
import com.avidia.fismobile.C0000R;
import com.avidia.fismobile.FisApplication;
import com.avidia.fismobile.e;
import org.json.JSONArray;
import org.json.JSONObject;

public class cd extends aj {
    private j P;
    /* access modifiers changed from: private */
    public CheckBox Q;
    /* access modifiers changed from: private */
    public CheckBox R;
    private e S;

    /* access modifiers changed from: private */
    public void C() {
        this.S.a(this.Q.isChecked(), this.R.isChecked());
    }

    private void b(String str, boolean z) {
        this.S.b();
        b().putString("ACCOUNTS_JSON", str);
        if (a.e) {
            Log.d(this.U, "Alert data:" + str);
        }
        try {
            JSONArray jSONArray = new JSONArray(str);
            for (int i = 0; i < jSONArray.length(); i++) {
                b bVar = new b();
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                bVar.b = ag.f(jSONObject.getString("InsertDte"));
                bVar.e = jSONObject.optInt("CommunicationType");
                if (bVar.e == 0) {
                    bVar.c = jSONObject.getString("Subject");
                } else {
                    bVar.c = jSONObject.getString("CustTempName");
                    bVar.f = jSONObject.getString("Subject");
                }
                bVar.a = jSONObject.optInt("CommunicationType");
                bVar.d = jSONObject.optInt("CommHistKey");
                this.S.a(bVar);
            }
            FisApplication k = FisApplication.k();
            c t = k.t();
            if (t == null) {
                t = new c();
            }
            t.a(this.S.c());
            k.a(t);
            this.S.a(t);
            if (z && !this.S.c().isEmpty()) {
                b(0);
            }
        } catch (Throwable th) {
            if (a.e) {
                Log.e(this.U, "", th);
            }
        }
        C();
        this.S.notifyDataSetChanged();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avidia.fismobile.view.cd.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.avidia.fismobile.view.aj.a(android.view.View, int):void
      android.support.v4.app.Fragment.a(android.content.Context, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.Fragment.a(int, java.lang.Object[]):java.lang.String
      android.support.v4.app.Fragment.a(int, android.support.v4.app.Fragment):void
      android.support.v4.app.Fragment.a(android.view.Menu, android.view.MenuInflater):void
      android.support.v4.app.Fragment.a(android.view.View, android.os.Bundle):void
      com.avidia.fismobile.view.cd.a(java.lang.String, boolean):void */
    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.P = new j();
        View inflate = layoutInflater.inflate(FisApplication.e() ? C0000R.layout.myalerts : C0000R.layout.myalerts_tablet, (ViewGroup) null);
        a(inflate, (int) C0000R.layout.window_caption, 0);
        a(inflate, (int) C0000R.string.myalerts);
        this.S = new e(this, (MyAlertsFragmentActivity) I());
        ((ListView) inflate.findViewById(C0000R.id.alerts_listview)).setAdapter((ListAdapter) this.S);
        this.Q = (CheckBox) inflate.findViewById(C0000R.id.filter_by_sms);
        this.R = (CheckBox) inflate.findViewById(C0000R.id.filter_by_email);
        this.Q.setOnCheckedChangeListener(new ce(this));
        this.R.setOnCheckedChangeListener(new cf(this));
        inflate.findViewById(C0000R.id.sms_filter_area).setOnClickListener(new cg(this));
        inflate.findViewById(C0000R.id.emal_filter_area).setOnClickListener(new ch(this));
        a(b().getString("ACCOUNTS_JSON"), false);
        return inflate;
    }

    public void a(String str, boolean z) {
        if (str != null) {
            b(str, z);
        }
    }

    public void b(int i) {
        b bVar = (b) this.S.getItem(i);
        c a = this.S.a();
        if (a != null) {
            a.b(bVar.d);
            FisApplication.k().a(a);
        }
        Bundle bundle = new Bundle();
        bundle.putString("alert_info", this.P.a(bVar));
        bundle.putInt("comm_hist_key", bVar.d);
        this.S.a(i);
        ((MyAlertsFragmentActivity) I()).b("ALERT_DETAILS", bundle, true);
    }
}
