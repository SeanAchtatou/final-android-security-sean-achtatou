package com.avidia.fismobile.view;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import com.avidia.b.e;
import com.avidia.e.ag;
import com.avidia.fismobile.C0000R;

class w implements View.OnClickListener {
    final /* synthetic */ o a;

    w(o oVar) {
        this.a = oVar;
    }

    public void onClick(View view) {
        String c = this.a.M();
        if (c != null) {
            this.a.a(c);
            return;
        }
        this.a.D();
        try {
            String b = this.a.b(this.a.i());
            Bundle bundle = new Bundle();
            bundle.putString("CLAIM", b);
            e eVar = new e();
            eVar.f(b);
            e unused = this.a.P = eVar;
            String string = this.a.b().getString("ACCOUNT_TYPE");
            if (!TextUtils.isEmpty(string)) {
                bundle.putString("ACCOUNT_TYPE", string);
            }
            ((ae) this.a.I()).b(bundle);
        } catch (Exception e) {
            ag.a(o.ao, "Error while parsing the claim", e);
            this.a.d((int) C0000R.string.internal_error);
        }
    }
}
