package com.avidia.fismobile.view;

import android.app.Dialog;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.avidia.b.e;
import com.avidia.b.g;
import com.avidia.b.h;
import com.avidia.b.u;
import com.avidia.b.v;
import com.avidia.e.ac;
import com.avidia.e.ag;
import com.avidia.e.i;
import com.avidia.e.j;
import com.avidia.e.w;
import com.avidia.fismobile.C0000R;
import com.avidia.fismobile.FisApplication;
import com.avidia.fismobile.ai;
import com.avidia.fismobile.an;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class o extends aj implements w {
    /* access modifiers changed from: private */
    public static final String ao = o.class.getSimpleName();
    /* access modifiers changed from: private */
    public e P;
    private List Q;
    private Dialog R;
    private ad S;
    /* access modifiers changed from: private */
    public TextView T;
    /* access modifiers changed from: private */
    public TextView V;
    private EditText W;
    private EditText X;
    /* access modifiers changed from: private */
    public Button Y;
    private Button Z;
    /* access modifiers changed from: private */
    public Button aa;
    private Button ab;
    /* access modifiers changed from: private */
    public com.avidia.fismobile.w ac;
    /* access modifiers changed from: private */
    public com.avidia.fismobile.w ad;
    /* access modifiers changed from: private */
    public com.avidia.fismobile.w ae;
    /* access modifiers changed from: private */
    public com.avidia.fismobile.w af;
    /* access modifiers changed from: private */
    public i ag;
    private MultiLineReceiptsView ah;
    private int ai;
    private String aj;
    /* access modifiers changed from: private */
    public ai ak;
    /* access modifiers changed from: private */
    public ai al;
    /* access modifiers changed from: private */
    public int am = 0;
    /* access modifiers changed from: private */
    public int an = 0;

    /* access modifiers changed from: private */
    public void D() {
        ae aeVar = (ae) I();
        if (aeVar != null) {
            aeVar.j();
        }
    }

    private void E() {
        FisApplication.k().f();
        an f = ((ae) I()).f();
        if (f != null) {
            K();
            this.ag = new i();
            f.B();
            K();
            f.a(this.ag, this);
            return;
        }
        ag.b(ao, "Unable to load contributions");
    }

    /* access modifiers changed from: private */
    public void F() {
        if (this.R != null) {
            this.R.dismiss();
            this.R = null;
        }
        this.aj = ag.e();
        this.R = ac.a((ae) I(), 310, this.aj, 320);
        this.R.show();
    }

    private String G() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("claimantSelection", this.am);
        jSONObject.put("sccSelection", this.an);
        jSONObject.put("startDateTextView", this.V.getText().toString());
        jSONObject.put("endDateTextView", this.T.getText().toString());
        jSONObject.put("claimantButton", this.Z.getText().toString());
        jSONObject.put("accTypeButton", this.Y.getText().toString());
        jSONObject.put("reimbMethodButton", this.aa.getText().toString());
        jSONObject.put("serviceTypeButton", this.ab.getText().toString());
        jSONObject.put("amountTextEditor", j.a(this.W.getText().toString()));
        jSONObject.put("commentsTextEditor", this.X.getText().toString());
        return jSONObject.toString();
    }

    private void L() {
        String string = b().getString("FORM_JSON");
        if (!TextUtils.isEmpty(string)) {
            try {
                JSONObject jSONObject = new JSONObject(string);
                this.am = jSONObject.getInt("claimantSelection");
                this.an = jSONObject.getInt("sccSelection");
                this.V.setText(jSONObject.optString("startDateTextView"));
                this.T.setText(jSONObject.optString("endDateTextView"));
                this.Z.setText(jSONObject.optString("claimantButton"));
                this.Y.setText(jSONObject.optString("accTypeButton"));
                this.aa.setText(jSONObject.optString("reimbMethodButton"));
                this.ab.setText(jSONObject.optString("serviceTypeButton"));
                this.W.setText(jSONObject.optString("amountTextEditor"));
                this.X.setText(jSONObject.optString("commentsTextEditor"));
            } catch (JSONException e) {
                ag.a(ao, "Error while restoring the form", e);
            }
        }
    }

    /* access modifiers changed from: private */
    public String M() {
        View i = i();
        if (i == null) {
            return a((int) C0000R.string.internal_error);
        }
        String N = N();
        if (N != null) {
            return N;
        }
        a((Boolean) false);
        if (!a(this.W.getText())) {
            return a((int) C0000R.string.enter_valid_amount);
        }
        g h = FisApplication.k().h();
        if (i.findViewById(C0000R.id.account_type_layout).getVisibility() == 0 && h.f() && TextUtils.isEmpty(this.Y.getText())) {
            return a((int) C0000R.string.please_select_acc_type);
        }
        if (i.findViewById(C0000R.id.service_category_code_layout).getVisibility() == 0 && h.g() && TextUtils.isEmpty(this.ab.getText())) {
            return a((int) C0000R.string.please_select_service_category);
        }
        if (i.findViewById(C0000R.id.reimbursement_method_layout).getVisibility() == 0 && h.e() && (TextUtils.isEmpty(this.aa.getText()) || this.aa.getText().toString().equalsIgnoreCase("None"))) {
            return a((int) C0000R.string.please_select_reimb_method);
        }
        if (i.findViewById(C0000R.id.comments_editor_layout).getVisibility() == 0 && h.h() && TextUtils.isEmpty(this.X.getText().toString().trim())) {
            return a((int) C0000R.string.please_add_comment);
        }
        if (TextUtils.isEmpty(this.Z.getText())) {
            return a((int) C0000R.string.please_select_claimant);
        }
        return null;
    }

    private String N() {
        String obj = this.V.getText().toString();
        String obj2 = this.T.getText().toString();
        String a = a((int) C0000R.string.claim_service_start_error_date_message, this.V.getText());
        String a2 = a((int) C0000R.string.claim_service_end_error_date_message, this.T.getText());
        if (TextUtils.isEmpty(obj)) {
            return a;
        }
        if (TextUtils.isEmpty(obj2)) {
            return a2;
        }
        Calendar h = ag.h(obj);
        try {
            Calendar h2 = ag.h(obj2);
            if (h == null) {
                return a;
            }
            if (h2 == null) {
                return a2;
            }
            if (h.getTimeInMillis() > h2.getTimeInMillis()) {
                return a2;
            }
            return null;
        } catch (Exception e) {
        }
    }

    private String a(com.avidia.b.w wVar) {
        String a = wVar.a();
        String b = wVar.b();
        if (TextUtils.isEmpty(a) || TextUtils.isEmpty(b)) {
            return TextUtils.isEmpty(a) ? !TextUtils.isEmpty(b) ? b : "" : a;
        }
        return String.format("%s (%s)", a, b);
    }

    /* access modifiers changed from: private */
    public void a(View view) {
        Button button;
        this.V.setOnClickListener(new t(this));
        this.T.setOnClickListener(new u(this));
        this.W.setOnFocusChangeListener(new v(this));
        a(this.Q);
        if (FisApplication.e()) {
            view.findViewById(C0000R.id.btn_header_left).setVisibility(8);
            button = (Button) view.findViewById(C0000R.id.btn_header_right);
            button.setText((int) C0000R.string.preview);
        } else {
            button = (Button) view.findViewById(C0000R.id.btn_preview);
            button.setVisibility(0);
        }
        button.setOnClickListener(new w(this));
        a(this.P, view);
        L();
    }

    private void a(View view, g gVar) {
        boolean a = gVar.a();
        this.aa.setVisibility(j.a(a));
        view.findViewById(C0000R.id.reimbursement_method_layout).setVisibility(j.a(a));
        boolean b = gVar.b();
        this.Y.setVisibility(j.a(b));
        view.findViewById(C0000R.id.account_type_layout).setVisibility(j.a(b));
        boolean c = gVar.c();
        this.ab.setVisibility(j.a(c));
        view.findViewById(C0000R.id.service_category_code_layout).setVisibility(j.a(c));
        boolean d = gVar.d();
        this.X.setVisibility(j.a(d));
        view.findViewById(C0000R.id.comments_editor_layout).setVisibility(j.a(d));
        String a2 = a((int) C0000R.string.reimbursement_method);
        String str = gVar.e() ? "* " + a2 : a2;
        String a3 = a((int) C0000R.string.account_type);
        String str2 = gVar.f() ? "* " + a3 : a3;
        String a4 = a((int) C0000R.string.service_category_code);
        String str3 = gVar.g() ? "* " + a4 : a4;
        String a5 = a((int) C0000R.string.comments);
        String str4 = gVar.h() ? "* " + a5 : a5;
        ((TextView) view.findViewById(C0000R.id.reimbursement_method_caption)).setText(str);
        ((TextView) view.findViewById(C0000R.id.account_type_caption)).setText(str2);
        ((TextView) view.findViewById(C0000R.id.service_category_code_caption)).setText(str3);
        ((TextView) view.findViewById(C0000R.id.comments_editor_text)).setText(str4);
    }

    private void a(e eVar, View view) {
        g h = FisApplication.k().h();
        if (h == null) {
            ag.b(ao, "Can't find the template");
            d((int) C0000R.string.server_error);
            return;
        }
        a(view, h);
        this.V.setText(ag.f(eVar.e()));
        if (eVar.f()) {
            this.T.setText(eVar.g());
        } else {
            this.T.setText(ag.f(eVar.g()));
        }
        List i = h.i();
        this.Z.setText(i.size() > 0 ? ((h) i.get(0)).b() : "");
        List j = h.j();
        this.aa.setText(j.size() > 0 ? (String) j.get(0) : "");
        List k = h.k();
        String string = b().getString("ACCOUNT_TYPE");
        this.Y.setText(k.size() > 0 ? (String) k.get((TextUtils.isEmpty(string) || !k.contains(string)) ? 0 : k.indexOf(string)) : "");
        List l = h.l();
        this.ab.setText(l.size() > 0 ? a((com.avidia.b.w) l.get(0)) : "");
        this.W.setText(ag.a(Double.valueOf(eVar.h())));
        this.X.setText(Html.fromHtml(eVar.i()));
        this.ac = new x(this, I(), this.Z);
        this.Z.setOnClickListener(new y(this, i));
        this.ad = new com.avidia.fismobile.w(I(), this.Y);
        this.Y.setOnClickListener(new z(this, k));
        this.ae = new com.avidia.fismobile.w(I(), this.aa);
        this.aa.setOnClickListener(new aa(this, j));
        this.af = new q(this, I(), this.ab);
        this.ab.setOnClickListener(new r(this, l));
    }

    /* access modifiers changed from: private */
    public void a(com.avidia.fismobile.w wVar, List list, int i) {
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(a((com.avidia.b.w) it.next()));
        }
        j.a(c(), wVar, arrayList, i);
    }

    /* access modifiers changed from: private */
    public void a(Boolean bool) {
        String obj = this.W.getText().toString();
        double b = j.b(obj);
        if (bool.booleanValue() && b == 0.0d) {
            this.W.setText("");
        } else if (bool.booleanValue()) {
            this.W.setText(j.a(obj));
        } else if (b == 0.0d || obj.equals(".") || TextUtils.isEmpty(obj)) {
            this.W.setText(ag.a((Number) 0));
        } else {
            String replaceAll = obj.replaceAll("\\$", "").replaceAll(",", "");
            String e = ag.e(replaceAll);
            if (!e.equalsIgnoreCase(replaceAll) || e.contains("\\$")) {
                this.W.setText(e);
            } else {
                this.W.setText("$" + e);
            }
        }
    }

    private void a(List list) {
        if (this.ai >= 1 && this.ai <= 3) {
            ae aeVar = (ae) I();
            if (this.ai == 2 || this.ai == 3 || (this.ai == 1 && list.size() == 0)) {
                this.ah.a(getClass(), aeVar, aeVar.getLayoutInflater(), aeVar.getWindowManager().getDefaultDisplay(), new s(this), list);
                return;
            }
            this.ah.a(getClass(), aeVar, aeVar.getLayoutInflater(), aeVar.getWindowManager().getDefaultDisplay(), (View.OnClickListener) null, list);
        }
    }

    /* access modifiers changed from: private */
    public void a(boolean z, Calendar calendar) {
        if (this.S != null) {
            this.S.dismiss();
        }
        if (calendar == null) {
            calendar = Calendar.getInstance();
        }
        this.S = new ad(this, I(), calendar.get(1), calendar.get(2), calendar.get(5), z);
        this.S.show();
    }

    private boolean a(CharSequence charSequence) {
        if (TextUtils.isEmpty(charSequence)) {
            return false;
        }
        try {
            double b = j.b(charSequence.toString());
            return b >= 0.01d && b < 2.147483647E9d;
        } catch (Exception e) {
            return false;
        }
    }

    /* access modifiers changed from: private */
    public String b(View view) {
        this.P.a(this.V.getText().toString());
        this.P.b(this.T.getText().toString());
        this.P.a(j.b(this.W.getText().toString()));
        this.P.k();
        if (view.findViewById(C0000R.id.comments_editor_layout).getVisibility() == 0) {
            this.P.d(this.X.getText().toString().trim());
        }
        if (view.findViewById(C0000R.id.reimbursement_method_layout).getVisibility() == 0) {
            this.P.c(this.aa.getText().toString());
        }
        if (view.findViewById(C0000R.id.account_type_layout).getVisibility() == 0) {
            this.P.e(this.Y.getText().toString());
        }
        g h = FisApplication.k().h();
        List l = h.l();
        if (view.findViewById(C0000R.id.service_category_code_layout).getVisibility() != 0 || this.an < 0 || l.isEmpty() || l.size() <= this.an) {
            this.P.a((com.avidia.b.w) null);
        } else {
            this.P.a((com.avidia.b.w) l.get(this.an));
        }
        if (this.am < 0 || h.i().size() <= this.am) {
            this.P.a((h) null);
        } else {
            this.P.a((h) h.i().get(this.am));
        }
        this.P.a(this.Q);
        return this.P.a(true);
    }

    /* access modifiers changed from: private */
    public void b(com.avidia.fismobile.w wVar, List list, int i) {
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(((h) it.next()).b());
        }
        j.a(c(), wVar, arrayList, i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avidia.fismobile.view.ac.<init>(com.avidia.fismobile.view.o, android.widget.TextView, int, boolean):void
     arg types: [com.avidia.fismobile.view.o, android.widget.TextView, ?, int]
     candidates:
      com.avidia.fismobile.view.ac.<init>(com.avidia.fismobile.view.o, android.widget.TextView, int, com.avidia.fismobile.view.p):void
      com.avidia.fismobile.view.ac.<init>(com.avidia.fismobile.view.o, android.widget.TextView, int, boolean):void */
    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate((int) C0000R.layout.claims_add_layout, (ViewGroup) null);
        a(inflate, (int) C0000R.layout.header_with_btns, (int) C0000R.layout.window_caption_tablet);
        a(inflate, (int) C0000R.string.add_claim);
        if (FisApplication.e()) {
            inflate.findViewById(C0000R.id.btn_header_left).setVisibility(8);
            ((Button) inflate.findViewById(C0000R.id.btn_header_right)).setText((int) C0000R.string.preview);
        } else {
            ((Button) inflate.findViewById(C0000R.id.btn_preview)).setVisibility(0);
        }
        this.ai = FisApplication.k().j();
        if (this.P == null) {
            this.P = new e();
        }
        if (this.Q == null) {
            this.Q = new ArrayList();
        }
        this.ah = (MultiLineReceiptsView) inflate.findViewById(C0000R.id.photo_container);
        this.V = (TextView) inflate.findViewById(C0000R.id.start_date);
        this.T = (TextView) inflate.findViewById(C0000R.id.end_date);
        this.X = (EditText) inflate.findViewById(C0000R.id.comments_editor);
        this.W = (EditText) inflate.findViewById(C0000R.id.amount_edit_text);
        this.W.addTextChangedListener(new ab(this, null));
        this.aa = (Button) inflate.findViewById(C0000R.id.reimbursement_method);
        this.Y = (Button) inflate.findViewById(C0000R.id.account_type);
        this.ab = (Button) inflate.findViewById(C0000R.id.service_category_code);
        this.Z = (Button) inflate.findViewById(C0000R.id.claimant);
        this.al = new ac(this, this.V, (int) C0000R.string.claim_service_start_error_date_message, true);
        this.ak = new ac(this, this.T, (int) C0000R.string.claim_service_end_error_date_message, (p) null);
        if (FisApplication.e()) {
            inflate.findViewById(C0000R.id.btn_header_left).setVisibility(8);
        } else {
            inflate.findViewById(C0000R.id.btn_preview).setVisibility(0);
        }
        if (bundle != null && this.ag == null) {
            a(inflate);
        } else if (this.ag == null) {
            E();
        }
        return inflate;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avidia.e.ag.a(android.graphics.Bitmap, boolean):java.lang.String
     arg types: [android.graphics.Bitmap, int]
     candidates:
      com.avidia.e.ag.a(org.json.JSONObject, java.lang.String):double
      com.avidia.e.ag.a(android.graphics.BitmapFactory$Options, int):int
      com.avidia.e.ag.a(android.net.Uri, android.support.v4.app.h):android.graphics.Bitmap
      com.avidia.e.ag.a(android.support.v4.app.h, java.lang.String):android.graphics.Bitmap
      com.avidia.e.ag.a(android.content.Context, java.lang.String):java.io.File
      com.avidia.e.ag.a(java.lang.String, java.lang.String):java.lang.String
      com.avidia.e.ag.a(int, android.content.Context):boolean
      com.avidia.e.ag.a(android.app.Activity, com.avidia.b.v):boolean
      com.avidia.e.ag.a(android.view.MenuItem, android.content.Context):boolean
      com.avidia.e.ag.a(android.graphics.Bitmap, boolean):java.lang.String */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x006e A[Catch:{ Exception -> 0x0055 }] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0093 A[Catch:{ Exception -> 0x0055 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(int r5, android.content.Intent r6) {
        /*
            r4 = this;
            r1 = 1
            java.lang.String r0 = ""
            r2 = 310(0x136, float:4.34E-43)
            if (r5 != r2) goto L_0x0081
            if (r6 == 0) goto L_0x002f
            android.os.Bundle r2 = r6.getExtras()     // Catch:{ Exception -> 0x0055 }
            java.lang.String r3 = "data"
            android.os.Parcelable r2 = r2.getParcelable(r3)     // Catch:{ Exception -> 0x0055 }
            if (r2 == 0) goto L_0x002f
            android.os.Bundle r0 = r6.getExtras()     // Catch:{ Exception -> 0x0055 }
            java.lang.String r2 = "data"
            android.os.Parcelable r0 = r0.getParcelable(r2)     // Catch:{ Exception -> 0x0055 }
            android.graphics.Bitmap r0 = (android.graphics.Bitmap) r0     // Catch:{ Exception -> 0x0055 }
            r2 = 1
            java.lang.String r2 = com.avidia.e.ag.a(r0, r2)     // Catch:{ Exception -> 0x0055 }
            boolean r0 = android.text.TextUtils.isEmpty(r2)     // Catch:{ Exception -> 0x0055 }
            if (r0 != 0) goto L_0x0097
            r0 = 0
            r1 = r0
            r0 = r2
        L_0x002f:
            if (r1 == 0) goto L_0x0068
            java.lang.String r0 = r4.aj     // Catch:{ Exception -> 0x0055 }
            android.net.Uri r0 = android.net.Uri.parse(r0)     // Catch:{ Exception -> 0x0055 }
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x0055 }
            java.net.URI r2 = new java.net.URI     // Catch:{ Exception -> 0x0055 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0055 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x0055 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0055 }
            boolean r0 = r1.isFile()     // Catch:{ Exception -> 0x0055 }
            if (r0 != 0) goto L_0x0064
            java.io.FileNotFoundException r0 = new java.io.FileNotFoundException     // Catch:{ Exception -> 0x0055 }
            java.lang.String r1 = r1.getAbsolutePath()     // Catch:{ Exception -> 0x0055 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0055 }
            throw r0     // Catch:{ Exception -> 0x0055 }
        L_0x0055:
            r0 = move-exception
            java.lang.String r1 = com.avidia.fismobile.view.o.ao
            java.lang.String r2 = "Error while saving the image"
            com.avidia.e.ag.a(r1, r2, r0)
            r4.H()
        L_0x0060:
            r4.J()
            return
        L_0x0064:
            java.lang.String r0 = r1.getAbsolutePath()     // Catch:{ Exception -> 0x0055 }
        L_0x0068:
            boolean r1 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Exception -> 0x0055 }
            if (r1 != 0) goto L_0x0093
            com.avidia.b.u r1 = new com.avidia.b.u     // Catch:{ Exception -> 0x0055 }
            r1.<init>()     // Catch:{ Exception -> 0x0055 }
            r1.d(r0)     // Catch:{ Exception -> 0x0055 }
            java.util.List r0 = r4.Q     // Catch:{ Exception -> 0x0055 }
            r0.add(r1)     // Catch:{ Exception -> 0x0055 }
            java.util.List r0 = r4.Q     // Catch:{ Exception -> 0x0055 }
            r4.a(r0)     // Catch:{ Exception -> 0x0055 }
            goto L_0x0060
        L_0x0081:
            android.net.Uri r0 = r6.getData()     // Catch:{ Exception -> 0x0055 }
            com.avidia.fismobile.view.al r1 = r4.I()     // Catch:{ Exception -> 0x0055 }
            android.graphics.Bitmap r0 = com.avidia.e.ag.a(r0, r1)     // Catch:{ Exception -> 0x0055 }
            r1 = 1
            java.lang.String r0 = com.avidia.e.ag.a(r0, r1)     // Catch:{ Exception -> 0x0055 }
            goto L_0x0068
        L_0x0093:
            r4.H()     // Catch:{ Exception -> 0x0055 }
            goto L_0x0060
        L_0x0097:
            r0 = r2
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avidia.fismobile.view.o.a(int, android.content.Intent):void");
    }

    public void a(int i, String str) {
        if (i >= 0) {
            try {
                if (i < this.Q.size()) {
                    ((u) this.Q.get(i)).d(str);
                }
            } catch (Exception e) {
                ag.b(ao, "Error while deleting message");
                H();
            }
        }
        if (I() != null) {
            a(this.Q);
        }
    }

    public void a(v vVar) {
        ((ae) I()).runOnUiThread(new p(this, vVar));
    }

    public void a_() {
        if (this.ag == null && i() != null) {
            a(this.Q);
        }
    }

    public void b(int i) {
        try {
            this.Q.remove(i);
        } catch (Exception e) {
            H();
        }
        if (I() != null) {
            a(this.Q);
        }
    }

    public void n() {
        try {
            b().putString("FORM_JSON", G());
        } catch (JSONException e) {
            ag.a(ao, "Error while parsing the form into json", e);
        }
        super.n();
    }

    public void o() {
        D();
        super.o();
    }

    public void q() {
        D();
        super.q();
    }
}
