package com.avidia.fismobile.view;

import com.avidia.fismobile.C0000R;
import com.avidia.fismobile.FisApplication;

public class bo {
    public static int a() {
        return FisApplication.e() ? C0000R.layout.activity_phone_layout : C0000R.layout.activity_tablet_layout;
    }

    public static int b() {
        return FisApplication.e() ? C0000R.id.frame_left : C0000R.id.frame_right;
    }

    public static int c() {
        return C0000R.id.frame_left;
    }

    public static int d() {
        return FisApplication.e() ? C0000R.layout.signin : C0000R.layout.signin_tablet;
    }

    public static int e() {
        return FisApplication.e() ? C0000R.layout.signin_key : C0000R.layout.signin_key_tablet;
    }

    public static int f() {
        return FisApplication.e() ? C0000R.layout.signin_question : C0000R.layout.signin_question_tablet;
    }
}
