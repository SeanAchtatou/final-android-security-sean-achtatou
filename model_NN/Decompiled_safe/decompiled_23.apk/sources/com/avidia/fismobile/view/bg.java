package com.avidia.fismobile.view;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.avidia.a.a;
import com.avidia.b.e;
import com.avidia.b.g;
import com.avidia.b.l;
import com.avidia.b.o;
import com.avidia.e.ag;
import com.avidia.fismobile.C0000R;
import com.avidia.fismobile.FisApplication;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class bg extends LinearLayout {
    private static final String b = bg.class.getSimpleName();
    private LayoutInflater a;

    public bg(Context context, LayoutInflater layoutInflater) {
        super(context);
        this.a = layoutInflater;
        setOrientation(1);
        setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
    }

    private static List a(String str, Context context) {
        e eVar;
        e eVar2 = new e();
        try {
            eVar2.f(str);
            eVar = eVar2;
        } catch (Exception e) {
            ag.b(b, "Error while parsing claim");
            eVar = null;
        }
        if (eVar == null) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList();
        g h = FisApplication.k().h();
        if (h == null) {
            return arrayList;
        }
        List<l> l = eVar.l();
        if (l == null) {
            a(context, arrayList, C0000R.string.transaction_date, ag.f(eVar.j()));
            a(context, arrayList, C0000R.string.service_start_date, ag.f(eVar.e()));
            if (!eVar.f()) {
                a(context, arrayList, C0000R.string.service_end_date, ag.f(eVar.g()));
            }
            if (h.a()) {
                a(context, arrayList, C0000R.string.reimbursement_method, eVar.b());
            }
            if (h.b()) {
                a(context, arrayList, C0000R.string.account_type, eVar.d());
            }
            if (h.c()) {
                a(context, arrayList, C0000R.string.service_category_code, eVar.n());
            }
            if (h.b("Type")) {
                a(context, arrayList, C0000R.string.type, eVar.a());
            }
            a(context, arrayList, C0000R.string.claimant, eVar.c().b());
            a(context, arrayList, C0000R.string.amount, ag.a(Double.valueOf(eVar.h())));
            if (h.d()) {
                a(context, arrayList, C0000R.string.comments, eVar.i());
            }
        } else {
            for (l lVar : l) {
                if (!"AccountDisplayHeader".equalsIgnoreCase(lVar.d()) && lVar.b() != 0) {
                    String d = TextUtils.isEmpty(lVar.c()) ? lVar.d() : lVar.c();
                    String g = eVar.g(lVar.d());
                    if (!TextUtils.isEmpty(g) && !"null".equalsIgnoreCase(g)) {
                        if ("TransactionDate".equalsIgnoreCase(lVar.d())) {
                            d = context.getString(C0000R.string.transaction_date);
                        }
                        a(arrayList, d, g);
                    }
                }
            }
        }
        return arrayList;
    }

    private static void a(Context context, List list, int i, String str) {
        a(list, context.getString(i), str);
    }

    private static void a(List list, String str, String str2) {
        if (!TextUtils.isEmpty(str2)) {
            try {
                list.add(new o(str, str2));
            } catch (Exception e) {
                ag.b(b, "Error while parsing key-value pair");
            }
        }
    }

    public void a(String str) {
        View view;
        for (o oVar : a(str, getContext())) {
            if (ag.k(oVar.b)) {
                View inflate = this.a.inflate((int) C0000R.layout.message_item, (ViewGroup) null);
                WebView webView = (WebView) inflate.findViewById(C0000R.id.message_html);
                webView.setBackgroundColor(0);
                String str2 = oVar.b;
                if (a.e) {
                    Log.d("AlertDetailsAdapter", "HtmlData:" + str2);
                }
                webView.loadDataWithBaseURL(null, str2, "text/html", null, null);
                view = inflate;
            } else {
                View inflate2 = this.a.inflate((int) C0000R.layout.key_value_item, (ViewGroup) null);
                ((TextView) inflate2.findViewById(C0000R.id.textview_key)).setText(oVar.a);
                ((TextView) inflate2.findViewById(C0000R.id.textview_value)).setText(oVar.b);
                view = inflate2;
            }
            ImageView imageView = new ImageView(getContext());
            imageView.setBackgroundResource(C0000R.drawable.listview_divider);
            addView(view);
            addView(imageView, -1, -2);
        }
    }
}
