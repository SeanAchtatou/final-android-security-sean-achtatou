package com.avidia.fismobile.view;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.avidia.fismobile.C0000R;
import com.avidia.fismobile.FisApplication;

public class bh extends aj {
    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate((int) C0000R.layout.clear_saved_id, (ViewGroup) null);
        a(inflate, (int) C0000R.layout.header_with_btns, (int) C0000R.layout.header_with_btns_tablet);
        a(inflate, (int) C0000R.string.clear_saved_id);
        Button button = (Button) inflate.findViewById(C0000R.id.btn_header_left);
        button.setText((int) C0000R.string.accounts_btn_signout);
        button.setOnClickListener(new bi(this));
        a((int) C0000R.id.btn_header_left_layout, button, inflate);
        inflate.findViewById(C0000R.id.btn_header_right).setVisibility(4);
        ((TextView) inflate.findViewById(C0000R.id.clear_message_tv)).setText(Html.fromHtml(a((int) C0000R.string.clear_message)));
        inflate.findViewById(C0000R.id.btn_clearsavedid).setOnClickListener(new bj(this));
        Button button2 = (Button) inflate.findViewById(C0000R.id.btn_cancel);
        if (FisApplication.e()) {
            button2.setOnClickListener(new bk(this));
        } else {
            button2.setVisibility(8);
        }
        return inflate;
    }

    /* access modifiers changed from: protected */
    public void a(int i, Button button, View view) {
        view.findViewById(i).setOnClickListener(new bl(this, button));
    }
}
