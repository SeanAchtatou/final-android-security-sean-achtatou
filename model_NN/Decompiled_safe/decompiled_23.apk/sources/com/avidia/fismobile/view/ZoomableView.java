package com.avidia.fismobile.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.widget.FrameLayout;
import com.avidia.fismobile.C0000R;

public class ZoomableView extends FrameLayout implements ScaleGestureDetector.OnScaleGestureListener {
    /* access modifiers changed from: private */
    public ImageViewTouchBase a;
    private GestureDetector b;
    private ScaleGestureDetector c;

    public ZoomableView(Context context) {
        super(context);
        a(context);
    }

    public ZoomableView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public ZoomableView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context);
    }

    private void a(Context context) {
        this.b = new GestureDetector(context, new cv(this));
        this.c = new ScaleGestureDetector(context, this);
    }

    public boolean a() {
        return (this.a == null || this.a.getDrawable() == null) ? false : true;
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        this.a = (ImageViewTouchBase) findViewById(C0000R.id.photo_image_view);
    }

    public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
        float f = 1.0f;
        ImageViewTouchBase imageViewTouchBase = this.a;
        float scaleFactor = scaleGestureDetector.getScaleFactor() * imageViewTouchBase.getScale();
        if (scaleFactor >= 1.0f) {
            f = scaleFactor > 8.0f ? 8.0f : scaleFactor;
        }
        imageViewTouchBase.a(f, scaleGestureDetector.getFocusX(), scaleGestureDetector.getFocusY(), 50.0f);
        return true;
    }

    public boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector) {
        return true;
    }

    public void onScaleEnd(ScaleGestureDetector scaleGestureDetector) {
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        this.c.onTouchEvent(motionEvent);
        if (this.c.isInProgress()) {
            return true;
        }
        this.b.onTouchEvent(motionEvent);
        return true;
    }
}
