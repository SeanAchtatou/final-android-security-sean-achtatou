package com.avidia.fismobile.view;

import android.widget.ListAdapter;
import android.widget.ListView;
import com.avidia.b.v;
import com.avidia.e.ag;
import com.avidia.e.g;
import com.avidia.fismobile.C0000R;
import com.avidia.fismobile.m;
import java.util.List;

class bf implements Runnable {
    final /* synthetic */ v a;
    final /* synthetic */ List b;
    final /* synthetic */ List c;
    final /* synthetic */ bb d;

    bf(bb bbVar, v vVar, List list, List list2) {
        this.d = bbVar;
        this.a = vVar;
        this.b = list;
        this.c = list2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avidia.fismobile.view.bb.a(com.avidia.fismobile.view.bb, boolean):void
     arg types: [com.avidia.fismobile.view.bb, int]
     candidates:
      com.avidia.fismobile.view.bb.a(com.avidia.fismobile.view.bb, com.avidia.e.g):com.avidia.e.g
      com.avidia.fismobile.view.bb.a(com.avidia.fismobile.view.bb, com.avidia.fismobile.m):com.avidia.fismobile.m
      com.avidia.fismobile.view.bb.a(com.avidia.fismobile.m, int):void
      com.avidia.fismobile.view.bb.a(com.avidia.fismobile.view.bb, java.util.List):void
      com.avidia.fismobile.view.bb.a(com.avidia.b.e, java.lang.String):void
      com.avidia.fismobile.view.aj.a(android.view.View, int):void
      android.support.v4.app.Fragment.a(android.content.Context, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.Fragment.a(int, java.lang.Object[]):java.lang.String
      android.support.v4.app.Fragment.a(int, android.support.v4.app.Fragment):void
      android.support.v4.app.Fragment.a(android.view.Menu, android.view.MenuInflater):void
      android.support.v4.app.Fragment.a(android.view.View, android.os.Bundle):void
      com.avidia.fismobile.view.bb.a(com.avidia.fismobile.view.bb, boolean):void */
    public void run() {
        g unused = this.d.P = (g) null;
        this.d.J();
        if (ag.a(this.d.c(), this.a) || this.d.i() == null) {
            this.d.J();
            return;
        }
        if (this.a.a) {
            this.d.b().putString("claimsJson", this.a.b);
            this.d.a(this.b);
            ListView listView = (ListView) this.d.i().findViewById(C0000R.id.claims_listview);
            int c2 = this.d.Q != null ? this.d.Q.c() : -1;
            m unused2 = this.d.Q = new m(this.d, this.b, this.c);
            listView.setAdapter((ListAdapter) this.d.Q);
            if (c2 == -1) {
                this.d.a(this.d.Q);
            } else {
                this.d.a(this.d.Q, c2);
            }
            this.d.Q.notifyDataSetChanged();
            this.d.c(true);
        } else {
            this.d.b(ag.b(this.a));
        }
        this.d.J();
    }
}
