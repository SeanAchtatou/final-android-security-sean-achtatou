package com.avidia.fismobile.view;

import android.graphics.Bitmap;

class bt implements Runnable {
    final /* synthetic */ Bitmap a;
    final /* synthetic */ boolean b;
    final /* synthetic */ ImageViewTouchBase c;

    bt(ImageViewTouchBase imageViewTouchBase, Bitmap bitmap, boolean z) {
        this.c = imageViewTouchBase;
        this.a = bitmap;
        this.b = z;
    }

    public void run() {
        this.c.b(this.a, this.b);
    }
}
