package com.avidia.fismobile.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.TextView;
import com.avidia.b.u;
import com.avidia.b.v;
import com.avidia.e.ag;
import com.avidia.fismobile.C0000R;
import java.io.File;
import org.json.JSONException;
import org.json.JSONObject;

public class cq extends aj {
    private static int W = Integer.MAX_VALUE;
    private int P = W;
    private Bitmap Q;
    private ImageViewTouchBase R;
    private TextView S;
    private u T;
    private boolean V = false;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avidia.fismobile.view.ImageViewTouchBase.a(android.graphics.Bitmap, boolean):void
     arg types: [android.graphics.Bitmap, int]
     candidates:
      com.avidia.fismobile.view.ImageViewTouchBase.a(android.graphics.Bitmap, android.graphics.Matrix):void
      com.avidia.fismobile.view.ImageViewTouchBase.a(android.graphics.Matrix, int):float
      com.avidia.fismobile.view.ImageViewTouchBase.a(float, float):void
      com.avidia.fismobile.view.ImageViewTouchBase.a(boolean, boolean):void
      com.avidia.fismobile.view.ImageViewTouchBase.a(android.graphics.Bitmap, boolean):void */
    /* access modifiers changed from: package-private */
    public void C() {
        if (this.P != W) {
            this.V = true;
            ae aeVar = (ae) I();
            if (aeVar != null) {
                aeVar.d(this.P);
                return;
            }
            return;
        }
        try {
            this.Q = ag.l(this.T.d());
            if (this.Q == null) {
                this.R.setVisibility(8);
                this.S.setVisibility(0);
            } else {
                this.R.a(this.Q, true);
                this.S.setVisibility(8);
                this.R.setVisibility(0);
            }
        } catch (Exception e) {
            ag.b(this.U, "ClaimsDetailsFragment: Can not get sender fragment");
        }
        if (this.Q == null) {
            this.R.setVisibility(8);
            this.S.setVisibility(0);
            return;
        }
        this.R.a(this.Q, true);
        this.S.setVisibility(8);
        this.R.setVisibility(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avidia.fismobile.view.ImageViewTouchBase.a(android.graphics.Bitmap, boolean):void
     arg types: [android.graphics.Bitmap, int]
     candidates:
      com.avidia.fismobile.view.ImageViewTouchBase.a(android.graphics.Bitmap, android.graphics.Matrix):void
      com.avidia.fismobile.view.ImageViewTouchBase.a(android.graphics.Matrix, int):float
      com.avidia.fismobile.view.ImageViewTouchBase.a(float, float):void
      com.avidia.fismobile.view.ImageViewTouchBase.a(boolean, boolean):void
      com.avidia.fismobile.view.ImageViewTouchBase.a(android.graphics.Bitmap, boolean):void */
    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate((int) C0000R.layout.photo_view_layout, (ViewGroup) null);
        a(inflate, (int) C0000R.layout.window_caption, (int) C0000R.layout.window_caption_tablet);
        a(inflate, (int) C0000R.string.receipt);
        this.S = (TextView) inflate.findViewById(C0000R.id.error_message);
        this.P = W;
        if (b().containsKey("filekey")) {
            this.P = b().getInt("filekey");
        } else if (b().containsKey("RECEIPT")) {
            try {
                this.T = new u();
                this.T.b(b().getString("RECEIPT"));
            } catch (JSONException e) {
                ag.b(this.U, "Error while parsing the receipt");
                this.R.setVisibility(8);
                this.S.setVisibility(0);
                return inflate;
            }
        }
        this.R = (ImageViewTouchBase) inflate.findViewById(C0000R.id.photo_image_view);
        this.Q = (Bitmap) b().getParcelable("Bitmap");
        if (this.Q != null) {
            this.R.a(this.Q, true);
        }
        if (bundle == null && !this.V) {
            C();
        }
        return inflate;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avidia.fismobile.view.ImageViewTouchBase.a(android.graphics.Bitmap, boolean):void
     arg types: [android.graphics.Bitmap, int]
     candidates:
      com.avidia.fismobile.view.ImageViewTouchBase.a(android.graphics.Bitmap, android.graphics.Matrix):void
      com.avidia.fismobile.view.ImageViewTouchBase.a(android.graphics.Matrix, int):float
      com.avidia.fismobile.view.ImageViewTouchBase.a(float, float):void
      com.avidia.fismobile.view.ImageViewTouchBase.a(boolean, boolean):void
      com.avidia.fismobile.view.ImageViewTouchBase.a(android.graphics.Bitmap, boolean):void */
    public void a(v vVar) {
        if (!(vVar.d instanceof Integer) || ((Integer) vVar.d).intValue() != this.P) {
            ((ae) I()).s();
            return;
        }
        try {
            JSONObject jSONObject = new JSONObject(vVar.b);
            String string = jSONObject.getString("Base64");
            switch (jSONObject.optInt("FileFormat", -1)) {
                default:
                    File d = ag.d(string, jSONObject.getString("FileName"));
                    Intent intent = new Intent();
                    intent.setAction("android.intent.action.VIEW");
                    intent.setDataAndType(Uri.fromFile(d), MimeTypeMap.getSingleton().getMimeTypeFromExtension(d.getName().substring(d.getName().indexOf(".") + 1)));
                    ((ae) I()).startActivityForResult(intent, 811);
                    ((ae) I()).s();
                    return;
                case 64:
                case 128:
                case 256:
                case 4096:
                    Bitmap a = ag.a(c(), string);
                    if (a == null) {
                        this.R.setVisibility(8);
                        this.S.setVisibility(0);
                        return;
                    }
                    this.R.a(a, true);
                    this.S.setVisibility(8);
                    this.R.setVisibility(0);
                    return;
            }
        } catch (Exception e) {
            ag.b(this.U, "Error while extracting the image");
            this.R.setVisibility(8);
            this.S.setVisibility(0);
            this.Q = null;
        }
        ag.b(this.U, "Error while extracting the image");
        this.R.setVisibility(8);
        this.S.setVisibility(0);
        this.Q = null;
    }
}
