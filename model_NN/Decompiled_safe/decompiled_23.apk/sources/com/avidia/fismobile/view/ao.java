package com.avidia.fismobile.view;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.avidia.b.t;
import com.avidia.b.v;
import com.avidia.e.ac;
import com.avidia.e.ag;
import com.avidia.e.c;
import com.avidia.e.d;
import com.avidia.e.q;
import com.avidia.e.w;
import com.avidia.fismobile.C0000R;
import com.avidia.fismobile.an;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URI;
import org.json.JSONObject;

public class ao extends ai implements w {
    /* access modifiers changed from: private */
    public String V;
    private Dialog W;

    /* access modifiers changed from: private */
    public void L() {
        if (this.W != null) {
            this.W.dismiss();
        }
        this.P = ag.e();
        this.W = ac.a(I(), 170, this.P, 160);
        this.W.show();
    }

    private void c(String str) {
        q cVar;
        if (this.Q == null) {
            H();
            J();
            return;
        }
        int p = this.Q.p();
        String jSONObject = t.c(str).toString();
        if (TextUtils.isEmpty(jSONObject)) {
            H();
            J();
            return;
        }
        an f = ((ae) I()).f();
        if (p < 0) {
            JSONObject jSONObject2 = new JSONObject(this.V);
            String optString = jSONObject2.optString("SeqNumber");
            if (TextUtils.isEmpty(optString)) {
                optString = jSONObject2.optString("ExternalSeqNumber");
            }
            String optString2 = jSONObject2.optString("TransactionId");
            String optString3 = jSONObject2.optString("SettlementDate");
            cVar = (TextUtils.isEmpty(optString) || TextUtils.isEmpty(optString2) || TextUtils.isEmpty(optString3)) ? null : new d(optString, optString2, optString3, jSONObject);
        } else {
            cVar = new c(p, jSONObject);
        }
        if (cVar == null) {
            if (f != null) {
                f.B();
            }
            H();
            J();
        } else if (f != null) {
            f.B();
            K();
            f.a(cVar, this);
        } else {
            ag.b(this.T, "Can not get sender fragment");
            J();
        }
    }

    /* access modifiers changed from: protected */
    public int D() {
        return C0000R.string.claim_details;
    }

    /* access modifiers changed from: protected */
    public int E() {
        return C0000R.layout.claim_details_activity;
    }

    /* access modifiers changed from: protected */
    public View.OnClickListener F() {
        return new ap(this);
    }

    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View a = super.a(layoutInflater, viewGroup, bundle);
        this.V = b().getString("CLAIM_ORIGINAL").trim();
        return a;
    }

    /* access modifiers changed from: package-private */
    public void a(int i, Intent intent) {
        if (i == 170 || i == 160) {
            Bitmap bitmap = null;
            if (i == 170) {
                boolean z = true;
                if (intent != null) {
                    try {
                        if (intent.getExtras().getParcelable("data") != null) {
                            Bitmap a = ag.a((Bitmap) intent.getExtras().getParcelable("data"));
                            if (a != null) {
                                z = false;
                                bitmap = a;
                            } else {
                                bitmap = a;
                            }
                        }
                    } catch (Exception e) {
                        H();
                        J();
                        return;
                    }
                }
                if (z) {
                    Uri parse = Uri.parse(this.P);
                    File file = new File(new URI(parse.toString()));
                    if (!file.isFile()) {
                        throw new FileNotFoundException(file.getAbsolutePath());
                    }
                    bitmap = ag.a(parse, I());
                }
            } else {
                bitmap = ag.a(intent.getData(), I());
            }
            String b = ag.b(bitmap);
            if (TextUtils.isEmpty(b)) {
                H();
            } else {
                c(b);
            }
        } else {
            J();
        }
    }

    /* access modifiers changed from: protected */
    public void a(View view) {
    }

    public void a(v vVar) {
        ((ae) I()).runOnUiThread(new aq(this, vVar));
    }
}
