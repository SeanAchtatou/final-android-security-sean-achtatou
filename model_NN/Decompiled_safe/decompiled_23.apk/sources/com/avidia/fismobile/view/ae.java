package com.avidia.fismobile.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import com.avidia.e.ag;
import com.avidia.e.o;
import com.avidia.fismobile.an;

public abstract class ae extends al implements bs {
    private ag o = new ag(this);

    private Fragment a(aj ajVar, String str, String str2) {
        if (ajVar != null && ajVar.g()) {
            String string = ajVar.b().getString("sender");
            if (cs.class.getSimpleName().equals(string)) {
                str2 = "TRANSACTION_DETAILS_FRAGMENT";
            } else if (o.class.getSimpleName().equals(string)) {
                str2 = "ADD_CLAIM_FRAGMENT";
            } else if (ar.class.getSimpleName().equals(string)) {
                str2 = "CLAIM_PREVIEW_FRAGMENT";
            } else if (ay.class.getSimpleName().equals(string)) {
                str2 = "CLAIM_SUCCESS_FRAGMENT";
            } else if (ao.class.getSimpleName().equals(string)) {
                str2 = "CLAIM_DETAILS_FRAGMENT";
            }
        }
        return a(str, str2);
    }

    private o r() {
        try {
            return (o) i("ADD_CLAIM_FRAGMENT");
        } catch (ClassCastException e) {
            o();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public aj a(String str, Bundle bundle, boolean z) {
        return str.equals("RECEIPT_VIEW_ONLY_FRAGMENT") ? a("RECEIPT_VIEW_ONLY_FRAGMENT", cq.class, bundle) : str.equals("ADD_CLAIM_FRAGMENT") ? a("ADD_CLAIM_FRAGMENT", o.class, bundle) : str.equals("RECEIPT_EDIT_FRAGMENT") ? a("RECEIPT_EDIT_FRAGMENT", cl.class, bundle) : str.equals("CLAIM_PREVIEW_FRAGMENT") ? a("CLAIM_PREVIEW_FRAGMENT", ar.class, bundle) : str.equals("CLAIM_SUCCESS_FRAGMENT") ? a("CLAIM_SUCCESS_FRAGMENT", ay.class, bundle) : str.equals("CLAIM_DETAILS_FRAGMENT") ? a("CLAIM_DETAILS_FRAGMENT", ao.class, bundle) : str.equals("CLAIMS_FRAGMENT") ? a("CLAIMS_FRAGMENT", bb.class, bundle) : z ? null : i(str);
    }

    public void a(int i, Class cls) {
        Bundle bundle = new Bundle();
        bundle.putInt("filekey", i);
        bundle.putString("sender", cls.getSimpleName());
        b("RECEIPT_VIEW_ONLY_FRAGMENT", bundle, true);
    }

    public void a(Bundle bundle, Class cls) {
        b("RECEIPT_EDIT_FRAGMENT", bundle, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avidia.fismobile.view.al.a(android.support.v4.app.Fragment, java.lang.String):void
     arg types: [com.avidia.fismobile.view.o, java.lang.String]
     candidates:
      com.avidia.fismobile.view.ae.a(int, java.lang.Class):void
      com.avidia.fismobile.view.ae.a(android.os.Bundle, java.lang.Class):void
      com.avidia.fismobile.view.ae.a(java.lang.String, int):void
      com.avidia.fismobile.view.ae.a(java.lang.String, java.lang.Class):void
      com.avidia.fismobile.view.al.a(com.avidia.fismobile.view.al, java.lang.Class):int
      com.avidia.fismobile.view.al.a(android.view.View, float):void
      com.avidia.fismobile.view.al.a(java.lang.Class, android.view.ViewGroup):void
      com.avidia.fismobile.view.al.a(java.lang.String, java.lang.String):com.avidia.fismobile.view.aj
      com.avidia.fismobile.view.al.a(com.avidia.fismobile.view.aj, java.lang.String):void
      com.avidia.fismobile.view.al.a(java.lang.String, android.os.Bundle):void
      com.avidia.fismobile.h.a(android.content.ComponentName, android.content.Context):boolean
      com.avidia.fismobile.h.a(int, android.widget.Button):void
      com.avidia.fismobile.view.bs.a(java.lang.String, java.lang.Class):void
      com.avidia.fismobile.view.al.a(android.support.v4.app.Fragment, java.lang.String):void */
    public void a(String str, int i) {
        o r = r();
        if (r != null) {
            r.a(i, str);
            a((Fragment) r, "RECEIPT_EDIT_FRAGMENT");
            return;
        }
        ag.b(this.p, "Error while replacing the fragment from RECEIPT_EDIT_FRAGMENT to AddClaimFragment");
    }

    public void a(String str, Class cls) {
        Bundle bundle = new Bundle();
        bundle.putString("RECEIPT", str);
        bundle.putString("sender", cls.getSimpleName());
        b("RECEIPT_VIEW_ONLY_FRAGMENT", bundle, true);
    }

    public void b(Bundle bundle) {
        b("CLAIM_PREVIEW_FRAGMENT", bundle, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avidia.fismobile.view.al.a(android.support.v4.app.Fragment, java.lang.String):void
     arg types: [com.avidia.fismobile.view.o, java.lang.String]
     candidates:
      com.avidia.fismobile.view.ae.a(int, java.lang.Class):void
      com.avidia.fismobile.view.ae.a(android.os.Bundle, java.lang.Class):void
      com.avidia.fismobile.view.ae.a(java.lang.String, int):void
      com.avidia.fismobile.view.ae.a(java.lang.String, java.lang.Class):void
      com.avidia.fismobile.view.al.a(com.avidia.fismobile.view.al, java.lang.Class):int
      com.avidia.fismobile.view.al.a(android.view.View, float):void
      com.avidia.fismobile.view.al.a(java.lang.Class, android.view.ViewGroup):void
      com.avidia.fismobile.view.al.a(java.lang.String, java.lang.String):com.avidia.fismobile.view.aj
      com.avidia.fismobile.view.al.a(com.avidia.fismobile.view.aj, java.lang.String):void
      com.avidia.fismobile.view.al.a(java.lang.String, android.os.Bundle):void
      com.avidia.fismobile.h.a(android.content.ComponentName, android.content.Context):boolean
      com.avidia.fismobile.h.a(int, android.widget.Button):void
      com.avidia.fismobile.view.bs.a(java.lang.String, java.lang.Class):void
      com.avidia.fismobile.view.al.a(android.support.v4.app.Fragment, java.lang.String):void */
    public void c(int i) {
        o oVar = (o) i("ADD_CLAIM_FRAGMENT");
        if (oVar == null) {
            o();
            return;
        }
        oVar.b(i);
        a((Fragment) oVar, "RECEIPT_EDIT_FRAGMENT");
    }

    public void c(Bundle bundle) {
        b("CLAIM_SUCCESS_FRAGMENT", bundle, true);
    }

    public void d(int i) {
        if (i != -1) {
            an f = f();
            if (f != null) {
                f.B();
                l();
                int unused = this.o.b = i;
                f.a(new o(i), this.o);
                return;
            }
            m();
            ag.b(this.p, "ClaimsDetailsFragment: Can not get sender fragment");
        }
    }

    public void d(Bundle bundle) {
        b("CLAIM_DETAILS_FRAGMENT", bundle, true);
    }

    /* access modifiers changed from: protected */
    public Fragment f(String str) {
        String str2 = "RECEIPT_VIEW_ONLY_FRAGMENT";
        aj ajVar = (aj) e().a(str2);
        if (ajVar == null || !ajVar.g()) {
            str2 = "RECEIPT_EDIT_FRAGMENT";
            ajVar = (aj) e().a("RECEIPT_EDIT_FRAGMENT");
        }
        return a(ajVar, str2, str);
    }

    public void g(String str) {
        Bundle bundle = new Bundle();
        if (!TextUtils.isEmpty(str)) {
            bundle.putString("ACCOUNT_TYPE", str);
        }
        b("ADD_CLAIM_FRAGMENT", bundle, true);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 != -1) {
            m();
        } else if (i == 120 || i == 130) {
            aj i3 = i("TRANSACTION_DETAILS_FRAGMENT");
            if (i3 == null || !(i3 instanceof cs)) {
                m();
                ag.b(this.p, "Trying to get TRANSACTION_DETAILS_FRAGMENT: Failed.");
                return;
            }
            ((cs) i3).b(i, i2, intent);
        } else if (i == 140 || i == 150) {
            aj i4 = i("RECEIPT_EDIT_FRAGMENT");
            if (i4 == null || !(i4 instanceof cl)) {
                m();
                ag.b(this.p, "Trying to get RECEIPT_EDIT_FRAGMENT: Failed.");
                return;
            }
            ((cl) i4).a(i, intent);
        } else if (i == 310 || i == 320) {
            o oVar = (o) i("ADD_CLAIM_FRAGMENT");
            if (oVar != null) {
                oVar.a(i, intent);
                return;
            }
            m();
            ag.b(this.p, "Trying to get ADD_CLAIM_FRAGMENT: Failed.");
        } else if (i == 160 || i == 170) {
            ao aoVar = (ao) i("CLAIM_DETAILS_FRAGMENT");
            if (aoVar != null) {
                aoVar.a(i, intent);
                return;
            }
            m();
            ag.b(this.p, "Trying to get ADD_CLAIM_FRAGMENT: Failed.");
        }
    }

    /* access modifiers changed from: package-private */
    public abstract void s();

    /* access modifiers changed from: protected */
    public void u() {
        a("CLAIMS_FRAGMENT", (Bundle) null);
    }
}
