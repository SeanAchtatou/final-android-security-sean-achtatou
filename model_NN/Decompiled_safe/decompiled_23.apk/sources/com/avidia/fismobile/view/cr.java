package com.avidia.fismobile.view;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.a.a.j;
import com.avidia.b.a;
import com.avidia.b.x;
import com.avidia.b.z;
import com.avidia.e.ag;
import com.avidia.fismobile.C0000R;
import com.avidia.fismobile.FisApplication;
import com.avidia.fismobile.bc;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class cr extends aj {
    public static final String P = cr.class.getSimpleName();
    private TextView Q;
    private TextView R;
    private ArrayList S;
    private bc T;
    private String V;
    private x W;
    private j X;
    private ListView Y;
    private View Z;

    private void C() {
        this.V = b().getString("recent_transactions");
        String string = b().getString("short_account_info");
        ag.c(P, "Short account info:" + string);
        this.W = (x) this.X.a(string, x.class);
        this.Q.setText(this.W.a);
        this.R.setText(ag.e(this.W.b));
        c(this.V);
    }

    private void c(String str) {
        ArrayList arrayList = new ArrayList();
        try {
            JSONArray jSONArray = new JSONArray(str);
            for (int i = 0; i < jSONArray.length(); i++) {
                z zVar = new z();
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                zVar.c = ag.e(jSONObject.getString("TransactionAmt"));
                zVar.a = ag.f(jSONObject.getString("TransactionDate"));
                zVar.b = jSONObject.getString("TransactionType");
                zVar.d = jSONObject.optBoolean("HasReceipt", false);
                zVar.e = jSONObject.optBoolean("CanUploadReceipt", false);
                zVar.f = jSONObject.optString("ReceiptsInfo");
                zVar.g = jSONObject.optString("TransactionId");
                zVar.h = jSONObject.optString("SettlementDate");
                zVar.i = jSONObject.optString("SeqNumber");
                arrayList.add(zVar);
            }
            this.Y.setVisibility(0);
            this.Z.setVisibility(8);
        } catch (Throwable th) {
            ag.a(P, "Error while parsing transaction info", th);
        }
        if (arrayList.size() > 0) {
            this.S.clear();
            this.S.addAll(arrayList);
            if (FisApplication.e() || ((AccountsFragmentActivity) I()).h("TRANSACTION_DETAILS_FRAGMENT")) {
                this.T.notifyDataSetChanged();
            } else {
                this.T.b(0);
            }
        } else {
            this.Y.setVisibility(8);
            this.Z.setVisibility(0);
        }
    }

    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate((int) C0000R.layout.recent_transactions, (ViewGroup) null);
        a(inflate, (int) C0000R.layout.window_caption, 0);
        a(inflate, (int) C0000R.string.recent_transactions);
        c((int) C0000R.string.recent_transactions);
        this.Q = (TextView) inflate.findViewById(C0000R.id.account_fullname);
        this.R = (TextView) inflate.findViewById(C0000R.id.account_balance);
        this.Z = inflate.findViewById(C0000R.id.error_message);
        this.S = new ArrayList();
        this.Y = (ListView) inflate.findViewById(C0000R.id.transactions_listview);
        this.T = new bc(this, this.S);
        this.Y.setAdapter((ListAdapter) this.T);
        this.X = new j();
        if (!TextUtils.isEmpty(b().getString("recent_transactions")) || !TextUtils.isEmpty(b().getString("short_account_info"))) {
            C();
        } else {
            a aVar = new a();
            try {
                aVar.a(b().getString("account_info"));
            } catch (Exception e) {
                ag.a(P, "Error while parsing account data", e);
            }
            AccountsFragmentActivity accountsFragmentActivity = (AccountsFragmentActivity) I();
            if (accountsFragmentActivity != null) {
                accountsFragmentActivity.d(aVar);
            }
        }
        return inflate;
    }

    public void b(int i) {
        Bundle bundle = new Bundle();
        bundle.putInt("position", i);
        bundle.putString("transactions_info", this.V);
        bundle.putString("account_info", b().getString("short_account_info"));
        bundle.putString("short_account_info", this.X.a(this.W));
        AccountsFragmentActivity accountsFragmentActivity = (AccountsFragmentActivity) I();
        if (accountsFragmentActivity != null) {
            accountsFragmentActivity.a(bundle);
        }
    }
}
