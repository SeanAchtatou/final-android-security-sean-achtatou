package com.avidia.fismobile.view;

import android.text.Editable;
import android.text.TextWatcher;

class ab implements TextWatcher {
    final /* synthetic */ o a;
    private String b;

    private ab(o oVar) {
        this.a = oVar;
        this.b = null;
    }

    /* synthetic */ ab(o oVar, p pVar) {
        this(oVar);
    }

    public void afterTextChanged(Editable editable) {
        if (this.b != null) {
            editable.replace(0, editable.length(), this.b);
            this.b = null;
        }
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        this.b = charSequence.toString();
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        String[] split = charSequence.toString().split("\\.");
        if (split == null || split.length != 2 || split[1].length() <= 2) {
            this.b = null;
        }
    }
}
