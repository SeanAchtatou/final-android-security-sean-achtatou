package com.avidia.fismobile.view;

import com.avidia.b.v;
import com.avidia.e.ag;

class ah implements Runnable {
    final /* synthetic */ v a;
    final /* synthetic */ ag b;

    ah(ag agVar, v vVar) {
        this.b = agVar;
        this.a = vVar;
    }

    public void run() {
        this.b.a.m();
        if (!ag.a(this.b.a, this.a)) {
            if (!this.a.a || !(this.a.d instanceof Integer) || ((Integer) this.a.d).intValue() != this.b.b) {
                this.b.a.c(ag.b(this.a));
                this.b.a.s();
                return;
            }
            cq cqVar = (cq) this.b.a.i("RECEIPT_VIEW_ONLY_FRAGMENT");
            if (cqVar != null) {
                cqVar.a(this.a);
            }
            System.gc();
        }
    }
}
