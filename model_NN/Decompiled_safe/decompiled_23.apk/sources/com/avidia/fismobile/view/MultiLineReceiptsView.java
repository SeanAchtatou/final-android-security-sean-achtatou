package com.avidia.fismobile.view;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import com.avidia.b.e;
import com.avidia.b.u;
import com.avidia.fismobile.h;
import java.util.Collections;

public class MultiLineReceiptsView extends LinearLayout {
    private static final String a = MultiLineReceiptsView.class.getSimpleName();

    public MultiLineReceiptsView(Context context) {
        super(context);
        setOrientation(1);
    }

    public MultiLineReceiptsView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setOrientation(1);
    }

    /* access modifiers changed from: protected */
    public View.OnClickListener a(Class cls, Activity activity, int i) {
        return new cb(this, activity, i, cls);
    }

    /* access modifiers changed from: protected */
    public View.OnClickListener a(Class cls, h hVar, u uVar, int i) {
        return new ca(this, hVar, cls, uVar, i);
    }

    public void a(Class cls, h hVar, LayoutInflater layoutInflater, Display display, View.OnClickListener onClickListener, e eVar) {
        a(cls, hVar, layoutInflater, display, onClickListener, eVar == null ? Collections.emptyList() : eVar.o());
    }

    /* JADX WARNING: Removed duplicated region for block: B:51:0x0056 A[EDGE_INSN: B:51:0x0056->B:13:0x0056 ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.Class r19, com.avidia.fismobile.h r20, android.view.LayoutInflater r21, android.view.Display r22, android.view.View.OnClickListener r23, java.util.List r24) {
        /*
            r18 = this;
            r18.removeAllViews()
            android.widget.LinearLayout r10 = new android.widget.LinearLayout
            r0 = r20
            r10.<init>(r0)
            android.widget.LinearLayout$LayoutParams r3 = new android.widget.LinearLayout$LayoutParams
            r4 = -1
            r5 = -2
            r3.<init>(r4, r5)
            r10.setLayoutParams(r3)
            r3 = 3
            r10.setGravity(r3)
            r3 = 0
            r10.setOrientation(r3)
            int r3 = r22.getWidth()
            boolean r4 = com.avidia.fismobile.FisApplication.e()
            if (r4 != 0) goto L_0x0034
            r4 = 1065353216(0x3f800000, float:1.0)
            com.avidia.fismobile.FisApplication r5 = com.avidia.fismobile.FisApplication.k()
            float r5 = r5.g()
            float r4 = r4 - r5
            float r3 = (float) r3
            float r3 = r3 * r4
            int r3 = (int) r3
        L_0x0034:
            int r13 = r3 + -10
            r8 = -1
            int r14 = r24.size()
            r5 = 0
            r4 = 0
            boolean r3 = com.avidia.fismobile.FisApplication.e()
            if (r3 == 0) goto L_0x0066
            r3 = 2130903050(0x7f03000a, float:1.7412907E38)
            r6 = r3
        L_0x0047:
            r11 = 0
            r7 = 0
            r3 = r4
            r12 = r5
        L_0x004b:
            int r4 = r14 + 1
            if (r7 >= r4) goto L_0x01a3
            int r9 = r3 + 1
            if (r7 < r14) goto L_0x006b
            if (r23 != 0) goto L_0x006b
            r5 = r11
        L_0x0056:
            r0 = r18
            r0.addView(r10)
            if (r5 == 0) goto L_0x0062
            r0 = r18
            r0.addView(r5)
        L_0x0062:
            r18.invalidate()
            return
        L_0x0066:
            r3 = 2130903051(0x7f03000b, float:1.741291E38)
            r6 = r3
            goto L_0x0047
        L_0x006b:
            android.widget.LinearLayout r15 = new android.widget.LinearLayout
            r0 = r20
            r15.<init>(r0)
            r3 = 0
            r15.setOrientation(r3)
            r3 = 81
            r15.setGravity(r3)
            r3 = 80
            r15.setVerticalGravity(r3)
            android.widget.AbsListView$LayoutParams r3 = new android.widget.AbsListView$LayoutParams
            r4 = -2
            r5 = -2
            r3.<init>(r4, r5)
            r15.setLayoutParams(r3)
            if (r7 < r14) goto L_0x0148
            r3 = 0
            r0 = r21
            android.view.View r5 = r0.inflate(r6, r3)
            r3 = 200300(0x30e6c, float:2.8068E-40)
            r5.setId(r3)
            if (r23 != 0) goto L_0x013a
            r3 = 2131427356(0x7f0b001c, float:1.8476326E38)
            android.view.View r3 = r5.findViewById(r3)
            r4 = 8
            r3.setVisibility(r4)
        L_0x00a7:
            int r3 = r5.getId()
            r4 = 200300(0x30e6c, float:2.8068E-40)
            if (r3 != r4) goto L_0x00cf
            com.avidia.fismobile.FisApplication r3 = com.avidia.fismobile.FisApplication.k()
            int r3 = r3.j()
            r4 = 2
            if (r3 >= r4) goto L_0x00c5
            java.lang.Class<com.avidia.fismobile.view.o> r3 = com.avidia.fismobile.view.o.class
            r0 = r19
            boolean r3 = r0.equals(r3)
            if (r3 == 0) goto L_0x0192
        L_0x00c5:
            r3 = 0
            r5.setVisibility(r3)
        L_0x00c9:
            boolean r3 = com.avidia.fismobile.FisApplication.e()
            if (r3 == 0) goto L_0x0056
        L_0x00cf:
            r3 = 0
            r4 = 0
            r5.measure(r3, r4)
            android.widget.LinearLayout$LayoutParams r3 = new android.widget.LinearLayout$LayoutParams
            int r4 = r5.getMeasuredWidth()
            r16 = -2
            r0 = r16
            r3.<init>(r4, r0)
            r4 = 81
            r3.gravity = r4
            r15.addView(r5, r3)
            r3 = 0
            r4 = 0
            r15.measure(r3, r4)
            int r3 = r5.getMeasuredWidth()
            int r3 = r3 + r12
            if (r3 >= r13) goto L_0x00f6
            if (r8 != r9) goto L_0x0199
        L_0x00f6:
            r3 = -1
            if (r8 != r3) goto L_0x01a0
            r3 = r7
        L_0x00fa:
            r4 = 0
            r0 = r18
            r0.addView(r10)
            android.widget.LinearLayout r8 = new android.widget.LinearLayout
            r0 = r20
            r8.<init>(r0)
            android.widget.LinearLayout$LayoutParams r5 = new android.widget.LinearLayout$LayoutParams
            r9 = -1
            r10 = -2
            r5.<init>(r9, r10)
            r8.setLayoutParams(r5)
            r5 = 0
            r8.setOrientation(r5)
            r5 = 3
            r8.setGravity(r5)
            android.widget.LinearLayout$LayoutParams r5 = new android.widget.LinearLayout$LayoutParams
            int r9 = r15.getMeasuredWidth()
            int r10 = r15.getMeasuredHeight()
            r5.<init>(r9, r10)
            r8.addView(r15, r5)
            int r5 = r15.getMeasuredWidth()
            r17 = r4
            r4 = r5
            r5 = r8
            r8 = r3
            r3 = r17
        L_0x0134:
            int r7 = r7 + 1
            r12 = r4
            r10 = r5
            goto L_0x004b
        L_0x013a:
            r3 = 2131427356(0x7f0b001c, float:1.8476326E38)
            android.view.View r3 = r5.findViewById(r3)
            r0 = r23
            r3.setOnClickListener(r0)
            goto L_0x00a7
        L_0x0148:
            r3 = 2130903091(0x7f030033, float:1.741299E38)
            r4 = 0
            r0 = r21
            android.view.View r5 = r0.inflate(r3, r4)
            r0 = r24
            java.lang.Object r3 = r0.get(r7)
            com.avidia.b.t r3 = (com.avidia.b.t) r3
            r4 = r5
            android.widget.TextView r4 = (android.widget.TextView) r4
            java.lang.String r16 = r3.a()
            java.lang.String r16 = com.avidia.e.ag.f(r16)
            r0 = r16
            r4.setText(r0)
            boolean r4 = r3 instanceof com.avidia.b.u
            if (r4 == 0) goto L_0x017f
            com.avidia.b.u r3 = (com.avidia.b.u) r3
            r0 = r18
            r1 = r19
            r2 = r20
            android.view.View$OnClickListener r3 = r0.a(r1, r2, r3, r7)
            r5.setOnClickListener(r3)
            goto L_0x00a7
        L_0x017f:
            int r3 = r3.b()
            r0 = r18
            r1 = r19
            r2 = r20
            android.view.View$OnClickListener r3 = r0.a(r1, r2, r3)
            r5.setOnClickListener(r3)
            goto L_0x00a7
        L_0x0192:
            r3 = 8
            r5.setVisibility(r3)
            goto L_0x00c9
        L_0x0199:
            r10.addView(r15)
            r4 = r3
            r5 = r10
            r3 = r9
            goto L_0x0134
        L_0x01a0:
            r3 = r8
            goto L_0x00fa
        L_0x01a3:
            r5 = r11
            goto L_0x0056
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avidia.fismobile.view.MultiLineReceiptsView.a(java.lang.Class, com.avidia.fismobile.h, android.view.LayoutInflater, android.view.Display, android.view.View$OnClickListener, java.util.List):void");
    }
}
