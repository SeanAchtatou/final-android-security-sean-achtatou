package com.avidia.fismobile.view;

import android.os.Bundle;
import android.view.View;
import com.avidia.b.u;
import com.avidia.fismobile.h;
import org.json.JSONException;

class ca implements View.OnClickListener {
    final /* synthetic */ h a;
    final /* synthetic */ Class b;
    final /* synthetic */ u c;
    final /* synthetic */ int d;
    final /* synthetic */ MultiLineReceiptsView e;

    ca(MultiLineReceiptsView multiLineReceiptsView, h hVar, Class cls, u uVar, int i) {
        this.e = multiLineReceiptsView;
        this.a = hVar;
        this.b = cls;
        this.c = uVar;
        this.d = i;
    }

    public void onClick(View view) {
        if (this.a instanceof ae) {
            Bundle bundle = new Bundle();
            if (this.b.equals(o.class)) {
                try {
                    bundle.putString("RECEIPT", this.c.c().toString());
                    bundle.putInt("INDEX", this.d);
                    bundle.putString("sender", o.class.getSimpleName());
                    ((ae) this.a).a(bundle, this.b);
                } catch (JSONException e2) {
                    this.a.o();
                }
            } else {
                try {
                    ((bs) this.a).a(this.c.c().toString(), this.b);
                } catch (JSONException e3) {
                    this.a.o();
                }
            }
        }
    }
}
