package com.avidia.fismobile.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.avidia.fismobile.C0000R;

public class bp extends aj {
    public static final String P = bp.class.getSimpleName();

    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate((int) C0000R.layout.about_us, (ViewGroup) null);
        a(inflate, (int) C0000R.layout.window_caption, (int) C0000R.layout.window_caption_tablet);
        a(inflate, b().getInt("HEADER_ID"));
        WebView webView = (WebView) inflate.findViewById(C0000R.id.web_view);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new br(this));
        webView.setBackgroundColor(0);
        webView.loadDataWithBaseURL(null, b().getString("html_text"), "text/html", "UTF-8", null);
        return inflate;
    }
}
