package com.avidia.fismobile.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.avidia.fismobile.C0000R;

public class bv extends aj {
    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate((int) C0000R.layout.moreactions, (ViewGroup) null);
        a(inflate, (int) C0000R.layout.window_caption, 0);
        a(inflate, (int) C0000R.string.more);
        inflate.findViewById(C0000R.id.action_aboutus).setOnClickListener(new bw(this));
        inflate.findViewById(C0000R.id.action_privacy).setOnClickListener(new bx(this));
        inflate.findViewById(C0000R.id.action_clearsavedid).setOnClickListener(new by(this));
        inflate.findViewById(C0000R.id.action_rate_this_app).setOnClickListener(new bz(this));
        return inflate;
    }
}
