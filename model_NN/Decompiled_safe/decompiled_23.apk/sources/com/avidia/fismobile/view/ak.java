package com.avidia.fismobile.view;

import android.view.View;

class ak implements View.OnClickListener {
    final /* synthetic */ View a;
    final /* synthetic */ aj b;

    ak(aj ajVar, View view) {
        this.b = ajVar;
        this.a = view;
    }

    public void onClick(View view) {
        if (this.a.isEnabled() && this.a.getVisibility() == 0 && this.a.isClickable()) {
            this.a.performClick();
        }
    }
}
