package com.avidia.fismobile.view;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.avidia.a.a;
import com.avidia.e.ag;
import com.avidia.fismobile.C0000R;

class br extends WebViewClient {
    final /* synthetic */ bp a;

    private br(bp bpVar) {
        this.a = bpVar;
    }

    private void a(String str) {
        try {
            String str2 = str.split(":")[1];
            if (a.e) {
                Log.d(bp.P, "Sending email to:" + str2);
            }
            Intent intent = new Intent("android.intent.action.SEND");
            intent.setType("text/plain");
            intent.putExtra("android.intent.extra.EMAIL", new String[]{str2});
            intent.putExtra("android.intent.extra.TEXT", new String[]{" "});
            ((MoreActionsFragmentActivity) this.a.I()).startActivityForResult(intent, 811);
        } catch (Exception e) {
            ag.a(bp.P, "Error when sending email to url:" + str, e);
            ((MoreActionsFragmentActivity) this.a.I()).b(true);
        }
    }

    private void b(String str) {
        try {
            if (a.e) {
                Log.e(bp.P, "Try to call to phone number:" + str);
            }
            ((MoreActionsFragmentActivity) this.a.I()).startActivityForResult(new Intent("android.intent.action.DIAL", Uri.parse(str)), 811);
        } catch (Exception e) {
            ag.a(bp.P, "Error when calling to tel number:" + str, e);
            ((MoreActionsFragmentActivity) this.a.I()).b(true);
            this.a.d((int) C0000R.string.unable_to_call);
        }
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (str.contains("mailto:")) {
            a(str);
            return true;
        } else if (str.contains("tel:")) {
            b(str);
            return true;
        } else if (!str.startsWith("http://") && !str.startsWith("https://")) {
            return super.shouldOverrideUrlLoading(webView, str);
        } else {
            webView.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            return true;
        }
    }
}
