package com.avidia.fismobile.view;

import android.os.Bundle;
import android.text.TextUtils;
import com.avidia.b.v;
import com.avidia.e.ag;

class ax implements Runnable {
    final /* synthetic */ v a;
    final /* synthetic */ ar b;

    ax(ar arVar, v vVar) {
        this.b = arVar;
        this.a = vVar;
    }

    public void run() {
        this.b.J();
        if (!ag.a(this.b.I(), this.a)) {
            if (this.a.c == null || this.a.c.a == null) {
                Bundle bundle = new Bundle();
                bundle.putString("CLAIM", this.a.b);
                String string = this.b.b().getString("ACCOUNT_TYPE");
                if (!TextUtils.isEmpty(string)) {
                    bundle.putString("ACCOUNT_TYPE", string);
                }
                ((ae) this.b.I()).c(bundle);
                return;
            }
            this.b.b(ag.b(this.a));
        }
    }
}
