package com.avidia.fismobile;

import android.view.View;
import android.widget.Button;

class i implements View.OnClickListener {
    final /* synthetic */ Button a;
    final /* synthetic */ h b;

    i(h hVar, Button button) {
        this.b = hVar;
        this.a = button;
    }

    public void onClick(View view) {
        if (this.a.isEnabled() && this.a.getVisibility() == 0 && this.a.isClickable()) {
            this.a.performClick();
        }
    }
}
