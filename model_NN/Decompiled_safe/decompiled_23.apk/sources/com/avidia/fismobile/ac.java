package com.avidia.fismobile;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.avidia.fismobile.view.AccountsFragmentActivity;

public class ac extends BaseAdapter {
    private static LayoutInflater c = null;
    String[] a;
    /* access modifiers changed from: private */
    public final AccountsFragmentActivity b;
    private int d;

    public ac(AccountsFragmentActivity accountsFragmentActivity, String[] strArr) {
        this(accountsFragmentActivity, strArr, 0);
    }

    public ac(AccountsFragmentActivity accountsFragmentActivity, String[] strArr, int i) {
        this.d = i;
        this.b = accountsFragmentActivity;
        this.a = strArr;
        c = (LayoutInflater) this.b.getSystemService("layout_inflater");
    }

    public int getCount() {
        return this.a.length;
    }

    public Object getItem(int i) {
        return Integer.valueOf(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = c.inflate((int) C0000R.layout.filter_item, (ViewGroup) null);
        }
        ImageView imageView = (ImageView) view.findViewById(C0000R.id.filter_arrow);
        if (FisApplication.e()) {
            imageView.setOnClickListener(new ad(this, i));
        } else {
            imageView.setVisibility(8);
        }
        if (this.d != 0) {
            view.setBackgroundColor(this.d);
            view.invalidate();
        }
        ((TextView) view.findViewById(C0000R.id.filter_name)).setText(this.a[i]);
        view.setOnClickListener(new ae(this, i));
        return view;
    }
}
