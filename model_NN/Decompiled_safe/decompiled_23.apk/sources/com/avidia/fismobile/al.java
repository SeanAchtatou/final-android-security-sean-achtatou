package com.avidia.fismobile;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.avidia.b.o;
import java.util.List;

public class al extends BaseAdapter {
    protected static LayoutInflater c = null;
    protected Activity a;
    protected List b;

    public al(Activity activity, List list) {
        this.a = activity;
        this.b = list;
        c = (LayoutInflater) this.a.getSystemService("layout_inflater");
    }

    /* renamed from: a */
    public o getItem(int i) {
        return (o) this.b.get(i);
    }

    public void a(List list) {
        this.b = list;
        notifyDataSetChanged();
    }

    public int getCount() {
        return this.b.size();
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = c.inflate((int) C0000R.layout.key_value_item, (ViewGroup) null);
        }
        ((TextView) view.findViewById(C0000R.id.textview_key)).setText(((o) this.b.get(i)).a);
        ((TextView) view.findViewById(C0000R.id.textview_value)).setText(((o) this.b.get(i)).b);
        return view;
    }
}
