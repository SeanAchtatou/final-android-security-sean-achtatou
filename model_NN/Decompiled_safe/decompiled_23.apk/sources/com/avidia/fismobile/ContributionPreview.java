package com.avidia.fismobile;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.avidia.b.a;
import com.avidia.b.j;
import com.avidia.b.o;
import com.avidia.b.v;
import com.avidia.e.ag;
import com.avidia.e.n;
import com.avidia.e.w;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

public class ContributionPreview extends h implements w {
    /* access modifiers changed from: private */
    public static final String s = ContributionPreview.class.getSimpleName();
    /* access modifiers changed from: private */
    public String o;
    /* access modifiers changed from: private */
    public String p;
    /* access modifiers changed from: private */
    public a q;
    private al r;

    private List a(j jVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new o(getString(C0000R.string.contribution_date), ag.f(jVar.e())));
        arrayList.add(new o(getString(C0000R.string.contribution_amount), ag.a(Double.valueOf(jVar.d()))));
        arrayList.add(new o(getString(C0000R.string.contribution_year), jVar.g() ? getString(C0000R.string.current_year) : getString(C0000R.string.previous_year)));
        String f = jVar.f();
        if (!TextUtils.isEmpty(f)) {
            arrayList.add(new o(getString(C0000R.string.withdrawal_date), ag.f(f)));
        }
        arrayList.add(new o(getString(C0000R.string.bank_account_for_withdrawal), jVar.h().e()));
        return arrayList;
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2) {
        TextView textView = (TextView) findViewById(C0000R.id.header_caption);
        Button button = (Button) findViewById(C0000R.id.submit);
        if ("TYPE_PREVIEW_EDIT".equals(str) || "TYPE_PREVIEW_ADD".equals(str)) {
            textView.setText((int) C0000R.string.preview);
            button.setVisibility(0);
            button.setOnClickListener(new r(this, str2));
        } else if ("TYPE_DETAILS".equals(str)) {
            button.setVisibility(8);
            textView.setText((int) C0000R.string.contribution_details);
        } else if ("TYPE_SUCCESS".equals(str)) {
            button.setVisibility(0);
            button.setText((int) C0000R.string.back_to_list);
            button.setOnClickListener(new t(this, null));
            textView.setText((int) C0000R.string.success);
            d(str2);
        } else {
            throw new RuntimeException("Unknown type of activity!");
        }
    }

    private void d(String str) {
        try {
            j a = j.a(str);
            ((TextView) findViewById(C0000R.id.confirmation_text)).setText(getString(C0000R.string.confirmation_num, new Object[]{a.c()}));
            findViewById(C0000R.id.confirmation_layout).setVisibility(0);
            this.r.a(a(a));
        } catch (Exception e) {
            if (com.avidia.a.a.e) {
                Log.d(s, "Unable to fill confirmation");
            }
        }
    }

    /* access modifiers changed from: private */
    public void e(String str) {
        an f = f();
        if (f != null) {
            f.B();
            l();
            f.a(new n(str, "TYPE_PREVIEW_ADD".equals(this.o)), this);
        } else if (com.avidia.a.a.e) {
            Log.e(s, "Unable to load contributions");
        }
    }

    public void a(v vVar) {
        runOnUiThread(new s(this, vVar));
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.contribution_preview);
        Bundle extras = getIntent().getExtras();
        String string = extras.getString("CONTRIBUTION");
        try {
            if (extras.containsKey("account_info")) {
                this.q = new a();
                this.q.a(new JSONObject(extras.getString("account_info")));
            }
            this.o = extras.getString("PREVIEW_ACTIVITY_TYPE");
            j a = j.a(string);
            String a2 = this.o.equals("TYPE_PREVIEW_ADD") ? a.a() : string;
            this.r = new al(this, a(a));
            ((ListView) findViewById(C0000R.id.preview_listview)).setAdapter((ListAdapter) this.r);
            if (bundle == null) {
                a(this.o, a2);
            }
        } catch (Exception e) {
            if (com.avidia.a.a.e) {
                Log.d(s, "Unable to parse contribution");
            }
            Toast.makeText(this, (int) C0000R.string.internal_error, 1).show();
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        this.o = bundle.getString("PREVIEW_ACTIVITY_TYPE");
        this.p = bundle.getString("CONTRIBUTION");
        a(this.o, this.p);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putString("PREVIEW_ACTIVITY_TYPE", this.o);
        bundle.putString("CONTRIBUTION", this.p);
    }
}
