package com.avidia.fismobile;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.avidia.b.j;
import com.avidia.e.ag;

public class o extends j {
    public static final String a = o.class.getSimpleName();
    /* access modifiers changed from: private */
    public ContributionsActivity e;

    public o(ContributionsActivity contributionsActivity) {
        super(contributionsActivity, C0000R.layout.contribution_item, C0000R.layout.contribution_item);
        this.e = contributionsActivity;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = this.b.inflate(this.c, (ViewGroup) null);
        }
        j jVar = (j) getItem(i);
        ((TextView) view.findViewById(C0000R.id.contribution_balance)).setText(ag.a(Double.valueOf(jVar.d())));
        ((TextView) view.findViewById(C0000R.id.contribution_date)).setText(ag.j(ag.f(jVar.e())));
        ((TextView) view.findViewById(C0000R.id.contribution_wdate)).setText(ag.j(ag.f(jVar.f())));
        ImageButton imageButton = (ImageButton) view.findViewById(C0000R.id.contribution_edit);
        imageButton.setOnClickListener(new p(this, jVar));
        a(view, C0000R.id.contribution_edit_layout, imageButton);
        ImageView imageView = (ImageView) view.findViewById(C0000R.id.contribution_arrow);
        imageView.setOnClickListener(new q(this, i));
        a(view, imageView);
        return view;
    }
}
