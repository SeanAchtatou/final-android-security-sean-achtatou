package com.avidia.fismobile;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import com.avidia.a.a;
import com.avidia.b.p;
import com.avidia.b.s;
import com.avidia.c.c;
import com.avidia.e.ag;
import com.avidia.fismobile.view.bo;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import org.json.JSONArray;

public class SignInQuestionActivity extends l implements View.OnClickListener, p {
    private static final String o = SignInQuestionActivity.class.getSimpleName();
    private static final List p = new ArrayList();
    private static int u;
    private final Random q = new Random();
    private TextView r;
    private View s;
    private String t;
    private Set v;

    private void a(View view, boolean z) {
        if (view != null) {
            view.setEnabled(z);
        }
    }

    private int b(int i) {
        return i == 0 ? C0000R.id.question_area_1 : C0000R.id.question_area_2;
    }

    private boolean b(Throwable th) {
        return th instanceof c;
    }

    private void c(boolean z) {
        for (View a : this.v) {
            a(a, z);
        }
    }

    public static boolean q() {
        return u >= 3;
    }

    public static void r() {
        u = 0;
    }

    private void s() {
        int nextInt;
        String stringExtra = getIntent().getStringExtra("questions");
        try {
            p.clear();
            JSONArray jSONArray = new JSONArray(stringExtra);
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < 2; i++) {
                do {
                    nextInt = this.q.nextInt(jSONArray.length());
                } while (arrayList.contains(Integer.valueOf(nextInt)));
                arrayList.add(Integer.valueOf(nextInt));
                String string = jSONArray.getJSONObject(nextInt).getString("Content");
                s sVar = new s();
                sVar.a = string;
                p.add(sVar);
            }
        } catch (Exception e) {
            if (a.e) {
                Log.e(o, "", e);
            }
        }
        for (int i2 = 0; i2 < p.size(); i2++) {
            View findViewById = findViewById(b(i2));
            ((TextView) findViewById.findViewById(C0000R.id.tv_question)).setText(((s) p.get(i2)).a);
            EditText editText = (EditText) findViewById.findViewById(C0000R.id.et_answer);
            if (editText != null) {
                this.v.add(editText);
                a(editText, this.s.getVisibility() != 0);
            }
        }
    }

    private void t() {
        v();
        j();
        w();
        u();
    }

    private void u() {
        if (p.size() > 0) {
            l();
            an f = f();
            f.B();
            f.a("https://m.wealthcareadmin.com/issuer/challenge/questions/answers/" + FisApplication.k().m(), ag.a(p), this);
        }
    }

    private void v() {
        this.r.setVisibility(4);
    }

    private void w() {
        View findViewById;
        for (int i = 0; i < p.size(); i++) {
            View findViewById2 = findViewById(b(i));
            if (!(findViewById2 == null || (findViewById = findViewById2.findViewById(C0000R.id.et_answer)) == null || !(findViewById instanceof EditText))) {
                this.v.add(findViewById);
                a(findViewById, this.s.getVisibility() != 0);
                ((s) p.get(i)).b = ((EditText) findViewById).getText().toString().trim();
            }
        }
    }

    private void x() {
        View findViewById;
        for (int i = 0; i < p.size(); i++) {
            View findViewById2 = findViewById(b(i));
            if (!(findViewById2 == null || (findViewById = findViewById2.findViewById(C0000R.id.et_answer)) == null || !(findViewById instanceof EditText))) {
                this.v.add(findViewById);
                a(findViewById, this.s.getVisibility() != 0);
                ((EditText) findViewById).setText("");
            }
        }
    }

    public void a(Throwable th) {
        if (b(th)) {
            runOnUiThread(new az(this));
            return;
        }
        u++;
        if (q()) {
            Intent intent = new Intent(this, LockedAccountActivity.class);
            intent.addFlags(67108864);
            intent.addFlags(268435456);
            startActivity(intent);
            finish();
        }
        runOnUiThread(new ba(this, ag.a(th)));
    }

    public void a_(String str) {
        FisApplication.k().b(this.t);
        if (FisApplication.k().o()) {
            FisApplication.k().n();
        }
        runOnUiThread(new ax(this));
        if (str != null) {
            Intent intent = new Intent(this, SignInKeyActivity.class);
            intent.putExtra("siteKey", str);
            startActivity(intent);
            finish();
            return;
        }
        runOnUiThread(new ay(this));
    }

    public void c(String str) {
        j();
        this.r.setText(Html.fromHtml(str));
        if (this.r.getVisibility() == 4) {
            Animation loadAnimation = AnimationUtils.loadAnimation(this, C0000R.anim.fadein);
            ((TextView) findViewById(C0000R.id.signin_question_donot_recognize)).setVisibility(4);
            this.r.startAnimation(loadAnimation);
            this.r.setVisibility(0);
        }
    }

    public void onClick(View view) {
        if (this.s != null && this.s.getVisibility() != 0) {
            t();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        boolean z = true;
        super.onCreate(bundle);
        this.v = new HashSet();
        setContentView(bo.f());
        this.r = (TextView) findViewById(C0000R.id.signin_question_erroranswer);
        this.s = findViewById(C0000R.id.pb_progress_bar);
        s();
        Button button = (Button) findViewById(C0000R.id.btn_question_signin);
        button.setOnClickListener(this);
        a(button, this.s.getVisibility() != 0);
        CheckBox checkBox = (CheckBox) findViewById(C0000R.id.cb_remember_device);
        this.v.add(button);
        this.v.add(checkBox);
        checkBox.setOnCheckedChangeListener(new aw(this));
        this.t = getIntent().getExtras().getString("online_id");
        if (this.t == null) {
            this.t = "";
        }
        if (!TextUtils.isEmpty(this.t)) {
            checkBox.setChecked(true);
        }
        if (this.s.getVisibility() == 0) {
            z = false;
        }
        c(z);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        m();
        x();
        super.onPause();
    }
}
