package com.avidia.fismobile;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.avidia.b.p;
import com.avidia.e.q;
import com.avidia.e.w;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class an extends Fragment {
    private final ExecutorService P = Executors.newFixedThreadPool(5);
    private Future Q;

    public void B() {
        if (this.Q != null) {
            this.Q.cancel(true);
        }
    }

    public void a(q qVar, w wVar) {
        B();
        this.Q = this.P.submit(new ab(qVar, wVar));
    }

    public void a(String str, String str2, p pVar) {
        B();
        this.Q = this.P.submit(new am(str, str2, pVar));
    }

    public void c(Bundle bundle) {
        super.c(bundle);
        b(true);
    }
}
