package com.avidia.fismobile;

import android.content.res.Configuration;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Menu;
import android.view.View;

public class l extends h {
    private void q() {
        if (!FisApplication.e()) {
            try {
                Display defaultDisplay = getWindowManager().getDefaultDisplay();
                DisplayMetrics displayMetrics = new DisplayMetrics();
                defaultDisplay.getMetrics(displayMetrics);
                int floor = (int) Math.floor(((double) Math.min(displayMetrics.widthPixels, displayMetrics.heightPixels)) * 0.8d);
                View findViewById = findViewById(C0000R.id.signin);
                if (findViewById != null) {
                    findViewById.getLayoutParams().width = floor;
                    findViewById.invalidate();
                }
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public int n() {
        return FisApplication.e() ? 2131296328 : 2131296329;
    }

    public void onBackPressed() {
        if (!p()) {
            super.onBackPressed();
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        q();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        FisApplication.k().c();
        q();
    }
}
