package com.avidia.fismobile;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Button;
import android.widget.ListAdapter;

public class w implements DialogInterface.OnClickListener {
    private final Context a;
    private Button b;
    private ListAdapter c;
    private AlertDialog d;
    private int e;

    public w(Context context, Button button) {
        this.a = context;
        this.b = button;
    }

    public void a(int i) {
        if (this.c != null && this.c.getCount() - 1 >= i) {
            this.e = i;
            if (i >= 0 && i < this.c.getCount()) {
                this.b.setText(this.c.getItem(i).toString());
            }
        }
    }

    public void a(ListAdapter listAdapter) {
        this.c = listAdapter;
    }

    public boolean a() {
        return this.d != null && this.d.isShowing();
    }

    public void b() {
        if (this.d != null) {
            this.d.dismiss();
            this.d = null;
        }
    }

    public void c() {
        if (this.c != null && this.c.getCount() != 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.a);
            if (this.e > -1) {
                this.d = builder.setSingleChoiceItems(this.c, this.e, this).show();
            }
        }
    }

    public int d() {
        return this.e;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        a(i);
        b();
    }
}
