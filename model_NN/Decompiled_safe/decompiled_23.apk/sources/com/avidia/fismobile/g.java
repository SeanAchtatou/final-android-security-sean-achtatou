package com.avidia.fismobile;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.avidia.a.a;
import com.avidia.b.o;
import java.util.ArrayList;

public class g extends BaseAdapter {
    private static LayoutInflater c = null;
    ArrayList a;
    private Activity b;

    public g(Activity activity, ArrayList arrayList) {
        this.b = activity;
        this.a = arrayList;
        c = (LayoutInflater) this.b.getSystemService("layout_inflater");
    }

    private boolean a(String str) {
        return "Message".equalsIgnoreCase(str);
    }

    public int getCount() {
        return this.a.size();
    }

    public Object getItem(int i) {
        return Integer.valueOf(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View view2;
        String str = ((o) this.a.get(i)).a;
        if (a(str)) {
            View inflate = c.inflate((int) C0000R.layout.message_item, (ViewGroup) null);
            WebView webView = (WebView) inflate.findViewById(C0000R.id.message_html);
            webView.setBackgroundColor(0);
            String str2 = "<html><body>" + ((o) this.a.get(i)).b + "</body></html>";
            if (a.e) {
                Log.d("AlertDetailsAdapter", "HtmlData:" + str2);
            }
            webView.loadDataWithBaseURL(null, str2, "text/html", null, null);
            view2 = inflate;
        } else {
            View inflate2 = c.inflate((int) C0000R.layout.key_value_item, (ViewGroup) null);
            ((TextView) inflate2.findViewById(C0000R.id.textview_value)).setText(((o) this.a.get(i)).b);
            view2 = inflate2;
        }
        ((TextView) view2.findViewById(C0000R.id.textview_key)).setText(str);
        return view2;
    }
}
