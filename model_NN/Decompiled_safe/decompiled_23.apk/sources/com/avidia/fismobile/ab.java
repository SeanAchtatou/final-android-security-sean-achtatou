package com.avidia.fismobile;

import android.text.TextUtils;
import com.avidia.b.m;
import com.avidia.b.v;
import com.avidia.e.ag;
import com.avidia.e.q;
import com.avidia.e.w;
import java.lang.ref.WeakReference;
import java.util.concurrent.Callable;

class ab implements Callable {
    private final WeakReference a;
    private final q b;

    public ab(q qVar, w wVar) {
        this.b = qVar;
        this.a = new WeakReference(wVar);
    }

    private w b() {
        if (this.a != null) {
            return (w) this.a.get();
        }
        return null;
    }

    /* renamed from: a */
    public String call() {
        v vVar;
        try {
            vVar = this.b.a();
            if (!vVar.a && vVar.c != null && TextUtils.isEmpty(vVar.c.b)) {
                vVar.c.b = ag.a(vVar.c.a);
            }
        } catch (Throwable th) {
            Throwable th2 = th;
            vVar = new v();
            vVar.a = false;
            vVar.c = new m(th2);
        }
        w b2 = b();
        if (b2 != null) {
            b2.a(vVar);
        }
        return "";
    }
}
