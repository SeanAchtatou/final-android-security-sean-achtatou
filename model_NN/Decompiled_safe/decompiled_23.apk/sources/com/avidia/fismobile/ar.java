package com.avidia.fismobile;

import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.TextView;

class ar implements TextView.OnEditorActionListener {
    final /* synthetic */ EditText a;
    final /* synthetic */ SignInActivity b;

    ar(SignInActivity signInActivity, EditText editText) {
        this.b = signInActivity;
        this.a = editText;
    }

    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        boolean z = false;
        if (i != 2) {
            return false;
        }
        String obj = this.a.getText().toString();
        if (this.b.s != null && this.b.s.isChecked()) {
            z = true;
        }
        this.b.a(obj, z);
        return true;
    }
}
