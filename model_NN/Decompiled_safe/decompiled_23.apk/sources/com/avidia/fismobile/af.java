package com.avidia.fismobile;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import java.util.Calendar;

public class af extends AlertDialog implements DatePicker.OnDateChangedListener {
    /* access modifiers changed from: private */
    public DatePicker a;
    /* access modifiers changed from: private */
    public final ai b;
    private final Calendar c;
    private final Calendar d = Calendar.getInstance();

    public af(Context context, ai aiVar, int i, int i2, int i3) {
        super(context, 2131296302);
        this.b = aiVar;
        this.c = Calendar.getInstance();
        this.c.set(i, i2, i3);
    }

    private void a(ViewGroup viewGroup) {
        if (viewGroup != null) {
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View childAt = viewGroup.getChildAt(i);
                if (childAt instanceof ViewGroup) {
                    a((ViewGroup) childAt);
                } else if (childAt != null && (childAt instanceof EditText)) {
                    childAt.setLongClickable(false);
                    childAt.setClickable(false);
                    childAt.setFocusable(false);
                }
            }
        }
    }

    public Calendar a() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.date_picker_dlg);
        this.a = (DatePicker) findViewById(C0000R.id.picker);
        findViewById(C0000R.id.btn_cancel).setOnClickListener(new ag(this));
        findViewById(C0000R.id.btn_ok).setOnClickListener(new ah(this));
        a(this.a);
        if (bundle == null) {
            this.a.init(this.c.get(1), this.c.get(2), this.c.get(5), this);
        }
    }

    public void onDateChanged(DatePicker datePicker, int i, int i2, int i3) {
        int i4 = this.d.get(1) + 20;
        int i5 = this.d.get(1) - 15;
        if (i > i4 || i < i5) {
            if (i <= i4) {
                i4 = i5;
            }
            this.a.init(i4, i2, i3, this);
            return;
        }
        this.c.set(1, i);
        this.c.set(2, i2);
        this.c.set(5, i3);
    }

    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        this.a.init(bundle.getInt("year"), bundle.getInt("month"), bundle.getInt("day"), this);
    }

    public Bundle onSaveInstanceState() {
        Bundle onSaveInstanceState = super.onSaveInstanceState();
        onSaveInstanceState.putInt("year", this.a.getYear());
        onSaveInstanceState.putInt("month", this.a.getMonth());
        onSaveInstanceState.putInt("day", this.a.getDayOfMonth());
        return onSaveInstanceState;
    }
}
