package com.avidia.fismobile;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.avidia.a.a;
import com.avidia.b.m;
import com.avidia.b.v;
import com.avidia.b.y;
import com.avidia.c.e;
import com.avidia.e.aa;
import com.avidia.e.ag;
import com.avidia.e.r;
import com.avidia.e.w;
import com.avidia.fismobile.view.AccountsFragmentActivity;
import com.avidia.fismobile.view.bo;
import java.io.StringReader;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.InputSource;

public class SignInKeyActivity extends l implements View.OnClickListener, w {
    public static final String o = SignInKeyActivity.class.getSimpleName();
    private ImageView p;
    private EditText q;
    private TextView r;
    private TextView s;
    private View t;
    /* access modifiers changed from: private */
    public au u;
    private View v;

    private void a(View view, boolean z) {
        if (view != null) {
            view.setEnabled(z);
        }
    }

    private void c(boolean z) {
        a(this.q, z);
        a(this.v, z);
    }

    private y e(String str) {
        y yVar = new y();
        try {
            JSONObject jSONObject = new JSONObject(str);
            yVar.a = jSONObject.getString("Phrase");
            yVar.b = ag.c(jSONObject.getString("SiteImage"));
        } catch (JSONException e) {
            if (a.e) {
                Log.e(o, "", e);
            }
            c(getString(C0000R.string.internal_error));
        }
        return yVar;
    }

    private y f(String str) {
        try {
            SAXParser newSAXParser = SAXParserFactory.newInstance().newSAXParser();
            aa aaVar = new aa();
            newSAXParser.parse(new InputSource(new StringReader(str)), aaVar);
            return aaVar.a();
        } catch (Throwable th) {
            if (!a.e) {
                return null;
            }
            Log.e(o, "", th);
            return null;
        }
    }

    /* access modifiers changed from: private */
    public void q() {
        j();
        l();
        String obj = this.q.getText().toString();
        if (TextUtils.isEmpty(obj.trim())) {
            a((int) C0000R.string.enter_non_empty_password);
            m();
            return;
        }
        an f = f();
        f.B();
        f.a(new com.avidia.e.y(obj), this);
    }

    private void r() {
        String stringExtra = getIntent().getStringExtra("siteKey");
        y e = getIntent().getBooleanExtra("jsonUsed", false) ? e(stringExtra) : f(stringExtra);
        if (e != null) {
            this.p.setImageBitmap(e.b);
            this.s.setText(e.a);
        }
    }

    /* access modifiers changed from: private */
    public void s() {
        this.r.setVisibility(8);
    }

    public void a(m mVar) {
        if (mVar != null && mVar.a != null) {
            Throwable th = mVar.a;
            if (!(th instanceof e) || ((e) th).a().length() <= 30) {
                m();
                c(ag.a(mVar.a));
                return;
            }
            Intent intent = new Intent(this, LockedAccountActivity.class);
            intent.addFlags(67108864);
            intent.addFlags(268435456);
            intent.putExtra("CAUSE", getString(C0000R.string.signin_error_locked_account_pwd));
            m();
            startActivity(intent);
            finish();
        }
    }

    public void a(v vVar) {
        runOnUiThread(new at(this, vVar));
    }

    public void c(String str) {
        j();
        this.r.setText(Html.fromHtml(str));
        if (this.r.getVisibility() != 0) {
            this.r.startAnimation(AnimationUtils.loadAnimation(this, C0000R.anim.fadein));
            this.r.setVisibility(0);
        }
    }

    public void d(String str) {
        String str2 = a.d[1];
        FisApplication.k().h(str);
        FisApplication.k().d(str2);
        Intent intent = new Intent(this, AccountsFragmentActivity.class);
        intent.putExtra("accountData", str);
        intent.putExtra("filter_name", str2);
        startActivity(intent);
        finish();
    }

    public void onClick(View view) {
        if (this.t != null && this.t.getVisibility() != 0) {
            s();
            q();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        boolean z = true;
        r.a = false;
        super.onCreate(bundle);
        setContentView(bo.e());
        this.t = findViewById(C0000R.id.pb_progress_bar);
        this.v = findViewById(C0000R.id.btn_key_signin);
        this.v.setOnClickListener(this);
        this.p = (ImageView) findViewById(C0000R.id.iv_site_image);
        this.q = (EditText) findViewById(C0000R.id.et_password);
        a(this.q, this.t.getVisibility() != 0);
        a(this.v, this.t.getVisibility() != 0);
        this.q.setOnEditorActionListener(new as(this));
        this.r = (TextView) findViewById(C0000R.id.signin_key_incorrect_password);
        this.s = (TextView) findViewById(C0000R.id.tv_phrase);
        this.u = new au(this, null);
        if (this.t.getVisibility() == 0) {
            z = false;
        }
        c(z);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        r();
    }
}
