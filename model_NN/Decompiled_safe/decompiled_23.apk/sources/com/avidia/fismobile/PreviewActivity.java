package com.avidia.fismobile;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.avidia.b.o;
import java.util.ArrayList;

public class PreviewActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.preview);
        ((TextView) findViewById(C0000R.id.header_caption)).setText((int) C0000R.string.preview);
        ArrayList arrayList = new ArrayList();
        arrayList.add(new o("Contribution Date", "11/12/2011"));
        arrayList.add(new o("Contribution Amount", "$50.00"));
        arrayList.add(new o("Withdrawal Date", "11/10/2011"));
        arrayList.add(new o("Bank Account\nfor Withdrawal", "My Checking"));
        ((ListView) findViewById(C0000R.id.preview_listview)).setAdapter((ListAdapter) new al(this, arrayList));
    }
}
