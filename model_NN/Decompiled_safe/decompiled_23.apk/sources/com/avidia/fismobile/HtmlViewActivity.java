package com.avidia.fismobile;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;
import com.avidia.fismobile.view.aj;
import com.avidia.fismobile.view.al;

public class HtmlViewActivity extends al {
    public static final String o = HtmlViewActivity.class.getSimpleName();

    /* access modifiers changed from: protected */
    public aj a(String str, Bundle bundle, boolean z) {
        return null;
    }

    /* access modifiers changed from: protected */
    public int b(Fragment fragment) {
        return 0;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (FisApplication.e()) {
            ((ViewGroup) findViewById(C0000R.id.fragment_header)).addView(getLayoutInflater().inflate((int) C0000R.layout.window_caption, (ViewGroup) null));
        } else {
            ((ViewGroup) findViewById(C0000R.id.fragment_header)).addView(getLayoutInflater().inflate((int) C0000R.layout.tablet_activity_header, (ViewGroup) null));
            findViewById(C0000R.id.base_layout).setBackgroundResource(C0000R.drawable.common_gradient_bg_with_logo_tablet);
        }
        v();
        TextView textView = (TextView) findViewById(C0000R.id.header_caption);
        if (textView != null) {
            textView.setText(getString(getIntent().getExtras().getInt("HEADER_ID")));
        }
        WebView webView = (WebView) findViewById(C0000R.id.web_view);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new ak(this));
        webView.setBackgroundColor(0);
        webView.loadDataWithBaseURL(null, getIntent().getStringExtra("html_text"), "text/html", "UTF-8", null);
    }

    /* access modifiers changed from: protected */
    public int q() {
        return C0000R.layout.about_us;
    }
}
