package com.avidia.fismobile;

import android.util.Log;
import android.view.View;
import android.widget.Toast;
import com.avidia.a.a;

class aa implements View.OnClickListener {
    final /* synthetic */ boolean a;
    final /* synthetic */ EditContributionActivity b;

    aa(EditContributionActivity editContributionActivity, boolean z) {
        this.b = editContributionActivity;
        this.a = z;
    }

    public void onClick(View view) {
        try {
            this.b.c(this.a);
        } catch (Exception e) {
            Toast.makeText(this.b, (int) C0000R.string.internal_error, 1).show();
            if (a.e) {
                Log.d(EditContributionActivity.o, "Unable to open preview: " + e.getMessage());
            }
        }
    }
}
