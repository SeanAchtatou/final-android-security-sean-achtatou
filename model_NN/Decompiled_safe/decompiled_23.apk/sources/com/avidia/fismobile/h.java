package com.avidia.fismobile;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.n;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Toast;
import com.a.a.ab;
import com.a.a.j;
import com.avidia.a.a;
import com.avidia.b.q;
import com.avidia.e.ag;
import com.avidia.fismobile.view.bm;
import java.util.List;

public abstract class h extends android.support.v4.app.h {
    protected j n;
    private an o;
    private bb p;
    private boolean q = true;

    private static boolean a(ComponentName componentName, Context context) {
        boolean z = true;
        if ("com.cooliris.media.Gallery".equalsIgnoreCase(componentName.getClassName())) {
            z = false;
        } else if ("com.android.contacts.activities.DialtactsActivity".equalsIgnoreCase(componentName.getClassName())) {
            z = false;
        }
        if (componentName.getPackageName().equals(context.getPackageName())) {
            FisApplication.k().c();
            return false;
        } else if (z) {
            return z;
        } else {
            FisApplication.k().a();
            return z;
        }
    }

    public static boolean a(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
        if (activityManager != null) {
            List<ActivityManager.RunningTaskInfo> runningTasks = activityManager.getRunningTasks(1);
            if (!runningTasks.isEmpty() && a(runningTasks.get(0).topActivity, context)) {
                return true;
            }
        }
        return false;
    }

    public void a(int i) {
        Toast.makeText(this, i, 1).show();
    }

    /* access modifiers changed from: protected */
    public void a(int i, Button button) {
        findViewById(i).setOnClickListener(new i(this, button));
    }

    public void b(String str) {
        Toast.makeText(this, str, 1).show();
    }

    public void b(boolean z) {
        this.q = z;
    }

    public void c(String str) {
        if (ag.k(str)) {
            bm bmVar = new bm(this);
            bmVar.a(Html.fromHtml(str));
            bmVar.setTitle((int) C0000R.string.server_error);
            bmVar.show();
            return;
        }
        b(str);
    }

    public an f() {
        if (this.o == null) {
            n e = e();
            Fragment a = e.a("senderFragment");
            if (a == null || !(a instanceof an)) {
                a = new an();
                e.a().a(a, "senderFragment");
            }
            this.o = (an) a;
        }
        return this.o;
    }

    /* access modifiers changed from: protected */
    public bb g() {
        if (this.p == null) {
            n e = e();
            Fragment a = e.a("timeout_fragment");
            if (a == null || !(a instanceof bb)) {
                a = new bb();
                e.a().a(a, "timeout_fragment");
            }
            this.p = (bb) a;
        }
        return this.p;
    }

    /* access modifiers changed from: protected */
    public void h() {
        g().B();
    }

    /* access modifiers changed from: protected */
    public void i() {
        g().C();
    }

    /* access modifiers changed from: protected */
    public void j() {
        View currentFocus = getCurrentFocus();
        if (currentFocus != null) {
            ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
        }
    }

    /* access modifiers changed from: protected */
    public q k() {
        q qVar;
        String string = getSharedPreferences("user_preferences", 0).getString("user_ud", "");
        if (TextUtils.isEmpty(string)) {
            return new q();
        }
        try {
            qVar = (q) this.n.a(string, q.class);
        } catch (ab e) {
            if (a.e) {
                Log.e(SignInActivity.o, "Error when parsing saved online id:" + string, e);
            }
            qVar = null;
        }
        return qVar == null ? new q() : qVar;
    }

    public void l() {
        View findViewById = findViewById(C0000R.id.pb_progress_bar);
        if (findViewById != null) {
            findViewById.bringToFront();
            findViewById.setVisibility(0);
        }
    }

    public void m() {
        View findViewById = findViewById(C0000R.id.pb_progress_bar);
        if (findViewById != null) {
            findViewById.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public int n() {
        return FisApplication.e() ? 2131296326 : 2131296327;
    }

    public void o() {
        a((int) C0000R.string.internal_error);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        this.q = true;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        int n2 = n();
        getApplication().setTheme(n2);
        setTheme(n2);
        super.onCreate(bundle);
        this.n = new j();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        if (p() || !FisApplication.e()) {
            return false;
        }
        getMenuInflater().inflate(C0000R.menu.common_menu, menu);
        if (FisApplication.k().j() == 0) {
            menu.findItem(C0000R.id.menu_claim).setVisible(false);
        }
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        return ag.a(menuItem, this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (a(this) && this.q) {
            if (!(this instanceof l)) {
                FisApplication.k().a(false);
            }
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if ((!(this instanceof l) && this.q && !FisApplication.k().d()) || FisApplication.k().b()) {
            ag.a(this);
        }
        FisApplication.k().c();
    }

    /* access modifiers changed from: protected */
    public boolean p() {
        View findViewById = findViewById(C0000R.id.pb_progress_bar);
        return findViewById != null && findViewById.getVisibility() == 0;
    }

    public void startActivityForResult(Intent intent, int i) {
        if ("android.media.action.IMAGE_CAPTURE".equalsIgnoreCase(intent.getAction()) || "android.intent.action.PICK".equalsIgnoreCase(intent.getAction()) || i == 811) {
            this.q = false;
            FisApplication.k().a();
        }
        super.startActivityForResult(intent, i);
    }
}
