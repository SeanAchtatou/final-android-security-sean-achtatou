package com.avidia.fismobile;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.BaseAdapter;
import java.util.ArrayList;
import java.util.List;

public abstract class j extends BaseAdapter {
    private List a;
    protected final LayoutInflater b;
    protected final int c;
    protected int d;

    public j(Activity activity, int i, int i2) {
        this.d = -1;
        this.b = (LayoutInflater) activity.getSystemService("layout_inflater");
        this.c = !FisApplication.e() ? i2 : i;
    }

    public j(Activity activity, List list, int i, int i2) {
        this(activity, i, i2);
        a(list);
    }

    private void a(View view, boolean z) {
        View findViewById = view.findViewById(C0000R.id.selection_marker);
        if (findViewById != null) {
            if (z) {
                findViewById.setVisibility(0);
            } else {
                findViewById.setVisibility(4);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avidia.fismobile.j.a(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      com.avidia.fismobile.j.a(android.view.View, android.view.View):void
      com.avidia.fismobile.j.a(android.view.View, boolean):void */
    /* access modifiers changed from: protected */
    public void a(View view) {
        if (view != null && !FisApplication.e()) {
            View findViewById = view.findViewById(C0000R.id.btn_arrow);
            if (findViewById != null) {
                findViewById.setVisibility(4);
            }
            view.setBackgroundResource(C0000R.color.listitem_background_selected_tablet);
            a(view, true);
        }
    }

    /* access modifiers changed from: protected */
    public void a(View view, int i, View view2) {
        a(view.findViewById(i), view2);
    }

    /* access modifiers changed from: protected */
    public void a(View view, View view2) {
        view.setOnClickListener(new k(this, view2));
    }

    public void a(List list) {
        if (list == null) {
            list = new ArrayList();
        }
        this.a = list;
        notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public List b() {
        return this.a;
    }

    public int c() {
        return this.d;
    }

    public int getCount() {
        if (this.a == null) {
            return 0;
        }
        return this.a.size();
    }

    public Object getItem(int i) {
        try {
            if (this.a == null) {
                return null;
            }
            return this.a.get(i);
        } catch (Exception e) {
            return null;
        }
    }

    public long getItemId(int i) {
        return (long) i;
    }
}
