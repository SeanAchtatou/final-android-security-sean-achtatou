package com.avidia.fismobile;

import android.util.Log;
import com.avidia.a.a;
import com.avidia.b.i;
import com.avidia.b.p;
import com.avidia.e.ag;
import com.avidia.e.r;
import java.lang.ref.WeakReference;
import java.util.concurrent.Callable;

class am implements Callable {
    public static final String a = am.class.getSimpleName();
    private String b;
    private String c;
    private WeakReference d;

    public am(String str, String str2, p pVar) {
        this.b = str;
        this.c = str2;
        this.d = new WeakReference(pVar);
    }

    private p b() {
        if (this.d != null) {
            return (p) this.d.get();
        }
        return null;
    }

    /* renamed from: a */
    public String call() {
        Throwable th;
        String str;
        try {
            str = r.a(this.b, ag.a(), com.avidia.e.p.POST, this.c, i.XML);
            try {
                p b2 = b();
                if (b2 != null) {
                    b2.a_(str);
                }
            } catch (Throwable th2) {
                th = th2;
                if (a.e) {
                    Log.e(a, "", th);
                }
                p b3 = b();
                if (b3 != null) {
                    b3.a(th);
                }
                return str;
            }
        } catch (Throwable th3) {
            Throwable th4 = th3;
            str = "";
            th = th4;
        }
        return str;
    }
}
