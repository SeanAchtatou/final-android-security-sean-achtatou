package com.avidia.fismobile;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.avidia.b.e;
import com.avidia.e.ag;
import com.avidia.fismobile.view.bb;
import java.util.List;

public class m extends j {
    /* access modifiers changed from: private */
    public List a;
    /* access modifiers changed from: private */
    public bb e;

    public m(bb bbVar, List list, List list2) {
        super(bbVar.c(), list, C0000R.layout.claims_item, C0000R.layout.claims_item_tablet);
        this.e = bbVar;
        this.a = list2;
    }

    public void a() {
        List b = b();
        if (b != null && !b.isEmpty() && this.a != null && !this.a.isEmpty()) {
            this.d = 0;
            this.e.b((e) b.get(0), (String) this.a.get(0));
        }
    }

    public void a(int i) {
        List b;
        if (i >= 0 && (b = b()) != null && !b.isEmpty() && this.a != null && !this.a.isEmpty() && i >= 0 && i < b.size() && i < this.a.size()) {
            this.d = i;
        }
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = this.b.inflate(this.c, (ViewGroup) null);
        e eVar = (e) getItem(i);
        ((TextView) inflate.findViewById(C0000R.id.date)).setText(ag.f(eVar.j()));
        ((TextView) inflate.findViewById(C0000R.id.type)).setText(eVar.a());
        ((TextView) inflate.findViewById(C0000R.id.amount)).setText(ag.a(Double.valueOf(eVar.h())));
        inflate.findViewById(C0000R.id.is_uploaded_icon).setVisibility(eVar.o().isEmpty() ? 8 : 0);
        inflate.setOnClickListener(new n(this, i, eVar));
        if (i == this.d) {
            a(inflate);
        }
        return inflate;
    }
}
