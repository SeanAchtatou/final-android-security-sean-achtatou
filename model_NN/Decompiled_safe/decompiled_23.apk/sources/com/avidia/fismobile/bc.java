package com.avidia.fismobile;

import android.support.v4.app.h;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.avidia.b.z;
import com.avidia.fismobile.view.cr;
import java.util.ArrayList;

public class bc extends BaseAdapter {
    private static LayoutInflater d = null;
    private final int a;
    private int b = -1;
    private ArrayList c;
    private cr e;

    public bc(cr crVar, ArrayList arrayList) {
        h c2 = crVar.c();
        this.e = crVar;
        this.c = arrayList;
        d = (LayoutInflater) c2.getSystemService("layout_inflater");
        if (FisApplication.e()) {
            this.a = C0000R.layout.transaction_item;
        } else {
            this.a = C0000R.layout.transaction_item_tablet;
        }
    }

    private void a(View view) {
        if (!FisApplication.e()) {
            View findViewById = view.findViewById(C0000R.id.btn_arrow);
            if (findViewById != null) {
                findViewById.setVisibility(4);
            }
            view.setBackgroundResource(C0000R.color.listitem_background_selected_tablet);
            a(view, true);
        }
    }

    private void a(View view, boolean z) {
        View findViewById = view.findViewById(C0000R.id.selection_marker);
        if (findViewById != null) {
            if (z) {
                findViewById.setVisibility(0);
            } else {
                findViewById.setVisibility(4);
            }
        }
    }

    /* renamed from: a */
    public z getItem(int i) {
        return (z) this.c.get(i);
    }

    public void b(int i) {
        this.b = i;
        notifyDataSetChanged();
        this.e.b(i);
    }

    public int getCount() {
        return this.c.size();
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = d.inflate(this.a, (ViewGroup) null);
        TextView textView = (TextView) inflate.findViewById(C0000R.id.transaction_amount);
        TextView textView2 = (TextView) inflate.findViewById(C0000R.id.transaction_date);
        TextView textView3 = (TextView) inflate.findViewById(C0000R.id.transaction_type);
        z zVar = (z) this.c.get(i);
        if (zVar.d) {
            inflate.findViewById(C0000R.id.is_uploaded_icon).setVisibility(0);
        } else {
            inflate.findViewById(C0000R.id.is_uploaded_icon).setVisibility(4);
        }
        textView2.setText(zVar.a);
        textView3.setText(zVar.b);
        textView.setText(zVar.c == null ? "" : zVar.c.trim());
        if (zVar.c == null || zVar.c.length() < 9) {
            textView.setTextSize(2, 15.0f);
        } else {
            textView.setTextSize(2, 13.0f);
        }
        inflate.setOnClickListener(new bd(this, i));
        if (i == this.b) {
            a(inflate);
        }
        return inflate;
    }
}
