package com.avidia.fismobile;

import android.view.View;

class k implements View.OnClickListener {
    final /* synthetic */ View a;
    final /* synthetic */ j b;

    k(j jVar, View view) {
        this.b = jVar;
        this.a = view;
    }

    public void onClick(View view) {
        if (this.a.isClickable() && this.a.isEnabled() && this.a.getVisibility() == 0) {
            this.a.performClick();
        }
    }
}
