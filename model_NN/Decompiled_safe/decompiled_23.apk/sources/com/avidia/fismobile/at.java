package com.avidia.fismobile;

import com.avidia.b.v;
import com.avidia.e.ag;
import com.avidia.e.z;

class at implements Runnable {
    final /* synthetic */ v a;
    final /* synthetic */ SignInKeyActivity b;

    at(SignInKeyActivity signInKeyActivity, v vVar) {
        this.b = signInKeyActivity;
        this.a = vVar;
    }

    public void run() {
        if (ag.a(this.b, this.a)) {
            this.b.m();
        } else if (this.a.a) {
            FisApplication.k().a(true);
            an f = this.b.f();
            f.B();
            f.a(new z(), this.b.u);
        } else {
            this.b.m();
            FisApplication.k().a(false);
            this.b.a(this.a.c);
        }
    }
}
