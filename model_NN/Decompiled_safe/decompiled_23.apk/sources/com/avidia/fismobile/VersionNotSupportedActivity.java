package com.avidia.fismobile;

import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

public class VersionNotSupportedActivity extends h {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.version_not_supported);
        ((TextView) findViewById(C0000R.id.tv_unsupported)).setText((int) C0000R.string.unsupported_version);
        TextView textView = (TextView) findViewById(C0000R.id.tv_version_download_link_text);
        String str = "<a href='" + getString(C0000R.string.version_link_url) + "'>" + getString(C0000R.string.version_link_url_title) + "</a>";
        textView.setText(Html.fromHtml("<html>" + String.format(getString(C0000R.string.version_link_full), str) + "</html>"));
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setEnabled(true);
    }
}
