package com.avidia.fismobile;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import com.avidia.e.ag;
import com.avidia.fismobile.view.AccountsFragmentActivity;
import java.util.ArrayList;
import java.util.Calendar;

public class a extends j {
    public static final String a = a.class.getSimpleName();
    /* access modifiers changed from: private */
    public final AccountsFragmentActivity e;
    private final int f;

    public a(AccountsFragmentActivity accountsFragmentActivity, ArrayList arrayList, int i) {
        super(accountsFragmentActivity, arrayList, C0000R.layout.account_item, C0000R.layout.account_item_tablet);
        this.e = accountsFragmentActivity;
        this.f = i;
    }

    private boolean c(com.avidia.b.a aVar) {
        try {
            long timeInMillis = ag.h(ag.f(aVar.e)).getTimeInMillis();
            Calendar instance = Calendar.getInstance();
            instance.set(10, 0);
            instance.set(12, 0);
            instance.set(13, 0);
            instance.set(14, 0);
            return timeInMillis >= instance.getTimeInMillis();
        } catch (Exception e2) {
            return false;
        }
    }

    public com.avidia.b.a a() {
        return (com.avidia.b.a) getItem(this.d);
    }

    public void a(com.avidia.b.a aVar) {
        int indexOf;
        if (aVar == null) {
            this.d = -1;
        } else if (b() != null && (indexOf = b().indexOf(aVar)) >= 0) {
            this.d = indexOf;
            notifyDataSetChanged();
        }
    }

    public void b(com.avidia.b.a aVar) {
        if (aVar == null && b().isEmpty()) {
            return;
        }
        if (aVar != null || !b().isEmpty()) {
            this.d = b().indexOf(aVar);
        } else {
            this.d = 0;
        }
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = this.b.inflate(this.c, (ViewGroup) null);
        com.avidia.b.a aVar = (com.avidia.b.a) getItem(i);
        ((TextView) inflate.findViewById(C0000R.id.account_type)).setText(aVar.b);
        ((TextView) inflate.findViewById(C0000R.id.account_balance)).setText(ag.e(aVar.a));
        ((TextView) inflate.findViewById(C0000R.id.account_start_date)).setText(ag.j(ag.f(aVar.k)));
        ((TextView) inflate.findViewById(C0000R.id.account_end_date)).setText(ag.j(ag.f(aVar.j)));
        ImageButton imageButton = (ImageButton) inflate.findViewById(C0000R.id.account_info);
        imageButton.setOnClickListener(new b(this, i));
        a(inflate, C0000R.id.account_info_layout, imageButton);
        ImageButton imageButton2 = (ImageButton) inflate.findViewById(C0000R.id.claims_button);
        if ((this.f == 1 || this.f == 3) && (((!ag.d(aVar.l) && aVar.f) || ag.d(aVar.l)) && c(aVar))) {
            imageButton2.setVisibility(0);
            imageButton2.setOnClickListener(new c(this, i));
        } else {
            imageButton2.setVisibility(8);
        }
        inflate.findViewById(C0000R.id.account_fund_layout).setVisibility(0);
        inflate.findViewById(C0000R.id.account_fund_layout).setVisibility(8);
        inflate.setOnClickListener(new d(this, i));
        if (i == this.d) {
            a(inflate);
        }
        return inflate;
    }
}
