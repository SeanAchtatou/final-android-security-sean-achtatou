package com.avidia.fismobile;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

public class LockedAccountActivity extends Activity {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.security_answers_locked_account);
        TextView textView = (TextView) findViewById(C0000R.id.tv_security_answers_locked_account);
        Bundle extras = getIntent().getExtras();
        if (extras == null || !extras.containsKey("CAUSE")) {
            textView.setText(Html.fromHtml(getString(C0000R.string.security_answers_attempts_limit_exhausted)));
        } else {
            textView.setText(Html.fromHtml(extras.getString("CAUSE")));
        }
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
