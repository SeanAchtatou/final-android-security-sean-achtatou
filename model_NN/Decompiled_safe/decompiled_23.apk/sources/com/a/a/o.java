package com.a.a;

import com.a.a.d.a;
import com.a.a.d.e;
import com.a.a.d.f;

class o extends af {
    final /* synthetic */ j a;

    o(j jVar) {
        this.a = jVar;
    }

    /* renamed from: a */
    public Float b(a aVar) {
        if (aVar.f() != e.NULL) {
            return Float.valueOf((float) aVar.k());
        }
        aVar.j();
        return null;
    }

    public void a(f fVar, Number number) {
        if (number == null) {
            fVar.f();
            return;
        }
        this.a.a((double) number.floatValue());
        fVar.a(number);
    }
}
