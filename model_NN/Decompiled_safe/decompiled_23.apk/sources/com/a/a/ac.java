package com.a.a;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public abstract class ac extends Enum {
    public static final ac a = new ad("DEFAULT", 0);
    public static final ac b = new ae("STRING", 1);
    private static final /* synthetic */ ac[] c = {a, b};

    private ac(String str, int i) {
    }

    /* synthetic */ ac(String str, int i, ad adVar) {
        this(str, i);
    }

    public static ac valueOf(String str) {
        return (ac) Enum.valueOf(ac.class, str);
    }

    public static ac[] values() {
        return (ac[]) c.clone();
    }
}
