package com.a.a;

import com.a.a.d.f;
import java.io.IOException;
import java.io.StringWriter;

public abstract class u {
    public Number a() {
        throw new UnsupportedOperationException(getClass().getSimpleName());
    }

    public String b() {
        throw new UnsupportedOperationException(getClass().getSimpleName());
    }

    public double c() {
        throw new UnsupportedOperationException(getClass().getSimpleName());
    }

    public long d() {
        throw new UnsupportedOperationException(getClass().getSimpleName());
    }

    public int e() {
        throw new UnsupportedOperationException(getClass().getSimpleName());
    }

    public boolean f() {
        throw new UnsupportedOperationException(getClass().getSimpleName());
    }

    public boolean g() {
        return this instanceof s;
    }

    public boolean h() {
        return this instanceof x;
    }

    public boolean i() {
        return this instanceof z;
    }

    public boolean j() {
        return this instanceof w;
    }

    public x k() {
        if (h()) {
            return (x) this;
        }
        throw new IllegalStateException("Not a JSON Object: " + this);
    }

    public s l() {
        if (g()) {
            return (s) this;
        }
        throw new IllegalStateException("This is not a JSON Array.");
    }

    public z m() {
        if (i()) {
            return (z) this;
        }
        throw new IllegalStateException("This is not a JSON Primitive.");
    }

    /* access modifiers changed from: package-private */
    public Boolean n() {
        throw new UnsupportedOperationException(getClass().getSimpleName());
    }

    public String toString() {
        try {
            StringWriter stringWriter = new StringWriter();
            f fVar = new f(stringWriter);
            fVar.b(true);
            com.a.a.b.u.a(this, fVar);
            return stringWriter.toString();
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }
}
