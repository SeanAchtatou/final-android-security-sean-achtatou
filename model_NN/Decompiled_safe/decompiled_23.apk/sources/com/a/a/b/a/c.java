package com.a.a.b.a;

import com.a.a.af;
import com.a.a.ag;
import com.a.a.b.b;
import com.a.a.b.f;
import com.a.a.c.a;
import com.a.a.j;
import java.lang.reflect.Type;
import java.util.Collection;

public final class c implements ag {
    private final f a;

    public c(f fVar) {
        this.a = fVar;
    }

    public af a(j jVar, a aVar) {
        Type b = aVar.b();
        Class a2 = aVar.a();
        if (!Collection.class.isAssignableFrom(a2)) {
            return null;
        }
        Type a3 = b.a(b, a2);
        return new d(this, jVar, a3, jVar.a(a.a(a3)), this.a.a(aVar));
    }
}
