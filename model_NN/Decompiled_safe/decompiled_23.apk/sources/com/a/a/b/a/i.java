package com.a.a.b.a;

import com.a.a.d.f;
import com.a.a.s;
import com.a.a.u;
import com.a.a.w;
import com.a.a.x;
import com.a.a.z;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public final class i extends f {
    private static final Writer a = new j();
    private static final z b = new z("closed");
    private final List c = new ArrayList();
    private String d;
    private u e = w.a;

    public i() {
        super(a);
    }

    private void a(u uVar) {
        if (this.d != null) {
            if (!uVar.j() || i()) {
                ((x) j()).a(this.d, uVar);
            }
            this.d = null;
        } else if (this.c.isEmpty()) {
            this.e = uVar;
        } else {
            u j = j();
            if (j instanceof s) {
                ((s) j).a(uVar);
                return;
            }
            throw new IllegalStateException();
        }
    }

    private u j() {
        return (u) this.c.get(this.c.size() - 1);
    }

    public f a(long j) {
        a(new z(Long.valueOf(j)));
        return this;
    }

    public f a(Number number) {
        if (number == null) {
            return f();
        }
        if (!g()) {
            double doubleValue = number.doubleValue();
            if (Double.isNaN(doubleValue) || Double.isInfinite(doubleValue)) {
                throw new IllegalArgumentException("JSON forbids NaN and infinities: " + number);
            }
        }
        a(new z(number));
        return this;
    }

    public f a(String str) {
        if (this.c.isEmpty() || this.d != null) {
            throw new IllegalStateException();
        } else if (j() instanceof x) {
            this.d = str;
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    public f a(boolean z) {
        a(new z(Boolean.valueOf(z)));
        return this;
    }

    public u a() {
        if (this.c.isEmpty()) {
            return this.e;
        }
        throw new IllegalStateException("Expected one JSON element but was " + this.c);
    }

    public f b() {
        s sVar = new s();
        a(sVar);
        this.c.add(sVar);
        return this;
    }

    public f b(String str) {
        if (str == null) {
            return f();
        }
        a(new z(str));
        return this;
    }

    public f c() {
        if (this.c.isEmpty() || this.d != null) {
            throw new IllegalStateException();
        } else if (j() instanceof s) {
            this.c.remove(this.c.size() - 1);
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    public void close() {
        if (!this.c.isEmpty()) {
            throw new IOException("Incomplete document");
        }
        this.c.add(b);
    }

    public f d() {
        x xVar = new x();
        a(xVar);
        this.c.add(xVar);
        return this;
    }

    public f e() {
        if (this.c.isEmpty() || this.d != null) {
            throw new IllegalStateException();
        } else if (j() instanceof x) {
            this.c.remove(this.c.size() - 1);
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    public f f() {
        a(w.a);
        return this;
    }

    public void flush() {
    }
}
