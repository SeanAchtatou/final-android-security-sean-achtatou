package com.a.a.b;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

class h implements s {
    final /* synthetic */ Constructor a;
    final /* synthetic */ f b;

    h(f fVar, Constructor constructor) {
        this.b = fVar;
        this.a = constructor;
    }

    public Object a() {
        try {
            return this.a.newInstance(null);
        } catch (InstantiationException e) {
            throw new RuntimeException("Failed to invoke " + this.a + " with no args", e);
        } catch (InvocationTargetException e2) {
            throw new RuntimeException("Failed to invoke " + this.a + " with no args", e2.getTargetException());
        } catch (IllegalAccessException e3) {
            throw new AssertionError(e3);
        }
    }
}
