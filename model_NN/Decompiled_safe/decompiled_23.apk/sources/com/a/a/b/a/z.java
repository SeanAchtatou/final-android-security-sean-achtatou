package com.a.a.b.a;

import com.a.a.af;
import com.a.a.d.a;
import com.a.a.d.f;

final class z extends af {
    z() {
    }

    /* renamed from: a */
    public Class b(a aVar) {
        throw new UnsupportedOperationException("Attempted to deserialize a java.lang.Class. Forgot to register a type adapter?");
    }

    public void a(f fVar, Class cls) {
        throw new UnsupportedOperationException("Attempted to serialize java.lang.Class: " + cls.getName() + ". Forgot to register a type adapter?");
    }
}
