package com.a.a.b.a;

import com.a.a.af;
import com.a.a.ag;
import com.a.a.u;
import java.net.InetAddress;
import java.net.URI;
import java.net.URL;
import java.util.BitSet;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.UUID;

public final class y {
    public static final af A = new ah();
    public static final ag B = a(StringBuffer.class, A);
    public static final af C = new ai();
    public static final ag D = a(URL.class, C);
    public static final af E = new aj();
    public static final ag F = a(URI.class, E);
    public static final af G = new al();
    public static final ag H = b(InetAddress.class, G);
    public static final af I = new am();
    public static final ag J = a(UUID.class, I);
    public static final ag K = new an();
    public static final af L = new ap();
    public static final ag M = b(Calendar.class, GregorianCalendar.class, L);
    public static final af N = new aq();
    public static final ag O = a(Locale.class, N);
    public static final af P = new ar();
    public static final ag Q = a(u.class, P);
    public static final ag R = a();
    public static final af a = new z();
    public static final ag b = a(Class.class, a);
    public static final af c = new ak();
    public static final ag d = a(BitSet.class, c);
    public static final af e = new av();
    public static final af f = new az();
    public static final ag g = a(Boolean.TYPE, Boolean.class, e);
    public static final af h = new ba();
    public static final ag i = a(Byte.TYPE, Byte.class, h);
    public static final af j = new bb();
    public static final ag k = a(Short.TYPE, Short.class, j);
    public static final af l = new bc();
    public static final ag m = a(Integer.TYPE, Integer.class, l);
    public static final af n = new bd();
    public static final af o = new be();
    public static final af p = new aa();
    public static final af q = new ab();
    public static final ag r = a(Number.class, q);
    public static final af s = new ac();
    public static final ag t = a(Character.TYPE, Character.class, s);
    public static final af u = new ad();
    public static final af v = new ae();
    public static final af w = new af();
    public static final ag x = a(String.class, u);
    public static final af y = new ag();
    public static final ag z = a(StringBuilder.class, y);

    public static ag a() {
        return new as();
    }

    public static ag a(Class cls, af afVar) {
        return new at(cls, afVar);
    }

    public static ag a(Class cls, Class cls2, af afVar) {
        return new au(cls, cls2, afVar);
    }

    public static ag b(Class cls, af afVar) {
        return new ax(cls, afVar);
    }

    public static ag b(Class cls, Class cls2, af afVar) {
        return new aw(cls, cls2, afVar);
    }
}
