package com.a.a.b.a;

import com.a.a.ab;
import com.a.a.af;
import com.a.a.ag;
import com.a.a.d.a;
import com.a.a.d.f;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public final class e extends af {
    public static final ag a = new f();
    private final DateFormat b = DateFormat.getDateTimeInstance(2, 2, Locale.US);
    private final DateFormat c = DateFormat.getDateTimeInstance(2, 2);
    private final DateFormat d = a();

    private static DateFormat a() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return simpleDateFormat;
    }

    private synchronized Date a(String str) {
        Date parse;
        try {
            parse = this.c.parse(str);
        } catch (ParseException e) {
            try {
                parse = this.b.parse(str);
            } catch (ParseException e2) {
                try {
                    parse = this.d.parse(str);
                } catch (ParseException e3) {
                    throw new ab(str, e3);
                }
            }
        }
        return parse;
    }

    /* renamed from: a */
    public Date b(a aVar) {
        if (aVar.f() != com.a.a.d.e.NULL) {
            return a(aVar.h());
        }
        aVar.j();
        return null;
    }

    public synchronized void a(f fVar, Date date) {
        if (date == null) {
            fVar.f();
        } else {
            fVar.b(this.b.format(date));
        }
    }
}
