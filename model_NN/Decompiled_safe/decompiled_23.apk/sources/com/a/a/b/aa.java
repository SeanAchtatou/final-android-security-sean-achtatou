package com.a.a.b;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;

final class aa extends AbstractSet {
    final /* synthetic */ y a;

    private aa(y yVar) {
        this.a = yVar;
    }

    public void clear() {
        this.a.clear();
    }

    public boolean contains(Object obj) {
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        Object obj2 = this.a.get(entry.getKey());
        return obj2 != null && obj2.equals(entry.getValue());
    }

    public Iterator iterator() {
        return new ab(this);
    }

    public boolean remove(Object obj) {
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        return this.a.a(entry.getKey(), entry.getValue());
    }

    public int size() {
        return this.a.d;
    }
}
