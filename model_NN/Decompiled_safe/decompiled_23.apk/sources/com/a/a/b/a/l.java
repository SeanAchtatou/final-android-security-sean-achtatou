package com.a.a.b.a;

import com.a.a.ab;
import com.a.a.af;
import com.a.a.b.q;
import com.a.a.b.s;
import com.a.a.d.a;
import com.a.a.d.e;
import com.a.a.d.f;
import com.a.a.j;
import com.a.a.u;
import com.a.a.z;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Map;

final class l extends af {
    final /* synthetic */ k a;
    private final af b;
    private final af c;
    private final s d;

    public l(k kVar, j jVar, Type type, af afVar, Type type2, af afVar2, s sVar) {
        this.a = kVar;
        this.b = new x(jVar, afVar, type);
        this.c = new x(jVar, afVar2, type2);
        this.d = sVar;
    }

    private String a(u uVar) {
        if (uVar.i()) {
            z m = uVar.m();
            if (m.p()) {
                return String.valueOf(m.a());
            }
            if (m.o()) {
                return Boolean.toString(m.f());
            }
            if (m.q()) {
                return m.b();
            }
            throw new AssertionError();
        } else if (uVar.j()) {
            return "null";
        } else {
            throw new AssertionError();
        }
    }

    /* renamed from: a */
    public Map b(a aVar) {
        e f = aVar.f();
        if (f == e.NULL) {
            aVar.j();
            return null;
        }
        Map map = (Map) this.d.a();
        if (f == e.BEGIN_ARRAY) {
            aVar.a();
            while (aVar.e()) {
                aVar.a();
                Object b2 = this.b.b(aVar);
                if (map.put(b2, this.c.b(aVar)) != null) {
                    throw new ab("duplicate key: " + b2);
                }
                aVar.b();
            }
            aVar.b();
            return map;
        }
        aVar.c();
        while (aVar.e()) {
            q.a.a(aVar);
            Object b3 = this.b.b(aVar);
            if (map.put(b3, this.c.b(aVar)) != null) {
                throw new ab("duplicate key: " + b3);
            }
        }
        aVar.d();
        return map;
    }

    public void a(f fVar, Map map) {
        int i = 0;
        if (map == null) {
            fVar.f();
        } else if (!this.a.b) {
            fVar.d();
            for (Map.Entry entry : map.entrySet()) {
                fVar.a(String.valueOf(entry.getKey()));
                this.c.a(fVar, entry.getValue());
            }
            fVar.e();
        } else {
            ArrayList arrayList = new ArrayList(map.size());
            ArrayList arrayList2 = new ArrayList(map.size());
            boolean z = false;
            for (Map.Entry entry2 : map.entrySet()) {
                u a2 = this.b.a(entry2.getKey());
                arrayList.add(a2);
                arrayList2.add(entry2.getValue());
                z = (a2.g() || a2.h()) | z;
            }
            if (z) {
                fVar.b();
                while (i < arrayList.size()) {
                    fVar.b();
                    com.a.a.b.u.a((u) arrayList.get(i), fVar);
                    this.c.a(fVar, arrayList2.get(i));
                    fVar.c();
                    i++;
                }
                fVar.c();
                return;
            }
            fVar.d();
            while (i < arrayList.size()) {
                fVar.a(a((u) arrayList.get(i)));
                this.c.a(fVar, arrayList2.get(i));
                i++;
            }
            fVar.e();
        }
    }
}
