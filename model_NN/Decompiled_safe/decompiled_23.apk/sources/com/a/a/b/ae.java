package com.a.a.b;

import java.util.Map;

class ae implements Map.Entry {
    final String a;
    Object b;
    final int c;
    ae d;
    ae e;
    ae f;

    ae() {
        this(null, null, 0, null, null, null);
        this.f = this;
        this.e = this;
    }

    ae(String str, Object obj, int i, ae aeVar, ae aeVar2, ae aeVar3) {
        this.a = str;
        this.b = obj;
        this.c = i;
        this.d = aeVar;
        this.e = aeVar2;
        this.f = aeVar3;
    }

    /* renamed from: a */
    public final String getKey() {
        return this.a;
    }

    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean equals(java.lang.Object r5) {
        /*
            r4 = this;
            r0 = 0
            boolean r1 = r5 instanceof java.util.Map.Entry
            if (r1 != 0) goto L_0x0006
        L_0x0005:
            return r0
        L_0x0006:
            java.util.Map$Entry r5 = (java.util.Map.Entry) r5
            java.lang.Object r1 = r5.getValue()
            java.lang.String r2 = r4.a
            java.lang.Object r3 = r5.getKey()
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x0005
            java.lang.Object r2 = r4.b
            if (r2 != 0) goto L_0x0020
            if (r1 != 0) goto L_0x0005
        L_0x001e:
            r0 = 1
            goto L_0x0005
        L_0x0020:
            java.lang.Object r2 = r4.b
            boolean r1 = r2.equals(r1)
            if (r1 == 0) goto L_0x0005
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.a.b.ae.equals(java.lang.Object):boolean");
    }

    public final Object getValue() {
        return this.b;
    }

    public final int hashCode() {
        int i = 0;
        int hashCode = this.a == null ? 0 : this.a.hashCode();
        if (this.b != null) {
            i = this.b.hashCode();
        }
        return hashCode ^ i;
    }

    public final Object setValue(Object obj) {
        Object obj2 = this.b;
        this.b = obj;
        return obj2;
    }

    public final String toString() {
        return this.a + "=" + this.b;
    }
}
