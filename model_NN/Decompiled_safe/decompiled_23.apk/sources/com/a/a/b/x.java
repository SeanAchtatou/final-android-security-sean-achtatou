package com.a.a.b;

class x implements CharSequence {
    char[] a;

    x() {
    }

    public char charAt(int i) {
        return this.a[i];
    }

    public int length() {
        return this.a.length;
    }

    public CharSequence subSequence(int i, int i2) {
        return new String(this.a, i, i2 - i);
    }
}
