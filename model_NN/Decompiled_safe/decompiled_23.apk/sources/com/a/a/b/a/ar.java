package com.a.a.b.a;

import com.a.a.af;
import com.a.a.b.r;
import com.a.a.d.a;
import com.a.a.d.f;
import com.a.a.s;
import com.a.a.u;
import com.a.a.w;
import com.a.a.x;
import com.a.a.z;
import java.util.Iterator;
import java.util.Map;

final class ar extends af {
    ar() {
    }

    /* renamed from: a */
    public u b(a aVar) {
        switch (aVar.f()) {
            case NUMBER:
                return new z(new r(aVar.h()));
            case BOOLEAN:
                return new z(Boolean.valueOf(aVar.i()));
            case STRING:
                return new z(aVar.h());
            case NULL:
                aVar.j();
                return w.a;
            case BEGIN_ARRAY:
                s sVar = new s();
                aVar.a();
                while (aVar.e()) {
                    sVar.a(b(aVar));
                }
                aVar.b();
                return sVar;
            case BEGIN_OBJECT:
                x xVar = new x();
                aVar.c();
                while (aVar.e()) {
                    xVar.a(aVar.g(), b(aVar));
                }
                aVar.d();
                return xVar;
            default:
                throw new IllegalArgumentException();
        }
    }

    public void a(f fVar, u uVar) {
        if (uVar == null || uVar.j()) {
            fVar.f();
        } else if (uVar.i()) {
            z m = uVar.m();
            if (m.p()) {
                fVar.a(m.a());
            } else if (m.o()) {
                fVar.a(m.f());
            } else {
                fVar.b(m.b());
            }
        } else if (uVar.g()) {
            fVar.b();
            Iterator it = uVar.l().iterator();
            while (it.hasNext()) {
                a(fVar, (u) it.next());
            }
            fVar.c();
        } else if (uVar.h()) {
            fVar.d();
            for (Map.Entry entry : uVar.k().o()) {
                fVar.a((String) entry.getKey());
                a(fVar, (u) entry.getValue());
            }
            fVar.e();
        } else {
            throw new IllegalArgumentException("Couldn't write " + uVar.getClass());
        }
    }
}
