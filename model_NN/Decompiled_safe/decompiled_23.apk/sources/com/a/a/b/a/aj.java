package com.a.a.b.a;

import com.a.a.af;
import com.a.a.d.a;
import com.a.a.d.e;
import com.a.a.d.f;
import com.a.a.v;
import java.net.URI;
import java.net.URISyntaxException;

final class aj extends af {
    aj() {
    }

    /* renamed from: a */
    public URI b(a aVar) {
        if (aVar.f() == e.NULL) {
            aVar.j();
            return null;
        }
        try {
            String h = aVar.h();
            if (!"null".equals(h)) {
                return new URI(h);
            }
            return null;
        } catch (URISyntaxException e) {
            throw new v(e);
        }
    }

    public void a(f fVar, URI uri) {
        fVar.b(uri == null ? null : uri.toASCIIString());
    }
}
