package com.a.a.b.a;

import com.a.a.a.b;
import com.a.a.af;
import com.a.a.ag;
import com.a.a.b.f;
import com.a.a.b.o;
import com.a.a.b.t;
import com.a.a.c.a;
import com.a.a.i;
import com.a.a.j;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.LinkedHashMap;
import java.util.Map;

public final class p implements ag {
    private final f a;
    private final i b;
    private final o c;

    public p(f fVar, i iVar, o oVar) {
        this.a = fVar;
        this.b = iVar;
        this.c = oVar;
    }

    private s a(j jVar, Field field, String str, a aVar, boolean z, boolean z2) {
        return new q(this, str, z, z2, jVar, aVar, field, t.a((Type) aVar.a()));
    }

    private String a(Field field) {
        b bVar = (b) field.getAnnotation(b.class);
        return bVar == null ? this.b.a(field) : bVar.a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.b.a.p.a(java.lang.reflect.Field, boolean):boolean
     arg types: [java.lang.reflect.Field, int]
     candidates:
      com.a.a.b.a.p.a(com.a.a.j, com.a.a.c.a):com.a.a.af
      com.a.a.ag.a(com.a.a.j, com.a.a.c.a):com.a.a.af
      com.a.a.b.a.p.a(java.lang.reflect.Field, boolean):boolean */
    private Map a(j jVar, a aVar, Class cls) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        if (cls.isInterface()) {
            return linkedHashMap;
        }
        Type b2 = aVar.b();
        while (cls != Object.class) {
            for (Field field : cls.getDeclaredFields()) {
                boolean a2 = a(field, true);
                boolean a3 = a(field, false);
                if (a2 || a3) {
                    field.setAccessible(true);
                    s a4 = a(jVar, field, a(field), a.a(com.a.a.b.b.a(aVar.b(), cls, field.getGenericType())), a2, a3);
                    s sVar = (s) linkedHashMap.put(a4.g, a4);
                    if (sVar != null) {
                        throw new IllegalArgumentException(b2 + " declares multiple JSON fields named " + sVar.g);
                    }
                }
            }
            aVar = a.a(com.a.a.b.b.a(aVar.b(), cls, cls.getGenericSuperclass()));
            cls = aVar.a();
        }
        return linkedHashMap;
    }

    public af a(j jVar, a aVar) {
        Class a2 = aVar.a();
        if (!Object.class.isAssignableFrom(a2)) {
            return null;
        }
        return new r(this, this.a.a(aVar), a(jVar, aVar, a2), null);
    }

    public boolean a(Field field, boolean z) {
        return !this.c.a(field.getType(), z) && !this.c.a(field, z);
    }
}
