package com.a.a.b.a;

import com.a.a.af;
import com.a.a.c.a;
import com.a.a.d.f;
import com.a.a.j;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;

final class x extends af {
    private final j a;
    private final af b;
    private final Type c;

    x(j jVar, af afVar, Type type) {
        this.a = jVar;
        this.b = afVar;
        this.c = type;
    }

    private Type a(Type type, Object obj) {
        return obj != null ? (type == Object.class || (type instanceof TypeVariable) || (type instanceof Class)) ? obj.getClass() : type : type;
    }

    public void a(f fVar, Object obj) {
        af afVar = this.b;
        Type a2 = a(this.c, obj);
        if (a2 != this.c) {
            afVar = this.a.a(a.a(a2));
            if ((afVar instanceof r) && !(this.b instanceof r)) {
                afVar = this.b;
            }
        }
        afVar.a(fVar, obj);
    }

    public Object b(com.a.a.d.a aVar) {
        return this.b.b(aVar);
    }
}
