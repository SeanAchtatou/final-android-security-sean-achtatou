package com.a.a.b.a;

import com.a.a.af;
import com.a.a.ag;
import com.a.a.b.b;
import com.a.a.b.f;
import com.a.a.c.a;
import com.a.a.j;
import java.lang.reflect.Type;
import java.util.Map;

public final class k implements ag {
    private final f a;
    /* access modifiers changed from: private */
    public final boolean b;

    public k(f fVar, boolean z) {
        this.a = fVar;
        this.b = z;
    }

    private af a(j jVar, Type type) {
        return (type == Boolean.TYPE || type == Boolean.class) ? y.f : jVar.a(a.a(type));
    }

    public af a(j jVar, a aVar) {
        Type b2 = aVar.b();
        if (!Map.class.isAssignableFrom(aVar.a())) {
            return null;
        }
        Type[] b3 = b.b(b2, b.e(b2));
        return new l(this, jVar, b3[0], a(jVar, b3[0]), b3[1], jVar.a(a.a(b3[1])), this.a.a(aVar));
    }
}
