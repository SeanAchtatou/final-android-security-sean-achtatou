package com.a.a.b;

import java.io.Serializable;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Type;

final class c implements Serializable, GenericArrayType {
    private final Type a;

    public c(Type type) {
        this.a = b.d(type);
    }

    public boolean equals(Object obj) {
        return (obj instanceof GenericArrayType) && b.a(this, (GenericArrayType) obj);
    }

    public Type getGenericComponentType() {
        return this.a;
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    public String toString() {
        return b.f(this.a) + "[]";
    }
}
