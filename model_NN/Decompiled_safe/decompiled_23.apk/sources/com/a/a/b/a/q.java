package com.a.a.b.a;

import com.a.a.af;
import com.a.a.c.a;
import com.a.a.d.f;
import com.a.a.j;
import java.lang.reflect.Field;

class q extends s {
    final af a = this.b.a(this.c);
    final /* synthetic */ j b;
    final /* synthetic */ a c;
    final /* synthetic */ Field d;
    final /* synthetic */ boolean e;
    final /* synthetic */ p f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    q(p pVar, String str, boolean z, boolean z2, j jVar, a aVar, Field field, boolean z3) {
        super(str, z, z2);
        this.f = pVar;
        this.b = jVar;
        this.c = aVar;
        this.d = field;
        this.e = z3;
    }

    /* access modifiers changed from: package-private */
    public void a(com.a.a.d.a aVar, Object obj) {
        Object b2 = this.a.b(aVar);
        if (b2 != null || !this.e) {
            this.d.set(obj, b2);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(f fVar, Object obj) {
        new x(this.b, this.a, this.c.b()).a(fVar, this.d.get(obj));
    }
}
