package com.a.a.b;

import java.util.AbstractCollection;
import java.util.Iterator;

final class ag extends AbstractCollection {
    final /* synthetic */ y a;

    private ag(y yVar) {
        this.a = yVar;
    }

    public void clear() {
        this.a.clear();
    }

    public boolean contains(Object obj) {
        return this.a.containsValue(obj);
    }

    public Iterator iterator() {
        return new ah(this);
    }

    public int size() {
        return this.a.d;
    }
}
