package com.a.a.b.a;

import com.a.a.af;
import com.a.a.ag;
import com.a.a.c.a;
import com.a.a.j;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Type;

final class b implements ag {
    b() {
    }

    public af a(j jVar, a aVar) {
        Type b = aVar.b();
        if (!(b instanceof GenericArrayType) && (!(b instanceof Class) || !((Class) b).isArray())) {
            return null;
        }
        Type g = com.a.a.b.b.g(b);
        return new a(jVar, jVar.a(a.a(g)), com.a.a.b.b.e(g));
    }
}
