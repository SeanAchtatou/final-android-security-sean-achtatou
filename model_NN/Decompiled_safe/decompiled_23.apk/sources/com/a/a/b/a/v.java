package com.a.a.b.a;

import com.a.a.ab;
import com.a.a.af;
import com.a.a.ag;
import com.a.a.d.a;
import com.a.a.d.e;
import com.a.a.d.f;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public final class v extends af {
    public static final ag a = new w();
    private final DateFormat b = new SimpleDateFormat("hh:mm:ss a");

    /* renamed from: a */
    public synchronized Time b(a aVar) {
        Time time;
        if (aVar.f() == e.NULL) {
            aVar.j();
            time = null;
        } else {
            try {
                time = new Time(this.b.parse(aVar.h()).getTime());
            } catch (ParseException e) {
                throw new ab(e);
            }
        }
        return time;
    }

    public synchronized void a(f fVar, Time time) {
        fVar.b(time == null ? null : this.b.format(time));
    }
}
