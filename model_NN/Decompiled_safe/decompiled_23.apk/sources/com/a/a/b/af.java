package com.a.a.b;

import java.util.Iterator;
import java.util.NoSuchElementException;

abstract class af implements Iterator {
    ae b;
    ae c;
    final /* synthetic */ y d;

    private af(y yVar) {
        this.d = yVar;
        this.b = this.d.a.e;
        this.c = null;
    }

    /* access modifiers changed from: package-private */
    public final ae b() {
        ae aeVar = this.b;
        if (aeVar == this.d.a) {
            throw new NoSuchElementException();
        }
        this.b = aeVar.e;
        this.c = aeVar;
        return aeVar;
    }

    public final boolean hasNext() {
        return this.b != this.d.a;
    }

    public final void remove() {
        if (this.c == null) {
            throw new IllegalStateException();
        }
        this.d.remove(this.c.a);
        this.c = null;
    }
}
