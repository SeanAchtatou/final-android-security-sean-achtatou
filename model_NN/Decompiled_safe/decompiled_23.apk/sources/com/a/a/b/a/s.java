package com.a.a.b.a;

import com.a.a.d.a;
import com.a.a.d.f;

abstract class s {
    final String g;
    final boolean h;
    final boolean i;

    protected s(String str, boolean z, boolean z2) {
        this.g = str;
        this.h = z;
        this.i = z2;
    }

    /* access modifiers changed from: package-private */
    public abstract void a(a aVar, Object obj);

    /* access modifiers changed from: package-private */
    public abstract void a(f fVar, Object obj);
}
