package com.a.a.b.a;

import com.a.a.ab;
import com.a.a.d.a;
import com.a.a.d.e;
import com.a.a.d.f;
import java.math.BigInteger;

final class af extends com.a.a.af {
    af() {
    }

    /* renamed from: a */
    public BigInteger b(a aVar) {
        if (aVar.f() == e.NULL) {
            aVar.j();
            return null;
        }
        try {
            return new BigInteger(aVar.h());
        } catch (NumberFormatException e) {
            throw new ab(e);
        }
    }

    public void a(f fVar, BigInteger bigInteger) {
        fVar.a(bigInteger);
    }
}
