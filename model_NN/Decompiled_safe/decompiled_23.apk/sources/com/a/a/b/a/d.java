package com.a.a.b.a;

import com.a.a.af;
import com.a.a.b.s;
import com.a.a.d.a;
import com.a.a.d.e;
import com.a.a.d.f;
import com.a.a.j;
import java.lang.reflect.Type;
import java.util.Collection;

final class d extends af {
    final /* synthetic */ c a;
    private final af b;
    private final s c;

    public d(c cVar, j jVar, Type type, af afVar, s sVar) {
        this.a = cVar;
        this.b = new x(jVar, afVar, type);
        this.c = sVar;
    }

    /* renamed from: a */
    public Collection b(a aVar) {
        if (aVar.f() == e.NULL) {
            aVar.j();
            return null;
        }
        Collection collection = (Collection) this.c.a();
        aVar.a();
        while (aVar.e()) {
            collection.add(this.b.b(aVar));
        }
        aVar.b();
        return collection;
    }

    public void a(f fVar, Collection collection) {
        if (collection == null) {
            fVar.f();
            return;
        }
        fVar.b();
        for (Object a2 : collection) {
            this.b.a(fVar, a2);
        }
        fVar.c();
    }
}
