package com.a.a.b.a;

import com.a.a.ab;
import com.a.a.af;
import com.a.a.b.s;
import com.a.a.d.a;
import com.a.a.d.e;
import com.a.a.d.f;
import java.util.Map;

public final class r extends af {
    final /* synthetic */ p a;
    private final s b;
    private final Map c;

    private r(p pVar, s sVar, Map map) {
        this.a = pVar;
        this.b = sVar;
        this.c = map;
    }

    /* synthetic */ r(p pVar, s sVar, Map map, q qVar) {
        this(pVar, sVar, map);
    }

    public void a(f fVar, Object obj) {
        if (obj == null) {
            fVar.f();
            return;
        }
        fVar.d();
        try {
            for (s sVar : this.c.values()) {
                if (sVar.h) {
                    fVar.a(sVar.g);
                    sVar.a(fVar, obj);
                }
            }
            fVar.e();
        } catch (IllegalAccessException e) {
            throw new AssertionError();
        }
    }

    public Object b(a aVar) {
        if (aVar.f() == e.NULL) {
            aVar.j();
            return null;
        }
        Object a2 = this.b.a();
        try {
            aVar.c();
            while (aVar.e()) {
                s sVar = (s) this.c.get(aVar.g());
                if (sVar == null || !sVar.i) {
                    aVar.n();
                } else {
                    sVar.a(aVar, a2);
                }
            }
            aVar.d();
            return a2;
        } catch (IllegalStateException e) {
            throw new ab(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        }
    }
}
