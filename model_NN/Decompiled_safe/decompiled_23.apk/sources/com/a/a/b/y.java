package com.a.a.b;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public final class y extends AbstractMap {
    private static final Map.Entry[] b = new ae[2];
    private static final int i = new Random().nextInt();
    /* access modifiers changed from: private */
    public ae a = new ae();
    private ae[] c = ((ae[]) b);
    /* access modifiers changed from: private */
    public int d;
    private int e = -1;
    private Set f;
    private Set g;
    private Collection h;

    private ae a(String str) {
        if (str == null) {
            return null;
        }
        int b2 = b(str);
        ae[] aeVarArr = this.c;
        for (ae aeVar = aeVarArr[(aeVarArr.length - 1) & b2]; aeVar != null; aeVar = aeVar.d) {
            String str2 = aeVar.a;
            if (str2 == str || (aeVar.c == b2 && str.equals(str2))) {
                return aeVar;
            }
        }
        return null;
    }

    private void a(ae aeVar) {
        aeVar.f.e = aeVar.e;
        aeVar.e.f = aeVar.f;
        aeVar.f = null;
        aeVar.e = null;
    }

    private void a(String str, Object obj, int i2, int i3) {
        ae aeVar = this.a;
        ae aeVar2 = aeVar.f;
        ae aeVar3 = new ae(str, obj, i2, this.c[i3], aeVar, aeVar2);
        ae[] aeVarArr = this.c;
        aeVar.f = aeVar3;
        aeVar2.e = aeVar3;
        aeVarArr[i3] = aeVar3;
    }

    /* access modifiers changed from: private */
    public boolean a(Object obj, Object obj2) {
        if (obj == null || !(obj instanceof String)) {
            return false;
        }
        int b2 = b((String) obj);
        ae[] aeVarArr = this.c;
        int length = b2 & (aeVarArr.length - 1);
        ae aeVar = aeVarArr[length];
        ae aeVar2 = null;
        while (aeVar != null) {
            if (aeVar.c != b2 || !obj.equals(aeVar.a)) {
                ae aeVar3 = aeVar;
                aeVar = aeVar.d;
                aeVar2 = aeVar3;
            } else if (obj2 != null ? !obj2.equals(aeVar.b) : aeVar.b != null) {
                return false;
            } else {
                if (aeVar2 == null) {
                    aeVarArr[length] = aeVar.d;
                } else {
                    aeVar2.d = aeVar.d;
                }
                this.d--;
                a(aeVar);
                return true;
            }
        }
        return false;
    }

    private ae[] a() {
        int i2;
        ae[] aeVarArr = this.c;
        int length = aeVarArr.length;
        if (length == 1073741824) {
            return aeVarArr;
        }
        ae[] a2 = a(length * 2);
        if (this.d == 0) {
            return a2;
        }
        for (int i3 = 0; i3 < length; i3++) {
            ae aeVar = aeVarArr[i3];
            if (aeVar != null) {
                int i4 = aeVar.c & length;
                a2[i3 | i4] = aeVar;
                ae aeVar2 = null;
                ae aeVar3 = aeVar;
                for (ae aeVar4 = aeVar.d; aeVar4 != null; aeVar4 = aeVar4.d) {
                    int i5 = aeVar4.c & length;
                    if (i5 != i4) {
                        if (aeVar2 == null) {
                            a2[i3 | i5] = aeVar4;
                        } else {
                            aeVar2.d = aeVar4;
                        }
                        i2 = i5;
                    } else {
                        aeVar3 = aeVar2;
                        i2 = i4;
                    }
                    i4 = i2;
                    aeVar2 = aeVar3;
                    aeVar3 = aeVar4;
                }
                if (aeVar2 != null) {
                    aeVar2.d = null;
                }
            }
        }
        return a2;
    }

    private ae[] a(int i2) {
        ae[] aeVarArr = new ae[i2];
        this.c = aeVarArr;
        this.e = (i2 >> 1) + (i2 >> 2);
        return aeVarArr;
    }

    private static int b(String str) {
        int i2 = i;
        for (int i3 = 0; i3 < str.length(); i3++) {
            int charAt = i2 + str.charAt(i3);
            int i4 = (charAt + charAt) << 10;
            i2 = i4 ^ (i4 >>> 6);
        }
        int i5 = ((i2 >>> 20) ^ (i2 >>> 12)) ^ i2;
        return (i5 >>> 4) ^ ((i5 >>> 7) ^ i5);
    }

    /* renamed from: a */
    public Object put(String str, Object obj) {
        if (str == null) {
            throw new NullPointerException("key == null");
        }
        int b2 = b(str);
        ae[] aeVarArr = this.c;
        int length = (aeVarArr.length - 1) & b2;
        ae aeVar = aeVarArr[length];
        while (aeVar != null) {
            if (aeVar.c != b2 || !str.equals(aeVar.a)) {
                aeVar = aeVar.d;
            } else {
                Object obj2 = aeVar.b;
                aeVar.b = obj;
                return obj2;
            }
        }
        int i2 = this.d;
        this.d = i2 + 1;
        if (i2 > this.e) {
            length = (a().length - 1) & b2;
        }
        a(str, obj, b2, length);
        return null;
    }

    public void clear() {
        if (this.d != 0) {
            Arrays.fill(this.c, (Object) null);
            this.d = 0;
        }
        ae aeVar = this.a;
        ae aeVar2 = aeVar.e;
        while (aeVar2 != aeVar) {
            ae aeVar3 = aeVar2.e;
            aeVar2.f = null;
            aeVar2.e = null;
            aeVar2 = aeVar3;
        }
        aeVar.f = aeVar;
        aeVar.e = aeVar;
    }

    public boolean containsKey(Object obj) {
        return (obj instanceof String) && a((String) obj) != null;
    }

    public Set entrySet() {
        Set set = this.g;
        if (set != null) {
            return set;
        }
        aa aaVar = new aa(this);
        this.g = aaVar;
        return aaVar;
    }

    public Object get(Object obj) {
        ae a2;
        if (!(obj instanceof String) || (a2 = a((String) obj)) == null) {
            return null;
        }
        return a2.b;
    }

    public Set keySet() {
        Set set = this.f;
        if (set != null) {
            return set;
        }
        ac acVar = new ac(this);
        this.f = acVar;
        return acVar;
    }

    public Object remove(Object obj) {
        if (obj == null || !(obj instanceof String)) {
            return null;
        }
        int b2 = b((String) obj);
        ae[] aeVarArr = this.c;
        int length = b2 & (aeVarArr.length - 1);
        ae aeVar = aeVarArr[length];
        ae aeVar2 = null;
        while (aeVar != null) {
            if (aeVar.c != b2 || !obj.equals(aeVar.a)) {
                ae aeVar3 = aeVar;
                aeVar = aeVar.d;
                aeVar2 = aeVar3;
            } else {
                if (aeVar2 == null) {
                    aeVarArr[length] = aeVar.d;
                } else {
                    aeVar2.d = aeVar.d;
                }
                this.d--;
                a(aeVar);
                return aeVar.b;
            }
        }
        return null;
    }

    public int size() {
        return this.d;
    }

    public Collection values() {
        Collection collection = this.h;
        if (collection != null) {
            return collection;
        }
        ag agVar = new ag(this);
        this.h = agVar;
        return agVar;
    }
}
