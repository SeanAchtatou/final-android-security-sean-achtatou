package com.a.a.b.a;

import com.a.a.af;
import com.a.a.d.a;
import com.a.a.d.e;
import com.a.a.d.f;
import java.net.InetAddress;

final class al extends af {
    al() {
    }

    /* renamed from: a */
    public InetAddress b(a aVar) {
        if (aVar.f() != e.NULL) {
            return InetAddress.getByName(aVar.h());
        }
        aVar.j();
        return null;
    }

    public void a(f fVar, InetAddress inetAddress) {
        fVar.b(inetAddress == null ? null : inetAddress.getHostAddress());
    }
}
