package com.a.a.b.a;

import com.a.a.ab;
import com.a.a.af;
import com.a.a.d.a;
import com.a.a.d.e;
import com.a.a.d.f;

final class ac extends af {
    ac() {
    }

    /* renamed from: a */
    public Character b(a aVar) {
        if (aVar.f() == e.NULL) {
            aVar.j();
            return null;
        }
        String h = aVar.h();
        if (h.length() == 1) {
            return Character.valueOf(h.charAt(0));
        }
        throw new ab("Expecting character, got: " + h);
    }

    public void a(f fVar, Character ch) {
        fVar.b(ch == null ? null : String.valueOf(ch));
    }
}
