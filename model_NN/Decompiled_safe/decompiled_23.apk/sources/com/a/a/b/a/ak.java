package com.a.a.b.a;

import com.a.a.ab;
import com.a.a.af;
import com.a.a.d.a;
import com.a.a.d.e;
import com.a.a.d.f;
import java.util.BitSet;

final class ak extends af {
    ak() {
    }

    /* renamed from: a */
    public BitSet b(a aVar) {
        boolean z;
        if (aVar.f() == e.NULL) {
            aVar.j();
            return null;
        }
        BitSet bitSet = new BitSet();
        aVar.a();
        e f = aVar.f();
        int i = 0;
        while (f != e.END_ARRAY) {
            switch (f) {
                case NUMBER:
                    if (aVar.m() == 0) {
                        z = false;
                        break;
                    } else {
                        z = true;
                        break;
                    }
                case BOOLEAN:
                    z = aVar.i();
                    break;
                case STRING:
                    String h = aVar.h();
                    try {
                        if (Integer.parseInt(h) == 0) {
                            z = false;
                            break;
                        } else {
                            z = true;
                            break;
                        }
                    } catch (NumberFormatException e) {
                        throw new ab("Error: Expecting: bitset number value (1, 0), Found: " + h);
                    }
                default:
                    throw new ab("Invalid bitset value type: " + f);
            }
            if (z) {
                bitSet.set(i);
            }
            i++;
            f = aVar.f();
        }
        aVar.b();
        return bitSet;
    }

    public void a(f fVar, BitSet bitSet) {
        if (bitSet == null) {
            fVar.f();
            return;
        }
        fVar.b();
        for (int i = 0; i < bitSet.length(); i++) {
            fVar.a((long) (bitSet.get(i) ? 1 : 0));
        }
        fVar.c();
    }
}
