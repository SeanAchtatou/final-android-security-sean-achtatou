package com.a.a.b;

import com.a.a.c.a;
import com.a.a.r;
import java.lang.reflect.Constructor;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.SortedSet;

public final class f {
    private final Map a;

    public f() {
        this(Collections.emptyMap());
    }

    public f(Map map) {
        this.a = map;
    }

    private s a(Class cls) {
        try {
            Constructor declaredConstructor = cls.getDeclaredConstructor(new Class[0]);
            if (!declaredConstructor.isAccessible()) {
                declaredConstructor.setAccessible(true);
            }
            return new h(this, declaredConstructor);
        } catch (NoSuchMethodException e) {
            return null;
        }
    }

    private s a(Type type, Class cls) {
        return new n(this, cls, type);
    }

    private s b(Class cls) {
        if (Collection.class.isAssignableFrom(cls)) {
            return SortedSet.class.isAssignableFrom(cls) ? new i(this) : Set.class.isAssignableFrom(cls) ? new j(this) : Queue.class.isAssignableFrom(cls) ? new k(this) : new l(this);
        }
        if (Map.class.isAssignableFrom(cls)) {
            return new m(this);
        }
        return null;
    }

    public s a(a aVar) {
        Type b = aVar.b();
        Class a2 = aVar.a();
        r rVar = (r) this.a.get(b);
        if (rVar != null) {
            return new g(this, rVar, b);
        }
        s a3 = a(a2);
        if (a3 != null) {
            return a3;
        }
        s b2 = b(a2);
        return b2 == null ? a(b, a2) : b2;
    }

    public String toString() {
        return this.a.toString();
    }
}
