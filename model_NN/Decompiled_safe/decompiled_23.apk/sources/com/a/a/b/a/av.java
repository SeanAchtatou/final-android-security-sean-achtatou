package com.a.a.b.a;

import com.a.a.af;
import com.a.a.d.a;
import com.a.a.d.e;
import com.a.a.d.f;

final class av extends af {
    av() {
    }

    /* renamed from: a */
    public Boolean b(a aVar) {
        if (aVar.f() != e.NULL) {
            return aVar.f() == e.STRING ? Boolean.valueOf(Boolean.parseBoolean(aVar.h())) : Boolean.valueOf(aVar.i());
        }
        aVar.j();
        return null;
    }

    public void a(f fVar, Boolean bool) {
        if (bool == null) {
            fVar.f();
        } else {
            fVar.a(bool.booleanValue());
        }
    }
}
