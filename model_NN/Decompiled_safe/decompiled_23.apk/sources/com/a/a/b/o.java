package com.a.a.b;

import com.a.a.a.c;
import com.a.a.a.d;
import com.a.a.af;
import com.a.a.ag;
import com.a.a.b;
import com.a.a.c.a;
import com.a.a.j;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.List;

public final class o implements ag, Cloneable {
    public static final o a = new o();
    private double b = -1.0d;
    private int c = 136;
    private boolean d = true;
    private boolean e;
    private List f = Collections.emptyList();
    private List g = Collections.emptyList();

    private boolean a(c cVar) {
        return cVar == null || cVar.a() <= this.b;
    }

    private boolean a(c cVar, d dVar) {
        return a(cVar) && a(dVar);
    }

    private boolean a(d dVar) {
        return dVar == null || dVar.a() > this.b;
    }

    private boolean a(Class cls) {
        return !Enum.class.isAssignableFrom(cls) && (cls.isAnonymousClass() || cls.isLocalClass());
    }

    private boolean b(Class cls) {
        return cls.isMemberClass() && !c(cls);
    }

    private boolean c(Class cls) {
        return (cls.getModifiers() & 8) != 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.b.o.a(java.lang.Class, boolean):boolean
     arg types: [java.lang.Class, int]
     candidates:
      com.a.a.b.o.a(com.a.a.a.c, com.a.a.a.d):boolean
      com.a.a.b.o.a(com.a.a.j, com.a.a.c.a):com.a.a.af
      com.a.a.b.o.a(java.lang.reflect.Field, boolean):boolean
      com.a.a.ag.a(com.a.a.j, com.a.a.c.a):com.a.a.af
      com.a.a.b.o.a(java.lang.Class, boolean):boolean */
    public af a(j jVar, a aVar) {
        Class a2 = aVar.a();
        boolean a3 = a(a2, true);
        boolean a4 = a(a2, false);
        if (a3 || a4) {
            return new p(this, a4, a3, jVar, aVar);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public o clone() {
        try {
            return (o) super.clone();
        } catch (CloneNotSupportedException e2) {
            throw new AssertionError();
        }
    }

    public boolean a(Class cls, boolean z) {
        if (this.b != -1.0d && !a((c) cls.getAnnotation(c.class), (d) cls.getAnnotation(d.class))) {
            return true;
        }
        if (!this.d && b(cls)) {
            return true;
        }
        if (a(cls)) {
            return true;
        }
        for (com.a.a.a a2 : z ? this.f : this.g) {
            if (a2.a(cls)) {
                return true;
            }
        }
        return false;
    }

    public boolean a(Field field, boolean z) {
        com.a.a.a.a aVar;
        if ((this.c & field.getModifiers()) != 0) {
            return true;
        }
        if (this.b != -1.0d && !a((c) field.getAnnotation(c.class), (d) field.getAnnotation(d.class))) {
            return true;
        }
        if (field.isSynthetic()) {
            return true;
        }
        if (this.e && ((aVar = (com.a.a.a.a) field.getAnnotation(com.a.a.a.a.class)) == null || (!z ? !aVar.b() : !aVar.a()))) {
            return true;
        }
        if (!this.d && b(field.getType())) {
            return true;
        }
        if (a(field.getType())) {
            return true;
        }
        List<com.a.a.a> list = z ? this.f : this.g;
        if (!list.isEmpty()) {
            b bVar = new b(field);
            for (com.a.a.a a2 : list) {
                if (a2.a(bVar)) {
                    return true;
                }
            }
        }
        return false;
    }
}
