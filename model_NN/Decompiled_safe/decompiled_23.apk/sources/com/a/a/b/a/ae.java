package com.a.a.b.a;

import com.a.a.ab;
import com.a.a.af;
import com.a.a.d.a;
import com.a.a.d.e;
import com.a.a.d.f;
import java.math.BigDecimal;

final class ae extends af {
    ae() {
    }

    /* renamed from: a */
    public BigDecimal b(a aVar) {
        if (aVar.f() == e.NULL) {
            aVar.j();
            return null;
        }
        try {
            return new BigDecimal(aVar.h());
        } catch (NumberFormatException e) {
            throw new ab(e);
        }
    }

    public void a(f fVar, BigDecimal bigDecimal) {
        fVar.a(bigDecimal);
    }
}
