package com.a.a.b;

import java.util.AbstractSet;
import java.util.Iterator;

final class ac extends AbstractSet {
    final /* synthetic */ y a;

    private ac(y yVar) {
        this.a = yVar;
    }

    public void clear() {
        this.a.clear();
    }

    public boolean contains(Object obj) {
        return this.a.containsKey(obj);
    }

    public Iterator iterator() {
        return new ad(this);
    }

    public boolean remove(Object obj) {
        int b = this.a.d;
        this.a.remove(obj);
        return this.a.d != b;
    }

    public int size() {
        return this.a.d;
    }
}
