package com.a.a.b.a;

import com.a.a.ab;
import com.a.a.af;
import com.a.a.ag;
import com.a.a.d.a;
import com.a.a.d.e;
import com.a.a.d.f;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public final class t extends af {
    public static final ag a = new u();
    private final DateFormat b = new SimpleDateFormat("MMM d, yyyy");

    /* renamed from: a */
    public synchronized Date b(a aVar) {
        Date date;
        if (aVar.f() == e.NULL) {
            aVar.j();
            date = null;
        } else {
            try {
                date = new Date(this.b.parse(aVar.h()).getTime());
            } catch (ParseException e) {
                throw new ab(e);
            }
        }
        return date;
    }

    public synchronized void a(f fVar, Date date) {
        fVar.b(date == null ? null : this.b.format(date));
    }
}
