package com.a.a.b.a;

import com.a.a.d.a;
import com.a.a.d.e;
import com.a.a.s;
import com.a.a.w;
import com.a.a.x;
import com.a.a.z;
import java.io.Reader;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class g extends a {
    private static final Reader a = new h();
    private static final Object b = new Object();
    private final List c;

    private void a(e eVar) {
        if (f() != eVar) {
            throw new IllegalStateException("Expected " + eVar + " but was " + f());
        }
    }

    private Object q() {
        return this.c.get(this.c.size() - 1);
    }

    private Object r() {
        return this.c.remove(this.c.size() - 1);
    }

    public void a() {
        a(e.BEGIN_ARRAY);
        this.c.add(((s) q()).iterator());
    }

    public void b() {
        a(e.END_ARRAY);
        r();
        r();
    }

    public void c() {
        a(e.BEGIN_OBJECT);
        this.c.add(((x) q()).o().iterator());
    }

    public void close() {
        this.c.clear();
        this.c.add(b);
    }

    public void d() {
        a(e.END_OBJECT);
        r();
        r();
    }

    public boolean e() {
        e f = f();
        return (f == e.END_OBJECT || f == e.END_ARRAY) ? false : true;
    }

    public e f() {
        if (this.c.isEmpty()) {
            return e.END_DOCUMENT;
        }
        Object q = q();
        if (q instanceof Iterator) {
            boolean z = this.c.get(this.c.size() - 2) instanceof x;
            Iterator it = (Iterator) q;
            if (!it.hasNext()) {
                return z ? e.END_OBJECT : e.END_ARRAY;
            }
            if (z) {
                return e.NAME;
            }
            this.c.add(it.next());
            return f();
        } else if (q instanceof x) {
            return e.BEGIN_OBJECT;
        } else {
            if (q instanceof s) {
                return e.BEGIN_ARRAY;
            }
            if (q instanceof z) {
                z zVar = (z) q;
                if (zVar.q()) {
                    return e.STRING;
                }
                if (zVar.o()) {
                    return e.BOOLEAN;
                }
                if (zVar.p()) {
                    return e.NUMBER;
                }
                throw new AssertionError();
            } else if (q instanceof w) {
                return e.NULL;
            } else {
                if (q == b) {
                    throw new IllegalStateException("JsonReader is closed");
                }
                throw new AssertionError();
            }
        }
    }

    public String g() {
        a(e.NAME);
        Map.Entry entry = (Map.Entry) ((Iterator) q()).next();
        this.c.add(entry.getValue());
        return (String) entry.getKey();
    }

    public String h() {
        e f = f();
        if (f == e.STRING || f == e.NUMBER) {
            return ((z) r()).b();
        }
        throw new IllegalStateException("Expected " + e.STRING + " but was " + f);
    }

    public boolean i() {
        a(e.BOOLEAN);
        return ((z) r()).f();
    }

    public void j() {
        a(e.NULL);
        r();
    }

    public double k() {
        e f = f();
        if (f == e.NUMBER || f == e.STRING) {
            double c2 = ((z) q()).c();
            if (p() || (!Double.isNaN(c2) && !Double.isInfinite(c2))) {
                r();
                return c2;
            }
            throw new NumberFormatException("JSON forbids NaN and infinities: " + c2);
        }
        throw new IllegalStateException("Expected " + e.NUMBER + " but was " + f);
    }

    public long l() {
        e f = f();
        if (f == e.NUMBER || f == e.STRING) {
            long d = ((z) q()).d();
            r();
            return d;
        }
        throw new IllegalStateException("Expected " + e.NUMBER + " but was " + f);
    }

    public int m() {
        e f = f();
        if (f == e.NUMBER || f == e.STRING) {
            int e = ((z) q()).e();
            r();
            return e;
        }
        throw new IllegalStateException("Expected " + e.NUMBER + " but was " + f);
    }

    public void n() {
        if (f() == e.NAME) {
            g();
        } else {
            r();
        }
    }

    public void o() {
        a(e.NAME);
        Map.Entry entry = (Map.Entry) ((Iterator) q()).next();
        this.c.add(entry.getValue());
        this.c.add(new z((String) entry.getKey()));
    }

    public String toString() {
        return getClass().getSimpleName();
    }
}
