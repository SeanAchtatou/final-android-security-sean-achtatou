package com.a.a.b.a;

import com.a.a.af;
import com.a.a.ag;
import com.a.a.d.e;
import com.a.a.d.f;
import com.a.a.j;
import java.lang.reflect.Array;
import java.util.ArrayList;

public final class a extends af {
    public static final ag a = new b();
    private final Class b;
    private final af c;

    public a(j jVar, af afVar, Class cls) {
        this.c = new x(jVar, afVar, cls);
        this.b = cls;
    }

    public void a(f fVar, Object obj) {
        if (obj == null) {
            fVar.f();
            return;
        }
        fVar.b();
        int length = Array.getLength(obj);
        for (int i = 0; i < length; i++) {
            this.c.a(fVar, Array.get(obj, i));
        }
        fVar.c();
    }

    public Object b(com.a.a.d.a aVar) {
        if (aVar.f() == e.NULL) {
            aVar.j();
            return null;
        }
        ArrayList arrayList = new ArrayList();
        aVar.a();
        while (aVar.e()) {
            arrayList.add(this.c.b(aVar));
        }
        aVar.b();
        Object newInstance = Array.newInstance(this.b, arrayList.size());
        for (int i = 0; i < arrayList.size(); i++) {
            Array.set(newInstance, i, arrayList.get(i));
        }
        return newInstance;
    }
}
