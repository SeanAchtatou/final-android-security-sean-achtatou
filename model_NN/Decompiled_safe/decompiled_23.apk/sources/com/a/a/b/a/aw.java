package com.a.a.b.a;

import com.a.a.af;
import com.a.a.ag;
import com.a.a.c.a;
import com.a.a.j;

final class aw implements ag {
    final /* synthetic */ Class a;
    final /* synthetic */ Class b;
    final /* synthetic */ af c;

    aw(Class cls, Class cls2, af afVar) {
        this.a = cls;
        this.b = cls2;
        this.c = afVar;
    }

    public af a(j jVar, a aVar) {
        Class a2 = aVar.a();
        if (a2 == this.a || a2 == this.b) {
            return this.c;
        }
        return null;
    }

    public String toString() {
        return "Factory[type=" + this.a.getName() + "+" + this.b.getName() + ",adapter=" + this.c + "]";
    }
}
