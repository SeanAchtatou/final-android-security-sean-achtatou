package com.a.a.b;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.lang.reflect.WildcardType;

final class e implements Serializable, WildcardType {
    private final Type a;
    private final Type b;

    public e(Type[] typeArr, Type[] typeArr2) {
        boolean z = true;
        a.a(typeArr2.length <= 1);
        a.a(typeArr.length == 1);
        if (typeArr2.length == 1) {
            a.a(typeArr2[0]);
            b.i(typeArr2[0]);
            a.a(typeArr[0] != Object.class ? false : z);
            this.b = b.d(typeArr2[0]);
            this.a = Object.class;
            return;
        }
        a.a(typeArr[0]);
        b.i(typeArr[0]);
        this.b = null;
        this.a = b.d(typeArr[0]);
    }

    public boolean equals(Object obj) {
        return (obj instanceof WildcardType) && b.a(this, (WildcardType) obj);
    }

    public Type[] getLowerBounds() {
        if (this.b == null) {
            return b.a;
        }
        return new Type[]{this.b};
    }

    public Type[] getUpperBounds() {
        return new Type[]{this.a};
    }

    public int hashCode() {
        return (this.b != null ? this.b.hashCode() + 31 : 1) ^ (this.a.hashCode() + 31);
    }

    public String toString() {
        return this.b != null ? "? super " + b.f(this.b) : this.a == Object.class ? "?" : "? extends " + b.f(this.a);
    }
}
