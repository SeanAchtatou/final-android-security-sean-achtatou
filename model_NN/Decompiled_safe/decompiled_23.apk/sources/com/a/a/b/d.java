package com.a.a.b;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;

final class d implements Serializable, ParameterizedType {
    private final Type a;
    private final Type b;
    private final Type[] c;

    public d(Type type, Type type2, Type... typeArr) {
        boolean z = true;
        if (type2 instanceof Class) {
            Class cls = (Class) type2;
            a.a(type != null || cls.getEnclosingClass() == null);
            if (type != null && cls.getEnclosingClass() == null) {
                z = false;
            }
            a.a(z);
        }
        this.a = type == null ? null : b.d(type);
        this.b = b.d(type2);
        this.c = (Type[]) typeArr.clone();
        for (int i = 0; i < this.c.length; i++) {
            a.a(this.c[i]);
            b.i(this.c[i]);
            this.c[i] = b.d(this.c[i]);
        }
    }

    public boolean equals(Object obj) {
        return (obj instanceof ParameterizedType) && b.a(this, (ParameterizedType) obj);
    }

    public Type[] getActualTypeArguments() {
        return (Type[]) this.c.clone();
    }

    public Type getOwnerType() {
        return this.a;
    }

    public Type getRawType() {
        return this.b;
    }

    public int hashCode() {
        return (Arrays.hashCode(this.c) ^ this.b.hashCode()) ^ b.b((Object) this.a);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder((this.c.length + 1) * 30);
        sb.append(b.f(this.b));
        if (this.c.length == 0) {
            return sb.toString();
        }
        sb.append("<").append(b.f(this.c[0]));
        for (int i = 1; i < this.c.length; i++) {
            sb.append(", ").append(b.f(this.c[i]));
        }
        return sb.append(">").toString();
    }
}
