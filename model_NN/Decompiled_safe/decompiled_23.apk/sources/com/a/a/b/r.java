package com.a.a.b;

import java.math.BigInteger;

public final class r extends Number {
    private final String a;

    public r(String str) {
        this.a = str;
    }

    public double doubleValue() {
        return Double.parseDouble(this.a);
    }

    public float floatValue() {
        return Float.parseFloat(this.a);
    }

    public int intValue() {
        try {
            return Integer.parseInt(this.a);
        } catch (NumberFormatException e) {
            try {
                return (int) Long.parseLong(this.a);
            } catch (NumberFormatException e2) {
                return new BigInteger(this.a).intValue();
            }
        }
    }

    public long longValue() {
        try {
            return Long.parseLong(this.a);
        } catch (NumberFormatException e) {
            return new BigInteger(this.a).longValue();
        }
    }

    public String toString() {
        return this.a;
    }
}
