package com.a.a.b.a;

import com.a.a.af;
import com.a.a.d.a;
import com.a.a.d.e;
import com.a.a.d.f;
import java.net.URL;

final class ai extends af {
    ai() {
    }

    /* renamed from: a */
    public URL b(a aVar) {
        if (aVar.f() == e.NULL) {
            aVar.j();
            return null;
        }
        String h = aVar.h();
        if (!"null".equals(h)) {
            return new URL(h);
        }
        return null;
    }

    public void a(f fVar, URL url) {
        fVar.b(url == null ? null : url.toExternalForm());
    }
}
