package com.a.a.b.a;

import com.a.a.af;
import com.a.a.ag;
import com.a.a.c.a;
import com.a.a.j;

final class at implements ag {
    final /* synthetic */ Class a;
    final /* synthetic */ af b;

    at(Class cls, af afVar) {
        this.a = cls;
        this.b = afVar;
    }

    public af a(j jVar, a aVar) {
        if (aVar.a() == this.a) {
            return this.b;
        }
        return null;
    }

    public String toString() {
        return "Factory[type=" + this.a.getName() + ",adapter=" + this.b + "]";
    }
}
