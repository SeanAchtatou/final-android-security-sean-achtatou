package com.a.a.b.a;

import com.a.a.a.b;
import com.a.a.af;
import com.a.a.d.a;
import com.a.a.d.e;
import com.a.a.d.f;
import java.util.HashMap;
import java.util.Map;

final class bf extends af {
    private final Map a = new HashMap();
    private final Map b = new HashMap();

    public bf(Class cls) {
        try {
            for (Enum enumR : (Enum[]) cls.getEnumConstants()) {
                String name = enumR.name();
                b bVar = (b) cls.getField(name).getAnnotation(b.class);
                String a2 = bVar != null ? bVar.a() : name;
                this.a.put(a2, enumR);
                this.b.put(enumR, a2);
            }
        } catch (NoSuchFieldException e) {
            throw new AssertionError();
        }
    }

    /* renamed from: a */
    public Enum b(a aVar) {
        if (aVar.f() != e.NULL) {
            return (Enum) this.a.get(aVar.h());
        }
        aVar.j();
        return null;
    }

    public void a(f fVar, Enum enumR) {
        fVar.b(enumR == null ? null : (String) this.b.get(enumR));
    }
}
