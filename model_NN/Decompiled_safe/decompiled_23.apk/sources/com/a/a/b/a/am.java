package com.a.a.b.a;

import com.a.a.af;
import com.a.a.d.a;
import com.a.a.d.e;
import com.a.a.d.f;
import java.util.UUID;

final class am extends af {
    am() {
    }

    /* renamed from: a */
    public UUID b(a aVar) {
        if (aVar.f() != e.NULL) {
            return UUID.fromString(aVar.h());
        }
        aVar.j();
        return null;
    }

    public void a(f fVar, UUID uuid) {
        fVar.b(uuid == null ? null : uuid.toString());
    }
}
