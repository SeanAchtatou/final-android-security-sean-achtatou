package com.a.a.b.a;

import com.a.a.af;
import com.a.a.d.a;
import com.a.a.d.f;
import java.sql.Timestamp;
import java.util.Date;

class ao extends af {
    final /* synthetic */ af a;
    final /* synthetic */ an b;

    ao(an anVar, af afVar) {
        this.b = anVar;
        this.a = afVar;
    }

    /* renamed from: a */
    public Timestamp b(a aVar) {
        Date date = (Date) this.a.b(aVar);
        if (date != null) {
            return new Timestamp(date.getTime());
        }
        return null;
    }

    public void a(f fVar, Timestamp timestamp) {
        this.a.a(fVar, timestamp);
    }
}
