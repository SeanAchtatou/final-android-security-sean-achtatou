package com.a.a.b.a;

import com.a.a.af;
import com.a.a.ag;
import com.a.a.b.y;
import com.a.a.d.a;
import com.a.a.d.f;
import com.a.a.j;
import java.util.ArrayList;

public final class m extends af {
    public static final ag a = new n();
    private final j b;

    private m(j jVar) {
        this.b = jVar;
    }

    /* synthetic */ m(j jVar, n nVar) {
        this(jVar);
    }

    public void a(f fVar, Object obj) {
        if (obj == null) {
            fVar.f();
            return;
        }
        af a2 = this.b.a((Class) obj.getClass());
        if (a2 instanceof m) {
            fVar.d();
            fVar.e();
            return;
        }
        a2.a(fVar, obj);
    }

    public Object b(a aVar) {
        switch (o.a[aVar.f().ordinal()]) {
            case 1:
                ArrayList arrayList = new ArrayList();
                aVar.a();
                while (aVar.e()) {
                    arrayList.add(b(aVar));
                }
                aVar.b();
                return arrayList;
            case 2:
                y yVar = new y();
                aVar.c();
                while (aVar.e()) {
                    yVar.put(aVar.g(), b(aVar));
                }
                aVar.d();
                return yVar;
            case 3:
                return aVar.h();
            case 4:
                return Double.valueOf(aVar.k());
            case 5:
                return Boolean.valueOf(aVar.i());
            case 6:
                aVar.j();
                return null;
            default:
                throw new IllegalStateException();
        }
    }
}
