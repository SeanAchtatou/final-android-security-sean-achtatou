package com.a.a.b.a;

import com.a.a.af;
import com.a.a.ag;
import com.a.a.c.a;
import com.a.a.j;

final class ax implements ag {
    final /* synthetic */ Class a;
    final /* synthetic */ af b;

    ax(Class cls, af afVar) {
        this.a = cls;
        this.b = afVar;
    }

    public af a(j jVar, a aVar) {
        if (this.a.isAssignableFrom(aVar.a())) {
            return this.b;
        }
        return null;
    }

    public String toString() {
        return "Factory[typeHierarchy=" + this.a.getName() + ",adapter=" + this.b + "]";
    }
}
