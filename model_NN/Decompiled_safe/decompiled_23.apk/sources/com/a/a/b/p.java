package com.a.a.b;

import com.a.a.af;
import com.a.a.c.a;
import com.a.a.d.f;
import com.a.a.j;

class p extends af {
    final /* synthetic */ boolean a;
    final /* synthetic */ boolean b;
    final /* synthetic */ j c;
    final /* synthetic */ a d;
    final /* synthetic */ o e;
    private af f;

    p(o oVar, boolean z, boolean z2, j jVar, a aVar) {
        this.e = oVar;
        this.a = z;
        this.b = z2;
        this.c = jVar;
        this.d = aVar;
    }

    private af a() {
        af afVar = this.f;
        if (afVar != null) {
            return afVar;
        }
        af a2 = this.c.a(this.e, this.d);
        this.f = a2;
        return a2;
    }

    public void a(f fVar, Object obj) {
        if (this.b) {
            fVar.f();
        } else {
            a().a(fVar, obj);
        }
    }

    public Object b(com.a.a.d.a aVar) {
        if (!this.a) {
            return a().b(aVar);
        }
        aVar.n();
        return null;
    }
}
