package com.a.a.b.a;

import com.a.a.af;
import com.a.a.d.a;
import com.a.a.d.e;
import com.a.a.d.f;
import java.util.Calendar;
import java.util.GregorianCalendar;

final class ap extends af {
    ap() {
    }

    /* renamed from: a */
    public Calendar b(a aVar) {
        int i = 0;
        if (aVar.f() == e.NULL) {
            aVar.j();
            return null;
        }
        aVar.c();
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        while (aVar.f() != e.END_OBJECT) {
            String g = aVar.g();
            int m = aVar.m();
            if ("year".equals(g)) {
                i6 = m;
            } else if ("month".equals(g)) {
                i5 = m;
            } else if ("dayOfMonth".equals(g)) {
                i4 = m;
            } else if ("hourOfDay".equals(g)) {
                i3 = m;
            } else if ("minute".equals(g)) {
                i2 = m;
            } else if ("second".equals(g)) {
                i = m;
            }
        }
        aVar.d();
        return new GregorianCalendar(i6, i5, i4, i3, i2, i);
    }

    public void a(f fVar, Calendar calendar) {
        if (calendar == null) {
            fVar.f();
            return;
        }
        fVar.d();
        fVar.a("year");
        fVar.a((long) calendar.get(1));
        fVar.a("month");
        fVar.a((long) calendar.get(2));
        fVar.a("dayOfMonth");
        fVar.a((long) calendar.get(5));
        fVar.a("hourOfDay");
        fVar.a((long) calendar.get(11));
        fVar.a("minute");
        fVar.a((long) calendar.get(12));
        fVar.a("second");
        fVar.a((long) calendar.get(13));
        fVar.e();
    }
}
