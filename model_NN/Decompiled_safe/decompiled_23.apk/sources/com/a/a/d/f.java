package com.a.a.d;

import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public class f implements Closeable, Flushable {
    private static final String[] a = new String[128];
    private static final String[] b = ((String[]) a.clone());
    private final Writer c;
    private final List d = new ArrayList();
    private String e;
    private String f;
    private boolean g;
    private boolean h;
    private String i;
    private boolean j;

    static {
        for (int i2 = 0; i2 <= 31; i2++) {
            a[i2] = String.format("\\u%04x", Integer.valueOf(i2));
        }
        a[34] = "\\\"";
        a[92] = "\\\\";
        a[9] = "\\t";
        a[8] = "\\b";
        a[10] = "\\n";
        a[13] = "\\r";
        a[12] = "\\f";
        b[60] = "\\u003c";
        b[62] = "\\u003e";
        b[38] = "\\u0026";
        b[61] = "\\u003d";
        b[39] = "\\u0027";
    }

    public f(Writer writer) {
        this.d.add(d.EMPTY_DOCUMENT);
        this.f = ":";
        this.j = true;
        if (writer == null) {
            throw new NullPointerException("out == null");
        }
        this.c = writer;
    }

    private d a() {
        int size = this.d.size();
        if (size != 0) {
            return (d) this.d.get(size - 1);
        }
        throw new IllegalStateException("JsonWriter is closed.");
    }

    private f a(d dVar, d dVar2, String str) {
        d a2 = a();
        if (a2 != dVar2 && a2 != dVar) {
            throw new IllegalStateException("Nesting problem: " + this.d);
        } else if (this.i != null) {
            throw new IllegalStateException("Dangling name: " + this.i);
        } else {
            this.d.remove(this.d.size() - 1);
            if (a2 == dVar2) {
                k();
            }
            this.c.write(str);
            return this;
        }
    }

    private f a(d dVar, String str) {
        e(true);
        this.d.add(dVar);
        this.c.write(str);
        return this;
    }

    private void a(d dVar) {
        this.d.set(this.d.size() - 1, dVar);
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0030  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void d(java.lang.String r8) {
        /*
            r7 = this;
            r1 = 0
            boolean r0 = r7.h
            if (r0 == 0) goto L_0x0025
            java.lang.String[] r0 = com.a.a.d.f.b
        L_0x0007:
            java.io.Writer r2 = r7.c
            java.lang.String r3 = "\""
            r2.write(r3)
            int r4 = r8.length()
            r3 = r1
        L_0x0013:
            if (r3 >= r4) goto L_0x0046
            char r2 = r8.charAt(r3)
            r5 = 128(0x80, float:1.794E-43)
            if (r2 >= r5) goto L_0x0028
            r2 = r0[r2]
            if (r2 != 0) goto L_0x002e
        L_0x0021:
            int r2 = r3 + 1
            r3 = r2
            goto L_0x0013
        L_0x0025:
            java.lang.String[] r0 = com.a.a.d.f.a
            goto L_0x0007
        L_0x0028:
            r5 = 8232(0x2028, float:1.1535E-41)
            if (r2 != r5) goto L_0x003f
            java.lang.String r2 = "\\u2028"
        L_0x002e:
            if (r1 >= r3) goto L_0x0037
            java.io.Writer r5 = r7.c
            int r6 = r3 - r1
            r5.write(r8, r1, r6)
        L_0x0037:
            java.io.Writer r1 = r7.c
            r1.write(r2)
            int r1 = r3 + 1
            goto L_0x0021
        L_0x003f:
            r5 = 8233(0x2029, float:1.1537E-41)
            if (r2 != r5) goto L_0x0021
            java.lang.String r2 = "\\u2029"
            goto L_0x002e
        L_0x0046:
            if (r1 >= r4) goto L_0x004f
            java.io.Writer r0 = r7.c
            int r2 = r4 - r1
            r0.write(r8, r1, r2)
        L_0x004f:
            java.io.Writer r0 = r7.c
            java.lang.String r1 = "\""
            r0.write(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.a.d.f.d(java.lang.String):void");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private void e(boolean z) {
        switch (g.a[a().ordinal()]) {
            case 1:
                if (!this.g) {
                    throw new IllegalStateException("JSON must have only one top-level value.");
                }
                break;
            case 2:
                break;
            case 3:
                a(d.NONEMPTY_ARRAY);
                k();
                return;
            case 4:
                this.c.append(',');
                k();
                return;
            case 5:
                this.c.append((CharSequence) this.f);
                a(d.NONEMPTY_OBJECT);
                return;
            default:
                throw new IllegalStateException("Nesting problem: " + this.d);
        }
        if (this.g || z) {
            a(d.NONEMPTY_DOCUMENT);
            return;
        }
        throw new IllegalStateException("JSON must start with an array or an object.");
    }

    private void j() {
        if (this.i != null) {
            l();
            d(this.i);
            this.i = null;
        }
    }

    private void k() {
        if (this.e != null) {
            this.c.write("\n");
            for (int i2 = 1; i2 < this.d.size(); i2++) {
                this.c.write(this.e);
            }
        }
    }

    private void l() {
        d a2 = a();
        if (a2 == d.NONEMPTY_OBJECT) {
            this.c.write(44);
        } else if (a2 != d.EMPTY_OBJECT) {
            throw new IllegalStateException("Nesting problem: " + this.d);
        }
        k();
        a(d.DANGLING_NAME);
    }

    public f a(long j2) {
        j();
        e(false);
        this.c.write(Long.toString(j2));
        return this;
    }

    public f a(Number number) {
        if (number == null) {
            return f();
        }
        j();
        String obj = number.toString();
        if (this.g || (!obj.equals("-Infinity") && !obj.equals("Infinity") && !obj.equals("NaN"))) {
            e(false);
            this.c.append((CharSequence) obj);
            return this;
        }
        throw new IllegalArgumentException("Numeric values must be finite, but was " + number);
    }

    public f a(String str) {
        if (str == null) {
            throw new NullPointerException("name == null");
        } else if (this.i != null) {
            throw new IllegalStateException();
        } else if (this.d.isEmpty()) {
            throw new IllegalStateException("JsonWriter is closed.");
        } else {
            this.i = str;
            return this;
        }
    }

    public f a(boolean z) {
        j();
        e(false);
        this.c.write(z ? "true" : "false");
        return this;
    }

    public f b() {
        j();
        return a(d.EMPTY_ARRAY, "[");
    }

    public f b(String str) {
        if (str == null) {
            return f();
        }
        j();
        e(false);
        d(str);
        return this;
    }

    public final void b(boolean z) {
        this.g = z;
    }

    public f c() {
        return a(d.EMPTY_ARRAY, d.NONEMPTY_ARRAY, "]");
    }

    public final void c(String str) {
        if (str.length() == 0) {
            this.e = null;
            this.f = ":";
            return;
        }
        this.e = str;
        this.f = ": ";
    }

    public final void c(boolean z) {
        this.h = z;
    }

    public void close() {
        this.c.close();
        int size = this.d.size();
        if (size > 1 || (size == 1 && this.d.get(size - 1) != d.NONEMPTY_DOCUMENT)) {
            throw new IOException("Incomplete document");
        }
        this.d.clear();
    }

    public f d() {
        j();
        return a(d.EMPTY_OBJECT, "{");
    }

    public final void d(boolean z) {
        this.j = z;
    }

    public f e() {
        return a(d.EMPTY_OBJECT, d.NONEMPTY_OBJECT, "}");
    }

    public f f() {
        if (this.i != null) {
            if (this.j) {
                j();
            } else {
                this.i = null;
                return this;
            }
        }
        e(false);
        this.c.write("null");
        return this;
    }

    public void flush() {
        if (this.d.isEmpty()) {
            throw new IllegalStateException("JsonWriter is closed.");
        }
        this.c.flush();
    }

    public boolean g() {
        return this.g;
    }

    public final boolean h() {
        return this.h;
    }

    public final boolean i() {
        return this.j;
    }
}
