package com.a.a.d;

import com.a.a.b.a.g;
import com.a.a.b.q;

final class b extends q {
    b() {
    }

    public void a(a aVar) {
        if (aVar instanceof g) {
            ((g) aVar).o();
            return;
        }
        aVar.f();
        if (aVar.l != e.NAME) {
            throw new IllegalStateException("Expected a name but was " + aVar.f() + " " + " at line " + aVar.t() + " column " + aVar.u());
        }
        String unused = aVar.n = aVar.m;
        String unused2 = aVar.m = null;
        e unused3 = aVar.l = e.STRING;
    }
}
