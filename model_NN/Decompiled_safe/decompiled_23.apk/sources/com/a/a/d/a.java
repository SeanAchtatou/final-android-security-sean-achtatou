package com.a.a.d;

import com.a.a.b.q;
import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.Reader;

public class a implements Closeable {
    private static final char[] a = ")]}'\n".toCharArray();
    private final i b = new i();
    private final Reader c;
    private boolean d = false;
    private final char[] e = new char[1024];
    private int f = 0;
    private int g = 0;
    private int h = 1;
    private int i = 1;
    private d[] j = new d[32];
    private int k = 0;
    /* access modifiers changed from: private */
    public e l;
    /* access modifiers changed from: private */
    public String m;
    /* access modifiers changed from: private */
    public String n;
    private int o;
    private int p;
    private boolean q;

    static {
        q.a = new b();
    }

    public a(Reader reader) {
        a(d.EMPTY_DOCUMENT);
        this.q = false;
        if (reader == null) {
            throw new NullPointerException("in == null");
        }
        this.c = reader;
    }

    private e a(char[] cArr, int i2, int i3) {
        int i4;
        int i5;
        char c2;
        int i6;
        char c3;
        char c4 = cArr[i2];
        if (c4 == '-') {
            i4 = i2 + 1;
            c4 = cArr[i4];
        } else {
            i4 = i2;
        }
        if (c4 == '0') {
            i5 = i4 + 1;
            c2 = cArr[i5];
        } else if (c4 < '1' || c4 > '9') {
            return e.STRING;
        } else {
            i5 = i4 + 1;
            c2 = cArr[i5];
            while (c2 >= '0' && c2 <= '9') {
                i5++;
                c2 = cArr[i5];
            }
        }
        if (c3 == '.') {
            i6++;
            c3 = cArr[i6];
            while (c3 >= '0' && c3 <= '9') {
                i6++;
                c3 = cArr[i6];
            }
        }
        char c5 = c3;
        int i7 = i6;
        char c6 = c5;
        if (c6 == 'e' || c6 == 'E') {
            int i8 = i7 + 1;
            char c7 = cArr[i8];
            if (c7 == '+' || c7 == '-') {
                i8++;
                c7 = cArr[i8];
            }
            if (c7 < '0' || c7 > '9') {
                return e.STRING;
            }
            int i9 = i8 + 1;
            i7 = i9;
            char c8 = cArr[i9];
            while (c8 >= '0' && c8 <= '9') {
                int i10 = i7 + 1;
                i7 = i10;
                c8 = cArr[i10];
            }
        }
        return i7 == i2 + i3 ? e.NUMBER : e.STRING;
    }

    private String a(char c2) {
        int i2;
        int i3;
        StringBuilder sb;
        int i4;
        char[] cArr = this.e;
        StringBuilder sb2 = null;
        do {
            int i5 = this.f;
            int i6 = this.g;
            int i7 = i5;
            while (i7 < i6) {
                int i8 = i7 + 1;
                char c3 = cArr[i7];
                if (c3 == c2) {
                    this.f = i8;
                    if (this.q) {
                        return "skipped!";
                    }
                    if (sb2 == null) {
                        return this.b.a(cArr, i5, (i8 - i5) - 1);
                    }
                    sb2.append(cArr, i5, (i8 - i5) - 1);
                    return sb2.toString();
                }
                if (c3 == '\\') {
                    this.f = i8;
                    if (sb2 == null) {
                        sb2 = new StringBuilder();
                    }
                    sb2.append(cArr, i5, (i8 - i5) - 1);
                    sb2.append(x());
                    int i9 = this.f;
                    sb = sb2;
                    i4 = i9;
                    int i10 = i9;
                    i2 = this.g;
                    i3 = i10;
                } else {
                    int i11 = i5;
                    i2 = i6;
                    i3 = i8;
                    sb = sb2;
                    i4 = i11;
                }
                i7 = i3;
                i6 = i2;
                i5 = i4;
                sb2 = sb;
            }
            if (sb2 == null) {
                sb2 = new StringBuilder();
            }
            sb2.append(cArr, i5, i7 - i5);
            this.f = i7;
        } while (a(1));
        throw b("Unterminated string");
    }

    private void a(d dVar) {
        if (this.k == this.j.length) {
            d[] dVarArr = new d[(this.k * 2)];
            System.arraycopy(this.j, 0, dVarArr, 0, this.k);
            this.j = dVarArr;
        }
        d[] dVarArr2 = this.j;
        int i2 = this.k;
        this.k = i2 + 1;
        dVarArr2[i2] = dVar;
    }

    private void a(e eVar) {
        f();
        if (this.l != eVar) {
            throw new IllegalStateException("Expected " + eVar + " but was " + f() + " at line " + t() + " column " + u());
        }
        q();
    }

    private boolean a(int i2) {
        char[] cArr = this.e;
        int i3 = this.h;
        int i4 = this.i;
        int i5 = this.f;
        for (int i6 = 0; i6 < i5; i6++) {
            if (cArr[i6] == 10) {
                i3++;
                i4 = 1;
            } else {
                i4++;
            }
        }
        this.h = i3;
        this.i = i4;
        if (this.g != this.f) {
            this.g -= this.f;
            System.arraycopy(cArr, this.f, cArr, 0, this.g);
        } else {
            this.g = 0;
        }
        this.f = 0;
        do {
            int read = this.c.read(cArr, this.g, cArr.length - this.g);
            if (read == -1) {
                return false;
            }
            this.g = read + this.g;
            if (this.h == 1 && this.i == 1 && this.g > 0 && cArr[0] == 65279) {
                this.f++;
                this.i--;
            }
        } while (this.g < i2);
        return true;
    }

    private boolean a(String str) {
        while (true) {
            if (this.f + str.length() > this.g && !a(str.length())) {
                return false;
            }
            int i2 = 0;
            while (i2 < str.length()) {
                if (this.e[this.f + i2] != str.charAt(i2)) {
                    this.f++;
                } else {
                    i2++;
                }
            }
            return true;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private e b(boolean z) {
        if (z) {
            this.j[this.k - 1] = d.NONEMPTY_ARRAY;
        } else {
            switch (d(true)) {
                case 44:
                    break;
                case 59:
                    v();
                    break;
                case 93:
                    this.k--;
                    e eVar = e.END_ARRAY;
                    this.l = eVar;
                    return eVar;
                default:
                    throw b("Unterminated array");
            }
        }
        switch (d(true)) {
            case 44:
            case 59:
                break;
            default:
                this.f--;
                return s();
            case 93:
                if (z) {
                    this.k--;
                    e eVar2 = e.END_ARRAY;
                    this.l = eVar2;
                    return eVar2;
                }
                break;
        }
        v();
        this.f--;
        this.n = "null";
        e eVar3 = e.NULL;
        this.l = eVar3;
        return eVar3;
    }

    private IOException b(String str) {
        throw new h(str + " at line " + t() + " column " + u());
    }

    private e c(boolean z) {
        if (z) {
            switch (d(true)) {
                case 125:
                    this.k--;
                    e eVar = e.END_OBJECT;
                    this.l = eVar;
                    return eVar;
                default:
                    this.f--;
                    break;
            }
        } else {
            switch (d(true)) {
                case 44:
                case 59:
                    break;
                case 125:
                    this.k--;
                    e eVar2 = e.END_OBJECT;
                    this.l = eVar2;
                    return eVar2;
                default:
                    throw b("Unterminated object");
            }
        }
        int d2 = d(true);
        switch (d2) {
            case 39:
                v();
            case 34:
                this.m = a((char) d2);
                break;
            default:
                v();
                this.f--;
                this.m = e(false);
                if (this.m.length() == 0) {
                    throw b("Expected name");
                }
                break;
        }
        this.j[this.k - 1] = d.DANGLING_NAME;
        e eVar3 = e.NAME;
        this.l = eVar3;
        return eVar3;
    }

    private int d(boolean z) {
        char[] cArr = this.e;
        int i2 = this.f;
        int i3 = this.g;
        while (true) {
            if (i2 == i3) {
                this.f = i2;
                if (a(1)) {
                    i2 = this.f;
                    i3 = this.g;
                } else if (!z) {
                    return -1;
                } else {
                    throw new EOFException("End of input at line " + t() + " column " + u());
                }
            }
            int i4 = i2 + 1;
            char c2 = cArr[i2];
            switch (c2) {
                case 9:
                case 10:
                case 13:
                case ' ':
                    i2 = i4;
                    break;
                case '#':
                    this.f = i4;
                    v();
                    w();
                    i2 = this.f;
                    i3 = this.g;
                    break;
                case '/':
                    this.f = i4;
                    if (i4 == i3) {
                        this.f--;
                        boolean a2 = a(2);
                        this.f++;
                        if (!a2) {
                            return c2;
                        }
                    }
                    v();
                    switch (cArr[this.f]) {
                        case '*':
                            this.f++;
                            if (!a("*/")) {
                                throw b("Unterminated comment");
                            }
                            i2 = this.f + 2;
                            i3 = this.g;
                            continue;
                        case '/':
                            this.f++;
                            w();
                            i2 = this.f;
                            i3 = this.g;
                            continue;
                        default:
                            return c2;
                    }
                default:
                    this.f = i4;
                    return c2;
            }
        }
    }

    private String e(boolean z) {
        String str = null;
        this.o = -1;
        this.p = 0;
        int i2 = 0;
        StringBuilder sb = null;
        while (true) {
            if (this.f + i2 < this.g) {
                switch (this.e[this.f + i2]) {
                    case 9:
                    case 10:
                    case 12:
                    case 13:
                    case ' ':
                    case ',':
                    case ':':
                    case '[':
                    case ']':
                    case '{':
                    case '}':
                        break;
                    case '#':
                    case '/':
                    case ';':
                    case '=':
                    case '\\':
                        v();
                        break;
                    default:
                        i2++;
                }
            } else if (i2 >= this.e.length) {
                if (sb == null) {
                    sb = new StringBuilder();
                }
                sb.append(this.e, this.f, i2);
                this.p += i2;
                this.f = i2 + this.f;
                if (!a(1)) {
                    i2 = 0;
                } else {
                    i2 = 0;
                }
            } else if (!a(i2 + 1)) {
                this.e[this.g] = 0;
            }
        }
        if (z && sb == null) {
            this.o = this.f;
        } else if (this.q) {
            str = "skipped!";
        } else if (sb == null) {
            str = this.b.a(this.e, this.f, i2);
        } else {
            sb.append(this.e, this.f, i2);
            str = sb.toString();
        }
        this.p += i2;
        this.f += i2;
        return str;
    }

    private void o() {
        d(true);
        this.f--;
        if (this.f + a.length <= this.g || a(a.length)) {
            int i2 = 0;
            while (i2 < a.length) {
                if (this.e[this.f + i2] == a[i2]) {
                    i2++;
                } else {
                    return;
                }
            }
            this.f += a.length;
        }
    }

    private e q() {
        f();
        e eVar = this.l;
        this.l = null;
        this.n = null;
        this.m = null;
        return eVar;
    }

    private e r() {
        switch (d(true)) {
            case 58:
                break;
            case 59:
            case 60:
            default:
                throw b("Expected ':'");
            case 61:
                v();
                if ((this.f < this.g || a(1)) && this.e[this.f] == '>') {
                    this.f++;
                    break;
                }
        }
        this.j[this.k - 1] = d.NONEMPTY_OBJECT;
        return s();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private e s() {
        int d2 = d(true);
        switch (d2) {
            case 34:
                break;
            case 39:
                v();
                break;
            case 91:
                a(d.EMPTY_ARRAY);
                e eVar = e.BEGIN_ARRAY;
                this.l = eVar;
                return eVar;
            case 123:
                a(d.EMPTY_OBJECT);
                e eVar2 = e.BEGIN_OBJECT;
                this.l = eVar2;
                return eVar2;
            default:
                this.f--;
                return y();
        }
        this.n = a((char) d2);
        e eVar3 = e.STRING;
        this.l = eVar3;
        return eVar3;
    }

    /* access modifiers changed from: private */
    public int t() {
        int i2 = this.h;
        for (int i3 = 0; i3 < this.f; i3++) {
            if (this.e[i3] == 10) {
                i2++;
            }
        }
        return i2;
    }

    /* access modifiers changed from: private */
    public int u() {
        int i2 = this.i;
        for (int i3 = 0; i3 < this.f; i3++) {
            i2 = this.e[i3] == 10 ? 1 : i2 + 1;
        }
        return i2;
    }

    private void v() {
        if (!this.d) {
            throw b("Use JsonReader.setLenient(true) to accept malformed JSON");
        }
    }

    private void w() {
        char c2;
        do {
            if (this.f < this.g || a(1)) {
                char[] cArr = this.e;
                int i2 = this.f;
                this.f = i2 + 1;
                c2 = cArr[i2];
                if (c2 == 13) {
                    return;
                }
            } else {
                return;
            }
        } while (c2 != 10);
    }

    private char x() {
        int i2;
        if (this.f != this.g || a(1)) {
            char[] cArr = this.e;
            int i3 = this.f;
            this.f = i3 + 1;
            char c2 = cArr[i3];
            switch (c2) {
                case 'b':
                    return 8;
                case 'f':
                    return 12;
                case 'n':
                    return 10;
                case 'r':
                    return 13;
                case 't':
                    return 9;
                case 'u':
                    if (this.f + 4 <= this.g || a(4)) {
                        int i4 = this.f;
                        int i5 = i4 + 4;
                        int i6 = i4;
                        char c3 = 0;
                        for (int i7 = i6; i7 < i5; i7++) {
                            char c4 = this.e[i7];
                            char c5 = (char) (c3 << 4);
                            if (c4 >= '0' && c4 <= '9') {
                                i2 = c4 - '0';
                            } else if (c4 >= 'a' && c4 <= 'f') {
                                i2 = (c4 - 'a') + 10;
                            } else if (c4 < 'A' || c4 > 'F') {
                                throw new NumberFormatException("\\u" + this.b.a(this.e, this.f, 4));
                            } else {
                                i2 = (c4 - 'A') + 10;
                            }
                            c3 = (char) (c5 + i2);
                        }
                        this.f += 4;
                        return c3;
                    }
                    throw b("Unterminated escape sequence");
                default:
                    return c2;
            }
        } else {
            throw b("Unterminated escape sequence");
        }
    }

    private e y() {
        this.n = e(true);
        if (this.p == 0) {
            throw b("Expected literal value");
        }
        this.l = z();
        if (this.l == e.STRING) {
            v();
        }
        return this.l;
    }

    private e z() {
        if (this.o == -1) {
            return e.STRING;
        }
        if (this.p == 4 && (('n' == this.e[this.o] || 'N' == this.e[this.o]) && (('u' == this.e[this.o + 1] || 'U' == this.e[this.o + 1]) && (('l' == this.e[this.o + 2] || 'L' == this.e[this.o + 2]) && ('l' == this.e[this.o + 3] || 'L' == this.e[this.o + 3]))))) {
            this.n = "null";
            return e.NULL;
        } else if (this.p == 4 && (('t' == this.e[this.o] || 'T' == this.e[this.o]) && (('r' == this.e[this.o + 1] || 'R' == this.e[this.o + 1]) && (('u' == this.e[this.o + 2] || 'U' == this.e[this.o + 2]) && ('e' == this.e[this.o + 3] || 'E' == this.e[this.o + 3]))))) {
            this.n = "true";
            return e.BOOLEAN;
        } else if (this.p == 5 && (('f' == this.e[this.o] || 'F' == this.e[this.o]) && (('a' == this.e[this.o + 1] || 'A' == this.e[this.o + 1]) && (('l' == this.e[this.o + 2] || 'L' == this.e[this.o + 2]) && (('s' == this.e[this.o + 3] || 'S' == this.e[this.o + 3]) && ('e' == this.e[this.o + 4] || 'E' == this.e[this.o + 4])))))) {
            this.n = "false";
            return e.BOOLEAN;
        } else {
            this.n = this.b.a(this.e, this.o, this.p);
            return a(this.e, this.o, this.p);
        }
    }

    public void a() {
        a(e.BEGIN_ARRAY);
    }

    public final void a(boolean z) {
        this.d = z;
    }

    public void b() {
        a(e.END_ARRAY);
    }

    public void c() {
        a(e.BEGIN_OBJECT);
    }

    public void close() {
        this.n = null;
        this.l = null;
        this.j[0] = d.CLOSED;
        this.k = 1;
        this.c.close();
    }

    public void d() {
        a(e.END_OBJECT);
    }

    public boolean e() {
        f();
        return (this.l == e.END_OBJECT || this.l == e.END_ARRAY) ? false : true;
    }

    public e f() {
        if (this.l != null) {
            return this.l;
        }
        switch (c.a[this.j[this.k - 1].ordinal()]) {
            case 1:
                if (this.d) {
                    o();
                }
                this.j[this.k - 1] = d.NONEMPTY_DOCUMENT;
                e s = s();
                if (this.d || this.l == e.BEGIN_ARRAY || this.l == e.BEGIN_OBJECT) {
                    return s;
                }
                throw new IOException("Expected JSON document to start with '[' or '{' but was " + this.l + " at line " + t() + " column " + u());
            case 2:
                return b(true);
            case 3:
                return b(false);
            case 4:
                return c(true);
            case 5:
                return r();
            case 6:
                return c(false);
            case 7:
                if (d(false) == -1) {
                    return e.END_DOCUMENT;
                }
                this.f--;
                if (this.d) {
                    return s();
                }
                throw b("Expected EOF");
            case 8:
                throw new IllegalStateException("JsonReader is closed");
            default:
                throw new AssertionError();
        }
    }

    public String g() {
        f();
        if (this.l != e.NAME) {
            throw new IllegalStateException("Expected a name but was " + f() + " at line " + t() + " column " + u());
        }
        String str = this.m;
        q();
        return str;
    }

    public String h() {
        f();
        if (this.l == e.STRING || this.l == e.NUMBER) {
            String str = this.n;
            q();
            return str;
        }
        throw new IllegalStateException("Expected a string but was " + f() + " at line " + t() + " column " + u());
    }

    public boolean i() {
        f();
        if (this.l != e.BOOLEAN) {
            throw new IllegalStateException("Expected a boolean but was " + this.l + " at line " + t() + " column " + u());
        }
        boolean z = this.n == "true";
        q();
        return z;
    }

    public void j() {
        f();
        if (this.l != e.NULL) {
            throw new IllegalStateException("Expected null but was " + this.l + " at line " + t() + " column " + u());
        }
        q();
    }

    public double k() {
        f();
        if (this.l == e.STRING || this.l == e.NUMBER) {
            double parseDouble = Double.parseDouble(this.n);
            if (parseDouble >= 1.0d && this.n.startsWith("0")) {
                throw new h("JSON forbids octal prefixes: " + this.n + " at line " + t() + " column " + u());
            } else if (this.d || (!Double.isNaN(parseDouble) && !Double.isInfinite(parseDouble))) {
                q();
                return parseDouble;
            } else {
                throw new h("JSON forbids NaN and infinities: " + this.n + " at line " + t() + " column " + u());
            }
        } else {
            throw new IllegalStateException("Expected a double but was " + this.l + " at line " + t() + " column " + u());
        }
    }

    public long l() {
        long j2;
        f();
        if (this.l == e.STRING || this.l == e.NUMBER) {
            try {
                j2 = Long.parseLong(this.n);
            } catch (NumberFormatException e2) {
                double parseDouble = Double.parseDouble(this.n);
                j2 = (long) parseDouble;
                if (((double) j2) != parseDouble) {
                    throw new NumberFormatException("Expected a long but was " + this.n + " at line " + t() + " column " + u());
                }
            }
            if (j2 < 1 || !this.n.startsWith("0")) {
                q();
                return j2;
            }
            throw new h("JSON forbids octal prefixes: " + this.n + " at line " + t() + " column " + u());
        }
        throw new IllegalStateException("Expected a long but was " + this.l + " at line " + t() + " column " + u());
    }

    public int m() {
        int i2;
        f();
        if (this.l == e.STRING || this.l == e.NUMBER) {
            try {
                i2 = Integer.parseInt(this.n);
            } catch (NumberFormatException e2) {
                double parseDouble = Double.parseDouble(this.n);
                i2 = (int) parseDouble;
                if (((double) i2) != parseDouble) {
                    throw new NumberFormatException("Expected an int but was " + this.n + " at line " + t() + " column " + u());
                }
            }
            if (((long) i2) < 1 || !this.n.startsWith("0")) {
                q();
                return i2;
            }
            throw new h("JSON forbids octal prefixes: " + this.n + " at line " + t() + " column " + u());
        }
        throw new IllegalStateException("Expected an int but was " + this.l + " at line " + t() + " column " + u());
    }

    public void n() {
        this.q = true;
        int i2 = 0;
        do {
            try {
                e q2 = q();
                if (q2 == e.BEGIN_ARRAY || q2 == e.BEGIN_OBJECT) {
                    i2++;
                    continue;
                } else if (q2 == e.END_ARRAY || q2 == e.END_OBJECT) {
                    i2--;
                    continue;
                }
            } finally {
                this.q = false;
            }
        } while (i2 != 0);
    }

    public final boolean p() {
        return this.d;
    }

    public String toString() {
        return getClass().getSimpleName() + " at line " + t() + " column " + u();
    }
}
