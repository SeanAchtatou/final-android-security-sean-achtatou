package com.a.a;

import com.a.a.d.a;
import com.a.a.d.f;

class q extends af {
    private af a;

    q() {
    }

    public void a(af afVar) {
        if (this.a != null) {
            throw new AssertionError();
        }
        this.a = afVar;
    }

    public void a(f fVar, Object obj) {
        if (this.a == null) {
            throw new IllegalStateException();
        }
        this.a.a(fVar, obj);
    }

    public Object b(a aVar) {
        if (this.a != null) {
            return this.a.b(aVar);
        }
        throw new IllegalStateException();
    }
}
