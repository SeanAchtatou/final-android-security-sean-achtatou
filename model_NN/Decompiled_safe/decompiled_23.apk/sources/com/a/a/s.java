package com.a.a;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class s extends u implements Iterable {
    private final List a = new ArrayList();

    public Number a() {
        if (this.a.size() == 1) {
            return ((u) this.a.get(0)).a();
        }
        throw new IllegalStateException();
    }

    public void a(u uVar) {
        if (uVar == null) {
            uVar = w.a;
        }
        this.a.add(uVar);
    }

    public String b() {
        if (this.a.size() == 1) {
            return ((u) this.a.get(0)).b();
        }
        throw new IllegalStateException();
    }

    public double c() {
        if (this.a.size() == 1) {
            return ((u) this.a.get(0)).c();
        }
        throw new IllegalStateException();
    }

    public long d() {
        if (this.a.size() == 1) {
            return ((u) this.a.get(0)).d();
        }
        throw new IllegalStateException();
    }

    public int e() {
        if (this.a.size() == 1) {
            return ((u) this.a.get(0)).e();
        }
        throw new IllegalStateException();
    }

    public boolean equals(Object obj) {
        return obj == this || ((obj instanceof s) && ((s) obj).a.equals(this.a));
    }

    public boolean f() {
        if (this.a.size() == 1) {
            return ((u) this.a.get(0)).f();
        }
        throw new IllegalStateException();
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    public Iterator iterator() {
        return this.a.iterator();
    }
}
