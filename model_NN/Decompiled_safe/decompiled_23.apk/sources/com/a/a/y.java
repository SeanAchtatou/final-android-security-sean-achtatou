package com.a.a;

public class y extends RuntimeException {
    public y(String str) {
        super(str);
    }

    public y(String str, Throwable th) {
        super(str, th);
    }

    public y(Throwable th) {
        super(th);
    }
}
