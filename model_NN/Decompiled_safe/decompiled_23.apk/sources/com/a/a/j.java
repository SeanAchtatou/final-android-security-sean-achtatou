package com.a.a;

import com.a.a.b.a.a;
import com.a.a.b.a.c;
import com.a.a.b.a.e;
import com.a.a.b.a.k;
import com.a.a.b.a.m;
import com.a.a.b.a.p;
import com.a.a.b.a.t;
import com.a.a.b.a.v;
import com.a.a.b.a.y;
import com.a.a.b.f;
import com.a.a.b.o;
import com.a.a.b.u;
import com.a.a.d.h;
import java.io.EOFException;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class j {
    final t a;
    final aa b;
    private final ThreadLocal c;
    private final Map d;
    private final List e;
    private final f f;
    private final boolean g;
    private final boolean h;
    private final boolean i;
    private final boolean j;

    public j() {
        this(o.a, c.IDENTITY, Collections.emptyMap(), false, false, false, true, false, false, ac.a, Collections.emptyList());
    }

    j(o oVar, i iVar, Map map, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, ac acVar, List list) {
        this.c = new k(this);
        this.d = Collections.synchronizedMap(new HashMap());
        this.a = new l(this);
        this.b = new m(this);
        this.f = new f(map);
        this.g = z;
        this.i = z3;
        this.h = z4;
        this.j = z5;
        ArrayList arrayList = new ArrayList();
        arrayList.add(y.Q);
        arrayList.add(m.a);
        arrayList.addAll(list);
        arrayList.add(y.x);
        arrayList.add(y.m);
        arrayList.add(y.g);
        arrayList.add(y.i);
        arrayList.add(y.k);
        arrayList.add(y.a(Long.TYPE, Long.class, a(acVar)));
        arrayList.add(y.a(Double.TYPE, Double.class, a(z6)));
        arrayList.add(y.a(Float.TYPE, Float.class, b(z6)));
        arrayList.add(y.r);
        arrayList.add(y.t);
        arrayList.add(y.z);
        arrayList.add(y.B);
        arrayList.add(y.a(BigDecimal.class, y.v));
        arrayList.add(y.a(BigInteger.class, y.w));
        arrayList.add(y.D);
        arrayList.add(y.F);
        arrayList.add(y.J);
        arrayList.add(y.O);
        arrayList.add(y.H);
        arrayList.add(y.d);
        arrayList.add(e.a);
        arrayList.add(y.M);
        arrayList.add(v.a);
        arrayList.add(t.a);
        arrayList.add(y.K);
        arrayList.add(a.a);
        arrayList.add(y.R);
        arrayList.add(y.b);
        arrayList.add(oVar);
        arrayList.add(new c(this.f));
        arrayList.add(new k(this.f, z2));
        arrayList.add(new p(this.f, iVar, oVar));
        this.e = Collections.unmodifiableList(arrayList);
    }

    private af a(ac acVar) {
        return acVar == ac.a ? y.n : new p(this);
    }

    private af a(boolean z) {
        return z ? y.p : new n(this);
    }

    private com.a.a.d.f a(Writer writer) {
        if (this.i) {
            writer.write(")]}'\n");
        }
        com.a.a.d.f fVar = new com.a.a.d.f(writer);
        if (this.j) {
            fVar.c("  ");
        }
        fVar.d(this.g);
        return fVar;
    }

    /* access modifiers changed from: private */
    public void a(double d2) {
        if (Double.isNaN(d2) || Double.isInfinite(d2)) {
            throw new IllegalArgumentException(d2 + " is not a valid double value as per JSON specification. To override this" + " behavior, use GsonBuilder.serializeSpecialDoubleValues() method.");
        }
    }

    private static void a(Object obj, com.a.a.d.a aVar) {
        if (obj != null) {
            try {
                if (aVar.f() != com.a.a.d.e.END_DOCUMENT) {
                    throw new v("JSON document was not fully consumed.");
                }
            } catch (h e2) {
                throw new ab(e2);
            } catch (IOException e3) {
                throw new v(e3);
            }
        }
    }

    private af b(boolean z) {
        return z ? y.o : new o(this);
    }

    public af a(ag agVar, com.a.a.c.a aVar) {
        boolean z = false;
        for (ag agVar2 : this.e) {
            if (z) {
                af a2 = agVar2.a(this, aVar);
                if (a2 != null) {
                    return a2;
                }
            } else if (agVar2 == agVar) {
                z = true;
            }
        }
        throw new IllegalArgumentException("GSON cannot serialize " + aVar);
    }

    /* JADX INFO: finally extract failed */
    public af a(com.a.a.c.a aVar) {
        af afVar = (af) this.d.get(aVar);
        if (afVar != null) {
            return afVar;
        }
        Map map = (Map) this.c.get();
        q qVar = (q) map.get(aVar);
        if (qVar != null) {
            return qVar;
        }
        q qVar2 = new q();
        map.put(aVar, qVar2);
        try {
            for (ag a2 : this.e) {
                af a3 = a2.a(this, aVar);
                if (a3 != null) {
                    qVar2.a(a3);
                    this.d.put(aVar, a3);
                    map.remove(aVar);
                    return a3;
                }
            }
            throw new IllegalArgumentException("GSON cannot handle " + aVar);
        } catch (Throwable th) {
            map.remove(aVar);
            throw th;
        }
    }

    public af a(Class cls) {
        return a(com.a.a.c.a.b(cls));
    }

    public Object a(com.a.a.d.a aVar, Type type) {
        boolean z = true;
        boolean p = aVar.p();
        aVar.a(true);
        try {
            aVar.f();
            z = false;
            Object b2 = a(com.a.a.c.a.a(type)).b(aVar);
            aVar.a(p);
            return b2;
        } catch (EOFException e2) {
            if (z) {
                aVar.a(p);
                return null;
            }
            throw new ab(e2);
        } catch (IllegalStateException e3) {
            throw new ab(e3);
        } catch (IOException e4) {
            throw new ab(e4);
        } catch (Throwable th) {
            aVar.a(p);
            throw th;
        }
    }

    public Object a(Reader reader, Type type) {
        com.a.a.d.a aVar = new com.a.a.d.a(reader);
        Object a2 = a(aVar, type);
        a(a2, aVar);
        return a2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.j.a(java.lang.String, java.lang.reflect.Type):java.lang.Object
     arg types: [java.lang.String, java.lang.Class]
     candidates:
      com.a.a.j.a(com.a.a.j, double):void
      com.a.a.j.a(java.lang.Object, com.a.a.d.a):void
      com.a.a.j.a(com.a.a.ag, com.a.a.c.a):com.a.a.af
      com.a.a.j.a(com.a.a.d.a, java.lang.reflect.Type):java.lang.Object
      com.a.a.j.a(java.io.Reader, java.lang.reflect.Type):java.lang.Object
      com.a.a.j.a(java.lang.String, java.lang.Class):java.lang.Object
      com.a.a.j.a(java.lang.Object, java.lang.reflect.Type):java.lang.String
      com.a.a.j.a(com.a.a.u, com.a.a.d.f):void
      com.a.a.j.a(com.a.a.u, java.lang.Appendable):void
      com.a.a.j.a(java.lang.String, java.lang.reflect.Type):java.lang.Object */
    public Object a(String str, Class cls) {
        return com.a.a.b.t.a(cls).cast(a(str, (Type) cls));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.j.a(java.io.Reader, java.lang.reflect.Type):java.lang.Object
     arg types: [java.io.StringReader, java.lang.reflect.Type]
     candidates:
      com.a.a.j.a(com.a.a.j, double):void
      com.a.a.j.a(java.lang.Object, com.a.a.d.a):void
      com.a.a.j.a(com.a.a.ag, com.a.a.c.a):com.a.a.af
      com.a.a.j.a(com.a.a.d.a, java.lang.reflect.Type):java.lang.Object
      com.a.a.j.a(java.lang.String, java.lang.Class):java.lang.Object
      com.a.a.j.a(java.lang.String, java.lang.reflect.Type):java.lang.Object
      com.a.a.j.a(java.lang.Object, java.lang.reflect.Type):java.lang.String
      com.a.a.j.a(com.a.a.u, com.a.a.d.f):void
      com.a.a.j.a(com.a.a.u, java.lang.Appendable):void
      com.a.a.j.a(java.io.Reader, java.lang.reflect.Type):java.lang.Object */
    public Object a(String str, Type type) {
        if (str == null) {
            return null;
        }
        return a((Reader) new StringReader(str), type);
    }

    public String a(u uVar) {
        StringWriter stringWriter = new StringWriter();
        a(uVar, stringWriter);
        return stringWriter.toString();
    }

    public String a(Object obj) {
        return obj == null ? a((u) w.a) : a(obj, obj.getClass());
    }

    public String a(Object obj, Type type) {
        StringWriter stringWriter = new StringWriter();
        a(obj, type, stringWriter);
        return stringWriter.toString();
    }

    public void a(u uVar, com.a.a.d.f fVar) {
        boolean g2 = fVar.g();
        fVar.b(true);
        boolean h2 = fVar.h();
        fVar.c(this.h);
        boolean i2 = fVar.i();
        fVar.d(this.g);
        try {
            u.a(uVar, fVar);
            fVar.b(g2);
            fVar.c(h2);
            fVar.d(i2);
        } catch (IOException e2) {
            throw new v(e2);
        } catch (Throwable th) {
            fVar.b(g2);
            fVar.c(h2);
            fVar.d(i2);
            throw th;
        }
    }

    public void a(u uVar, Appendable appendable) {
        try {
            a(uVar, a(u.a(appendable)));
        } catch (IOException e2) {
            throw new RuntimeException(e2);
        }
    }

    public void a(Object obj, Type type, com.a.a.d.f fVar) {
        af a2 = a(com.a.a.c.a.a(type));
        boolean g2 = fVar.g();
        fVar.b(true);
        boolean h2 = fVar.h();
        fVar.c(this.h);
        boolean i2 = fVar.i();
        fVar.d(this.g);
        try {
            a2.a(fVar, obj);
            fVar.b(g2);
            fVar.c(h2);
            fVar.d(i2);
        } catch (IOException e2) {
            throw new v(e2);
        } catch (Throwable th) {
            fVar.b(g2);
            fVar.c(h2);
            fVar.d(i2);
            throw th;
        }
    }

    public void a(Object obj, Type type, Appendable appendable) {
        try {
            a(obj, type, a(u.a(appendable)));
        } catch (IOException e2) {
            throw new v(e2);
        }
    }

    public String toString() {
        return "{" + "serializeNulls:" + this.g + "factories:" + this.e + ",instanceCreators:" + this.f + "}";
    }
}
