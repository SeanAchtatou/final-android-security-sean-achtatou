package com.a.a;

public final class w extends u {
    public static final w a = new w();

    public boolean equals(Object obj) {
        return this == obj || (obj instanceof w);
    }

    public int hashCode() {
        return w.class.hashCode();
    }
}
