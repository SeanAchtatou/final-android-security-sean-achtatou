package com.a.a;

import com.a.a.b.a;
import com.a.a.b.y;
import java.util.Set;

public final class x extends u {
    private final y a = new y();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.b.y.a(java.lang.String, java.lang.Object):java.lang.Object
     arg types: [java.lang.String, com.a.a.u]
     candidates:
      com.a.a.b.y.a(java.lang.Object, java.lang.Object):boolean
      com.a.a.b.y.a(java.lang.String, java.lang.Object):java.lang.Object */
    public void a(String str, u uVar) {
        if (uVar == null) {
            uVar = w.a;
        }
        this.a.put((String) a.a(str), (Object) uVar);
    }

    public boolean equals(Object obj) {
        return obj == this || ((obj instanceof x) && ((x) obj).a.equals(this.a));
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    public Set o() {
        return this.a.entrySet();
    }
}
