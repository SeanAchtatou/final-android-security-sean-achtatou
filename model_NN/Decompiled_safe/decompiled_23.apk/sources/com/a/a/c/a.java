package com.a.a.c;

import com.a.a.b.b;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class a {
    final Class a;
    final Type b;
    final int c;

    protected a() {
        this.b = a((Class) getClass());
        this.a = b.e(this.b);
        this.c = this.b.hashCode();
    }

    a(Type type) {
        this.b = b.d((Type) com.a.a.b.a.a(type));
        this.a = b.e(this.b);
        this.c = this.b.hashCode();
    }

    public static a a(Type type) {
        return new a(type);
    }

    static Type a(Class cls) {
        Type genericSuperclass = cls.getGenericSuperclass();
        if (!(genericSuperclass instanceof Class)) {
            return b.d(((ParameterizedType) genericSuperclass).getActualTypeArguments()[0]);
        }
        throw new RuntimeException("Missing type parameter.");
    }

    public static a b(Class cls) {
        return new a(cls);
    }

    public final Class a() {
        return this.a;
    }

    public final Type b() {
        return this.b;
    }

    public final boolean equals(Object obj) {
        return (obj instanceof a) && b.a(this.b, ((a) obj).b);
    }

    public final int hashCode() {
        return this.c;
    }

    public final String toString() {
        return b.f(this.b);
    }
}
