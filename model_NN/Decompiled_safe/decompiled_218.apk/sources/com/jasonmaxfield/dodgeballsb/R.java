package com.jasonmaxfield.dodgeballsb;

public final class R {

    public static final class anim {
        public static final int slide_left_in = 2130968576;
        public static final int slide_left_out = 2130968577;
        public static final int slide_right_in = 2130968578;
        public static final int slide_right_out = 2130968579;
    }

    public static final class array {
        public static final int file_names = 2131296259;
        public static final int new_filenames = 2131296261;
        public static final int new_titles = 2131296260;
        public static final int tab_titles = 2131296256;
        public static final int tab_values = 2131296257;
        public static final int titles = 2131296258;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int background = 2130837504;
        public static final int icon = 2130837505;
        public static final int search = 2130837506;
        public static final int star = 2130837507;
        public static final int type_ringtone = 2130837508;
    }

    public static final class id {
        public static final int adView = 2131427338;
        public static final int buttonPlaySound = 2131427339;
        public static final int itemClearFavorites = 2131427341;
        public static final int list1 = 2131427335;
        public static final int list2 = 2131427336;
        public static final int list3 = 2131427337;
        public static final int lvSoundPick = 2131427333;
        public static final int mainlist = 2131427334;
        public static final int prefs = 2131427342;
        public static final int row_display_name = 2131427331;
        public static final int row_ringtone = 2131427329;
        public static final int row_starred = 2131427330;
        public static final int search_filter = 2131427328;
        public static final int stop = 2131427340;
        public static final int tvFavorites = 2131427332;
    }

    public static final class integer {
        public static final int fling_animation_duration = 2131165184;
    }

    public static final class layout {
        public static final int choose_contact = 2130903040;
        public static final int contact_row = 2130903041;
        public static final int listitem = 2130903042;
        public static final int sound_pick_layout = 2130903043;
        public static final int tab = 2130903044;
        public static final int widget_layout = 2130903045;
    }

    public static final class menu {
        public static final int menu = 2131361792;
    }

    public static final class raw {
        public static final int a_lot_dumber = 2131099648;
        public static final int adaa = 2131099649;
        public static final int adopted = 2131099650;
        public static final int better_runs = 2131099651;
        public static final int bleed_my_own_blood = 2131099652;
        public static final int blindfold = 2131099653;
        public static final int blood_and_semen = 2131099654;
        public static final int bold_strategy = 2131099655;
        public static final int bollocks = 2131099656;
        public static final int bullseye = 2131099657;
        public static final int bunch_of_retards = 2131099658;
        public static final int cock_flavored_lollipop = 2131099659;
        public static final int convenient_for_you = 2131099660;
        public static final int couldnt_hit_water = 2131099661;
        public static final int cram_it = 2131099662;
        public static final int cup_holders = 2131099663;
        public static final int damn_you_bernice = 2131099664;
        public static final int dodge_a_wrench_long = 2131099665;
        public static final int dodge_a_wrench_short = 2131099666;
        public static final int dodgeball_is_a_sport = 2131099667;
        public static final int drain_the_sea_monster = 2131099668;
        public static final int drink_my_own_urine = 2131099669;
        public static final int f_n_a = 2131099670;
        public static final int f_n_chuck_norris = 2131099671;
        public static final int facial = 2131099672;
        public static final int fanzi = 2131099673;
        public static final int gar_this_sucks = 2131099674;
        public static final int get_a_load_of_this_guy = 2131099675;
        public static final int gotta_be_the_hair = 2131099676;
        public static final int had_me_at_blood_and_semen = 2131099677;
        public static final int have_a_goal = 2131099678;
        public static final int have_a_goal_long = 2131099679;
        public static final int hippies = 2131099680;
        public static final int hump_it_into_submission = 2131099681;
        public static final int i_didn_t_think_nazi_camp_got_out_til_8 = 2131099682;
        public static final int i_know_you_know = 2131099683;
        public static final int im_your_boss = 2131099684;
        public static final int jackpot = 2131099685;
        public static final int joanie_love_chachi = 2131099686;
        public static final int jokey_jokemaker = 2131099687;
        public static final int l_for_love = 2131099688;
        public static final int lesbian = 2131099689;
        public static final int mate_long = 2131099690;
        public static final int mate_short = 2131099691;
        public static final int mental_sweat = 2131099692;
        public static final int metaphor = 2131099693;
        public static final int might_be_dead = 2131099694;
        public static final int mouth_where_our_balls_are = 2131099695;
        public static final int ms_mr = 2131099696;
        public static final int my_sweet_dick = 2131099697;
        public static final int nice_and_deep = 2131099698;
        public static final int not_a_loser = 2131099699;
        public static final int ouch_town = 2131099700;
        public static final int peckerslap = 2131099701;
        public static final int personality = 2131099702;
        public static final int philosophizer = 2131099703;
        public static final int pirate_long = 2131099704;
        public static final int pirate_short = 2131099705;
        public static final int pumpkins = 2131099706;
        public static final int queer_bag = 2131099707;
        public static final int read_about_it = 2131099708;
        public static final int ready_for_the_hurricane = 2131099709;
        public static final int real_freakin_naughty = 2131099710;
        public static final int satisfy_a_camel = 2131099711;
        public static final int scared_the_jeepers = 2131099712;
        public static final int shackles = 2131099713;
        public static final int suit_yourself_queer = 2131099714;
        public static final int super_psyched = 2131099715;
        public static final int take_care_of_your_balls = 2131099716;
        public static final int thank_you_chuck_norris = 2131099717;
        public static final int that_dike_can_play = 2131099718;
        public static final int the_5_ds = 2131099719;
        public static final int theyre_too_good = 2131099720;
        public static final int thong_song = 2131099721;
        public static final int threw_up_in_my_mouth = 2131099722;
        public static final int we_will_rock_you = 2131099723;
        public static final int were_better_than_you = 2131099724;
        public static final int whats_a_taint = 2131099725;
        public static final int white = 2131099726;
        public static final int why_hit_a_girl = 2131099727;
    }

    public static final class string {
        public static final int app_name = 2131230720;
        public static final int choose_contact_ringtone_button = 2131230722;
        public static final int choose_contact_title = 2131230723;
        public static final int contact_picker = 2131230725;
        public static final int help = 2131230721;
        public static final int success_contact_ringtone = 2131230724;
    }

    public static final class xml {
        public static final int prefs = 2131034112;
        public static final int widget_info = 2131034113;
    }
}
