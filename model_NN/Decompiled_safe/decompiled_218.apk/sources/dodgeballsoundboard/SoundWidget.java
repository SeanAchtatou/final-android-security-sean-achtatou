package dodgeballsoundboard;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.widget.RemoteViews;
import android.widget.Toast;
import com.jasonmaxfield.dodgeballsb.R;

public class SoundWidget extends AppWidgetProvider {
    public static String ACTION_WIDGET_CONFIGURE = "ConfigureWidget";
    public static String ACTION_WIDGET_RECEIVER = "ActionReceiverWidget";
    private MediaPlayer mMediaPlayer = null;

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    public void onReceive(Context context, Intent intent) {
        if ("android.appwidget.action.APPWIDGET_DELETED".equals(intent.getAction())) {
            int appWidgetId = intent.getExtras().getInt("appWidgetId", 0);
            if (appWidgetId != 0) {
                onDeleted(context, new int[]{appWidgetId});
                return;
            }
            return;
        }
        if (intent.getAction().equals(ACTION_WIDGET_RECEIVER)) {
            int resId = intent.getIntExtra("ressound", 0);
            if (resId == 0) {
                Toast.makeText(context, "Please re-add widget", 1);
            } else {
                if (this.mMediaPlayer != null) {
                    this.mMediaPlayer.stop();
                    this.mMediaPlayer.release();
                }
                this.mMediaPlayer = MediaPlayer.create(context, resId);
                this.mMediaPlayer.start();
            }
        }
        super.onReceive(context, intent);
    }

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {
        int rawId = SoundPickActivity.loadResId(context, appWidgetId);
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), (int) R.layout.widget_layout);
        Intent active = new Intent(context, SoundWidget.class);
        active.setAction(ACTION_WIDGET_RECEIVER);
        active.putExtra("ressound", rawId);
        active.putExtra("appWidgetId", appWidgetId);
        active.setData(Uri.withAppendedPath(Uri.parse("soundwidget://widget/id/#" + rawId + appWidgetId), String.valueOf(appWidgetId)));
        remoteViews.setTextViewText(R.id.buttonPlaySound, SoundPickActivity.loadTitlePref(context, appWidgetId));
        remoteViews.setOnClickPendingIntent(R.id.buttonPlaySound, PendingIntent.getBroadcast(context, 0, active, 0));
        appWidgetManager.updateAppWidget(appWidgetId, remoteViews);
    }
}
