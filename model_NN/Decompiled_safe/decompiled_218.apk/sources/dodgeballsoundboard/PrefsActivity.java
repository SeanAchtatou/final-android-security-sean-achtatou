package dodgeballsoundboard;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;
import com.jasonmaxfield.dodgeballsb.R;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class PrefsActivity extends PreferenceActivity {
    private static final String TAG = "DodgeballSoundboard";
    int faves = 0;
    boolean success = false;
    /* access modifiers changed from: private */
    public String versionNameText;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.faves = getIntent().getIntExtra("faves_number", 0);
        addPreferencesFromResource(R.xml.prefs);
        Preference pref = findPreference("version");
        try {
            this.versionNameText = String.valueOf(getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            Log.d(TAG, "Couldn't get Version Number");
        }
        pref.setSummary("Version: " + this.versionNameText);
        getPreferenceManager().findPreference("donate").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setData(Uri.parse("https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=S3DD74UNLWPV6&lc=US&item_name=Donations&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donate_SM%2egif%3aNonHosted"));
                PrefsActivity.this.startActivity(intent);
                return true;
            }
        });
        getPreferenceManager().findPreference("request_email").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                Intent emailIntent = new Intent("android.intent.action.SEND");
                emailIntent.putExtra("android.intent.extra.EMAIL", new String[]{"thejasonmaxfield@gmail.com"});
                emailIntent.putExtra("android.intent.extra.SUBJECT", "Dodgeball Soundboard Request");
                emailIntent.putExtra("android.intent.extra.TEXT", "Scene/Time:\n\nCharacter:\n\nLine:");
                emailIntent.setType("plain/text");
                PrefsActivity.this.startActivity(emailIntent);
                return true;
            }
        });
        getPreferenceManager().findPreference("send_email").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                Intent emailIntent = new Intent("android.intent.action.SEND");
                emailIntent.putExtra("android.intent.extra.EMAIL", new String[]{"thejasonmaxfield@gmail.com"});
                emailIntent.putExtra("android.intent.extra.SUBJECT", "dodgeball Soundboard");
                emailIntent.putExtra("android.intent.extra.TEXT", "\n\n\nVersion: " + PrefsActivity.this.versionNameText);
                emailIntent.setType("plain/text");
                PrefsActivity.this.startActivity(emailIntent);
                return true;
            }
        });
        getPreferenceManager().findPreference("market").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setData(Uri.parse("market://search?q=pub:\"Jason Maxfield\""));
                PrefsActivity.this.startActivity(intent);
                return true;
            }
        });
        getPreferenceManager().findPreference("help").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(PrefsActivity.this);
                dialogBuilder.setTitle("Help/Tips");
                TextView textView = new TextView(PrefsActivity.this);
                textView.setMovementMethod(LinkMovementMethod.getInstance());
                textView.setText((int) R.string.help);
                textView.setTextSize(2, 16.0f);
                textView.setTextColor(-1);
                dialogBuilder.setView(textView);
                dialogBuilder.setPositiveButton("Close", (DialogInterface.OnClickListener) null);
                dialogBuilder.show();
                return true;
            }
        });
        getPreferenceManager().findPreference("backup").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                PrefsActivity.this.backupDialog();
                return true;
            }
        });
        getPreferenceManager().findPreference("restore").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                PrefsActivity.this.restoreDialog();
                return true;
            }
        });
    }

    public void backupDialog() {
        new AlertDialog.Builder(this).setMessage("If backup exists, this will overwrite it.\nAre you sure?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                PrefsActivity.this.backupFavorites("favorite_titles.txt");
                PrefsActivity.this.backupFavorites("filenames.txt");
                if (PrefsActivity.this.success) {
                    Toast.makeText(PrefsActivity.this.getApplicationContext(), "Backup Complete", 0).show();
                    PrefsActivity.this.success = false;
                    return;
                }
                Toast.makeText(PrefsActivity.this.getApplicationContext(), "There were no favorites found", 0).show();
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        }).create().show();
    }

    public void restoreDialog() {
        new AlertDialog.Builder(this).setMessage("This will overwrite existing favorites.\nAre you sure?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                PrefsActivity.this.restoreFavorites("favorite_titles.txt");
                PrefsActivity.this.restoreFavorites("filenames.txt");
                if (PrefsActivity.this.success) {
                    Toast.makeText(PrefsActivity.this.getApplicationContext(), "Restore Complete", 0).show();
                    PrefsActivity.this.success = false;
                    return;
                }
                Toast.makeText(PrefsActivity.this.getApplicationContext(), "There were no backups found", 0).show();
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        }).create().show();
    }

    public void backupFavorites(String fname) {
        byte[] buffer = null;
        String path = String.valueOf(Environment.getExternalStorageDirectory().toString()) + "/Dodgeball/";
        if (this.faves > 0) {
            try {
                FileInputStream fIn = openFileInput(fname);
                buffer = new byte[fIn.available()];
                fIn.read(buffer);
                fIn.close();
            } catch (IOException e) {
                Log.d(TAG, "Unable to load fav file");
            }
            File newFile = new File(String.valueOf(path) + fname);
            if (!newFile.exists()) {
                new File(path).mkdirs();
            }
            newFile.delete();
            try {
                FileOutputStream save = new FileOutputStream(String.valueOf(path) + fname);
                save.write(buffer);
                save.flush();
                save.close();
            } catch (FileNotFoundException e2) {
                Log.d(TAG, "Unable to find sdcard file");
            } catch (IOException e3) {
                Log.d(TAG, "Unable to write sdcard file");
            }
            this.success = true;
        }
    }

    public void restoreFavorites(String fname) {
        byte[] buffer = null;
        String sdpath = String.valueOf(Environment.getExternalStorageDirectory().toString()) + "/Dodgeball/";
        if (new File(sdpath, fname).exists()) {
            try {
                InputStream fIn = new BufferedInputStream(new FileInputStream(String.valueOf(sdpath) + fname));
                buffer = new byte[fIn.available()];
                fIn.read(buffer);
                fIn.close();
            } catch (IOException e) {
                Log.d(TAG, "Unable to load fav file");
            }
            String dataPath = getApplicationContext().getFilesDir() + "/";
            File dataFile = new File(fname);
            if (dataFile.exists()) {
                dataFile.delete();
            }
            try {
                FileOutputStream save = new FileOutputStream(String.valueOf(dataPath) + fname);
                save.write(buffer);
                save.flush();
                save.close();
            } catch (FileNotFoundException e2) {
                Log.d(TAG, "Unable to find sdcard file");
            } catch (IOException e3) {
                Log.d(TAG, "Unable to write sdcard file");
            }
            this.success = true;
        }
    }
}
