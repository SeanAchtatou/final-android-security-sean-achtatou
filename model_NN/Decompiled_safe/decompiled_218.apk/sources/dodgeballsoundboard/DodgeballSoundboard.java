package dodgeballsoundboard;

import android.app.TabActivity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextMenu;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.Toast;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.jasonmaxfield.dodgeballsb.R;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class DodgeballSoundboard extends TabActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    private static final int ADDALARM = 5;
    private static final int ADDFAV = 0;
    private static final int ADDRING = 1;
    private static final int FAVID = 1;
    private static final String FILENAMES = "filenames.txt";
    private static final String FILENAME_TITLES = "favorite_titles.txt";
    private static final String LIST1_TAB_TAG = "Old";
    private static final String LIST2_TAB_TAG = "New";
    private static final String LIST3_TAB_TAG = "Favorites";
    private static final int NEWID = 2;
    private static final int OLDID = 3;
    private static final int REMOVEFAV = 7;
    private static final int SETCONTACT = 4;
    private static final int SETNOTIF = 3;
    private static final int SETRING = 2;
    private static final int SHARE = 6;
    private static final int SWIPE_MAX_OFF_PATH = 50;
    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
    private static final String TAG = "DodgeballSoundboard";
    /* access modifiers changed from: private */
    public String[] FNames;
    /* access modifiers changed from: private */
    public String[] FavoriteFNames = null;
    private String[] FavoriteTitles = null;
    /* access modifiers changed from: private */
    public String[] NewFNames;
    private String[] NewTitles;
    private String[] Titles;
    private FrameLayout fLayout;
    List<String> favoriteFNamesStrings = new ArrayList();
    List<String> favoriteTitlesStrings = new ArrayList();
    /* access modifiers changed from: private */
    public GestureDetector gestureDetector;
    View.OnTouchListener gestureListener;
    /* access modifiers changed from: private */
    public ListView lvFavorites;
    private int lvId = ADDFAV;
    /* access modifiers changed from: private */
    public ListView lvNew;
    /* access modifiers changed from: private */
    public ListView lvOld;
    MediaPlayer mp;
    SharedPreferences prefs;
    /* access modifiers changed from: private */
    public boolean swipe = true;
    /* access modifiers changed from: private */
    public TabHost tabHost;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.tab);
        this.mp = new MediaPlayer();
        this.fLayout = (FrameLayout) findViewById(16908305);
        this.prefs = PreferenceManager.getDefaultSharedPreferences(this);
        this.prefs.registerOnSharedPreferenceChangeListener(this);
        setVolumeControlStream(3);
        this.favoriteTitlesStrings = new ReadFiles(this, null).execute(FILENAME_TITLES, this.favoriteTitlesStrings);
        this.favoriteFNamesStrings = new ReadFiles(this, null).execute(FILENAMES, this.favoriteFNamesStrings);
        this.Titles = getResources().getStringArray(R.array.titles);
        Arrays.sort(this.Titles, new Comparator<String>() {
            public int compare(String s1, String s2) {
                return s1.compareToIgnoreCase(s2);
            }
        });
        this.FNames = getResources().getStringArray(R.array.file_names);
        Arrays.sort(this.FNames, new Comparator<String>() {
            public int compare(String s1, String s2) {
                return s1.compareToIgnoreCase(s2);
            }
        });
        this.NewTitles = getResources().getStringArray(R.array.new_titles);
        this.NewFNames = getResources().getStringArray(R.array.new_filenames);
        this.FavoriteFNames = new String[this.favoriteFNamesStrings.size()];
        this.favoriteFNamesStrings.toArray(this.FavoriteFNames);
        this.FavoriteTitles = new String[this.favoriteTitlesStrings.size()];
        this.favoriteTitlesStrings.toArray(this.FavoriteTitles);
        this.tabHost = getTabHost();
        ((AdView) findViewById(R.id.adView)).loadAd(new AdRequest());
        this.lvOld = (ListView) findViewById(R.id.list1);
        this.lvOld.setAdapter((ListAdapter) new ArrayAdapter(this, (int) R.layout.listitem, this.Titles));
        registerForContextMenu(this.lvOld);
        this.lvNew = (ListView) findViewById(R.id.list2);
        this.lvNew.setAdapter((ListAdapter) new ArrayAdapter(this, (int) R.layout.listitem, this.NewTitles));
        registerForContextMenu(this.lvNew);
        this.lvFavorites = (ListView) findViewById(R.id.list3);
        this.lvFavorites.setAdapter((ListAdapter) new ArrayAdapter(this, 17367043, this.favoriteTitlesStrings));
        registerForContextMenu(this.lvFavorites);
        this.lvOld.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (DodgeballSoundboard.this.swipe) {
                    String filename = DodgeballSoundboard.this.FNames[position];
                    AssetFileDescriptor afd = DodgeballSoundboard.this.getResources().openRawResourceFd(DodgeballSoundboard.this.getResources().getIdentifier(filename, "raw", "com.jasonmaxfield.dodgeballsb"));
                    DodgeballSoundboard.this.mp.reset();
                    try {
                        DodgeballSoundboard.this.mp.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                        DodgeballSoundboard.this.mp.prepare();
                    } catch (IOException e) {
                        Log.d(DodgeballSoundboard.TAG, "Unable to load Favorite sound " + filename);
                    }
                    DodgeballSoundboard.this.mp.start();
                    DodgeballSoundboard.this.swipe = true;
                }
            }
        });
        this.lvNew.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (DodgeballSoundboard.this.swipe) {
                    String filename = DodgeballSoundboard.this.NewFNames[position];
                    AssetFileDescriptor afd = DodgeballSoundboard.this.getResources().openRawResourceFd(DodgeballSoundboard.this.getResources().getIdentifier(filename, "raw", "com.jasonmaxfield.dodgeballsb"));
                    DodgeballSoundboard.this.mp.reset();
                    try {
                        DodgeballSoundboard.this.mp.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                        DodgeballSoundboard.this.mp.prepare();
                    } catch (IOException e) {
                        Log.d(DodgeballSoundboard.TAG, "Unable to load New sound " + filename);
                    }
                    DodgeballSoundboard.this.mp.start();
                    DodgeballSoundboard.this.swipe = true;
                }
            }
        });
        this.lvFavorites.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (DodgeballSoundboard.this.swipe) {
                    String filename = DodgeballSoundboard.this.FavoriteFNames[position];
                    int rawId = DodgeballSoundboard.this.getResources().getIdentifier(filename, "raw", "com.jasonmaxfield.dodgeballsb");
                    if (rawId != 0) {
                        AssetFileDescriptor afd = DodgeballSoundboard.this.getResources().openRawResourceFd(rawId);
                        DodgeballSoundboard.this.mp.reset();
                        try {
                            DodgeballSoundboard.this.mp.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                            DodgeballSoundboard.this.mp.prepare();
                        } catch (IOException e) {
                            Log.d(DodgeballSoundboard.TAG, "Unable to load Favorite sound " + filename);
                        }
                        DodgeballSoundboard.this.mp.start();
                        DodgeballSoundboard.this.swipe = true;
                        return;
                    }
                    Toast.makeText(DodgeballSoundboard.this, "Unable to play sound. Please remove and re-add to favorites", 1).show();
                }
            }
        });
        this.gestureDetector = new GestureDetector(new MyGestureDetector());
        this.gestureListener = new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                DodgeballSoundboard.this.swipe = true;
                DodgeballSoundboard.this.gestureDetector.onTouchEvent(event);
                return false;
            }
        };
        if (!this.prefs.getBoolean("swipeTabs", true)) {
            this.lvOld.setOnTouchListener(null);
            this.lvNew.setOnTouchListener(null);
            this.lvFavorites.setOnTouchListener(null);
        } else {
            this.lvOld.setOnTouchListener(this.gestureListener);
            this.lvNew.setOnTouchListener(this.gestureListener);
            this.lvFavorites.setOnTouchListener(this.gestureListener);
        }
        this.tabHost.addTab(this.tabHost.newTabSpec(LIST1_TAB_TAG).setIndicator(LIST1_TAB_TAG).setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String arg0) {
                return DodgeballSoundboard.this.lvOld;
            }
        }));
        this.tabHost.addTab(this.tabHost.newTabSpec(LIST2_TAB_TAG).setIndicator(LIST2_TAB_TAG).setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String arg0) {
                return DodgeballSoundboard.this.lvNew;
            }
        }));
        this.tabHost.addTab(this.tabHost.newTabSpec(LIST3_TAB_TAG).setIndicator(LIST3_TAB_TAG).setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String arg0) {
                return DodgeballSoundboard.this.lvFavorites;
            }
        }));
        this.fLayout.setBackgroundDrawable(getResources().getDrawable(getResources().getIdentifier("background", "drawable", "com.jasonmaxfield.dodgeballsb")));
        this.tabHost.getTabWidget().getChildAt(ADDFAV).getLayoutParams().height = 70;
        this.tabHost.getTabWidget().getChildAt(1).getLayoutParams().height = 70;
        this.tabHost.getTabWidget().getChildAt(2).getLayoutParams().height = 70;
        this.tabHost.setCurrentTab(Integer.parseInt(this.prefs.getString("tabs", "1")));
    }

    public void setListViewId(int id) {
        this.lvId = id;
    }

    public int getListViewId() {
        return this.lvId;
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        setListViewId(ADDFAV);
        switch (v.getId()) {
            case R.id.list1:
                contextMenu(menu, menuInfo, this.Titles);
                setListViewId(3);
                return;
            case R.id.list2:
                contextMenu(menu, menuInfo, this.NewTitles);
                setListViewId(2);
                return;
            case R.id.list3:
                favoritesContextMenu(menu, menuInfo, this.FavoriteTitles);
                setListViewId(1);
                return;
            default:
                return;
        }
    }

    public void contextMenu(ContextMenu menu, ContextMenu.ContextMenuInfo menuInfo, String[] string) {
        menu.setHeaderTitle(string[((AdapterView.AdapterContextMenuInfo) menuInfo).position]);
        menu.add((int) ADDFAV, (int) ADDFAV, (int) ADDFAV, "Add to Favorites");
        menu.add((int) ADDFAV, 1, 1, "Add to Ringtones");
        menu.add((int) ADDFAV, 2, 2, "Set as Phone Ringtone");
        menu.add((int) ADDFAV, 3, 3, "Set as Notification Ringtone");
        menu.add((int) ADDFAV, (int) SETCONTACT, (int) SETCONTACT, "Set as Contact Ringtone");
        menu.add((int) ADDFAV, (int) ADDALARM, (int) ADDALARM, "Add to Alarms");
        menu.add((int) ADDFAV, (int) SHARE, (int) SHARE, "Share");
    }

    public void favoritesContextMenu(ContextMenu menu, ContextMenu.ContextMenuInfo menuInfo, String[] string) {
        menu.setHeaderTitle(string[((AdapterView.AdapterContextMenuInfo) menuInfo).position]);
        menu.add((int) ADDFAV, (int) REMOVEFAV, (int) ADDFAV, "Remove from Favorites");
        menu.add((int) ADDFAV, 1, 1, "Add to Ringtones");
        menu.add((int) ADDFAV, 2, 2, "Set as Phone Ringtone");
        menu.add((int) ADDFAV, 3, 3, "Set as Notification Ringtone");
        menu.add((int) ADDFAV, (int) SETCONTACT, (int) SETCONTACT, "Set as Contact Ringtone");
        menu.add((int) ADDFAV, (int) ADDALARM, (int) ADDALARM, "Add to Alarms");
        menu.add((int) ADDFAV, (int) SHARE, (int) SHARE, "Share");
    }

    public boolean onContextItemSelected(MenuItem item) {
        int menuItemIndex = item.getItemId();
        int id = ((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).position;
        switch (getListViewId()) {
            case 1:
                String favoriteFNames = this.FavoriteFNames[id];
                String favoriteTitles = this.FavoriteTitles[id];
                int favRawId = getRawId(favoriteFNames);
                if (menuItemIndex == REMOVEFAV) {
                    removeFromFavorites(favoriteTitles, favoriteFNames);
                    return true;
                }
                setRingtone(favRawId, menuItemIndex, favoriteFNames, favoriteTitles);
                return true;
            case 2:
                String newFilename = this.NewFNames[id];
                String newTitle = this.NewTitles[id];
                int newRawId = getRawId(newFilename);
                if (menuItemIndex == 0) {
                    addToFavorites(newTitle, newFilename);
                    return true;
                }
                setRingtone(newRawId, menuItemIndex, newFilename, newTitle);
                return true;
            case 3:
                String oldFilename = this.FNames[id];
                String oldTitle = this.Titles[id];
                int oldRawId = getRawId(oldFilename);
                if (menuItemIndex == 0) {
                    addToFavorites(oldTitle, oldFilename);
                    return true;
                }
                setRingtone(oldRawId, menuItemIndex, oldFilename, oldTitle);
                return true;
            default:
                return true;
        }
    }

    public void addToFavorites(String title, String filename) {
        ((ArrayAdapter) this.lvFavorites.getAdapter()).add(title);
        this.favoriteFNamesStrings.add(filename);
        this.FavoriteFNames = new String[this.favoriteFNamesStrings.size()];
        this.favoriteFNamesStrings.toArray(this.FavoriteFNames);
        this.FavoriteTitles = new String[this.favoriteTitlesStrings.size()];
        this.favoriteTitlesStrings.toArray(this.FavoriteTitles);
        Toast.makeText(this, String.valueOf(title) + " added to Favorites", (int) ADDFAV).show();
    }

    public void removeFromFavorites(String title, String filename) {
        ((ArrayAdapter) this.lvFavorites.getAdapter()).remove(title);
        this.favoriteFNamesStrings.remove(filename);
        this.FavoriteFNames = new String[this.favoriteFNamesStrings.size()];
        this.favoriteFNamesStrings.toArray(this.FavoriteFNames);
        this.FavoriteTitles = new String[this.favoriteTitlesStrings.size()];
        this.favoriteTitlesStrings.toArray(this.FavoriteTitles);
        Toast.makeText(this, String.valueOf(title) + " removed from Favorites", (int) ADDFAV).show();
    }

    public int getRawId(String fname) {
        return getResources().getIdentifier(fname, "raw", "com.jasonmaxfield.dodgeballsb");
    }

    public void onPause() {
        super.onPause();
        writeFiles();
        if (this.mp != null) {
            if (this.mp.isPlaying()) {
                this.mp.stop();
                this.mp.reset();
            }
            this.mp.release();
            this.mp = null;
        }
    }

    public void onResume() {
        super.onResume();
        setFavorites();
        this.mp = new MediaPlayer();
    }

    public void onStop() {
        super.onStop();
        if (this.mp != null) {
            if (this.mp.isPlaying()) {
                this.mp.stop();
                this.mp.reset();
            }
            this.mp.release();
            this.mp = null;
        }
    }

    public void writeFiles() {
        new WriteFiles(this, null).execute(FILENAME_TITLES, this.favoriteTitlesStrings);
        new WriteFiles(this, null).execute(FILENAMES, this.favoriteFNamesStrings);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public int favesLength() {
        return this.favoriteTitlesStrings.size();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.stop:
                if (this.mp == null || !this.mp.isPlaying()) {
                    return true;
                }
                this.mp.stop();
                return true;
            case R.id.itemClearFavorites:
                ((ArrayAdapter) this.lvFavorites.getAdapter()).clear();
                this.favoriteFNamesStrings.clear();
                this.FavoriteFNames = new String[this.favoriteFNamesStrings.size()];
                this.favoriteFNamesStrings.toArray(this.FavoriteFNames);
                this.FavoriteTitles = new String[this.favoriteTitlesStrings.size()];
                this.favoriteTitlesStrings.toArray(this.FavoriteTitles);
                Toast.makeText(this, "Favorites list is now empty", (int) ADDFAV).show();
                return true;
            case R.id.prefs:
                startActivity(new Intent(this, PrefsActivity.class).addFlags(131072).putExtra("faves_number", favesLength()));
                return true;
            default:
                return true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    public void setRingtone(int ressound, int option, String fname, String title) {
        byte[] buffer = null;
        InputStream fIn = getBaseContext().getResources().openRawResource(ressound);
        try {
            buffer = new byte[fIn.available()];
            fIn.read(buffer);
            fIn.close();
        } catch (IOException e) {
            Log.d(TAG, "Unable to load raw file");
        }
        String path = String.valueOf(Environment.getExternalStorageDirectory().toString()) + "/media/audio/ringtones/";
        switch (option) {
            case 1:
            case 2:
            case SETCONTACT /*4*/:
            case SHARE /*6*/:
                path = String.valueOf(Environment.getExternalStorageDirectory().toString()) + "/media/audio/ringtones/";
                break;
            case 3:
                path = String.valueOf(Environment.getExternalStorageDirectory().toString()) + "/media/audio/notifications/";
                break;
            case ADDALARM /*5*/:
                path = String.valueOf(Environment.getExternalStorageDirectory().toString()) + "/media/audio/alarms/";
                break;
        }
        String filename = String.valueOf(fname) + ".ogg";
        if (!new File(path).exists()) {
            new File(path).mkdirs();
        }
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(String.valueOf(path) + filename);
            fileOutputStream.write(buffer);
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (FileNotFoundException e2) {
            Log.d(TAG, "Unable to find sdcard file");
        } catch (IOException e3) {
            Log.d(TAG, "Unable to write sdcard file");
        }
        sendBroadcast(new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE", Uri.parse("file://" + path + filename)));
        File k = new File(path, filename);
        ContentValues values = new ContentValues();
        values.put("_data", k.getAbsolutePath());
        values.put("title", title);
        values.put("mime_type", "audio/ogg");
        switch (option) {
            case 1:
            case SHARE /*6*/:
                values.put("is_ringtone", (Boolean) true);
                values.put("is_notification", (Boolean) true);
                values.put("is_alarm", (Boolean) false);
                break;
            case 2:
            case SETCONTACT /*4*/:
                values.put("is_ringtone", (Boolean) true);
                values.put("is_notification", (Boolean) false);
                values.put("is_alarm", (Boolean) false);
                break;
            case 3:
                values.put("is_ringtone", (Boolean) false);
                values.put("is_notification", (Boolean) true);
                values.put("is_alarm", (Boolean) false);
                break;
            case ADDALARM /*5*/:
                values.put("is_ringtone", (Boolean) false);
                values.put("is_notification", (Boolean) false);
                values.put("is_alarm", (Boolean) true);
                break;
        }
        values.put("is_music", (Boolean) false);
        Uri uri = MediaStore.Audio.Media.getContentUriForPath(k.getAbsolutePath());
        getContentResolver().delete(uri, "_data=\"" + k.getAbsolutePath() + "\"", null);
        Uri newUri = getContentResolver().insert(uri, values);
        switch (option) {
            case 1:
                Toast.makeText(this, String.valueOf(title) + " added to Ringtones", 1).show();
                return;
            case 2:
                RingtoneManager.setActualDefaultRingtoneUri(this, 1, newUri);
                Toast.makeText(this, String.valueOf(title) + " set as Phone Ringtone", 1).show();
                return;
            case 3:
                RingtoneManager.setActualDefaultRingtoneUri(this, 2, newUri);
                Toast.makeText(this, String.valueOf(title) + " set as Notification Ringtone", 1).show();
                return;
            case SETCONTACT /*4*/:
                String nUri = newUri.toString();
                Intent intent = new Intent(this, ContactPicker.class);
                intent.putExtra("com.jasonmaxfield.dodgeballsb.DodgeballSoundboard", nUri);
                startActivity(intent);
                return;
            case ADDALARM /*5*/:
                Toast.makeText(this, String.valueOf(title) + " added to Alarms", 1).show();
                return;
            case SHARE /*6*/:
                Intent intent2 = new Intent("android.intent.action.SEND");
                intent2.setType("audio/ogg");
                intent2.putExtra("android.intent.extra.STREAM", Uri.parse("file://" + k.getAbsolutePath()));
                startActivity(Intent.createChooser(intent2, "Send file"));
                return;
            default:
                return;
        }
    }

    private class WriteFiles extends AsyncTask<String, Integer, String> {
        private WriteFiles() {
        }

        /* synthetic */ WriteFiles(DodgeballSoundboard dodgeballSoundboard, WriteFiles writeFiles) {
            this();
        }

        public void execute(String fname, List<String> listStrings) {
            File file = new File(fname);
            if (!file.exists()) {
                file.delete();
            }
            try {
                ObjectOutputStream out = new ObjectOutputStream(DodgeballSoundboard.this.openFileOutput(fname, DodgeballSoundboard.ADDFAV));
                out.writeObject(listStrings);
                out.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            return null;
        }
    }

    public void setFavorites() {
        this.favoriteTitlesStrings = new ReadFiles(this, null).execute(FILENAME_TITLES, this.favoriteTitlesStrings);
        this.favoriteFNamesStrings = new ReadFiles(this, null).execute(FILENAMES, this.favoriteFNamesStrings);
        this.FavoriteFNames = new String[this.favoriteFNamesStrings.size()];
        this.favoriteFNamesStrings.toArray(this.FavoriteFNames);
        this.FavoriteTitles = new String[this.favoriteTitlesStrings.size()];
        this.favoriteTitlesStrings.toArray(this.FavoriteTitles);
        this.lvFavorites = (ListView) findViewById(R.id.list3);
        this.lvFavorites.setAdapter((ListAdapter) new ArrayAdapter(this, 17367043, this.favoriteTitlesStrings));
    }

    private class ReadFiles extends AsyncTask<String, Integer, String> {
        private ReadFiles() {
        }

        /* synthetic */ ReadFiles(DodgeballSoundboard dodgeballSoundboard, ReadFiles readFiles) {
            this();
        }

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v6, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: java.util.List<java.lang.String>} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.util.List<java.lang.String> execute(java.lang.String r7, java.util.List<java.lang.String> r8) {
            /*
                r6 = this;
                java.io.File r2 = new java.io.File
                dodgeballsoundboard.DodgeballSoundboard r5 = dodgeballsoundboard.DodgeballSoundboard.this
                java.io.File r5 = r5.getFilesDir()
                r2.<init>(r5, r7)
                dodgeballsoundboard.DodgeballSoundboard r5 = dodgeballsoundboard.DodgeballSoundboard.this     // Catch:{ FileNotFoundException -> 0x0022, IOException -> 0x0028, ClassNotFoundException -> 0x002e }
                java.io.FileInputStream r3 = r5.openFileInput(r7)     // Catch:{ FileNotFoundException -> 0x0022, IOException -> 0x0028, ClassNotFoundException -> 0x002e }
                java.io.ObjectInputStream r4 = new java.io.ObjectInputStream     // Catch:{ FileNotFoundException -> 0x0022, IOException -> 0x0028, ClassNotFoundException -> 0x002e }
                r4.<init>(r3)     // Catch:{ FileNotFoundException -> 0x0022, IOException -> 0x0028, ClassNotFoundException -> 0x002e }
                java.lang.Object r5 = r4.readObject()     // Catch:{ FileNotFoundException -> 0x0022, IOException -> 0x0028, ClassNotFoundException -> 0x002e }
                r0 = r5
                java.util.List r0 = (java.util.List) r0     // Catch:{ FileNotFoundException -> 0x0022, IOException -> 0x0028, ClassNotFoundException -> 0x002e }
                r8 = r0
                r4.close()     // Catch:{ FileNotFoundException -> 0x0022, IOException -> 0x0028, ClassNotFoundException -> 0x002e }
            L_0x0021:
                return r8
            L_0x0022:
                r5 = move-exception
                r1 = r5
                r1.printStackTrace()
                goto L_0x0021
            L_0x0028:
                r5 = move-exception
                r1 = r5
                r1.printStackTrace()
                goto L_0x0021
            L_0x002e:
                r5 = move-exception
                r1 = r5
                r1.printStackTrace()
                goto L_0x0021
            */
            throw new UnsupportedOperationException("Method not decompiled: dodgeballsoundboard.DodgeballSoundboard.ReadFiles.execute(java.lang.String, java.util.List):java.util.List");
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            return null;
        }
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (!this.prefs.getBoolean("swipeTabs", true)) {
            this.lvOld.setOnTouchListener(null);
            this.lvNew.setOnTouchListener(null);
            this.lvFavorites.setOnTouchListener(null);
            return;
        }
        this.lvOld.setOnTouchListener(this.gestureListener);
        this.lvNew.setOnTouchListener(this.gestureListener);
        this.lvFavorites.setOnTouchListener(this.gestureListener);
    }

    class MyGestureDetector extends GestureDetector.SimpleOnGestureListener {
        int REL_SWIPE_MAX_OFF_PATH = ((int) (((float) (this.dm.densityDpi * DodgeballSoundboard.SWIPE_MAX_OFF_PATH)) / 160.0f));
        int REL_SWIPE_MIN_DISTANCE = ((int) (((float) (this.dm.densityDpi * DodgeballSoundboard.SWIPE_MIN_DISTANCE)) / 160.0f));
        int REL_SWIPE_THRESHOLD_VELOCITY = ((int) (((float) (this.dm.densityDpi * DodgeballSoundboard.SWIPE_THRESHOLD_VELOCITY)) / 160.0f));
        DisplayMetrics dm;
        Animation mLeftInAnimation;
        Animation mLeftOutAnimation;
        Animation mRightInAnimation;
        Animation mRightOutAnimation;

        MyGestureDetector() {
            this.dm = DodgeballSoundboard.this.getResources().getDisplayMetrics();
        }

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            Animation animation;
            Animation animation2;
            this.mRightInAnimation = AnimationUtils.loadAnimation(DodgeballSoundboard.this, R.anim.slide_right_in);
            this.mRightOutAnimation = AnimationUtils.loadAnimation(DodgeballSoundboard.this, R.anim.slide_right_out);
            this.mLeftInAnimation = AnimationUtils.loadAnimation(DodgeballSoundboard.this, R.anim.slide_left_in);
            this.mLeftOutAnimation = AnimationUtils.loadAnimation(DodgeballSoundboard.this, R.anim.slide_left_out);
            try {
                if (Math.abs(e1.getY() - e2.getY()) > ((float) this.REL_SWIPE_MAX_OFF_PATH)) {
                    return DodgeballSoundboard.ADDFAV;
                }
                int tabCount = DodgeballSoundboard.this.tabHost.getTabWidget().getTabCount();
                int currentTab = DodgeballSoundboard.this.tabHost.getCurrentTab();
                boolean right = (e1.getX() - e2.getX() <= ((float) this.REL_SWIPE_MIN_DISTANCE) || Math.abs(velocityX) <= ((float) this.REL_SWIPE_THRESHOLD_VELOCITY)) ? DodgeballSoundboard.ADDFAV : true;
                int newTab = MathUtils.constrain((right ? 1 : -1) + currentTab, (int) DodgeballSoundboard.ADDFAV, tabCount - 1);
                if (newTab != currentTab) {
                    View currentView = DodgeballSoundboard.this.tabHost.getCurrentView();
                    DodgeballSoundboard.this.tabHost.setCurrentTab(newTab);
                    View newView = DodgeballSoundboard.this.tabHost.getCurrentView();
                    if (right) {
                        animation = this.mRightInAnimation;
                    } else {
                        animation = this.mLeftInAnimation;
                    }
                    newView.startAnimation(animation);
                    if (right) {
                        animation2 = this.mRightOutAnimation;
                    } else {
                        animation2 = this.mLeftOutAnimation;
                    }
                    currentView.startAnimation(animation2);
                    DodgeballSoundboard.this.swipe = false;
                }
                return DodgeballSoundboard.ADDFAV;
            } catch (Exception e) {
            }
        }
    }
}
