package dodgeballsoundboard;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RemoteViews;
import com.jasonmaxfield.dodgeballsb.R;
import java.util.ArrayList;
import java.util.List;

public class SoundPickActivity extends Activity {
    private static final String FILENAMES = "filenames.txt";
    private static final String FILENAME_TITLES = "favorite_titles.txt";
    private static final String PREFS_NAME = "com.jasonmaxfield.dodgeballsb.SoundWidget";
    private static final String PREF_RES_NAME = "res_";
    private static final String PREF_TITLE_KEY = "title_";
    /* access modifiers changed from: private */
    public String[] FavoriteFNames = null;
    /* access modifiers changed from: private */
    public String[] FavoriteTitles = null;
    List<String> favoriteFNamesStrings = new ArrayList();
    List<String> favoriteTitlesStrings = new ArrayList();
    private ListView listView1;
    int mAppWidgetId = 0;
    SharedPreferences prefs;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setResult(0);
        setContentView((int) R.layout.sound_pick_layout);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.mAppWidgetId = extras.getInt("appWidgetId", 0);
        }
        if (this.mAppWidgetId == 0) {
            finish();
        }
        this.favoriteTitlesStrings = readFiles(FILENAME_TITLES, this.favoriteTitlesStrings);
        this.favoriteFNamesStrings = readFiles(FILENAMES, this.favoriteFNamesStrings);
        this.FavoriteFNames = new String[this.favoriteFNamesStrings.size()];
        this.favoriteFNamesStrings.toArray(this.FavoriteFNames);
        this.FavoriteTitles = new String[this.favoriteTitlesStrings.size()];
        this.favoriteTitlesStrings.toArray(this.FavoriteTitles);
        this.listView1 = (ListView) findViewById(R.id.lvSoundPick);
        this.listView1.setAdapter((ListAdapter) new ArrayAdapter(this, 17367043, this.favoriteTitlesStrings));
        this.listView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Context context = SoundPickActivity.this;
                AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
                RemoteViews views = new RemoteViews(context.getPackageName(), (int) R.layout.widget_layout);
                String title = SoundPickActivity.this.FavoriteTitles[position];
                String filename = SoundPickActivity.this.FavoriteFNames[position];
                SoundPickActivity.saveTitlePref(context, SoundPickActivity.this.mAppWidgetId, title);
                SoundPickActivity.saveResId(context, SoundPickActivity.this.mAppWidgetId, filename);
                appWidgetManager.updateAppWidget(SoundPickActivity.this.mAppWidgetId, views);
                SoundWidget.updateAppWidget(context, appWidgetManager, SoundPickActivity.this.mAppWidgetId);
                Intent resultValue = new Intent();
                resultValue.putExtra("appWidgetId", SoundPickActivity.this.mAppWidgetId);
                SoundPickActivity.this.setResult(-1, resultValue);
                SoundPickActivity.this.finish();
            }
        });
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v4, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: java.util.List<java.lang.String>} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<java.lang.String> readFiles(java.lang.String r7, java.util.List<java.lang.String> r8) {
        /*
            r6 = this;
            java.io.File r2 = new java.io.File
            java.io.File r5 = r6.getFilesDir()
            r2.<init>(r5, r7)
            java.io.FileInputStream r3 = r6.openFileInput(r7)     // Catch:{ FileNotFoundException -> 0x001e, IOException -> 0x0024, ClassNotFoundException -> 0x002a }
            java.io.ObjectInputStream r4 = new java.io.ObjectInputStream     // Catch:{ FileNotFoundException -> 0x001e, IOException -> 0x0024, ClassNotFoundException -> 0x002a }
            r4.<init>(r3)     // Catch:{ FileNotFoundException -> 0x001e, IOException -> 0x0024, ClassNotFoundException -> 0x002a }
            java.lang.Object r5 = r4.readObject()     // Catch:{ FileNotFoundException -> 0x001e, IOException -> 0x0024, ClassNotFoundException -> 0x002a }
            r0 = r5
            java.util.List r0 = (java.util.List) r0     // Catch:{ FileNotFoundException -> 0x001e, IOException -> 0x0024, ClassNotFoundException -> 0x002a }
            r8 = r0
            r4.close()     // Catch:{ FileNotFoundException -> 0x001e, IOException -> 0x0024, ClassNotFoundException -> 0x002a }
        L_0x001d:
            return r8
        L_0x001e:
            r5 = move-exception
            r1 = r5
            r1.printStackTrace()
            goto L_0x001d
        L_0x0024:
            r5 = move-exception
            r1 = r5
            r1.printStackTrace()
            goto L_0x001d
        L_0x002a:
            r5 = move-exception
            r1 = r5
            r1.printStackTrace()
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: dodgeballsoundboard.SoundPickActivity.readFiles(java.lang.String, java.util.List):java.util.List");
    }

    static void saveTitlePref(Context context, int appWidgetId, String text) {
        SharedPreferences.Editor prefs2 = context.getSharedPreferences(PREFS_NAME, 0).edit();
        prefs2.putString(PREF_TITLE_KEY + appWidgetId, text);
        prefs2.commit();
    }

    static void saveResId(Context context, int appWidgetId, String filename) {
        SharedPreferences.Editor prefs2 = context.getSharedPreferences(PREFS_NAME, 0).edit();
        prefs2.putString(PREF_RES_NAME + appWidgetId, filename);
        prefs2.commit();
        Log.e("DodgeballSoundboard", "saveRes filename: " + filename);
    }

    static String loadTitlePref(Context context, int appWidgetId) {
        String title = context.getSharedPreferences(PREFS_NAME, 0).getString(PREF_TITLE_KEY + appWidgetId, null);
        if (title != null) {
            return title;
        }
        return "Dodgeball Soundboard";
    }

    static int loadResId(Context context, int appWidgetId) {
        String filename = context.getSharedPreferences(PREFS_NAME, 0).getString(PREF_RES_NAME + appWidgetId, null);
        Log.e("DodgeballSoundboard", "loadRes filename: " + filename);
        if (filename != null) {
            return context.getResources().getIdentifier(filename, "raw", "com.jasonmaxfield.dodgeballsb");
        }
        return 0;
    }
}
