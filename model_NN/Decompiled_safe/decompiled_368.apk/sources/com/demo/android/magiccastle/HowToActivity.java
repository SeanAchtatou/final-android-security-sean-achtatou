package com.demo.android.magiccastle;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

public class HowToActivity extends Activity {
    private boolean a(Context context, String str) {
        return context.getPackageManager().queryIntentActivities(new Intent(str), 65536).size() > 0;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (a(this, "android.service.wallpaper.LIVE_WALLPAPER_CHOOSER")) {
            startActivity(new Intent("android.service.wallpaper.LIVE_WALLPAPER_CHOOSER"));
            Toast.makeText(this, (int) R.string.howto_ok, 1).show();
        } else {
            Toast.makeText(this, (int) R.string.howto_nosupport, 1).show();
        }
        finish();
    }
}
