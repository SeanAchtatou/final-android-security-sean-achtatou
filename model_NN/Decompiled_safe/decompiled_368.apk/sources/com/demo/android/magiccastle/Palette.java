package com.demo.android.magiccastle;

import java.util.ArrayList;

public class Palette {
    static int a = 100;

    /* renamed from: a  reason: collision with other field name */
    static boolean f19a = true;
    static int b = 280;

    /* renamed from: b  reason: collision with other field name */
    static boolean f20b = true;

    /* renamed from: a  reason: collision with other field name */
    private final float f21a = 3.1415925f;

    /* renamed from: a  reason: collision with other field name */
    ArrayList f22a = new ArrayList();

    /* renamed from: b  reason: collision with other field name */
    ArrayList f23b = new ArrayList();
    private int c = 0;

    /* renamed from: c  reason: collision with other field name */
    ArrayList f24c = new ArrayList();
    private int d = 0;

    Palette(a[] aVarArr, b[] bVarArr) {
        for (a add : aVarArr) {
            this.f23b.add(add);
        }
        for (b add2 : bVarArr) {
            this.f24c.add(add2);
        }
        this.c = aVarArr.length;
        this.d = bVarArr.length;
    }

    static double a(double d2, double d3) {
        return (Math.floor(((double) a) * d2) % Math.floor(((double) a) * d3)) / ((double) a);
    }

    static int a(byte b2) {
        return b2 >= 0 ? b2 : 256 - (-b2);
    }

    /* access modifiers changed from: package-private */
    public a a(a aVar, a aVar2, double d2, double d3) {
        a aVar3 = new a();
        if (d3 < 1.0E-6d && d3 > -1.0E-6d) {
            return aVar;
        }
        double d4 = d2 < 0.0d ? 0.0d : d2;
        if (d4 > d3) {
            d4 = d3;
        }
        aVar3.a = (int) Math.floor(((double) aVar.a) + ((((double) (aVar2.a - aVar.a)) * d4) / d3));
        aVar3.b = (int) Math.floor(((double) aVar.b) + ((((double) (aVar2.b - aVar.b)) * d4) / d3));
        aVar3.c = (int) Math.floor(((d4 * ((double) (aVar2.c - aVar.c))) / d3) + ((double) aVar.c));
        return aVar3;
    }

    /* access modifiers changed from: package-private */
    public void a(a aVar, a aVar2) {
        int i = aVar.a;
        aVar.a = aVar2.a;
        aVar2.a = i;
        int i2 = aVar.b;
        aVar.b = aVar2.b;
        aVar2.b = i2;
        int i3 = aVar.c;
        aVar.c = aVar2.c;
        aVar2.c = i3;
    }

    /* access modifiers changed from: package-private */
    public void a(ArrayList arrayList, b bVar) {
        int i = (bVar.d - bVar.c) + 1;
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < i / 2) {
                a aVar = (a) arrayList.get(bVar.d - i3);
                a((a) arrayList.get(bVar.c + i3), aVar);
                arrayList.set(bVar.c + i3, aVar);
                arrayList.set(bVar.d - i3, aVar);
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(ArrayList arrayList, b bVar, double d2) {
        double floor = Math.floor(d2);
        int i = 0;
        while (true) {
            int i2 = i;
            if (((double) i2) < floor) {
                a aVar = (a) arrayList.get(bVar.d);
                int i3 = bVar.d - 1;
                while (true) {
                    int i4 = i3;
                    if (i4 < bVar.c) {
                        break;
                    }
                    arrayList.set(i4 + 1, (a) arrayList.get(i4));
                    i3 = i4 - 1;
                }
                arrayList.set(bVar.c, aVar);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(a[] aVarArr, long j, float f, boolean z) {
        a(aVarArr, this.f22a);
        this.f22a.size();
        if (f20b) {
            double d2 = 0.0d;
            for (int i = 0; i < this.d; i++) {
                b bVar = (b) this.f24c.get(i);
                if (bVar.a != 0) {
                    int i2 = (bVar.d - bVar.c) + 1;
                    int floor = (int) (((double) bVar.a) / Math.floor((double) (((float) b) / f)));
                    if (bVar.b < 3) {
                        d2 = a((double) (j / ((long) (1000 / floor))), (double) i2);
                    } else if (bVar.b == 3) {
                        d2 = a((double) (j / ((long) (1000 / floor))), (double) (i2 * 2));
                        if (d2 >= ((double) i2)) {
                            d2 = ((double) (i2 * 2)) - d2;
                        }
                    } else if (bVar.b < 6) {
                        d2 = Math.sin(((a((double) (j / ((long) (1000 / floor))), (double) i2) * 3.141592502593994d) * 2.0d) / ((double) i2)) + 1.0d;
                        if (bVar.b == 4) {
                            d2 *= (double) (i2 / 4);
                        } else if (bVar.b == 5) {
                            d2 *= (double) (i2 / 2);
                        }
                    }
                    if (bVar.b == 2) {
                        a(this.f22a, bVar);
                    }
                    if (!f19a || !z) {
                        a(this.f22a, bVar, d2);
                    } else {
                        b((a[]) this.f22a.toArray(new a[this.f22a.size()]), bVar, d2);
                    }
                    if (bVar.b == 2) {
                        a(this.f22a, bVar);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(a[] aVarArr, b bVar, double d2) {
        double floor = Math.floor(d2);
        for (int i = 0; ((double) i) < floor; i++) {
            a aVar = aVarArr[bVar.d];
            for (int i2 = bVar.d - 1; i2 >= bVar.c; i2--) {
                aVarArr[i2 + 1] = aVarArr[i2];
            }
            aVarArr[bVar.c] = aVar;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(a[] aVarArr, ArrayList arrayList) {
        if (!arrayList.isEmpty()) {
            arrayList.clear();
        }
        arrayList.size();
        for (int i = 0; i < aVarArr.length; i++) {
            a aVar = new a();
            aVar.a = aVarArr[i].a;
            aVar.b = aVarArr[i].b;
            aVar.c = aVarArr[i].c;
            arrayList.add(aVar);
        }
    }

    /* access modifiers changed from: package-private */
    public a[] a() {
        return (a[]) this.f22a.toArray(new a[this.f22a.size()]);
    }

    /* access modifiers changed from: package-private */
    public void b(a[] aVarArr, b bVar, double d2) {
        a(aVarArr, bVar, d2);
        double floor = Math.floor(d2 - Math.floor(d2));
        a aVar = aVarArr[bVar.d];
        int i = bVar.d - 1;
        while (true) {
            int i2 = i;
            if (i2 >= bVar.c) {
                aVarArr[i2 + 1] = a(aVarArr[i2 + 1], aVarArr[i2], floor, (double) a);
                i = i2 - 1;
            } else {
                aVarArr[bVar.c] = a(aVarArr[bVar.c], aVar, floor, (double) a);
                return;
            }
        }
    }
}
