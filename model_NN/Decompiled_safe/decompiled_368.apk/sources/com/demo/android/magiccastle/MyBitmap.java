package com.demo.android.magiccastle;

import java.util.ArrayList;

public class MyBitmap {
    static int a = 640;

    /* renamed from: a  reason: collision with other field name */
    static int[] f15a = new int[(a * b)];
    static int b = 480;

    /* renamed from: a  reason: collision with other field name */
    Palette f16a;

    /* renamed from: a  reason: collision with other field name */
    ArrayList f17a = new ArrayList();

    /* renamed from: a  reason: collision with other field name */
    byte[] f18a;

    MyBitmap(int[] iArr, int[] iArr2) {
        a(iArr, iArr2);
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:27:0x006b */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:23:0x0033 */
    /* JADX WARN: Type inference failed for: r7v0, types: [byte[]] */
    /* JADX WARN: Type inference failed for: r7v1, types: [byte, int] */
    /* JADX WARN: Type inference failed for: r7v2 */
    /* JADX WARN: Type inference failed for: r4v6, types: [byte[]] */
    /* JADX WARN: Type inference failed for: r4v7, types: [byte, int] */
    /* JADX WARN: Type inference failed for: r4v8 */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Incorrect type for immutable var: ssa=byte, code=int, for r4v7, types: [byte, int] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=byte, code=int, for r7v1, types: [byte, int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.Bitmap a(boolean r13) {
        /*
            r12 = this;
            r11 = 256(0x100, float:3.59E-43)
            r10 = 0
            com.demo.android.magiccastle.Palette r0 = r12.f16a
            com.demo.android.magiccastle.a[] r1 = r0.a()
            if (r13 == 0) goto L_0x0051
            java.util.ArrayList r0 = r12.f17a
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x0051
            byte[] r0 = r12.f18a
            java.util.ArrayList r0 = r12.f17a
            int r2 = r0.size()
            r3 = r10
        L_0x001c:
            if (r3 >= r2) goto L_0x0044
            java.util.ArrayList r0 = r12.f17a
            java.lang.Object r0 = r0.get(r3)
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            byte[] r4 = r12.f18a
            byte r4 = r4[r0]
            if (r4 >= 0) goto L_0x0033
            int r4 = -r4
            int r4 = r11 - r4
        L_0x0033:
            r4 = r1[r4]
            r4.a()
            int[] r5 = com.demo.android.magiccastle.MyBitmap.f15a
            int r4 = r4.a()
            r5[r0] = r4
            int r0 = r3 + 1
            r3 = r0
            goto L_0x001c
        L_0x0044:
            int[] r0 = com.demo.android.magiccastle.MyBitmap.f15a
            int r1 = com.demo.android.magiccastle.MyBitmap.a
            int r2 = com.demo.android.magiccastle.MyBitmap.b
            android.graphics.Bitmap$Config r3 = android.graphics.Bitmap.Config.ARGB_8888
            android.graphics.Bitmap r0 = android.graphics.Bitmap.createBitmap(r0, r1, r2, r3)
        L_0x0050:
            return r0
        L_0x0051:
            int r0 = com.demo.android.magiccastle.MyBitmap.a
            int r2 = com.demo.android.magiccastle.MyBitmap.b
            byte[] r3 = r12.f18a
            int r3 = r3.length
            r3 = r10
            r4 = r10
            r5 = r10
        L_0x005b:
            if (r3 >= r2) goto L_0x0083
            r6 = r5
            r5 = r4
            r4 = r10
        L_0x0060:
            if (r4 >= r0) goto L_0x007e
            byte[] r7 = r12.f18a
            byte r7 = r7[r6]
            if (r7 >= 0) goto L_0x006b
            int r7 = -r7
            int r7 = r11 - r7
        L_0x006b:
            r7 = r1[r7]
            int[] r8 = com.demo.android.magiccastle.MyBitmap.f15a
            int r9 = r5 + 1
            int r7 = r7.a()
            r8[r5] = r7
            int r5 = r6 + 1
            int r4 = r4 + 1
            r6 = r5
            r5 = r9
            goto L_0x0060
        L_0x007e:
            int r3 = r3 + 1
            r4 = r5
            r5 = r6
            goto L_0x005b
        L_0x0083:
            int[] r0 = com.demo.android.magiccastle.MyBitmap.f15a
            int r1 = com.demo.android.magiccastle.MyBitmap.a
            int r2 = com.demo.android.magiccastle.MyBitmap.b
            android.graphics.Bitmap$Config r3 = android.graphics.Bitmap.Config.ARGB_8888
            android.graphics.Bitmap r0 = android.graphics.Bitmap.createBitmap(r0, r1, r2, r3)
            r1 = 1
            com.demo.android.magiccastle.MagicCastleService.f2b = r1
            goto L_0x0050
        */
        throw new UnsupportedOperationException("Method not decompiled: com.demo.android.magiccastle.MyBitmap.a(boolean):android.graphics.Bitmap");
    }

    /* access modifiers changed from: package-private */
    public void a() {
        int[] iArr = new int[256];
        int size = this.f16a.f24c.size();
        for (int i = 0; i < size; i++) {
            b bVar = (b) this.f16a.f24c.get(i);
            if (bVar.a != 0) {
                for (int i2 = bVar.c; i2 <= bVar.d; i2++) {
                    iArr[i2] = 1;
                }
            }
        }
        byte[] bArr = this.f18a;
        int i3 = a;
        int i4 = b;
        if (!this.f17a.isEmpty()) {
            this.f17a.clear();
        }
        int i5 = 0;
        int i6 = 0;
        while (i5 < i4) {
            int i7 = i6;
            for (int i8 = 0; i8 < i3; i8++) {
                if (iArr[Palette.a(bArr[i7])] != 0) {
                    this.f17a.add(Integer.valueOf(i7));
                }
                i7++;
            }
            i5++;
            i6 = i7;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int[] iArr, int[] iArr2) {
        int length = iArr.length / 3;
        a[] aVarArr = new a[length];
        for (int i = 0; i < length; i++) {
            aVarArr[i] = new a();
            aVarArr[i].a = iArr[i * 3];
            aVarArr[i].b = iArr[(i * 3) + 1];
            aVarArr[i].c = iArr[(i * 3) + 2];
        }
        int length2 = iArr2.length / 4;
        b[] bVarArr = new b[length2];
        for (int i2 = 0; i2 < length2; i2++) {
            bVarArr[i2] = new b();
            bVarArr[i2].b = iArr2[i2 * 4];
            bVarArr[i2].a = iArr2[(i2 * 4) + 1];
            bVarArr[i2].c = iArr2[(i2 * 4) + 2];
            bVarArr[i2].d = iArr2[(i2 * 4) + 3];
        }
        this.f16a = new Palette(aVarArr, bVarArr);
        this.f18a = MagicCastleService.f1a;
    }
}
