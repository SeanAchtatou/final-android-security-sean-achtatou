package com.demo.android.magiccastle;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

public class MagicCastleActivity extends PreferenceActivity {
    private LinearLayout a = null;

    /* renamed from: a  reason: collision with other field name */
    private AdView f0a = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.a = new LinearLayout(this);
        this.a.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.a.setOrientation(1);
        this.f0a = new AdView(this, AdSize.BANNER, "a14e4b6af018c91");
        this.f0a.loadAd(new AdRequest());
        this.a.addView(this.f0a);
        ListView listView = new ListView(this);
        listView.setId(16908298);
        listView.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        listView.setDrawSelectorOnTop(false);
        this.a.addView(listView);
        setContentView(this.a);
        addPreferencesFromResource(R.xml.setting);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.f0a != null) {
            this.f0a.destroy();
        }
    }

    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        String key = preference.getKey();
        if ("more".equals(key)) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:\"A.O.I Studio\"")));
        } else if ("rec1".equals(key)) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.demo.android.waterfallwallpaper")));
        } else if ("rec2".equals(key)) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.reactor.livewallpaper.easter")));
        } else if ("rec3".equals(key)) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.reactor.livewallpaper.fallingflower")));
        }
        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }
}
