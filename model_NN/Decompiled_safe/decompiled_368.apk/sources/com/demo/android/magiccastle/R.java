package com.demo.android.magiccastle;

public final class R {

    public final class array {
        public static final int scene_list = 2131034112;
        public static final int scene_list_value = 2131034113;
    }

    public final class attr {
    }

    public final class drawable {
        public static final int icon = 2130837504;
        public static final int more = 2130837505;
        public static final int rec_icon1 = 2130837506;
        public static final int rec_icon2 = 2130837507;
        public static final int rec_icon3 = 2130837508;
    }

    public final class id {
        public static final int pref_img = 2131230720;
        public static final int pref_title = 2131230721;
    }

    public final class layout {
        public static final int preference_widget_image = 2130903040;
    }

    public final class string {
        public static final int Daylight_Saving_Time = 2131099664;
        public static final int Daylight_Saving_Time_Off = 2131099666;
        public static final int Daylight_Saving_Time_On = 2131099665;
        public static final int Screen_Adaptation = 2131099661;
        public static final int Screen_Adaptation_By_Image = 2131099663;
        public static final int Screen_Adaptation_By_Screen = 2131099662;
        public static final int app_name = 2131099648;
        public static final int howto_nosupport = 2131099673;
        public static final int howto_ok = 2131099674;
        public static final int more = 2131099672;
        public static final int rec_name1 = 2131099669;
        public static final int rec_name2 = 2131099670;
        public static final int rec_name3 = 2131099671;
        public static final int recommand_preference = 2131099668;
        public static final int sakura_setting = 2131099667;
        public static final int scene_afternoon = 2131099655;
        public static final int scene_changebytime = 2131099659;
        public static final int scene_dlg_title = 2131099653;
        public static final int scene_dusk = 2131099656;
        public static final int scene_morning = 2131099654;
        public static final int scene_night = 2131099657;
        public static final int scene_rain = 2131099658;
        public static final int scene_random = 2131099660;
        public static final int scene_summary = 2131099652;
        public static final int scene_title = 2131099651;
        public static final int wallpaper_author = 2131099649;
        public static final int wallpaper_desc = 2131099650;
    }

    public final class style {
        public static final int Theme_ScreenSaver = 2131165184;
    }

    public final class xml {
        public static final int livewallpaper = 2130968576;
        public static final int setting = 2130968577;
    }
}
