package com.demo.android.magiccastle;

import android.graphics.Matrix;
import android.service.wallpaper.WallpaperService;
import android.util.DisplayMetrics;

public class MagicCastleService extends WallpaperService {
    static long a = 14133;

    /* renamed from: a  reason: collision with other field name */
    static byte[] f1a = null;
    static long b = 3187;

    /* renamed from: b  reason: collision with other field name */
    static boolean f2b = false;

    /* renamed from: a  reason: collision with other field name */
    float f3a = 4.0f;

    /* renamed from: a  reason: collision with other field name */
    int f4a = -1;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with other field name */
    public Matrix f5a = new Matrix();

    /* renamed from: a  reason: collision with other field name */
    MyBitmap f6a;

    /* renamed from: a  reason: collision with other field name */
    boolean f7a = false;

    /* renamed from: b  reason: collision with other field name */
    float f8b = 0.0f;

    /* renamed from: b  reason: collision with other field name */
    int f9b = 0;
    float c = 0.0f;

    /* renamed from: c  reason: collision with other field name */
    private int f10c = 480;

    /* renamed from: c  reason: collision with other field name */
    private final long f11c = 900000;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with other field name */
    public boolean f12c = false;
    private int d = 854;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with other field name */
    public long f13d = 0;
    /* access modifiers changed from: private */
    public int e = 3;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with other field name */
    public long f14e = 0;
    /* access modifiers changed from: private */
    public int f = 1;
    /* access modifiers changed from: private */
    public int g = 0;
    private final int h = 4;

    static /* synthetic */ int f(MagicCastleService magicCastleService) {
        int i = magicCastleService.g + 1;
        magicCastleService.g = i;
        return i;
    }

    public WallpaperService.Engine onCreateEngine() {
        new DisplayMetrics();
        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        this.f10c = displayMetrics.widthPixels;
        this.d = displayMetrics.heightPixels;
        f2b = false;
        this.f12c = false;
        return new c(this);
    }
}
