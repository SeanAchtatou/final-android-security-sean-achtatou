package com.demo.android.magiccastle;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.service.wallpaper.WallpaperService;
import android.util.DisplayMetrics;
import android.view.SurfaceHolder;
import android.view.ViewGroup;
import android.view.WindowManager;
import com.admob.android.ads.AdListener;
import com.admob.android.ads.AdView;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Random;

class c extends WallpaperService.Engine implements AdListener {
    float a = 1.0f;

    /* renamed from: a  reason: collision with other field name */
    int f25a = 0;

    /* renamed from: a  reason: collision with other field name */
    private long f26a = 80;

    /* renamed from: a  reason: collision with other field name */
    private Paint f27a = new Paint();

    /* renamed from: a  reason: collision with other field name */
    private final Handler f28a = new Handler();

    /* renamed from: a  reason: collision with other field name */
    private WindowManager.LayoutParams f29a = null;

    /* renamed from: a  reason: collision with other field name */
    private WindowManager f30a = null;

    /* renamed from: a  reason: collision with other field name */
    private AdView f31a = null;

    /* renamed from: a  reason: collision with other field name */
    final /* synthetic */ MagicCastleService f32a;

    /* renamed from: a  reason: collision with other field name */
    private final Runnable f33a = new d(this);
    float b = 1.0f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    c(MagicCastleService magicCastleService) {
        super(magicCastleService);
        this.f32a = magicCastleService;
    }

    private void a() {
        this.a = ((float) MagicCastleService.a(this.f32a)) / ((float) MyBitmap.a);
        this.b = ((float) MagicCastleService.b(this.f32a)) / ((float) MyBitmap.b);
        if (this.a > this.b) {
            MagicCastleService.a(this.f32a).postScale(this.a, this.a);
        } else {
            MagicCastleService.a(this.f32a).postScale(this.b, this.b);
        }
    }

    private void a(String str) {
        try {
            InputStream open = this.f32a.getAssets().open(str);
            int available = open.available();
            MagicCastleService.f1a = new byte[available];
            open.read(MagicCastleService.f1a, 0, available);
            open.close();
        } catch (Exception e) {
        }
    }

    /* JADX WARN: Type inference failed for: r3v1, types: [byte[]] */
    /* JADX WARN: Type inference failed for: r6v13, types: [byte] */
    /* JADX WARN: Type inference failed for: r6v14, types: [byte] */
    /* JADX WARN: Type inference failed for: r6v15, types: [byte, int] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(java.lang.String r11, java.lang.String r12) {
        /*
            r10 = this;
            r9 = 0
            r8 = 256(0x100, float:3.59E-43)
            r7 = 0
            com.demo.android.magiccastle.MagicCastleService r0 = r10.f32a     // Catch:{ Exception -> 0x008c }
            android.content.res.AssetManager r0 = r0.getAssets()     // Catch:{ Exception -> 0x008c }
            java.io.InputStream r1 = r0.open(r11)     // Catch:{ Exception -> 0x008c }
            int r2 = r1.available()     // Catch:{ Exception -> 0x008c }
            byte[] r3 = new byte[r2]     // Catch:{ Exception -> 0x008c }
            int[] r4 = new int[r2]     // Catch:{ Exception -> 0x008c }
            r5 = 0
            r1.read(r3, r5, r2)     // Catch:{ Exception -> 0x0030 }
            r5 = r7
        L_0x001b:
            if (r5 >= r2) goto L_0x003d
            byte r6 = r3[r5]     // Catch:{ Exception -> 0x0030 }
            if (r6 >= 0) goto L_0x002b
            byte r6 = r3[r5]     // Catch:{ Exception -> 0x0030 }
            int r6 = -r6
            int r6 = r8 - r6
            r4[r5] = r6     // Catch:{ Exception -> 0x0030 }
        L_0x0028:
            int r5 = r5 + 1
            goto L_0x001b
        L_0x002b:
            byte r6 = r3[r5]     // Catch:{ Exception -> 0x0030 }
            r4[r5] = r6     // Catch:{ Exception -> 0x0030 }
            goto L_0x0028
        L_0x0030:
            r0 = move-exception
            r0 = r9
            r1 = r4
        L_0x0033:
            com.demo.android.magiccastle.MagicCastleService r2 = r10.f32a
            com.demo.android.magiccastle.MyBitmap r3 = new com.demo.android.magiccastle.MyBitmap
            r3.<init>(r1, r0)
            r2.f6a = r3
            return
        L_0x003d:
            r1.close()     // Catch:{ Exception -> 0x0030 }
            java.io.InputStream r0 = r0.open(r12)     // Catch:{ Exception -> 0x0030 }
            int r1 = r0.available()     // Catch:{ Exception -> 0x0030 }
            byte[] r2 = new byte[r1]     // Catch:{ Exception -> 0x0030 }
            int r3 = r1 / 2
            int[] r3 = new int[r3]     // Catch:{ Exception -> 0x0030 }
            r5 = 0
            r0.read(r2, r5, r1)     // Catch:{ Exception -> 0x0082 }
            r5 = r7
        L_0x0053:
            int r6 = r1 / 2
            if (r5 >= r6) goto L_0x0086
            int r6 = r5 * 2
            byte r6 = r2[r6]     // Catch:{ Exception -> 0x0082 }
            if (r6 >= 0) goto L_0x0072
            int r6 = r5 * 2
            int r6 = r6 + 1
            byte r6 = r2[r6]     // Catch:{ Exception -> 0x0082 }
            int r6 = r6 << 8
            int r7 = r5 * 2
            byte r7 = r2[r7]     // Catch:{ Exception -> 0x0082 }
            int r7 = -r7
            int r7 = r8 - r7
            int r6 = r6 + r7
            r3[r5] = r6     // Catch:{ Exception -> 0x0082 }
        L_0x006f:
            int r5 = r5 + 1
            goto L_0x0053
        L_0x0072:
            int r6 = r5 * 2
            int r6 = r6 + 1
            byte r6 = r2[r6]     // Catch:{ Exception -> 0x0082 }
            int r6 = r6 << 8
            int r7 = r5 * 2
            byte r7 = r2[r7]     // Catch:{ Exception -> 0x0082 }
            int r6 = r6 + r7
            r3[r5] = r6     // Catch:{ Exception -> 0x0082 }
            goto L_0x006f
        L_0x0082:
            r0 = move-exception
            r0 = r3
            r1 = r4
            goto L_0x0033
        L_0x0086:
            r0.close()     // Catch:{ Exception -> 0x0082 }
            r0 = r3
            r1 = r4
            goto L_0x0033
        L_0x008c:
            r0 = move-exception
            r0 = r9
            r1 = r9
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: com.demo.android.magiccastle.c.a(java.lang.String, java.lang.String):void");
    }

    private void b() {
        d();
        this.f32a.f6a.a();
    }

    private void c() {
        SharedPreferences sharedPreferences = this.f32a.getSharedPreferences("com.demo.android.magiccastle_preferences", 0);
        String string = sharedPreferences.getString("scene", "morning");
        boolean z = sharedPreferences.getBoolean("Daylight_Saving_Time_Set", false);
        if (isPreview()) {
            if (this.f32a.g == 4) {
                if (this.f32a.e == 1) {
                    int unused = this.f32a.e = 2;
                } else if (this.f32a.e == 2) {
                    int unused2 = this.f32a.e = 3;
                } else if (this.f32a.e == 3) {
                    int unused3 = this.f32a.e = 4;
                } else if (this.f32a.e == 4) {
                    int unused4 = this.f32a.e = 5;
                } else {
                    int unused5 = this.f32a.e = 1;
                }
            }
            int unused6 = this.f32a.g = 0;
        } else if (string.compareTo("morning") == 0) {
            int unused7 = this.f32a.e = 1;
        } else if (string.compareTo("afternoon") == 0) {
            int unused8 = this.f32a.e = 2;
        } else if (string.compareTo("dusk") == 0) {
            int unused9 = this.f32a.e = 3;
        } else if (string.compareTo("night") == 0) {
            int unused10 = this.f32a.e = 4;
        } else if (string.compareTo("rain") == 0) {
            int unused11 = this.f32a.e = 5;
        } else if (string.compareTo("changebytime") == 0) {
            int i = Calendar.getInstance().get(11);
            if (z) {
                if (i >= 6 && i <= 11) {
                    int unused12 = this.f32a.e = 1;
                } else if (i >= 12 && i <= 16) {
                    int unused13 = this.f32a.e = 2;
                } else if (i < 17 || i > 19) {
                    int unused14 = this.f32a.e = 4;
                } else {
                    int unused15 = this.f32a.e = 3;
                }
            } else if (i >= 7 && i <= 11) {
                int unused16 = this.f32a.e = 1;
            } else if (i >= 12 && i <= 15) {
                int unused17 = this.f32a.e = 2;
            } else if (i < 16 || i > 18) {
                int unused18 = this.f32a.e = 4;
            } else {
                int unused19 = this.f32a.e = 3;
            }
        } else if (string.compareTo("random") == 0) {
            int i2 = Calendar.getInstance().get(11);
            if (z) {
                if (i2 >= 6 && i2 <= 11) {
                    int unused20 = this.f32a.e = new Random().nextInt(2) == 0 ? 1 : 5;
                } else if (i2 >= 12 && i2 <= 16) {
                    int unused21 = this.f32a.e = new Random().nextInt(2) == 0 ? 2 : 5;
                } else if (i2 < 17 || i2 > 19) {
                    int unused22 = this.f32a.e = 4;
                } else {
                    int unused23 = this.f32a.e = 3;
                }
            } else if (i2 >= 7 && i2 <= 11) {
                int unused24 = this.f32a.e = new Random().nextInt(2) == 0 ? 1 : 5;
            } else if (i2 >= 12 && i2 <= 15) {
                int unused25 = this.f32a.e = new Random().nextInt(2) == 0 ? 2 : 5;
            } else if (i2 < 16 || i2 > 18) {
                int unused26 = this.f32a.e = 4;
            } else {
                int unused27 = this.f32a.e = 3;
            }
        }
        if (this.f32a.e != this.f32a.f) {
            int unused28 = this.f32a.f = this.f32a.e;
            MagicCastleService.f2b = false;
            b();
        }
    }

    private void d() {
        switch (this.f32a.e) {
            case 1:
                a("SwampCathedralDayPixel.dat");
                a("SwampCathedralDayColor.dat", "SwampCathedralDayCycle.dat");
                return;
            case 2:
                a("DesertHeatWaveDayPixel.dat");
                a("DesertHeatWaveDayColor.dat", "DesertHeatWaveDayCycle.dat");
                return;
            case 3:
                a("MountainFortressDuskPixel.dat");
                a("MountainFortressDuskColor.dat", "MountainFortressDuskCycle.dat");
                return;
            case 4:
                a("HarborTownNightPixel.dat");
                a("HarborTownNightColor.dat", "HarborTownNightCycle.dat");
                return;
            case 5:
                a("HauntedCastleRuinsRainPixel.dat");
                a("HauntedCastleRuinsRainColor.dat", "HauntedCastleRuinsRainCycle.dat");
                return;
            default:
                a("NightPixel.dat");
                a("NightColor.dat", "NightCycle.dat");
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.demo.android.magiccastle.MagicCastleService.b(com.demo.android.magiccastle.MagicCastleService, long):long
     arg types: [com.demo.android.magiccastle.MagicCastleService, int]
     candidates:
      com.demo.android.magiccastle.MagicCastleService.b(com.demo.android.magiccastle.MagicCastleService, int):int
      com.demo.android.magiccastle.MagicCastleService.b(com.demo.android.magiccastle.MagicCastleService, long):long */
    /* access modifiers changed from: private */
    public void e() {
        SurfaceHolder surfaceHolder = getSurfaceHolder();
        Canvas lockCanvas = surfaceHolder.lockCanvas();
        if (lockCanvas != null) {
            try {
                lockCanvas.drawColor(-16777216);
                this.f27a.setColor(-16777216);
                this.f32a.f6a.f16a.a((a[]) this.f32a.f6a.f16a.f23b.toArray(new a[this.f32a.f6a.f16a.f23b.size()]), MagicCastleService.a, this.f32a.f3a, this.f32a.f7a);
                MagicCastleService.a += MagicCastleService.b;
                Bitmap a2 = this.f32a.f6a.a(MagicCastleService.f2b && this.f32a.c == this.f32a.f8b && this.f32a.f9b == this.f32a.f4a);
                this.f32a.c = this.f32a.f8b;
                this.f32a.f9b = this.f32a.f4a;
                if (isPreview()) {
                    lockCanvas.drawBitmap(a2, new Rect(0, 0, a2.getWidth(), a2.getHeight()), new Rect(0, 0, MagicCastleService.a(this.f32a), MagicCastleService.b(this.f32a)), this.f27a);
                    MagicCastleService.f(this.f32a);
                    if (this.f32a.g == 4) {
                        c();
                    }
                    if (this.f32a.g >= 5) {
                        int unused = this.f32a.g = 0;
                    }
                } else if (MagicCastleService.a(this.f32a)) {
                    lockCanvas.drawBitmap(a2, new Rect(0, 0, a2.getWidth(), a2.getHeight()), new Rect(0, 0, MagicCastleService.a(this.f32a), MagicCastleService.b(this.f32a)), this.f27a);
                } else {
                    lockCanvas.drawBitmap(a2, MagicCastleService.a(this.f32a), this.f27a);
                    long currentTimeMillis = System.currentTimeMillis();
                    if (MagicCastleService.a(this.f32a) == 0) {
                        long unused2 = this.f32a.f14e = currentTimeMillis;
                    } else {
                        long unused3 = this.f32a.f13d = currentTimeMillis - MagicCastleService.a(this.f32a);
                        if (MagicCastleService.b(this.f32a) < 0) {
                            long unused4 = this.f32a.f13d = 0L;
                            long unused5 = this.f32a.f14e = currentTimeMillis;
                        }
                        if (MagicCastleService.b(this.f32a) >= 900000) {
                            c();
                            long unused6 = this.f32a.f13d = 0L;
                            long unused7 = this.f32a.f14e = currentTimeMillis;
                        }
                    }
                }
                if (lockCanvas != null) {
                    surfaceHolder.unlockCanvasAndPost(lockCanvas);
                }
            } catch (Exception e) {
                if (lockCanvas != null) {
                    surfaceHolder.unlockCanvasAndPost(lockCanvas);
                }
            } catch (Throwable th) {
                if (lockCanvas != null) {
                    surfaceHolder.unlockCanvasAndPost(lockCanvas);
                }
                throw th;
            }
        }
        this.f28a.removeCallbacks(this.f33a);
        this.f28a.postDelayed(this.f33a, this.f26a);
    }

    public void onCreate(SurfaceHolder surfaceHolder) {
        super.onCreate(surfaceHolder);
        c();
        int unused = this.f32a.f = this.f32a.e;
        a();
        b();
        if (isPreview()) {
            this.f30a = (WindowManager) this.f32a.getSystemService("window");
            this.f29a = new WindowManager.LayoutParams(-1, -2);
            this.f29a.alpha = 0.0f;
            this.f29a.flags = 8;
            this.f29a.flags |= 262144;
            this.f29a.flags |= 512;
            this.f29a.type = 2003;
            DisplayMetrics displayMetrics = new DisplayMetrics();
            this.f30a.getDefaultDisplay().getMetrics(displayMetrics);
            this.f29a.height = (int) (displayMetrics.density * 50.0f);
            this.f29a.gravity = 48;
            this.f29a.format = -1;
            this.f29a.token = null;
            this.f29a.x = 0;
            this.f29a.y = 0;
            this.f31a = new AdView(this.f32a, null);
            this.f31a.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
            this.f31a.setBackgroundColor(-16777216);
            this.f31a.setPrimaryTextColor(-1);
            this.f31a.setSecondaryTextColor(-3355444);
            this.f31a.setKeywords("Android app game wallpaper magic castle");
            this.f31a.setRequestInterval(20);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        this.f28a.removeCallbacks(this.f33a);
        if (this.f31a != null) {
            this.f31a.setRequestInterval(0);
            this.f31a.setAdListener(null);
            this.f31a.cleanup();
            this.f31a = null;
            if (this.f30a != null) {
                try {
                    this.f30a.removeView(this.f31a);
                    this.f30a = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void onFailedToReceiveAd(AdView adView) {
    }

    public void onFailedToReceiveRefreshedAd(AdView adView) {
    }

    public void onOffsetsChanged(float f, float f2, float f3, float f4, int i, int i2) {
        super.onOffsetsChanged(f, f2, f3, f4, i, i2);
        Matrix unused = this.f32a.f5a = new Matrix();
        if (isPreview()) {
            MagicCastleService.a(this.f32a).postScale(this.a, this.b, 0.0f, 0.0f);
            return;
        }
        this.f25a = (int) (((((float) MyBitmap.a) * this.b) - ((float) MagicCastleService.a(this.f32a))) * f);
        if (this.b == 1.0f) {
            MagicCastleService.a(this.f32a).postTranslate((float) (-this.f25a), 0.0f);
        } else {
            MagicCastleService.a(this.f32a).postScale(this.b, this.b, (float) this.f25a, 0.0f);
        }
    }

    public void onReceiveAd(AdView adView) {
        if (isPreview()) {
            if (this.f29a != null) {
                this.f29a.alpha = 0.5f;
            }
            if (this.f30a != null && this.f31a != null) {
                try {
                    this.f30a.removeView(this.f31a);
                } catch (Exception e) {
                } finally {
                    this.f30a.addView(this.f31a, this.f29a);
                }
            }
        }
    }

    public void onReceiveRefreshedAd(AdView adView) {
        if (isPreview()) {
            if (this.f29a != null) {
                this.f29a.alpha = 0.5f;
            }
            if (this.f30a != null && this.f31a != null) {
                try {
                    this.f30a.removeView(this.f31a);
                } catch (Exception e) {
                } finally {
                    this.f30a.addView(this.f31a, this.f29a);
                }
            }
        }
    }

    public void onSurfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        super.onSurfaceChanged(surfaceHolder, i, i2, i3);
    }

    public void onSurfaceCreated(SurfaceHolder surfaceHolder) {
        super.onSurfaceCreated(surfaceHolder);
    }

    public void onSurfaceDestroyed(SurfaceHolder surfaceHolder) {
        super.onSurfaceDestroyed(surfaceHolder);
    }

    public void onVisibilityChanged(boolean z) {
        super.onVisibilityChanged(z);
        c();
        boolean unused = this.f32a.f12c = this.f32a.getSharedPreferences("com.demo.android.magiccastle_preferences", 0).getBoolean("Screen_Adaptation_Set", false);
        if (z) {
            e();
            if (isPreview() && this.f30a != null && this.f31a != null) {
                this.f31a.setAdListener(this);
                this.f31a.requestFreshAd();
                try {
                    this.f30a.removeView(this.f31a);
                } catch (Exception e) {
                } finally {
                    this.f30a.addView(this.f31a, this.f29a);
                }
            }
        } else {
            this.f28a.removeCallbacks(this.f33a);
            if (isPreview() && this.f30a != null && this.f31a != null) {
                try {
                    this.f31a.setAdListener(null);
                    this.f30a.removeView(this.f31a);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
    }
}
