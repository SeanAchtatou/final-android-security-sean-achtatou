package com.demo.android.magiccastle;

class a {
    int a = 0;
    int b = 0;
    int c = 0;

    a() {
        a(0, 0, 0);
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return -16777216 | (this.a << 16) | (this.b << 8) | this.c;
    }

    /* access modifiers changed from: package-private */
    public void a(int i, int i2, int i3) {
        this.a = i;
        this.b = i2;
        this.c = i3;
    }
}
