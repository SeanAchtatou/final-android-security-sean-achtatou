package com.admob.android.ads;

public interface InterstitialAdListener {
    void onFailedToReceiveInterstitial(InterstitialAd interstitialAd);

    void onReceiveInterstitial(InterstitialAd interstitialAd);
}
