package com.loveg.HYDigimonI;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class HYDigimonI extends Activity {
    protected static InputStream is;
    private AdView adView;
    /* access modifiers changed from: private */
    public Integer[] mImageIds = {Integer.valueOf((int) R.drawable.img1), Integer.valueOf((int) R.drawable.img2), Integer.valueOf((int) R.drawable.img3), Integer.valueOf((int) R.drawable.img4), Integer.valueOf((int) R.drawable.img5), Integer.valueOf((int) R.drawable.img6), Integer.valueOf((int) R.drawable.img7), Integer.valueOf((int) R.drawable.img8), Integer.valueOf((int) R.drawable.img9), Integer.valueOf((int) R.drawable.img10), Integer.valueOf((int) R.drawable.img11), Integer.valueOf((int) R.drawable.img12), Integer.valueOf((int) R.drawable.img13), Integer.valueOf((int) R.drawable.img14), Integer.valueOf((int) R.drawable.img15), Integer.valueOf((int) R.drawable.img16), Integer.valueOf((int) R.drawable.img17), Integer.valueOf((int) R.drawable.img18), Integer.valueOf((int) R.drawable.img19), Integer.valueOf((int) R.drawable.img20), Integer.valueOf((int) R.drawable.img21), Integer.valueOf((int) R.drawable.img22), Integer.valueOf((int) R.drawable.img23), Integer.valueOf((int) R.drawable.img24), Integer.valueOf((int) R.drawable.img25), Integer.valueOf((int) R.drawable.img26), Integer.valueOf((int) R.drawable.img27), Integer.valueOf((int) R.drawable.img28), Integer.valueOf((int) R.drawable.img29), Integer.valueOf((int) R.drawable.img30), Integer.valueOf((int) R.drawable.img31)};
    /* access modifiers changed from: private */
    public String sCountImg = "31";
    /* access modifiers changed from: private */
    public String sCurrentImg = "1";
    private final String sPref = "img";
    private final String sSuffix = ".jpg";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        new Bundle();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.getString("COUNTIMG") != null) {
                this.sCountImg = bundle.getString("COUNTIMG");
            }
            if (bundle.getString("CURRENTIMG") != null) {
                this.sCurrentImg = bundle.getString("CURRENTIMG");
            }
        }
        ImageButton pributton = (ImageButton) findViewById(R.id.ImageButtonPri);
        ImageButton nextbutton = (ImageButton) findViewById(R.id.ImageButtonNext);
        ImageButton savebutton = (ImageButton) findViewById(R.id.ImageButtonSave);
        ImageButton wallpapersbutton = (ImageButton) findViewById(R.id.ImageButtonWallpapers);
        nextbutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(HYDigimonI.this, HYDigimonI.class);
                Bundle bundle = new Bundle();
                bundle.putString("COUNTIMG", HYDigimonI.this.sCountImg);
                bundle.putString("CURRENTIMG", new StringBuilder(String.valueOf(Integer.valueOf(HYDigimonI.this.sCurrentImg).intValue() + 1)).toString());
                intent.putExtras(bundle);
                HYDigimonI.this.startActivity(intent);
                HYDigimonI.this.finish();
            }
        });
        pributton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(HYDigimonI.this, HYDigimonI.class);
                Bundle bundle = new Bundle();
                bundle.putString("COUNTIMG", HYDigimonI.this.sCountImg);
                bundle.putString("CURRENTIMG", new StringBuilder(String.valueOf(Integer.valueOf(HYDigimonI.this.sCurrentImg).intValue() - 1)).toString());
                intent.putExtras(bundle);
                HYDigimonI.this.startActivity(intent);
                HYDigimonI.this.finish();
            }
        });
        savebutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                IOException e;
                try {
                    FileOutputStream fos = new FileOutputStream("/sdcard/love_" + System.currentTimeMillis() + ".jpg");
                    try {
                        HYDigimonI.is = HYDigimonI.this.getBaseContext().getResources().openRawResource(HYDigimonI.this.mImageIds[Integer.valueOf(HYDigimonI.this.sCurrentImg).intValue() - 1].intValue());
                        byte[] buffer = new byte[1024];
                        while (HYDigimonI.is.read(buffer, 0, buffer.length) >= 0) {
                            fos.write(buffer, 0, buffer.length);
                        }
                        fos.close();
                        HYDigimonI.this.showDialog("Save successful.");
                    } catch (IOException e2) {
                        e = e2;
                        e.printStackTrace();
                    } catch (Throwable th) {
                        fos.close();
                        throw th;
                    }
                } catch (IOException e3) {
                    e = e3;
                    e.printStackTrace();
                }
            }
        });
        wallpapersbutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    HYDigimonI.is = HYDigimonI.this.getBaseContext().getResources().openRawResource(HYDigimonI.this.mImageIds[Integer.valueOf(HYDigimonI.this.sCurrentImg).intValue() - 1].intValue());
                    HYDigimonI.this.setWallpaper(HYDigimonI.is);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                HYDigimonI.this.showDialog("Setting WallPaper successful.");
            }
        });
        wallpapersbutton.setImageResource(R.drawable.setting);
        wallpapersbutton.getBackground().setAlpha(0);
        savebutton.setImageResource(R.drawable.save);
        savebutton.getBackground().setAlpha(0);
        pributton.setImageResource(R.drawable.l);
        nextbutton.setImageResource(R.drawable.r);
        pributton.getBackground().setAlpha(0);
        nextbutton.getBackground().setAlpha(0);
        savebutton.setVisibility(0);
        if (Integer.valueOf(this.sCurrentImg).intValue() == 1) {
            pributton.setEnabled(false);
            nextbutton.setEnabled(true);
        } else {
            pributton.setEnabled(true);
        }
        if (Integer.valueOf(this.sCurrentImg) == Integer.valueOf(this.sCountImg)) {
            nextbutton.setEnabled(false);
        } else {
            nextbutton.setEnabled(true);
        }
        if (!(Integer.valueOf(this.sCurrentImg).intValue() == 1 || Integer.valueOf(this.sCurrentImg) == Integer.valueOf(this.sCountImg))) {
            pributton.setEnabled(true);
            nextbutton.setEnabled(true);
        }
        ImageView imgv = (ImageView) findViewById(R.id.ImageView01);
        imgv.setImageResource(this.mImageIds[Integer.valueOf(this.sCurrentImg).intValue() - 1].intValue());
        imgv.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(HYDigimonI.this, fullScreenAct.class);
                Bundle bundle = new Bundle();
                bundle.putString("COUNTIMG", HYDigimonI.this.sCountImg);
                bundle.putString("CURRENTIMG", new StringBuilder().append(Integer.valueOf(HYDigimonI.this.sCurrentImg)).toString());
                intent.putExtras(bundle);
                HYDigimonI.this.startActivity(intent);
            }
        });
        this.adView = new AdView(this, AdSize.BANNER, "a14e363a9b1d766");
        ((LinearLayout) findViewById(R.id.AdmainLayout)).addView(this.adView);
        this.adView.loadAd(new AdRequest());
    }

    public void setWallpaper(InputStream data) throws IOException {
        super.setWallpaper(data);
    }

    /* access modifiers changed from: private */
    public void showDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(msg).setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        builder.create().show();
    }

    public void onDestroy() {
        this.adView.destroy();
        super.onDestroy();
    }
}
