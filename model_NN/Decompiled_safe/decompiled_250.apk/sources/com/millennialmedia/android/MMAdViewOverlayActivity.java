package com.millennialmedia.android;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import com.millennialmedia.android.MMAdViewSDK;

public class MMAdViewOverlayActivity extends Activity implements AccelerometerListener {
    private static Context context;
    private MMAdViewWebOverlay mmOverlay;
    protected Boolean shouldAccelerate;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        long time = 600;
        String transition = null;
        int padding = 0;
        boolean titlebar = false;
        String title = null;
        boolean bottombar = true;
        boolean bottombarEnabled = true;
        boolean isTransparent = false;
        boolean cachedAdView = false;
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            time = extras.getLong("transitionTime", 600);
            transition = extras.getString("overlayTransition");
            padding = extras.getInt("shouldResizeOverlay", 0);
            titlebar = extras.getBoolean("shouldShowTitlebar", false);
            title = extras.getString("overlayTitle");
            bottombar = extras.getBoolean("shouldShowBottomBar", true);
            bottombarEnabled = extras.getBoolean("shouldEnableBottomBar", true);
            isTransparent = extras.getBoolean("shouldMakeOverlayTransparent", false);
            cachedAdView = extras.getBoolean("cachedAdView", false);
        }
        MMAdViewSDK.Log.v("Padding: " + padding + " Time: " + time + " Transition: " + transition + " Title: " + title + " Bottom bar: " + bottombarEnabled + " Should accelerate: " + this.shouldAccelerate + " Tranparent: " + isTransparent + " Cached Ad: " + cachedAdView);
        MMAdViewSDK.Log.v("Path: " + getIntent().getData().getLastPathSegment());
        this.mmOverlay = new MMAdViewWebOverlay(this, padding, time, transition, titlebar, title, bottombar, bottombarEnabled, isTransparent);
        setTheme(16973840);
        setContentView(this.mmOverlay);
        this.mmOverlay.loadWebContent(getIntent().getDataString());
        this.shouldAccelerate = Boolean.valueOf(getIntent().getBooleanExtra("canAccelerate", false));
        if (cachedAdView) {
            setRequestedOrientation(0);
        } else {
            setRequestedOrientation(1);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        MMAdViewSDK.Log.d("Overlay onResume");
        if (this.shouldAccelerate.booleanValue()) {
            AccelerometerHelper.startListening(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        MMAdViewSDK.Log.d("Overlay onDestroy");
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        MMAdViewSDK.Log.d("Overlay onPause");
        if (AccelerometerHelper.isListening()) {
            AccelerometerHelper.stopListening();
        }
        setResult(0);
    }

    public void onBackPressed() {
        if (!this.mmOverlay.goBack()) {
            super.onBackPressed();
        }
    }

    public static Context getContext() {
        return context;
    }

    public void didAccelerate(float x, float y, float z) {
        MMAdViewSDK.Log.v("Accelerometer x:" + x + " y:" + y + " z:" + z);
        this.mmOverlay.injectJS("javascript:didAccelerate(" + x + "," + y + "," + z + ")");
    }

    public void didShake(float force) {
        MMAdViewSDK.Log.v("Phone shaken: " + force);
        this.mmOverlay.injectJS("javascript:didShake(" + force + ")");
    }
}
