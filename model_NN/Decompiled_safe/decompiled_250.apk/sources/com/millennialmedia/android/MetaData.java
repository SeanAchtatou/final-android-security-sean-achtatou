package com.millennialmedia.android;

import org.json.JSONException;
import org.json.JSONObject;

class MetaData {
    int acid;
    String ip;
    String urid;
    String version;

    MetaData() {
    }

    MetaData(JSONObject object) throws JSONException {
        deserializeMetaDataFromObj(object);
    }

    /* access modifiers changed from: package-private */
    public void deserializeMetaDataFromObj(JSONObject metaObject) throws JSONException {
        this.acid = metaObject.getInt("acid");
        this.ip = metaObject.getString("ip");
        this.urid = metaObject.getString("urid");
        this.version = metaObject.getString("version");
    }
}
