package com.image.fycrtones;

import android.app.Activity;
import android.os.Build;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.adwhirl.AdWhirlLayout;

public class AdsView4 {
    private static final boolean blackscreen = isBlackScreen();

    private static boolean isBlackScreen() {
        return Build.VERSION.SDK.equalsIgnoreCase("3");
    }

    public static void createAdWhirl(Activity activity) {
        int w;
        if (blackscreen) {
            w = 48;
        } else {
            w = -2;
        }
        try {
            ((LinearLayout) activity.findViewById(R.id.AdsView4)).addView(new AdWhirlLayout(activity, "4ece4f3211f245b2b1d7d8e0c54b05af"), new RelativeLayout.LayoutParams(-1, w));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
