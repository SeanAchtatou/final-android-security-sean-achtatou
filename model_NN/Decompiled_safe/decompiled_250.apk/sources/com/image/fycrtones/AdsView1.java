package com.image.fycrtones;

import android.app.Activity;
import android.os.Build;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.adwhirl.AdWhirlLayout;

public class AdsView1 {
    private static final boolean blackscreen = isBlackScreen();

    private static boolean isBlackScreen() {
        return Build.VERSION.SDK.equalsIgnoreCase("3");
    }

    public static void createAdWhirl(Activity activity) {
        int w;
        if (blackscreen) {
            w = 48;
        } else {
            w = -2;
        }
        try {
            ((LinearLayout) activity.findViewById(R.id.AdsView1)).addView(new AdWhirlLayout(activity, "4edad4d5eac94ca39c27685e1361b697"), new RelativeLayout.LayoutParams(-1, w));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
