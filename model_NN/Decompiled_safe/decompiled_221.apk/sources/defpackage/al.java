package defpackage;

import android.content.DialogInterface;
import com.tencent.securemodule.ui.TransparentActivity;

/* renamed from: al  reason: default package */
public class al implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TransparentActivity f15a;

    public al(TransparentActivity transparentActivity) {
        this.f15a = transparentActivity;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.f15a.l.sendEmptyMessage(1);
        new TransparentActivity.a(0).start();
    }
}
