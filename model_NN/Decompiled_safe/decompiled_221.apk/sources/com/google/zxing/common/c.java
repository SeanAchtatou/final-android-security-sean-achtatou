package com.google.zxing.common;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private final byte[] f159a;
    private int b;
    private int c;

    public c(byte[] bArr) {
        this.f159a = bArr;
    }

    public int a() {
        return ((this.f159a.length - this.b) * 8) - this.c;
    }

    public int a(int i) {
        byte b2;
        int i2;
        if (i < 1 || i > 32) {
            throw new IllegalArgumentException();
        }
        if (this.c > 0) {
            int i3 = 8 - this.c;
            int i4 = i < i3 ? i : i3;
            int i5 = i3 - i4;
            int i6 = (((255 >> (8 - i4)) << i5) & this.f159a[this.b]) >> i5;
            int i7 = i - i4;
            this.c = i4 + this.c;
            if (this.c == 8) {
                this.c = 0;
                this.b++;
            }
            b2 = i6;
            i2 = i7;
        } else {
            b2 = 0;
            i2 = i;
        }
        if (i2 <= 0) {
            return b2;
        }
        while (i2 >= 8) {
            b2 = (b2 << 8) | (this.f159a[this.b] & 255);
            this.b++;
            i2 -= 8;
        }
        if (i2 <= 0) {
            return b2;
        }
        int i8 = 8 - i2;
        int i9 = (b2 << i2) | ((((255 >> i8) << i8) & this.f159a[this.b]) >> i8);
        this.c = i2 + this.c;
        return i9;
    }
}
