package com.google.zxing.d.a;

public final class o {

    /* renamed from: a  reason: collision with root package name */
    public static final o f179a = new o(0, 1, "L");
    public static final o b = new o(1, 0, "M");
    public static final o c = new o(2, 3, "Q");
    public static final o d = new o(3, 2, "H");
    private static final o[] e = {b, f179a, d, c};
    private final int f;
    private final int g;
    private final String h;

    private o(int i, int i2, String str) {
        this.f = i;
        this.g = i2;
        this.h = str;
    }

    public static o a(int i) {
        if (i >= 0 && i < e.length) {
            return e[i];
        }
        throw new IllegalArgumentException();
    }

    public int a() {
        return this.f;
    }

    public String toString() {
        return this.h;
    }
}
