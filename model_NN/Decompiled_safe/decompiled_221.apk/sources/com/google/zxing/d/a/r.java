package com.google.zxing.d.a;

import com.google.zxing.FormatException;
import com.google.zxing.common.b;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.SwitchButton;
import com.tencent.assistant.component.TotalTabLayout;

public final class r {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f182a = {31892, 34236, 39577, 42195, 48118, 51042, 55367, 58893, 63784, 68472, 70749, 76311, 79154, 84390, 87683, 92361, 96236, 102084, 102881, 110507, 110734, 117786, 119615, 126325, 127568, 133589, 136944, 141498, 145311, 150283, 152622, 158308, 161089, 167017};
    private static final r[] b = f();
    private final int c;
    private final int[] d;
    private final t[] e;
    private final int f;

    private r(int i, int[] iArr, t tVar, t tVar2, t tVar3, t tVar4) {
        this.c = i;
        this.d = iArr;
        this.e = new t[]{tVar, tVar2, tVar3, tVar4};
        int a2 = tVar.a();
        s[] b2 = tVar.b();
        int i2 = 0;
        for (s sVar : b2) {
            i2 += (sVar.b() + a2) * sVar.a();
        }
        this.f = i2;
    }

    public static r a(int i) {
        if (i % 4 != 1) {
            throw FormatException.a();
        }
        try {
            return b((i - 17) >> 2);
        } catch (IllegalArgumentException e2) {
            throw FormatException.a();
        }
    }

    public static r b(int i) {
        if (i >= 1 && i <= 40) {
            return b[i - 1];
        }
        throw new IllegalArgumentException();
    }

    static r c(int i) {
        int i2 = Integer.MAX_VALUE;
        int i3 = 0;
        for (int i4 = 0; i4 < f182a.length; i4++) {
            int i5 = f182a[i4];
            if (i5 == i) {
                return b(i4 + 7);
            }
            int a2 = p.a(i, i5);
            if (a2 < i2) {
                i3 = i4 + 7;
                i2 = a2;
            }
        }
        if (i2 <= 3) {
            return b(i3);
        }
        return null;
    }

    private static r[] f() {
        return new r[]{new r(1, new int[0], new t(7, new s(1, 19)), new t(10, new s(1, 16)), new t(13, new s(1, 13)), new t(17, new s(1, 9))), new r(2, new int[]{6, 18}, new t(10, new s(1, 34)), new t(16, new s(1, 28)), new t(22, new s(1, 22)), new t(28, new s(1, 16))), new r(3, new int[]{6, 22}, new t(15, new s(1, 55)), new t(26, new s(1, 44)), new t(18, new s(2, 17)), new t(22, new s(2, 13))), new r(4, new int[]{6, 26}, new t(20, new s(1, 80)), new t(18, new s(2, 32)), new t(26, new s(2, 24)), new t(16, new s(4, 9))), new r(5, new int[]{6, 30}, new t(26, new s(1, 108)), new t(24, new s(2, 43)), new t(18, new s(2, 15), new s(2, 16)), new t(22, new s(2, 11), new s(2, 12))), new r(6, new int[]{6, 34}, new t(18, new s(2, 68)), new t(16, new s(4, 27)), new t(24, new s(4, 19)), new t(28, new s(4, 15))), new r(7, new int[]{6, 22, 38}, new t(20, new s(2, 78)), new t(18, new s(4, 31)), new t(18, new s(2, 14), new s(4, 15)), new t(26, new s(4, 13), new s(1, 14))), new r(8, new int[]{6, 24, 42}, new t(24, new s(2, 97)), new t(22, new s(2, 38), new s(2, 39)), new t(22, new s(4, 18), new s(2, 19)), new t(26, new s(4, 14), new s(2, 15))), new r(9, new int[]{6, 26, 46}, new t(30, new s(2, 116)), new t(22, new s(3, 36), new s(2, 37)), new t(20, new s(4, 16), new s(4, 17)), new t(24, new s(4, 12), new s(4, 13))), new r(10, new int[]{6, 28, 50}, new t(18, new s(2, 68), new s(2, 69)), new t(26, new s(4, 43), new s(1, 44)), new t(24, new s(6, 19), new s(2, 20)), new t(28, new s(6, 15), new s(2, 16))), new r(11, new int[]{6, 30, 54}, new t(20, new s(4, 81)), new t(30, new s(1, 50), new s(4, 51)), new t(28, new s(4, 22), new s(4, 23)), new t(24, new s(3, 12), new s(8, 13))), new r(12, new int[]{6, 32, 58}, new t(24, new s(2, 92), new s(2, 93)), new t(22, new s(6, 36), new s(2, 37)), new t(26, new s(4, 20), new s(6, 21)), new t(28, new s(7, 14), new s(4, 15))), new r(13, new int[]{6, 34, 62}, new t(26, new s(4, 107)), new t(22, new s(8, 37), new s(1, 38)), new t(24, new s(8, 20), new s(4, 21)), new t(22, new s(12, 11), new s(4, 12))), new r(14, new int[]{6, 26, 46, 66}, new t(30, new s(3, 115), new s(1, 116)), new t(24, new s(4, 40), new s(5, 41)), new t(20, new s(11, 16), new s(5, 17)), new t(24, new s(11, 12), new s(5, 13))), new r(15, new int[]{6, 26, 48, 70}, new t(22, new s(5, 87), new s(1, 88)), new t(24, new s(5, 41), new s(5, 42)), new t(30, new s(5, 24), new s(7, 25)), new t(24, new s(11, 12), new s(7, 13))), new r(16, new int[]{6, 26, 50, 74}, new t(24, new s(5, 98), new s(1, 99)), new t(28, new s(7, 45), new s(3, 46)), new t(24, new s(15, 19), new s(2, 20)), new t(30, new s(3, 15), new s(13, 16))), new r(17, new int[]{6, 30, 54, 78}, new t(28, new s(1, 107), new s(5, 108)), new t(28, new s(10, 46), new s(1, 47)), new t(28, new s(1, 22), new s(15, 23)), new t(28, new s(2, 14), new s(17, 15))), new r(18, new int[]{6, 30, 56, 82}, new t(30, new s(5, 120), new s(1, 121)), new t(26, new s(9, 43), new s(4, 44)), new t(28, new s(17, 22), new s(1, 23)), new t(28, new s(2, 14), new s(19, 15))), new r(19, new int[]{6, 30, 58, 86}, new t(28, new s(3, 113), new s(4, 114)), new t(26, new s(3, 44), new s(11, 45)), new t(26, new s(17, 21), new s(4, 22)), new t(26, new s(9, 13), new s(16, 14))), new r(20, new int[]{6, 34, 62, 90}, new t(28, new s(3, 107), new s(5, 108)), new t(26, new s(3, 41), new s(13, 42)), new t(30, new s(15, 24), new s(5, 25)), new t(28, new s(15, 15), new s(10, 16))), new r(21, new int[]{6, 28, 50, 72, 94}, new t(28, new s(4, 116), new s(4, 117)), new t(26, new s(17, 42)), new t(28, new s(17, 22), new s(6, 23)), new t(30, new s(19, 16), new s(6, 17))), new r(22, new int[]{6, 26, 50, 74, 98}, new t(28, new s(2, 111), new s(7, 112)), new t(28, new s(17, 46)), new t(30, new s(7, 24), new s(16, 25)), new t(24, new s(34, 13))), new r(23, new int[]{6, 30, 54, 78, 102}, new t(30, new s(4, 121), new s(5, 122)), new t(28, new s(4, 47), new s(14, 48)), new t(30, new s(11, 24), new s(14, 25)), new t(30, new s(16, 15), new s(14, 16))), new r(24, new int[]{6, 28, 54, 80, 106}, new t(30, new s(6, 117), new s(4, 118)), new t(28, new s(6, 45), new s(14, 46)), new t(30, new s(11, 24), new s(16, 25)), new t(30, new s(30, 16), new s(2, 17))), new r(25, new int[]{6, 32, 58, 84, NormalErrorRecommendPage.ERROR_TYPE_SEARCH_RESULT_EMPTY}, new t(26, new s(8, 106), new s(4, 107)), new t(28, new s(8, 47), new s(13, 48)), new t(30, new s(7, 24), new s(22, 25)), new t(30, new s(22, 15), new s(13, 16))), new r(26, new int[]{6, 30, 58, 86, 114}, new t(28, new s(10, 114), new s(2, 115)), new t(28, new s(19, 46), new s(4, 47)), new t(28, new s(28, 22), new s(6, 23)), new t(30, new s(33, 16), new s(4, 17))), new r(27, new int[]{6, 34, 62, 90, 118}, new t(30, new s(8, 122), new s(4, 123)), new t(28, new s(22, 45), new s(3, 46)), new t(30, new s(8, 23), new s(26, 24)), new t(30, new s(12, 15), new s(28, 16))), new r(28, new int[]{6, 26, 50, 74, 98, 122}, new t(30, new s(3, 117), new s(10, 118)), new t(28, new s(3, 45), new s(23, 46)), new t(30, new s(4, 24), new s(31, 25)), new t(30, new s(11, 15), new s(31, 16))), new r(29, new int[]{6, 30, 54, 78, 102, 126}, new t(30, new s(7, 116), new s(7, 117)), new t(28, new s(21, 45), new s(7, 46)), new t(30, new s(1, 23), new s(37, 24)), new t(30, new s(19, 15), new s(26, 16))), new r(30, new int[]{6, 26, 52, 78, 104, 130}, new t(30, new s(5, 115), new s(10, 116)), new t(28, new s(19, 47), new s(10, 48)), new t(30, new s(15, 24), new s(25, 25)), new t(30, new s(23, 15), new s(25, 16))), new r(31, new int[]{6, 30, 56, 82, 108, 134}, new t(30, new s(13, 115), new s(3, 116)), new t(28, new s(2, 46), new s(29, 47)), new t(30, new s(42, 24), new s(1, 25)), new t(30, new s(23, 15), new s(28, 16))), new r(32, new int[]{6, 34, 60, 86, 112, 138}, new t(30, new s(17, 115)), new t(28, new s(10, 46), new s(23, 47)), new t(30, new s(10, 24), new s(35, 25)), new t(30, new s(19, 15), new s(35, 16))), new r(33, new int[]{6, 30, 58, 86, 114, 142}, new t(30, new s(17, 115), new s(1, 116)), new t(28, new s(14, 46), new s(21, 47)), new t(30, new s(29, 24), new s(19, 25)), new t(30, new s(11, 15), new s(46, 16))), new r(34, new int[]{6, 34, 62, 90, 118, 146}, new t(30, new s(13, 115), new s(6, 116)), new t(28, new s(14, 46), new s(23, 47)), new t(30, new s(44, 24), new s(7, 25)), new t(30, new s(59, 16), new s(1, 17))), new r(35, new int[]{6, 30, 54, 78, 102, 126, SwitchButton.SWITCH_ANIMATION_DURATION}, new t(30, new s(12, 121), new s(7, 122)), new t(28, new s(12, 47), new s(26, 48)), new t(30, new s(39, 24), new s(14, 25)), new t(30, new s(22, 15), new s(41, 16))), new r(36, new int[]{6, 24, 50, 76, 102, 128, 154}, new t(30, new s(6, 121), new s(14, 122)), new t(28, new s(6, 47), new s(34, 48)), new t(30, new s(46, 24), new s(10, 25)), new t(30, new s(2, 15), new s(64, 16))), new r(37, new int[]{6, 28, 54, 80, 106, 132, 158}, new t(30, new s(17, 122), new s(4, 123)), new t(28, new s(29, 46), new s(14, 47)), new t(30, new s(49, 24), new s(10, 25)), new t(30, new s(24, 15), new s(46, 16))), new r(38, new int[]{6, 32, 58, 84, NormalErrorRecommendPage.ERROR_TYPE_SEARCH_RESULT_EMPTY, TotalTabLayout.START_ID_INDEX, 162}, new t(30, new s(4, 122), new s(18, 123)), new t(28, new s(13, 46), new s(32, 47)), new t(30, new s(48, 24), new s(14, 25)), new t(30, new s(42, 15), new s(32, 16))), new r(39, new int[]{6, 26, 54, 82, NormalErrorRecommendPage.ERROR_TYPE_SEARCH_RESULT_EMPTY, 138, 166}, new t(30, new s(20, 117), new s(4, 118)), new t(28, new s(40, 47), new s(7, 48)), new t(30, new s(43, 24), new s(22, 25)), new t(30, new s(10, 15), new s(67, 16))), new r(40, new int[]{6, 30, 58, 86, 114, 142, 170}, new t(30, new s(19, 118), new s(6, 119)), new t(28, new s(18, 47), new s(31, 48)), new t(30, new s(34, 24), new s(34, 25)), new t(30, new s(20, 15), new s(61, 16)))};
    }

    public int a() {
        return this.c;
    }

    public t a(o oVar) {
        return this.e[oVar.a()];
    }

    public int[] b() {
        return this.d;
    }

    public int c() {
        return this.f;
    }

    public int d() {
        return (this.c * 4) + 17;
    }

    /* access modifiers changed from: package-private */
    public b e() {
        int d2 = d();
        b bVar = new b(d2);
        bVar.a(0, 0, 9, 9);
        bVar.a(d2 - 8, 0, 8, 9);
        bVar.a(0, d2 - 8, 9, 8);
        int length = this.d.length;
        for (int i = 0; i < length; i++) {
            int i2 = this.d[i] - 2;
            for (int i3 = 0; i3 < length; i3++) {
                if (!((i == 0 && (i3 == 0 || i3 == length - 1)) || (i == length - 1 && i3 == 0))) {
                    bVar.a(this.d[i3] - 2, i2, 5, 5);
                }
            }
        }
        bVar.a(6, 9, 1, d2 - 17);
        bVar.a(9, 6, d2 - 17, 1);
        if (this.c > 6) {
            bVar.a(d2 - 11, 0, 3, 6);
            bVar.a(0, d2 - 11, 6, 3);
        }
        return bVar;
    }

    public String toString() {
        return String.valueOf(this.c);
    }
}
