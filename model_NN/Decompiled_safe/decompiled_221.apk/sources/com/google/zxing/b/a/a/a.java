package com.google.zxing.b.a.a;

import java.util.Vector;

final class a {
    private a() {
    }

    static com.google.zxing.common.a a(Vector vector) {
        int size = (vector.size() << 1) - 1;
        com.google.zxing.common.a aVar = new com.google.zxing.common.a((((b) vector.lastElement()).c() == null ? size - 1 : size) * 12);
        int a2 = ((b) vector.elementAt(0)).c().a();
        int i = 11;
        int i2 = 0;
        while (i >= 0) {
            if (((1 << i) & a2) != 0) {
                aVar.b(i2);
            }
            i--;
            i2++;
        }
        int i3 = i2;
        for (int i4 = 1; i4 < vector.size(); i4++) {
            b bVar = (b) vector.elementAt(i4);
            int a3 = bVar.b().a();
            int i5 = 11;
            while (i5 >= 0) {
                if (((1 << i5) & a3) != 0) {
                    aVar.b(i3);
                }
                i5--;
                i3++;
            }
            if (bVar.c() != null) {
                int a4 = bVar.c().a();
                for (int i6 = 11; i6 >= 0; i6--) {
                    if (((1 << i6) & a4) != 0) {
                        aVar.b(i3);
                    }
                    i3++;
                }
            }
        }
        return aVar;
    }
}
