package com.qq.AppService;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import com.qq.a.a.d;
import com.qq.provider.h;
import com.qq.provider.y;
import com.tencent.connect.common.Constants;
import com.tencent.open.SocialConstants;

/* compiled from: ProGuard */
public class SmsSentReceiver extends BroadcastReceiver {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void onReceive(Context context, Intent intent) {
        int intExtra;
        int i = 0;
        if (intent.getAction().startsWith("ACTION_SMS_SENT")) {
            int intExtra2 = intent.getIntExtra("_id", -1);
            if (intExtra2 > 0) {
                int resultCode = getResultCode();
                switch (resultCode) {
                    case -1:
                        Uri withAppendedPath = Uri.withAppendedPath(d.f259a, Constants.STR_EMPTY + intExtra2);
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("status", (Integer) 0);
                        contentValues.put(SocialConstants.PARAM_TYPE, (Integer) 2);
                        context.getContentResolver().update(withAppendedPath, contentValues, null, null);
                        h.a(intExtra2, 2, 0);
                        Log.e("com.qq.connect", " modify Sms " + intExtra2 + " to complete");
                        return;
                    default:
                        Uri withAppendedPath2 = Uri.withAppendedPath(d.f259a, Constants.STR_EMPTY + intExtra2);
                        ContentValues contentValues2 = new ContentValues();
                        contentValues2.put(SocialConstants.PARAM_TYPE, (Integer) 5);
                        contentValues2.put("status", (Integer) 128);
                        context.getContentResolver().update(withAppendedPath2, contentValues2, null, null);
                        Log.e("com.qq.connect.smsHandler", " send failed resultCode" + resultCode);
                        h.a(intExtra2, 6, 128);
                        return;
                }
            }
        } else if (intent.getAction().startsWith("ACTION_SMS_DELIVERY") && (intExtra = intent.getIntExtra("_id", -1)) > 0) {
            int resultCode2 = getResultCode();
            byte[] byteArrayExtra = intent.getByteArrayExtra("pdu");
            if (byteArrayExtra != null) {
                try {
                    i = y.a(byteArrayExtra);
                } catch (Throwable th) {
                    return;
                }
            }
            switch (resultCode2) {
                case -1:
                    Uri withAppendedPath3 = Uri.withAppendedPath(d.f259a, Constants.STR_EMPTY + intExtra);
                    ContentValues contentValues3 = new ContentValues();
                    if (i == 0 || i == 131072) {
                        contentValues3.put(SocialConstants.PARAM_TYPE, (Integer) 2);
                        contentValues3.put("status", (Integer) -1);
                    } else {
                        contentValues3.put(SocialConstants.PARAM_TYPE, (Integer) 5);
                        contentValues3.put("status", (Integer) 128);
                    }
                    context.getContentResolver().update(withAppendedPath3, contentValues3, null, null);
                    if (i == 0 || i == 131072) {
                        h.a(intExtra, 2, -1);
                    } else {
                        h.a(intExtra, 5, 128);
                    }
                    Log.e("com.qq.connect", " delivery got it for  sms:" + intExtra + " status:" + i);
                    return;
                default:
                    return;
            }
        }
    }
}
