package com.qq.AppService;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

/* compiled from: ProGuard */
public final class af {
    public static int a(Context context) {
        try {
            WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
            if (wifiManager == null) {
                return -1;
            }
            if (!wifiManager.isWifiEnabled()) {
                return -1;
            }
            for (int i = 0; i < 50; i++) {
                WifiInfo connectionInfo = wifiManager.getConnectionInfo();
                if (connectionInfo != null && connectionInfo.getBSSID() != null) {
                    return connectionInfo.getIpAddress();
                }
            }
            return -1;
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public static InetAddress b(Context context) {
        Enumeration<InetAddress> inetAddresses;
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                NetworkInterface nextElement = networkInterfaces.nextElement();
                if (nextElement != null && nextElement.getName() == null) {
                    if ((nextElement.getName().equals("wl0.1") || nextElement.getName().equals("eth0") || nextElement.getName().equals("tiap0")) && (inetAddresses = nextElement.getInetAddresses()) != null) {
                        while (inetAddresses.hasMoreElements()) {
                            InetAddress nextElement2 = inetAddresses.nextElement();
                            if (nextElement2 != null && nextElement2.isSiteLocalAddress()) {
                                return nextElement2;
                            }
                        }
                        continue;
                    }
                }
            }
            return null;
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    public static int a(InetAddress inetAddress) {
        if (inetAddress == null) {
            return -1;
        }
        byte[] address = inetAddress.getAddress();
        return (address[0] & 255) | ((address[3] << 24) & -16777216) | 0 | ((address[2] << 16) & 16711680) | ((address[1] << 8) & 65280);
    }
}
