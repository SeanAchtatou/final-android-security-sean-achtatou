package com.qq.AppService;

import android.opengl.GLSurfaceView;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/* compiled from: ProGuard */
class al implements GLSurfaceView.Renderer {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ OpenGLActivity f219a;

    al(OpenGLActivity openGLActivity) {
        this.f219a = openGLActivity;
    }

    public void onSurfaceCreated(GL10 gl10, EGLConfig eGLConfig) {
        String glGetString = gl10.glGetString(7937);
        String glGetString2 = gl10.glGetString(7936);
        String glGetString3 = gl10.glGetString(7938);
        String glGetString4 = gl10.glGetString(7939);
        OpenGLActivity.b = new am();
        OpenGLActivity.b.f220a = glGetString;
        OpenGLActivity.b.b = glGetString2;
        OpenGLActivity.b.c = glGetString3;
        OpenGLActivity.b.d = glGetString4;
        OpenGLActivity.f205a = gl10.glGetString(7937);
        synchronized (OpenGLActivity.c) {
            OpenGLActivity.c.notifyAll();
        }
        this.f219a.finish();
    }

    public void onSurfaceChanged(GL10 gl10, int i, int i2) {
    }

    public void onDrawFrame(GL10 gl10) {
    }
}
