package com.qq.AppService;

import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Process;
import android.text.TextUtils;
import com.qq.provider.i;
import com.qq.util.p;
import com.tencent.assistant.Global;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.backgroundscan.BackgroundReceiver;
import com.tencent.assistant.db.table.k;
import com.tencent.assistant.e.a;
import com.tencent.assistant.event.EventController;
import com.tencent.assistant.event.EventDispatcher;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.floatingwindow.FloatWindowReceiver;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.notification.v;
import com.tencent.assistant.module.a.a.e;
import com.tencent.assistant.module.a.a.h;
import com.tencent.assistant.module.a.b;
import com.tencent.assistant.module.a.j;
import com.tencent.assistant.module.a.n;
import com.tencent.assistant.module.update.c;
import com.tencent.assistant.module.wisedownload.l;
import com.tencent.assistant.plugin.mgr.d;
import com.tencent.assistant.plugin.proxy.PluginProxyActivity;
import com.tencent.assistant.plugin.system.DockReceiver;
import com.tencent.assistant.plugin.system.FreewifiReceiver;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.cl;
import com.tencent.assistant.utils.co;
import com.tencent.assistant.utils.cq;
import com.tencent.downloadsdk.DownloadManager;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public class AstApp extends Application {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f201a;
    public static boolean b;
    public static long d = 0;
    private static final String[] e = {"/sdcard/Android", "/sdcard/DCIM"};
    /* access modifiers changed from: private */
    public static AstApp f;
    private static Activity k = null;
    private static cq l = null;
    private static Activity m = null;
    /* access modifiers changed from: private */
    public static volatile boolean o = false;
    private static boolean q = false;
    /* access modifiers changed from: private */
    public static long v = 0;
    /* access modifiers changed from: private */
    public static long w = 0;
    private static Runnable z = new p();
    long c = 0;
    private EventDispatcher g;
    private EventController h;
    private int i = -1;
    private boolean j = false;
    private boolean n = true;
    /* access modifiers changed from: private */
    public cl p = new cl();
    private DockReceiver r = null;
    private BackgroundReceiver s = null;
    private FloatWindowReceiver t = null;
    private FreewifiReceiver u;
    private Map<String, a> x = new HashMap();
    private BroadcastReceiver y = new k(this);

    public void onCreate() {
        super.onCreate();
        d = System.currentTimeMillis();
        f = this;
        co.a(false);
        com.tencent.assistant.login.a.a.h();
        DownloadManager.a().b(f);
        DownloadManager.a().a(5, new Class[]{k.class});
        this.g = EventDispatcher.getInstance(null);
        this.h = EventController.getInstance();
        String packageName = getPackageName();
        String a2 = p.a(Process.myPid());
        if (a2 == null || !a2.startsWith(packageName)) {
            a2 = p.a(this, Process.myPid());
        }
        if (a2 != null && packageName != null && a2.equals(packageName)) {
            try {
                E();
            } catch (Throwable th) {
                th.printStackTrace();
            }
            g();
        } else if (a2 != null && packageName != null && a2.equals(packageName + ":connect")) {
            y();
        }
    }

    private void y() {
        this.i = 1;
        this.g.setListener(this.h);
        A();
        z();
        i.a();
    }

    private void z() {
        TemporaryThreadManager.get().start(new i(this));
    }

    private void A() {
        registerReceiver(this.y, new IntentFilter("com.tencent.android.qqdownloader.action.QUERY_CONNECT_STATE"));
    }

    private void B() {
        TemporaryThreadManager.get().start(new j(this));
    }

    /* access modifiers changed from: private */
    public void C() {
        XLog.d("miles", "AstApp >> startDirCreateObservers");
        List<String> a2 = a("/sdcard");
        HashSet hashSet = new HashSet(Arrays.asList(e));
        if (a2 != null && !a2.isEmpty()) {
            for (String next : a2) {
                if (!hashSet.contains(next)) {
                    a aVar = new a(next, Process.PROC_COMBINE);
                    aVar.startWatching();
                    this.x.put(next, aVar);
                }
            }
        }
    }

    private List<String> a(String str) {
        ArrayList arrayList = new ArrayList();
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        File file = new File(str);
        if (!file.exists() || !file.isDirectory()) {
            return null;
        }
        arrayList.add(str);
        for (File file2 : file.listFiles()) {
            if (file2.exists() && file2.isDirectory()) {
                arrayList.add(file2.getAbsolutePath());
            }
        }
        return arrayList;
    }

    public void a() {
        XLog.i("KillMe", "updateLastActivityCreateTime");
        w = System.currentTimeMillis();
    }

    private void D() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.SCREEN_OFF");
        intentFilter.addAction("android.intent.action.SCREEN_ON");
        if (this.r == null) {
            this.r = new DockReceiver();
        }
        registerReceiver(this.r, intentFilter);
        if (this.t == null) {
            this.t = new FloatWindowReceiver();
        }
        registerReceiver(this.t, intentFilter);
        if (m.a().u()) {
            b();
        }
        IntentFilter intentFilter2 = new IntentFilter();
        intentFilter2.addAction("android.intent.action.USER_PRESENT");
        if (this.s == null) {
            this.s = new BackgroundReceiver();
        }
        registerReceiver(this.s, intentFilter2);
    }

    public void b() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.wifi.SCAN_RESULTS");
        if (this.u == null) {
            this.u = new FreewifiReceiver();
        }
        registerReceiver(this.u, intentFilter);
    }

    public void c() {
        if (this.u != null) {
            unregisterReceiver(this.u);
        }
    }

    private void E() {
        int a2 = m.a().a("bao_current_version_code", 0);
        int appVersionCode = Global.getAppVersionCode();
        if (appVersionCode != a2) {
            q = true;
            TemporaryThreadManager.get().start(new l(this, a2, appVersionCode));
            return;
        }
        q = false;
        d.a(true);
    }

    public static boolean d() {
        return q;
    }

    /* access modifiers changed from: private */
    public void F() {
        d.b().a(this);
    }

    public void e() {
        this.c = System.currentTimeMillis();
    }

    public void f() {
        v = System.currentTimeMillis();
        XLog.i("KillMe", "astapp exit called.");
        Intent intent = new Intent("com.tencent.android.qqdownloader.action.EXIT_APP");
        intent.addCategory("android.intent.category.DEFAULT");
        sendBroadcast(intent);
        v.a().b();
        TemporaryThreadManager.get().start(new m(this));
    }

    /* access modifiers changed from: private */
    public void G() {
        ba.a().postDelayed(new n(this), 800);
    }

    public void onLowMemory() {
        super.onLowMemory();
        if (this.i == 0) {
            com.tencent.assistant.manager.cq.a().b();
        }
    }

    public void g() {
        com.tencent.beacon.event.a.a(this);
        this.i = 0;
        this.g.setListener(this.h);
        c.a().b();
        l.a().b();
        com.tencent.assistant.module.a.d.a().a(new h());
        com.tencent.assistant.module.a.d.a().a(new b());
        com.tencent.assistant.module.a.d.a().a(new j());
        com.tencent.assistant.module.a.d.a().a(new com.tencent.assistant.module.a.a.b());
        com.tencent.assistant.module.a.d.a().a(new com.tencent.assistant.module.a.c(getApplicationContext()));
        com.tencent.assistant.module.a.d.a().a(new com.tencent.assistant.module.a.a.c());
        com.tencent.assistant.module.a.d.a().a(new com.tencent.assistant.module.a.k());
        com.tencent.assistant.module.a.d.a().a(new e());
        com.tencent.assistant.module.a.d.a().a(new com.tencent.assistant.module.a.a.d());
        com.tencent.assistant.module.a.d.a().a(new n());
        com.tencent.assistant.module.a.d.a().a(new com.tencent.assistant.module.a.a.i());
        com.tencent.assistant.module.a.d.a().a(new com.tencent.assistant.module.a.i());
        com.tencent.assistant.module.a.d.a().b();
        if (this.n) {
            this.n = false;
            i().sendBroadcast(new Intent("com.tencent.assistant.ipc.firststart.action"));
        }
        D();
        if (Global.isGray() || Global.isDev()) {
            B();
        }
        ba.a().postDelayed(new o(this), 10000);
    }

    /* access modifiers changed from: protected */
    public int h() {
        if (AppService.q()) {
            return 2;
        }
        if (AppService.r()) {
            return 1;
        }
        if (AppService.s()) {
            return 1;
        }
        if (!AppService.t()) {
            return AppService.u() ? 3 : 0;
        }
        return 2;
    }

    public static AstApp i() {
        return f;
    }

    public EventDispatcher j() {
        return this.g;
    }

    public EventController k() {
        return this.h;
    }

    public boolean l() {
        return this.j;
    }

    public void a(boolean z2, int i2) {
        if (this.j && !z2) {
            this.j = z2;
            this.g.sendMessageDelayed(this.g.obtainMessage(1032), (long) i2);
        } else if (!this.j && z2) {
            this.j = z2;
            this.g.sendMessageDelayed(this.g.obtainMessage(EventDispatcherEnum.UI_EVENT_APP_GOFRONT), (long) i2);
        }
    }

    public static BaseActivity m() {
        if (k == null || !(k instanceof BaseActivity)) {
            return null;
        }
        return (BaseActivity) k;
    }

    public static Activity n() {
        if (k != null) {
            return k;
        }
        return null;
    }

    public static void a(Activity activity) {
        if (l == null) {
            l = new cq(8);
        }
        if (activity != null) {
            if (activity instanceof PluginProxyActivity) {
                l.a(((PluginProxyActivity) activity).a());
            } else {
                l.a(activity.getClass().getSimpleName());
            }
        }
        k = activity;
    }

    public static String o() {
        if (l != null) {
            return l.toString();
        }
        return "NotAdd";
    }

    public static Activity p() {
        return m;
    }

    public static Runnable q() {
        return z;
    }

    public static boolean r() {
        return o;
    }

    private static void a(PackageManager packageManager, String str) {
        try {
            packageManager.setComponentEnabledSetting(new ComponentName(i().getPackageName(), str), 2, 1);
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: private */
    public static void H() {
        if (!m.a().x()) {
            PackageManager packageManager = i().getPackageManager();
            a(packageManager, "com.tencent.android.receive.PackageManagerReceiver");
            a(packageManager, "com.tencent.android.receive.NotificationReceiver");
            m.a().m(true);
        }
    }

    public boolean s() {
        return this.i == 0;
    }

    /* access modifiers changed from: private */
    public static void I() {
        if (!m.a().m()) {
            m.a().e(true);
        }
    }
}
