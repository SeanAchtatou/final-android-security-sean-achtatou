package com.qq.util;

import android.text.TextUtils;
import com.qq.AppService.AstApp;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.regex.Pattern;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

/* compiled from: ProGuard */
public class g {
    private static g b;

    /* renamed from: a  reason: collision with root package name */
    private i f367a;

    public static synchronized g a() {
        g gVar;
        synchronized (g.class) {
            if (b == null) {
                b = new g();
            }
            gVar = b;
        }
        return gVar;
    }

    public ArrayList<String> b() {
        if (this.f367a == null) {
            return null;
        }
        return this.f367a.d;
    }

    public int a(String str) {
        int lastIndexOf;
        if (str == null || str.trim().length() == 0 || (lastIndexOf = str.lastIndexOf(47)) == -1) {
            return 0;
        }
        String substring = str.substring(0, lastIndexOf);
        if (this.f367a == null) {
            return 0;
        }
        if (e(substring) || d(substring)) {
            return 1;
        }
        if (b(substring)) {
            return 2;
        }
        return c(substring);
    }

    private boolean b(String str) {
        return str.endsWith("DCIM/Camera/MobileAssistant");
    }

    private int c(String str) {
        ArrayList<String> arrayList = this.f367a.d;
        if (arrayList == null) {
            return 0;
        }
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            String str2 = arrayList.get(i);
            if (str2 != null && str.endsWith(str2)) {
                return i + 3;
            }
        }
        return 0;
    }

    private boolean d(String str) {
        ArrayList<String> arrayList = this.f367a.c;
        if (arrayList == null) {
            return false;
        }
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            String str2 = arrayList.get(i);
            if (str2 != null && Pattern.compile(str2).matcher(str).find()) {
                return true;
            }
        }
        return false;
    }

    private boolean e(String str) {
        ArrayList<String> arrayList = this.f367a.b;
        if (arrayList == null) {
            return false;
        }
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            String str2 = arrayList.get(i);
            if (str2 != null && str.endsWith(str2)) {
                return true;
            }
        }
        return false;
    }

    public void c() {
        InputStream inputStream = null;
        try {
            inputStream = AstApp.i().getAssets().open("VideoFilter.xml");
            this.f367a = a(inputStream);
            if (inputStream != null) {
                try {
                    inputStream.close();
                    return;
                } catch (IOException e) {
                    e = e;
                }
            } else {
                return;
            }
            e.printStackTrace();
        } catch (Throwable th) {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
            throw th;
        }
    }

    public void d() {
        new h(this).start();
    }

    private i a(InputStream inputStream) {
        XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
        newPullParser.setInput(inputStream, "utf-8");
        ArrayList<String> arrayList = null;
        ArrayList<String> arrayList2 = null;
        ArrayList<String> arrayList3 = null;
        i iVar = null;
        String str = null;
        for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
            String name = newPullParser.getName();
            if (eventType == 2) {
                if ("A".equals(name)) {
                    iVar = new i();
                } else if ("V".equals(name)) {
                    str = "V";
                } else if ("B".equals(name)) {
                    arrayList3 = new ArrayList<>();
                } else if ("C".equals(name)) {
                    str = "C";
                } else if ("D".equals(name)) {
                    str = "D";
                    if (arrayList2 == null) {
                        arrayList2 = new ArrayList<>();
                    }
                } else if ("E".equals(name)) {
                    arrayList = new ArrayList<>();
                } else if ("F".equals(name)) {
                    str = "F";
                }
            } else if (eventType == 3) {
                if ("B".equals(name)) {
                    if (iVar != null) {
                        if (arrayList3 != null && arrayList3.size() > 0) {
                            iVar.b = arrayList3;
                        }
                        if (arrayList2 != null && arrayList2.size() > 0) {
                            iVar.c = arrayList2;
                        }
                    }
                } else if ("E".equals(name) && iVar != null && arrayList != null && arrayList.size() > 0) {
                    iVar.d = arrayList;
                }
            } else if (eventType == 4) {
                String text = newPullParser.getText();
                if ("V".equals(str)) {
                    if (iVar != null && !TextUtils.isEmpty(text)) {
                        iVar.f369a = Integer.parseInt(text);
                    }
                } else if ("C".equals(str)) {
                    if (arrayList3 != null && !TextUtils.isEmpty(text)) {
                        arrayList3.add(text);
                    }
                } else if ("D".equals(str)) {
                    if (arrayList2 != null && !TextUtils.isEmpty(text)) {
                        arrayList2.add(text);
                    }
                } else if ("F".equals(str) && arrayList != null && !TextUtils.isEmpty(text)) {
                    arrayList.add(text);
                }
                str = null;
            }
        }
        return iVar;
    }
}
