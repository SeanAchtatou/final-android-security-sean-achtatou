package com.qq.util;

/* compiled from: ProGuard */
public class s {
    public static String a(int i) {
        if (i == 0) {
            return "root";
        }
        if (i < 1000) {
            return "linux";
        }
        if (i == 1000) {
            return "system";
        }
        if (i == 2000) {
            return "shell";
        }
        if (i >= 10000) {
            return "app_" + (i - 10000);
        }
        return "android";
    }
}
