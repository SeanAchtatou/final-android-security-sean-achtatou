package com.qq.util;

import com.qq.AppService.s;
import java.util.Random;

/* compiled from: ProGuard */
public final class QQUtils {

    /* compiled from: ProGuard */
    public enum Quality {
        Qua_40,
        Qua_100
    }

    public static String a(String str, Quality quality) {
        if (s.b(str)) {
            return null;
        }
        Random random = new Random();
        StringBuilder sb = new StringBuilder(128);
        sb.append("http://");
        sb.append('q');
        sb.append(random.nextInt(4) + 1);
        sb.append(".qlogo.cn/g?b=qq&nk=");
        sb.append(str);
        if (quality == Quality.Qua_100) {
            sb.append("&s=100&t=");
        } else {
            sb.append("&s=40&t=");
        }
        sb.append(System.currentTimeMillis());
        return sb.toString();
    }
}
