package com.qq.m;

import android.os.Process;
import java.util.Vector;

/* compiled from: ProGuard */
public final class i extends Thread {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ h f317a;
    private volatile boolean b = false;
    private volatile boolean c = false;
    private volatile boolean d = false;
    private int e = 0;
    private Vector<String> f = new Vector<>((int) Process.PROC_COMBINE);

    public i(h hVar, int i) {
        this.f317a = hVar;
        this.e = i;
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 153 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r7 = this;
            r6 = 1
            r0 = 0
            super.run()
            r7.b = r6
            r7.d = r0
            com.qq.AppService.AstApp r1 = com.qq.AppService.AstApp.i()
            if (r1 != 0) goto L_0x0013
        L_0x000f:
            return
        L_0x0010:
            r0 = 0
            r7.c = r0     // Catch:{ Throwable -> 0x0069 }
        L_0x0013:
            boolean r0 = r7.b     // Catch:{ Throwable -> 0x0069 }
            if (r0 == 0) goto L_0x006d
            boolean r0 = r7.c     // Catch:{ Throwable -> 0x0069 }
            if (r0 != 0) goto L_0x002e
            java.util.Vector<java.lang.String> r2 = r7.f     // Catch:{ Throwable -> 0x0069 }
            monitor-enter(r2)     // Catch:{ Throwable -> 0x0069 }
            java.util.Vector<java.lang.String> r0 = r7.f     // Catch:{ all -> 0x0083 }
            int r0 = r0.size()     // Catch:{ all -> 0x0083 }
            if (r0 != 0) goto L_0x002d
            java.util.Vector<java.lang.String> r0 = r7.f     // Catch:{ InterruptedException -> 0x007e }
            r3 = 1000(0x3e8, double:4.94E-321)
            r0.wait(r3)     // Catch:{ InterruptedException -> 0x007e }
        L_0x002d:
            monitor-exit(r2)     // Catch:{ all -> 0x0083 }
        L_0x002e:
            java.util.Vector<java.lang.String> r0 = r7.f     // Catch:{ Throwable -> 0x0069 }
            int r0 = r0.size()     // Catch:{ Throwable -> 0x0069 }
            if (r0 <= 0) goto L_0x0091
            boolean r0 = r7.b     // Catch:{ Throwable -> 0x0069 }
            if (r0 == 0) goto L_0x0091
            java.util.Vector<java.lang.String> r0 = r7.f     // Catch:{ Throwable -> 0x0069 }
            r2 = 0
            java.lang.Object r0 = r0.get(r2)     // Catch:{ Throwable -> 0x0069 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Throwable -> 0x0069 }
            if (r0 == 0) goto L_0x0062
            int r2 = r7.e     // Catch:{ Throwable -> 0x0086 }
            com.qq.m.h r3 = r7.f317a     // Catch:{ Throwable -> 0x0086 }
            long r3 = r3.f     // Catch:{ Throwable -> 0x0086 }
            com.qq.m.j r0 = com.qq.m.h.a(r1, r0, r2, r3)     // Catch:{ Throwable -> 0x0086 }
            if (r0 == 0) goto L_0x0062
            com.qq.m.h r2 = r7.f317a     // Catch:{ Throwable -> 0x0086 }
            long r2 = r2.f     // Catch:{ Throwable -> 0x0086 }
            r4 = 0
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 == 0) goto L_0x0062
            com.qq.provider.h.a(r0)     // Catch:{ Throwable -> 0x0086 }
        L_0x0062:
            java.util.Vector<java.lang.String> r0 = r7.f     // Catch:{ Throwable -> 0x0069 }
            r2 = 0
            r0.remove(r2)     // Catch:{ Throwable -> 0x0069 }
            goto L_0x002e
        L_0x0069:
            r0 = move-exception
            r0.printStackTrace()
        L_0x006d:
            com.qq.m.h r0 = r7.f317a
            long r0 = r0.f
            com.qq.provider.h.a(r6, r0)
            java.lang.String r0 = "com.qq.connect"
            java.lang.String r1 = "ScanThread parser  over!"
            android.util.Log.d(r0, r1)
            goto L_0x000f
        L_0x007e:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x0083 }
            goto L_0x002d
        L_0x0083:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0083 }
            throw r0     // Catch:{ Throwable -> 0x0069 }
        L_0x0086:
            r0 = move-exception
            java.lang.String r2 = "com.qq.connect"
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x0069 }
            android.util.Log.w(r2, r0)     // Catch:{ Throwable -> 0x0069 }
            goto L_0x0062
        L_0x0091:
            java.lang.String r0 = "com.qq.connect"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0069 }
            r2.<init>()     // Catch:{ Throwable -> 0x0069 }
            java.lang.String r3 = " tasks = "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x0069 }
            java.util.Vector<java.lang.String> r3 = r7.f     // Catch:{ Throwable -> 0x0069 }
            int r3 = r3.size()     // Catch:{ Throwable -> 0x0069 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x0069 }
            java.lang.String r3 = " isRunning :"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x0069 }
            boolean r3 = r7.b     // Catch:{ Throwable -> 0x0069 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x0069 }
            java.lang.String r3 = " scanover:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x0069 }
            boolean r3 = r7.d     // Catch:{ Throwable -> 0x0069 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x0069 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x0069 }
            android.util.Log.w(r0, r2)     // Catch:{ Throwable -> 0x0069 }
            java.util.Vector<java.lang.String> r0 = r7.f     // Catch:{ Throwable -> 0x0069 }
            int r0 = r0.size()     // Catch:{ Throwable -> 0x0069 }
            if (r0 != 0) goto L_0x0013
            boolean r0 = r7.d     // Catch:{ Throwable -> 0x0069 }
            if (r0 == 0) goto L_0x0010
            goto L_0x006d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.m.i.run():void");
    }

    public void a(String str) {
        if (str != null) {
            this.c = true;
            this.f.add(str);
            synchronized (this.f) {
                this.f.notifyAll();
            }
        }
    }

    public void a() {
        this.d = true;
        this.c = false;
        synchronized (this.f) {
            this.f.notifyAll();
        }
    }
}
