package com.qq.l;

import java.util.List;
import java.util.Vector;

/* compiled from: ProGuard */
public class p extends a {

    /* renamed from: a  reason: collision with root package name */
    private static p f311a = null;
    private List<d> b;
    private o c;
    private int d;
    private int e;
    private int f;
    private String g;
    private n h;
    private String i;
    private int j;

    public static synchronized p m() {
        p pVar;
        synchronized (p.class) {
            if (f311a == null) {
                f311a = new p();
            }
            pVar = f311a;
        }
        return pVar;
    }

    private p() {
        this.b = null;
        this.d = -1;
        this.e = -1;
        this.f = -1;
        this.j = 0;
        this.b = new Vector();
    }

    public void a(int i2, int i3, int i4) {
        this.d = i2;
        this.e = i3;
        this.f = i4;
    }

    public void a(int i2) {
        this.f = i2;
    }

    public void n() {
        this.d = -1;
        this.e = -1;
        this.f = -1;
    }

    public void a(String str) {
        this.g = str;
    }

    public void o() {
        this.g = null;
    }

    public void a(n nVar) {
        this.h = nVar;
    }

    public n p() {
        if (this.h == null) {
            this.h = new k();
        }
        return this.h;
    }

    public void q() {
        this.h = null;
    }

    public void b(String str) {
        this.i = str;
    }

    public void b(int i2) {
        this.j = i2;
    }

    public synchronized void b(int i2, int i3, int i4) {
        this.c = o.a();
        this.c.b(this.d);
        this.c.c(this.e);
        this.c.d(this.f);
        this.c.e(c());
        this.c.f(i2);
        this.c.g(i3);
        this.c.h(i4);
        this.c.a(System.currentTimeMillis());
        this.c.h(g());
        this.c.i(this.g);
        this.c.j(d() + "_" + e());
        this.c.e(a());
        this.c.f(e());
        this.c.g(f());
        this.c.c(b());
        this.c.b(i());
        this.c.d(j());
        this.c.c(h());
        this.c.a(true);
        this.c.a(this.j);
        this.b.add(new d("connection_total_st", this.c.b()));
        if (this.b.size() >= 8) {
            k().a(this.b);
            this.b.clear();
        }
    }

    public synchronized void c(int i2, int i3, int i4) {
        if (this.c != null) {
            this.c.b(System.currentTimeMillis());
            this.c.b(this.d);
            this.c.c(this.e);
            this.c.d(this.f);
            this.c.f(i2);
            this.c.g(i3);
            this.c.h(i4);
            this.c.a(false);
            this.c.a(this.i);
            this.i = null;
            this.b.add(new d("connection_total_st", this.c.b()));
            if (this.b.size() >= 8) {
                k().a(this.b);
                this.b.clear();
            }
            this.c = null;
            this.j = 0;
        }
    }

    public void l() {
        synchronized (this) {
            if (!this.b.isEmpty()) {
                k().a(this.b);
                this.b.clear();
            }
        }
    }
}
