package com.qq.l;

import com.qq.AppService.AppService;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class o {
    private final String A = "null";
    private boolean B = true;
    private String C;
    private int D = 0;

    /* renamed from: a  reason: collision with root package name */
    private final int f310a = 1;
    private int b = -1;
    private int c = -1;
    private int d = -1;
    private int e = -1;
    private int f;
    private final String g = "null";
    private int h;
    private int i = -1;
    private final String j = "null";
    private long k = 0;
    private long l;
    private long m;
    private String n;
    private String o;
    private String p;
    private String q;
    private String r;
    private String s;
    private String t = "null";
    private String u = "null";
    private String v = "null";
    private final String w = "null";
    private final String x = "null";
    private final String y = "null";
    private long z;

    public void a(int i2) {
        this.D = i2;
    }

    public void a(String str) {
        this.C = str;
    }

    public void b(String str) {
        this.t = str;
    }

    public void c(String str) {
        this.u = str;
    }

    public void d(String str) {
        this.v = str;
    }

    public void a(boolean z2) {
        this.B = z2;
    }

    public static o a() {
        return new o();
    }

    public void b(int i2) {
        this.b = i2;
    }

    public void c(int i2) {
        this.c = i2;
    }

    public void d(int i2) {
        this.d = i2;
    }

    public void e(int i2) {
        this.e = i2;
    }

    public void f(int i2) {
        this.f = i2;
    }

    public void g(int i2) {
        this.h = i2;
    }

    public void h(int i2) {
        this.i = i2;
    }

    public void a(long j2) {
        this.l = j2;
    }

    public void b(long j2) {
        this.m = j2;
        if (this.m >= this.l) {
            this.k = this.m - this.l;
        } else {
            this.k = 0;
        }
    }

    public void e(String str) {
        this.q = str;
    }

    public void c(long j2) {
        this.z = j2;
    }

    public void f(String str) {
        this.r = str;
    }

    public void g(String str) {
        this.s = str;
    }

    public void h(String str) {
        this.n = str;
    }

    public void i(String str) {
        this.o = str;
    }

    public void j(String str) {
        this.p = str;
    }

    public String b() {
        StringBuilder sb = new StringBuilder();
        sb.append(1).append("\t");
        sb.append(this.b == -1 ? "null" : Integer.valueOf(this.b)).append("\t");
        sb.append(this.c == -1 ? "null" : Integer.valueOf(this.c)).append("\t");
        sb.append(this.d == -1 ? "null" : Integer.valueOf(this.d)).append("\t");
        sb.append(this.e).append("\t");
        sb.append(this.f).append("\t");
        sb.append("null").append("\t");
        sb.append(this.h).append("\t");
        sb.append(this.i == -1 ? "null" : Integer.valueOf(this.i)).append("\t");
        sb.append("null").append("\t");
        sb.append(this.k).append("\t");
        sb.append(this.n).append("\t");
        sb.append(this.o == null ? "null" : this.o).append("\t");
        sb.append(this.p).append("\t");
        sb.append(this.q).append("\t");
        sb.append(this.r).append("\t");
        sb.append(this.s).append("\t");
        sb.append(this.t).append("\t");
        sb.append(this.u).append("\t");
        sb.append(this.v).append("\t");
        sb.append("null").append("\t");
        sb.append("null").append("\t");
        sb.append("null").append("\t");
        if (this.B) {
            sb.append(AppService.M() + Constants.STR_EMPTY).append("\t");
        } else if (this.f == 511 || this.f == 607 || this.f == 706) {
            sb.append(AppService.P() + Constants.STR_EMPTY).append("\t");
        } else {
            sb.append(AppService.M() + Constants.STR_EMPTY).append("\t");
        }
        sb.append(this.z).append("\t");
        sb.append("null").append("\t");
        sb.append(this.C == null ? "null" : this.C).append("\t");
        sb.append(this.D).append("\t");
        sb.append(0);
        return sb.toString();
    }

    public String toString() {
        return "TotalConnectionItemTpl [type=" + this.b + ", subType=" + this.c + ", wsubType=" + this.d + ", networkType=" + this.e + ", step=" + this.f + ", stepStatus=" + this.h + ", reason=" + this.i + ", cost=" + this.k + ", version=" + this.n + ", sessionId=" + this.o + ", deviceId=" + this.p + ", deviceOs=" + this.q + ", imei=" + this.r + ", imsi=" + this.s + ", deviceStorage=" + this.z + "]";
    }
}
