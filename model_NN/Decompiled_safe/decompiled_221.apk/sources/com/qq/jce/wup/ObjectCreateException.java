package com.qq.jce.wup;

public class ObjectCreateException extends RuntimeException {
    public ObjectCreateException(Exception ex) {
        super(ex);
    }
}
