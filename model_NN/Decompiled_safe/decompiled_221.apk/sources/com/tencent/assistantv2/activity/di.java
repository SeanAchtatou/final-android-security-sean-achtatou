package com.tencent.assistantv2.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.link.b;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class di implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchActivity f2832a;

    di(SearchActivity searchActivity) {
        this.f2832a = searchActivity;
    }

    public void onClick(View view) {
        String scheme;
        String str = (String) view.getTag(R.id.search_hot_word_more_url);
        XLog.d("SearchActivity", str);
        if (!TextUtils.isEmpty(str) && (scheme = new Intent("android.intent.action.VIEW", Uri.parse(str)).getScheme()) != null) {
            if (scheme.equals("tmast") || scheme.equals("http")) {
                this.f2832a.n = true;
                Bundle bundle = new Bundle();
                bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.f2832a.f());
                b.b(AstApp.i(), str, bundle);
            }
        }
    }
}
