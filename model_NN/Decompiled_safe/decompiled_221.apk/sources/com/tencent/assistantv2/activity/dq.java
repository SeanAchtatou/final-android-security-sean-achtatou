package com.tencent.assistantv2.activity;

import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class dq implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchActivity f2840a;

    dq(SearchActivity searchActivity) {
        this.f2840a = searchActivity;
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void afterTextChanged(Editable editable) {
        String unused = this.f2840a.M = (String) null;
        String unused2 = this.f2840a.N = (String) null;
        this.f2840a.y.d();
        this.f2840a.y.a(editable.toString());
        if (!TextUtils.isEmpty(editable.toString()) && TextUtils.isEmpty(editable.toString().trim())) {
            return;
        }
        if (editable.length() > 0) {
            if (this.f2840a.u != null) {
                this.f2840a.u.a(0);
            }
            if (!this.f2840a.n && this.f2840a.H != null) {
                Message obtainMessage = this.f2840a.H.obtainMessage(0);
                obtainMessage.obj = editable;
                this.f2840a.H.removeMessages(0);
                this.f2840a.D.a(this.f2840a.F);
                this.f2840a.H.sendMessageDelayed(obtainMessage, 500);
            }
            this.f2840a.n = false;
        } else if (!this.f2840a.G) {
            this.f2840a.finish();
        } else {
            if (this.f2840a.u != null) {
                this.f2840a.u.a(8);
                this.f2840a.u.a(this.f2840a.getString(R.string.search_hint_default));
                String unused3 = this.f2840a.M = (String) null;
                String unused4 = this.f2840a.N = (String) null;
            }
            this.f2840a.B();
            this.f2840a.C();
            this.f2840a.H.removeMessages(0);
            this.f2840a.D.a(this.f2840a.F);
        }
    }
}
