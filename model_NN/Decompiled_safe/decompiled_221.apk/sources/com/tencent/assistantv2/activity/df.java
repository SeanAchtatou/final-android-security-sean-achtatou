package com.tencent.assistantv2.activity;

import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;

/* compiled from: ProGuard */
class df extends ApkResCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchActivity f2829a;

    df(SearchActivity searchActivity) {
        this.f2829a = searchActivity;
    }

    public void onInstalledApkDataChanged(LocalApkInfo localApkInfo, int i) {
        this.f2829a.runOnUiThread(new dg(this));
    }
}
