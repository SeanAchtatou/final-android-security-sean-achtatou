package com.tencent.assistantv2.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.text.TextUtils;
import com.tencent.assistant.protocol.jce.DesktopShortCut;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.thumbnailCache.o;
import com.tencent.assistant.thumbnailCache.p;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.open.SocialConstants;

/* compiled from: ProGuard */
class cn implements p {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MainActivity f2810a;

    cn(MainActivity mainActivity) {
        this.f2810a = mainActivity;
    }

    public void thumbnailRequestFailed(o oVar) {
    }

    public void thumbnailRequestCompleted(o oVar) {
        Bitmap bitmap = oVar.f;
        if (bitmap != null && !bitmap.isRecycled() && this.f2810a.Y != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.f2810a.Y.size()) {
                    DesktopShortCut desktopShortCut = (DesktopShortCut) this.f2810a.Y.get(i2);
                    if (oVar.c() != null && oVar.c().equals(desktopShortCut.a())) {
                        Intent intent = new Intent("com.tencent.assistant.SHORTCUT");
                        intent.setClassName("com.tencent.android.qqdownloader", "com.tencent.assistant.activity.ShortCutActivity");
                        intent.setFlags(67108864);
                        if (!TextUtils.isEmpty(desktopShortCut.c())) {
                            intent.putExtra("pkgName", desktopShortCut.c());
                            if (!TextUtils.isEmpty(desktopShortCut.d())) {
                                intent.putExtra("channelId", desktopShortCut.d());
                            }
                        } else if (!TextUtils.isEmpty(desktopShortCut.e().a())) {
                            intent.putExtra(SocialConstants.PARAM_URL, desktopShortCut.e().a());
                        }
                        this.f2810a.a(ThumbnailUtils.extractThumbnail(bitmap, df.b(48.0f), df.b(48.0f)), desktopShortCut.b(), intent);
                        this.f2810a.sendBroadcast(intent);
                        k.a(new STInfoV2(204003, "03_001", 2000, STConst.ST_DEFAULT_SLOT, 100));
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }
}
