package com.tencent.assistantv2.activity;

import android.content.Context;
import android.os.Bundle;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.module.k;
import com.tencent.assistantv2.adapter.RankNormalListAdapter;
import com.tencent.assistantv2.component.AppRankListPage;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;

/* compiled from: ProGuard */
public class AppRankActivity extends BaseActivity {
    private Context n;
    private SecondNavigationTitleViewV5 t;
    private AppRankListPage u;
    private RankNormalListAdapter v;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.n = this;
        setContentView((int) R.layout.activity_apprank);
        i();
        j();
    }

    private void i() {
        this.t = (SecondNavigationTitleViewV5) findViewById(R.id.apprank_title);
        this.t.d(false);
        this.t.a(this);
        this.t.b(getString(R.string.apprank_title));
        this.t.i();
        this.u = (AppRankListPage) findViewById(R.id.apprank_listpage);
    }

    private void j() {
        k kVar = new k(-1, 2, 30);
        this.v = new RankNormalListAdapter(this.n, this.u, kVar.a());
        this.v.a(RankNormalListAdapter.ListType.LISTTYPEGAMESORT);
        this.v.a(false);
        this.v.a(f(), -100);
        this.u.a(kVar);
        this.u.a(this.v);
        this.u.b();
        this.v.c();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.v != null) {
            this.v.b();
        }
        this.t.m();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.v != null) {
            this.v.c();
        }
        this.t.l();
    }

    public int f() {
        return 200502;
    }
}
