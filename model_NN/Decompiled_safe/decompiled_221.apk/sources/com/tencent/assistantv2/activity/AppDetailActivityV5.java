package com.tencent.assistantv2.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.ShareBaseActivity;
import com.tencent.assistant.adapter.an;
import com.tencent.assistant.component.CommentDetailView;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.PopViewDialog;
import com.tencent.assistant.component.TotalTabLayout;
import com.tencent.assistant.component.appdetail.AppBarTabView;
import com.tencent.assistant.component.appdetail.AppdetailDownloadBar;
import com.tencent.assistant.component.appdetail.AppdetailFloatingDialog;
import com.tencent.assistant.component.appdetail.AppdetailScrollView;
import com.tencent.assistant.component.appdetail.AppdetailViewPager;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.assistant.component.appdetail.CustomLinearLayout;
import com.tencent.assistant.component.appdetail.HorizonScrollPicViewer;
import com.tencent.assistant.component.appdetail.process.AppdetailActionUIListener;
import com.tencent.assistant.component.appdetail.process.a;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewPageScrollListener;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.g.e;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.login.d;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.manager.be;
import com.tencent.assistant.model.ShareAppModel;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.c;
import com.tencent.assistant.module.cq;
import com.tencent.assistant.module.ex;
import com.tencent.assistant.module.h;
import com.tencent.assistant.module.t;
import com.tencent.assistant.module.u;
import com.tencent.assistant.protocol.jce.AppDetailWithComment;
import com.tencent.assistant.protocol.jce.CommentTagInfo;
import com.tencent.assistant.protocol.jce.GetRecommendAppListResponse;
import com.tencent.assistant.protocol.jce.InstalledAppItem;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.ct;
import com.tencent.assistant.utils.df;
import com.tencent.assistant.utils.i;
import com.tencent.assistantv2.b.j;
import com.tencent.assistantv2.b.m;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.component.appdetail.AppDetailHeaderViewV5;
import com.tencent.assistantv2.component.appdetail.AppDetailLinearLayoutV5;
import com.tencent.assistantv2.component.appdetail.AppDetailViewV5;
import com.tencent.assistantv2.component.appdetail.AppdetailRelatedViewV5;
import com.tencent.assistantv2.component.appdetail.AppdetailTabViewV5;
import com.tencent.assistantv2.component.appdetail.b;
import com.tencent.assistantv2.component.appdetail.y;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.model.STCommonInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
public class AppDetailActivityV5 extends ShareBaseActivity implements PopViewDialog.IPopViewSaveParams, UIEventListener {
    public static ArrayList<String> w = new ArrayList<>();
    /* access modifiers changed from: private */
    public AppdetailViewPager A;
    private an B;
    private List<View> C = new ArrayList();
    private AppdetailScrollView D;
    /* access modifiers changed from: private */
    public AppDetailLinearLayoutV5 E;
    /* access modifiers changed from: private */
    public AppDetailHeaderViewV5 F;
    /* access modifiers changed from: private */
    public SecondNavigationTitleViewV5 G;
    /* access modifiers changed from: private */
    public AppdetailTabViewV5 H;
    /* access modifiers changed from: private */
    public AppdetailRelatedViewV5 I;
    private GetRecommendAppListResponse J;
    private cq K = new cq();
    /* access modifiers changed from: private */
    public CommentDetailTabView L;
    /* access modifiers changed from: private */
    public boolean M = false;
    /* access modifiers changed from: private */
    public boolean N = false;
    private CustomLinearLayout O;
    /* access modifiers changed from: private */
    public TotalTabLayout P;
    private int[] Q = {R.string.title_app_detail, R.string.title_user_comment};
    /* access modifiers changed from: private */
    public int R = 0;
    private RelativeLayout S;
    private LoadingView T;
    private NormalErrorRecommendPage U;
    private ViewStub V;
    private ViewStub W;
    /* access modifiers changed from: private */
    public AppdetailDownloadBar X;
    private boolean Y = false;
    /* access modifiers changed from: private */
    public h Z = new h();
    private final int aA = 9;
    private final String aB = "authorModel";
    private final String aC = "hasNext";
    private final String aD = "pageContext";
    private final String aE = "pageText";
    private final String aF = "same_tag_app";
    private final String aG = "selectedCommentList";
    private final String aH = "goodTagList";
    private final String aI = "allTagList";
    private String aJ;
    private boolean aK = false;
    /* access modifiers changed from: private */
    public byte aL = 0;
    /* access modifiers changed from: private */
    public String aM = Constants.STR_EMPTY;
    /* access modifiers changed from: private */
    public String aN;
    /* access modifiers changed from: private */
    public byte aO = 0;
    /* access modifiers changed from: private */
    public byte aP = 0;
    /* access modifiers changed from: private */
    public LocalApkInfo aQ = null;
    private Bundle aR;
    private a aS;
    /* access modifiers changed from: private */
    public AppdetailActionUIListener aT;
    /* access modifiers changed from: private */
    public TextView aU = null;
    /* access modifiers changed from: private */
    public int aV = 0;
    /* access modifiers changed from: private */
    public boolean aW = false;
    private j aX = new j();
    private m aY = new m();
    private long aZ;
    private ex aa = new ex();
    private t ab = new t(this, null);
    /* access modifiers changed from: private */
    public SimpleAppModel ac;
    /* access modifiers changed from: private */
    public ShareAppModel ad;
    /* access modifiers changed from: private */
    public c ae;
    /* access modifiers changed from: private */
    public PopViewDialog af;
    private final int ag = 3;
    /* access modifiers changed from: private */
    public int ah = 3;
    private final int ai = 0;
    private final int aj = 1;
    private final int ak = 2;
    /* access modifiers changed from: private */
    public String al;
    /* access modifiers changed from: private */
    public int am;
    /* access modifiers changed from: private */
    public int an;
    /* access modifiers changed from: private */
    public boolean ao = true;
    private STCommonInfo ap = null;
    /* access modifiers changed from: private */
    public STInfoV2 aq = null;
    /* access modifiers changed from: private */
    public boolean ar = false;
    /* access modifiers changed from: private */
    public boolean as = false;
    private final int at = 1;
    private final int au = 2;
    private final int av = 3;
    private final int aw = 4;
    private final int ax = 5;
    private final int ay = 6;
    private final int az = 7;
    private boolean ba = false;
    /* access modifiers changed from: private */
    public ViewPageScrollListener bb = new l(this);
    /* access modifiers changed from: private */
    public y bc = new m(this);
    private View.OnClickListener bd = new p(this);
    private View.OnClickListener be = new q(this);
    private b bf = new r(this);
    private AppdetailFloatingDialog.IOnFloatViewListener bg = new s(this);
    private CommentDetailView.CommentSucceedListener bh = new c(this);
    /* access modifiers changed from: private */
    public Handler bi = new e(this);
    private HorizonScrollPicViewer.IShowPictureListener bj = new f(this);
    private AppdetailActionUIListener bk = new g(this);
    private com.tencent.assistant.component.appdetail.process.m bl = new h(this);
    /* access modifiers changed from: private */
    public DialogInterface.OnCancelListener bm = new i(this);
    protected AppBarTabView n;
    protected int t;
    ViewInvalidateMessage[] u = new ViewInvalidateMessage[9];
    int[] v = new int[9];
    public ArrayList<CommentTagInfo> x = new ArrayList<>();
    public u y = new d(this);
    /* access modifiers changed from: private */
    public Context z;

    static {
        w.add(com.tencent.assistant.b.a.f845a);
        w.add(com.tencent.assistant.b.a.b);
        w.add(com.tencent.assistant.b.a.d);
        w.add(com.tencent.assistant.b.a.e);
    }

    public int f() {
        if (this.R == 0) {
            return STConst.ST_PAGE_APP_DETAIL;
        }
        if (this.R == 1) {
            return STConst.ST_PAGE_APP_DETAIL_COMMENT;
        }
        if (this.R == 2) {
            return STConst.ST_PAGE_APP_DETAIL_APPBAR;
        }
        return STConst.ST_PAGE_APP_DETAIL;
    }

    public boolean g() {
        return false;
    }

    public void b(boolean z2) {
        i();
        this.aq.isImmediately = com.tencent.assistant.st.h.a(this.ac.au);
        this.aq.actionId = 100;
        if (z2) {
            this.aq.isImmediately = false;
        }
        k.a(this.aq);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        w();
        try {
            super.onCreate(bundle);
            O();
            setContentView((int) R.layout.activity_appdetail_layout_v5);
            this.z = this;
            A();
            z();
            N();
        } catch (Throwable th) {
            this.ba = true;
            com.tencent.assistant.manager.cq.a().b();
            finish();
        }
    }

    private void w() {
        x();
        if (this.ac == null) {
            finish();
            return;
        }
        this.Z.register(this.ab);
        this.aa.register(this.ab);
        this.K.register(this.ab);
        this.aX.register(this.ab);
        this.aY.register(this.ab);
    }

    private void x() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.aR = extras;
            if (extras.get("st_common_data") instanceof STCommonInfo) {
                this.ap = (STCommonInfo) extras.get("st_common_data");
            }
            this.ao = extras.getBoolean("same_tag_app");
            this.aJ = extras.getString(com.tencent.assistant.b.a.p);
            this.aK = extras.getBoolean(com.tencent.assistant.b.a.t);
            this.R = ct.a(extras.getString(com.tencent.assistant.b.a.U), 0);
            this.aZ = ct.c(extras.getString(com.tencent.assistant.b.a.V));
            if (!this.M && this.R == 1) {
                M();
            }
            if (this.aK) {
                com.tencent.assistant.manager.b.a().a(this.aJ);
                this.aM = extras.getString(com.tencent.assistant.b.a.I);
            }
            y();
            this.ac = (SimpleAppModel) extras.get(CommentDetailTabView.PARAMS_SIMPLE_MODEL_INFO);
            if (this.ac == null) {
                String string = extras.getString(com.tencent.assistant.b.a.C);
                int d = ct.d(extras.getString(com.tencent.assistant.b.a.J));
                String string2 = extras.getString(com.tencent.assistant.b.a.s);
                if (!TextUtils.isEmpty(string2)) {
                    try {
                        this.aL = Byte.valueOf(string2).byteValue();
                    } catch (Exception e) {
                    }
                }
                String string3 = extras.getString(com.tencent.assistant.b.a.h);
                String string4 = extras.getString("com.tencent.assistant.PACKAGE_NAME");
                if (!TextUtils.isEmpty(string4)) {
                    if (this.ac == null) {
                        this.ac = new SimpleAppModel();
                        this.ac.c = string4;
                        this.ac.y = b(extras.getString(com.tencent.assistant.b.a.W));
                    }
                    if (!TextUtils.isEmpty(string3)) {
                        try {
                            this.ac.g = Integer.valueOf(string3).intValue();
                        } catch (Exception e2) {
                        }
                    }
                    String string5 = extras.getString("com.tencent.assistant.APK_ID");
                    if (!TextUtils.isEmpty(string5)) {
                        try {
                            this.ac.b = Long.valueOf(string5).longValue();
                        } catch (Exception e3) {
                        }
                    }
                    if (!"ANDROIDQQ".equals(string)) {
                        this.ac.ac = string;
                    }
                    this.ac.ad = d;
                    return;
                }
                long j = extras.getLong("com.tencent.assistant.APP_ID", -1);
                if (j > 0) {
                    if (this.ac == null) {
                        this.ac = new SimpleAppModel();
                        this.ac.f1634a = j;
                        this.ac.y = b(extras.getString(com.tencent.assistant.b.a.W));
                    }
                    if (!TextUtils.isEmpty(string3)) {
                        try {
                            this.ac.g = Integer.valueOf(string3).intValue();
                        } catch (Exception e4) {
                        }
                    }
                    String string6 = extras.getString("com.tencent.assistant.APK_ID");
                    if (!TextUtils.isEmpty(string6)) {
                        try {
                            this.ac.b = Long.valueOf(string6).longValue();
                        } catch (Exception e5) {
                        }
                    }
                    if (!"ANDROIDQQ".equals(string)) {
                        this.ac.ac = string;
                    }
                    this.ac.ad = d;
                }
            } else if (this.ac.d()) {
                this.aL = 1;
            } else if (this.ac.e()) {
                this.aL = 2;
            } else {
                this.aL = this.ac.Q;
            }
        }
    }

    private void y() {
        String string = this.aR.getString(com.tencent.assistant.b.a.g);
        String string2 = this.aR.getString(com.tencent.assistant.b.a.s);
        if (this.aK || !TextUtils.isEmpty(string) || !TextUtils.isEmpty(string2)) {
            String string3 = this.aR.getString(com.tencent.assistant.b.a.A);
            if (!TextUtils.isEmpty(string3)) {
                this.aN = string3;
                this.aO = 1;
                this.aP = 1;
                return;
            }
            String string4 = this.aR.getString(com.tencent.assistant.b.a.B);
            if (!TextUtils.isEmpty(string4)) {
                this.aN = string4;
                this.aO = 2;
                this.aP = 1;
                return;
            }
            String string5 = this.aR.getString(com.tencent.assistant.b.a.y);
            String string6 = this.aR.getString(com.tencent.assistant.b.a.z);
            if (!TextUtils.isEmpty(string5) && !TextUtils.isEmpty(string6)) {
                this.aN = string5;
                if ("game_openId".equals(string6)) {
                    this.aO = 5;
                    byte b = 0;
                    if (!TextUtils.isEmpty(string)) {
                        try {
                            b = Byte.valueOf(string).byteValue();
                        } catch (Exception e) {
                        }
                    }
                    if ((b & 1) > 0) {
                        this.aP = 1;
                        return;
                    } else if ((b & 2) > 0) {
                        this.aP = 2;
                        return;
                    } else {
                        return;
                    }
                } else if ("code".equals(string6)) {
                    this.aO = 1;
                    this.aP = 1;
                    return;
                } else if ("qqNumber".equals(string6)) {
                    this.aO = 3;
                    this.aP = 2;
                    return;
                }
            }
        }
        if (!d.a().j()) {
            return;
        }
        if (d.a().k()) {
            this.aO = 3;
            this.aP = 2;
        } else if (d.a().l()) {
            this.aO = 6;
            this.aP = 1;
        }
    }

    private void z() {
        if (R()) {
            TemporaryThreadManager.get().start(new b(this));
        } else {
            TemporaryThreadManager.get().start(new j(this));
        }
    }

    private void A() {
        if (this.ac != null) {
            this.E = (AppDetailLinearLayoutV5) findViewById(R.id.appdetail_container);
            this.E.a(this.bf);
            this.S = (RelativeLayout) findViewById(R.id.content_view);
            this.W = (ViewStub) findViewById(R.id.loading_stub);
            this.T = (LoadingView) findViewById(R.id.loading_view);
            this.V = (ViewStub) findViewById(R.id.error_page_stub);
            this.U = (NormalErrorRecommendPage) findViewById(R.id.error_page);
            B();
            C();
            this.D = (AppdetailScrollView) findViewById(R.id.parent_scrollview);
            G();
            H();
            this.D.setInnerScrollListener(this.A.getInnerScrollView());
            if (a(this.ae)) {
                c(true);
            } else {
                c(false);
            }
            this.aS = new a();
            this.aS.a(this.bk);
            this.aU = (TextView) findViewById(R.id.bubble);
            this.aU.setVisibility(8);
            this.E.a(new k(this));
            I();
        }
    }

    private void B() {
        this.G = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        String string = this.aR.getString(com.tencent.assistant.b.a.D);
        if (!this.aK || !TextUtils.isEmpty(string)) {
            this.G.b(getResources().getString(R.string.detail_title));
        } else {
            this.G.a(getResources().getString(R.string.detail_title));
        }
        this.G.a(this.bg);
        if (a(this.ae)) {
            this.G.a(this.ac, i(), this.ae);
            if (this.ac != null && be.a().c(this.ac.c)) {
                this.G.f();
            }
        }
        this.G.a(this);
        this.G.e();
        this.G.i();
        this.G.a();
    }

    private void C() {
        this.F = (AppDetailHeaderViewV5) findViewById(R.id.simple_msg_view);
        if (a(this.ae)) {
            this.F.a(this.ac, i(), this.ae.f1660a.f1993a.b.get(0).b, this.ae.f1660a.f1993a.f1989a.f, this.ae.f1660a.f1993a.f1989a.e);
        }
    }

    private void D() {
        this.H = new AppdetailTabViewV5(this);
        if (this.ae != null) {
            this.H.a(this.ae, this.ac);
            this.H.a(this.A.getPicViewerListener());
            this.H.a(this.bf);
            this.H.a(this.bj);
            this.A.setPicViewer(this.H.f());
        }
        this.I = this.H.a();
        this.L = new CommentDetailTabView(this, this.bh);
        this.C.add(this.H);
        this.C.add(this.L);
    }

    private void F() {
        this.n = new AppBarTabView(this);
        this.C.add(this.n);
        if (this.B != null) {
            this.B.a(this.C);
            this.B.notifyDataSetChanged();
        }
        this.Q = new int[]{R.string.title_app_detail, R.string.title_user_comment, R.string.title_app_bar};
        if (this.P != null) {
            this.O.removeAllViews();
            G();
        }
    }

    private void G() {
        this.O = (CustomLinearLayout) findViewById(R.id.tab_view);
        this.P = new TotalTabLayout(this, this.Q);
        this.P.getTabLayout().setLayoutParams(new ViewGroup.LayoutParams(-1, df.b(38.0f)));
        this.O.addView(this.P.getTabLayout());
        this.P.init(this.R);
        this.P.setTabClickListener(this.bd);
        if (this.ae != null) {
            this.P.updateTextValue(getResources().getString(R.string.title_user_comment), "评论(" + ct.a(this.ae.f1660a.b) + ")");
        }
    }

    private void H() {
        D();
        this.B = new an(this.C);
        this.A = (AppdetailViewPager) findViewById(R.id.appdetail_viewpager);
        this.A.setAdapter(this.B);
        this.A.setCurrentItem(this.R);
        this.A.setOnPageChangeListener(this.bb);
        this.L.setPageChangeListener(this.bb);
    }

    private void I() {
        this.X = (AppdetailDownloadBar) findViewById(R.id.floating_layout);
        this.af = new PopViewDialog(this.z, R.style.dialog, this.ac.c, this.L);
        this.af.setShareEngine(E());
        this.af.registerOnGetCommentListReqStart(this.y);
        K();
        c(1);
        this.aT = this.X.getUiListener();
    }

    /* access modifiers changed from: private */
    public void c(int i) {
        switch (i) {
            case 1:
                this.X.setVisibility(0);
                this.X.setState(1);
                return;
            case 2:
                this.X.setVisibility(0);
                this.X.setState(2);
                return;
            case 3:
                this.X.setVisibility(8);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void c(boolean z2) {
        if (z2) {
            this.S.setVisibility(0);
            if (this.T != null) {
                this.T.setVisibility(8);
            }
            if (this.U != null) {
                this.U.setVisibility(8);
                return;
            }
            return;
        }
        this.S.setVisibility(4);
        if (this.T == null) {
            this.T = (LoadingView) this.W.inflate();
        } else {
            this.T.setVisibility(0);
        }
        if (this.U != null) {
            this.U.setVisibility(8);
        }
    }

    private void d(int i) {
        this.S.setVisibility(4);
        if (this.T != null) {
            this.T.setVisibility(8);
        }
        if (this.U == null) {
            this.U = (NormalErrorRecommendPage) this.V.inflate();
            this.U.setButtonClickListener(this.be);
        } else {
            this.U.setVisibility(0);
        }
        this.U.setErrorType(i);
    }

    /* access modifiers changed from: private */
    public void J() {
        if (a(this.ae)) {
            c(true);
            if (this.ac != null) {
                if (this.ae != null) {
                    this.P.updateTextValue(getResources().getString(R.string.title_user_comment), "评论(" + ct.a(this.ae.f1660a.b) + ")");
                }
                this.ac.a(this.ae.b);
                this.ac.a(this.ae.f1660a);
                P();
                String string = this.aR.getString(com.tencent.assistant.b.a.D);
                if (!this.aK || !TextUtils.isEmpty(string)) {
                    this.G.b(getResources().getString(R.string.detail_title));
                } else {
                    this.G.a(getResources().getString(R.string.detail_title));
                }
                if (!R()) {
                    u.b(this.ac);
                } else if (!u.a(this.ae.f1660a.d) || !(this.ac.b == -99 || this.ac.b == this.ae.f1660a.d.r)) {
                    u.b(this.ac);
                } else {
                    u.a(this.ae.f1660a.d, this.ac);
                }
                this.ac.af = this.aM;
                this.ac.q = this.ae.f1660a.f1993a.f1989a.h.b;
                this.F.a(this.ac, i(), this.ae.f1660a.f1993a.b.get(0).b, this.ae.f1660a.f1993a.f1989a.f, this.ae.f1660a.f1993a.f1989a.e);
                this.G.a(this.ac, i(), this.ae);
                if (this.ac != null && be.a().c(this.ac.c)) {
                    this.G.f();
                }
                this.H.a(this.ae, this.ac);
                this.H.a(this.A.getPicViewerListener());
                this.H.a(this.bf);
                this.H.a(this.bj);
                this.A.setPicViewer(this.H.f());
                K();
                this.G.b(this.ac.g());
                if (!TextUtils.isEmpty(this.ae.f1660a.g)) {
                    F();
                }
            }
        }
    }

    private void K() {
        if (this.ae != null) {
            long j = this.ae.f1660a.f1993a.b.get(0).f1982a;
            boolean z2 = this.ae.f1660a.c;
            this.af.setVersion(this.ac.g, this.ac.f);
            this.af.setPkgName(this.ae.b());
            this.aS.a(this.ac, j, z2, this.af, i(), this.aR, this.aK, this.ae.f1660a.e, this.z, this.ae.f1660a.f, this.bl);
            this.aS.a(a(this.ae, this.ac));
            this.X.setSimpleAppModel(this.aS);
            this.X.mstate = this.R + 1;
            this.X.showType = this.aV;
            if (!this.Y) {
                this.Y = true;
                this.X.onCreate();
                this.X.setMyScrolView(this.H.c(), this.D, this.H.b(), AppDetailViewV5.a(this.ae, this.ac) == AppDetailViewV5.APPDETAIL_MODE.NEED_UPDATE);
                String string = this.aR.getString(com.tencent.assistant.b.a.e);
                if (!TextUtils.isEmpty(string)) {
                    String[] split = string.split(";");
                    ArrayList arrayList = new ArrayList();
                    for (String add : split) {
                        arrayList.add(add);
                    }
                    if (arrayList.contains("1") && this.aV != 3) {
                        this.X.scrolViewToBottom(true);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public ShareAppModel a(c cVar, SimpleAppModel simpleAppModel) {
        if (cVar == null) {
            return null;
        }
        if (this.ad == null) {
            this.ad = new ShareAppModel();
        }
        this.ad.f1632a = cVar.f1660a.f1993a.f1989a.e;
        this.ad.c = j();
        this.ad.d = this.aL;
        this.ad.e = simpleAppModel.f1634a;
        this.ad.g = simpleAppModel.d;
        this.ad.h = simpleAppModel.q;
        this.ad.i = simpleAppModel.p;
        this.ad.j = simpleAppModel.k;
        this.ad.f = simpleAppModel.e;
        if (!TextUtils.isEmpty(simpleAppModel.ai)) {
            this.ad.l = simpleAppModel.ai;
        } else {
            this.ad.l = cVar.f1660a.i;
        }
        if (!TextUtils.isEmpty(simpleAppModel.aj)) {
            this.ad.m = simpleAppModel.aj;
        } else {
            this.ad.m = cVar.f1660a.j;
        }
        return this.ad;
    }

    private void L() {
        this.I = this.H.a();
        this.I.a(this.J);
    }

    /* access modifiers changed from: private */
    public void M() {
        if (a(this.ae) && !this.M) {
            HashMap hashMap = new HashMap();
            hashMap.put(CommentDetailTabView.PARAMS_SIMPLE_MODEL_INFO, this.ac);
            hashMap.put(CommentDetailTabView.PARAMS_RATING_INFO, this.ae.f1660a.f1993a.f1989a.h);
            hashMap.put(CommentDetailTabView.PARAMS_APK_ID, Long.valueOf(this.ae.f1660a.f1993a.b.get(0).f1982a));
            hashMap.put(CommentDetailTabView.PARAMS_COMMENT_COUNT, Long.valueOf(this.ae.f1660a.b));
            hashMap.put(CommentDetailTabView.PARAMS_VERSION_CODE, Integer.valueOf(this.ae.f1660a.f1993a.b.get(0).c));
            hashMap.put(CommentDetailTabView.PARAMS_REPLY_ID, Long.valueOf(this.aZ));
            hashMap.put(CommentDetailTabView.PARAMS_ALLTAG_LIST, this.x);
            this.L.initDetailView(hashMap);
            this.M = true;
        }
    }

    /* access modifiers changed from: private */
    public void N() {
        if (this.ac != null) {
            InstalledAppItem installedAppItem = new InstalledAppItem();
            installedAppItem.b = this.ac.f1634a;
            if (this.ac.c == null) {
                installedAppItem.f2221a = Constants.STR_EMPTY;
            } else {
                installedAppItem.f2221a = this.ac.c;
            }
            this.v[1] = this.aa.a(1, installedAppItem, null, null, (byte) 0, Constants.STR_EMPTY);
            this.v[2] = this.K.a(this.ac.c);
            this.v[6] = this.aX.a(this.ac);
            this.v[7] = this.aY.a(this.ac, d.a().j(), this.ac.f1634a, d.a().n());
        }
    }

    /* access modifiers changed from: private */
    public void a(int i, int i2, c cVar, int i3) {
        t a2;
        if (this.v[3] != i) {
            e(10);
        } else if (i2 == 0) {
            if (cVar == null || !a(cVar)) {
                String string = this.aR.getString(com.tencent.assistant.b.a.D);
                if (!this.aK || !TextUtils.isEmpty(string)) {
                    d(80);
                } else {
                    e(10);
                    d(70);
                }
                this.U.setActivityPageId(STConst.ST_PAGE_APP_DETAIL_APP_NOT_EXIST);
            } else {
                this.aV = i3;
                this.ae = cVar;
                J();
                if (!this.M && this.R == 1) {
                    M();
                }
                this.af.setShareAppModel(a(cVar, this.ac));
                this.I = this.H.a();
                if (this.I != null) {
                    this.I.a(i3);
                    this.I.a(cVar.f1660a.f1993a.f1989a.f);
                }
                b(false);
                if (AppDetailViewV5.a(cVar, this.ac) == AppDetailViewV5.APPDETAIL_MODE.NEED_UPDATE && cVar.f1660a.f1993a.b.get(0).F > 10000) {
                    this.F.a(String.format(getResources().getString(R.string.appdetail_friends_update), new BigDecimal(((double) cVar.f1660a.f1993a.b.get(0).F) / 10000.0d).setScale(1, 4)));
                }
                this.aW = true;
                if (this.u[2] != null) {
                    this.bb.sendMessage(this.u[2]);
                }
                if (this.u[1] != null) {
                    this.bb.sendMessage(this.u[1]);
                }
                if (this.u[4] != null) {
                    this.bb.sendMessage(this.u[4]);
                }
                if (this.u[5] != null) {
                    this.bb.sendMessage(this.u[5]);
                }
                if (this.u[6] != null) {
                    this.bb.sendMessage(this.u[6]);
                }
                if (!(this.u[7] == null || AppDetailViewV5.a(cVar, this.ac) == AppDetailViewV5.APPDETAIL_MODE.NEED_UPDATE)) {
                    this.bb.sendMessage(this.u[7]);
                }
            }
            this.ah = 3;
        } else if (-800 == i2) {
            d(30);
            this.U.setActivityPageId(this.U.checkNoNetworkExistInstallApps() ? STConst.ST_PAGE_APP_DETAIL_NO_NETWORK_TO_UPDATE : STConst.ST_PAGE_APP_DETAIL_NO_NETWORK_TO_MANAGER);
            e(10);
        } else if (this.ah <= 0) {
            d(20);
            e(10);
        } else {
            if (R()) {
                a2 = this.Z.a(this.ac, this.aQ, this.aL, this.aN, this.aO, this.aP);
            } else {
                a2 = this.Z.a(this.ac, this.aL, this.aN, this.aO, this.aP);
            }
            this.v[3] = a2.b;
            this.ah--;
        }
    }

    private void O() {
        this.aq = STInfoBuilder.buildSTInfo(this, this.ac, STConst.ST_DEFAULT_SLOT, 100, null);
        if (this.aq == null) {
            this.aq = new STInfoV2(f(), STConst.ST_DEFAULT_SLOT, p(), STConst.ST_DEFAULT_SLOT, 100);
        }
        this.aq.updateWithExternalPara(this.p);
        if (this.ap != null) {
            this.aq.contentId = this.ap.contentId;
            this.aq.searchPreId = this.ap.searchPreId;
            this.aq.searchId = this.ap.searchId;
            this.aq.expatiation = this.ap.expatiation;
            this.aq.pushInfo = this.ap.pushInfo;
        }
    }

    private void P() {
        if (this.aq != null) {
            this.aq.updateWithSimpleAppModel(this.ac);
        }
    }

    public STInfoV2 i() {
        if (this.aq == null) {
            this.aq = STInfoBuilder.buildSTInfo(this, this.ac, STConst.ST_DEFAULT_SLOT, 100, null);
            this.aq.updateWithExternalPara(this.p);
        }
        this.aq.scene = f();
        this.aq.isImmediately = false;
        this.aq.status = STConst.ST_STATUS_DEFAULT;
        return this.aq;
    }

    private void e(int i) {
        String string = this.aR.getString(com.tencent.assistant.b.a.j);
        String string2 = this.aR.getString(com.tencent.assistant.b.a.h);
        String string3 = this.aR.getString("com.tencent.assistant.PACKAGE_NAME");
        if (!TextUtils.isEmpty(string)) {
            k.a("StatIpcToAppDetail", this.aR.getString(com.tencent.assistant.b.a.ac), com.tencent.assistant.st.h.a(this.aR.getString(com.tencent.assistant.b.a.n), this.aR.getString(com.tencent.assistant.b.a.o), this.aR.getString(com.tencent.assistant.b.a.C), string, this.aR.getString(com.tencent.assistant.b.a.y), this.ac != null ? String.valueOf(this.ac.f1634a) : Constants.STR_EMPTY, this.aR.getString(com.tencent.assistant.b.a.ad), string3, string2) + "_" + i);
        }
    }

    /* access modifiers changed from: private */
    public void a(int i, GetRecommendAppListResponse getRecommendAppListResponse) {
        ViewInvalidateMessage viewInvalidateMessage = new ViewInvalidateMessage(1);
        viewInvalidateMessage.arg1 = i;
        if (i == 0) {
            if (getRecommendAppListResponse == null || getRecommendAppListResponse.b.size() == 0) {
                viewInvalidateMessage.arg1 = -1;
            } else {
                this.J = getRecommendAppListResponse;
            }
        }
        this.bb.sendMessage(viewInvalidateMessage);
    }

    /* access modifiers changed from: private */
    public void f(int i) {
        L();
    }

    /* access modifiers changed from: private */
    public void a(int i, int i2, boolean z2, List<SimpleAppModel> list, boolean z3, byte[] bArr) {
        if (z2) {
            ViewInvalidateMessage viewInvalidateMessage = new ViewInvalidateMessage(2);
            viewInvalidateMessage.arg1 = i2;
            HashMap hashMap = new HashMap();
            viewInvalidateMessage.params = hashMap;
            hashMap.put("hasNext", Boolean.valueOf(z3));
            hashMap.put("pageContext", bArr);
            if (i2 == 0) {
                hashMap.put("authorModel", list);
            }
            this.bb.sendMessage(viewInvalidateMessage);
        }
    }

    /* access modifiers changed from: private */
    public void a(int i, List<SimpleAppModel> list, boolean z2, byte[] bArr) {
        String str;
        this.I = this.H.a();
        if (i != 0 || list == null) {
            this.I.a(null, null, null, null, false);
            return;
        }
        if (this.ae != null) {
            str = this.ae.f1660a.f1993a.f1989a.f;
        } else {
            str = null;
        }
        this.I.a(this.ac.c, str, new ArrayList(list), bArr, z2);
    }

    /* access modifiers changed from: private */
    public boolean Q() {
        if (!this.aK || this.aV != 0 || !TextUtils.isEmpty(this.aR.getString(com.tencent.assistant.b.a.D)) || DownloadProxy.a().a(this.ac) == null) {
            return false;
        }
        return true;
    }

    public static boolean a(c cVar) {
        AppDetailWithComment appDetailWithComment;
        if (cVar == null || cVar.f1660a == null || (appDetailWithComment = cVar.f1660a) == null || appDetailWithComment.f1993a == null || appDetailWithComment.f1993a.f1989a == null || appDetailWithComment.f1993a.b == null || appDetailWithComment.f1993a.b.size() == 0) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (!this.ba) {
            this.F.b();
            this.L.onPause();
            this.X.onPause();
            if (!this.as) {
                this.bi.sendEmptyMessageDelayed(1, 100);
            }
            this.G.m();
            if (this.af == null || !this.af.isShowing()) {
                this.N = false;
                return;
            }
            this.af.hide();
            this.N = true;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.ba) {
            this.F.a();
            if (this.R == 0 && this.H != null && this.ar) {
                this.H.d();
                this.H.requestLayout();
                this.ar = false;
            }
            this.as = false;
            this.L.onResume();
            this.X.onResume();
            this.G.l();
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_COLLECTION_SUCCESS, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_COLLECTION_FAIL, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_INSTALL, this);
            AstApp.i().k().addUIEventListener(1013, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UNINSTALL, this);
            if (this.X != null && this.X.getNeedScrollToBottomOnResume() == 1) {
                this.X.scrolViewToBottom(true);
                this.X.setNeedScrollToBottomOnResume(2);
            }
            if (this.I != null && this.aW) {
                this.I.c();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (!this.ba) {
            if (this.X != null) {
                this.X.onDestroy();
                this.Y = false;
            }
            if (this.I != null) {
                this.I.b();
            }
            if (this.af != null && this.af.isShowing()) {
                this.af.dismiss();
            }
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_COLLECTION_SUCCESS, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_COLLECTION_FAIL, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_INSTALL, this);
            AstApp.i().k().removeUIEventListener(1013, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UNINSTALL, this);
        }
    }

    public void onSavedParams(String str, int i, int i2, boolean z2) {
        this.al = str;
        this.am = i;
        this.an = i2;
    }

    /* access modifiers changed from: private */
    public boolean R() {
        return this.aK;
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE:
                if (a(this.ae)) {
                    long j = this.ae.f1660a.f1993a.b.get(0).f1982a;
                    boolean z2 = this.ae.f1660a.c;
                    byte b = this.ae.f1660a.e;
                    this.aS.a(a(this.ae, this.ac));
                    this.X.setSimpleAppModel(this.aS);
                    return;
                }
                return;
            case 1010:
            default:
                return;
            case EventDispatcherEnum.UI_EVENT_APP_INSTALL:
                String str = Constants.STR_EMPTY;
                if (message.obj != null && (message.obj instanceof String)) {
                    str = (String) message.obj;
                }
                if (a(this.ae) && str.equals(this.ae.a().c)) {
                    this.aQ = ApkResourceManager.getInstance().getInstalledApkInfo(this.ac.c, true);
                    long j2 = this.ae.f1660a.f1993a.b.get(0).f1982a;
                    boolean z3 = this.ae.f1660a.c;
                    byte b2 = this.ae.f1660a.e;
                    this.aS.a(a(this.ae, this.ac));
                    this.X.setSimpleAppModel(this.aS);
                    if (this.aQ == null) {
                        this.X.setClickBtnForCmdText(false);
                    } else {
                        this.X.setClickBtnForCmdText(this.aQ.mVersionCode == this.ae.a().g);
                    }
                    if (this.X.mstate == 2) {
                        this.X.setState(2);
                        return;
                    }
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_APP_UNINSTALL:
                String str2 = Constants.STR_EMPTY;
                if (message.obj != null && (message.obj instanceof String)) {
                    str2 = (String) message.obj;
                }
                if (a(this.ae) && str2.equals(this.ae.a().c)) {
                    this.X.setClickBtnForCmdText(false);
                    long j3 = this.ae.f1660a.f1993a.b.get(0).f1982a;
                    boolean z4 = this.ae.f1660a.c;
                    byte b3 = this.ae.f1660a.e;
                    this.aS.a(a(this.ae, this.ac));
                    this.X.setSimpleAppModel(this.aS);
                    if (this.X.mstate == 2) {
                        this.X.setState(2);
                        return;
                    }
                    return;
                }
                return;
        }
    }

    public Bundle j() {
        if (this.aR == null || !a(this.ae)) {
            return null;
        }
        Bundle bundle = new Bundle();
        bundle.putString(com.tencent.assistant.b.a.c, this.ac.c);
        bundle.putString(com.tencent.assistant.b.a.h, this.ac.g + Constants.STR_EMPTY);
        bundle.putString(com.tencent.assistant.b.a.C, this.ac.ac);
        bundle.putString(com.tencent.assistant.b.a.s, ((int) this.aL) + Constants.STR_EMPTY);
        if (!this.aR.isEmpty()) {
            for (String next : this.aR.keySet()) {
                if (w.contains(next)) {
                    bundle.putString(next, this.aR.getString(next));
                }
            }
        }
        return bundle;
    }

    /* access modifiers changed from: protected */
    public e v() {
        e E2 = E();
        E2.b(true);
        E2.a(true);
        return E2;
    }

    private byte[] b(String str) {
        try {
            if (!TextUtils.isEmpty(str)) {
                return i.a(str, 0);
            }
        } catch (Exception e) {
        }
        return null;
    }
}
