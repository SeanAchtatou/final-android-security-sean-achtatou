package com.tencent.assistantv2.activity;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class dj extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchActivity f2833a;

    dj(SearchActivity searchActivity) {
        this.f2833a = searchActivity;
    }

    public void onTMAClick(View view) {
        if (this.f2833a.G) {
            this.f2833a.u.a().setText((CharSequence) null);
            this.f2833a.u.a().setSelection(0);
            this.f2833a.B();
            return;
        }
        this.f2833a.finish();
    }

    public STInfoV2 getStInfo(View view) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f2833a, 200);
        buildSTInfo.slotId = a.a("03", "003");
        return buildSTInfo;
    }
}
