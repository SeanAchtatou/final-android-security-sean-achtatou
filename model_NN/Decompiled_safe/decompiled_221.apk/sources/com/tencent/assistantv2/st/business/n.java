package com.tencent.assistantv2.st.business;

import android.os.Build;
import com.tencent.assistant.db.table.x;
import com.tencent.assistant.protocol.jce.StatDevProcessError;
import com.tencent.assistant.utils.bh;

/* compiled from: ProGuard */
public class n extends BaseSTManagerV2 {
    public byte getSTType() {
        return 19;
    }

    public void flush() {
    }

    public void a(int i, String str) {
        String str2 = Build.BRAND;
        String str3 = Build.MODEL;
        x.a().a(getSTType(), bh.a(new StatDevProcessError(i, str, str2 + " " + str3, Build.VERSION.SDK)));
    }
}
