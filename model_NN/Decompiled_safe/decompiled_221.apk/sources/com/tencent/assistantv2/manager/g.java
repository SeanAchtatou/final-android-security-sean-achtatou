package com.tencent.assistantv2.manager;

import com.tencent.assistant.model.r;
import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistantv2.b.ad;
import com.tencent.assistantv2.model.a.j;
import java.util.List;

/* compiled from: ProGuard */
class g implements CallbackHelper.Caller<j> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f3276a;
    final /* synthetic */ int b;
    final /* synthetic */ boolean c;
    final /* synthetic */ ad d;
    final /* synthetic */ boolean e;
    final /* synthetic */ List f;
    final /* synthetic */ r g;
    final /* synthetic */ List h;
    final /* synthetic */ f i;

    g(f fVar, int i2, int i3, boolean z, ad adVar, boolean z2, List list, r rVar, List list2) {
        this.i = fVar;
        this.f3276a = i2;
        this.b = i3;
        this.c = z;
        this.d = adVar;
        this.e = z2;
        this.f = list;
        this.g = rVar;
        this.h = list2;
    }

    /* renamed from: a */
    public void call(j jVar) {
        jVar.a(this.f3276a, this.b, this.c, this.d, this.e, this.f, this.g, this.h);
    }
}
