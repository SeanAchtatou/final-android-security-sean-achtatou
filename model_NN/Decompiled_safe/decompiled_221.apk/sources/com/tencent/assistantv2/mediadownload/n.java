package com.tencent.assistantv2.mediadownload;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import com.tencent.assistant.b.a;

/* compiled from: ProGuard */
public class n {

    /* renamed from: a  reason: collision with root package name */
    private String f3296a = null;
    private String b = "DF";
    private String c = null;
    private byte d = 1;

    public String a() {
        return this.f3296a;
    }

    public String b() {
        return this.b;
    }

    public String c() {
        return this.c;
    }

    public static n a(Intent intent) {
        if (intent == null) {
            return null;
        }
        if ("android.intent.action.VIEW".equals(intent.getAction())) {
            Uri data = intent.getData();
            if (data == null) {
                return null;
            }
            n nVar = new n();
            nVar.f3296a = data.toString();
            return nVar;
        }
        Bundle extras = intent.getExtras();
        if (extras == null) {
            return null;
        }
        String string = extras.getString(a.K);
        String string2 = extras.getString(a.L);
        String string3 = extras.getString(a.M);
        if (TextUtils.isEmpty(string)) {
            return null;
        }
        n nVar2 = new n();
        nVar2.f3296a = string;
        if (!TextUtils.isEmpty(string2)) {
            nVar2.b = string2;
        }
        nVar2.c = string3;
        nVar2.d = 2;
        return nVar2;
    }

    public byte d() {
        return this.d;
    }

    public String toString() {
        return "OutterCallDownloadInfo{uriFromOutCall='" + this.f3296a + '\'' + ", callFrom='" + this.b + '\'' + ", urlTicket='" + this.c + '\'' + '}';
    }
}
