package com.tencent.assistantv2.mediadownload;

import android.os.Handler;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.download.k;
import com.tencent.assistant.event.EventDispatcher;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager;
import com.tencent.assistantv2.db.a.b;
import com.tencent.assistantv2.model.AbstractDownloadInfo;
import com.tencent.assistantv2.model.a;
import com.tencent.assistantv2.model.d;
import com.tencent.downloadsdk.DownloadManager;
import com.tencent.downloadsdk.DownloadTask;
import com.tencent.downloadsdk.ac;
import com.tencent.downloadsdk.ae;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public class c {
    private static final Object e = new Object();
    private static c f;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Map<String, d> f3285a = new ConcurrentHashMap(5);
    /* access modifiers changed from: private */
    public b b = new b();
    /* access modifiers changed from: private */
    public EventDispatcher c = AstApp.i().j();
    /* access modifiers changed from: private */
    public InstallUninstallDialogManager d = new InstallUninstallDialogManager();
    private Handler g = k.a();
    private ae h = new h(this);

    private c() {
        this.d.a(true);
        try {
            List<d> a2 = this.b.a();
            ArrayList<ac> a3 = DownloadManager.a().a(100);
            for (d next : a2) {
                next.d();
                this.f3285a.put(next.c, next);
            }
            if (a3 != null) {
                Iterator<ac> it = a3.iterator();
                while (it.hasNext()) {
                    ac next2 = it.next();
                    d dVar = this.f3285a.get(next2.b);
                    if (dVar != null) {
                        if (dVar.k == null) {
                            dVar.k = new a();
                        }
                        dVar.k.b = next2.c;
                        dVar.k.f3314a = next2.d;
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public boolean a(d dVar) {
        this.g.post(new d(this, dVar));
        return true;
    }

    public boolean b(d dVar) {
        if (dVar == null || TextUtils.isEmpty(dVar.c)) {
            return false;
        }
        if (dVar.e() || dVar.c()) {
            this.c.sendMessage(this.c.obtainMessage(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_SUCC, dVar));
            d(dVar);
            return true;
        }
        String a2 = dVar.a();
        if (TextUtils.isEmpty(a2) || !a2.startsWith("/data/data/")) {
            ArrayList arrayList = new ArrayList(1);
            arrayList.add(dVar.b);
            DownloadTask downloadTask = new DownloadTask(100, dVar.c, 0, 0, dVar.a(), dVar.f3307a, arrayList, null);
            ae a3 = com.tencent.assistantv2.st.k.a(dVar.c, 0, 0, (byte) dVar.f(), dVar.l, SimpleDownloadInfo.UIType.NORMAL, SimpleDownloadInfo.DownloadType.FILE);
            downloadTask.b = DownloadInfo.getPriority(SimpleDownloadInfo.DownloadType.FILE, SimpleDownloadInfo.UIType.NORMAL);
            dVar.g = downloadTask.d();
            downloadTask.a(this.h);
            downloadTask.a(a3);
            DownloadManager.a().a(downloadTask);
            boolean c2 = c(dVar);
            if (DownloadManager.a().c(dVar.f(), dVar.c) || DownloadProxy.a().h() < 2) {
                if (dVar.i != AbstractDownloadInfo.DownState.DOWNLOADING) {
                    dVar.i = AbstractDownloadInfo.DownState.DOWNLOADING;
                    this.c.sendMessage(this.c.obtainMessage(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_START, dVar));
                }
                this.c.sendMessage(this.c.obtainMessage(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOADING, dVar));
            } else {
                dVar.i = AbstractDownloadInfo.DownState.QUEUING;
                this.c.sendMessage(this.c.obtainMessage(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_QUEUING, dVar));
            }
            if (c2) {
                this.c.sendMessage(this.c.obtainMessage(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_ADD, dVar));
            }
            this.b.a(dVar);
            return true;
        }
        TemporaryThreadManager.get().start(new e(this));
        return false;
    }

    private boolean c(d dVar) {
        try {
            if (!this.f3285a.containsKey(dVar.c)) {
                this.f3285a.put(dVar.c, dVar);
                dVar.e = System.currentTimeMillis();
                return true;
            }
            if (this.f3285a.get(dVar.c) != dVar) {
                this.f3285a.put(dVar.c, dVar);
            }
            return false;
        } catch (NullPointerException e2) {
            return false;
        }
    }

    private void d(d dVar) {
        if (dVar != null && !TextUtils.isEmpty(dVar.c)) {
            this.f3285a.put(dVar.c, dVar);
            this.b.a(dVar);
        }
    }

    public void a(String str) {
        TemporaryThreadManager.get().start(new f(this, str));
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        d dVar;
        if (!TextUtils.isEmpty(str) && (dVar = this.f3285a.get(str)) != null) {
            DownloadManager.a().a(100, str);
            if (dVar.i == AbstractDownloadInfo.DownState.DOWNLOADING || dVar.i == AbstractDownloadInfo.DownState.QUEUING) {
                dVar.i = AbstractDownloadInfo.DownState.PAUSED;
                this.c.sendMessage(this.c.obtainMessage(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_PAUSE, dVar));
                this.b.a(dVar);
            }
        }
    }

    public boolean b(String str) {
        this.g.post(new g(this, str));
        return true;
    }

    public boolean a(String str, boolean z) {
        d remove;
        if (TextUtils.isEmpty(str) || (remove = this.f3285a.remove(str)) == null) {
            return false;
        }
        DownloadManager.a().b(100, str);
        this.b.a(str);
        if (z) {
            FileUtil.deleteFile(remove.h);
        }
        remove.i = AbstractDownloadInfo.DownState.DELETE;
        this.c.sendMessage(this.c.obtainMessage(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_DELETE, remove));
        return true;
    }

    public List<d> a() {
        return a(0);
    }

    private List<d> a(int i) {
        ArrayList arrayList = new ArrayList(this.f3285a.size());
        for (Map.Entry next : this.f3285a.entrySet()) {
            if (i == 0 || (i == 1 && (((d) next.getValue()).i == AbstractDownloadInfo.DownState.DOWNLOADING || ((d) next.getValue()).i == AbstractDownloadInfo.DownState.QUEUING))) {
                arrayList.add(next.getValue());
            }
        }
        return arrayList;
    }

    public List<d> b() {
        return a(1);
    }

    public static c c() {
        if (f == null) {
            synchronized (e) {
                if (f == null) {
                    f = new c();
                }
            }
        }
        return f;
    }

    public void d() {
        TemporaryThreadManager.get().start(new k(this));
    }

    public void e() {
        TemporaryThreadManager.get().start(new l(this));
    }

    public void f() {
        if (this.f3285a != null && this.f3285a.size() > 0) {
            for (Map.Entry<String, d> key : this.f3285a.entrySet()) {
                c((String) key.getKey());
            }
        }
    }

    public int g() {
        int i = 0;
        if (this.f3285a == null || this.f3285a.size() <= 0) {
            return 0;
        }
        Iterator<Map.Entry<String, d>> it = this.f3285a.entrySet().iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            Map.Entry next = it.next();
            if (next.getValue() != null && (((d) next.getValue()).i == AbstractDownloadInfo.DownState.DOWNLOADING || ((d) next.getValue()).i == AbstractDownloadInfo.DownState.QUEUING)) {
                i2++;
            }
            i = i2;
        }
    }

    public d a(n nVar) {
        if (nVar == null || TextUtils.isEmpty(nVar.c())) {
            return null;
        }
        return this.f3285a.get(d.a(nVar.c(), nVar.b()));
    }
}
