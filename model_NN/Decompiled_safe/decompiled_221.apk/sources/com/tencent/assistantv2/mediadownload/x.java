package com.tencent.assistantv2.mediadownload;

import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistantv2.model.AbstractDownloadInfo;
import com.tencent.assistantv2.model.h;
import com.tencent.downloadsdk.DownloadManager;

/* compiled from: ProGuard */
class x implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ o f3306a;

    x(o oVar) {
        this.f3306a = oVar;
    }

    public void run() {
        for (h next : this.f3306a.a()) {
            DownloadManager.a().a(8, next.c);
            if (next.i == AbstractDownloadInfo.DownState.DOWNLOADING || next.i == AbstractDownloadInfo.DownState.QUEUING) {
                next.i = AbstractDownloadInfo.DownState.FAIL;
                this.f3306a.c.sendMessage(this.f3306a.c.obtainMessage(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_FAIL, next));
                this.f3306a.b.a(next);
            }
        }
    }
}
