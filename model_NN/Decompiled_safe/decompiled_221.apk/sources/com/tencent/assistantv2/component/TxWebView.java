package com.tencent.assistantv2.component;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.webkit.ValueCallback;
import android.widget.ZoomButtonsController;
import com.tencent.assistant.apilevel.FullscreenableChromeClient;
import com.tencent.assistant.js.JsBridge;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.dh;
import com.tencent.assistant.utils.r;
import com.tencent.assistant.utils.t;
import com.tencent.connect.common.Constants;
import com.tencent.smtt.sdk.WebChromeClient;
import com.tencent.smtt.sdk.WebSettings;
import com.tencent.smtt.sdk.WebView;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class TxWebView extends WebView implements NetworkMonitor.ConnectivityChangeListener {
    private static final int b = r.d();
    private static ArrayList<String> j = new ArrayList<>();
    private static ArrayList<String> k = new ArrayList<>();

    /* renamed from: a  reason: collision with root package name */
    private final String f3020a;
    private eb c;
    /* access modifiers changed from: private */
    public ec d;
    /* access modifiers changed from: private */
    public Context e;
    private JsBridge f;
    private WebChromeClient g;
    private WebSettings h;
    private ea i;

    static {
        j.add("MT870");
        j.add("XT910");
        j.add("XT928");
        j.add("MT917");
        j.add("Lenovo A60");
        k.add("GT-N7100");
        k.add("GT-N7102");
        k.add("GT-N7105");
        k.add("GT-N7108");
        k.add("GT-N7108D");
        k.add("GT-N7109");
    }

    public TxWebView(Context context) {
        this(context, null);
    }

    public TxWebView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3020a = "/qqdownloader/3";
        this.h = null;
        this.e = context;
        h();
        setBackgroundColor(0);
    }

    private void h() {
        try {
            Method method = getClass().getMethod("removeJavascriptInterface", String.class);
            if (method != null) {
                method.invoke(this, "searchBoxJavaBridge_");
            }
        } catch (Exception e2) {
        }
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    public void a(JsBridge jsBridge, eb ebVar, ec ecVar, dz dzVar) {
        String str;
        this.f = jsBridge;
        this.c = ebVar;
        this.d = ecVar;
        requestFocus();
        setFocusable(true);
        setFocusableInTouchMode(true);
        setScrollbarFadingEnabled(true);
        setScrollBarStyle(0);
        this.h = getSettings();
        String str2 = Constants.STR_EMPTY;
        try {
            str2 = new android.webkit.WebView(this.e).getSettings().getUserAgentString();
        } catch (Exception e2) {
        }
        if (!TextUtils.isEmpty(dzVar.f3190a)) {
            str = str2 + "/apiLevel/" + Build.VERSION.SDK_INT + dzVar.f3190a;
            this.h.setUserAgentString(str);
        } else {
            str = str2 + "/qqdownloader/3" + "/apiLevel/" + Build.VERSION.SDK_INT;
            this.h.setUserAgentString(str);
        }
        XLog.i("TxWebView", "UA : " + str);
        if (dzVar != null) {
            this.h.setBuiltInZoomControls(dzVar.e);
        }
        this.h.setJavaScriptEnabled(true);
        if (dzVar != null) {
            this.h.setCacheMode(dzVar.d);
        }
        this.h.setSavePassword(false);
        if (Build.VERSION.SDK_INT >= 8 && Build.VERSION.SDK_INT < 18) {
            this.h.setPluginState(WebSettings.PluginState.ON);
        }
        this.h.setAppCacheEnabled(true);
        this.h.setAppCachePath(FileUtil.getWebViewCacheDir());
        this.h.setDomStorageEnabled(true);
        this.h.setDatabaseEnabled(true);
        this.h.setDatabasePath(FileUtil.getWebViewCacheDir());
        this.h.setGeolocationEnabled(true);
        this.h.setGeolocationDatabasePath(FileUtil.getWebViewCacheDir());
        a(dzVar == null ? 0 : dzVar.b);
        if (i()) {
            if (b >= 7) {
                try {
                    this.h.getClass().getMethod("setLoadWithOverviewMode", Boolean.TYPE).invoke(this.h, true);
                } catch (Exception e3) {
                }
            }
            if (t.f2723a) {
                if (b < 11) {
                    try {
                        Field declaredField = WebView.class.getDeclaredField("mZoomButtonsController");
                        declaredField.setAccessible(true);
                        ZoomButtonsController zoomButtonsController = new ZoomButtonsController(this);
                        zoomButtonsController.getZoomControls().setVisibility(8);
                        declaredField.set(this, zoomButtonsController);
                    } catch (Exception e4) {
                    }
                } else {
                    try {
                        this.h.getClass().getMethod("setDisplayZoomControls", Boolean.TYPE).invoke(this.h, false);
                    } catch (Exception e5) {
                    }
                }
            }
        }
        a(dzVar);
        dh.a();
    }

    public void a() {
        setDownloadListener(null);
        removeAllViews();
        try {
            setVisibility(8);
            stopLoading();
            clearHistory();
            destroy();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void b() {
        try {
            Method method = WebView.class.getMethod("disablePlatformNotifications", new Class[0]);
            if (method != null) {
                method.invoke(null, new Object[0]);
            }
        } catch (IllegalArgumentException e2) {
            e2.printStackTrace();
        } catch (IllegalAccessException e3) {
            e3.printStackTrace();
        } catch (Throwable th) {
            th.printStackTrace();
        }
        a("onPause");
    }

    public void c() {
        try {
            Method method = WebView.class.getMethod("enablePlatformNotifications", new Class[0]);
            if (method != null) {
                method.invoke(null, new Object[0]);
            }
        } catch (IllegalArgumentException e2) {
            e2.printStackTrace();
        } catch (IllegalAccessException e3) {
            e3.printStackTrace();
        } catch (Throwable th) {
            th.printStackTrace();
        }
        a("onResume");
    }

    private void a(String str) {
        try {
            WebView.class.getMethod(str, new Class[0]).invoke(this, new Object[0]);
        } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e2) {
        }
    }

    private void a(dz dzVar) {
        if (b < 14 || (dzVar.c && !j())) {
            dx dxVar = new dx(this.e, this.f, this.d, this.c);
            this.g = dxVar;
            setWebChromeClient(dxVar);
        } else {
            FullscreenableChromeClient fullscreenableChromeClient = new FullscreenableChromeClient(this.e, this.f, this.d, this.c);
            this.g = fullscreenableChromeClient;
            setWebChromeClient(fullscreenableChromeClient);
        }
        setWebViewClient(new ed(this.e, this.f, this.d, dzVar));
        setDownloadListener(new dy(this));
    }

    public ValueCallback<Uri> d() {
        if (this.g instanceof dx) {
            return ((dx) this.g).a();
        }
        return ((FullscreenableChromeClient) this.g).getUploadMessage();
    }

    public void e() {
        if (this.g instanceof dx) {
            ((dx) this.g).b();
        } else {
            ((FullscreenableChromeClient) this.g).clearUploadMessage();
        }
    }

    public void a(View view, int i2) {
        if (view != null && Build.VERSION.SDK_INT > 10) {
            try {
                view.getClass().getMethod("setLayerType", Integer.TYPE, Paint.class).invoke(view, Integer.valueOf(i2), null);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public void f() {
        XLog.i("TxWebView", "openHardware");
    }

    public void g() {
        XLog.i("TxWebView", "closeHardWare");
        a(this, 1);
    }

    private boolean i() {
        String str = Build.MODEL;
        return !str.contains("vivo") && !j.contains(str);
    }

    private boolean j() {
        String str = Build.MODEL;
        XLog.i("TxWebView", "[isSupportTencentVideoFullScreenPlay] : " + str);
        return !k.contains(str);
    }

    private void a(int i2) {
        String str = Build.MANUFACTURER + "_" + Build.MODEL;
        if (i2 <= 0) {
            String d2 = m.a().d((byte) 0);
            String d3 = m.a().d((byte) 1);
            String e2 = m.a().e((byte) 0);
            String e3 = m.a().e((byte) 1);
            if (!TextUtils.isEmpty(e3) || !TextUtils.isEmpty(e2)) {
                int i3 = Build.VERSION.SDK_INT;
                if (!TextUtils.isEmpty(e2) && a(e2, i3 + Constants.STR_EMPTY)) {
                    g();
                } else if (!TextUtils.isEmpty(e3) && a(e3, i3 + Constants.STR_EMPTY)) {
                    f();
                }
            }
            if (TextUtils.isEmpty(d2) && TextUtils.isEmpty(d3)) {
                return;
            }
            if (!TextUtils.isEmpty(d2) && str.matches(d2)) {
                g();
            } else if (!TextUtils.isEmpty(d3) && str.matches(d3)) {
                f();
            }
        } else if (i2 == 1) {
            f();
        } else if (i2 == 2) {
            g();
        }
    }

    private boolean a(String str, String str2) {
        String[] split;
        if (TextUtils.isEmpty(str) || (split = str.split("\\|")) == null) {
            return false;
        }
        for (String str3 : split) {
            if (!TextUtils.isEmpty(str3) && str3.equals(str2)) {
                return true;
            }
        }
        return false;
    }

    public void onConnected(APN apn) {
        if (this.h != null) {
            this.h.setCacheMode(-1);
        }
    }

    public void onDisconnected(APN apn) {
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
    }

    /* access modifiers changed from: protected */
    public void onScrollChanged(int i2, int i3, int i4, int i5) {
        super.onScrollChanged(i2, i3, i4, i5);
        if (this.i != null) {
            this.i.a(i2, i3, i4, i5);
        }
    }

    public void a(ea eaVar) {
        this.i = eaVar;
    }
}
