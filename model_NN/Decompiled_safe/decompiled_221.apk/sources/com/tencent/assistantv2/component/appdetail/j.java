package com.tencent.assistantv2.component.appdetail;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.activity.AuthorOtherAppsActivity;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.ArrayList;

/* compiled from: ProGuard */
class j extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f3074a;
    final /* synthetic */ ArrayList b;
    final /* synthetic */ byte[] c;
    final /* synthetic */ boolean d;
    final /* synthetic */ AppdetailRelatedViewV5 e;

    j(AppdetailRelatedViewV5 appdetailRelatedViewV5, String str, ArrayList arrayList, byte[] bArr, boolean z) {
        this.e = appdetailRelatedViewV5;
        this.f3074a = str;
        this.b = arrayList;
        this.c = bArr;
        this.d = z;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.e.f3046a, AuthorOtherAppsActivity.class);
        intent.putExtra("author_name", this.e.p);
        intent.putExtra("pkgname", this.f3074a);
        intent.putExtra("first_page_data", this.b);
        intent.putExtra("page_context", this.c);
        intent.putExtra("has_next", this.d);
        this.e.f3046a.startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        if (!(this.e.f3046a instanceof AppDetailActivityV5)) {
            return null;
        }
        STInfoV2 i = ((AppDetailActivityV5) this.e.f3046a).i();
        i.slotId = this.e.a();
        i.actionId = 200;
        return i;
    }
}
