package com.tencent.assistantv2.component;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.utils.ck;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class cy extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SecondNavigationTitleViewV5 f3167a;

    cy(SecondNavigationTitleViewV5 secondNavigationTitleViewV5) {
        this.f3167a = secondNavigationTitleViewV5;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(com.tencent.assistantv2.component.SecondNavigationTitleViewV5, boolean):boolean
     arg types: [com.tencent.assistantv2.component.SecondNavigationTitleViewV5, int]
     candidates:
      com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(com.tencent.assistantv2.component.SecondNavigationTitleViewV5, java.lang.String):com.tencent.assistantv2.st.page.STInfoV2
      com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(java.lang.String, int):void
      com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(java.lang.String, android.text.TextUtils$TruncateAt):void
      com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(boolean, int):void
      com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(com.tencent.assistantv2.component.SecondNavigationTitleViewV5, boolean):boolean */
    public void onTMAClick(View view) {
        this.f3167a.s.setShowCareFirstTime(!ck.b());
        ck.a(this.f3167a.s.isShowUserCare());
        if (this.f3167a.r != null) {
            this.f3167a.r.setVisibility(8);
        }
        if (this.f3167a.z) {
            boolean unused = this.f3167a.z = false;
        }
        try {
            if (this.f3167a.k != null && !this.f3167a.k.isFinishing()) {
                this.f3167a.s.show();
            }
        } catch (Throwable th) {
        }
        if (this.f3167a.y != null && this.f3167a.w != null) {
            this.f3167a.s.refreshState(this.f3167a.y.f1660a.f, this.f3167a.w.f1634a);
        }
    }

    public STInfoV2 getStInfo() {
        return this.f3167a.e(a.a("05", "001"));
    }
}
