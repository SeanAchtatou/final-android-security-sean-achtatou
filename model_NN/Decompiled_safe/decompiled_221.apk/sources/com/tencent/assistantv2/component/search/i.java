package com.tencent.assistantv2.component.search;

import android.view.View;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class i extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchHotwordsView f3239a;

    i(SearchHotwordsView searchHotwordsView) {
        this.f3239a = searchHotwordsView;
    }

    public void onTMAClick(View view) {
        if (this.f3239a.i != null) {
            this.f3239a.i.onClick(view);
        }
    }

    public STInfoV2 getStInfo() {
        if (!(this.f3239a.getContext() instanceof BaseActivity)) {
            return null;
        }
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3239a.getContext(), 200);
        buildSTInfo.slotId = a.a("04", "103");
        buildSTInfo.status = "01";
        return buildSTInfo;
    }
}
