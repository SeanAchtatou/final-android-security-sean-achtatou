package com.tencent.assistantv2.component;

import android.os.Bundle;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.link.b;
import com.tencent.assistant.manager.BookingManager;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class n extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ STInfoV2 f3221a;
    final /* synthetic */ BookingButton b;

    n(BookingButton bookingButton, STInfoV2 sTInfoV2) {
        this.b = bookingButton;
        this.f3221a = sTInfoV2;
    }

    public void onTMAClick(View view) {
        Bundle bundle = new Bundle();
        if (this.f3221a != null) {
            bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.f3221a.scene);
        }
        b.a(this.b.getContext(), this.b.b.n, bundle);
    }

    public STInfoV2 getStInfo() {
        if (this.f3221a != null) {
            this.f3221a.status = this.b.b() == BookingManager.BOOKINGSTATUS.UNBOOKED ? "01" : "02";
        }
        return this.f3221a;
    }
}
