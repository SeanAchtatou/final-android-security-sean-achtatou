package com.tencent.assistantv2.component;

import android.os.Bundle;
import android.view.View;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.login.d;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class ce extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RankFriendsLoginErrorView f3151a;

    ce(RankFriendsLoginErrorView rankFriendsLoginErrorView) {
        this.f3151a = rankFriendsLoginErrorView;
    }

    public void onTMAClick(View view) {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConst.KEY_LOGIN_TYPE, 5);
        bundle.putInt(AppConst.KEY_FROM_TYPE, 13);
        d.a().a(AppConst.IdentityType.MOBILEQ, bundle);
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3151a.getContext(), 200);
        buildSTInfo.slotId = a.a(RankNormalListView.ST_HIDE_INSTALLED_APPS, "001");
        return buildSTInfo;
    }
}
