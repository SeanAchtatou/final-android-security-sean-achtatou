package com.tencent.assistantv2.component.appdetail;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.adapter.AppDetailAppCfgAdapter;
import com.tencent.assistant.component.appdetail.AppdetailViewPager;
import com.tencent.assistant.component.appdetail.FriendRankView;
import com.tencent.assistant.component.appdetail.HorizonScrollPicViewer;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.c;
import com.tencent.assistant.protocol.jce.AppHotFriend;
import com.tencent.assistant.protocol.jce.AppTagInfo;
import com.tencent.assistant.protocol.jce.CommentDetail;
import com.tencent.assistant.protocol.jce.CommentTagInfo;
import com.tencent.assistant.protocol.jce.RelateNews;
import com.tencent.assistant.utils.cv;
import com.tencent.assistant.utils.df;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class AppDetailViewV5 extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    AdapterView.OnItemClickListener f3044a = new f(this);
    private HorizonScrollPicViewer b;
    private View c;
    /* access modifiers changed from: private */
    public ExpandableTextViewV5 d;
    /* access modifiers changed from: private */
    public ImageView e;
    private TXImageView f;
    private View g;
    private TextView h;
    /* access modifiers changed from: private */
    public ListView i;
    /* access modifiers changed from: private */
    public AppDetailAppCfgAdapter j;
    /* access modifiers changed from: private */
    public SimpleAppModel k;
    private c l;
    /* access modifiers changed from: private */
    public Context m;
    private LayoutInflater n;
    private boolean o = false;
    private boolean p = true;
    private b q;
    private int[] r = {R.string.chinese, R.string.english, R.string.other};
    private FriendRankView s;
    private FriendTalkView t;
    private DetailTagView u;
    private DetailGameNewsView v;
    private ViewStub w;
    private View.OnClickListener x = new e(this);

    /* compiled from: ProGuard */
    public enum APPDETAIL_MODE {
        JUST_OPEN,
        NEED_UPDATE,
        NOT_INSTALLED,
        FROM_OTHER_APPLICATION
    }

    public AppDetailViewV5(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.m = context;
    }

    public AppDetailViewV5(Context context) {
        super(context);
        this.m = context;
    }

    public static APPDETAIL_MODE a(c cVar, SimpleAppModel simpleAppModel) {
        LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(simpleAppModel.c);
        if (localApkInfo == null) {
            return APPDETAIL_MODE.NOT_INSTALLED;
        }
        if (localApkInfo.mVersionCode < cVar.f1660a.a().a().get(0).c) {
            return APPDETAIL_MODE.NEED_UPDATE;
        }
        return APPDETAIL_MODE.JUST_OPEN;
    }

    public void b(c cVar, SimpleAppModel simpleAppModel) {
        if (cVar != null && simpleAppModel != null) {
            this.l = cVar;
            this.k = simpleAppModel;
            d();
            i();
            a(a(this.l.f1660a.f1993a.f1989a.m));
        }
    }

    private r a(RelateNews relateNews) {
        boolean z;
        boolean z2;
        if (relateNews == null || relateNews.d == null || relateNews.d.size() == 0) {
            return null;
        }
        r rVar = new r();
        rVar.f3082a = relateNews.f2275a;
        rVar.b = relateNews.b;
        if (relateNews.e == 1) {
            z = true;
        } else {
            z = false;
        }
        rVar.d = z;
        int i2 = 0;
        while (i2 < 3 && i2 < relateNews.d.size()) {
            rVar.c[i2] = new s();
            rVar.c[i2].c = relateNews.d.get(i2).c;
            if (relateNews.d.get(i2).d == 0) {
                rVar.c[i2].b = "[评测]" + relateNews.d.get(i2).b;
            } else if (relateNews.d.get(i2).d == 1) {
                rVar.c[i2].b = "[攻略]" + relateNews.d.get(i2).b;
            } else if (relateNews.d.get(i2).d == 2) {
                rVar.c[i2].b = relateNews.d.get(i2).b;
            } else if (relateNews.d.get(i2).d == 3) {
                rVar.c[i2].b = relateNews.d.get(i2).b;
            }
            rVar.c[i2].f3083a = relateNews.d.get(i2).e;
            s sVar = rVar.c[i2];
            if (relateNews.d.get(i2).d == 3) {
                z2 = true;
            } else {
                z2 = false;
            }
            sVar.d = z2;
            if (rVar.c[i2].d) {
                rVar.c[i2].e = relateNews.d.get(i2).f;
            } else {
                rVar.c[i2].f = relateNews.d.get(i2).f;
            }
            i2++;
        }
        return rVar;
    }

    private void d() {
        this.n = LayoutInflater.from(this.m);
        switch (i.f3073a[a(this.l, this.k).ordinal()]) {
            case 1:
                this.n.inflate((int) R.layout.app_detail_layout_v5_for_install, this);
                break;
            case 2:
                this.n.inflate((int) R.layout.app_detail_layout_v5_for_open, this);
                break;
            case 3:
                this.n.inflate((int) R.layout.app_detail_layout_v5_for_update, this);
                break;
            case 4:
                this.n.inflate((int) R.layout.app_detail_layout_v5_for_install, this);
                break;
        }
        e();
        f();
        g();
        h();
        l();
        m();
        n();
        o();
    }

    private void e() {
    }

    private void f() {
        this.b = (HorizonScrollPicViewer) findViewById(R.id.my_view_pager);
        if (k()) {
            this.b.setVisibility(0);
        } else {
            this.b.setVisibility(8);
        }
    }

    private void g() {
        this.c = findViewById(R.id.description_detail_layout);
        this.c.setOnClickListener(this.x);
        this.h = (TextView) findViewById(R.id.update_time);
        this.d = (ExpandableTextViewV5) findViewById(R.id.description_txt);
        this.e = (ImageView) findViewById(R.id.expand_more_img);
        this.g = findViewById(R.id.gift_area_layout);
        this.f = (TXImageView) findViewById(R.id.gift_icon);
        if (this.k.T == null || this.k.T.b() == null || TextUtils.isEmpty(this.k.T.b().f1970a)) {
            this.g.setVisibility(8);
            return;
        }
        this.f.setVisibility(0);
        ((TextView) this.f.findViewById(R.id.my_gift_txt)).setText(this.k.T.a());
        if (!TextUtils.isEmpty(this.k.T.a())) {
            this.f.updateImageView(this.k.T.a(), R.drawable.user_task_icon_gift, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        }
        this.g.setOnClickListener(new d(this));
    }

    private void h() {
        this.i = (ListView) findViewById(R.id.product_cfg_list);
        this.i.setSelector(new ColorDrawable(0));
        this.i.setCacheColorHint(0);
        this.i.setVisibility(8);
        ImageView imageView = new ImageView(this.m);
        imageView.setLayoutParams(new AbsListView.LayoutParams(-1, df.a(this.m, 6.0f)));
        imageView.setBackgroundColor(getResources().getColor(17170445));
        this.i.setDivider(imageView.getDrawable());
        this.i.setDividerHeight(df.a(this.m, 8.0f));
        this.j = new AppDetailAppCfgAdapter(this.m, null);
        this.i.setAdapter((ListAdapter) this.j);
        this.i.setOnItemClickListener(this.f3044a);
    }

    private void i() {
        if (this.l != null) {
            String str = this.l.f1660a.f1993a.b.get(0).l;
            if (k()) {
                this.b.setVisibility(0);
                this.b.refreshData(this.l.f1660a.f1993a.b.get(0));
            } else {
                this.b.setVisibility(8);
            }
            String str2 = this.l.f1660a.f1993a.f1989a.e;
            if (this.l.f1660a.f1993a.b.get(0).r >= this.r.length) {
                int length = this.r.length - 1;
            }
            String c2 = cv.c(Long.valueOf(this.l.f1660a.f1993a.b.get(0).i * 1000));
            String str3 = String.format(getResources().getString(R.string.update_time), c2) + "\n";
            if (this.h != null) {
                this.h.setText(str3);
            }
            if (a(this.l, this.k) != APPDETAIL_MODE.NEED_UPDATE) {
                String str4 = this.l.f1660a.f1993a.b.get(0).j;
                if (TextUtils.isEmpty(str4)) {
                    str4 = this.l.f1660a.f1993a.b.get(0).k;
                }
                String replaceAll = str4.replaceAll("[\\n]{3}", "\n\n");
                if (TextUtils.isEmpty(replaceAll)) {
                    replaceAll = getResources().getString(R.string.no_any_current_monent);
                }
                this.d.setText(replaceAll);
            } else {
                String str5 = this.l.f1660a.f1993a.b.get(0).k;
                if (TextUtils.isEmpty(str5)) {
                    String str6 = this.l.f1660a.f1993a.b.get(0).j;
                    if (TextUtils.isEmpty(str6)) {
                        this.d.setText(getResources().getString(R.string.no_any_current_monent));
                    } else {
                        this.d.setText(str6.replaceAll("[\\n]{3}", "\n\n"));
                    }
                } else {
                    String format = String.format(getResources().getString(R.string.update_info_description), str5);
                    if (!TextUtils.isEmpty(format)) {
                        format = format + "\n";
                    }
                    SpannableString spannableString = new SpannableString(format + this.l.f1660a.f1993a.b.get(0).j);
                    spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.appdetail_update_info_title)), 0, 5, 33);
                    this.d.setText(spannableString);
                }
            }
            this.d.a(true);
            this.d.post(new g(this));
            j();
            if (this.p) {
                this.o = true;
                this.p = false;
            }
        }
    }

    private void j() {
        int size;
        if (this.l.f1660a.f1993a.d != null && (size = this.l.f1660a.f1993a.d.size()) > 0) {
            this.i.postDelayed(new h(this), 100);
            this.j.a(this.l.f1660a.f1993a.d);
            ViewGroup.LayoutParams layoutParams = this.i.getLayoutParams();
            View view = this.j.getView(0, null, this.i);
            view.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
            layoutParams.height = ((size - 1) * this.i.getDividerHeight()) + (view.getMeasuredHeight() * size);
            this.i.setLayoutParams(layoutParams);
            this.i.requestLayout();
        }
    }

    public void a() {
        if (this.b != null) {
            this.b.onResume();
        }
    }

    public void b() {
        if (this.b != null) {
            this.b.onPause();
        }
    }

    public void a(HorizonScrollPicViewer.IPicViewerListener iPicViewerListener) {
        this.b.setPicViewerListener(iPicViewerListener);
    }

    public AppdetailViewPager.IHorizonScrollPicViewer c() {
        return this.b.getScrollPicViewerListener();
    }

    public void a(HorizonScrollPicViewer.IShowPictureListener iShowPictureListener) {
        this.b.setShowPictureListener(iShowPictureListener);
    }

    private boolean k() {
        if ((!com.tencent.assistant.net.c.d() || com.tencent.assistant.net.c.k()) && m.a().p()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
        if (this.o) {
            this.o = false;
        }
    }

    public void a(b bVar) {
        this.q = bVar;
    }

    private void l() {
        this.s = (FriendRankView) findViewById(R.id.friend_rank_view);
    }

    private void m() {
        this.t = (FriendTalkView) findViewById(R.id.my_friends_area);
    }

    public void a(ArrayList<CommentDetail> arrayList, ArrayList<CommentTagInfo> arrayList2) {
        this.t = (FriendTalkView) findViewById(R.id.my_friends_area);
        this.t.a("精彩评论", arrayList, arrayList2);
        if (this.t.getVisibility() == 8 && this.s.getVisibility() == 8) {
            findViewById(R.id.friends_show_area).setVisibility(8);
        } else {
            findViewById(R.id.friends_show_area).setVisibility(0);
        }
    }

    public void a(y yVar) {
        this.t.a(yVar);
    }

    private void n() {
        this.u = (DetailTagView) findViewById(R.id.my_tag_area);
    }

    private void o() {
        this.w = (ViewStub) findViewById(R.id.detail_game_news_stub);
    }

    public void a(r rVar) {
        if (this.v == null) {
            this.w.inflate();
            this.v = (DetailGameNewsView) findViewById(R.id.game_about_area);
        }
        this.v.a(rVar);
    }

    public void a(List<AppTagInfo> list, long j2, String str) {
        this.u.a("应用标签", list, j2, str);
    }

    public void a(String str, ArrayList<AppHotFriend> arrayList) {
        this.s.refresh(str, arrayList);
        if (this.t.getVisibility() == 8 && this.s.getVisibility() == 8) {
            findViewById(R.id.friends_show_area).setVisibility(8);
        } else {
            findViewById(R.id.friends_show_area).setVisibility(0);
        }
    }
}
