package com.tencent.assistantv2.component.appdetail;

import android.os.Bundle;
import android.view.View;
import com.tencent.assistant.link.b;

/* compiled from: ProGuard */
class d implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppDetailViewV5 f3068a;

    d(AppDetailViewV5 appDetailViewV5) {
        this.f3068a = appDetailViewV5;
    }

    public void onClick(View view) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("com.tencent.assistant.ACTION_URL", this.f3068a.k.T.b());
        b.a(this.f3068a.m, this.f3068a.k.T.b().f1970a, bundle);
    }
}
