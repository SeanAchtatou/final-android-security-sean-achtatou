package com.tencent.assistantv2.component;

import android.content.Context;
import android.text.TextUtils;

/* compiled from: ProGuard */
class z {

    /* renamed from: a  reason: collision with root package name */
    public String f3267a;
    public int b;
    boolean c;
    public int d;
    public int e;

    private z() {
    }

    /* synthetic */ z(u uVar) {
        this();
    }

    public void a(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.f3267a = str;
            this.c = true;
        }
    }

    public String a(Context context) {
        return this.c ? this.f3267a : context.getResources().getString(this.b);
    }
}
