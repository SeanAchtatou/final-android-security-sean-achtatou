package com.tencent.assistantv2.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.HomePageBanner;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.adapter.smartlist.SmartListAdapter;
import com.tencent.assistantv2.component.banner.e;
import com.tencent.assistantv2.component.banner.f;

/* compiled from: ProGuard */
public class SoftwareBannerView extends QuickBannerView {
    private HomePageBanner f;

    public SoftwareBannerView(Context context) {
        this(context, null);
    }

    public SoftwareBannerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, SmartListAdapter.SmartListType.AppPage.ordinal());
    }

    public SoftwareBannerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void b() {
        LinearLayout linearLayout;
        int i;
        if (this.b != null && this.b.size() != 0) {
            removeAllViews();
            setOrientation(1);
            LinearLayout g = g();
            this.f = new HomePageBanner(getContext());
            this.f.setLockAllWhenTouch(true);
            g.addView(this.f);
            addView(g);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
            layoutParams.setMargins(df.a(getContext(), 4.0f), df.a(getContext(), 2.0f), df.a(getContext(), 4.0f), df.a(getContext(), 0.0f));
            this.f.setLayoutParams(layoutParams);
            if (this.d == SmartListAdapter.SmartListType.GamePage.ordinal()) {
                this.f.setBannerSlotTag("05");
                this.f.refreshBanner(this.b, 9);
            } else {
                this.f.setBannerSlotTag("05");
                this.f.refreshBanner(this.b, 4);
            }
            LinearLayout g2 = g();
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -2);
            layoutParams2.setMargins(k(), l(), m(), n());
            g2.setLayoutParams(layoutParams2);
            addView(g2);
            this.f3004a = e.a(this.b, this.d == SmartListAdapter.SmartListType.GamePage.ordinal() ? 10 : 3, 5);
            g2.setBackgroundResource(R.drawable.common_cardbg_normal_full);
            int i2 = 0;
            int i3 = 0;
            for (f fVar : this.f3004a) {
                if (i3 >= this.e) {
                    linearLayout = c();
                    addView(linearLayout);
                    i = 0;
                } else {
                    linearLayout = g2;
                    i = i3;
                }
                fVar.a(a());
                View b = fVar.b(getContext(), this, this.d, this.c, i2);
                b.setBackgroundResource(R.drawable.v2_button_background_light_selector);
                if (b != null) {
                    ViewGroup.LayoutParams layoutParams3 = b.getLayoutParams();
                    if (layoutParams3 == null) {
                        layoutParams3 = new LinearLayout.LayoutParams(-1, -1);
                    }
                    layoutParams3.height = j();
                    if (layoutParams3 instanceof LinearLayout.LayoutParams) {
                        ((LinearLayout.LayoutParams) layoutParams3).weight = (float) fVar.a();
                    }
                    linearLayout.addView(b, layoutParams3);
                }
                i2++;
                g2 = linearLayout;
                i3 = fVar.a() + i;
            }
        }
    }

    /* access modifiers changed from: protected */
    public LinearLayout g() {
        LinearLayout linearLayout = new LinearLayout(getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        linearLayout.setGravity(17);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(0);
        return linearLayout;
    }

    public void h() {
        if (this.f != null) {
            this.f.stopPlay();
        }
    }

    public void i() {
        if (this.f != null) {
            this.f.startPlay();
        }
    }

    public int j() {
        return df.a(getContext(), 84.5f);
    }

    public int k() {
        return df.a(getContext(), 2.0f);
    }

    public int l() {
        return df.a(getContext(), 6.0f);
    }

    public int m() {
        return df.a(getContext(), 2.0f);
    }

    public int n() {
        return df.a(getContext(), 1.5f);
    }

    public int o() {
        return 6;
    }
}
