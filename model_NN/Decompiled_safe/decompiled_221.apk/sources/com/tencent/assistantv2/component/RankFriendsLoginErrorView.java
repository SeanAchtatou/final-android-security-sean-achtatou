package com.tencent.assistantv2.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.login.d;

/* compiled from: ProGuard */
public class RankFriendsLoginErrorView extends RelativeLayout {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f3007a;
    private TextView b;
    private View c;
    private View d;
    private ImageView e;

    public RankFriendsLoginErrorView(Context context) {
        this(context, null);
    }

    public RankFriendsLoginErrorView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3007a = context;
        b();
    }

    private void b() {
        ((LayoutInflater) this.f3007a.getSystemService("layout_inflater")).inflate((int) R.layout.rankfriends_loginerror, this);
        this.e = (ImageView) findViewById(R.id.friends_loginerror_image);
        this.b = (TextView) findViewById(R.id.friends_loginerror_desc);
        this.c = findViewById(R.id.qq_account_login);
        this.c.setOnClickListener(new ce(this));
        this.d = findViewById(R.id.wx_account_login);
        this.d.setOnClickListener(new cf(this));
    }

    public void a() {
        if (!d.a().j()) {
            this.b.setText((int) R.string.rank_friends_login_tip);
        }
    }

    public void setVisibility(int i) {
        if (i == 0) {
            this.e.setBackgroundResource(R.drawable.common_icon_toplist_qqloginpic);
        } else {
            this.e.setBackgroundResource(0);
        }
        super.setVisibility(i);
    }
}
