package com.tencent.assistantv2.component;

import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
/* synthetic */ class av {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f3094a = new int[AppConst.AppState.values().length];

    static {
        try {
            f3094a[AppConst.AppState.DOWNLOADING.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f3094a[AppConst.AppState.QUEUING.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f3094a[AppConst.AppState.FAIL.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f3094a[AppConst.AppState.PAUSED.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f3094a[AppConst.AppState.INSTALLING.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
    }
}
