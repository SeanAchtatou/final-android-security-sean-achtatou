package com.tencent.assistantv2.component.fps;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
public class FPSNormalItemSubView extends RelativeLayout {
    public FPSNormalItemSubView(Context context) {
        this(context, null);
    }

    public FPSNormalItemSubView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public FPSNormalItemSubView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context, attributeSet);
    }

    private void a(Context context, AttributeSet attributeSet) {
        LayoutInflater.from(context).inflate((int) R.layout.app_gamehome_card_item, this);
        setBackgroundResource(R.drawable.bg_gamehome_card_selector);
    }
}
