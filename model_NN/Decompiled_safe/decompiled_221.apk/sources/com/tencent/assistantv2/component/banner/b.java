package com.tencent.assistantv2.component.banner;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.ba;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class b extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f3100a;
    final /* synthetic */ boolean b;
    final /* synthetic */ ImageView c;
    final /* synthetic */ int d;
    final /* synthetic */ a e;

    b(a aVar, Context context, boolean z, ImageView imageView, int i) {
        this.e = aVar;
        this.f3100a = context;
        this.b = z;
        this.c = imageView;
        this.d = i;
    }

    public void onTMAClick(View view) {
        com.tencent.assistant.link.b.a(this.f3100a, this.e.f3103a.e);
        if (this.b && this.c != null) {
            TemporaryThreadManager.get().start(new c(this));
            ba.a().postDelayed(new d(this), 1000);
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3100a, 200);
        buildSTInfo.slotId = a.a(this.e.b, this.d);
        return buildSTInfo;
    }
}
