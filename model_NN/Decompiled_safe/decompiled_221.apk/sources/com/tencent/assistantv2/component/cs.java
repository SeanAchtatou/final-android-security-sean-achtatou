package com.tencent.assistantv2.component;

import android.view.ViewGroup;
import android.view.animation.Transformation;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ba;

/* compiled from: ProGuard */
class cs implements cr {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ScaleRelativeLayout f3162a;

    cs(ScaleRelativeLayout scaleRelativeLayout) {
        this.f3162a = scaleRelativeLayout;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.component.ScaleRelativeLayout.a(com.tencent.assistantv2.component.ScaleRelativeLayout, boolean):boolean
     arg types: [com.tencent.assistantv2.component.ScaleRelativeLayout, int]
     candidates:
      com.tencent.assistantv2.component.ScaleRelativeLayout.a(com.tencent.assistantv2.component.ScaleRelativeLayout, int):int
      com.tencent.assistantv2.component.ScaleRelativeLayout.a(com.tencent.assistantv2.component.ScaleRelativeLayout, boolean):boolean */
    public void a(float f, Transformation transformation, float f2, float f3) {
        ViewGroup.LayoutParams layoutParams;
        if (this.f3162a.c != null) {
            if (f == 0.0f) {
                boolean unused = this.f3162a.f = false;
                this.f3162a.c.a();
            }
            if (f == 1.0f && !this.f3162a.f) {
                boolean unused2 = this.f3162a.f = true;
                this.f3162a.c.b();
                this.f3162a.clearAnimation();
                this.f3162a.requestLayout();
            }
            this.f3162a.c.a(f, transformation);
        }
        if (this.f3162a.g == 0) {
            if (this.f3162a.a()) {
                transformation.setAlpha(f);
            }
            if (!(f == 1.0f || (layoutParams = this.f3162a.getLayoutParams()) == null)) {
                layoutParams.width = (int) (this.f3162a.d * f);
                layoutParams.height = (int) (this.f3162a.e * f);
                this.f3162a.setLayoutParams(layoutParams);
            }
        }
        if (this.f3162a.g == -1) {
            if (this.f3162a.a()) {
                transformation.setAlpha(1.0f - f);
            }
            if (f != 1.0f) {
                ViewGroup.LayoutParams layoutParams2 = this.f3162a.getLayoutParams();
                if (layoutParams2 != null) {
                    layoutParams2.width = (int) (this.f3162a.d * (1.0f - f));
                    layoutParams2.height = (int) (this.f3162a.e * (1.0f - f));
                    this.f3162a.setLayoutParams(layoutParams2);
                }
            } else {
                this.f3162a.setVisibility(8);
                int unused3 = this.f3162a.g = -1;
            }
        }
        if (this.f3162a.b.hasEnded()) {
            XLog.i(this.f3162a.f3010a, ">>onEnded>>");
            ba.a().postDelayed(new ct(this), 50);
        }
    }
}
