package com.tencent.assistantv2.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;

/* compiled from: ProGuard */
public class WelcomeView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private LayoutInflater f3026a;
    private RelativeLayout b;
    private RelativeLayout c;
    private RelativeLayout d;
    private TXImageView e;
    private TXImageView f;
    private TXImageView g;
    private TextView h;
    private TextView i;
    private TextView j;
    private TextView k;
    private TextView l;
    private TextView m;
    private LinearLayout n;
    private RelativeLayout o;
    private RelativeLayout p;
    private ImageView q;

    public WelcomeView(Context context) {
        super(context);
        this.f3026a = LayoutInflater.from(context);
        a();
    }

    public WelcomeView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3026a = LayoutInflater.from(context);
        a();
    }

    private void a() {
        View inflate = this.f3026a.inflate((int) R.layout.guide_view_4, this);
        this.p = (RelativeLayout) inflate.findViewById(R.id.layout_root);
        this.o = (RelativeLayout) inflate.findViewById(R.id.layout_friends);
        this.n = (LinearLayout) inflate.findViewById(R.id.layout_share);
        this.b = (RelativeLayout) inflate.findViewById(R.id.layout_friend1);
        this.c = (RelativeLayout) inflate.findViewById(R.id.layout_friend2);
        this.d = (RelativeLayout) inflate.findViewById(R.id.layout_friend3);
        this.e = (TXImageView) inflate.findViewById(R.id.icon1);
        this.f = (TXImageView) inflate.findViewById(R.id.icon2);
        this.g = (TXImageView) inflate.findViewById(R.id.icon3);
        this.h = (TextView) inflate.findViewById(R.id.tv_name1);
        this.i = (TextView) inflate.findViewById(R.id.tv_name2);
        this.j = (TextView) inflate.findViewById(R.id.tv_name3);
        this.k = (TextView) inflate.findViewById(R.id.tv_tacit1);
        this.l = (TextView) inflate.findViewById(R.id.tv_tacit2);
        this.m = (TextView) inflate.findViewById(R.id.tv_tacit3);
        this.q = (ImageView) inflate.findViewById(R.id.iv_title);
    }
}
