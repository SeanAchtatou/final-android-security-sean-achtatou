package com.tencent.assistantv2.component;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
public class cp extends Animation {
    private static int d = 5;
    private static cp e = null;

    /* renamed from: a  reason: collision with root package name */
    private View[] f3160a = null;
    private int b = 0;
    private int[] c = {R.drawable.head_icon_circle_bg1, R.drawable.head_icon_circle_bg2, R.drawable.head_icon_circle_bg3, R.drawable.head_icon_circle_bg4, R.drawable.head_icon_circle_bg5, R.drawable.head_icon_circle_bg6};

    public static cp a(View[] viewArr) {
        if (viewArr == null) {
            return null;
        }
        if (e == null) {
            e = new cp(viewArr, viewArr.length);
        }
        return e;
    }

    private cp(View[] viewArr, int i) {
        this.f3160a = viewArr;
        this.b = i;
    }

    /* access modifiers changed from: protected */
    public void applyTransformation(float f, Transformation transformation) {
        int i = (int) (1000.0f * f);
        if (0 != getDuration()) {
            a(((int) (((long) i) / (getDuration() / ((long) d)))) + 1);
        }
        super.applyTransformation(f, transformation);
    }

    private void a(int i) {
        for (int i2 = 0; i2 < this.b; i2++) {
            this.f3160a[(this.b - 1) - i2].setBackgroundResource(this.c[(i2 + i) % this.b]);
        }
    }
}
