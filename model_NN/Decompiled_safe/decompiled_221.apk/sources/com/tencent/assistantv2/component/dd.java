package com.tencent.assistantv2.component;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.activity.HelperFeedbackActivity;

/* compiled from: ProGuard */
class dd implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SecondNavigationTitleViewV5 f3173a;

    dd(SecondNavigationTitleViewV5 secondNavigationTitleViewV5) {
        this.f3173a = secondNavigationTitleViewV5;
    }

    public void onClick(View view) {
        this.f3173a.b.startActivity(new Intent(this.f3173a.b, HelperFeedbackActivity.class));
    }
}
