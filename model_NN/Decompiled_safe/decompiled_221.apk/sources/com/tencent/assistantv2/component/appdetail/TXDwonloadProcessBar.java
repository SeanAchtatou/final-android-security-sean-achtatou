package com.tencent.assistantv2.component.appdetail;

import android.content.Context;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.manager.f;
import com.tencent.assistant.manager.g;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.utils.ba;

/* compiled from: ProGuard */
public class TXDwonloadProcessBar extends FrameLayout implements UIEventListener, g {

    /* renamed from: a  reason: collision with root package name */
    ProgressBar f3059a;
    View[] b;
    /* access modifiers changed from: private */
    public SimpleAppModel c;

    /* compiled from: ProGuard */
    public enum InfoType {
        STAR_DOWNTIMES_SIZE,
        CATEGORY_SIZE
    }

    public TXDwonloadProcessBar(Context context) {
        this(context, null);
        b();
    }

    public TXDwonloadProcessBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        b();
    }

    private void c(SimpleAppModel simpleAppModel) {
        this.c = simpleAppModel;
        a(this.c.t());
        f.a().a(this.c.q(), this);
    }

    public void a(SimpleAppModel simpleAppModel, View view) {
        this.b = new View[]{view};
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams) getLayoutParams();
        layoutParams2.height = layoutParams.height;
        setLayoutParams(layoutParams2);
        c(simpleAppModel);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
    }

    public void a(SimpleAppModel simpleAppModel, View[] viewArr) {
        this.b = viewArr;
        c(simpleAppModel);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
    }

    /* access modifiers changed from: private */
    public void a(AppConst.AppState appState) {
        int i;
        DownloadInfo d = DownloadProxy.a().d(this.c.q());
        if (d != null) {
            i = d.getProgress();
        } else {
            i = 0;
        }
        if (appState == AppConst.AppState.DOWNLOADING || appState == AppConst.AppState.PAUSED || appState == AppConst.AppState.FAIL || appState == AppConst.AppState.QUEUING) {
            a(0);
            this.f3059a.setProgress(i);
            return;
        }
        a(8);
    }

    public void a(SimpleAppModel simpleAppModel) {
        AppConst.AppState t = this.c.t();
        if (t == AppConst.AppState.DOWNLOAD || t == AppConst.AppState.DOWNLOADED || t == AppConst.AppState.INSTALLED) {
            a(8);
        } else {
            a(0);
        }
    }

    public void b(SimpleAppModel simpleAppModel) {
        AppConst.AppState t = this.c.t();
        if (t == AppConst.AppState.DOWNLOAD || t == AppConst.AppState.DOWNLOADED || t == AppConst.AppState.INSTALLED) {
            a(8);
        } else {
            a(0);
        }
    }

    private void b() {
        this.f3059a = (ProgressBar) inflate(getContext(), R.layout.download_progress_bar_layout, this).findViewById(R.id.down_progress);
    }

    private void a(int i) {
        if (i == 8) {
            setVisibility(8);
            if (this.b != null) {
                for (View view : this.b) {
                    if (view != null) {
                        view.setVisibility(0);
                    }
                }
            }
        } else if (i == 0) {
            setVisibility(0);
            if (this.b != null) {
                for (View view2 : this.b) {
                    if (view2 != null) {
                        view2.setVisibility(8);
                    }
                }
            }
        }
    }

    public void a() {
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
    }

    public void onAppStateChange(String str, AppConst.AppState appState) {
        if (str != null && this.c != null && str.equals(this.c.q())) {
            ba.a().post(new ae(this, str, appState));
        }
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE:
                if (message.obj instanceof DownloadInfo) {
                    DownloadInfo downloadInfo = (DownloadInfo) message.obj;
                    if (this.f3059a != null && downloadInfo != null && this.c != null && downloadInfo.downloadTicket != null && this.c.q().equals(downloadInfo.downloadTicket)) {
                        c(this.c);
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }
}
