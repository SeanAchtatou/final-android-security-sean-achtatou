package com.tencent.assistantv2.component.categorydetail;

import com.tencent.assistant.model.e;
import com.tencent.assistant.module.callback.c;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.protocol.jce.TagGroup;
import com.tencent.assistantv2.component.banner.f;
import java.util.List;

/* compiled from: ProGuard */
class b implements c {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CategoryDetailListView f3142a;

    b(CategoryDetailListView categoryDetailListView) {
        this.f3142a = categoryDetailListView;
    }

    public void a(int i, int i2, boolean z, List<e> list, List<TagGroup> list2) {
        if (list != null) {
            if (this.f3142a.b == null) {
                this.f3142a.d();
            }
            this.f3142a.b.a(z, list, (List<f>) null, (List<ColorCardItem>) null);
        }
        if (!this.f3142a.e.a()) {
            if (list2 != null && !list2.isEmpty()) {
                this.f3142a.e.a(list2, this.f3142a.f);
                this.f3142a.a(list2);
            }
            this.f3142a.b(i, i2, z);
            return;
        }
        this.f3142a.a(i, i2, z);
    }
}
