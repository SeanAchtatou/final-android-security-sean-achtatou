package com.tencent.assistantv2.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Message;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.android.qqdownloader.b;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.manager.f;
import com.tencent.assistant.manager.g;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.d;
import com.tencent.assistant.model.h;
import com.tencent.assistant.module.u;
import com.tencent.assistant.module.wisedownload.r;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.bt;
import com.tencent.assistant.utils.installuninstall.InstallUninstallTaskBean;
import com.tencent.assistantv2.component.fps.FPSImageView;
import com.tencent.assistantv2.component.fps.FPSProgressBar;
import com.tencent.assistantv2.component.fps.FPSRatingView;
import com.tencent.assistantv2.component.fps.FPSTextView;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class ListItemInfoView extends RelativeLayout implements UIEventListener, g {

    /* renamed from: a  reason: collision with root package name */
    FPSRatingView f2997a;
    FPSTextView b;
    FPSTextView c;
    FPSTextView d;
    FPSImageView e;
    FPSProgressBar f;
    TextView g;
    TextView h;
    ViewStub i;
    ViewGroup j;
    FPSTextView k;
    FPSTextView l;
    private aw m;
    private InfoType n;
    /* access modifiers changed from: private */
    public SimpleAppModel o;
    private boolean p;
    private MovingProgressBar q;
    private int r;
    private int s;
    private boolean t;

    /* compiled from: ProGuard */
    public enum InfoType {
        STAR_DOWNTIMES_SIZE,
        CATEGORY_SIZE,
        UPDATE_INFO,
        STAR_CATEGORY_SIZE,
        DOWNLOAD_PROGRESS_ONLY,
        ONEMORE_DESC,
        DOWNTIMES_SIZE,
        NO_APPINFO
    }

    public ListItemInfoView(Context context) {
        this(context, null);
    }

    public ListItemInfoView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.n = InfoType.STAR_DOWNTIMES_SIZE;
        this.p = true;
        this.r = -1;
        this.s = -1;
        this.t = true;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, b.i);
        a();
        if (obtainStyledAttributes != null) {
            int i2 = obtainStyledAttributes.getInt(1, 0);
            if (i2 == InfoType.CATEGORY_SIZE.ordinal()) {
                a(InfoType.CATEGORY_SIZE);
            } else if (i2 == InfoType.UPDATE_INFO.ordinal()) {
                a(InfoType.UPDATE_INFO);
            } else if (i2 == InfoType.STAR_CATEGORY_SIZE.ordinal()) {
                a(InfoType.STAR_CATEGORY_SIZE);
            } else if (i2 == InfoType.DOWNLOAD_PROGRESS_ONLY.ordinal()) {
                a(InfoType.DOWNLOAD_PROGRESS_ONLY);
            } else if (i2 == InfoType.DOWNTIMES_SIZE.ordinal()) {
                a(InfoType.DOWNTIMES_SIZE);
            } else if (i2 == InfoType.NO_APPINFO.ordinal()) {
                a(InfoType.NO_APPINFO);
            } else {
                a(InfoType.STAR_DOWNTIMES_SIZE);
            }
            obtainStyledAttributes.recycle();
        }
    }

    public void a(SimpleAppModel simpleAppModel) {
        if (simpleAppModel != null) {
            this.o = simpleAppModel;
            a(u.e(simpleAppModel));
            f.a().a(this.o.q(), this);
        }
    }

    public void a(SimpleAppModel simpleAppModel, d dVar) {
        if (simpleAppModel != null) {
            if (dVar == null) {
                dVar = u.e(simpleAppModel);
            }
            this.o = simpleAppModel;
            a(dVar);
            f.a().a(dVar.f1661a, this);
        }
    }

    public void a(InfoType infoType) {
        this.t = true;
        this.n = infoType;
        if (this.n == InfoType.CATEGORY_SIZE) {
            a(false);
        } else if (this.n == InfoType.UPDATE_INFO) {
            a(false);
            this.b.setVisibility(8);
            View inflate = this.i.inflate();
            this.k = (FPSTextView) findViewById(R.id.old_version_tv);
            this.l = (FPSTextView) findViewById(R.id.new_version_tv);
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.d.getLayoutParams();
            layoutParams.addRule(1, R.id.old_new_version_layout);
            this.d.setLayoutParams(layoutParams);
            this.j = (LinearLayout) inflate.findViewById(R.id.old_new_version_layout);
            RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.j.getLayoutParams();
            layoutParams2.addRule(1, 0);
            this.j.setLayoutParams(layoutParams2);
        } else if (this.n == InfoType.STAR_CATEGORY_SIZE) {
            a(true);
        } else if (this.n == InfoType.DOWNTIMES_SIZE) {
            a(false);
        } else if (this.n == InfoType.ONEMORE_DESC) {
            a(false);
        } else if (this.n == InfoType.NO_APPINFO) {
            a(8);
        } else if (this.n == InfoType.DOWNLOAD_PROGRESS_ONLY) {
            a(false);
        }
    }

    private void a(boolean z) {
        this.p = z;
        if (!this.p) {
            this.f2997a.setVisibility(8);
        } else {
            this.f2997a.setVisibility(0);
        }
    }

    /* access modifiers changed from: private */
    public void a(d dVar) {
        DownloadInfo downloadInfo;
        int i2;
        String valueOf;
        String valueOf2;
        DownloadInfo downloadInfo2 = null;
        this.t = true;
        this.o.s();
        h hVar = this.o.aq;
        if (hVar != null) {
            this.b.setText(hVar.b);
        }
        if (getVisibility() != 0) {
            setVisibility(0);
        }
        AppConst.AppState appState = dVar != null ? dVar.c : AppConst.AppState.ILLEGAL;
        ax axVar = new ax(this, null);
        switch (av.f3094a[appState.ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
                String str = Constants.STR_EMPTY;
                if (dVar != null) {
                    downloadInfo2 = dVar.b;
                }
                if (downloadInfo2 == null) {
                    downloadInfo = DownloadProxy.a().d(this.o.q());
                } else {
                    downloadInfo = downloadInfo2;
                }
                if (downloadInfo != null) {
                    i2 = downloadInfo.getProgress();
                    if (downloadInfo.response != null) {
                        str = downloadInfo.response.c;
                    }
                } else {
                    i2 = 0;
                }
                b(axVar, 0);
                a(axVar, 8);
                axVar.i = 8;
                if (this.m != null) {
                    this.m.a(8);
                }
                this.f.setProgress(i2);
                this.f.setSecondaryProgress(0);
                if (downloadInfo != null) {
                    if (downloadInfo.isSllUpdate()) {
                        this.h.setText(bt.a(downloadInfo.sllFileSize));
                    } else {
                        this.h.setText(bt.a(downloadInfo.fileSize));
                    }
                }
                if (appState == AppConst.AppState.PAUSED || appState == AppConst.AppState.FAIL) {
                    this.g.setText(getContext().getResources().getString(R.string.down_page_pause));
                } else if (appState == AppConst.AppState.QUEUING) {
                    this.g.setText(String.format(getContext().getResources().getString(R.string.down_page_queuing), str));
                } else {
                    this.g.setText(String.format(getContext().getResources().getString(R.string.down_page_downloading), str));
                }
                c();
                break;
            case 5:
                b();
                b(axVar, 8);
                a(axVar, 8);
                this.g.setText(getContext().getResources().getString(R.string.install_show_message));
                axVar.g = 0;
                axVar.h = 8;
                break;
            default:
                c();
                b(axVar, 8);
                if (this.n == InfoType.NO_APPINFO) {
                    a(axVar, 8);
                } else {
                    a(axVar, 0);
                }
                if (this.m != null) {
                    this.m.a(0);
                }
                axVar.i = 0;
                if (this.n == InfoType.CATEGORY_SIZE || this.n == InfoType.STAR_CATEGORY_SIZE) {
                    try {
                        this.b.setText(this.o.S);
                    } catch (Exception e2) {
                    }
                } else if (this.n == InfoType.DOWNLOAD_PROGRESS_ONLY) {
                    setVisibility(8);
                } else if (this.n == InfoType.UPDATE_INFO) {
                    if (TextUtils.isEmpty(this.o.f) || TextUtils.isEmpty(this.o.E)) {
                        valueOf = String.valueOf(this.o.g);
                        valueOf2 = String.valueOf(this.o.D);
                    } else if (this.o.f.equals(this.o.E)) {
                        valueOf = this.o.f + "_" + this.o.g;
                        valueOf2 = this.o.E + "_" + this.o.D;
                    } else {
                        valueOf = this.o.f;
                        valueOf2 = this.o.E;
                    }
                    String a2 = a(7, valueOf2);
                    String a3 = a(7, valueOf);
                    this.k.setText(a2);
                    this.l.setText(a3);
                } else {
                    this.b.setText(this.o.aM.f1669a);
                    if (this.o.a()) {
                        axVar.b = 8;
                    } else {
                        axVar.b = 0;
                    }
                }
                DownloadInfo downloadInfo3 = dVar == null ? null : dVar.b;
                if ((r.b(downloadInfo3) || r.a(downloadInfo3)) && (dVar == null || dVar.c != AppConst.AppState.INSTALLED)) {
                    this.c.setText((int) R.string.update_info_zero);
                    axVar.e = 0;
                    axVar.d = 0;
                    this.d.setText(this.o.aM.b);
                    if (getContext() != null) {
                        this.c.setTextColor(getContext().getResources().getColor(R.color.apk_zero_size));
                    }
                } else if (!this.o.a() || (dVar != null && dVar.c == AppConst.AppState.INSTALLED)) {
                    axVar.e = 8;
                    axVar.d = 8;
                    this.c.setText(this.o.aM.b);
                    if (getContext() != null) {
                        this.c.setTextColor(getContext().getResources().getColor(R.color.common_listiteminfo));
                    }
                } else {
                    axVar.e = 0;
                    axVar.d = 0;
                    this.d.setText(this.o.aM.b);
                    this.c.setText(this.o.aM.c);
                    if (getContext() != null) {
                        this.c.setTextColor(getContext().getResources().getColor(R.color.apk_truesize));
                    }
                }
                if (this.n == InfoType.NO_APPINFO) {
                    axVar.b = 8;
                    break;
                }
                break;
        }
        if (this.p) {
            this.f2997a.setRating(this.o.q);
        } else {
            axVar.f3095a = 8;
        }
        a(axVar);
    }

    private String a(int i2, String str) {
        if (TextUtils.isEmpty(str)) {
            return Constants.STR_EMPTY;
        }
        if (str.length() <= i2) {
            return str;
        }
        if (i2 <= "..".length()) {
            return str.substring(0, i2);
        }
        return str.substring(0, (i2 - 1) - "..".length()) + ".." + str.substring(str.length() - 2, str.length());
    }

    public void a(aw awVar) {
        this.m = awVar;
    }

    private void a() {
        View inflate = inflate(getContext(), R.layout.list_item_progress_layout, this);
        this.f2997a = (FPSRatingView) inflate.findViewById(R.id.app_ratingview);
        this.b = (FPSTextView) inflate.findViewById(R.id.download_times_txt);
        this.c = (FPSTextView) inflate.findViewById(R.id.app_size_text);
        this.d = (FPSTextView) inflate.findViewById(R.id.app_true_size_text);
        this.e = (FPSImageView) inflate.findViewById(R.id.app_size_redline);
        this.f = (FPSProgressBar) inflate.findViewById(R.id.down_progress);
        this.g = (TextView) inflate.findViewById(R.id.speed);
        this.h = (TextView) inflate.findViewById(R.id.percent);
        this.i = (ViewStub) inflate.findViewById(R.id.old_new_version_stub);
        this.q = (MovingProgressBar) inflate.findViewById(R.id.mybtn_listitem_install_cursor);
        this.q.a(getResources().getDimensionPixelSize(R.dimen.app_detail_pic_gap));
        this.q.b(getResources().getDimensionPixelSize(R.dimen.install_progress_bar_width));
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC, this);
        AstApp.i().k().addUIEventListener(1027, this);
    }

    public void onAppStateChange(String str, AppConst.AppState appState) {
        if (str != null && this.o != null && str.equals(this.o.q())) {
            ba.a().post(new au(this, str, u.a(this.o, appState)));
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void handleUIEvent(Message message) {
        d e2;
        if (message.obj instanceof InstallUninstallTaskBean) {
            String str = ((InstallUninstallTaskBean) message.obj).downloadTicket;
            if (!TextUtils.isEmpty(str) && this.o != null && str.equals(this.o.q()) && (e2 = u.e(this.o)) != null) {
                switch (message.what) {
                    case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START:
                        if (this.m != null) {
                            this.m.a(8);
                            break;
                        }
                        break;
                    case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC:
                    case 1027:
                        break;
                    default:
                        return;
                }
                a(e2);
            }
        }
    }

    private void b() {
        this.q.setVisibility(0);
        this.q.a();
    }

    private void c() {
        this.q.b();
        this.q.setVisibility(8);
    }

    public void a(int i2) {
        this.f2997a.setVisibility(i2);
        this.b.setVisibility(i2);
        this.c.setVisibility(i2);
        this.d.setVisibility(i2);
        this.e.setVisibility(i2);
        if (this.j != null) {
            this.j.setVisibility(i2);
        }
    }

    private void a(ax axVar, int i2) {
        axVar.f3095a = i2;
        axVar.b = i2;
        axVar.c = i2;
        axVar.d = i2;
        axVar.i = i2;
        axVar.e = i2;
    }

    private void b(ax axVar, int i2) {
        axVar.g = i2;
        axVar.h = i2;
        axVar.f = i2;
    }

    private void a(ax axVar) {
        if (axVar != null) {
            this.f2997a.setVisibility(axVar.f3095a);
            this.b.setVisibility(axVar.b);
            this.c.setVisibility(axVar.c);
            this.d.setVisibility(axVar.d);
            this.e.setVisibility(axVar.e);
            if (this.j != null) {
                this.j.setVisibility(axVar.i);
            }
            this.f.setVisibility(axVar.f);
            this.g.setVisibility(axVar.g);
            this.h.setVisibility(axVar.h);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        if (FPSTextView.a()) {
            super.onMeasure(i2, i3);
        } else if (this.t || this.r == -1 || this.s == -1 || this.r == 16777215 || this.s == 16777215 || this.s > 200) {
            super.onMeasure(i2, i3);
            this.r = getMeasuredWidth();
            this.s = getMeasuredHeight();
            this.t = false;
        } else {
            setMeasuredDimension(this.r, this.s);
        }
    }
}
