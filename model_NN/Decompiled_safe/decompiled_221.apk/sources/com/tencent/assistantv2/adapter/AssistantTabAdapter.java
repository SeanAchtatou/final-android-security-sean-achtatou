package com.tencent.assistantv2.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.ApkMgrActivity;
import com.tencent.assistant.activity.AppBackupActivity;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.activity.HelperFAQActivity;
import com.tencent.assistant.activity.InstalledAppManagerActivity;
import com.tencent.assistant.activity.PhotoBackupNewActivity;
import com.tencent.assistant.activity.PluginDetailActivity;
import com.tencent.assistant.activity.SpaceCleanActivity;
import com.tencent.assistant.activity.UpdateListActivity;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.manager.as;
import com.tencent.assistant.model.PluginStartEntry;
import com.tencent.assistant.model.j;
import com.tencent.assistant.module.callback.r;
import com.tencent.assistant.module.di;
import com.tencent.assistant.module.dk;
import com.tencent.assistant.module.dm;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.accelerate.PluginAccelerateBridgeActivity;
import com.tencent.assistant.plugin.mgr.c;
import com.tencent.assistant.plugin.mgr.d;
import com.tencent.assistant.plugin.proxy.PluginProxyActivity;
import com.tencent.assistant.plugin.system.PluginBackToBaoReceiver;
import com.tencent.assistant.protocol.jce.ActionUrl;
import com.tencent.assistant.protocol.jce.GetManageInfoListResponse;
import com.tencent.assistant.protocol.jce.MAManageInfo;
import com.tencent.assistant.securescan.StartScanActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.ct;
import com.tencent.assistantv2.activity.AssistantTabActivity;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.connector.ConnectionActivity;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/* compiled from: ProGuard */
public class AssistantTabAdapter extends BaseAdapter implements UIEventListener, r, dk {
    private static final String i = AstApp.i().getPackageName();
    private static HashMap<String, Integer> l = new HashMap<>();

    /* renamed from: a  reason: collision with root package name */
    List<c> f2867a = Collections.synchronizedList(new LinkedList());
    protected boolean b = false;
    protected boolean c = false;
    private String d = "AssistantTabAdapter";
    private Context e;
    private LayoutInflater f;
    private AstApp g = AstApp.i();
    private List<PluginStartEntry> h = new ArrayList();
    private Random j = new Random(System.currentTimeMillis());
    private b k = null;

    /* compiled from: ProGuard */
    public enum ItemPositionType {
        TOP,
        MID,
        BOTTOM,
        SINGLE
    }

    static {
        l.put("com.tencent.android.qqdownloader_com.tencent.assistant.activity.InstalledAppManagerActivity", Integer.valueOf((int) R.drawable.helper_cardicon_app));
        l.put("com.tencent.android.qqdownloader_com.tencent.assistant.activity.ApkMgrActivity", Integer.valueOf((int) R.drawable.helper_cardicon_apkmgr));
        l.put("com.tencent.android.qqdownloader_com.tencent.assistant.activity.SpaceCleanActivity", Integer.valueOf((int) R.drawable.helper_cardicon_qingli));
        l.put("com.assistant.accelerate_com.assistant.accelerate.MobileAccelerateActivity", Integer.valueOf((int) R.drawable.helper_cardicon_speed));
        l.put("com.tencent.android.qqdownloader_com.tencent.assistant.securescan.StartScanActivity", Integer.valueOf((int) R.drawable.helper_cardicon_safescan));
        l.put("com.tencent.android.qqdownloader_com.tencent.assistant.activity.AppBackupActivity", Integer.valueOf((int) R.drawable.helper_cardicon_backup));
        l.put("com.tencent.android.qqdownloader_com.tencent.assistant.activity.PhotoBackupNewActivity", Integer.valueOf((int) R.drawable.helper_cardicon_pic));
        l.put("com.tencent.android.qqdownloader_com.tencent.connector.ConnectionActivity", Integer.valueOf((int) R.drawable.helper_cardicon_pcline));
        l.put("com.tencent.mobileassistant_wifitransfer_com.tencent.assistant.activity.WifiTransferActivity", Integer.valueOf((int) R.drawable.helper_cardicon_tran));
        l.put("com.tencent.assistant.freewifi_com.tencent.assistant.freewifi.FreewifiActivity", Integer.valueOf((int) R.drawable.helper_cardicon_freewifi));
    }

    public AssistantTabAdapter(Context context) {
        this.e = context;
        this.f = LayoutInflater.from(context);
        e();
    }

    private void e() {
        this.f2867a.clear();
        a();
        GetManageInfoListResponse d2 = as.w().d();
        if (d2 == null || d2.b == null || d2.b.size() < 5) {
            c cVar = new c(this);
            cVar.h = "uninstall";
            cVar.i = R.drawable.helper_cardicon_app;
            cVar.j = R.string.soft_admin;
            this.f2867a.add(cVar);
            c cVar2 = new c(this);
            cVar2.h = "apk_mgr";
            cVar2.i = R.drawable.helper_cardicon_apkmgr;
            cVar2.j = R.string.apkmgr_title;
            this.f2867a.add(cVar2);
            c cVar3 = new c(this);
            cVar3.h = "rubbish";
            cVar3.i = R.drawable.helper_cardicon_qingli;
            cVar3.j = R.string.rubbish_clear;
            this.f2867a.add(cVar3);
            c cVar4 = new c(this);
            cVar4.h = "applist_backup";
            cVar4.i = R.drawable.helper_cardicon_backup;
            cVar4.j = R.string.app_backup;
            this.f2867a.add(cVar4);
            c cVar5 = new c(this);
            cVar5.h = "secure_scan";
            cVar5.i = R.drawable.helper_cardicon_safescan;
            cVar5.j = R.string.secure_scan;
            this.f2867a.add(cVar5);
            c cVar6 = new c(this);
            cVar6.h = "connect_pc";
            cVar6.i = R.drawable.helper_cardicon_pcline;
            cVar6.j = R.string.cmn_to_pc;
            this.f2867a.add(cVar6);
            c cVar7 = new c(this);
            cVar7.h = "photo_backup";
            cVar7.i = R.drawable.helper_cardicon_pic;
            cVar7.j = R.string.pic_bak;
            this.f2867a.add(cVar7);
        } else {
            ArrayList<MAManageInfo> arrayList = d2.b;
            int i2 = 0;
            for (int i3 = 0; i3 < arrayList.size(); i3++) {
                MAManageInfo mAManageInfo = arrayList.get(i3);
                if (!(mAManageInfo == null || (mAManageInfo.c == 0 && dm.a().b(mAManageInfo.f2232a) == null))) {
                    c cVar8 = new c(this);
                    cVar8.b = mAManageInfo.c;
                    cVar8.h = "item_entry_" + i2;
                    cVar8.c = mAManageInfo.f2232a;
                    cVar8.f2888a = mAManageInfo.b;
                    cVar8.d = mAManageInfo.d;
                    cVar8.e = mAManageInfo.e;
                    cVar8.f = mAManageInfo.f;
                    cVar8.g = mAManageInfo.g;
                    this.f2867a.add(i2, cVar8);
                    i2++;
                }
            }
        }
        if (this.f2867a.size() % 3 != 0) {
            int size = 3 - (this.f2867a.size() % 3);
            for (int i4 = 0; i4 < size; i4++) {
                c cVar9 = new c(this);
                cVar9.h = "empty";
                cVar9.i = -1;
                cVar9.j = R.string.p_empty_str;
                this.f2867a.add(cVar9);
            }
        }
    }

    public void a() {
        this.h.clear();
        List<j> b2 = dm.a().b();
        if (b2 != null && b2.size() > 0) {
            for (j next : b2) {
                if (!TextUtils.isEmpty(next.p)) {
                    if ("p.com.tencent.assistant.activity.PhotoBackupNewActivity".equals(next.p) || PhotoBackupNewActivity.class.getName().equals(next.p)) {
                        next.c = AstApp.i().getPackageName();
                        next.p = PhotoBackupNewActivity.class.getName();
                    } else if ("p.com.tencent.connector.ConnectionActivity".equals(next.p) || ConnectionActivity.class.getName().equals(next.p)) {
                        next.c = AstApp.i().getPackageName();
                        next.p = ConnectionActivity.class.getName();
                    }
                    this.h.add(new PluginStartEntry(next.f1666a, next.e, next.c, next.d, next.p, next.h));
                }
            }
        }
    }

    public int getCount() {
        if (this.f2867a != null) {
            return this.f2867a.size();
        }
        return 0;
    }

    public Object getItem(int i2) {
        if (this.f2867a == null || this.f2867a.size() <= i2) {
            return null;
        }
        return this.f2867a.get(i2);
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x007b A[Catch:{ Throwable -> 0x008e }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View getView(int r6, android.view.View r7, android.view.ViewGroup r8) {
        /*
            r5 = this;
            java.lang.Object r0 = r5.getItem(r6)
            com.tencent.assistantv2.adapter.c r0 = (com.tencent.assistantv2.adapter.c) r0
            if (r7 == 0) goto L_0x0016
            java.lang.Object r1 = r7.getTag()     // Catch:{ Throwable -> 0x008e }
            if (r1 == 0) goto L_0x0016
            java.lang.Object r1 = r7.getTag()     // Catch:{ Throwable -> 0x008e }
            boolean r1 = r1 instanceof com.tencent.assistantv2.adapter.d     // Catch:{ Throwable -> 0x008e }
            if (r1 != 0) goto L_0x0085
        L_0x0016:
            com.tencent.assistantv2.adapter.d r2 = new com.tencent.assistantv2.adapter.d     // Catch:{ Throwable -> 0x008e }
            r2.<init>(r5)     // Catch:{ Throwable -> 0x008e }
            android.view.LayoutInflater r1 = r5.f     // Catch:{ Throwable -> 0x008e }
            r3 = 2130903259(0x7f0300db, float:1.741333E38)
            r4 = 0
            android.view.View r7 = r1.inflate(r3, r4)     // Catch:{ Throwable -> 0x008e }
            r1 = 2131165569(0x7f070181, float:1.7945359E38)
            android.view.View r1 = r7.findViewById(r1)     // Catch:{ Throwable -> 0x008e }
            r2.f2889a = r1     // Catch:{ Throwable -> 0x008e }
            r1 = 2131165374(0x7f0700be, float:1.7944963E38)
            android.view.View r1 = r7.findViewById(r1)     // Catch:{ Throwable -> 0x008e }
            com.tencent.assistant.component.txscrollview.TXImageView r1 = (com.tencent.assistant.component.txscrollview.TXImageView) r1     // Catch:{ Throwable -> 0x008e }
            r2.b = r1     // Catch:{ Throwable -> 0x008e }
            r1 = 2131165375(0x7f0700bf, float:1.7944965E38)
            android.view.View r1 = r7.findViewById(r1)     // Catch:{ Throwable -> 0x008e }
            android.widget.TextView r1 = (android.widget.TextView) r1     // Catch:{ Throwable -> 0x008e }
            r2.c = r1     // Catch:{ Throwable -> 0x008e }
            r1 = 2131166170(0x7f0703da, float:1.7946578E38)
            android.view.View r1 = r7.findViewById(r1)     // Catch:{ Throwable -> 0x008e }
            android.view.ViewStub r1 = (android.view.ViewStub) r1     // Catch:{ Throwable -> 0x008e }
            r2.d = r1     // Catch:{ Throwable -> 0x008e }
            r1 = 2131166171(0x7f0703db, float:1.794658E38)
            android.view.View r1 = r7.findViewById(r1)     // Catch:{ Throwable -> 0x008e }
            android.widget.ImageView r1 = (android.widget.ImageView) r1     // Catch:{ Throwable -> 0x008e }
            r2.e = r1     // Catch:{ Throwable -> 0x008e }
            r1 = 2131166172(0x7f0703dc, float:1.7946582E38)
            android.view.View r1 = r7.findViewById(r1)     // Catch:{ Throwable -> 0x008e }
            android.widget.TextView r1 = (android.widget.TextView) r1     // Catch:{ Throwable -> 0x008e }
            r2.f = r1     // Catch:{ Throwable -> 0x008e }
            r7.setTag(r2)     // Catch:{ Throwable -> 0x008e }
            r1 = r7
        L_0x0069:
            r5.a(r2, r0, r6)     // Catch:{ Throwable -> 0x008e }
            r5.c(r6)     // Catch:{ Throwable -> 0x008e }
            r2 = 2131165261(0x7f07004d, float:1.7944734E38)
            java.lang.String r3 = r5.a(r6)     // Catch:{ Throwable -> 0x008e }
            r1.setTag(r2, r3)     // Catch:{ Throwable -> 0x008e }
            if (r0 == 0) goto L_0x0083
            r2 = 2131165266(0x7f070052, float:1.7944744E38)
            java.lang.String r0 = r0.h     // Catch:{ Throwable -> 0x008e }
            r1.setTag(r2, r0)     // Catch:{ Throwable -> 0x008e }
        L_0x0083:
            r0 = r1
        L_0x0084:
            return r0
        L_0x0085:
            java.lang.Object r1 = r7.getTag()     // Catch:{ Throwable -> 0x008e }
            com.tencent.assistantv2.adapter.d r1 = (com.tencent.assistantv2.adapter.d) r1     // Catch:{ Throwable -> 0x008e }
            r2 = r1
            r1 = r7
            goto L_0x0069
        L_0x008e:
            r0 = move-exception
            android.view.View r0 = new android.view.View
            com.qq.AppService.AstApp r1 = com.qq.AppService.AstApp.i()
            r0.<init>(r1)
            goto L_0x0084
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistantv2.adapter.AssistantTabAdapter.getView(int, android.view.View, android.view.ViewGroup):android.view.View");
    }

    private void a(d dVar, c cVar, int i2) {
        if (dVar != null && cVar != null) {
            if ("empty".equals(cVar.h)) {
                dVar.f2889a.setEnabled(false);
                dVar.f2889a.setFocusable(true);
            } else {
                dVar.f2889a.setEnabled(true);
                dVar.f2889a.setFocusable(false);
            }
            if (cVar.i > 0) {
                dVar.b.setVisibility(0);
                dVar.b.setImageDrawableAndResetImageUrlString(this.e.getResources().getDrawable(cVar.i));
                dVar.c.setText(this.e.getString(cVar.j));
            } else if (cVar.i == -1) {
                dVar.b.setVisibility(8);
                dVar.c.setText(this.e.getString(cVar.j));
            } else {
                Integer num = l.get((cVar.c != null ? cVar.c : Constants.STR_EMPTY) + "_" + (cVar.d != null ? cVar.d : Constants.STR_EMPTY));
                dVar.b.updateImageView(cVar.f, num != null ? num.intValue() : R.drawable.icon_default_plugin, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
                dVar.b.setVisibility(0);
                dVar.c.setText(cVar.f2888a);
            }
            a(dVar, i2);
            if (a(cVar)) {
                a(dVar, cVar);
            } else if (dVar.e != null && dVar.e.getVisibility() != 8) {
                dVar.e.setVisibility(8);
            }
        }
    }

    private void a(d dVar, int i2) {
        if (i2 == 0) {
            try {
                dVar.f2889a.setBackgroundResource(R.drawable.card_upleft_selector);
            } catch (Throwable th) {
            }
        } else if (i2 == 1) {
            dVar.f2889a.setBackgroundResource(R.drawable.card_upmid_selector);
        } else if (i2 == 2) {
            dVar.f2889a.setBackgroundResource(R.drawable.card_upright_selector);
        } else if (i2 == getCount() - 1) {
            dVar.f2889a.setBackgroundResource(R.drawable.card_downright_selector);
        } else if (i2 == getCount() - 2) {
            dVar.f2889a.setBackgroundResource(R.drawable.card_downmid_selector);
        } else if (i2 == getCount() - 3) {
            dVar.f2889a.setBackgroundResource(R.drawable.card_downleft_selector);
        } else if (i2 % 3 == 0) {
            dVar.f2889a.setBackgroundResource(R.drawable.card_midleft_selector);
        } else if (i2 % 3 == 1) {
            dVar.f2889a.setBackgroundResource(R.drawable.card_midmid_selector);
        } else if (i2 % 3 == 2) {
            dVar.f2889a.setBackgroundResource(R.drawable.card_midright_selector);
        }
    }

    private boolean a(c cVar) {
        if (cVar == null || cVar.h == null) {
            return false;
        }
        if ("connect_pc".equals(cVar.h)) {
            return true;
        }
        if (cVar.b != 0 || !"p.com.tencent.android.qqdownloader".equals(cVar.c) || !"p.com.tencent.connector.ConnectionActivity".equals(cVar.d)) {
            return false;
        }
        return true;
    }

    private boolean a(String str) {
        c cVar;
        if (TextUtils.isEmpty(str) || !str.startsWith("item_entry_") || (cVar = this.f2867a.get(ct.a(str.substring("item_entry_".length()), -1))) == null || !"com.assistant.accelerate".equals(cVar.c) || !"com.assistant.accelerate.MobileAccelerateActivity".equals(cVar.d)) {
            return false;
        }
        return true;
    }

    public void c() {
        this.g.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_GET_USERINFO_SUCCESS, this);
        this.g.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_GET_USERINFO_FAIL, this);
        this.g.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_CONNECTOR_STATE_CHANGE, this);
        this.g.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_EXTRACT_FINISH, this);
        this.g.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_SUCC, this);
        dm.a().unregister(this);
        di.a().b(this);
    }

    public void d() {
        dm.a().register(this);
        di.a().a(this);
        this.g.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD, this);
        this.g.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        this.g.k().addUIEventListener(1016, this);
        this.g.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UPDATE_LIST_CHANGED, this);
        this.g.k().addUIEventListener(1019, this);
        this.g.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UPDATE_IGNORE_LIST_DELETE, this);
        this.g.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_START, this);
        this.g.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_DELETE, this);
        this.g.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_CONNECTOR_STATE_CHANGE, this);
        this.g.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_EXTRACT_FINISH, this);
        this.g.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_SUCC, this);
        notifyDataSetChanged();
        Intent intent = new Intent();
        intent.setAction("com.tencent.android.qqdownloader.action.QUERY_CONNECT_STATE");
        AstApp.i().sendBroadcast(intent);
    }

    public void a(long j2) {
        ba.a().post(new a(this));
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE:
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD:
            case 1016:
            case EventDispatcherEnum.UI_EVENT_APP_UPDATE_LIST_CHANGED:
            case 1019:
            case EventDispatcherEnum.UI_EVENT_APP_UPDATE_IGNORE_LIST_DELETE:
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_START:
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_DELETE:
            case EventDispatcherEnum.UI_EVENT_GET_USERINFO_SUCCESS:
            case EventDispatcherEnum.UI_EVENT_GET_USERINFO_FAIL:
            case EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_SUCC:
            case EventDispatcherEnum.UI_EVENT_PLUGIN_CONNECTOR_STATE_CHANGE:
            case EventDispatcherEnum.UI_EVENT_PLUGIN_EXTRACT_FINISH:
                ba.a().post(new b(this));
                return;
            case EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS:
                Bundle bundle = (Bundle) message.obj;
                if (bundle != null && bundle.containsKey("pageId") && bundle.containsKey("tag")) {
                    a(bundle.getInt("pageId"), bundle.getString("tag"));
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void a(d dVar, c cVar) {
        if (dVar != null && cVar != null) {
            switch (PluginBackToBaoReceiver.b()) {
                case 0:
                    dVar.c.setText((int) R.string.cmn_to_pc);
                    return;
                case 1:
                    if (TextUtils.isEmpty(PluginBackToBaoReceiver.a())) {
                        dVar.c.setText((int) R.string.cmn_to_pc);
                        return;
                    } else {
                        dVar.c.setText(this.e.getString(R.string.label_connection_established) + PluginBackToBaoReceiver.a());
                        return;
                    }
                case 2:
                case 3:
                    if (TextUtils.isEmpty(PluginBackToBaoReceiver.a())) {
                        dVar.c.setText((int) R.string.cmn_to_pc);
                        return;
                    } else {
                        dVar.c.setText(this.e.getString(R.string.label_connection_established) + PluginBackToBaoReceiver.a());
                        return;
                    }
                default:
                    return;
            }
        }
    }

    public void a(int i2, String str, View view) {
        if (a(str) && view.getTag() != null) {
            d dVar = (d) view.getTag();
            if (!(dVar.e == null || dVar.e.getVisibility() == 8)) {
                dVar.e.setVisibility(8);
            }
            PluginAccelerateBridgeActivity.a(this.e);
        }
        a(i2, str);
    }

    public void a(int i2, String str) {
        c cVar;
        int i3;
        if ("update".equals(str)) {
            Intent intent = new Intent(this.e, UpdateListActivity.class);
            intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, i2);
            this.e.startActivity(intent);
        } else if ("file_exchange".equals(str)) {
        } else {
            if ("uninstall".equals(str)) {
                Intent intent2 = new Intent(this.e, InstalledAppManagerActivity.class);
                intent2.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, i2);
                intent2.putExtra("activityTitleName", this.e.getResources().getString(R.string.soft_admin));
                this.e.startActivity(intent2);
            } else if ("help".equals(str)) {
                Intent intent3 = new Intent(this.e, HelperFAQActivity.class);
                intent3.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, i2);
                intent3.putExtra("com.tencent.assistant.BROWSER_URL", "http://maweb.3g.qq.com/help/help.html");
                this.e.startActivity(intent3);
            } else if ("rubbish".equals(str)) {
                Intent intent4 = new Intent(this.e, SpaceCleanActivity.class);
                intent4.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, i2);
                intent4.putExtra("dock_plugin", b("com.assistant.accelerate"));
                this.e.startActivity(intent4);
            } else if ("apk_mgr".equals(str)) {
                Intent intent5 = new Intent(this.e, ApkMgrActivity.class);
                intent5.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, i2);
                this.e.startActivity(intent5);
            } else if (str != null && str.startsWith("item_entry_")) {
                int a2 = ct.a(str.substring("item_entry_".length()), -1);
                if (a2 < 0 || a2 >= this.f2867a.size()) {
                    cVar = null;
                } else {
                    cVar = this.f2867a.get(a2);
                }
                if (cVar == null) {
                    return;
                }
                if (cVar.b == 0) {
                    PluginInfo a3 = d.b().a(cVar.c);
                    PluginStartEntry a4 = a(cVar.c, cVar.d);
                    if (a4 != null) {
                        i3 = a4.b();
                    } else {
                        i3 = 0;
                    }
                    int a5 = c.a(cVar.c);
                    if (a3 != null && (a3.getVersion() >= i3 || a5 >= 0)) {
                        PluginInfo.PluginEntry pluginEntryByStartActivity = a3.getPluginEntryByStartActivity(cVar.d);
                        if (pluginEntryByStartActivity != null) {
                            try {
                                PluginProxyActivity.a(this.e, pluginEntryByStartActivity.getHostPlugInfo().getPackageName(), pluginEntryByStartActivity.getHostPlugInfo().getVersion(), pluginEntryByStartActivity.getStartActivity(), pluginEntryByStartActivity.getHostPlugInfo().getInProcess(), null, pluginEntryByStartActivity.getHostPlugInfo().getLaunchApplication());
                            } catch (Throwable th) {
                                th.printStackTrace();
                            }
                            STInfoV2 sTInfoV2 = new STInfoV2(STConst.ST_PAGE_PLUGIN, STConst.ST_DEFAULT_SLOT, i2, STConst.ST_DEFAULT_SLOT, 100);
                            sTInfoV2.extraData = (a4 != null ? Integer.valueOf(a4.d()) : "0") + "|" + pluginEntryByStartActivity.getHostPlugInfo().getPackageName() + "|" + pluginEntryByStartActivity.getStartActivity() + "|" + 1;
                            k.a(sTInfoV2);
                            return;
                        }
                        Toast.makeText(this.e, (int) R.string.plugin_entry_not_exist, 0).show();
                    } else if (a4 == null || a4.d() <= 0) {
                        Toast.makeText(this.e, (int) R.string.plugin_entry_not_exist, 0).show();
                    } else {
                        try {
                            Intent intent6 = new Intent(this.e, PluginDetailActivity.class);
                            intent6.putExtra("plugin_start_entry", a4);
                            intent6.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, i2);
                            this.e.startActivity(intent6);
                        } catch (Throwable th2) {
                            th2.printStackTrace();
                            if (this.j.nextInt(100) < 10) {
                                throw new RuntimeException(th2);
                            }
                        }
                    }
                } else if (cVar.b != 1) {
                } else {
                    if (cVar.e != null && !TextUtils.isEmpty(cVar.e.f1970a)) {
                        ActionUrl actionUrl = cVar.e;
                        Bundle bundle = new Bundle();
                        bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, i2);
                        bundle.putSerializable("dock_plugin", b("com.assistant.accelerate"));
                        com.tencent.assistant.link.b.a(this.e, bundle, actionUrl);
                    } else if (!TextUtils.isEmpty(cVar.c)) {
                        Intent launchIntentForPackage = this.e.getPackageManager().getLaunchIntentForPackage(cVar.c);
                        launchIntentForPackage.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, i2);
                        if (!TextUtils.isEmpty(cVar.d)) {
                            launchIntentForPackage.setClassName(this.e, cVar.d);
                        }
                        if (!(this.e instanceof Activity)) {
                            launchIntentForPackage.setFlags(268435456);
                        }
                        this.e.startActivity(launchIntentForPackage);
                    }
                }
            } else if ("photo_backup".equals(str)) {
                try {
                    Intent intent7 = new Intent(this.e, Class.forName("com.tencent.assistant.activity.PhotoBackupNewActivity"));
                    intent7.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, i2);
                    intent7.putExtra("activityTitleName", this.e.getResources().getString(R.string.cmn_to_pc));
                    this.e.startActivity(intent7);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            } else if ("connect_pc".equals(str)) {
                try {
                    Intent intent8 = new Intent(this.e, Class.forName("com.tencent.connector.ConnectionActivity"));
                    intent8.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, i2);
                    intent8.putExtra("activityTitleName", this.e.getResources().getString(R.string.cmn_to_pc));
                    this.e.startActivity(intent8);
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
            } else if ("applist_backup".equals(str)) {
                Intent intent9 = new Intent(this.e, AppBackupActivity.class);
                intent9.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, i2);
                this.e.startActivity(intent9);
            } else if ("secure_scan".equals(str)) {
                Intent intent10 = new Intent(this.e, StartScanActivity.class);
                intent10.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, i2);
                this.e.startActivity(intent10);
            }
        }
    }

    private PluginStartEntry b(String str) {
        if (str == null) {
            return null;
        }
        for (PluginStartEntry next : this.h) {
            if (next.a().equals(str)) {
                return next;
            }
        }
        return null;
    }

    private PluginStartEntry a(String str, String str2) {
        if (str == null) {
            return null;
        }
        for (PluginStartEntry next : this.h) {
            if (next.a().equals(str) && next.c() != null && next.c().equals(str2)) {
                return next;
            }
        }
        return null;
    }

    public void notifyDataSetChanged() {
        e();
        super.notifyDataSetChanged();
        if (this.e instanceof AssistantTabActivity) {
            XLog.i(this.d, ">>trigger notifyDataSetChanged>>");
            ((AssistantTabActivity) this.e).j();
        }
    }

    public void b() {
    }

    public void a(List<j> list) {
        notifyDataSetChanged();
    }

    public void b(int i2) {
        Toast.makeText(this.e, (int) R.string.plugin_list_fail, 0).show();
    }

    public String a(int i2) {
        return "07_" + ct.a(i2 + 1);
    }

    private STInfoV2 c(int i2) {
        String a2 = a(i2);
        if (!(this.e instanceof BaseActivity)) {
            return STInfoBuilder.buildSTInfo(this.e, 100);
        }
        if (this.k == null) {
            this.k = new b();
        }
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.e, null, a2, 100, null);
        this.k.exposure(buildSTInfo);
        return buildSTInfo;
    }
}
