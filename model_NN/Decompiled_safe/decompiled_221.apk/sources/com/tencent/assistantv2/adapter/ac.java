package com.tencent.assistantv2.adapter;

import android.view.View;
import android.widget.ImageView;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.component.fps.FPSTextView;

/* compiled from: ProGuard */
class ac {

    /* renamed from: a  reason: collision with root package name */
    View f2877a;
    TXAppIconView b;
    FPSTextView c;
    DownloadButton d;
    ListItemInfoView e;
    ImageView f;
    final /* synthetic */ u g;

    private ac(u uVar) {
        this.g = uVar;
    }

    /* synthetic */ ac(u uVar, v vVar) {
        this(uVar);
    }
}
