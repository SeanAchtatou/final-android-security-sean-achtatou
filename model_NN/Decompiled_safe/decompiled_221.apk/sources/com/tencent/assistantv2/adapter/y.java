package com.tencent.assistantv2.adapter;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.activity.BrowserActivity;
import com.tencent.assistant.model.SimpleAppModel;

/* compiled from: ProGuard */
class y implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f2936a;
    final /* synthetic */ u b;

    y(u uVar, SimpleAppModel simpleAppModel) {
        this.b = uVar;
        this.f2936a = simpleAppModel;
    }

    public void onClick(View view) {
        Intent intent = new Intent(this.b.e, BrowserActivity.class);
        intent.putExtra("com.tencent.assistant.BROWSER_URL", this.f2936a.Z);
        this.b.e.startActivity(intent);
    }
}
