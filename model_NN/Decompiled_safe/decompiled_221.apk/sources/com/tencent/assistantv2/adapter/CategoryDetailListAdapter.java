package com.tencent.assistantv2.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.adapter.c;
import com.tencent.assistant.component.AppIconView;
import com.tencent.assistant.component.appdetail.process.s;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.m;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.d;
import com.tencent.assistant.module.u;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.df;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.model.STCommonInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import java.util.List;

/* compiled from: ProGuard */
public class CategoryDetailListAdapter extends BaseAdapter implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    private static int f2870a = 0;
    private static int b = (f2870a + 1);
    private static int c = (b + 1);
    private boolean d;
    /* access modifiers changed from: private */
    public Context e;
    private LayoutInflater f;
    private List<SimpleAppModel> g;
    /* access modifiers changed from: private */
    public View h;
    private long i;
    private long j;
    private b k;
    private n l;
    private IViewInvalidater m;

    public int getCount() {
        if (this.g == null) {
            return 0;
        }
        return this.g.size();
    }

    public Object getItem(int i2) {
        if (this.g == null) {
            return null;
        }
        return this.g.get(i2);
    }

    public View getView(int i2, View view, ViewGroup viewGroup) {
        SimpleAppModel simpleAppModel;
        if (this.g == null || i2 >= this.g.size()) {
            simpleAppModel = null;
        } else {
            simpleAppModel = this.g.get(i2);
        }
        d e2 = u.e(simpleAppModel);
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.e, simpleAppModel, a.a("06", i2), 100, null, e2);
        if (buildSTInfo != null) {
            buildSTInfo.updateContentId(STCommonInfo.ContentIdType.CATEGORY, this.i + "_" + this.j);
        }
        XLog.d("CategoryDetailListAdapter", "position:" + i2 + " name:" + simpleAppModel.d);
        if (f2870a == getItemViewType(i2)) {
            return a(view, simpleAppModel, i2, buildSTInfo, e2);
        }
        if (b == getItemViewType(i2)) {
            return b(view, simpleAppModel, i2, buildSTInfo, e2);
        }
        return view;
    }

    private View a(View view, SimpleAppModel simpleAppModel, int i2, STInfoV2 sTInfoV2, d dVar) {
        o oVar;
        if (view == null || view.getTag() == null || ((n) view.getTag()).f2898a == null) {
            this.l = new n(this, null);
            oVar = new o(this, null);
            view = this.f.inflate((int) R.layout.category_detail_item, (ViewGroup) null);
            oVar.f2899a = view.findViewById(R.id.empty_view);
            oVar.b = view.findViewById(R.id.app_item);
            oVar.c = view.findViewById(R.id.sortLayout);
            oVar.e = (TextView) view.findViewById(R.id.sort_text);
            oVar.f = (TextView) view.findViewById(R.id.rank_text);
            oVar.g = (TXAppIconView) view.findViewById(R.id.app_icon_img);
            oVar.g.setInvalidater(this.m);
            oVar.h = (TextView) view.findViewById(R.id.app_name_txt);
            oVar.i = (DownloadButton) view.findViewById(R.id.state_app_btn);
            oVar.j = (ListItemInfoView) view.findViewById(R.id.download_info);
            oVar.k = (TextView) view.findViewById(R.id.desc);
            oVar.d = (ImageView) view.findViewById(R.id.sort_num_image);
            oVar.l = (ImageView) view.findViewById(R.id.last_line);
            this.l.f2898a = oVar;
            view.setTag(this.l);
        } else {
            oVar = ((n) view.getTag()).f2898a;
        }
        oVar.b.setOnClickListener(new g(this, simpleAppModel, i2));
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) oVar.g.getLayoutParams();
        RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) oVar.d.getLayoutParams();
        layoutParams.leftMargin = df.a(this.e, 7.0f);
        layoutParams2.leftMargin = 0;
        oVar.c.setVisibility(8);
        oVar.g.setLayoutParams(layoutParams);
        oVar.d.setLayoutParams(layoutParams2);
        if (i2 == 0) {
            oVar.f2899a.setVisibility(0);
            if (t.b > 320) {
                oVar.b.setBackgroundResource(R.drawable.common_cardbg_top_selector);
            } else {
                oVar.b.setBackgroundResource(R.drawable.common_cardbg_middle_selector);
            }
            oVar.l.setImageResource(R.color.common_cutline);
        } else if (i2 == getCount() - 1) {
            oVar.f2899a.setVisibility(8);
            oVar.b.setBackgroundResource(R.drawable.common_cardbg_bottom_selector);
            oVar.l.setImageResource(R.color.transparent);
        } else {
            oVar.f2899a.setVisibility(8);
            oVar.b.setBackgroundResource(R.drawable.common_cardbg_middle_selector);
            oVar.l.setImageResource(R.color.common_cutline);
        }
        a(oVar, simpleAppModel, i2, sTInfoV2, dVar);
        return view;
    }

    private View b(View view, SimpleAppModel simpleAppModel, int i2, STInfoV2 sTInfoV2, d dVar) {
        m mVar;
        if (view == null || ((n) view.getTag()).b == null) {
            this.l = new n(this, null);
            mVar = new m(this, null);
            view = this.f.inflate((int) R.layout.competitive_card_v5, (ViewGroup) null);
            mVar.f2897a = (TextView) view.findViewById(R.id.title);
            mVar.b = (TXImageView) view.findViewById(R.id.pic);
            mVar.d = (ImageView) view.findViewById(R.id.vedio);
            mVar.e = (AppIconView) view.findViewById(R.id.icon);
            mVar.f = (DownloadButton) view.findViewById(R.id.download_soft_btn);
            mVar.g = (ListItemInfoView) view.findViewById(R.id.download_info);
            mVar.h = (TextView) view.findViewById(R.id.name);
            mVar.c = (ImageView) view.findViewById(R.id.sort_num_image);
            mVar.i = (TextView) view.findViewById(R.id.description);
            this.l.b = mVar;
            view.setTag(this.l);
        } else {
            mVar = ((n) view.getTag()).b;
        }
        view.setOnClickListener(new h(this, simpleAppModel, i2));
        a(mVar, simpleAppModel, i2, sTInfoV2, dVar);
        return view;
    }

    public int getItemViewType(int i2) {
        if (!b()) {
            return f2870a;
        }
        SimpleAppModel.CARD_TYPE card_type = this.g.get(i2).U;
        if (SimpleAppModel.CARD_TYPE.NORMAL == card_type) {
            return f2870a;
        }
        if (SimpleAppModel.CARD_TYPE.QUALITY == card_type) {
            return b;
        }
        return f2870a;
    }

    public int getViewTypeCount() {
        return c;
    }

    private void a(o oVar, SimpleAppModel simpleAppModel, int i2, STInfoV2 sTInfoV2, d dVar) {
        if (simpleAppModel != null && oVar != null) {
            oVar.h.setText(simpleAppModel.d);
            if (this.d) {
                if (1 == (((int) (simpleAppModel.B >> 2)) & 3)) {
                    Drawable drawable = this.e.getResources().getDrawable(R.drawable.appdownload_icon_original);
                    drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
                    oVar.h.setCompoundDrawablePadding(df.b(6.0f));
                    oVar.h.setCompoundDrawables(null, null, drawable, null);
                } else {
                    oVar.h.setCompoundDrawables(null, null, null, null);
                }
            }
            a.a(sTInfoV2);
            if (simpleAppModel != null) {
                oVar.g.updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            }
            oVar.i.a(simpleAppModel, dVar);
            oVar.j.a(simpleAppModel, dVar);
            if (s.a(simpleAppModel)) {
                oVar.i.setClickable(false);
            } else {
                oVar.i.setClickable(true);
                if (dVar != null && dVar.c == AppConst.AppState.DOWNLOAD && (s.a(simpleAppModel) || (simpleAppModel != null && simpleAppModel.h()))) {
                    sTInfoV2.status = "02";
                }
                oVar.i.a(sTInfoV2, new i(this), dVar, oVar.i, oVar.j);
            }
            c.a(this.e, simpleAppModel, oVar.h, false);
            if (!TextUtils.isEmpty(simpleAppModel.X)) {
                oVar.k.setText(simpleAppModel.X);
                oVar.k.setVisibility(0);
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) oVar.h.getLayoutParams();
                layoutParams.topMargin = df.a(this.e, 11.0f);
                oVar.h.setLayoutParams(layoutParams);
                RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) oVar.j.getLayoutParams();
                layoutParams2.topMargin = df.a(this.e, 3.0f);
                oVar.j.setLayoutParams(layoutParams2);
            } else {
                oVar.k.setVisibility(8);
                RelativeLayout.LayoutParams layoutParams3 = (RelativeLayout.LayoutParams) oVar.h.getLayoutParams();
                layoutParams3.topMargin = df.a(this.e, 13.0f);
                oVar.h.setLayoutParams(layoutParams3);
                RelativeLayout.LayoutParams layoutParams4 = (RelativeLayout.LayoutParams) oVar.j.getLayoutParams();
                layoutParams4.topMargin = df.a(this.e, 8.0f);
                oVar.j.setLayoutParams(layoutParams4);
            }
            a(a(simpleAppModel, i2, null, 100));
        }
    }

    private void a(m mVar, SimpleAppModel simpleAppModel, int i2, STInfoV2 sTInfoV2, d dVar) {
        if (simpleAppModel != null && mVar != null) {
            if (!TextUtils.isEmpty(simpleAppModel.Y)) {
                mVar.b.updateImageView(simpleAppModel.Y, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            } else {
                mVar.b.setImageResource(R.drawable.pic_defaule);
            }
            if (simpleAppModel.Z == null || simpleAppModel.Z.length() == 0) {
                mVar.d.setVisibility(8);
                mVar.b.setDuplicateParentStateEnabled(true);
                mVar.b.setClickable(false);
            } else {
                mVar.d.setVisibility(0);
                mVar.b.setDuplicateParentStateEnabled(false);
                mVar.b.setClickable(true);
                mVar.b.setOnClickListener(new j(this, simpleAppModel));
            }
            mVar.e.setSimpleAppModel(simpleAppModel, a.a(sTInfoV2), this.i);
            mVar.f.a(simpleAppModel);
            mVar.g.a(simpleAppModel);
            mVar.h.setText(simpleAppModel.d);
            if (TextUtils.isEmpty(simpleAppModel.X)) {
                mVar.i.setVisibility(8);
            } else {
                mVar.i.setVisibility(0);
                mVar.i.setText(simpleAppModel.X);
            }
            if (s.a(simpleAppModel)) {
                mVar.f.setClickable(false);
            } else {
                mVar.f.setClickable(true);
                if (dVar != null && dVar.c == AppConst.AppState.DOWNLOAD && (s.a(simpleAppModel) || (simpleAppModel != null && simpleAppModel.h()))) {
                    sTInfoV2.status = "02";
                }
                mVar.f.a(sTInfoV2, new k(this), dVar, mVar.f, mVar.g);
            }
            c.a(this.e, simpleAppModel, mVar.h, false);
            a(a(simpleAppModel, i2, null, 100));
        }
    }

    public void handleUIEvent(Message message) {
        DownloadInfo downloadInfo;
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE:
                if (message.obj instanceof DownloadInfo) {
                    downloadInfo = (DownloadInfo) message.obj;
                } else {
                    downloadInfo = null;
                }
                if (downloadInfo != null && !TextUtils.isEmpty(downloadInfo.downloadTicket)) {
                    for (SimpleAppModel q : this.g) {
                        if (q.q().equals(downloadInfo.downloadTicket)) {
                            a();
                            return;
                        }
                    }
                    return;
                }
                return;
            case 1016:
                u.g(this.g);
                a();
                return;
            case EventDispatcherEnum.UI_EVENT_DOWNLOADLIST_READY:
                a();
                return;
            default:
                return;
        }
    }

    private void a() {
        ba.a().post(new l(this));
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    private boolean b() {
        if ((!com.tencent.assistant.net.c.d() || com.tencent.assistant.net.c.k()) && m.a().p()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public STInfoV2 a(SimpleAppModel simpleAppModel, int i2, String str, int i3) {
        STInfoV2 sTInfoV2 = null;
        if (this.e instanceof BaseActivity) {
            sTInfoV2 = STInfoBuilder.buildSTInfo(this.e, i3);
            sTInfoV2.slotId = a.a("06", i2);
            sTInfoV2.updateWithSimpleAppModel(simpleAppModel);
            if (str != null) {
                sTInfoV2.status = str;
            }
            if (sTInfoV2 != null) {
                sTInfoV2.updateContentId(STCommonInfo.ContentIdType.CATEGORY, this.i + "_" + this.j);
            }
        }
        return sTInfoV2;
    }

    private void a(STInfoV2 sTInfoV2) {
        if (this.k == null) {
            this.k = new b();
        }
        this.k.exposure(sTInfoV2);
    }
}
