package com.tencent.assistantv2.b;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.protocol.jce.GetAppTagInfoListRequest;
import com.tencent.assistant.protocol.jce.GetAppTagInfoListResponse;
import com.tencent.assistantv2.model.a.c;

/* compiled from: ProGuard */
public class j extends BaseEngine<c> {
    public int a(SimpleAppModel simpleAppModel) {
        return send(new GetAppTagInfoListRequest(simpleAppModel.f1634a, simpleAppModel.c));
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChangedInMainThread(new k(this, i, (GetAppTagInfoListResponse) jceStruct2));
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        GetAppTagInfoListResponse getAppTagInfoListResponse = (GetAppTagInfoListResponse) jceStruct2;
        notifyDataChangedInMainThread(new l(this, i, i2));
    }
}
