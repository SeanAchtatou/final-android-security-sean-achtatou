package com.tencent.assistantv2.b;

import android.os.Handler;
import android.util.SparseArray;
import com.qq.AppService.AstApp;
import com.qq.taf.jce.JceStruct;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.protocol.jce.GetNavigationRequest;
import com.tencent.assistant.protocol.jce.GetNavigationResponse;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.bh;
import com.tencent.assistantv2.model.a.h;
import java.util.Iterator;

/* compiled from: ProGuard */
public class ae extends BaseEngine<h> {

    /* renamed from: a  reason: collision with root package name */
    private static ae f2943a = null;
    private static SparseArray<ak> b = new SparseArray<>(4);
    private static final Object c = new Object();
    /* access modifiers changed from: private */
    public static final int[] d = {0, 1, 2};
    /* access modifiers changed from: private */
    public static String[] e;
    private static Handler f = null;

    public static synchronized ae a() {
        ae aeVar;
        synchronized (ae.class) {
            if (f2943a == null) {
                f2943a = new ae();
            }
            aeVar = f2943a;
        }
        return aeVar;
    }

    private ae() {
        e = AstApp.i().getResources().getStringArray(R.array.navigation_default_items);
        try {
            f = ba.a("GetNavigationEngineHandler");
        } catch (Throwable th) {
            cq.a().b();
        }
        a(0);
        if (f != null) {
            f.postDelayed(new af(this), 5000);
        } else {
            XLog.e("GetNavigationEngine", "GetNavigationEngine HandlerUtils.getHandler() == null");
        }
    }

    private ak d(int i) {
        GetNavigationResponse getNavigationResponse;
        byte[] e2 = m.a().e(i);
        if (e2 != null && e2.length > 0) {
            try {
                getNavigationResponse = (GetNavigationResponse) bh.b(e2, GetNavigationResponse.class);
            } catch (Exception e3) {
                XLog.e("GetNavigationEngine", "response to navi object fail.typeid:" + i + ".ex:" + e3);
            }
            return ak.a(i, getNavigationResponse);
        }
        getNavigationResponse = null;
        return ak.a(i, getNavigationResponse);
    }

    public ak a(int i) {
        ak akVar = b.get(i);
        if (akVar == null) {
            synchronized (c) {
                akVar = b.get(i);
                if (akVar == null) {
                    akVar = d(i);
                    b.put(i, akVar);
                }
            }
        }
        return akVar;
    }

    public void b(int i) {
        TemporaryThreadManager.get().start(new ag(this, i));
    }

    public int c(int i) {
        return m.a().a("key_navi_reminder_t_" + i, 0);
    }

    /* access modifiers changed from: private */
    public int a(long j, int i) {
        GetNavigationRequest getNavigationRequest = new GetNavigationRequest();
        getNavigationRequest.f2145a = j;
        getNavigationRequest.b = i;
        return send(getNavigationRequest);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        String str;
        int i2;
        GetNavigationResponse getNavigationResponse = (GetNavigationResponse) jceStruct2;
        GetNavigationRequest getNavigationRequest = (GetNavigationRequest) jceStruct;
        if (getNavigationResponse == null || getNavigationRequest == null || getNavigationResponse.b == getNavigationRequest.a()) {
            XLog.e("GetNavigationEngine", "GetNavigationEngine has null value.seq:" + i + ",request:" + jceStruct + ",response:" + getNavigationResponse);
            return;
        }
        int b2 = getNavigationRequest.b();
        ak b3 = ak.b(b2, getNavigationResponse);
        m.a().b("key_navi_reminder_t_" + b2, (Object) 0);
        if (b3 != null) {
            byte[] a2 = bh.a(getNavigationResponse);
            if (a2 != null) {
                m.a().a(b2, a2);
            }
            ak akVar = b.get(b2);
            if (akVar == null || akVar.b == null || akVar.b == null || akVar.b.size() <= 0) {
                notifyDataChangedInMainThread(new ai(this, i, b2, b3));
                return;
            }
            Iterator<al> it = akVar.b.iterator();
            while (true) {
                if (!it.hasNext()) {
                    str = null;
                    i2 = 0;
                    break;
                }
                al next = it.next();
                if (next.c > 0) {
                    i2 = next.c;
                    String str2 = next.f2950a;
                    m.a().b("key_navi_reminder_t_" + b2, Integer.valueOf(i2));
                    str = str2;
                    break;
                }
            }
            for (al next2 : akVar.b) {
                if (next2.f2950a == null || !next2.f2950a.equals(str)) {
                    next2.c = 0;
                } else {
                    next2.c = i2;
                }
            }
            notifyDataChangedInMainThread(new ah(this, i, b2, akVar));
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        XLog.e("GetNavigationEngine", "GetNavigationEngine error code:" + i2 + ".seq:" + i + ",request:" + jceStruct + ",response:" + jceStruct2);
    }
}
