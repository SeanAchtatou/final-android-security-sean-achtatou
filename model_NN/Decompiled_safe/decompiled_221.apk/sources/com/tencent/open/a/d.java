package com.tencent.open.a;

import java.io.File;
import java.io.FileFilter;

/* compiled from: ProGuard */
class d implements FileFilter {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f3752a;

    d(b bVar) {
        this.f3752a = bVar;
    }

    public boolean accept(File file) {
        if (file.getName().endsWith(this.f3752a.i()) && b.f(file) != -1) {
            return true;
        }
        return false;
    }
}
