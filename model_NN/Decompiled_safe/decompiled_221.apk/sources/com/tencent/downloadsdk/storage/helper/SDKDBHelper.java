package com.tencent.downloadsdk.storage.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.tencent.downloadsdk.storage.a.a;
import com.tencent.downloadsdk.storage.a.b;
import com.tencent.downloadsdk.storage.a.c;
import com.tencent.downloadsdk.storage.a.e;
import com.tencent.downloadsdk.storage.a.f;
import java.util.ArrayList;
import java.util.Arrays;

public class SDKDBHelper extends SqliteHelper {
    private static final String DB_NAME = "tmassistant_sdk.db";
    private static int DB_VERSION = 2;
    private static Class<?>[] ENTRUST_TABLESS;
    private static Class<?>[] TABLESS = {c.class, e.class, f.class, a.class, b.class};
    protected static SqliteHelper mInstance = null;

    protected SDKDBHelper(Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i) {
        super(context, str, cursorFactory, i);
    }

    public static synchronized SqliteHelper getDBHelper(Context context) {
        SqliteHelper sqliteHelper;
        synchronized (SDKDBHelper.class) {
            if (mInstance == null) {
                mInstance = new SDKDBHelper(context, DB_NAME, null, DB_VERSION);
            }
            sqliteHelper = mInstance;
        }
        return sqliteHelper;
    }

    public int getDBVersion() {
        return DB_VERSION;
    }

    public Class<?>[] getTables() {
        if (ENTRUST_TABLESS == null || ENTRUST_TABLESS.length <= 0) {
            return TABLESS;
        }
        ArrayList arrayList = new ArrayList(Arrays.asList(TABLESS));
        arrayList.addAll(Arrays.asList(ENTRUST_TABLESS));
        return (Class[]) arrayList.toArray(new Class[0]);
    }

    public void setEntrustTable(int i, Class<?>[] clsArr) {
        if (i > DB_VERSION) {
            DB_VERSION = i;
        }
        ENTRUST_TABLESS = clsArr;
    }
}
