package com.tencent.downloadsdk.network;

import java.io.IOException;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.protocol.HttpContext;

final class b extends DefaultHttpRequestRetryHandler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f3619a;
    private long b = 0;
    private long c = System.currentTimeMillis();

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b(a aVar) {
        super(3, false);
        this.f3619a = aVar;
    }

    public boolean retryRequest(IOException iOException, int i, HttpContext httpContext) {
        boolean retryRequest = b.super.retryRequest(iOException, i, httpContext);
        if (!retryRequest) {
            return retryRequest;
        }
        long currentTimeMillis = System.currentTimeMillis();
        this.b += currentTimeMillis - this.c;
        this.c = currentTimeMillis;
        if (this.b > ((long) this.f3619a.d)) {
            return false;
        }
        return retryRequest;
    }
}
