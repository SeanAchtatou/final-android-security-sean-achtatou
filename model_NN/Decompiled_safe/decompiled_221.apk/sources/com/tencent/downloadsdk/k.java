package com.tencent.downloadsdk;

import android.content.Context;
import android.net.Uri;
import android.os.PowerManager;
import android.os.SystemClock;
import android.text.TextUtils;
import com.tencent.connect.common.Constants;
import com.tencent.downloadsdk.a.b;
import com.tencent.downloadsdk.a.c;
import com.tencent.downloadsdk.network.HttpUtils;
import com.tencent.downloadsdk.network.a;
import com.tencent.downloadsdk.network.d;
import com.tencent.downloadsdk.utils.f;
import com.tencent.downloadsdk.utils.g;
import com.tencent.downloadsdk.utils.o;
import com.tencent.downloadsdk.utils.p;
import com.tencent.securemodule.impl.ErrorCode;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.Future;

public class k implements Runnable {
    /* access modifiers changed from: private */
    public HashMap<String, String> A = new HashMap<>();
    /* access modifiers changed from: private */
    public volatile boolean B = false;
    private long C = 0;
    private HashMap<String, String> D;
    private d<Void> E = new l(this);

    /* renamed from: a  reason: collision with root package name */
    protected an f3610a;
    protected ArrayList<String> b;
    public String c;
    protected volatile Future<?> d;
    protected long e;
    protected long f;
    protected long g;
    /* access modifiers changed from: private */
    public int h;
    private String i;
    private Throwable j;
    /* access modifiers changed from: private */
    public byte[] k;
    /* access modifiers changed from: private */
    public byte[] l;
    /* access modifiers changed from: private */
    public int m;
    /* access modifiers changed from: private */
    public DownloadSettingInfo n;
    /* access modifiers changed from: private */
    public b o;
    /* access modifiers changed from: private */
    public m p;
    private ai q;
    /* access modifiers changed from: private */
    public long r;
    private long s;
    /* access modifiers changed from: private */
    public long t;
    /* access modifiers changed from: private */
    public int u;
    /* access modifiers changed from: private */
    public String v;
    private String w;
    private ArrayList<String> x;
    private ArrayList<String> y;
    private a z;

    public k(int i2, String str, an anVar, ai aiVar, DownloadSettingInfo downloadSettingInfo, c cVar, m mVar, HashMap<String, String> hashMap) {
        this.h = i2;
        this.i = str;
        this.f3610a = anVar;
        this.q = aiVar;
        this.n = downloadSettingInfo;
        this.l = o.a().a(this.n.h);
        this.m = 0;
        if (cVar != null) {
            this.o = cVar.a(this.f3610a.c, this.n.m);
        }
        this.p = mVar;
        this.s = anVar.f + anVar.g;
        this.t = this.f3610a.e - anVar.g;
        this.x = new ArrayList<>();
        this.b = new ArrayList<>();
        this.y = new ArrayList<>();
        this.D = hashMap;
    }

    static /* synthetic */ int a(k kVar, int i2) {
        int i3 = kVar.u + i2;
        kVar.u = i3;
        return i3;
    }

    private n a(String str) {
        int i2;
        Thread currentThread = Thread.currentThread();
        if (this.p != null) {
            this.p.a(this.f3610a);
        }
        n nVar = new n();
        nVar.f3616a = -1000;
        nVar.b = false;
        if (TextUtils.isEmpty(str)) {
            nVar.f3616a = -16;
            return nVar;
        }
        long j2 = this.t - ((long) this.u);
        if (this.n.r > 0 && ((double) j2) > ((double) this.n.r) * 1.5d && j2 >= this.n.r) {
            j2 = this.n.r;
        }
        String a2 = HttpUtils.a(this.s + ((long) this.u), j2);
        this.A.clear();
        this.A.put("User-Agent", "AssistantDownloader");
        String c2 = HttpUtils.c(str);
        if (!TextUtils.isEmpty(c2)) {
            this.A.put("Host", c2);
        }
        if (this.z == null) {
            this.z = new a(this.n.g * 1000, this.n.f * 1000);
        }
        this.z.a(true);
        this.z.a(this.D);
        if (p.a(str)) {
            try {
                str = p.a(str, b.b(), this.f3610a.b, this.f3610a.c + Constants.STR_EMPTY);
            } catch (UnsupportedEncodingException e2) {
            }
        }
        int i3 = 0;
        boolean z2 = true;
        while (true) {
            boolean z3 = z2;
            i2 = i3;
            String str2 = a2;
            if (!currentThread.isInterrupted() && z3 && i2 < this.n.l) {
                if (Thread.currentThread().isInterrupted()) {
                    nVar.f3616a = b();
                    return nVar;
                }
                f.b("DownloadSegment", "segId: " + this.f3610a.c + " wakeLock.acquire()");
                if (str2 != null && !Constants.STR_EMPTY.equals(str2)) {
                    this.A.put("Range", str2);
                }
                com.tencent.downloadsdk.network.c a3 = this.z.a(HttpUtils.a(str), this.n.n, null, this.A, false, this.E);
                f.b("DownloadSegment", "download status code:" + a3.c);
                f.b("DownloadSegment", "download result code:" + a3.j);
                this.c = a3.a();
                if (this.k == null && a3.l != null) {
                    this.k = a3.l;
                }
                if (a3.k != null) {
                    this.y.add(a3.k.getLocalizedMessage());
                }
                if (a3.j != 0) {
                    nVar.f3616a = a3.j;
                    if (a3.j == -16 || a3.j == -21) {
                        nVar.b = true;
                    }
                    if (a3.j == -83 || a3.j == -85 || a3.j == -87 || a3.j == -89) {
                        nVar.b = true;
                    }
                    z2 = false;
                    i3 = i2;
                    a2 = str2;
                } else {
                    int i4 = a3.c;
                    if (i4 == 200 || i4 == 206 || i4 == 413 || i4 == 416) {
                        if (((long) this.u) == this.t) {
                            if (this.p != null) {
                                this.p.a(this.f3610a, this.f3610a.d, str);
                            }
                            nVar.f3616a = a3.j;
                            z2 = false;
                            i3 = i2;
                            a2 = str2;
                        } else if (this.r == 0) {
                            nVar.b = true;
                            nVar.f3616a = -37;
                            z2 = false;
                            i3 = i2;
                            a2 = str2;
                        } else {
                            long j3 = this.t - ((long) this.u);
                            if (this.n.r > 0 && ((double) j3) > ((double) this.n.r) * 1.5d && j3 >= this.n.r) {
                                j3 = this.n.r;
                            }
                            if (j3 < 0) {
                                nVar.b = true;
                                nVar.f3616a = -36;
                                z2 = false;
                                i3 = i2;
                                a2 = str2;
                            } else {
                                String a4 = HttpUtils.a(this.s + ((long) this.u), j3);
                                z2 = z3;
                                i3 = i2;
                                a2 = a4;
                            }
                        }
                    } else if (i4 == 301 || i4 == 302 || i4 == 303 || i4 == 307) {
                        if (i2 == 0) {
                            this.w = a3.b;
                        }
                        int i5 = i2 + 1;
                        String a5 = a3.a("Location");
                        if (TextUtils.isEmpty(a5) || a5.trim().length() == 0) {
                            nVar.b = true;
                            z3 = false;
                            nVar.f3616a = -31;
                        } else {
                            try {
                                if (a5.startsWith("/")) {
                                    Uri parse = Uri.parse(str);
                                    String host = parse.getHost();
                                    String scheme = parse.getScheme();
                                    if (!TextUtils.isEmpty(host) && !TextUtils.isEmpty(scheme)) {
                                        a5 = scheme + "://" + host + a5;
                                    }
                                }
                            } catch (Exception e3) {
                            }
                        }
                        this.x.add(a5 + Constants.STR_EMPTY);
                        i3 = i5;
                        str = a5;
                        a2 = str2;
                        z2 = z3;
                    } else if (i4 < 0) {
                        nVar.b = true;
                        nVar.f3616a = -24;
                        z2 = false;
                        i3 = i2;
                        a2 = str2;
                    } else {
                        nVar.b = true;
                        nVar.f3616a = -i4;
                        z2 = false;
                        i3 = i2;
                        a2 = str2;
                    }
                }
            }
        }
        if (currentThread.isInterrupted()) {
            f.b("interrupted", "DownloadSegment:" + this.f3610a.c + " currentThread:" + currentThread + ".isInterrupted() in redirect");
        }
        if (i2 == this.n.l) {
            nVar.f3616a = -1;
            nVar.b = true;
        }
        return nVar;
    }

    /* access modifiers changed from: private */
    public int b() {
        if (this.z == null) {
            return 1;
        }
        this.z.a();
        return 1;
    }

    static /* synthetic */ int b(k kVar, int i2) {
        int i3 = kVar.m + i2;
        kVar.m = i3;
        return i3;
    }

    static /* synthetic */ long b(k kVar, long j2) {
        long j3 = kVar.r + j2;
        kVar.r = j3;
        return j3;
    }

    private final boolean c() {
        long elapsedRealtime = SystemClock.elapsedRealtime();
        if (this.C == 0) {
            this.C = elapsedRealtime;
        } else {
            long j2 = elapsedRealtime - this.C;
            long j3 = 60000;
            if (com.tencent.downloadsdk.utils.k.d()) {
                j3 = 80000;
            } else if (com.tencent.downloadsdk.utils.k.c()) {
                j3 = 100000;
            }
            if (j2 > j3) {
                f.c("DownloadSegment", "segment:" + this.f3610a.b + ",retry cost time:" + j2 + " over max limit:" + j3 + ", stop retry.");
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void d() {
        this.C = 0;
    }

    public com.tencent.downloadsdk.b.a a(long j2, String str, ArrayList<String> arrayList, String str2, String str3, int i2, long j3, long j4, long j5, long j6, long j7, int i3, int i4, int i5, ArrayList<String> arrayList2, ArrayList<String> arrayList3, String str4) {
        com.tencent.downloadsdk.b.a aVar = new com.tencent.downloadsdk.b.a();
        aVar.f3599a = j2;
        aVar.b = str;
        aVar.c = arrayList;
        if (arrayList == null || arrayList.size() == 0) {
            aVar.d = str;
        } else {
            aVar.d = arrayList.get(arrayList.size() - 1);
        }
        aVar.h = str2;
        aVar.e = i2;
        aVar.f = j3;
        aVar.i = j4;
        aVar.j = j5;
        aVar.k = j6;
        aVar.l = j7;
        aVar.m = i3;
        aVar.n = i4;
        aVar.o = i5;
        aVar.p = arrayList2;
        aVar.q = arrayList3;
        aVar.g = str3;
        aVar.r = str4;
        return aVar;
    }

    public void a() {
        f.b("DownloadSegment", "DownloadSegment cancel:" + this.f3610a.c);
        this.B = true;
        if (this.d != null) {
            this.d.cancel(true);
            f.b("interrupted", "DownloadSegment:" + this.f3610a.c + " currentThread" + this.d + "call Interrupted()");
            if (this.z != null) {
                this.z.a();
                return;
            }
            return;
        }
        com.tencent.downloadsdk.b.a a2 = a(this.f3610a.c, this.f3610a.d, this.x, this.v, this.w, this.n.b, this.n.f3579a, this.f3610a.i, this.o.h, this.o.f, this.o.g, 0, 1, 2, this.b, this.y, this.c);
        f.b("DownloadSegment", "DownloadSegment run finished id:" + this.f3610a.b);
        if (this.p != null) {
            this.p.a(this.f3610a, a2, this.e + "+" + this.f + "+" + this.g + "+" + this.b.toString());
        }
        this.p = null;
    }

    public void run() {
        int i2;
        n nVar;
        boolean z2;
        boolean z3;
        if (!this.B) {
            f.a("DownloadSegment", "DownloadSegment: " + this.f3610a.c + " run in: " + Thread.currentThread().getName());
            this.e = System.currentTimeMillis();
            Thread currentThread = Thread.currentThread();
            Context b2 = DownloadManager.a().b();
            PowerManager.WakeLock newWakeLock = ((PowerManager) b2.getSystemService("power")).newWakeLock(1, getClass().getSimpleName());
            newWakeLock.setReferenceCounted(false);
            newWakeLock.acquire();
            this.o.a();
            int i3 = this.n.d;
            n nVar2 = new n();
            nVar2.f3616a = -1000;
            int i4 = 1;
            int a2 = this.q.a();
            boolean z4 = true;
            int i5 = 0;
            while (true) {
                i2 = i5;
                nVar = nVar2;
                boolean z5 = z4;
                if (!this.B && !currentThread.isInterrupted() && z5 && a2 > 0 && c()) {
                    String a3 = this.q.a(i2 % a2);
                    this.x.add(i2 + Constants.STR_EMPTY);
                    this.x.add(a3);
                    if (i2 > 0) {
                        SystemClock.sleep(2000);
                    }
                    boolean z6 = true;
                    nVar2 = nVar;
                    boolean z7 = z5;
                    int i6 = 0;
                    z4 = z7;
                    while (!this.B && !currentThread.isInterrupted() && z6 && i6 < i3) {
                        this.k = new byte[0];
                        n a4 = a(a3);
                        if (a4.f3616a == 0) {
                            this.g = System.currentTimeMillis();
                            if (this.p != null) {
                                this.p.b(this.f3610a);
                            }
                            i4 = 0;
                            z2 = false;
                            z3 = false;
                        } else if (a4.f3616a == 1) {
                            if (this.p != null) {
                                this.p.c(this.f3610a);
                            }
                            z2 = false;
                            z3 = false;
                        } else if (i6 == i3 - 1) {
                            z2 = z4;
                            z3 = false;
                        } else if (a4.b) {
                            z2 = z4;
                            z3 = false;
                        } else if (!c()) {
                            z2 = false;
                            z3 = false;
                        } else {
                            SystemClock.sleep(2000);
                            z2 = z4;
                            z3 = z6;
                        }
                        this.b.add(a4.f3616a + Constants.STR_EMPTY);
                        i6++;
                        z6 = z3;
                        z4 = z2;
                        nVar2 = a4;
                    }
                    if (currentThread.isInterrupted()) {
                        f.b("interrupted", "DownloadSegment:" + this.f3610a.c + " currentThread:" + currentThread + ".isInterrupted() in inner loop");
                    }
                    i5 = i2 + 1;
                }
            }
            if (currentThread.isInterrupted() && this.B) {
                nVar.f3616a = 1;
                if (this.p != null) {
                    this.p.c(this.f3610a);
                }
                f.b("interrupted", "DownloadSegment:" + this.f3610a.c + " currentThread:" + currentThread + ".isInterrupted() in outer loop");
            }
            if (i2 >= a2 && nVar.f3616a < 0) {
                nVar.f3616a += ErrorCode.ERR_POST;
            }
            if (!(nVar.f3616a == 0 || nVar.f3616a == 1 || this.p == null)) {
                this.p.a(this.f3610a, nVar.f3616a, this.k, this.j);
            }
            this.o.b();
            if (newWakeLock != null && newWakeLock.isHeld()) {
                newWakeLock.release();
            }
            com.tencent.downloadsdk.b.a a5 = a(this.f3610a.c, this.f3610a.d, this.x, this.v, this.w, this.n.b, this.n.f3579a, this.f3610a.i, this.o.h, this.o.f, this.o.g, g.i(b2), nVar.f3616a, i4, this.b, this.y, this.c);
            if (this.p != null) {
                this.p.a(this.f3610a, a5, this.e + "+" + this.f + "+" + this.g + "+" + this.b.toString());
            }
            this.p = null;
            f.b("DownloadSegment", "DownloadSegment run finished id:" + this.f3610a.b + ",result.mResultCode:" + nVar.f3616a + ",segment:" + this + ",urlCount:" + a2 + ",urlRetryIndex:" + i2 + ",maxRetryTimes:" + i3);
        }
    }
}
