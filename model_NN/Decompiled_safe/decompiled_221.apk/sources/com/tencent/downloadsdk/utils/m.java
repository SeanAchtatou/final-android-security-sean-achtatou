package com.tencent.downloadsdk.utils;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import com.tencent.connect.common.Constants;

public class m {
    protected static String b = "NA";

    /* renamed from: a  reason: collision with root package name */
    protected Context f3653a = null;

    public m(Context context) {
        this.f3653a = context;
    }

    private StringBuffer a(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        if (TextUtils.isEmpty(str)) {
            stringBuffer.append("NA");
            return stringBuffer;
        }
        char[] charArray = str.toCharArray();
        for (int i = 0; i < charArray.length; i++) {
            if (!(charArray[i] <= ' ' || charArray[i] == '/' || charArray[i] == '_' || charArray[i] == '&' || charArray[i] == '|' || charArray[i] == '-')) {
                stringBuffer.append(charArray[i]);
            }
        }
        return stringBuffer;
    }

    public static String b() {
        return "100".contains("BuildNo") ? "0000" : "100";
    }

    private int c() {
        return this.f3653a.getResources().getDisplayMetrics().widthPixels;
    }

    private int d() {
        return this.f3653a.getResources().getDisplayMetrics().heightPixels;
    }

    private int e() {
        return 0;
    }

    private String f() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(a(Build.BRAND));
        stringBuffer.append("_");
        stringBuffer.append(a(Build.MODEL));
        return stringBuffer.toString();
    }

    private String g() {
        StringBuffer stringBuffer = new StringBuffer();
        String str = Build.VERSION.RELEASE;
        if (TextUtils.isEmpty(str)) {
            stringBuffer.append("NA");
        } else {
            stringBuffer.append(str);
        }
        stringBuffer.append("_");
        stringBuffer.append(Build.VERSION.SDK_INT);
        return stringBuffer.toString();
    }

    public String a() {
        l lVar = new l();
        lVar.c = b();
        lVar.d = Constants.STR_EMPTY;
        lVar.e = b;
        lVar.f = g();
        lVar.h = d();
        lVar.g = c();
        lVar.i = e();
        lVar.f3652a = f();
        lVar.b = "100";
        return lVar.a();
    }
}
