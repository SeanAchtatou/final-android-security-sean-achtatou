package com.tencent.downloadsdk.utils;

import android.util.Log;
import com.tencent.connect.common.Constants;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class n {
    public static Object a(String str) {
        Object obj = null;
        if (str == null || !Constants.STR_EMPTY.equals(str)) {
            return null;
        }
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(a.a(str, 0));
            ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
            obj = objectInputStream.readObject();
            byteArrayInputStream.close();
            objectInputStream.close();
            return obj;
        } catch (Exception e) {
            Log.e("SerializeTools", "=====deserialize==wrong==" + e.getMessage());
            return obj;
        }
    }

    public static String a(Object obj) {
        if (obj == null) {
            return null;
        }
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(obj);
            objectOutputStream.flush();
            objectOutputStream.close();
            byteArrayOutputStream.close();
            return a.b(byteArrayOutputStream.toByteArray(), 0);
        } catch (Exception e) {
            f.d("SerializeTools", "Serialize Error" + e.getMessage());
            return null;
        }
    }
}
