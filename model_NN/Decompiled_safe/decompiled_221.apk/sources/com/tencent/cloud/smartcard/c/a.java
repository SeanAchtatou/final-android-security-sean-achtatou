package com.tencent.cloud.smartcard.c;

import com.tencent.assistant.manager.smartcard.y;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.protocol.jce.SmartCardTagInfo;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class a extends y {
    public boolean a(i iVar, List<Long> list) {
        ArrayList<SmartCardTagInfo> arrayList;
        if (!((Boolean) b(iVar).first).booleanValue()) {
            return false;
        }
        if (iVar instanceof com.tencent.cloud.smartcard.b.a) {
            if (((com.tencent.cloud.smartcard.b.a) iVar).e() == null || ((com.tencent.cloud.smartcard.b.a) iVar).e().b() == null) {
                arrayList = null;
            } else {
                arrayList = ((com.tencent.cloud.smartcard.b.a) iVar).e().b();
            }
            if (arrayList == null || arrayList.size() < 6) {
                return false;
            }
        }
        return true;
    }
}
