package com.tencent.cloud.smartcard.b;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.a.s;
import com.tencent.assistant.protocol.jce.SmartCardTag;
import com.tencent.assistant.protocol.jce.SmartCardTitle;
import java.util.List;

/* compiled from: ProGuard */
public class a extends com.tencent.assistant.model.a.a {

    /* renamed from: a  reason: collision with root package name */
    private SmartCardTag f3463a;

    public boolean a(byte b, JceStruct jceStruct) {
        if (b != 37 || !(jceStruct instanceof SmartCardTag)) {
            return false;
        }
        this.f3463a = (SmartCardTag) jceStruct;
        this.i = b;
        this.j = this.f3463a.f;
        SmartCardTitle a2 = this.f3463a.a();
        if (a2 != null) {
            this.k = a2.b;
            this.o = a2.c;
            this.n = a2.d;
        }
        return true;
    }

    public List<SimpleAppModel> a() {
        return null;
    }

    public SmartCardTag e() {
        return this.f3463a;
    }

    public s b() {
        s sVar = new s();
        sVar.e = this.i;
        sVar.f = this.j;
        sVar.f1652a = this.f3463a.c;
        sVar.b = this.f3463a.d;
        return sVar;
    }
}
