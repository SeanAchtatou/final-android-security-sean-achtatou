package com.tencent.cloud.smartcard.view;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.link.b;
import com.tencent.assistant.protocol.jce.SmartCardTagInfo;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class a extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SmartCardTagInfo f3466a;
    final /* synthetic */ int b;
    final /* synthetic */ NormalSmartCardLabelItem c;

    a(NormalSmartCardLabelItem normalSmartCardLabelItem, SmartCardTagInfo smartCardTagInfo, int i) {
        this.c = normalSmartCardLabelItem;
        this.f3466a = smartCardTagInfo;
        this.b = i;
    }

    public void onTMAClick(View view) {
        b.a(this.c.f1115a, this.f3466a.c);
    }

    public STInfoV2 getStInfo() {
        return this.c.a(com.tencent.assistantv2.st.page.a.a("04", this.b), 200);
    }
}
