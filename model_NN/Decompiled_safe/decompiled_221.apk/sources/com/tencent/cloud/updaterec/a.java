package com.tencent.cloud.updaterec;

import android.content.Context;
import android.text.TextUtils;
import android.widget.ListView;
import android.widget.TextView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.w;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.dg;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class a implements w {
    public a(Context context, ListView listView, h hVar) {
        a(context, listView, hVar);
    }

    public void a(Context context, ListView listView, h hVar) {
        dg.a("OMAEDC_CTX", context);
        dg.a("OMAEDC_LV", listView);
        dg.a("OMAEDC_ADP", hVar);
    }

    public void a(int i, int i2, ArrayList<SimpleAppModel> arrayList) {
        ListView listView = (ListView) dg.a("OMAEDC_LV");
        Context context = (Context) dg.a("OMAEDC_CTX");
        h hVar = (h) dg.a("OMAEDC_ADP");
        if (listView != null && hVar != null && context != null) {
            int childCount = listView.getChildCount();
            for (int i3 = 0; i3 < childCount; i3++) {
                g a2 = hVar.a(listView.getChildAt(i3).getTag());
                TextView c = hVar.c(listView.getChildAt(i3).getTag());
                if (!(a2 == null || a2.f3474a == null)) {
                    Integer num = (Integer) a2.f3474a.getTag();
                    String a3 = hVar.a();
                    if (num != null && num.intValue() == i) {
                        if (arrayList != null && arrayList.size() > 0) {
                            if (!TextUtils.isEmpty(arrayList.get(0).V)) {
                                a2.k.setText(arrayList.get(0).V);
                            }
                            if (c != null) {
                                c.setVisibility(8);
                            }
                            com.tencent.cloud.b.a.a(context, a2, arrayList, a3);
                            return;
                        } else if (i2 == 0) {
                            com.tencent.cloud.b.a.a(a2);
                            return;
                        } else {
                            if (c != null) {
                                c.setVisibility(8);
                            }
                            com.tencent.cloud.b.a.a(context, a2);
                            ba.a().postDelayed(new b(this, a2, i, c, a2), 3000);
                            return;
                        }
                    }
                }
            }
        }
    }
}
