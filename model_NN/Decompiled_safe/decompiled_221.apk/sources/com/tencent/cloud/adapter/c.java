package com.tencent.cloud.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.cloud.updaterec.UpdateRecListItemInfoView;

/* compiled from: ProGuard */
class c {

    /* renamed from: a  reason: collision with root package name */
    TXAppIconView f3462a;
    TextView b;
    DownloadButton c;
    View d;
    TextView e;
    TextView f;
    ImageView g;
    UpdateRecListItemInfoView h;
    final /* synthetic */ UpdateRecOneMoreAdapter i;

    private c(UpdateRecOneMoreAdapter updateRecOneMoreAdapter) {
        this.i = updateRecOneMoreAdapter;
    }

    /* synthetic */ c(UpdateRecOneMoreAdapter updateRecOneMoreAdapter, a aVar) {
        this(updateRecOneMoreAdapter);
    }
}
