package com.tencent.connect.share;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.tencent.assistant.component.txscrollview.TXTabBarLayout;
import com.tencent.open.utils.AsynLoadImgBack;

/* compiled from: ProGuard */
final class e extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AsynLoadImgBack f3503a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    e(Looper looper, AsynLoadImgBack asynLoadImgBack) {
        super(looper);
        this.f3503a = asynLoadImgBack;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case TXTabBarLayout.TABITEM_TIPS_TEXT_ID:
                this.f3503a.saved(0, (String) message.obj);
                return;
            case 102:
                this.f3503a.saved(message.arg1, null);
                return;
            default:
                super.handleMessage(message);
                return;
        }
    }
}
