package com.tencent.connect.share;

import android.graphics.Bitmap;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import com.tencent.assistant.component.txscrollview.TXTabBarLayout;
import com.tencent.open.a.n;
import com.tencent.open.utils.Util;

/* compiled from: ProGuard */
final class f implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f3504a;
    final /* synthetic */ Handler b;

    f(String str, Handler handler) {
        this.f3504a = str;
        this.b = handler;
    }

    public void run() {
        String a2;
        Bitmap a3 = d.a(this.f3504a, 140);
        if (a3 != null) {
            String str = Environment.getExternalStorageDirectory() + "/tmp/";
            String str2 = "share2qq_temp" + Util.encrypt(this.f3504a) + ".jpg";
            if (!d.b(this.f3504a, 140, 140)) {
                n.b("AsynScaleCompressImage", "not out of bound,not compress!");
                a2 = this.f3504a;
            } else {
                n.b("AsynScaleCompressImage", "out of bound,compress!");
                a2 = d.a(a3, str, str2);
            }
            n.b("AsynScaleCompressImage", "-->destFilePath: " + a2);
            if (a2 != null) {
                Message obtainMessage = this.b.obtainMessage(TXTabBarLayout.TABITEM_TIPS_TEXT_ID);
                obtainMessage.obj = a2;
                this.b.sendMessage(obtainMessage);
                return;
            }
        }
        Message obtainMessage2 = this.b.obtainMessage(102);
        obtainMessage2.arg1 = 3;
        this.b.sendMessage(obtainMessage2);
    }
}
