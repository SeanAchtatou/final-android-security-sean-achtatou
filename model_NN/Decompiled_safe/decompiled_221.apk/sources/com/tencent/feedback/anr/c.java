package com.tencent.feedback.anr;

import com.tencent.feedback.common.g;
import java.util.HashMap;

/* compiled from: ProGuard */
final class c implements e {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ com.tencent.feedback.eup.jni.c f3674a;
    private /* synthetic */ boolean b;

    c(com.tencent.feedback.eup.jni.c cVar, boolean z) {
        this.f3674a = cVar;
        this.b = z;
    }

    public final boolean a(String str, int i, String str2, String str3) {
        g.b("new thread %s", str);
        if (this.f3674a.f3699a > 0 && this.f3674a.c > 0 && this.f3674a.b != null) {
            if (this.f3674a.d == null) {
                this.f3674a.d = new HashMap();
            }
            this.f3674a.d.put(str, new String[]{str2, str3, new StringBuilder().append(i).toString()});
        }
        return true;
    }

    public final boolean a(long j, long j2, String str) {
        g.b("new process %s", str);
        if (!str.equals(str)) {
            return true;
        }
        this.f3674a.f3699a = j;
        this.f3674a.b = str;
        this.f3674a.c = j2;
        if (!this.b) {
            return false;
        }
        return true;
    }

    public final boolean a(long j) {
        g.b("process end %d", Long.valueOf(j));
        if (this.f3674a.f3699a <= 0 || this.f3674a.c <= 0 || this.f3674a.b == null) {
            return true;
        }
        return false;
    }
}
