package com.tencent.feedback.anr;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Process;
import android.util.SparseArray;
import com.tencent.connect.common.Constants;
import com.tencent.feedback.common.PlugInInfo;
import com.tencent.feedback.common.b;
import com.tencent.feedback.common.g;
import com.tencent.feedback.common.service.a;
import com.tencent.feedback.eup.d;
import com.tencent.feedback.eup.e;
import com.tencent.feedback.eup.f;
import com.tencent.feedback.eup.i;
import com.tencent.feedback.eup.jni.c;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: ProGuard */
public class ANRHandleServiceTask implements Parcelable, a {
    public static final Parcelable.Creator<ANRHandleServiceTask> CREATOR = new a();

    /* renamed from: a  reason: collision with root package name */
    public static AtomicBoolean f3672a = new AtomicBoolean(false);
    public static c b = null;
    private final boolean c;
    private final String d;
    private final long e;
    private final String f;
    private final Map<String, PlugInInfo> g;

    public ANRHandleServiceTask(Parcel parcel) {
        this.c = parcel.readInt() > 0;
        this.d = parcel.readString();
        this.f = parcel.readString();
        this.e = parcel.readLong();
        int readInt = parcel.readInt();
        if (readInt > 0) {
            this.g = new HashMap();
            for (int i = 0; i < readInt; i++) {
                PlugInInfo plugInInfo = new PlugInInfo(parcel);
                this.g.put(plugInInfo.f3676a, plugInInfo);
            }
            return;
        }
        this.g = null;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.c ? 1 : -1);
        parcel.writeString(this.d);
        parcel.writeString(this.f);
        parcel.writeLong(this.e);
        if (this.g == null || this.g.size() <= 0) {
            parcel.writeInt(0);
            return;
        }
        parcel.writeInt(this.g.size());
        for (String str : this.g.keySet()) {
            parcel.writeParcelable(this.g.get(str), 0);
        }
    }

    public String a() {
        return "2000";
    }

    public int b() {
        return 1000;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.eup.c.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.tencent.feedback.eup.c.a(android.content.Context, java.lang.String):void
      com.tencent.feedback.eup.c.a(boolean, boolean):void
      com.tencent.feedback.eup.c.a(android.content.Context, boolean):void */
    /* JADX WARNING: Code restructure failed: missing block: B:106:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
        r0 = a(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00de, code lost:
        if (r0 == null) goto L_0x0135;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00e4, code lost:
        if (r0.size() <= 0) goto L_0x0135;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00e6, code lost:
        r1 = android.os.Process.myUid();
        r2 = r0.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00f2, code lost:
        if (r2.hasNext() == false) goto L_0x0126;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00f4, code lost:
        r0 = r2.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00fe, code lost:
        if (r1 != r0.c()) goto L_0x00ee;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0100, code lost:
        com.tencent.feedback.common.g.b("has anr in process %s handle it and leave", r0.a());
        r0.a(r5);
        a(r10, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0115, code lost:
        com.tencent.feedback.anr.ANRHandleServiceTask.f3672a.set(false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:?, code lost:
        com.tencent.feedback.common.g.b("not my anr ignorl ,leave", new java.lang.Object[0]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x012e, code lost:
        com.tencent.feedback.anr.ANRHandleServiceTask.f3672a.set(false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:?, code lost:
        java.lang.Thread.sleep(500);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x0155, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:?, code lost:
        r0.printStackTrace();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.tencent.feedback.common.service.RQDService r10, android.content.Intent r11) {
        /*
            r9 = this;
            r2 = 1
            r7 = 0
            com.tencent.feedback.eup.d r0 = com.tencent.feedback.eup.c.a()
            if (r0 != 0) goto L_0x0016
            java.lang.String r0 = "rqdp{ init service eup}"
            java.lang.Object[] r1 = new java.lang.Object[r7]
            com.tencent.feedback.common.g.b(r0, r1)
            android.content.Context r0 = r10.getApplicationContext()
            com.tencent.feedback.eup.c.a(r0, r7)
        L_0x0016:
            java.util.concurrent.atomic.AtomicBoolean r0 = com.tencent.feedback.anr.ANRHandleServiceTask.f3672a
            boolean r0 = r0.get()
            if (r0 == 0) goto L_0x0026
            java.lang.String r0 = "handling task already exist!"
            java.lang.Object[] r1 = new java.lang.Object[r7]
            com.tencent.feedback.common.g.b(r0, r1)
        L_0x0025:
            return
        L_0x0026:
            java.util.concurrent.atomic.AtomicBoolean r0 = com.tencent.feedback.anr.ANRHandleServiceTask.f3672a
            r0.set(r2)
            android.util.SparseArray r1 = r9.d()     // Catch:{ Throwable -> 0x0071 }
            if (r1 == 0) goto L_0x0037
            int r0 = r1.size()     // Catch:{ Throwable -> 0x0071 }
            if (r0 > 0) goto L_0x0045
        L_0x0037:
            java.lang.String r0 = "impossible not pid in same app"
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Throwable -> 0x0071 }
            com.tencent.feedback.common.g.d(r0, r1)     // Catch:{ Throwable -> 0x0071 }
            java.util.concurrent.atomic.AtomicBoolean r0 = com.tencent.feedback.anr.ANRHandleServiceTask.f3672a
            r0.set(r7)
            goto L_0x0025
        L_0x0045:
            com.tencent.feedback.eup.jni.c r0 = r9.c()     // Catch:{ Throwable -> 0x0071 }
            if (r0 != 0) goto L_0x005c
            java.lang.String r0 = "read dump info fail,try again "
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x0071 }
            com.tencent.feedback.common.g.d(r0, r2)     // Catch:{ Throwable -> 0x0071 }
            r2 = 1000(0x3e8, double:4.94E-321)
            java.lang.Thread.sleep(r2)     // Catch:{ InterruptedException -> 0x006c }
        L_0x0058:
            com.tencent.feedback.eup.jni.c r0 = r9.c()     // Catch:{ Throwable -> 0x0071 }
        L_0x005c:
            if (r0 != 0) goto L_0x008c
            java.lang.String r0 = "read dump info fail again ,let it go"
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Throwable -> 0x0071 }
            com.tencent.feedback.common.g.d(r0, r1)     // Catch:{ Throwable -> 0x0071 }
            java.util.concurrent.atomic.AtomicBoolean r0 = com.tencent.feedback.anr.ANRHandleServiceTask.f3672a
            r0.set(r7)
            goto L_0x0025
        L_0x006c:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Throwable -> 0x0071 }
            goto L_0x0058
        L_0x0071:
            r0 = move-exception
            java.lang.String r1 = "task throw upload by catch"
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x011f }
            com.tencent.feedback.common.g.d(r1, r2)     // Catch:{ all -> 0x011f }
            java.lang.Thread r1 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x011f }
            r2 = 0
            r3 = 0
            com.tencent.feedback.eup.c.a(r1, r0, r2, r3)     // Catch:{ all -> 0x011f }
            r0.printStackTrace()     // Catch:{ all -> 0x011f }
            java.util.concurrent.atomic.AtomicBoolean r0 = com.tencent.feedback.anr.ANRHandleServiceTask.f3672a
            r0.set(r7)
            goto L_0x0025
        L_0x008c:
            java.lang.Class<com.tencent.feedback.anr.ANRHandleServiceTask> r2 = com.tencent.feedback.anr.ANRHandleServiceTask.class
            monitor-enter(r2)     // Catch:{ Throwable -> 0x0071 }
            com.tencent.feedback.eup.jni.c r3 = com.tencent.feedback.anr.ANRHandleServiceTask.b     // Catch:{ all -> 0x011c }
            if (r3 == 0) goto L_0x00d5
            com.tencent.feedback.eup.jni.c r3 = com.tencent.feedback.anr.ANRHandleServiceTask.b     // Catch:{ all -> 0x011c }
            long r3 = r3.f3699a     // Catch:{ all -> 0x011c }
            long r5 = r0.f3699a     // Catch:{ all -> 0x011c }
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 != 0) goto L_0x00d5
            com.tencent.feedback.eup.jni.c r3 = com.tencent.feedback.anr.ANRHandleServiceTask.b     // Catch:{ all -> 0x011c }
            java.lang.String r3 = r3.b     // Catch:{ all -> 0x011c }
            java.lang.String r4 = r0.b     // Catch:{ all -> 0x011c }
            boolean r3 = r3.equals(r4)     // Catch:{ all -> 0x011c }
            if (r3 == 0) goto L_0x00d5
            com.tencent.feedback.eup.jni.c r3 = com.tencent.feedback.anr.ANRHandleServiceTask.b     // Catch:{ all -> 0x011c }
            long r3 = r3.c     // Catch:{ all -> 0x011c }
            long r5 = r0.c     // Catch:{ all -> 0x011c }
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 != 0) goto L_0x00d5
            java.lang.String r0 = "same trace file same anr ,has handled! %s %d"
            r1 = 2
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x011c }
            r3 = 0
            com.tencent.feedback.eup.jni.c r4 = com.tencent.feedback.anr.ANRHandleServiceTask.b     // Catch:{ all -> 0x011c }
            java.lang.String r4 = r4.b     // Catch:{ all -> 0x011c }
            r1[r3] = r4     // Catch:{ all -> 0x011c }
            r3 = 1
            com.tencent.feedback.eup.jni.c r4 = com.tencent.feedback.anr.ANRHandleServiceTask.b     // Catch:{ all -> 0x011c }
            long r4 = r4.c     // Catch:{ all -> 0x011c }
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ all -> 0x011c }
            r1[r3] = r4     // Catch:{ all -> 0x011c }
            com.tencent.feedback.common.g.b(r0, r1)     // Catch:{ all -> 0x011c }
            monitor-exit(r2)     // Catch:{ all -> 0x011c }
            java.util.concurrent.atomic.AtomicBoolean r0 = com.tencent.feedback.anr.ANRHandleServiceTask.f3672a
            r0.set(r7)
            goto L_0x0025
        L_0x00d5:
            com.tencent.feedback.anr.ANRHandleServiceTask.b = r0     // Catch:{ all -> 0x011c }
            long r5 = r0.c     // Catch:{ all -> 0x011c }
            monitor-exit(r2)     // Catch:{ all -> 0x011c }
            java.util.List r0 = r9.a(r10)     // Catch:{ Throwable -> 0x0071 }
            if (r0 == 0) goto L_0x0135
            int r2 = r0.size()     // Catch:{ Throwable -> 0x0071 }
            if (r2 <= 0) goto L_0x0135
            int r1 = android.os.Process.myUid()     // Catch:{ Throwable -> 0x0071 }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ Throwable -> 0x0071 }
        L_0x00ee:
            boolean r0 = r2.hasNext()     // Catch:{ Throwable -> 0x0071 }
            if (r0 == 0) goto L_0x0126
            java.lang.Object r0 = r2.next()     // Catch:{ Throwable -> 0x0071 }
            com.tencent.feedback.anr.b r0 = (com.tencent.feedback.anr.b) r0     // Catch:{ Throwable -> 0x0071 }
            int r3 = r0.c()     // Catch:{ Throwable -> 0x0071 }
            if (r1 != r3) goto L_0x00ee
            java.lang.String r1 = "has anr in process %s handle it and leave"
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x0071 }
            r3 = 0
            java.lang.String r4 = r0.a()     // Catch:{ Throwable -> 0x0071 }
            r2[r3] = r4     // Catch:{ Throwable -> 0x0071 }
            com.tencent.feedback.common.g.b(r1, r2)     // Catch:{ Throwable -> 0x0071 }
            r0.a(r5)     // Catch:{ Throwable -> 0x0071 }
            r9.a(r10, r0)     // Catch:{ Throwable -> 0x0071 }
            java.util.concurrent.atomic.AtomicBoolean r0 = com.tencent.feedback.anr.ANRHandleServiceTask.f3672a
            r0.set(r7)
            goto L_0x0025
        L_0x011c:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ Throwable -> 0x0071 }
            throw r0     // Catch:{ Throwable -> 0x0071 }
        L_0x011f:
            r0 = move-exception
            java.util.concurrent.atomic.AtomicBoolean r1 = com.tencent.feedback.anr.ANRHandleServiceTask.f3672a
            r1.set(r7)
            throw r0
        L_0x0126:
            java.lang.String r0 = "not my anr ignorl ,leave"
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Throwable -> 0x0071 }
            com.tencent.feedback.common.g.b(r0, r1)     // Catch:{ Throwable -> 0x0071 }
            java.util.concurrent.atomic.AtomicBoolean r0 = com.tencent.feedback.anr.ANRHandleServiceTask.f3672a
            r0.set(r7)
            goto L_0x0025
        L_0x0135:
            r2 = 500(0x1f4, double:2.47E-321)
            java.lang.Thread.sleep(r2)     // Catch:{ InterruptedException -> 0x0155 }
        L_0x013a:
            android.util.SparseArray r3 = r9.d()     // Catch:{ Throwable -> 0x0071 }
            if (r3 == 0) goto L_0x0146
            int r0 = r3.size()     // Catch:{ Throwable -> 0x0071 }
            if (r0 > 0) goto L_0x015a
        L_0x0146:
            java.lang.String r0 = "impossible not pid in same app"
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Throwable -> 0x0071 }
            com.tencent.feedback.common.g.d(r0, r1)     // Catch:{ Throwable -> 0x0071 }
            java.util.concurrent.atomic.AtomicBoolean r0 = com.tencent.feedback.anr.ANRHandleServiceTask.f3672a
            r0.set(r7)
            goto L_0x0025
        L_0x0155:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Throwable -> 0x0071 }
            goto L_0x013a
        L_0x015a:
            r0 = r7
        L_0x015b:
            int r2 = r1.size()     // Catch:{ Throwable -> 0x0071 }
            if (r0 >= r2) goto L_0x0193
            int r2 = r1.keyAt(r0)     // Catch:{ Throwable -> 0x0071 }
            java.lang.Object r4 = r3.get(r2)     // Catch:{ Throwable -> 0x0071 }
            if (r4 != 0) goto L_0x0190
            java.lang.Object r3 = r1.get(r2)     // Catch:{ Throwable -> 0x0071 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ Throwable -> 0x0071 }
            java.lang.String r0 = "found process been kill pid:%d pn:%s , it should be anr proc ,handle it and leave"
            r1 = 2
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Throwable -> 0x0071 }
            r4 = 0
            java.lang.Integer r8 = java.lang.Integer.valueOf(r2)     // Catch:{ Throwable -> 0x0071 }
            r1[r4] = r8     // Catch:{ Throwable -> 0x0071 }
            r4 = 1
            r1[r4] = r3     // Catch:{ Throwable -> 0x0071 }
            com.tencent.feedback.common.g.b(r0, r1)     // Catch:{ Throwable -> 0x0071 }
            r4 = 0
            r0 = r9
            r1 = r10
            r0.a(r1, r2, r3, r4, r5)     // Catch:{ Throwable -> 0x0071 }
            java.util.concurrent.atomic.AtomicBoolean r0 = com.tencent.feedback.anr.ANRHandleServiceTask.f3672a
            r0.set(r7)
            goto L_0x0025
        L_0x0190:
            int r0 = r0 + 1
            goto L_0x015b
        L_0x0193:
            java.util.concurrent.atomic.AtomicBoolean r0 = com.tencent.feedback.anr.ANRHandleServiceTask.f3672a
            r0.set(r7)
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.anr.ANRHandleServiceTask.a(com.tencent.feedback.common.service.RQDService, android.content.Intent):void");
    }

    public c c() {
        if ("/data/anr/traces.txt" == 0) {
            g.d("path:%s", "/data/anr/traces.txt");
            return null;
        }
        c cVar = new c();
        b.a("/data/anr/traces.txt", new d(cVar, false));
        if (cVar.f3699a > 0 && cVar.c > 0 && cVar.b != null) {
            return cVar;
        }
        g.d("first dump error %s", cVar.f3699a + " " + cVar.c + " " + cVar.b);
        return null;
    }

    public SparseArray<String> d() {
        return b.a(Process.myUid());
    }

    public List<b> a(Context context) {
        b bVar;
        if (context == null) {
            return null;
        }
        ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
        int i = 0;
        while (true) {
            int i2 = i + 1;
            if (((long) i) >= 20) {
                return null;
            }
            List<ActivityManager.ProcessErrorStateInfo> processesInErrorState = activityManager.getProcessesInErrorState();
            if (processesInErrorState != null) {
                ArrayList arrayList = new ArrayList();
                for (ActivityManager.ProcessErrorStateInfo next : processesInErrorState) {
                    if (next.condition == 2) {
                        g.b("anr error found in %s \n lMsg:%s\n sMsg:%s", next.processName, next.longMsg, next.shortMsg);
                        if (next == null || next.condition != 2) {
                            bVar = null;
                        } else {
                            b bVar2 = new b();
                            bVar2.a(next.processName);
                            int i3 = next.pid;
                            bVar2.b(next.longMsg);
                            bVar2.a(next.uid);
                            bVar = bVar2;
                        }
                        arrayList.add(bVar);
                    }
                }
                if (arrayList.size() > 0) {
                    return arrayList;
                }
            }
            try {
                Thread.sleep(500);
                i = i2;
            } catch (InterruptedException e2) {
                e2.printStackTrace();
                i = i2;
            }
        }
    }

    public void a(Context context, int i, String str, String str2, long j) {
        if (context != null && i > 0 && str != null) {
            b bVar = new b();
            bVar.a(str);
            bVar.a(j);
            if (str2 == null) {
                str2 = "unvisiable ANR";
            }
            bVar.b(str2);
            a(context, bVar);
        }
    }

    public String a(Map<String, String[]> map) {
        if (map == null || map.size() <= 0) {
            return null;
        }
        HashMap hashMap = new HashMap();
        HashMap hashMap2 = new HashMap();
        Pattern compile = Pattern.compile("held by tid=\\d+");
        for (Map.Entry next : map.entrySet()) {
            hashMap.put(((String[]) next.getValue())[2], next.getKey());
            Matcher matcher = compile.matcher(((String[]) next.getValue())[1]);
            if (matcher.find()) {
                hashMap2.put(((String[]) next.getValue())[2], null);
                String group = matcher.group();
                hashMap2.put(group.substring(group.indexOf("=") + 1), null);
            } else if ("main".equals(next.getKey())) {
                hashMap2.put(((String[]) next.getValue())[2], null);
            }
        }
        if (hashMap2.size() <= 0) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (String str : hashMap2.keySet()) {
            hashMap2.put(str, hashMap.get(str));
            String str2 = (String) hashMap.get(str);
            String str3 = map.get(str2)[0];
            stringBuffer.append("\"" + str2 + "\" tid=" + str + " :\n" + str3 + "\n" + map.get(str2)[1] + "\n");
        }
        return stringBuffer.toString();
    }

    public void a(Context context, b bVar) {
        c cVar;
        if (context != null && bVar != null) {
            if (bVar.a() == null || "/data/anr/traces.txt" == 0) {
                cVar = null;
            } else {
                cVar = new c();
                b.a("/data/anr/traces.txt", new c(cVar, true));
                if (cVar.f3699a <= 0 || cVar.c <= 0 || cVar.b == null) {
                    cVar = null;
                }
            }
            if (cVar != null) {
                String a2 = a(cVar.d);
                if (a2 == null) {
                    a2 = "dump traces fail!";
                }
                bVar.c(a2);
            }
            b(context, bVar);
        }
    }

    public void b(Context context, b bVar) {
        d dVar = new d();
        dVar.a(this.c);
        e a2 = f.a(context, this.d, this.f, this.e, this.g, bVar.a(), "main", Constants.STR_EMPTY, "ANR_RQD_EXCEPTION", Constants.STR_EMPTY, bVar.e(), bVar.b(), bVar.d(), null);
        a2.e(true);
        boolean a3 = i.a(context).a(a2, dVar);
        g.b("sha1:%s %d", a2.q(), Integer.valueOf(a2.o()));
        g.b("handle anr %b", Boolean.valueOf(a3));
    }
}
