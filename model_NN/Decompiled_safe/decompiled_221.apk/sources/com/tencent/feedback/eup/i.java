package com.tencent.feedback.eup;

import android.content.Context;
import android.os.Process;
import com.tencent.connect.common.Constants;
import com.tencent.feedback.common.b;
import com.tencent.feedback.common.g;
import com.tencent.feedback.proguard.ac;
import com.tencent.open.SocialConstants;
import java.io.File;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public final class i implements Thread.UncaughtExceptionHandler {
    private static i c = null;

    /* renamed from: a  reason: collision with root package name */
    private Thread.UncaughtExceptionHandler f3694a = null;
    private Context b = null;

    public static synchronized i a(Context context) {
        i iVar;
        synchronized (i.class) {
            if (c == null && context != null) {
                c = new i(context);
            }
            iVar = c;
        }
        return iVar;
    }

    private i(Context context) {
        this.b = context.getApplicationContext();
    }

    public final synchronized void a() {
        g.e("rqdp{ eup regist}", new Object[0]);
        Thread.UncaughtExceptionHandler defaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
        if (defaultUncaughtExceptionHandler != this) {
            this.f3694a = defaultUncaughtExceptionHandler;
            Thread.setDefaultUncaughtExceptionHandler(this);
        }
    }

    public final synchronized void b() {
        g.e("rqdp{ eup unregister}", new Object[0]);
        if (Thread.getDefaultUncaughtExceptionHandler() == this) {
            Thread.setDefaultUncaughtExceptionHandler(this.f3694a);
            this.f3694a = null;
        }
    }

    private synchronized void a(Thread thread, Throwable th) {
        if (this.f3694a != null) {
            g.e("rqdp{ syscrhandle!}", new Object[0]);
            this.f3694a.uncaughtException(thread, th);
        } else {
            g.e("rqdp{ kill!}", new Object[0]);
            Process.killProcess(Process.myPid());
            System.exit(1);
        }
    }

    private static void c() {
        g gVar = new g();
        gVar.setName("ImmediateEUP");
        gVar.start();
        try {
            gVar.join(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private int a(List<e> list, int i, boolean z) {
        Context context = this.b;
        if (list == null || i <= 0) {
            return 0;
        }
        ArrayList arrayList = new ArrayList();
        Collections.sort(list, new h());
        Iterator<e> it = list.iterator();
        while (it.hasNext() && i > arrayList.size()) {
            e next = it.next();
            if (next.b() && !z) {
                break;
            }
            arrayList.add(next);
            it.remove();
        }
        if (arrayList.size() > 0) {
            return f.a(context, arrayList);
        }
        return 0;
    }

    public final boolean a(e eVar, d dVar) {
        e eVar2;
        int size;
        e eVar3;
        boolean z;
        if (eVar == null && dVar == null) {
            g.c("handler exception data params error", new Object[0]);
            return false;
        }
        if (dVar.e()) {
            Context context = this.b;
            if (eVar == null) {
                eVar3 = null;
            } else {
                String c2 = ac.c(ac.c(eVar.h()).getBytes());
                if (c2 == null) {
                    g.c("rqdp{  md5 error!}", new Object[0]);
                    eVar3 = null;
                } else {
                    eVar.g(c2);
                    eVar.b(true);
                    eVar.b(1);
                    eVar.a(0);
                    List<e> a2 = f.a(context, 1, SocialConstants.PARAM_APP_DESC, -1, c2, -1, -1, -1, -1, -1, -1, null);
                    if (a2 == null || a2.size() <= 0) {
                        g.b("rqdp{  new one ,no merged!}", new Object[0]);
                        eVar3 = null;
                    } else {
                        eVar3 = a2.get(0);
                        if (eVar3.n() == null || !eVar3.n().contains(new StringBuilder().append(eVar.i()).toString())) {
                            eVar3.b(eVar3.o() + 1);
                            if (eVar3.n() == null) {
                                eVar3.f(Constants.STR_EMPTY);
                            }
                            eVar3.f(eVar3.n() + eVar.i() + "\n");
                            g.b("rqdp{  EUPDAO.insertOrUpdateEUP() start}", new Object[0]);
                            if (context == null || eVar3 == null) {
                                g.c("rqdp{  context == null || bean == null,pls check}", new Object[0]);
                                z = false;
                            } else {
                                ArrayList arrayList = new ArrayList();
                                arrayList.add(eVar3);
                                z = f.b(context, arrayList);
                            }
                            if (z) {
                                g.g("rqdp{  eupMeg update success} %b , c:%d , at:%s up:%d", Boolean.valueOf(z), Integer.valueOf(eVar3.o()), eVar3.n(), Integer.valueOf(eVar3.l()));
                                if (eVar.r() != null) {
                                    File file = new File(eVar.r());
                                    if (file.exists() && file.isFile()) {
                                        file.delete();
                                    }
                                }
                            }
                        } else {
                            g.b("rqdp{ already merged} %d", Long.valueOf(eVar.i()));
                        }
                    }
                }
            }
            if (eVar3 != null) {
                g.e("merge success return", new Object[0]);
                if (!eVar3.y() && eVar3.o() >= 2) {
                    g.e("rqdp{ may be crash too frequent! do immediate upload in merge!}", new Object[0]);
                    c();
                }
                return true;
            }
        }
        int a3 = dVar.a();
        List<e> a4 = f.a(this.b, a3 + 1, "asc", -1, null, -1, -1, -1, -1, -1, -1, null);
        if (a4 == null || a4.size() <= 0 || (size = (a4.size() - a3) + 1) <= 0 || a(a4, size, eVar.b()) >= size) {
            if (a4 != null && a4.size() > 1) {
                e eVar4 = a4.get(0);
                Iterator<e> it = a4.iterator();
                while (true) {
                    eVar2 = eVar4;
                    if (!it.hasNext()) {
                        break;
                    }
                    eVar4 = it.next();
                    if (eVar2.i() >= eVar4.i() || !eVar4.b()) {
                        eVar4 = eVar2;
                    }
                }
                if (eVar2.b() && eVar.i() - eVar2.i() < 60000) {
                    g.e("rqdp{ may be crash too frequent! do immediate upload in not merge!}", new Object[0]);
                    c();
                }
            }
            f.a(this.b, eVar, dVar);
            if (b.e(this.b)) {
                g.e("save log", new Object[0]);
                eVar.a(ac.a(dVar.g(), dVar.f()));
            } else {
                eVar.a((byte[]) null);
            }
            boolean a5 = f.a(this.b, eVar);
            g.g("store new eup pn:%s, isMe:%b , isNa:%b , res:%b ", eVar.s(), Boolean.valueOf(eVar.c()), Boolean.valueOf(eVar.d()), Boolean.valueOf(a5));
            return a5;
        }
        g.e("can't add more eup!", new Object[0]);
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x00c5 A[Catch:{ Throwable -> 0x0158 }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0103  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x017b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(java.lang.String r26, java.lang.Throwable r27, java.lang.String r28, byte[] r29, boolean r30) {
        /*
            r25 = this;
            r0 = r25
            android.content.Context r1 = r0.b
            java.lang.String r15 = com.tencent.feedback.common.b.h(r1)
            java.lang.String r5 = ""
            java.lang.String r4 = ""
            if (r27 == 0) goto L_0x010e
            java.lang.String r19 = r27.getMessage()
        L_0x0012:
            if (r27 == 0) goto L_0x0112
            java.lang.Class r1 = r27.getClass()
            java.lang.String r3 = r1.getName()
        L_0x001c:
            com.tencent.feedback.eup.k r1 = com.tencent.feedback.eup.k.k()
            if (r1 != 0) goto L_0x0116
            java.lang.String r1 = "rqdp{  instance == null}"
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]
            com.tencent.feedback.common.g.c(r1, r2)
            r1 = 0
        L_0x002b:
            java.util.Date r2 = new java.util.Date
            r2.<init>()
            long r7 = r2.getTime()
            com.tencent.feedback.eup.d r2 = com.tencent.feedback.eup.c.a()     // Catch:{ Throwable -> 0x011c }
            r0 = r27
            java.lang.String r5 = com.tencent.feedback.eup.f.a(r0, r2)     // Catch:{ Throwable -> 0x011c }
        L_0x003e:
            if (r5 == 0) goto L_0x0053
            java.lang.String r2 = "\n"
            boolean r2 = r5.contains(r2)
            if (r2 == 0) goto L_0x0053
            r2 = 0
            java.lang.String r4 = "\n"
            int r4 = r5.indexOf(r4)
            java.lang.String r4 = r5.substring(r2, r4)
        L_0x0053:
            java.lang.String r2 = "rqdp{ stack:}%s"
            r6 = 1
            java.lang.Object[] r6 = new java.lang.Object[r6]
            r9 = 0
            r6[r9] = r5
            com.tencent.feedback.common.g.b(r2, r6)
            if (r30 == 0) goto L_0x0152
            if (r1 == 0) goto L_0x0152
            java.lang.String r2 = "get crash extra..."
            r6 = 0
            java.lang.Object[] r6 = new java.lang.Object[r6]
            com.tencent.feedback.common.g.b(r2, r6)
            if (r1 == 0) goto L_0x007b
            java.lang.String r2 = "your crmsg"
            r6 = 0
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x012a }
            com.tencent.feedback.common.g.a(r2, r6)     // Catch:{ Throwable -> 0x012a }
            r2 = 0
            r6 = -10000(0xffffffffffffd8f0, float:NaN)
            java.lang.String r28 = r1.b(r2, r3, r4, r5, r6, r7)     // Catch:{ Throwable -> 0x012a }
        L_0x007b:
            if (r1 == 0) goto L_0x0152
            java.lang.String r2 = "your crdata"
            r6 = 0
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x013f }
            com.tencent.feedback.common.g.a(r2, r6)     // Catch:{ Throwable -> 0x013f }
            r2 = 0
            r6 = -10000(0xffffffffffffd8f0, float:NaN)
            byte[] r29 = r1.a(r2, r3, r4, r5, r6, r7)     // Catch:{ Throwable -> 0x013f }
            r24 = r29
            r23 = r28
        L_0x0090:
            r0 = r25
            android.content.Context r2 = r0.b
            com.tencent.feedback.common.e r2 = com.tencent.feedback.common.e.a(r2)
            r0 = r25
            android.content.Context r9 = r0.b
            java.lang.String r10 = r2.i()
            java.lang.String r11 = r2.p()
            long r12 = r2.l()
            java.util.Map r14 = r2.y()
            r16 = r26
            r17 = r4
            r18 = r3
            r20 = r5
            r21 = r7
            com.tencent.feedback.eup.e r13 = com.tencent.feedback.eup.f.a(r9, r10, r11, r12, r14, r15, r16, r17, r18, r19, r20, r21, r23, r24)
            r0 = r30
            r13.a(r0)
            java.util.Map r2 = com.tencent.feedback.eup.f.a()     // Catch:{ Throwable -> 0x0158 }
            if (r2 == 0) goto L_0x00e1
            java.util.Map r6 = r13.F()     // Catch:{ Throwable -> 0x0158 }
            r6.putAll(r2)     // Catch:{ Throwable -> 0x0158 }
            if (r26 == 0) goto L_0x00e1
            java.lang.String r2 = r26.trim()     // Catch:{ Throwable -> 0x0158 }
            int r2 = r2.length()     // Catch:{ Throwable -> 0x0158 }
            if (r2 <= 0) goto L_0x00e1
            java.util.Map r2 = r13.F()     // Catch:{ Throwable -> 0x0158 }
            r0 = r26
            r2.remove(r0)     // Catch:{ Throwable -> 0x0158 }
        L_0x00e1:
            r12 = 1
            if (r30 == 0) goto L_0x0179
            if (r1 == 0) goto L_0x0179
            java.lang.String r2 = "your ask2save"
            r6 = 0
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x0166 }
            com.tencent.feedback.common.g.a(r2, r6)     // Catch:{ Throwable -> 0x0166 }
            r2 = 0
            r6 = -10000(0xffffffffffffd8f0, float:NaN)
            java.lang.String r9 = r13.m()     // Catch:{ Throwable -> 0x0166 }
            java.lang.String r10 = r13.G()     // Catch:{ Throwable -> 0x0166 }
            java.lang.String r11 = r13.x()     // Catch:{ Throwable -> 0x0166 }
            boolean r1 = r1.a(r2, r3, r4, r5, r6, r7, r9, r10, r11)     // Catch:{ Throwable -> 0x0166 }
        L_0x0101:
            if (r1 == 0) goto L_0x017b
            com.tencent.feedback.eup.d r1 = com.tencent.feedback.eup.c.a()
            r0 = r25
            boolean r1 = r0.a(r13, r1)
        L_0x010d:
            return r1
        L_0x010e:
            java.lang.String r19 = ""
            goto L_0x0012
        L_0x0112:
            java.lang.String r3 = ""
            goto L_0x001c
        L_0x0116:
            com.tencent.feedback.eup.b r1 = r1.p()
            goto L_0x002b
        L_0x011c:
            r2 = move-exception
            java.lang.String r6 = "create stack from throw fail!"
            r9 = 0
            java.lang.Object[] r9 = new java.lang.Object[r9]
            com.tencent.feedback.common.g.d(r6, r9)
            r2.printStackTrace()
            goto L_0x003e
        L_0x012a:
            r2 = move-exception
            java.lang.String r6 = "rqdp{ get extra msg error} %s"
            r9 = 1
            java.lang.Object[] r9 = new java.lang.Object[r9]
            r10 = 0
            java.lang.String r11 = r2.toString()
            r9[r10] = r11
            com.tencent.feedback.common.g.d(r6, r9)
            r2.printStackTrace()
            goto L_0x007b
        L_0x013f:
            r2 = move-exception
            java.lang.String r6 = "rqdp{ get extra msg error} %s"
            r9 = 1
            java.lang.Object[] r9 = new java.lang.Object[r9]
            r10 = 0
            java.lang.String r11 = r2.toString()
            r9[r10] = r11
            com.tencent.feedback.common.g.d(r6, r9)
            r2.printStackTrace()
        L_0x0152:
            r24 = r29
            r23 = r28
            goto L_0x0090
        L_0x0158:
            r2 = move-exception
            java.lang.String r6 = "get all threads stack fail"
            r9 = 0
            java.lang.Object[] r9 = new java.lang.Object[r9]
            com.tencent.feedback.common.g.d(r6, r9)
            r2.printStackTrace()
            goto L_0x00e1
        L_0x0166:
            r1 = move-exception
            java.lang.String r2 = "rqdp{ get extra msg error} %s"
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]
            r4 = 0
            java.lang.String r5 = r1.toString()
            r3[r4] = r5
            com.tencent.feedback.common.g.d(r2, r3)
            r1.printStackTrace()
        L_0x0179:
            r1 = r12
            goto L_0x0101
        L_0x017b:
            java.lang.String r1 = "not to save"
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]
            com.tencent.feedback.common.g.c(r1, r2)
            r1 = 0
            goto L_0x010d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.eup.i.a(java.lang.String, java.lang.Throwable, java.lang.String, byte[], boolean):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0052 A[SYNTHETIC, Splitter:B:23:0x0052] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void uncaughtException(java.lang.Thread r9, java.lang.Throwable r10) {
        /*
            r8 = this;
            r0 = 0
            r6 = 1
            monitor-enter(r8)
            if (r10 == 0) goto L_0x0008
            r10.printStackTrace()     // Catch:{ all -> 0x0071 }
        L_0x0008:
            com.tencent.feedback.eup.k r1 = com.tencent.feedback.eup.k.k()     // Catch:{ all -> 0x0071 }
            if (r1 != 0) goto L_0x0057
            java.lang.String r1 = "rqdp{  instance == null}"
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x0071 }
            com.tencent.feedback.common.g.c(r1, r2)     // Catch:{ all -> 0x0071 }
            r7 = r0
        L_0x0017:
            if (r7 == 0) goto L_0x0025
            java.lang.String r0 = "your crhandler start"
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Throwable -> 0x005d }
            com.tencent.feedback.common.g.a(r0, r1)     // Catch:{ Throwable -> 0x005d }
            r0 = 0
            r7.a(r0)     // Catch:{ Throwable -> 0x005d }
        L_0x0025:
            if (r9 != 0) goto L_0x0074
            java.lang.String r1 = ""
        L_0x0029:
            r3 = 0
            r4 = 0
            r5 = 1
            r0 = r8
            r2 = r10
            boolean r0 = r0.a(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x0071 }
            java.lang.String r1 = "rqdp{ handle eup result} %b"
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x0071 }
            r3 = 0
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x0071 }
            r2[r3] = r0     // Catch:{ all -> 0x0071 }
            com.tencent.feedback.common.g.e(r1, r2)     // Catch:{ all -> 0x0071 }
            if (r7 == 0) goto L_0x008c
            java.lang.String r0 = "your crhandler end"
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Throwable -> 0x0079 }
            com.tencent.feedback.common.g.a(r0, r1)     // Catch:{ Throwable -> 0x0079 }
            r0 = 0
            boolean r0 = r7.b(r0)     // Catch:{ Throwable -> 0x0079 }
        L_0x0050:
            if (r0 == 0) goto L_0x0055
            r8.a(r9, r10)     // Catch:{ all -> 0x0071 }
        L_0x0055:
            monitor-exit(r8)
            return
        L_0x0057:
            com.tencent.feedback.eup.b r0 = r1.p()     // Catch:{ all -> 0x0071 }
            r7 = r0
            goto L_0x0017
        L_0x005d:
            r0 = move-exception
            java.lang.String r1 = "rqdp{ handle start error} %s"
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x0071 }
            r3 = 0
            java.lang.String r4 = r0.toString()     // Catch:{ all -> 0x0071 }
            r2[r3] = r4     // Catch:{ all -> 0x0071 }
            com.tencent.feedback.common.g.d(r1, r2)     // Catch:{ all -> 0x0071 }
            r0.printStackTrace()     // Catch:{ all -> 0x0071 }
            goto L_0x0025
        L_0x0071:
            r0 = move-exception
            monitor-exit(r8)
            throw r0
        L_0x0074:
            java.lang.String r1 = r9.getName()     // Catch:{ all -> 0x0071 }
            goto L_0x0029
        L_0x0079:
            r0 = move-exception
            java.lang.String r1 = "rqdp{ your crash handle end error} %s"
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x0071 }
            r3 = 0
            java.lang.String r4 = r0.toString()     // Catch:{ all -> 0x0071 }
            r2[r3] = r4     // Catch:{ all -> 0x0071 }
            com.tencent.feedback.common.g.d(r1, r2)     // Catch:{ all -> 0x0071 }
            r0.printStackTrace()     // Catch:{ all -> 0x0071 }
        L_0x008c:
            r0 = r6
            goto L_0x0050
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.eup.i.uncaughtException(java.lang.Thread, java.lang.Throwable):void");
    }
}
