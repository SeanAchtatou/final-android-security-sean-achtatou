package com.tencent.feedback.eup;

import android.content.Context;
import com.tencent.feedback.a.b;
import com.tencent.feedback.a.i;
import com.tencent.feedback.a.j;
import com.tencent.feedback.common.g;
import com.tencent.feedback.proguard.ac;
import com.tencent.feedback.proguard.aj;
import com.tencent.feedback.proguard.ao;
import com.tencent.securemodule.service.Constants;

/* compiled from: ProGuard */
public final class k extends com.tencent.feedback.common.k {
    private static k b;
    private d c = null;
    private d d = null;
    private i e = null;
    private b f = null;
    private final boolean g;

    public static synchronized k a(Context context, String str, boolean z, i iVar, b bVar, b bVar2, d dVar) {
        k kVar;
        synchronized (k.class) {
            if (b == null) {
                g.e("rqdp{  eup create instance}", new Object[0]);
                k kVar2 = new k(context, str, false, iVar, bVar, bVar2, dVar);
                b = kVar2;
                kVar2.a(true);
            }
            kVar = b;
        }
        return kVar;
    }

    public static synchronized k k() {
        k kVar;
        synchronized (k.class) {
            kVar = b;
        }
        return kVar;
    }

    public static synchronized i a(Context context, boolean z) {
        j a2;
        synchronized (k.class) {
            a2 = j.a(context, z);
        }
        return a2;
    }

    public static boolean l() {
        if (!m()) {
            return false;
        }
        g.e("rqdp{  doUploadExceptionDatas}", new Object[0]);
        k k = k();
        if (k != null) {
            return k.h();
        }
        g.c("rqdp{  instance == null}", new Object[0]);
        return false;
    }

    public static boolean a(Thread thread, Throwable th, String str, byte[] bArr) {
        g.e("rqdp{  handleCatchException}", new Object[0]);
        if (!m()) {
            return false;
        }
        k k = k();
        if (k == null) {
            g.c("rqdp{  instance == null}", new Object[0]);
            return false;
        }
        if (k.a()) {
            try {
                i s = k.s();
                if (s == null) {
                    g.c("rqdp{  imposiable chandler null!}", new Object[0]);
                    return false;
                }
                return s.a(thread == null ? null : thread.getName(), th, str, bArr, false);
            } catch (Throwable th2) {
                th2.printStackTrace();
                g.d("rqdp{  handleCatchException error} %s", th2.toString());
            }
        }
        return false;
    }

    public static boolean m() {
        k k = k();
        if (k == null) {
            g.d("rqdp{  not init eup}", new Object[0]);
            return false;
        }
        boolean a2 = k.a();
        if (!a2 || !k.r()) {
            return a2;
        }
        return k.b();
    }

    private k(Context context, String str, boolean z, i iVar, b bVar, b bVar2, d dVar) {
        super(context, str, 3, Constants.PLATFROM_ANDROID_PAD, 302, iVar, new j(context.getApplicationContext()), bVar);
        if (dVar != null) {
            g.e("rqdp{  cus eupstrategy} %s", dVar);
            this.c = dVar;
        } else {
            g.e("rqdp{  default eupstrategy}", new Object[0]);
            this.c = new d();
        }
        this.e = i.a(this.f3685a);
        this.f = bVar2;
        this.g = z;
    }

    private synchronized boolean r() {
        return this.g;
    }

    public final synchronized d n() {
        return this.c;
    }

    public final synchronized d o() {
        return this.d;
    }

    public final synchronized void a(d dVar) {
        this.d = dVar;
    }

    private synchronized i s() {
        return this.e;
    }

    public final synchronized b p() {
        return this.f;
    }

    public final void f() {
        int i = -1;
        super.f();
        Context context = this.f3685a;
        g.b("rqdp{  EUPDAO.deleteEup() start}", new Object[0]);
        if (context == null) {
            g.c("rqdp{  deleteEup() context is null arg}", new Object[0]);
        } else {
            i = aj.a(context, new int[]{1, 2}, -1, Long.MAX_VALUE, -1, -1);
        }
        g.e("rqdp{  eup clear} %d ", Integer.valueOf(i));
        g.e("rqdp{  eup strategy clear} %d ", Integer.valueOf(ac.b(this.f3685a, 302)));
    }

    public final boolean i() {
        return o() != null;
    }

    public final boolean h() {
        if (super.h()) {
            l a2 = l.a(this.f3685a);
            i c2 = c();
            if (a2 == null || c2 == null) {
                g.c("rqdp{  upDatas or uphandler null!}", new Object[0]);
                return false;
            }
            try {
                c2.a(a2);
                return true;
            } catch (Throwable th) {
                th.printStackTrace();
                g.d("rqdp{  upload eupdata error} %s", th.toString());
            }
        }
        return false;
    }

    public final int g() {
        d q = q();
        if (q == null || super.g() < 0) {
            return -1;
        }
        if (!q.e()) {
            g.e("rqdp{  in no merge}", new Object[0]);
            return f.b(this.f3685a);
        }
        g.e("rqdp{  in merge}", new Object[0]);
        if (f.a(this.f3685a)) {
            return 1;
        }
        return 0;
    }

    public final d q() {
        d dVar;
        try {
            if (ao.a(this.f3685a).b().h()) {
                dVar = o();
            } else {
                dVar = null;
            }
            if (dVar == null) {
                return n();
            }
            return dVar;
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    public final synchronized void b(boolean z) {
        super.b(z);
        if (a()) {
            this.e.a();
        } else {
            this.e.b();
        }
    }

    public final void e() {
        int i = -1;
        super.e();
        Context context = this.f3685a;
        g.b("rqdp{  EUPDAO.deleteEup() start}", new Object[0]);
        if (context == null) {
            g.c("rqdp{  deleteEup() context is null arg}", new Object[0]);
        } else {
            i = aj.a(context, new int[]{1, 2}, -1, Long.MAX_VALUE, 3, -1);
        }
        g.b("remove fail updata num :%d", Integer.valueOf(i));
    }
}
