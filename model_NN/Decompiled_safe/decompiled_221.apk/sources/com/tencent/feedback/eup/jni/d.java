package com.tencent.feedback.eup.jni;

import android.content.Context;
import com.tencent.feedback.common.g;
import com.tencent.feedback.eup.e;
import com.tencent.feedback.eup.f;
import java.io.File;
import java.util.Collections;
import java.util.LinkedList;

/* compiled from: ProGuard */
public final class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private File f3700a;
    /* access modifiers changed from: private */
    public long b;
    private int c;
    private int d;
    private Context e;

    static /* synthetic */ int a(d dVar) {
        int i = dVar.d;
        dVar.d = i + 1;
        return i;
    }

    public d(Context context, String str, long j, int i) {
        this.f3700a = new File(str);
        this.b = j;
        this.c = i;
        this.e = context;
    }

    public final void run() {
        this.d = 0;
        if (this.f3700a == null || !this.f3700a.exists() || !this.f3700a.isDirectory()) {
            g.c("rqdp{  TombFilesCleanTask mDir == null || !mDir.exists() || !mDir.isDirectory() ,pls check!}", new Object[0]);
            return;
        }
        g.a("rqdp{ start to find native record}", new Object[0]);
        e a2 = c.a(this.e, this.f3700a.getAbsolutePath());
        if (a2 != null) {
            g.a("found a record insert %s", a2.e());
            a2.f(true);
            f.a(this.e, a2);
        }
        File file = new File(this.f3700a.getAbsolutePath(), "rqd_record.eup");
        if (file.exists() && file.canWrite()) {
            file.delete();
            g.b("delete record file %s", file.getAbsoluteFile());
        }
        g.a("rqdp{  start to clean} %s.* rqdp{  in dir} %s rqdp{  which time <} %s rqdp{  and max file nums should <} %s", "tomb_", this.f3700a.getAbsolutePath(), Long.valueOf(this.b), Integer.valueOf(this.c));
        LinkedList linkedList = new LinkedList();
        String[] list = this.f3700a.list(new f(this, 5, 4, linkedList));
        int length = list != null ? list.length : 0;
        if (length > 0) {
            g.b("rqdp{  delete old num} %d", Integer.valueOf(length));
            a(this.f3700a.getAbsolutePath(), list);
        }
        int i = (this.d - length) - this.c;
        int size = linkedList.size();
        if (i > 0 && size > 0) {
            g.a("rqdp{  should delete not too old file num} %d", Integer.valueOf(i));
            Collections.sort(linkedList);
            if (size <= i) {
                i = size;
            }
            String[] strArr = new String[i];
            StringBuffer stringBuffer = new StringBuffer();
            int i2 = 0;
            while (i2 < linkedList.size() && i2 < strArr.length) {
                stringBuffer.append("tomb_");
                stringBuffer.append(linkedList.get(i2));
                stringBuffer.append(".txt");
                strArr[i2] = stringBuffer.toString();
                stringBuffer.delete(0, stringBuffer.length());
                i2++;
            }
            g.b("rqdp{  delete not too old files} %d", Integer.valueOf(strArr.length));
            a(this.f3700a.getAbsolutePath(), strArr);
        }
        g.a("rqdp{  clean end!}", new Object[0]);
    }

    private static void a(String str, String[] strArr) {
        if (strArr == null || strArr.length <= 0) {
            g.c("rqdp{  fileNameList == null || fileNameList.length <= 0}", new Object[0]);
            return;
        }
        for (String file : strArr) {
            File file2 = new File(str, file);
            if (file2.exists() && file2.canWrite()) {
                g.b("rqdp{  file delete} %s", file2.getPath());
                file2.delete();
            }
        }
    }
}
