package com.tencent.feedback.proguard;

import com.tencent.feedback.common.c;
import com.tencent.feedback.common.g;
import java.util.Date;

/* compiled from: ProGuard */
final class ax implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ aw f3735a;

    ax(aw awVar) {
        this.f3735a = awVar;
    }

    public final void run() {
        long e = this.f3735a.e();
        long time = new Date().getTime();
        long j = e - time;
        c f = this.f3735a.f();
        if (f == null) {
            return;
        }
        if (j > 0) {
            f.a(this, j + 1000);
            return;
        }
        g.e("rqdp{  next day to upload}", new Object[0]);
        this.f3735a.c();
        long d = this.f3735a.d();
        this.f3735a.b(d);
        f.a(this, d - time);
        g.b("rqdp{ next day %d}", Long.valueOf(d - time));
    }
}
