package com.tencent.feedback.proguard;

import com.tencent.connect.common.Constants;

public final class W extends C0008j {

    /* renamed from: a  reason: collision with root package name */
    public String f3716a = Constants.STR_EMPTY;
    public String b = Constants.STR_EMPTY;
    public int c = 0;
    public int d = 0;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int */
    public final void a(ag agVar) {
        this.f3716a = agVar.b(0, true);
        this.b = agVar.b(1, true);
        this.c = agVar.a(this.c, 2, true);
        this.d = agVar.a(this.d, 3, true);
    }

    public final void a(ah ahVar) {
        ahVar.a(this.f3716a, 0);
        ahVar.a(this.b, 1);
        ahVar.a(this.c, 2);
        ahVar.a(this.d, 3);
    }

    public final void a(StringBuilder sb, int i) {
    }
}
