package com.tencent.feedback.proguard;

import com.tencent.connect.common.Constants;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* compiled from: ProGuard */
public class ad extends ac {
    protected HashMap<String, byte[]> d = null;
    private HashMap<String, Object> e = new HashMap<>();
    private ag f = new ag();

    public void d() {
        this.d = new HashMap<>();
    }

    public <T> void a(String str, Object obj) {
        if (this.d == null) {
            super.a(str, obj);
        } else if (str == null) {
            throw new IllegalArgumentException("put key can not is null");
        } else if (obj == null) {
            throw new IllegalArgumentException("put value can not is null");
        } else if (obj instanceof Set) {
            throw new IllegalArgumentException("can not support Set");
        } else {
            ah ahVar = new ah();
            ahVar.a(this.b);
            ahVar.a(obj, 0);
            this.d.put(str, ai.a(ahVar.a()));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.lang.Object, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object */
    public final <T> T b(String str, Object obj) {
        byte[] bArr;
        if (this.d != null) {
            if (!this.d.containsKey(str)) {
                return null;
            }
            if (this.e.containsKey(str)) {
                return this.e.get(str);
            }
            try {
                this.f.a(this.d.get(str));
                this.f.a(this.b);
                T a2 = this.f.a(obj, 0, true);
                if (a2 == null) {
                    return a2;
                }
                this.e.put(str, a2);
                return a2;
            } catch (Exception e2) {
                throw new C0005b(e2);
            }
        } else if (!this.f3720a.containsKey(str)) {
            return null;
        } else {
            if (this.e.containsKey(str)) {
                return this.e.get(str);
            }
            byte[] bArr2 = new byte[0];
            Iterator it = ((HashMap) this.f3720a.get(str)).entrySet().iterator();
            if (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                entry.getKey();
                bArr = (byte[]) entry.getValue();
            } else {
                bArr = bArr2;
            }
            try {
                this.f.a(bArr);
                this.f.a(this.b);
                T a3 = this.f.a(obj, 0, true);
                this.e.put(str, a3);
                return a3;
            } catch (Exception e3) {
                throw new C0005b(e3);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ah.a(java.util.Map, int):void
     arg types: [java.util.HashMap<java.lang.String, byte[]>, int]
     candidates:
      com.tencent.feedback.proguard.ah.a(byte, int):void
      com.tencent.feedback.proguard.ah.a(float, int):void
      com.tencent.feedback.proguard.ah.a(int, int):void
      com.tencent.feedback.proguard.ah.a(long, int):void
      com.tencent.feedback.proguard.ah.a(com.tencent.feedback.proguard.j, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.Object, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.String, int):void
      com.tencent.feedback.proguard.ah.a(java.util.Collection, int):void
      com.tencent.feedback.proguard.ah.a(short, int):void
      com.tencent.feedback.proguard.ah.a(boolean, int):void
      com.tencent.feedback.proguard.ah.a(byte[], int):void
      com.tencent.feedback.proguard.ah.a(java.util.Map, int):void */
    public byte[] a() {
        if (this.d == null) {
            return super.a();
        }
        ah ahVar = new ah(0);
        ahVar.a(this.b);
        ahVar.a((Map) this.d, 0);
        return ai.a(ahVar.a());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
     arg types: [java.util.HashMap, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V> */
    public void a(byte[] bArr) {
        try {
            super.a(bArr);
        } catch (Exception e2) {
            this.f.a(bArr);
            this.f.a(this.b);
            HashMap hashMap = new HashMap(1);
            hashMap.put(Constants.STR_EMPTY, new byte[0]);
            this.d = this.f.a((Map) hashMap, 0, false);
        }
    }
}
