package com.tencent.f.a.a;

/* compiled from: ProGuard */
public final class c implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private long f3665a;

    public c(long j) {
        this.f3665a = j;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof c) || this.f3665a != ((c) obj).b()) {
            return false;
        }
        return true;
    }

    public byte[] a() {
        return new byte[]{(byte) ((int) (this.f3665a & 255)), (byte) ((int) ((this.f3665a & 65280) >> 8)), (byte) ((int) ((this.f3665a & 16711680) >> 16)), (byte) ((int) ((this.f3665a & 4278190080L) >> 24))};
    }

    public long b() {
        return this.f3665a;
    }

    public int hashCode() {
        return (int) this.f3665a;
    }
}
