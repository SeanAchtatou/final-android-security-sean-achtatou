package com.tencent.b.a;

import android.content.Context;
import com.tencent.b.b.a;
import com.tencent.b.b.b;
import com.tencent.b.c.g;
import com.tencent.b.d.k;
import java.util.ArrayList;
import java.util.Arrays;

public class h implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private Context f3367a = null;
    private a b = null;
    private int c = 0;

    public h(Context context, int i, a aVar) {
        this.f3367a = context;
        this.c = i;
        this.b = aVar;
    }

    private void a() {
        b a2 = g.a(this.f3367a).a(new ArrayList(Arrays.asList(1)));
        b a3 = g.a(this.f3367a).a(new ArrayList(Arrays.asList(2)));
        b a4 = g.a(this.f3367a).a(new ArrayList(Arrays.asList(4)));
        if (!k.b(a2, a3) || !k.b(a2, a4)) {
            b a5 = k.a(k.a(a2, a3), k.a(a2, a4));
            k.a("local mid check failed, redress with mid:" + a5.toString());
            g.a(this.f3367a).a(a5);
            return;
        }
        k.a("local mid check passed.");
    }

    private void b() {
        d.a(this.f3367a).a(new g(this.f3367a), new i(this));
    }

    private void c() {
        com.tencent.b.c.a b2 = g.a(this.f3367a).b();
        if (b2 == null) {
            k.a("CheckEntity is null");
            return;
        }
        int c2 = b2.c() + 1;
        long abs = Math.abs(System.currentTimeMillis() - b2.b());
        k.a("check entity: " + b2.toString() + ",duration:" + abs);
        if ((c2 <= b2.d() || abs <= a.f3361a) && abs <= ((long) b2.a()) * a.f3361a) {
            b2.b(c2);
            g.a(this.f3367a).a(b2);
            return;
        }
        a();
        b();
    }

    public void run() {
        k.a("request type:" + this.c);
        switch (this.c) {
            case 1:
                if (k.b(this.f3367a)) {
                    d.a(this.f3367a).a(new g(this.f3367a), this.b);
                    return;
                } else {
                    this.b.a(-10010, "network not available.");
                    return;
                }
            case 2:
                if (k.b(this.f3367a)) {
                    c();
                    return;
                }
                return;
            default:
                k.a("wrong type:" + this.c);
                return;
        }
    }
}
