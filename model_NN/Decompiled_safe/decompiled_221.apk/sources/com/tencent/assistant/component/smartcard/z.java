package com.tencent.assistant.component.smartcard;

import android.content.Context;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.link.b;
import com.tencent.assistantv2.model.SimpleEbookModel;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class z extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchSmartCardEbookRelateItem f1165a;
    private Context b;
    private int c;
    private SimpleEbookModel d;

    public z(SearchSmartCardEbookRelateItem searchSmartCardEbookRelateItem, Context context, int i, SimpleEbookModel simpleEbookModel) {
        this.f1165a = searchSmartCardEbookRelateItem;
        this.b = context;
        this.c = i;
        this.d = simpleEbookModel;
    }

    public void onTMAClick(View view) {
        if (this.d != null) {
            b.a(this.b, this.d.l);
        }
    }

    public STInfoV2 getStInfo(View view) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f1165a.getContext(), 200);
        buildSTInfo.slotId = a.a(Constants.VIA_REPORT_TYPE_SHARE_TO_QZONE, this.c);
        if (this.f1165a.m != null) {
            buildSTInfo.searchId = this.f1165a.m.c();
            buildSTInfo.extraData = this.f1165a.m.d() + ";" + this.d.f3310a;
        }
        buildSTInfo.status = "0" + (this.c + 2);
        return buildSTInfo;
    }
}
