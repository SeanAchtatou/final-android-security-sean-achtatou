package com.tencent.assistant.component;

import android.view.View;
import com.tencent.assistant.activity.SelfUpdateActivity;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.manager.SelfUpdateManager;
import com.tencent.assistant.st.STConstAction;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class dj extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SelfNormalUpdateView f1019a;

    dj(SelfNormalUpdateView selfNormalUpdateView) {
        this.f1019a = selfNormalUpdateView;
    }

    public void onTMAClick(View view) {
        this.f1019a.update();
        SelfUpdateManager.a().o().b(false);
    }

    public STInfoV2 getStInfo() {
        if (!(this.f1019a.getContext() instanceof SelfUpdateActivity)) {
            return null;
        }
        STInfoV2 i = ((SelfUpdateActivity) this.f1019a.getContext()).i();
        i.slotId = SecondNavigationTitleView.TMA_ST_NAVBAR_SEARCH_TAG;
        if (!SelfUpdateManager.a().a(this.f1019a.mUpdateInfo.e)) {
            i.actionId = 900;
            return i;
        }
        i.actionId = STConstAction.ACTION_HIT_INSTALL;
        return i;
    }
}
