package com.tencent.assistant.component;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.utils.df;

/* compiled from: ProGuard */
public class StrinptTipsView extends LinearLayout {
    public StrinptTipsView(Context context) {
        super(context);
        init();
    }

    public StrinptTipsView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    private void init() {
        setOrientation(0);
    }

    public void setSelect(int i, int i2) {
        removeAllViews();
        if (i2 <= i && i >= 2) {
            for (int i3 = 0; i3 < i; i3++) {
                ImageView imageView = new ImageView(getContext());
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) df.a(7.0f), (int) df.a(7.0f));
                if (i3 == i2) {
                    imageView.setBackgroundResource(R.drawable.banner_indicator_01);
                } else {
                    imageView.setBackgroundResource(R.drawable.banner_indicator_02);
                }
                layoutParams.leftMargin = (int) df.a(5.0f);
                layoutParams.rightMargin = (int) df.a(5.0f);
                addView(imageView, layoutParams);
            }
        }
    }
}
