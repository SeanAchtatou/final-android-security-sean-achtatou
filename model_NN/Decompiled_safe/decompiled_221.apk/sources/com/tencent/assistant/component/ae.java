package com.tencent.assistant.component;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.protocol.jce.CommentTagInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class ae extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f883a;
    final /* synthetic */ CommentTagInfo b;
    final /* synthetic */ CommentHeaderTagView c;

    ae(CommentHeaderTagView commentHeaderTagView, String str, CommentTagInfo commentTagInfo) {
        this.c = commentHeaderTagView;
        this.f883a = str;
        this.b = commentTagInfo;
    }

    public STInfoV2 getStInfo(View view) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.c.mContext, 200);
        buildSTInfo.scene = STConst.ST_PAGE_APP_DETAIL_COMMENT;
        String str = STConst.ST_DEFAULT_SLOT;
        if (view.getTag(R.id.tma_st_slot_tag) instanceof String) {
            str = this.f883a + ((String) view.getTag(R.id.tma_st_slot_tag));
        }
        buildSTInfo.slotId = str;
        return buildSTInfo;
    }

    public void onTMAClick(View view) {
        int childCount = this.c.taglistlayout.getChildCount();
        for (int i = 0; i < childCount; i++) {
            RelativeLayout relativeLayout = (RelativeLayout) this.c.taglistlayout.getChildAt(i);
            View findViewById = relativeLayout.findViewById(R.id.commenttag_selectimg_id);
            if (findViewById != null) {
                relativeLayout.removeView(findViewById);
            }
        }
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(7, view.getId());
        layoutParams.addRule(8, view.getId());
        ImageView imageView = new ImageView(this.c.mContext);
        imageView.setImageResource(R.drawable.pinglun_icon_on);
        imageView.setId(R.id.commenttag_selectimg_id);
        ((RelativeLayout) view.getParent()).addView(imageView, layoutParams);
        if (this.c.mClickListener != null) {
            this.c.mClickListener.a(view, this.b);
        }
    }
}
