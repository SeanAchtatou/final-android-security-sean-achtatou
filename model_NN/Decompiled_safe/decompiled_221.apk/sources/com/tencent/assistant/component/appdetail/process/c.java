package com.tencent.assistant.component.appdetail.process;

import android.os.Bundle;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.login.a.a;
import com.tencent.assistant.login.d;

/* compiled from: ProGuard */
class c extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f928a;

    c(b bVar) {
        this.f928a = bVar;
    }

    public void onLeftBtnClick() {
    }

    public void onRightBtnClick() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConst.KEY_LOGIN_TYPE, 5);
        bundle.putInt(AppConst.KEY_FROM_TYPE, 12);
        a.a(12);
        d.a().a(AppConst.IdentityType.MOBILEQ, bundle);
    }

    public void onCancell() {
    }
}
