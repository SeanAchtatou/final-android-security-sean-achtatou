package com.tencent.assistant.component.appdetail;

import android.view.GestureDetector;
import android.view.MotionEvent;

/* compiled from: ProGuard */
class p extends GestureDetector.SimpleOnGestureListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppdetailViewPager f925a;

    p(AppdetailViewPager appdetailViewPager) {
        this.f925a = appdetailViewPager;
    }

    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        return false;
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        if (this.f925a.h) {
            if (this.f925a.k != null) {
                this.f925a.k.fling(-((int) f));
            }
            boolean unused = this.f925a.h = false;
        }
        return false;
    }
}
