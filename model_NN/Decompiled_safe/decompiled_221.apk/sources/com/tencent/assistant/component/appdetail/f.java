package com.tencent.assistant.component.appdetail;

import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class f implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppdetailDownloadBar f915a;

    f(AppdetailDownloadBar appdetailDownloadBar) {
        this.f915a = appdetailDownloadBar;
    }

    public void run() {
        int measuredHeight = this.f915a.h.getMeasuredHeight() - this.f915a.H.getHeight();
        if (this.f915a.h.findViewById(R.id.my_tag_area).getVisibility() == 0) {
            measuredHeight = this.f915a.h.findViewById(R.id.my_tag_area).getTop();
        } else if (this.f915a.h.findViewById(R.id.recommend_view).getVisibility() == 0) {
            measuredHeight = this.f915a.h.findViewById(R.id.recommend_view).getTop();
        }
        if (measuredHeight < 0) {
            measuredHeight = 0;
        }
        this.f915a.H.startScrollToBottom(measuredHeight);
        this.f915a.b.smoothScrollHeader(true);
    }
}
