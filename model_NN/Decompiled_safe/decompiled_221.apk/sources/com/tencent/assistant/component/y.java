package com.tencent.assistant.component;

import android.widget.TextView;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class y implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TextView f1221a;
    final /* synthetic */ x b;

    y(x xVar, TextView textView) {
        this.b = xVar;
        this.f1221a = textView;
    }

    public void run() {
        if (this.b.f.praiseCount >= 3) {
            Toast.makeText(this.b.f.mContext, this.b.f.mContext.getString(R.string.comment_praise_tofast), 0).show();
            this.f1221a.setEnabled(false);
            int unused = this.b.f.praiseCount = 0;
            return;
        }
        this.f1221a.setEnabled(true);
    }
}
