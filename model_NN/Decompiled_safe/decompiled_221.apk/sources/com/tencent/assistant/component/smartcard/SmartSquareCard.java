package com.tencent.assistant.component.smartcard;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.model.a.t;
import com.tencent.assistant.model.a.u;
import com.tencent.assistantv2.st.b.d;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import java.util.List;

/* compiled from: ProGuard */
public class SmartSquareCard extends NormalSmartcardBaseItem {
    private TextView f;
    private TextView i;
    private SmartSquareNode j;

    public SmartSquareCard(Context context) {
        this(context, null);
    }

    public SmartSquareCard(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public SmartSquareCard(Context context, i iVar, SmartcardListener smartcardListener, IViewInvalidater iViewInvalidater) {
        super(context, iVar, smartcardListener, iViewInvalidater);
    }

    public void setDataSource(List<t> list, IViewInvalidater iViewInvalidater) {
        this.j.setDataSource(list, iViewInvalidater, new am(this));
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.b.inflate((int) R.layout.smartcard_square, this);
        this.f = (TextView) findViewById(R.id.title);
        this.j = (SmartSquareNode) findViewById(R.id.smart_square_node);
        this.i = (TextView) findViewById(R.id.more_txt);
        f();
    }

    /* access modifiers changed from: protected */
    public void b() {
        f();
    }

    private void f() {
        u uVar = null;
        if (this.smartcardModel instanceof u) {
            uVar = (u) this.smartcardModel;
        }
        if (uVar != null) {
            this.f.setText(uVar.k);
            if (uVar.b()) {
                this.f.setBackgroundResource(R.drawable.common_index_tag);
                this.f.setTextColor(getResources().getColor(R.color.smart_card_applist_title_font_color));
            } else {
                this.f.setBackgroundResource(0);
                this.f.setTextColor(getResources().getColor(R.color.apk_name_v5));
            }
            if (!TextUtils.isEmpty(uVar.n)) {
                if (TextUtils.isEmpty(uVar.o)) {
                    this.i.setText(uVar.o);
                } else {
                    this.i.setText(getResources().getString(R.string.more));
                }
                String str = uVar.n;
                this.i.setVisibility(0);
                this.i.setOnClickListener(new an(this, str));
            } else {
                this.i.setVisibility(8);
            }
            setDataSource(uVar.a(), this.d);
        }
    }

    /* access modifiers changed from: private */
    public STInfoV2 a(t tVar, int i2) {
        STInfoV2 a2 = a(a.a("03", i2), 100);
        if (!(a2 == null || tVar == null)) {
            a2.updateWithSimpleAppModel(tVar.f1653a);
        }
        if (this.e == null) {
            this.e = new d();
        }
        this.e.a(a2);
        return a2;
    }
}
