package com.tencent.assistant.component.txscrollview;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.adapter.TXTabViewPageAdapter;
import com.tencent.assistant.component.txscrollview.TXTabBarLayoutBase;
import com.tencent.assistant.utils.df;

/* compiled from: ProGuard */
public class TXTabViewPage extends RelativeLayout implements ViewPager.OnPageChangeListener, TXTabBarLayoutBase.ITXTabBarLayoutLinstener {

    /* renamed from: a  reason: collision with root package name */
    protected TXTabViewPageAdapter f1196a = null;
    private Context b;
    private TXTabBarLayout c = null;
    private TXViewPager d = null;
    private View e = null;
    private int f = 0;
    private int g = 0;
    private int h = 0;
    private ITXTabViewPageListener i;

    /* compiled from: ProGuard */
    public interface ITXTabViewPageListener {
        void onTxTabViewPageSelected(int i);

        void onTxTabViewPageWillSelect(int i);
    }

    public TXTabViewPage(Context context) {
        super(context);
        this.b = context;
        a();
    }

    public TXTabViewPage(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.b = context;
        a();
        setGravity(1);
    }

    public void setAdapter(TXTabViewPageAdapter tXTabViewPageAdapter) {
        this.f1196a = tXTabViewPageAdapter;
        this.c.setItemStringList(this.f1196a.getTitleList());
        this.d.setAdapter(this.f1196a);
        requestLayout();
    }

    public LinearLayout getTabItemView(int i2) {
        if (this.c != null) {
            return this.c.getTabItemView(i2);
        }
        return null;
    }

    public void setPageSelected(int i2) {
        if (this.c != null) {
            this.c.setTabItemSelected(i2);
        }
        if (this.d != null) {
            this.d.setCurrentItem(i2);
        }
    }

    private void a() {
        b();
        c();
    }

    private void b() {
        if (this.c != null && this.c.getParent() == this) {
            removeView(this.c);
            this.c = null;
        }
        this.c = new TXTabBarLayout(this.b);
        this.c.setLinstener(this);
        this.c.setId(100);
        this.c.setBackgroundColor(-1);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, df.a(this.b, 37.0f));
        layoutParams.addRule(10);
        this.c.setGravity(1);
        addView(this.c, layoutParams);
        this.e = new View(this.b);
        this.e.setId(TXTabBarLayout.TABITEM_TIPS_TEXT_ID);
        this.e.setBackgroundColor(this.b.getResources().getColor(R.color.appadmin_tab_bottom_line));
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, 1);
        layoutParams2.addRule(3, 100);
        addView(this.e, layoutParams2);
    }

    private void c() {
        if (this.d != null && this.d.getParent() == this) {
            this.d.removeAllViews();
            removeView(this.d);
            this.d = null;
        }
        this.d = new TXViewPager(this.b);
        this.d.setOnPageChangeListener(this);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams.addRule(3, TXTabBarLayout.TABITEM_TIPS_TEXT_ID);
        this.d.setGravity(1);
        addView(this.d, layoutParams);
    }

    public void onTxTabBarLayoutItemClick(int i2) {
        this.h = -1;
        this.d.setCurrentItem(i2);
        if (this.i != null) {
            this.i.onTxTabViewPageWillSelect(i2);
        }
    }

    public void onPageScrollStateChanged(int i2) {
        if (this.f1196a != null && i2 == 2 && this.g != 0 && this.h != -1) {
            int i3 = this.h + this.g;
            if (i3 < 0) {
                i3 = 0;
            }
            if (i3 >= this.f1196a.getCount()) {
                i3 = this.f1196a.getCount() - 1;
            }
            if (this.i != null) {
                this.i.onTxTabViewPageWillSelect(i3);
            }
            this.g = 0;
        }
    }

    public void onPageScrolled(int i2, float f2, int i3) {
        if (i3 > this.f) {
            this.g = 1;
        } else if (i3 < this.f) {
            this.g = -1;
        } else {
            this.g = 0;
        }
        this.f = i3;
        this.c.scrollCursor(i2, f2);
    }

    public void onPageSelected(int i2) {
        this.c.setTabItemSelected(i2);
        if (this.i != null) {
            this.i.onTxTabViewPageSelected(i2);
        }
        this.h = i2;
    }

    public void setListener(ITXTabViewPageListener iTXTabViewPageListener) {
        this.i = iTXTabViewPageListener;
    }
}
