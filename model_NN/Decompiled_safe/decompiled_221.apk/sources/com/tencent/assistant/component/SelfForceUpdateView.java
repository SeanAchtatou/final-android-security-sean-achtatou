package com.tencent.assistant.component;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Html;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.SelfNormalUpdateView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.manager.SelfUpdateManager;
import com.tencent.assistant.utils.FunctionUtils;
import com.tencent.assistant.utils.bt;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class SelfForceUpdateView extends RelativeLayout implements UIEventListener {
    private static final String TMA_ST_BTN_SLOT_CANCEL = "03_001";
    private static final String TMA_ST_BTN_SLOT_UPDATE = "03_002";
    public AstApp mApp;
    private TextView mDownloadFileSizeText;
    public DownloadInfo mDownloadInfo;
    private View mDownloadLayout;
    private TextView mDownloadProgressText;
    private Button mFinishBtn;
    private ProgressBar mProgressBar;
    private View mRootView;
    private TXImageView mSelfUpdateBanner;
    private Button mUpdateBtn;
    public SelfUpdateManager.SelfUpdateInfo mUpdateInfo;
    private SelfNormalUpdateView.UpdateListener mUpdateListener;
    private TextView mVersionFeatureText;
    private TextView mVersionTips1;
    private TextView mVersionTips2;

    public SelfForceUpdateView(Context context) {
        this(context, null);
    }

    public SelfForceUpdateView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    private void init() {
        this.mApp = AstApp.i();
        initLayout();
    }

    private void initLayout() {
        this.mRootView = LayoutInflater.from(getContext()).inflate((int) R.layout.dialog_selfupdate_force, this);
        this.mSelfUpdateBanner = (TXImageView) this.mRootView.findViewById(R.id.self_update_banner);
        this.mVersionFeatureText = (TextView) this.mRootView.findViewById(R.id.msg_versionfeature);
        this.mFinishBtn = (Button) this.mRootView.findViewById(R.id.btn_ignore);
        this.mUpdateBtn = (Button) this.mRootView.findViewById(R.id.btn_update);
        this.mVersionTips1 = (TextView) this.mRootView.findViewById(R.id.title_tip_1);
        this.mVersionTips2 = (TextView) this.mRootView.findViewById(R.id.title_tip_2);
        this.mDownloadLayout = this.mRootView.findViewById(R.id.downloadlayout);
        this.mDownloadProgressText = (TextView) this.mRootView.findViewById(R.id.update_download_txt_speed);
        this.mDownloadFileSizeText = (TextView) this.mRootView.findViewById(R.id.update_download_txt_per);
        this.mProgressBar = (ProgressBar) this.mRootView.findViewById(R.id.update_download_progressbar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public void initView() {
        this.mUpdateInfo = SelfUpdateManager.a().d();
        if (this.mUpdateInfo != null) {
            if (!TextUtils.isEmpty(this.mUpdateInfo.h)) {
                this.mVersionFeatureText.setText(String.format(getResources().getString(R.string.dialog_update_feature), this.mUpdateInfo.h));
            }
            updateUpdateView();
            if (!TextUtils.isEmpty(this.mUpdateInfo.q)) {
                this.mVersionTips2.setText(Html.fromHtml(this.mUpdateInfo.q));
            } else {
                this.mVersionTips2.setText(Constants.STR_EMPTY);
            }
            this.mSelfUpdateBanner.setImageBitmap(FunctionUtils.a(BitmapFactory.decodeResource(this.mApp.getResources(), R.drawable.self_update_banner)));
            this.mFinishBtn.setOnClickListener(new db(this));
            this.mUpdateBtn.setOnClickListener(new dc(this));
            if (m.a().a("update_force_downloading", false)) {
                this.mDownloadLayout.setVisibility(0);
                new Handler(Looper.getMainLooper()).postDelayed(new dd(this), 300);
                return;
            }
            this.mDownloadLayout.setVisibility(8);
            return;
        }
        this.mUpdateListener.dissmssDialog();
    }

    private void updateUpdateView() {
        if (SelfUpdateManager.a().a(this.mUpdateInfo.e)) {
            this.mVersionTips1.setVisibility(0);
            this.mVersionTips1.setText((int) R.string.selfupdate_apk_file_ready);
            this.mUpdateBtn.setText((int) R.string.selfupdate_update_right_now);
            this.mUpdateBtn.setTextColor(getResources().getColor(R.color.appdetail_btn_text_color_to_install));
            this.mUpdateBtn.setBackgroundResource(R.drawable.appdetail_bar_btn_to_install_selector_v5);
            return;
        }
        this.mVersionTips1.setVisibility(8);
        this.mUpdateBtn.setText(Constants.STR_EMPTY);
        this.mUpdateBtn.append(new SpannableString(getResources().getString(R.string.download)));
        SpannableString spannableString = new SpannableString(" (" + bt.b(this.mUpdateInfo.a()) + ")");
        spannableString.setSpan(new AbsoluteSizeSpan(15, true), 0, spannableString.length(), 17);
        this.mUpdateBtn.append(spannableString);
        this.mUpdateBtn.setBackgroundResource(R.drawable.selfupdate_btn_blue_selector_v5);
    }

    public void setUpdateListener(SelfNormalUpdateView.UpdateListener updateListener) {
        this.mUpdateListener = updateListener;
    }

    public void onResume() {
        registerUiEvent();
        updateUpdateView();
        this.mDownloadInfo = SelfUpdateManager.a().i();
        if (this.mDownloadInfo != null && !SelfUpdateManager.a().a(this.mUpdateInfo.e)) {
            this.mProgressBar.setProgress(SimpleDownloadInfo.getPercent(DownloadProxy.a().c(this.mDownloadInfo.downloadTicket)));
            this.mDownloadProgressText.setText(this.mDownloadInfo.response.c);
            this.mDownloadFileSizeText.setText(this.mDownloadInfo.response.f1263a + "/" + this.mDownloadInfo.response.b);
        } else if (this.mDownloadInfo != null && SelfUpdateManager.a().a(this.mUpdateInfo.e)) {
            this.mProgressBar.setProgress(100);
            this.mDownloadProgressText.setText(Constants.STR_EMPTY);
            this.mDownloadFileSizeText.setText(this.mDownloadInfo.response.f1263a + "/" + this.mDownloadInfo.response.b);
        }
    }

    public void onDestroy() {
        unRegisterUiEvent();
    }

    private void registerUiEvent() {
        this.mApp.k().addUIEventListener(1010, this);
        this.mApp.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING_START, this);
        this.mApp.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING, this);
        this.mApp.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING, this);
        this.mApp.k().addUIEventListener(1007, this);
        this.mApp.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE, this);
        this.mApp.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC, this);
    }

    private void unRegisterUiEvent() {
        this.mApp.k().removeUIEventListener(1010, this);
        this.mApp.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING_START, this);
        this.mApp.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING, this);
        this.mApp.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING, this);
        this.mApp.k().removeUIEventListener(1007, this);
        this.mApp.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE, this);
        this.mApp.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC, this);
    }

    /* access modifiers changed from: protected */
    public void update() {
        updateUI();
        startDownload();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, boolean]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    private void updateUI() {
        this.mDownloadLayout.setVisibility(0);
        m.a().b("update_force_downloading", (Object) true);
    }

    /* access modifiers changed from: private */
    public void startDownload() {
        SelfUpdateManager.a().a(SelfUpdateManager.SelfUpdateType.FORCE);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, boolean]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    public void handleUIEvent(Message message) {
        if (this.mUpdateInfo != null) {
            this.mDownloadInfo = SelfUpdateManager.a().i();
            if (this.mDownloadInfo != null) {
                this.mDownloadInfo = DownloadProxy.a().d(this.mDownloadInfo.downloadTicket);
            }
            if (this.mDownloadInfo != null && this.mDownloadInfo.packageName.equals(AstApp.i().getPackageName())) {
                switch (message.what) {
                    case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING /*1003*/:
                    case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING_START /*1004*/:
                        this.mProgressBar.setSecondaryProgress(0);
                        this.mProgressBar.setProgress(SimpleDownloadInfo.getPercent(DownloadProxy.a().c(this.mDownloadInfo.downloadTicket)));
                        String str = this.mDownloadInfo.response.c;
                        this.mDownloadProgressText.setText(String.format(getContext().getResources().getString(R.string.down_page_downloading), str));
                        this.mDownloadFileSizeText.setText(bt.a(this.mDownloadInfo.response.f1263a) + "/" + bt.a(this.mDownloadInfo.response.b));
                        this.mUpdateBtn.setEnabled(false);
                        this.mUpdateBtn.setTextColor(getResources().getColor(R.color.appadmin_disabled_text));
                        this.mUpdateBtn.setBackgroundResource(R.drawable.common_btn_big_disabled);
                        this.mUpdateBtn.setText((int) R.string.plugin_down_dialog_downloading);
                        return;
                    case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE /*1005*/:
                        this.mProgressBar.setSecondaryProgress(0);
                        if (this.mDownloadInfo != null) {
                            this.mProgressBar.setProgress(SimpleDownloadInfo.getPercent(DownloadProxy.a().c(this.mDownloadInfo.downloadTicket)));
                        }
                        this.mDownloadProgressText.setText(String.format(getContext().getResources().getString(R.string.down_page_pause), new Object[0]));
                        this.mDownloadFileSizeText.setText(bt.a(this.mDownloadInfo.response.f1263a) + "/" + bt.a(this.mDownloadInfo.response.b));
                        this.mUpdateBtn.setEnabled(true);
                        this.mUpdateBtn.setText((int) R.string.continuing);
                        this.mUpdateBtn.setBackgroundResource(R.drawable.selfupdate_btn_blue_selector_v5);
                        this.mUpdateBtn.setOnClickListener(new dg(this));
                        return;
                    case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC /*1006*/:
                        this.mProgressBar.setSecondaryProgress(0);
                        this.mDownloadLayout.setVisibility(8);
                        this.mUpdateBtn.setEnabled(true);
                        updateUpdateView();
                        this.mUpdateBtn.setOnClickListener(new dh(this));
                        return;
                    case 1007:
                        m.a().b("update_force_downloading", (Object) true);
                        this.mProgressBar.setProgress(0);
                        this.mProgressBar.setSecondaryProgress(100);
                        this.mUpdateBtn.setEnabled(true);
                        updateUpdateView();
                        this.mUpdateBtn.setOnClickListener(new df(this));
                        return;
                    case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING /*1008*/:
                    case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE /*1009*/:
                    default:
                        return;
                    case 1010:
                        this.mProgressBar.setSecondaryProgress(0);
                        this.mProgressBar.setProgress(100);
                        this.mDownloadLayout.setOnClickListener(new de(this));
                        if (!SelfUpdateManager.a().b()) {
                            SelfUpdateManager.a().c();
                            return;
                        }
                        return;
                }
            }
        }
    }
}
