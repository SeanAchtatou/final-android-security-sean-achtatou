package com.tencent.assistant.component.listview;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class g extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1087a;
    final /* synthetic */ e b;

    g(e eVar, int i) {
        this.b = eVar;
        this.f1087a = i;
    }

    public void onTMAClick(View view) {
        this.b.f1085a.adapter.a(this.f1087a, !this.b.f1085a.g.isSelected());
        this.b.f1085a.adapter.notifyDataSetChanged();
        if (this.b.f1085a.adapter.f759a != null) {
            this.b.f1085a.adapter.f759a.sendMessage(this.b.f1085a.adapter.f759a.obtainMessage(110005, new LocalApkInfo()));
        }
    }

    public STInfoV2 getStInfo() {
        return STInfoBuilder.buildSTInfo(this.b.f1085a.getContext(), 200);
    }
}
