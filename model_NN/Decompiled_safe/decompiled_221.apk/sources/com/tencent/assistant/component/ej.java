package com.tencent.assistant.component;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.localres.model.LocalApkInfo;

/* compiled from: ProGuard */
class ej extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UserAppListView f1045a;

    ej(UserAppListView userAppListView) {
        this.f1045a = userAppListView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.adapter.UserInstalledAppListAdapter.a(boolean, boolean):void
     arg types: [boolean, int]
     candidates:
      com.tencent.assistant.adapter.UserInstalledAppListAdapter.a(int, int):com.tencent.assistantv2.st.page.STInfoV2
      com.tencent.assistant.adapter.UserInstalledAppListAdapter.a(android.widget.TextView, long):void
      com.tencent.assistant.adapter.UserInstalledAppListAdapter.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, int):void
      com.tencent.assistant.adapter.UserInstalledAppListAdapter.a(boolean, boolean):void */
    public void onTMAClick(View view) {
        if (this.f1045a.mFooterView.getFooterViewEnable() && this.f1045a.mUninstallAppList.size() > 0) {
            this.f1045a.mUninstallFailSet.clear();
            this.f1045a.startUninstallApp((LocalApkInfo) this.f1045a.mUninstallAppList.get(0));
            boolean unused = this.f1045a.mUninstalling = true;
            boolean unused2 = this.f1045a.mIsUserStartUninstall = true;
            this.f1045a.mAdapter.a(this.f1045a.mUninstalling, true);
            this.f1045a.updateFootView();
            this.f1045a.reportBatchUninstall(this.f1045a.mUninstallAppList.size());
        }
    }
}
