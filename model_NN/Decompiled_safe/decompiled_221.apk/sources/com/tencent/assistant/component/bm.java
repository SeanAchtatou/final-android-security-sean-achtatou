package com.tencent.assistant.component;

import com.tencent.assistant.activity.PhotoBackupNewActivity;
import com.tencent.assistant.adapter.wifitransfer.WifiTransferMediaAdapter;
import com.tencent.assistant.adapter.wifitransfer.j;
import com.tencent.assistant.component.txscrollview.TXGridView;

/* compiled from: ProGuard */
class bm implements j {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PhotoBackupMiddleGridView f968a;

    bm(PhotoBackupMiddleGridView photoBackupMiddleGridView) {
        this.f968a = photoBackupMiddleGridView;
    }

    public void a(WifiTransferMediaAdapter<?> wifiTransferMediaAdapter, int i) {
        TXGridView access$000 = this.f968a.mImageGridView;
        if (!(this.f968a.activity instanceof PhotoBackupNewActivity)) {
            return;
        }
        if (i <= 0) {
            access$000.setTipsView(this.f968a.createNoDataTipsView());
        } else if (!this.f968a.isFirstSet) {
            this.f968a.isFirstSet = true;
            access$000.setTipsView(null);
        }
    }

    public void a() {
        this.f968a.updateCheckBox();
    }
}
