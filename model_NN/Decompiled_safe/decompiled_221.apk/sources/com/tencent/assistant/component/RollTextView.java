package com.tencent.assistant.component;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.TextView;
import com.tencent.assistant.plugin.PluginProxyUtils;
import java.math.BigDecimal;
import java.text.DecimalFormat;

/* compiled from: ProGuard */
public class RollTextView extends TextView {
    public static final int COLOR_BLACK = 2;
    public static final int COLOR_BLUE = 1;
    private static final int MSG_ROLL = 100000;
    private DecimalFormat df0 = new DecimalFormat("0");
    private DecimalFormat df1 = new DecimalFormat("0.0");
    private int mColorType = 1;
    private Context mContext;
    private DecimalFormat mCurrentFormat = null;
    /* access modifiers changed from: private */
    public double mCurrentValue;
    /* access modifiers changed from: private */
    public double mDestValue;
    /* access modifiers changed from: private */
    public Handler mHandler = new cv(this);
    /* access modifiers changed from: private */
    public double mRate;
    private Typeface mTextFont;
    private float scareValue = 1.0f;
    private TextView unitView = null;

    static /* synthetic */ double access$118(RollTextView rollTextView, double d) {
        double d2 = rollTextView.mCurrentValue + d;
        rollTextView.mCurrentValue = d2;
        return d2;
    }

    public void setScareValue(float f) {
        this.scareValue = f;
        setText(valueToString(getSizeValue(this.mCurrentValue)));
        setUnit(this.mDestValue);
    }

    public void setColorType(int i) {
        this.mColorType = i;
        if (this.mColorType == 1) {
            setTextColor(Color.argb(255, 0, 163, 224));
        } else {
            setTextColor(Color.argb(255, 52, 52, 52));
        }
    }

    public int getColorType() {
        return this.mColorType;
    }

    public long getDestValue() {
        return (long) this.mDestValue;
    }

    public RollTextView(Context context) {
        super(context);
        this.mContext = context;
        this.mCurrentValue = 0.0d;
        this.mTextFont = PluginProxyUtils.getTypeFace();
        setTypeface(this.mTextFont);
        setTextSize(1, 32.0f);
    }

    public RollTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.mContext = context;
        this.mCurrentValue = 0.0d;
        this.mTextFont = PluginProxyUtils.getTypeFace();
        setTextSize(32.0f);
        setTypeface(this.mTextFont);
    }

    /* access modifiers changed from: private */
    public String valueToString(double d) {
        String.valueOf(d);
        if (this.mCurrentFormat != null) {
            return this.mCurrentFormat.format(d);
        }
        if (d < 100.0d) {
            return this.df1.format(d);
        }
        return this.df0.format(d);
    }

    public void setCurrentFormat(DecimalFormat decimalFormat) {
        this.mCurrentFormat = decimalFormat;
    }

    public void setValue(double d) {
        this.mHandler.removeMessages(MSG_ROLL);
        if (d == 0.0d || !this.df1.format(d).equals(this.df1.format(this.mCurrentValue))) {
            this.mDestValue = d;
            this.mRate = (this.mDestValue - this.mCurrentValue) / 20.0d;
            this.mRate = new BigDecimal(this.mRate).setScale(2, 4).doubleValue();
            this.mHandler.sendEmptyMessage(MSG_ROLL);
        }
    }

    public void setValueDirect(double d) {
        this.mHandler.removeMessages(MSG_ROLL);
        if (d < 0.0d) {
            d = 0.0d;
        }
        this.mDestValue = d;
        this.mCurrentValue = d;
        this.mHandler.sendEmptyMessage(MSG_ROLL);
    }

    public void setUnitView(TextView textView) {
        this.unitView = textView;
        this.unitView.setTypeface(this.mTextFont);
        this.unitView.setTextSize(20.0f);
    }

    /* access modifiers changed from: private */
    public double getSizeValue(double d) {
        if (d < 0.0d) {
            d = 0.0d;
        }
        while (d >= 1000.0d) {
            d /= 1024.0d;
        }
        return d;
    }

    /* access modifiers changed from: private */
    public void setUnit(double d) {
        String str;
        if (this.unitView != null) {
            if (d < 1000.0d) {
                str = "KB";
            } else {
                double d2 = d / 1024.0d;
                if (d2 < 1000.0d) {
                    str = "MB";
                } else {
                    double d3 = d2 / 1024.0d;
                    str = "GB";
                }
            }
            this.unitView.setText(str);
            if (this.mColorType == 1) {
                this.unitView.setTextColor(Color.argb(255, 0, 163, 224));
            } else {
                this.unitView.setTextColor(Color.argb(255, 52, 52, 52));
            }
        }
    }
}
