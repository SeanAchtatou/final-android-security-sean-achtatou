package com.tencent.assistant.component;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
public class TabView extends RelativeLayout {
    private ImageView mIcon;
    private TextView mTitle;
    private Button mToastBtn;

    public TabView(Context context) {
        super(context);
        init(context);
    }

    public TabView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(context);
    }

    public TabView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.tencent.assistant.component.TabView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void init(Context context) {
        ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.assistant_tab_view, (ViewGroup) this, true);
        setClickable(true);
        this.mTitle = (TextView) findViewById(R.id.title);
        this.mIcon = (ImageView) findViewById(R.id.icon);
        this.mToastBtn = (Button) findViewById(R.id.toast_number);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return true;
    }

    public void setTitle(int i) {
        if (i != 0) {
            this.mTitle.setText(i);
        }
    }

    public void setTitle(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.mTitle.setText(str);
        }
    }

    public void setTitleTextCollor(int i) {
        this.mTitle.setTextColor(getContext().getResources().getColor(i));
    }

    public void setIcon(int i) {
        this.mIcon.setImageResource(i);
    }

    public void setToastNumber(int i) {
        if (i > 0) {
            this.mToastBtn.setVisibility(0);
            if (i > 99) {
                this.mToastBtn.setText(String.valueOf("99+"));
            } else {
                this.mToastBtn.setText(String.valueOf(i));
            }
        } else {
            this.mToastBtn.setVisibility(8);
        }
    }
}
