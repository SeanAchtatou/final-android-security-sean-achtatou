package com.tencent.assistant.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
public class FAQFootView extends LinearLayout {
    private Context context;
    private TextView titleView;

    public FAQFootView(Context context2) {
        super(context2);
        this.context = context2;
        initView();
    }

    public FAQFootView(Context context2, AttributeSet attributeSet) {
        super(context2, attributeSet);
        this.context = context2;
        initView();
    }

    private void initView() {
        this.titleView = (TextView) LayoutInflater.from(this.context).inflate((int) R.layout.footer_layout, this).findViewById(R.id.text);
        this.titleView.setText(this.context.getString(R.string.help_feedback));
    }

    public void updateFeedbackViewState(int i) {
        if (i > 0) {
            this.titleView.setText(String.format(this.context.getString(R.string.help_feedback_has_reply), Integer.valueOf(i)));
            return;
        }
        this.titleView.setText(this.context.getString(R.string.help_feedback));
    }
}
