package com.tencent.assistant.component.appdetail.process;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.u;
import com.tencent.assistant.utils.e;

/* compiled from: ProGuard */
public class s {
    public static boolean a(SimpleAppModel simpleAppModel) {
        return b(simpleAppModel) || c(simpleAppModel);
    }

    public static boolean a(SimpleAppModel simpleAppModel, AppConst.AppState appState) {
        return b(simpleAppModel, appState) || c(simpleAppModel, appState);
    }

    public static boolean b(SimpleAppModel simpleAppModel) {
        return b(simpleAppModel, null);
    }

    public static boolean b(SimpleAppModel simpleAppModel, AppConst.AppState appState) {
        if (simpleAppModel == null) {
            return false;
        }
        if (appState == null) {
            appState = u.d(simpleAppModel);
        }
        if ((appState == AppConst.AppState.INSTALLED || appState == AppConst.AppState.DOWNLOAD || appState == AppConst.AppState.UPDATE) && simpleAppModel.d() && (simpleAppModel.P & 3) > 0 && !e.a(simpleAppModel.c, simpleAppModel.g, simpleAppModel.ad)) {
            return true;
        }
        return false;
    }

    public static boolean c(SimpleAppModel simpleAppModel) {
        return c(simpleAppModel, null);
    }

    public static boolean c(SimpleAppModel simpleAppModel, AppConst.AppState appState) {
        if (simpleAppModel == null) {
            return false;
        }
        if (appState == null) {
            appState = u.d(simpleAppModel);
        }
        if ((appState == AppConst.AppState.INSTALLED || appState == AppConst.AppState.DOWNLOAD || appState == AppConst.AppState.UPDATE) && simpleAppModel.e() && (simpleAppModel.P & 3) > 0 && !e.a(simpleAppModel.c, simpleAppModel.g, simpleAppModel.ad)) {
            return true;
        }
        return false;
    }

    public static boolean a(byte b, byte b2) {
        return ((b >> b2) & 1) > 0;
    }
}
