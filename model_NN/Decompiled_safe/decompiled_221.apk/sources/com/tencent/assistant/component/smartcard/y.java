package com.tencent.assistant.component.smartcard;

import android.os.Bundle;
import android.view.View;
import com.tencent.assistant.link.b;
import com.tencent.assistant.plugin.PluginActivity;

/* compiled from: ProGuard */
class y implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchSmartCardBaseItem f1164a;

    y(SearchSmartCardBaseItem searchSmartCardBaseItem) {
        this.f1164a = searchSmartCardBaseItem;
    }

    public void onClick(View view) {
        Bundle bundle = new Bundle();
        bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.f1164a.a(0));
        b.b(this.f1164a.f1115a, this.f1164a.smartcardModel.n, bundle);
        this.f1164a.a("03_001", 200, this.f1164a.smartcardModel.r, -1);
    }
}
