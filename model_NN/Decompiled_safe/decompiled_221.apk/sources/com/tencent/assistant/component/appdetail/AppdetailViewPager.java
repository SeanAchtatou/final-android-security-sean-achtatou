package com.tencent.assistant.component.appdetail;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.appdetail.HorizonScrollPicViewer;

/* compiled from: ProGuard */
public class AppdetailViewPager extends ViewPager implements IAppdetailView {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public boolean f900a = false;
    private boolean b = false;
    private int c = 10;
    private float d;
    private GestureDetector e;
    private IInnerScrollListener f = new n(this);
    private float g;
    /* access modifiers changed from: private */
    public boolean h = false;
    private int i = -1;
    private HorizonScrollPicViewer.IPicViewerListener j = new o(this);
    /* access modifiers changed from: private */
    public IHorizonScrollPicViewer k;

    /* compiled from: ProGuard */
    public interface IHorizonScrollPicViewer {
        boolean canScrollToLeft();

        boolean canScrollToRight();

        void fling(int i);

        void scrollTo(int i);
    }

    public AppdetailViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public AppdetailViewPager(Context context) {
        super(context);
        a(context);
    }

    private void a(Context context) {
        setBackgroundResource(R.color.app_detail_bg);
        this.e = new GestureDetector(new p(this));
        this.c = ViewConfiguration.get(context).getScaledTouchSlop();
    }

    public IInnerScrollListener getInnerScrollView() {
        return this.f;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.g = motionEvent.getX();
                this.d = 0.0f;
                this.i = -1;
                this.f900a = false;
                break;
            case 2:
                float x = motionEvent.getX();
                this.d += Math.abs(x - this.g);
                this.g = x;
                if (this.d > ((float) this.c)) {
                    return true;
                }
                break;
        }
        return super.onInterceptTouchEvent(motionEvent);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z = false;
        boolean z2 = true;
        this.e.onTouchEvent(motionEvent);
        switch (motionEvent.getAction()) {
            case 0:
                this.g = motionEvent.getX();
                this.i = -1;
                break;
            case 1:
            default:
                if (this.f900a) {
                    if (this.f900a) {
                        z2 = false;
                    }
                    this.f900a = z2;
                    break;
                }
                break;
            case 2:
                float x = motionEvent.getX();
                int i2 = (int) (x - this.g);
                this.g = x;
                if (i2 >= 0 && this.i != 2 && this.f900a && this.k != null) {
                    this.k.scrollTo(-i2);
                    this.h = true;
                    if (-1 != this.i) {
                        return true;
                    }
                    this.i = 1;
                    return true;
                } else if (i2 >= 0 || this.i == 2 || !this.f900a || this.k == null || !this.k.canScrollToRight()) {
                    a(i2);
                    this.i = 2;
                    this.h = false;
                    break;
                } else {
                    this.k.scrollTo(-i2);
                    this.h = true;
                    if (-1 != this.i) {
                        return true;
                    }
                    this.i = 1;
                    return true;
                }
                break;
        }
        try {
            z = super.onTouchEvent(motionEvent);
        } catch (Exception e2) {
        }
        return z;
    }

    private void a(int i2) {
        scrollTo(getScrollX() - i2, getScrollY());
    }

    public HorizonScrollPicViewer.IPicViewerListener getPicViewerListener() {
        return this.j;
    }

    public void setPicViewer(IHorizonScrollPicViewer iHorizonScrollPicViewer) {
        this.k = iHorizonScrollPicViewer;
    }
}
