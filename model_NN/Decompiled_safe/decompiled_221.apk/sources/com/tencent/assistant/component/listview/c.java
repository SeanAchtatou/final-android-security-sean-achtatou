package com.tencent.assistant.component.listview;

import android.view.View;
import android.view.animation.Animation;
import com.tencent.assistant.component.listview.AnimationExpandableListView;

/* compiled from: ProGuard */
class c implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ View f1083a;
    final /* synthetic */ AnimationExpandableListView.ItemAnimatorLisenter b;
    final /* synthetic */ AnimationExpandableListView c;

    c(AnimationExpandableListView animationExpandableListView, View view, AnimationExpandableListView.ItemAnimatorLisenter itemAnimatorLisenter) {
        this.c = animationExpandableListView;
        this.f1083a = view;
        this.b = itemAnimatorLisenter;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        this.f1083a.clearAnimation();
        this.f1083a.setVisibility(0);
        this.b.onAnimatorEnd();
    }
}
