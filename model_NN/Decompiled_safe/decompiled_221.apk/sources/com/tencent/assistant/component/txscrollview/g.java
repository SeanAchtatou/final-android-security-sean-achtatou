package com.tencent.assistant.component.txscrollview;

import android.graphics.Bitmap;
import android.text.TextUtils;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.thumbnailCache.k;
import com.tencent.assistant.utils.ba;
import com.tencent.connect.common.Constants;
import java.util.HashMap;

/* compiled from: ProGuard */
class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TXImageView f1203a;

    g(TXImageView tXImageView) {
        this.f1203a = tXImageView;
    }

    public void run() {
        String str;
        if (TextUtils.isEmpty(this.f1203a.mImageUrlString)) {
            str = Constants.STR_EMPTY;
        } else {
            str = this.f1203a.mImageUrlString;
        }
        Bitmap a2 = k.b().a(str, this.f1203a.mImageType.getThumbnailRequestType(), this.f1203a.self);
        if (this.f1203a.invalidater == null) {
            ba.a().post(new h(this, str, a2));
        } else if (!TextUtils.isEmpty(str) && str.equals(this.f1203a.mImageUrlString)) {
            if (a2 != null && !a2.isRecycled()) {
                Bitmap unused = this.f1203a.mBitmap = a2;
            }
            ViewInvalidateMessage viewInvalidateMessage = new ViewInvalidateMessage(0);
            HashMap hashMap = new HashMap();
            hashMap.put("URL", str);
            hashMap.put("TYPE", Integer.valueOf(this.f1203a.mImageType.getThumbnailRequestType()));
            viewInvalidateMessage.params = hashMap;
            viewInvalidateMessage.target = this.f1203a.invalidateHandler;
            this.f1203a.invalidater.dispatchMessage(viewInvalidateMessage);
        }
    }
}
