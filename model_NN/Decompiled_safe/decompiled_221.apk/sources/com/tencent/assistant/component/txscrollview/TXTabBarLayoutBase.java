package com.tencent.assistant.component.txscrollview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.df;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
public class TXTabBarLayoutBase extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    protected ArrayList<View> f1195a;
    protected LinearLayout b;
    protected View c;
    protected Context d;
    protected int e = 0;
    protected double f = 0.0d;
    protected double g = 0.0d;
    protected double h = 0.0d;
    protected RelativeLayout.LayoutParams i;
    protected ITXTabBarLayoutLinstener j = null;
    private View.OnClickListener k = null;

    /* compiled from: ProGuard */
    public interface ITXTabBarLayoutLinstener {
        void onTxTabBarLayoutItemClick(int i);
    }

    public TXTabBarLayoutBase(Context context) {
        super(context);
        this.d = context;
        this.f1195a = new ArrayList<>();
    }

    public TXTabBarLayoutBase(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.d = context;
        this.f1195a = new ArrayList<>();
    }

    public void setTabItemList(ArrayList<View> arrayList) {
        if (arrayList != null && arrayList.size() > 0) {
            Iterator<View> it = this.f1195a.iterator();
            while (it.hasNext()) {
                View next = it.next();
                if (next.getParent() == this.b && this.b != null) {
                    this.b.removeView(next);
                }
            }
            if (this.b != null && this.b.getParent() == this) {
                removeView(this.b);
                this.b = null;
            }
            this.f1195a.clear();
            this.f1195a.addAll(arrayList);
            this.k = new t(this);
            b();
            requestLayout();
        }
    }

    public void setTabItemSelected(int i2) {
        if (i2 >= 0 && i2 < this.f1195a.size()) {
            c(i2);
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (i2 != i4 || i3 != i5) {
            b();
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        c();
        a();
    }

    private void c() {
        int i2 = 0;
        if (this.b != null) {
            Iterator<View> it = this.f1195a.iterator();
            while (it.hasNext()) {
                View next = it.next();
                if (next.getParent() == this.b && this.b != null) {
                    this.b.removeView(next);
                }
            }
            if (this.b.getParent() == this) {
                removeView(this.b);
                this.b = null;
            }
        }
        this.b = new LinearLayout(this.d);
        this.b.setId(100);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams.addRule(15);
        this.b.setOrientation(0);
        this.b.setGravity(17);
        addView(this.b, layoutParams);
        int size = this.f1195a.size();
        if (size > 0) {
            this.h = (double) df.a(this.d, 0.0f);
            this.f = ((((double) getWidth()) * 1.0d) - (this.h * ((double) (size - 1)))) / ((double) size);
            Iterator<View> it2 = this.f1195a.iterator();
            while (it2.hasNext()) {
                View next2 = it2.next();
                LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams((int) this.f, -1);
                next2.setOnClickListener(this.k);
                next2.setTag(Integer.valueOf(i2));
                this.b.addView(next2, layoutParams2);
                int i3 = i2 + 1;
                if (i3 < this.f1195a.size()) {
                    LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams((int) this.h, -1);
                    ImageView imageView = new ImageView(this.d);
                    imageView.setImageResource(R.drawable.cut_line);
                    this.b.addView(imageView, layoutParams3);
                }
                i2 = i3;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
    }

    /* access modifiers changed from: private */
    public void c(int i2) {
        if (i2 != this.e) {
            int b2 = b(i2);
            this.e = i2;
            layoutCursorView(b2);
            if (this.j != null) {
                this.j.onTxTabBarLayoutItemClick(this.e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(int i2) {
        layoutCursorView(b(i2));
    }

    /* access modifiers changed from: protected */
    public int b(int i2) {
        return (int) ((this.f * ((double) i2)) + ((this.f - this.g) / 2.0d));
    }

    public void setLinstener(ITXTabBarLayoutLinstener iTXTabBarLayoutLinstener) {
        this.j = iTXTabBarLayoutLinstener;
    }

    public void layoutCursorView(int i2) {
        this.i = (RelativeLayout.LayoutParams) this.c.getLayoutParams();
        this.i.leftMargin = i2;
        this.c.layout(this.i.leftMargin, this.c.getTop(), this.c.getWidth() + this.i.leftMargin, this.c.getBottom());
        XLog.i("TXTabBarLayout", "leftMargin: " + this.i.leftMargin);
    }
}
