package com.tencent.assistant.component.smartcard;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.a.k;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class s extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ k f1158a;
    final /* synthetic */ NormalSmartcardPersonalizedItem b;

    s(NormalSmartcardPersonalizedItem normalSmartcardPersonalizedItem, k kVar) {
        this.b = normalSmartcardPersonalizedItem;
        this.f1158a = kVar;
    }

    public void onTMAClick(View view) {
        NormalSmartcardPersonalizedItem.a(this.b, this.f1158a.d);
        int unused = this.b.n = this.b.n % this.f1158a.c.size();
        this.b.b();
    }

    public STInfoV2 getStInfo() {
        return this.b.a("05_001", 200);
    }
}
