package com.tencent.assistant.component;

import android.database.DataSetObserver;

/* compiled from: ProGuard */
class as extends DataSetObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ HorizontalListView f948a;

    as(HorizontalListView horizontalListView) {
        this.f948a = horizontalListView;
    }

    public void onChanged() {
        synchronized (this.f948a) {
            boolean unused = this.f948a.mDataChanged = true;
        }
        int unused2 = this.f948a.mEstimateMaxX = 0;
        int unused3 = this.f948a.mChildWidth = 0;
        this.f948a.invalidate();
        this.f948a.requestLayout();
    }

    public void onInvalidated() {
        this.f948a.reset();
        this.f948a.invalidate();
        this.f948a.requestLayout();
    }
}
