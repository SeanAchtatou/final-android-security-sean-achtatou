package com.tencent.assistant.component.treasurebox;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.Html;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.model.s;
import com.tencent.assistant.protocol.jce.AutoDownloadInfo;
import com.tencent.assistant.thumbnailCache.k;
import com.tencent.assistant.thumbnailCache.o;
import com.tencent.assistant.thumbnailCache.p;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.e;

/* compiled from: ProGuard */
public class AppTreasureBoxCell1 extends AppTreasureBoxCell implements p {
    private TextView d = null;
    private TextView e = null;
    private ImageView f = null;
    private TextView g = null;
    private LinearLayout h = null;
    private Button i = null;
    private ImageView j;
    private AppTreasureBoxCellToucher k = null;
    private TranslateAnimation l = null;
    /* access modifiers changed from: private */
    public boolean m = false;
    private boolean n;
    private String o;

    public AppTreasureBoxCell1(Context context) {
        super(context);
        a(context);
    }

    public AppTreasureBoxCell1(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    private void a(Context context) {
        LayoutInflater.from(context).inflate((int) R.layout.app_treasure_box_item_normal, this);
        this.h = (LinearLayout) findViewById(R.id.above_item);
        this.d = (TextView) findViewById(R.id.title_below);
        this.e = (TextView) findViewById(R.id.desc_below);
        this.i = (Button) findViewById(R.id.btn_below);
        this.f = (ImageView) findViewById(R.id.icon_below);
        this.g = (TextView) findViewById(R.id.title_above);
        this.j = (ImageView) findViewById(R.id.divider);
        this.k = new AppTreasureBoxCellToucher(this);
        this.h.setOnTouchListener(this.k);
        setOnClickListener(this);
        this.i.setOnClickListener(this);
    }

    public void onLeaveScreen() {
        this.f1175a = false;
        endDemoAnimation();
    }

    public void onGoinScreen() {
        this.f1175a = true;
        if (this.b == 0 && !this.n) {
            startDemoAnimation();
        }
    }

    public LinearLayout getAboveArea() {
        return this.h;
    }

    public void onClick(View view) {
        if (view == this.i) {
            if (this.c != null) {
                this.c.onActionBtnClick(this.b, 1);
            }
        } else if (view == this && this.c != null) {
            this.c.onActionBtnClick(this.b, 0);
        }
    }

    public void setBottomDividerVisibility(boolean z) {
        this.j.setVisibility(z ? 0 : 8);
    }

    public void refreshData(s sVar) {
        int i2;
        if (sVar != null && sVar.f1672a != null) {
            AutoDownloadInfo autoDownloadInfo = sVar.f1672a;
            if (!TextUtils.isEmpty(autoDownloadInfo.b)) {
                this.d.setText(autoDownloadInfo.b);
            }
            if (!TextUtils.isEmpty(autoDownloadInfo.r)) {
                this.e.setText(autoDownloadInfo.r);
            } else if (!TextUtils.isEmpty(autoDownloadInfo.p)) {
                this.e.setText(autoDownloadInfo.p);
            }
            if (sVar.c) {
                this.i.setText((int) R.string.app_treasure_box_btn_root_installing_txt);
            } else if (e.a(autoDownloadInfo.f2010a, autoDownloadInfo.d)) {
                this.i.setText((int) R.string.app_treasure_box_btn_open_txt);
                this.i.setBackgroundResource(R.drawable.btn_gray_selector);
                this.i.setTextColor(getResources().getColor(R.color.blue_color_down));
            } else {
                this.i.setText((int) R.string.app_treasure_box_btn_install_txt);
                this.i.setBackgroundResource(R.drawable.app_treasure_box_btn_install_selector);
                this.i.setTextColor(getResources().getColor(R.color.treasure_box_normal_btn_below));
            }
            if (!TextUtils.isEmpty(autoDownloadInfo.s)) {
                this.g.setText(Html.fromHtml(autoDownloadInfo.s));
            }
            if (autoDownloadInfo.n != null && !TextUtils.isEmpty(autoDownloadInfo.n.f2252a)) {
                this.o = autoDownloadInfo.n.f2252a;
                a();
            }
            this.n = sVar.b;
            LinearLayout linearLayout = this.h;
            if (!sVar.b) {
                i2 = 0;
            } else {
                i2 = 8;
            }
            linearLayout.setVisibility(i2);
        }
    }

    public void startDemoAnimation() {
        this.m = true;
        ba.a().postDelayed(new a(this), 500);
    }

    public void beginDemoAnimation() {
        if (this.m && this.h != null) {
            this.h.clearAnimation();
            this.l = new TranslateAnimation(0.0f, (float) ((int) TypedValue.applyDimension(1, 50.0f, getResources().getDisplayMetrics())), 0.0f, 0.0f);
            this.l.setInterpolator(AnimationUtils.loadInterpolator(getContext(), 17432583));
            this.l.setDuration(1300);
            this.l.setRepeatMode(2);
            this.l.setRepeatCount(1);
            this.l.setFillAfter(false);
            this.l.setAnimationListener(new b(this));
            this.h.startAnimation(this.l);
        }
    }

    public void endDemoAnimation() {
        this.m = false;
        if (this.h != null) {
            this.h.clearAnimation();
        }
        this.l = null;
    }

    private void a() {
        Bitmap a2 = k.b().a(this.o, 1, this);
        if (a2 != null && !a2.isRecycled()) {
            this.f.setImageBitmap(a2);
        }
    }

    public void thumbnailRequestStarted(o oVar) {
    }

    public void thumbnailRequestCompleted(o oVar) {
        if (oVar != null) {
            String c = oVar.c();
            if (!TextUtils.isEmpty(c) && c.endsWith(this.o) && oVar.f != null && !oVar.f.isRecycled()) {
                this.f.setImageBitmap(oVar.f);
            }
        }
    }

    public void thumbnailRequestCancelled(o oVar) {
    }

    public void thumbnailRequestFailed(o oVar) {
    }

    public LinearLayout getAboveAreaView() {
        return this.h;
    }
}
