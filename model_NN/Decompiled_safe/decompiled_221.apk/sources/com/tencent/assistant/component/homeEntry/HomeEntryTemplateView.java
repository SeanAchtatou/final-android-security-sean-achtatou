package com.tencent.assistant.component.homeEntry;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.protocol.jce.EntranceTemplate;
import com.tencent.assistant.thumbnailCache.k;
import com.tencent.assistant.thumbnailCache.o;
import com.tencent.assistant.thumbnailCache.p;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ba;

/* compiled from: ProGuard */
public class HomeEntryTemplateView extends LinearLayout implements p {

    /* renamed from: a  reason: collision with root package name */
    LinearLayout f1053a = null;
    /* access modifiers changed from: private */
    public Context b;
    public long mHomeEntryDataRevision;
    public EntranceTemplate mSourceData;

    public HomeEntryTemplateView(Context context, EntranceTemplate entranceTemplate, long j) {
        super(context);
        this.b = context;
        this.mHomeEntryDataRevision = j;
        this.mSourceData = entranceTemplate;
        init(context);
    }

    public void init(Context context) {
        this.f1053a = (LinearLayout) LayoutInflater.from(this.b).inflate((int) R.layout.home_entry_template_view, (ViewGroup) null);
        addView(this.f1053a);
        setGravity(17);
        setBackgroundResource(R.drawable.bg_card_selector_padding);
        requestImg();
        this.f1053a.setOnClickListener(new d(this));
        ba.a().postDelayed(new e(this), 2000);
    }

    public void addMyChildView(View view) {
        this.f1053a.addView(view);
    }

    public void requestImg() {
        if (this.mSourceData == null || TextUtils.isEmpty(this.mSourceData.b)) {
            XLog.v("home_entry", "template---requestImg--template name:" + getClass().getSimpleName() + "--no image data");
            return;
        }
        Bitmap a2 = k.b().a(this.mSourceData.b, 1, this);
        if (a2 != null && !a2.isRecycled()) {
            XLog.v("home_entry", "template---requestImg--template name:" + getClass().getSimpleName() + "--bitmap get");
            ba.a().postDelayed(new g(this, a2), 15);
        }
    }

    public void thumbnailRequestStarted(o oVar) {
    }

    public void thumbnailRequestCompleted(o oVar) {
        if (oVar != null) {
            String c = oVar.c();
            if (!TextUtils.isEmpty(c) && c.endsWith(this.mSourceData.b) && oVar.f != null && !oVar.f.isRecycled()) {
                setBackgroundResource(0);
                this.f1053a.setBackgroundDrawable(new BitmapDrawable(oVar.f));
            }
        }
    }

    public void thumbnailRequestCancelled(o oVar) {
    }

    public void thumbnailRequestFailed(o oVar) {
    }
}
