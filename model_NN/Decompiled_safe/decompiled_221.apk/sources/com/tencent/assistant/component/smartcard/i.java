package com.tencent.assistant.component.smartcard;

import android.os.Bundle;
import android.view.View;
import com.qq.AppService.AstApp;
import com.tencent.assistant.b.a;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.link.b;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class i extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ STInfoV2 f1148a;
    final /* synthetic */ NormalSmartCardSelfUpdateItem b;

    i(NormalSmartCardSelfUpdateItem normalSmartCardSelfUpdateItem, STInfoV2 sTInfoV2) {
        this.b = normalSmartCardSelfUpdateItem;
        this.f1148a = sTInfoV2;
    }

    public void onTMAClick(View view) {
        Bundle bundle = new Bundle();
        bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.b.a(this.b.contextToPageId(this.b.f1115a)));
        b.b(this.b.getContext(), "tmast://appdetails?" + a.c + "=" + AstApp.i().getPackageName(), bundle);
    }

    public STInfoV2 getStInfo() {
        if (this.f1148a != null) {
            this.f1148a.actionId = 200;
        }
        return this.f1148a;
    }
}
