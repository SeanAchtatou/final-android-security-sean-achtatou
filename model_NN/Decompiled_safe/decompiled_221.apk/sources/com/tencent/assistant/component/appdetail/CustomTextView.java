package com.tencent.assistant.component.appdetail;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.TextView;

/* compiled from: ProGuard */
public class CustomTextView extends TextView {

    /* renamed from: a  reason: collision with root package name */
    private CustomTextViewInterface f904a = null;
    private boolean b = false;

    /* compiled from: ProGuard */
    public interface CustomTextViewInterface {
        void viewExposureST();
    }

    public CustomTextView(Context context) {
        super(context);
    }

    public CustomTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public CustomTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void setStListener(CustomTextViewInterface customTextViewInterface) {
        this.f904a = customTextViewInterface;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.b) {
            if (this.f904a != null) {
                this.f904a.viewExposureST();
            }
            this.b = true;
        }
    }
}
