package com.tencent.assistant.component.txscrollview;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.cj;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.component.RankHeaderLayout;
import com.tencent.assistantv2.manager.i;

/* compiled from: ProGuard */
public class RankRefreshGetMoreListView extends TXRefreshScrollViewBase<ListView> implements AbsListView.OnScrollListener {
    private static int mTryTimes = 1;
    /* access modifiers changed from: private */
    public boolean bReseted = true;
    private boolean end = false;
    public boolean isHideInstalledAppAreaAdded = false;
    protected TXLoadingLayoutBase mFooterLoadingView;
    protected TXRefreshScrollViewBase.RefreshState mGetMoreRefreshState = TXRefreshScrollViewBase.RefreshState.RESET;
    private AbsListView.OnScrollListener mIScrollListener;
    protected RankHeaderLayout mRankHeaderView;
    private ListAdapter mRawAdapter;
    protected int mScrollState = 0;
    private ImageView mTopPaddingImage;
    private int mTopPaddingSize = 0;
    private boolean needAnimation = true;

    public RankRefreshGetMoreListView(Context context) {
        super(context, TXScrollViewBase.ScrollDirection.SCROLL_DIRECTION_VERTICAL, TXScrollViewBase.ScrollMode.PULL_FROM_START);
    }

    public RankRefreshGetMoreListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* access modifiers changed from: protected */
    public void updateUIFroMode() {
        LinearLayout.LayoutParams loadingLayoutLayoutParams = getLoadingLayoutLayoutParams();
        if (this.mHeaderLayout != null) {
            if (this.mHeaderLayout.getParent() == this) {
                removeView(this.mHeaderLayout);
            }
            addViewInternal(this.mHeaderLayout, 0, loadingLayoutLayoutParams);
        }
        this.mFooterLayout = null;
        refreshLoadingLayoutSize();
    }

    public void onRefreshComplete(boolean z) {
        onRefreshComplete(z, true);
    }

    public void onRefreshComplete(boolean z, boolean z2) {
        super.onRefreshComplete(z);
        if (this.mGetMoreRefreshState == TXRefreshScrollViewBase.RefreshState.REFRESHING) {
            if (z) {
                this.mGetMoreRefreshState = TXRefreshScrollViewBase.RefreshState.RESET;
            } else {
                this.mGetMoreRefreshState = TXRefreshScrollViewBase.RefreshState.REFRESH_LOAD_FINISH;
            }
        } else if (this.mGetMoreRefreshState == TXRefreshScrollViewBase.RefreshState.REFRESH_LOAD_FINISH) {
            this.mGetMoreRefreshState = TXRefreshScrollViewBase.RefreshState.RESET;
        } else if (this.mGetMoreRefreshState == TXRefreshScrollViewBase.RefreshState.RESET && !z) {
            this.mGetMoreRefreshState = TXRefreshScrollViewBase.RefreshState.REFRESH_LOAD_FINISH;
        }
        updateFootViewState(z2);
        if (z2) {
            mTryTimes = 1;
        } else {
            mTryTimes--;
        }
    }

    public void onTopRefreshComplete() {
        if (this.bReseted) {
            this.bReseted = false;
            onReset();
        }
    }

    public void onTopRefreshCompleteNoAnimation() {
        if (this.bReseted) {
            this.bReseted = false;
            this.needAnimation = false;
            onReset();
        }
    }

    /* access modifiers changed from: protected */
    public void onReset() {
        int i;
        RankHeaderLayout rankHeaderLayout;
        TXLoadingLayoutBase tXLoadingLayoutBase;
        boolean z;
        if (this.mCurScrollState != TXScrollViewBase.ScrollState.ScrollState_FromStart || this.mHeaderLayout == null || this.mRankHeaderView == null) {
            i = 0;
            rankHeaderLayout = null;
            tXLoadingLayoutBase = null;
            z = false;
        } else {
            tXLoadingLayoutBase = this.mHeaderLayout;
            rankHeaderLayout = this.mRankHeaderView;
            i = 0 - this.mHeaderLayout.getContentSize();
            z = Math.abs(((ListView) this.mScrollContentView).getFirstVisiblePosition() - 0) <= 1;
        }
        if (rankHeaderLayout != null && rankHeaderLayout.getVisibility() == 0) {
            rankHeaderLayout.setVisibility(8);
            tXLoadingLayoutBase.showAllSubViews();
            if (z) {
                ((ListView) this.mScrollContentView).setSelection(0);
                contentViewScrollTo(i);
            }
        }
        this.mIsBeingDragged = false;
        this.mLayoutVisibilityChangesEnabled = true;
        if (this.needAnimation) {
            smoothScrollTo(0);
        } else {
            scrollTo(0, 0);
            this.needAnimation = true;
        }
        postDelayed(new a(this), 200);
    }

    /* access modifiers changed from: protected */
    public ListView createScrollContentView(Context context) {
        ListView listView = new ListView(context);
        if (this.mScrollMode != TXScrollViewBase.ScrollMode.NONE) {
            this.mRankHeaderView = buildListViewHeader(context, listView, TXScrollViewBase.ScrollMode.PULL_FROM_START);
            this.mFooterLoadingView = new RefreshListLoading(context, TXScrollViewBase.ScrollDirection.SCROLL_DIRECTION_VERTICAL, TXScrollViewBase.ScrollMode.PULL_FROM_END);
            this.mFooterLoadingView.setVisibility(0);
            listView.addFooterView(this.mFooterLoadingView);
            listView.setOnScrollListener(this);
        }
        return listView;
    }

    /* access modifiers changed from: protected */
    public RankHeaderLayout buildListViewHeader(Context context, ListView listView, TXScrollViewBase.ScrollMode scrollMode) {
        FrameLayout frameLayout = new FrameLayout(getContext());
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -2, 1);
        RankHeaderLayout rankHeaderLayout = (RankHeaderLayout) createLoadingLayout(context, scrollMode);
        rankHeaderLayout.setVisibility(8);
        frameLayout.addView(rankHeaderLayout, layoutParams);
        FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(-1, df.a(context, (float) this.mTopPaddingSize));
        this.mTopPaddingImage = new ImageView(context);
        layoutParams2.topMargin = rankHeaderLayout.getHeight();
        frameLayout.addView(this.mTopPaddingImage, layoutParams2);
        listView.addHeaderView(frameLayout, null, false);
        return rankHeaderLayout;
    }

    public void setPaddingTopSize(int i) {
        this.mTopPaddingSize = i;
    }

    public void addFooterView(View view) {
        if (view != null) {
            if (this.mFooterLoadingView != null) {
                ((ListView) this.mScrollContentView).removeFooterView(this.mFooterLoadingView);
            }
            this.mFooterLoadingView = (TXLoadingLayoutBase) view;
            ((ListView) this.mScrollContentView).addFooterView(this.mFooterLoadingView);
            this.mFooterLoadingView.setVisibility(0);
            updateFootViewState();
        }
    }

    public void addClickLoadMore() {
        this.mFooterLoadingView.setOnClickListener(new b(this));
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
        this.mScrollState = i;
        if (this.mIScrollListener != null) {
            this.mIScrollListener.onScrollStateChanged(absListView, i);
        }
        if (i == 0 && this.end) {
            onEndPostion();
            mTryTimes = 1;
        }
    }

    private void onEndPostion() {
        if (this.mGetMoreRefreshState == TXRefreshScrollViewBase.RefreshState.RESET) {
            if (this.mRefreshListViewListener != null) {
                this.mRefreshListViewListener.onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState.ScrollState_FromEnd);
            }
            this.mGetMoreRefreshState = TXRefreshScrollViewBase.RefreshState.REFRESHING;
            updateFootViewState();
        }
    }

    /* access modifiers changed from: protected */
    public void updateFootViewState() {
        updateFootViewState(true);
    }

    /* access modifiers changed from: protected */
    public void updateFootViewText() {
        int i = 0;
        if (getRawAdapter() != null) {
            i = getRawAdapter().getCount();
        }
        if (this.mFooterLoadingView != null && i > 0) {
            this.mFooterLoadingView.loadFinish(cj.b(getContext(), i));
        }
    }

    /* access modifiers changed from: protected */
    public void updateFootViewState(boolean z) {
        if (this.mFooterLoadingView != null) {
            switch (c.f1199a[this.mGetMoreRefreshState.ordinal()]) {
                case 1:
                    if (z) {
                        this.mFooterLoadingView.loadSuc();
                        return;
                    } else {
                        this.mFooterLoadingView.loadFail();
                        return;
                    }
                case 2:
                    updateFootViewText();
                    return;
                case 3:
                    this.mFooterLoadingView.refreshing();
                    return;
                default:
                    return;
            }
        }
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if (this.mScrollContentView != null) {
            if (this.isHideInstalledAppAreaAdded) {
                this.mHeaderLayout.findViewById(R.id.tips).setVisibility(0);
            } else {
                ((RankHeaderLayout) this.mHeaderLayout).a(i.a().b().c());
                this.mHeaderLayout.findViewById(R.id.tips).setVisibility(8);
            }
            if (this.mIScrollListener != null) {
                this.mIScrollListener.onScroll(absListView, i, i2, i3);
            }
            this.end = isReadyForScrollEnd();
            if (this.end && mTryTimes > 0 && !isContentFullScreen()) {
                onEndPostion();
            }
        }
    }

    /* access modifiers changed from: protected */
    public TXLoadingLayoutBase createLoadingLayout(Context context, TXScrollViewBase.ScrollMode scrollMode) {
        return new RankHeaderLayout(context, TXScrollViewBase.ScrollDirection.SCROLL_DIRECTION_VERTICAL, scrollMode);
    }

    /* access modifiers changed from: protected */
    public boolean isReadyForScrollStart() {
        View childAt;
        ListAdapter adapter = ((ListView) this.mScrollContentView).getAdapter();
        if (adapter == null || adapter.isEmpty()) {
            return true;
        }
        if (((ListView) this.mScrollContentView).getFirstVisiblePosition() > 1 || (childAt = ((ListView) this.mScrollContentView).getChildAt(0)) == null) {
            return false;
        }
        return childAt.getTop() >= ((ListView) this.mScrollContentView).getTop();
    }

    public void setRankHeaderPaddingBottomAdded(int i) {
        if (this.mHeaderLayout != null && (this.mHeaderLayout instanceof RankHeaderLayout)) {
            ((RankHeaderLayout) this.mHeaderLayout).a(i);
        }
    }

    /* access modifiers changed from: protected */
    public boolean isReadyForScrollEnd() {
        View childAt;
        ListAdapter adapter = ((ListView) this.mScrollContentView).getAdapter();
        if (adapter == null || adapter.isEmpty()) {
            return true;
        }
        int count = ((ListView) this.mScrollContentView).getCount() - 1;
        int lastVisiblePosition = ((ListView) this.mScrollContentView).getLastVisiblePosition();
        if (lastVisiblePosition < count - 1 || (childAt = ((ListView) this.mScrollContentView).getChildAt(lastVisiblePosition - ((ListView) this.mScrollContentView).getFirstVisiblePosition())) == null) {
            return false;
        }
        return childAt.getBottom() <= ((ListView) this.mScrollContentView).getBottom();
    }

    /* access modifiers changed from: protected */
    public boolean isContentFullScreen() {
        if (((ListView) this.mScrollContentView).getFirstVisiblePosition() > 0) {
            return true;
        }
        if (((ListView) this.mScrollContentView).getLastVisiblePosition() < ((ListView) this.mScrollContentView).getCount() - 1) {
            return true;
        }
        return false;
    }

    public void setDivider(Drawable drawable) {
        ((ListView) this.mScrollContentView).setDivider(drawable);
    }

    public void setSelection(int i) {
        ((ListView) this.mScrollContentView).setSelection(i);
    }

    public void setAdapter(ListAdapter listAdapter) {
        if (listAdapter != null) {
            this.mRawAdapter = listAdapter;
            ((ListView) this.mScrollContentView).setAdapter(listAdapter);
        }
    }

    public void setSelector(Drawable drawable) {
        if (this.mScrollContentView != null && drawable != null) {
            ((ListView) this.mScrollContentView).setSelector(drawable);
        }
    }

    public int getFirstVisiblePosition() {
        if (this.mScrollContentView != null) {
            return ((ListView) this.mScrollContentView).getFirstVisiblePosition();
        }
        return -1;
    }

    public ListView getListView() {
        return (ListView) this.mScrollContentView;
    }

    public ListAdapter getRawAdapter() {
        return this.mRawAdapter;
    }

    public void setOnScrollerListener(AbsListView.OnScrollListener onScrollListener) {
        this.mIScrollListener = onScrollListener;
    }

    /* access modifiers changed from: protected */
    public int getSmoothScrollDuration() {
        return 200;
    }

    /* access modifiers changed from: protected */
    public int scrollMoveEvent() {
        XLog.d("RefreshListLoading", " scrollMoveEvent");
        int scrollMoveEvent = super.scrollMoveEvent();
        int i = 0;
        if (scrollMoveEvent != 0) {
            if (this.mCurScrollState == TXScrollViewBase.ScrollState.ScrollState_FromStart && this.mHeaderLayout != null) {
                i = this.mHeaderLayout.getContentSize() >> 1;
                this.mHeaderLayout.onPull(scrollMoveEvent);
            } else if (this.mCurScrollState == TXScrollViewBase.ScrollState.ScrollState_FromEnd && this.mFooterLayout != null) {
                i = this.mFooterLayout.getContentSize();
                this.mFooterLayout.onPull(scrollMoveEvent);
            }
            if (this.mCurScrollState == TXScrollViewBase.ScrollState.ScrollState_FromEnd && this.mRefreshState == TXRefreshScrollViewBase.RefreshState.REFRESH_LOAD_FINISH) {
                return scrollMoveEvent;
            }
            if (this.mRefreshState != TXRefreshScrollViewBase.RefreshState.PULL_TO_REFRESH && i >= Math.abs(scrollMoveEvent)) {
                setState(TXRefreshScrollViewBase.RefreshState.PULL_TO_REFRESH);
            } else if (this.mRefreshState == TXRefreshScrollViewBase.RefreshState.PULL_TO_REFRESH && i < Math.abs(scrollMoveEvent)) {
                setState(TXRefreshScrollViewBase.RefreshState.RELEASE_TO_REFRESH);
            }
        }
        return scrollMoveEvent;
    }

    public void setShowHeaderFilterInfo(boolean z) {
        if (this.mHeaderLayout != null) {
            ((RankHeaderLayout) this.mHeaderLayout).b(z);
        }
    }

    public void setTopPaddingSize(int i) {
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.mTopPaddingImage.getLayoutParams();
        layoutParams.height = df.a(getContext(), (float) i);
        this.mTopPaddingImage.setLayoutParams(layoutParams);
        requestLayout();
    }
}
