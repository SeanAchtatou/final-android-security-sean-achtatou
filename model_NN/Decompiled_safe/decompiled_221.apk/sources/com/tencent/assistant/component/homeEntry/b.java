package com.tencent.assistant.component.homeEntry;

import android.graphics.Bitmap;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class b implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Bitmap f1055a;
    final /* synthetic */ HomeEntryCellBase b;

    b(HomeEntryCellBase homeEntryCellBase, Bitmap bitmap) {
        this.b = homeEntryCellBase;
        this.f1055a = bitmap;
    }

    public void run() {
        XLog.v("home_entry", "cell---requestImg--cell name:" + getClass().getSimpleName() + "--bitmap refresh2222222---------------netWork");
        this.b.setImg(this.f1055a);
    }
}
