package com.tencent.assistant.component;

import android.view.View;
import com.tencent.assistant.activity.SelfUpdateActivity;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.manager.SelfUpdateManager;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class di extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SelfNormalUpdateView f1018a;

    di(SelfNormalUpdateView selfNormalUpdateView) {
        this.f1018a = selfNormalUpdateView;
    }

    public void onTMAClick(View view) {
        this.f1018a.ignoreUpdate();
        SelfUpdateManager.a().o().b(true);
    }

    public STInfoV2 getStInfo() {
        if (!(this.f1018a.getContext() instanceof SelfUpdateActivity)) {
            return null;
        }
        STInfoV2 i = ((SelfUpdateActivity) this.f1018a.getContext()).i();
        i.slotId = "01_002";
        i.actionId = 200;
        return i;
    }
}
