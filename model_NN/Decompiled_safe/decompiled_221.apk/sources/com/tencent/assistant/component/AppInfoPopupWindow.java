package com.tencent.assistant.component;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.PopupWindow;
import com.tencent.android.qqdownloader.R;
import com.tencent.connect.common.Constants;
import java.util.List;

/* compiled from: ProGuard */
public class AppInfoPopupWindow {
    private List<Button> buttonList;
    private int contentHeight = 0;
    private View contentView;
    private Context context;
    private PopupWindow popupWindow;
    public String slot = Constants.STR_EMPTY;
    private Object tag;

    public AppInfoPopupWindow(Context context2, View view, int i, int i2, List<Button> list) {
        this.context = context2;
        this.contentHeight = i2;
        this.popupWindow = new PopupWindow(view, -2, this.contentHeight);
        this.contentView = view;
        this.buttonList = list;
    }

    public void dismiss() {
        this.popupWindow.dismiss();
    }

    public boolean isShowing() {
        return this.popupWindow.isShowing();
    }

    public void showAtView(View view, int i, int i2) {
        showAtView(view, null, i, i2, 0, 0);
    }

    public void showAtView(View view, int i, int i2, int i3, int i4) {
        showAtView(view, null, i, i2, i3, i4);
    }

    public void showAtView(View view, Object obj, int i, int i2, int i3, int i4) {
        int i5;
        if (!this.popupWindow.isShowing()) {
            this.tag = obj;
            int top = view.getTop();
            int i6 = -(view.getHeight() + this.contentHeight);
            if (top < this.contentHeight) {
                int i7 = i + 0;
                View view2 = this.contentView;
                if (i4 <= 0) {
                    i4 = R.drawable.down_pop_bg_r;
                }
                view2.setBackgroundResource(i4);
                i5 = i7;
            } else {
                int i8 = i6 + i2;
                View view3 = this.contentView;
                if (i3 <= 0) {
                    i3 = R.drawable.down_pop_bg_n;
                }
                view3.setBackgroundResource(i3);
                i5 = i8;
            }
            int[] iArr = new int[2];
            view.getLocationInWindow(iArr);
            int height = view.getHeight() + iArr[1];
            if (this.context == null) {
                return;
            }
            if (!(this.context instanceof Activity) || !((Activity) this.context).isFinishing()) {
                this.popupWindow.showAtLocation(view, 49, 0, i5 + height);
            }
        }
    }

    public Object getTag() {
        return this.tag;
    }

    public void setButtonText(int i, String str) {
        if (this.buttonList != null && this.buttonList.size() > i) {
            this.buttonList.get(i).setText(str);
        }
    }
}
