package com.tencent.assistant.component;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RatingBar;

/* compiled from: ProGuard */
public class RatingView extends RatingBar {
    public RatingView(Context context) {
        this(context, null);
    }

    public RatingView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setClickable(false);
    }

    public void setRating(double d) {
        try {
            setRating((float) d);
        } catch (Exception e) {
        }
    }

    public void setEnable(boolean z) {
        setClickable(z);
        setEnabled(z);
        setStepSize(1.0f);
    }
}
