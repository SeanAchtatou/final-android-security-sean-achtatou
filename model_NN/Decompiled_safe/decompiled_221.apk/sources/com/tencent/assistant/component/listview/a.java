package com.tencent.assistant.component.listview;

import com.tencent.assistant.component.listview.AnimationExpandableListView;

/* compiled from: ProGuard */
class a implements AnimationExpandableListView.ItemAnimatorLisenter {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1081a;
    final /* synthetic */ AnimationExpandableListView.ItemAnimatorLisenter b;
    final /* synthetic */ AnimationExpandableListView c;

    a(AnimationExpandableListView animationExpandableListView, int i, AnimationExpandableListView.ItemAnimatorLisenter itemAnimatorLisenter) {
        this.c = animationExpandableListView;
        this.f1081a = i;
        this.b = itemAnimatorLisenter;
    }

    public void onAnimatorEnd() {
        this.c.deleteAllVisibleItem(this.f1081a + 1, this.b);
    }
}
