package com.tencent.assistant.component;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class cb extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PopViewDialog f984a;

    cb(PopViewDialog popViewDialog) {
        this.f984a = popViewDialog;
    }

    public void onTMAClick(View view) {
        this.f984a.shareBtnView2.setSelected(!this.f984a.shareBtnView2.isSelected());
        if (this.f984a.shareBtnView2.isSelected()) {
            boolean unused = this.f984a.shareToWX = true;
            boolean unused2 = this.f984a.shareToQZ = false;
            this.f984a.shareBtnView.setSelected(false);
            if (!this.f984a.loginProxy.l()) {
                this.f984a.errorTips.setText((int) R.string.comment_share_wxtips);
                this.f984a.errorTips.setVisibility(0);
                return;
            }
            return;
        }
        boolean unused3 = this.f984a.shareToWX = false;
        this.f984a.errorTips.setVisibility(4);
    }

    public STInfoV2 getStInfo() {
        STInfoV2 access$200 = this.f984a.buildFloatingSTInfo();
        if (access$200 != null) {
            access$200.slotId = a.a(this.f984a.getColumn(), "005");
            if (!this.f984a.shareBtnView.isSelected()) {
                access$200.status = "01";
            } else {
                access$200.status = "02";
            }
            access$200.actionId = 200;
        }
        return access$200;
    }
}
