package com.tencent.assistant.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMASwitchButtonClickListener;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ba;

/* compiled from: ProGuard */
public class SwitchButton extends RelativeLayout implements View.OnClickListener {
    public static final int SWITCH_ANIMATION_DURATION = 150;
    /* access modifiers changed from: private */
    public boolean isSwitchListenerOn = false;
    /* access modifiers changed from: private */
    public boolean isSwitchOn = false;
    private TextView left_text;
    private int mAnimationDuration;
    private Context mContext;
    private int mCycle;
    private LayoutInflater mInflater;
    private Animation mScaleCloseAnimation;
    private Animation mScaleOpenAnimation;
    private Animation mTransCloseAnimation;
    private dq mTransCloseAnimationListener = new Cdo(this);
    private Animation mTransOpenAnimation;
    private dq mTransOpenAnimationListener = new dn(this);
    /* access modifiers changed from: private */
    public View mView;
    private int mWidth;
    /* access modifiers changed from: private */
    public OnTMASwitchButtonClickListener onSwitchButtonClickListener;
    private TextView right_text;
    private ImageView switch_default_Bkg;
    private ImageView switch_left_Bkg;
    private ImageView switch_off_Bkg;
    private ImageView switch_on_Bkg;
    private ImageView switch_right_Bkg;

    public SwitchButton(Context context) {
        super(context);
        this.mContext = context;
        this.mView = this;
        this.mAnimationDuration = SWITCH_ANIMATION_DURATION;
        init();
    }

    public SwitchButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.mContext = context;
        this.mView = this;
        this.mAnimationDuration = SWITCH_ANIMATION_DURATION;
        init();
    }

    public void setAnimationDuration(int i) {
        this.mAnimationDuration = i;
    }

    public int getAnimationDuration() {
        return this.mAnimationDuration;
    }

    private void init() {
        setOnClickListener(this);
        this.mInflater = LayoutInflater.from(this.mContext);
        this.mInflater.inflate((int) R.layout.setting_switch_view_layout, this);
        this.switch_default_Bkg = (ImageView) findViewById(R.id.img_default_bg);
        this.switch_on_Bkg = (ImageView) findViewById(R.id.img_open_bg);
        this.switch_off_Bkg = (ImageView) findViewById(R.id.img_close_bg);
        this.switch_left_Bkg = (ImageView) findViewById(R.id.img_switch_left);
        this.switch_right_Bkg = (ImageView) findViewById(R.id.img_switch_right);
        this.left_text = (TextView) findViewById(R.id.text_left);
        this.right_text = (TextView) findViewById(R.id.text_right);
        setTitlesOfSwitch(this.mContext.getString(R.string.setting_left_title), this.mContext.getString(R.string.setting_right_title));
    }

    private void initAnimation() {
        this.mWidth = getWidth();
        if (this.switch_left_Bkg.getVisibility() == 0) {
            this.mCycle = this.switch_left_Bkg.getWidth();
        } else {
            this.mCycle = this.switch_right_Bkg.getWidth();
        }
        XLog.i("ighuang", "switch-width= " + this.mWidth + "cycle-width= " + this.mCycle);
        this.mScaleOpenAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.setting_switch_scale_out);
        this.mScaleCloseAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.setting_switch_scale_in);
        this.mScaleOpenAnimation.setDuration((long) this.mAnimationDuration);
        this.mScaleCloseAnimation.setDuration((long) this.mAnimationDuration);
        this.mTransOpenAnimation = new TranslateAnimation(0.0f, (float) (this.mWidth - this.mCycle), 0.0f, 0.0f);
        this.mTransOpenAnimation.setDuration((long) this.mAnimationDuration);
        this.mTransOpenAnimation.setInterpolator(new AccelerateInterpolator());
        this.mTransOpenAnimation.setAnimationListener(this.mTransOpenAnimationListener);
        this.mTransCloseAnimation = new TranslateAnimation(0.0f, (float) (-(this.mWidth - this.mCycle)), 0.0f, 0.0f);
        this.mTransCloseAnimation.setDuration((long) this.mAnimationDuration);
        this.mTransCloseAnimation.setInterpolator(new AccelerateInterpolator());
        this.mTransCloseAnimation.setAnimationListener(this.mTransCloseAnimationListener);
    }

    public void setSwitchState(boolean z) {
        this.isSwitchOn = z;
        initSwitchButton();
    }

    public void setTitlesOfSwitch(String str, String str2) {
        this.left_text.setText(str);
        this.right_text.setText(str2);
    }

    public boolean getSwitchState() {
        return this.isSwitchOn;
    }

    public void updateSwitchStateWithAnim(boolean z) {
        this.isSwitchOn = z;
        startSwitchAnimation();
    }

    private void initSwitchButton() {
        if (this.isSwitchOn) {
            setOffToOnView();
        } else {
            setOnToOffView();
        }
    }

    /* access modifiers changed from: private */
    public void setOffToOnView() {
        this.switch_off_Bkg.clearAnimation();
        this.switch_left_Bkg.setVisibility(4);
        this.switch_right_Bkg.setVisibility(0);
        this.switch_off_Bkg.setVisibility(4);
        this.left_text.setVisibility(0);
        this.right_text.setVisibility(4);
        this.switch_on_Bkg.setVisibility(0);
        XLog.i("ighuang_auto", "setOnView_finish");
    }

    /* access modifiers changed from: private */
    public void setOnToOffView() {
        this.switch_off_Bkg.clearAnimation();
        this.switch_left_Bkg.setVisibility(0);
        this.switch_right_Bkg.setVisibility(4);
        this.switch_off_Bkg.setVisibility(0);
        this.left_text.setVisibility(4);
        this.right_text.setVisibility(0);
        this.switch_on_Bkg.setVisibility(4);
        XLog.i("ighuang_auto", "setOffView_finish");
    }

    private void startSwitchAnimation() {
        initAnimation();
        if (this.isSwitchOn) {
            this.switch_left_Bkg.startAnimation(this.mTransOpenAnimation);
            this.switch_off_Bkg.setVisibility(0);
            this.switch_off_Bkg.startAnimation(this.mScaleOpenAnimation);
            this.right_text.setVisibility(4);
            this.switch_on_Bkg.setVisibility(0);
            return;
        }
        this.switch_right_Bkg.startAnimation(this.mTransCloseAnimation);
        this.switch_off_Bkg.setVisibility(0);
        this.switch_off_Bkg.startAnimation(this.mScaleCloseAnimation);
        this.left_text.setVisibility(4);
    }

    public void onClick(View view) {
        updateSwitchStateWithAnim(!getSwitchState());
        ba.a().postDelayed(new dp(this), (long) this.mAnimationDuration);
    }

    public void setOnSwitchListener(OnTMASwitchButtonClickListener onTMASwitchButtonClickListener) {
        this.onSwitchButtonClickListener = onTMASwitchButtonClickListener;
        this.isSwitchListenerOn = true;
    }

    public void setEnabled(boolean z) {
        super.setEnabled(z);
        if (z) {
            this.switch_default_Bkg.setVisibility(8);
            this.switch_off_Bkg.setVisibility(0);
            this.switch_on_Bkg.setVisibility(0);
            return;
        }
        this.switch_default_Bkg.setVisibility(0);
        this.switch_off_Bkg.setVisibility(4);
        this.switch_on_Bkg.setVisibility(4);
    }
}
