package com.tencent.assistant.component.listview;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.adapter.d;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;
import com.tencent.assistant.component.listview.AnimationExpandableListView;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.ba;
import java.util.ArrayList;
import java.util.Map;

/* compiled from: ProGuard */
public class ApkResultListView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private Context f1076a;
    public d adapter;
    private LayoutInflater b;
    /* access modifiers changed from: private */
    public View c;
    /* access modifiers changed from: private */
    public AnimationExpandableListView d;
    /* access modifiers changed from: private */
    public RelativeLayout e;
    /* access modifiers changed from: private */
    public TextView f;
    /* access modifiers changed from: private */
    public TextView g;
    /* access modifiers changed from: private */
    public int h;
    /* access modifiers changed from: private */
    public int i;
    /* access modifiers changed from: private */
    public int j;
    private ListViewScrollListener k;

    public ApkResultListView(Context context) {
        this(context, null);
    }

    public ApkResultListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = 0;
        this.i = -1;
        this.k = new e(this);
        this.f1076a = context;
        this.b = (LayoutInflater) this.f1076a.getSystemService("layout_inflater");
        a();
    }

    public void deleteItem(AnimationExpandableListView.ItemAnimatorLisenter itemAnimatorLisenter) {
        Pair pair = this.adapter.c().get(0);
        this.d.delete(((Integer) pair.first).intValue(), ((Integer) pair.second).intValue(), true, itemAnimatorLisenter);
    }

    public AnimationExpandableListView getAnimationListView() {
        return this.d;
    }

    public d getAdapter() {
        return this.adapter;
    }

    public void deleteSelectItems() {
    }

    private void a() {
        this.c = this.b.inflate((int) R.layout.apkresult_component_view, this);
        this.d = (AnimationExpandableListView) this.c.findViewById(R.id.list_view);
        this.d.setGroupIndicator(null);
        this.d.setDivider(null);
        this.d.setChildDivider(null);
        this.d.setSelector(R.drawable.transparent_selector);
        this.d.setOnScrollListener(this.k);
        this.d.setOnGroupClickListener(new d(this));
        this.adapter = new d(this.f1076a, this.d);
        this.d.setAdapter(this.adapter);
        this.h = this.adapter.getGroupCount();
        this.f = (TextView) this.c.findViewById(R.id.group_title);
        this.g = (TextView) this.c.findViewById(R.id.select_all);
    }

    /* access modifiers changed from: private */
    public int b() {
        int i2 = this.j;
        int pointToPosition = this.d.pointToPosition(0, this.j);
        if (pointToPosition == -1 || ExpandableListView.getPackedPositionGroup(this.d.getExpandableListPosition(pointToPosition)) == this.i) {
            return i2;
        }
        View expandChildAt = this.d.getExpandChildAt(pointToPosition - this.d.getFirstVisiblePosition());
        if (expandChildAt != null) {
            return expandChildAt.getTop();
        }
        return 100;
    }

    /* access modifiers changed from: private */
    public void c() {
        for (int i2 = 0; i2 < this.adapter.getGroupCount(); i2++) {
            this.d.expandGroup(i2);
        }
    }

    public void refreshData(Map<Integer, ArrayList<LocalApkInfo>> map, boolean z, boolean z2) {
        ba.a().post(new h(this, map, z2));
    }

    public void deleteAllSelectedItems() {
        ba.a().post(new i(this));
    }

    public void setApkHandleToAdapter(Handler handler) {
        this.adapter.f759a = handler;
    }

    public void setApkChildListenerToAdapter(ExpandableListView.OnChildClickListener onChildClickListener) {
        this.d.setOnChildClickListener(onChildClickListener);
    }

    public String getListSlotId(int i2, int i3) {
        if (this.adapter != null) {
            return this.adapter.b(i2, i3);
        }
        return STConst.ST_DEFAULT_SLOT;
    }
}
