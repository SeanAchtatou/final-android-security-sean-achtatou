package com.tencent.assistant.component;

import android.view.View;

/* compiled from: ProGuard */
class p implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CategoryListPage f1100a;

    p(CategoryListPage categoryListPage) {
        this.f1100a = categoryListPage;
    }

    public void onClick(View view) {
        this.f1100a.refreshData();
        this.f1100a.onNetworkLoading();
    }
}
