package com.tencent.assistant.component.txscrollview;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.utils.cj;

/* compiled from: ProGuard */
public class TXRefreshListView extends TXRefreshScrollViewBase<ListView> {
    protected ITXRefreshListLoadingLayoutCreateCallBack mCreateLoadingLayoutCallBack = null;
    protected TXLoadingLayoutBase mFooterLoadingView;
    protected TXLoadingLayoutBase mHeaderLoadingView;
    protected ListAdapter mRawAdapter;

    public TXRefreshListView(Context context, TXScrollViewBase.ScrollMode scrollMode) {
        super(context, TXScrollViewBase.ScrollDirection.SCROLL_DIRECTION_VERTICAL, scrollMode);
    }

    public TXRefreshListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void setAdapter(ListAdapter listAdapter) {
        this.mRawAdapter = listAdapter;
        ((ListView) this.mScrollContentView).setAdapter(listAdapter);
    }

    public ListAdapter getAdapter() {
        return ((ListView) this.mScrollContentView).getAdapter();
    }

    public ListAdapter getRawAdapter() {
        return this.mRawAdapter;
    }

    public void setDivider(Drawable drawable) {
        ((ListView) this.mScrollContentView).setDivider(drawable);
    }

    public void setVerticalScrollBarEnabled(boolean z) {
        ((ListView) this.mScrollContentView).setVerticalScrollBarEnabled(z);
    }

    public void setSelector(Drawable drawable) {
        if (this.mScrollContentView != null && drawable != null) {
            ((ListView) this.mScrollContentView).setSelector(drawable);
        }
    }

    public void addHeaderView(View view) {
        if (this.mScrollMode == TXScrollViewBase.ScrollMode.PULL_FROM_END || this.mScrollMode == TXScrollViewBase.ScrollMode.NONE) {
            ((ListView) this.mScrollContentView).addHeaderView(view);
        }
    }

    public void addFooterView(View view) {
        if (this.mScrollMode == TXScrollViewBase.ScrollMode.PULL_FROM_START || this.mScrollMode == TXScrollViewBase.ScrollMode.NONE) {
            ((ListView) this.mScrollContentView).addFooterView(view);
        }
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        ((ListView) this.mScrollContentView).setOnItemClickListener(onItemClickListener);
    }

    public void setOnItemSelectedListener(AdapterView.OnItemSelectedListener onItemSelectedListener) {
        ((ListView) this.mScrollContentView).setOnItemSelectedListener(onItemSelectedListener);
    }

    public void reset() {
        if (this.mRefreshState == TXRefreshScrollViewBase.RefreshState.REFRESH_LOAD_FINISH) {
            setState(TXRefreshScrollViewBase.RefreshState.RESET);
        }
    }

    /* access modifiers changed from: protected */
    public void onReset() {
        int i;
        int i2;
        TXLoadingLayoutBase tXLoadingLayoutBase;
        boolean z;
        TXLoadingLayoutBase tXLoadingLayoutBase2 = null;
        boolean z2 = true;
        boolean z3 = false;
        if (this.mCurScrollState == TXScrollViewBase.ScrollState.ScrollState_FromStart && this.mHeaderLayout != null && this.mHeaderLoadingView != null) {
            TXLoadingLayoutBase tXLoadingLayoutBase3 = this.mHeaderLayout;
            TXLoadingLayoutBase tXLoadingLayoutBase4 = this.mHeaderLoadingView;
            int contentSize = 0 - this.mHeaderLayout.getContentSize();
            if (Math.abs(((ListView) this.mScrollContentView).getFirstVisiblePosition() - 0) <= 1) {
                z = true;
            } else {
                z = false;
            }
            i = contentSize;
            i2 = 0;
            z3 = z;
            tXLoadingLayoutBase2 = tXLoadingLayoutBase4;
            tXLoadingLayoutBase = tXLoadingLayoutBase3;
        } else if (this.mCurScrollState != TXScrollViewBase.ScrollState.ScrollState_FromEnd || this.mFooterLayout == null || this.mFooterLoadingView == null) {
            i = 0;
            i2 = 0;
            tXLoadingLayoutBase = null;
        } else {
            TXLoadingLayoutBase tXLoadingLayoutBase5 = this.mFooterLayout;
            TXLoadingLayoutBase tXLoadingLayoutBase6 = this.mFooterLoadingView;
            int count = ((ListView) this.mScrollContentView).getCount() - 1;
            int contentSize2 = this.mFooterLayout.getContentSize();
            if (Math.abs(((ListView) this.mScrollContentView).getLastVisiblePosition() - count) > 1) {
                z2 = false;
            }
            z3 = z2;
            tXLoadingLayoutBase2 = tXLoadingLayoutBase6;
            i = contentSize2;
            i2 = count;
            tXLoadingLayoutBase = tXLoadingLayoutBase5;
        }
        if (tXLoadingLayoutBase2 != null && tXLoadingLayoutBase2.getVisibility() == 0) {
            tXLoadingLayoutBase2.setVisibility(8);
            tXLoadingLayoutBase.showAllSubViews();
            if (z3) {
                ((ListView) this.mScrollContentView).setSelection(i2);
                contentViewScrollTo(i);
            }
        }
        super.onReset();
    }

    /* access modifiers changed from: protected */
    public void onRefreshing() {
        int i;
        TXLoadingLayoutBase tXLoadingLayoutBase;
        TXLoadingLayoutBase tXLoadingLayoutBase2;
        TXLoadingLayoutBase tXLoadingLayoutBase3;
        int i2;
        ListAdapter adapter = ((ListView) this.mScrollContentView).getAdapter();
        if (adapter == null || adapter.isEmpty()) {
            super.onRefreshing();
            return;
        }
        if (this.mCurScrollState == TXScrollViewBase.ScrollState.ScrollState_FromStart && this.mHeaderLayout != null && this.mHeaderLoadingView != null) {
            TXLoadingLayoutBase tXLoadingLayoutBase4 = this.mHeaderLayout;
            TXLoadingLayoutBase tXLoadingLayoutBase5 = this.mHeaderLoadingView;
            TXLoadingLayoutBase tXLoadingLayoutBase6 = this.mFooterLoadingView;
            i = getScrollY() + this.mHeaderLayout.getContentSize();
            tXLoadingLayoutBase3 = tXLoadingLayoutBase4;
            tXLoadingLayoutBase2 = tXLoadingLayoutBase5;
            tXLoadingLayoutBase = tXLoadingLayoutBase6;
            i2 = 0;
        } else if (this.mCurScrollState != TXScrollViewBase.ScrollState.ScrollState_FromEnd || this.mFooterLayout == null || this.mFooterLoadingView == null) {
            i = 0;
            tXLoadingLayoutBase = null;
            tXLoadingLayoutBase2 = null;
            tXLoadingLayoutBase3 = null;
            i2 = 0;
        } else {
            tXLoadingLayoutBase3 = this.mFooterLayout;
            tXLoadingLayoutBase2 = this.mFooterLoadingView;
            tXLoadingLayoutBase = this.mHeaderLoadingView;
            i2 = ((ListView) this.mScrollContentView).getCount() - 1;
            i = getScrollY() - this.mFooterLayout.getContentSize();
        }
        if (tXLoadingLayoutBase3 != null) {
            tXLoadingLayoutBase3.reset();
            tXLoadingLayoutBase3.hideAllSubViews();
        }
        if (tXLoadingLayoutBase != null) {
            tXLoadingLayoutBase.setVisibility(8);
        }
        if (tXLoadingLayoutBase2 != null) {
            tXLoadingLayoutBase2.setVisibility(0);
            tXLoadingLayoutBase2.refreshing();
        }
        this.mLayoutVisibilityChangesEnabled = false;
        contentViewScrollTo(i);
        ((ListView) this.mScrollContentView).setSelection(i2);
        smoothScrollTo(0, new p(this));
    }

    /* access modifiers changed from: protected */
    public void onLoadFinish() {
        int i;
        if (this.mCurScrollState == TXScrollViewBase.ScrollState.ScrollState_FromStart && this.mHeaderLayout != null && this.mHeaderLoadingView != null) {
            TXLoadingLayoutBase tXLoadingLayoutBase = this.mHeaderLayout;
            TXLoadingLayoutBase tXLoadingLayoutBase2 = this.mHeaderLoadingView;
            int contentSize = 0 - this.mHeaderLayout.getContentSize();
            boolean z = Math.abs(((ListView) this.mScrollContentView).getFirstVisiblePosition() - 0) <= 1;
            if (tXLoadingLayoutBase2 != null && tXLoadingLayoutBase2.getVisibility() == 0) {
                tXLoadingLayoutBase2.setVisibility(8);
                tXLoadingLayoutBase.showAllSubViews();
                if (z) {
                    ((ListView) this.mScrollContentView).setSelection(0);
                    contentViewScrollTo(contentSize);
                }
            }
            super.onReset();
        } else if (this.mCurScrollState == TXScrollViewBase.ScrollState.ScrollState_FromEnd && this.mFooterLayout != null && this.mFooterLoadingView != null) {
            TXLoadingLayoutBase tXLoadingLayoutBase3 = this.mFooterLayout;
            TXLoadingLayoutBase tXLoadingLayoutBase4 = this.mFooterLoadingView;
            int count = ((ListView) this.mScrollContentView).getCount() - 1;
            int scrollY = getScrollY() - this.mFooterLayout.getContentSize();
            if (tXLoadingLayoutBase3 != null) {
                tXLoadingLayoutBase3.reset();
                tXLoadingLayoutBase3.hideAllSubViews();
            }
            if (tXLoadingLayoutBase4 != null) {
                tXLoadingLayoutBase4.setVisibility(0);
                if (getRawAdapter() != null) {
                    i = getRawAdapter().getCount();
                } else {
                    i = 0;
                }
                tXLoadingLayoutBase4.loadFinish(cj.b(getContext(), i));
            }
            this.mLayoutVisibilityChangesEnabled = false;
            smoothScrollTo(0);
        }
    }

    /* access modifiers changed from: protected */
    public TXLoadingLayoutBase createLoadingLayout(Context context, TXScrollViewBase.ScrollMode scrollMode) {
        TXLoadingLayoutBase tXLoadingLayoutBase = null;
        if (this.mCreateLoadingLayoutCallBack != null) {
            tXLoadingLayoutBase = this.mCreateLoadingLayoutCallBack.createLoadingLayout(context, scrollMode);
        }
        return tXLoadingLayoutBase != null ? tXLoadingLayoutBase : new RefreshListLoading(context, TXScrollViewBase.ScrollDirection.SCROLL_DIRECTION_VERTICAL, scrollMode);
    }

    /* access modifiers changed from: protected */
    public TXLoadingLayoutBase buildListViewHeadOrFootView(Context context, ListView listView, TXScrollViewBase.ScrollMode scrollMode) {
        FrameLayout frameLayout = new FrameLayout(getContext());
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -2, 1);
        TXLoadingLayoutBase createLoadingLayout = createLoadingLayout(context, scrollMode);
        createLoadingLayout.setVisibility(8);
        frameLayout.addView(createLoadingLayout, layoutParams);
        if (scrollMode == TXScrollViewBase.ScrollMode.PULL_FROM_START) {
            listView.addHeaderView(frameLayout, null, false);
        } else {
            listView.addFooterView(frameLayout, null, false);
        }
        return createLoadingLayout;
    }

    /* access modifiers changed from: protected */
    public ListView createScrollContentView(Context context) {
        ListView listView = new ListView(context);
        if (this.mScrollMode == TXScrollViewBase.ScrollMode.PULL_FROM_START) {
            this.mHeaderLoadingView = buildListViewHeadOrFootView(context, listView, TXScrollViewBase.ScrollMode.PULL_FROM_START);
        } else if (this.mScrollMode == TXScrollViewBase.ScrollMode.PULL_FROM_END) {
            this.mFooterLoadingView = buildListViewHeadOrFootView(context, listView, TXScrollViewBase.ScrollMode.PULL_FROM_END);
        } else if (this.mScrollMode == TXScrollViewBase.ScrollMode.BOTH) {
            this.mHeaderLoadingView = buildListViewHeadOrFootView(context, listView, TXScrollViewBase.ScrollMode.PULL_FROM_START);
            this.mFooterLoadingView = buildListViewHeadOrFootView(context, listView, TXScrollViewBase.ScrollMode.PULL_FROM_END);
        }
        return listView;
    }

    /* access modifiers changed from: protected */
    public boolean isReadyForScrollStart() {
        View childAt;
        ListAdapter adapter = ((ListView) this.mScrollContentView).getAdapter();
        if (adapter == null || adapter.isEmpty()) {
            return true;
        }
        if (((ListView) this.mScrollContentView).getFirstVisiblePosition() > 1 || (childAt = ((ListView) this.mScrollContentView).getChildAt(0)) == null) {
            return false;
        }
        return childAt.getTop() >= ((ListView) this.mScrollContentView).getTop();
    }

    /* access modifiers changed from: protected */
    public boolean isReadyForScrollEnd() {
        View childAt;
        ListAdapter adapter = ((ListView) this.mScrollContentView).getAdapter();
        if (adapter == null || adapter.isEmpty()) {
            return true;
        }
        int count = ((ListView) this.mScrollContentView).getCount() - 1;
        int lastVisiblePosition = ((ListView) this.mScrollContentView).getLastVisiblePosition();
        if (lastVisiblePosition < count - 1 || (childAt = ((ListView) this.mScrollContentView).getChildAt(lastVisiblePosition - ((ListView) this.mScrollContentView).getFirstVisiblePosition())) == null) {
            return false;
        }
        return childAt.getBottom() <= ((ListView) this.mScrollContentView).getBottom();
    }

    /* access modifiers changed from: protected */
    public boolean isContentFullScreen() {
        return false;
    }

    public void setCreateLoadingLayoutCallBack(ITXRefreshListLoadingLayoutCreateCallBack iTXRefreshListLoadingLayoutCreateCallBack) {
        this.mCreateLoadingLayoutCallBack = iTXRefreshListLoadingLayoutCreateCallBack;
    }

    public void setOnScrollListener(AbsListView.OnScrollListener onScrollListener) {
        if (this.mScrollContentView != null) {
            ((ListView) this.mScrollContentView).setOnScrollListener(onScrollListener);
        }
    }

    public View getListViewChildAt(int i) {
        if (this.mScrollContentView != null) {
            return ((ListView) this.mScrollContentView).getChildAt(i);
        }
        return null;
    }

    public ListView getListView() {
        return (ListView) this.mScrollContentView;
    }

    public int getFirstVisiblePosition() {
        if (this.mScrollContentView != null) {
            return ((ListView) this.mScrollContentView).getFirstVisiblePosition();
        }
        return 0;
    }

    public int pointToPosition(int i, int i2) {
        if (this.mScrollContentView != null) {
            return ((ListView) this.mScrollContentView).pointToPosition(i, i2);
        }
        return 0;
    }

    public void setOverscrollFooter(Drawable drawable) {
        if (this.mScrollContentView != null) {
            try {
                ((ListView) this.mScrollContentView).getClass().getMethod("setOverscrollFooter", Drawable.class).invoke(this.mScrollContentView, drawable);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void setSelection(int i) {
        ((ListView) this.mScrollContentView).setSelection(i);
    }

    public void setCacheColorHint(int i) {
        ((ListView) this.mScrollContentView).setCacheColorHint(0);
    }
}
