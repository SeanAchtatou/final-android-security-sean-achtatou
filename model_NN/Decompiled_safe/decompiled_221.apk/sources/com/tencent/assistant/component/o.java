package com.tencent.assistant.component;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.protocol.jce.TagGroup;

/* compiled from: ProGuard */
class o implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppTagHeader f1099a;

    private o(AppTagHeader appTagHeader) {
        this.f1099a = appTagHeader;
    }

    public void onClick(View view) {
        if (((TagGroup) view.getTag()) != null && !view.equals(this.f1099a.mTagArray.get(this.f1099a.mCurrentSelected))) {
            this.f1099a.selectTag(((Integer) view.getTag(R.id.category_detail_btn_index)).intValue());
            if (this.f1099a.tagClickListener != null) {
                this.f1099a.tagClickListener.onClick(view);
            }
        }
    }
}
