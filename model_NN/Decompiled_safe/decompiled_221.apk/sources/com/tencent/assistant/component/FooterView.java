package com.tencent.assistant.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;

/* compiled from: ProGuard */
public class FooterView extends LinearLayout {
    private Context context;
    private TextView extraView;
    private LinearLayout footerView;
    private TextView titleView;

    public FooterView(Context context2) {
        super(context2);
        this.context = context2;
        initView();
    }

    public FooterView(Context context2, AttributeSet attributeSet) {
        super(context2, attributeSet);
        this.context = context2;
        initView();
    }

    private void initView() {
        View inflate = LayoutInflater.from(this.context).inflate((int) R.layout.footer_layout, this);
        this.footerView = (LinearLayout) inflate.findViewById(R.id.footer);
        this.titleView = (TextView) inflate.findViewById(R.id.text);
        this.extraView = (TextView) inflate.findViewById(R.id.extra_text);
    }

    public void updateContent(String str) {
        this.titleView.setText(str);
        this.extraView.setVisibility(8);
    }

    public void updateContent(String str, String str2) {
        this.titleView.setText(str);
        this.extraView.setVisibility(0);
        this.extraView.setText(str2);
    }

    public void setFooterViewEnable(boolean z) {
        this.footerView.setEnabled(z);
        this.titleView.setEnabled(z);
        this.extraView.setEnabled(z);
    }

    public boolean getFooterViewEnable() {
        return this.footerView.isEnabled();
    }

    public void setOnFooterViewClickListener(OnTMAParamClickListener onTMAParamClickListener) {
        this.footerView.setOnClickListener(onTMAParamClickListener);
    }
}
