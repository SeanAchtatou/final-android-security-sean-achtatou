package com.tencent.assistant.component.listview;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;

/* compiled from: ProGuard */
class e extends ListViewScrollListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ApkResultListView f1085a;

    e(ApkResultListView apkResultListView) {
        this.f1085a = apkResultListView;
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
        super.onScrollStateChanged(absListView, i);
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if (this.f1085a.e == null || this.f1085a.d == null) {
            RelativeLayout unused = this.f1085a.e = (RelativeLayout) this.f1085a.c.findViewById(R.id.pop_bar);
            this.f1085a.e.setOnClickListener(new f(this));
            return;
        }
        if (i == 0) {
            this.f1085a.e.setVisibility(8);
        }
        int pointToPosition = this.f1085a.d.pointToPosition(0, this.f1085a.b() - 10);
        int pointToPosition2 = this.f1085a.d.pointToPosition(0, 0);
        long j = 0;
        if (pointToPosition2 != -1) {
            long expandableListPosition = this.f1085a.d.getExpandableListPosition(pointToPosition2);
            int packedPositionChild = ExpandableListView.getPackedPositionChild(expandableListPosition);
            int packedPositionGroup = ExpandableListView.getPackedPositionGroup(expandableListPosition);
            if (packedPositionChild == -1) {
                View expandChildAt = this.f1085a.d.getExpandChildAt(pointToPosition2 - this.f1085a.d.getFirstVisiblePosition());
                if (expandChildAt == null) {
                    int unused2 = this.f1085a.j = 100;
                } else {
                    int unused3 = this.f1085a.j = expandChildAt.getHeight();
                }
            }
            if (this.f1085a.j != 0) {
                if (this.f1085a.h > 0 && packedPositionGroup != -1) {
                    int unused4 = this.f1085a.i = packedPositionGroup;
                    String str = (String) this.f1085a.adapter.getGroup(packedPositionGroup);
                    if (!TextUtils.isEmpty(str)) {
                        this.f1085a.f.setText(str);
                        this.f1085a.e.setVisibility(0);
                    }
                    this.f1085a.g.setSelected(this.f1085a.adapter.a(packedPositionGroup));
                    this.f1085a.g.setOnClickListener(new g(this, packedPositionGroup));
                }
                if (this.f1085a.h == 0) {
                    this.f1085a.e.setVisibility(8);
                }
                j = expandableListPosition;
            } else {
                return;
            }
        }
        if (this.f1085a.i == -1) {
            return;
        }
        if (j == 0 && (pointToPosition == 0 || pointToPosition == -1)) {
            this.f1085a.e.setVisibility(8);
            return;
        }
        int d = this.f1085a.b();
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.f1085a.e.getLayoutParams();
        marginLayoutParams.topMargin = -(this.f1085a.j - d);
        this.f1085a.e.setLayoutParams(marginLayoutParams);
    }
}
