package com.tencent.assistant.component;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.tencent.assistant.adapter.AppCategoryListAdapter;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;
import com.tencent.assistant.component.invalidater.ViewPageScrollListener;
import com.tencent.assistant.component.txscrollview.TXGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.module.az;
import com.tencent.assistant.module.callback.j;
import com.tencent.assistant.protocol.jce.AppCategory;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.business.CostTimeSTManager;
import com.tencent.assistantv2.st.k;
import java.util.List;

/* compiled from: ProGuard */
public class CategoryListView extends TXGetMoreListView {
    private final int CATEGORY_TYPE_COLOR_CARD;
    private final int CATEGORY_TYPE_COMMON;
    private final int CATEGORY_TYPE_NORMAL;
    private final int MSG_REFRESH_FROM_LOCAL;
    private final int MSG_REFRESH_FROM_NETWORK;
    private j callBack;
    private az engine;
    /* access modifiers changed from: private */
    public AppCategoryListAdapter mAdapter;
    /* access modifiers changed from: private */
    public long mCategoryId;
    /* access modifiers changed from: private */
    public CategoryApplistRefreshListener mListener;
    /* access modifiers changed from: private */
    public ViewInvalidateMessageHandler pageMessageHandler;
    private int retryCount;
    /* access modifiers changed from: private */
    public ViewPageScrollListener viewPagerlistener;

    /* compiled from: ProGuard */
    public interface CategoryApplistRefreshListener {
        void onErrorHappened(int i);

        void onNetworkLoading();

        void onNetworkNoError();

        void onNextPageLoadFailed();
    }

    public void registerListener(CategoryApplistRefreshListener categoryApplistRefreshListener) {
        this.mListener = categoryApplistRefreshListener;
    }

    public CategoryListView(Context context) {
        super(context);
        this.engine = null;
        this.mCategoryId = 0;
        this.retryCount = 3;
        this.MSG_REFRESH_FROM_NETWORK = 1;
        this.MSG_REFRESH_FROM_LOCAL = 2;
        this.CATEGORY_TYPE_COLOR_CARD = 0;
        this.CATEGORY_TYPE_COMMON = 1;
        this.CATEGORY_TYPE_NORMAL = 2;
        this.callBack = new q(this);
        this.pageMessageHandler = new r(this);
        this.engine = az.a();
        this.engine.register(this.callBack);
        initAdapter();
        setDivider(null);
    }

    public CategoryListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.engine = null;
        this.mCategoryId = 0;
        this.retryCount = 3;
        this.MSG_REFRESH_FROM_NETWORK = 1;
        this.MSG_REFRESH_FROM_LOCAL = 2;
        this.CATEGORY_TYPE_COLOR_CARD = 0;
        this.CATEGORY_TYPE_COMMON = 1;
        this.CATEGORY_TYPE_NORMAL = 2;
        this.callBack = new q(this);
        this.pageMessageHandler = new r(this);
        this.engine = az.a();
        this.engine.register(this.callBack);
        initAdapter();
        setDivider(null);
    }

    public CategoryListView(Context context, TXScrollViewBase.ScrollMode scrollMode) {
        super(context);
        this.engine = null;
        this.mCategoryId = 0;
        this.retryCount = 3;
        this.MSG_REFRESH_FROM_NETWORK = 1;
        this.MSG_REFRESH_FROM_LOCAL = 2;
        this.CATEGORY_TYPE_COLOR_CARD = 0;
        this.CATEGORY_TYPE_COMMON = 1;
        this.CATEGORY_TYPE_NORMAL = 2;
        this.callBack = new q(this);
        this.pageMessageHandler = new r(this);
        this.engine = az.a();
        this.engine.register(this.callBack);
        initAdapter();
        setDivider(null);
    }

    public void setViewPageListener(ViewPageScrollListener viewPageScrollListener) {
        this.viewPagerlistener = viewPageScrollListener;
    }

    public void initAdapter() {
        ListAdapter adapter = ((ListView) this.mScrollContentView).getAdapter();
        if (adapter instanceof HeaderViewListAdapter) {
            this.mAdapter = (AppCategoryListAdapter) ((HeaderViewListAdapter) adapter).getWrappedAdapter();
        } else {
            this.mAdapter = (AppCategoryListAdapter) ((ListView) this.mScrollContentView).getAdapter();
        }
    }

    public void onPause() {
    }

    public void onResume() {
    }

    public void recycleData() {
        super.recycleData();
        this.engine.unregister(this.callBack);
    }

    public void refreshData() {
        if (this.mAdapter == null) {
            initAdapter();
        }
        if (this.mAdapter == null || this.mAdapter.getCount() <= 0) {
            this.engine.c();
            return;
        }
        this.viewPagerlistener.sendMessage(new ViewInvalidateMessage(2, null, this.pageMessageHandler));
    }

    /* access modifiers changed from: private */
    public void onCategoryListLoadFinishedHandler(int i, int i2, List<ColorCardItem> list, List<AppCategory> list2, List<AppCategory> list3) {
        if (i2 == 0) {
            this.mListener.onNetworkNoError();
            if (!(this.mAdapter == null || list3 == null)) {
                this.mAdapter.a(list, list2, list3);
                k.a((int) STConst.ST_PAGE_SOFTWARE_CATEGORY, CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
            }
        } else {
            k.a((int) STConst.ST_PAGE_SOFTWARE_CATEGORY, CostTimeSTManager.TIMETYPE.CANCEL, System.currentTimeMillis());
            if (-800 == i2) {
                this.mListener.onErrorHappened(30);
                return;
            } else if (this.retryCount <= 0) {
                this.mListener.onErrorHappened(20);
                return;
            } else {
                this.engine.e();
                this.retryCount--;
            }
        }
        if (this.mAdapter == null) {
            initAdapter();
        }
        if (this.mAdapter != null) {
            this.mAdapter.notifyDataSetChanged();
        }
    }

    public void setCategoryType(long j) {
        this.mCategoryId = j;
    }

    public ListView getListView() {
        return (ListView) this.mScrollContentView;
    }
}
