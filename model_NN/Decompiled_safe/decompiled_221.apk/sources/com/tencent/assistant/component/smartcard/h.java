package com.tencent.assistant.component.smartcard;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.link.b;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class h extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1147a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ NormalSmartCardQianghaoItem c;

    h(NormalSmartCardQianghaoItem normalSmartCardQianghaoItem, String str, STInfoV2 sTInfoV2) {
        this.c = normalSmartCardQianghaoItem;
        this.f1147a = str;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        b.a(this.c.getContext(), this.f1147a);
    }

    public STInfoV2 getStInfo() {
        if (!(this.b == null || this.c.f == null)) {
            this.b.updateStatusToDetail(this.c.f.b);
        }
        return this.b;
    }
}
