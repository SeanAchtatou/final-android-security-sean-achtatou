package com.tencent.assistant.component;

import com.tencent.assistant.activity.ShareBaseActivity;
import com.tencent.assistant.g.a;

/* compiled from: ProGuard */
class dl extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ShareAppDialog f1021a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    dl(ShareAppDialog shareAppDialog, ShareBaseActivity shareBaseActivity, ShareAppBar shareAppBar) {
        super(shareBaseActivity, shareAppBar);
        this.f1021a = shareAppDialog;
    }

    public void shareToWX() {
        super.shareToWX();
        this.f1021a.dismiss();
    }

    public void shareToTimeLine() {
        super.shareToTimeLine();
        this.f1021a.dismiss();
    }

    public void shareToQZ() {
        super.shareToQZ();
        this.f1021a.dismiss();
    }

    public void shareToQQ() {
        super.shareToQQ();
        this.f1021a.dismiss();
    }
}
