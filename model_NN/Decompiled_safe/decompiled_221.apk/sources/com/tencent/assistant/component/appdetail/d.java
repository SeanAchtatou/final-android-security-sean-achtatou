package com.tencent.assistant.component.appdetail;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.tencent.assistant.js.JsBridge;
import com.tencent.assistant.link.b;
import com.tencent.assistant.net.c;
import com.tencent.assistant.plugin.PluginActivity;

/* compiled from: ProGuard */
class d extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppBarTabView f913a;

    private d(AppBarTabView appBarTabView) {
        this.f913a = appBarTabView;
    }

    /* synthetic */ d(AppBarTabView appBarTabView, a aVar) {
        this(appBarTabView);
    }

    public void onPageFinished(WebView webView, String str) {
        this.f913a.f.setVisibility(8);
        this.f913a.h.doPageLoadFinished();
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        this.f913a.f.setVisibility(0);
        if (str.startsWith("http") || str.startsWith("https")) {
            this.f913a.h.loadAuthorization(str);
        }
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        if (str.startsWith("http") || str.startsWith("https")) {
            return super.shouldOverrideUrlLoading(webView, str);
        }
        if (str.startsWith(JsBridge.JS_BRIDGE_SCHEME)) {
            this.f913a.h.invoke(str);
            return true;
        } else if (!str.equals("about:blank;") && !str.equals("about:blank")) {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
            if (!b.a(webView.getContext(), intent) || intent.getScheme() == null) {
                return false;
            }
            if (intent.getScheme().equals("tmast")) {
                Bundle bundle = new Bundle();
                bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.f913a.c.f());
                b.b(this.f913a.c, str, bundle);
                return true;
            }
            intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.f913a.c.f());
            this.f913a.c.startActivity(intent);
            return true;
        } else if (Build.VERSION.SDK_INT >= 11) {
            return false;
        } else {
            return true;
        }
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        super.onReceivedError(webView, i, str, str2);
        this.f913a.d();
        if (!c.a()) {
            this.f913a.a(3);
        } else {
            this.f913a.a(2);
        }
    }
}
