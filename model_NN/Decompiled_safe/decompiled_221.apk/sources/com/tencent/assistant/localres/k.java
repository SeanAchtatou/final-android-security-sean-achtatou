package com.tencent.assistant.localres;

import android.content.pm.IPackageStatsObserver;
import android.content.pm.PackageStats;
import com.tencent.assistant.localres.model.LocalApkInfo;

/* compiled from: ProGuard */
class k extends IPackageStatsObserver.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ApkResourceManager f1435a;

    k(ApkResourceManager apkResourceManager) {
        this.f1435a = apkResourceManager;
    }

    public void onGetStatsCompleted(PackageStats packageStats, boolean z) {
        LocalApkInfo localApkInfo = (LocalApkInfo) this.f1435a.h.get(packageStats.packageName);
        if (localApkInfo != null) {
            localApkInfo.occupySize = packageStats.codeSize + packageStats.dataSize;
            this.f1435a.a(localApkInfo);
        }
    }
}
