package com.tencent.assistant.localres.localapk.loadapkservice;

import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
public class NativeExceptionHandler {
    public static native int init();

    static {
        try {
            System.loadLibrary("nativeexceptionhandler");
            XLog.d("NativeExceptionHandler", "load library nativeexceptionhandler success");
        } catch (Throwable th) {
            XLog.e("NativeExceptionHandler", "load library nativeexceptionhandler failed");
            th.printStackTrace();
        }
    }

    public static void crashReport() {
        XLog.d("NativeExceptionHandler", "Java crashReport was called");
        GetApkInfoService.a().b();
        XLog.d("NativeExceptionHandler", "Java crashReport DONE.");
    }
}
