package com.tencent.assistant.localres.localapk.loadapkservice;

import android.app.Service;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.table.m;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bj;
import com.tencent.assistant.utils.e;
import com.tencent.connect.common.Constants;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public class GetApkInfoService extends Service {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f1446a = GetApkInfoService.class.getSimpleName();
    private static GetApkInfoService b;
    private Messenger c = null;
    private Map<String, LocalApkInfo> d = Collections.synchronizedMap(new HashMap());
    private m e;
    private String f;
    private String[] g = {"apk", "APK"};

    public IBinder onBind(Intent intent) {
        XLog.d(f1446a, "onBind");
        return this.c.getBinder();
    }

    public void onCreate() {
        super.onCreate();
        b = this;
        d();
        XLog.d(f1446a, "onCreate");
    }

    public void onDestroy() {
        XLog.d(f1446a, "onDestroy");
    }

    public static GetApkInfoService a() {
        return b;
    }

    /* access modifiers changed from: private */
    public void a(Messenger messenger, int i, int i2, Bundle bundle) {
        Message obtain = Message.obtain(null, i, i2, 0);
        obtain.setData(bundle);
        XLog.d(f1446a, "replyMsgToClient msg.what: " + i + " requestId: " + i2);
        try {
            messenger.send(obtain);
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }

    private void a(int i, Message message) {
        Bundle bundle = new Bundle();
        bundle.putInt("result", i);
        a(message.replyTo, 3, message.arg1, bundle);
    }

    private void a(LocalApkInfo localApkInfo, Message message) {
        a(message.replyTo, 2, message.arg1, b(localApkInfo));
    }

    private void a(int i, String str, LocalApkInfo localApkInfo, Message message) {
        Bundle bundle;
        if (localApkInfo != null) {
            bundle = b(localApkInfo);
            bundle.putInt("result", 0);
        } else {
            bundle = new Bundle();
            bundle.putInt("result", -1);
        }
        bundle.putInt("actionid", i);
        bundle.putString("apkpath", str);
        a(message.replyTo, 4, message.arg1, bundle);
    }

    private String a(LocalApkInfo localApkInfo) {
        if (TextUtils.isEmpty(localApkInfo.mLocalFilePath)) {
            return null;
        }
        return localApkInfo.mLocalFilePath + localApkInfo.occupySize + localApkInfo.mLastModified;
    }

    private String a(String str, long j, long j2) {
        return str + j + j2;
    }

    private void d() {
        NativeExceptionHandler.init();
        a.a().a(getApplicationContext());
        this.e = new m();
        for (LocalApkInfo next : this.e.a()) {
            this.d.put(a(next), next);
        }
        HandlerThread handlerThread = new HandlerThread("getapkinfo");
        handlerThread.start();
        this.c = new Messenger(new b(handlerThread.getLooper(), a()));
        XLog.d(f1446a, "init success.");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.localres.localapk.loadapkservice.GetApkInfoService.a(java.util.List<java.lang.String>, boolean, android.os.Message, java.util.Map<java.lang.String, com.tencent.assistant.localres.model.LocalApkInfo>):int
     arg types: [java.util.ArrayList<java.lang.String>, int, android.os.Message, java.util.Map]
     candidates:
      com.tencent.assistant.localres.localapk.loadapkservice.GetApkInfoService.a(int, java.lang.String, com.tencent.assistant.localres.model.LocalApkInfo, android.os.Message):void
      com.tencent.assistant.localres.localapk.loadapkservice.GetApkInfoService.a(android.os.Messenger, int, int, android.os.Bundle):void
      com.tencent.assistant.localres.localapk.loadapkservice.GetApkInfoService.a(java.util.List<java.lang.String>, boolean, android.os.Message, java.util.Map<java.lang.String, com.tencent.assistant.localres.model.LocalApkInfo>):int */
    /* access modifiers changed from: private */
    public void a(Message message) {
        Map synchronizedMap = Collections.synchronizedMap(new HashMap());
        ArrayList<String> stringArrayList = message.getData().getStringArrayList("indirs");
        ArrayList<String> stringArrayList2 = message.getData().getStringArrayList("exdirs");
        a((List<String>) stringArrayList, true, message, (Map<String, LocalApkInfo>) synchronizedMap);
        a((List<String>) stringArrayList2, false, message, (Map<String, LocalApkInfo>) synchronizedMap);
        if (!synchronizedMap.isEmpty()) {
            a(0, message);
        } else {
            a(-1, message);
        }
        if (stringArrayList != null && !stringArrayList.isEmpty()) {
            Iterator<Map.Entry<String, LocalApkInfo>> it = this.d.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry next = it.next();
                String str = (String) next.getKey();
                LocalApkInfo localApkInfo = (LocalApkInfo) next.getValue();
                if (localApkInfo.mIsInternalDownload && !synchronizedMap.containsKey(str)) {
                    XLog.d(f1446a, "remove internal apk " + str);
                    it.remove();
                    this.e.a(localApkInfo.mLocalFilePath);
                }
            }
        }
        if (stringArrayList2 != null && !stringArrayList2.isEmpty()) {
            Iterator<Map.Entry<String, LocalApkInfo>> it2 = this.d.entrySet().iterator();
            while (it2.hasNext()) {
                Map.Entry next2 = it2.next();
                String str2 = (String) next2.getKey();
                LocalApkInfo localApkInfo2 = (LocalApkInfo) next2.getValue();
                if (!localApkInfo2.mIsInternalDownload && !synchronizedMap.containsKey(str2)) {
                    XLog.d(f1446a, "remove external apk " + str2);
                    it2.remove();
                    this.e.a(localApkInfo2.mLocalFilePath);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(Message message) {
        String string = message.getData().getString("apkpath");
        int i = message.getData().getInt("actionid");
        boolean z = message.getData().getBoolean("isinternal");
        XLog.d(f1446a, "getApkInfo processing apk: " + string);
        long[] a2 = a(string);
        LocalApkInfo localApkInfo = this.d.get(a(string, a2[0], a2[1]));
        if (localApkInfo == null && (localApkInfo = b(string)) != null) {
            localApkInfo.occupySize = a2[0];
            localApkInfo.mLastModified = a2[1];
            localApkInfo.mIsInternalDownload = z;
        }
        a(i, string, localApkInfo, message);
    }

    private int a(List<String> list, boolean z, Message message, Map<String, LocalApkInfo> map) {
        List<String> a2 = a(list);
        if (a2 == null || a2.isEmpty()) {
            return 0;
        }
        int i = 0;
        for (String next : a2) {
            XLog.d(f1446a, "buildLocalApkInfo, processing " + (z ? "internal" : "external") + " apk: " + next);
            long[] a3 = a(next);
            String a4 = a(next, a3[0], a3[1]);
            LocalApkInfo localApkInfo = this.d.get(a4);
            if (localApkInfo != null) {
                i++;
                map.put(a4, localApkInfo);
                a(localApkInfo, message);
            } else {
                this.f = next;
                LocalApkInfo b2 = b(next);
                this.f = null;
                if (b2 != null) {
                    b2.occupySize = a3[0];
                    b2.mLastModified = a3[1];
                    b2.mIsInternalDownload = z;
                    map.put(a4, b2);
                    this.d.put(a4, b2);
                    XLog.d(f1446a, "buildLocalApkInfo, insert apk " + next);
                    this.e.a(b2);
                    i++;
                    a(b2, message);
                } else if (z) {
                    File file = new File(next);
                    if (file.exists()) {
                        file.delete();
                    }
                }
            }
            i = i;
        }
        return i;
    }

    private List<String> a(List<String> list) {
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        try {
            for (String scanFile : list) {
                List<String> scanFile2 = FileUtil.scanFile(scanFile, Arrays.asList(this.g));
                if (scanFile2 != null && !scanFile2.isEmpty()) {
                    arrayList.addAll(scanFile2);
                }
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return arrayList;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x003c A[SYNTHETIC, Splitter:B:20:0x003c] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0049 A[SYNTHETIC, Splitter:B:27:0x0049] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private long[] a(java.lang.String r8) {
        /*
            r7 = this;
            r0 = 2
            long[] r5 = new long[r0]
            r5 = {0, 0} // fill-array
            java.io.File r6 = new java.io.File
            r6.<init>(r8)
            long r0 = r6.length()
            r2 = 0
            int r2 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r2 > 0) goto L_0x0025
            r4 = 0
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0035, all -> 0x0045 }
            r3.<init>(r6)     // Catch:{ Exception -> 0x0035, all -> 0x0045 }
            int r0 = r3.available()     // Catch:{ Exception -> 0x0054 }
            long r0 = (long) r0
            if (r3 == 0) goto L_0x0025
            r3.close()     // Catch:{ IOException -> 0x0030 }
        L_0x0025:
            r2 = 0
            r5[r2] = r0
            r0 = 1
            long r1 = r6.lastModified()
            r5[r0] = r1
            return r5
        L_0x0030:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0025
        L_0x0035:
            r2 = move-exception
            r3 = r4
        L_0x0037:
            r2.printStackTrace()     // Catch:{ all -> 0x0052 }
            if (r3 == 0) goto L_0x0025
            r3.close()     // Catch:{ IOException -> 0x0040 }
            goto L_0x0025
        L_0x0040:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0025
        L_0x0045:
            r0 = move-exception
            r3 = r4
        L_0x0047:
            if (r3 == 0) goto L_0x004c
            r3.close()     // Catch:{ IOException -> 0x004d }
        L_0x004c:
            throw r0
        L_0x004d:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x004c
        L_0x0052:
            r0 = move-exception
            goto L_0x0047
        L_0x0054:
            r2 = move-exception
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.localres.localapk.loadapkservice.GetApkInfoService.a(java.lang.String):long[]");
    }

    private LocalApkInfo b(String str) {
        PackageManager packageManager = AstApp.i().getBaseContext().getPackageManager();
        try {
            PackageInfo packageArchiveInfo = packageManager.getPackageArchiveInfo(str, 0);
            if (packageArchiveInfo == null) {
                return null;
            }
            LocalApkInfo localApkInfo = new LocalApkInfo();
            ApplicationInfo applicationInfo = packageArchiveInfo.applicationInfo;
            applicationInfo.sourceDir = str;
            applicationInfo.publicSourceDir = str;
            localApkInfo.mAppIconRes = applicationInfo.icon;
            localApkInfo.mPackageName = applicationInfo.packageName;
            localApkInfo.mVersionCode = packageArchiveInfo.versionCode;
            localApkInfo.mVersionName = packageArchiveInfo.versionName;
            localApkInfo.mIsSelect = false;
            localApkInfo.mLocalFilePath = str;
            if (packageArchiveInfo.signatures == null || packageArchiveInfo.signatures.length <= 0) {
                localApkInfo.signature = Constants.STR_EMPTY;
            } else {
                localApkInfo.signature = bj.b(packageArchiveInfo.signatures[packageArchiveInfo.signatures.length - 1].toCharsString());
            }
            localApkInfo.mGrayVersionCode = e.b(packageManager, str);
            CharSequence applicationLabel = packageManager.getApplicationLabel(applicationInfo);
            localApkInfo.mAppName = applicationLabel == null ? "未知" : applicationLabel.toString();
            return localApkInfo;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    private Bundle b(LocalApkInfo localApkInfo) {
        Bundle bundle = new Bundle();
        bundle.putInt("iconres", localApkInfo.mAppIconRes);
        bundle.putString("pkgname", localApkInfo.mPackageName);
        bundle.putInt("vercode", localApkInfo.mVersionCode);
        bundle.putString("vername", localApkInfo.mVersionName);
        bundle.putString("filepath", localApkInfo.mLocalFilePath);
        bundle.putString("signature", localApkInfo.signature);
        bundle.putInt("grayvercode", localApkInfo.mGrayVersionCode);
        bundle.putString("appname", localApkInfo.mAppName);
        bundle.putLong("occupysize", localApkInfo.occupySize);
        bundle.putLong("lastmodified", localApkInfo.mLastModified);
        bundle.putBoolean("isinternal", localApkInfo.mIsInternalDownload);
        return bundle;
    }

    public static LocalApkInfo a(Bundle bundle) {
        LocalApkInfo localApkInfo = new LocalApkInfo();
        try {
            localApkInfo.mAppIconRes = bundle.getInt("iconres");
        } catch (Throwable th) {
            th.printStackTrace();
        }
        localApkInfo.mPackageName = bundle.getString("pkgname");
        localApkInfo.mVersionCode = bundle.getInt("vercode");
        localApkInfo.mVersionName = bundle.getString("vername");
        localApkInfo.mLocalFilePath = bundle.getString("filepath");
        localApkInfo.signature = bundle.getString("signature");
        localApkInfo.mGrayVersionCode = bundle.getInt("grayvercode");
        localApkInfo.mAppName = bundle.getString("appname");
        localApkInfo.occupySize = bundle.getLong("occupysize");
        localApkInfo.mLastModified = bundle.getLong("lastmodified");
        localApkInfo.mIsInternalDownload = bundle.getBoolean("isinternal");
        return localApkInfo;
    }

    public void b() {
        XLog.d(f1446a, "Bad apk found, delete " + this.f);
        if (!TextUtils.isEmpty(this.f)) {
            File file = new File(this.f);
            if (file.exists()) {
                file.delete();
            }
            e.b(this.f.substring(Environment.getExternalStorageDirectory().getAbsolutePath().length(), this.f.lastIndexOf("/")), this.f.substring(this.f.lastIndexOf("/") + 1));
        }
    }
}
