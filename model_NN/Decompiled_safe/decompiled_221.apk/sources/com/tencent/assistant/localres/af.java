package com.tencent.assistant.localres;

import com.tencent.assistant.localres.callback.b;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.module.callback.CallbackHelper;
import java.util.List;

/* compiled from: ProGuard */
class af implements CallbackHelper.Caller<b> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ List f1418a;
    final /* synthetic */ LocalApkInfo b;
    final /* synthetic */ boolean c;
    final /* synthetic */ aa d;

    af(aa aaVar, List list, LocalApkInfo localApkInfo, boolean z) {
        this.d = aaVar;
        this.f1418a = list;
        this.b = localApkInfo;
        this.c = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.localres.callback.b.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, com.tencent.assistant.localres.model.LocalApkInfo, boolean, boolean):void
     arg types: [java.util.List, com.tencent.assistant.localres.model.LocalApkInfo, boolean, int]
     candidates:
      com.tencent.assistant.localres.callback.b.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, boolean, boolean, int):void
      com.tencent.assistant.localres.callback.b.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, com.tencent.assistant.localres.model.LocalApkInfo, boolean, boolean):void */
    /* renamed from: a */
    public void call(b bVar) {
        bVar.a((List<LocalApkInfo>) this.f1418a, this.b, this.c, true);
    }
}
