package com.tencent.assistant.localres;

import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
class c extends com.tencent.assistant.localres.callback.c {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ApkResourceManager f1427a;

    c(ApkResourceManager apkResourceManager) {
        this.f1427a = apkResourceManager;
    }

    public void a(List<LocalApkInfo> list, LocalApkInfo localApkInfo, boolean z, boolean z2) {
        if (z) {
            Iterator it = this.f1427a.e.iterator();
            while (it.hasNext()) {
                ApkResCallback apkResCallback = (ApkResCallback) ((WeakReference) it.next()).get();
                if (apkResCallback != null) {
                    apkResCallback.onDeleteApkSuccess(localApkInfo);
                }
            }
        }
    }
}
