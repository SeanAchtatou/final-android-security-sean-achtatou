package com.tencent.assistant.localres;

import com.tencent.assistant.localres.LocalMediaLoader;
import java.lang.ref.WeakReference;
import java.util.Iterator;

/* compiled from: ProGuard */
class am implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ LocalMediaLoader f1425a;

    am(LocalMediaLoader localMediaLoader) {
        this.f1425a = localMediaLoader;
    }

    public void run() {
        synchronized (this.f1425a.e) {
            Iterator<WeakReference<LocalMediaLoader.ILocalMediaLoaderListener<T>>> it = this.f1425a.e.iterator();
            while (it.hasNext()) {
                LocalMediaLoader.ILocalMediaLoaderListener iLocalMediaLoaderListener = (LocalMediaLoader.ILocalMediaLoaderListener) it.next().get();
                if (iLocalMediaLoaderListener != null) {
                    iLocalMediaLoaderListener.onLocalMediaLoaderOver();
                }
            }
        }
    }
}
