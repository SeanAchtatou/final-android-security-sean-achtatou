package com.tencent.assistant.localres.localapk;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.tencent.assistant.localres.localapk.loadapkservice.GetApkInfoService;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.XLog;
import java.lang.ref.WeakReference;

/* compiled from: ProGuard */
class i extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private WeakReference<a> f1445a;

    i(Looper looper, a aVar) {
        super(looper);
        this.f1445a = new WeakReference<>(aVar);
    }

    public void handleMessage(Message message) {
        XLog.d(a.c, "[LocalApkLoader] handleMessage what.msg: " + message.what);
        a aVar = this.f1445a.get();
        if (aVar != null) {
            switch (message.what) {
                case 1:
                    XLog.d(a.c, "reply msg: " + message.getData().getString("s"));
                    return;
                case 2:
                    if (message.arg1 == aVar.f) {
                        LocalApkInfo a2 = GetApkInfoService.a(message.getData());
                        String a3 = aVar.a(a2.mPackageName, a2.mVersionCode, a2.mGrayVersionCode);
                        aVar.d(a2);
                        if (a2.mIsInternalDownload) {
                            aVar.d.put(a3, a2);
                        } else {
                            aVar.e.put(a3, a2);
                        }
                        if (aVar.f1437a) {
                            aVar.e(a2);
                            return;
                        }
                        return;
                    }
                    return;
                case 3:
                    if (message.arg1 == aVar.f) {
                        int i = message.getData().getInt("result");
                        if (aVar.f1437a) {
                            aVar.f1437a = false;
                            aVar.d(i);
                            return;
                        }
                        return;
                    }
                    return;
                case 4:
                    int i2 = message.getData().getInt("actionid");
                    String string = message.getData().getString("apkpath");
                    LocalApkInfo localApkInfo = null;
                    if (message.getData().getInt("result") == 0) {
                        localApkInfo = GetApkInfoService.a(message.getData());
                    }
                    aVar.a(i2, string, localApkInfo);
                    return;
                default:
                    super.handleMessage(message);
                    return;
            }
        }
    }
}
