package com.tencent.assistant.localres;

import com.qq.AppService.AstApp;
import com.tencent.assistant.db.table.n;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.e;
import java.util.List;

/* compiled from: ProGuard */
class i implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ long f1433a;
    final /* synthetic */ boolean b;
    final /* synthetic */ ApkResourceManager c;

    i(ApkResourceManager apkResourceManager, long j, boolean z) {
        this.c = apkResourceManager;
        this.f1433a = j;
        this.b = z;
    }

    public void run() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        List<String> b2 = e.b(AstApp.i());
        for (String str : b2) {
            LocalApkInfo localApkInfo = (LocalApkInfo) this.c.h.get(str);
            if (localApkInfo != null) {
                localApkInfo.mLastLaunchTime = this.f1433a;
                localApkInfo.mFakeLastLaunchTime = this.f1433a;
            }
        }
        this.c.j();
        if (this.c.c == null) {
            n unused = this.c.c = new n(AstApp.i());
        }
        this.c.c.a(b2, this.f1433a);
        if (this.b) {
            this.c.h();
        }
    }
}
