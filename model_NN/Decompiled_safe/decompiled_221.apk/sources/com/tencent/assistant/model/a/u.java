package com.tencent.assistant.model.a;

import com.tencent.assistant.protocol.jce.SmartCardTemplate;
import com.tencent.assistant.protocol.jce.SmartCardTemplateItem;
import com.tencent.assistant.utils.XLog;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class u extends i {

    /* renamed from: a  reason: collision with root package name */
    public List<t> f1654a;
    private boolean b = true;
    private int c;
    private String d;

    public boolean a(int i, int i2, SmartCardTemplate smartCardTemplate) {
        this.i = i;
        this.k = smartCardTemplate.f2335a;
        this.m = smartCardTemplate.d;
        this.o = smartCardTemplate.e;
        this.n = smartCardTemplate.f;
        this.c = smartCardTemplate.g;
        this.d = smartCardTemplate.h;
        this.b = smartCardTemplate.i;
        this.j = smartCardTemplate.j;
        if (this.f1654a == null) {
            this.f1654a = new ArrayList();
        } else {
            this.f1654a.clear();
        }
        if (smartCardTemplate.b == null || smartCardTemplate.b.size() <= 0) {
            XLog.d("SmartCard", "smartCardTemplate.items is null or zero.");
            return false;
        } else if (i2 == 2 && smartCardTemplate.a().size() < 2) {
            XLog.d("SmartCard", "template type is MATRIX BUT size is:" + smartCardTemplate.a().size());
            return false;
        } else if (i2 != 3 || smartCardTemplate.a().size() >= 2) {
            Iterator<SmartCardTemplateItem> it = smartCardTemplate.b.iterator();
            while (it.hasNext()) {
                SmartCardTemplateItem next = it.next();
                t tVar = new t();
                tVar.b = next.b;
                tVar.c = next.c;
                tVar.f1653a = com.tencent.assistant.module.u.a(next.f2337a);
                if (tVar.f1653a != null) {
                    com.tencent.assistant.module.u.a(tVar.f1653a);
                    if (next.f2337a != null) {
                        if (next.f2337a.h != null) {
                            int length = next.f2337a.h.length;
                        }
                        if (next.f2337a.h != null) {
                            new String(next.f2337a.h);
                        }
                    }
                }
                this.f1654a.add(tVar);
            }
            return true;
        } else {
            XLog.d("SmartCard", "template type is ONE_LINE BUT size is:" + smartCardTemplate.a().size());
            return false;
        }
    }

    public List<t> a() {
        return this.f1654a;
    }

    public boolean b() {
        return this.b;
    }

    public int c() {
        return this.c;
    }

    public List<Long> h() {
        ArrayList arrayList = new ArrayList();
        if (this.f1654a != null && this.f1654a.size() > 0) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.f1654a.size()) {
                    break;
                }
                arrayList.add(Long.valueOf(this.f1654a.get(i2).f1653a.f1634a));
                i = i2 + 1;
            }
        }
        return arrayList;
    }

    public String d() {
        return i() + "_" + c();
    }

    public void a(List<Long> list) {
        if (list != null && list.size() != 0 && this.f1654a != null && this.f1654a.size() != 0) {
            ArrayList arrayList = new ArrayList();
            for (t next : this.f1654a) {
                if (list.contains(Long.valueOf(next.f1653a.f1634a))) {
                    arrayList.add(next);
                }
            }
            this.f1654a.removeAll(arrayList);
        }
    }
}
