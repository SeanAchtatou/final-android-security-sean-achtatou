package com.tencent.assistant.model;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.tencent.assistant.b.a;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class ShareAppModel extends ShareModel {
    public static final Parcelable.Creator<ShareAppModel> CREATOR = new l();

    /* renamed from: a  reason: collision with root package name */
    public String f1632a;
    public String b;
    public Bundle c;
    public int d;
    public long e;
    public String f;
    public String g;
    public double h;
    public long i;
    public long j;
    public String k;
    public String l;
    public String m;

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeString(this.f1632a);
        parcel.writeString(this.b);
        parcel.writeBundle(this.c);
        parcel.writeInt(this.d);
        parcel.writeLong(this.e);
        parcel.writeString(this.f);
        parcel.writeString(this.g);
        parcel.writeDouble(this.h);
        parcel.writeLong(this.i);
        parcel.writeLong(this.j);
        parcel.writeString(this.k);
        parcel.writeString(this.l);
        parcel.writeString(this.m);
    }

    public static ShareAppModel a(SimpleAppModel simpleAppModel) {
        ShareAppModel shareAppModel = new ShareAppModel();
        shareAppModel.e = simpleAppModel.f1634a;
        shareAppModel.g = simpleAppModel.d;
        shareAppModel.h = simpleAppModel.q;
        shareAppModel.i = simpleAppModel.p;
        shareAppModel.j = simpleAppModel.k;
        shareAppModel.f = simpleAppModel.e;
        shareAppModel.f1632a = Constants.STR_EMPTY;
        shareAppModel.l = simpleAppModel.ai;
        shareAppModel.m = simpleAppModel.aj;
        Bundle bundle = new Bundle();
        bundle.putString(a.c, simpleAppModel.c);
        bundle.putString(a.h, simpleAppModel.g + Constants.STR_EMPTY);
        bundle.putString(a.C, simpleAppModel.ac);
        shareAppModel.c = bundle;
        return shareAppModel;
    }
}
