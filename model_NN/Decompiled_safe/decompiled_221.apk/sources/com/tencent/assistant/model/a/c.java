package com.tencent.assistant.model.a;

import android.net.Uri;
import android.text.TextUtils;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.u;
import com.tencent.assistant.protocol.jce.SmartCardReservation;
import com.tencent.assistant.utils.ct;

/* compiled from: ProGuard */
public class c extends i {

    /* renamed from: a  reason: collision with root package name */
    public SimpleAppModel f1637a;
    public String b;
    public boolean c;
    public int d;
    public int e;
    public int f;
    public long g;
    public int h = 0;

    public void a(SmartCardReservation smartCardReservation, int i) {
        b(smartCardReservation, i);
    }

    private void b(SmartCardReservation smartCardReservation, int i) {
        this.i = i;
        if (smartCardReservation != null) {
            this.k = smartCardReservation.f2331a;
            this.f1637a = u.a(smartCardReservation.b);
            this.b = smartCardReservation.c;
            this.e = smartCardReservation.f;
            this.f = smartCardReservation.g;
            this.c = smartCardReservation.d;
            this.d = smartCardReservation.e;
            this.n = smartCardReservation.h;
            this.g = a(smartCardReservation.h);
            this.h = smartCardReservation.i;
        }
    }

    private long a(String str) {
        if (TextUtils.isEmpty(str)) {
            return 0;
        }
        this.g = ct.c(Uri.parse(str).getQueryParameter("subscriptionId"));
        return this.g;
    }
}
