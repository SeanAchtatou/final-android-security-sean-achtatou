package com.tencent.assistant.model.a;

import com.tencent.assistant.manager.bd;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.u;
import com.tencent.assistant.protocol.jce.SmartCardGrabNumber;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class n extends i {

    /* renamed from: a  reason: collision with root package name */
    public String f1647a;
    public SimpleAppModel b;
    public int c;
    public String d;
    public boolean e;
    public int f;
    public int g;
    public int h;
    public String u;
    public boolean v = false;

    public void a(SmartCardGrabNumber smartCardGrabNumber, int i) {
        b(smartCardGrabNumber, i);
    }

    private void b(SmartCardGrabNumber smartCardGrabNumber, int i) {
        this.i = i;
        if (smartCardGrabNumber != null) {
            this.f1647a = smartCardGrabNumber.f2319a;
            this.b = u.a(smartCardGrabNumber.b);
            this.c = smartCardGrabNumber.c;
            this.d = smartCardGrabNumber.d;
            this.g = smartCardGrabNumber.g;
            this.h = smartCardGrabNumber.h;
            this.e = smartCardGrabNumber.e;
            this.f = smartCardGrabNumber.f;
            this.u = smartCardGrabNumber.i;
            bd.a().a(Long.valueOf(this.b.f1634a), this.c);
        }
    }

    public List<Long> h() {
        ArrayList arrayList = new ArrayList();
        if (this.b != null) {
            arrayList.add(Long.valueOf(this.b.f1634a));
        }
        return arrayList;
    }

    public void a(List<Long> list) {
        if (list != null && list.size() != 0 && this.b != null && list.contains(Long.valueOf(this.b.f1634a))) {
            this.v = true;
        }
    }
}
