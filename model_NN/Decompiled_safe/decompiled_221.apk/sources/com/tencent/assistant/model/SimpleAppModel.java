package com.tencent.assistant.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.Html;
import android.text.TextUtils;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.module.u;
import com.tencent.assistant.protocol.jce.ActionUrl;
import com.tencent.assistant.protocol.jce.ApkInfo;
import com.tencent.assistant.protocol.jce.AppDetailEx;
import com.tencent.assistant.protocol.jce.AppDetailWithComment;
import com.tencent.assistant.protocol.jce.RelateNews;
import com.tencent.assistant.utils.bt;
import com.tencent.assistant.utils.ct;
import com.tencent.assistant.utils.g;
import com.tencent.assistantv2.model.c;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class SimpleAppModel extends c implements Parcelable {
    public static final Parcelable.Creator<SimpleAppModel> CREATOR = new n();
    public int A;
    public long B = 0;
    public String C = null;
    public int D = 0;
    public String E = null;
    public int F = -1;
    public String G = Constants.STR_EMPTY;
    public long H = 0;
    public long I = -1;
    public String J = null;
    public long K;
    public long L = 0;
    public long M = 0;
    public long N = 0;
    public byte O = 1;
    public byte P = 0;
    public byte Q = 0;
    public long R = 0;
    public String S = "未知分类";
    public AppDetailEx T;
    public CARD_TYPE U = CARD_TYPE.NORMAL;
    public String V = Constants.STR_EMPTY;
    public int W = 0;
    public CharSequence X = Constants.STR_EMPTY;
    public String Y = Constants.STR_EMPTY;
    public String Z = Constants.STR_EMPTY;

    /* renamed from: a  reason: collision with root package name */
    public long f1634a = 0;
    public int aA;
    public byte aB = 0;
    public String aC = Constants.STR_EMPTY;
    public String aD = Constants.STR_EMPTY;
    public o aE = new o();
    public String aF = Constants.STR_EMPTY;
    public String aG = Constants.STR_EMPTY;
    public String aH = Constants.STR_EMPTY;
    public int aI = 0;
    public String aJ = Constants.STR_EMPTY;
    public String aK = Constants.STR_EMPTY;
    public ActionUrl aL = null;
    public p aM;
    public ActionUrl aa = null;
    public boolean ab = false;
    public String ac;
    public int ad = 0;
    public String ae;
    public String af;
    public byte ag;
    public int ah;
    public String ai;
    public String aj;
    public String ak = Constants.STR_EMPTY;
    public int al = 0;
    public int am = 0;
    public ArrayList<String> an;
    public ArrayList<String> ao;
    public ArrayList<String> ap;
    public h aq = null;
    public int ar = 0;
    public int as = 0;
    public int at = 0;
    public byte au = 0;
    public g av;
    public RelateNews aw;
    public String ax;
    public String ay = Constants.STR_EMPTY;
    public ActionUrl az = null;
    public long b = -99;
    public String c = null;
    public String d = null;
    public String e = null;
    public String f = null;
    public int g = -1;
    public int h = 0;
    public String i = null;
    public ArrayList<String> j;
    public long k = 0;
    public String l = null;
    public String m = null;
    public long n = -1;
    public String o;
    public long p = -1;
    public double q = -1.0d;
    public long r = 0;
    public double s = 0.0d;
    public String t = null;
    public ArrayList<String> u;
    public long v = 0;
    public String w = null;
    public boolean x = false;
    public byte[] y = null;
    public String z;

    /* compiled from: ProGuard */
    public enum CARD_TYPE {
        NORMAL,
        QUALITY,
        SIMPLE,
        UNKNOWN
    }

    public SimpleAppModel(AppDetailWithComment appDetailWithComment) {
        a(appDetailWithComment);
    }

    public SimpleAppModel(LocalApkInfo localApkInfo) {
        a(localApkInfo);
    }

    public SimpleAppModel() {
    }

    public SimpleAppModel a(AppDetailWithComment appDetailWithComment) {
        if (appDetailWithComment != null) {
            this.f1634a = appDetailWithComment.f1993a.f1989a.f1999a;
            this.c = appDetailWithComment.f1993a.f1989a.b;
            this.d = appDetailWithComment.f1993a.f1989a.c;
            this.p = appDetailWithComment.f1993a.f1989a.g;
            this.q = appDetailWithComment.f1993a.f1989a.h.b;
            if (!appDetailWithComment.f1993a.b.isEmpty()) {
                this.e = a(appDetailWithComment.f1993a.b.get(0));
                this.W = appDetailWithComment.f1993a.b.get(0).s;
                ApkInfo apkInfo = appDetailWithComment.f1993a.b.get(0);
                this.f = apkInfo.b;
                this.g = apkInfo.c;
                this.b = apkInfo.f1982a;
                this.i = apkInfo.e;
                this.j = u.a((byte) 1, apkInfo.u);
                this.k = apkInfo.f;
                this.o = apkInfo.k;
                if (!TextUtils.isEmpty(apkInfo.l)) {
                    try {
                        this.X = Html.fromHtml(apkInfo.l);
                    } catch (Exception e2) {
                        this.X = apkInfo.l;
                        e2.printStackTrace();
                    }
                }
                this.m = apkInfo.p;
                this.n = apkInfo.i;
                this.B = apkInfo.n;
                this.P = apkInfo.w;
                this.ac = apkInfo.x;
                this.ad = apkInfo.y;
                this.ar = apkInfo.A;
                this.as = apkInfo.B;
                this.at = apkInfo.C;
            }
            this.T = appDetailWithComment.f1993a.c;
            this.R = appDetailWithComment.f1993a.f1989a.d;
            this.ah = appDetailWithComment.h;
            this.ai = appDetailWithComment.i;
            this.aj = appDetailWithComment.j;
            this.aB = appDetailWithComment.f1993a.f1989a.n;
        }
        return this;
    }

    public SimpleAppModel a(LocalApkInfo localApkInfo) {
        if (localApkInfo != null) {
            this.d = localApkInfo.mAppName;
            this.c = localApkInfo.mPackageName;
            this.D = localApkInfo.mVersionCode;
            this.E = localApkInfo.mVersionName;
            this.F = localApkInfo.launchCount;
            this.G = localApkInfo.mLocalFilePath;
            this.H = localApkInfo.occupySize;
            this.I = localApkInfo.mInstallDate;
            this.J = localApkInfo.signature;
            this.K = (long) localApkInfo.flags;
            this.C = localApkInfo.mSortKey;
            this.L = localApkInfo.mLastLaunchTime;
            this.M = localApkInfo.mDataUsage;
            this.N = localApkInfo.mBatteryUsage;
            this.O = localApkInfo.mInstalleLocation;
            this.ad = localApkInfo.mGrayVersionCode;
        }
        return this;
    }

    public boolean a() {
        if (!b() || this.x || bt.a(this.v).equals(bt.a(this.k)) || !u()) {
            return false;
        }
        return true;
    }

    public boolean b() {
        return ct.b(this.u) || !TextUtils.isEmpty(this.t);
    }

    private boolean u() {
        return g.a(this.c, this.g, this.ad, this.z, this.A);
    }

    public String a(ApkInfo apkInfo) {
        if (apkInfo.d != null) {
            return apkInfo.d.f2252a;
        }
        return null;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        int i3;
        parcel.writeLong(this.f1634a);
        parcel.writeLong(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
        parcel.writeLong(this.p);
        parcel.writeDouble(this.q);
        parcel.writeString(this.f);
        parcel.writeInt(this.g);
        parcel.writeString(this.i);
        if (this.j != null) {
            parcel.writeByte((byte) 1);
            parcel.writeStringList(this.j);
        } else {
            parcel.writeByte((byte) 0);
        }
        parcel.writeLong(this.k);
        parcel.writeString(this.o);
        TextUtils.writeToParcel(this.X, parcel, 0);
        parcel.writeString(this.m);
        parcel.writeLong(this.n);
        parcel.writeString(this.t);
        if (this.u != null) {
            parcel.writeByte((byte) 1);
            parcel.writeStringList(this.u);
        } else {
            parcel.writeByte((byte) 0);
        }
        parcel.writeLong(this.v);
        parcel.writeString(this.w);
        if (this.x) {
            i3 = 1;
        } else {
            i3 = 0;
        }
        parcel.writeInt(i3);
        parcel.writeLong(this.B);
        parcel.writeString(this.C);
        parcel.writeInt(this.D);
        parcel.writeString(this.E);
        parcel.writeInt(this.F);
        parcel.writeString(this.G);
        parcel.writeLong(this.H);
        parcel.writeLong(this.I);
        parcel.writeString(this.J);
        parcel.writeLong(this.K);
        parcel.writeLong(this.L);
        parcel.writeLong(this.M);
        parcel.writeLong(this.N);
        parcel.writeByte(this.O);
        parcel.writeInt(this.ab ? 1 : 0);
        parcel.writeByte(this.P);
        parcel.writeString(this.ac);
        parcel.writeInt(this.ad);
        parcel.writeByte(this.Q);
        if (this.y == null || this.y.length <= 0) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(this.y.length);
            parcel.writeByteArray(this.y);
        }
        parcel.writeString(this.af);
        parcel.writeLong(this.R);
        parcel.writeInt(this.ah);
        parcel.writeString(this.ai);
        parcel.writeString(this.aj);
        if (this.an != null) {
            parcel.writeByte((byte) 1);
            parcel.writeStringList(this.an);
        } else {
            parcel.writeByte((byte) 0);
        }
        if (this.ao != null) {
            parcel.writeByte((byte) 1);
            parcel.writeStringList(this.ao);
        } else {
            parcel.writeByte((byte) 0);
        }
        parcel.writeInt(this.ar);
        parcel.writeInt(this.as);
        parcel.writeInt(this.at);
        parcel.writeByte(this.au);
        parcel.writeString(this.z);
        parcel.writeInt(this.A);
        parcel.writeByte(this.ag);
        parcel.writeByte(this.aB);
        parcel.writeInt(this.aA);
        parcel.writeString(this.aF);
        parcel.writeString(this.aG);
        parcel.writeString(this.aH);
        parcel.writeInt(this.aI);
        parcel.writeString(this.aJ);
        parcel.writeString(this.aK);
        if (this.ap != null) {
            parcel.writeByte((byte) 1);
            parcel.writeStringList(this.ap);
        } else {
            parcel.writeByte((byte) 0);
        }
        parcel.writeString(this.aC);
        parcel.writeString(this.aD);
        parcel.writeString(this.aE.f1668a);
        parcel.writeString(this.aE.b);
    }

    public boolean c() {
        return (((int) (this.B >> 10)) & 1) == 1;
    }

    public boolean d() {
        return (((int) (this.B >> 11)) & 1) == 1 && this.P > 0;
    }

    public boolean e() {
        return (((int) (this.B >> 12)) & 1) == 1 && this.P > 0;
    }

    public boolean f() {
        return (((int) (this.B >>> 13)) & 1) == 1;
    }

    public boolean g() {
        return (((int) (this.B >>> 14)) & 1) == 1;
    }

    public boolean h() {
        return (((int) (this.B >> 8)) & 1) == 1 || (((int) (this.B >> 9)) & 1) == 1;
    }

    public boolean i() {
        return (((int) (this.B >> 9)) & 1) == 1;
    }

    public boolean j() {
        return this.ah == 1;
    }

    public boolean k() {
        return (((int) (this.B >>> 18)) & 1) == 1;
    }

    public boolean l() {
        return (((int) (this.B >>> 19)) & 1) == 1;
    }

    public boolean m() {
        return (((int) (this.B >>> 15)) & 1) == 1;
    }

    public boolean n() {
        return (((int) (this.B >>> 16)) & 1) == 1;
    }

    public boolean o() {
        return (((int) (this.B >>> 17)) & 1) == 1;
    }

    public boolean p() {
        return this.aA == 1;
    }

    public String q() {
        DownloadInfo a2 = DownloadProxy.a().a(this.c, this.g, this.ad);
        return a2 != null ? a2.downloadTicket : String.valueOf(this.b);
    }

    public SimpleDownloadInfo.DownloadType r() {
        return SimpleDownloadInfo.DownloadType.APK;
    }

    public void s() {
        if (this.aM == null || this.aM.d != this.v) {
            if (this.aM == null) {
                this.aM = new p();
            }
            if (!TextUtils.isEmpty(this.ax)) {
                this.aM.f1669a = this.ax;
            } else {
                this.aM.f1669a = ct.a(this.p, 0);
            }
            this.aM.b = bt.a(this.k);
            this.aM.c = bt.a(this.v);
        }
    }
}
