package com.tencent.assistant.model.a;

import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.utils.ct;

/* compiled from: ProGuard */
public class t {

    /* renamed from: a  reason: collision with root package name */
    public SimpleAppModel f1653a;
    public String b;
    public String c;

    public Spanned a() {
        if (TextUtils.isEmpty(this.b)) {
            return ct.f2674a;
        }
        try {
            return Html.fromHtml(this.b);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
