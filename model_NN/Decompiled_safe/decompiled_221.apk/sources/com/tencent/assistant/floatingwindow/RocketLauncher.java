package com.tencent.assistant.floatingwindow;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Vibrator;
import android.view.LayoutInflater;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.utils.df;

/* compiled from: ProGuard */
public class RocketLauncher extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    public static int f1282a;
    public static int b;
    public static int c;
    /* access modifiers changed from: private */
    public ImageView d = ((ImageView) findViewById(R.id.iv_launcher_bottom));
    /* access modifiers changed from: private */
    public RelativeLayout e = ((RelativeLayout) findViewById(R.id.rl_launcher_cloud));
    private ImageView f = ((ImageView) findViewById(R.id.iv_launcher_cloud_01));
    private ImageView g = ((ImageView) findViewById(R.id.iv_launcher_cloud_02));
    private ImageView h = ((ImageView) findViewById(R.id.iv_launcher_tower));
    private AnimationDrawable i;
    private Animation j;
    private Vibrator k;

    public RocketLauncher(Context context) {
        super(context);
        LayoutInflater.from(context).inflate((int) R.layout.launcher, this);
        f1282a = (int) context.getResources().getDimension(R.dimen.floating_window_rocket_laucher_cloud_width);
        b = (int) context.getResources().getDimension(R.dimen.floating_window_rocket_laucher_cloud_height);
        c = (int) context.getResources().getDimension(R.dimen.floating_window_rocket_laucher_layout_height);
        this.d.setBackgroundResource(R.drawable.rocket_launcher_anim_drawable);
        this.i = (AnimationDrawable) this.d.getBackground();
        this.i.start();
        this.k = (Vibrator) context.getSystemService("vibrator");
        this.d.setVisibility(4);
        this.j = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
        this.j.setDuration(200);
        this.j.setAnimationListener(new s(this));
        this.d.startAnimation(this.j);
    }

    public void a(boolean z) {
        if (z) {
            this.k.vibrate(1000);
        } else {
            this.k.cancel();
        }
    }

    public void a() {
        this.i.stop();
        this.j.cancel();
        this.d.setVisibility(8);
        this.h.setVisibility(0);
        this.f.setVisibility(0);
        this.g.setVisibility(0);
        this.k.cancel();
        this.h.startAnimation(b());
    }

    private Animation b() {
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) (-df.a(AstApp.i(), 230.0f)));
        translateAnimation.setDuration(900);
        translateAnimation.setInterpolator(new AccelerateInterpolator());
        translateAnimation.setFillAfter(true);
        translateAnimation.setAnimationListener(new t(this));
        return translateAnimation;
    }

    /* access modifiers changed from: private */
    public Animation c() {
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(1000);
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        return alphaAnimation;
    }
}
