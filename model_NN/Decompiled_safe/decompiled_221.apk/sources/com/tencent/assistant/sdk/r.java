package com.tencent.assistant.sdk;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.login.d;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.sdk.param.a;
import com.tencent.assistant.sdk.param.jce.GetDownloadProgressResponse;
import com.tencent.assistant.sdk.param.jce.GetDownloadStateResponse;
import com.tencent.assistant.sdk.param.jce.IPCBaseParam;
import com.tencent.assistant.sdk.param.jce.IPCDownloadParam;
import com.tencent.assistant.sdk.param.jce.IPCHead;
import com.tencent.assistant.sdk.param.jce.IPCRequest;
import com.tencent.assistant.sdk.param.jce.IPCResponse;
import com.tencent.assistant.st.h;
import com.tencent.assistantv2.st.k;
import com.tencent.connect.common.Constants;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
public abstract class r {

    /* renamed from: a  reason: collision with root package name */
    protected IPCHead f2493a;
    protected IPCBaseParam b;
    protected Context c;
    public String d = Constants.STR_EMPTY;
    protected int e;
    protected List<IPCDownloadParam> f;
    protected HashMap<String, Bundle> g = new HashMap<>();
    protected String h;
    protected String i;
    protected String j;

    /* access modifiers changed from: protected */
    public abstract JceStruct a();

    /* access modifiers changed from: protected */
    public abstract void a(JceStruct jceStruct);

    /* access modifiers changed from: protected */
    public abstract boolean a(DownloadInfo downloadInfo);

    /* access modifiers changed from: protected */
    public abstract IPCBaseParam b();

    public r(Context context, IPCRequest iPCRequest) {
        this.c = context;
        this.e = 0;
        a(iPCRequest);
    }

    public r(Context context, IPCRequest iPCRequest, String str) {
        this.d = str;
        this.c = context;
        this.e = 0;
        a(iPCRequest);
    }

    private void a(IPCRequest iPCRequest) {
        if (iPCRequest != null) {
            JceStruct a2 = a.a(iPCRequest);
            this.f2493a = iPCRequest.a();
            a(a2);
        }
    }

    public byte[] c() {
        IPCResponse a2;
        JceStruct a3 = a();
        if (a3 == null || (a2 = a.a(this.f2493a, a3)) == null) {
            return null;
        }
        return a.a(a2);
    }

    public byte[] a(String str) {
        IPCResponse a2;
        d(str);
        JceStruct c2 = c(str);
        if (c2 == null || (a2 = a.a(this.f2493a, c2)) == null) {
            return null;
        }
        return a.a(a2);
    }

    /* access modifiers changed from: protected */
    public void d() {
        if (!TextUtils.isEmpty(this.i) && this.i.equals("qqNumber")) {
            String str = this.h;
            if (!TextUtils.isEmpty(str) && str.indexOf("&") != -1) {
                String a2 = com.tencent.assistant.sdk.b.a.a(str);
                String b2 = com.tencent.assistant.sdk.b.a.b(str);
                Bundle bundle = new Bundle();
                bundle.putInt(AppConst.KEY_FROM_TYPE, 5);
                bundle.putString(AppConst.KEY_QUICKLOGIN_UIN, a2);
                bundle.putString(AppConst.KEY_QUICKLOGIN_BUFFER_STR, b2);
                d.a().a(AppConst.IdentityType.MOBILEQ, bundle);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void b(String str) {
        String str2 = this.f2493a != null ? this.f2493a.e : Constants.STR_EMPTY;
        String str3 = Constants.STR_EMPTY;
        if (!(this.f2493a == null || this.b == null)) {
            str3 = h.a(this.f2493a.c, this.f2493a.d, this.b.h, this.b.g, this.b.e, this.b.b, this.b.i, this.b.d, this.b.c);
        }
        k.a(str, str2, str3);
    }

    /* access modifiers changed from: protected */
    public String e() {
        if (this.f2493a != null) {
            return this.f2493a.a();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public String f() {
        if (this.f2493a != null) {
            return this.f2493a.b();
        }
        return null;
    }

    public void a(String str, DownloadInfo downloadInfo, boolean z) {
        if (this.g == null) {
            this.g = new HashMap<>();
        }
        Bundle bundle = this.g.get(str);
        if (bundle == null) {
            bundle = new Bundle();
        }
        bundle.clear();
        bundle.putBoolean("send_progress", z);
        bundle.putInt("download_state", com.tencent.assistant.sdk.b.a.a(downloadInfo));
        if (downloadInfo.downloadState != SimpleDownloadInfo.DownloadState.DOWNLOADING || !z) {
            bundle.putInt("download_errorcode", downloadInfo.errorCode);
        } else {
            bundle.putLong("download_received_len", downloadInfo.response.f1263a);
            bundle.putLong("download_total_len", downloadInfo.response.b);
        }
        this.g.put(str, bundle);
    }

    /* access modifiers changed from: protected */
    public JceStruct c(String str) {
        Bundle bundle;
        if (this.g == null || (bundle = this.g.get(str)) == null) {
            return null;
        }
        int i2 = bundle.getInt("download_state");
        boolean z = bundle.getBoolean("send_progress");
        if (i2 != 1 || !z) {
            return f(str);
        }
        return e(str);
    }

    /* access modifiers changed from: protected */
    public void d(String str) {
        Bundle bundle;
        if (this.g != null && (bundle = this.g.get(str)) != null) {
            int i2 = bundle.getInt("download_state");
            boolean z = bundle.getBoolean("send_progress");
            if (i2 != 1 || !z) {
                this.f2493a.b = 2;
            } else {
                this.f2493a.b = 3;
            }
        }
    }

    private synchronized JceStruct e(String str) {
        GetDownloadProgressResponse getDownloadProgressResponse;
        Bundle bundle;
        getDownloadProgressResponse = new GetDownloadProgressResponse();
        IPCBaseParam b2 = b();
        if (b2 == null) {
            IPCBaseParam iPCBaseParam = new IPCBaseParam();
            iPCBaseParam.d = str;
            if (!TextUtils.isEmpty(str) && this.f != null) {
                int i2 = 0;
                while (true) {
                    int i3 = i2;
                    if (i3 >= this.f.size()) {
                        break;
                    }
                    IPCDownloadParam iPCDownloadParam = this.f.get(i3);
                    if (iPCDownloadParam != null && iPCDownloadParam.f2477a != null && str.equals(iPCDownloadParam.f2477a.d)) {
                        b2 = iPCDownloadParam.f2477a;
                        break;
                    }
                    i2 = i3 + 1;
                }
            }
            b2 = iPCBaseParam;
        }
        getDownloadProgressResponse.a(b2);
        if (!(this.g == null || (bundle = this.g.get(str)) == null)) {
            getDownloadProgressResponse.a(bundle.getLong("download_received_len"));
            getDownloadProgressResponse.b(bundle.getLong("download_total_len"));
        }
        getDownloadProgressResponse.c(DownloadProxy.a().l());
        getDownloadProgressResponse.c(DownloadProxy.a().k());
        return getDownloadProgressResponse;
    }

    private synchronized JceStruct f(String str) {
        GetDownloadStateResponse getDownloadStateResponse;
        Bundle bundle;
        getDownloadStateResponse = new GetDownloadStateResponse();
        IPCBaseParam b2 = b();
        if (b2 == null) {
            IPCBaseParam iPCBaseParam = new IPCBaseParam();
            iPCBaseParam.d = str;
            if (!TextUtils.isEmpty(str) && this.f != null) {
                int i2 = 0;
                while (true) {
                    int i3 = i2;
                    if (i3 >= this.f.size()) {
                        break;
                    }
                    IPCDownloadParam iPCDownloadParam = this.f.get(i3);
                    if (iPCDownloadParam != null && iPCDownloadParam.f2477a != null && str.equals(iPCDownloadParam.f2477a.d)) {
                        b2 = iPCDownloadParam.f2477a;
                        break;
                    }
                    i2 = i3 + 1;
                }
            }
            b2 = iPCBaseParam;
        }
        getDownloadStateResponse.a(b2);
        if (!(this.g == null || (bundle = this.g.get(str)) == null)) {
            getDownloadStateResponse.a(bundle.getInt("download_state"));
            getDownloadStateResponse.b(bundle.getInt("download_errorcode"));
        }
        return getDownloadStateResponse;
    }
}
