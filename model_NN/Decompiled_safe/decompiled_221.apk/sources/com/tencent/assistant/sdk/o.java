package com.tencent.assistant.sdk;

import android.content.Context;
import android.text.TextUtils;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.a;
import com.tencent.assistant.link.b;
import com.tencent.assistant.sdk.param.jce.IPCBaseParam;
import com.tencent.assistant.sdk.param.jce.IPCRequest;
import com.tencent.assistant.sdk.param.jce.OperateDownloadTaskRequest;

/* compiled from: ProGuard */
public class o extends r {
    private OperateDownloadTaskRequest k;

    public o(Context context, IPCRequest iPCRequest) {
        super(context, iPCRequest);
    }

    /* access modifiers changed from: protected */
    public void a(JceStruct jceStruct) {
        DownloadInfo a2;
        if (jceStruct instanceof OperateDownloadTaskRequest) {
            this.k = (OperateDownloadTaskRequest) jceStruct;
            if (this.k != null) {
                this.b = this.k.b;
                if (this.b != null) {
                    this.h = this.b.e;
                    this.i = this.b.f;
                    this.j = this.b.g;
                }
                b("StatSdkToIpc");
                d();
                if ((this.k.f2482a == 4 || this.k.f2482a == 5) && (a2 = p.a(this.k.a())) != null) {
                    a.a().b(a2.downloadTicket);
                }
                b.a(this.c, a(this.k));
            }
        }
    }

    private String a(OperateDownloadTaskRequest operateDownloadTaskRequest) {
        String a2 = a(operateDownloadTaskRequest.f2482a);
        return a2 + b(operateDownloadTaskRequest);
    }

    private String a(int i) {
        String str;
        switch (i) {
            case 1:
                str = "appdetails";
                break;
            case 2:
                str = "download";
                break;
            case 3:
                str = "appdetails";
                break;
            case 4:
                str = "appdetails";
                break;
            case 5:
                str = "download";
                break;
            default:
                str = "appdetails";
                break;
        }
        return "tpmast://" + str + "?";
    }

    private String b(OperateDownloadTaskRequest operateDownloadTaskRequest) {
        String str = com.tencent.assistant.b.a.n + "=" + this.f2493a.c + "&" + com.tencent.assistant.b.a.o + "=" + this.f2493a.d + "&" + com.tencent.assistant.b.a.i + "=" + operateDownloadTaskRequest.b.f2475a + "&" + com.tencent.assistant.b.a.f845a + "=" + operateDownloadTaskRequest.b.b + "&" + com.tencent.assistant.b.a.c + "=" + operateDownloadTaskRequest.b.d + "&" + com.tencent.assistant.b.a.h + "=" + operateDownloadTaskRequest.b.c + "&" + com.tencent.assistant.b.a.e + "=" + operateDownloadTaskRequest.c + "&" + com.tencent.assistant.b.a.j + "=" + operateDownloadTaskRequest.b.g + "&" + com.tencent.assistant.b.a.y + "=" + com.tencent.assistant.sdk.b.a.a(operateDownloadTaskRequest.b.e) + "&" + com.tencent.assistant.b.a.z + "=" + operateDownloadTaskRequest.b.f + "&" + com.tencent.assistant.b.a.C + "=" + operateDownloadTaskRequest.b.h + "&" + com.tencent.assistant.b.a.ac + "=" + this.f2493a.e;
        if (!TextUtils.isEmpty(operateDownloadTaskRequest.d)) {
            str = str + "&" + com.tencent.assistant.b.a.s + "=" + operateDownloadTaskRequest.d;
        }
        if (!TextUtils.isEmpty(operateDownloadTaskRequest.e)) {
            str = str + "&" + com.tencent.assistant.b.a.g + "=" + operateDownloadTaskRequest.e;
        }
        String str2 = str + "&" + com.tencent.assistant.b.a.u + "=" + true;
        if (!TextUtils.isEmpty(com.tencent.assistant.sdk.b.a.b(operateDownloadTaskRequest.b.e))) {
            str2 = str2 + "&" + com.tencent.assistant.b.a.v + "=" + true;
        }
        return str2 + "&" + com.tencent.assistant.b.a.ag + "=" + operateDownloadTaskRequest.f2482a;
    }

    public IPCBaseParam b() {
        if (this.k == null) {
            return null;
        }
        return this.k.a();
    }

    /* access modifiers changed from: protected */
    public JceStruct a() {
        return null;
    }

    /* access modifiers changed from: protected */
    public boolean a(DownloadInfo downloadInfo) {
        return this.b != null && !TextUtils.isEmpty(this.b.c) && this.b.c.equals(String.valueOf(downloadInfo.versionCode)) && !TextUtils.isEmpty(this.b.d) && this.b.d.equals(downloadInfo.packageName);
    }
}
