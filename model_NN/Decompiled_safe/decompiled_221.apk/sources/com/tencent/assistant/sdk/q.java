package com.tencent.assistant.sdk;

import android.os.Binder;
import com.tencent.assistant.sdk.a.b;
import com.tencent.assistant.sdk.a.d;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class q extends b {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SDKSupportService f2492a;

    q(SDKSupportService sDKSupportService) {
        this.f2492a = sDKSupportService;
    }

    public int a(String str, String str2, d dVar) {
        String str3 = Constants.STR_EMPTY;
        String[] packagesForUid = this.f2492a.getPackageManager().getPackagesForUid(Binder.getCallingUid());
        if (packagesForUid != null && packagesForUid.length > 0) {
            str3 = packagesForUid[0];
        }
        return RequestHandler.a().a(this.f2492a.a(), str3, str, str2, dVar);
    }

    public int a(d dVar) {
        return RequestHandler.a().a(dVar);
    }

    public byte[] a(String str, byte[] bArr) {
        String str2 = Constants.STR_EMPTY;
        String[] packagesForUid = this.f2492a.getPackageManager().getPackagesForUid(Binder.getCallingUid());
        if (packagesForUid != null && packagesForUid.length > 0) {
            str2 = packagesForUid[0];
        }
        return RequestHandler.a().a(this.f2492a.a(), this.f2492a, str2, str, bArr);
    }

    public void b(String str, byte[] bArr) {
        String str2 = Constants.STR_EMPTY;
        String[] packagesForUid = this.f2492a.getPackageManager().getPackagesForUid(Binder.getCallingUid());
        if (packagesForUid != null && packagesForUid.length > 0) {
            str2 = packagesForUid[0];
        }
        RequestHandler.a().b(this.f2492a.a(), this.f2492a, str2, str, bArr);
    }
}
