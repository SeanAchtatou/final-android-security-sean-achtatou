package com.tencent.assistant.uninstall;

import android.content.Context;

/* compiled from: ProGuard */
class d {
    d() {
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0033, code lost:
        if (r0 != null) goto L_0x0035;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x003d, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x003e, code lost:
        r4 = r1;
        r1 = r0;
        r0 = r4;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x002b A[SYNTHETIC, Splitter:B:18:0x002b] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0032 A[ExcHandler: Exception (e java.lang.Exception), Splitter:B:3:0x0007] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void b(android.content.Context r5, int r6) {
        /*
            java.lang.Class<com.tencent.assistant.uninstall.d> r2 = com.tencent.assistant.uninstall.d.class
            monitor-enter(r2)
            r0 = 0
            java.lang.String r1 = "watch.version"
            r3 = 0
            java.io.FileOutputStream r0 = r5.openFileOutput(r1, r3)     // Catch:{ Exception -> 0x0032, all -> 0x0025 }
            if (r0 == 0) goto L_0x001e
            java.lang.String r1 = java.lang.String.valueOf(r6)     // Catch:{ Exception -> 0x0032, all -> 0x003d }
            byte[] r1 = r1.getBytes()     // Catch:{ Exception -> 0x0032, all -> 0x003d }
            int r3 = r1.length     // Catch:{ Exception -> 0x0032, all -> 0x003d }
            if (r3 <= 0) goto L_0x001e
            r0.write(r1)     // Catch:{ Exception -> 0x0032, all -> 0x003d }
            r0.flush()     // Catch:{ Exception -> 0x0032, all -> 0x003d }
        L_0x001e:
            if (r0 == 0) goto L_0x0023
            r0.close()     // Catch:{ IOException -> 0x0039 }
        L_0x0023:
            monitor-exit(r2)
            return
        L_0x0025:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0029:
            if (r1 == 0) goto L_0x002e
            r1.close()     // Catch:{ IOException -> 0x003b }
        L_0x002e:
            throw r0     // Catch:{ all -> 0x002f }
        L_0x002f:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x0032:
            r1 = move-exception
            if (r0 == 0) goto L_0x0023
            r0.close()     // Catch:{ IOException -> 0x0039 }
            goto L_0x0023
        L_0x0039:
            r0 = move-exception
            goto L_0x0023
        L_0x003b:
            r1 = move-exception
            goto L_0x002e
        L_0x003d:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.uninstall.d.b(android.content.Context, int):void");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x002e A[SYNTHETIC, Splitter:B:18:0x002e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized int c(android.content.Context r6) {
        /*
            java.lang.Class<com.tencent.assistant.uninstall.d> r2 = com.tencent.assistant.uninstall.d.class
            monitor-enter(r2)
            r0 = 0
            java.lang.String r1 = "watch.version"
            java.io.FileInputStream r1 = r6.openFileInput(r1)     // Catch:{ Exception -> 0x0035, all -> 0x0028 }
            if (r1 == 0) goto L_0x0058
            int r0 = r1.available()     // Catch:{ Exception -> 0x0069, all -> 0x0067 }
            if (r0 <= 0) goto L_0x0058
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x0069, all -> 0x0067 }
            r1.read(r0)     // Catch:{ Exception -> 0x0069, all -> 0x0067 }
            java.lang.String r3 = "UTF-8"
            java.lang.String r0 = org.apache.http.util.EncodingUtils.getString(r0, r3)     // Catch:{ Exception -> 0x0069, all -> 0x0067 }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ Exception -> 0x0069, all -> 0x0067 }
            if (r1 == 0) goto L_0x0026
            r1.close()     // Catch:{ IOException -> 0x004f }
        L_0x0026:
            monitor-exit(r2)
            return r0
        L_0x0028:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x002c:
            if (r1 == 0) goto L_0x0031
            r1.close()     // Catch:{ IOException -> 0x0046 }
        L_0x0031:
            throw r0     // Catch:{ all -> 0x0032 }
        L_0x0032:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x0035:
            r1 = move-exception
        L_0x0036:
            if (r0 == 0) goto L_0x003b
            r0.close()     // Catch:{ IOException -> 0x003d }
        L_0x003b:
            r0 = 0
            goto L_0x0026
        L_0x003d:
            r0 = move-exception
            java.lang.String r1 = "uninstall"
            java.lang.String r3 = "<java> getVersionCode"
            com.tencent.assistant.utils.XLog.e(r1, r3, r0)     // Catch:{ all -> 0x0032 }
            goto L_0x003b
        L_0x0046:
            r1 = move-exception
            java.lang.String r3 = "uninstall"
            java.lang.String r4 = "<java> getVersionCode"
            com.tencent.assistant.utils.XLog.e(r3, r4, r1)     // Catch:{ all -> 0x0032 }
            goto L_0x0031
        L_0x004f:
            r1 = move-exception
            java.lang.String r3 = "uninstall"
            java.lang.String r4 = "<java> getVersionCode"
            com.tencent.assistant.utils.XLog.e(r3, r4, r1)     // Catch:{ all -> 0x0032 }
            goto L_0x0026
        L_0x0058:
            if (r1 == 0) goto L_0x003b
            r1.close()     // Catch:{ IOException -> 0x005e }
            goto L_0x003b
        L_0x005e:
            r0 = move-exception
            java.lang.String r1 = "uninstall"
            java.lang.String r3 = "<java> getVersionCode"
            com.tencent.assistant.utils.XLog.e(r1, r3, r0)     // Catch:{ all -> 0x0032 }
            goto L_0x003b
        L_0x0067:
            r0 = move-exception
            goto L_0x002c
        L_0x0069:
            r0 = move-exception
            r0 = r1
            goto L_0x0036
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.uninstall.d.c(android.content.Context):int");
    }

    /* access modifiers changed from: private */
    public static int d(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (Exception e) {
            return 0;
        }
    }
}
