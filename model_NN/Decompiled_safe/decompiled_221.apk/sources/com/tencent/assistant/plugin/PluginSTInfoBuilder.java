package com.tencent.assistant.plugin;

import android.content.Context;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;

/* compiled from: ProGuard */
public class PluginSTInfoBuilder extends STInfoBuilder {
    public static PluginSTInfoV2 buildSTInfo(Context context, int i) {
        return new PluginSTInfoV2(STInfoBuilder.buildSTInfo(context, i));
    }

    public static PluginSTInfoV2 buildSTInfo(Context context, SimpleAppModel simpleAppModel, String str, int i, String str2) {
        return (PluginSTInfoV2) STInfoBuilder.buildSTInfo(context, simpleAppModel, str, i, str2);
    }

    public static StatInfo buildDownloadSTInfo(Context context, SimpleAppModel simpleAppModel) {
        return STInfoBuilder.buildDownloadSTInfo(context, simpleAppModel);
    }
}
