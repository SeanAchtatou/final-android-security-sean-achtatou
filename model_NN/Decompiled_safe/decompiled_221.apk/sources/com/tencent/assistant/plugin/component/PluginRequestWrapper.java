package com.tencent.assistant.plugin.component;

import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;

/* compiled from: ProGuard */
public class PluginRequestWrapper extends JceStruct {
    private int cmdId;
    private JceStruct jceStruct;

    public PluginRequestWrapper(int i, JceStruct jceStruct2) {
        this.cmdId = i;
        this.jceStruct = jceStruct2;
    }

    public int getCmdId() {
        return this.cmdId;
    }

    public JceStruct getRequestJceStruct() {
        return this.jceStruct;
    }

    public void writeTo(JceOutputStream jceOutputStream) {
    }

    public void readFrom(JceInputStream jceInputStream) {
    }
}
