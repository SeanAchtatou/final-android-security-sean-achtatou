package com.tencent.assistant.plugin.mgr;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.table.r;
import com.tencent.assistant.plugin.PluginContext;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.PluginLoaderInfo;
import com.tencent.assistant.plugin.annotation.PluginActivityAnnotaion;
import com.tencent.assistant.plugin.annotation.PluginActivityLaunchMode;
import com.tencent.assistant.plugin.annotation.PluginActivityType;
import com.tencent.assistant.plugin.proxy.ConnectNotDimDialogPluginProxyActivity;
import com.tencent.assistant.plugin.proxy.ConnectPluginProxyActivity;
import com.tencent.assistant.plugin.proxy.ConnectReportViewProxyActivity;
import com.tencent.assistant.plugin.proxy.ConnectSingleInstancePluginProxyActivity;
import com.tencent.assistant.plugin.proxy.ConnectSingleTaskPluginProxyActivity;
import com.tencent.assistant.plugin.proxy.ConnectSingleTopPluginProxyActivity;
import com.tencent.assistant.plugin.proxy.ConnectSingleTopReportViewProxyActivity;
import com.tencent.assistant.plugin.proxy.ConnectTranslucentPluginProxyActivity;
import com.tencent.assistant.plugin.proxy.NotDimDialogPluginProxyActivity;
import com.tencent.assistant.plugin.proxy.PluginProxyActivity;
import com.tencent.assistant.plugin.proxy.ReportViewProxyActivity;
import com.tencent.assistant.plugin.proxy.SingleTaskPluginProxyActivity;
import com.tencent.assistant.plugin.proxy.TranslucentSinglePluginProxyActivity;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bj;
import com.tencent.assistant.utils.e;
import dalvik.system.DexClassLoader;
import java.io.File;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public class c {

    /* renamed from: a  reason: collision with root package name */
    private static final Map<String, PluginLoaderInfo> f1947a = new ConcurrentHashMap();

    public static PluginActivityAnnotaion a(PluginLoaderInfo pluginLoaderInfo, String str) {
        try {
            return (PluginActivityAnnotaion) pluginLoaderInfo.loadClass(str).getAnnotation(PluginActivityAnnotaion.class);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Class<? extends PluginProxyActivity> a(int i, PluginActivityAnnotaion pluginActivityAnnotaion) {
        if (i == 1) {
            if (pluginActivityAnnotaion != null) {
                String value = pluginActivityAnnotaion.value();
                String launchMode = pluginActivityAnnotaion.launchMode();
                if (TextUtils.isEmpty(value)) {
                    if (TextUtils.isEmpty(launchMode)) {
                        return ConnectPluginProxyActivity.class;
                    }
                    if (launchMode.equals(PluginActivityLaunchMode.LAUNCH_MODE_SINGLE_TOP)) {
                        return ConnectSingleTopPluginProxyActivity.class;
                    }
                    if (launchMode.equals(PluginActivityLaunchMode.LAUNCH_MODE_SINGLE_TASK)) {
                        return ConnectSingleTaskPluginProxyActivity.class;
                    }
                    if (launchMode.equals(PluginActivityLaunchMode.LAUNCH_MODE_SINGLE_INSTANCE)) {
                        return ConnectSingleInstancePluginProxyActivity.class;
                    }
                    throw new IllegalArgumentException("not found activity.type:" + value + ",launchMode:" + launchMode);
                } else if (value.equals(PluginActivityType.TYPE_NOT_DIM_DIALOG)) {
                    return ConnectNotDimDialogPluginProxyActivity.class;
                } else {
                    if (value.equals(PluginActivityType.TYPE_DIM_CENTER_VIEW)) {
                        if (TextUtils.isEmpty(launchMode)) {
                            return ConnectReportViewProxyActivity.class;
                        }
                        if (launchMode.equals(PluginActivityLaunchMode.LAUNCH_MODE_SINGLE_TOP)) {
                            return ConnectSingleTopReportViewProxyActivity.class;
                        }
                        throw new IllegalArgumentException("not found activity.type:" + value + ",launchMode:" + launchMode);
                    } else if (!value.equals(PluginActivityType.TYPE_TRANSLUCENT) || !TextUtils.isEmpty(launchMode)) {
                        return ConnectPluginProxyActivity.class;
                    } else {
                        return ConnectTranslucentPluginProxyActivity.class;
                    }
                }
            }
            return ConnectPluginProxyActivity.class;
        } else if (pluginActivityAnnotaion == null) {
            return PluginProxyActivity.class;
        } else {
            String value2 = pluginActivityAnnotaion.value();
            String launchMode2 = pluginActivityAnnotaion.launchMode();
            if (PluginActivityLaunchMode.LAUNCH_MODE_SINGLE_INSTANCE.equals(launchMode2) && PluginActivityType.TYPE_TRANSLUCENT.equals(value2)) {
                return TranslucentSinglePluginProxyActivity.class;
            }
            if (PluginActivityLaunchMode.LAUNCH_MODE_SINGLE_TASK.equals(launchMode2)) {
                return SingleTaskPluginProxyActivity.class;
            }
            if (!TextUtils.isEmpty(launchMode2)) {
                throw new IllegalArgumentException("not found activity.type:" + value2 + ",launchMode:" + launchMode2);
            } else if (TextUtils.isEmpty(value2)) {
                return PluginProxyActivity.class;
            } else {
                if (value2.equals(PluginActivityType.TYPE_NOT_DIM_DIALOG)) {
                    return NotDimDialogPluginProxyActivity.class;
                }
                if (value2.equals(PluginActivityType.TYPE_DIM_CENTER_VIEW)) {
                    return ReportViewProxyActivity.class;
                }
                throw new IllegalArgumentException("not found activity.type:" + value2 + ",launchMode:" + launchMode2);
            }
        }
    }

    public static PluginLoaderInfo a(Context context, PluginInfo pluginInfo) {
        if (pluginInfo == null) {
            return null;
        }
        PluginLoaderInfo pluginLoaderInfo = f1947a.get(pluginInfo.getPackageName());
        boolean z = (pluginLoaderInfo == null || pluginInfo.getVersion() == pluginLoaderInfo.getContext().getPluginInfo().getVersion()) ? false : true;
        if (pluginLoaderInfo == null || z) {
            if (z) {
                e.a();
            }
            pluginLoaderInfo = k(context, pluginInfo);
        }
        if (pluginLoaderInfo == null) {
        }
        return pluginLoaderInfo;
    }

    private static PluginLoaderInfo k(Context context, PluginInfo pluginInfo) {
        String pluginApkPath = pluginInfo.getPluginApkPath();
        if (!a(pluginInfo) || !a(context, pluginApkPath)) {
            return null;
        }
        ClassLoader b = b(context, pluginInfo);
        PluginLoaderInfo pluginLoaderInfo = new PluginLoaderInfo(b, new PluginContext(context, 0, pluginInfo, b));
        f1947a.put(pluginInfo.getPackageName(), pluginLoaderInfo);
        return pluginLoaderInfo;
    }

    public static ClassLoader b(Context context, PluginInfo pluginInfo) {
        return new DexClassLoader(pluginInfo.getPluginApkPath(), context.getDir("plugin_dex_cache", 0).getAbsolutePath(), pluginInfo.getPluginLibPath(), AstApp.i().getClassLoader());
    }

    private static boolean a(PluginInfo pluginInfo) {
        if (pluginInfo == null) {
            return false;
        }
        String pluginApkPath = pluginInfo.getPluginApkPath();
        if (TextUtils.isEmpty(pluginApkPath)) {
            return false;
        }
        if (new File(pluginApkPath).exists()) {
            return true;
        }
        new r().b(pluginInfo.getPackageName());
        XLog.w("Plugin", "plugin file:" + pluginApkPath + " not exist.delete db record.packagename:" + pluginInfo.getPackageName());
        return false;
    }

    public static int a(String str) {
        PluginLoaderInfo pluginLoaderInfo = f1947a.get(str);
        if (pluginLoaderInfo == null) {
            return -1;
        }
        return pluginLoaderInfo.getContext().getPluginInfo().getVersion();
    }

    private static boolean a(Context context, String str) {
        PackageInfo a2;
        try {
            if (e.c() || (a2 = e.a((PackageManager) null, str, 64)) == null) {
                return true;
            }
            String b = bj.b(a2.signatures[a2.signatures.length - 1].toCharsString());
            if (TextUtils.isEmpty(b) || !"6A95826F74986030F16DE061551E6DE3".equals(b)) {
                return false;
            }
            return true;
        } catch (Throwable th) {
            th.printStackTrace();
            return true;
        }
    }

    public static BroadcastReceiver c(Context context, PluginInfo pluginInfo) {
        return (BroadcastReceiver) a(context, pluginInfo, pluginInfo.getSmsReceiverImpl());
    }

    public static BroadcastReceiver d(Context context, PluginInfo pluginInfo) {
        return (BroadcastReceiver) a(context, pluginInfo, pluginInfo.getMainReceiverImpl());
    }

    public static BroadcastReceiver e(Context context, PluginInfo pluginInfo) {
        return (BroadcastReceiver) a(context, pluginInfo, pluginInfo.getApkRecieverImpl());
    }

    public static BroadcastReceiver f(Context context, PluginInfo pluginInfo) {
        return (BroadcastReceiver) a(context, pluginInfo, pluginInfo.getExtendReceiverImpl(PluginInfo.META_DATA_QREADER_RECEIVER));
    }

    public static BroadcastReceiver g(Context context, PluginInfo pluginInfo) {
        return (BroadcastReceiver) a(context, pluginInfo, pluginInfo.getSmsSentReceiverImpl());
    }

    public static BroadcastReceiver h(Context context, PluginInfo pluginInfo) {
        return (BroadcastReceiver) a(context, pluginInfo, pluginInfo.getHeartBeatReceiverImpl());
    }

    public static BroadcastReceiver i(Context context, PluginInfo pluginInfo) {
        return (BroadcastReceiver) a(context, pluginInfo, pluginInfo.getDockReceiverImpl());
    }

    public static BroadcastReceiver j(Context context, PluginInfo pluginInfo) {
        return (BroadcastReceiver) a(context, pluginInfo, pluginInfo.getExtendReceiverImpl(PluginInfo.META_DATA_FREE_WIFI_RECEIVER));
    }

    public static <T> T a(Context context, PluginInfo pluginInfo, String str) {
        PluginLoaderInfo a2;
        Class<?> cls;
        if (TextUtils.isEmpty(str) || (a2 = a(context, pluginInfo)) == null) {
            return null;
        }
        try {
            cls = a2.loadClass(str);
        } catch (Throwable th) {
            th.printStackTrace();
            cls = null;
        }
        if (cls == null) {
            return null;
        }
        try {
            return cls.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
            return null;
        } catch (Throwable th2) {
            th2.printStackTrace();
            return null;
        }
    }
}
