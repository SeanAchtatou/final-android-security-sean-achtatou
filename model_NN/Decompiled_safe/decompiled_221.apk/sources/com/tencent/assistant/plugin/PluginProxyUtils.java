package com.tencent.assistant.plugin;

import android.app.Activity;
import android.app.Dialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import com.qq.AppService.AstApp;
import com.qq.util.x;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.Global;
import com.tencent.assistant.component.appdetail.HorizonImageListView;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.link.b;
import com.tencent.assistant.login.d;
import com.tencent.assistant.manager.notification.y;
import com.tencent.assistant.module.aw;
import com.tencent.assistant.net.c;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.STConstAction;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.bj;
import com.tencent.assistant.utils.bt;
import com.tencent.assistant.utils.bu;
import com.tencent.assistant.utils.bv;
import com.tencent.assistant.utils.cv;
import com.tencent.assistant.utils.df;
import com.tencent.assistant.utils.installuninstall.p;
import com.tencent.assistant.utils.m;
import com.tencent.assistant.utils.r;
import com.tencent.assistant.utils.t;
import com.tencent.assistant.utils.v;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class PluginProxyUtils {
    public static int dip2px(Context context, float f) {
        return df.a(context, f);
    }

    public static int getSpValueInt(float f) {
        return df.b(f);
    }

    public static Intent openFile(String str) {
        return bu.a(str);
    }

    public static Uri saveMediaEntry(ContentResolver contentResolver, String str, String str2, String str3) {
        return bu.a(contentResolver, str, str2, str3);
    }

    public static boolean hasAbility(Context context, Intent intent) {
        return b.a(context, intent);
    }

    public static void innerForward(Context context, String str, Bundle bundle) {
        b.b(context, str, bundle);
    }

    public static void setIsHotWifi(boolean z) {
        c.a(z);
    }

    public static boolean IsAirModeOn(Context context) {
        return c.a(context);
    }

    public static void show1BtnDialog(Activity activity, AppConst.OneBtnDialogInfo oneBtnDialogInfo) {
        v.a(activity, oneBtnDialogInfo);
    }

    public static void show2BtnDialog(Activity activity, AppConst.TwoBtnDialogInfo twoBtnDialogInfo) {
        v.a(activity, twoBtnDialogInfo);
    }

    public static Dialog showLoadingDialog(Activity activity, String str) {
        AppConst.LoadingDialogInfo loadingDialogInfo = new AppConst.LoadingDialogInfo();
        loadingDialogInfo.loadingText = str;
        return v.a(activity, loadingDialogInfo);
    }

    public static void showContentViewDialog(Activity activity, View view) {
        v.a(activity, view);
    }

    public static long getAvailableSDCardSize() {
        return bt.a();
    }

    public static String formatSizeM(long j) {
        return bt.a(j);
    }

    public static String getHMSDataFromMillisecond(long j) {
        return cv.a(j);
    }

    public static Handler getMainHandler() {
        return ba.a();
    }

    public static synchronized int getUniqueId() {
        int a2;
        synchronized (PluginProxyUtils.class) {
            a2 = r.a();
        }
        return a2;
    }

    public static byte[] intToBytes(int i) {
        return r.a(i);
    }

    public static int bytesToInt(byte[] bArr, int i) {
        return r.a(bArr, i);
    }

    public static String remoteFileIsExit(String str, String str2, long j) {
        return FileUtil.remoteFileIsExit(str, str2, j);
    }

    public static String getWifiDir(String str) {
        return FileUtil.getWifiDir(str);
    }

    public static String getUnusedFilePath(String str) {
        return FileUtil.getUnusedFilePath(str);
    }

    public static boolean isWifi() {
        return c.d();
    }

    public static boolean isDev() {
        return Global.isDev();
    }

    public static boolean isMobileNetwork() {
        return c.e() || c.f();
    }

    public static boolean isLogin() {
        return d.a().j();
    }

    public static void login() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConst.KEY_LOGIN_TYPE, 2);
        bundle.putInt(AppConst.KEY_FROM_TYPE, 17);
        d.a().a(AppConst.IdentityType.NONE, bundle);
    }

    public static String getDeviceName() {
        return t.u();
    }

    public static String getImei() {
        return t.g();
    }

    public static int dip2px(Context context, int i) {
        return df.a(context, (float) i);
    }

    public static int px2dip(Context context, float f) {
        return df.b(context, f);
    }

    public static Notification createNotification(Context context, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, PendingIntent pendingIntent, boolean z, boolean z2) {
        return y.a(context, charSequence, charSequence2, charSequence3, pendingIntent, z, z2);
    }

    public static void onProcessListItemCllick(int i, boolean z, int i2) {
        k.a(new STInfoV2(STConst.ST_PAGE_DOCK_ACCE_LIST, "03_" + String.format("%03d", Integer.valueOf(i + 1)), STConst.ST_PAGE_PERSON_CENTER_NO_LOGIN, STConst.ST_DEFAULT_SLOT, 200));
    }

    public static void onOneKeyAccelerateItemClick(int i, boolean z, int i2, boolean z2) {
        int i3;
        int i4;
        if (z2) {
            i3 = STConst.ST_PAGE_SOFTWARE_UNINSTALL_DOWNLOAD_ROOT_UTIL;
        } else {
            i3 = STConst.ST_PAGE_DOCK_ACCE_LIST;
        }
        if (z) {
            i4 = STConstAction.ACTION_HIT_APK_CHECK;
        } else {
            i4 = STConstAction.ACTION_HIT_APK_UNCHECK;
        }
        k.a(new STInfoV2(i3, "03_" + String.format("%03d", Integer.valueOf(i + 1)), STConst.ST_PAGE_PERSON_CENTER_NO_LOGIN, STConst.ST_DEFAULT_SLOT, i4));
    }

    public static void onDeepAccelerateItemClick(int i, int i2, boolean z, int i3) {
        int i4;
        String str;
        if (z) {
            i4 = STConstAction.ACTION_HIT_WISE_SWITCH_ON;
        } else {
            i4 = STConstAction.ACTION_HIT_WISE_SWITCH_OFF;
        }
        if (i == 0) {
            str = "03_";
        } else if (i == 1) {
            str = "04_";
        } else {
            str = i == 2 ? HorizonImageListView.TMA_ST_HORIZON_IMAGE_TAG : Constants.STR_EMPTY;
        }
        k.a(new STInfoV2(STConst.ST_PAGE_DOCK_AUTO_HAS_ROOT, str + String.format("%03d", Integer.valueOf(i2 + 1)), STConst.ST_PAGE_PERSON_CENTER_NO_LOGIN, STConst.ST_DEFAULT_SLOT, i4));
    }

    public static void onAccelerateActivityBrowser(boolean z, boolean z2) {
        if (z) {
            k.a(new STInfoV2(STConst.ST_PAGE_DOCK_AUTO_HAS_ROOT, STConst.ST_DEFAULT_SLOT, STConst.ST_PAGE_PERSON_CENTER_NO_LOGIN, STConst.ST_DEFAULT_SLOT, 100));
        } else if (z2) {
            k.a(new STInfoV2(STConst.ST_PAGE_SOFTWARE_UNINSTALL_DOWNLOAD_ROOT_UTIL, STConst.ST_DEFAULT_SLOT, STConst.ST_PAGE_PERSON_CENTER_NO_LOGIN, STConst.ST_DEFAULT_SLOT, 100));
        } else {
            k.a(new STInfoV2(STConst.ST_PAGE_DOCK_ACCE_LIST, STConst.ST_DEFAULT_SLOT, STConst.ST_PAGE_PERSON_CENTER_NO_LOGIN, STConst.ST_DEFAULT_SLOT, 100));
        }
    }

    public static void onAccelerateActivityBrowser(int i) {
        k.a(new STInfoV2(STConst.ST_PAGE_DOCK_AUTO_HAS_ROOT, STConst.ST_DEFAULT_SLOT, i, STConst.ST_DEFAULT_SLOT, 100));
    }

    public static void onAccelerateBtnClick(boolean z) {
        if (z) {
            k.a(new STInfoV2(STConst.ST_PAGE_SOFTWARE_UNINSTALL_DOWNLOAD_ROOT_UTIL, "04_001", STConst.ST_PAGE_PERSON_CENTER_NO_LOGIN, STConst.ST_DEFAULT_SLOT, 200));
        } else {
            k.a(new STInfoV2(STConst.ST_PAGE_DOCK_ACCE_LIST, "04_001", STConst.ST_PAGE_PERSON_CENTER_NO_LOGIN, STConst.ST_DEFAULT_SLOT, 200));
        }
    }

    public static void onAccelerateActivityBrowser() {
        k.a(new STInfoV2(STConst.ST_PAGE_DOCK_ACCE_LIST, STConst.ST_DEFAULT_SLOT, STConst.ST_PAGE_PERSON_CENTER_NO_LOGIN, STConst.ST_DEFAULT_SLOT, 100));
    }

    public static void onLauncherAccelerateRun() {
        k.a(new STInfoV2(STConst.ST_PAGE_DOCK_NO_ROOT_DESK_PAGEID, STConst.ST_DEFAULT_SLOT, STConst.ST_PAGE_DOCK_NO_ROOT_DESK_PAGEID, STConst.ST_DEFAULT_SLOT, 100));
    }

    public static void onAccelerateShortcutCreate() {
        k.a(new STInfoV2(STConst.ST_PAGE_DOCK_ADD_SHORCUT_PAGEID, STConst.ST_DEFAULT_SLOT, 2000, STConst.ST_DEFAULT_SLOT, 100));
    }

    public static Bitmap getBitmap(int i) {
        return m.a(i);
    }

    public static void onLauncherAccelerateRun(boolean z) {
        if (z) {
            k.a(new STInfoV2(STConst.ST_PAGE_DOCK_ROOT_DESK_PAGEID, STConst.ST_DEFAULT_SLOT, STConst.ST_PAGE_DOCK_ROOT_DESK_PAGEID, STConst.ST_DEFAULT_SLOT, 100));
        } else {
            k.a(new STInfoV2(STConst.ST_PAGE_DOCK_NO_ROOT_DESK_PAGEID, STConst.ST_DEFAULT_SLOT, STConst.ST_PAGE_DOCK_NO_ROOT_DESK_PAGEID, STConst.ST_DEFAULT_SLOT, 100));
        }
    }

    public static void onAccelerateRichContentClick(boolean z) {
        if (z) {
            k.a(new STInfoV2(STConst.ST_PAGE_DOCK_ROOT_DESK_PAGEID, "03_001", STConst.ST_PAGE_DOCK_ROOT_DESK_PAGEID, STConst.ST_DEFAULT_SLOT, 200));
        } else {
            k.a(new STInfoV2(STConst.ST_PAGE_DOCK_NO_ROOT_DESK_PAGEID, "03_001", STConst.ST_PAGE_DOCK_NO_ROOT_DESK_PAGEID, STConst.ST_DEFAULT_SLOT, 200));
        }
    }

    public static String convert2SortKey(String str) {
        return bv.a(str);
    }

    public static String getPreActivityTagName() {
        return PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME;
    }

    public static void setDeviceRootStatus() {
        if (com.tencent.assistant.m.a().n() != AppConst.ROOT_STATUS.ROOTED) {
            com.tencent.assistant.m.a().a(t.n());
        }
        if (com.tencent.assistant.m.a().i()) {
            com.tencent.assistant.m.a().a(AppConst.ROOT_STATUS.ROOTED);
        }
    }

    public static void setSkipHtmlResponse(boolean z) {
        aw.sKipHtmlResponse = z;
    }

    public static void reportBtnClick(int i, int i2, String str) {
        logReportV2(i, str, i2, 200, null);
    }

    public static void logReportV2(int i, String str, int i2, int i3, String str2) {
        STInfoV2 sTInfoV2 = new STInfoV2(i, str, i2, STConst.ST_DEFAULT_SLOT, i3);
        if (sTInfoV2 != null) {
            sTInfoV2.extraData = str2;
            XLog.i("PluginLoginIn", "[logReportV2] --> pageId = " + i + ", slotId = " + str + ", prePageId = " + i2 + ", actionId = " + i3 + ", extraData = " + str2);
            k.a(sTInfoV2);
        }
    }

    public static void logReportV2(int i, String str, int i2, int i3, String str2, String str3) {
        STInfoV2 sTInfoV2 = new STInfoV2(i, str, i2, STConst.ST_DEFAULT_SLOT, i3);
        sTInfoV2.status = str2;
        if (sTInfoV2 != null) {
            sTInfoV2.extraData = str3;
            XLog.i("PluginLoginIn", "[logReportV2] --> pageId = " + i + ", slotId = " + str + ", prePageId = " + i2 + ", actionId = " + i3 + ", extraData = " + str3);
            k.a(sTInfoV2);
        }
    }

    public static void logReportV2(int i, String str, int i2, int i3, String str2, String str3, boolean z) {
        STInfoV2 sTInfoV2 = new STInfoV2(i, str, i2, STConst.ST_DEFAULT_SLOT, i3);
        sTInfoV2.status = str2;
        sTInfoV2.isImmediately = z;
        if (sTInfoV2 != null) {
            sTInfoV2.extraData = str3;
            XLog.i("PluginLoginIn", "[logReportV2] --> pageId = " + i + ", slotId = " + str + ", prePageId = " + i2 + ", actionId = " + i3 + ", extraData = " + str3);
            k.a(sTInfoV2);
        }
    }

    public static void reportPush(int i, int i2, String str, String str2, boolean z) {
        com.tencent.assistantv2.st.page.c.a(i, i2, str, str2, z);
    }

    public static Typeface getTypeFace() {
        try {
            return Typeface.createFromAsset(AstApp.i().getAssets(), "fonts/Numbers.otf");
        } catch (Throwable th) {
            th.printStackTrace();
            return Typeface.DEFAULT;
        }
    }

    public static String getSwitchPhoneDir(String str) {
        return FileUtil.getSwitchPhoneDir(str);
    }

    public static String getAPKDir() {
        return FileUtil.getAPKDir();
    }

    public static String getCameraDir(String str) {
        return FileUtil.getCameraDir(str);
    }

    public static String getSwitchPhoneDir() {
        return FileUtil.getSwitchPhoneDir();
    }

    public static long getFileSize(String str) {
        return FileUtil.getFileSize(str);
    }

    public static String readFile(String str) {
        return FileUtil.read(str);
    }

    public static long getOrCreateThreadId(Context context, String str) {
        return x.a(context, str);
    }

    public static String toMD5(String str) {
        return bj.b(str);
    }

    public static void startPatchInstall(ArrayList<DownloadInfo> arrayList, boolean z) {
        p.a().b(arrayList, z);
    }

    public static boolean canRootInstall() {
        return p.a().b();
    }

    public static void installApk(DownloadInfo downloadInfo, boolean z) {
        p.a().a(downloadInfo, z);
    }
}
