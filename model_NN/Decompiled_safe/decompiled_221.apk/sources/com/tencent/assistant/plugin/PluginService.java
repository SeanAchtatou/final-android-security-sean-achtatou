package com.tencent.assistant.plugin;

import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

/* compiled from: ProGuard */
public class PluginService extends Service implements IPluginService {

    /* renamed from: a  reason: collision with root package name */
    private Service f1936a;

    public void attachBaseService(Service service) {
        this.f1936a = service;
        attachBaseContext(this.f1936a.getBaseContext());
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void stopPluginSelf() {
        this.f1936a.stopSelf();
    }

    public void startPluginForeground(int i, Notification notification) {
        this.f1936a.startForeground(i, notification);
    }

    public void stopPluginForeground(boolean z) {
        this.f1936a.stopForeground(z);
    }

    public Context getApplicationContext() {
        return this.f1936a.getApplicationContext();
    }
}
