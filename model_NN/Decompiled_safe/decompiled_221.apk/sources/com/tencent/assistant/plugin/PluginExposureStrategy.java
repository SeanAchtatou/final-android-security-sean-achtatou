package com.tencent.assistant.plugin;

import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public class PluginExposureStrategy extends b {
    public static PluginExposureStrategy instance;

    private PluginExposureStrategy() {
    }

    public static synchronized PluginExposureStrategy getInstance() {
        PluginExposureStrategy pluginExposureStrategy;
        synchronized (PluginExposureStrategy.class) {
            if (instance == null) {
                instance = new PluginExposureStrategy();
            }
            pluginExposureStrategy = instance;
        }
        return pluginExposureStrategy;
    }

    public STInfoV2 getExposureSTInfoV2(byte[] bArr, long j, int i, int i2, String str) {
        return super.getExposureSTInfoV2(bArr, j, i, i2, str);
    }

    public void exposure(STInfoV2 sTInfoV2) {
        super.exposure(sTInfoV2);
    }
}
