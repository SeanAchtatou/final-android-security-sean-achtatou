package com.tencent.assistant.protocol.scu;

import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.protocol.ProtocolDecoder;
import com.tencent.assistant.protocol.a.e;
import com.tencent.assistant.protocol.b;
import com.tencent.assistant.protocol.jce.MultiCmdRequest;
import com.tencent.assistant.protocol.jce.MultiCmdResponse;
import com.tencent.assistant.protocol.jce.ReqHead;
import com.tencent.assistant.protocol.jce.Request;
import com.tencent.assistant.protocol.jce.Response;
import com.tencent.assistant.protocol.jce.SingleCmdRequest;
import com.tencent.assistant.protocol.jce.SingleCmdResponse;
import com.tencent.assistant.protocol.l;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class k {
    public static l a(int i, List<JceStruct> list) {
        int i2 = 0;
        if (list == null || list.size() == 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList(list.size());
        ArrayList arrayList2 = new ArrayList();
        boolean z = false;
        for (int i3 = 0; i3 < list.size(); i3++) {
            try {
                JceStruct jceStruct = list.get(i3);
                SingleCmdRequest singleCmdRequest = new SingleCmdRequest();
                if (e.a().a(jceStruct)) {
                    singleCmdRequest.f2311a = e.a().b(singleCmdRequest);
                    jceStruct = e.a().c(jceStruct);
                } else {
                    singleCmdRequest.f2311a = a(jceStruct);
                }
                if (singleCmdRequest.f2311a == 200) {
                    z = true;
                }
                arrayList.add(Integer.valueOf(singleCmdRequest.f2311a));
                singleCmdRequest.b = l.b(jceStruct);
                arrayList2.add(singleCmdRequest);
            } catch (Throwable th) {
                cq.a().b();
            }
        }
        MultiCmdRequest multiCmdRequest = new MultiCmdRequest();
        multiCmdRequest.a(arrayList2);
        Request request = new Request();
        ReqHead reqHead = new ReqHead();
        reqHead.b = 1000;
        if (z) {
            i2 = 1;
        }
        reqHead.t = (byte) i2;
        request.head = reqHead;
        request.body = l.b(multiCmdRequest);
        l lVar = new l();
        lVar.f2443a = request;
        lVar.b = arrayList;
        return lVar;
    }

    public static List<RequestResponePair> a(int i, List<JceStruct> list, Response response, ProtocolDecoder protocolDecoder) {
        if (list == null || list.size() == 0) {
            return null;
        }
        List<RequestResponePair> b = b(i, list);
        if (response == null) {
            return b;
        }
        MultiCmdResponse a2 = a(response.body);
        if (a2 == null) {
            for (RequestResponePair next : b) {
                if (next.errorCode == 0) {
                    next.errorCode = -841;
                }
            }
            return b;
        }
        ArrayList<SingleCmdResponse> a3 = a2.a();
        if (a3 == null) {
            return b;
        }
        return a(b, a3, protocolDecoder);
    }

    private static List<RequestResponePair> b(int i, List<JceStruct> list) {
        ArrayList arrayList = new ArrayList();
        if (list != null && list.size() != 0) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= list.size()) {
                    break;
                }
                RequestResponePair requestResponePair = new RequestResponePair();
                requestResponePair.request = list.get(i3);
                requestResponePair.errorCode = i;
                arrayList.add(requestResponePair);
                i2 = i3 + 1;
            }
        }
        return arrayList;
    }

    private static MultiCmdResponse a(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        MultiCmdResponse multiCmdResponse = new MultiCmdResponse();
        try {
            JceInputStream jceInputStream = new JceInputStream(bArr);
            jceInputStream.setServerEncoding("utf-8");
            multiCmdResponse.readFrom(jceInputStream);
            return multiCmdResponse;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static List<RequestResponePair> a(List<RequestResponePair> list, List<SingleCmdResponse> list2, ProtocolDecoder protocolDecoder) {
        if (!(list == null || list.size() == 0 || list2 == null)) {
            if (protocolDecoder == null) {
                protocolDecoder = b.a();
            }
            for (int i = 0; i < list.size(); i++) {
                RequestResponePair requestResponePair = list.get(i);
                JceStruct jceStruct = requestResponePair.request;
                if (jceStruct != null) {
                    int a2 = a(jceStruct);
                    for (int i2 = 0; i2 < list2.size(); i2++) {
                        SingleCmdResponse singleCmdResponse = list2.get(i2);
                        if (singleCmdResponse != null && a2 == singleCmdResponse.f2312a) {
                            requestResponePair.errorCode = singleCmdResponse.b;
                            JceStruct decodeResponseBody = protocolDecoder.decodeResponseBody(jceStruct, singleCmdResponse.c);
                            if (decodeResponseBody == null && requestResponePair.errorCode == 0) {
                                requestResponePair.errorCode = -841;
                            }
                            requestResponePair.response = decodeResponseBody;
                        }
                        if (requestResponePair.response != null) {
                            break;
                        }
                    }
                    if (requestResponePair.response == null && requestResponePair.errorCode == 0) {
                        requestResponePair.errorCode = -841;
                    }
                }
            }
        }
        return list;
    }

    public static int a(JceStruct jceStruct) {
        return l.a(jceStruct);
    }
}
