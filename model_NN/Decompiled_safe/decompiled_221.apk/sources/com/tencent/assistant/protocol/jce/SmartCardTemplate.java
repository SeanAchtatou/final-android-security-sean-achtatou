package com.tencent.assistant.protocol.jce;

import com.qq.taf.jce.JceDisplayer;
import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.qq.taf.jce.JceUtil;
import com.tencent.connect.common.Constants;
import com.tencent.open.SocialConstants;
import java.util.ArrayList;
import java.util.Collection;

/* compiled from: ProGuard */
public final class SmartCardTemplate extends JceStruct implements Cloneable {
    static ArrayList<SmartCardTemplateItem> k;
    static final /* synthetic */ boolean l = (!SmartCardTemplate.class.desiredAssertionStatus());

    /* renamed from: a  reason: collision with root package name */
    public String f2335a = Constants.STR_EMPTY;
    public ArrayList<SmartCardTemplateItem> b = null;
    public long c = 0;
    public String d = Constants.STR_EMPTY;
    public String e = Constants.STR_EMPTY;
    public String f = Constants.STR_EMPTY;
    public int g = 0;
    public String h = Constants.STR_EMPTY;
    public boolean i = true;
    public int j = 0;

    public ArrayList<SmartCardTemplateItem> a() {
        return this.b;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        SmartCardTemplate smartCardTemplate = (SmartCardTemplate) obj;
        if (!JceUtil.equals(this.f2335a, smartCardTemplate.f2335a) || !JceUtil.equals(this.b, smartCardTemplate.b) || !JceUtil.equals(this.c, smartCardTemplate.c) || !JceUtil.equals(this.d, smartCardTemplate.d) || !JceUtil.equals(this.e, smartCardTemplate.e) || !JceUtil.equals(this.f, smartCardTemplate.f) || !JceUtil.equals(this.g, smartCardTemplate.g) || !JceUtil.equals(this.h, smartCardTemplate.h) || !JceUtil.equals(this.i, smartCardTemplate.i) || !JceUtil.equals(this.j, smartCardTemplate.j)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        try {
            throw new Exception("Need define key first!");
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e2) {
            if (l) {
                return null;
            }
            throw new AssertionError();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceOutputStream.write(java.util.Collection, int):void
     arg types: [java.util.ArrayList<com.tencent.assistant.protocol.jce.SmartCardTemplateItem>, int]
     candidates:
      com.qq.taf.jce.JceOutputStream.write(byte, int):void
      com.qq.taf.jce.JceOutputStream.write(double, int):void
      com.qq.taf.jce.JceOutputStream.write(float, int):void
      com.qq.taf.jce.JceOutputStream.write(int, int):void
      com.qq.taf.jce.JceOutputStream.write(long, int):void
      com.qq.taf.jce.JceOutputStream.write(com.qq.taf.jce.JceStruct, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Byte, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Double, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Float, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Integer, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Long, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Short, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.String, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void
      com.qq.taf.jce.JceOutputStream.write(short, int):void
      com.qq.taf.jce.JceOutputStream.write(boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(byte[], int):void
      com.qq.taf.jce.JceOutputStream.write(double[], int):void
      com.qq.taf.jce.JceOutputStream.write(float[], int):void
      com.qq.taf.jce.JceOutputStream.write(int[], int):void
      com.qq.taf.jce.JceOutputStream.write(long[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object[], int):void
      com.qq.taf.jce.JceOutputStream.write(short[], int):void
      com.qq.taf.jce.JceOutputStream.write(boolean[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Collection, int):void */
    public void writeTo(JceOutputStream jceOutputStream) {
        jceOutputStream.write(this.f2335a, 0);
        jceOutputStream.write((Collection) this.b, 1);
        jceOutputStream.write(this.c, 2);
        if (this.d != null) {
            jceOutputStream.write(this.d, 3);
        }
        if (this.e != null) {
            jceOutputStream.write(this.e, 4);
        }
        if (this.f != null) {
            jceOutputStream.write(this.f, 5);
        }
        jceOutputStream.write(this.g, 6);
        if (this.h != null) {
            jceOutputStream.write(this.h, 7);
        }
        jceOutputStream.write(this.i, 8);
        jceOutputStream.write(this.j, 9);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.ArrayList<com.tencent.assistant.protocol.jce.SmartCardTemplateItem>, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
     arg types: [long, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
     arg types: [boolean, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean */
    public void readFrom(JceInputStream jceInputStream) {
        this.f2335a = jceInputStream.readString(0, true);
        if (k == null) {
            k = new ArrayList<>();
            k.add(new SmartCardTemplateItem());
        }
        this.b = (ArrayList) jceInputStream.read((Object) k, 1, true);
        this.c = jceInputStream.read(this.c, 2, false);
        this.d = jceInputStream.readString(3, false);
        this.e = jceInputStream.readString(4, false);
        this.f = jceInputStream.readString(5, false);
        this.g = jceInputStream.read(this.g, 6, false);
        this.h = jceInputStream.readString(7, false);
        this.i = jceInputStream.read(this.i, 8, false);
        this.j = jceInputStream.read(this.j, 9, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.display(java.util.Collection, java.lang.String):com.qq.taf.jce.JceDisplayer
     arg types: [java.util.ArrayList<com.tencent.assistant.protocol.jce.SmartCardTemplateItem>, java.lang.String]
     candidates:
      com.qq.taf.jce.JceDisplayer.display(byte, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(char, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(double, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(float, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(int, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(long, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(com.qq.taf.jce.JceStruct, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.Object, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.String, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.util.Map, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(short, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(boolean, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(byte[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(char[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(double[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(float[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(int[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(long[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.Object[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(short[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.util.Collection, java.lang.String):com.qq.taf.jce.JceDisplayer */
    public void display(StringBuilder sb, int i2) {
        JceDisplayer jceDisplayer = new JceDisplayer(sb, i2);
        jceDisplayer.display(this.f2335a, "title");
        jceDisplayer.display((Collection) this.b, "items");
        jceDisplayer.display(this.c, "uin");
        jceDisplayer.display(this.d, SocialConstants.PARAM_APP_DESC);
        jceDisplayer.display(this.e, "actionText");
        jceDisplayer.display(this.f, "actionUrl");
        jceDisplayer.display(this.g, "tempateType");
        jceDisplayer.display(this.h, "pic");
        jceDisplayer.display(this.i, "showTitleBg");
        jceDisplayer.display(this.j, "topicId");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer
     arg types: [java.lang.String, int]
     candidates:
      com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(boolean, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer
     arg types: [java.util.ArrayList<com.tencent.assistant.protocol.jce.SmartCardTemplateItem>, int]
     candidates:
      com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(boolean, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer
     arg types: [long, int]
     candidates:
      com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(boolean, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer
     arg types: [int, int]
     candidates:
      com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(boolean, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.displaySimple(boolean, boolean):com.qq.taf.jce.JceDisplayer
     arg types: [boolean, int]
     candidates:
      com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(boolean, boolean):com.qq.taf.jce.JceDisplayer */
    public void displaySimple(StringBuilder sb, int i2) {
        JceDisplayer jceDisplayer = new JceDisplayer(sb, i2);
        jceDisplayer.displaySimple(this.f2335a, true);
        jceDisplayer.displaySimple((Collection) this.b, true);
        jceDisplayer.displaySimple(this.c, true);
        jceDisplayer.displaySimple(this.d, true);
        jceDisplayer.displaySimple(this.e, true);
        jceDisplayer.displaySimple(this.f, true);
        jceDisplayer.displaySimple(this.g, true);
        jceDisplayer.displaySimple(this.h, true);
        jceDisplayer.displaySimple(this.i, true);
        jceDisplayer.displaySimple(this.j, false);
    }
}
