package com.tencent.assistant.protocol.jce;

import com.qq.taf.jce.JceDisplayer;
import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.qq.taf.jce.JceUtil;
import com.tencent.open.SocialConstants;
import java.util.ArrayList;
import java.util.Collection;

/* compiled from: ProGuard */
public final class ListRecommend extends JceStruct implements Cloneable {
    static ListRecommendIIT f;
    static ArrayList<RecommendIT> g;
    static ListRecommendTop h;
    static final /* synthetic */ boolean i = (!ListRecommend.class.desiredAssertionStatus());

    /* renamed from: a  reason: collision with root package name */
    public byte f2228a = 0;
    public byte b = 0;
    public ListRecommendIIT c = null;
    public ArrayList<RecommendIT> d = null;
    public ListRecommendTop e = null;

    public byte a() {
        return this.f2228a;
    }

    public ListRecommendIIT b() {
        return this.c;
    }

    public ArrayList<RecommendIT> c() {
        return this.d;
    }

    public ListRecommendTop d() {
        return this.e;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        ListRecommend listRecommend = (ListRecommend) obj;
        if (!JceUtil.equals(this.f2228a, listRecommend.f2228a) || !JceUtil.equals(this.b, listRecommend.b) || !JceUtil.equals(this.c, listRecommend.c) || !JceUtil.equals(this.d, listRecommend.d) || !JceUtil.equals(this.e, listRecommend.e)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        try {
            throw new Exception("Need define key first!");
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e2) {
            if (i) {
                return null;
            }
            throw new AssertionError();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceOutputStream.write(com.qq.taf.jce.JceStruct, int):void
     arg types: [com.tencent.assistant.protocol.jce.ListRecommendIIT, int]
     candidates:
      com.qq.taf.jce.JceOutputStream.write(byte, int):void
      com.qq.taf.jce.JceOutputStream.write(double, int):void
      com.qq.taf.jce.JceOutputStream.write(float, int):void
      com.qq.taf.jce.JceOutputStream.write(int, int):void
      com.qq.taf.jce.JceOutputStream.write(long, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Byte, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Double, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Float, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Integer, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Long, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Short, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.String, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Collection, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void
      com.qq.taf.jce.JceOutputStream.write(short, int):void
      com.qq.taf.jce.JceOutputStream.write(boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(byte[], int):void
      com.qq.taf.jce.JceOutputStream.write(double[], int):void
      com.qq.taf.jce.JceOutputStream.write(float[], int):void
      com.qq.taf.jce.JceOutputStream.write(int[], int):void
      com.qq.taf.jce.JceOutputStream.write(long[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object[], int):void
      com.qq.taf.jce.JceOutputStream.write(short[], int):void
      com.qq.taf.jce.JceOutputStream.write(boolean[], int):void
      com.qq.taf.jce.JceOutputStream.write(com.qq.taf.jce.JceStruct, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceOutputStream.write(java.util.Collection, int):void
     arg types: [java.util.ArrayList<com.tencent.assistant.protocol.jce.RecommendIT>, int]
     candidates:
      com.qq.taf.jce.JceOutputStream.write(byte, int):void
      com.qq.taf.jce.JceOutputStream.write(double, int):void
      com.qq.taf.jce.JceOutputStream.write(float, int):void
      com.qq.taf.jce.JceOutputStream.write(int, int):void
      com.qq.taf.jce.JceOutputStream.write(long, int):void
      com.qq.taf.jce.JceOutputStream.write(com.qq.taf.jce.JceStruct, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Byte, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Double, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Float, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Integer, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Long, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Short, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.String, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void
      com.qq.taf.jce.JceOutputStream.write(short, int):void
      com.qq.taf.jce.JceOutputStream.write(boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(byte[], int):void
      com.qq.taf.jce.JceOutputStream.write(double[], int):void
      com.qq.taf.jce.JceOutputStream.write(float[], int):void
      com.qq.taf.jce.JceOutputStream.write(int[], int):void
      com.qq.taf.jce.JceOutputStream.write(long[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object[], int):void
      com.qq.taf.jce.JceOutputStream.write(short[], int):void
      com.qq.taf.jce.JceOutputStream.write(boolean[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Collection, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceOutputStream.write(com.qq.taf.jce.JceStruct, int):void
     arg types: [com.tencent.assistant.protocol.jce.ListRecommendTop, int]
     candidates:
      com.qq.taf.jce.JceOutputStream.write(byte, int):void
      com.qq.taf.jce.JceOutputStream.write(double, int):void
      com.qq.taf.jce.JceOutputStream.write(float, int):void
      com.qq.taf.jce.JceOutputStream.write(int, int):void
      com.qq.taf.jce.JceOutputStream.write(long, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Byte, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Double, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Float, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Integer, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Long, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Short, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.String, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Collection, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void
      com.qq.taf.jce.JceOutputStream.write(short, int):void
      com.qq.taf.jce.JceOutputStream.write(boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(byte[], int):void
      com.qq.taf.jce.JceOutputStream.write(double[], int):void
      com.qq.taf.jce.JceOutputStream.write(float[], int):void
      com.qq.taf.jce.JceOutputStream.write(int[], int):void
      com.qq.taf.jce.JceOutputStream.write(long[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object[], int):void
      com.qq.taf.jce.JceOutputStream.write(short[], int):void
      com.qq.taf.jce.JceOutputStream.write(boolean[], int):void
      com.qq.taf.jce.JceOutputStream.write(com.qq.taf.jce.JceStruct, int):void */
    public void writeTo(JceOutputStream jceOutputStream) {
        jceOutputStream.write(this.f2228a, 0);
        jceOutputStream.write(this.b, 1);
        if (this.c != null) {
            jceOutputStream.write((JceStruct) this.c, 2);
        }
        if (this.d != null) {
            jceOutputStream.write((Collection) this.d, 3);
        }
        if (this.e != null) {
            jceOutputStream.write((JceStruct) this.e, 4);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
     arg types: [com.tencent.assistant.protocol.jce.ListRecommendIIT, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.ArrayList<com.tencent.assistant.protocol.jce.RecommendIT>, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
     arg types: [com.tencent.assistant.protocol.jce.ListRecommendTop, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct */
    public void readFrom(JceInputStream jceInputStream) {
        this.f2228a = jceInputStream.read(this.f2228a, 0, true);
        this.b = jceInputStream.read(this.b, 1, true);
        if (f == null) {
            f = new ListRecommendIIT();
        }
        this.c = (ListRecommendIIT) jceInputStream.read((JceStruct) f, 2, false);
        if (g == null) {
            g = new ArrayList<>();
            g.add(new RecommendIT());
        }
        this.d = (ArrayList) jceInputStream.read((Object) g, 3, false);
        if (h == null) {
            h = new ListRecommendTop();
        }
        this.e = (ListRecommendTop) jceInputStream.read((JceStruct) h, 4, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.display(com.qq.taf.jce.JceStruct, java.lang.String):com.qq.taf.jce.JceDisplayer
     arg types: [com.tencent.assistant.protocol.jce.ListRecommendIIT, java.lang.String]
     candidates:
      com.qq.taf.jce.JceDisplayer.display(byte, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(char, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(double, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(float, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(int, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(long, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.Object, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.String, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.util.Collection, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.util.Map, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(short, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(boolean, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(byte[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(char[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(double[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(float[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(int[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(long[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.Object[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(short[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(com.qq.taf.jce.JceStruct, java.lang.String):com.qq.taf.jce.JceDisplayer */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.display(java.util.Collection, java.lang.String):com.qq.taf.jce.JceDisplayer
     arg types: [java.util.ArrayList<com.tencent.assistant.protocol.jce.RecommendIT>, java.lang.String]
     candidates:
      com.qq.taf.jce.JceDisplayer.display(byte, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(char, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(double, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(float, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(int, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(long, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(com.qq.taf.jce.JceStruct, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.Object, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.String, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.util.Map, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(short, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(boolean, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(byte[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(char[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(double[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(float[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(int[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(long[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.Object[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(short[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.util.Collection, java.lang.String):com.qq.taf.jce.JceDisplayer */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.display(com.qq.taf.jce.JceStruct, java.lang.String):com.qq.taf.jce.JceDisplayer
     arg types: [com.tencent.assistant.protocol.jce.ListRecommendTop, java.lang.String]
     candidates:
      com.qq.taf.jce.JceDisplayer.display(byte, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(char, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(double, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(float, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(int, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(long, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.Object, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.String, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.util.Collection, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.util.Map, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(short, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(boolean, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(byte[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(char[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(double[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(float[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(int[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(long[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.Object[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(short[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(com.qq.taf.jce.JceStruct, java.lang.String):com.qq.taf.jce.JceDisplayer */
    public void display(StringBuilder sb, int i2) {
        JceDisplayer jceDisplayer = new JceDisplayer(sb, i2);
        jceDisplayer.display(this.f2228a, SocialConstants.PARAM_TYPE);
        jceDisplayer.display(this.b, "majorImageType");
        jceDisplayer.display((JceStruct) this.c, "listRecommendIIT");
        jceDisplayer.display((Collection) this.d, "listRecommendITIT");
        jceDisplayer.display((JceStruct) this.e, "listRecommendtop");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer
     arg types: [byte, int]
     candidates:
      com.qq.taf.jce.JceDisplayer.displaySimple(char, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(boolean, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer
     arg types: [com.tencent.assistant.protocol.jce.ListRecommendIIT, int]
     candidates:
      com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(boolean, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer
     arg types: [java.util.ArrayList<com.tencent.assistant.protocol.jce.RecommendIT>, int]
     candidates:
      com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(boolean, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer
     arg types: [com.tencent.assistant.protocol.jce.ListRecommendTop, int]
     candidates:
      com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(boolean, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer */
    public void displaySimple(StringBuilder sb, int i2) {
        JceDisplayer jceDisplayer = new JceDisplayer(sb, i2);
        jceDisplayer.displaySimple(this.f2228a, true);
        jceDisplayer.displaySimple(this.b, true);
        jceDisplayer.displaySimple((JceStruct) this.c, true);
        jceDisplayer.displaySimple((Collection) this.d, true);
        jceDisplayer.displaySimple((JceStruct) this.e, false);
    }
}
