package com.tencent.assistant.protocol.a.b;

import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.Global;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.module.nac.d;
import com.tencent.assistant.module.nac.f;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.protocol.a.c;
import com.tencent.assistant.protocol.jce.Net;
import com.tencent.assistant.protocol.jce.RspHead;
import com.tencent.assistant.protocol.jce.StatApiInvoking;
import com.tencent.assistant.protocol.jce.StatCSChannelData;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.r;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

/* compiled from: ProGuard */
public class b extends c implements NetworkMonitor.ConnectivityChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private DefaultHttpClient f1960a;

    public b() {
        cq.a().a(this);
    }

    public void a(RspHead rspHead) {
        if (rspHead != null) {
            if (rspHead.d != null) {
                d.a().a(rspHead.d);
            }
            if (!TextUtils.isEmpty(rspHead.f)) {
                Global.setPhoneGuid(rspHead.f);
            }
            if (rspHead.c > 0) {
                Global.setAreacode(rspHead.c);
            }
            if (!TextUtils.isEmpty(rspHead.g)) {
                Global.setClientIp(rspHead.g);
            }
            if (rspHead.i != null) {
                com.tencent.assistant.login.d.a().a(rspHead.i);
            }
        }
    }

    public synchronized DefaultHttpClient a() {
        if (this.f1960a == null) {
            this.f1960a = com.tencent.assistant.protocol.c.a();
        }
        return this.f1960a;
    }

    public void onConnected(APN apn) {
        g();
    }

    public void onDisconnected(APN apn) {
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
        g();
    }

    public synchronized void g() {
        if (this.f1960a != null) {
            com.tencent.assistant.protocol.c.a((HttpClient) this.f1960a);
            com.tencent.assistant.protocol.c.b((HttpClient) this.f1960a);
        } else {
            try {
                this.f1960a = a();
            } catch (OutOfMemoryError e) {
                System.gc();
                try {
                    Thread.sleep(50);
                } catch (Exception e2) {
                }
                this.f1960a = a();
            }
        }
        return;
    }

    public com.tencent.assistant.protocol.a.a.b b() {
        int i;
        byte b = 0;
        f a2 = d.a().a(1);
        if (a2 == null) {
            return null;
        }
        if (a2.f1851a != null) {
            i = r.a(a2.f1851a.f2215a);
        } else {
            i = 0;
        }
        byte b2 = -1;
        if (a2.b != null) {
            b2 = (byte) a2.b.ordinal();
        }
        com.tencent.assistant.net.b h = com.tencent.assistant.net.c.h();
        Net net = new Net();
        net.f2241a = h.f1925a.getIntValue();
        net.d = h.b;
        net.e = h.c;
        if (h.d) {
            b = 1;
        }
        net.f = b;
        net.b = i;
        net.c = b2;
        net.h = h.e;
        net.g = h.f;
        com.tencent.assistant.protocol.a.a.b bVar = new com.tencent.assistant.protocol.a.a.b();
        bVar.f1958a = net;
        bVar.c = a2.c;
        bVar.b = a2.a();
        return bVar;
    }

    public void a(boolean z, long j) {
        d.a().a(1, z, j);
    }

    public com.tencent.assistant.protocol.a.a.c c() {
        com.tencent.assistant.protocol.a.a.c cVar = new com.tencent.assistant.protocol.a.a.c();
        cVar.f1959a = AstApp.i().l() ? (byte) 1 : 2;
        cVar.b = Global.getMoloDeviceId();
        cVar.c = Global.getAreacode();
        cVar.d = Global.getPhoneGuid();
        cVar.e = Global.getQUA();
        cVar.f = Global.getPhoneTerminal();
        cVar.g = com.tencent.assistant.login.d.a().g();
        cVar.h = com.tencent.assistant.st.f.c();
        cVar.i = com.tencent.assistant.st.f.a();
        cVar.j = com.tencent.assistant.st.f.d();
        cVar.k = Global.getTerminalExtra();
        return cVar;
    }

    public String d() {
        return Global.HTTP_USER_AGENT;
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0057 A[SYNTHETIC, Splitter:B:26:0x0057] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x006a A[SYNTHETIC, Splitter:B:35:0x006a] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x006f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.tencent.assistant.protocol.a.a.a r11) {
        /*
            r10 = this;
            r8 = 0
            if (r11 != 0) goto L_0x0004
        L_0x0003:
            return
        L_0x0004:
            int r1 = r11.f1957a
            java.lang.Exception r9 = r11.d
            int r2 = r11.b
            long r3 = r11.c
            java.lang.String r5 = r11.e
            com.tencent.assistant.protocol.jce.Net r6 = r11.f
            java.util.List<java.lang.Integer> r7 = r11.g
            r0 = r10
            com.tencent.assistant.protocol.jce.StatApiInvoking r3 = r0.a(r1, r2, r3, r5, r6, r7)
            if (r1 == 0) goto L_0x0043
            r0 = -800(0xfffffffffffffce0, float:NaN)
            if (r1 == r0) goto L_0x0043
            if (r9 == 0) goto L_0x0043
            java.io.StringWriter r2 = new java.io.StringWriter     // Catch:{ Exception -> 0x0050, all -> 0x0065 }
            r2.<init>()     // Catch:{ Exception -> 0x0050, all -> 0x0065 }
            java.io.PrintWriter r1 = new java.io.PrintWriter     // Catch:{ Exception -> 0x0080, all -> 0x0078 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0080, all -> 0x0078 }
            r9.printStackTrace(r1)     // Catch:{ Exception -> 0x0084, all -> 0x007b }
            r2.flush()     // Catch:{ Exception -> 0x0084, all -> 0x007b }
            java.lang.String r0 = r2.toString()     // Catch:{ Exception -> 0x0084, all -> 0x007b }
            byte[] r0 = r0.getBytes()     // Catch:{ Exception -> 0x0084, all -> 0x007b }
            r3.g = r0     // Catch:{ Exception -> 0x0084, all -> 0x007b }
            if (r2 == 0) goto L_0x003e
            r2.close()     // Catch:{ IOException -> 0x004b }
        L_0x003e:
            if (r1 == 0) goto L_0x0043
            r1.close()
        L_0x0043:
            com.tencent.assistantv2.st.business.a r0 = com.tencent.assistantv2.st.business.a.a()
            r0.a(r3)
            goto L_0x0003
        L_0x004b:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x003e
        L_0x0050:
            r0 = move-exception
            r1 = r8
        L_0x0052:
            r0.printStackTrace()     // Catch:{ all -> 0x007d }
            if (r8 == 0) goto L_0x005a
            r8.close()     // Catch:{ IOException -> 0x0060 }
        L_0x005a:
            if (r1 == 0) goto L_0x0043
            r1.close()
            goto L_0x0043
        L_0x0060:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x005a
        L_0x0065:
            r0 = move-exception
            r1 = r8
            r2 = r8
        L_0x0068:
            if (r2 == 0) goto L_0x006d
            r2.close()     // Catch:{ IOException -> 0x0073 }
        L_0x006d:
            if (r1 == 0) goto L_0x0072
            r1.close()
        L_0x0072:
            throw r0
        L_0x0073:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x006d
        L_0x0078:
            r0 = move-exception
            r1 = r8
            goto L_0x0068
        L_0x007b:
            r0 = move-exception
            goto L_0x0068
        L_0x007d:
            r0 = move-exception
            r2 = r8
            goto L_0x0068
        L_0x0080:
            r0 = move-exception
            r1 = r8
            r8 = r2
            goto L_0x0052
        L_0x0084:
            r0 = move-exception
            r8 = r2
            goto L_0x0052
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.protocol.a.b.b.a(com.tencent.assistant.protocol.a.a.a):void");
    }

    private StatApiInvoking a(int i, int i2, long j, String str, Net net, List<Integer> list) {
        StatApiInvoking statApiInvoking = new StatApiInvoking();
        statApiInvoking.f2349a = i2;
        statApiInvoking.b = m.a().A() + j;
        statApiInvoking.c = System.currentTimeMillis() + m.a().A();
        statApiInvoking.d = str;
        statApiInvoking.e = Global.getClientIp();
        statApiInvoking.f = (short) i;
        com.tencent.assistant.net.b h = com.tencent.assistant.net.c.h();
        statApiInvoking.k = net.f2241a;
        statApiInvoking.h = h.b;
        statApiInvoking.i = h.c;
        statApiInvoking.j = (byte) (h.d ? 1 : 0);
        if (list != null) {
            statApiInvoking.l = new ArrayList<>(list);
        }
        return statApiInvoking;
    }

    public String e() {
        return Global.getPhoneGuid();
    }

    public void a(StatCSChannelData statCSChannelData) {
        TemporaryThreadManager.get().start(new c(this, statCSChannelData));
    }

    public int f() {
        if (com.tencent.assistant.net.c.d()) {
            return 1;
        }
        if (com.tencent.assistant.net.c.f()) {
            return 2;
        }
        if (com.tencent.assistant.net.c.e()) {
            return 3;
        }
        return -1;
    }
}
