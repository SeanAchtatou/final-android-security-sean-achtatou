package com.tencent.assistant.protocol.scu;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.protocol.a.b;
import com.tencent.assistant.protocol.a.e;
import java.util.List;

/* compiled from: ProGuard */
public class n {

    /* renamed from: a  reason: collision with root package name */
    b f2445a = e.a().k();

    public boolean a(List<JceStruct> list) {
        if (list == null) {
            return false;
        }
        if (this.f2445a == null) {
            return false;
        }
        for (JceStruct a2 : list) {
            if (this.f2445a.a(k.a(a2))) {
                return true;
            }
        }
        return false;
    }
}
