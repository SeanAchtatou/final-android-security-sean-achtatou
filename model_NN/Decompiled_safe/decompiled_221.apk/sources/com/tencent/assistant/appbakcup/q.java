package com.tencent.assistant.appbakcup;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Message;
import com.qq.AppService.AstApp;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.module.aw;
import com.tencent.assistant.module.update.k;
import com.tencent.assistant.protocol.jce.BackupApp;
import com.tencent.assistant.protocol.jce.BackupDevice;
import com.tencent.assistant.protocol.jce.GetBackupAppsRequest;
import com.tencent.assistant.protocol.jce.GetBackupAppsResponse;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class q extends aw {

    /* renamed from: a  reason: collision with root package name */
    private int f842a;
    private ArrayList<BackupApp> b;
    private int c;

    public int a(BackupDevice backupDevice) {
        if (this.f842a != -1) {
            cancel(this.f842a);
        }
        GetBackupAppsRequest getBackupAppsRequest = new GetBackupAppsRequest();
        getBackupAppsRequest.a(1);
        getBackupAppsRequest.a(backupDevice);
        getBackupAppsRequest.a(k.a(false));
        this.f842a = send(getBackupAppsRequest);
        return this.f842a;
    }

    public ArrayList<BackupApp> a() {
        return this.b;
    }

    public int b() {
        return this.c;
    }

    public boolean c() {
        if (this.b == null) {
            return true;
        }
        return this.b.isEmpty();
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        GetBackupAppsResponse getBackupAppsResponse = (GetBackupAppsResponse) jceStruct2;
        ArrayList<BackupApp> b2 = getBackupAppsResponse.b();
        ArrayList<BackupApp> arrayList = new ArrayList<>();
        PackageManager packageManager = AstApp.i().getPackageManager();
        if (!(packageManager == null || b2 == null || b2.isEmpty())) {
            for (BackupApp next : b2) {
                PackageInfo packageInfo = null;
                try {
                    packageInfo = packageManager.getPackageInfo(next.d, 0);
                } catch (Exception e) {
                }
                if (packageInfo == null || packageInfo.versionCode != next.j) {
                    arrayList.add(next);
                }
            }
        }
        this.b = arrayList;
        this.c = getBackupAppsResponse.c();
        Message obtainMessage = AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_GET_BACKUP_APPS_SUCCESS);
        obtainMessage.arg1 = this.f842a;
        AstApp.i().j().sendMessage(obtainMessage);
        this.f842a = -1;
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        if (this.b != null) {
            this.b.clear();
        }
        Message obtainMessage = AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_GET_BACKUP_APPS_FAIL);
        obtainMessage.arg1 = this.f842a;
        obtainMessage.arg2 = i2;
        AstApp.i().j().sendMessage(obtainMessage);
        this.f842a = -1;
    }
}
