package com.tencent.assistant.db.table;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.helper.AstDbHelper;
import com.tencent.assistant.db.helper.SQLiteDatabaseWrapper;
import com.tencent.assistant.db.helper.SqliteHelper;
import com.tencent.assistant.model.a.q;

/* compiled from: ProGuard */
public class ac implements IBaseTable {
    public synchronized boolean a(q qVar) {
        int i;
        boolean z = true;
        boolean z2 = false;
        synchronized (this) {
            if (qVar != null) {
                SQLiteDatabaseWrapper writableDatabaseWrapper = getHelper().getWritableDatabaseWrapper();
                ContentValues contentValues = new ContentValues();
                contentValues.put("card_type", Integer.valueOf(qVar.f1650a));
                contentValues.put("card_id", Integer.valueOf(qVar.b));
                contentValues.put("is_show", Integer.valueOf(qVar.c ? 1 : 0));
                if (qVar.d) {
                    i = 1;
                } else {
                    i = 0;
                }
                contentValues.put("is_close", Integer.valueOf(i));
                contentValues.put("record_time", Long.valueOf(qVar.e));
                if (((int) (writableDatabaseWrapper.insert("smart_card_show_record_table", null, contentValues) + ((long) 0))) <= 0) {
                    z = false;
                }
                z2 = z;
            }
        }
        return z2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0073 A[SYNTHETIC, Splitter:B:20:0x0073] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.List<com.tencent.assistant.model.a.q> a() {
        /*
            r13 = this;
            r1 = 0
            r3 = 0
            r2 = 1
            monitor-enter(r13)
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ all -> 0x008c }
            r4.<init>()     // Catch:{ all -> 0x008c }
            com.tencent.assistant.db.helper.SqliteHelper r0 = r13.getHelper()     // Catch:{ all -> 0x008c }
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r0 = r0.getReadableDatabaseWrapper()     // Catch:{ all -> 0x008c }
            java.lang.String r5 = "select * from smart_card_show_record_table"
            r6 = 0
            android.database.Cursor r1 = r0.rawQuery(r5, r6)     // Catch:{ Exception -> 0x007c }
            if (r1 == 0) goto L_0x0071
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x007c }
            if (r0 == 0) goto L_0x0071
            java.lang.String r0 = "card_type"
            int r5 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x007c }
            java.lang.String r0 = "card_id"
            int r6 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x007c }
            java.lang.String r0 = "is_show"
            int r7 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x007c }
            java.lang.String r0 = "is_close"
            int r8 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x007c }
            java.lang.String r0 = "record_time"
            int r9 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x007c }
        L_0x003e:
            com.tencent.assistant.model.a.q r10 = new com.tencent.assistant.model.a.q     // Catch:{ Exception -> 0x007c }
            r10.<init>()     // Catch:{ Exception -> 0x007c }
            int r0 = r1.getInt(r5)     // Catch:{ Exception -> 0x007c }
            r10.f1650a = r0     // Catch:{ Exception -> 0x007c }
            int r0 = r1.getInt(r6)     // Catch:{ Exception -> 0x007c }
            r10.b = r0     // Catch:{ Exception -> 0x007c }
            int r0 = r1.getInt(r7)     // Catch:{ Exception -> 0x007c }
            if (r0 != r2) goto L_0x0078
            r0 = r2
        L_0x0056:
            r10.c = r0     // Catch:{ Exception -> 0x007c }
            int r0 = r1.getInt(r8)     // Catch:{ Exception -> 0x007c }
            if (r0 != r2) goto L_0x007a
            r0 = r2
        L_0x005f:
            r10.d = r0     // Catch:{ Exception -> 0x007c }
            int r0 = r1.getInt(r9)     // Catch:{ Exception -> 0x007c }
            long r11 = (long) r0     // Catch:{ Exception -> 0x007c }
            r10.e = r11     // Catch:{ Exception -> 0x007c }
            r4.add(r10)     // Catch:{ Exception -> 0x007c }
            boolean r0 = r1.moveToNext()     // Catch:{ Exception -> 0x007c }
            if (r0 != 0) goto L_0x003e
        L_0x0071:
            if (r1 == 0) goto L_0x0076
            r1.close()     // Catch:{ all -> 0x008c }
        L_0x0076:
            monitor-exit(r13)
            return r4
        L_0x0078:
            r0 = r3
            goto L_0x0056
        L_0x007a:
            r0 = r3
            goto L_0x005f
        L_0x007c:
            r0 = move-exception
            java.lang.String r2 = "SmartCardShowRecordTable"
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x008f }
            com.tencent.assistant.utils.XLog.d(r2, r0)     // Catch:{ all -> 0x008f }
            if (r1 == 0) goto L_0x0076
            r1.close()     // Catch:{ all -> 0x008c }
            goto L_0x0076
        L_0x008c:
            r0 = move-exception
            monitor-exit(r13)
            throw r0
        L_0x008f:
            r0 = move-exception
            if (r1 == 0) goto L_0x0095
            r1.close()     // Catch:{ all -> 0x008c }
        L_0x0095:
            throw r0     // Catch:{ all -> 0x008c }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.ac.a():java.util.List");
    }

    public synchronized int a(long j) {
        return getHelper().getWritableDatabaseWrapper().delete("smart_card_show_record_table", "record_time < ?", new String[]{Long.toString(j / 1000)}) + 0;
    }

    public int tableVersion() {
        return 1;
    }

    public String tableName() {
        return "smart_card_show_record_table";
    }

    public String createTableSQL() {
        return "CREATE TABLE if not exists smart_card_show_record_table( _id INTEGER PRIMARY KEY AUTOINCREMENT, card_type INTEGER, card_id INTEGER, is_show INTEGER, is_close INTEGER, record_time INTEGER); ";
    }

    public String[] getAlterSQL(int i, int i2) {
        if (i2 != 6) {
            return null;
        }
        return new String[]{"CREATE TABLE if not exists smart_card_show_record_table( _id INTEGER PRIMARY KEY AUTOINCREMENT, card_type INTEGER, card_id INTEGER, is_show INTEGER, is_close INTEGER, record_time INTEGER); "};
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public SqliteHelper getHelper() {
        return AstDbHelper.get(AstApp.i());
    }
}
