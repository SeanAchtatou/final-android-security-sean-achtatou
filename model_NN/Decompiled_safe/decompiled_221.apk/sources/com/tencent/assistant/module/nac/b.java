package com.tencent.assistant.module.nac;

import com.tencent.assistant.Global;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

/* compiled from: ProGuard */
public class b {

    /* renamed from: a  reason: collision with root package name */
    public HashMap<String, c> f1847a = new HashMap<>();

    public b() {
        this.f1847a.put(Global.getServerAddress(), new c(this, 1, Global.getServerAddress(), 80, null, new ArrayList(), new ArrayList()));
    }

    public short a(String str) {
        try {
            c cVar = this.f1847a.get(new URL(str).getHost());
            if (cVar != null) {
                return cVar.f1848a;
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return -1;
    }
}
