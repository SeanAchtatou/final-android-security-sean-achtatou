package com.tencent.assistant.module;

import com.qq.AppService.AstApp;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.d.b;
import com.tencent.assistant.d.e;
import com.tencent.assistant.module.callback.ah;
import com.tencent.assistant.protocol.jce.GetSmartCardsRequest;
import com.tencent.assistant.protocol.jce.GetSmartCardsResponse;
import com.tencent.assistant.protocol.jce.LbsData;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
public class fn extends BaseEngine<ah> {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public b f1825a;
    /* access modifiers changed from: private */
    public LbsData b;
    private e c;

    public fn() {
        this.f1825a = null;
        this.c = new fr(this);
        this.f1825a = new b(AstApp.i().getApplicationContext(), this.c);
    }

    public int a(int i) {
        TemporaryThreadManager.get().start(new fo(this));
        GetSmartCardsRequest getSmartCardsRequest = new GetSmartCardsRequest();
        getSmartCardsRequest.f2167a = i;
        getSmartCardsRequest.b = this.b;
        return send(getSmartCardsRequest);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChanged(new fp(this, i, (GetSmartCardsResponse) jceStruct2));
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChanged(new fq(this, i, i2));
    }
}
