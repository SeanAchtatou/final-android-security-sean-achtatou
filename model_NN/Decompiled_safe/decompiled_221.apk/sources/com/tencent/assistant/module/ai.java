package com.tencent.assistant.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.callback.ab;
import com.tencent.assistant.protocol.jce.SuggestRequest;
import com.tencent.assistant.protocol.jce.SuggestResponse;

/* compiled from: ProGuard */
public class ai extends BaseEngine<ab> {

    /* renamed from: a  reason: collision with root package name */
    private int f1693a = -1;

    public void a() {
        cancel(this.f1693a);
        this.f1693a = -1;
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        if (jceStruct2 != null && i == this.f1693a) {
            SuggestResponse suggestResponse = (SuggestResponse) jceStruct2;
            notifyDataChangedInMainThread(new aj(this, u.b(suggestResponse.c), suggestResponse));
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChangedInMainThread(new ak(this, i2));
    }

    public int a(String str) {
        SuggestRequest suggestRequest = new SuggestRequest();
        suggestRequest.f2385a = str;
        this.f1693a = send(suggestRequest);
        return this.f1693a;
    }

    public void a(int i) {
        super.cancel(i);
        this.f1693a = -1;
    }
}
