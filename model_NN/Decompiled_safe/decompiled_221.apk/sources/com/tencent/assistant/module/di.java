package com.tencent.assistant.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.Global;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.as;
import com.tencent.assistant.model.QuickEntranceNotify;
import com.tencent.assistant.protocol.jce.GetManageInfoListRequest;
import com.tencent.assistant.protocol.jce.GetManageInfoListResponse;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

/* compiled from: ProGuard */
public class di extends aw implements eb {
    private static di d;

    /* renamed from: a  reason: collision with root package name */
    protected ReferenceQueue<dk> f1775a;
    protected ConcurrentLinkedQueue<WeakReference<dk>> b;
    private String c = "GetManageIconEngine";

    public static synchronized di a() {
        di diVar;
        synchronized (di.class) {
            if (d == null) {
                d = new di();
            }
            diVar = d;
        }
        return diVar;
    }

    public di() {
        dy.a().a(this);
        this.f1775a = new ReferenceQueue<>();
        this.b = new ConcurrentLinkedQueue<>();
    }

    public void a(dk dkVar) {
        if (dkVar != null) {
            while (true) {
                Reference<? extends dk> poll = this.f1775a.poll();
                if (poll == null) {
                    break;
                }
                this.b.remove(poll);
            }
            Iterator<WeakReference<dk>> it = this.b.iterator();
            while (it.hasNext()) {
                if (((dk) it.next().get()) == dkVar) {
                    return;
                }
            }
            this.b.add(new WeakReference(dkVar, this.f1775a));
        }
    }

    public void b(dk dkVar) {
        if (dkVar != null) {
            Iterator<WeakReference<dk>> it = this.b.iterator();
            while (it.hasNext()) {
                WeakReference next = it.next();
                if (((dk) next.get()) == dkVar) {
                    this.b.remove(next);
                    return;
                }
            }
        }
    }

    public void b() {
        TemporaryThreadManager.get().start(new dj(this));
    }

    /* access modifiers changed from: private */
    public boolean c() {
        GetManageInfoListResponse d2 = as.w().d();
        if (d2 == null) {
            return false;
        }
        long a2 = m.a().a((byte) 17);
        long j = d2.c;
        if (m.a().a("key_g_m_i_appver", -1) == Global.getAppVersionCode() && a2 != -1 && a2 <= j) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: private */
    public int e() {
        GetManageInfoListRequest getManageInfoListRequest = new GetManageInfoListRequest();
        int appVersionCode = Global.getAppVersionCode();
        getManageInfoListRequest.f2135a = appVersionCode;
        XLog.d("jeson", "sendRequest baoVersion = " + appVersionCode);
        return send(getManageInfoListRequest);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        if (jceStruct2 != null) {
            GetManageInfoListResponse getManageInfoListResponse = (GetManageInfoListResponse) jceStruct2;
            if (getManageInfoListResponse.f2136a == 0) {
                m.a().b("key_g_m_i_appver", Integer.valueOf(Global.getAppVersionCode()));
                as.w().a(getManageInfoListResponse);
                a(getManageInfoListResponse.c);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        XLog.e("jeson", "GetManageInfoListRequest onRequestFailed ; errorCode=" + i2);
    }

    public void d() {
        GetManageInfoListResponse d2 = as.w().d();
        if (d2 != null) {
            long a2 = m.a().a((byte) 17);
            long j = d2.c;
            if (a2 == -1 || a2 > j) {
                e();
                return;
            }
            return;
        }
        e();
    }

    public void a(ArrayList<QuickEntranceNotify> arrayList) {
    }

    /* access modifiers changed from: protected */
    public void a(long j) {
        Iterator<WeakReference<dk>> it = this.b.iterator();
        while (it.hasNext()) {
            dk dkVar = (dk) it.next().get();
            if (dkVar != null) {
                dkVar.a(j);
            }
        }
    }
}
