package com.tencent.assistant.module.wisedownload.condition;

import com.qq.ndk.NativeFileObject;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.module.wisedownload.b;
import com.tencent.assistant.module.wisedownload.condition.ThresholdCondition;
import com.tencent.assistant.module.wisedownload.r;
import com.tencent.assistant.module.wisedownload.t;
import com.tencent.assistant.protocol.jce.AutoDownloadCfg;
import com.tencent.assistant.utils.cv;
import com.tencent.assistant.utils.e;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class i extends ThresholdCondition {
    private int c;
    private int d;
    private int e;
    private int f;
    private long g;

    public i(b bVar) {
        a(bVar);
    }

    public void a(b bVar) {
        AutoDownloadCfg j;
        if (bVar != null && (j = bVar.j()) != null) {
            this.c = j.c;
            this.d = j.h;
            this.e = j.i;
            this.f = j.j;
            this.g = h();
        }
    }

    public boolean a() {
        a(ThresholdCondition.CONDITION_RESULT_CODE.OK);
        if (!o()) {
            return false;
        }
        return b();
    }

    private boolean o() {
        long f2 = r.f();
        if (f2 == 0) {
            a(ThresholdCondition.CONDITION_RESULT_CODE.FAIL_OTHER_NO_APP);
            return false;
        }
        boolean a2 = a(f2 + ((long) (this.c * NativeFileObject.S_IFREG)));
        if (a2) {
            return a2;
        }
        a(ThresholdCondition.CONDITION_RESULT_CODE.FAIL_OTHER_SPACE);
        return a2;
    }

    public boolean b() {
        int i;
        int i2;
        List<DownloadInfo> a2 = t.a(true, true);
        if (a2 == null || a2.isEmpty()) {
            i2 = 0;
            i = 0;
        } else {
            i2 = 0;
            i = 0;
            for (DownloadInfo next : a2) {
                if (next != null) {
                    if (next.downloadEndTime >= this.g && !e.a(next.packageName, next.versionCode)) {
                        i++;
                    }
                    if (cv.d(next.downloadEndTime)) {
                        i2++;
                    }
                }
                i = i;
                i2 = i2;
            }
        }
        if (i >= this.d) {
            a(ThresholdCondition.CONDITION_RESULT_CODE.FAIL_OTHER_PHONE);
        } else if (i2 >= this.e) {
            a(ThresholdCondition.CONDITION_RESULT_CODE.FAIL_OTHER_WEEK);
        }
        if (i >= this.d || i2 >= this.e) {
            return false;
        }
        return true;
    }

    public long h() {
        long currentTimeMillis = System.currentTimeMillis();
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(currentTimeMillis);
        instance.set(6, instance.get(6) - this.f);
        return instance.getTimeInMillis();
    }

    public boolean i() {
        long f2 = r.f();
        if (f2 == 0) {
            return true;
        }
        return a(f2 + ((long) (this.c * NativeFileObject.S_IFREG)));
    }

    public boolean j() {
        return r.f() != 0;
    }

    public int k() {
        return this.d;
    }

    public int l() {
        int i = 0;
        List<DownloadInfo> a2 = t.a(true, true);
        if (a2 == null || a2.isEmpty()) {
            return 0;
        }
        Iterator<DownloadInfo> it = a2.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            DownloadInfo next = it.next();
            if (next != null && next.downloadEndTime >= this.g && !e.a(next.packageName, next.versionCode)) {
                i2++;
            }
            i = i2;
        }
    }

    public int m() {
        return this.e;
    }

    public int n() {
        int i = 0;
        List<DownloadInfo> a2 = t.a(true, true);
        if (a2 == null || a2.isEmpty()) {
            return 0;
        }
        Iterator<DownloadInfo> it = a2.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            DownloadInfo next = it.next();
            if (next != null && cv.d(next.downloadEndTime)) {
                i2++;
            }
            i = i2;
        }
    }
}
