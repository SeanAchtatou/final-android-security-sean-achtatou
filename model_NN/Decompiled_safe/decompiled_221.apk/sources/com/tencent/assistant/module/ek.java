package com.tencent.assistant.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.callback.k;
import com.tencent.assistant.protocol.jce.ModifyAppCommentRequest;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
public class ek extends BaseEngine<k> {
    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        ModifyAppCommentRequest modifyAppCommentRequest = (ModifyAppCommentRequest) jceStruct;
        XLog.d("comment", "ModifyAppCommentEngine.onRequestSuccessed, ModifyAppCommentRequest=" + modifyAppCommentRequest.toString());
        notifyDataChangedInMainThread(new el(this, i, modifyAppCommentRequest));
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        XLog.d("comment", "ModifyAppCommentEngine.onRequestFailed, errorCode=" + i2);
        notifyDataChangedInMainThread(new em(this, i, i2, (ModifyAppCommentRequest) jceStruct));
    }
}
