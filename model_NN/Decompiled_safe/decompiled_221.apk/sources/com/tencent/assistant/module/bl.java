package com.tencent.assistant.module;

import com.tencent.assistant.protocol.jce.GetCommentListResponse;
import java.util.ArrayList;

/* compiled from: ProGuard */
class bl implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GetCommentListResponse f1724a;
    final /* synthetic */ int b;
    final /* synthetic */ boolean c;
    final /* synthetic */ bi d;

    bl(bi biVar, GetCommentListResponse getCommentListResponse, int i, boolean z) {
        this.d = biVar;
        this.f1724a = getCommentListResponse;
        this.b = i;
        this.c = z;
    }

    public void run() {
        boolean z = true;
        byte[] unused = this.d.f = this.f1724a.d;
        bi biVar = this.d;
        if (this.f1724a.c != 1) {
            z = false;
        }
        boolean unused2 = biVar.e = z;
        ArrayList arrayList = new ArrayList();
        if (this.d.i) {
            if (this.f1724a.j != null) {
                arrayList.addAll(this.f1724a.j);
            }
        } else if (this.d.k != null) {
            arrayList.addAll(this.d.k);
        }
        this.d.notifyDataChanged(new bm(this, arrayList));
        if (this.d.e) {
            this.d.g.a(this.d.f);
        }
    }
}
