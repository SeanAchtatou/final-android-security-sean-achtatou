package com.tencent.assistant.module;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.model.SimpleAppModel;

/* compiled from: ProGuard */
class eq implements w {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ en f1803a;

    eq(en enVar) {
        this.f1803a = enVar;
    }

    public boolean a(SimpleAppModel simpleAppModel) {
        DownloadInfo a2 = DownloadProxy.a().a(simpleAppModel);
        AppConst.AppState d = u.d(simpleAppModel);
        return (a2 != null && a2.uiType == SimpleDownloadInfo.UIType.NORMAL) || d == AppConst.AppState.INSTALLED || d == AppConst.AppState.UPDATE;
    }
}
