package com.tencent.assistant.module.callback;

import com.tencent.assistant.module.callback.CallbackHelper;

/* compiled from: ProGuard */
class h implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CallbackHelper.Caller f1741a;
    final /* synthetic */ CallbackHelper b;

    h(CallbackHelper callbackHelper, CallbackHelper.Caller caller) {
        this.b = callbackHelper;
        this.f1741a = caller;
    }

    public void run() {
        this.b.broadcast(this.f1741a);
    }
}
