package com.tencent.assistant.module;

import com.tencent.assistant.module.callback.CallbackHelper;

/* compiled from: ProGuard */
class au implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CallbackHelper.Caller f1705a;
    final /* synthetic */ BaseEngine b;

    au(BaseEngine baseEngine, CallbackHelper.Caller caller) {
        this.b = baseEngine;
        this.f1705a = caller;
    }

    public void run() {
        this.b.notifyDataChanged(this.f1705a);
    }
}
