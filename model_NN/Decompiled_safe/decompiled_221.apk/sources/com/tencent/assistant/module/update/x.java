package com.tencent.assistant.module.update;

import com.tencent.assistant.m;
import com.tencent.assistant.protocol.jce.AutoDownloadInfo;
import com.tencent.assistant.protocol.jce.GetAutoDownloadRequest;
import com.tencent.assistant.utils.cv;
import java.util.ArrayList;

/* compiled from: ProGuard */
class x implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ u f1901a;

    x(u uVar) {
        this.f1901a = uVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    public void run() {
        boolean b = cv.b(m.a().a("app_update_refresh_suc_time", 0L));
        ArrayList a2 = this.f1901a.a(b);
        ArrayList c = this.f1901a.m();
        GetAutoDownloadRequest getAutoDownloadRequest = new GetAutoDownloadRequest();
        getAutoDownloadRequest.a(b ? (byte) 1 : 0);
        getAutoDownloadRequest.a(a2);
        getAutoDownloadRequest.b(c);
        getAutoDownloadRequest.a(this.f1901a.n());
        if (this.f1901a.c != null) {
            getAutoDownloadRequest.c(this.f1901a.a((ArrayList<AutoDownloadInfo>) this.f1901a.c.b()));
        }
        int unused = this.f1901a.b = this.f1901a.send(getAutoDownloadRequest);
    }
}
