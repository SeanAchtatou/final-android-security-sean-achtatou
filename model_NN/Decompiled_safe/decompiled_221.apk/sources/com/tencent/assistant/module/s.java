package com.tencent.assistant.module;

import com.tencent.assistant.manager.as;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.protocol.jce.GetAppListResponse;
import java.util.ArrayList;

/* compiled from: ProGuard */
class s implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ r f1856a;

    s(r rVar) {
        this.f1856a = rVar;
    }

    public void run() {
        boolean z = true;
        GetAppListResponse a2 = as.w().a(this.f1856a.f1855a.f1840a, this.f1856a.f1855a.b, this.f1856a.c);
        if (a2 == null || a2.e != this.f1856a.f1855a.f || a2.b == null || a2.b.size() <= 0) {
            int unused = this.f1856a.h = this.f1856a.f1855a.a(this.f1856a.h, this.f1856a.c);
            return;
        }
        r rVar = this.f1856a;
        long j = a2.e;
        ArrayList<SimpleAppModel> b = u.b(a2.b);
        if (a2.d != 1) {
            z = false;
        }
        rVar.a(j, b, z, a2.c);
    }
}
