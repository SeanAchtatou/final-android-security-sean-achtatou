package com.tencent.assistant.module;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.module.callback.af;
import com.tencent.assistant.protocol.jce.GetDesktopShortcutResponse;

/* compiled from: ProGuard */
class fl implements CallbackHelper.Caller<af> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1823a;
    final /* synthetic */ GetDesktopShortcutResponse b;
    final /* synthetic */ fk c;

    fl(fk fkVar, int i, GetDesktopShortcutResponse getDesktopShortcutResponse) {
        this.c = fkVar;
        this.f1823a = i;
        this.b = getDesktopShortcutResponse;
    }

    /* renamed from: a */
    public void call(af afVar) {
        afVar.a(this.f1823a, 0, this.b.a());
    }
}
