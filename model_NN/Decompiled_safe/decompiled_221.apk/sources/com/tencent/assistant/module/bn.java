package com.tencent.assistant.module;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.module.callback.k;
import com.tencent.assistant.protocol.jce.GetCommentListRequest;

/* compiled from: ProGuard */
class bn implements CallbackHelper.Caller<k> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1726a;
    final /* synthetic */ int b;
    final /* synthetic */ boolean c;
    final /* synthetic */ GetCommentListRequest d;
    final /* synthetic */ bi e;

    bn(bi biVar, int i, int i2, boolean z, GetCommentListRequest getCommentListRequest) {
        this.e = biVar;
        this.f1726a = i;
        this.b = i2;
        this.c = z;
        this.d = getCommentListRequest;
    }

    /* renamed from: a */
    public void call(k kVar) {
        kVar.a(this.f1726a, this.b, this.c, this.d.h, null, null, null, false, null, null);
    }
}
