package com.tencent.assistant.module;

import com.tencent.assistant.protocol.jce.SearchResponse;
import java.util.ArrayList;

/* compiled from: ProGuard */
class af implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ boolean f1690a;
    final /* synthetic */ ArrayList b;
    final /* synthetic */ int c;
    final /* synthetic */ SearchResponse d;
    final /* synthetic */ boolean e;
    final /* synthetic */ String f;
    final /* synthetic */ int g;
    final /* synthetic */ AppSearchResultEngine h;

    af(AppSearchResultEngine appSearchResultEngine, boolean z, ArrayList arrayList, int i, SearchResponse searchResponse, boolean z2, String str, int i2) {
        this.h = appSearchResultEngine;
        this.f1690a = z;
        this.b = arrayList;
        this.c = i;
        this.d = searchResponse;
        this.e = z2;
        this.f = str;
        this.g = i2;
    }

    public void run() {
        if (this.f1690a) {
            this.h.k.clear();
        }
        this.h.k.addAll(this.b);
        this.h.notifyDataChanged(new ag(this));
    }
}
