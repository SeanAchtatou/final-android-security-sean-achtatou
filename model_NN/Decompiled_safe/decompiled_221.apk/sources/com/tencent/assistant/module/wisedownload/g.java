package com.tencent.assistant.module.wisedownload;

import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.manager.SelfUpdateManager;
import com.tencent.assistant.module.update.u;
import com.tencent.assistant.module.wisedownload.condition.c;
import com.tencent.assistant.module.wisedownload.condition.k;
import com.tencent.assistant.protocol.jce.AutoDownloadInfo;
import java.io.File;
import java.util.List;

/* compiled from: ProGuard */
public class g extends b {
    public g() {
        a();
    }

    public void a() {
        this.d = new k(this);
    }

    /* access modifiers changed from: protected */
    public void c() {
        this.b = new c(this);
    }

    public boolean e() {
        return this.e;
    }

    public boolean b() {
        List<AutoDownloadInfo> g;
        if (k() || (g = u.a().g()) == null || g.isEmpty()) {
            return false;
        }
        AutoDownloadInfo autoDownloadInfo = g.get(0);
        DownloadInfo a2 = DownloadProxy.a().a(autoDownloadInfo.f2010a, autoDownloadInfo.d);
        if (a2 == null || a2.downloadState != SimpleDownloadInfo.DownloadState.SUCC) {
            return true;
        }
        return !new File(a2.getFilePath()).exists();
    }

    private boolean k() {
        if (SelfUpdateManager.a().d() == null) {
            return false;
        }
        if (SelfUpdateManager.a().d().z == 1) {
            return true;
        }
        return false;
    }

    public void a(o oVar) {
        boolean z;
        boolean z2 = false;
        if (oVar != null) {
            boolean e = e();
            if (this.d == null || !(this.d instanceof k)) {
                z = false;
            } else {
                k kVar = (k) this.d;
                z = kVar.b();
                z2 = kVar.h();
            }
            oVar.a(e, z, z2);
        }
    }
}
