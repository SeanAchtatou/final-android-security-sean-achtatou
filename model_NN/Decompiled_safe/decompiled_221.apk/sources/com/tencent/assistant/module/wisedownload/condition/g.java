package com.tencent.assistant.module.wisedownload.condition;

import com.tencent.assistant.module.wisedownload.b;
import com.tencent.assistant.module.wisedownload.condition.ThresholdCondition;

/* compiled from: ProGuard */
public class g {
    public static ThresholdCondition a(b bVar, ThresholdCondition.CONDITION_TYPE condition_type) {
        if (bVar == null) {
            return null;
        }
        switch (h.f1911a[condition_type.ordinal()]) {
            case 1:
                return new m(bVar);
            case 2:
                return new j(bVar);
            case 3:
                return new n(bVar);
            default:
                return null;
        }
    }
}
