package com.tencent.assistant.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.module.callback.u;
import com.tencent.assistant.protocol.jce.GetUserInfoRequest;
import com.tencent.assistant.protocol.jce.GetUserInfoResponse;

/* compiled from: ProGuard */
class ef implements CallbackHelper.Caller<u> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1793a;
    final /* synthetic */ JceStruct b;
    final /* synthetic */ GetUserInfoResponse c;
    final /* synthetic */ ee d;

    ef(ee eeVar, int i, JceStruct jceStruct, GetUserInfoResponse getUserInfoResponse) {
        this.d = eeVar;
        this.f1793a = i;
        this.b = jceStruct;
        this.c = getUserInfoResponse;
    }

    /* renamed from: a */
    public void call(u uVar) {
        uVar.a(this.f1793a, (GetUserInfoRequest) this.b, this.c);
    }
}
