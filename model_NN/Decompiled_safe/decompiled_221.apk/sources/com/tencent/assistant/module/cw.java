package com.tencent.assistant.module;

import android.text.TextUtils;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.callback.n;
import com.tencent.assistant.protocol.jce.GetDomainCapabilityRequest;
import com.tencent.assistant.protocol.jce.GetDomainCapabilityResponse;

/* compiled from: ProGuard */
public class cw extends BaseEngine<n> {
    public int a(String str) {
        if (TextUtils.isEmpty(str)) {
            return -1;
        }
        return send(new GetDomainCapabilityRequest(str));
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChangedInMainThread(new cx(this, (GetDomainCapabilityResponse) jceStruct2, i));
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChangedInMainThread(new cy(this, i, i2));
    }
}
