package com.tencent.assistant.utils;

import android.text.TextUtils;

/* compiled from: ProGuard */
public final class cd {

    /* renamed from: a  reason: collision with root package name */
    private String f2665a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private int h;
    private int i;
    private int j;
    private int k;
    private boolean l;

    public void a(String str) {
        this.f2665a = str;
    }

    public void b(String str) {
        this.b = str;
    }

    public void c(String str) {
        this.c = str;
    }

    public void d(String str) {
        this.d = str;
    }

    public void e(String str) {
        this.e = str;
    }

    public void f(String str) {
        this.f = str;
    }

    public void g(String str) {
        this.g = str;
    }

    public void a(int i2) {
        this.h = i2;
    }

    public void b(int i2) {
        this.i = i2;
    }

    public void c(int i2) {
        this.j = i2;
    }

    public void d(int i2) {
        this.k = i2;
    }

    public void a(boolean z) {
        this.l = z;
    }

    public String a() {
        String b2 = b();
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.f2665a);
        stringBuffer.append("_");
        stringBuffer.append(this.c);
        if (!TextUtils.isEmpty(this.e)) {
            stringBuffer.append("_");
            stringBuffer.append(this.e).append("_").append(this.d);
        }
        stringBuffer.append("/");
        stringBuffer.append(b2);
        stringBuffer.append("&NA/");
        stringBuffer.append(b2);
        stringBuffer.append("&");
        stringBuffer.append(this.g);
        stringBuffer.append("_");
        stringBuffer.append(this.j);
        stringBuffer.append("_");
        stringBuffer.append(this.k);
        stringBuffer.append("_");
        stringBuffer.append(this.l ? 1 : 0);
        stringBuffer.append("&");
        stringBuffer.append(this.i / 16);
        stringBuffer.append("_");
        stringBuffer.append(this.h / 16);
        stringBuffer.append("_");
        stringBuffer.append("14&");
        stringBuffer.append(this.b);
        stringBuffer.append("&");
        stringBuffer.append(this.f);
        stringBuffer.append("&");
        stringBuffer.append("NA");
        stringBuffer.append("&");
        stringBuffer.append("V3");
        return stringBuffer.toString();
    }

    private String b() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("0");
        stringBuffer.append(this.c.subSequence(0, 1));
        stringBuffer.append(this.d);
        return stringBuffer.toString();
    }
}
