package com.tencent.assistant.utils;

import android.widget.Toast;
import com.tencent.assistant.m;
import com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse;

/* compiled from: ProGuard */
class ca implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ boolean f2663a;
    final /* synthetic */ by b;

    ca(by byVar, boolean z) {
        this.b = byVar;
        this.f2663a = z;
    }

    public void run() {
        if (this.b.f2661a == null || !(this.b.f2661a.b == 1 || this.b.f2661a.b == 3)) {
            XLog.e("zhangyuanchao", "-------mRecoverAppList------:" + this.b.f2661a.b + "," + this.b.f2661a.c.size());
            if (this.f2663a) {
                GetPhoneUserAppListResponse getPhoneUserAppListResponse = (GetPhoneUserAppListResponse) bh.b(m.a().e("key_re_app_list_first_response"), GetPhoneUserAppListResponse.class);
                if (getPhoneUserAppListResponse != null) {
                    bx.a(this.b.b, false, this.b.c, getPhoneUserAppListResponse, this.b.d);
                } else {
                    Toast.makeText(this.b.b, "恭喜你，你已经恢复了所有的应用！", 4).show();
                }
            } else {
                Toast.makeText(this.b.b, "恭喜你，你已经安装了装机必备的应用！", 4).show();
            }
        } else {
            bx.a(this.b.b, true);
        }
    }
}
