package com.tencent.assistant.utils.a;

import java.util.LinkedHashMap;
import java.util.Map;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private String f2615a;
    private final LinkedHashMap<String, Long> b = new LinkedHashMap<>();

    public LinkedHashMap<String, Long> a() {
        return this.b;
    }

    public boolean b() {
        return !this.b.isEmpty();
    }

    public void a(String str) {
        this.f2615a = str;
        k kVar = new k();
        try {
            if (kVar.a(this.f2615a)) {
                for (Map.Entry next : kVar.b().entrySet()) {
                    this.b.put(next.getKey(), Long.valueOf((long) (((f) next.getValue()).i | ((f) next.getValue()).g)));
                }
            } else if (!this.b.isEmpty()) {
                this.b.clear();
            }
        } catch (Exception e) {
            this.b.clear();
        }
    }
}
