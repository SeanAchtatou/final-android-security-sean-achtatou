package com.tencent.assistant.utils;

import android.app.Dialog;
import android.view.inputmethod.InputMethodManager;

/* compiled from: ProGuard */
final class ae implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Dialog f2627a;

    ae(Dialog dialog) {
        this.f2627a = dialog;
    }

    public void run() {
        if (this.f2627a.getOwnerActivity() != null && !this.f2627a.getOwnerActivity().isFinishing() && this.f2627a.isShowing()) {
            try {
                ((InputMethodManager) this.f2627a.getOwnerActivity().getSystemService("input_method")).toggleSoftInput(0, 2);
            } catch (Throwable th) {
            }
        }
    }
}
