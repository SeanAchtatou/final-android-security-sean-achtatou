package com.tencent.assistant.utils.installuninstall;

import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class v extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ InstallUninstallTaskBean f2713a;
    final /* synthetic */ p b;

    v(p pVar, InstallUninstallTaskBean installUninstallTaskBean) {
        this.b = pVar;
        this.f2713a = installUninstallTaskBean;
    }

    public void onLeftBtnClick() {
        int i;
        k.a(new STInfoV2(STConst.ST_DIALOG_INSTALL_ROOT_AUTH_ALERT, "03_001", AstApp.m() != null ? AstApp.m().f() : 2000, STConst.ST_DEFAULT_SLOT, 200));
        if (this.f2713a.action == 1) {
            i = 0;
        } else {
            i = 1;
        }
        p.b(1, i);
        Toast.makeText(AstApp.i().getBaseContext(), AstApp.i().getBaseContext().getString(R.string.request_root_negative_click_toast), 1).show();
        if (this.f2713a.action == 1) {
            ac.a().a(this.f2713a);
        } else if (this.f2713a.action == -1 && this.f2713a.trySystemAfterSilentFail) {
            this.b.f2708a.sendMessage(this.b.f2708a.obtainMessage(1024, this.f2713a.packageName));
            InstallUninstallUtil.a(this.f2713a.packageName);
        }
    }

    public void onRightBtnClick() {
        int i;
        k.a(new STInfoV2(STConst.ST_DIALOG_INSTALL_ROOT_AUTH_ALERT, "03_002", AstApp.m() != null ? AstApp.m().f() : 2000, STConst.ST_DEFAULT_SLOT, 200));
        if (this.f2713a.action == 1) {
            i = 0;
        } else {
            i = 1;
        }
        p.b(2, i);
        p.e().post(new w(this));
    }

    public void onCancell() {
        int i;
        k.a(new STInfoV2(STConst.ST_DIALOG_INSTALL_ROOT_AUTH_ALERT, "03_001", AstApp.m() != null ? AstApp.m().f() : 2000, STConst.ST_DEFAULT_SLOT, 200));
        if (this.f2713a.action == 1) {
            i = 0;
        } else {
            i = 1;
        }
        p.b(1, i);
        try {
            Toast.makeText(AstApp.i().getBaseContext(), AstApp.i().getBaseContext().getString(R.string.request_root_negative_click_toast), 1).show();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        if (this.f2713a.action == 1) {
            ac.a().a(this.f2713a);
        } else if (this.f2713a.action == -1 && this.f2713a.trySystemAfterSilentFail) {
            this.b.f2708a.sendMessage(this.b.f2708a.obtainMessage(1024, this.f2713a.packageName));
            InstallUninstallUtil.a(this.f2713a.packageName);
        }
    }
}
