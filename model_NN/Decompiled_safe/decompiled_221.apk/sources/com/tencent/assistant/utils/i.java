package com.tencent.assistant.utils;

/* compiled from: ProGuard */
public class i {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ boolean f2681a = (!i.class.desiredAssertionStatus());

    public static byte[] a(String str, int i) {
        return a(str.getBytes(), i);
    }

    public static byte[] a(byte[] bArr, int i) {
        return a(bArr, 0, bArr.length, i);
    }

    public static byte[] a(byte[] bArr, int i, int i2, int i3) {
        k kVar = new k(i3, new byte[((i2 * 3) / 4)]);
        if (!kVar.a(bArr, i, i2, true)) {
            throw new IllegalArgumentException("bad base-64");
        } else if (kVar.b == kVar.f2718a.length) {
            return kVar.f2718a;
        } else {
            byte[] bArr2 = new byte[kVar.b];
            System.arraycopy(kVar.f2718a, 0, bArr2, 0, kVar.b);
            return bArr2;
        }
    }

    private i() {
    }
}
