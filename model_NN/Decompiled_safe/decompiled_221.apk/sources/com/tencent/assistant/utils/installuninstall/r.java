package com.tencent.assistant.utils.installuninstall;

import com.tencent.assistant.download.DownloadInfo;
import java.util.ArrayList;

/* compiled from: ProGuard */
class r implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ArrayList f2710a;
    final /* synthetic */ boolean b;
    final /* synthetic */ p c;

    r(p pVar, ArrayList arrayList, boolean z) {
        this.c = pVar;
        this.f2710a = arrayList;
        this.b = z;
    }

    public void run() {
        if (this.f2710a != null && !this.f2710a.isEmpty()) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.f2710a.size()) {
                    DownloadInfo downloadInfo = (DownloadInfo) this.f2710a.get(i2);
                    this.c.a(downloadInfo.downloadTicket, downloadInfo.packageName, downloadInfo.name, downloadInfo.getFilePath(), downloadInfo.versionCode, downloadInfo.signatrue, downloadInfo.downloadTicket, downloadInfo.fileSize, this.b, downloadInfo);
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }
}
