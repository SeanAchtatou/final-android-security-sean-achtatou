package com.tencent.assistant.utils;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.homeEntry.HomeEntryCell1;
import com.tencent.assistant.component.homeEntry.HomeEntryCell2;
import com.tencent.assistant.component.homeEntry.HomeEntryCellBase;
import com.tencent.assistant.component.homeEntry.HomeEntryTemplateView;
import com.tencent.assistant.protocol.jce.EntranceBlock;
import com.tencent.assistant.protocol.jce.EntranceTemplate;
import com.tencent.assistantv2.manager.f;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class bc {
    private static HomeEntryCellBase a(Context context, EntranceBlock entranceBlock) {
        if (entranceBlock.f2065a == 1) {
            return new HomeEntryCell1(context, entranceBlock);
        }
        if (entranceBlock.f2065a == 2) {
            return new HomeEntryCell2(context, entranceBlock);
        }
        return null;
    }

    public static HomeEntryTemplateView a(Context context, HomeEntryTemplateView homeEntryTemplateView) {
        if (homeEntryTemplateView == null || a(homeEntryTemplateView)) {
            XLog.v("home_entry", "factory--getHomeTemplateView--create new view ");
            return a(context);
        }
        XLog.v("home_entry", "factory--getHomeTemplateView--use cur view ");
        return homeEntryTemplateView;
    }

    public static HomeEntryTemplateView a(Context context) {
        EntranceTemplate j = f.a().j();
        long k = f.a().k();
        if (j == null || j.f == null || j.f.isEmpty()) {
            XLog.v("home_entry", "factory--createHomeTemplateView--data is null ");
            return null;
        }
        EntranceTemplate entranceTemplate = new EntranceTemplate(j.f2066a, j.b, j.c, j.d, j.e, j.f);
        HomeEntryTemplateView homeEntryTemplateView = new HomeEntryTemplateView(context, entranceTemplate, k);
        int size = entranceTemplate.f.size();
        int i = 0;
        int i2 = 0;
        while (i < size) {
            ArrayList arrayList = entranceTemplate.f.get(i);
            int size2 = arrayList.size();
            LinearLayout linearLayout = new LinearLayout(context);
            if (size2 == 0) {
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
                if (layoutParams instanceof LinearLayout.LayoutParams) {
                    layoutParams.height = df.a(context, 90.0f);
                }
                linearLayout.setLayoutParams(layoutParams);
                linearLayout.setVisibility(4);
                linearLayout.setClickable(false);
            } else {
                linearLayout.setOrientation(0);
                linearLayout.setGravity(17);
                linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                for (int i3 = 0; i3 < size2; i3++) {
                    HomeEntryCellBase a2 = a(context, (EntranceBlock) arrayList.get(i3));
                    if (a2 != null) {
                        if (!(((EntranceBlock) arrayList.get(i3)).f2065a != 2 || i3 == 0 || ((EntranceBlock) arrayList.get(i3 - 1)).f2065a == 2)) {
                            linearLayout.addView(a(context, 1));
                        }
                        linearLayout.addView(a2);
                        if (((EntranceBlock) arrayList.get(i3)).f2065a == 2 && i3 != size2 - 1) {
                            linearLayout.addView(a(context, 1));
                        }
                        XLog.v("home_entry", "factory--createHomeTemplateView--rank = " + size + "--collum= " + size2 + "--cell type= " + ((int) ((EntranceBlock) arrayList.get(i3)).f2065a));
                    }
                }
            }
            homeEntryTemplateView.addMyChildView(linearLayout);
            if (i < size - 1 && ((int) entranceTemplate.f2066a) != 1) {
                homeEntryTemplateView.addMyChildView(a(context, 2));
            }
            i++;
            i2 = size2;
        }
        XLog.v("home_entry", "factory--createHomeTemplateView--total rank = " + size + "--total collum= " + i2);
        return homeEntryTemplateView;
    }

    public static View a(Context context, int i) {
        LinearLayout.LayoutParams layoutParams;
        ImageView imageView = new ImageView(context);
        if (i == 1) {
            layoutParams = new LinearLayout.LayoutParams(-2, -1);
            layoutParams.width = df.a(context, 0.5f);
        } else if (i == 2) {
            layoutParams = new LinearLayout.LayoutParams(-1, -2);
            layoutParams.height = df.a(context, 0.5f);
        } else {
            layoutParams = new LinearLayout.LayoutParams(-1, -2);
            layoutParams.height = df.a(context, 0.5f);
        }
        if (layoutParams instanceof LinearLayout.LayoutParams) {
        }
        imageView.setLayoutParams(layoutParams);
        imageView.setBackgroundColor(context.getResources().getColor(R.color.home_entry_dividing_line_color));
        return imageView;
    }

    public static boolean a(HomeEntryTemplateView homeEntryTemplateView) {
        boolean z = false;
        XLog.v("home_entry", "factory--isTemplateViewOutOfData--curView is null= " + (homeEntryTemplateView == null) + "--curView.mSourceData is null= " + (homeEntryTemplateView.mSourceData == null));
        if (homeEntryTemplateView == null || homeEntryTemplateView.mSourceData == null) {
            return true;
        }
        EntranceTemplate j = f.a().j();
        long k = f.a().k();
        if (j == null) {
            XLog.v("home_entry", "factory--isTemplateViewOutOfData--homeEntryData == null");
            return true;
        }
        XLog.v("home_entry", "factory--isTemplateViewOutOfData--compare outOfData--newId= " + j.f2066a + "--oldId= " + homeEntryTemplateView.mSourceData.f2066a + "--newRevision= " + k + "--oldRevision= " + homeEntryTemplateView.mHomeEntryDataRevision);
        if (!(j.f2066a == homeEntryTemplateView.mSourceData.f2066a && k == homeEntryTemplateView.mHomeEntryDataRevision)) {
            z = true;
        }
        return z;
    }

    public static void a(String str, STInfoV2 sTInfoV2) {
        if (!TextUtils.isEmpty(str) && sTInfoV2 != null) {
            int indexOf = str.indexOf("@");
            int length = str.length();
            if (indexOf == -1 || indexOf >= length) {
                sTInfoV2.slotId = str;
                return;
            }
            sTInfoV2.slotId = str.substring(0, indexOf);
            sTInfoV2.pushInfo = str.substring(indexOf + 1, length);
        }
    }
}
