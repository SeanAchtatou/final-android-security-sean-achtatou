package com.tencent.assistant.utils.a;

import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.module.b.b;
import com.tencent.assistant.utils.g;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.connect.common.Constants;
import java.io.File;

/* compiled from: ProGuard */
public class d {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f2617a = false;

    public static int a(DownloadInfo downloadInfo, String str, String str2) {
        String str3;
        if (downloadInfo == null) {
            return -1;
        }
        LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(downloadInfo.packageName);
        if (localApkInfo != null) {
            str3 = localApkInfo.mLocalFilePath;
        } else {
            str3 = null;
        }
        boolean endsWith = AstApp.i().getPackageName().endsWith(downloadInfo.packageName);
        long j = downloadInfo.appId;
        long j2 = downloadInfo.apkId;
        StatInfo statInfo = downloadInfo.statInfo;
        if (!g.b(downloadInfo)) {
            k.a(endsWith, j, j2, -1, "runtime_non_sll", statInfo, a(-1, j));
            return -1;
        }
        a(true);
        b a2 = new i().a(str3, str2, str);
        k.a(endsWith, j, j2, a2.f2616a, a2.b, statInfo, a(a2.f2616a, j));
        a(false);
        a(a2, str3, str2, j, j2);
        return a2.f2616a;
    }

    public static String a(int i, long j) {
        LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(j);
        if (localApkInfo != null) {
            return localApkInfo.manifestMd5;
        }
        return Constants.STR_EMPTY;
    }

    public static void a(b bVar, String str, String str2, long j, long j2) {
        try {
            if (!TextUtils.isEmpty(str2)) {
                File file = new File(str2);
                if (!file.exists()) {
                    return;
                }
                if (bVar == null || bVar.f2616a == 0 || !b.d()) {
                    file.delete();
                    return;
                }
                String str3 = str2 + ".patch";
                File file2 = new File(str3);
                if (file2.exists()) {
                    file2.delete();
                }
                if (!file.renameTo(file2)) {
                    str3 = str2;
                }
                b.a().a(j, j2, str3, str, bVar.f2616a, bVar.b);
            }
        } catch (SecurityException e) {
        }
    }

    public static boolean a() {
        return f2617a;
    }

    private static void a(boolean z) {
        f2617a = z;
    }
}
