package com.tencent.assistant.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import com.qq.AppService.AstApp;
import com.tencent.assistant.Global;
import com.tencent.beacon.event.a;
import java.lang.reflect.Method;
import java.util.HashMap;

/* compiled from: ProGuard */
public class r {

    /* renamed from: a  reason: collision with root package name */
    protected static int f2721a = 0;
    public static int b = -1;
    public static int c = -1;
    public static int d = -1;
    public static int e = -1;
    private static int f = -1;

    public static synchronized int a() {
        int i;
        synchronized (r.class) {
            i = f2721a;
            f2721a = i + 1;
        }
        return i;
    }

    public static byte[] a(int i) {
        byte[] bArr = new byte[4];
        for (int i2 = 0; i2 < 4; i2++) {
            bArr[i2] = (byte) (i >>> (24 - (i2 * 8)));
        }
        return bArr;
    }

    public static int a(byte[] bArr, int i) {
        byte b2 = 0;
        for (int i2 = i; i2 < i + 4; i2++) {
            b2 = (b2 << 8) | (bArr[i2] & 255);
        }
        return b2;
    }

    public static int a(String str) {
        if (str == null) {
            return 0;
        }
        try {
            int indexOf = str.indexOf(".");
            int indexOf2 = str.indexOf(".", indexOf + 1);
            int indexOf3 = str.indexOf(".", indexOf2 + 1);
            long[] jArr = {Long.parseLong(str.substring(0, indexOf)), Long.parseLong(str.substring(indexOf + 1, indexOf2)), Long.parseLong(str.substring(indexOf2 + 1, indexOf3)), Long.parseLong(str.substring(indexOf3 + 1))};
            return (int) (jArr[0] + (jArr[3] << 24) + (jArr[2] << 16) + (jArr[1] << 8));
        } catch (Throwable th) {
            return 0;
        }
    }

    public static int b() {
        if (b == -1) {
            b = AstApp.i().getResources().getDisplayMetrics().widthPixels;
        }
        return b;
    }

    public static int c() {
        if (c == -1) {
            c = AstApp.i().getResources().getDisplayMetrics().heightPixels;
        }
        return c;
    }

    public static int d() {
        try {
            return Integer.valueOf(Build.VERSION.SDK).intValue();
        } catch (NumberFormatException e2) {
            return 0;
        }
    }

    public static boolean e() {
        boolean z = false;
        boolean z2 = false;
        for (Method method : MotionEvent.class.getDeclaredMethods()) {
            if (method.getName().equals("getPointerCount")) {
                z2 = true;
            }
            if (method.getName().equals("getPointerId")) {
                z = true;
            }
        }
        if (d() >= 7) {
            return true;
        }
        if (!z2 || !z) {
            return false;
        }
        return true;
    }

    public static int b(String str) {
        return a(str, 0);
    }

    public static int a(String str, int i) {
        try {
            return Integer.parseInt(str);
        } catch (Exception e2) {
            e2.printStackTrace();
            return i;
        }
    }

    public static String b(String str, int i) {
        return str + "_  _" + Integer.toString(i);
    }

    public static boolean a(String str, Bundle bundle) {
        try {
            Intent launchIntentForPackage = AstApp.i().getPackageManager().getLaunchIntentForPackage(str);
            if (launchIntentForPackage == null && AstApp.i().getPackageManager().getPackageInfo(str, 1) != null) {
                launchIntentForPackage = new Intent(str);
            }
            if (launchIntentForPackage != null) {
                if (bundle != null) {
                    launchIntentForPackage.putExtras(bundle);
                }
                launchIntentForPackage.setFlags(268435456);
                AstApp.i().startActivity(launchIntentForPackage);
                return true;
            }
        } catch (Exception e2) {
        }
        return false;
    }

    public static boolean b(String str, Bundle bundle) {
        try {
            if (TextUtils.isEmpty(str)) {
                return false;
            }
            Intent intent = new Intent(AstApp.i(), Class.forName(str));
            if (bundle != null) {
                intent.putExtras(bundle);
            }
            intent.setFlags(268435456);
            AstApp.i().startActivity(intent);
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    public static void c(String str) {
        PackageManager packageManager = AstApp.i().getPackageManager();
        String[] strArr = {"com.tencent.assistant.module.timer.ScheduleJobReceiver", "com.qq.AppService.MainReceiver", "com.qq.AppService.APKReceiver", "com.tencent.wcs.proxy.heartbeat.HeartBeatService$HeartBeatTask", "com.tencent.assistant.plugin.system.PluginBackToBaoReceiver", "com.tencent.assistant.receiver.QubeThemeInstallReceiver", "com.tencent.assistant.receiver.SDKRelatedReceiver", "com.tencent.assistant.plugin.system.DockReceiver", "com.tencent.assistant.receiver.BatteryStatusReceiver", "com.tencent.assistant.plugin.system.QReaderReceiver", "com.tencent.assistant.module.timer.RecoverAppListReceiver"};
        StringBuilder sb = new StringBuilder();
        String packageName = AstApp.i().getPackageName();
        boolean z = false;
        for (int i = 0; i < strArr.length; i++) {
            ComponentName componentName = new ComponentName(packageName, strArr[i]);
            if (packageManager.getComponentEnabledSetting(componentName) == 2) {
                XLog.d("CommonUtil", "component " + componentName.toString() + " is disabled. bring it up!");
                packageManager.setComponentEnabledSetting(componentName, 1, 1);
                int lastIndexOf = strArr[i].lastIndexOf(".");
                int i2 = lastIndexOf == -1 ? 0 : lastIndexOf + 1;
                sb.append(strArr[i].substring(i2, i2 + 4)).append(";");
                z = true;
            }
        }
        if (z) {
            AstApp.b = true;
            HashMap hashMap = new HashMap();
            hashMap.put("B1", sb.toString());
            hashMap.put("B2", Global.getPhoneGuidAndGen());
            hashMap.put("B3", Global.getQUAForBeacon());
            hashMap.put("B4", t.g());
            hashMap.put("B5", str);
            XLog.d("beacon", "beacon report >> AutoStartDiabled. " + hashMap.toString());
            a.a("AutoStartDiabled", true, -1, -1, hashMap, true);
        }
        AstApp.f201a = true;
    }

    public static final void a(int i, int i2) {
        try {
            byte[] bArr = new byte[(i * i2)];
            return;
        } catch (OutOfMemoryError e2) {
            throw e2;
        }
    }

    public static final float f() {
        return ((float) g()) / 48.0f;
    }

    public static final int g() {
        return (int) ((Runtime.getRuntime().maxMemory() / 1024) / 1024);
    }

    public static void a(Context context, long j) {
        ((AlarmManager) context.getSystemService("alarm")).set(0, 21600000 + j, PendingIntent.getBroadcast(context, 0, new Intent("com.tencent.assistant.timer.revcover.app"), 268435456));
    }
}
