package com.tencent.assistant.utils.installuninstall;

import android.os.Message;
import com.tencent.assistant.event.listener.UIEventListener;

/* compiled from: ProGuard */
class af implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ac f2692a;

    private af(ac acVar) {
        this.f2692a = acVar;
    }

    /* synthetic */ af(ac acVar, ad adVar) {
        this(acVar);
    }

    public void handleUIEvent(Message message) {
        if (message.what == 1031) {
            this.f2692a.h();
            this.f2692a.l();
            synchronized (this.f2692a.d) {
                this.f2692a.d.notifyAll();
            }
        }
    }
}
