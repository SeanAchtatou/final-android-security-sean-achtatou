package com.tencent.assistant.utils;

import android.app.Dialog;
import android.view.View;
import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
final class al implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppConst.TwoBtnDialogInfo f2633a;
    final /* synthetic */ Dialog b;

    al(AppConst.TwoBtnDialogInfo twoBtnDialogInfo, Dialog dialog) {
        this.f2633a = twoBtnDialogInfo;
        this.b = dialog;
    }

    public void onClick(View view) {
        if (this.f2633a != null) {
            this.f2633a.onLeftBtnClick();
            this.b.dismiss();
        }
    }
}
