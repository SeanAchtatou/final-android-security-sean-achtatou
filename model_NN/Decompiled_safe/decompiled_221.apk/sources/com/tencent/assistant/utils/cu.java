package com.tencent.assistant.utils;

import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class cu {

    /* renamed from: a  reason: collision with root package name */
    private long f2675a = System.currentTimeMillis();
    private String b = Constants.STR_EMPTY;

    public String a() {
        return a(null);
    }

    public String a(String str) {
        String str2;
        long currentTimeMillis = System.currentTimeMillis();
        long j = currentTimeMillis - this.f2675a;
        if (str != null) {
            this.b = str;
        }
        if (j > 10000) {
            str2 = this.b + " cost:" + (j / 1000) + "s";
        } else {
            str2 = this.b + " cost:" + j + "ms";
        }
        this.f2675a = currentTimeMillis;
        return str2;
    }

    public String toString() {
        return a();
    }
}
