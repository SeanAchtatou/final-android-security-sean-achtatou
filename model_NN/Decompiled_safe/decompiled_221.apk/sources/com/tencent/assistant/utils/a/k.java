package com.tencent.assistant.utils.a;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/* compiled from: ProGuard */
public class k {

    /* renamed from: a  reason: collision with root package name */
    LinkedHashMap<String, j> f2622a = new LinkedHashMap<>();
    LinkedHashMap<String, f> b = new LinkedHashMap<>();
    ArrayList<String> c = new ArrayList<>();
    h d = new h();
    long e = 0;
    String f = null;

    public boolean a(String str) {
        RandomAccessFile randomAccessFile;
        this.f = str;
        try {
            randomAccessFile = new RandomAccessFile(str, "r");
            if (randomAccessFile != null) {
                try {
                    this.e = randomAccessFile.length();
                    if (!a(randomAccessFile)) {
                        if (randomAccessFile == null) {
                            return false;
                        }
                        randomAccessFile.close();
                        return false;
                    }
                } catch (Throwable th) {
                    th = th;
                }
            }
            if (randomAccessFile != null) {
                randomAccessFile.close();
            }
            c();
            a();
            return true;
        } catch (Throwable th2) {
            th = th2;
            randomAccessFile = null;
            if (randomAccessFile != null) {
                randomAccessFile.close();
            }
            throw th;
        }
    }

    private boolean a(RandomAccessFile randomAccessFile) {
        long j = 1024;
        boolean z = true;
        if (1024 > this.e) {
            j = this.e;
        }
        byte[] bArr = new byte[1024];
        try {
            randomAccessFile.skipBytes((int) (this.e - j));
            if (((long) randomAccessFile.read(bArr, 0, (int) j)) != j) {
                z = false;
            }
        } catch (IOException e2) {
            e2.printStackTrace();
            z = false;
        }
        if (!z) {
            return false;
        }
        for (int i = 0; ((long) i) < j - 4; i++) {
            if ((bArr[i] << 24) + (bArr[i + 1] << 16) + (bArr[i + 2] << 8) + bArr[i + 3] == 1347093766) {
                try {
                    DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(bArr, i + 4, (int) (j - ((long) (i + 4)))));
                    this.d.a(dataInputStream);
                    dataInputStream.close();
                } catch (IOException e3) {
                    z = false;
                }
            }
        }
        return z;
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:37:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void c() {
        /*
            r5 = this;
            r2 = 0
            java.io.DataInputStream r1 = new java.io.DataInputStream     // Catch:{ Exception -> 0x0067, all -> 0x005a }
            java.io.BufferedInputStream r0 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x0067, all -> 0x005a }
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0067, all -> 0x005a }
            java.lang.String r4 = r5.f     // Catch:{ Exception -> 0x0067, all -> 0x005a }
            r3.<init>(r4)     // Catch:{ Exception -> 0x0067, all -> 0x005a }
            r0.<init>(r3)     // Catch:{ Exception -> 0x0067, all -> 0x005a }
            r1.<init>(r0)     // Catch:{ Exception -> 0x0067, all -> 0x005a }
            com.tencent.assistant.utils.a.h r0 = r5.d     // Catch:{ Exception -> 0x004e }
            int r0 = r0.f     // Catch:{ Exception -> 0x004e }
            long r2 = (long) r0     // Catch:{ Exception -> 0x004e }
            r1.skip(r2)     // Catch:{ Exception -> 0x004e }
            r0 = 0
        L_0x001b:
            int r2 = r1.available()     // Catch:{ Exception -> 0x004e }
            r3 = 4
            if (r2 < r3) goto L_0x0062
            if (r0 != 0) goto L_0x0062
            int r2 = r1.readInt()     // Catch:{ Exception -> 0x004e }
            switch(r2) {
                case 1347092738: goto L_0x002c;
                case 1347093766: goto L_0x0058;
                default: goto L_0x002b;
            }     // Catch:{ Exception -> 0x004e }
        L_0x002b:
            goto L_0x001b
        L_0x002c:
            com.tencent.assistant.utils.a.f r2 = new com.tencent.assistant.utils.a.f     // Catch:{ Exception -> 0x004e }
            r2.<init>()     // Catch:{ Exception -> 0x004e }
            r2.a(r1)     // Catch:{ Exception -> 0x004e }
            java.lang.String r3 = new java.lang.String     // Catch:{ Exception -> 0x004e }
            byte[] r4 = r2.s     // Catch:{ Exception -> 0x004e }
            r3.<init>(r4)     // Catch:{ Exception -> 0x004e }
            java.lang.String r4 = "META-INF/"
            boolean r4 = r3.startsWith(r4)     // Catch:{ Exception -> 0x004e }
            if (r4 == 0) goto L_0x0048
            java.util.ArrayList<java.lang.String> r4 = r5.c     // Catch:{ Exception -> 0x004e }
            r4.add(r3)     // Catch:{ Exception -> 0x004e }
        L_0x0048:
            java.util.LinkedHashMap<java.lang.String, com.tencent.assistant.utils.a.f> r4 = r5.b     // Catch:{ Exception -> 0x004e }
            r4.put(r3, r2)     // Catch:{ Exception -> 0x004e }
            goto L_0x001b
        L_0x004e:
            r0 = move-exception
        L_0x004f:
            r0.printStackTrace()     // Catch:{ all -> 0x0065 }
            if (r1 == 0) goto L_0x0057
        L_0x0054:
            r1.close()
        L_0x0057:
            return
        L_0x0058:
            r0 = 1
            goto L_0x001b
        L_0x005a:
            r0 = move-exception
            r1 = r2
        L_0x005c:
            if (r1 == 0) goto L_0x0061
            r1.close()
        L_0x0061:
            throw r0
        L_0x0062:
            if (r1 == 0) goto L_0x0057
            goto L_0x0054
        L_0x0065:
            r0 = move-exception
            goto L_0x005c
        L_0x0067:
            r0 = move-exception
            r1 = r2
            goto L_0x004f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.utils.a.k.c():void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:45:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a() {
        /*
            r8 = this;
            r4 = 1
            r2 = 0
            java.io.DataInputStream r5 = new java.io.DataInputStream     // Catch:{ Exception -> 0x0075, all -> 0x007f }
            java.io.BufferedInputStream r1 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x0075, all -> 0x007f }
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0075, all -> 0x007f }
            java.lang.String r6 = r8.f     // Catch:{ Exception -> 0x0075, all -> 0x007f }
            r3.<init>(r6)     // Catch:{ Exception -> 0x0075, all -> 0x007f }
            r1.<init>(r3)     // Catch:{ Exception -> 0x0075, all -> 0x007f }
            r5.<init>(r1)     // Catch:{ Exception -> 0x0075, all -> 0x007f }
            r3 = 0
        L_0x0014:
            int r1 = r5.available()     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            r2 = 4
            if (r1 < r2) goto L_0x0087
            if (r3 != 0) goto L_0x0087
            int r1 = r5.readInt()     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            switch(r1) {
                case 1347092738: goto L_0x0071;
                case 1347093252: goto L_0x0027;
                case 1347093766: goto L_0x0073;
                case 1347094280: goto L_0x006a;
                default: goto L_0x0024;
            }     // Catch:{ Exception -> 0x0092, all -> 0x008d }
        L_0x0024:
            r1 = r3
        L_0x0025:
            r3 = r1
            goto L_0x0014
        L_0x0027:
            com.tencent.assistant.utils.a.j r6 = new com.tencent.assistant.utils.a.j     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            r6.<init>()     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            r6.a(r5)     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            java.lang.String r7 = new java.lang.String     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            byte[] r1 = r6.k     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            r7.<init>(r1)     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            java.util.LinkedHashMap<java.lang.String, com.tencent.assistant.utils.a.f> r1 = r8.b     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            java.lang.Object r1 = r1.get(r7)     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            r0 = r1
            com.tencent.assistant.utils.a.f r0 = (com.tencent.assistant.utils.a.f) r0     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            r2 = r0
            short r1 = r6.j     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            r2.k = r1     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            short r1 = r2.k     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            if (r1 <= 0) goto L_0x0052
            byte[] r1 = r6.l     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            java.lang.Object r1 = r1.clone()     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            byte[] r1 = (byte[]) r1     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            r2.t = r1     // Catch:{ Exception -> 0x0092, all -> 0x008d }
        L_0x0052:
            int r1 = r2.h     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            r6.g = r1     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            int r1 = r2.i     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            r6.h = r1     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            int r1 = r2.g     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            r6.f = r1     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            int r1 = r6.g     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            r5.skipBytes(r1)     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            java.util.LinkedHashMap<java.lang.String, com.tencent.assistant.utils.a.j> r1 = r8.f2622a     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            r1.put(r7, r6)     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            r1 = r3
            goto L_0x0025
        L_0x006a:
            r1 = 12
            r5.skipBytes(r1)     // Catch:{ Exception -> 0x0092, all -> 0x008d }
            r1 = r3
            goto L_0x0025
        L_0x0071:
            r1 = r4
            goto L_0x0025
        L_0x0073:
            r1 = r4
            goto L_0x0025
        L_0x0075:
            r1 = move-exception
        L_0x0076:
            r1.printStackTrace()     // Catch:{ all -> 0x008f }
            if (r2 == 0) goto L_0x007e
            r2.close()
        L_0x007e:
            return
        L_0x007f:
            r1 = move-exception
            r5 = r2
        L_0x0081:
            if (r5 == 0) goto L_0x0086
            r5.close()
        L_0x0086:
            throw r1
        L_0x0087:
            if (r5 == 0) goto L_0x007e
            r5.close()
            goto L_0x007e
        L_0x008d:
            r1 = move-exception
            goto L_0x0081
        L_0x008f:
            r1 = move-exception
            r5 = r2
            goto L_0x0081
        L_0x0092:
            r1 = move-exception
            r2 = r5
            goto L_0x0076
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.utils.a.k.a():void");
    }

    public f b(String str) {
        return this.b.get(str);
    }

    public int c(String str) {
        f fVar = this.b.get(str);
        if (fVar == null) {
            throw new FileNotFoundException();
        }
        j jVar = this.f2622a.get(str);
        if (jVar == null) {
            throw new FileNotFoundException();
        }
        short s = jVar.j;
        return fVar.q + s + jVar.i + 30;
    }

    public LinkedHashMap<String, f> b() {
        return this.b;
    }
}
