package com.tencent.assistant.utils;

import android.content.Context;
import android.content.Intent;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.StartPopWindowActivity;
import com.tencent.assistant.db.table.s;
import com.tencent.assistant.manager.bo;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.mgr.d;
import com.tencent.assistant.plugin.proxy.PluginProxyActivity;
import com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse;
import com.tencent.assistant.protocol.jce.PopUpInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class bx {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f2660a = true;
    /* access modifiers changed from: private */
    public static s b = new s();
    /* access modifiers changed from: private */
    public static ArrayList<PopUpInfo> c = new ArrayList<>();

    public static synchronized void a(Context context, boolean z, int i, GetPhoneUserAppListResponse getPhoneUserAppListResponse, boolean z2) {
        synchronized (bx.class) {
            TemporaryThreadManager.get().start(new by(getPhoneUserAppListResponse, context, i, z2, z));
        }
    }

    /* access modifiers changed from: private */
    public static void b(Context context, PopUpInfo popUpInfo, int i) {
        PopUpInfo popUpInfo2 = new PopUpInfo();
        popUpInfo2.f2258a = popUpInfo.f2258a;
        popUpInfo2.d = System.currentTimeMillis();
        popUpInfo2.f = popUpInfo.f;
        b.a(popUpInfo2);
        c.add(0, popUpInfo2);
        Intent intent = new Intent(context, StartPopWindowActivity.class);
        intent.putExtra("extra_pop_info", popUpInfo);
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, i);
        try {
            context.startActivity(intent);
        } catch (Throwable th) {
        }
    }

    public static void a() {
        PopUpInfo popUpInfo = new PopUpInfo();
        popUpInfo.d = System.currentTimeMillis();
        popUpInfo.f2258a = 0;
        popUpInfo.f = 889032704;
        b.a(popUpInfo);
        c.add(0, popUpInfo);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void a(Context context, GetPhoneUserAppListResponse getPhoneUserAppListResponse, int i, boolean z) {
        a();
        Intent intent = new Intent(context, StartPopWindowActivity.class);
        intent.putExtra("extra_pop_info", getPhoneUserAppListResponse);
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, i);
        intent.putExtra("extra_is_recover", true);
        intent.putExtra("extra_special_title", z);
        try {
            context.startActivity(intent);
        } catch (Throwable th) {
        }
    }

    public static void a(Context context, boolean z) {
        XLog.e("zhangyuanchao", "-------showHuanJiDialog------:");
        cc ccVar = new cc();
        ccVar.hasTitle = true;
        ccVar.titleRes = context.getResources().getString(R.string.pop_huanji_title);
        if (z) {
            ccVar.contentRes = context.getResources().getString(R.string.pop_huanji_content_no_apps);
        } else {
            ccVar.contentRes = context.getResources().getString(R.string.pop_huanji_content);
        }
        ccVar.lBtnTxtRes = context.getResources().getString(R.string.down_page_dialog_left_del);
        ccVar.rBtnTxtRes = context.getResources().getString(R.string.pop_huanji_right_btn);
        ccVar.blockCaller = true;
        v.a(ccVar);
        k.a(new STInfoV2(STConst.ST_PAGE_GUIDE_RESULT_HUANJI_DIALOG, STConst.ST_DEFAULT_SLOT, 0, STConst.ST_DEFAULT_SLOT, 100));
        bo.i = false;
    }

    public static void b() {
        PluginInfo.PluginEntry pluginEntryByStartActivity;
        PluginInfo a2 = d.b().a("com.tencent.mobileassistant_wifitransfer");
        if (a2 != null && (pluginEntryByStartActivity = a2.getPluginEntryByStartActivity("com.tencent.assistant.switchphoneactivity.SwitchPhoneActivity")) != null) {
            try {
                PluginProxyActivity.a(AstApp.i(), pluginEntryByStartActivity.getHostPlugInfo().getPackageName(), pluginEntryByStartActivity.getHostPlugInfo().getVersion(), pluginEntryByStartActivity.getStartActivity(), pluginEntryByStartActivity.getHostPlugInfo().getInProcess(), null, pluginEntryByStartActivity.getHostPlugInfo().getLaunchApplication());
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    public static void a(int i, long j, int i2) {
        STInfoV2 sTInfoV2 = new STInfoV2(STConst.ST_PAGE_NECESSARY_POP, STConst.ST_DEFAULT_SLOT, i, STConst.ST_DEFAULT_SLOT, 100);
        sTInfoV2.extraData = j + "|||" + i2;
        k.a(sTInfoV2);
    }
}
