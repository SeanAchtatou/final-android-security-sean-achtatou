package com.tencent.assistant.utils.installuninstall;

/* compiled from: ProGuard */
class j extends n {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ InstallUninstallDialogManager f2704a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    j(InstallUninstallDialogManager installUninstallDialogManager) {
        super(installUninstallDialogManager);
        this.f2704a = installUninstallDialogManager;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean
     arg types: [com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, int]
     candidates:
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT):com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_TYPE, com.tencent.assistant.utils.installuninstall.InstallUninstallTaskBean):boolean
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean */
    public void onBtnClick() {
        boolean unused = this.f2704a.c = false;
        this.f2704a.c();
        this.f2704a.a(this.b.downloadTicket);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean
     arg types: [com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, int]
     candidates:
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT):com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_TYPE, com.tencent.assistant.utils.installuninstall.InstallUninstallTaskBean):boolean
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean */
    public void onCancell() {
        boolean unused = this.f2704a.c = false;
        this.f2704a.c();
        this.f2704a.a(this.b.downloadTicket);
    }
}
