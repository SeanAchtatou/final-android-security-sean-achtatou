package com.tencent.assistant.utils;

import android.content.DialogInterface;
import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
final class ah implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppConst.TwoBtnDialogInfo f2629a;

    ah(AppConst.TwoBtnDialogInfo twoBtnDialogInfo) {
        this.f2629a = twoBtnDialogInfo;
    }

    public void onCancel(DialogInterface dialogInterface) {
        this.f2629a.onCancell();
    }
}
