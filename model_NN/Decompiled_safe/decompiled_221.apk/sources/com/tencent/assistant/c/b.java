package com.tencent.assistant.c;

import android.content.Context;
import android.view.ViewStub;
import com.tencent.assistant.protocol.jce.NpcCfg;

/* compiled from: ProGuard */
public class b {

    /* renamed from: a  reason: collision with root package name */
    a f866a = null;

    public void a(Context context, ViewStub viewStub, boolean z) {
        NpcCfg b;
        if (z || (b = c.a().b()) == null) {
            this.f866a = new h(context, viewStub);
            return;
        }
        this.f866a = new d(context, viewStub);
        ((d) this.f866a).a(b);
    }

    public void a(boolean z) {
        if (this.f866a == null) {
            return;
        }
        if (this.f866a instanceof h) {
            this.f866a.a(z);
        } else {
            this.f866a.a(false);
        }
    }

    public a a() {
        return this.f866a;
    }
}
