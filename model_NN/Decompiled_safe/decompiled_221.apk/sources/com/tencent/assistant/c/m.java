package com.tencent.assistant.c;

import android.os.Handler;
import android.os.Message;
import com.tencent.assistant.component.treasurebox.AppTreasureEntryBlinkEyesView;

/* compiled from: ProGuard */
class m extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ h f875a;

    m(h hVar) {
        this.f875a = hVar;
    }

    public void handleMessage(Message message) {
        boolean z = true;
        int i = 0;
        switch (message.what) {
            case 1:
                if (message.arg1 != 1) {
                    z = false;
                }
                if (!this.f875a.g) {
                    if (this.f875a.e != null) {
                        AppTreasureEntryBlinkEyesView b = this.f875a.e;
                        if (!z) {
                            i = 8;
                        }
                        b.setVisibility(i);
                    }
                    if (this.f875a.f != null) {
                        this.f875a.f.setVisibility(8);
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }
}
