package com.tencent.assistant.backgroundscan;

import android.app.Notification;
import com.tencent.assistant.db.table.f;
import com.tencent.assistant.manager.notification.v;
import com.tencent.assistant.utils.XLog;
import java.util.List;

/* compiled from: ProGuard */
class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ byte f852a;
    final /* synthetic */ b b;

    c(b bVar, byte b2) {
        this.b = bVar;
        this.f852a = b2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.notification.v.a(int, android.app.Notification, int, boolean):void
     arg types: [int, android.app.Notification, int, int]
     candidates:
      com.tencent.assistant.manager.notification.v.a(int, com.tencent.assistant.protocol.jce.PushInfo, byte[], boolean):void
      com.tencent.assistant.manager.notification.v.a(int, android.app.Notification, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.backgroundscan.a.a(com.tencent.assistant.backgroundscan.a, boolean):boolean
     arg types: [com.tencent.assistant.backgroundscan.a, int]
     candidates:
      com.tencent.assistant.backgroundscan.a.a(com.tencent.assistant.backgroundscan.a, byte):android.app.Notification
      com.tencent.assistant.backgroundscan.a.a(byte, com.tencent.assistant.protocol.jce.ContextItem):java.lang.CharSequence
      com.tencent.assistant.backgroundscan.a.a(byte, com.tencent.assistant.backgroundscan.BackgroundScan):java.lang.String
      com.tencent.assistant.backgroundscan.a.a(byte, int):void
      com.tencent.assistant.backgroundscan.a.a(com.tencent.assistant.backgroundscan.a, boolean):boolean */
    public void run() {
        XLog.d("BackgroundScan", "<push> send Notification");
        Notification a2 = this.b.b.b(this.f852a);
        if (a2 != null) {
            XLog.d("BackgroundScan", "<push> send Notification successful !!!");
            v.a().a(119, a2, 0, true);
            this.b.b.a(this.f852a);
            List<BackgroundScan> b2 = f.a().b();
            if (b2.size() > 0) {
                for (BackgroundScan next : b2) {
                    next.e = -2;
                    if (this.f852a == next.f846a) {
                        next.d = System.currentTimeMillis();
                    }
                    f.a().b(next);
                }
            }
            this.b.b.f();
            n.a().a("b_new_scan_push_exp", this.f852a, this.b.b.d, this.b.b.e);
        }
        boolean unused = this.b.b.b = false;
    }
}
