package com.tencent.assistant.backgroundscan;

import android.text.TextUtils;
import com.tencent.assistant.Global;
import com.tencent.assistant.m;
import com.tencent.assistant.plugin.PluginProxyManager;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.t;
import com.tencent.beacon.event.a;
import com.tencent.connect.common.Constants;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

/* compiled from: ProGuard */
public class n {

    /* renamed from: a  reason: collision with root package name */
    private static n f863a = null;

    private n() {
    }

    public static synchronized n a() {
        n nVar;
        synchronized (n.class) {
            if (f863a == null) {
                f863a = new n();
            }
            nVar = f863a;
        }
        return nVar;
    }

    private String a(byte b) {
        switch (b) {
            case 1:
                return "b_new_scan_health";
            case 2:
                return "b_new_scan_mem";
            case 3:
                return "b_new_scan_pkg";
            case 4:
                return "b_new_scan_rubbish";
            case 5:
                return "b_new_scan_bigfile";
            case 6:
                return "b_new_scan_virus";
            default:
                return Constants.STR_EMPTY;
        }
    }

    public final void a(String str, byte b) {
        a(str, b, null, null);
    }

    public final void a(String str, byte b, String str2, String str3) {
        a(str, b, str2, str3, null, 0);
    }

    public final void a(String str, byte b, String str2, String str3, String str4, long j) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", PluginProxyManager.getPhoneGuid());
        hashMap.put("B2", Global.getQUAForBeacon());
        if (b > 0) {
            hashMap.put("B3", a(b));
        }
        if (!TextUtils.isEmpty(str2)) {
            hashMap.put("B4", str2.replaceAll("\\d|M|m|G|g|K|k|B|b|\\.|%", Constants.STR_EMPTY));
        }
        if (!TextUtils.isEmpty(str3)) {
            hashMap.put("B5", str3.replaceAll("\\d|M|m|G|g|K|k|B|b|\\.|%", Constants.STR_EMPTY));
        }
        if (!TextUtils.isEmpty(str4)) {
            hashMap.put("B6", str4);
        }
        if (j > 0) {
            hashMap.put("B7", j + Constants.STR_EMPTY);
        }
        XLog.d("BackgroundScan", "<report> Background scan beacon report eventName = " + str + ", para = " + hashMap.toString());
        a.a(str, true, -1, -1, hashMap, true);
    }

    public synchronized void b() {
        TemporaryThreadManager.get().start(new o(this));
    }

    /* access modifiers changed from: private */
    public void c() {
        HashMap hashMap = new HashMap();
        boolean o = m.a().o();
        hashMap.put("B1", o ? "1" : "0");
        hashMap.put("B2", Global.getPhoneGuid());
        hashMap.put("B3", Global.getQUAForBeacon());
        hashMap.put("B4", t.g());
        XLog.d("BackgroundScan", "<report> Background scan beacon report eventName = FloatWindowSwitchState, para = " + hashMap.toString());
        a.a("FloatWindowSwitchState", o, -1, -1, hashMap, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    /* access modifiers changed from: private */
    public int d() {
        long a2 = m.a().a("key_float_window_last_report_date", 0L);
        if (a2 != 0) {
            try {
                return Integer.parseInt(new SimpleDateFormat("yyyyMMdd").format(new Date(a2)));
            } catch (NumberFormatException e) {
            }
        }
        return 0;
    }

    /* access modifiers changed from: private */
    public void e() {
        m.a().b("key_float_window_last_report_date", Long.valueOf(System.currentTimeMillis()));
    }

    /* access modifiers changed from: private */
    public int f() {
        try {
            return Integer.parseInt(new SimpleDateFormat("yyyyMMdd").format(new Date()));
        } catch (NumberFormatException e) {
            return 0;
        }
    }
}
