package com.tencent.assistant.backgroundscan;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: ProGuard */
public class BackgroundScan implements Parcelable {

    /* renamed from: a  reason: collision with root package name */
    public byte f846a;
    public long b;
    public double c;
    public long d;
    public long e;

    public BackgroundScan() {
    }

    public BackgroundScan(byte b2, long j, double d2, long j2, long j3) {
        this.f846a = b2;
        this.b = j;
        this.c = d2;
        this.d = j2;
        this.e = j3;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
    }

    public String toString() {
        return ((int) this.f846a) + "," + this.b + "," + this.c + "," + this.d + "," + this.e;
    }
}
