package com.tencent.assistant.g;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import com.tencent.assistant.model.ShareBaseModel;
import com.tencent.assistant.thumbnailCache.k;
import com.tencent.mm.sdk.modelmsg.WXMediaMessage;
import com.tencent.mm.sdk.modelmsg.WXWebpageObject;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

/* compiled from: ProGuard */
class j implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f1316a;
    final /* synthetic */ ShareBaseModel b;
    final /* synthetic */ e c;

    j(e eVar, Context context, ShareBaseModel shareBaseModel) {
        this.c = eVar;
        this.f1316a = context;
        this.b = shareBaseModel;
    }

    public void run() {
        if (this.c.b == null) {
            this.c.b = WXAPIFactory.createWXAPI(this.f1316a, "wx3909f6add1206543", false);
        }
        this.c.b.registerApp("wx3909f6add1206543");
        WXWebpageObject wXWebpageObject = new WXWebpageObject();
        wXWebpageObject.webpageUrl = this.b.d;
        this.c.f = new WXMediaMessage(wXWebpageObject);
        this.c.f.title = this.b.f1633a;
        this.c.f.description = this.b.b;
        if (TextUtils.isEmpty(this.b.c)) {
            this.c.a(this.c.f, (Bitmap) null);
            return;
        }
        Bitmap a2 = k.b().a(this.b.c, 1, this.c);
        if (a2 != null && !a2.isRecycled()) {
            this.c.a(this.c.f, a2);
        }
    }
}
