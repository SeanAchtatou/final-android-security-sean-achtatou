package com.tencent.assistant.adapter;

import android.widget.ImageView;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.a;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.component.y;

/* compiled from: ProGuard */
class ce extends y {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f738a;
    final /* synthetic */ OneMoreAdapter b;

    ce(OneMoreAdapter oneMoreAdapter, SimpleAppModel simpleAppModel) {
        this.b = oneMoreAdapter;
        this.f738a = simpleAppModel;
    }

    public void a(DownloadInfo downloadInfo) {
        ImageView imageView = (ImageView) this.b.e.findViewWithTag(downloadInfo.downloadTicket);
        if (downloadInfo != null && downloadInfo.needReCreateInfo(this.f738a)) {
            DownloadProxy.a().b(downloadInfo.downloadTicket);
            downloadInfo = DownloadInfo.createDownloadInfo(this.f738a, null);
        }
        a.a().a(downloadInfo);
        com.tencent.assistant.utils.a.a(imageView);
    }
}
