package com.tencent.assistant.adapter.wifitransfer;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.localres.LocalImageLoader;
import com.tencent.assistant.localres.LocalMediaManager;
import com.tencent.assistant.localres.model.LocalImage;

/* compiled from: ProGuard */
public class ImageGridViewAdapter extends WifiTransferMediaAdapter<LocalImage> {
    public ImageGridViewAdapter(Context context) {
        super(context);
        this.f812a = (LocalImageLoader) LocalMediaManager.getInstance().getLoader(1);
        this.f812a.registerListener(this);
    }

    /* access modifiers changed from: protected */
    public View e() {
        View inflate = View.inflate(this.d, R.layout.wifitransfer_pic_gridview_item, null);
        a aVar = new a(this);
        aVar.b = (TXImageView) inflate.findViewById(R.id.gridview_item_imageview);
        aVar.c = (ImageView) inflate.findViewById(R.id.gridview_item_checkbox);
        aVar.d = (TextView) inflate.findViewById(R.id.checkbox);
        inflate.setTag(aVar);
        return inflate;
    }

    /* access modifiers changed from: protected */
    public void a(View view, LocalImage localImage, int i) {
        ((a) view.getTag()).b.updateImageView(localImage.thumbnailPath, R.drawable.pic_default_pic, TXImageView.TXImageViewType.LOCAL_LARGER_IMAGE_THUMBNAIL);
    }
}
