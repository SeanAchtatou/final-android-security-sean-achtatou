package com.tencent.assistant.adapter.a;

import android.content.Context;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.adapter.OneMoreAdapter;
import com.tencent.assistant.manager.u;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.en;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class e {
    public static void a(Context context, ListView listView, d dVar, Object obj, SimpleAppModel simpleAppModel, int i) {
        if (listView != null && dVar != null && obj != null) {
            c b = dVar.b(obj);
            dVar.d(obj);
            if (b != null) {
                int firstVisiblePosition = listView.getFirstVisiblePosition();
                int lastVisiblePosition = listView.getLastVisiblePosition() + 1;
                String str = (String) b.d.getTag();
                if (firstVisiblePosition != lastVisiblePosition && ((i != 0 && i < firstVisiblePosition) || i > lastVisiblePosition || (str != null && (simpleAppModel == null || !str.equals(simpleAppModel.q()))))) {
                    b.c = null;
                    b.k = 0;
                    b.d.setTag(null);
                }
                if (b.k == 2) {
                    b.h.setVisibility(8);
                    b.j.setVisibility(8);
                    b.d.setVisibility(0);
                    b.e.setVisibility(0);
                    b.b.setVisibility(0);
                    b.f686a.setVisibility(0);
                    BaseAdapter baseAdapter = (BaseAdapter) b.f686a.getAdapter();
                    if (baseAdapter != null) {
                        baseAdapter.notifyDataSetChanged();
                    }
                } else if (b.k == 3) {
                    a(context, b);
                } else if (b.k == 1) {
                    b(context, b);
                } else if (b.k == 0) {
                    a(b);
                }
            }
        }
    }

    public static boolean a(Context context, SimpleAppModel simpleAppModel, ListView listView, d dVar, View view, boolean z) {
        boolean z2;
        c a2;
        if (listView == null) {
            return false;
        }
        int a3 = u.a().a("oneMoreApp", 0);
        if (!z || a3 != 1) {
            z2 = false;
        } else {
            z2 = true;
        }
        int childCount = listView.getChildCount();
        boolean z3 = false;
        for (int i = 0; i < childCount; i++) {
            View childAt = listView.getChildAt(i);
            c a4 = dVar.a(childAt.getTag());
            if (a4 != null) {
                if (childAt == view && a4.k == 2 && z2) {
                    z3 = true;
                } else if (a4.k != 0) {
                    a(a4);
                }
            }
        }
        if (!z2 || z3 || view == null || (a2 = dVar.a(view.getTag())) == null || simpleAppModel == null) {
            return false;
        }
        a2.f686a.setTag(Integer.valueOf(en.a().a(1, simpleAppModel.f1634a)));
        a2.d.setTag(simpleAppModel.q());
        b(context, a2);
        return true;
    }

    public static void a(Context context, c cVar, ArrayList<SimpleAppModel> arrayList, String str) {
        cVar.d.setVisibility(0);
        OneMoreAdapter oneMoreAdapter = new OneMoreAdapter(context, cVar.f686a, arrayList);
        oneMoreAdapter.a(str);
        cVar.f686a.setAdapter((ListAdapter) oneMoreAdapter);
        cVar.f686a.setVisibility(0);
        cVar.b.setVisibility(0);
        cVar.j.setVisibility(8);
        cVar.e.setVisibility(0);
        cVar.h.setVisibility(0);
        cVar.k = 2;
    }

    public static void a(c cVar) {
        cVar.f686a.setAdapter((ListAdapter) null);
        cVar.f686a.setTag(null);
        cVar.b.setVisibility(8);
        cVar.j.setVisibility(8);
        cVar.h.setVisibility(8);
        cVar.d.setVisibility(8);
        cVar.e.setVisibility(8);
        cVar.k = 0;
        cVar.c = null;
    }

    public static void a(Context context, c cVar) {
        cVar.b.setVisibility(8);
        cVar.e.setVisibility(0);
        cVar.d.setVisibility(0);
        cVar.i.setVisibility(0);
        cVar.i.setText(context.getResources().getString(R.string.one_more_app_fail));
        cVar.f.setVisibility(8);
        cVar.j.setVisibility(0);
        cVar.g.setVisibility(0);
        cVar.h.setVisibility(8);
        cVar.k = 3;
    }

    public static void b(Context context, c cVar) {
        cVar.b.setVisibility(8);
        cVar.j.setVisibility(0);
        cVar.d.setVisibility(0);
        cVar.i.setVisibility(0);
        cVar.i.setText(context.getResources().getString(R.string.one_more_app_loading));
        cVar.f.setVisibility(0);
        cVar.g.setVisibility(8);
        cVar.e.setVisibility(0);
        cVar.k = 1;
    }
}
