package com.tencent.assistant.adapter;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class cj extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ cm f742a;
    final /* synthetic */ LocalApkInfo b;
    final /* synthetic */ STInfoV2 c;
    final /* synthetic */ PreInstalledAppListAdapter d;

    cj(PreInstalledAppListAdapter preInstalledAppListAdapter, cm cmVar, LocalApkInfo localApkInfo, STInfoV2 sTInfoV2) {
        this.d = preInstalledAppListAdapter;
        this.f742a = cmVar;
        this.b = localApkInfo;
        this.c = sTInfoV2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.adapter.PreInstalledAppListAdapter.a(com.tencent.assistant.adapter.PreInstalledAppListAdapter, boolean):boolean
     arg types: [com.tencent.assistant.adapter.PreInstalledAppListAdapter, int]
     candidates:
      com.tencent.assistant.adapter.PreInstalledAppListAdapter.a(int, int):com.tencent.assistantv2.st.page.STInfoV2
      com.tencent.assistant.adapter.PreInstalledAppListAdapter.a(android.widget.TextView, long):void
      com.tencent.assistant.adapter.PreInstalledAppListAdapter.a(com.tencent.assistant.adapter.cm, com.tencent.assistant.localres.model.LocalApkInfo):void
      com.tencent.assistant.adapter.PreInstalledAppListAdapter.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, int):void
      com.tencent.assistant.adapter.PreInstalledAppListAdapter.a(boolean, boolean):void
      com.tencent.assistant.adapter.PreInstalledAppListAdapter.a(com.tencent.assistant.adapter.PreInstalledAppListAdapter, boolean):boolean */
    public void onTMAClick(View view) {
        if (this.d.i) {
            boolean unused = this.d.i = false;
            this.d.a(this.f742a, this.b);
            return;
        }
        this.d.b(this.f742a, this.b);
    }

    public STInfoV2 getStInfo() {
        this.c.actionId = 200;
        this.c.status = "02";
        this.c.extraData = this.b.mAppName;
        return this.c;
    }
}
