package com.tencent.assistant.adapter;

import android.os.Bundle;
import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.link.b;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class p implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppCategoryListAdapter f805a;

    p(AppCategoryListAdapter appCategoryListAdapter) {
        this.f805a = appCategoryListAdapter;
    }

    public void onClick(View view) {
        ColorCardItem colorCardItem = (ColorCardItem) view.getTag(R.id.category_data);
        if (colorCardItem != null) {
            Bundle bundle = new Bundle();
            if (this.f805a.g instanceof BaseActivity) {
                bundle.putSerializable(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, Integer.valueOf(((BaseActivity) this.f805a.g).f()));
            }
            bundle.putSerializable("com.tencent.assistant.ACTION_URL", colorCardItem.b());
            if (colorCardItem.b() != null) {
                XLog.d("AppCategoryListAdapter", "colorCardListener:(url)" + colorCardItem.b().f1970a);
                b.b(this.f805a.g, colorCardItem.b().f1970a, bundle);
            }
        }
    }
}
