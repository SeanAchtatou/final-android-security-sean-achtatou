package com.tencent.assistant.adapter;

import android.content.Context;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.AppIconView;
import com.tencent.assistant.component.RatingView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.model.AppGroupInfo;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.d;
import com.tencent.assistant.module.u;
import com.tencent.assistant.utils.bt;
import com.tencent.assistant.utils.ct;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/* compiled from: ProGuard */
public class cw extends BaseExpandableListAdapter implements UIEventListener {
    private static int f = 0;
    private static int g = (f + 1);

    /* renamed from: a  reason: collision with root package name */
    public AstApp f755a = AstApp.i();
    private LinkedHashMap<AppGroupInfo, ArrayList<SimpleAppModel>> b;
    /* access modifiers changed from: private */
    public Context c;
    private LayoutInflater d;
    private LayoutInflater e;
    /* access modifiers changed from: private */
    public View h;
    private HashMap<String, Integer> i = new HashMap<>();
    private ArrayList<AppGroupInfo> j;
    /* access modifiers changed from: private */
    public StatInfo k = new StatInfo();
    private int l = 2000;
    private long m = -100;

    public void a(Map<AppGroupInfo, ArrayList<SimpleAppModel>> map, long j2) {
        this.k.f3356a = j2;
        if (map != null && map.size() > 0) {
            if (this.b == null) {
                this.b = new LinkedHashMap<>();
            } else if (!this.b.isEmpty()) {
                this.b.clear();
            }
            if (this.j == null) {
                this.j = new ArrayList<>();
            } else if (!this.j.isEmpty()) {
                this.j.clear();
            }
            LinkedHashMap<AppGroupInfo, ArrayList<SimpleAppModel>> a2 = a(map);
            this.b.putAll(a2);
            for (AppGroupInfo add : a2.keySet()) {
                this.j.add(add);
            }
            notifyDataSetChanged();
        }
    }

    public void b(Map<AppGroupInfo, ArrayList<SimpleAppModel>> map, long j2) {
        this.k.f3356a = j2;
        if (map != null && map.size() > 0) {
            if (this.b == null) {
                this.b = new LinkedHashMap<>();
            }
            LinkedHashMap<AppGroupInfo, ArrayList<SimpleAppModel>> a2 = a(map);
            this.b.putAll(a2);
            for (AppGroupInfo add : a2.keySet()) {
                this.j.add(add);
            }
            notifyDataSetChanged();
        }
    }

    private LinkedHashMap<AppGroupInfo, ArrayList<SimpleAppModel>> a(Map<AppGroupInfo, ArrayList<SimpleAppModel>> map) {
        LinkedHashMap<AppGroupInfo, ArrayList<SimpleAppModel>> linkedHashMap = new LinkedHashMap<>();
        if (map != null && !map.isEmpty()) {
            for (AppGroupInfo next : map.keySet()) {
                ArrayList arrayList = map.get(next);
                ArrayList arrayList2 = new ArrayList();
                ArrayList arrayList3 = new ArrayList();
                ArrayList arrayList4 = new ArrayList();
                if (arrayList != null && !arrayList.isEmpty() && arrayList.size() > 5) {
                    Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        SimpleAppModel simpleAppModel = (SimpleAppModel) it.next();
                        if (ApkResourceManager.getInstance().getLocalApkInfo(simpleAppModel.c) != null) {
                            arrayList3.add(simpleAppModel);
                        } else {
                            arrayList4.add(simpleAppModel);
                        }
                    }
                    if (arrayList4.size() >= 5) {
                        arrayList2.addAll(arrayList4.subList(0, 5));
                        arrayList = arrayList2;
                    } else {
                        arrayList2.addAll(arrayList4);
                        if (arrayList2.size() < 5) {
                            arrayList2.addAll(arrayList2.size(), arrayList3.subList(0, 5 - arrayList2.size()));
                            arrayList = arrayList2;
                        } else {
                            arrayList = arrayList2;
                        }
                    }
                }
                linkedHashMap.put(next, arrayList);
            }
        }
        return linkedHashMap;
    }

    public Object getChild(int i2, int i3) {
        if (this.b == null || this.b.size() <= 0) {
            return null;
        }
        return this.b.get(this.j.get(i2)).get(i3);
    }

    public long getChildId(int i2, int i3) {
        return (long) i3;
    }

    public View getGroupView(int i2, boolean z, View view, ViewGroup viewGroup) {
        dd ddVar;
        if (view == null) {
            view = this.d.inflate((int) R.layout.rankrecommendlist_group_item, (ViewGroup) null);
            dd ddVar2 = new dd(this);
            ddVar2.f763a = (TextView) view.findViewById(R.id.group_title);
            view.setTag(ddVar2);
            ddVar = ddVar2;
        } else {
            ddVar = (dd) view.getTag();
        }
        ddVar.f763a.setText(this.j.get(i2).a());
        return view;
    }

    public View getChildView(int i2, int i3, boolean z, View view, ViewGroup viewGroup) {
        SimpleAppModel simpleAppModel;
        int i4;
        ArrayList arrayList = this.b.get(this.j.get(i2));
        if (arrayList == null || i3 > arrayList.size() - 1) {
            simpleAppModel = null;
        } else {
            simpleAppModel = (SimpleAppModel) arrayList.get(i3);
        }
        if (view != null) {
            i4 = ((Integer) view.getTag(R.id.card_type)).intValue();
        } else {
            i4 = 0;
        }
        if (f == a(i2, i3) && f == i4) {
            view = a(view, simpleAppModel, i2, i3, z);
        } else if (f == a(i2, i3) && g == i4) {
            view = a(null, simpleAppModel, i2, i3, z);
        } else if (g == a(i2, i3) && g == i4) {
            view = a(view, simpleAppModel, i2, i3);
        } else if (g == a(i2, i3) && f == i4) {
            view = a((View) null, simpleAppModel, i2, i3);
        }
        if (view != null) {
            if (z) {
                view.setPadding(0, 0, 0, df.b(3.0f));
            } else {
                view.setPadding(0, 0, 0, 0);
            }
        }
        return view;
    }

    private View a(View view, SimpleAppModel simpleAppModel, int i2, int i3, boolean z) {
        de deVar;
        if (view == null) {
            de deVar2 = new de(this, null);
            view = this.e.inflate((int) R.layout.app_item_no_desc, (ViewGroup) null);
            deVar2.f764a = view.findViewById(R.id.app_item);
            deVar2.c = (AppIconView) view.findViewById(R.id.app_icon_img);
            deVar2.d = (TextView) view.findViewById(R.id.app_name_txt);
            deVar2.g = (DownloadButton) view.findViewById(R.id.state_app_btn);
            deVar2.e = (RatingView) view.findViewById(R.id.app_ratingview);
            deVar2.f = (TextView) view.findViewById(R.id.download_times_txt);
            deVar2.h = view.findViewById(R.id.app_updatesizeinfo);
            deVar2.i = (TextView) view.findViewById(R.id.app_size_sumsize);
            deVar2.j = (TextView) view.findViewById(R.id.app_score_truesize);
            deVar2.k = (TextView) view.findViewById(R.id.app_size_text);
            deVar2.b = (ImageView) view.findViewById(R.id.sort_num_image);
            deVar2.l = (ImageView) view.findViewById(R.id.last_line);
            view.setTag(deVar2);
            deVar = deVar2;
        } else {
            deVar = (de) view.getTag();
        }
        view.setTag(R.id.card_type, Integer.valueOf(f));
        deVar.f764a.setTag(R.id.tma_st_slot_tag, b(i2, i3));
        deVar.f764a.setOnClickListener(new cx(this, simpleAppModel, i2, i3));
        a(deVar, simpleAppModel, i2, i3);
        if (z) {
            deVar.f764a.setBackgroundResource(R.drawable.bg_card_download_down_selector);
            deVar.l.setVisibility(8);
        } else {
            deVar.f764a.setBackgroundResource(R.drawable.bg_card_download_middel_selector);
            deVar.l.setVisibility(0);
        }
        return view;
    }

    private View a(View view, SimpleAppModel simpleAppModel, int i2, int i3) {
        dc dcVar;
        if (view == null) {
            dc dcVar2 = new dc(this, null);
            view = this.e.inflate((int) R.layout.competitive_card, (ViewGroup) null);
            dcVar2.f762a = (TextView) view.findViewById(R.id.title);
            dcVar2.b = (TXImageView) view.findViewById(R.id.pic);
            dcVar2.c = (ImageView) view.findViewById(R.id.vedio);
            dcVar2.d = (AppIconView) view.findViewById(R.id.icon);
            dcVar2.e = (DownloadButton) view.findViewById(R.id.download_soft_btn);
            dcVar2.f = (TextView) view.findViewById(R.id.name);
            dcVar2.g = (TextView) view.findViewById(R.id.app_size_sumsize);
            dcVar2.h = (ImageView) view.findViewById(R.id.app_size_redline);
            dcVar2.i = (TextView) view.findViewById(R.id.app_score_truesize);
            dcVar2.j = (TextView) view.findViewById(R.id.app_size_text);
            dcVar2.k = (TextView) view.findViewById(R.id.description);
            view.setTag(dcVar2);
            dcVar = dcVar2;
        } else {
            dcVar = (dc) view.getTag();
        }
        view.setTag(R.id.card_type, Integer.valueOf(g));
        view.setTag(R.id.tma_st_slot_tag, b(i2, i3));
        view.setOnClickListener(new cy(this, simpleAppModel, i2, i3));
        a(dcVar, simpleAppModel, i2, i3);
        return view;
    }

    public int getChildrenCount(int i2) {
        if (this.b == null || this.j == null || i2 >= this.j.size()) {
            return 0;
        }
        ArrayList arrayList = this.b.get(this.j.get(i2));
        if (arrayList != null) {
            return arrayList.size();
        }
        return 0;
    }

    public Object getGroup(int i2) {
        if (this.j == null || this.j.size() <= 0 || i2 >= this.j.size() || i2 < 0) {
            return null;
        }
        return this.j.get(i2);
    }

    public int getGroupCount() {
        if (this.b == null || this.b.size() <= 0) {
            return 0;
        }
        return this.b.size();
    }

    public long getGroupId(int i2) {
        return (long) i2;
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isChildSelectable(int i2, int i3) {
        return false;
    }

    /* access modifiers changed from: private */
    public String b(int i2, int i3) {
        return "03_" + String.valueOf(i2 + 1) + String.format("%02d", Integer.valueOf(i3 + 1));
    }

    private void a(de deVar, SimpleAppModel simpleAppModel, int i2, int i3) {
        if (simpleAppModel != null && deVar != null) {
            deVar.d.setText(simpleAppModel.d);
            deVar.c.setSimpleAppModel(simpleAppModel, this.k, -100);
            deVar.g.a(simpleAppModel);
            deVar.g.setTag(R.id.group_position, Integer.valueOf(i2));
            deVar.f.setText(ct.a(simpleAppModel.p, 0));
            deVar.e.setRating(simpleAppModel.q);
            if (simpleAppModel.a()) {
                deVar.h.setVisibility(0);
                deVar.k.setVisibility(8);
                deVar.i.setText(bt.a(simpleAppModel.k));
                deVar.j.setText(bt.a(simpleAppModel.v));
            } else {
                deVar.h.setVisibility(8);
                deVar.k.setVisibility(0);
                deVar.k.setText(bt.a(simpleAppModel.k));
            }
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.c, simpleAppModel, b(i2, i3), 200, null);
            buildSTInfo.rankGroupId = this.j.get(i2).f1628a;
            deVar.g.a(buildSTInfo, new cz(this), (d) null, deVar.g);
            c.a(this.c, simpleAppModel, deVar.d, false);
        }
    }

    private void a(dc dcVar, SimpleAppModel simpleAppModel, int i2, int i3) {
        if (simpleAppModel != null && dcVar != null) {
            dcVar.f762a.setText(simpleAppModel.V);
            if (!TextUtils.isEmpty(simpleAppModel.Y)) {
                dcVar.b.updateImageView(simpleAppModel.Y, -1, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            }
            if (simpleAppModel.Z == null || simpleAppModel.Z.length() == 0) {
                dcVar.c.setVisibility(8);
            } else {
                dcVar.c.setVisibility(0);
                dcVar.b.setOnClickListener(new da(this, simpleAppModel));
            }
            dcVar.d.setSimpleAppModel(simpleAppModel, this.k, -100);
            dcVar.d.setTag(simpleAppModel.q());
            dcVar.e.a(simpleAppModel);
            dcVar.e.setTag(R.id.group_position, Integer.valueOf(i2));
            dcVar.f.setText(simpleAppModel.d);
            if (simpleAppModel.a()) {
                dcVar.h.setVisibility(0);
                dcVar.g.setVisibility(0);
                dcVar.i.setVisibility(0);
                dcVar.j.setVisibility(8);
                dcVar.g.setText(bt.a(simpleAppModel.k));
                dcVar.i.setText(bt.a(simpleAppModel.v));
            } else {
                dcVar.h.setVisibility(8);
                dcVar.g.setVisibility(8);
                dcVar.i.setVisibility(8);
                dcVar.j.setVisibility(0);
                dcVar.j.setText(bt.a(simpleAppModel.k));
            }
            if (TextUtils.isEmpty(simpleAppModel.X)) {
                dcVar.k.setVisibility(8);
            } else {
                dcVar.k.setVisibility(0);
                dcVar.k.setText(simpleAppModel.X);
            }
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.c, simpleAppModel, b(i2, i3), 200, null);
            buildSTInfo.rankGroupId = this.j.get(i2).f1628a;
            dcVar.e.a(buildSTInfo, new db(this), (d) null, dcVar.e);
        }
    }

    public void handleUIEvent(Message message) {
        DownloadButton downloadButton = (DownloadButton) this.h.findViewById(R.id.state_app_btn);
        if (downloadButton != null) {
            int intValue = ((Integer) downloadButton.getTag(R.id.group_position)).intValue();
            if (this.j != null && intValue < this.j.size()) {
                ArrayList arrayList = this.b.get(this.j.get(intValue));
                switch (message.what) {
                    case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE /*1009*/:
                        DownloadInfo downloadInfo = null;
                        if (!(message.obj instanceof DownloadInfo) || ((downloadInfo = (DownloadInfo) message.obj) != null && !TextUtils.isEmpty(downloadInfo.downloadTicket))) {
                            Iterator it = arrayList.iterator();
                            while (it.hasNext()) {
                                if (((SimpleAppModel) it.next()).q().equals(downloadInfo.downloadTicket)) {
                                    notifyDataSetChanged();
                                }
                            }
                            notifyDataSetChanged();
                            return;
                        }
                        return;
                    case 1016:
                        u.g(arrayList);
                        notifyDataSetChanged();
                        return;
                    default:
                        return;
                }
            }
        }
    }

    public int a(int i2, int i3) {
        SimpleAppModel.CARD_TYPE card_type;
        ArrayList arrayList;
        SimpleAppModel simpleAppModel;
        SimpleAppModel.CARD_TYPE card_type2 = SimpleAppModel.CARD_TYPE.NORMAL;
        if (this.b == null || this.j == null || i2 >= this.j.size() || (arrayList = this.b.get(this.j.get(i2))) == null || i3 >= arrayList.size() || (simpleAppModel = (SimpleAppModel) arrayList.get(i3)) == null) {
            card_type = card_type2;
        } else {
            card_type = simpleAppModel.U;
        }
        if (SimpleAppModel.CARD_TYPE.NORMAL == card_type) {
            return f;
        }
        if (SimpleAppModel.CARD_TYPE.QUALITY == card_type) {
            return g;
        }
        return f;
    }
}
