package com.tencent.assistant.adapter;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.k;
import com.tencent.assistantv2.model.ItemElement;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class ap extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f702a;
    final /* synthetic */ ChildSettingAdapter b;

    ap(ChildSettingAdapter childSettingAdapter, int i) {
        this.b = childSettingAdapter;
        this.f702a = i;
    }

    public void onTMAClick(View view) {
        ItemElement itemElement = (ItemElement) this.b.getItem(this.f702a);
        if (itemElement != null) {
            ar arVar = (ar) view.getTag();
            arVar.e.setSelected(!arVar.e.isSelected());
            k.a(itemElement.d, arVar.e.isSelected());
        }
    }

    public STInfoV2 getStInfo(View view) {
        if (!(view.getTag() instanceof ar)) {
            return null;
        }
        return this.b.a(this.b.a(this.f702a), this.b.a(((ar) view.getTag()).e.isSelected()), 200);
    }
}
