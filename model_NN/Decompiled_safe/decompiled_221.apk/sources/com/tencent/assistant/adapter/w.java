package com.tencent.assistant.adapter;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.appdetail.process.s;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.update.k;
import com.tencent.assistant.protocol.jce.AppUpdateInfo;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.component.BreakTextView;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.cloud.updaterec.g;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public abstract class w {
    public static Pair<View, x> a(Context context, LayoutInflater layoutInflater, View view, int i) {
        x xVar;
        if (view == null || !(view.getTag() instanceof x)) {
            view = layoutInflater.inflate((int) R.layout.app_updatelist_item, (ViewGroup) null);
            x xVar2 = new x();
            xVar2.f821a = view;
            xVar2.b = (RelativeLayout) view.findViewById(R.id.root);
            xVar2.c = (TXAppIconView) view.findViewById(R.id.app_icon_img);
            xVar2.d = (TextView) view.findViewById(R.id.title);
            xVar2.f = (DownloadButton) view.findViewById(R.id.state_app_btn);
            xVar2.e = (RelativeLayout) view.findViewById(R.id.title_layout);
            xVar2.g = (ListItemInfoView) view.findViewById(R.id.download_info);
            xVar2.h = (TextView) view.findViewById(R.id.desc);
            xVar2.l = (ImageView) view.findViewById(R.id.ignore_img);
            if (i == 2) {
                xVar2.l.setImageDrawable(context.getResources().getDrawable(R.drawable.reminder));
            }
            xVar2.j = (BreakTextView) view.findViewById(R.id.new_feature);
            xVar2.k = (TextView) view.findViewById(R.id.new_feature_all);
            xVar2.i = (RelativeLayout) view.findViewById(R.id.description_bottomlayout);
            xVar2.m = (ImageView) view.findViewById(R.id.show_more);
            xVar2.n = new g();
            xVar2.n.f3474a = (ListView) view.findViewById(R.id.one_more_list);
            xVar2.n.b = (RelativeLayout) view.findViewById(R.id.one_more_list_parent);
            xVar2.n.f3474a.setDivider(null);
            xVar2.n.e = (ImageView) view.findViewById(R.id.one_more_app_line_top_long);
            xVar2.n.j = (RelativeLayout) view.findViewById(R.id.app_one_more_loading_parent);
            xVar2.n.i = (TextView) view.findViewById(R.id.app_one_more_loading);
            xVar2.n.f = (ProgressBar) view.findViewById(R.id.app_one_more_loading_gif);
            xVar2.n.g = (ImageView) view.findViewById(R.id.one_more_app_error_img);
            xVar2.n.d = (ImageView) view.findViewById(R.id.one_more_app_arrow);
            xVar2.n.h = (TextView) view.findViewById(R.id.app_one_more_desc);
            xVar2.n.k = (TextView) view.findViewById(R.id.one_more_title);
            xVar2.e.setTag(xVar2);
            view.setTag(xVar2);
            xVar = xVar2;
        } else {
            xVar = (x) view.getTag();
        }
        return Pair.create(view, xVar);
    }

    public static void a(Context context, x xVar, SimpleAppModel simpleAppModel, STInfoV2 sTInfoV2) {
        xVar.b.setPadding(0, 0, 0, df.a(context, 5.0f));
        xVar.d.setText(simpleAppModel.d);
        xVar.g.a(simpleAppModel);
        xVar.c.updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        xVar.f.a(simpleAppModel);
        if (s.a(simpleAppModel)) {
            xVar.f.setClickable(false);
        } else {
            xVar.f.setClickable(true);
            xVar.f.a(sTInfoV2);
        }
        String a2 = a(simpleAppModel.c);
        if (TextUtils.isEmpty(a2)) {
            xVar.h.setVisibility(8);
            ViewGroup.LayoutParams layoutParams = xVar.e.getLayoutParams();
            if (layoutParams instanceof RelativeLayout.LayoutParams) {
                ((RelativeLayout.LayoutParams) layoutParams).setMargins(((RelativeLayout.LayoutParams) layoutParams).leftMargin, (int) context.getResources().getDimension(R.dimen.update_list_item_title_noreason_margin_top), ((RelativeLayout.LayoutParams) layoutParams).rightMargin, (int) context.getResources().getDimension(R.dimen.update_list_item_title_noreason_margin_bottom));
                return;
            }
            return;
        }
        xVar.h.setVisibility(0);
        xVar.h.setText(Html.fromHtml(a2));
        ViewGroup.LayoutParams layoutParams2 = xVar.e.getLayoutParams();
        if (layoutParams2 instanceof RelativeLayout.LayoutParams) {
            ((RelativeLayout.LayoutParams) layoutParams2).setMargins(((RelativeLayout.LayoutParams) layoutParams2).leftMargin, (int) context.getResources().getDimension(R.dimen.update_list_item_title_margin_top), ((RelativeLayout.LayoutParams) layoutParams2).rightMargin, (int) context.getResources().getDimension(R.dimen.update_list_item_title_margin_bottom));
        }
    }

    private static String a(String str) {
        AppUpdateInfo a2 = k.b().a(str);
        return (a2 == null || a2.v == null) ? Constants.STR_EMPTY : a2.v;
    }
}
