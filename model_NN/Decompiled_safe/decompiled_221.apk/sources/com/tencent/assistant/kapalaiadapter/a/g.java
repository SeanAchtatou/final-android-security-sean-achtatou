package com.tencent.assistant.kapalaiadapter.a;

import android.content.Context;
import android.telephony.TelephonyManager;

/* compiled from: ProGuard */
public class g implements j {

    /* renamed from: a  reason: collision with root package name */
    private Object f1385a = null;

    public Object a(int i, Context context) {
        if (this.f1385a == null) {
            try {
                this.f1385a = com.tencent.assistant.kapalaiadapter.g.a("com.yulong.android.telephony.CPTelephonyManager", "getDefault");
            } catch (Throwable th) {
            }
        }
        return this.f1385a;
    }

    public String b(int i, Context context) {
        try {
            return (String) com.tencent.assistant.kapalaiadapter.g.a(a(i, context), "getDualSubscriberId", new Object[]{Integer.valueOf(i + 1)});
        } catch (Throwable th) {
            return null;
        }
    }

    public String c(int i, Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
    }
}
