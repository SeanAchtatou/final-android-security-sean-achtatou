package com.tencent.assistant.graphic;

import android.graphics.Bitmap;
import android.os.Process;
import com.tencent.assistant.utils.r;
import com.tencent.connect.common.Constants;
import com.tencent.tmsecurelite.optimize.ISystemOptimize;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: ProGuard */
public class c {
    private static int W;
    private Bitmap A;
    private f B;
    private boolean C;
    private byte[] D;
    private int E;
    private int F;
    private int G;
    private boolean H;
    private int I;
    private int J;
    private short[] K;
    private byte[] L;
    private byte[] M;
    private byte[] N;
    private f O;
    private int P;
    private d Q;
    private int R;
    private boolean S;
    private boolean T;
    private int U;
    private int V;

    /* renamed from: a  reason: collision with root package name */
    public int f1327a;
    public int b;
    private InputStream c;
    private int d;
    private boolean e;
    private int f;
    private int g;
    private int[] h;
    private int[] i;
    private int[] j;
    private int k;
    private int l;
    private int m;
    private int n;
    private boolean o;
    private boolean p;
    private int q;
    private int r;
    private int s;
    private int t;
    private int u;
    private int v;
    private int w;
    private int x;
    private int y;
    private Bitmap z;

    public c(InputStream inputStream, d dVar, boolean z2, int i2) {
        this(inputStream, dVar, 0, z2, i2);
    }

    public c(InputStream inputStream, d dVar, int i2, boolean z2, int i3) {
        this.g = 1;
        this.B = null;
        this.C = false;
        this.D = new byte[Process.PROC_COMBINE];
        this.E = 0;
        this.F = 0;
        this.G = 0;
        this.H = false;
        this.I = 0;
        this.Q = null;
        this.S = true;
        this.T = false;
        this.U = 0;
        this.V = 0;
        this.c = inputStream;
        this.Q = dVar;
        this.R = i2;
        this.T = z2;
        this.U = i3;
    }

    public void a(int i2) {
        W = i2;
    }

    public int a() {
        if (this.c == null) {
            return 2;
        }
        int e2 = e();
        try {
            this.c.close();
            this.c = null;
            return e2;
        } catch (IOException e3) {
            return e2;
        }
    }

    public int b() {
        return this.d;
    }

    public int c() {
        return this.P;
    }

    private void d() {
        int i2;
        int i3;
        int[] iArr = new int[(this.f1327a * this.b)];
        if (this.G > 0) {
            if (this.G == 3) {
                int i4 = this.P - 2;
                if (i4 > 0) {
                    this.A = b(i4 - 1);
                } else {
                    this.A = null;
                }
            }
            if (this.A != null) {
                this.A.getPixels(iArr, 0, this.f1327a, 0, 0, this.f1327a, this.b);
                if (this.G == 2) {
                    if (!this.H) {
                        i3 = this.m;
                    } else {
                        i3 = 0;
                    }
                    for (int i5 = 0; i5 < this.y; i5++) {
                        int i6 = ((this.w + i5) * this.f1327a) + this.v;
                        int i7 = this.x + i6;
                        while (i6 < i7) {
                            iArr[i6] = i3;
                            i6++;
                        }
                    }
                }
            }
        }
        int i8 = 8;
        int i9 = 1;
        int i10 = 0;
        for (int i11 = 0; i11 < this.u; i11++) {
            if (this.p) {
                if (i10 >= this.u) {
                    i9++;
                    switch (i9) {
                        case 2:
                            i10 = 4;
                            break;
                        case 3:
                            i10 = 2;
                            i8 = 4;
                            break;
                        case 4:
                            i10 = 1;
                            i8 = 2;
                            break;
                    }
                }
                int i12 = i10;
                i10 += i8;
                i2 = i12;
            } else {
                i2 = i11;
            }
            int i13 = i2 + this.s;
            if (i13 < this.b) {
                int i14 = this.f1327a * i13;
                int i15 = i14 + this.r;
                int i16 = this.t + i15;
                if (this.f1327a + i14 < i16) {
                    i16 = this.f1327a + i14;
                }
                int i17 = this.t * i11;
                int i18 = i15;
                while (i18 < i16) {
                    int i19 = i17 + 1;
                    int i20 = this.j[this.N[i17] & 255];
                    if (i20 != 0) {
                        iArr[i18] = i20;
                    }
                    i18++;
                    i17 = i19;
                }
            }
        }
        this.z = Bitmap.createBitmap(iArr, this.f1327a, this.b, Bitmap.Config.ARGB_4444);
    }

    public Bitmap b(int i2) {
        f c2 = c(i2);
        if (c2 == null) {
            return null;
        }
        return c2.f1329a;
    }

    public f c(int i2) {
        int i3 = 0;
        for (f fVar = this.O; fVar != null; fVar = fVar.c) {
            if (i3 == i2) {
                return fVar;
            }
            i3++;
        }
        return null;
    }

    private int e() {
        h();
        if (this.c != null) {
            m();
            if (!g()) {
                try {
                    k();
                } catch (OutOfMemoryError e2) {
                    this.d = -2;
                    u();
                }
                if (this.d == -2) {
                    this.d = 2;
                } else if (this.P < 0) {
                    this.d = 1;
                    if (this.Q != null) {
                        this.Q.a(false, -1);
                    }
                } else {
                    this.d = -1;
                    if (this.Q != null) {
                        this.Q.a(true, -1);
                    }
                }
            }
        } else {
            this.d = 2;
            if (this.Q != null) {
                this.Q.a(false, -1);
            }
        }
        return this.d;
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:59:0x007d */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:65:0x007d */
    /* JADX WARN: Type inference failed for: r8v2 */
    /* JADX WARN: Type inference failed for: r7v2 */
    /* JADX WARN: Type inference failed for: r8v5 */
    /* JADX WARN: Type inference failed for: r8v8, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void f() {
        /*
            r23 = this;
            r15 = -1
            r0 = r23
            int r1 = r0.t
            r0 = r23
            int r2 = r0.u
            int r16 = r1 * r2
            r0 = r23
            byte[] r1 = r0.N
            if (r1 == 0) goto L_0x001a
            r0 = r23
            byte[] r1 = r0.N
            int r1 = r1.length
            r0 = r16
            if (r1 >= r0) goto L_0x0022
        L_0x001a:
            r0 = r16
            byte[] r1 = new byte[r0]
            r0 = r23
            r0.N = r1
        L_0x0022:
            r0 = r23
            short[] r1 = r0.K
            if (r1 != 0) goto L_0x0030
            r1 = 4096(0x1000, float:5.74E-42)
            short[] r1 = new short[r1]
            r0 = r23
            r0.K = r1
        L_0x0030:
            r0 = r23
            byte[] r1 = r0.L
            if (r1 != 0) goto L_0x003e
            r1 = 4096(0x1000, float:5.74E-42)
            byte[] r1 = new byte[r1]
            r0 = r23
            r0.L = r1
        L_0x003e:
            r0 = r23
            byte[] r1 = r0.M
            if (r1 != 0) goto L_0x004c
            r1 = 4097(0x1001, float:5.741E-42)
            byte[] r1 = new byte[r1]
            r0 = r23
            r0.M = r1
        L_0x004c:
            int r17 = r23.i()
            r1 = 1
            int r18 = r1 << r17
            int r19 = r18 + 1
            int r11 = r18 + 2
            int r3 = r17 + 1
            r1 = 1
            int r1 = r1 << r3
            int r4 = r1 + -1
            r1 = 0
        L_0x005e:
            r0 = r18
            if (r1 >= r0) goto L_0x0073
            r0 = r23
            short[] r2 = r0.K
            r5 = 0
            r2[r1] = r5
            r0 = r23
            byte[] r2 = r0.L
            byte r5 = (byte) r1
            r2[r1] = r5
            int r1 = r1 + 1
            goto L_0x005e
        L_0x0073:
            r12 = 0
            r1 = 0
            r5 = r12
            r6 = r12
            r7 = r12
            r14 = r1
            r2 = r12
            r8 = r12
            r10 = r15
            r1 = r12
        L_0x007d:
            r0 = r16
            if (r14 >= r0) goto L_0x008d
            if (r5 != 0) goto L_0x016c
            if (r8 >= r3) goto L_0x00ae
            if (r2 != 0) goto L_0x009d
            int r2 = r23.j()
            if (r2 > 0) goto L_0x009c
        L_0x008d:
            r1 = r12
        L_0x008e:
            r0 = r16
            if (r1 >= r0) goto L_0x0167
            r0 = r23
            byte[] r2 = r0.N
            r3 = 0
            r2[r1] = r3
            int r1 = r1 + 1
            goto L_0x008e
        L_0x009c:
            r1 = 0
        L_0x009d:
            r0 = r23
            byte[] r9 = r0.D
            byte r9 = r9[r1]
            r9 = r9 & 255(0xff, float:3.57E-43)
            int r9 = r9 << r8
            int r7 = r7 + r9
            int r8 = r8 + 8
            int r1 = r1 + 1
            int r2 = r2 + -1
            goto L_0x007d
        L_0x00ae:
            r9 = r7 & r4
            int r7 = r7 >> r3
            int r8 = r8 - r3
            if (r9 > r11) goto L_0x008d
            r0 = r19
            if (r9 == r0) goto L_0x008d
            r0 = r18
            if (r9 != r0) goto L_0x00c6
            int r3 = r17 + 1
            r4 = 1
            int r4 = r4 << r3
            int r4 = r4 + -1
            int r11 = r18 + 2
            r10 = r15
            goto L_0x007d
        L_0x00c6:
            if (r10 != r15) goto L_0x00da
            r0 = r23
            byte[] r10 = r0.M
            int r6 = r5 + 1
            r0 = r23
            byte[] r13 = r0.L
            byte r13 = r13[r9]
            r10[r5] = r13
            r5 = r6
            r10 = r9
            r6 = r9
            goto L_0x007d
        L_0x00da:
            if (r9 != r11) goto L_0x0168
            r0 = r23
            byte[] r0 = r0.M
            r20 = r0
            int r13 = r5 + 1
            byte r6 = (byte) r6
            r20[r5] = r6
            r6 = r10
        L_0x00e8:
            r0 = r18
            if (r6 <= r0) goto L_0x0106
            r0 = r23
            byte[] r0 = r0.M
            r20 = r0
            int r5 = r13 + 1
            r0 = r23
            byte[] r0 = r0.L
            r21 = r0
            byte r21 = r21[r6]
            r20[r13] = r21
            r0 = r23
            short[] r13 = r0.K
            short r6 = r13[r6]
            r13 = r5
            goto L_0x00e8
        L_0x0106:
            r0 = r23
            byte[] r5 = r0.L
            byte r5 = r5[r6]
            r6 = r5 & 255(0xff, float:3.57E-43)
            r5 = 4096(0x1000, float:5.74E-42)
            if (r11 >= r5) goto L_0x008d
            r0 = r23
            byte[] r0 = r0.M
            r20 = r0
            int r5 = r13 + 1
            byte r0 = (byte) r6
            r21 = r0
            r20[r13] = r21
            r0 = r23
            short[] r13 = r0.K
            short r10 = (short) r10
            r13[r11] = r10
            r0 = r23
            byte[] r10 = r0.L
            byte r13 = (byte) r6
            r10[r11] = r13
            int r10 = r11 + 1
            r11 = r10 & r4
            if (r11 != 0) goto L_0x013a
            r11 = 4096(0x1000, float:5.74E-42)
            if (r10 >= r11) goto L_0x013a
            int r3 = r3 + 1
            int r4 = r4 + r10
        L_0x013a:
            r22 = r5
            r5 = r7
            r7 = r9
            r9 = r4
            r4 = r6
            r6 = r8
            r8 = r3
            r3 = r22
        L_0x0144:
            int r11 = r3 + -1
            r0 = r23
            byte[] r13 = r0.N
            int r3 = r12 + 1
            r0 = r23
            byte[] r0 = r0.M
            r20 = r0
            byte r20 = r20[r11]
            r13[r12] = r20
            int r12 = r14 + 1
            r14 = r12
            r12 = r3
            r3 = r8
            r8 = r6
            r6 = r4
            r4 = r9
            r22 = r7
            r7 = r5
            r5 = r11
            r11 = r10
            r10 = r22
            goto L_0x007d
        L_0x0167:
            return
        L_0x0168:
            r13 = r5
            r6 = r9
            goto L_0x00e8
        L_0x016c:
            r9 = r4
            r4 = r6
            r6 = r8
            r8 = r3
            r3 = r5
            r5 = r7
            r7 = r10
            r10 = r11
            goto L_0x0144
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.graphic.c.f():void");
    }

    private boolean g() {
        return this.d != 0;
    }

    private void h() {
        this.d = 0;
        this.P = 0;
        this.O = null;
        this.h = null;
        this.i = null;
    }

    private int i() {
        try {
            return this.c.read();
        } catch (Exception e2) {
            this.d = 1;
            return 0;
        }
    }

    private int j() {
        this.E = i();
        int i2 = 0;
        if (this.E > 0) {
            while (i2 < this.E) {
                try {
                    int read = this.c.read(this.D, i2, this.E - i2);
                    if (read == -1) {
                        break;
                    }
                    i2 += read;
                } catch (Exception e2) {
                }
            }
            if (i2 < this.E) {
                this.d = 1;
            }
        }
        return i2;
    }

    private int[] d(int i2) {
        int i3;
        int i4 = 0;
        int i5 = i2 * 3;
        int[] iArr = null;
        byte[] bArr = new byte[i5];
        try {
            i3 = this.c.read(bArr);
        } catch (Exception e2) {
            i3 = 0;
        }
        if (i3 < i5) {
            this.d = 1;
        } else {
            iArr = new int[Process.PROC_COMBINE];
            for (int i6 = 0; i6 < i2; i6++) {
                int i7 = i4 + 1;
                int i8 = i7 + 1;
                i4 = i8 + 1;
                iArr[i6] = ((bArr[i4] & 255) << 16) | -16777216 | ((bArr[i7] & 255) << 8) | (bArr[i8] & 255);
            }
        }
        return iArr;
    }

    private void k() {
        boolean z2 = false;
        while (!z2 && !g()) {
            switch (i()) {
                case 0:
                    break;
                case ISystemOptimize.T_cancelScanRubbish /*33*/:
                    switch (i()) {
                        case 249:
                            l();
                            continue;
                        case 255:
                            j();
                            String str = Constants.STR_EMPTY;
                            for (int i2 = 0; i2 < 11; i2++) {
                                str = str + ((char) this.D[i2]);
                            }
                            if (!str.equals("NETSCAPE2.0")) {
                                s();
                                break;
                            } else {
                                p();
                                continue;
                            }
                        default:
                            s();
                            continue;
                    }
                case 44:
                    n();
                    break;
                case 59:
                    z2 = true;
                    break;
                default:
                    this.d = 1;
                    break;
            }
        }
    }

    private void l() {
        boolean z2 = true;
        i();
        int i2 = i();
        this.F = (i2 & 28) >> 2;
        if (this.F == 0) {
            this.F = 1;
        }
        if ((i2 & 1) == 0) {
            z2 = false;
        }
        this.H = z2;
        this.I = q() * 10;
        this.J = i();
        i();
    }

    private void m() {
        String str = Constants.STR_EMPTY;
        for (int i2 = 0; i2 < 6; i2++) {
            str = str + ((char) i());
        }
        if (!str.toUpperCase().startsWith("GIF")) {
            this.d = 1;
            return;
        }
        o();
        if (this.e && !g()) {
            this.h = d(this.f);
            this.l = this.h[this.k];
        }
    }

    private void n() {
        boolean z2;
        int i2;
        this.r = q();
        this.s = q();
        this.t = q();
        this.u = q();
        int i3 = i();
        this.o = (i3 & 128) != 0;
        if ((i3 & 64) != 0) {
            z2 = true;
        } else {
            z2 = false;
        }
        this.p = z2;
        this.q = 2 << (i3 & 7);
        if (this.o) {
            this.i = d(this.q);
            this.j = this.i;
        } else {
            this.j = this.h;
            if (this.k == this.J) {
                this.l = 0;
            }
        }
        if (this.H) {
            i2 = this.j[this.J];
            this.j[this.J] = 0;
        } else {
            i2 = 0;
        }
        if (this.j == null) {
            this.d = 1;
        }
        if (!g()) {
            f();
            s();
            if (!g()) {
                if (this.V > this.U) {
                    this.V = 0;
                }
                if (this.V != 0) {
                    this.V++;
                    return;
                }
                this.V++;
                if (this.T || this.P <= W) {
                    this.P++;
                    if (!this.T) {
                        r.a(this.f1327a * this.b, 5);
                        d();
                        if (this.O == null) {
                            this.O = new f(this.z, this.I * (this.U + 1));
                            this.B = this.O;
                        } else {
                            f fVar = this.O;
                            while (fVar.c != null) {
                                fVar = fVar.c;
                            }
                            fVar.c = new f(this.z, this.I * (this.U + 1));
                        }
                        if (this.H) {
                            this.j[this.J] = i2;
                        }
                        r();
                        if (this.Q != null) {
                            this.Q.a(true, this.P);
                        }
                        if (t()) {
                            this.d = -1;
                        }
                    }
                }
            }
        }
    }

    private void o() {
        this.f1327a = q();
        this.b = q();
        int i2 = i();
        this.e = (i2 & 128) != 0;
        this.f = 2 << (i2 & 7);
        this.k = i();
        this.n = i();
    }

    private void p() {
        do {
            j();
            if (this.D[0] == 1) {
                this.g = (this.D[1] & 255) | ((this.D[2] & 255) << 8);
            }
            if (this.E <= 0) {
                return;
            }
        } while (!g());
    }

    private int q() {
        return i() | (i() << 8);
    }

    private void r() {
        this.G = this.F;
        this.v = this.r;
        this.w = this.s;
        this.x = this.t;
        this.y = this.u;
        this.A = this.z;
        this.m = this.l;
        this.F = 0;
        this.H = false;
        this.I = 0;
        this.i = null;
    }

    private void s() {
        do {
            j();
            if (this.E <= 0) {
                return;
            }
        } while (!g());
    }

    private boolean t() {
        if (this.R == 0) {
            return false;
        }
        int i2 = 0;
        for (f fVar = this.O; fVar != null; fVar = fVar.c) {
            if (fVar.f1329a != null) {
                i2 += fVar.f1329a.getWidth() * fVar.f1329a.getHeight() * 2;
            }
        }
        return i2 > this.R;
    }

    private void u() {
        for (f fVar = this.O; fVar != null; fVar = fVar.c) {
            if (fVar.f1329a != null && !fVar.f1329a.isRecycled()) {
                fVar.f1329a.recycle();
                fVar.f1329a = null;
            }
        }
    }
}
