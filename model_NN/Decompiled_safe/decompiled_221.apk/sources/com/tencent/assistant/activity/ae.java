package com.tencent.assistant.activity;

import android.os.Handler;
import android.os.Message;

/* compiled from: ProGuard */
class ae extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppBackupActivity f393a;

    ae(AppBackupActivity appBackupActivity) {
        this.f393a = appBackupActivity;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 11901:
                this.f393a.a(this.f393a.Z, 0.0f, 90.0f, false, this.f393a.ak);
                return;
            case 11902:
                this.f393a.R.setVisibility(0);
                this.f393a.S.setVisibility(0);
                this.f393a.R.startAnimation(this.f393a.V);
                this.f393a.S.startAnimation(this.f393a.W);
                return;
            default:
                return;
        }
    }
}
