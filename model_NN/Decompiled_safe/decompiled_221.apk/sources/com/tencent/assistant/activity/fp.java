package com.tencent.assistant.activity;

import android.os.Message;
import com.tencent.assistant.utils.XLog;
import com.tencent.tmsecurelite.commom.DataEntity;
import com.tencent.tmsecurelite.commom.d;
import java.util.ArrayList;

/* compiled from: ProGuard */
class fp extends d {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SpaceCleanActivity f585a;

    fp(SpaceCleanActivity spaceCleanActivity) {
        this.f585a = spaceCleanActivity;
    }

    public void a(int i, ArrayList<DataEntity> arrayList) {
        XLog.d("miles", "onArrayResultGot.. paramAnonymousInt = " + i + " DataEntity = " + arrayList.toString());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.SpaceCleanActivity.a(com.tencent.assistant.activity.SpaceCleanActivity, java.lang.String, boolean):void
     arg types: [com.tencent.assistant.activity.SpaceCleanActivity, java.lang.String, int]
     candidates:
      com.tencent.assistant.activity.SpaceCleanActivity.a(long, boolean, int):void
      com.tencent.assistant.activity.SpaceCleanActivity.a(com.tencent.assistant.activity.SpaceCleanActivity, long, boolean):void
      com.tencent.assistant.activity.SpaceCleanActivity.a(com.tencent.assistant.activity.SpaceCleanActivity, android.content.pm.PackageManager, java.lang.String):boolean
      com.tencent.assistant.activity.BaseActivity.a(com.tencent.assistantv2.st.page.STPageInfo, java.lang.String, java.lang.String):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, boolean, boolean):android.support.v4.app.z
      android.support.v4.app.FragmentActivity.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      com.tencent.assistant.activity.SpaceCleanActivity.a(com.tencent.assistant.activity.SpaceCleanActivity, java.lang.String, boolean):void */
    public void a(int i, DataEntity dataEntity) {
        XLog.d("miles", "onResultGot.. paramAnonymousInt = " + i + " DataEntity = " + dataEntity.toString());
        this.f585a.W.removeMessages(30);
        Message.obtain(this.f585a.W, 27).sendToTarget();
        long unused = this.f585a.U = System.currentTimeMillis();
        this.f585a.a("SpaceCleanDoClear", true);
    }
}
