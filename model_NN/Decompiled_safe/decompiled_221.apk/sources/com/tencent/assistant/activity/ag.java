package com.tencent.assistant.activity;

import com.tencent.assistant.appbakcup.BackupAppListAdapter;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class ag implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ af f395a;

    ag(af afVar) {
        this.f395a = afVar;
    }

    public void run() {
        BackupAppListAdapter a2;
        if (this.f395a.f394a.E != null && this.f395a.f394a.E.isShowing() && (a2 = this.f395a.f394a.E.a()) != null) {
            XLog.d("AppBackupActivity", "call --notifyDataSetChanged");
            a2.notifyDataSetChanged();
        }
    }
}
