package com.tencent.assistant.activity;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.module.u;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class hf extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UpdateListActivity f629a;

    hf(UpdateListActivity updateListActivity) {
        this.f629a = updateListActivity;
    }

    public void onTMAClick(View view) {
        if (u.b(this.f629a.I) != 0) {
            this.f629a.n.startActivity(new Intent(this.f629a.n, UpdateIgnoreListActivity.class));
        }
    }

    public STInfoV2 getStInfo() {
        if (this.f629a.w == null) {
            return null;
        }
        STInfoV2 buildUpdateStatInfo = this.f629a.w.buildUpdateStatInfo();
        if (buildUpdateStatInfo == null) {
            return buildUpdateStatInfo;
        }
        buildUpdateStatInfo.slotId = a.a("06", "001");
        return buildUpdateStatInfo;
    }
}
