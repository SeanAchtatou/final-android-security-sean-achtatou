package com.tencent.assistant.activity;

import android.view.inputmethod.InputMethodManager;
import java.util.TimerTask;

/* compiled from: ProGuard */
class dv extends TimerTask {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ HelperFeedbackActivity f537a;

    dv(HelperFeedbackActivity helperFeedbackActivity) {
        this.f537a = helperFeedbackActivity;
    }

    public void run() {
        try {
            ((InputMethodManager) this.f537a.getSystemService("input_method")).toggleSoftInput(0, 2);
        } catch (Throwable th) {
        }
    }
}
