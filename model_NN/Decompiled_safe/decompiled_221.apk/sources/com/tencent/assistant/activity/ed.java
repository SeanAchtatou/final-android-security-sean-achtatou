package com.tencent.assistant.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.m;

/* compiled from: ProGuard */
class ed extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ImageView f546a;
    final /* synthetic */ ImageView b;
    final /* synthetic */ ImageView c;
    final /* synthetic */ ImageView d;
    final /* synthetic */ TextView e;
    final /* synthetic */ TextView f;
    final /* synthetic */ TextView g;
    final /* synthetic */ TextView h;
    final /* synthetic */ InstalledAppManagerActivity i;

    ed(InstalledAppManagerActivity installedAppManagerActivity, ImageView imageView, ImageView imageView2, ImageView imageView3, ImageView imageView4, TextView textView, TextView textView2, TextView textView3, TextView textView4) {
        this.i = installedAppManagerActivity;
        this.f546a = imageView;
        this.b = imageView2;
        this.c = imageView3;
        this.d = imageView4;
        this.e = textView;
        this.f = textView2;
        this.g = textView3;
        this.h = textView4;
    }

    public void onTMAClick(View view) {
        int unused = this.i.D = 2;
        this.i.z.setImageDrawable(this.i.getResources().getDrawable(R.drawable.common_icon_name));
        this.i.y.setText((int) R.string.sort_type_by_name);
        this.f546a.setSelected(false);
        this.b.setSelected(false);
        this.c.setSelected(true);
        this.d.setSelected(false);
        this.e.setTextColor(this.i.getResources().getColor(R.color.appadmin_sort_text_normol));
        this.f.setTextColor(this.i.getResources().getColor(R.color.appadmin_sort_text_normol));
        this.g.setTextColor(this.i.getResources().getColor(R.color.appadmin_sort_text_selected));
        this.h.setTextColor(this.i.getResources().getColor(R.color.appadmin_sort_text_normol));
        this.i.B.dismiss();
        this.i.A();
        if (this.i.L == 0) {
            this.i.F.setSelection(0);
        } else {
            this.i.G.setSelection(0);
        }
        m.a().b("key_app_uninstall_last_sort_type", Integer.valueOf(this.i.D));
    }
}
