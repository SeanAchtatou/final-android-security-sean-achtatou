package com.tencent.assistant.activity;

import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.model.AppGroupInfo;

/* compiled from: ProGuard */
class dm implements AbsListView.OnScrollListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GroupListActivity f528a;

    dm(GroupListActivity groupListActivity) {
        this.f528a = groupListActivity;
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if (this.f528a.y == null || this.f528a.n == null) {
            RelativeLayout unused = this.f528a.y = (RelativeLayout) this.f528a.findViewById(R.id.pop_bar);
            this.f528a.y.setOnClickListener(new dn(this));
            return;
        }
        if (i == 0) {
            this.f528a.y.setVisibility(8);
        }
        int pointToPosition = this.f528a.n.pointToPosition(0, this.f528a.y() - 10);
        int pointToPosition2 = this.f528a.n.pointToPosition(0, 0);
        long j = 0;
        if (pointToPosition2 != -1) {
            long expandableListPosition = this.f528a.n.getExpandableListPosition(pointToPosition2);
            int packedPositionChild = ExpandableListView.getPackedPositionChild(expandableListPosition);
            int packedPositionGroup = ExpandableListView.getPackedPositionGroup(expandableListPosition);
            if (packedPositionChild == -1) {
                View expandChildAt = this.f528a.n.getExpandChildAt(pointToPosition2 - this.f528a.n.getFirstVisiblePosition());
                if (expandChildAt == null) {
                    int unused2 = this.f528a.C = 100;
                } else {
                    int unused3 = this.f528a.C = expandChildAt.getHeight();
                }
            }
            if (this.f528a.C != 0) {
                if (this.f528a.D > 0) {
                    int unused4 = this.f528a.B = packedPositionGroup;
                    AppGroupInfo appGroupInfo = (AppGroupInfo) this.f528a.w.getGroup(packedPositionGroup);
                    if (appGroupInfo != null) {
                        this.f528a.z.setText(appGroupInfo.a());
                    }
                    if (this.f528a.B != packedPositionGroup || !this.f528a.n.isGroupExpanded(packedPositionGroup)) {
                        this.f528a.y.setVisibility(8);
                    } else {
                        this.f528a.y.setVisibility(0);
                    }
                }
                if (this.f528a.D == 0) {
                    this.f528a.y.setVisibility(8);
                }
                j = expandableListPosition;
            } else {
                return;
            }
        }
        if (this.f528a.B == -1) {
            return;
        }
        if (j == 0 && pointToPosition == 0) {
            this.f528a.y.setVisibility(8);
            return;
        }
        int g = this.f528a.y();
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.f528a.y.getLayoutParams();
        marginLayoutParams.topMargin = -(this.f528a.C - g);
        this.f528a.y.setLayoutParams(marginLayoutParams);
    }
}
