package com.tencent.assistant.activity;

import com.tencent.assistant.module.callback.p;
import com.tencent.assistant.protocol.jce.Feedback;
import java.util.ArrayList;

/* compiled from: ProGuard */
class dw extends p {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ HelperFeedbackActivity f538a;

    dw(HelperFeedbackActivity helperFeedbackActivity) {
        this.f538a = helperFeedbackActivity;
    }

    public void a(int i, boolean z, boolean z2, ArrayList<Feedback> arrayList) {
        if (i != 0) {
            return;
        }
        if (arrayList.size() > 0) {
            if (z) {
                this.f538a.x.a(arrayList);
            } else {
                this.f538a.x.b(arrayList);
            }
            this.f538a.w.setVisibility(0);
            return;
        }
        this.f538a.w.setVisibility(8);
    }
}
