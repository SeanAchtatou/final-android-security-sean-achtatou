package com.tencent.assistant.activity.debug;

import android.view.View;
import android.widget.AdapterView;
import com.tencent.assistant.Global;
import com.tencent.assistant.m;

/* compiled from: ProGuard */
class av implements AdapterView.OnItemSelectedListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ServerAdressSettingActivity f496a;

    av(ServerAdressSettingActivity serverAdressSettingActivity) {
        this.f496a = serverAdressSettingActivity;
    }

    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
        if (i != this.f496a.e) {
            m.a().a(this.f496a.c = i);
            this.f496a.b.setSelection(this.f496a.f);
            this.f496a.d.setText("当前：" + Global.getServerAddressName());
            this.f496a.c();
        }
    }

    public void onNothingSelected(AdapterView<?> adapterView) {
    }
}
