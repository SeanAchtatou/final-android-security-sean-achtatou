package com.tencent.assistant.activity;

import android.view.View;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class av implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppTreasureBoxActivity f410a;

    av(AppTreasureBoxActivity appTreasureBoxActivity) {
        this.f410a = appTreasureBoxActivity;
    }

    public void onClick(View view) {
        this.f410a.onCloseBtnClick();
        k.a(new STInfoV2(this.f410a.f(), this.f410a.b(4, 1), this.f410a.p(), STConst.ST_DEFAULT_SLOT, 200));
    }
}
