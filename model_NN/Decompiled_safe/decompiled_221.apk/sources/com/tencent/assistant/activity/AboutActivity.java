package com.tencent.assistant.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.Global;
import com.tencent.assistant.adapter.AboutAdapter;
import com.tencent.assistant.manager.SelfUpdateManager;
import com.tencent.assistant.module.et;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.v;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.model.ItemElement;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.ArrayList;
import java.util.Random;

/* compiled from: ProGuard */
public class AboutActivity extends BaseActivity {
    /* access modifiers changed from: private */
    public String A = null;
    private Context n;
    private SecondNavigationTitleViewV5 t;
    private ImageView u;
    private TextView v;
    private TextView w;
    private ListView x;
    /* access modifiers changed from: private */
    public AboutAdapter y;
    /* access modifiers changed from: private */
    public et z = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int i = displayMetrics.densityDpi;
        int i2 = displayMetrics.heightPixels;
        int i3 = displayMetrics.widthPixels;
        if (i < 320 || (i2 == 960 && i3 == 640)) {
            setContentView((int) R.layout.activity_about);
        } else {
            setContentView((int) R.layout.activity_about_hdpi);
        }
        this.n = this;
        this.y = new AboutAdapter(this.n);
        j();
        i();
    }

    private void i() {
        this.u = (ImageView) findViewById(R.id.about_logo);
        this.u.setOnClickListener(new a(this));
        this.u.setOnLongClickListener(new b(this));
        this.t = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.t.a(this);
        this.t.b(getString(R.string.about_title));
        this.t.d();
        this.t.c(new d(this));
        this.v = (TextView) findViewById(R.id.app_name);
        this.w = (TextView) findViewById(R.id.about_version);
        this.w.setText(Global.getAppVersionName());
        this.x = (ListView) findViewById(R.id.content_list);
        this.x.setAdapter((ListAdapter) this.y);
        this.x.setDivider(null);
        this.x.setOnItemClickListener(new e(this));
    }

    private void j() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new ItemElement(this.n.getString(R.string.about_check_version), null, 1, 0, 0));
        arrayList.add(new ItemElement(this.n.getString(R.string.about_help_feedback), null, 2, 1, 0));
        arrayList.add(new ItemElement(this.n.getString(R.string.about_welcome_page), null, 3, 2, 0));
        arrayList.add(new ItemElement(this.n.getString(R.string.about_declare), null, 4, 3, 0));
        this.y.a(arrayList);
    }

    /* access modifiers changed from: private */
    public void v() {
        f fVar = new f(this);
        fVar.titleRes = getString(R.string.ver_low_tips_title);
        fVar.contentRes = this.A;
        fVar.btnTxtRes = getString(R.string.ver_low_tips_ok);
        v.a(fVar);
    }

    /* access modifiers changed from: private */
    public void a(ItemElement itemElement) {
        switch (itemElement.c) {
            case 1:
                SelfUpdateManager.a().a(true);
                SelfUpdateManager.a().o().a(true);
                return;
            case 2:
                Intent intent = new Intent(this.n, HelperFAQActivity.class);
                intent.putExtra("com.tencent.assistant.BROWSER_URL", "http://maweb.3g.qq.com/help/help.html");
                this.n.startActivity(intent);
                b("04_001");
                return;
            case 3:
                this.n.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("tmast://guide")));
                return;
            case 4:
                this.n.startActivity(new Intent(this.n, AboutDeclareActivity.class));
                return;
            default:
                return;
        }
    }

    private void b(String str) {
        k.a(new STInfoV2(f(), str, p(), STConst.ST_DEFAULT_SLOT, 200));
    }

    public int f() {
        return STConst.ST_PAGE_ABOUT;
    }

    /* access modifiers changed from: private */
    public String b(int i) {
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i2 = 0; i2 < i; i2++) {
            sb.append(random.nextInt(10));
        }
        return sb.toString();
    }

    public void onResume() {
        super.onResume();
        if (this.y != null) {
            this.y.a();
        }
    }

    public void onPause() {
        super.onPause();
        if (this.y != null) {
            this.y.b();
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.y != null) {
            this.y.c();
        }
    }
}
