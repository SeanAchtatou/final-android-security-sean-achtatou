package com.tencent.assistant.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.a;
import com.tencent.assistant.link.b;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.bg;
import com.tencent.assistant.utils.e;
import com.tencent.assistant.utils.installuninstall.p;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.open.SocialConstants;
import java.util.Arrays;
import java.util.List;

/* compiled from: ProGuard */
public class ShortCutActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        k.a(new STInfoV2(STConst.ST_PAGE_SHORT_CUT, "03_001", 2000, STConst.ST_DEFAULT_SLOT, 100));
        a();
        finish();
    }

    private void a() {
        Intent intent = getIntent();
        String a2 = bg.a(intent, "pkgName");
        String a3 = bg.a(intent, SocialConstants.PARAM_URL);
        if (!TextUtils.isEmpty(a2)) {
            TemporaryThreadManager.get().start(new fn(this, this, a2, bg.a(intent, "channelId"), bg.a(intent, "name")));
        } else if (!TextUtils.isEmpty(a3)) {
            b.a(this, a3);
            k.a(new STInfoV2(STConst.ST_PAGE_SHORT_CUT, "07_001", 2000, STConst.ST_DEFAULT_SLOT, 200));
        }
    }

    /* access modifiers changed from: private */
    public void a(Context context, String str, String str2, String str3) {
        if (ApkResourceManager.getInstance().getInstalledApkInfo(str) != null) {
            a.a().c(str);
            k.a(new STInfoV2(STConst.ST_PAGE_SHORT_CUT, "04_001", 2000, STConst.ST_DEFAULT_SLOT, 200));
            return;
        }
        LocalApkInfo a2 = a(str);
        if (a2 != null) {
            a2.mPackageName = str;
            a(a2, str3);
            k.a(new STInfoV2(STConst.ST_PAGE_SHORT_CUT, "05_001", 2000, STConst.ST_DEFAULT_SLOT, 200));
            return;
        }
        String str4 = "tmast://appdetails?pname=" + str + "&oplist=3";
        if (!TextUtils.isEmpty(str2)) {
            str4 + "&channelid=" + str2;
        }
        b.a(context, "tmast://appdetails?pname=" + str + "&oplist=3");
        k.a(new STInfoV2(STConst.ST_PAGE_SHORT_CUT, "06_001", 2000, STConst.ST_DEFAULT_SLOT, 200));
    }

    private void a(LocalApkInfo localApkInfo, String str) {
        int i;
        localApkInfo.mApkState = 2;
        String ddownloadTicket = ApkResourceManager.getInstance().getDdownloadTicket(localApkInfo);
        DownloadInfo d = DownloadProxy.a().d(ddownloadTicket);
        if (d == null) {
            d = new DownloadInfo();
            d.packageName = localApkInfo != null ? localApkInfo.mPackageName : null;
            if (localApkInfo != null) {
                i = localApkInfo.mVersionCode;
            } else {
                i = 0;
            }
            d.versionCode = i;
            d.scene = STConst.ST_PAGE_SHORT_CUT;
        }
        p.a().a(ddownloadTicket, localApkInfo.mPackageName, str, localApkInfo.mLocalFilePath, localApkInfo.mVersionCode, localApkInfo.signature, ddownloadTicket, localApkInfo.occupySize, false, d);
    }

    public LocalApkInfo a(String str) {
        List<String> list;
        try {
            list = FileUtil.scanFile(FileUtil.getAPKDir(), Arrays.asList("apk", "APK"));
        } catch (Exception e) {
            list = null;
        }
        if (list != null && list.size() > 0) {
            for (String next : list) {
                if (!TextUtils.isEmpty(next) && next.contains(str)) {
                    return e.c(next);
                }
            }
        }
        return null;
    }
}
