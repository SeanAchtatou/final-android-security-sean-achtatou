package com.tencent.assistant.activity;

import android.os.Message;
import com.tencent.assistant.localres.callback.c;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.XLog;
import java.util.List;

/* compiled from: ProGuard */
class l extends c {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ApkMgrActivity f643a;

    l(ApkMgrActivity apkMgrActivity) {
        this.f643a = apkMgrActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, long, boolean):void
     arg types: [com.tencent.assistant.activity.ApkMgrActivity, int, int]
     candidates:
      com.tencent.assistant.activity.ApkMgrActivity.a(int, long, boolean):void
      com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, long, int):void
      com.tencent.assistant.activity.BaseActivity.a(com.tencent.assistantv2.st.page.STPageInfo, java.lang.String, java.lang.String):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, boolean, boolean):android.support.v4.app.z
      android.support.v4.app.FragmentActivity.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, long, boolean):void */
    public void a(List<LocalApkInfo> list, boolean z, boolean z2, int i) {
        long j;
        XLog.d("ApkMgrActivity", "onApkLoadFinish--apkInfos = " + list);
        if (!this.f643a.E && !this.f643a.G) {
            long unused = this.f643a.I = System.currentTimeMillis();
            if (this.f643a.H != 0) {
                j = this.f643a.I - this.f643a.H;
            } else {
                j = 0;
            }
            this.f643a.a(j, list == null ? 0 : list.size());
            if (i == 0) {
                XLog.d("ApkMgrActivity", "onApkLoadFinish--");
                if (list == null || list.isEmpty()) {
                    boolean unused2 = this.f643a.C = true;
                }
                if (z2) {
                    this.f643a.h();
                }
                if (this.f643a.I - this.f643a.H < 1200) {
                    this.f643a.R.postDelayed(new m(this, list, z), 1200 - (this.f643a.I - this.f643a.H));
                } else {
                    this.f643a.a(list, false, z, 1, true);
                }
            } else {
                boolean unused3 = this.f643a.C = true;
                if (this.f643a.I - this.f643a.H < 1200) {
                    this.f643a.R.postDelayed(new n(this), 1200 - (this.f643a.I - this.f643a.H));
                } else {
                    this.f643a.a(0L, false);
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, long, boolean):void
     arg types: [com.tencent.assistant.activity.ApkMgrActivity, int, int]
     candidates:
      com.tencent.assistant.activity.ApkMgrActivity.a(int, long, boolean):void
      com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, long, int):void
      com.tencent.assistant.activity.BaseActivity.a(com.tencent.assistantv2.st.page.STPageInfo, java.lang.String, java.lang.String):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, boolean, boolean):android.support.v4.app.z
      android.support.v4.app.FragmentActivity.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, long, boolean):void */
    public void a(List<LocalApkInfo> list) {
        XLog.d("ApkMgrActivity", "onApkLoadPortionComplete--apkInfos = " + list);
        if (!this.f643a.E && !this.f643a.G) {
            if (list == null || list.isEmpty()) {
                this.f643a.a(0L, false);
            } else {
                this.f643a.a(list, true, false, 1, true);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, long, int, boolean, int):void
     arg types: [com.tencent.assistant.activity.ApkMgrActivity, int, int, int, int]
     candidates:
      com.tencent.assistant.activity.ApkMgrActivity.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, boolean, boolean, int, boolean):void
      com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, long, int, boolean, int):void */
    public void a(List<LocalApkInfo> list, LocalApkInfo localApkInfo, boolean z, boolean z2) {
        XLog.d("ApkMgrActivity", "onApkDeleteFinish--apkInfos = " + list);
        if (!this.f643a.G) {
            if (!z2) {
                this.f643a.a(list, false, false, 2, false);
            } else if (!z) {
            } else {
                if (list.size() == 0) {
                    this.f643a.a(0L, 0, false, 2);
                    Message obtainMessage = this.f643a.R.obtainMessage(110009);
                    obtainMessage.obj = Long.valueOf(localApkInfo.occupySize);
                    this.f643a.R.sendMessageDelayed(obtainMessage, 1000);
                    return;
                }
                this.f643a.a(list, false, false, 2, true);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, long, int, boolean, int):void
     arg types: [com.tencent.assistant.activity.ApkMgrActivity, int, int, int, int]
     candidates:
      com.tencent.assistant.activity.ApkMgrActivity.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, boolean, boolean, int, boolean):void
      com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, long, int, boolean, int):void */
    public void a(List<LocalApkInfo> list, int i, long j) {
        XLog.d("ApkMgrActivity", "onApkBatchDeleteSucceed--apkInfos = " + list);
        if (!this.f643a.G) {
            if (i > 0) {
                if (list.size() == 0) {
                    this.f643a.a(0L, 0, false, 2);
                    this.f643a.Q.deleteAllVisibleItem(new o(this, j));
                } else {
                    this.f643a.a(list, false, false, 2, false);
                    this.f643a.x();
                }
            }
            boolean unused = this.f643a.E = false;
        }
    }

    public void b(List<LocalApkInfo> list, int i, long j) {
        XLog.d("ApkMgrActivity", "onApkDidChange--apkInfos = " + list);
        if (!this.f643a.E && !this.f643a.G) {
            if (list == null || list.size() == 0) {
                Message obtainMessage = this.f643a.R.obtainMessage(110009);
                obtainMessage.obj = Long.valueOf(j);
                this.f643a.R.sendMessageDelayed(obtainMessage, 0);
                return;
            }
            this.f643a.a(list, false, false, 3, true);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, int, long, boolean):void
     arg types: [com.tencent.assistant.activity.ApkMgrActivity, int, long, int]
     candidates:
      com.tencent.assistant.activity.ApkMgrActivity.a(long, int, boolean, int):void
      com.tencent.assistant.activity.BaseActivity.a(android.widget.LinearLayout, int, int, int):void
      com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, int, long, boolean):void */
    public void a(int i, int i2, long j) {
        this.f643a.a(i2, j, false);
    }
}
