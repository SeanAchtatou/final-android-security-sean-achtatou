package com.tencent.assistant.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.Global;
import com.tencent.assistant.adapter.TXTabViewPageAdapter;
import com.tencent.assistant.component.PreInstallAppListView;
import com.tencent.assistant.component.UserAppListView;
import com.tencent.assistant.component.txscrollview.TXTabViewPage;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.l;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.manager.cr;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.protocol.jce.StatAppInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.b;
import com.tencent.assistant.utils.e;
import com.tencent.assistant.utils.installuninstall.InstallUninstallTaskBean;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.activity.AssistantTabActivity;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.st.business.x;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.beacon.event.a;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
public class InstalledAppManagerActivity extends BaseActivity implements TXTabViewPage.ITXTabViewPageListener, UIEventListener {
    private String A;
    /* access modifiers changed from: private */
    public PopupWindow B = null;
    private boolean C = false;
    /* access modifiers changed from: private */
    public int D = 0;
    private TXTabViewPage E;
    /* access modifiers changed from: private */
    public UserAppListView F;
    /* access modifiers changed from: private */
    public PreInstallAppListView G;
    private TextView H;
    private short I = 0;
    private short J = 0;
    private x K = null;
    /* access modifiers changed from: private */
    public int L = 0;
    /* access modifiers changed from: private */
    public int M = 0;
    /* access modifiers changed from: private */
    public int N = 0;
    private boolean O = false;
    private boolean P = true;
    private boolean Q = false;
    private boolean R = false;
    private boolean S = false;
    private List<StatAppInfo> T = new ArrayList();
    private boolean U = false;
    /* access modifiers changed from: private */
    public Handler V = new ef(this);
    /* access modifiers changed from: private */
    public ApkResourceManager n = ApkResourceManager.getInstance();
    /* access modifiers changed from: private */
    public List<LocalApkInfo> t = Collections.synchronizedList(new ArrayList());
    /* access modifiers changed from: private */
    public List<LocalApkInfo> u = Collections.synchronizedList(new ArrayList());
    /* access modifiers changed from: private */
    public eh v = new eh(this, null);
    private SecondNavigationTitleViewV5 w;
    private RelativeLayout x;
    /* access modifiers changed from: private */
    public TextView y;
    /* access modifiers changed from: private */
    public ImageView z;

    static /* synthetic */ int n(InstalledAppManagerActivity installedAppManagerActivity) {
        int i = installedAppManagerActivity.N;
        installedAppManagerActivity.N = i + 1;
        return i;
    }

    /* access modifiers changed from: protected */
    public boolean g() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            setContentView((int) R.layout.installed_app_manager_layout);
            if (bundle != null) {
                this.L = bundle.getInt("pageIndex");
            } else {
                this.L = getIntent().getIntExtra("pageIndex", 0);
            }
            v();
            j();
            i();
            w();
        } catch (Exception e) {
            this.U = true;
            finish();
        }
    }

    public void i() {
        this.K = new x();
        this.A = getIntent().getStringExtra("activityTitleName");
        TemporaryThreadManager.get().start(new dy(this));
    }

    private void w() {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getPhoneGuidAndGen());
        hashMap.put("B2", Global.getQUAForBeacon());
        hashMap.put("B3", t.g());
        XLog.d("beacon", "beacon report >> expose_appuninstall. " + hashMap.toString());
        a.a("expose_appuninstall", true, -1, -1, hashMap, true);
    }

    public void j() {
        y();
        x();
    }

    public void v() {
        Bundle extras = getIntent().getExtras();
        this.O = extras != null && extras.getBoolean(com.tencent.assistant.b.a.N, false);
        if (this.O) {
            this.D = 1;
        } else {
            this.D = m.a().a("key_app_uninstall_last_sort_type", 0);
        }
    }

    private void x() {
        this.E = new TXTabViewPage(this);
        TXTabViewPageAdapter tXTabViewPageAdapter = new TXTabViewPageAdapter();
        this.H = (TextView) findViewById(R.id.dialog);
        this.F = new UserAppListView(this);
        this.F.setTextChooser(this.H);
        this.F.setHandlerToAdaper(this.V);
        this.G = new PreInstallAppListView(this);
        this.G.setTextChooser(this.H);
        this.G.setHandlerToAdaper(this.V);
        String string = getString(R.string.title_user_app);
        String string2 = getString(R.string.title_preinstall_app);
        tXTabViewPageAdapter.addPageItem(string, this.F);
        tXTabViewPageAdapter.addPageItem(string2, this.G);
        this.E.setAdapter(tXTabViewPageAdapter);
        this.E.setListener(this);
        ((RelativeLayout) findViewById(R.id.content_view)).addView(this.E, -1, -1);
        this.E.setPageSelected(this.L);
    }

    private void y() {
        int i;
        int i2;
        this.w = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.w.b(getResources().getString(R.string.soft_admin));
        this.w.a(this);
        this.w.b(this.A);
        this.w.d();
        this.w.c(new dz(this));
        this.x = (RelativeLayout) findViewById(R.id.right_layout_ext);
        this.z = (ImageView) findViewById(R.id.right_img_ext);
        this.y = (TextView) findViewById(R.id.sort_type_name);
        this.x.setOnClickListener(new ea(this));
        if (this.D == 0) {
            i = R.drawable.common_icon_time;
            i2 = R.string.sort_type_by_time;
        } else if (this.D == 1) {
            i = R.drawable.common_icon_kongjian;
            i2 = R.string.sort_type_by_size;
        } else if (this.D == 2) {
            i = R.drawable.common_icon_name;
            i2 = R.string.sort_type_by_name;
        } else {
            i = R.drawable.common_icon_instal;
            i2 = R.string.sort_type_by_install_time;
        }
        this.z.setImageDrawable(getResources().getDrawable(i));
        this.y.setText(i2);
    }

    /* access modifiers changed from: private */
    public void z() {
        if (!this.C) {
            if (this.B == null || !this.B.isShowing()) {
                if (this.B == null) {
                    try {
                        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(this).inflate((int) R.layout.app_manage_sorttype_pop_layout, (ViewGroup) null);
                        LinearLayout linearLayout2 = (LinearLayout) linearLayout.findViewById(R.id.ll_sort_by_time);
                        ImageView imageView = (ImageView) linearLayout.findViewById(R.id.iv_sort_by_time);
                        TextView textView = (TextView) linearLayout.findViewById(R.id.tv_sort_by_time);
                        LinearLayout linearLayout3 = (LinearLayout) linearLayout.findViewById(R.id.ll_sort_by_size);
                        ImageView imageView2 = (ImageView) linearLayout.findViewById(R.id.iv_sort_by_size);
                        TextView textView2 = (TextView) linearLayout.findViewById(R.id.tv_sort_by_size);
                        LinearLayout linearLayout4 = (LinearLayout) linearLayout.findViewById(R.id.ll_sort_by_name);
                        ImageView imageView3 = (ImageView) linearLayout.findViewById(R.id.iv_sort_by_name);
                        TextView textView3 = (TextView) linearLayout.findViewById(R.id.tv_sort_by_name);
                        LinearLayout linearLayout5 = (LinearLayout) linearLayout.findViewById(R.id.ll_sort_by_install_time);
                        ImageView imageView4 = (ImageView) linearLayout.findViewById(R.id.iv_sort_by_install_time);
                        TextView textView4 = (TextView) linearLayout.findViewById(R.id.tv_sort_by_install_time);
                        if (this.D == 0) {
                            this.z.setImageDrawable(getResources().getDrawable(R.drawable.common_icon_time));
                            this.y.setText((int) R.string.sort_type_by_time);
                            imageView.setSelected(true);
                            textView.setTextColor(getResources().getColor(R.color.appadmin_sort_text_selected));
                        } else if (this.D == 1) {
                            this.z.setImageDrawable(getResources().getDrawable(R.drawable.common_icon_kongjian));
                            this.y.setText((int) R.string.sort_type_by_size);
                            imageView2.setSelected(true);
                            textView2.setTextColor(getResources().getColor(R.color.appadmin_sort_text_selected));
                        } else if (this.D == 2) {
                            this.z.setImageDrawable(getResources().getDrawable(R.drawable.common_icon_name));
                            this.y.setText((int) R.string.sort_type_by_name);
                            imageView3.setSelected(true);
                            textView3.setTextColor(getResources().getColor(R.color.appadmin_sort_text_selected));
                        } else {
                            this.z.setImageDrawable(getResources().getDrawable(R.drawable.common_icon_instal));
                            this.y.setText((int) R.string.sort_type_by_install_time);
                            imageView4.setSelected(true);
                            textView4.setTextColor(getResources().getColor(R.color.appadmin_sort_text_selected));
                        }
                        linearLayout2.setOnClickListener(new eb(this, imageView, imageView2, imageView3, imageView4, textView, textView2, textView3, textView4));
                        linearLayout3.setOnClickListener(new ec(this, imageView, imageView2, imageView3, imageView4, textView, textView2, textView3, textView4));
                        linearLayout4.setOnClickListener(new ed(this, imageView, imageView2, imageView3, imageView4, textView, textView2, textView3, textView4));
                        linearLayout5.setOnClickListener(new ee(this, imageView, imageView2, imageView3, imageView4, textView, textView2, textView3, textView4));
                        this.B = new PopupWindow(linearLayout, -2, -2);
                    } catch (Throwable th) {
                        cq.a().b();
                        return;
                    }
                }
                this.B.showAsDropDown(this.w, this.w.getWidth() - this.B.getWidth(), 0);
                return;
            }
            this.B.dismiss();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.InstalledAppManagerActivity.a(boolean, java.lang.String, java.lang.String):void
     arg types: [int, java.lang.String, java.lang.String]
     candidates:
      com.tencent.assistant.activity.InstalledAppManagerActivity.a(com.tencent.assistant.activity.InstalledAppManagerActivity, com.tencent.assistant.localres.model.LocalApkInfo, int):void
      com.tencent.assistant.activity.BaseActivity.a(com.tencent.assistantv2.st.page.STPageInfo, java.lang.String, java.lang.String):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, boolean, boolean):android.support.v4.app.z
      android.support.v4.app.FragmentActivity.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      com.tencent.assistant.activity.InstalledAppManagerActivity.a(boolean, java.lang.String, java.lang.String):void */
    /* access modifiers changed from: private */
    public void a(LocalApkInfo localApkInfo, int i) {
        if (i == 1) {
            if (e.b(localApkInfo.flags)) {
                localApkInfo.mIsSelect = this.G.getUninstallAppList().contains(localApkInfo);
                this.u.add(localApkInfo);
                return;
            }
            localApkInfo.mIsSelect = this.F.getUninstallAppList().contains(localApkInfo);
            this.t.add(localApkInfo);
        } else if (i == 2) {
            ArrayList arrayList = new ArrayList();
            if (e.b(localApkInfo.flags)) {
                for (LocalApkInfo next : this.u) {
                    if (!localApkInfo.mPackageName.equals(next.mPackageName)) {
                        arrayList.add(next);
                    }
                }
                this.u.clear();
                this.u.addAll(arrayList);
                this.R = this.G.onAppUninstallFinish(localApkInfo, null, true);
                a(true, localApkInfo.mPackageName, "system");
            } else {
                for (LocalApkInfo next2 : this.t) {
                    if (!localApkInfo.mPackageName.equals(next2.mPackageName)) {
                        arrayList.add(next2);
                    }
                }
                this.t.clear();
                this.t.addAll(arrayList);
                this.Q = this.F.onAppUninstallFinish(localApkInfo);
                this.S = false;
                a(true, localApkInfo.mPackageName, "personal");
            }
            StatAppInfo statAppInfo = new StatAppInfo();
            statAppInfo.f2352a = localApkInfo.mPackageName;
            statAppInfo.c = localApkInfo.mVersionCode;
            statAppInfo.b = localApkInfo.signature;
            this.T.add(statAppInfo);
            this.J = (short) (this.J + 1);
        } else {
            if (i == 4) {
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, boolean]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    /* access modifiers changed from: private */
    public void A() {
        ArrayList arrayList = new ArrayList(this.t);
        ArrayList arrayList2 = new ArrayList(this.u);
        if (this.D == 0) {
            l.c(arrayList);
            l.c(arrayList2);
            this.F.hideSideBar();
            this.G.hideSideBar();
        } else if (this.D == 1) {
            l.a(arrayList);
            l.a(arrayList2);
            this.F.hideSideBar();
            this.G.hideSideBar();
        } else if (this.D == 2) {
            l.g(arrayList);
            l.g(arrayList2);
            this.F.showSideBar();
            this.G.showSideBar();
        } else if (this.D == 3) {
            l.b(arrayList);
            l.b(arrayList2);
            this.F.hideSideBar();
            this.G.hideSideBar();
        }
        if ((this.L != 0 || !arrayList.isEmpty()) && (this.L != 1 || !arrayList2.isEmpty())) {
            this.x.setVisibility(0);
        } else {
            this.x.setVisibility(8);
        }
        this.F.refreshData(arrayList, this.D);
        this.G.refreshData(arrayList2, this.D);
        if (!m.a().a("key_has_report_preinstall_applist", false) && !this.u.isEmpty()) {
            m.a().b("key_has_report_preinstall_applist", (Object) true);
            TemporaryThreadManager.get().start(new eg(this));
        }
    }

    /* access modifiers changed from: private */
    public void a(LocalApkInfo localApkInfo) {
        if (e.b(localApkInfo.flags)) {
            this.G.updateFootView(localApkInfo);
        } else {
            this.F.updateFootView(localApkInfo);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (!this.U) {
            this.K.a(this.J, this.I, (ArrayList) this.T);
            ApkResourceManager.getInstance().unRegisterApkResCallback(this.v);
            this.V.removeMessages(10702);
            this.V.removeMessages(10703);
            this.V.removeMessages(10701);
            this.V.removeMessages(10704);
            this.V.removeMessages(10705);
            this.V.removeMessages(10706);
            AstApp.i().k().removeUIEventListener(1024, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_START, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_FAIL, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_SUCC, this);
            b.a();
            if (this.O) {
                com.tencent.assistant.utils.installuninstall.a.a().c();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.U) {
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (!this.U && this.w != null) {
            this.w.m();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.U) {
            AstApp.i().k().addUIEventListener(1024, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_START, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_FAIL, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_SUCC, this);
            if (this.G != null) {
                if (m.a().n() != AppConst.ROOT_STATUS.ROOTED) {
                    m.a().a(t.n());
                }
                if (m.a().i()) {
                    m.a().a(AppConst.ROOT_STATUS.ROOTED);
                }
                if (!this.R) {
                    this.G.onResume();
                }
            }
            if (this.F != null && (!this.Q || this.S)) {
                this.F.onResume();
            }
            if (this.w != null) {
                this.w.l();
            }
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (4 != i) {
            return super.onKeyDown(i, keyEvent);
        }
        B();
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void B() {
        if (!this.r) {
            finish();
            return;
        }
        Intent intent = new Intent(this, AssistantTabActivity.class);
        intent.setFlags(67108864);
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, f());
        intent.putExtra(com.tencent.assistant.b.a.G, true);
        startActivity(intent);
        this.r = false;
        finish();
        XLog.i("miles", "InstalledAppManagerActivity >> key back finish");
    }

    /* access modifiers changed from: private */
    public synchronized void a(List<LocalApkInfo> list) {
        if (list != null) {
            if (!(this.t == null || this.u == null)) {
                this.t.clear();
                this.u.clear();
                String c = e.c();
                for (LocalApkInfo next : list) {
                    if (!"com.tencent.android.qqdownloader".equals(next.mPackageName)) {
                        if (c != null && c.equals(next.mPackageName)) {
                            long currentTimeMillis = System.currentTimeMillis();
                            next.mLastLaunchTime = currentTimeMillis;
                            next.mFakeLastLaunchTime = currentTimeMillis;
                        }
                        if (this.P) {
                            next.mIsSelect = false;
                        }
                        if ((next.flags & 1) == 0 || (next.flags & 128) != 0) {
                            this.t.add(next);
                        } else if (b(next)) {
                            this.u.add(next);
                        }
                    }
                }
                this.P = false;
            }
        }
    }

    private boolean b(LocalApkInfo localApkInfo) {
        if (localApkInfo.occupySize < 524288 || localApkInfo.mUid == 1000 || localApkInfo.mUid == 1001) {
            return false;
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.InstalledAppManagerActivity.a(boolean, java.lang.String, java.lang.String):void
     arg types: [int, java.lang.String, java.lang.String]
     candidates:
      com.tencent.assistant.activity.InstalledAppManagerActivity.a(com.tencent.assistant.activity.InstalledAppManagerActivity, com.tencent.assistant.localres.model.LocalApkInfo, int):void
      com.tencent.assistant.activity.BaseActivity.a(com.tencent.assistantv2.st.page.STPageInfo, java.lang.String, java.lang.String):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, boolean, boolean):android.support.v4.app.z
      android.support.v4.app.FragmentActivity.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      com.tencent.assistant.activity.InstalledAppManagerActivity.a(boolean, java.lang.String, java.lang.String):void */
    public void handleUIEvent(Message message) {
        LocalApkInfo localApkInfo;
        LocalApkInfo localApkInfo2;
        boolean z2 = true;
        switch (message.what) {
            case 1024:
                if (message.obj != null && (message.obj instanceof String)) {
                    this.S = true;
                    XLog.d("miles", "InstalledAppManageActivity handleUIEvent. event = " + message.what + ", pkgname = " + ((String) message.obj));
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START /*1025*/:
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC /*1026*/:
            case 1027:
            default:
                return;
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_START /*1028*/:
                XLog.d("miles", "InstalledAppManageActivity handleUIEvent. event = " + message.what);
                if (message.obj != null && (message.obj instanceof InstallUninstallTaskBean) && (localApkInfo2 = ApkResourceManager.getInstance().getLocalApkInfo(((InstallUninstallTaskBean) message.obj).packageName)) != null) {
                    if (e.b(localApkInfo2.flags)) {
                        this.R = true;
                        return;
                    } else {
                        this.Q = true;
                        return;
                    }
                } else {
                    return;
                }
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_SUCC /*1029*/:
                XLog.d("miles", "InstalledAppManageActivity handleUIEvent. event = " + message.what);
                if (!(message.obj == null || !(message.obj instanceof InstallUninstallTaskBean) || (localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(((InstallUninstallTaskBean) message.obj).packageName)) == null)) {
                    if (e.b(localApkInfo.flags)) {
                        this.R = this.G.getNextUninstallApp() != null;
                    } else {
                        if (this.F.getNextUninstallApp() == null) {
                            z2 = false;
                        }
                        this.Q = z2;
                    }
                }
                if (this.F != null) {
                    this.F.refreshUi();
                }
                if (this.G != null) {
                    this.G.refreshUi();
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_FAIL /*1030*/:
                if (message.obj != null && (message.obj instanceof InstallUninstallTaskBean)) {
                    InstallUninstallTaskBean installUninstallTaskBean = (InstallUninstallTaskBean) message.obj;
                    XLog.d("miles", "handleUIEvent. appName = " + installUninstallTaskBean.appName + ", pkg = " + installUninstallTaskBean.packageName);
                    try {
                        Toast.makeText(this, installUninstallTaskBean.appName + "卸载失败", 0).show();
                    } catch (Exception e) {
                    }
                    LocalApkInfo localApkInfo3 = ApkResourceManager.getInstance().getLocalApkInfo(installUninstallTaskBean.packageName);
                    if (localApkInfo3 == null || !e.b(localApkInfo3.flags)) {
                        a(false, installUninstallTaskBean.packageName, "personal");
                    } else {
                        this.R = this.G.onAppUninstallFinish(null, installUninstallTaskBean.packageName, false);
                        a(false, installUninstallTaskBean.packageName, "system");
                    }
                }
                XLog.d("miles", "InstalledAppManageActivity handleUIEvent. event = " + message.what);
                if (this.F != null) {
                    this.F.refreshUi();
                }
                if (this.G != null) {
                    this.G.refreshUi();
                    return;
                }
                return;
        }
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        if (motionEvent == null) {
            return false;
        }
        switch (motionEvent.getAction()) {
            case 0:
                if (this.B != null && this.B.isShowing()) {
                    this.B.dismiss();
                    this.C = true;
                    break;
                } else {
                    this.C = false;
                    break;
                }
                break;
        }
        try {
            return super.dispatchTouchEvent(motionEvent);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void onTxTabViewPageSelected(int i) {
        XLog.d("miles", "Tab " + i + " selected.");
        this.L = i;
        b(i);
        if ((i != 0 || !this.t.isEmpty()) && (i != 1 || !this.u.isEmpty())) {
            this.x.setVisibility(0);
        } else {
            this.x.setVisibility(8);
        }
    }

    public void onTxTabViewPageWillSelect(int i) {
    }

    private void b(int i) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this, 100);
        if (i == 0) {
            buildSTInfo.scene = STConst.ST_PAGE_USER_APP_UNINSTALL;
        } else if (i == 1) {
            if (C()) {
                buildSTInfo.scene = STConst.ST_PAGE_PRE_APP_UNINSTALL_ROOT;
            } else {
                buildSTInfo.scene = STConst.ST_PAGE_PRE_APP_UNINSTALL_NO_ROOT;
            }
        }
        com.tencent.assistantv2.st.b.b.getInstance().exposure(buildSTInfo);
    }

    private boolean C() {
        return cr.a().c() || m.a().n() == AppConst.ROOT_STATUS.ROOTED;
    }

    private void a(boolean z2, String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", z2 ? "success" : "fail");
        hashMap.put("B2", str);
        hashMap.put("B3", str2);
        hashMap.put("B4", t.u());
        hashMap.put("B5", Build.VERSION.RELEASE);
        hashMap.put("B6", Global.getPhoneGuidAndGen());
        hashMap.put("B7", Global.getQUAForBeacon());
        hashMap.put("B8", t.g());
        a.a("AppUninstallResult", z2, -1, -1, hashMap, false);
        XLog.d("beacon", "beacon report >> event: AppUninstallResult, params : " + hashMap.toString());
    }
}
