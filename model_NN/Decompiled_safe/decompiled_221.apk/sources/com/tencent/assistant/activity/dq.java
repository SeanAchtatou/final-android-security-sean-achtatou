package com.tencent.assistant.activity;

import android.content.Intent;
import android.view.View;

/* compiled from: ProGuard */
class dq implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ HelperFAQActivity f532a;

    dq(HelperFAQActivity helperFAQActivity) {
        this.f532a = helperFAQActivity;
    }

    public void onClick(View view) {
        this.f532a.startActivity(new Intent(this.f532a.y, HelperFeedbackActivity.class));
    }
}
