package com.tencent.assistant.activity;

import android.app.Activity;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.View;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.module.bp;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.activity.ay;
import com.tencent.assistantv2.adapter.RankNormalListAdapter;
import com.tencent.assistantv2.component.GameRankNormalListView;
import com.tencent.assistantv2.component.RankNormalListView;
import com.tencent.assistantv2.component.ar;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class cy extends ay implements UIEventListener, ar {
    protected View.OnClickListener P = new cz(this);
    private final String S = "GameBandNetFragment:";
    private LoadingView T;
    private NormalErrorRecommendPage U;
    /* access modifiers changed from: private */
    public GameRankNormalListView V;
    private bp W = null;
    /* access modifiers changed from: private */
    public RankNormalListAdapter X = null;
    private long Y;
    private long Z;
    private long aa;
    private long ab;
    private ListViewScrollListener ac = new da(this);
    private ApkResCallback.Stub ad = new db(this);

    public cy() {
    }

    public cy(Activity activity) {
        super(activity);
    }

    public void d(Bundle bundle) {
        super.d(bundle);
        d((int) R.layout.gameband_normal_fragment);
        A();
        this.V.a(true);
    }

    public void A() {
        this.V = (GameRankNormalListView) e((int) R.id.applist);
        this.V.setTopPaddingSize(6);
        this.V.setVisibility(8);
        this.V.o = true;
        this.V.a((ay) this);
        this.T = (LoadingView) e((int) R.id.loading_view);
        this.T.setVisibility(0);
        this.U = (NormalErrorRecommendPage) e((int) R.id.error_page);
        this.U.setButtonClickListener(this.P);
        this.U.setIsAutoLoading(true);
        this.Y = b().getLong("subId");
        this.Z = b().getLong("subAppListType");
        this.aa = b().getLong("subPageSize");
        this.ab = b().getLong("flag");
        this.W = new bp(this.Y, (int) this.Z, (short) ((int) this.aa));
        this.V.a(this.W);
        this.V.a((ar) this);
        this.V.setDivider(null);
        this.V.a(this.ac);
        this.X = new RankNormalListAdapter(this.Q, this.V, this.W.a());
        this.X.a(J(), -100);
        this.X.a(RankNormalListAdapter.ListType.LISTTYPEGAMESORT);
        this.X.a(a((byte) ((int) this.ab)));
        this.X.a(B());
        this.V.c();
        this.V.setAdapter(this.X);
        this.X.a(this.ac);
    }

    public String B() {
        return Constants.STR_EMPTY;
    }

    public void d(boolean z) {
        this.V.b();
        this.X.notifyDataSetChanged();
        ApkResourceManager.getInstance().registerApkResCallback(this.ad);
    }

    public void k() {
        super.k();
        this.X.b();
        ApkResourceManager.getInstance().unRegisterApkResCallback(this.ad);
    }

    public void n() {
        super.n();
        this.X.b();
        this.V.recycleData();
        ApkResourceManager.getInstance().unRegisterApkResCallback(this.ad);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_HOTTAB_DOWNLOAD_FILTER_SHOW, this);
    }

    public void e(boolean z) {
        if (z) {
            this.X.a(RankNormalListAdapter.ListType.LISTTYPEGAMESORT);
        } else {
            this.X.a(RankNormalListAdapter.ListType.LISTTYPENORMAL);
        }
    }

    public void f(boolean z) {
        this.X.a(z);
    }

    public void b(int i) {
        this.X.b(i);
    }

    public void C() {
        Log.e("YYB5_0", "GameBandNetFragment:onPageTurnBackground------------2:::");
    }

    public int D() {
        return 0;
    }

    public int E() {
        return 2;
    }

    public void F() {
        this.T.setVisibility(0);
        this.V.setVisibility(8);
        this.U.setVisibility(8);
    }

    public void c(int i) {
        this.T.setVisibility(8);
        this.V.setVisibility(8);
        this.U.setVisibility(0);
        this.U.setErrorType(i);
    }

    public void G() {
        this.V.setVisibility(0);
        this.U.setVisibility(8);
        this.T.setVisibility(8);
    }

    public void H() {
    }

    private boolean a(byte b) {
        return (b & 1) == 1;
    }

    private void N() {
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_HOTTAB_DOWNLOAD_FILTER_SHOW /*1135*/:
                N();
                return;
            default:
                return;
        }
    }

    public void I() {
        M();
    }

    public int J() {
        return STConst.ST_PAGE_GAME_RANKING;
    }

    public String K() {
        return RankNormalListView.ST_HIDE_INSTALLED_APPS;
    }
}
