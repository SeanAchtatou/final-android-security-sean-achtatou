package com.tencent.assistant.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.l;
import android.support.v4.app.r;
import android.util.SparseArray;
import com.tencent.assistantv2.b.i;
import java.lang.ref.WeakReference;
import java.util.List;

/* compiled from: ProGuard */
public class cw extends r {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameRankActivity f464a;
    private Activity b;
    private List<i> c;
    private SparseArray<WeakReference<Fragment>> d = new SparseArray<>();

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cw(GameRankActivity gameRankActivity, l lVar, Activity activity, List<i> list) {
        super(lVar);
        this.f464a = gameRankActivity;
        this.b = activity;
        this.c = list;
    }

    public Fragment a(int i) {
        Fragment fragment;
        WeakReference weakReference;
        i iVar = this.c.get(i);
        if (iVar == null || (weakReference = this.d.get(iVar.f)) == null || weakReference.get() == null) {
            fragment = null;
        } else {
            fragment = (Fragment) weakReference.get();
        }
        if (fragment == null && (fragment = a(iVar)) != null) {
            this.d.put(iVar.f, new WeakReference(fragment));
        }
        return fragment;
    }

    public int getCount() {
        return this.c.size();
    }

    private Fragment a(i iVar) {
        Bundle bundle = new Bundle();
        bundle.putLong("subId", iVar.e);
        bundle.putLong("subAppListType", (long) iVar.f);
        bundle.putLong("subPageSize", (long) iVar.g);
        bundle.putLong("flag", (long) iVar.h);
        switch (iVar.f) {
            case 8:
                dg dgVar = new dg(this.b);
                dgVar.b(bundle);
                return dgVar;
            case 9:
                dd ddVar = new dd(this.b);
                ddVar.b(bundle);
                return ddVar;
            case 10:
                df dfVar = new df(this.b);
                dfVar.b(bundle);
                return dfVar;
            case 11:
                de deVar = new de(this.b);
                deVar.b(bundle);
                return deVar;
            default:
                de deVar2 = new de(this.b);
                deVar2.b(bundle);
                return deVar2;
        }
    }
}
