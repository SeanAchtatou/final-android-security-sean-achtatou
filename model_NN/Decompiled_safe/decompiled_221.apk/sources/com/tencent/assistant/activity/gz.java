package com.tencent.assistant.activity;

import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;
import java.util.List;

/* compiled from: ProGuard */
class gz extends ApkResCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TencentAppListActivity f622a;

    gz(TencentAppListActivity tencentAppListActivity) {
        this.f622a = tencentAppListActivity;
    }

    public void onLoadInstalledApkSuccess(List<LocalApkInfo> list) {
        this.f622a.runOnUiThread(new ha(this));
    }
}
