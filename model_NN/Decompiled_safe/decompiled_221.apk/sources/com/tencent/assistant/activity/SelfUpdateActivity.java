package com.tencent.assistant.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.SelfForceUpdateView;
import com.tencent.assistant.component.SelfNormalUpdateView;
import com.tencent.assistant.manager.SelfUpdateManager;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class SelfUpdateActivity extends BaseActivity implements SelfNormalUpdateView.UpdateListener {
    public View n;
    public RelativeLayout t;
    private boolean u = false;
    private SelfForceUpdateView v;
    private SelfNormalUpdateView w;
    private SelfUpdateManager.SelfUpdateInfo x;

    public int f() {
        if (this.x == null || this.x.y == null || Constants.STR_EMPTY.equals(this.x.y.trim())) {
            return STConst.ST_PAGE_SELF_UPDATE;
        }
        return STConst.ST_PAGE_SELF_UPDATE_GRAY;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        setContentView((int) R.layout.dialog_layout);
        getWindow().setLayout(-1, -2);
        this.n = findViewById(R.id.content_root);
        this.t = (RelativeLayout) findViewById(R.id.dialog_root);
        this.n.setOnClickListener(new fh(this));
        b(getIntent());
        super.onCreate(bundle);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        b(intent);
    }

    private void b(Intent intent) {
        this.x = SelfUpdateManager.a().d();
        if (this.x != null) {
            this.u = SelfUpdateManager.a().k();
        }
        j();
    }

    public void onResume() {
        super.onResume();
        if (this.v != null) {
            this.v.onResume();
        }
        if (this.w != null) {
            this.w.onResume();
        }
        overridePendingTransition(-1, -1);
    }

    public void h() {
        k.a(i());
    }

    public STInfoV2 i() {
        STInfoV2 s = s();
        s.slotId = f() + Constants.STR_EMPTY;
        return s;
    }

    public void onPause() {
        super.onPause();
        overridePendingTransition(-1, -1);
    }

    public void onDestroy() {
        if (this.v != null) {
            this.v.onDestroy();
        }
        if (this.w != null) {
            this.w.onDestroy();
        }
        super.onDestroy();
    }

    private void j() {
        this.t.removeAllViews();
        if (this.x == null) {
            finish();
        } else if (this.u) {
            this.v = new SelfForceUpdateView(this);
            this.v.setUpdateListener(this);
            b(this.v);
            this.v.initView();
        } else {
            this.w = new SelfNormalUpdateView(this);
            this.w.setUpdateListener(this);
            b(this.w);
            this.w.initView();
        }
    }

    public void finish() {
        super.finish();
        overridePendingTransition(-1, -1);
    }

    public void b(View view) {
        if (view != null) {
            this.t.removeAllViews();
            this.t.addView(view);
            return;
        }
        finish();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return true;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (!this.u) {
            if (i == 4) {
                SelfUpdateManager.a().n().a();
            } else if (i == 82) {
                return true;
            }
            return super.onKeyDown(i, keyEvent);
        } else if (i == 4) {
            if (getParent() != null) {
                return getParent().moveTaskToBack(true);
            }
            return moveTaskToBack(true);
        } else if (i != 82) {
            return super.onKeyDown(i, keyEvent);
        } else {
            return true;
        }
    }

    public void dissmssDialog() {
        finish();
    }
}
