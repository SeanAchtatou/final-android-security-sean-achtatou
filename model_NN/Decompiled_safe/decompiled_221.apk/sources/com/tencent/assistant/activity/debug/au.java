package com.tencent.assistant.activity.debug;

import android.view.View;
import com.tencent.assistant.plugin.QReaderClient;

/* compiled from: ProGuard */
class au implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ QReaderPluginDebugActivity f495a;

    au(QReaderPluginDebugActivity qReaderPluginDebugActivity) {
        this.f495a = qReaderPluginDebugActivity;
    }

    public void onClick(View view) {
        QReaderClient.getInstance().downloadQQReader();
    }
}
