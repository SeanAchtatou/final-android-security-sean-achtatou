package com.tencent.assistant.activity;

import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.b;
import com.tencent.assistant.protocol.jce.TagGroup;
import java.util.List;

/* compiled from: ProGuard */
class hb implements b {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TencentAppListActivity f625a;

    hb(TencentAppListActivity tencentAppListActivity) {
        this.f625a = tencentAppListActivity;
    }

    public void a(int i, int i2, boolean z, List<SimpleAppModel> list, List<TagGroup> list2) {
        if (i2 == 0) {
            if (!(list == null || this.f625a.v == null)) {
                this.f625a.v.a(z, list, this.f625a.n.h());
            }
            if (this.f625a.v.getCount() > 0) {
                this.f625a.w();
                this.f625a.t.onRefreshComplete(this.f625a.n.h(), true);
                return;
            }
            this.f625a.b(10);
        } else if (!z) {
            this.f625a.t.onRefreshComplete(this.f625a.n.h(), false);
        } else if (-800 == i2) {
            this.f625a.b(30);
        } else {
            this.f625a.b(20);
        }
    }
}
