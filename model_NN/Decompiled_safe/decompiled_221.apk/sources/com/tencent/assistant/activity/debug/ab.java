package com.tencent.assistant.activity.debug;

import android.view.View;
import com.tencent.assistant.m;
import com.tencent.assistant.module.update.AppUpdateConst;
import com.tencent.assistant.module.update.c;
import java.util.Map;

/* compiled from: ProGuard */
class ab implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DActivity f476a;

    ab(DActivity dActivity) {
        this.f476a = dActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    public void onClick(View view) {
        m.a().b("app_update_refresh_suc_time", (Object) -1);
        m.a().b("app_upload_all_succ_time", (Object) -1);
        m.a().b("app_update_response_succ_time", (Object) -1);
        c.a().a(AppUpdateConst.RequestLaunchType.TYPE_ASSISTANT_RETRY, (Map<String, String>) null);
    }
}
