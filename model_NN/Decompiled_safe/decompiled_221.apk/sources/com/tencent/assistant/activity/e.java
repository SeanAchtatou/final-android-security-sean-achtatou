package com.tencent.assistant.activity;

import android.view.View;
import android.widget.AdapterView;
import com.tencent.assistantv2.model.ItemElement;

/* compiled from: ProGuard */
class e implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AboutActivity f542a;

    e(AboutActivity aboutActivity) {
        this.f542a = aboutActivity;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        ItemElement itemElement = (ItemElement) this.f542a.y.getItem(i);
        if (itemElement != null) {
            this.f542a.a(itemElement);
        }
    }
}
