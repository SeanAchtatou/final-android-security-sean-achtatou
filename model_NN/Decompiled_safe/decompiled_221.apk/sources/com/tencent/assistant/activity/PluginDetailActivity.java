package com.tencent.assistant.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.PluginDownStateButton;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.manager.av;
import com.tencent.assistant.model.PluginStartEntry;
import com.tencent.assistant.model.j;
import com.tencent.assistant.module.u;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.mgr.c;
import com.tencent.assistant.plugin.mgr.d;
import com.tencent.assistant.plugin.proxy.PluginProxyActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class PluginDetailActivity extends BaseActivity {
    private PluginStartEntry n;
    private TXImageView t;
    private TextView u;
    private PluginDownStateButton v;
    private SecondNavigationTitleViewV5 w;
    private TextView x;
    private boolean y = true;

    public int f() {
        return STConst.ST_PAGE_PLUGIN;
    }

    public void onCreate(Bundle bundle) {
        PluginInfo.PluginEntry pluginEntry;
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_plugin_detail);
        Intent intent = getIntent();
        if (intent != null && intent.hasExtra("plugin_start_entry")) {
            this.n = (PluginStartEntry) intent.getSerializableExtra("plugin_start_entry");
        }
        j();
        if (this.n != null) {
            PluginInfo a2 = d.b().a(this.n.a());
            if (a2 != null && (a2.getVersion() >= this.n.b() || c.a(this.n.a()) >= 0)) {
                PluginInfo.PluginEntry pluginEntryByStartActivity = a2.getPluginEntryByStartActivity(this.n.c());
                if (pluginEntryByStartActivity == null) {
                    pluginEntry = new PluginInfo.PluginEntry(a2, Constants.STR_EMPTY, null, this.n.c());
                } else {
                    pluginEntry = pluginEntryByStartActivity;
                }
                if (pluginEntry != null) {
                    try {
                        PluginProxyActivity.a(this, pluginEntry.getHostPlugInfo().getPackageName(), pluginEntry.getHostPlugInfo().getVersion(), pluginEntry.getStartActivity(), pluginEntry.getHostPlugInfo().getInProcess(), null, pluginEntry.getHostPlugInfo().getLaunchApplication());
                    } catch (Throwable th) {
                        th.printStackTrace();
                    }
                    String str = this.n.d() + "|" + pluginEntry.getHostPlugInfo().getPackageName() + "|" + pluginEntry.getStartActivity() + "|" + 1;
                    STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this, 100);
                    if (buildSTInfo != null) {
                        buildSTInfo.extraData = str;
                        k.a(buildSTInfo);
                    }
                    finish();
                    return;
                }
            }
            j a3 = av.a().a(this.n.d());
            if (a3 == null) {
                a3 = av.a().a(this.n.a());
            }
            if (a3 == null) {
                Toast.makeText(this, (int) R.string.plugin_not_exist, 0).show();
                finish();
                return;
            }
            this.w.b(a3.e);
            this.w.d();
            this.t.updateImageView(a3.i, R.drawable.icon_default_pic_empty, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            this.u.setText(a3.f);
            if (a2 == null || a2.getVersion() == this.n.b()) {
                this.x.setVisibility(8);
                this.v.setUpdateState(false);
            } else {
                this.x.setText(String.format(getResources().getString(R.string.plugin_update_tips), a3.e));
                this.x.setVisibility(0);
                this.v.setUpdateState(true);
            }
            this.v.setDownloadInfo(a3, this.n);
            return;
        }
        finish();
    }

    private DownloadInfo i() {
        j jVar;
        j a2 = av.a().a(this.n.d());
        if (a2 == null) {
            jVar = av.a().a(this.n.a());
        } else {
            jVar = a2;
        }
        if (jVar == null) {
            return null;
        }
        DownloadInfo b = av.a().b(jVar);
        if (b == null) {
            return av.a().a(jVar, SimpleDownloadInfo.UIType.PLUGIN_PREDOWNLOAD);
        }
        return b;
    }

    private void j() {
        this.t = (TXImageView) findViewById(R.id.plugin_pic);
        this.u = (TextView) findViewById(R.id.plugin_desc);
        this.v = (PluginDownStateButton) findViewById(R.id.plugin_down_btn);
        this.v.setHost(this);
        this.w = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.w.d(false);
        this.w.a(this);
        this.x = (TextView) findViewById(R.id.plugin_update_tips);
    }

    private void a(j jVar, PluginStartEntry pluginStartEntry) {
        if (d.b().a(pluginStartEntry.a(), pluginStartEntry.b()) == null) {
            DownloadInfo i = i();
            if (u.a(i, jVar, (PluginInfo) null) == AppConst.AppState.DOWNLOADED) {
                TemporaryThreadManager.get().start(new eu(this, i, jVar));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.v != null) {
            this.v.onPause();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.v != null) {
            this.v.onResume();
        }
        if (this.y) {
            j a2 = av.a().a(this.n.d());
            if (a2 == null) {
                a2 = av.a().a(this.n.a());
            }
            a(a2, this.n);
            this.y = false;
        }
    }
}
