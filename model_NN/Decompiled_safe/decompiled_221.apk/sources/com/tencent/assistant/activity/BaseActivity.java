package com.tencent.assistant.activity;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.TypedArray;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.b.a;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.manager.SelfUpdateManager;
import com.tencent.assistant.manager.be;
import com.tencent.assistant.module.callback.ad;
import com.tencent.assistant.module.fa;
import com.tencent.assistant.module.timer.RecoverAppListReceiver;
import com.tencent.assistant.module.timer.job.SelfUpdateTimerJob;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.st.f;
import com.tencent.assistant.utils.FunctionUtils;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.bg;
import com.tencent.assistant.utils.dh;
import com.tencent.assistantv2.activity.MainActivity;
import com.tencent.assistantv2.st.b;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STExternalInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.STPageInfo;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class BaseActivity extends FragmentActivity {
    private static boolean A = false;
    /* access modifiers changed from: private */
    public static boolean B = false;
    private static Handler C = ba.a("BaseActivityHandler");
    static Handler s = new bh();
    private static boolean z = true;
    /* access modifiers changed from: private */
    public ad D = new bi(this);
    private List<bj> n = new ArrayList();
    protected STPageInfo o = new STPageInfo();
    public STExternalInfo p = new STExternalInfo();
    protected String q = Constants.STR_EMPTY;
    protected boolean r = false;
    private LinearLayout t = null;
    private LayoutInflater u = null;
    /* access modifiers changed from: private */
    public PopupWindow v = null;
    private bk w = null;
    private Dialog x;
    private BroadcastReceiver y = new bc(this);

    /* access modifiers changed from: protected */
    public void a(Intent intent) {
        if (!isFinishing()) {
            finish();
        }
    }

    public void startActivity(Intent intent) {
        try {
            b(intent);
            super.startActivity(intent);
        } catch (Throwable th) {
        }
    }

    public void startActivityForResult(Intent intent, int i) {
        try {
            b(intent);
            super.startActivityForResult(intent, i);
        } catch (Throwable th) {
        }
    }

    private void b(Intent intent) {
        if (intent != null) {
            Bundle a2 = bg.a(intent);
            if (a2 == null || a2.get(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME) == null) {
                intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.o.f3358a);
                intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_SLOT_TAG_NAME, this.o.e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        AstApp.i().a();
        overridePendingTransition(R.anim.activity_right_in, R.anim.activity_left_out);
        IntentFilter intentFilter = new IntentFilter("com.tencent.android.qqdownloader.action.EXIT_APP");
        intentFilter.addCategory("android.intent.category.DEFAULT");
        try {
            registerReceiver(this.y, intentFilter);
        } catch (Exception e) {
        }
        this.u = (LayoutInflater) getSystemService("layout_inflater");
        i();
        int a2 = bg.a(getIntent(), PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, 2000);
        if (a2 == -1000) {
            f.a(a2);
        } else {
            f.a(f());
        }
        r();
        if (g()) {
            h();
        }
        dh.a();
        AstApp.a(this);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
    }

    private void i() {
        Bundle a2 = bg.a(getIntent());
        if (a2 != null) {
            this.r = a2.getBoolean(a.G);
            this.q = a2.getString(a.n);
            if (!B) {
                B = a2.getBoolean(a.t);
            }
            a(a2);
        }
    }

    private void a(Bundle bundle) {
        if (bundle != null) {
            this.p.f3357a = bundle.getString(a.j);
            this.p.b = bundle.getString(a.y);
            this.p.c = bundle.getString(a.n);
            this.p.d = bundle.getString(a.o);
            this.p.e = bundle.getString(a.ac);
            this.p.f = bundle.getString(a.ad);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        s.sendEmptyMessageDelayed(10086, 100);
        if (this.x != null) {
            this.x.dismiss();
        }
        if (!(this instanceof SelfUpdateActivity)) {
            fa.a().unregister(this.D);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        int i = 0;
        super.onResume();
        s.removeMessages(10086);
        m();
        AstApp.a(this);
        if (!(this instanceof SelfUpdateActivity)) {
            TemporaryThreadManager.get().start(new bd(this));
        }
        if (z) {
            z = false;
            if (k()) {
                A = true;
                XLog.d("voken", this + "mFirstOnResume : 检查更新");
                SelfUpdateTimerJob.e().d();
            }
            int i2 = this instanceof MainActivity ? 1000 : 0;
            if (C != null) {
                C.postDelayed(new be(this), (long) i2);
            }
        }
        if (this instanceof MainActivity) {
            i = 2000;
        }
        if (l() && MainActivity.x == RecoverAppListReceiver.RecoverAppListState.NOMAL.ordinal() && C != null) {
            C.postDelayed(new bf(this), (long) i);
        }
    }

    /* access modifiers changed from: protected */
    public boolean k() {
        return !B;
    }

    /* access modifiers changed from: protected */
    public boolean l() {
        return !B;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.AppService.AstApp.a(boolean, int):void
     arg types: [int, int]
     candidates:
      com.qq.AppService.AstApp.a(android.content.pm.PackageManager, java.lang.String):void
      com.qq.AppService.AstApp.a(boolean, int):void */
    /* access modifiers changed from: protected */
    public void m() {
        AstApp.i().a(true, 0);
        AstApp.i().j().sendEmptyMessage(EventDispatcherEnum.UI_EVENT_APP_ATFRONT);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        try {
            unregisterReceiver(this.y);
        } catch (Exception e) {
        }
        if (AstApp.m() == this) {
            AstApp.a((Activity) null);
        }
        super.onDestroy();
    }

    public void onBackPressed() {
        try {
            super.onBackPressed();
        } catch (Exception e) {
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            if (o() == 0) {
                return false;
            }
            if (!TextUtils.isEmpty(this.q) || this.r) {
                finish();
                return true;
            }
        }
        return super.onKeyDown(i, keyEvent);
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00a1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void finish() {
        /*
            r7 = this;
            r2 = 0
            r6 = 10
            java.lang.String r0 = r7.q
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x00a9
            com.qq.AppService.AstApp r0 = com.qq.AppService.AstApp.i()
            java.lang.String r1 = "android.permission.GET_TASKS"
            int r0 = r0.checkCallingOrSelfPermission(r1)
            if (r0 != 0) goto L_0x00a9
            java.lang.String r0 = "activity"
            java.lang.Object r0 = r7.getSystemService(r0)
            android.app.ActivityManager r0 = (android.app.ActivityManager) r0
            r1 = 1
            java.util.List r1 = r0.getRecentTasks(r6, r1)
            if (r1 == 0) goto L_0x00a9
            java.util.Iterator r3 = r1.iterator()
        L_0x002a:
            boolean r1 = r3.hasNext()
            if (r1 == 0) goto L_0x00a9
            java.lang.Object r1 = r3.next()
            android.app.ActivityManager$RecentTaskInfo r1 = (android.app.ActivityManager.RecentTaskInfo) r1
            android.content.Intent r1 = r1.baseIntent
            if (r1 == 0) goto L_0x002a
            android.content.ComponentName r4 = r1.getComponent()
            java.lang.String r4 = r4.getPackageName()
            java.lang.String r5 = r7.q
            boolean r4 = r4.equals(r5)
            if (r4 == 0) goto L_0x002a
            java.lang.String r3 = r7.q
            java.lang.String r4 = "com.tencent.mobileqq"
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x00e5
            android.content.ComponentName r3 = r1.getComponent()
            if (r3 == 0) goto L_0x00e5
            java.lang.String r3 = "com.tencent.mobileqq.activity.LoginActivity"
            android.content.ComponentName r4 = r1.getComponent()
            java.lang.String r4 = r4.getClassName()
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x00e5
            java.util.List r0 = r0.getRunningTasks(r6)
            if (r0 == 0) goto L_0x00e5
            java.util.Iterator r3 = r0.iterator()
        L_0x0074:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x00e5
            java.lang.Object r0 = r3.next()
            android.app.ActivityManager$RunningTaskInfo r0 = (android.app.ActivityManager.RunningTaskInfo) r0
            android.content.ComponentName r4 = r0.baseActivity
            if (r4 == 0) goto L_0x0074
            java.lang.String r0 = "com.tencent.mobileqq"
            java.lang.String r5 = r4.getPackageName()
            boolean r0 = r0.equals(r5)
            if (r0 == 0) goto L_0x0074
            android.content.Intent r0 = new android.content.Intent
            r0.<init>(r1)
            r0.setComponent(r4)
        L_0x0098:
            if (r0 != 0) goto L_0x009f
            android.content.Intent r0 = new android.content.Intent
            r0.<init>(r1)
        L_0x009f:
            if (r0 == 0) goto L_0x00a9
            r1 = 1048576(0x100000, float:1.469368E-39)
            r0.addFlags(r1)
            r7.startActivity(r0)     // Catch:{ ActivityNotFoundException -> 0x00e1, Exception -> 0x00e3 }
        L_0x00a9:
            boolean r0 = r7.r
            if (r0 == 0) goto L_0x00ce
            com.tencent.assistantv2.activity.MainActivity r0 = com.tencent.assistantv2.activity.MainActivity.i()
            if (r0 == 0) goto L_0x00c3
            com.tencent.assistantv2.activity.MainActivity r0 = com.tencent.assistantv2.activity.MainActivity.i()
            if (r0 == 0) goto L_0x00ce
            com.qq.AppService.AstApp r0 = com.qq.AppService.AstApp.i()
            boolean r0 = r0.l()
            if (r0 != 0) goto L_0x00ce
        L_0x00c3:
            com.qq.AppService.AstApp r0 = com.qq.AppService.AstApp.i()
            java.lang.String r0 = r0.getPackageName()
            com.tencent.assistant.utils.r.a(r0, r2)
        L_0x00ce:
            super.finish()
            boolean r0 = r7.n()
            if (r0 == 0) goto L_0x00e0
            r0 = 2130968578(0x7f040002, float:1.7545814E38)
            r1 = 2130968581(0x7f040005, float:1.754582E38)
            r7.overridePendingTransition(r0, r1)
        L_0x00e0:
            return
        L_0x00e1:
            r0 = move-exception
            goto L_0x00a9
        L_0x00e3:
            r0 = move-exception
            goto L_0x00a9
        L_0x00e5:
            r0 = r2
            goto L_0x0098
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.activity.BaseActivity.finish():void");
    }

    public boolean n() {
        return true;
    }

    public int o() {
        return 1;
    }

    public int f() {
        return 2000;
    }

    public int p() {
        return this.o.c;
    }

    public void a(int i) {
        this.o.c = i;
        be.a().b(i);
    }

    /* access modifiers changed from: protected */
    public boolean g() {
        return true;
    }

    public STPageInfo q() {
        this.o.f3358a = f();
        this.o.c = p();
        return this.o;
    }

    public void a(String str, String str2) {
        this.o.e = com.tencent.assistantv2.st.page.a.b(str, str2);
    }

    /* access modifiers changed from: protected */
    public void a(STPageInfo sTPageInfo, String str, String str2) {
        if (sTPageInfo != null) {
            this.o.c = sTPageInfo.f3358a;
            this.o.d = com.tencent.assistantv2.st.page.a.b(str, str2);
        }
    }

    /* access modifiers changed from: protected */
    public void r() {
        this.o.c = bg.a(getIntent(), PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, 2000);
        this.o.d = bg.a(getIntent(), PluginActivity.PARAMS_PRE_ACTIVITY_SLOT_TAG_NAME);
    }

    /* access modifiers changed from: protected */
    public STInfoV2 s() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this, 100);
        if (buildSTInfo != null) {
            buildSTInfo.updateWithExternalPara(this.p);
        }
        return buildSTInfo;
    }

    /* access modifiers changed from: protected */
    public void h() {
        k.a(s());
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add("menu");
        return true;
    }

    public boolean onMenuOpened(int i, Menu menu) {
        this.n.clear();
        this.n.add(new bj(this, R.string.menu_feedBack, R.drawable.icon_feedback));
        this.n.add(new bj(this, R.string.menu_setting, R.drawable.icon_setting));
        this.n.add(new bj(this, R.string.menu_exit, R.drawable.icon_shutdown));
        this.t = (LinearLayout) this.u.inflate((int) R.layout.menu_layout, (ViewGroup) null);
        if (this.t != null) {
            this.v = new PopupWindow(this.t, -1, -2);
            this.v.setOutsideTouchable(true);
            this.v.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
            this.w = new bk(this);
            a(this.t, 0, this.n.size() - 1, 1);
        }
        if (!(this.v == null || this.t == null)) {
            if (this.v.isShowing()) {
                this.v.dismiss();
            } else {
                this.t.setFocusable(true);
                this.t.setOnKeyListener(new bg(this));
                this.t.setFocusableInTouchMode(true);
                if (!isFinishing() && !this.v.isShowing()) {
                    try {
                        this.v.showAtLocation(this.t, 80, 0, 0);
                        this.v.setFocusable(true);
                        this.v.update();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return false;
    }

    private void a(LinearLayout linearLayout, int i, int i2, int i3) {
        TypedArray obtainTypedArray = getResources().obtainTypedArray(R.array.menu_item_bgs);
        while (i <= i2) {
            bj bjVar = this.n.get(i);
            int a2 = bjVar.a();
            LinearLayout linearLayout2 = (LinearLayout) this.u.inflate((int) R.layout.menu_item, (ViewGroup) null);
            ((ImageView) linearLayout2.findViewById(R.id.menu_icon)).setBackgroundResource(obtainTypedArray.getResourceId(i, 0));
            ((TextView) linearLayout2.findViewById(R.id.menu_text)).setText(a2);
            linearLayout2.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
            linearLayout2.setId(bjVar.a());
            if (i == i2) {
                linearLayout2.findViewById(R.id.menu_line).setVisibility(8);
            } else {
                linearLayout2.findViewById(R.id.menu_line).setVisibility(0);
            }
            linearLayout.addView(linearLayout2);
            this.t.findViewById(bjVar.a()).setOnClickListener(this.w);
            i++;
        }
        linearLayout.setWeightSum((float) ((i2 - i) + 1));
        obtainTypedArray.recycle();
    }

    /* access modifiers changed from: protected */
    public void a(View view) {
        if (this.v != null && this.v.isShowing()) {
            this.v.dismiss();
        }
        switch (view.getId()) {
            case R.string.menu_setting /*2131362223*/:
                startActivity(new Intent(this, SettingActivity.class));
                return;
            case R.string.menu_feedBack /*2131362224*/:
                startActivity(new Intent(this, HelperFeedbackActivity.class));
                return;
            case R.string.menu_exit /*2131362225*/:
                b.a().e();
                FunctionUtils.a(this);
                return;
            default:
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.AppService.AstApp.a(boolean, int):void
     arg types: [int, int]
     candidates:
      com.qq.AppService.AstApp.a(android.content.pm.PackageManager, java.lang.String):void
      com.qq.AppService.AstApp.a(boolean, int):void */
    protected static void t() {
        List<ActivityManager.RunningTaskInfo> runningTasks;
        try {
            if (AstApp.i().checkCallingOrSelfPermission("android.permission.GET_TASKS") != 0 || (runningTasks = ((ActivityManager) AstApp.i().getApplicationContext().getSystemService("activity")).getRunningTasks(1)) == null || runningTasks.isEmpty()) {
                return;
            }
            if (!runningTasks.get(0).topActivity.getPackageName().equals(AstApp.i().getPackageName())) {
                AstApp.i().a(false, 0);
            } else {
                AstApp.i().a(true, 0);
            }
        } catch (Throwable th) {
        }
    }

    public void a(Dialog dialog) {
        if (dialog != null) {
            this.x = dialog;
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        if (SelfUpdateManager.a().h()) {
            if (SelfUpdateManager.a().d() != null && SelfUpdateManager.a().k()) {
                SelfUpdateManager.a().b(false);
            }
        } else if (SelfUpdateManager.a().d() != null && !SelfUpdateManager.a().k()) {
            if (A) {
                A = false;
                if (SelfUpdateManager.a().p()) {
                    return;
                }
            } else {
                return;
            }
        }
        SelfUpdateManager.a().d(true);
        Intent intent = new Intent(this, SelfUpdateActivity.class);
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, f());
        intent.addFlags(335544320);
        startActivity(intent);
    }
}
