package com.tencent.assistant.tagpage;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import com.tencent.assistant.protocol.jce.AppTagInfo;
import com.tencent.assistant.utils.XLog;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/* compiled from: ProGuard */
public class w {
    public static void a(Context context, AppTagInfo appTagInfo, String str) {
        if (context != null && appTagInfo != null && !TextUtils.isEmpty(appTagInfo.f2005a)) {
            StringBuffer stringBuffer = new StringBuffer("tmast://tagpage?tagID=");
            stringBuffer.append(appTagInfo.f2005a);
            stringBuffer.append("&tagName=");
            stringBuffer.append(appTagInfo.b);
            stringBuffer.append("&tagSubTitle=");
            stringBuffer.append(appTagInfo.e);
            if (!TextUtils.isEmpty(str)) {
                try {
                    stringBuffer.append("&firstIconUrl=");
                    stringBuffer.append(URLEncoder.encode(str, "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(stringBuffer.toString()));
            intent.setFlags(268435456);
            context.startActivity(intent);
            XLog.i("TagPageUtils", "[openNewTagPage] ---> url : " + stringBuffer.toString());
        }
    }
}
