package com.tencent.assistant.tagpage;

import android.view.SurfaceHolder;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class o implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SurfaceHolder f2559a;
    final /* synthetic */ TagPageCardAdapter b;

    o(TagPageCardAdapter tagPageCardAdapter, SurfaceHolder surfaceHolder) {
        this.b = tagPageCardAdapter;
        this.f2559a = surfaceHolder;
    }

    public void run() {
        try {
            this.b.k.reset();
            this.b.k.setDataSource(((SimpleAppModel) this.b.getItem(TagPageCardAdapter.f2542a)).aF);
            this.b.k.setOnPreparedListener(this.b);
            this.b.k.setOnCompletionListener(this.b);
            this.b.k.setOnBufferingUpdateListener(this.b);
            this.b.k.setOnErrorListener(this.b);
            this.b.k.setVolume(0.0f, 0.0f);
            this.b.k.setDisplay(this.f2559a);
            this.b.k.prepare();
        } catch (Exception e) {
            XLog.e("TagPageCradAdapter", "[surfaceCreated] ---> e.printStackTrace() = " + e.toString());
            e.printStackTrace();
        }
    }
}
