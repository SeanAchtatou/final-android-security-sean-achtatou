package com.tencent.assistant.tagpage;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Process;
import android.util.Log;
import com.tencent.assistant.utils.df;

/* compiled from: ProGuard */
public class a {
    public static Bitmap a(Bitmap bitmap, int i, int i2) {
        Bitmap bitmap2;
        if (bitmap == null) {
            return null;
        }
        try {
            Bitmap a2 = a(bitmap, 3, 8, 8);
            if (!(bitmap == bitmap || bitmap == null || bitmap.isRecycled())) {
                bitmap.recycle();
            }
            if (a2 != null) {
                int width = (int) (((float) a2.getWidth()) * 0.03f);
                int height = (int) (0.03f * ((float) a2.getHeight()));
                if (height > 0) {
                    bitmap2 = Bitmap.createBitmap(a2, width, height, a2.getWidth() - (width << 1), a2.getHeight() - (height << 1));
                    if (!a2.isRecycled()) {
                        a2.recycle();
                    }
                } else {
                    bitmap2 = a2;
                }
            } else {
                bitmap2 = null;
            }
            if (bitmap2 == null) {
                return bitmap2;
            }
            float height2 = (((float) bitmap2.getHeight()) * 1.0f) / ((float) bitmap2.getWidth());
            int b = df.b() + 100;
            Bitmap b2 = b(bitmap2, b, (int) (height2 * ((float) b)));
            if (!bitmap2.isRecycled()) {
                bitmap2.recycle();
            }
            if (b2 != null) {
                Log.i("Blur", "bitmap4.getWidth() = " + b2.getWidth() + ", bitmap4.getHeight() = " + b2.getHeight());
            }
            return b2;
        } catch (Exception e) {
            Log.e("Blur", e.toString());
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap b(Bitmap bitmap, int i, int i2) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Matrix matrix = new Matrix();
        matrix.postScale(((float) i) / ((float) width), ((float) i2) / ((float) height));
        try {
            return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        } catch (Throwable th) {
            return null;
        }
    }

    public static Bitmap a(Bitmap bitmap, int i, int i2, int i3) {
        Bitmap bitmap2 = null;
        if (bitmap == null) {
            return null;
        }
        try {
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            int[] iArr = new int[(width * height)];
            int[] iArr2 = new int[(width * height)];
            Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            try {
                bitmap.getPixels(iArr, 0, width, 0, 0, width, height);
                for (int i4 = 0; i4 < i; i4++) {
                    a(iArr, iArr2, width, height, (float) i2);
                    a(iArr2, iArr, height, width, (float) i3);
                }
                b(iArr, iArr2, width, height, (float) i2);
                b(iArr2, iArr, height, width, (float) i3);
                createBitmap.setPixels(iArr, 0, width, 0, 0, width, height);
                return createBitmap;
            } catch (Exception e) {
                e = e;
                bitmap2 = createBitmap;
                Log.e("Blur", e.toString());
                return bitmap2;
            } catch (OutOfMemoryError e2) {
                e = e2;
                bitmap2 = createBitmap;
                Log.e("Blur", e.toString());
                return bitmap2;
            } catch (Error e3) {
                e = e3;
                bitmap2 = createBitmap;
                Log.e("Blur", e.toString());
                return bitmap2;
            }
        } catch (Exception e4) {
            e = e4;
            Log.e("Blur", e.toString());
            return bitmap2;
        } catch (OutOfMemoryError e5) {
            e = e5;
            Log.e("Blur", e.toString());
            return bitmap2;
        } catch (Error e6) {
            e = e6;
            Log.e("Blur", e.toString());
            return bitmap2;
        }
    }

    private static void a(int[] iArr, int[] iArr2, int i, int i2, float f) {
        int i3;
        int i4 = i - 1;
        int i5 = (int) f;
        int i6 = (i5 * 2) + 1;
        int[] iArr3 = new int[(i6 * Process.PROC_COMBINE)];
        for (int i7 = 0; i7 < i6 * Process.PROC_COMBINE; i7++) {
            iArr3[i7] = i7 / i6;
        }
        int i8 = 0;
        int i9 = 0;
        while (true) {
            int i10 = i8;
            if (i9 < i2) {
                int i11 = 0;
                int i12 = 0;
                int i13 = 0;
                int i14 = 0;
                for (int i15 = -i5; i15 <= i5; i15++) {
                    int i16 = iArr[a(i15, 0, i - 1) + i10];
                    i11 += (i16 >> 24) & 255;
                    i12 += (i16 >> 16) & 255;
                    i13 += (i16 >> 8) & 255;
                    i14 += i16 & 255;
                }
                int i17 = i12;
                int i18 = i11;
                int i19 = i9;
                int i20 = i14;
                int i21 = i13;
                int i22 = 0;
                while (i22 < i) {
                    iArr2[i19] = (iArr3[i18] << 24) | (iArr3[i17] << 16) | (iArr3[i21] << 8) | iArr3[i20];
                    int i23 = i22 + i5 + 1;
                    if (i23 > i4) {
                        i3 = i4;
                    } else {
                        i3 = i23;
                    }
                    int i24 = i22 - i5;
                    if (i24 < 0) {
                        i24 = 0;
                    }
                    int i25 = iArr[i3 + i10];
                    int i26 = iArr[i24 + i10];
                    i18 += ((i25 >> 24) & 255) - ((i26 >> 24) & 255);
                    i17 += ((16711680 & i25) - (16711680 & i26)) >> 16;
                    i21 += ((65280 & i25) - (65280 & i26)) >> 8;
                    i22++;
                    i19 += i2;
                    i20 += (i25 & 255) - (i26 & 255);
                }
                i8 = i10 + i;
                i9++;
            } else {
                return;
            }
        }
    }

    private static void b(int[] iArr, int[] iArr2, int i, int i2, float f) {
        float f2 = f - ((float) ((int) f));
        float f3 = 1.0f / (1.0f + (2.0f * f2));
        int i3 = 0;
        int i4 = 0;
        while (true) {
            int i5 = i4;
            int i6 = i3;
            if (i5 < i2) {
                iArr2[i5] = iArr[0];
                int i7 = i5 + i2;
                for (int i8 = 1; i8 < i - 1; i8++) {
                    int i9 = i6 + i8;
                    int i10 = iArr[i9 - 1];
                    int i11 = iArr[i9];
                    int i12 = iArr[i9 + 1];
                    int i13 = (i12 >> 24) & 255;
                    int i14 = (i12 >> 16) & 255;
                    int i15 = (int) (((float) (((i10 >> 8) & 255) + ((i12 >> 8) & 255))) * f2);
                    int i16 = ((int) (((float) ((i12 & 255) + (i10 & 255))) * f2)) + (i11 & 255);
                    iArr2[i7] = ((int) (((float) i16) * f3)) | (((int) (((float) (((int) (((float) (((i10 >> 24) & 255) + i13)) * f2)) + ((i11 >> 24) & 255))) * f3)) << 24) | (((int) (((float) (((int) (((float) (((i10 >> 16) & 255) + i14)) * f2)) + ((i11 >> 16) & 255))) * f3)) << 16) | (((int) (((float) (i15 + ((i11 >> 8) & 255))) * f3)) << 8);
                    i7 += i2;
                }
                iArr2[i7] = iArr[i - 1];
                i3 = i6 + i;
                i4 = i5 + 1;
            } else {
                return;
            }
        }
    }

    private static int a(int i, int i2, int i3) {
        if (i < i2) {
            return i2;
        }
        return i > i3 ? i3 : i;
    }
}
