package com.tencent.assistant.tagpage;

import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.view.TextureView;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class s implements TextureView.SurfaceTextureListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TagPageCardAdapter f2563a;

    private s(TagPageCardAdapter tagPageCardAdapter) {
        this.f2563a = tagPageCardAdapter;
    }

    /* synthetic */ s(TagPageCardAdapter tagPageCardAdapter, j jVar) {
        this(tagPageCardAdapter);
    }

    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
        XLog.i("TagPageCradAdapter", "*** onSurfaceTextureAvailable ***");
        if (this.f2563a.k == null) {
            MediaPlayer unused = this.f2563a.k = new MediaPlayer();
        }
        TemporaryThreadManager.get().start(new t(this, surfaceTexture));
    }

    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
        XLog.i("TagPageCradAdapter", "*** onSurfaceTextureSizeChanged ***");
        if (this.f2563a.l != null) {
            this.f2563a.l.f.b();
        }
    }

    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        XLog.i("TagPageCradAdapter", "*** onSurfaceTextureDestroyed ***");
        return true;
    }

    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
    }
}
