package com.tencent.assistant.guessfavor;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.DownloadProgressButton;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorPage;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.login.d;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.net.c;
import com.tencent.assistant.protocol.jce.RecommendAppInfoEx;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.st.page.STPageInfo;
import java.util.List;

/* compiled from: ProGuard */
public class GuessFavorActivity extends BaseActivity implements UIEventListener, i {
    private TextView A;
    private LinearLayout B;
    private Button C;
    private String D = DownloadProgressButton.TMA_ST_NAVBAR_DOWNLOAD_TAG;
    private String E = "05_001";
    private final String F = "Jie";
    private boolean G = false;
    private ApkResCallback.Stub H = new a(this);
    private OnTMAParamExClickListener I = new d(this);
    /* access modifiers changed from: private */
    public Context n;
    private SecondNavigationTitleViewV5 t;
    private ListView u;
    /* access modifiers changed from: private */
    public GuessFavorAdapter v;
    /* access modifiers changed from: private */
    public j w;
    private ViewStub x = null;
    /* access modifiers changed from: private */
    public NormalErrorPage y = null;
    /* access modifiers changed from: private */
    public LoadingView z = null;

    public int f() {
        return STConst.ST_PAGE_GUESS_FAVOR_LOGIN;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_guess_favor);
        this.n = this;
        z();
        i();
    }

    private void i() {
        if (this.w == null) {
            this.w = new j();
            this.w.register(this);
        }
        this.w.a();
        w();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.v.c();
        this.t.l();
        v();
        XLog.i("Jie", "adapter onResume");
        y();
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
        ApkResourceManager.getInstance().registerApkResCallback(this.H);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.v != null) {
            this.v.b();
        }
        this.t.m();
        j();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.v != null) {
            this.v.d();
        }
        if (this.w != null) {
            this.w.b();
            this.w.unregister(this);
        }
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
        ApkResourceManager.getInstance().unRegisterApkResCallback(this.H);
    }

    private void j() {
        if (this.z != null && this.z.getVisibility() == 0) {
            this.G = true;
            this.z.setVisibility(8);
        }
    }

    private void v() {
        if (this.z != null && this.G) {
            this.z.setVisibility(0);
        }
    }

    /* access modifiers changed from: private */
    public void w() {
        if (this.z != null) {
            this.z.setVisibility(0);
        }
        if (this.u != null) {
            this.u.setVisibility(8);
        }
        if (this.C != null) {
            this.C.setEnabled(false);
            this.C.setVisibility(8);
        }
    }

    private void x() {
        this.G = false;
        if (this.z != null) {
            this.z.setVisibility(8);
        }
        if (this.u != null) {
            this.u.setVisibility(0);
        }
        if (this.C != null) {
            this.C.setEnabled(true);
        }
    }

    private void y() {
        this.t.b(getResources().getString(R.string.p_my_fav));
        if (this.B == null) {
            return;
        }
        if (d.a().j()) {
            this.B.setVisibility(8);
        } else {
            this.B.setVisibility(0);
        }
    }

    private void z() {
        this.x = (ViewStub) findViewById(R.id.error_stub);
        this.t = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.t.a(this);
        this.t.i();
        this.z = (LoadingView) findViewById(R.id.loading);
        this.u = (ListView) findViewById(R.id.list_view);
        this.v = new GuessFavorAdapter(this.n, this.u);
        this.u.setDivider(null);
        this.u.setAdapter((ListAdapter) this.v);
        this.u.setOnItemClickListener(new c(this));
        if (this.z == null || this.z.getVisibility() != 0) {
            this.u.setVisibility(0);
        } else {
            this.u.setVisibility(8);
        }
        this.A = (TextView) findViewById(R.id.tv_login);
        this.A.setTag(R.id.tma_st_slot_tag, this.D);
        this.C = (Button) findViewById(R.id.btn_change);
        this.C.setTag(R.id.tma_st_slot_tag, this.E);
        this.B = (LinearLayout) findViewById(R.id.layout_login);
        this.A.setOnClickListener(this.I);
        this.C.setOnClickListener(this.I);
        this.C.setVisibility(8);
    }

    private void b(int i) {
        if (this.y == null) {
            this.x.inflate();
            this.y = (NormalErrorPage) findViewById(R.id.error);
            this.y.setButtonClickListener(new e(this));
        }
        this.y.setErrorType(i);
        if (i == 4) {
            this.y.setErrorText(getResources().getString(R.string.collection_empty_tip), null);
        }
        if (this.u != null) {
            this.u.setVisibility(8);
        }
        this.y.setVisibility(0);
        if (this.C != null) {
            this.C.setVisibility(8);
        }
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS:
                XLog.i("Jie", "UI_EVENT_LOGIN_SUCCESS");
                i();
                if (this.B != null) {
                    this.B.setVisibility(8);
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_LOGIN_FAIL:
            case EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL:
            default:
                return;
        }
    }

    public void a(int i, int i2, List<RecommendAppInfoEx> list, List<SimpleAppModel> list2) {
        x();
        if (this.u != null) {
            if (i2 == 0) {
                if (!(list == null || this.v == null)) {
                    this.v.a(list, list2);
                }
                if (list == null || list.isEmpty()) {
                    b(4);
                } else {
                    if (this.y != null) {
                        this.y.setVisibility(8);
                    }
                    this.u.setVisibility(0);
                    if (i == -1) {
                        Toast.makeText(this, getString(R.string.disconnected), 0).show();
                    }
                }
                this.C.setVisibility(0);
            } else if (i2 == -800) {
                b(3);
            } else {
                b(2);
            }
        }
    }

    public void a(int i, int i2) {
        x();
        if (!c.a()) {
            b(3);
        } else {
            b(2);
        }
    }

    public STPageInfo q() {
        this.o.f3358a = STConst.ST_PAGE_GUESS_FAVOR_LOGIN;
        return this.o;
    }
}
