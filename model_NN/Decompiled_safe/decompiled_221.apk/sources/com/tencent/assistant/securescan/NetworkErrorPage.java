package com.tencent.assistant.securescan;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
public class NetworkErrorPage extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private LayoutInflater f2494a;
    private Context b;
    private Button c;
    /* access modifiers changed from: private */
    public View.OnClickListener d;
    private View.OnClickListener e = new a(this);

    public NetworkErrorPage(Context context) {
        super(context);
        this.b = context;
        a(context);
    }

    public NetworkErrorPage(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.b = context;
        a(context);
    }

    private void a(Context context) {
        this.f2494a = LayoutInflater.from(context);
        this.f2494a.inflate((int) R.layout.secure_scan_error_network, this);
        this.c = (Button) findViewById(R.id.setting_network);
        this.c.setTag(R.id.tma_st_slot_tag, "03_001");
        this.c.setOnClickListener(this.e);
    }
}
