package com.tencent.assistant.securescan;

import android.util.Log;
import android.view.animation.Animation;

/* compiled from: ProGuard */
class p implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ScanningAnimView f2514a;

    p(ScanningAnimView scanningAnimView) {
        this.f2514a = scanningAnimView;
    }

    public void onAnimationStart(Animation animation) {
        Log.i("ScanningAnimView", "onAnimationStart:currentViewIndex=" + this.f2514a.q);
        if (this.f2514a.q == 0) {
            long unused = this.f2514a.x = System.currentTimeMillis();
            Log.i("ScanningAnimView", "diff: " + (this.f2514a.x - this.f2514a.w));
        }
        if (this.f2514a.q > 0) {
            this.f2514a.g.setFlipInterval(1500);
            if (this.f2514a.q % 2 == 0) {
                if (this.f2514a.n != null && !this.f2514a.n.isRecycled()) {
                    this.f2514a.j.setImageBitmap(this.f2514a.n);
                }
                int unused2 = this.f2514a.r = 0;
                this.f2514a.v.sendEmptyMessageDelayed(11, 100);
            } else {
                if (this.f2514a.n != null && !this.f2514a.n.isRecycled()) {
                    this.f2514a.k.setImageBitmap(this.f2514a.n);
                }
                int unused3 = this.f2514a.r = 0;
                this.f2514a.v.sendEmptyMessageDelayed(11, 100);
            }
        }
        ScanningAnimView.o(this.f2514a);
    }

    public void onAnimationRepeat(Animation animation) {
        Log.i("ScanningAnimView", "onAnimationRepeat");
    }

    public void onAnimationEnd(Animation animation) {
        Log.i("ScanningAnimView", "onAnimationEnd");
    }
}
