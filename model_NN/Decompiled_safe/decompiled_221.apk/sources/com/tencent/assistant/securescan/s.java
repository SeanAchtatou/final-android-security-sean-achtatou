package com.tencent.assistant.securescan;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.br;

/* compiled from: ProGuard */
class s extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ StartScanActivity f2517a;

    s(StartScanActivity startScanActivity) {
        this.f2517a = startScanActivity;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                LocalApkInfo localApkInfo = (LocalApkInfo) message.obj;
                if (this.f2517a.P != null && this.f2517a.a(localApkInfo).booleanValue() && this.f2517a.P.n(localApkInfo.mPackageName).booleanValue()) {
                    this.f2517a.P.o(localApkInfo.mPackageName);
                    if (!this.f2517a.P.p(localApkInfo.mPackageName).booleanValue()) {
                        this.f2517a.P.q(localApkInfo.mPackageName);
                        Log.i("StartScanActivity", "install offical PackageName= " + localApkInfo.mPackageName + "from PC");
                    }
                    this.f2517a.P.h(localApkInfo.mPackageName);
                    this.f2517a.b(this.f2517a.P.b());
                    this.f2517a.P.f(localApkInfo.mPackageName);
                    Log.i("StartScanActivity", "install offical PackageName= " + localApkInfo.mPackageName);
                }
                if (localApkInfo != null) {
                    br.b(localApkInfo.mPackageName);
                    br.a(0, StartScanActivity.class);
                    return;
                }
                return;
            case 2:
                LocalApkInfo localApkInfo2 = (LocalApkInfo) message.obj;
                if (this.f2517a.P != null && this.f2517a.P.a() > 0) {
                    if (!this.f2517a.a(localApkInfo2).booleanValue()) {
                        this.f2517a.P.g(localApkInfo2.mPackageName);
                        Log.i("StartScanActivity", "uninstall no-offical PackageName= " + localApkInfo2.mPackageName);
                    } else if (this.f2517a.P.l(localApkInfo2.mPackageName).booleanValue()) {
                        this.f2517a.P.m(localApkInfo2.mPackageName);
                        Message message2 = new Message();
                        message2.obj = localApkInfo2;
                        message2.what = 11;
                        this.f2517a.P.f2503a.sendMessage(message2);
                        this.f2517a.P.e(localApkInfo2.mPackageName);
                        Log.i("StartScanActivity", "uninstall offical PackageName= " + localApkInfo2.mPackageName);
                    } else {
                        if (!this.f2517a.P.n(localApkInfo2.mPackageName).booleanValue()) {
                            this.f2517a.P.g(localApkInfo2.mPackageName);
                        }
                        Log.i("StartScanActivity", "no userAction,uninstall offical PackageName= " + localApkInfo2.mPackageName);
                    }
                }
                if (localApkInfo2 != null) {
                    br.b(localApkInfo2.mPackageName);
                    br.b(0, StartScanActivity.class);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
