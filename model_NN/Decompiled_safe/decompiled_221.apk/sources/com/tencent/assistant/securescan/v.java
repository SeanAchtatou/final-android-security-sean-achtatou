package com.tencent.assistant.securescan;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAClickListener;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
class v extends OnTMAClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ StartScanActivity f2520a;

    v(StartScanActivity startScanActivity) {
        this.f2520a = startScanActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.securescan.StartScanActivity.b(com.tencent.assistant.securescan.StartScanActivity, java.lang.Boolean):java.lang.Boolean
     arg types: [com.tencent.assistant.securescan.StartScanActivity, int]
     candidates:
      com.tencent.assistant.securescan.StartScanActivity.b(com.tencent.assistant.securescan.StartScanActivity, int):int
      com.tencent.assistant.securescan.StartScanActivity.b(com.tencent.assistant.securescan.StartScanActivity, java.lang.Boolean):java.lang.Boolean */
    public void onTMAClick(View view) {
        this.f2520a.A.setVisibility(0);
        this.f2520a.x.j();
        this.f2520a.C.setVisibility(8);
        this.f2520a.H.startScanAnim();
        Boolean unused = this.f2520a.S = (Boolean) true;
        TemporaryThreadManager.get().start(new w(this));
    }
}
