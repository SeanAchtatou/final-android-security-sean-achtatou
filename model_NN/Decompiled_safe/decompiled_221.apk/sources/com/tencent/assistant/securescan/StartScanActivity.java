package com.tencent.assistant.securescan;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.ViewStub;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.Global;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.txscrollview.TXExpandableListView;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.m;
import com.tencent.assistant.module.ct;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.protocol.jce.AppSimpleDetail;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.br;
import com.tencent.assistant.utils.installuninstall.InstallUninstallTaskBean;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.activity.AssistantTabActivity;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.beacon.event.a;
import com.tencent.connect.common.Constants;
import com.tencent.securemodule.impl.AppInfo;
import com.tencent.securemodule.impl.SecureModuleService;
import com.tencent.securemodule.service.ISecureModuleService;
import com.tencent.securemodule.service.ProductInfo;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;

/* compiled from: ProGuard */
public class StartScanActivity extends BaseActivity implements UIEventListener, m {
    /* access modifiers changed from: private */
    public RelativeLayout A;
    /* access modifiers changed from: private */
    public y B;
    /* access modifiers changed from: private */
    public NormalErrorRecommendPage C;
    private NoRiskFoundPage D;
    private ViewStub E = null;
    private ViewStub F = null;
    private ViewStub G = null;
    /* access modifiers changed from: private */
    public ScanningAnimView H;
    /* access modifiers changed from: private */
    public List<AppInfo> I;
    private ArrayList<SimpleAppModel> J;
    /* access modifiers changed from: private */
    public List<SimpleAppModel> K;
    private ApkResourceManager L = ApkResourceManager.getInstance();
    private aa M;
    /* access modifiers changed from: private */
    public ct N;
    private TXExpandableListView O;
    /* access modifiers changed from: private */
    public e P;
    private AstApp Q;
    /* access modifiers changed from: private */
    public Boolean R = false;
    /* access modifiers changed from: private */
    public Boolean S = false;
    private Boolean T = true;
    private Animation U;
    /* access modifiers changed from: private */
    public int V = -1;
    private int W = 2000;
    /* access modifiers changed from: private */
    public long X = 0;
    /* access modifiers changed from: private */
    public long Y = 0;
    /* access modifiers changed from: private */
    public Handler Z = new q(this);
    private boolean aa = false;
    /* access modifiers changed from: private */
    public Handler ab = new s(this);
    /* access modifiers changed from: private */
    public ab n;
    /* access modifiers changed from: private */
    public int t = -1;
    private int u = -1;
    /* access modifiers changed from: private */
    public ISecureModuleService v;
    /* access modifiers changed from: private */
    public Context w;
    /* access modifiers changed from: private */
    public SecondNavigationTitleViewV5 x;
    private FrameLayout y;
    private TextView z;

    /* access modifiers changed from: private */
    public void x() {
        try {
            new Timer().schedule(this.n, 30000);
        } catch (Exception e) {
            Log.i("StartScanActivity", e.getMessage());
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            setContentView((int) R.layout.activity_startscan);
            this.w = this;
            this.W = getIntent().getIntExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, 2000);
            z();
            A();
            if (a(this.w)) {
                this.S = true;
                TemporaryThreadManager.get().start(new r(this));
            } else {
                c(30);
            }
            y();
        } catch (Throwable th) {
            this.aa = true;
            cq.a().b();
            finish();
        }
    }

    private void y() {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getPhoneGuidAndGen());
        hashMap.put("B2", Global.getQUAForBeacon());
        hashMap.put("B3", t.g());
        XLog.d("beacon", "beacon report >> expose_securescan. " + hashMap.toString());
        a.a("expose_securescan", true, -1, -1, hashMap, true);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.aa) {
            if (this.S.booleanValue()) {
                this.H.startScanAnim();
            }
            if (this.x != null) {
                this.x.l();
            }
            this.Q.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_START, this);
            this.Q.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_FAIL, this);
            this.Q.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_SUCC, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        }
    }

    private void z() {
        this.I = new ArrayList();
        this.J = new ArrayList<>();
        this.K = new ArrayList();
        this.P = new e(this.w, this.I, this.J, this.K, this.O);
        this.B = new y(this, null);
        this.N = new ct();
        this.v = SecureModuleService.getInstance(getApplicationContext());
        this.M = new aa(this, null);
        this.L.registerApkResCallback(this.M);
        this.N.register(this);
        this.Q = AstApp.i();
        this.n = new ab(this);
    }

    private void A() {
        this.x = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.x.a(this);
        this.x.b(getString(R.string.secure_scan));
        this.x.d();
        this.x.c(new t(this));
        this.U = AnimationUtils.loadAnimation(this, R.anim.safescan_fade_out);
        this.U.setAnimationListener(new z(this, null));
        this.H = (ScanningAnimView) findViewById(R.id.scan_anim_content);
        this.A = (RelativeLayout) findViewById(R.id.scanning_layout);
        this.y = (FrameLayout) findViewById(R.id.header_view);
        this.y.setVisibility(8);
        this.z = (TextView) findViewById(R.id.result_text);
        this.E = (ViewStub) findViewById(R.id.group_list_stub);
        this.G = (ViewStub) findViewById(R.id.nofound_stub);
        this.F = (ViewStub) findViewById(R.id.error_stub);
    }

    public static boolean a(Context context) {
        NetworkInfo activeNetworkInfo;
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null || !activeNetworkInfo.isConnected() || activeNetworkInfo.getState() != NetworkInfo.State.CONNECTED) {
                return false;
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void i() {
        String str;
        String channelId = Global.getChannelId();
        int parseInt = Integer.parseInt(Global.getBuildNo());
        try {
            str = AstApp.i().getPackageManager().getPackageInfo(AstApp.i().getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            str = null;
        }
        Log.i("StartScanActivity", "channelId:" + channelId + " versionName:" + str + " buildNo:" + parseInt);
        if (this.v.register(new ProductInfo(41, str, parseInt, 0, channelId, null)) == 0) {
            this.v.registerCloudScanListener(this, this.B);
            this.v.setNotificationUIEnable(false);
            this.X = System.currentTimeMillis();
            this.v.cloudScan();
            Log.i("StartScanActivity", "startScan");
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        Log.i("StartScanActivity", "onPause");
        super.onPause();
        if (!this.aa) {
            this.H.stopScanAnim();
            if (this.x != null) {
                this.x.m();
            }
        }
    }

    public void onDestroy() {
        super.onDestroy();
        Log.i("StartScanActivity", "onDestroy---this = " + this);
        if (!this.aa) {
            if (this.n != null) {
                this.n.cancel();
            }
            if (this.v != null) {
                this.v.unregisterCloudScanListener(this, this.B);
            }
            if (this.N != null) {
                this.N.unregister(this);
            }
            if (this.H != null) {
                this.H.destroy();
            }
            ApkResourceManager.getInstance().unRegisterApkResCallback(this.M);
            this.Q.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_START, this);
            this.Q.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_FAIL, this);
            this.Q.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_SUCC, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (4 != i) {
            return super.onKeyDown(i, keyEvent);
        }
        B();
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void B() {
        if (!this.r) {
            finish();
            return;
        }
        Intent intent = new Intent(this, AssistantTabActivity.class);
        intent.setFlags(67108864);
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, f());
        intent.putExtra(com.tencent.assistant.b.a.G, true);
        startActivity(intent);
        this.r = false;
        finish();
        XLog.i("StartScanActivity", "StartScanActivity >> key back finish");
    }

    /* access modifiers changed from: private */
    public void C() {
        this.P.a(this.I, this.J, this.K);
        D();
        if (this.O == null) {
            this.E.inflate();
            this.O = (TXExpandableListView) findViewById(R.id.group_list);
            this.O.setGroupIndicator(null);
            this.O.setAdapter(this.P);
            this.O.setDivider(null);
            this.O.setOnGroupClickListener(new u(this));
            this.O.setVisibility(8);
        }
        for (int i = 0; i < this.P.getGroupCount(); i++) {
            this.O.expandGroup(i);
        }
        this.P.notifyDataSetChanged();
        this.O.setVisibility(0);
        this.y.setVisibility(0);
    }

    private void D() {
        if (this.I.size() > 0) {
            this.z.setText(Html.fromHtml("<html >扫描出" + this.P.b() + "个风险应用</html>"));
            this.z.setTag(Integer.valueOf((int) R.id.result_text));
        }
    }

    /* access modifiers changed from: protected */
    public void b(int i) {
        if (i >= 0) {
            this.z.setText(Html.fromHtml("<html >扫描出" + i + "个风险应用</html>"));
        }
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE:
                if (message.obj != null && (message.obj instanceof DownloadInfo) && this.T.booleanValue()) {
                    DownloadInfo downloadInfo = (DownloadInfo) message.obj;
                    switch (x.f2522a[downloadInfo.downloadState.ordinal()]) {
                        case 1:
                            break;
                        case 2:
                            if (this.P != null) {
                                if (!this.P.j(downloadInfo.packageName).booleanValue() || this.P.k(downloadInfo.packageName) != 10) {
                                    this.P.i(downloadInfo.packageName);
                                    this.P.g(downloadInfo.packageName);
                                    Log.i("StartScanActivity", "delete item from UI_EVENT_APP_DOWNLOAD_DELETE");
                                    break;
                                } else {
                                    return;
                                }
                            }
                            break;
                        default:
                            if (this.P != null) {
                                if (!this.P.j(downloadInfo.packageName).booleanValue() || this.P.k(downloadInfo.packageName) != 10) {
                                    this.P.g(downloadInfo.packageName);
                                    Log.i("StartScanActivity", "delete item from UI_EVENT_APP_DOWNLOAD_DELETE");
                                    break;
                                } else {
                                    return;
                                }
                            }
                            break;
                    }
                }
                a((Boolean) true);
                return;
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_START:
                if (message.obj != null && (message.obj instanceof InstallUninstallTaskBean)) {
                    InstallUninstallTaskBean installUninstallTaskBean = (InstallUninstallTaskBean) message.obj;
                    if (this.P == null) {
                        return;
                    }
                    if (a(installUninstallTaskBean).booleanValue()) {
                        this.P.d(installUninstallTaskBean.packageName);
                        return;
                    } else {
                        this.P.c(installUninstallTaskBean.packageName);
                        return;
                    }
                } else {
                    return;
                }
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_SUCC:
                if (message.obj instanceof InstallUninstallTaskBean) {
                    br.b(((InstallUninstallTaskBean) message.obj).packageName);
                    br.b(0, StartScanActivity.class);
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_FAIL:
                if (message.obj != null && (message.obj instanceof InstallUninstallTaskBean)) {
                    InstallUninstallTaskBean installUninstallTaskBean2 = (InstallUninstallTaskBean) message.obj;
                    if (this.P == null) {
                        return;
                    }
                    if (a(installUninstallTaskBean2).booleanValue()) {
                        this.P.a(installUninstallTaskBean2.packageName);
                        return;
                    } else {
                        this.P.b(installUninstallTaskBean2.packageName);
                        return;
                    }
                } else {
                    return;
                }
            default:
                return;
        }
    }

    private Boolean a(InstallUninstallTaskBean installUninstallTaskBean) {
        if (installUninstallTaskBean != null) {
            Iterator<SimpleAppModel> it = this.J.iterator();
            while (it.hasNext()) {
                SimpleAppModel next = it.next();
                if (next != null && next.c.equals(installUninstallTaskBean.packageName)) {
                    return true;
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public Boolean a(LocalApkInfo localApkInfo) {
        if (localApkInfo != null) {
            Iterator<SimpleAppModel> it = this.J.iterator();
            while (it.hasNext()) {
                SimpleAppModel next = it.next();
                if (next != null && next.c.equals(localApkInfo.mPackageName)) {
                    return true;
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void j() {
        try {
            if (this.D == null) {
                this.G.inflate();
                this.D = (NoRiskFoundPage) findViewById(R.id.nofound_page);
            }
            if (this.A != null && this.A.getVisibility() == 0) {
                this.A.setVisibility(8);
            }
            if (this.y != null && this.y.getVisibility() == 0) {
                this.y.setVisibility(8);
            }
            if (this.C != null && this.C.getVisibility() == 0) {
                this.C.setVisibility(8);
            }
            this.D.setVisibility(0);
            this.u = 11;
            w();
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void c(int i) {
        if (this.C == null) {
            this.F.inflate();
            this.C = (NormalErrorRecommendPage) findViewById(R.id.error_page);
        }
        if (this.A != null && this.A.getVisibility() == 0) {
            this.A.setVisibility(8);
        }
        if (this.D != null && this.D.getVisibility() == 0) {
            this.D.setVisibility(8);
        }
        if (this.y != null && this.y.getVisibility() == 0) {
            this.y.setVisibility(8);
        }
        this.C.setErrorType(i);
        this.C.setVisibility(0);
        if (i == 30) {
            this.C.setErrorText(this.w.getResources().getString(R.string.secure_scan_error_page_tip));
        }
        this.C.setButtonClickListener(new v(this));
        this.u = 10;
        w();
    }

    /* access modifiers changed from: protected */
    public void v() {
        d(12);
        this.u = 12;
        this.x.i();
        w();
    }

    /* access modifiers changed from: private */
    public void d(int i) {
        this.t = i;
        this.A.startAnimation(this.U);
    }

    /* access modifiers changed from: protected */
    public void a(Boolean bool) {
        synchronized (this.T) {
            this.T = bool;
        }
    }

    public int f() {
        switch (this.u) {
            case 10:
                return STConst.ST_PAGE_ERROR_NETWORK_SAFE_SCAN;
            case 11:
                return STConst.ST_PAGE_EMPTY_SAFE_SCAN;
            case 12:
                return STConst.ST_PAGE_RESULT_SAFE_SCAN;
            default:
                return STConst.ST_PAGE_SAFE_SCAN;
        }
    }

    public void w() {
        STInfoV2 sTInfoV2 = new STInfoV2(f(), STConst.ST_DEFAULT_SLOT, this.W, STConst.ST_DEFAULT_SLOT, 100);
        if (sTInfoV2 != null) {
            k.a(sTInfoV2);
        }
    }

    public void a(int i, int i2, List<AppSimpleDetail> list) {
        Log.i("StartScanActivity", "onGetBatchAppInfoSuccess:batchRequestId=" + this.V + "seq=" + i);
        this.n.cancel();
        if (this.V == i) {
            if (list == null || list.size() <= 0) {
            }
            v();
        }
    }

    public void a(int i, int i2) {
        this.n.cancel();
        Log.i("StartScanActivity", "onGetAppInfoFail:batchRequestId=" + this.V + "seq=" + i);
        if (this.V == i) {
            d(10);
        }
    }

    /* access modifiers changed from: private */
    public void a(long j, List<AppInfo> list) {
        int i;
        HashMap hashMap = new HashMap();
        StringBuilder sb = new StringBuilder();
        if (list != null) {
            int size = list.size();
            for (AppInfo appInfo : list) {
                sb.append(appInfo.softName).append(";");
            }
            i = size;
        } else {
            i = 0;
        }
        hashMap.put("B1", i + Constants.STR_EMPTY);
        hashMap.put("B2", Global.getPhoneGuidAndGen());
        hashMap.put("B3", Global.getQUAForBeacon());
        hashMap.put("B4", t.g());
        if (list != null) {
            hashMap.put("B5", sb.toString());
        }
        a.a("VirusScan", true, j, -1, hashMap, false);
        XLog.d("beacon", "beacon report >> event: VirusScan, totalTime : " + j + ", params : " + hashMap.toString());
    }
}
