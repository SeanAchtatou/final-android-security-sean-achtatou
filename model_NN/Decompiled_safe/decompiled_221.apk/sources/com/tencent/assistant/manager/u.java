package com.tencent.assistant.manager;

import android.text.TextUtils;
import com.tencent.assistant.m;
import com.tencent.assistant.protocol.jce.AutoDownloadPushCfg;
import com.tencent.assistant.protocol.jce.CommonCfg;
import com.tencent.assistant.protocol.jce.CommonCfgItem;
import com.tencent.assistant.protocol.jce.DownloadCfg;
import com.tencent.assistant.protocol.jce.DownloadCheckCfg;
import com.tencent.assistant.protocol.jce.ExternalCallYYBCfg;
import com.tencent.assistant.protocol.jce.FloatWindowCfg;
import com.tencent.assistant.protocol.jce.PushMsgCfg;
import com.tencent.assistant.protocol.jce.StatCfg;
import com.tencent.assistant.protocol.jce.TempRootCfg;
import com.tencent.assistant.protocol.jce.TimerCfg;
import com.tencent.assistant.protocol.jce.WebviewCfg;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bh;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;

/* compiled from: ProGuard */
public class u {

    /* renamed from: a  reason: collision with root package name */
    private static u f1623a;
    private ReferenceQueue<v> b = new ReferenceQueue<>();
    private ConcurrentLinkedQueue<WeakReference<v>> c = new ConcurrentLinkedQueue<>();

    private u() {
    }

    public static synchronized u a() {
        u uVar;
        synchronized (u.class) {
            if (f1623a == null) {
                f1623a = new u();
            }
            uVar = f1623a;
        }
        return uVar;
    }

    public void a(v vVar) {
        if (vVar != null) {
            while (true) {
                Reference<? extends v> poll = this.b.poll();
                if (poll == null) {
                    break;
                }
                this.c.remove(poll);
            }
            Iterator<WeakReference<v>> it = this.c.iterator();
            while (it.hasNext()) {
                if (((v) it.next().get()) == vVar) {
                    return;
                }
            }
            this.c.add(new WeakReference(vVar, this.b));
        }
    }

    public void a(TimerCfg timerCfg, HashMap<String, Object> hashMap) {
        if (timerCfg != null) {
            int a2 = m.a().a("union_update_interval", -1);
            byte a3 = m.a().a("union_update_retry", (byte) -1);
            if (a2 != timerCfg.b) {
                hashMap.put("union_update_interval", Integer.valueOf(timerCfg.b));
                m.a().b("union_update_interval", Integer.valueOf(timerCfg.b));
            }
            if (a3 != timerCfg.c) {
                hashMap.put("union_update_retry", Byte.valueOf(timerCfg.c));
                m.a().b("union_update_retry", Byte.valueOf(timerCfg.c));
            }
        }
    }

    public void b(TimerCfg timerCfg, HashMap<String, Object> hashMap) {
        if (timerCfg != null && m.a().a("home_page_interval", -1) != timerCfg.b) {
            hashMap.put("home_page_interval", Integer.valueOf(timerCfg.b));
            m.a().b("home_page_interval", Integer.valueOf(timerCfg.b));
        }
    }

    public void a(int i, HashMap<String, Object> hashMap) {
        if (i > 0 && m.a().a("key_wifi_auto_download_install_task_screenon", -1) != i) {
            hashMap.put("key_wifi_auto_download_install_task_screenon", Integer.valueOf(i));
            m.a().b("key_wifi_auto_download_install_task_screenon", Integer.valueOf(i));
        }
    }

    public void b(int i, HashMap<String, Object> hashMap) {
        if (i > 0 && m.a().a("key_wifi_auto_download_install_task_screenoff", -1) != i) {
            hashMap.put("key_wifi_auto_download_install_task_screenoff", Integer.valueOf(i));
            m.a().b("key_wifi_auto_download_install_task_screenoff", Integer.valueOf(i));
        }
    }

    public void c(TimerCfg timerCfg, HashMap<String, Object> hashMap) {
        if (timerCfg != null && ((long) m.a().ae()) != ((long) timerCfg.b)) {
            hashMap.put("key_system_install_result_check_interval", Integer.valueOf(timerCfg.b));
            m.a().g(timerCfg.b);
        }
    }

    public void d(TimerCfg timerCfg, HashMap<String, Object> hashMap) {
        if (timerCfg != null) {
            int a2 = m.a().a("setting_sync_interval", -1);
            byte a3 = m.a().a("setting_sync_retry", (byte) -1);
            if (a2 != timerCfg.b) {
                hashMap.put("setting_sync_interval", Integer.valueOf(timerCfg.b));
                m.a().b("setting_sync_interval", Integer.valueOf(timerCfg.b));
            }
            if (a3 != timerCfg.c) {
                hashMap.put("setting_sync_retry", Byte.valueOf(timerCfg.c));
                m.a().b("setting_sync_retry", Byte.valueOf(timerCfg.c));
            }
        }
    }

    public void e(TimerCfg timerCfg, HashMap<String, Object> hashMap) {
        if (timerCfg != null) {
            int a2 = m.a().a("app_update_check_interval", -1);
            byte a3 = m.a().a("app_update_check_retry", (byte) -1);
            if (a2 != timerCfg.b) {
                hashMap.put("app_update_check_interval", Integer.valueOf(timerCfg.b));
                m.a().b("app_update_check_interval", Integer.valueOf(timerCfg.b));
            }
            if (a3 != timerCfg.c) {
                hashMap.put("app_update_check_retry", Byte.valueOf(timerCfg.c));
                m.a().b("app_update_check_retry", Byte.valueOf(timerCfg.c));
            }
        }
    }

    public void f(TimerCfg timerCfg, HashMap<String, Object> hashMap) {
        if (timerCfg != null) {
            int a2 = m.a().a("self_update_check_interval", -1);
            byte a3 = m.a().a("self_update_check_retry", (byte) -1);
            if (a2 != timerCfg.b) {
                hashMap.put("self_update_check_interval", Integer.valueOf(timerCfg.b));
                m.a().b("self_update_check_interval", Integer.valueOf(timerCfg.b));
            }
            if (a3 != timerCfg.c) {
                hashMap.put("self_update_check_retry", Byte.valueOf(timerCfg.c));
                m.a().b("self_update_check_retry", Byte.valueOf(timerCfg.c));
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(byte, int):void
     arg types: [byte, short]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(byte, int):void */
    public void a(StatCfg statCfg, HashMap<String, Object> hashMap) {
        if (statCfg != null) {
            byte b2 = statCfg.f2356a;
            if (statCfg.b != m.a().c(b2)) {
                hashMap.put("ST_netType_" + ((int) b2), Byte.valueOf(statCfg.b));
                m.a().a(b2, statCfg.b);
            }
            if (statCfg.c != m.a().b(b2)) {
                hashMap.put("ST_size_" + ((int) b2), Short.valueOf(statCfg.c));
                m.a().a(b2, (int) statCfg.c);
            }
            if (statCfg.d != m.a().w()) {
                hashMap.put("ST_interval", Integer.valueOf(statCfg.d));
                m.a().c(statCfg.d);
            }
        }
    }

    public void a(DownloadCfg downloadCfg, HashMap<String, Object> hashMap) {
        if (downloadCfg != null) {
            m.a().b(downloadCfg.f2056a, downloadCfg.b);
            m.a().d(downloadCfg.f2056a, downloadCfg.c);
            m.a().c(downloadCfg.f2056a, downloadCfg.d);
        }
    }

    public void a(HashMap<String, Object> hashMap) {
        Iterator<WeakReference<v>> it = this.c.iterator();
        while (it.hasNext()) {
            v vVar = (v) it.next().get();
            if (vVar != null) {
                vVar.a(hashMap);
            }
        }
    }

    public void a(WebviewCfg webviewCfg, HashMap<String, Object> hashMap) {
        if (webviewCfg != null) {
            m.a().b(webviewCfg.f2423a);
            if (webviewCfg.b != null && webviewCfg.b.size() > 0) {
                String str = webviewCfg.b.get(0);
                String str2 = webviewCfg.b.get(1);
                if (!TextUtils.isEmpty(str)) {
                    m.a().a((byte) 0, str);
                }
                if (!TextUtils.isEmpty(str2)) {
                    m.a().a((byte) 1, str2);
                }
            }
            if (webviewCfg.c != null && webviewCfg.c.size() > 0) {
                String str3 = webviewCfg.c.get(0);
                String str4 = webviewCfg.c.get(1);
                if (!TextUtils.isEmpty(str3)) {
                    m.a().b((byte) 0, str3);
                }
                if (!TextUtils.isEmpty(str4)) {
                    m.a().b((byte) 1, str4);
                }
            }
            hashMap.put("key_webview_config_json", webviewCfg.f2423a);
        }
    }

    public void a(byte[] bArr, HashMap<String, Object> hashMap) {
        m.a().a(bArr);
    }

    public void b(byte[] bArr, HashMap<String, Object> hashMap) {
        m.a().b(bArr);
    }

    public void a(byte[] bArr) {
        m.a().c(bArr);
    }

    public void a(DownloadCheckCfg downloadCheckCfg, HashMap<String, Object> hashMap) {
        if (downloadCheckCfg != null) {
            m.a().u(downloadCheckCfg.f2057a);
            m.a().v(downloadCheckCfg.b);
        }
    }

    public void b(byte[] bArr) {
        XLog.i("BackgroundScan", "<settings> updateLocalManagePushCfg !");
        if (bArr != null) {
            PushMsgCfg pushMsgCfg = (PushMsgCfg) bh.b(bArr, PushMsgCfg.class);
            if (pushMsgCfg.b != null && pushMsgCfg.b.size() > 0) {
                byte[] J = m.a().J();
                if (J == null || !pushMsgCfg.equals((PushMsgCfg) bh.b(J, PushMsgCfg.class))) {
                    m.a().e(bArr);
                    m.a().p(true);
                    XLog.d("BackgroundScan", "<settings> update local background scan settings successful !!!");
                    return;
                }
                XLog.d("BackgroundScan", "<settings> not change, no need to update local background scan settings !");
                return;
            }
        }
        XLog.e("BackgroundScan", "<settings> settings data from server is empty !!!");
    }

    public void a(TempRootCfg tempRootCfg) {
        XLog.i("TempRoot", "<settings> updateTempRootCfg !");
        if (tempRootCfg != null) {
            m.a().b("key_temp_root_unlock_period", Long.valueOf(tempRootCfg.f2390a));
            XLog.d("TempRoot", "<settings> tempRootCfg.unLockTimeGap = " + tempRootCfg.f2390a);
            XLog.d("TempRoot", "<settings> update temp root settings successful !!!");
        }
    }

    public void a(ExternalCallYYBCfg externalCallYYBCfg) {
        XLog.i("ExternalCallYYBCfg", "<settings> updateExternalCallYYBCfg !");
        if (externalCallYYBCfg != null) {
            m.a().b("key_external_call_yyb_flag", Integer.valueOf(externalCallYYBCfg.f2068a));
            m.a().b("key_external_call_yyb_url", externalCallYYBCfg.b);
            XLog.d("ExternalCallYYBCfg", "<settings> callCfg.flag = " + externalCallYYBCfg.f2068a + ",callCfg.url = " + externalCallYYBCfg.b);
        }
    }

    public void a(FloatWindowCfg floatWindowCfg) {
        boolean z = true;
        XLog.i("floatingwindow", "<settings> updateFloatWindowCfg !");
        if (floatWindowCfg != null) {
            m a2 = m.a();
            if (floatWindowCfg.f2072a != 1) {
                z = false;
            }
            a2.b("key_float_window_should_auto_show", Boolean.valueOf(z));
            XLog.d("floatingwindow", "<settings> floatWindowCfg.isOpen = " + floatWindowCfg.f2072a);
            XLog.d("floatingwindow", "<settings> update float window settings successful !!!");
        }
    }

    public void a(CommonCfg commonCfg, HashMap<String, Object> hashMap) {
        if (commonCfg != null && commonCfg.a() != null) {
            Iterator<CommonCfgItem> it = commonCfg.a().iterator();
            while (it.hasNext()) {
                CommonCfgItem next = it.next();
                if (next.a() == 2) {
                    m.a().h(next.b);
                }
            }
        }
    }

    public void a(AutoDownloadPushCfg autoDownloadPushCfg) {
        if (autoDownloadPushCfg != null) {
            m.a().b("key_auto_download_push_max_count_per_day", Integer.valueOf(autoDownloadPushCfg.b));
            m.a().b("key_auto_download_push_max_count_per_week", Integer.valueOf(autoDownloadPushCfg.f2014a));
        }
    }

    public void c(byte[] bArr) {
        if (bArr != null && bArr.length > 0) {
            m.a().d(bArr);
        }
    }

    public void a(Map<String, String> map, HashMap<String, Object> hashMap) {
        String a2;
        if (map != null) {
            for (Map.Entry next : map.entrySet()) {
                if (next.getValue() != null && ((a2 = m.a().a((String) next.getKey(), (Object) null)) == null || !a2.equals(next.getValue()))) {
                    String str = "exs_" + ((String) next.getKey());
                    hashMap.put(str, next.getValue());
                    m.a().b(str, next.getValue());
                }
            }
        }
    }

    public int a(String str, int i) {
        return m.a().a("exs_" + str, i);
    }
}
