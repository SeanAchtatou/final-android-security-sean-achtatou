package com.tencent.assistant.manager.notification;

import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;

/* compiled from: ProGuard */
class i extends ApkResCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ h f1568a;

    i(h hVar) {
        this.f1568a = hVar;
    }

    public void onInstalledApkDataChanged(LocalApkInfo localApkInfo, int i) {
        n a2;
        if (localApkInfo != null && i == 2 && this.f1568a.b != null && !this.f1568a.b.b() && (a2 = this.f1568a.b.a(localApkInfo.mPackageName)) != null) {
            v.a().a(a2.a());
        }
    }
}
