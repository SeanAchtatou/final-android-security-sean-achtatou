package com.tencent.assistant.manager;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.os.Message;
import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.t;
import com.tencent.assistant.protocol.jce.VerifyInfo;
import com.tencent.assistant.utils.e;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class n implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    public final byte f1548a = 1;
    public final byte b = 2;
    private bc c;
    private ct d;
    private SimpleAppModel e;
    private int f = -1;
    private int g = -1;
    private AstApp h;

    public n() {
        a();
    }

    private void a() {
        this.h = AstApp.i();
        this.h.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_WX_AUTH_SUCCESS, this);
        this.h.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_WX_AUTH_FAIL, this);
        this.h.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_WX_WRITE_TOKEN_SUCCESS, this);
        this.h.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_WX_WRITE_TOKEN_FAIL, this);
        this.h.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_WX_UNINSTALLED, this);
        this.h.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_QQ_AUTH_FAIL, this);
        this.h.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_QQ_AUTH_SUCCESS, this);
        this.h.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_QQ_WRITE_AUTH_SUCCESS, this);
        this.h.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_QQ_WRITE_AUTH_FAIL, this);
        this.h.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_QQ_AUTH_CANCEL, this);
    }

    public int a(int i, SimpleAppModel simpleAppModel, Activity activity) {
        if (simpleAppModel == null || activity == null) {
            return -1;
        }
        if (simpleAppModel.Q != 2 && simpleAppModel.Q != 1) {
            return -1;
        }
        this.e = simpleAppModel;
        if (i == 1) {
            if (this.d == null) {
                this.d = ct.b();
            }
            this.f = this.d.a(activity);
            this.g = this.f;
            return this.g;
        } else if (i != 2 || simpleAppModel.Q != 2) {
            return -1;
        } else {
            if (this.c == null) {
                this.c = bc.b();
                this.c.c();
            }
            this.f = this.c.a(activity);
            this.g = this.f;
            return this.g;
        }
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_WX_AUTH_SUCCESS:
                if (this.f == message.arg2) {
                    this.f = -1;
                    b(Constants.STR_EMPTY, (byte) 1, (byte) 1);
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_WX_AUTH_FAIL:
                if (this.f == message.arg2) {
                    this.f = -1;
                    b(Constants.STR_EMPTY, (byte) 0, (byte) 1);
                    Message obtainMessage = AstApp.i().j().obtainMessage();
                    obtainMessage.what = EventDispatcherEnum.UI_EVENT_WX_AUTH_FAIL_FOR_WEBVIEW;
                    obtainMessage.arg1 = 5;
                    obtainMessage.arg2 = this.g;
                    obtainMessage.obj = this.e;
                    this.h.j().sendMessage(obtainMessage);
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_WX_AUTH_CANCEL:
            case EventDispatcherEnum.UI_EVENT_QQ_AUTH_CANCEL:
                if (this.f == message.arg2) {
                    this.f = -1;
                    Message obtainMessage2 = AstApp.i().j().obtainMessage();
                    obtainMessage2.what = EventDispatcherEnum.UI_EVENT_QQ_AUTH_FAIL_FOR_WEBVIEW;
                    obtainMessage2.arg1 = 6;
                    obtainMessage2.arg2 = this.g;
                    obtainMessage2.obj = this.e;
                    this.h.j().sendMessage(obtainMessage2);
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_WX_WRITE_TOKEN_SUCCESS:
                if (this.f == message.arg2) {
                    this.f = -1;
                    int i = message.arg1;
                    t tVar = (t) message.obj;
                    if (tVar != null && tVar.f1676a != null && tVar.f1676a.i == 1) {
                        Message obtainMessage3 = AstApp.i().j().obtainMessage();
                        if (i == 0) {
                            obtainMessage3.what = EventDispatcherEnum.UI_EVENT_WX_AUTH_SUCCESS_FOR_WEBVIEW;
                        } else {
                            obtainMessage3.what = EventDispatcherEnum.UI_EVENT_WX_AUTH_FAIL_FOR_WEBVIEW;
                        }
                        obtainMessage3.arg1 = i;
                        obtainMessage3.arg2 = this.g;
                        obtainMessage3.obj = this.e;
                        this.h.j().sendMessage(obtainMessage3);
                        return;
                    }
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_WX_WRITE_TOKEN_FAIL:
                if (this.f == message.arg2) {
                    this.f = -1;
                    t tVar2 = (t) message.obj;
                    if (tVar2 != null && tVar2.f1676a != null && tVar2.f1676a.i == 1) {
                        Message obtainMessage4 = AstApp.i().j().obtainMessage();
                        obtainMessage4.what = EventDispatcherEnum.UI_EVENT_WX_AUTH_FAIL_FOR_WEBVIEW;
                        obtainMessage4.arg1 = 7;
                        obtainMessage4.arg2 = this.g;
                        obtainMessage4.obj = this.e;
                        this.h.j().sendMessage(obtainMessage4);
                        return;
                    }
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_WX_UNINSTALLED:
                Message obtainMessage5 = AstApp.i().j().obtainMessage();
                obtainMessage5.what = EventDispatcherEnum.UI_EVENT_WX_AUTH_FAIL_FOR_WEBVIEW;
                obtainMessage5.arg2 = this.g;
                obtainMessage5.obj = this.e;
                this.h.j().sendMessage(obtainMessage5);
                return;
            case EventDispatcherEnum.UI_EVENT_QQ_AUTH_SUCCESS:
                if (this.f == message.arg2) {
                    this.f = -1;
                    this.f = a(((Bundle) message.obj).getString(bc.f), (byte) 1, (byte) 4);
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_QQ_AUTH_FAIL:
                if (this.f == message.arg2) {
                    this.f = -1;
                    this.f = a(Constants.STR_EMPTY, (byte) 1, (byte) 4);
                    Message obtainMessage6 = AstApp.i().j().obtainMessage();
                    obtainMessage6.what = EventDispatcherEnum.UI_EVENT_QQ_AUTH_FAIL_FOR_WEBVIEW;
                    obtainMessage6.arg1 = 5;
                    obtainMessage6.arg2 = this.g;
                    obtainMessage6.obj = this.e;
                    this.h.j().sendMessage(obtainMessage6);
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_QQ_WRITE_AUTH_SUCCESS:
                if (this.f == message.arg2) {
                    this.f = -1;
                    int i2 = message.arg1;
                    if (((t) message.obj).f1676a.i == 1) {
                        Message obtainMessage7 = AstApp.i().j().obtainMessage();
                        if (i2 == 0) {
                            obtainMessage7.what = EventDispatcherEnum.UI_EVENT_QQ_AUTH_SUCCESS_FOR_WEBVIEW;
                        } else {
                            obtainMessage7.what = EventDispatcherEnum.UI_EVENT_QQ_AUTH_FAIL_FOR_WEBVIEW;
                        }
                        obtainMessage7.arg1 = i2;
                        obtainMessage7.arg2 = this.g;
                        obtainMessage7.obj = this.e;
                        this.h.j().sendMessage(obtainMessage7);
                        return;
                    }
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_QQ_WRITE_AUTH_FAIL:
                if (this.f == message.arg2) {
                    this.f = -1;
                    if (((t) message.obj).f1676a.i == 1) {
                        Message obtainMessage8 = AstApp.i().j().obtainMessage();
                        obtainMessage8.what = EventDispatcherEnum.UI_EVENT_QQ_AUTH_FAIL_FOR_WEBVIEW;
                        obtainMessage8.arg1 = 7;
                        obtainMessage8.arg2 = this.g;
                        obtainMessage8.obj = this.e;
                        this.h.j().sendMessage(obtainMessage8);
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void b(String str, byte b2, byte b3) {
        if (str == null) {
            str = Constants.STR_EMPTY;
        }
        VerifyInfo verifyInfo = new VerifyInfo();
        verifyInfo.f2419a = this.e.f1634a;
        verifyInfo.b = this.e.b;
        verifyInfo.c = this.e.c;
        verifyInfo.g = str;
        verifyInfo.f = Constants.STR_EMPTY;
        verifyInfo.j = "wx3909f6add1206543";
        verifyInfo.i = b2;
        verifyInfo.h = b3;
        verifyInfo.e = b();
        verifyInfo.d = this.e.g;
        verifyInfo.l = this.e.Q;
        verifyInfo.m = 1;
        t tVar = new t();
        tVar.f1676a = verifyInfo;
        this.f = this.d.a(tVar);
    }

    public int a(String str, byte b2, byte b3) {
        if (str == null) {
            str = Constants.STR_EMPTY;
        }
        t tVar = new t();
        VerifyInfo verifyInfo = new VerifyInfo();
        verifyInfo.f2419a = this.e.f1634a;
        verifyInfo.b = this.e.b;
        verifyInfo.c = this.e.c;
        verifyInfo.g = str;
        verifyInfo.f = Constants.STR_EMPTY;
        verifyInfo.j = bc.e;
        verifyInfo.i = b2;
        verifyInfo.h = b3;
        verifyInfo.e = c();
        verifyInfo.d = this.e.g;
        verifyInfo.l = this.e.Q;
        verifyInfo.m = 2;
        tVar.f1676a = verifyInfo;
        return this.c.a(tVar);
    }

    private int b() {
        PackageInfo d2 = e.d("com.tencent.mm", 0);
        if (d2 != null) {
            return d2.versionCode;
        }
        return 0;
    }

    private int c() {
        PackageInfo d2 = e.d("com.tencent.mobileqq", 0);
        if (d2 != null) {
            return d2.versionCode;
        }
        return 0;
    }
}
