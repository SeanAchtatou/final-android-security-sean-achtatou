package com.tencent.assistant.manager.notification;

import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.activity.BrowserActivity;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.txscrollview.TXTabBarLayout;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.a;
import com.tencent.assistant.link.b;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.manager.SelfUpdateManager;
import com.tencent.assistant.manager.notification.a.p;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.protocol.jce.ActionUrl;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.f;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.e;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.activity.MainActivity;
import com.tencent.assistantv2.st.page.c;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
public class AbstractNotificationService extends Service {
    public void onCreate() {
        super.onCreate();
    }

    public void onStart(Intent intent, int i) {
        a(intent);
        super.onStart(intent, i);
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onLowMemory() {
        super.onLowMemory();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.download.a.a(com.tencent.assistant.download.DownloadInfo, boolean):boolean
     arg types: [com.tencent.assistant.download.DownloadInfo, int]
     candidates:
      com.tencent.assistant.download.a.a(com.tencent.assistant.download.a, java.util.List):java.util.List
      com.tencent.assistant.download.a.a(java.util.ArrayList<com.tencent.assistant.download.DownloadInfo>, boolean):void
      com.tencent.assistant.download.a.a(long, long):boolean
      com.tencent.assistant.download.a.a(com.tencent.assistant.download.a, boolean):boolean
      com.tencent.assistant.download.a.a(java.lang.String, int):boolean
      com.tencent.assistant.download.a.a(java.util.List<com.tencent.assistant.model.SimpleAppModel>, com.tencent.assistantv2.st.model.StatInfo):int
      com.tencent.assistant.download.a.a(com.tencent.assistant.download.DownloadInfo, com.tencent.assistant.download.SimpleDownloadInfo$UIType):void
      com.tencent.assistant.download.a.a(com.tencent.assistant.model.SimpleAppModel, com.tencent.assistantv2.st.model.StatInfo):void
      com.tencent.assistant.download.a.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.download.a.a(java.lang.String, boolean):boolean
      com.tencent.assistant.download.a.a(com.tencent.assistant.download.DownloadInfo, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private boolean a(Intent intent) {
        String str;
        String str2;
        DownloadInfo downloadInfo;
        if (intent == null) {
            return false;
        }
        int intExtra = intent.getIntExtra("notification_id", -1);
        switch (intExtra) {
            case TXTabBarLayout.TABITEM_TIPS_TEXT_ID:
                a(0);
                break;
            case 102:
                a(0);
                break;
            case 103:
                DownloadProxy.a().e();
                break;
            case 104:
                a(1);
                break;
            case 105:
                a(2);
                break;
            case 107:
                v.a().c();
                String stringExtra = intent.getStringExtra("notification_data");
                if (!TextUtils.isEmpty(stringExtra)) {
                    a.a().c(stringExtra);
                    break;
                }
                break;
            case 109:
            case 113:
                a(0);
                break;
            case NormalErrorRecommendPage.ERROR_TYPE_SEARCH_RESULT_EMPTY:
                a();
                break;
            case 111:
                f();
                break;
            case 112:
            case 115:
                int intExtra2 = intent.getIntExtra("notification_id", -1);
                if (-1 != intExtra2) {
                    v.a().a(intExtra2);
                }
                e();
                ActionUrl actionUrl = (ActionUrl) intent.getSerializableExtra("notification_action");
                if (actionUrl != null && !TextUtils.isEmpty(actionUrl.a())) {
                    String a2 = actionUrl.a();
                    if (!a2.contains("?")) {
                        str2 = a2 + com.tencent.assistant.link.a.b;
                    } else {
                        str2 = a2 + com.tencent.assistant.link.a.f1394a;
                    }
                    Intent intent2 = new Intent("android.intent.action.VIEW", Uri.parse(str2));
                    intent2.setFlags(268435456);
                    int a3 = c.a(intExtra, intent.getIntExtra("notification_push_type", STConst.ST_PAGE_PUSH));
                    intent2.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, a3);
                    intent2.putExtra("preActivityPushInfo", intent.getStringExtra("notification_push_extra"));
                    intent2.putExtra("com.tencent.assistant.ACTION_URL", actionUrl);
                    intent2.putExtra(com.tencent.assistant.b.a.G, true);
                    ArrayList<String> stringArrayListExtra = intent.getStringArrayListExtra(com.tencent.assistant.b.a.R);
                    if (stringArrayListExtra != null && stringArrayListExtra.size() > 0) {
                        intent2.putStringArrayListExtra(com.tencent.assistant.b.a.R, stringArrayListExtra);
                    }
                    a(intExtra, a3, intent.getLongExtra("notification_push_id", 0), intent.getStringExtra("notification_push_extra"), intent.getByteArrayExtra("notification_push_recommend_id"));
                    try {
                        AstApp.i().startActivity(intent2);
                        break;
                    } catch (Exception e) {
                        break;
                    }
                }
            case 114:
                v.a().a(false, false, 0);
                List<DownloadInfo> e2 = DownloadProxy.a().e(AstApp.i().getPackageName());
                if (e2 != null && e2.size() > 0 && (downloadInfo = e2.get(0)) != null && e.c(AstApp.i().getPackageName(), downloadInfo.versionCode) && !a.a().a(downloadInfo, false)) {
                    a(2);
                    break;
                }
            case 116:
                c.a(116, 0, false);
                b();
                break;
            case 117:
                c.a(117, intent.getIntExtra("notification_push_sub_type", 0), false);
                c();
                break;
            case 118:
                c.a(118, intent.getIntExtra("notification_push_sub_type", 0), false);
                d();
                break;
            case 120:
                String stringExtra2 = intent.getStringExtra("notification_push_extra");
                if (!TextUtils.isEmpty(stringExtra2)) {
                    DownloadInfo d = DownloadProxy.a().d(stringExtra2);
                    if (d != null) {
                        Intent intent3 = new Intent(AstApp.i(), AppDetailActivityV5.class);
                        intent3.setFlags(268435456);
                        intent3.putExtra("com.tencent.assistant.PACKAGE_NAME", d.packageName);
                        intent3.putExtra("com.tencent.assistant.APK_ID", d.apkId);
                        intent3.putExtra("com.tencent.assistant.APP_ID", d.appId);
                        intent3.putExtra(com.tencent.assistant.b.a.C, d.channelId);
                        intent3.putExtra("preActivityTagInfo", com.tencent.assistantv2.st.page.a.a((int) STConst.ST_PAGE_LOCAL_PUSH, "03_001"));
                        AstApp.i().startActivity(intent3);
                    }
                    c.a(120, 0, false);
                }
                XLog.d("WiseDownload", "Booking download notification clicked!!!");
                break;
            case 121:
                ActionUrl actionUrl2 = (ActionUrl) intent.getSerializableExtra("notification_action");
                if (actionUrl2 != null && !TextUtils.isEmpty(actionUrl2.a())) {
                    String a4 = actionUrl2.a();
                    if (!a4.contains("?")) {
                        str = a4 + com.tencent.assistant.link.a.b;
                    } else {
                        str = a4 + com.tencent.assistant.link.a.f1394a;
                    }
                    Intent intent4 = new Intent("android.intent.action.VIEW", Uri.parse(str));
                    if (b.a(AstApp.i(), intent4)) {
                        intent4.setFlags(268435456);
                        intent4.putExtra("preActivityPushInfo", intent.getStringExtra("notification_push_extra"));
                        intent4.putExtra("com.tencent.assistant.ACTION_URL", actionUrl2);
                        intent4.putExtra(com.tencent.assistant.b.a.G, true);
                        intent4.putExtra("preActivityTagInfo", com.tencent.assistantv2.st.page.a.a((int) STConst.ST_PAGE_LOCAL_PUSH, "03_001"));
                        AstApp.i().startActivity(intent4);
                        XLog.d("WiseDownload", "subscription download notification clicked!!!");
                        c.a(115, 13, false);
                        break;
                    }
                }
                break;
            case 123:
                String stringExtra3 = intent.getStringExtra("notification_action");
                String stringExtra4 = intent.getStringExtra("notification_push_extra");
                Intent intent5 = new Intent(AstApp.i(), BrowserActivity.class);
                intent5.putExtra("com.tencent.assistant.BROWSER_URL", stringExtra3);
                intent5.putExtra("goback", "2");
                intent5.putExtra("com.tencent.assistant.activity.BROWSER_TYPE", "0");
                intent5.putExtra("preActivityPushInfo", stringExtra4);
                intent5.putExtra("preActivityTagInfo", com.tencent.assistantv2.st.page.a.a((int) STConst.ST_PAGE_LOCAL_PUSH, "03_001"));
                intent5.setFlags(268435456);
                AstApp.i().startActivity(intent5);
                c.a(123, (int) STConst.ST_PAGE_UPDATE_PUSH, 0, p.f(), (byte[]) null);
                break;
            case 124:
                String stringExtra5 = intent.getStringExtra("notification_data");
                h.a().a(stringExtra5);
                if (intent.getAction() != "android.intent.action.DELETE" && !TextUtils.isEmpty(stringExtra5)) {
                    a.a().c(stringExtra5);
                }
                c.a(124, 0, false);
                break;
        }
        return true;
    }

    private void e() {
        Method method;
        int i = Build.VERSION.SDK_INT;
        try {
            Object systemService = getSystemService("statusbar");
            Class<?> cls = Class.forName("android.app.StatusBarManager");
            if (systemService != null) {
                if (i <= 16) {
                    method = cls.getMethod("collapse", new Class[0]);
                } else {
                    method = cls.getMethod("collapsePanels", new Class[0]);
                }
                method.setAccessible(true);
                method.invoke(systemService, new Object[0]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
    }

    /* access modifiers changed from: protected */
    public void b() {
    }

    /* access modifiers changed from: protected */
    public void c() {
    }

    /* access modifiers changed from: protected */
    public void d() {
    }

    private void f() {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), MainActivity.class);
        intent.setFlags(268435456);
        AstApp.i().startActivity(intent);
    }

    private void a(int i) {
        Uri a2;
        int i2 = STConst.ST_PAGE_UPDATE_PUSH;
        int i3 = 2000;
        HashMap hashMap = new HashMap();
        hashMap.put(com.tencent.assistant.b.a.D, "1");
        switch (i) {
            case 0:
                hashMap.put(com.tencent.assistant.b.a.w, com.tencent.assistant.b.a.w);
                a2 = b.a("tmast", "download", hashMap);
                break;
            case 1:
            case 3:
                if (i == 3) {
                    hashMap.put(STConst.ST_PUSH_TO_UPDATE_KEY, String.valueOf((int) STConst.ST_PAGE_UPDATE_PUSH));
                } else {
                    i2 = 2000;
                }
                int i4 = i2;
                a2 = b.a("tmast", "update", hashMap);
                i3 = i4;
                break;
            case 2:
                List<DownloadInfo> e = DownloadProxy.a().e(AstApp.i().getPackageName());
                if (e != null && e.size() > 0) {
                    hashMap.put(com.tencent.assistant.b.a.c, AstApp.i().getPackageName());
                    hashMap.put(com.tencent.assistant.b.a.b, String.valueOf(e.get(0).apkId));
                    if (!(SelfUpdateManager.a().d() == null || SelfUpdateManager.a().d().y == null)) {
                        hashMap.put(com.tencent.assistant.b.a.C, String.valueOf(SelfUpdateManager.a().d().y));
                    }
                    a2 = b.a("tmast", "appdetails", hashMap);
                    break;
                } else {
                    v.a().a(false, false, 0);
                }
                break;
            default:
                a2 = null;
                break;
        }
        if (a2 != null) {
            Intent intent = new Intent("android.intent.action.VIEW", a2);
            intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, i3);
            intent.setFlags(268435456);
            try {
                AstApp.i().startActivity(intent);
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    private void a(int i, int i2, long j, String str, byte[] bArr) {
        f.a(j);
        f.c(str);
        if (i == 112) {
            f.a((byte) 4);
        } else if (i == 115) {
            f.a((byte) 7);
        }
        c.a(i, i2, j, str, bArr);
    }
}
