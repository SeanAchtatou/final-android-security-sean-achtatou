package com.tencent.assistant.manager;

import com.qq.AppService.AstApp;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.db.a;
import com.tencent.assistant.protocol.jce.AppUpdateInfo;
import com.tencent.assistant.protocol.jce.AutoDownloadCfg;
import com.tencent.assistant.protocol.jce.AutoDownloadInfo;
import com.tencent.assistant.protocol.jce.CheckSelfUpdateResponse;
import com.tencent.assistant.protocol.jce.DownloadButtonSpecailInfoList;
import com.tencent.assistant.protocol.jce.GetAppCategoryResponse;
import com.tencent.assistant.protocol.jce.GetAppDetailResponse;
import com.tencent.assistant.protocol.jce.GetAppListResponse;
import com.tencent.assistant.protocol.jce.GetAutoDownloadResponse;
import com.tencent.assistant.protocol.jce.GetDiscoverResponse;
import com.tencent.assistant.protocol.jce.GetExplicitHotWordsResponse;
import com.tencent.assistant.protocol.jce.GetGroupAppsResponse;
import com.tencent.assistant.protocol.jce.GetManageInfoListResponse;
import com.tencent.assistant.protocol.jce.GetOpRegularInfoResponse;
import com.tencent.assistant.protocol.jce.GetRecommendTabPageResponse;
import com.tencent.assistant.protocol.jce.GetSmartCardsResponse;
import com.tencent.assistant.protocol.jce.GftGetAppCategoryResponse;
import com.tencent.assistant.protocol.jce.GftGetAppListResponse;
import com.tencent.assistant.protocol.jce.GftGetGameGiftFlagResponse;
import com.tencent.assistant.protocol.jce.GftGetRecommendTabPageResponse;
import com.tencent.assistant.protocol.jce.IPData;
import com.tencent.assistant.protocol.jce.NpcListCfg;
import com.tencent.assistant.protocol.jce.SearchAdvancedHotWordsResponse;
import com.tencent.assistant.protocol.jce.UserActivityItem;
import com.tencent.assistant.protocol.jce.UserTaskCfg;
import com.tencent.assistant.utils.FileUtil;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public class o {
    public GetDiscoverResponse a(byte[] bArr) {
        String str = "discover";
        if (bArr != null && bArr.length > 0) {
            str = str + "_" + d(bArr);
        }
        return (GetDiscoverResponse) a.a(str, "home", GetDiscoverResponse.class);
    }

    public GetSmartCardsResponse a() {
        return (GetSmartCardsResponse) a.a("smart_card", "smarthome", GetSmartCardsResponse.class);
    }

    public void a(byte[] bArr, GetDiscoverResponse getDiscoverResponse) {
        String str = "discover";
        if (bArr == null || bArr.length == 0) {
            a.a("home");
        } else {
            str = str + "_" + d(bArr);
        }
        a.a(str, "home", getDiscoverResponse);
    }

    public void a(GetSmartCardsResponse getSmartCardsResponse) {
        a.a("smart_card", "smarthome", getSmartCardsResponse);
    }

    public GetAppCategoryResponse b() {
        return (GetAppCategoryResponse) a.a("category", GetAppCategoryResponse.class);
    }

    public void a(GetAppCategoryResponse getAppCategoryResponse) {
        a.a("category", getAppCategoryResponse);
    }

    public GetAppListResponse a(long j, int i, byte[] bArr) {
        String str = "app_list_" + j + "_" + i;
        if (bArr != null && bArr.length > 0) {
            str = str + "_" + d(bArr);
        }
        return (GetAppListResponse) a.a(str, "al" + j + "-" + i, GetAppListResponse.class);
    }

    public void a(long j, int i, byte[] bArr, GetAppListResponse getAppListResponse) {
        String str = "app_list_" + j + "_" + i;
        if (bArr == null || bArr.length == 0) {
            a.a("al" + j + "-" + i);
        } else {
            str = str + "_" + d(bArr);
        }
        a.a(str, "al" + j + "-" + i, getAppListResponse);
    }

    public GftGetAppListResponse b(long j, int i, byte[] bArr) {
        String str = "game_app_list_" + j + "_" + i;
        if (bArr != null && bArr.length > 0) {
            str = str + "_" + d(bArr);
        }
        return (GftGetAppListResponse) a.a(str, "gameal" + j + "-" + i, GftGetAppListResponse.class);
    }

    public void a(long j, int i, byte[] bArr, GftGetAppListResponse gftGetAppListResponse) {
        String str = "game_app_list_" + j + "_" + i;
        if (bArr == null || bArr.length == 0) {
            a.a("gameal" + j + "-" + i);
        } else {
            str = str + "_" + d(bArr);
        }
        a.a(str, "gameal" + j + "-" + i, gftGetAppListResponse);
    }

    public GetRecommendTabPageResponse b(byte[] bArr) {
        String str = "app_recommend_tab_list";
        if (bArr != null && bArr.length > 0) {
            str = str + "_" + d(bArr);
        }
        return (GetRecommendTabPageResponse) a.a(str, "al", GetRecommendTabPageResponse.class);
    }

    public void a(byte[] bArr, GetRecommendTabPageResponse getRecommendTabPageResponse) {
        String str = "app_recommend_tab_list";
        if (bArr == null || bArr.length == 0) {
            a.a("al");
        } else {
            str = str + "_" + d(bArr);
        }
        a.a(str, "al", getRecommendTabPageResponse);
    }

    public void a(byte[] bArr, GftGetRecommendTabPageResponse gftGetRecommendTabPageResponse) {
        String str = "game_tab_list";
        if (bArr == null || bArr.length == 0) {
            a.a("gl");
        } else {
            str = str + "_" + d(bArr);
        }
        a.a(str, "gl", gftGetRecommendTabPageResponse);
    }

    public GftGetRecommendTabPageResponse c(byte[] bArr) {
        String str = "game_tab_list";
        if (bArr != null && bArr.length > 0) {
            str = str + "_" + d(bArr);
        }
        return (GftGetRecommendTabPageResponse) a.a(str, "gl", GftGetRecommendTabPageResponse.class);
    }

    public GetGroupAppsResponse a(String str, byte[] bArr) {
        String str2 = "group_app_list_" + str;
        if (bArr != null && bArr.length > 0) {
            str2 = str2 + "_" + d(bArr);
        }
        return (GetGroupAppsResponse) a.a(str2, "group_app_list", GetGroupAppsResponse.class);
    }

    public void a(String str, byte[] bArr, GetGroupAppsResponse getGroupAppsResponse) {
        String str2 = "group_app_list_" + str;
        if (bArr == null || bArr.length == 0) {
            a.a("group_app_list_" + str);
        } else {
            str2 = str2 + "_" + d(bArr);
        }
        a.a(str2, "group_app_list", getGroupAppsResponse);
    }

    public void a(GetOpRegularInfoResponse getOpRegularInfoResponse) {
        a.a("op_regular_push_response");
        a.a("op_regular_push_response", "op_regular_push_response", getOpRegularInfoResponse);
    }

    public GetOpRegularInfoResponse c() {
        return (GetOpRegularInfoResponse) a.a("op_regular_push_response", "op_regular_push_response", GetOpRegularInfoResponse.class);
    }

    public void a(GetManageInfoListResponse getManageInfoListResponse) {
        a.a(AstApp.i().getFilesDir().getAbsolutePath() + FileUtil.APP_SDCARD_UNAMOUNT_ROOT_PATH + FileUtil.CACHE_DIR_PATH, "manage_info_list", (String) null, getManageInfoListResponse);
    }

    public GetManageInfoListResponse d() {
        return (GetManageInfoListResponse) a.a(AstApp.i().getFilesDir().getAbsolutePath() + FileUtil.APP_SDCARD_UNAMOUNT_ROOT_PATH + FileUtil.CACHE_DIR_PATH, "manage_info_list", (String) null, GetManageInfoListResponse.class);
    }

    public void a(SearchAdvancedHotWordsResponse searchAdvancedHotWordsResponse) {
        a.a("hot_words_advance", "hotwords", searchAdvancedHotWordsResponse);
    }

    public SearchAdvancedHotWordsResponse e() {
        return (SearchAdvancedHotWordsResponse) a.a("hot_words_advance", "hotwords", SearchAdvancedHotWordsResponse.class);
    }

    public void a(GetExplicitHotWordsResponse getExplicitHotWordsResponse) {
        a.a("hot_words_explicit", "hotwords", getExplicitHotWordsResponse);
    }

    public GetExplicitHotWordsResponse f() {
        return (GetExplicitHotWordsResponse) a.a("hot_words_explicit", "hotwords", GetExplicitHotWordsResponse.class);
    }

    public List<IPData> g() {
        return a.b("ip_data_list", IPData.class);
    }

    public Map<Integer, ArrayList<AppUpdateInfo>> h() {
        Hashtable hashtable = new Hashtable();
        int i = 1;
        while (true) {
            int i2 = i;
            if (i2 > 3) {
                return hashtable;
            }
            ArrayList arrayList = (ArrayList) a.b("app_update_info_" + i2, AppUpdateInfo.class);
            if (arrayList != null && !arrayList.isEmpty()) {
                hashtable.put(Integer.valueOf(i2), arrayList);
            }
            i = i2 + 1;
        }
    }

    public boolean a(List<IPData> list) {
        return a.a("ip_data_list", list);
    }

    public boolean a(Map<Integer, ArrayList<AppUpdateInfo>> map) {
        boolean b;
        int i = 1;
        boolean z = false;
        while (i <= 3) {
            String str = "app_update_info_" + i;
            if (!map.containsKey(Integer.valueOf(i)) || map.get(Integer.valueOf(i)) == null || map.get(Integer.valueOf(i)).isEmpty()) {
                b = a.b(str);
            } else {
                try {
                    b = a.a(str, map.get(Integer.valueOf(i))) && z;
                } catch (Throwable th) {
                    cq.a().b();
                    b = z;
                }
            }
            i++;
            z = b;
        }
        return z;
    }

    private static String d(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        if (bArr == null || bArr.length <= 0) {
            return "NA";
        }
        for (byte b : bArr) {
            stringBuffer.append((int) b);
        }
        return stringBuffer.toString();
    }

    public AutoDownloadCfg i() {
        return (AutoDownloadCfg) a.a("auto_download_Cfg_Cache_Path", AutoDownloadCfg.class);
    }

    public void a(AutoDownloadCfg autoDownloadCfg) {
        a.a("auto_download_Cfg_Cache_Path", autoDownloadCfg);
    }

    public GetAutoDownloadResponse j() {
        return (GetAutoDownloadResponse) a.a("auto_download_response_data_path", GetAutoDownloadResponse.class);
    }

    public void a(GetAutoDownloadResponse getAutoDownloadResponse) {
        a.a("auto_download_response_data_path", getAutoDownloadResponse);
    }

    public List<AutoDownloadInfo> k() {
        return a.b("auto_download_ignore_new_app_list_data_path", AutoDownloadInfo.class);
    }

    public void b(List<AutoDownloadInfo> list) {
        a.a("auto_download_ignore_new_app_list_data_path", list);
    }

    public void l() {
        a.b("auto_download_ignore_new_app_list_data_path");
    }

    public CheckSelfUpdateResponse m() {
        return (CheckSelfUpdateResponse) a.a("self_update_cache_path", CheckSelfUpdateResponse.class);
    }

    public void a(CheckSelfUpdateResponse checkSelfUpdateResponse) {
        a.a("self_update_cache_path", checkSelfUpdateResponse);
    }

    public void a(UserTaskCfg userTaskCfg) {
        a.a("user_task_cfg_cache_path", userTaskCfg);
    }

    public UserTaskCfg n() {
        return (UserTaskCfg) a.a("user_task_cfg_cache_path", UserTaskCfg.class);
    }

    public void b(UserTaskCfg userTaskCfg) {
        a.a("user_task_cfg_old_cache_path", userTaskCfg);
    }

    public UserTaskCfg o() {
        return (UserTaskCfg) a.a("user_task_cfg_old_cache_path", UserTaskCfg.class);
    }

    public void a(NpcListCfg npcListCfg) {
        a.a("npc_cfg_cache_path", npcListCfg);
    }

    public NpcListCfg p() {
        return (NpcListCfg) a.a("npc_cfg_cache_path", NpcListCfg.class);
    }

    public void b(NpcListCfg npcListCfg) {
        a.a("game_npc_cfg_cache_path", npcListCfg);
    }

    public NpcListCfg q() {
        return (NpcListCfg) a.a("game_npc_cfg_cache_path", NpcListCfg.class);
    }

    public void a(GftGetGameGiftFlagResponse gftGetGameGiftFlagResponse) {
        a.a("game_gift_flag_path", gftGetGameGiftFlagResponse);
    }

    public GftGetGameGiftFlagResponse r() {
        return (GftGetGameGiftFlagResponse) a.a("game_gift_flag_path", GftGetGameGiftFlagResponse.class);
    }

    public void a(GetAppDetailResponse getAppDetailResponse) {
        a.a("self_share_data", getAppDetailResponse);
    }

    public GetAppDetailResponse s() {
        return (GetAppDetailResponse) a.a("self_share_data", GetAppDetailResponse.class);
    }

    public void a(String str, UserActivityItem userActivityItem) {
        a.a(str, userActivityItem);
    }

    public UserActivityItem a(String str) {
        return (UserActivityItem) a.a(str, UserActivityItem.class);
    }

    public boolean c(List<? extends JceStruct> list) {
        return a.a("uc_cache_data", list);
    }

    public List<? extends JceStruct> a(Class<? extends JceStruct> cls) {
        return a.b("uc_cache_data", cls);
    }

    public void a(DownloadButtonSpecailInfoList downloadButtonSpecailInfoList) {
        a.a("specail_apps_download_btn_style_cfg", downloadButtonSpecailInfoList);
    }

    public DownloadButtonSpecailInfoList t() {
        return (DownloadButtonSpecailInfoList) a.a("specail_apps_download_btn_style_cfg", DownloadButtonSpecailInfoList.class);
    }

    public void u() {
        a.b("specail_apps_download_btn_style_cfg");
    }

    public GftGetAppCategoryResponse v() {
        return (GftGetAppCategoryResponse) a.a("game_category", GftGetAppCategoryResponse.class);
    }

    public void a(GftGetAppCategoryResponse gftGetAppCategoryResponse) {
        a.a("game_category", gftGetAppCategoryResponse);
    }
}
