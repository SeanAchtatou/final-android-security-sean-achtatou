package com.tencent.assistant.manager.smartcard.a;

import android.content.Context;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.smartcard.NormalSmartcardBaseItem;
import com.tencent.assistant.component.smartcard.SmartcardListener;
import com.tencent.assistant.manager.smartcard.b.b;
import com.tencent.assistant.manager.smartcard.c;
import com.tencent.assistant.manager.smartcard.view.NormalSmartCardGiftTemplateItem;
import com.tencent.assistant.manager.smartcard.y;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.protocol.jce.SmartCardTemplate3;

/* compiled from: ProGuard */
public class a extends c {
    public Class<? extends JceStruct> a() {
        return SmartCardTemplate3.class;
    }

    public com.tencent.assistant.model.a.a b() {
        return new b();
    }

    /* access modifiers changed from: protected */
    public y c() {
        return new com.tencent.assistant.manager.smartcard.a.a.a();
    }

    public NormalSmartcardBaseItem a(Context context, i iVar, SmartcardListener smartcardListener, IViewInvalidater iViewInvalidater) {
        return new NormalSmartCardGiftTemplateItem(context, iVar, smartcardListener, iViewInvalidater);
    }
}
