package com.tencent.assistant.manager;

import com.tencent.assistant.db.a;
import com.tencent.wcs.jce.ServerList;

/* compiled from: ProGuard */
public class as extends o {

    /* renamed from: a  reason: collision with root package name */
    private static as f1489a = null;

    public static synchronized as w() {
        as asVar;
        synchronized (as.class) {
            if (f1489a == null) {
                f1489a = new as();
            }
            asVar = f1489a;
        }
        return asVar;
    }

    private as() {
    }

    public boolean a(ServerList serverList) {
        return a.a("connect_ip_data_list", serverList);
    }

    public ServerList x() {
        return (ServerList) a.a("connect_ip_data_list", ServerList.class);
    }
}
