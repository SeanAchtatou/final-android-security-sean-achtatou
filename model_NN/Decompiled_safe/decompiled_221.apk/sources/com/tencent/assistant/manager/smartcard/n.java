package com.tencent.assistant.manager.smartcard;

import com.qq.AppService.AstApp;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.manager.SelfUpdateManager;
import com.tencent.assistant.manager.ck;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.model.a.o;
import com.tencent.assistant.model.a.r;
import com.tencent.assistant.model.a.s;
import com.tencent.assistant.utils.cv;
import com.tencent.assistant.utils.e;
import java.util.List;

/* compiled from: ProGuard */
public class n extends y {
    public boolean a(i iVar, List<Long> list) {
        if (iVar == null || iVar.i != 6) {
            return false;
        }
        return a((o) iVar, (r) this.f1610a.get(Integer.valueOf(iVar.i)), (s) this.b.get(Integer.valueOf(iVar.i)));
    }

    private boolean a(o oVar, r rVar, s sVar) {
        if (sVar == null) {
            return false;
        }
        if (rVar == null) {
            rVar = new r();
            rVar.f = oVar.j;
            rVar.e = oVar.i;
            this.f1610a.put(Integer.valueOf(rVar.e), rVar);
        }
        SelfUpdateManager.SelfUpdateInfo d = SelfUpdateManager.a().d();
        ck n = SelfUpdateManager.a().n();
        if ((n == null || !cv.b(n.c)) && !SelfUpdateManager.a().j()) {
            if (d != null) {
                String packageName = AstApp.i().getPackageName();
                if (ApkResourceManager.getInstance().getLocalApkInfoFromCacheOrByFileName(packageName, d.e, 0) == null) {
                    return false;
                }
                LocalApkInfo installedApkInfo = ApkResourceManager.getInstance().getInstalledApkInfo(packageName);
                if (installedApkInfo == null) {
                    installedApkInfo = e.b(packageName);
                }
                if (installedApkInfo == null || installedApkInfo.mVersionCode < d.e) {
                    oVar.b = installedApkInfo;
                    oVar.f1648a = d;
                } else {
                    a(oVar.s, oVar.j + "||" + oVar.i + "|" + 6, oVar.i);
                    return false;
                }
            }
            if (oVar.b == null || oVar.f1648a == null) {
                return false;
            }
            if (rVar.b >= sVar.b) {
                a(oVar.s, oVar.j + "||" + oVar.i + "|" + 1, oVar.i);
                return false;
            } else if (rVar.f1651a >= sVar.f1652a) {
                a(oVar.s, oVar.j + "||" + oVar.i + "|" + 2, oVar.i);
                return false;
            } else if (SelfUpdateManager.a().q()) {
                a(oVar.s, oVar.j + "||" + oVar.i + "|" + 5, oVar.i);
                return false;
            } else {
                SelfUpdateManager.a().c(true);
                return true;
            }
        } else {
            a(oVar.s, oVar.j + "||" + oVar.i + "|" + 5, oVar.i);
            return false;
        }
    }

    public void a(i iVar) {
        if (iVar != null && iVar.i == 6) {
            o oVar = (o) iVar;
            s sVar = (s) this.b.get(Integer.valueOf(oVar.i));
            if (sVar != null) {
                oVar.p = sVar.d;
            }
        }
    }
}
