package com.tencent.assistant.manager.notification;

import android.graphics.Bitmap;
import com.tencent.assistant.thumbnailCache.p;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
public class d {

    /* renamed from: a  reason: collision with root package name */
    String f1564a;
    int b;
    g c;
    final /* synthetic */ a d;
    /* access modifiers changed from: private */
    public p e = new f(this);

    public d(a aVar, String str, int i) {
        this.d = aVar;
        this.b = i;
        this.f1564a = str;
    }

    public void a(g gVar) {
        this.c = gVar;
    }

    public void a() {
        TemporaryThreadManager.get().start(new e(this));
    }

    /* access modifiers changed from: protected */
    public synchronized void a(int i, Bitmap bitmap) {
        if (this.c != null) {
            this.c.a(bitmap);
        }
    }
}
