package com.tencent.assistant.manager;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.download.a;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.model.j;
import com.tencent.assistant.module.callback.r;
import com.tencent.assistant.module.dm;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.mgr.c;
import com.tencent.assistant.plugin.mgr.d;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ba;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* compiled from: ProGuard */
public class av implements UIEventListener, NetworkMonitor.ConnectivityChangeListener, r {

    /* renamed from: a  reason: collision with root package name */
    private static av f1491a;
    private static HashSet<String> e = new HashSet<>(1);
    private static Handler f = null;
    private AstApp b;
    private Set<String> c = null;
    private Object d = new Object();

    static {
        e.add("com.tencent.assistant.freewifi");
    }

    public static synchronized av a() {
        av avVar;
        synchronized (av.class) {
            if (f1491a == null) {
                f1491a = new av();
            }
            avVar = f1491a;
        }
        return avVar;
    }

    private av() {
        c();
    }

    private void c() {
        this.b = AstApp.i();
        d();
        dm.a().register(this);
        cq.a().a(this);
    }

    private void d() {
        this.b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_QUEUING, this);
        this.b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_DOWNLOADING, this);
        this.b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_PAUSE, this);
        this.b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_SUCC, this);
        this.b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_FAIL, this);
        this.b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FAIL, this);
        this.b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_SUCC, this);
        this.b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FAIL_AND_RETRY, this);
    }

    public DownloadInfo a(j jVar) {
        return a(jVar, SimpleDownloadInfo.UIType.NORMAL);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.DownloadProxy.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.manager.DownloadProxy.a(com.tencent.assistant.manager.DownloadProxy, android.app.Dialog):android.app.Dialog
      com.tencent.assistant.manager.DownloadProxy.a(com.tencent.assistant.manager.DownloadProxy, com.tencent.assistant.net.APN):com.tencent.assistant.net.APN
      com.tencent.assistant.manager.DownloadProxy.a(com.tencent.assistant.manager.DownloadProxy, com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager):com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager
      com.tencent.assistant.manager.DownloadProxy.a(com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList<com.tencent.assistant.download.DownloadInfo>):java.lang.String
      com.tencent.assistant.manager.DownloadProxy.a(com.tencent.assistant.download.DownloadInfo, com.tencent.assistant.download.SimpleDownloadInfo$DownloadState):void
      com.tencent.assistant.manager.DownloadProxy.a(java.lang.String, int):com.tencent.assistant.download.DownloadInfo
      com.tencent.assistant.manager.DownloadProxy.a(com.tencent.assistant.download.SimpleDownloadInfo$DownloadType, boolean):java.util.ArrayList<com.tencent.assistant.download.DownloadInfo>
      com.tencent.assistant.manager.DownloadProxy.a(java.lang.String, boolean):void */
    public DownloadInfo b(j jVar) {
        DownloadInfo c2 = DownloadProxy.a().c(jVar.a());
        if (c2 == null) {
            return null;
        }
        if (c2.versionCode == jVar.d) {
            return c2;
        }
        DownloadProxy.a().a(jVar.a(), false);
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.DownloadProxy.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.manager.DownloadProxy.a(com.tencent.assistant.manager.DownloadProxy, android.app.Dialog):android.app.Dialog
      com.tencent.assistant.manager.DownloadProxy.a(com.tencent.assistant.manager.DownloadProxy, com.tencent.assistant.net.APN):com.tencent.assistant.net.APN
      com.tencent.assistant.manager.DownloadProxy.a(com.tencent.assistant.manager.DownloadProxy, com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager):com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager
      com.tencent.assistant.manager.DownloadProxy.a(com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList<com.tencent.assistant.download.DownloadInfo>):java.lang.String
      com.tencent.assistant.manager.DownloadProxy.a(com.tencent.assistant.download.DownloadInfo, com.tencent.assistant.download.SimpleDownloadInfo$DownloadState):void
      com.tencent.assistant.manager.DownloadProxy.a(java.lang.String, int):com.tencent.assistant.download.DownloadInfo
      com.tencent.assistant.manager.DownloadProxy.a(com.tencent.assistant.download.SimpleDownloadInfo$DownloadType, boolean):java.util.ArrayList<com.tencent.assistant.download.DownloadInfo>
      com.tencent.assistant.manager.DownloadProxy.a(java.lang.String, boolean):void */
    public DownloadInfo a(j jVar, SimpleDownloadInfo.UIType uIType) {
        DownloadInfo d2 = DownloadProxy.a().d(jVar.a());
        if (d2 == null) {
            d2 = DownloadInfo.createDownloadInfo(jVar);
        } else if (d2.versionCode != jVar.d) {
            DownloadProxy.a().a(jVar.a(), false);
            d2 = DownloadInfo.createDownloadInfo(jVar);
        }
        d2.uiType = uIType;
        DownloadProxy.a().d(d2);
        return d2;
    }

    public void c(j jVar) {
        DownloadInfo a2 = a(jVar);
        if (a2.downloadState != SimpleDownloadInfo.DownloadState.DOWNLOADING) {
            DownloadProxy.a().c(a2);
        }
    }

    public void d(j jVar) {
        if (jVar != null && !TextUtils.isEmpty(jVar.o)) {
            STInfoV2 sTInfoV2 = new STInfoV2(STConst.ST_PAGE_PLUGIN, STConst.ST_DEFAULT_SLOT, STConst.ST_PAGE_PLUGIN, STConst.ST_DEFAULT_SLOT, 200);
            sTInfoV2.extraData = jVar.f1666a + "|" + jVar.c + "|" + jVar.p + "|" + 3;
            k.a(sTInfoV2);
            DownloadProxy.a().c(a(jVar, SimpleDownloadInfo.UIType.PLUGIN_PREDOWNLOAD));
        }
    }

    public void e(j jVar) {
        DownloadInfo d2 = DownloadProxy.a().d(jVar.a());
        if (d2 != null) {
            a.a().b(d2.downloadTicket);
        }
    }

    public void handleUIEvent(Message message) {
        j a2;
        String str = Constants.STR_EMPTY;
        if (message.obj instanceof String) {
            str = (String) message.obj;
        }
        if (!TextUtils.isEmpty(str)) {
            switch (message.what) {
                case EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_DOWNLOADING:
                case EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_PAUSE:
                case EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_QUEUING:
                default:
                    return;
                case EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLOAD_SUCC:
                    DownloadInfo c2 = DownloadProxy.a().c(str);
                    if (c2 != null && (a2 = dm.a().a(str)) != null) {
                        TemporaryThreadManager.get().start(new aw(this, c2, a2));
                        return;
                    }
                    return;
                case EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_SUCC:
                case EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FAIL:
                    DownloadProxy.a().b(str);
                    return;
                case EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FAIL_AND_RETRY:
                    if (!a.a().a(str)) {
                        DownloadProxy.a().b(str);
                        this.b.j().sendMessage(this.b.j().obtainMessage(EventDispatcherEnum.UI_EVENT_PLUGIN_INSTALL_FINAL_FAIL, str));
                        return;
                    }
                    return;
            }
        }
    }

    public void b() {
    }

    public void a(List<j> list) {
        b(list);
        e().postDelayed(new ax(this, list), 10000);
    }

    private void c(String str) {
        synchronized (this.d) {
            if (this.c == null) {
                this.c = new HashSet(1);
            }
            this.c.add(str);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(java.util.List<com.tencent.assistant.model.j> r6) {
        /*
            r5 = this;
            java.lang.Object r1 = r5.d
            monitor-enter(r1)
            java.util.Set<java.lang.String> r0 = r5.c     // Catch:{ all -> 0x002f }
            if (r0 == 0) goto L_0x000f
            if (r6 == 0) goto L_0x000f
            boolean r0 = r6.isEmpty()     // Catch:{ all -> 0x002f }
            if (r0 == 0) goto L_0x0011
        L_0x000f:
            monitor-exit(r1)     // Catch:{ all -> 0x002f }
        L_0x0010:
            return
        L_0x0011:
            java.util.Iterator r2 = r6.iterator()     // Catch:{ all -> 0x002f }
        L_0x0015:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x002f }
            if (r0 == 0) goto L_0x0032
            java.lang.Object r0 = r2.next()     // Catch:{ all -> 0x002f }
            com.tencent.assistant.model.j r0 = (com.tencent.assistant.model.j) r0     // Catch:{ all -> 0x002f }
            java.util.Set<java.lang.String> r3 = r5.c     // Catch:{ all -> 0x002f }
            java.lang.String r4 = r0.c     // Catch:{ all -> 0x002f }
            boolean r3 = r3.contains(r4)     // Catch:{ all -> 0x002f }
            if (r3 == 0) goto L_0x0015
            r5.c(r0)     // Catch:{ all -> 0x002f }
            goto L_0x0015
        L_0x002f:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x002f }
            throw r0
        L_0x0032:
            r0 = 0
            r5.c = r0     // Catch:{ all -> 0x002f }
            monitor-exit(r1)     // Catch:{ all -> 0x002f }
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.manager.av.b(java.util.List):void");
    }

    public j a(String str) {
        return dm.a().b(str);
    }

    public j a(int i) {
        return dm.a().a(i);
    }

    public void b(String str) {
        c(str);
        dm.a().a(0L);
    }

    /* access modifiers changed from: private */
    public synchronized void c(List<j> list) {
        if (list != null) {
            for (j next : list) {
                if (f(next)) {
                    d(next);
                }
            }
        }
    }

    private boolean f(j jVar) {
        if (jVar.n == 0) {
            return false;
        }
        PluginInfo a2 = d.b().a(jVar.c);
        if (e != null && e.contains("com.tencent.assistant.freewifi") && jVar != null && a2 != null && a2.getVersion() < jVar.d) {
            XLog.i("PluginDownloadManager", "[PluginDownloadmanager] ---> canPreDownloadPlugin pluginPackageName = com.tencent.assistant.freewifi");
            return true;
        } else if ((a2 != null && (a2.getVersion() >= jVar.d || c.a(jVar.c) >= 0)) || !com.tencent.assistant.plugin.mgr.a.a(jVar.c, jVar.d)) {
            return false;
        } else {
            DownloadInfo c2 = DownloadProxy.a().c(jVar.a());
            if (c2 != null && (c2.uiType == SimpleDownloadInfo.UIType.NORMAL || c2.downloadState == SimpleDownloadInfo.DownloadState.DOWNLOADING || c2.downloadState == SimpleDownloadInfo.DownloadState.SUCC || c2.downloadState == SimpleDownloadInfo.DownloadState.QUEUING)) {
                return false;
            }
            int i = jVar.n;
            if (i >= 1 && com.tencent.assistant.net.c.d() && !com.tencent.assistant.net.c.k()) {
                return true;
            }
            if (i >= 2 && com.tencent.assistant.net.c.f()) {
                return true;
            }
            if (i < 3 || !com.tencent.assistant.net.c.e()) {
                return false;
            }
            return true;
        }
    }

    public void b(int i) {
        this.b.j().sendMessage(this.b.j().obtainMessage(EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLIST_FAIL));
    }

    public void onConnected(APN apn) {
        e().postDelayed(new ay(this), 1000);
    }

    public void onDisconnected(APN apn) {
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
        e().postDelayed(new az(this), 1000);
    }

    private synchronized Handler e() {
        if (f == null) {
            f = ba.a("plugin_down_delay");
        }
        return f;
    }
}
