package com.tencent.assistant.manager.smartcard;

import android.content.Context;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.smartcard.NormalSmartcardBaseItem;
import com.tencent.assistant.component.smartcard.SmartcardListener;
import com.tencent.assistant.model.a.a;
import com.tencent.assistant.model.a.i;

/* compiled from: ProGuard */
public abstract class c {

    /* renamed from: a  reason: collision with root package name */
    private y f1593a = null;
    private Object b = new Object();

    public abstract NormalSmartcardBaseItem a(Context context, i iVar, SmartcardListener smartcardListener, IViewInvalidater iViewInvalidater);

    public abstract Class<? extends JceStruct> a();

    public abstract a b();

    /* access modifiers changed from: protected */
    public abstract y c();

    public y d() {
        if (this.f1593a != null) {
            return this.f1593a;
        }
        synchronized (this.b) {
            if (this.f1593a == null) {
                this.f1593a = c();
            }
        }
        return this.f1593a;
    }
}
