package com.tencent.assistant.manager.notification.a;

import android.graphics.Bitmap;
import android.widget.RemoteViews;
import com.tencent.assistant.manager.notification.a.a.e;

/* compiled from: ProGuard */
class l implements e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RemoteViews f1558a;
    final /* synthetic */ int[] b;
    final /* synthetic */ int c;
    final /* synthetic */ k d;

    l(k kVar, RemoteViews remoteViews, int[] iArr, int i) {
        this.d = kVar;
        this.f1558a = remoteViews;
        this.b = iArr;
        this.c = i;
    }

    public void a(Bitmap bitmap) {
        if (bitmap != null && !bitmap.isRecycled()) {
            this.f1558a.setViewVisibility(this.b[this.c], 0);
            this.f1558a.setImageViewBitmap(this.b[this.c], bitmap);
        }
    }
}
