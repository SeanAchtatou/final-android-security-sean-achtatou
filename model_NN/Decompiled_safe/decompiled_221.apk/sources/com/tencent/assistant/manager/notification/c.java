package com.tencent.assistant.manager.notification;

import android.graphics.Bitmap;
import com.tencent.assistant.m;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Bitmap f1563a;
    final /* synthetic */ b b;

    c(b bVar, Bitmap bitmap) {
        this.b = bVar;
        this.f1563a = bitmap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.notification.v.a(int, android.app.Notification, int, boolean):void
     arg types: [int, android.app.Notification, int, int]
     candidates:
      com.tencent.assistant.manager.notification.v.a(int, com.tencent.assistant.protocol.jce.PushInfo, byte[], boolean):void
      com.tencent.assistant.manager.notification.v.a(int, android.app.Notification, int, boolean):void */
    public void run() {
        v.a().a(115, this.b.b.a(this.b.f1562a.downloadTicket, this.f1563a), 0, true);
        this.b.b.a(0);
        m.a().g(Constants.STR_EMPTY);
    }
}
