package com.tencent.assistant.manager.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.StartPopWindowActivity;
import com.tencent.assistant.b.a;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.txscrollview.TXTabBarLayout;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.DownloadInfoWrapper;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.link.b;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.manager.ba;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse;
import com.tencent.assistant.protocol.jce.PushInfo;
import com.tencent.assistant.service.DownloadingService;
import com.tencent.assistant.service.SelfUpdateService;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bt;
import com.tencent.assistantv2.activity.MainActivity;
import com.tencent.assistantv2.mediadownload.o;
import com.tencent.assistantv2.model.d;
import com.tencent.assistantv2.model.h;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.c;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class v {
    private static v c = null;

    /* renamed from: a  reason: collision with root package name */
    NotificationManager f1581a = ((NotificationManager) this.b.getSystemService("notification"));
    private Context b = AstApp.i();
    private long d = 0;
    private long e = 0;
    /* access modifiers changed from: private */
    public t f;
    private ApkResCallback.Stub g = new x(this);

    private v() {
    }

    public static synchronized v a() {
        v vVar;
        synchronized (v.class) {
            if (c == null) {
                c = new v();
            }
            vVar = c;
        }
        return vVar;
    }

    public void b() {
        DownloadingService.a(this.b);
        try {
            this.f1581a.cancelAll();
        } catch (NullPointerException e2) {
            e2.printStackTrace();
        }
    }

    public void a(int i) {
        try {
            this.f1581a.cancel(i);
        } catch (NullPointerException e2) {
            e2.printStackTrace();
        }
    }

    public void a(int i, PushInfo pushInfo, byte[] bArr, boolean z) {
        o.a().a(i, pushInfo, bArr, z);
    }

    public void a(int i, PushInfo pushInfo, byte[] bArr, boolean z, String str) {
        o.a().a(i, pushInfo, bArr, z, str);
    }

    public void b(int i, PushInfo pushInfo, byte[] bArr, boolean z) {
        o.a().a(i, pushInfo, bArr, z);
    }

    public void b(int i) {
        String string = this.b.getResources().getString(R.string.app_name);
        String str = null;
        if (i == 0) {
            str = this.b.getResources().getString(R.string.photo_backup_start);
        } else if (i == 1) {
            str = this.b.getResources().getString(R.string.photo_backup_success);
        } else if (i == -1) {
            str = this.b.getResources().getString(R.string.photo_backup_failed);
        }
        Intent intent = new Intent(this.b, NotificationService.class);
        intent.putExtra("notification_id", (int) NormalErrorRecommendPage.ERROR_TYPE_SEARCH_RESULT_EMPTY);
        a((int) NormalErrorRecommendPage.ERROR_TYPE_SEARCH_RESULT_EMPTY, y.a(this.b, string, str, str, PendingIntent.getService(this.b, NormalErrorRecommendPage.ERROR_TYPE_SEARCH_RESULT_EMPTY, intent, 268435456), true, false));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.notification.v.a(int, android.app.Notification, int, boolean):void
     arg types: [int, android.app.Notification, int, int]
     candidates:
      com.tencent.assistant.manager.notification.v.a(int, com.tencent.assistant.protocol.jce.PushInfo, byte[], boolean):void
      com.tencent.assistant.manager.notification.v.a(int, android.app.Notification, int, boolean):void */
    public void a(List<LocalApkInfo> list) {
        String str;
        if (m.a().s() && list != null && list.size() != 0) {
            String string = this.b.getResources().getString(R.string.space_clean_use_less_push_title);
            if (list.size() == 1) {
                str = list.get(0).mAppName + this.b.getResources().getString(R.string.space_clean_single_app_not_use);
            } else {
                str = list.size() + this.b.getResources().getString(R.string.space_clean_multi_app_not_use);
            }
            Intent intent = new Intent(this.b, NotificationService.class);
            intent.putExtra("notification_id", 116);
            a(116, y.a(this.b, string, str, str, PendingIntent.getService(this.b, 116, intent, 268435456), true, false), 0, true);
        }
    }

    public void a(int i, long j) {
        String string;
        String format;
        int i2;
        if (m.a().s()) {
            String a2 = bt.a(j, 0);
            if (i == 0) {
                string = this.b.getResources().getString(R.string.space_clean_not_enough_space_title);
                format = this.b.getResources().getString(R.string.space_clean_not_enough_space_text);
                i2 = 7;
            } else if (i == 1) {
                string = this.b.getResources().getString(R.string.space_clean_too_much_rubbish_title);
                format = String.format(this.b.getResources().getString(R.string.space_clean_too_much_rubbish), a2);
                i2 = 6;
            } else if (i == 2) {
                string = this.b.getResources().getString(R.string.space_clean_too_much_rubbish_title);
                format = String.format(this.b.getResources().getString(R.string.space_clean_too_much_apks), a2);
                i2 = 8;
            } else {
                return;
            }
            Intent intent = new Intent(this.b, NotificationService.class);
            intent.putExtra("notification_id", 117);
            intent.putExtra("notification_push_sub_type", i2);
            a(117, y.a(this.b, string, format, format, PendingIntent.getService(this.b, 117, intent, 268435456), true, false));
            c.a(117, i2, true);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
     arg types: [java.lang.String, java.lang.String]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent} */
    public void a(boolean z) {
        int i;
        String string;
        String string2;
        ArrayList<DownloadInfo> a2 = DownloadProxy.a().a(true);
        List<h> b2 = o.c().b();
        List<d> b3 = com.tencent.assistantv2.mediadownload.c.c().b();
        int size = a2 != null ? a2.size() : 0;
        if (b2 != null) {
            size += b2.size();
        }
        if (b3 != null) {
            i = size + b3.size();
        } else {
            i = size;
        }
        if (i <= 0) {
            try {
                Intent intent = new Intent(this.b, DownloadingService.class);
                intent.putExtra("born", false);
                this.b.startService(intent);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        } else {
            if (i == 1) {
                String str = Constants.STR_EMPTY;
                if (a2 != null && a2.size() > 0) {
                    DownloadInfo downloadInfo = a2.get(0);
                    if (downloadInfo == null || downloadInfo.packageName == null || !downloadInfo.packageName.equals(AstApp.i().getPackageName())) {
                        str = downloadInfo.name;
                    } else if (!z) {
                        a(false, true, SimpleDownloadInfo.getPercent(downloadInfo));
                        return;
                    } else {
                        a(true, true, SimpleDownloadInfo.getPercent(downloadInfo));
                        return;
                    }
                } else if (b2 != null && b2.size() > 0) {
                    str = b2.get(0).n;
                } else if (b3 != null && b3.size() > 0) {
                    str = b3.get(0).g();
                }
                string = this.b.getResources().getString(R.string.downloading_notification_one_title, str);
            } else {
                string = this.b.getResources().getString(R.string.downloading_notification_more_title, Integer.valueOf(i));
            }
            StringBuilder sb = new StringBuilder();
            if (i > 1) {
                ArrayList arrayList = new ArrayList(i);
                if (a2 != null && a2.size() > 0) {
                    Iterator<DownloadInfo> it = a2.iterator();
                    while (it.hasNext()) {
                        arrayList.add(new DownloadInfoWrapper(it.next()));
                    }
                }
                if (b2 != null && b2.size() > 0) {
                    for (h downloadInfoWrapper : b2) {
                        arrayList.add(new DownloadInfoWrapper(downloadInfoWrapper));
                    }
                }
                if (b3 != null && b3.size() > 0) {
                    for (d downloadInfoWrapper2 : b3) {
                        arrayList.add(new DownloadInfoWrapper(downloadInfoWrapper2));
                    }
                }
                Collections.sort(arrayList, new w(this));
                for (int i2 = 0; i2 < i; i2++) {
                    DownloadInfoWrapper downloadInfoWrapper3 = (DownloadInfoWrapper) arrayList.get(i2);
                    if (downloadInfoWrapper3 != null) {
                        String e3 = downloadInfoWrapper3.e();
                        if (!TextUtils.isEmpty(e3)) {
                            sb.append(e3);
                            if (i2 != i - 1) {
                                sb.append(" 、");
                            }
                        }
                    }
                    if (!(downloadInfoWrapper3 == null || downloadInfoWrapper3.f1246a != DownloadInfoWrapper.InfoType.App || downloadInfoWrapper3.b == null || downloadInfoWrapper3.b.packageName == null || !downloadInfoWrapper3.b.packageName.equals(AstApp.i().getPackageName()))) {
                        if (!z) {
                            a(false, true, SimpleDownloadInfo.getPercent(downloadInfoWrapper3.b));
                        } else {
                            a(true, true, SimpleDownloadInfo.getPercent(downloadInfoWrapper3.b));
                        }
                    }
                }
                string2 = sb.toString();
            } else {
                string2 = this.b.getResources().getString(R.string.downloading_notification_content);
            }
            this.f1581a.cancel(112);
            this.f1581a.cancel(109);
            this.f1581a.cancel(113);
            Intent intent2 = new Intent(this.b, DownloadingService.class);
            intent2.putExtra("born", true);
            intent2.putExtra("title", (CharSequence) string);
            intent2.putExtra("content", string2);
            intent2.putExtra("ticker", (CharSequence) string);
            intent2.putExtra("notification_id", (int) TXTabBarLayout.TABITEM_TIPS_TEXT_ID);
            try {
                this.b.startService(intent2);
            } catch (SecurityException e4) {
            }
        }
    }

    public void a(String str, boolean z) {
        if (TextUtils.isEmpty(str) || !z) {
            this.f1581a.cancel(106);
            return;
        }
        String string = this.b.getResources().getString(R.string.silent_install_notification_title, str);
        Notification a2 = y.a(this.b, string, null, string, PendingIntent.getActivity(this.b, 106, new Intent(), 268435456), false, true);
        if (a2 != null) {
            this.f1581a.cancel(112);
            a(106, a2);
        }
    }

    public void b(boolean z) {
        String string;
        String str;
        String str2 = null;
        if (this.f == null || this.f.b()) {
            this.f1581a.cancel(107);
            return;
        }
        this.f1581a.cancel(112);
        this.f1581a.cancel(107);
        String d2 = this.f.d();
        int a2 = this.f.a();
        if (a2 == 1) {
            str = this.f.e();
            string = this.b.getResources().getString(R.string.silent_install_suc_notification_single_title, d2);
            d2 = this.b.getResources().getString(R.string.silent_install_suc_notification_single_content);
        } else {
            string = this.b.getResources().getString(R.string.silent_install_suc_notification_more_title, Integer.valueOf(a2));
            str = null;
        }
        if (z) {
            str2 = string;
        }
        Intent intent = new Intent(this.b, NotificationService.class);
        intent.putExtra("notification_id", 107);
        intent.putExtra("notification_data", str);
        PendingIntent service = PendingIntent.getService(this.b, 107, intent, 268435456);
        Intent intent2 = new Intent(this.b, NotificationService.class);
        intent2.putExtra("notification_id", 107);
        a(107, y.a(this.b, R.drawable.notification_manual_icon, string, d2, str2, System.currentTimeMillis(), service, PendingIntent.getService(this.b, 0, intent2, 268435456), true, false));
    }

    public void a(boolean z, int i) {
        if (!z || i <= 0) {
            this.f1581a.cancel(109);
            return;
        }
        this.f1581a.cancel(112);
        this.f1581a.cancel(113);
        String string = this.b.getResources().getString(R.string.fail_notification_title, Integer.valueOf(i));
        String string2 = this.b.getResources().getString(R.string.fail_notification_network_alter_content);
        Intent intent = new Intent(this.b, NotificationService.class);
        intent.putExtra("notification_id", 109);
        a(109, y.a(this.b, string, string2, string, PendingIntent.getService(this.b, 109, intent, 268435456), true, false));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private void d() {
        Intent intent = new Intent(this.b, DownloadingService.class);
        intent.putExtra("born", false);
        try {
            this.b.startService(intent);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void b(boolean z, int i) {
        String str;
        if (i <= 0) {
            try {
                this.f1581a.cancel(113);
            } catch (NullPointerException e2) {
                e2.printStackTrace();
            }
        } else {
            d();
            this.f1581a.cancel(112);
            this.f1581a.cancel(109);
            String string = this.b.getResources().getString(R.string.fail_notification_title, Integer.valueOf(i));
            Intent intent = new Intent(this.b, NotificationService.class);
            intent.putExtra("notification_id", 113);
            PendingIntent service = PendingIntent.getService(this.b, 113, intent, 268435456);
            if (z) {
                str = string;
            } else {
                str = null;
            }
            a(113, y.a(this.b, string, null, str, service, true, false));
        }
    }

    public boolean c(int i) {
        this.e = System.currentTimeMillis();
        if (this.d != 0 && this.e - this.d < 700 && i < 95 && i > 0) {
            return false;
        }
        this.d = this.e;
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
     arg types: [java.lang.String, java.lang.String]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent} */
    public void a(boolean z, boolean z2, int i) {
        DownloadInfo downloadInfo;
        String string;
        String string2;
        int i2;
        boolean z3 = false;
        if (!z) {
            Intent intent = new Intent(this.b, SelfUpdateService.class);
            intent.putExtra("born", false);
            try {
                this.b.startService(intent);
            } catch (SecurityException e2) {
            }
        } else {
            List<DownloadInfo> e3 = DownloadProxy.a().e(AstApp.i().getPackageName());
            if (e3 != null && !e3.isEmpty() && (downloadInfo = e3.get(0)) != null) {
                if (downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.FAIL || downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.PAUSED) {
                    z3 = true;
                }
                if (z3) {
                    string = this.b.getResources().getString(R.string.selfupdate_notification_pause);
                } else if (i > 0 && i < 100) {
                    string = this.b.getResources().getString(R.string.selfupdate_notification_msg) + "  " + String.valueOf(i) + "%";
                } else if ((i == 100 || i == 0) && downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.SUCC) {
                    string = this.b.getResources().getString(R.string.selfupdate_notification_succ);
                } else {
                    string = this.b.getResources().getString(R.string.selfupdate_notification_msg);
                }
                if ((i == 100 || i == 0) && downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.SUCC) {
                    string2 = this.b.getResources().getString(R.string.sleftupdate_clickinstall);
                } else {
                    string2 = this.b.getResources().getString(R.string.downloading_notification_content);
                }
                if (i == 100 || downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.SUCC) {
                    i2 = 114;
                } else {
                    i2 = 105;
                }
                String str = z2 ? string : null;
                Intent intent2 = new Intent(this.b, SelfUpdateService.class);
                intent2.putExtra("born", true);
                intent2.putExtra("title", (CharSequence) string);
                intent2.putExtra("content", (CharSequence) string2);
                intent2.putExtra("ticker", (CharSequence) str);
                intent2.putExtra("notification_id", i2);
                try {
                    this.b.startService(intent2);
                } catch (SecurityException e4) {
                }
            }
        }
    }

    public void c(boolean z) {
        if (!z) {
            this.f1581a.cancel(115);
            return;
        }
        PushInfo c2 = ba.a().c();
        if (c2 == null || TextUtils.isEmpty(c2.b())) {
            c.a(115, c2, (byte[]) null, 3);
            this.f1581a.cancel(115);
            return;
        }
        String b2 = c2.b();
        String c3 = c2.c();
        if (c2.d() != null && !TextUtils.isEmpty(c2.d().a())) {
            if (!b.a(this.b, new Intent("android.intent.action.VIEW", Uri.parse(c2.d().a())))) {
                c.a(115, c2, (byte[]) null, 3);
                this.f1581a.cancel(115);
                return;
            }
        }
        Intent intent = new Intent(this.b, NotificationService.class);
        intent.putExtra("notification_id", 115);
        if (c2.d() != null) {
            intent.putExtra("notification_action", c2.d());
        }
        intent.putExtra("notification_push_id", c2.f2265a);
        intent.putExtra("notification_push_type", c.a(115, c2));
        intent.putExtra("notification_push_extra", c.a(c2, 0));
        Notification a2 = y.a(this.b, b2, c3, b2, PendingIntent.getService(this.b, 115, intent, 268435456), true, false);
        if (a2 != null) {
            a(115, a2);
            c.a(115, c2, (byte[]) null, 0);
            return;
        }
        c.a(115, c2, (byte[]) null, 3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.notification.v.a(int, android.app.Notification, int, boolean):void
     arg types: [int, android.app.Notification, int, int]
     candidates:
      com.tencent.assistant.manager.notification.v.a(int, com.tencent.assistant.protocol.jce.PushInfo, byte[], boolean):void
      com.tencent.assistant.manager.notification.v.a(int, android.app.Notification, int, boolean):void */
    public void a(GetPhoneUserAppListResponse getPhoneUserAppListResponse, boolean z) {
        String str;
        String str2;
        PendingIntent activity;
        this.f1581a.cancel(122);
        if (getPhoneUserAppListResponse.b == 1 || getPhoneUserAppListResponse.b == 3) {
            str2 = this.b.getResources().getString(R.string.pop_up_tip_notification_title);
            str = this.b.getResources().getString(R.string.pop_up_tip_notification_content);
        } else if (getPhoneUserAppListResponse.b == 2) {
            str2 = this.b.getResources().getString(R.string.pop_up_tip_notification_title_2);
            str = this.b.getResources().getString(R.string.pop_up_tip_notification_content_2);
        } else {
            str2 = this.b.getResources().getString(R.string.pop_up_tip_notification_title_3);
            str = this.b.getResources().getString(R.string.pop_up_tip_notification_content_3);
        }
        if (!z) {
            Intent intent = new Intent(this.b, StartPopWindowActivity.class);
            intent.addFlags(268435456);
            intent.putExtra(a.G, true);
            intent.putExtra("extra_pop_info", getPhoneUserAppListResponse);
            intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, -1);
            intent.putExtra("extra_is_recover", true);
            activity = PendingIntent.getActivity(this.b, 122, intent, 268435456);
        } else {
            Intent intent2 = new Intent(this.b, MainActivity.class);
            intent2.putExtra(a.G, true);
            intent2.putExtra("action_key_push_huanji", true);
            XLog.e("zhangyuanchao", "-------isFromPush:-------" + intent2.getBooleanExtra("action_key_push_huanji", false));
            activity = PendingIntent.getActivity(this.b, 122, intent2, 268435456);
        }
        Notification a2 = y.a(this.b, str2, str, str2, activity, true, false);
        if (a2 != null) {
            a(122, a2, 14, true);
        }
    }

    public void c() {
        if (this.f != null) {
            this.f.c();
            this.f = null;
        }
    }

    public void a(String str, String str2) {
        if (this.f == null) {
            this.f = new t();
        }
        this.f.a(str, str2);
    }

    public synchronized void a(int i, Notification notification) {
        if (notification != null) {
            try {
                this.f1581a.cancel(i);
                this.f1581a.notify(i, notification);
            } catch (Throwable th) {
                a(i, Constants.STR_EMPTY + th);
            }
        }
        return;
    }

    public void a(int i, Notification notification, int i2, boolean z) {
        a(i, notification);
        if (z) {
            c.a(i, i2, z);
        }
    }

    public void a(int i, String str) {
        int i2;
        if (i == 112 || i == 115) {
            i2 = 1000;
        } else {
            i2 = 1001;
        }
        k.a(i2, str);
    }

    public static int d(int i) {
        return i + 1000;
    }
}
