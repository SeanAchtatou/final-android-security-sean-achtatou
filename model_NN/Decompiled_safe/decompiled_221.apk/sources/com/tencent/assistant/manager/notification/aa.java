package com.tencent.assistant.manager.notification;

import android.app.NotificationManager;
import android.graphics.Bitmap;
import com.qq.AppService.AstApp;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.manager.notification.a.a.e;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ba;

/* compiled from: ProGuard */
class aa implements e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfo f1560a;
    final /* synthetic */ z b;

    aa(z zVar, DownloadInfo downloadInfo) {
        this.b = zVar;
        this.f1560a = downloadInfo;
    }

    public void a(Bitmap bitmap) {
        if (bitmap != null && !bitmap.isRecycled()) {
            XLog.d("SubscriptionDownload", "LoadImageFinish SUCC ");
            ((NotificationManager) AstApp.i().getSystemService("notification")).cancel(115);
            ba.a().post(new ab(this, bitmap));
        }
    }
}
