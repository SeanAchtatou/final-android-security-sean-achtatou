package com.tencent.assistant.manager;

import android.text.TextUtils;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.ao;
import java.util.ArrayList;

/* compiled from: ProGuard */
class ak extends ApkResCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadProxy f1482a;

    private ak(DownloadProxy downloadProxy) {
        this.f1482a = downloadProxy;
    }

    /* synthetic */ ak(DownloadProxy downloadProxy, w wVar) {
        this(downloadProxy);
    }

    public void onInstalledApkDataChanged(LocalApkInfo localApkInfo, int i) {
        if (localApkInfo != null && !TextUtils.isEmpty(localApkInfo.mPackageName)) {
            ao a2 = ao.a();
            if (a2.c(localApkInfo.mPackageName)) {
                TemporaryThreadManager.get().start(new al(this, localApkInfo, a2));
            }
            ArrayList<DownloadInfo> a3 = DownloadProxy.a().a(SimpleDownloadInfo.DownloadType.APK);
            if (a3 == null) {
                return;
            }
            if (i == 1) {
                if (m.a().l()) {
                    String unused = this.f1482a.c(localApkInfo, a3);
                }
                String b = this.f1482a.a(localApkInfo, a3);
                this.f1482a.b(localApkInfo, a3);
                this.f1482a.d.sendMessage(this.f1482a.d.obtainMessage(1013, b));
                this.f1482a.d.sendMessage(this.f1482a.d.obtainMessage(EventDispatcherEnum.UI_EVENT_APP_INSTALL, localApkInfo.mPackageName));
            } else if (i == 2) {
                this.f1482a.d.sendMessage(this.f1482a.d.obtainMessage(EventDispatcherEnum.UI_EVENT_APP_UNINSTALL, localApkInfo.mPackageName));
            }
        }
    }

    public void onDeleteApkSuccess(LocalApkInfo localApkInfo) {
        String ddownloadTicket = ApkResourceManager.getInstance().getDdownloadTicket(localApkInfo);
        if (TextUtils.isEmpty(ddownloadTicket)) {
            this.f1482a.d.sendMessage(this.f1482a.d.obtainMessage(EventDispatcherEnum.UI_EVENT_APK_DELETE, ddownloadTicket));
        }
    }
}
