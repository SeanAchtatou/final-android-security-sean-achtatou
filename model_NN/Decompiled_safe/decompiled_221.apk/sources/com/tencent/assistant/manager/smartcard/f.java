package com.tencent.assistant.manager.smartcard;

import com.tencent.assistant.model.a.h;
import com.tencent.assistant.model.a.i;
import java.util.List;

/* compiled from: ProGuard */
public class f extends y {
    public boolean a(i iVar, List<Long> list) {
        if (iVar == null || !(iVar instanceof h)) {
            return false;
        }
        h hVar = (h) iVar;
        if (!((Boolean) b(iVar).first).booleanValue()) {
            return false;
        }
        if (hVar.b() < 5) {
            return false;
        }
        return true;
    }
}
