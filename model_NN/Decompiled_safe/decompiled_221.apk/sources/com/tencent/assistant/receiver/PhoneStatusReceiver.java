package com.tencent.assistant.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import com.tencent.assistant.module.wisedownload.i;
import com.tencent.assistant.utils.r;

/* compiled from: ProGuard */
public class PhoneStatusReceiver extends BroadcastReceiver {
    private static volatile Handler b;

    /* renamed from: a  reason: collision with root package name */
    private volatile Looper f2446a;

    public PhoneStatusReceiver() {
        HandlerThread handlerThread = new HandlerThread("IntentService[WiseDownloadMessageQueue]");
        handlerThread.start();
        this.f2446a = handlerThread.getLooper();
        b = new Handler(this.f2446a);
    }

    public void onReceive(Context context, Intent intent) {
        a(intent);
    }

    public void a(Intent intent) {
        b.post(new c(this, intent));
    }

    public void b(Intent intent) {
        if (intent != null) {
            String action = intent.getAction();
            if ("android.intent.action.BATTERY_CHANGED".equals(action)) {
                i.a().a(intent);
            } else if ("android.intent.action.SCREEN_ON".equals(action)) {
                i.a().b(intent);
                r.c("screenOn");
            } else if ("android.intent.action.SCREEN_OFF".equals(action)) {
                i.a().c(intent);
            } else if ("android.intent.action.USER_PRESENT".equals(action)) {
                i.a().d(intent);
            } else if ("ACTION_ON_CONNECTIVITY_CHANGE".equals(action)) {
                i.a().m();
            } else if ("ACTION_ON_CONNECTED".equals(action)) {
                i.a().k();
            } else if ("ACTION_ON_DISCONNECTED".equals(action)) {
                i.a().l();
            } else if ("ACTION_ON_TIME_POINT".equals(action)) {
                i.a().h();
            } else if ("ACTION_ON_DOWNLOAD_SUCC".equals(action)) {
                i.a().i();
            } else if ("ACTION_ON_DOWNLOAD_FAIL".equals(action)) {
                i.a().j();
            }
        }
    }
}
