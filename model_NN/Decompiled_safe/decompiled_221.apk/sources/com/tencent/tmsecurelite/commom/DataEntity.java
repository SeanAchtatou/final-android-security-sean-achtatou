package com.tencent.tmsecurelite.commom;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public final class DataEntity extends JSONObject implements Parcelable {
    public static final Parcelable.Creator<DataEntity> CREATOR = new a();

    public DataEntity() {
    }

    public DataEntity(Parcel parcel) {
        super(parcel.readString());
    }

    public static ArrayList<DataEntity> readFromParcel(Parcel parcel) {
        ArrayList<DataEntity> arrayList = new ArrayList<>();
        try {
            int readInt = parcel.readInt();
            arrayList.ensureCapacity(readInt);
            for (int i = 0; i < readInt; i++) {
                arrayList.add(i, new DataEntity(parcel));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return arrayList;
    }

    public static DataEntity readDataFromParcel(Parcel parcel) {
        try {
            return new DataEntity(parcel);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void writeToParcel(List<DataEntity> list, Parcel parcel) {
        parcel.writeInt(list.size());
        for (DataEntity writeToParcel : list) {
            writeToParcel.writeToParcel(parcel, 0);
        }
    }

    public static void writeToParcel(DataEntity dataEntity, Parcel parcel) {
        dataEntity.writeToParcel(parcel, 0);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(toString());
    }
}
