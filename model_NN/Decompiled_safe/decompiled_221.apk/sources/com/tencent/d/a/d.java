package com.tencent.d.a;

import com.qq.taf.jce.JceStruct;

/* compiled from: ProGuard */
class d extends b {
    static final /* synthetic */ boolean g;
    private static final byte[] h = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
    private static final byte[] i = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
    int c;
    public final boolean d;
    public final boolean e;
    public final boolean f;
    private final byte[] j;
    private int k;
    private final byte[] l;

    static {
        boolean z;
        if (!a.class.desiredAssertionStatus()) {
            z = true;
        } else {
            z = false;
        }
        g = z;
    }

    public d(int i2, byte[] bArr) {
        boolean z;
        boolean z2 = true;
        this.f3573a = bArr;
        this.d = (i2 & 1) == 0;
        if ((i2 & 2) == 0) {
            z = true;
        } else {
            z = false;
        }
        this.e = z;
        this.f = (i2 & 4) == 0 ? false : z2;
        this.l = (i2 & 8) == 0 ? h : i;
        this.j = new byte[2];
        this.c = 0;
        this.k = this.e ? 19 : -1;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean a(byte[] bArr, int i2, int i3, boolean z) {
        int i4;
        int i5;
        int i6;
        int i7;
        byte b;
        int i8;
        byte b2;
        int i9;
        byte b3;
        int i10;
        int i11;
        int i12;
        int i13;
        byte[] bArr2 = this.l;
        byte[] bArr3 = this.f3573a;
        int i14 = 0;
        int i15 = this.k;
        int i16 = i3 + i2;
        byte b4 = -1;
        switch (this.c) {
            case 0:
                i4 = i2;
                break;
            case 1:
                if (i2 + 2 <= i16) {
                    int i17 = i2 + 1;
                    b4 = ((this.j[0] & 255) << 16) | ((bArr[i2] & 255) << 8) | (bArr[i17] & 255);
                    this.c = 0;
                    i4 = i17 + 1;
                    break;
                }
                i4 = i2;
                break;
            case 2:
                if (i2 + 1 <= i16) {
                    i4 = i2 + 1;
                    b4 = ((this.j[0] & 255) << 16) | ((this.j[1] & 255) << 8) | (bArr[i2] & 255);
                    this.c = 0;
                    break;
                }
                i4 = i2;
                break;
            default:
                i4 = i2;
                break;
        }
        if (b4 != -1) {
            bArr3[0] = bArr2[(b4 >> 18) & 63];
            bArr3[1] = bArr2[(b4 >> JceStruct.ZERO_TAG) & 63];
            bArr3[2] = bArr2[(b4 >> 6) & 63];
            i14 = 4;
            bArr3[3] = bArr2[b4 & 63];
            i15--;
            if (i15 == 0) {
                if (this.f) {
                    i13 = 5;
                    bArr3[4] = JceStruct.SIMPLE_LIST;
                } else {
                    i13 = 4;
                }
                i14 = i13 + 1;
                bArr3[i13] = 10;
                i15 = 19;
            }
        }
        while (true) {
            int i18 = i6;
            int i19 = i5;
            if (i4 + 3 > i16) {
                if (z) {
                    if (i4 - this.c == i16 - 1) {
                        if (this.c > 0) {
                            i11 = 1;
                            b3 = this.j[0];
                            i10 = i4;
                        } else {
                            b3 = bArr[i4];
                            i10 = i4 + 1;
                            i11 = 0;
                        }
                        int i20 = (b3 & 255) << 4;
                        this.c -= i11;
                        int i21 = i19 + 1;
                        bArr3[i19] = bArr2[(i20 >> 6) & 63];
                        int i22 = i21 + 1;
                        bArr3[i21] = bArr2[i20 & 63];
                        if (this.d) {
                            int i23 = i22 + 1;
                            bArr3[i22] = 61;
                            i22 = i23 + 1;
                            bArr3[i23] = 61;
                        }
                        if (this.e) {
                            if (this.f) {
                                bArr3[i22] = JceStruct.SIMPLE_LIST;
                                i22++;
                            }
                            i19 = i22 + 1;
                            bArr3[i22] = 10;
                            i4 = i10;
                        } else {
                            i4 = i10;
                            i19 = i22;
                        }
                    } else if (i4 - this.c == i16 - 2) {
                        if (this.c > 1) {
                            i8 = 1;
                            b = this.j[0];
                        } else {
                            b = bArr[i4];
                            i4++;
                            i8 = 0;
                        }
                        int i24 = (b & 255) << 10;
                        if (this.c > 0) {
                            b2 = this.j[i8];
                            i8++;
                        } else {
                            b2 = bArr[i4];
                            i4++;
                        }
                        int i25 = ((b2 & 255) << 2) | i24;
                        this.c -= i8;
                        int i26 = i19 + 1;
                        bArr3[i19] = bArr2[(i25 >> 12) & 63];
                        int i27 = i26 + 1;
                        bArr3[i26] = bArr2[(i25 >> 6) & 63];
                        int i28 = i27 + 1;
                        bArr3[i27] = bArr2[i25 & 63];
                        if (this.d) {
                            i9 = i28 + 1;
                            bArr3[i28] = 61;
                        } else {
                            i9 = i28;
                        }
                        if (this.e) {
                            if (this.f) {
                                bArr3[i9] = JceStruct.SIMPLE_LIST;
                                i9++;
                            }
                            i19 = i9 + 1;
                            bArr3[i9] = 10;
                        } else {
                            i19 = i9;
                        }
                    } else if (this.e && i19 > 0 && i18 != 19) {
                        if (this.f) {
                            i7 = i19 + 1;
                            bArr3[i19] = JceStruct.SIMPLE_LIST;
                        } else {
                            i7 = i19;
                        }
                        i19 = i7 + 1;
                        bArr3[i7] = 10;
                    }
                    if (!g && this.c != 0) {
                        throw new AssertionError();
                    } else if (!g && i4 != i16) {
                        throw new AssertionError();
                    }
                } else if (i4 == i16 - 1) {
                    byte[] bArr4 = this.j;
                    int i29 = this.c;
                    this.c = i29 + 1;
                    bArr4[i29] = bArr[i4];
                } else if (i4 == i16 - 2) {
                    byte[] bArr5 = this.j;
                    int i30 = this.c;
                    this.c = i30 + 1;
                    bArr5[i30] = bArr[i4];
                    byte[] bArr6 = this.j;
                    int i31 = this.c;
                    this.c = i31 + 1;
                    bArr6[i31] = bArr[i4 + 1];
                }
                this.b = i19;
                this.k = i18;
                return true;
            }
            byte b5 = ((bArr[i4] & 255) << 16) | ((bArr[i4 + 1] & 255) << 8) | (bArr[i4 + 2] & 255);
            bArr3[i19] = bArr2[(b5 >> 18) & 63];
            bArr3[i19 + 1] = bArr2[(b5 >> JceStruct.ZERO_TAG) & 63];
            bArr3[i19 + 2] = bArr2[(b5 >> 6) & 63];
            bArr3[i19 + 3] = bArr2[b5 & 63];
            i4 += 3;
            i5 = i19 + 4;
            i6 = i18 - 1;
            if (i6 == 0) {
                if (this.f) {
                    i12 = i5 + 1;
                    bArr3[i5] = JceStruct.SIMPLE_LIST;
                } else {
                    i12 = i5;
                }
                i5 = i12 + 1;
                bArr3[i12] = 10;
                i6 = 19;
            }
        }
    }
}
