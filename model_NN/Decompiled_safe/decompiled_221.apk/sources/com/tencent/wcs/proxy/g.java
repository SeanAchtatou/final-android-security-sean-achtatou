package com.tencent.wcs.proxy;

import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.STConstAction;
import com.tencent.assistant.utils.ba;
import com.tencent.wcs.c.b;
import com.tencent.wcs.proxy.b.e;
import com.tencent.wcs.proxy.b.h;
import com.tencent.wcs.proxy.b.k;
import com.tencent.wcs.proxy.b.n;
import com.tencent.wcs.proxy.b.q;
import com.tencent.wcs.proxy.b.t;

/* compiled from: ProGuard */
class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f3917a;

    g(c cVar) {
        this.f3917a = cVar;
    }

    public void run() {
        synchronized (this.f3917a.l) {
            if (this.f3917a.b != null) {
                this.f3917a.b.d();
                this.f3917a.g();
                this.f3917a.b.a(100);
                this.f3917a.b.a(1997);
                this.f3917a.b.a((int) STConstAction.ACTION_HIT_PAUSE);
                this.f3917a.b.a((int) STConst.ST_PAGE_NECESSARY);
                this.f3917a.b.a(1996);
                this.f3917a.b.a(1994);
                this.f3917a.b.a((int) STConstAction.ACTION_HIT_EXPAND);
            }
            if (this.f3917a.e != null) {
                this.f3917a.e.d();
            }
            if (this.f3917a.f != null) {
                this.f3917a.f.b();
            }
            e.b().a();
            h.b().a();
            n.b().a();
            q.b().a();
            t.b().a();
            k.b().a();
            ba.a().post(new h(this));
        }
        b.a("WanServiceManager stop wireless service over...");
    }
}
