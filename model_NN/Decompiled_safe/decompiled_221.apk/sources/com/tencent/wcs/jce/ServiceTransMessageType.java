package com.tencent.wcs.jce;

import java.io.Serializable;

/* compiled from: ProGuard */
public final class ServiceTransMessageType implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public static final ServiceTransMessageType f3887a = new ServiceTransMessageType(0, 0, "SERVICE_TRANS_MESSAGE_TYPE_DATA");
    public static final ServiceTransMessageType b = new ServiceTransMessageType(1, 1, "SERVICE_TRANS_MESSAGE_TYPE_CONNECT");
    public static final ServiceTransMessageType c = new ServiceTransMessageType(2, 2, "SERVICE_TRANS_MESSAGE_TYPE_CLOSE");
    public static final ServiceTransMessageType d = new ServiceTransMessageType(3, 3, "SERVICE_TRANS_MESSAGE_TYPE_PACKET_LOST");
    public static final ServiceTransMessageType e = new ServiceTransMessageType(4, 4, "SERVICE_TRANS_MESSAGE_TYPE_ACK");
    public static final ServiceTransMessageType f = new ServiceTransMessageType(5, 5, "SERVICE_TRANS_MESSAGE_TYPE_PACKET_RESEND");
    static final /* synthetic */ boolean g = (!ServiceTransMessageType.class.desiredAssertionStatus());
    private static ServiceTransMessageType[] h = new ServiceTransMessageType[6];
    private int i;
    private String j = new String();

    public String toString() {
        return this.j;
    }

    private ServiceTransMessageType(int i2, int i3, String str) {
        this.j = str;
        this.i = i3;
        h[i2] = this;
    }
}
