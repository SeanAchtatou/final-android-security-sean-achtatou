package com.tencent.wcs.agent;

import android.util.SparseArray;
import com.qq.taf.jce.JceInputStream;
import com.qq.util.k;
import com.tencent.wcs.agent.config.a;
import com.tencent.wcs.b.c;
import com.tencent.wcs.jce.FinishMessage;
import com.tencent.wcs.jce.TransDataMessage;
import com.tencent.wcs.proxy.b;
import com.tencent.wcs.proxy.b.t;
import com.tencent.wcs.proxy.c.f;
import com.tencent.wcs.proxy.e.d;
import com.tencent.wcs.proxy.l;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/* compiled from: ProGuard */
public class e implements p, b {

    /* renamed from: a  reason: collision with root package name */
    private final a f3832a;
    private final l b;
    private final com.tencent.wcs.a.b c;
    private com.tencent.wcs.f.e d;
    /* access modifiers changed from: private */
    public SparseArray<l> e = new SparseArray<>();

    public e(l lVar, com.tencent.wcs.a.b bVar, a aVar) {
        this.f3832a = aVar;
        this.b = lVar;
        this.c = bVar;
    }

    private void a(TransDataMessage transDataMessage) {
        l lVar;
        if (c.f3846a) {
            com.tencent.wcs.c.b.a("AgentServiceOriginal receive data type " + transDataMessage.g() + " , at seq " + transDataMessage.f() + " channel id " + transDataMessage.a() + " message id " + transDataMessage.e() + " size " + (transDataMessage.d() != null ? transDataMessage.d().length : 0) + " remote " + this.f3832a.a());
        }
        if (this.d == null) {
            this.d = new com.tencent.wcs.f.e(this);
            this.d.a();
        }
        int e2 = transDataMessage.e();
        synchronized (this.e) {
            lVar = this.e.get(e2);
            if (lVar == null) {
                int g = transDataMessage.g();
                if (g == 0 || g == 1) {
                    l lVar2 = new l(transDataMessage.a(), transDataMessage.e(), transDataMessage.b(), transDataMessage.c(), this.b, this.f3832a);
                    lVar2.a(this);
                    lVar2.a();
                    this.e.put(e2, lVar2);
                    if (lVar2.d() == 14089 || lVar2.d() == 14087) {
                        lVar = lVar2;
                    } else {
                        this.d.b(e2);
                        lVar = lVar2;
                    }
                } else if (!(g == 2 || g == 4 || g == 5 || g != 3)) {
                    new l(transDataMessage.a(), transDataMessage.e(), transDataMessage.b(), transDataMessage.c(), this.b, this.f3832a).a(null, transDataMessage.f(), 5);
                }
            }
        }
        if (lVar != null) {
            switch (transDataMessage.g()) {
                case 3:
                    lVar.a(transDataMessage.f(), transDataMessage.d());
                    t.b().b((Object) transDataMessage);
                    break;
                case 4:
                    lVar.a(transDataMessage.f());
                    t.b().b((Object) transDataMessage);
                    break;
                default:
                    lVar.a(transDataMessage);
                    break;
            }
            if (lVar.d() != 14089 && lVar.d() != 14087) {
                this.d.d(e2);
            }
        }
    }

    public void a(int i) {
        this.d.c(i);
        com.tencent.wcs.proxy.e.a.a().a(new f(this, i), 5000, TimeUnit.MILLISECONDS);
    }

    public void a(f fVar) {
        com.tencent.wcs.proxy.a.c a2;
        int i = 0;
        JceInputStream jceInputStream = new JceInputStream(fVar.c());
        TransDataMessage transDataMessage = (TransDataMessage) t.b().c((Object[]) new Void[0]);
        try {
            transDataMessage.readFrom(jceInputStream);
            if (!c.m && ((a2 = this.b.a()) == null || a2.c())) {
                if (c.f3846a) {
                    StringBuilder append = new StringBuilder().append("AgentServiceOriginal reject data caused by network limit, type ").append(transDataMessage.g()).append(" , at seq ").append(transDataMessage.f()).append(" channel id ").append(transDataMessage.a()).append(" message id ").append(transDataMessage.e()).append(" size ");
                    if (transDataMessage.d() != null) {
                        i = transDataMessage.d().length;
                    }
                    com.tencent.wcs.c.b.a(append.append(i).append(" remote ").append(this.f3832a.a()).toString());
                }
                this.b.a(1996, new FinishMessage(transDataMessage.c(), transDataMessage.b()));
                t.b().b((Object) transDataMessage);
                a();
            } else if (this.c == null || this.c.c() || this.f3832a.a(transDataMessage.a()) == 14092) {
                if (!(transDataMessage.d() == null || transDataMessage.d().length <= 0 || this.f3832a.a(transDataMessage.a()) == 14087)) {
                    try {
                        transDataMessage.a(k.b("TencentMolo&&##%%!!!1234".getBytes("UTF-8"), transDataMessage.d()));
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
                if (transDataMessage.d() != null && transDataMessage.d().length > 0 && transDataMessage.h() == 1) {
                    transDataMessage.a(d.b(transDataMessage.d()));
                }
                a(transDataMessage);
            } else {
                if (c.f3846a) {
                    StringBuilder append2 = new StringBuilder().append("AgentServiceOriginal reject data caused by security, type ").append(transDataMessage.g()).append(" , at seq ").append(transDataMessage.f()).append(" channel id ").append(transDataMessage.a()).append(" message id ").append(transDataMessage.e()).append(" size ");
                    if (transDataMessage.d() != null) {
                        i = transDataMessage.d().length;
                    }
                    com.tencent.wcs.c.b.a(append2.append(i).append(" port ").append(this.f3832a.a(transDataMessage.a())).append(" remote ").append(this.f3832a.a()).toString());
                }
                if (this.b != null) {
                    this.b.k();
                }
                t.b().b((Object) transDataMessage);
            }
        } catch (Throwable th) {
            com.tencent.wcs.c.b.a("AgentServiceOriginal jceRead failed ,cmdid " + fVar.d());
            t.b().b((Object) transDataMessage);
        }
    }

    public void a() {
        if (this.d != null) {
            this.d.b();
        }
        com.tencent.wcs.proxy.e.a.a().a(new g(this), 5000, TimeUnit.MILLISECONDS);
    }

    public void b() {
        if (this.d != null) {
            this.d.b();
        }
        ArrayList arrayList = new ArrayList();
        synchronized (this.e) {
            int size = this.e.size();
            for (int i = 0; i < size; i++) {
                l valueAt = this.e.valueAt(i);
                if (valueAt != null) {
                    arrayList.add(valueAt);
                }
            }
            this.e.clear();
        }
        int size2 = arrayList.size();
        for (int i2 = 0; i2 < size2; i2++) {
            ((l) arrayList.get(i2)).b();
            ((l) arrayList.get(i2)).a((p) null);
        }
        arrayList.clear();
    }
}
