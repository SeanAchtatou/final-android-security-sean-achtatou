package com.tencent.beacon.heatmap;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.FrameLayout;

/* compiled from: ProGuard */
public final class d extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    private final GestureDetector f3452a;

    public final boolean dispatchTouchEvent(MotionEvent motionEvent) {
        this.f3452a.onTouchEvent(motionEvent);
        return super.dispatchTouchEvent(motionEvent);
    }
}
