package com.tencent.beacon.b;

import android.content.Context;
import com.tencent.beacon.a.g;
import com.tencent.beacon.a.h;
import com.tencent.beacon.c.a.b;
import com.tencent.beacon.event.f;
import com.tencent.connect.common.Constants;
import java.io.RandomAccessFile;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;

/* compiled from: ProGuard */
public class a {
    private static a f;

    /* renamed from: a  reason: collision with root package name */
    private String f3402a;
    private String b;
    private String c;
    private String d;
    private String e;

    private a(Context context) {
        this.f3402a = Constants.STR_EMPTY;
        this.b = Constants.STR_EMPTY;
        this.c = Constants.STR_EMPTY;
        this.d = Constants.STR_EMPTY;
        this.e = Constants.STR_EMPTY;
        this.b = g.m().i();
        h.a(context);
        this.c = h.e(context);
        this.d = h.c(context);
        this.e = h.d(context);
        try {
            String b2 = com.tencent.beacon.a.a.b(context, "QIMEI_DENGTA", Constants.STR_EMPTY);
            if (b2 != null && !Constants.STR_EMPTY.equals(b2)) {
                this.f3402a = b2;
            }
        } catch (Exception e2) {
        }
    }

    public static a a(Context context) {
        if (f == null) {
            f = new a(context);
        }
        return f;
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0075 A[SYNTHETIC, Splitter:B:18:0x0075] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x008c A[SYNTHETIC, Splitter:B:25:0x008c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long a(java.lang.String r8, int r9) {
        /*
            r2 = -1
            r7 = 0
            if (r8 == 0) goto L_0x0007
            if (r9 > 0) goto L_0x0009
        L_0x0007:
            r0 = r2
        L_0x0008:
            return r0
        L_0x0009:
            r1 = 0
            long r5 = java.lang.System.currentTimeMillis()
            java.net.InetSocketAddress r0 = new java.net.InetSocketAddress
            r0.<init>(r8, r9)
            java.net.Socket r4 = new java.net.Socket     // Catch:{ Throwable -> 0x0049 }
            r4.<init>()     // Catch:{ Throwable -> 0x0049 }
            r1 = 30000(0x7530, float:4.2039E-41)
            r4.connect(r0, r1)     // Catch:{ Throwable -> 0x00a1, all -> 0x009e }
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x00a1, all -> 0x009e }
            long r0 = r0 - r5
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00a1, all -> 0x009e }
            java.lang.String r6 = " s conn:"
            r5.<init>(r6)     // Catch:{ Throwable -> 0x00a1, all -> 0x009e }
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ Throwable -> 0x00a1, all -> 0x009e }
            java.lang.String r5 = r5.toString()     // Catch:{ Throwable -> 0x00a1, all -> 0x009e }
            r6 = 0
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x00a1, all -> 0x009e }
            com.tencent.beacon.d.a.a(r5, r6)     // Catch:{ Throwable -> 0x00a1, all -> 0x009e }
            r4.close()     // Catch:{ IOException -> 0x003b }
            goto L_0x0008
        L_0x003b:
            r2 = move-exception
            r2.printStackTrace()
            java.lang.String r2 = r2.getMessage()
            java.lang.Object[] r3 = new java.lang.Object[r7]
            com.tencent.beacon.d.a.d(r2, r3)
            goto L_0x0008
        L_0x0049:
            r0 = move-exception
        L_0x004a:
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0089 }
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0089 }
            com.tencent.beacon.d.a.d(r0, r4)     // Catch:{ all -> 0x0089 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0089 }
            java.lang.String r4 = " exception:"
            r0.<init>(r4)     // Catch:{ all -> 0x0089 }
            java.lang.StringBuilder r0 = r0.append(r8)     // Catch:{ all -> 0x0089 }
            java.lang.String r4 = ":"
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ all -> 0x0089 }
            java.lang.StringBuilder r0 = r0.append(r9)     // Catch:{ all -> 0x0089 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0089 }
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0089 }
            com.tencent.beacon.d.a.b(r0, r4)     // Catch:{ all -> 0x0089 }
            if (r1 == 0) goto L_0x0078
            r1.close()     // Catch:{ IOException -> 0x007a }
        L_0x0078:
            r0 = r2
            goto L_0x0008
        L_0x007a:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.String r0 = r0.getMessage()
            java.lang.Object[] r1 = new java.lang.Object[r7]
            com.tencent.beacon.d.a.d(r0, r1)
            r0 = r2
            goto L_0x0008
        L_0x0089:
            r0 = move-exception
        L_0x008a:
            if (r1 == 0) goto L_0x008f
            r1.close()     // Catch:{ IOException -> 0x0090 }
        L_0x008f:
            throw r0
        L_0x0090:
            r1 = move-exception
            r1.printStackTrace()
            java.lang.String r1 = r1.getMessage()
            java.lang.Object[] r2 = new java.lang.Object[r7]
            com.tencent.beacon.d.a.d(r1, r2)
            goto L_0x008f
        L_0x009e:
            r0 = move-exception
            r1 = r4
            goto L_0x008a
        L_0x00a1:
            r0 = move-exception
            r1 = r4
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.beacon.b.a.a(java.lang.String, int):long");
    }

    public String a() {
        if (this.f3402a == null || Constants.STR_EMPTY.equals(this.f3402a)) {
            return this.b;
        }
        return this.f3402a;
    }

    public void a(String str) {
        this.f3402a = str;
    }

    public String b() {
        return this.b;
    }

    public String c() {
        return this.c;
    }

    public String d() {
        return this.d;
    }

    public String e() {
        return this.e;
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:95:0x005f */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:94:0x005f */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v46, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v51, resolved type: int} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x01e4 A[SYNTHETIC, Splitter:B:51:0x01e4] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01e9 A[SYNTHETIC, Splitter:B:54:0x01e9] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x01ee A[SYNTHETIC, Splitter:B:57:0x01ee] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0211 A[SYNTHETIC, Splitter:B:68:0x0211] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0216 A[SYNTHETIC, Splitter:B:71:0x0216] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x021b A[SYNTHETIC, Splitter:B:74:0x021b] */
    /* JADX WARNING: Removed duplicated region for block: B:99:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.tencent.beacon.b.d a(java.lang.String r12, boolean r13) {
        /*
            r1 = 0
            r5 = 0
            if (r12 == 0) goto L_0x000c
            java.lang.String r0 = ""
            boolean r0 = r12.equals(r0)
            if (r0 == 0) goto L_0x000e
        L_0x000c:
            r0 = r1
        L_0x000d:
            return r0
        L_0x000e:
            com.tencent.beacon.b.d r0 = new com.tencent.beacon.b.d
            r0.<init>()
            java.net.URL r6 = new java.net.URL     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            r6.<init>(r12)     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            java.lang.String r7 = r6.getHost()     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            java.net.InetAddress r4 = java.net.InetAddress.getByName(r7)     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            long r8 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            long r2 = r8 - r2
            r0.f3405a = r2     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            java.lang.String r3 = " dns: "
            r2.<init>(r3)     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            long r8 = r0.f3405a     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            java.lang.StringBuilder r2 = r2.append(r8)     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            com.tencent.beacon.d.a.a(r2, r3)     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            java.lang.StringBuffer r8 = new java.lang.StringBuffer     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            r8.<init>()     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            byte[] r9 = r4.getAddress()     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            r3 = r5
        L_0x004d:
            int r2 = r9.length     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            if (r3 >= r2) goto L_0x0071
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            java.lang.String r2 = "."
            r10.<init>(r2)     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            byte r2 = r9[r3]     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            if (r2 >= 0) goto L_0x006e
            byte r2 = r9[r3]     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            int r2 = r2 + 256
        L_0x005f:
            java.lang.StringBuilder r2 = r10.append(r2)     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            r8.append(r2)     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            int r2 = r3 + 1
            r3 = r2
            goto L_0x004d
        L_0x006e:
            byte r2 = r9[r3]     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            goto L_0x005f
        L_0x0071:
            r2 = 1
            java.lang.String r2 = r8.substring(r2)     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            r0.f = r2     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            r2 = 0
            r8.setLength(r2)     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            if (r13 != 0) goto L_0x000d
            int r2 = r6.getPort()     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            if (r2 < 0) goto L_0x01bd
        L_0x0084:
            java.net.InetSocketAddress r3 = new java.net.InetSocketAddress     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            r3.<init>(r4, r2)     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            long r8 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            java.net.Socket r4 = new java.net.Socket     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            r4.<init>()     // Catch:{ Throwable -> 0x01cf, all -> 0x020c }
            r2 = 30000(0x7530, float:4.2039E-41)
            r4.connect(r3, r2)     // Catch:{ Throwable -> 0x023f, all -> 0x0237 }
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x023f, all -> 0x0237 }
            long r2 = r2 - r8
            r0.b = r2     // Catch:{ Throwable -> 0x023f, all -> 0x0237 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x023f, all -> 0x0237 }
            java.lang.String r3 = " conn: "
            r2.<init>(r3)     // Catch:{ Throwable -> 0x023f, all -> 0x0237 }
            long r8 = r0.b     // Catch:{ Throwable -> 0x023f, all -> 0x0237 }
            java.lang.StringBuilder r2 = r2.append(r8)     // Catch:{ Throwable -> 0x023f, all -> 0x0237 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x023f, all -> 0x0237 }
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x023f, all -> 0x0237 }
            com.tencent.beacon.d.a.a(r2, r3)     // Catch:{ Throwable -> 0x023f, all -> 0x0237 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x023f, all -> 0x0237 }
            r2.<init>()     // Catch:{ Throwable -> 0x023f, all -> 0x0237 }
            java.lang.String r3 = r6.getPath()     // Catch:{ Throwable -> 0x023f, all -> 0x0237 }
            java.lang.StringBuilder r3 = r2.append(r3)     // Catch:{ Throwable -> 0x023f, all -> 0x0237 }
            java.lang.String r2 = r6.getQuery()     // Catch:{ Throwable -> 0x023f, all -> 0x0237 }
            if (r2 == 0) goto L_0x01c1
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x023f, all -> 0x0237 }
            java.lang.String r8 = "?"
            r2.<init>(r8)     // Catch:{ Throwable -> 0x023f, all -> 0x0237 }
            java.lang.String r6 = r6.getQuery()     // Catch:{ Throwable -> 0x023f, all -> 0x0237 }
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ Throwable -> 0x023f, all -> 0x0237 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x023f, all -> 0x0237 }
        L_0x00db:
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ Throwable -> 0x023f, all -> 0x0237 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x023f, all -> 0x0237 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x023f, all -> 0x0237 }
            java.lang.String r6 = "HEAD "
            r3.<init>(r6)     // Catch:{ Throwable -> 0x023f, all -> 0x0237 }
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ Throwable -> 0x023f, all -> 0x0237 }
            java.lang.String r3 = " HTTP/1.1 \r\nHost: "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x023f, all -> 0x0237 }
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ Throwable -> 0x023f, all -> 0x0237 }
            java.lang.String r3 = "\r\nConnection: close\r\n\r\n"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x023f, all -> 0x0237 }
            java.lang.String r6 = r2.toString()     // Catch:{ Throwable -> 0x023f, all -> 0x0237 }
            java.io.OutputStream r3 = r4.getOutputStream()     // Catch:{ Throwable -> 0x023f, all -> 0x0237 }
            java.io.InputStream r2 = r4.getInputStream()     // Catch:{ Throwable -> 0x0245, all -> 0x023a }
            long r7 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x024a }
            java.lang.String r1 = "UTF-8"
            byte[] r1 = r6.getBytes(r1)     // Catch:{ Throwable -> 0x024a }
            r3.write(r1)     // Catch:{ Throwable -> 0x024a }
            r3.flush()     // Catch:{ Throwable -> 0x024a }
            long r9 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x024a }
            long r6 = r9 - r7
            r0.c = r6     // Catch:{ Throwable -> 0x024a }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x024a }
            java.lang.String r6 = " re: "
            r1.<init>(r6)     // Catch:{ Throwable -> 0x024a }
            long r6 = r0.c     // Catch:{ Throwable -> 0x024a }
            java.lang.StringBuilder r1 = r1.append(r6)     // Catch:{ Throwable -> 0x024a }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x024a }
            r6 = 0
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x024a }
            com.tencent.beacon.d.a.a(r1, r6)     // Catch:{ Throwable -> 0x024a }
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x024a }
            r2.read()     // Catch:{ Throwable -> 0x024a }
            long r8 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x024a }
            long r6 = r8 - r6
            r0.d = r6     // Catch:{ Throwable -> 0x024a }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x024a }
            java.lang.String r6 = " wait: "
            r1.<init>(r6)     // Catch:{ Throwable -> 0x024a }
            long r6 = r0.d     // Catch:{ Throwable -> 0x024a }
            java.lang.StringBuilder r1 = r1.append(r6)     // Catch:{ Throwable -> 0x024a }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x024a }
            r6 = 0
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x024a }
            com.tencent.beacon.d.a.a(r1, r6)     // Catch:{ Throwable -> 0x024a }
            r1 = 500(0x1f4, float:7.0E-43)
            byte[] r1 = new byte[r1]     // Catch:{ Throwable -> 0x024a }
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x024a }
            int r1 = r2.read(r1)     // Catch:{ Throwable -> 0x024a }
            long r8 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x024a }
            long r6 = r8 - r6
            r0.e = r6     // Catch:{ Throwable -> 0x024a }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x024a }
            java.lang.String r7 = " readp: "
            r6.<init>(r7)     // Catch:{ Throwable -> 0x024a }
            long r7 = r0.e     // Catch:{ Throwable -> 0x024a }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Throwable -> 0x024a }
            java.lang.String r6 = r6.toString()     // Catch:{ Throwable -> 0x024a }
            r7 = 0
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Throwable -> 0x024a }
            com.tencent.beacon.d.a.a(r6, r7)     // Catch:{ Throwable -> 0x024a }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x024a }
            java.lang.String r7 = " datasize: "
            r6.<init>(r7)     // Catch:{ Throwable -> 0x024a }
            java.lang.StringBuilder r1 = r6.append(r1)     // Catch:{ Throwable -> 0x024a }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x024a }
            r6 = 0
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x024a }
            com.tencent.beacon.d.a.a(r1, r6)     // Catch:{ Throwable -> 0x024a }
            if (r3 == 0) goto L_0x01a4
            r3.close()     // Catch:{ IOException -> 0x01c5 }
        L_0x01a4:
            if (r2 == 0) goto L_0x01a9
            r2.close()     // Catch:{ IOException -> 0x01ca }
        L_0x01a9:
            r4.close()     // Catch:{ IOException -> 0x01ae }
            goto L_0x000d
        L_0x01ae:
            r1 = move-exception
            r1.printStackTrace()
            java.lang.String r1 = r1.getMessage()
            java.lang.Object[] r2 = new java.lang.Object[r5]
            com.tencent.beacon.d.a.d(r1, r2)
            goto L_0x000d
        L_0x01bd:
            r2 = 80
            goto L_0x0084
        L_0x01c1:
            java.lang.String r2 = ""
            goto L_0x00db
        L_0x01c5:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x01a4
        L_0x01ca:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x01a9
        L_0x01cf:
            r2 = move-exception
            r3 = r1
            r4 = r1
            r11 = r2
            r2 = r1
            r1 = r11
        L_0x01d5:
            r1.printStackTrace()     // Catch:{ all -> 0x023c }
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x023c }
            r6 = 0
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ all -> 0x023c }
            com.tencent.beacon.d.a.d(r1, r6)     // Catch:{ all -> 0x023c }
            if (r3 == 0) goto L_0x01e7
            r3.close()     // Catch:{ IOException -> 0x0202 }
        L_0x01e7:
            if (r2 == 0) goto L_0x01ec
            r2.close()     // Catch:{ IOException -> 0x0207 }
        L_0x01ec:
            if (r4 == 0) goto L_0x000d
            r4.close()     // Catch:{ IOException -> 0x01f3 }
            goto L_0x000d
        L_0x01f3:
            r1 = move-exception
            r1.printStackTrace()
            java.lang.String r1 = r1.getMessage()
            java.lang.Object[] r2 = new java.lang.Object[r5]
            com.tencent.beacon.d.a.d(r1, r2)
            goto L_0x000d
        L_0x0202:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x01e7
        L_0x0207:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x01ec
        L_0x020c:
            r0 = move-exception
            r3 = r1
            r4 = r1
        L_0x020f:
            if (r3 == 0) goto L_0x0214
            r3.close()     // Catch:{ IOException -> 0x021f }
        L_0x0214:
            if (r1 == 0) goto L_0x0219
            r1.close()     // Catch:{ IOException -> 0x0224 }
        L_0x0219:
            if (r4 == 0) goto L_0x021e
            r4.close()     // Catch:{ IOException -> 0x0229 }
        L_0x021e:
            throw r0
        L_0x021f:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0214
        L_0x0224:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0219
        L_0x0229:
            r1 = move-exception
            r1.printStackTrace()
            java.lang.String r1 = r1.getMessage()
            java.lang.Object[] r2 = new java.lang.Object[r5]
            com.tencent.beacon.d.a.d(r1, r2)
            goto L_0x021e
        L_0x0237:
            r0 = move-exception
            r3 = r1
            goto L_0x020f
        L_0x023a:
            r0 = move-exception
            goto L_0x020f
        L_0x023c:
            r0 = move-exception
            r1 = r2
            goto L_0x020f
        L_0x023f:
            r2 = move-exception
            r3 = r1
            r11 = r1
            r1 = r2
            r2 = r11
            goto L_0x01d5
        L_0x0245:
            r2 = move-exception
            r11 = r2
            r2 = r1
            r1 = r11
            goto L_0x01d5
        L_0x024a:
            r1 = move-exception
            goto L_0x01d5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.beacon.b.a.a(java.lang.String, boolean):com.tencent.beacon.b.d");
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x0067 A[SYNTHETIC, Splitter:B:35:0x0067] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] a(java.lang.Object r5) {
        /*
            r0 = 0
            r3 = 0
            java.lang.String r1 = "en obj 2 bytes "
            java.lang.Object[] r2 = new java.lang.Object[r3]
            com.tencent.beacon.d.a.b(r1, r2)
            if (r5 == 0) goto L_0x0013
            java.lang.Class<java.io.Serializable> r1 = java.io.Serializable.class
            boolean r1 = r1.isInstance(r5)
            if (r1 != 0) goto L_0x001b
        L_0x0013:
            java.lang.String r1 = "not serial obj "
            java.lang.Object[] r2 = new java.lang.Object[r3]
            com.tencent.beacon.d.a.c(r1, r2)
        L_0x001a:
            return r0
        L_0x001b:
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream
            r3.<init>()
            java.io.ObjectOutputStream r2 = new java.io.ObjectOutputStream     // Catch:{ Throwable -> 0x0040, all -> 0x0062 }
            r2.<init>(r3)     // Catch:{ Throwable -> 0x0040, all -> 0x0062 }
            r2.writeObject(r5)     // Catch:{ Throwable -> 0x007a }
            r2.flush()     // Catch:{ Throwable -> 0x007a }
            byte[] r0 = r3.toByteArray()     // Catch:{ Throwable -> 0x007a }
            r2.close()     // Catch:{ IOException -> 0x003b }
        L_0x0032:
            r3.close()     // Catch:{ IOException -> 0x0036 }
            goto L_0x001a
        L_0x0036:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x001a
        L_0x003b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0032
        L_0x0040:
            r1 = move-exception
            r2 = r0
        L_0x0042:
            r1.printStackTrace()     // Catch:{ all -> 0x0078 }
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x0078 }
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0078 }
            com.tencent.beacon.d.a.d(r1, r4)     // Catch:{ all -> 0x0078 }
            if (r2 == 0) goto L_0x0054
            r2.close()     // Catch:{ IOException -> 0x005d }
        L_0x0054:
            r3.close()     // Catch:{ IOException -> 0x0058 }
            goto L_0x001a
        L_0x0058:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x001a
        L_0x005d:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0054
        L_0x0062:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x0065:
            if (r2 == 0) goto L_0x006a
            r2.close()     // Catch:{ IOException -> 0x006e }
        L_0x006a:
            r3.close()     // Catch:{ IOException -> 0x0073 }
        L_0x006d:
            throw r0
        L_0x006e:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x006a
        L_0x0073:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x006d
        L_0x0078:
            r0 = move-exception
            goto L_0x0065
        L_0x007a:
            r1 = move-exception
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.beacon.b.a.a(java.lang.Object):byte[]");
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x0055 A[SYNTHETIC, Splitter:B:34:0x0055] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Object a(byte[] r5) {
        /*
            r2 = 0
            r0 = 0
            java.lang.String r1 = "de byte 2 obj "
            java.lang.Object[] r2 = new java.lang.Object[r2]
            com.tencent.beacon.d.a.b(r1, r2)
            if (r5 == 0) goto L_0x000e
            int r1 = r5.length
            if (r1 >= 0) goto L_0x000f
        L_0x000e:
            return r0
        L_0x000f:
            java.io.ByteArrayInputStream r3 = new java.io.ByteArrayInputStream
            r3.<init>(r5)
            java.io.ObjectInputStream r2 = new java.io.ObjectInputStream     // Catch:{ Throwable -> 0x002e, all -> 0x0050 }
            r2.<init>(r3)     // Catch:{ Throwable -> 0x002e, all -> 0x0050 }
            java.lang.Object r0 = r2.readObject()     // Catch:{ Throwable -> 0x0068 }
            r2.close()     // Catch:{ IOException -> 0x0029 }
        L_0x0020:
            r3.close()     // Catch:{ IOException -> 0x0024 }
            goto L_0x000e
        L_0x0024:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x000e
        L_0x0029:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0020
        L_0x002e:
            r1 = move-exception
            r2 = r0
        L_0x0030:
            r1.printStackTrace()     // Catch:{ all -> 0x0066 }
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x0066 }
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0066 }
            com.tencent.beacon.d.a.d(r1, r4)     // Catch:{ all -> 0x0066 }
            if (r2 == 0) goto L_0x0042
            r2.close()     // Catch:{ IOException -> 0x004b }
        L_0x0042:
            r3.close()     // Catch:{ IOException -> 0x0046 }
            goto L_0x000e
        L_0x0046:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x000e
        L_0x004b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0042
        L_0x0050:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x0053:
            if (r2 == 0) goto L_0x0058
            r2.close()     // Catch:{ IOException -> 0x005c }
        L_0x0058:
            r3.close()     // Catch:{ IOException -> 0x0061 }
        L_0x005b:
            throw r0
        L_0x005c:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0058
        L_0x0061:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x005b
        L_0x0066:
            r0 = move-exception
            goto L_0x0053
        L_0x0068:
            r1 = move-exception
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.beacon.b.a.a(byte[]):java.lang.Object");
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:107:0x0082 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:106:0x0082 */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v51, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v56, resolved type: int} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:109:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0073 A[Catch:{ Throwable -> 0x0098, all -> 0x023a }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00ad A[SYNTHETIC, Splitter:B:34:0x00ad] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00b2 A[SYNTHETIC, Splitter:B:37:0x00b2] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00b7 A[SYNTHETIC, Splitter:B:40:0x00b7] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x012d A[Catch:{ Throwable -> 0x026d, all -> 0x0265 }] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0204 A[SYNTHETIC, Splitter:B:58:0x0204] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0209 A[SYNTHETIC, Splitter:B:61:0x0209] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0220  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x023f A[SYNTHETIC, Splitter:B:79:0x023f] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0244 A[SYNTHETIC, Splitter:B:82:0x0244] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0249 A[SYNTHETIC, Splitter:B:85:0x0249] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.tencent.beacon.b.d a(java.lang.String r12, java.lang.String r13, java.lang.String r14) {
        /*
            r1 = 0
            r5 = 0
            if (r12 == 0) goto L_0x0020
            java.lang.String r0 = ""
            boolean r0 = r12.equals(r0)
            if (r0 != 0) goto L_0x0020
            if (r13 == 0) goto L_0x0020
            java.lang.String r0 = ""
            boolean r0 = r13.equals(r0)
            if (r0 != 0) goto L_0x0020
            if (r14 == 0) goto L_0x0020
            java.lang.String r0 = ""
            boolean r0 = r14.equals(r0)
            if (r0 == 0) goto L_0x0022
        L_0x0020:
            r0 = r1
        L_0x0021:
            return r0
        L_0x0022:
            com.tencent.beacon.b.d r0 = new com.tencent.beacon.b.d
            r0.<init>()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "http://"
            r2.<init>(r3)
            java.lang.StringBuilder r2 = r2.append(r13)
            java.lang.StringBuilder r2 = r2.append(r14)
            java.lang.String r2 = r2.toString()
            java.net.URL r6 = new java.net.URL     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            r6.<init>(r2)     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            java.net.InetAddress r2 = java.net.InetAddress.getByName(r13)     // Catch:{ Exception -> 0x0091 }
            long r7 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x027d }
            long r3 = r7 - r3
            r0.f3405a = r3     // Catch:{ Exception -> 0x027d }
        L_0x004f:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            java.lang.String r4 = " dns: }"
            r3.<init>(r4)     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            long r7 = r0.f3405a     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            java.lang.StringBuilder r3 = r3.append(r7)     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            com.tencent.beacon.d.a.a(r3, r4)     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            r4.<init>()     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            byte[] r7 = r2.getAddress()     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            r3 = r5
        L_0x0070:
            int r2 = r7.length     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            if (r3 >= r2) goto L_0x00ce
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            java.lang.String r2 = "."
            r8.<init>(r2)     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            byte r2 = r7[r3]     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            if (r2 >= 0) goto L_0x00cb
            byte r2 = r7[r3]     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            int r2 = r2 + 256
        L_0x0082:
            java.lang.StringBuilder r2 = r8.append(r2)     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            r4.append(r2)     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            int r2 = r3 + 1
            r3 = r2
            goto L_0x0070
        L_0x0091:
            r2 = move-exception
            r2 = r1
        L_0x0093:
            r3 = -1
            r0.f3405a = r3     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            goto L_0x004f
        L_0x0098:
            r2 = move-exception
            r3 = r1
            r4 = r1
            r11 = r2
            r2 = r1
            r1 = r11
        L_0x009e:
            r1.printStackTrace()     // Catch:{ all -> 0x026a }
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x026a }
            r6 = 0
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ all -> 0x026a }
            com.tencent.beacon.d.a.d(r1, r6)     // Catch:{ all -> 0x026a }
            if (r3 == 0) goto L_0x00b0
            r3.close()     // Catch:{ IOException -> 0x022e }
        L_0x00b0:
            if (r2 == 0) goto L_0x00b5
            r2.close()     // Catch:{ IOException -> 0x0234 }
        L_0x00b5:
            if (r4 == 0) goto L_0x0021
            r4.close()     // Catch:{ IOException -> 0x00bc }
            goto L_0x0021
        L_0x00bc:
            r1 = move-exception
            r1.printStackTrace()
            java.lang.String r1 = r1.getMessage()
            java.lang.Object[] r2 = new java.lang.Object[r5]
            com.tencent.beacon.d.a.d(r1, r2)
            goto L_0x0021
        L_0x00cb:
            byte r2 = r7[r3]     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            goto L_0x0082
        L_0x00ce:
            r2 = 1
            java.lang.String r2 = r4.substring(r2)     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            r0.f = r2     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            r2 = 0
            r4.setLength(r2)     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            java.lang.String r2 = ":"
            java.lang.String[] r2 = r12.split(r2)     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            java.net.InetSocketAddress r3 = new java.net.InetSocketAddress     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            r4 = 0
            r4 = r2[r4]     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            r7 = 1
            r2 = r2[r7]     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            int r2 = java.lang.Integer.parseInt(r2)     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            r3.<init>(r4, r2)     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            long r7 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            java.net.Socket r4 = new java.net.Socket     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            r4.<init>()     // Catch:{ Throwable -> 0x0098, all -> 0x023a }
            r2 = 30000(0x7530, float:4.2039E-41)
            r4.connect(r3, r2)     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
            long r2 = r2 - r7
            r0.b = r2     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
            java.lang.String r3 = " conn: "
            r2.<init>(r3)     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
            long r7 = r0.b     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
            com.tencent.beacon.d.a.a(r2, r3)     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
            r2.<init>()     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
            java.lang.String r3 = r6.getPath()     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
            java.lang.StringBuilder r3 = r2.append(r3)     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
            java.lang.String r2 = r6.getQuery()     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
            if (r2 == 0) goto L_0x0220
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
            java.lang.String r7 = "?"
            r2.<init>(r7)     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
            java.lang.String r6 = r6.getQuery()     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
        L_0x0140:
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
            java.lang.String r6 = "HEAD "
            r3.<init>(r6)     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
            java.lang.String r3 = " HTTP/1.1 \r\nHost: "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
            java.lang.StringBuilder r2 = r2.append(r13)     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
            java.lang.String r3 = "\r\nConnection: close\r\n\r\n"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
            java.lang.String r6 = r2.toString()     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
            java.lang.String r2 = "IP"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
            java.lang.String r7 = "host test: "
            r3.<init>(r7)     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
            android.util.Log.i(r2, r3)     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
            java.io.OutputStream r3 = r4.getOutputStream()     // Catch:{ Throwable -> 0x026d, all -> 0x0265 }
            java.io.InputStream r2 = r4.getInputStream()     // Catch:{ Throwable -> 0x0274, all -> 0x0268 }
            long r7 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x027a }
            java.lang.String r1 = "UTF-8"
            byte[] r1 = r6.getBytes(r1)     // Catch:{ Throwable -> 0x027a }
            r3.write(r1)     // Catch:{ Throwable -> 0x027a }
            r3.flush()     // Catch:{ Throwable -> 0x027a }
            long r9 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x027a }
            long r6 = r9 - r7
            r0.c = r6     // Catch:{ Throwable -> 0x027a }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x027a }
            java.lang.String r6 = " re: "
            r1.<init>(r6)     // Catch:{ Throwable -> 0x027a }
            long r6 = r0.c     // Catch:{ Throwable -> 0x027a }
            java.lang.StringBuilder r1 = r1.append(r6)     // Catch:{ Throwable -> 0x027a }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x027a }
            r6 = 0
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x027a }
            com.tencent.beacon.d.a.a(r1, r6)     // Catch:{ Throwable -> 0x027a }
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x027a }
            r2.read()     // Catch:{ Throwable -> 0x027a }
            long r8 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x027a }
            long r6 = r8 - r6
            r0.d = r6     // Catch:{ Throwable -> 0x027a }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x027a }
            java.lang.String r6 = " wait: "
            r1.<init>(r6)     // Catch:{ Throwable -> 0x027a }
            long r6 = r0.d     // Catch:{ Throwable -> 0x027a }
            java.lang.StringBuilder r1 = r1.append(r6)     // Catch:{ Throwable -> 0x027a }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x027a }
            r6 = 0
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x027a }
            com.tencent.beacon.d.a.a(r1, r6)     // Catch:{ Throwable -> 0x027a }
            r1 = 500(0x1f4, float:7.0E-43)
            byte[] r1 = new byte[r1]     // Catch:{ Throwable -> 0x027a }
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x027a }
            r2.read(r1)     // Catch:{ Throwable -> 0x027a }
            long r8 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x027a }
            long r6 = r8 - r6
            r0.e = r6     // Catch:{ Throwable -> 0x027a }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x027a }
            java.lang.String r6 = " readp: "
            r1.<init>(r6)     // Catch:{ Throwable -> 0x027a }
            long r6 = r0.e     // Catch:{ Throwable -> 0x027a }
            java.lang.StringBuilder r1 = r1.append(r6)     // Catch:{ Throwable -> 0x027a }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x027a }
            r6 = 0
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x027a }
            com.tencent.beacon.d.a.a(r1, r6)     // Catch:{ Throwable -> 0x027a }
            if (r3 == 0) goto L_0x0207
            r3.close()     // Catch:{ IOException -> 0x0224 }
        L_0x0207:
            if (r2 == 0) goto L_0x020c
            r2.close()     // Catch:{ IOException -> 0x0229 }
        L_0x020c:
            r4.close()     // Catch:{ IOException -> 0x0211 }
            goto L_0x0021
        L_0x0211:
            r1 = move-exception
            r1.printStackTrace()
            java.lang.String r1 = r1.getMessage()
            java.lang.Object[] r2 = new java.lang.Object[r5]
            com.tencent.beacon.d.a.d(r1, r2)
            goto L_0x0021
        L_0x0220:
            java.lang.String r2 = ""
            goto L_0x0140
        L_0x0224:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0207
        L_0x0229:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x020c
        L_0x022e:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00b0
        L_0x0234:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00b5
        L_0x023a:
            r0 = move-exception
            r3 = r1
            r4 = r1
        L_0x023d:
            if (r3 == 0) goto L_0x0242
            r3.close()     // Catch:{ IOException -> 0x024d }
        L_0x0242:
            if (r1 == 0) goto L_0x0247
            r1.close()     // Catch:{ IOException -> 0x0252 }
        L_0x0247:
            if (r4 == 0) goto L_0x024c
            r4.close()     // Catch:{ IOException -> 0x0257 }
        L_0x024c:
            throw r0
        L_0x024d:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0242
        L_0x0252:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0247
        L_0x0257:
            r1 = move-exception
            r1.printStackTrace()
            java.lang.String r1 = r1.getMessage()
            java.lang.Object[] r2 = new java.lang.Object[r5]
            com.tencent.beacon.d.a.d(r1, r2)
            goto L_0x024c
        L_0x0265:
            r0 = move-exception
            r3 = r1
            goto L_0x023d
        L_0x0268:
            r0 = move-exception
            goto L_0x023d
        L_0x026a:
            r0 = move-exception
            r1 = r2
            goto L_0x023d
        L_0x026d:
            r2 = move-exception
            r3 = r1
            r11 = r1
            r1 = r2
            r2 = r11
            goto L_0x009e
        L_0x0274:
            r2 = move-exception
            r11 = r2
            r2 = r1
            r1 = r11
            goto L_0x009e
        L_0x027a:
            r1 = move-exception
            goto L_0x009e
        L_0x027d:
            r3 = move-exception
            goto L_0x0093
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.beacon.b.a.a(java.lang.String, java.lang.String, java.lang.String):com.tencent.beacon.b.d");
    }

    public static byte[] a(byte[] bArr, int i, String str) {
        if (bArr == null || i == -1) {
            return bArr;
        }
        com.tencent.beacon.d.a.b("enD:} %d %d", Integer.valueOf(bArr.length), Integer.valueOf(i));
        try {
            return h.b(i, str, bArr);
        } catch (Throwable th) {
            th.printStackTrace();
            com.tencent.beacon.d.a.d("err enD: %s", th.toString());
            return null;
        }
    }

    public static byte[] b(byte[] bArr, int i, String str) {
        if (bArr == null || i == -1) {
            return bArr;
        }
        try {
            return h.a(i, str, bArr);
        } catch (Throwable th) {
            th.printStackTrace();
            com.tencent.beacon.d.a.d("err unD: %s", th.toString());
            return null;
        }
    }

    public static byte[] a(byte[] bArr, int i) {
        if (bArr == null || i == -1) {
            return bArr;
        }
        com.tencent.beacon.d.a.b("zp: %s len: %s", Integer.valueOf(i), Integer.valueOf(bArr.length));
        try {
            return h.a(i, bArr);
        } catch (Throwable th) {
            th.printStackTrace();
            com.tencent.beacon.d.a.d("err zp : %s", th.toString());
            return null;
        }
    }

    public static byte[] b(byte[] bArr, int i) {
        if (bArr == null || i == -1) {
            return bArr;
        }
        com.tencent.beacon.d.a.b("unzp: %s len: %s", Integer.valueOf(i), Integer.valueOf(bArr.length));
        try {
            return h.b(i, bArr);
        } catch (Throwable th) {
            th.printStackTrace();
            com.tencent.beacon.d.a.d("err unzp}" + th.toString(), new Object[0]);
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x004e A[Catch:{ Throwable -> 0x014f, all -> 0x0187 }] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x011c A[SYNTHETIC, Splitter:B:25:0x011c] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0121 A[SYNTHETIC, Splitter:B:28:0x0121] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0126  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x01fa A[Catch:{ Throwable -> 0x014f, all -> 0x0187 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.tencent.beacon.b.d a(java.lang.String r19, java.lang.String r20) {
        /*
            if (r19 == 0) goto L_0x000c
            java.lang.String r2 = ""
            r0 = r19
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x000e
        L_0x000c:
            r2 = 0
        L_0x000d:
            return r2
        L_0x000e:
            com.tencent.beacon.b.d r3 = new com.tencent.beacon.b.d
            r3.<init>()
            r6 = 0
            r5 = 0
            r4 = 0
            java.net.URL r9 = new java.net.URL     // Catch:{ Throwable -> 0x014f }
            r0 = r19
            r9.<init>(r0)     // Catch:{ Throwable -> 0x014f }
            r2 = 0
            java.lang.String r8 = android.net.Proxy.getDefaultHost()     // Catch:{ Throwable -> 0x014f }
            int r7 = android.net.Proxy.getDefaultPort()     // Catch:{ Throwable -> 0x014f }
            if (r8 == 0) goto L_0x0034
            java.lang.String r10 = r8.trim()     // Catch:{ Throwable -> 0x014f }
            int r10 = r10.length()     // Catch:{ Throwable -> 0x014f }
            if (r10 == 0) goto L_0x0034
            if (r7 > 0) goto L_0x01b6
        L_0x0034:
            java.lang.String r7 = " no default proxy!"
            r8 = 0
            java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Throwable -> 0x014f }
            com.tencent.beacon.d.a.a(r7, r8)     // Catch:{ Throwable -> 0x014f }
            if (r20 == 0) goto L_0x0048
            java.lang.String r7 = r20.trim()     // Catch:{ Throwable -> 0x014f }
            int r7 = r7.length()     // Catch:{ Throwable -> 0x014f }
            if (r7 > 0) goto L_0x012c
        L_0x0048:
            r7 = 0
        L_0x0049:
            if (r7 != 0) goto L_0x01b2
            r2 = 0
        L_0x004c:
            if (r2 != 0) goto L_0x01fa
            java.net.URLConnection r2 = r9.openConnection()     // Catch:{ Throwable -> 0x014f }
            java.net.HttpURLConnection r2 = (java.net.HttpURLConnection) r2     // Catch:{ Throwable -> 0x014f }
            r6 = r2
        L_0x0055:
            r2 = 1
            r6.setDoInput(r2)     // Catch:{ Throwable -> 0x014f }
            r2 = 1
            r6.setDoOutput(r2)     // Catch:{ Throwable -> 0x014f }
            r2 = 30000(0x7530, float:4.2039E-41)
            r6.setConnectTimeout(r2)     // Catch:{ Throwable -> 0x014f }
            r2 = 30000(0x7530, float:4.2039E-41)
            r6.setReadTimeout(r2)     // Catch:{ Throwable -> 0x014f }
            long r7 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x014f }
            r6.connect()     // Catch:{ Throwable -> 0x014f }
            long r9 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x014f }
            java.io.OutputStream r4 = r6.getOutputStream()     // Catch:{ Throwable -> 0x014f }
            long r11 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x014f }
            java.io.InputStream r5 = r6.getInputStream()     // Catch:{ Throwable -> 0x014f }
            long r13 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x014f }
        L_0x0082:
            int r2 = r5.read()     // Catch:{ Throwable -> 0x014f }
            r15 = -1
            if (r2 != r15) goto L_0x0082
            long r15 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x014f }
            r17 = -1
            r0 = r17
            r3.f3405a = r0     // Catch:{ Throwable -> 0x014f }
            java.lang.String r2 = ""
            r3.f = r2     // Catch:{ Throwable -> 0x014f }
            long r7 = r9 - r7
            r3.b = r7     // Catch:{ Throwable -> 0x014f }
            long r7 = r11 - r9
            r3.c = r7     // Catch:{ Throwable -> 0x014f }
            long r7 = r13 - r11
            r3.d = r7     // Catch:{ Throwable -> 0x014f }
            long r7 = r15 - r13
            r3.e = r7     // Catch:{ Throwable -> 0x014f }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x014f }
            java.lang.String r7 = " dns:"
            r2.<init>(r7)     // Catch:{ Throwable -> 0x014f }
            long r7 = r3.f3405a     // Catch:{ Throwable -> 0x014f }
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ Throwable -> 0x014f }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x014f }
            r7 = 0
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Throwable -> 0x014f }
            com.tencent.beacon.d.a.a(r2, r7)     // Catch:{ Throwable -> 0x014f }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x014f }
            java.lang.String r7 = " connetionElapse:"
            r2.<init>(r7)     // Catch:{ Throwable -> 0x014f }
            long r7 = r3.b     // Catch:{ Throwable -> 0x014f }
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ Throwable -> 0x014f }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x014f }
            r7 = 0
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Throwable -> 0x014f }
            com.tencent.beacon.d.a.a(r2, r7)     // Catch:{ Throwable -> 0x014f }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x014f }
            java.lang.String r7 = " requestElapse:"
            r2.<init>(r7)     // Catch:{ Throwable -> 0x014f }
            long r7 = r3.c     // Catch:{ Throwable -> 0x014f }
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ Throwable -> 0x014f }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x014f }
            r7 = 0
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Throwable -> 0x014f }
            com.tencent.beacon.d.a.a(r2, r7)     // Catch:{ Throwable -> 0x014f }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x014f }
            java.lang.String r7 = " waitElapse:"
            r2.<init>(r7)     // Catch:{ Throwable -> 0x014f }
            long r7 = r3.d     // Catch:{ Throwable -> 0x014f }
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ Throwable -> 0x014f }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x014f }
            r7 = 0
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Throwable -> 0x014f }
            com.tencent.beacon.d.a.a(r2, r7)     // Catch:{ Throwable -> 0x014f }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x014f }
            java.lang.String r7 = " readResponeElapse:"
            r2.<init>(r7)     // Catch:{ Throwable -> 0x014f }
            long r7 = r3.e     // Catch:{ Throwable -> 0x014f }
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ Throwable -> 0x014f }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x014f }
            r7 = 0
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Throwable -> 0x014f }
            com.tencent.beacon.d.a.a(r2, r7)     // Catch:{ Throwable -> 0x014f }
            if (r4 == 0) goto L_0x011f
            r4.close()     // Catch:{ IOException -> 0x020a }
        L_0x011f:
            if (r5 == 0) goto L_0x0124
            r5.close()     // Catch:{ IOException -> 0x0210 }
        L_0x0124:
            if (r6 == 0) goto L_0x0129
            r6.disconnect()
        L_0x0129:
            r2 = r3
            goto L_0x000d
        L_0x012c:
            com.tencent.beacon.b.c r7 = new com.tencent.beacon.b.c     // Catch:{ Throwable -> 0x014f }
            r7.<init>()     // Catch:{ Throwable -> 0x014f }
            java.lang.String r8 = r20.toLowerCase()     // Catch:{ Throwable -> 0x014f }
            java.lang.String r10 = "ctwap"
            boolean r10 = r8.contains(r10)     // Catch:{ Throwable -> 0x014f }
            if (r10 == 0) goto L_0x016d
            java.lang.String r8 = " search ctwap"
            r10 = 0
            java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ Throwable -> 0x014f }
            com.tencent.beacon.d.a.a(r8, r10)     // Catch:{ Throwable -> 0x014f }
            java.lang.String r8 = "10.0.0.200"
            r7.f3404a = r8     // Catch:{ Throwable -> 0x014f }
            r8 = 80
            r7.b = r8     // Catch:{ Throwable -> 0x014f }
            goto L_0x0049
        L_0x014f:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ all -> 0x0187 }
            java.lang.String r2 = r2.getMessage()     // Catch:{ all -> 0x0187 }
            r7 = 0
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ all -> 0x0187 }
            com.tencent.beacon.d.a.d(r2, r7)     // Catch:{ all -> 0x0187 }
            if (r4 == 0) goto L_0x0162
            r4.close()     // Catch:{ IOException -> 0x0216 }
        L_0x0162:
            if (r5 == 0) goto L_0x0167
            r5.close()     // Catch:{ IOException -> 0x021c }
        L_0x0167:
            if (r6 == 0) goto L_0x0129
            r6.disconnect()
            goto L_0x0129
        L_0x016d:
            java.lang.String r10 = "cmwap"
            boolean r10 = r8.contains(r10)     // Catch:{ Throwable -> 0x014f }
            if (r10 == 0) goto L_0x0198
            java.lang.String r8 = " search cmwap"
            r10 = 0
            java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ Throwable -> 0x014f }
            com.tencent.beacon.d.a.a(r8, r10)     // Catch:{ Throwable -> 0x014f }
            java.lang.String r8 = "10.0.0.172"
            r7.f3404a = r8     // Catch:{ Throwable -> 0x014f }
            r8 = 80
            r7.b = r8     // Catch:{ Throwable -> 0x014f }
            goto L_0x0049
        L_0x0187:
            r2 = move-exception
            if (r4 == 0) goto L_0x018d
            r4.close()     // Catch:{ IOException -> 0x0222 }
        L_0x018d:
            if (r5 == 0) goto L_0x0192
            r5.close()     // Catch:{ IOException -> 0x0228 }
        L_0x0192:
            if (r6 == 0) goto L_0x0197
            r6.disconnect()
        L_0x0197:
            throw r2
        L_0x0198:
            java.lang.String r10 = "uniwap"
            boolean r8 = r8.contains(r10)     // Catch:{ Throwable -> 0x014f }
            if (r8 == 0) goto L_0x0049
            java.lang.String r8 = " search uniwap"
            r10 = 0
            java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ Throwable -> 0x014f }
            com.tencent.beacon.d.a.a(r8, r10)     // Catch:{ Throwable -> 0x014f }
            java.lang.String r8 = "10.0.0.172"
            r7.f3404a = r8     // Catch:{ Throwable -> 0x014f }
            r8 = 80
            r7.b = r8     // Catch:{ Throwable -> 0x014f }
            goto L_0x0049
        L_0x01b2:
            java.lang.String r8 = r7.f3404a     // Catch:{ Throwable -> 0x014f }
            int r7 = r7.b     // Catch:{ Throwable -> 0x014f }
        L_0x01b6:
            if (r8 == 0) goto L_0x004c
            java.lang.String r10 = r8.trim()     // Catch:{ Throwable -> 0x014f }
            int r10 = r10.length()     // Catch:{ Throwable -> 0x014f }
            if (r10 <= 0) goto L_0x004c
            java.net.InetSocketAddress r2 = new java.net.InetSocketAddress     // Catch:{ Throwable -> 0x014f }
            r2.<init>(r8, r7)     // Catch:{ Throwable -> 0x014f }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x014f }
            java.lang.String r11 = " apn }"
            r10.<init>(r11)     // Catch:{ Throwable -> 0x014f }
            r0 = r20
            java.lang.StringBuilder r10 = r10.append(r0)     // Catch:{ Throwable -> 0x014f }
            java.lang.String r11 = "  find proxy [}"
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Throwable -> 0x014f }
            java.lang.StringBuilder r8 = r10.append(r8)     // Catch:{ Throwable -> 0x014f }
            java.lang.String r10 = ":"
            java.lang.StringBuilder r8 = r8.append(r10)     // Catch:{ Throwable -> 0x014f }
            java.lang.StringBuilder r7 = r8.append(r7)     // Catch:{ Throwable -> 0x014f }
            java.lang.String r8 = "]"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Throwable -> 0x014f }
            java.lang.String r7 = r7.toString()     // Catch:{ Throwable -> 0x014f }
            r8 = 0
            java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Throwable -> 0x014f }
            com.tencent.beacon.d.a.a(r7, r8)     // Catch:{ Throwable -> 0x014f }
            goto L_0x004c
        L_0x01fa:
            java.net.Proxy r7 = new java.net.Proxy     // Catch:{ Throwable -> 0x014f }
            java.net.Proxy$Type r8 = java.net.Proxy.Type.HTTP     // Catch:{ Throwable -> 0x014f }
            r7.<init>(r8, r2)     // Catch:{ Throwable -> 0x014f }
            java.net.URLConnection r2 = r9.openConnection(r7)     // Catch:{ Throwable -> 0x014f }
            java.net.HttpURLConnection r2 = (java.net.HttpURLConnection) r2     // Catch:{ Throwable -> 0x014f }
            r6 = r2
            goto L_0x0055
        L_0x020a:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x011f
        L_0x0210:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0124
        L_0x0216:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0162
        L_0x021c:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0167
        L_0x0222:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x018d
        L_0x0228:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x0192
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.beacon.b.a.a(java.lang.String, java.lang.String):com.tencent.beacon.b.d");
    }

    public static b a(int i, g gVar, byte[] bArr, int i2, int i3) {
        com.tencent.beacon.d.a.b("en2Req", new Object[0]);
        if (gVar == null) {
            com.tencent.beacon.d.a.d("error no com info! ", new Object[0]);
            return null;
        }
        try {
            b bVar = new b();
            synchronized (gVar) {
                bVar.j = gVar.a();
                bVar.k = gVar.b();
                bVar.f3409a = gVar.c();
                bVar.b = gVar.j();
                bVar.c = gVar.d();
                bVar.d = gVar.e();
                bVar.e = gVar.f();
                bVar.l = Constants.STR_EMPTY;
                if (i == 100) {
                    HashMap hashMap = new HashMap();
                    hashMap.put("A1", com.tencent.beacon.event.a.a());
                    hashMap.put("A2", gVar.i());
                    f a2 = f.a(gVar.l());
                    hashMap.put("A4", a2.c());
                    hashMap.put("A6", a2.b());
                    hashMap.put("A7", a2.d());
                    StringBuilder sb = new StringBuilder();
                    Context l = gVar.l();
                    if (f == null) {
                        f = new a(l);
                    }
                    hashMap.put("A3", sb.append(f.a()).toString());
                    hashMap.put("A23", a2.e());
                    h.a(gVar.l());
                    hashMap.put("A33", h.k(gVar.l()));
                    if (com.tencent.beacon.a.a.g(gVar.l())) {
                        hashMap.put("A66", "F");
                    } else {
                        hashMap.put("A66", "B");
                    }
                    hashMap.put("A67", com.tencent.beacon.a.a.i(gVar.l()));
                    hashMap.put("A68", new StringBuilder().append(com.tencent.beacon.a.a.h(gVar.l())).toString());
                    bVar.l = h.a(hashMap);
                }
            }
            bVar.f = i;
            bVar.h = (byte) i3;
            bVar.i = (byte) i2;
            if (bArr == null) {
                bArr = Constants.STR_EMPTY.getBytes();
            }
            bVar.g = bArr;
            return bVar;
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    public static byte[] a(byte[] bArr, int i, int i2, String str) {
        if (bArr == null) {
            return null;
        }
        try {
            return a(a(bArr, i), i2, str);
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    public static byte[] b(byte[] bArr, int i, int i2, String str) {
        try {
            return b(b(bArr, i2, str), i);
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static long f() {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            return simpleDateFormat.parse(simpleDateFormat.format(new Date())).getTime();
        } catch (Throwable th) {
            th.printStackTrace();
            return -1;
        }
    }

    public static String g() {
        try {
            return new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(new Date());
        } catch (Throwable th) {
            th.printStackTrace();
            return Constants.STR_EMPTY;
        }
    }

    public static String b(String str) {
        try {
            RandomAccessFile randomAccessFile = new RandomAccessFile(str, "r");
            String readLine = randomAccessFile.readLine();
            if (readLine != null) {
                randomAccessFile.close();
                return readLine;
            }
        } catch (Exception e2) {
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x002d A[SYNTHETIC, Splitter:B:12:0x002d] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0032 A[SYNTHETIC, Splitter:B:15:0x0032] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0070 A[SYNTHETIC, Splitter:B:40:0x0070] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0075 A[SYNTHETIC, Splitter:B:43:0x0075] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.ArrayList<java.lang.String> a(java.lang.String[] r6) {
        /*
            r1 = 0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            java.lang.Runtime r2 = java.lang.Runtime.getRuntime()     // Catch:{ Throwable -> 0x0088, all -> 0x006c }
            java.lang.Process r4 = r2.exec(r6)     // Catch:{ Throwable -> 0x0088, all -> 0x006c }
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ Throwable -> 0x0088, all -> 0x006c }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ Throwable -> 0x0088, all -> 0x006c }
            java.io.InputStream r5 = r4.getInputStream()     // Catch:{ Throwable -> 0x0088, all -> 0x006c }
            r2.<init>(r5)     // Catch:{ Throwable -> 0x0088, all -> 0x006c }
            r3.<init>(r2)     // Catch:{ Throwable -> 0x0088, all -> 0x006c }
        L_0x001c:
            java.lang.String r2 = r3.readLine()     // Catch:{ Throwable -> 0x0026, all -> 0x0083 }
            if (r2 == 0) goto L_0x0037
            r0.add(r2)     // Catch:{ Throwable -> 0x0026, all -> 0x0083 }
            goto L_0x001c
        L_0x0026:
            r0 = move-exception
            r2 = r1
        L_0x0028:
            r0.printStackTrace()     // Catch:{ all -> 0x0085 }
            if (r3 == 0) goto L_0x0030
            r3.close()     // Catch:{ IOException -> 0x0062 }
        L_0x0030:
            if (r2 == 0) goto L_0x0035
            r2.close()     // Catch:{ IOException -> 0x0067 }
        L_0x0035:
            r0 = r1
        L_0x0036:
            return r0
        L_0x0037:
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ Throwable -> 0x0026, all -> 0x0083 }
            java.io.InputStreamReader r5 = new java.io.InputStreamReader     // Catch:{ Throwable -> 0x0026, all -> 0x0083 }
            java.io.InputStream r4 = r4.getErrorStream()     // Catch:{ Throwable -> 0x0026, all -> 0x0083 }
            r5.<init>(r4)     // Catch:{ Throwable -> 0x0026, all -> 0x0083 }
            r2.<init>(r5)     // Catch:{ Throwable -> 0x0026, all -> 0x0083 }
        L_0x0045:
            java.lang.String r4 = r2.readLine()     // Catch:{ Throwable -> 0x004f }
            if (r4 == 0) goto L_0x0051
            r0.add(r4)     // Catch:{ Throwable -> 0x004f }
            goto L_0x0045
        L_0x004f:
            r0 = move-exception
            goto L_0x0028
        L_0x0051:
            r3.close()     // Catch:{ IOException -> 0x005d }
        L_0x0054:
            r2.close()     // Catch:{ IOException -> 0x0058 }
            goto L_0x0036
        L_0x0058:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0036
        L_0x005d:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0054
        L_0x0062:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0030
        L_0x0067:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0035
        L_0x006c:
            r0 = move-exception
            r3 = r1
        L_0x006e:
            if (r3 == 0) goto L_0x0073
            r3.close()     // Catch:{ IOException -> 0x0079 }
        L_0x0073:
            if (r1 == 0) goto L_0x0078
            r1.close()     // Catch:{ IOException -> 0x007e }
        L_0x0078:
            throw r0
        L_0x0079:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0073
        L_0x007e:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0078
        L_0x0083:
            r0 = move-exception
            goto L_0x006e
        L_0x0085:
            r0 = move-exception
            r1 = r2
            goto L_0x006e
        L_0x0088:
            r0 = move-exception
            r2 = r1
            r3 = r1
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.beacon.b.a.a(java.lang.String[]):java.util.ArrayList");
    }

    public static String c(String str) {
        try {
            byte[] digest = MessageDigest.getInstance("MD5").digest(str.getBytes());
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b2 : digest) {
                byte b3 = b2 & 255;
                if (b3 < 16) {
                    stringBuffer.append(0);
                }
                stringBuffer.append(Integer.toHexString(b3));
            }
            return stringBuffer.toString();
        } catch (Exception e2) {
            e2.printStackTrace();
            return str;
        }
    }

    public static String d(String str) {
        String c2 = c(str);
        if (c2 == null) {
            return c2;
        }
        try {
            return c2.substring(8, 24);
        } catch (Exception e2) {
            return c2;
        }
    }

    public static String a(Context context, int i) {
        try {
            f a2 = f.a(context);
            String a3 = a2.a();
            return d(a3 + "_" + a2.b() + "_" + new Date().getTime() + "_" + i);
        } catch (Exception e2) {
            return null;
        }
    }

    public static HashSet<String> a(ArrayList<String> arrayList) {
        int size = arrayList.size();
        if (arrayList == null || size <= 0) {
            return null;
        }
        HashSet<String> hashSet = new HashSet<>(size);
        Iterator<String> it = arrayList.iterator();
        while (it.hasNext()) {
            hashSet.add(it.next());
        }
        return hashSet;
    }
}
