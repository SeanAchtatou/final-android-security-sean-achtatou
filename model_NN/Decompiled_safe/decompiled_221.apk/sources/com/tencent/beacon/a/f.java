package com.tencent.beacon.a;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.tencent.beacon.d.a;

/* compiled from: ProGuard */
public final class f extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private boolean f3397a = false;

    public final void onReceive(Context context, Intent intent) {
        NetworkInfo.State state;
        NetworkInfo.State state2;
        if (context == null) {
            try {
                a.c(" onReceive context is null!", new Object[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager == null) {
                a.c(" onReceive ConnectivityManager is null!", new Object[0]);
                return;
            }
            NetworkInfo networkInfo = connectivityManager.getNetworkInfo(1);
            NetworkInfo networkInfo2 = connectivityManager.getNetworkInfo(0);
            if (networkInfo != null) {
                state = networkInfo.getState();
            } else {
                state = null;
            }
            if (networkInfo2 != null) {
                state2 = networkInfo2.getState();
            } else {
                state2 = null;
            }
            if (state != null && state2 != null && NetworkInfo.State.CONNECTED != state && NetworkInfo.State.CONNECTED == state2) {
                i.c(context);
            } else if (state != null && NetworkInfo.State.CONNECTED == state) {
                i.c(context);
            }
        }
    }

    public final void a(Context context) {
        if (context == null) {
            a.c(" Context is null!", new Object[0]);
        } else if (!this.f3397a) {
            this.f3397a = true;
            context.registerReceiver(this, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        }
    }
}
