package com.tencent.beacon.a.b;

import com.tencent.beacon.c.e.e;
import com.tencent.connect.common.Constants;
import java.util.Map;
import java.util.Set;

/* compiled from: ProGuard */
public final class h {

    /* renamed from: a  reason: collision with root package name */
    private final int f3393a;
    /* access modifiers changed from: private */
    public boolean b = false;
    private String c = Constants.STR_EMPTY;
    private Map<String, String> d = null;
    private Set<String> e = null;
    private e f = null;
    private Set<String> g = null;

    public h(int i) {
        this.f3393a = i;
    }

    public final boolean a() {
        return this.b;
    }

    public final void a(boolean z) {
        this.b = z;
    }

    public final String b() {
        return this.c;
    }

    public final void a(String str) {
        this.c = str;
    }

    public final Map<String, String> c() {
        return this.d;
    }

    public final void a(Map<String, String> map) {
        this.d = map;
    }

    public final Set<String> d() {
        return this.e;
    }

    public final void a(Set<String> set) {
        this.e = set;
    }

    public final e e() {
        return this.f;
    }

    public final void a(e eVar) {
        this.f = eVar;
    }

    public final int f() {
        return this.f3393a;
    }

    public final Set<String> g() {
        return this.g;
    }

    public final void b(Set<String> set) {
        this.g = set;
    }
}
