package com.tencent.beacon.event;

import android.content.Context;
import com.tencent.beacon.a.d;
import com.tencent.beacon.a.h;
import com.tencent.beacon.b.a;
import com.tencent.beacon.f.b;
import java.util.List;

/* compiled from: ProGuard */
final class o extends b {
    private List<i> e = null;
    private Context f;
    private Long[] g = null;
    private boolean h = false;

    public o(Context context, List<i> list) {
        super(context, 1, 2);
        this.e = list;
        this.f = context;
        if (this.e.size() == 1 && "rqd_heartbeat".equals(this.e.get(0).d())) {
            this.h = true;
        }
        this.d = a.a(context, 2);
        com.tencent.beacon.d.a.a("real rid:%s", this.d);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0023, code lost:
        if (r0 == null) goto L_0x0025;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized com.tencent.beacon.c.a.b a() {
        /*
            r4 = this;
            r1 = 0
            monitor-enter(r4)
            java.lang.String r0 = " TUUD.GetUD start"
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x0034 }
            com.tencent.beacon.d.a.b(r0, r2)     // Catch:{ all -> 0x0034 }
            java.util.List<com.tencent.beacon.event.i> r0 = r4.e     // Catch:{ all -> 0x0034 }
            if (r0 == 0) goto L_0x0016
            java.util.List<com.tencent.beacon.event.i> r0 = r4.e     // Catch:{ all -> 0x0034 }
            int r0 = r0.size()     // Catch:{ all -> 0x0034 }
            if (r0 > 0) goto L_0x0019
        L_0x0016:
            r0 = r1
        L_0x0017:
            monitor-exit(r4)
            return r0
        L_0x0019:
            android.content.Context r0 = r4.f     // Catch:{ Throwable -> 0x0027 }
            int r2 = r4.f3447a     // Catch:{ Throwable -> 0x0027 }
            java.util.List<com.tencent.beacon.event.i> r3 = r4.e     // Catch:{ Throwable -> 0x0027 }
            com.tencent.beacon.c.a.b r0 = com.tencent.beacon.event.p.a(r0, r2, r3)     // Catch:{ Throwable -> 0x0027 }
            if (r0 != 0) goto L_0x0017
        L_0x0025:
            r0 = r1
            goto L_0x0017
        L_0x0027:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x0034 }
            java.lang.String r0 = " TUUD.GetUD start error"
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x0034 }
            com.tencent.beacon.d.a.d(r0, r2)     // Catch:{ all -> 0x0034 }
            goto L_0x0025
        L_0x0034:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.beacon.event.o.a():com.tencent.beacon.c.a.b");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.a.d.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.beacon.a.d.a(java.lang.Runnable, long):void
      com.tencent.beacon.a.d.a(int, boolean):void */
    public final synchronized void b(boolean z) {
        com.tencent.beacon.d.a.b(" TimeUpUploadDatas.done(), result:%b", Boolean.valueOf(z));
        if (this.e != null && !z) {
            com.tencent.beacon.d.a.f(" upload failed, save to db", new Object[0]);
            if (!this.h) {
                this.g = h.a(this.f, this.e);
                this.e = null;
            }
        }
        if (z && this.h) {
            Context context = this.f;
            d.a().a(108, true);
            com.tencent.beacon.a.a.a(context, "HEART_DENGTA", a.g());
            com.tencent.beacon.d.a.a("heartbeat uploaded sucess!", new Object[0]);
        }
        if (z && this.g != null) {
            h.a(this.f, this.g);
        }
        if (z && this.g == null && this.e != null) {
            this.e = null;
        }
    }
}
