package com.tencent.beacon.event;

import android.content.Context;
import com.tencent.beacon.a.h;
import com.tencent.beacon.d.a;
import com.tencent.beacon.f.b;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public final class p extends b {
    private com.tencent.beacon.c.a.b e;
    private List<i> f;

    private synchronized void a(com.tencent.beacon.c.a.b bVar) {
        this.e = bVar;
    }

    private synchronized List<i> f() {
        return this.f;
    }

    private synchronized void a(List<i> list) {
        this.f = list;
    }

    public final synchronized com.tencent.beacon.c.a.b a() {
        return null;
    }

    public static com.tencent.beacon.c.a.b a(Context context, int i, List<i> list) {
        byte[] a2;
        if (list == null || list.size() <= 0) {
            return null;
        }
        try {
            a.b(" current size:}" + list.size(), new Object[0]);
            com.tencent.beacon.c.b.b b = b(list);
            if (b == null || (a2 = b.a()) == null) {
                return null;
            }
            return a(context, i, a2);
        } catch (Throwable th) {
            th.printStackTrace();
            a.d(" RealTimeRecordUploadDatas.encode2EventRecordPackage error}", new Object[0]);
            return null;
        }
    }

    private static com.tencent.beacon.c.b.b b(List<i> list) {
        if (list == null || list.size() <= 0) {
            return null;
        }
        try {
            com.tencent.beacon.c.b.b bVar = new com.tencent.beacon.c.b.b();
            ArrayList<com.tencent.beacon.c.b.a> arrayList = new ArrayList<>();
            for (i e2 : list) {
                com.tencent.beacon.c.b.a e3 = b.e(e2);
                if (e3 != null) {
                    arrayList.add(e3);
                }
            }
            bVar.f3412a = arrayList;
            a.b(" RealTimeRecordUploadDatas.encode2EventRecordPackage() end}", new Object[0]);
            return bVar;
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    public final void b(boolean z) {
        a((com.tencent.beacon.c.a.b) null);
        List<i> f2 = f();
        a((List<i>) null);
        if (f2 != null) {
            if (!z) {
                a.b(" isHandled false , record to db}", new Object[0]);
                h.a((Context) null, f2);
            }
            f2.clear();
        }
    }
}
