package com.tencent.beacon.c.e;

import com.tencent.beacon.e.a;
import com.tencent.beacon.e.d;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public final class c extends com.tencent.beacon.e.c {

    /* renamed from: a  reason: collision with root package name */
    public String f3420a = Constants.STR_EMPTY;
    public int b = 0;
    public boolean c = true;
    private int d = 0;

    public final void a(d dVar) {
        dVar.a(this.f3420a, 0);
        dVar.a(this.b, 1);
        dVar.a(this.d, 2);
        dVar.a(this.c, 3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.b, java.nio.ByteBuffer):int
      com.tencent.beacon.e.a.a(int, boolean):boolean */
    public final void a(a aVar) {
        this.f3420a = aVar.b(0, true);
        this.b = aVar.a(this.b, 1, true);
        this.d = aVar.a(this.d, 2, false);
        boolean z = this.c;
        this.c = aVar.a(3, false);
    }
}
