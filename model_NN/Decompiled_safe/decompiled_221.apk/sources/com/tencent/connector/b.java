package com.tencent.connector;

import android.media.MediaPlayer;

/* compiled from: ProGuard */
class b implements MediaPlayer.OnCompletionListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CaptureActivity f3517a;

    b(CaptureActivity captureActivity) {
        this.f3517a = captureActivity;
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        this.f3517a.i.release();
        MediaPlayer unused = this.f3517a.i = (MediaPlayer) null;
    }
}
