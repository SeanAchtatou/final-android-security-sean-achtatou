package com.tencent.connector.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.connector.ConnectionActivity;

/* compiled from: ProGuard */
public class ContentConnectionInitNoLogin extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private Context f3522a;
    /* access modifiers changed from: private */
    public ConnectionActivity b;
    private Button c;
    private Button d;
    private View.OnClickListener e;
    private View.OnClickListener f;

    public ContentConnectionInitNoLogin(Context context) {
        super(context);
        this.f3522a = context;
    }

    public ContentConnectionInitNoLogin(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3522a = context;
    }

    public ContentConnectionInitNoLogin(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f3522a = context;
    }

    public void setHostActivity(ConnectionActivity connectionActivity) {
        this.b = connectionActivity;
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        this.c = (Button) findViewById(R.id.login_to_connect);
        this.d = (Button) findViewById(R.id.scan);
        this.e = new e(this);
        this.f = new f(this);
        if (this.c != null) {
            this.c.setOnClickListener(this.e);
        }
        if (this.d != null) {
            this.d.setOnClickListener(this.f);
        }
    }

    public void displayInit() {
    }
}
