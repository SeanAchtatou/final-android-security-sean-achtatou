package com.tencent.connector;

import android.app.ActivityManager;
import android.content.Intent;
import com.qq.AppService.AstApp;
import com.tencent.connector.ipc.a;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
class k implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ j f3555a;

    k(j jVar) {
        this.f3555a = jVar;
    }

    public void run() {
        boolean z;
        List<ActivityManager.RunningTaskInfo> runningTasks;
        boolean unused = this.f3555a.f3554a = true;
        ActivityManager activityManager = (ActivityManager) AstApp.i().getSystemService("activity");
        if (activityManager != null) {
            int i = 0;
            boolean z2 = false;
            while (i < 300) {
                if (a.b(AstApp.i()) && (runningTasks = activityManager.getRunningTasks(5)) != null) {
                    Iterator<ActivityManager.RunningTaskInfo> it = runningTasks.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        ActivityManager.RunningTaskInfo next = it.next();
                        if (next.id == this.f3555a.b) {
                            Intent intent = new Intent();
                            intent.setComponent(next.topActivity);
                            intent.setFlags(268435456);
                            if (next.topActivity.getClassName().equals(ConnectionActivity.class.getName())) {
                            }
                            AstApp.i().startActivity(intent);
                            z = true;
                            break;
                        }
                    }
                }
                z = z2;
                if (z) {
                    break;
                }
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                i++;
                z2 = z;
            }
        }
        boolean unused2 = this.f3555a.f3554a = false;
    }
}
