package com.tencent.connector.qrcode.a;

import com.google.zxing.j;
import com.google.zxing.k;
import com.tencent.connector.component.ViewfinderView;

/* compiled from: ProGuard */
public final class g implements k {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f3563a = false;
    private final ViewfinderView b;

    public g(ViewfinderView viewfinderView) {
        this.b = viewfinderView;
    }

    public void a(j jVar) {
        f3563a = true;
        this.b.addPossibleResultPoint(jVar);
    }
}
