package android.support.v4.view;

import android.os.Build;
import android.view.ViewConfiguration;

/* compiled from: ProGuard */
public class ViewConfigurationCompat {
    static final bk IMPL;

    static {
        if (Build.VERSION.SDK_INT >= 11) {
            IMPL = new bj();
        } else {
            IMPL = new bi();
        }
    }

    public static int getScaledPagingTouchSlop(ViewConfiguration viewConfiguration) {
        return IMPL.a(viewConfiguration);
    }
}
