package android.support.v4.view;

import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.view.a.a;
import android.support.v4.view.a.g;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;

/* compiled from: ProGuard */
interface be {
    int a(View view);

    void a(View view, int i, int i2, int i3, int i4);

    void a(View view, int i, Paint paint);

    void a(View view, AccessibilityDelegateCompat accessibilityDelegateCompat);

    void a(View view, a aVar);

    void a(View view, AccessibilityEvent accessibilityEvent);

    void a(View view, Runnable runnable);

    void a(View view, Runnable runnable, long j);

    void a(View view, boolean z);

    boolean a(View view, int i);

    boolean a(View view, int i, Bundle bundle);

    void b(View view, AccessibilityEvent accessibilityEvent);

    boolean b(View view);

    boolean b(View view, int i);

    void c(View view);

    void c(View view, int i);

    int d(View view);

    void d(View view, int i);

    g e(View view);

    void e(View view, int i);

    int f(View view);

    int g(View view);
}
