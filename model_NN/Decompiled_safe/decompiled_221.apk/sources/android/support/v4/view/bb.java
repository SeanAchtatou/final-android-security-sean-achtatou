package android.support.v4.view;

import android.support.v4.view.a.a;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;

/* compiled from: ProGuard */
class bb extends ba {
    bb() {
    }

    public boolean a(View view, int i) {
        return bh.a(view, i);
    }

    public boolean b(View view, int i) {
        return bh.b(view, i);
    }

    public void a(View view, AccessibilityEvent accessibilityEvent) {
        bh.a(view, accessibilityEvent);
    }

    public void b(View view, AccessibilityEvent accessibilityEvent) {
        bh.b(view, accessibilityEvent);
    }

    public void a(View view, a aVar) {
        bh.b(view, aVar.a());
    }

    public void a(View view, AccessibilityDelegateCompat accessibilityDelegateCompat) {
        bh.a(view, accessibilityDelegateCompat.getBridge());
    }
}
