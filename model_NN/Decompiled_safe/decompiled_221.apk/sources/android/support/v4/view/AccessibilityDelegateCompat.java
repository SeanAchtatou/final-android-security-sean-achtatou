package android.support.v4.view;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.a.a;
import android.support.v4.view.a.g;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;

/* compiled from: ProGuard */
public class AccessibilityDelegateCompat {
    private static final Object DEFAULT_DELEGATE = IMPL.a();
    private static final c IMPL;
    final Object mBridge = IMPL.a(this);

    static {
        if (Build.VERSION.SDK_INT >= 16) {
            IMPL = new d();
        } else if (Build.VERSION.SDK_INT >= 14) {
            IMPL = new a();
        } else {
            IMPL = new f();
        }
    }

    /* access modifiers changed from: package-private */
    public Object getBridge() {
        return this.mBridge;
    }

    public void sendAccessibilityEvent(View view, int i) {
        IMPL.a(DEFAULT_DELEGATE, view, i);
    }

    public void sendAccessibilityEventUnchecked(View view, AccessibilityEvent accessibilityEvent) {
        IMPL.d(DEFAULT_DELEGATE, view, accessibilityEvent);
    }

    public boolean dispatchPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
        return IMPL.a(DEFAULT_DELEGATE, view, accessibilityEvent);
    }

    public void onPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
        IMPL.c(DEFAULT_DELEGATE, view, accessibilityEvent);
    }

    public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
        IMPL.b(DEFAULT_DELEGATE, view, accessibilityEvent);
    }

    public void onInitializeAccessibilityNodeInfo(View view, a aVar) {
        IMPL.a(DEFAULT_DELEGATE, view, aVar);
    }

    public boolean onRequestSendAccessibilityEvent(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
        return IMPL.a(DEFAULT_DELEGATE, viewGroup, view, accessibilityEvent);
    }

    public g getAccessibilityNodeProvider(View view) {
        return IMPL.a(DEFAULT_DELEGATE, view);
    }

    public boolean performAccessibilityAction(View view, int i, Bundle bundle) {
        return IMPL.a(DEFAULT_DELEGATE, view, i, bundle);
    }
}
