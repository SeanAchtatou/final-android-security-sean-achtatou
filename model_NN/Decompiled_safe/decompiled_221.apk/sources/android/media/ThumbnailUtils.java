package android.media;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import java.io.IOException;

/* compiled from: ProGuard */
public class ThumbnailUtils {
    public static final int OPTIONS_RECYCLE_INPUT = 2;
    public static final int TARGET_SIZE_MICRO_THUMBNAIL = 96;
    public static final int TARGET_SIZE_MINI_THUMBNAIL = 320;

    /* JADX WARNING: Removed duplicated region for block: B:49:0x00a8  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x00cb A[SYNTHETIC, Splitter:B:65:0x00cb] */
    /* JADX WARNING: Removed duplicated region for block: B:78:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap createImageThumbnail(java.lang.String r10, int r11) {
        /*
            r9 = -1
            r5 = 96
            r2 = 0
            r0 = 1
            r1 = 0
            if (r11 != r0) goto L_0x0009
            r2 = r0
        L_0x0009:
            if (r2 == 0) goto L_0x007c
            r0 = 320(0x140, float:4.48E-43)
            r4 = r0
        L_0x000e:
            if (r2 == 0) goto L_0x007e
            r0 = 196608(0x30000, float:2.75506E-40)
        L_0x0012:
            android.media.b r2 = new android.media.b
            r2.<init>()
            java.io.File r3 = new java.io.File
            r3.<init>(r10)
            boolean r3 = r3.exists()
            if (r3 == 0) goto L_0x00de
            java.lang.String r3 = ".jpg"
            boolean r3 = r10.endsWith(r3)
            if (r3 != 0) goto L_0x004a
            java.lang.String r3 = ".png"
            boolean r3 = r10.endsWith(r3)
            if (r3 != 0) goto L_0x004a
            java.lang.String r3 = ".jpeg"
            boolean r3 = r10.endsWith(r3)
            if (r3 != 0) goto L_0x004a
            java.lang.String r3 = ".gif"
            boolean r3 = r10.endsWith(r3)
            if (r3 != 0) goto L_0x004a
            java.lang.String r3 = ".bmp"
            boolean r3 = r10.endsWith(r3)
            if (r3 == 0) goto L_0x00de
        L_0x004a:
            a(r10, r4, r0, r2)     // Catch:{ Throwable -> 0x0081 }
        L_0x004d:
            android.graphics.Bitmap r3 = r2.b
        L_0x004f:
            if (r3 != 0) goto L_0x00dc
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Exception -> 0x00b3, all -> 0x00c7 }
            r2.<init>(r10)     // Catch:{ Exception -> 0x00b3, all -> 0x00c7 }
            java.io.FileDescriptor r6 = r2.getFD()     // Catch:{ Exception -> 0x00d9, all -> 0x00d4 }
            android.graphics.BitmapFactory$Options r7 = new android.graphics.BitmapFactory$Options     // Catch:{ Exception -> 0x00d9, all -> 0x00d4 }
            r7.<init>()     // Catch:{ Exception -> 0x00d9, all -> 0x00d4 }
            r8 = 1
            r7.inSampleSize = r8     // Catch:{ Exception -> 0x00d9, all -> 0x00d4 }
            r8 = 1
            r7.inJustDecodeBounds = r8     // Catch:{ Exception -> 0x00d9, all -> 0x00d4 }
            r8 = 0
            android.graphics.BitmapFactory.decodeFileDescriptor(r6, r8, r7)     // Catch:{ Exception -> 0x00d9, all -> 0x00d4 }
            boolean r8 = r7.mCancel     // Catch:{ Exception -> 0x00d9, all -> 0x00d4 }
            if (r8 != 0) goto L_0x0075
            int r8 = r7.outWidth     // Catch:{ Exception -> 0x00d9, all -> 0x00d4 }
            if (r8 == r9) goto L_0x0075
            int r8 = r7.outHeight     // Catch:{ Exception -> 0x00d9, all -> 0x00d4 }
            if (r8 != r9) goto L_0x008b
        L_0x0075:
            if (r2 == 0) goto L_0x007a
            r2.close()     // Catch:{ IOException -> 0x0086 }
        L_0x007a:
            r0 = r1
        L_0x007b:
            return r0
        L_0x007c:
            r4 = r5
            goto L_0x000e
        L_0x007e:
            r0 = 16384(0x4000, float:2.2959E-41)
            goto L_0x0012
        L_0x0081:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x004d
        L_0x0086:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x007a
        L_0x008b:
            int r0 = a(r7, r4, r0)     // Catch:{ Exception -> 0x00d9, all -> 0x00d4 }
            r7.inSampleSize = r0     // Catch:{ Exception -> 0x00d9, all -> 0x00d4 }
            r0 = 0
            r7.inJustDecodeBounds = r0     // Catch:{ Exception -> 0x00d9, all -> 0x00d4 }
            r0 = 0
            r7.inDither = r0     // Catch:{ Exception -> 0x00d9, all -> 0x00d4 }
            android.graphics.Bitmap$Config r0 = android.graphics.Bitmap.Config.ARGB_8888     // Catch:{ Exception -> 0x00d9, all -> 0x00d4 }
            r7.inPreferredConfig = r0     // Catch:{ Exception -> 0x00d9, all -> 0x00d4 }
            r0 = 0
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeFileDescriptor(r6, r0, r7)     // Catch:{ Exception -> 0x00d9, all -> 0x00d4 }
            if (r2 == 0) goto L_0x00a5
            r2.close()     // Catch:{ IOException -> 0x00ae }
        L_0x00a5:
            r1 = 3
            if (r11 != r1) goto L_0x007b
            r1 = 2
            android.graphics.Bitmap r0 = extractThumbnail(r0, r5, r5, r1)
            goto L_0x007b
        L_0x00ae:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00a5
        L_0x00b3:
            r0 = move-exception
        L_0x00b4:
            java.lang.String r2 = "ThumbnailUtils"
            java.lang.String r4 = ""
            android.util.Log.e(r2, r4, r0)     // Catch:{ all -> 0x00d6 }
            if (r1 == 0) goto L_0x00dc
            r1.close()     // Catch:{ IOException -> 0x00c2 }
        L_0x00c0:
            r0 = r3
            goto L_0x00a5
        L_0x00c2:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00c0
        L_0x00c7:
            r0 = move-exception
            r2 = r1
        L_0x00c9:
            if (r2 == 0) goto L_0x00ce
            r2.close()     // Catch:{ IOException -> 0x00cf }
        L_0x00ce:
            throw r0
        L_0x00cf:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00ce
        L_0x00d4:
            r0 = move-exception
            goto L_0x00c9
        L_0x00d6:
            r0 = move-exception
            r2 = r1
            goto L_0x00c9
        L_0x00d9:
            r0 = move-exception
            r1 = r2
            goto L_0x00b4
        L_0x00dc:
            r0 = r3
            goto L_0x00a5
        L_0x00de:
            r3 = r1
            goto L_0x004f
        */
        throw new UnsupportedOperationException("Method not decompiled: android.media.ThumbnailUtils.createImageThumbnail(java.lang.String, int):android.graphics.Bitmap");
    }

    public static Bitmap createVideoThumbnail(String str, int i) {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        try {
            mediaMetadataRetriever.setMode(2);
            mediaMetadataRetriever.setDataSource(str);
            bitmap = mediaMetadataRetriever.captureFrame();
            try {
                mediaMetadataRetriever.release();
            } catch (RuntimeException e) {
            }
        } catch (IllegalArgumentException e2) {
            try {
                mediaMetadataRetriever.release();
            } catch (RuntimeException e3) {
            }
        } catch (RuntimeException e4) {
            try {
                mediaMetadataRetriever.release();
            } catch (RuntimeException e5) {
            }
        } catch (Throwable th) {
            try {
                mediaMetadataRetriever.release();
            } catch (RuntimeException e6) {
            }
            throw th;
        }
        if (i != 3 || bitmap == null) {
            return bitmap;
        }
        return extractThumbnail(bitmap, 96, 96, 2);
    }

    public static Bitmap extractThumbnail(Bitmap bitmap, int i, int i2) {
        return extractThumbnail(bitmap, i, i2, 0);
    }

    public static Bitmap extractThumbnail(Bitmap bitmap, int i, int i2, int i3) {
        float height;
        if (bitmap == null) {
            return null;
        }
        if (bitmap.getWidth() < bitmap.getHeight()) {
            height = ((float) i) / ((float) bitmap.getWidth());
        } else {
            height = ((float) i2) / ((float) bitmap.getHeight());
        }
        Matrix matrix = new Matrix();
        matrix.setScale(height, height);
        return a(matrix, bitmap, i, i2, i3 | 1);
    }

    private static int a(BitmapFactory.Options options, int i, int i2) {
        int b = b(options, i, i2);
        if (b > 8) {
            return ((b + 7) / 8) * 8;
        }
        int i3 = 1;
        while (i3 < b) {
            i3 <<= 1;
        }
        return i3;
    }

    private static int b(BitmapFactory.Options options, int i, int i2) {
        double d = (double) options.outWidth;
        double d2 = (double) options.outHeight;
        int ceil = i2 == -1 ? 1 : (int) Math.ceil(Math.sqrt((d * d2) / ((double) i2)));
        int min = i == -1 ? 128 : (int) Math.min(Math.floor(d / ((double) i)), Math.floor(d2 / ((double) i)));
        if (min < ceil) {
            return ceil;
        }
        if (i2 == -1 && i == -1) {
            return 1;
        }
        if (i != -1) {
            return min;
        }
        return ceil;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private static Bitmap a(Matrix matrix, Bitmap bitmap, int i, int i2, int i3) {
        boolean z;
        Matrix matrix2;
        Bitmap bitmap2;
        boolean z2 = (i3 & 1) != 0;
        if ((i3 & 2) != 0) {
            z = true;
        } else {
            z = false;
        }
        int width = bitmap.getWidth() - i;
        int height = bitmap.getHeight() - i2;
        if (z2 || (width >= 0 && height >= 0)) {
            float width2 = (float) bitmap.getWidth();
            float height2 = (float) bitmap.getHeight();
            if (width2 / height2 > ((float) i) / ((float) i2)) {
                float f = ((float) i2) / height2;
                if (f < 0.9f || f > 1.0f) {
                    matrix.setScale(f, f);
                } else {
                    matrix = null;
                }
                matrix2 = matrix;
            } else {
                float f2 = ((float) i) / width2;
                if (f2 < 0.9f || f2 > 1.0f) {
                    matrix.setScale(f2, f2);
                    matrix2 = matrix;
                } else {
                    matrix2 = null;
                }
            }
            if (matrix2 != null) {
                bitmap2 = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix2, true);
            } else {
                bitmap2 = bitmap;
            }
            if (z && bitmap2 != bitmap) {
                bitmap.recycle();
            }
            Bitmap createBitmap = Bitmap.createBitmap(bitmap2, Math.max(0, bitmap2.getWidth() - i) / 2, Math.max(0, bitmap2.getHeight() - i2) / 2, i, i2);
            if (createBitmap == bitmap2) {
                return createBitmap;
            }
            if (!z && bitmap2 == bitmap) {
                return createBitmap;
            }
            bitmap2.recycle();
            return createBitmap;
        }
        Bitmap createBitmap2 = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap2);
        int max = Math.max(0, width / 2);
        int max2 = Math.max(0, height / 2);
        Rect rect = new Rect(max, max2, Math.min(i, bitmap.getWidth()) + max, Math.min(i2, bitmap.getHeight()) + max2);
        int width3 = (i - rect.width()) / 2;
        int height3 = (i2 - rect.height()) / 2;
        canvas.drawBitmap(bitmap, rect, new Rect(width3, height3, i - width3, i2 - height3), (Paint) null);
        if (z) {
            bitmap.recycle();
        }
        return createBitmap2;
    }

    private static void a(String str, int i, int i2, b bVar) {
        int i3;
        if (str != null) {
            byte[] bArr = null;
            try {
                ExifInterface exifInterface = new ExifInterface(str);
                if (exifInterface != null) {
                    bArr = exifInterface.getThumbnail();
                }
            } catch (IOException e) {
                Log.w("ThumbnailUtils", e);
            }
            BitmapFactory.Options options = new BitmapFactory.Options();
            BitmapFactory.Options options2 = new BitmapFactory.Options();
            if (bArr != null) {
                options2.inJustDecodeBounds = true;
                BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options2);
                options2.inSampleSize = a(options2, i, i2);
                i3 = options2.outWidth / options2.inSampleSize;
            } else {
                i3 = 0;
            }
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(str, options);
            options.inSampleSize = a(options, i, i2);
            int i4 = options.outWidth / options.inSampleSize;
            if (bArr == null || i3 < i4) {
                options.inJustDecodeBounds = false;
                bVar.b = BitmapFactory.decodeFile(str, options);
                return;
            }
            int i5 = options2.outWidth;
            int i6 = options2.outHeight;
            options2.inJustDecodeBounds = false;
            bVar.b = BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options2);
            if (bVar.b != null) {
                bVar.f33a = bArr;
                bVar.c = i5;
                bVar.d = i6;
            }
        }
    }
}
