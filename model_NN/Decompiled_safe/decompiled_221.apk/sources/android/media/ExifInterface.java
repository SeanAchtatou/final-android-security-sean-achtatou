package android.media;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

/* compiled from: ProGuard */
public class ExifInterface {
    public static final int ORIENTATION_FLIP_HORIZONTAL = 2;
    public static final int ORIENTATION_FLIP_VERTICAL = 4;
    public static final int ORIENTATION_NORMAL = 1;
    public static final int ORIENTATION_ROTATE_180 = 3;
    public static final int ORIENTATION_ROTATE_270 = 8;
    public static final int ORIENTATION_ROTATE_90 = 6;
    public static final int ORIENTATION_TRANSPOSE = 5;
    public static final int ORIENTATION_TRANSVERSE = 7;
    public static final int ORIENTATION_UNDEFINED = 0;
    public static final String TAG_DATETIME = "DateTime";
    public static final String TAG_FLASH = "Flash";
    public static final String TAG_FOCAL_LENGTH = "FocalLength";
    public static final String TAG_GPS_DATESTAMP = "GPSDateStamp";
    public static final String TAG_GPS_LATITUDE = "GPSLatitude";
    public static final String TAG_GPS_LATITUDE_REF = "GPSLatitudeRef";
    public static final String TAG_GPS_LONGITUDE = "GPSLongitude";
    public static final String TAG_GPS_LONGITUDE_REF = "GPSLongitudeRef";
    public static final String TAG_GPS_PROCESSING_METHOD = "GPSProcessingMethod";
    public static final String TAG_GPS_TIMESTAMP = "GPSTimeStamp";
    public static final String TAG_IMAGE_LENGTH = "ImageLength";
    public static final String TAG_IMAGE_WIDTH = "ImageWidth";
    public static final String TAG_MAKE = "Make";
    public static final String TAG_MODEL = "Model";
    public static final String TAG_ORIENTATION = "Orientation";
    public static final String TAG_WHITE_BALANCE = "WhiteBalance";
    public static final int WHITEBALANCE_AUTO = 0;
    public static final int WHITEBALANCE_MANUAL = 1;

    /* renamed from: a  reason: collision with root package name */
    private static SimpleDateFormat f32a = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");
    private static Object e = new Object();
    private String b;
    private HashMap<String, String> c;
    private boolean d;

    private native void commitChangesNative(String str);

    private native String getAttributesNative(String str);

    private native byte[] getThumbnailNative(String str);

    private native void saveAttributesNative(String str, String str2);

    static {
        System.loadLibrary("exif");
        f32a.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    public ExifInterface(String str) {
        this.b = str;
        a();
    }

    public String getAttribute(String str) {
        return this.c.get(str);
    }

    public int getAttributeInt(String str, int i) {
        String str2 = this.c.get(str);
        if (str2 == null) {
            return i;
        }
        try {
            return Integer.valueOf(str2).intValue();
        } catch (NumberFormatException e2) {
            return i;
        }
    }

    public double getAttributeDouble(String str, double d2) {
        String str2 = this.c.get(str);
        if (str2 == null) {
            return d2;
        }
        try {
            int indexOf = str2.indexOf("/");
            if (indexOf == -1) {
                return d2;
            }
            double parseDouble = Double.parseDouble(str2.substring(indexOf + 1));
            if (parseDouble != 0.0d) {
                return Double.parseDouble(str2.substring(0, indexOf)) / parseDouble;
            }
            return d2;
        } catch (NumberFormatException e2) {
            return d2;
        }
    }

    public void setAttribute(String str, String str2) {
        this.c.put(str, str2);
    }

    private void a() {
        String attributesNative;
        this.c = new HashMap<>();
        synchronized (e) {
            attributesNative = getAttributesNative(this.b);
        }
        int indexOf = attributesNative.indexOf(32);
        int parseInt = Integer.parseInt(attributesNative.substring(0, indexOf));
        int i = indexOf + 1;
        for (int i2 = 0; i2 < parseInt; i2++) {
            int indexOf2 = attributesNative.indexOf(61, i);
            String substring = attributesNative.substring(i, indexOf2);
            int i3 = indexOf2 + 1;
            int indexOf3 = attributesNative.indexOf(32, i3);
            int parseInt2 = Integer.parseInt(attributesNative.substring(i3, indexOf3));
            int i4 = indexOf3 + 1;
            String substring2 = attributesNative.substring(i4, i4 + parseInt2);
            i = parseInt2 + i4;
            if (substring.equals("hasThumbnail")) {
                this.d = substring2.equalsIgnoreCase("true");
            } else {
                this.c.put(substring, substring2);
            }
        }
    }

    public void saveAttributes() {
        StringBuilder sb = new StringBuilder();
        int size = this.c.size();
        if (this.c.containsKey("hasThumbnail")) {
            size--;
        }
        sb.append(size + " ");
        for (Map.Entry next : this.c.entrySet()) {
            String str = (String) next.getKey();
            if (!str.equals("hasThumbnail")) {
                String str2 = (String) next.getValue();
                sb.append(str + "=");
                sb.append(str2.length() + " ");
                sb.append(str2);
            }
        }
        String sb2 = sb.toString();
        synchronized (e) {
            saveAttributesNative(this.b, sb2);
            commitChangesNative(this.b);
        }
    }

    public boolean hasThumbnail() {
        return this.d;
    }

    public byte[] getThumbnail() {
        byte[] thumbnailNative;
        synchronized (e) {
            thumbnailNative = getThumbnailNative(this.b);
        }
        return thumbnailNative;
    }

    public boolean getLatLong(float[] fArr) {
        String str = this.c.get(TAG_GPS_LATITUDE);
        String str2 = this.c.get(TAG_GPS_LATITUDE_REF);
        String str3 = this.c.get(TAG_GPS_LONGITUDE);
        String str4 = this.c.get(TAG_GPS_LONGITUDE_REF);
        if (str == null || str2 == null || str3 == null || str4 == null) {
            return false;
        }
        fArr[0] = a(str, str2);
        fArr[1] = a(str3, str4);
        return true;
    }

    public long getDateTime() {
        String str = this.c.get(TAG_DATETIME);
        if (str == null) {
            return -1;
        }
        try {
            Date parse = f32a.parse(str, new ParsePosition(0));
            if (parse == null) {
                return -1;
            }
            return parse.getTime();
        } catch (IllegalArgumentException e2) {
            return -1;
        }
    }

    public long getGpsDateTime() {
        String str = this.c.get(TAG_GPS_DATESTAMP);
        String str2 = this.c.get(TAG_GPS_TIMESTAMP);
        if (str == null || str2 == null) {
            return -1;
        }
        String str3 = str + ' ' + str2;
        if (str3 == null) {
            return -1;
        }
        try {
            Date parse = f32a.parse(str3, new ParsePosition(0));
            if (parse == null) {
                return -1;
            }
            return parse.getTime();
        } catch (IllegalArgumentException e2) {
            return -1;
        }
    }

    private static float a(String str, String str2) {
        try {
            String[] split = str.split(",");
            String[] split2 = split[0].split("/");
            String[] split3 = split[1].split("/");
            String[] split4 = split[2].split("/");
            float parseFloat = ((Float.parseFloat(split4[0].trim()) / Float.parseFloat(split4[1].trim())) / 3600.0f) + ((float) ((int) (Float.parseFloat(split2[0].trim()) / Float.parseFloat(split2[1].trim())))) + (((float) ((int) (Float.parseFloat(split3[0].trim()) / Float.parseFloat(split3[1].trim())))) / 60.0f);
            if (str2.equals("S") || str2.equals("W")) {
                return -parseFloat;
            }
            return parseFloat;
        } catch (RuntimeException e2) {
            return 0.0f;
        }
    }
}
