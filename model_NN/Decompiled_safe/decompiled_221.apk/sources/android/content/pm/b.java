package android.content.pm;

import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.ZipInputStream;

/* compiled from: ProGuard */
public class b {
    /* JADX WARNING: Removed duplicated region for block: B:47:0x007e A[SYNTHETIC, Splitter:B:47:0x007e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.content.pm.a a(java.io.File r7, int r8) {
        /*
            r2 = 0
            android.content.pm.a r3 = new android.content.pm.a
            r3.<init>()
            if (r7 == 0) goto L_0x000e
            boolean r0 = r7.exists()
            if (r0 != 0) goto L_0x0010
        L_0x000e:
            r0 = r3
        L_0x000f:
            return r0
        L_0x0010:
            java.util.zip.ZipInputStream r0 = new java.util.zip.ZipInputStream     // Catch:{ FileNotFoundException -> 0x001f }
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x001f }
            r1.<init>(r7)     // Catch:{ FileNotFoundException -> 0x001f }
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x001f }
            r4 = r0
        L_0x001b:
            if (r4 != 0) goto L_0x0025
            r0 = r3
            goto L_0x000f
        L_0x001f:
            r0 = move-exception
            r0.printStackTrace()
            r4 = r2
            goto L_0x001b
        L_0x0025:
            r0 = 0
            r1 = r8 & 1
            if (r1 <= 0) goto L_0x002b
            r0 = 1
        L_0x002b:
            java.util.zip.ZipEntry r1 = r4.getNextEntry()     // Catch:{ Exception -> 0x00c7 }
            if (r1 == 0) goto L_0x0071
            java.lang.String r1 = r1.getName()     // Catch:{ Exception -> 0x00c7 }
            java.lang.String r5 = "resources.arsc"
            boolean r5 = r1.equals(r5)     // Catch:{ Exception -> 0x00c7 }
            if (r5 == 0) goto L_0x004f
            if (r0 == 0) goto L_0x004f
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x00c7 }
            r1.<init>()     // Catch:{ Exception -> 0x00c7 }
            a(r4, r1)     // Catch:{ Exception -> 0x006a }
            r1.flush()     // Catch:{ Exception -> 0x006a }
            r4.closeEntry()     // Catch:{ Exception -> 0x006a }
        L_0x004d:
            r2 = r1
            goto L_0x002b
        L_0x004f:
            java.lang.String r5 = "AndroidManifest.xml"
            boolean r1 = r1.equals(r5)     // Catch:{ Exception -> 0x00c7 }
            if (r1 == 0) goto L_0x005f
            android.content.pm.c.a(r4, r3, r8)     // Catch:{ Exception -> 0x00c7 }
            r4.closeEntry()     // Catch:{ Exception -> 0x00c7 }
            r1 = r2
            goto L_0x004d
        L_0x005f:
            r4.closeEntry()     // Catch:{ Exception -> 0x0064 }
            r1 = r2
            goto L_0x004d
        L_0x0064:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ Exception -> 0x00c7 }
            r1 = r2
            goto L_0x004d
        L_0x006a:
            r2 = move-exception
            r6 = r2
            r2 = r1
            r1 = r6
        L_0x006e:
            r1.printStackTrace()
        L_0x0071:
            r4.close()     // Catch:{ Throwable -> 0x0083 }
        L_0x0074:
            boolean r1 = android.content.b.a.e.a(r3)
            if (r0 == 0) goto L_0x007c
            if (r1 != 0) goto L_0x008d
        L_0x007c:
            if (r2 == 0) goto L_0x0081
            r2.close()     // Catch:{ Exception -> 0x0088 }
        L_0x0081:
            r0 = r3
            goto L_0x000f
        L_0x0083:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0074
        L_0x0088:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0081
        L_0x008d:
            if (r2 != 0) goto L_0x0092
            r0 = r3
            goto L_0x000f
        L_0x0092:
            java.io.ByteArrayInputStream r0 = new java.io.ByteArrayInputStream
            byte[] r1 = r2.toByteArray()
            r0.<init>(r1)
            r2.close()     // Catch:{ IOException -> 0x00bd }
        L_0x009e:
            android.content.b.a.e.a(r3, r0)
            r0.close()     // Catch:{ IOException -> 0x00c2 }
        L_0x00a4:
            java.lang.String r0 = r3.g
            if (r0 == 0) goto L_0x00ba
            java.lang.String r0 = r3.g
            r1 = 47
            int r0 = r0.lastIndexOf(r1)
            java.lang.String r1 = r3.g
            int r0 = r0 + 1
            java.lang.String r0 = r1.substring(r0)
            r3.h = r0
        L_0x00ba:
            r0 = r3
            goto L_0x000f
        L_0x00bd:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x009e
        L_0x00c2:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00a4
        L_0x00c7:
            r1 = move-exception
            goto L_0x006e
        */
        throw new UnsupportedOperationException("Method not decompiled: android.content.pm.b.a(java.io.File, int):android.content.pm.a");
    }

    private static void a(ZipInputStream zipInputStream, OutputStream outputStream) {
        byte[] bArr = new byte[8192];
        int i = 1;
        while (i != -1) {
            try {
                i = zipInputStream.read(bArr);
                if (i > 0) {
                    outputStream.write(bArr, 0, i);
                }
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
        }
    }
}
