package com.friendscoders.android.funbattery;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import java.net.URL;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public class SkinsActivity extends Activity {
    LazyAdapter adapter;
    public View.OnClickListener backListener = new View.OnClickListener() {
        public void onClick(View arg0) {
            SkinsActivity.this.finish();
        }
    };
    ProgressDialog dialog;
    ListView list;
    public View.OnClickListener refreshListener = new View.OnClickListener() {
        public void onClick(View arg0) {
            SkinsActivity.this.loadSkins();
            SkinsActivity.this.adapter.imageLoader.clearCache();
            SkinsActivity.this.adapter.notifyDataSetChanged();
        }
    };
    List<Skin> skins;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.skins_list);
        this.list = (ListView) findViewById(R.id.list);
        loadSkins();
        ((Button) findViewById(R.id.btnBack)).setOnClickListener(this.backListener);
        ((Button) findViewById(R.id.btnRefresh)).setOnClickListener(this.refreshListener);
        ((ImageView) findViewById(R.id.imgFC)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SkinsActivity.this.startActivity(new Intent(SkinsActivity.this, CreditsActivity.class));
            }
        });
    }

    /* access modifiers changed from: private */
    public void viewSkin(int i) {
        Skin skin = this.skins.get(i);
        Intent intent = new Intent(this, SkinDetailsActivity.class);
        intent.putExtra("folder", skin.getFolder());
        intent.putExtra("name", skin.getName());
        intent.putExtra("skin", skin);
        startActivity(intent);
    }

    public void onDestroy() {
        this.adapter.imageLoader.stopThread();
        this.list.setAdapter((ListAdapter) null);
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    public XMLReader initializeReader() throws ParserConfigurationException, SAXException {
        return SAXParserFactory.newInstance().newSAXParser().getXMLReader();
    }

    private class ListThread extends Thread {
        private Handler handler = new Handler() {
            public void handleMessage(Message msg) {
                SkinsActivity.this.skins = ListThread.this.sk;
                SkinsActivity.this.adapter = new LazyAdapter(SkinsActivity.this, SkinsActivity.this.skins);
                SkinsActivity.this.list.setAdapter((ListAdapter) SkinsActivity.this.adapter);
                SkinsActivity.this.list.setClickable(true);
                SkinsActivity.this.list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
                        SkinsActivity.this.viewSkin(position);
                    }
                });
                SkinsActivity.this.dialog.dismiss();
            }
        };
        List<Skin> sk;

        public ListThread() {
        }

        public void run() {
            try {
                URL url = new URL("http://www.friendscoders.com/android/funbattery/skinsxml.php");
                XMLReader xr = SkinsActivity.this.initializeReader();
                SkinsHandler skinsHandler = new SkinsHandler();
                xr.setContentHandler(skinsHandler);
                xr.parse(new InputSource(url.openStream()));
                this.sk = skinsHandler.retrieveSkins();
            } catch (Exception e) {
                Log.e("SKINS", "Parsing error", e);
            } finally {
                this.handler.sendEmptyMessage(0);
            }
        }
    }

    /* access modifiers changed from: private */
    public void loadSkins() {
        this.dialog = ProgressDialog.show(this, "", "Loading...", true);
        new ListThread().start();
    }
}
