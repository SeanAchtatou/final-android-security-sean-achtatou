package com.friendscoders.android.funbattery;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;
import com.friendscoders.android.funbattery.util.Utils;

public class MenuActivity extends Activity {
    String[] items = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.menu);
        ((ImageView) findViewById(R.id.imgFC)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MenuActivity.this.startActivity(new Intent(MenuActivity.this, CreditsActivity.class));
            }
        });
        ((ImageButton) findViewById(R.id.btnUsage)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MenuActivity.this.startActivity(new Intent("android.intent.action.POWER_USAGE_SUMMARY"));
            }
        });
        ((ImageButton) findViewById(R.id.btnDisplay)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent("com.android.settings.DISPLAY_SETTINGS");
                intent.addFlags(268435456);
                intent.addCategory("android.intent.category.DEFAULT");
                MenuActivity.this.startActivity(intent);
            }
        });
        ((ImageButton) findViewById(R.id.btnSkins)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Utils.isConnected(MenuActivity.this.getApplicationContext())) {
                    MenuActivity.this.startActivity(new Intent(MenuActivity.this, SkinsActivity.class));
                    return;
                }
                Toast.makeText(MenuActivity.this.getApplicationContext(), (int) R.string.msgInternet, 0).show();
            }
        });
        ((ImageButton) findViewById(R.id.btnAbout)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MenuActivity.this.startActivity(new Intent(MenuActivity.this, AboutActivity.class));
            }
        });
        ((ImageButton) findViewById(R.id.btnCredits)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MenuActivity.this.startActivity(new Intent(MenuActivity.this, CreditsActivity.class));
            }
        });
        ((ImageButton) findViewById(R.id.btnClose)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MenuActivity.this.finish();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
