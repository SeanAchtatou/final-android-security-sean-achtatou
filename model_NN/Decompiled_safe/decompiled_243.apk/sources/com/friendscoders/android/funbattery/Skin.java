package com.friendscoders.android.funbattery;

import java.io.Serializable;

public class Skin implements Serializable {
    public static final long serialVersionUID = 5780;
    private String author = "";
    private int downloads = 0;
    private String folder = "";
    private int id = 0;
    private String name = "";
    private int votes = 0;

    public int getId() {
        return this.id;
    }

    public void setId(int id2) {
        this.id = id2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getFolder() {
        return this.folder;
    }

    public void setFolder(String folder2) {
        this.folder = folder2;
    }

    public String toString() {
        return "Skin: " + this.id + " | " + this.name;
    }

    public void setDownloads(int downloads2) {
        this.downloads = downloads2;
    }

    public int getDownloads() {
        return this.downloads;
    }

    public void setVotes(int votes2) {
        this.votes = votes2;
    }

    public int getVotes() {
        return this.votes;
    }

    public String getAuthor() {
        return this.author;
    }

    public void setAuthor(String author2) {
        this.author = author2;
    }
}
