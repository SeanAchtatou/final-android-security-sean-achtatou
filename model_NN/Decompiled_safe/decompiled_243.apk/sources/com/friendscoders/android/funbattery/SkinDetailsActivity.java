package com.friendscoders.android.funbattery;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.friendscoders.android.funbattery.FunBatteryWidget;
import com.friendscoders.android.funbattery.util.Utils;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

public class SkinDetailsActivity extends Activity {
    public View.OnClickListener backListener = new View.OnClickListener() {
        public void onClick(View arg0) {
            SkinDetailsActivity.this.finish();
        }
    };
    private File cacheDir = null;
    /* access modifiers changed from: private */
    public ProgressDialog dialog;
    public View.OnClickListener downloadListener = new View.OnClickListener() {
        public void onClick(View arg0) {
            SkinDetailsActivity.this.dialog = ProgressDialog.show(SkinDetailsActivity.this, "", "Downloading...", true);
            new DownloadThread().start();
        }
    };
    /* access modifiers changed from: private */
    public Skin skin;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.skin_details);
        this.skin = (Skin) getIntent().getSerializableExtra("skin");
        ((TextView) findViewById(R.id.txtSkinName)).setText(this.skin.getName());
        ((ImageView) findViewById(R.id.icon)).setImageBitmap(getIcon());
        ((TextView) findViewById(R.id.txtSkinDownloads)).setText(String.valueOf(this.skin.getDownloads()));
        ((TextView) findViewById(R.id.txtSkinAuthor)).setText(String.valueOf(this.skin.getAuthor()));
        Button btnDownload = (Button) findViewById(R.id.btnDownload);
        btnDownload.setOnClickListener(this.downloadListener);
        ((Button) findViewById(R.id.btnBack)).setOnClickListener(this.backListener);
        ((ImageView) findViewById(R.id.imgFC)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SkinDetailsActivity.this.startActivity(new Intent(SkinDetailsActivity.this, CreditsActivity.class));
            }
        });
        SharedPreferences settings = getSharedPreferences(FunBatteryWidget.PREFS_NAME, 0);
        String skin_folder = "";
        if (settings != null) {
            skin_folder = settings.getString(FunBatteryWidget.KEY_SKIN, FunBatteryWidget.SKIN_DEFAULT);
        }
        if (skin_folder.equalsIgnoreCase(this.skin.getFolder())) {
            btnDownload.setEnabled(false);
            btnDownload.setVisibility(4);
            ((TextView) findViewById(R.id.txtMsg)).setText("N.B.: " + getString(R.string.msgSkinAlreadyInstalled));
        }
    }

    private Bitmap getIcon() {
        try {
            new File(Utils.PATH_SD + this.skin.getFolder()).mkdirs();
            String urlSD = Utils.PATH_SD + this.skin.getFolder() + Utils.ICON;
            Utils.downloadFile(Utils.URL + this.skin.getFolder() + Utils.ICON, urlSD);
            return BitmapFactory.decodeFile(urlSD);
        } catch (Exception e) {
            Log.e("FUN BATTERY", "Icon Error: " + e.getMessage());
            return null;
        }
    }

    public void onDestroy() {
        super.onDestroy();
        try {
            if (this.cacheDir != null) {
                this.cacheDir.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class DownloadThread extends Thread {
        private Handler handler = new Handler() {
            public void handleMessage(Message msg) {
                if (SkinDetailsActivity.this.dialog != null) {
                    SkinDetailsActivity.this.dialog.dismiss();
                }
                Toast.makeText(SkinDetailsActivity.this.getApplicationContext(), (int) R.string.msgSkinDownload, 0).show();
                SkinDetailsActivity.this.finish();
            }
        };

        public DownloadThread() {
        }

        public void run() {
            try {
                new File(Utils.PATH_SD + SkinDetailsActivity.this.skin.getFolder()).mkdirs();
                for (String img : Utils.IMAGES) {
                    Utils.downloadFile(Utils.URL + SkinDetailsActivity.this.skin.getFolder() + img, Utils.PATH_SD + SkinDetailsActivity.this.skin.getFolder() + img);
                }
                SharedPreferences settings = SkinDetailsActivity.this.getSharedPreferences(FunBatteryWidget.PREFS_NAME, 0);
                if (settings != null) {
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString(FunBatteryWidget.KEY_SKIN, SkinDetailsActivity.this.skin.getFolder());
                    editor.commit();
                }
                SkinDetailsActivity.this.getApplicationContext().startService(new Intent(SkinDetailsActivity.this.getApplicationContext(), FunBatteryWidget.UpdateService.class));
                updateDownloads();
            } catch (Exception e) {
                Log.e("SKIN", "Download error", e);
            } finally {
                this.handler.sendEmptyMessage(0);
            }
        }

        private void updateDownloads() {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://www.friendscoders.com/android/funbattery/updateDownloads.php");
            try {
                ArrayList<NameValuePair> postParameters = new ArrayList<>();
                postParameters.add(new BasicNameValuePair("id", String.valueOf(SkinDetailsActivity.this.skin.getId())));
                httppost.setEntity(new UrlEncodedFormEntity(postParameters));
                httpclient.execute(httppost);
            } catch (ClientProtocolException e) {
                Log.e("POST", e.getMessage());
            } catch (IOException e2) {
                Log.e("POST", e2.getMessage());
            }
        }
    }
}
