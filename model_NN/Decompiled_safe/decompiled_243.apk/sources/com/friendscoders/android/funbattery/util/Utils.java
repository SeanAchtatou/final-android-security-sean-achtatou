package com.friendscoders.android.funbattery.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import java.io.InputStream;
import java.io.OutputStream;

public final class Utils {
    private static final int BUFFER_SIZE = 4096;
    public static final String ICON = "icon.png";
    public static final String[] IMAGES = {"level0.png", "level20.png", "level40.png", "level60.png", "level80.png", "level100.png"};
    public static final String PATH_SD = "/sdcard/app/funbattery/";
    public static final String URL = "http://www.friendscoders.com/android/funbattery/";
    public static final String XML_SKINS = "skinsxml.php";

    public static void CopyStream(InputStream is, OutputStream os) {
        try {
            byte[] bytes = new byte[1024];
            while (true) {
                int count = is.read(bytes, 0, 1024);
                if (count != -1) {
                    os.write(bytes, 0, count);
                } else {
                    return;
                }
            }
        } catch (Exception e) {
        }
    }

    /* JADX WARN: Type inference failed for: r10v5, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0051 A[SYNTHETIC, Splitter:B:22:0x0051] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0056 A[Catch:{ IOException -> 0x007a }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0063 A[SYNTHETIC, Splitter:B:31:0x0063] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0068 A[Catch:{ IOException -> 0x0071 }] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x006d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean downloadFile(java.lang.String r14, java.lang.String r15) {
        /*
            r13 = -1
            r12 = 0
            r9 = 0
            r1 = 0
            r3 = 0
            java.net.URL r8 = new java.net.URL     // Catch:{ Exception -> 0x004e, all -> 0x0060 }
            r8.<init>(r14)     // Catch:{ Exception -> 0x004e, all -> 0x0060 }
            java.net.URLConnection r10 = r8.openConnection()     // Catch:{ Exception -> 0x004e, all -> 0x0060 }
            r0 = r10
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x004e, all -> 0x0060 }
            r9 = r0
            java.io.BufferedInputStream r2 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x004e, all -> 0x0060 }
            java.io.InputStream r10 = r9.getInputStream()     // Catch:{ Exception -> 0x004e, all -> 0x0060 }
            r2.<init>(r10)     // Catch:{ Exception -> 0x004e, all -> 0x0060 }
            java.io.BufferedOutputStream r4 = new java.io.BufferedOutputStream     // Catch:{ Exception -> 0x007c, all -> 0x0073 }
            java.io.FileOutputStream r10 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x007c, all -> 0x0073 }
            r10.<init>(r15)     // Catch:{ Exception -> 0x007c, all -> 0x0073 }
            r4.<init>(r10)     // Catch:{ Exception -> 0x007c, all -> 0x0073 }
            r6 = 4096(0x1000, float:5.74E-42)
            r10 = 4096(0x1000, float:5.74E-42)
            byte[] r5 = new byte[r10]     // Catch:{ Exception -> 0x007f, all -> 0x0076 }
            r7 = 0
        L_0x002c:
            r10 = 0
            r11 = 4096(0x1000, float:5.74E-42)
            int r7 = r2.read(r5, r10, r11)     // Catch:{ Exception -> 0x007f, all -> 0x0076 }
            if (r7 == r13) goto L_0x0039
            r10 = 0
            r4.write(r5, r10, r7)     // Catch:{ Exception -> 0x007f, all -> 0x0076 }
        L_0x0039:
            if (r7 != r13) goto L_0x002c
            if (r2 == 0) goto L_0x0040
            r2.close()     // Catch:{ IOException -> 0x0083 }
        L_0x0040:
            if (r4 == 0) goto L_0x0045
            r4.close()     // Catch:{ IOException -> 0x0083 }
        L_0x0045:
            if (r9 == 0) goto L_0x004a
            r9.disconnect()
        L_0x004a:
            r10 = 1
            r3 = r4
            r1 = r2
        L_0x004d:
            return r10
        L_0x004e:
            r10 = move-exception
        L_0x004f:
            if (r1 == 0) goto L_0x0054
            r1.close()     // Catch:{ IOException -> 0x007a }
        L_0x0054:
            if (r3 == 0) goto L_0x0059
            r3.close()     // Catch:{ IOException -> 0x007a }
        L_0x0059:
            if (r9 == 0) goto L_0x005e
            r9.disconnect()
        L_0x005e:
            r10 = r12
            goto L_0x004d
        L_0x0060:
            r10 = move-exception
        L_0x0061:
            if (r1 == 0) goto L_0x0066
            r1.close()     // Catch:{ IOException -> 0x0071 }
        L_0x0066:
            if (r3 == 0) goto L_0x006b
            r3.close()     // Catch:{ IOException -> 0x0071 }
        L_0x006b:
            if (r9 == 0) goto L_0x0070
            r9.disconnect()
        L_0x0070:
            throw r10
        L_0x0071:
            r11 = move-exception
            goto L_0x006b
        L_0x0073:
            r10 = move-exception
            r1 = r2
            goto L_0x0061
        L_0x0076:
            r10 = move-exception
            r3 = r4
            r1 = r2
            goto L_0x0061
        L_0x007a:
            r10 = move-exception
            goto L_0x0059
        L_0x007c:
            r10 = move-exception
            r1 = r2
            goto L_0x004f
        L_0x007f:
            r10 = move-exception
            r3 = r4
            r1 = r2
            goto L_0x004f
        L_0x0083:
            r10 = move-exception
            goto L_0x0045
        */
        throw new UnsupportedOperationException("Method not decompiled: com.friendscoders.android.funbattery.util.Utils.downloadFile(java.lang.String, java.lang.String):boolean");
    }

    public static boolean isConnected(Context cnt) {
        ConnectivityManager connec = (ConnectivityManager) cnt.getSystemService("connectivity");
        if ((connec != null && connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) || connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED) {
            return true;
        }
        if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED || connec.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED) {
            return false;
        }
        return false;
    }
}
