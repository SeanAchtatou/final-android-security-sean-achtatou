package com.friendscoders.android.funbattery;

import java.util.ArrayList;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SkinsHandler extends DefaultHandler {
    private StringBuffer buffer = new StringBuffer();
    private Skin skin = null;
    private List<Skin> skins = null;

    public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
        this.buffer.setLength(0);
        if (localName.equals("skins")) {
            this.skins = new ArrayList();
        } else if (localName.equals("skin")) {
            this.skin = new Skin();
            this.skin.setId(Integer.parseInt(atts.getValue("id")));
        }
    }

    public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
        if (localName.equals("skin")) {
            this.skins.add(this.skin);
        } else if (localName.equals("folder")) {
            this.skin.setFolder(this.buffer.toString());
        } else if (localName.equals("name")) {
            this.skin.setName(this.buffer.toString());
        } else if (localName.equals("downloads")) {
            this.skin.setDownloads(new Integer(this.buffer.toString()).intValue());
        } else if (localName.equals("author")) {
            this.skin.setAuthor(this.buffer.toString());
        }
    }

    public void characters(char[] ch, int start, int length) {
        this.buffer.append(ch, start, length);
    }

    public List<Skin> retrieveSkins() {
        return this.skins;
    }
}
