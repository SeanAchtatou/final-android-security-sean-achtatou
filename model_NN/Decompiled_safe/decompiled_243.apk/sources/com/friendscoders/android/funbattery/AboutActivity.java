package com.friendscoders.android.funbattery;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

public class AboutActivity extends Activity {
    public View.OnClickListener backListener = new View.OnClickListener() {
        public void onClick(View arg0) {
            AboutActivity.this.finish();
        }
    };
    WebView browser;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.about);
        String txtAbout = getResources().getString(R.string.txtAbout);
        String txtAbout1 = getResources().getString(R.string.txtAbout1);
        String txtAbout2 = getResources().getString(R.string.txtAbout2);
        this.browser = (WebView) findViewById(R.id.webkit);
        this.browser.getSettings().setJavaScriptEnabled(true);
        this.browser.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
        this.browser.getSettings().setPluginsEnabled(false);
        this.browser.getSettings().setSupportMultipleWindows(false);
        this.browser.getSettings().setSupportZoom(false);
        this.browser.setVerticalScrollBarEnabled(true);
        this.browser.setHorizontalScrollBarEnabled(false);
        this.browser.loadDataWithBaseURL("fake://not/needed", "<html><body style=\"text-align:center; background: #FFF; color: #000000;\"><a href=\"http://www.friendscoders.com\"><img src=\"file:///android_asset/friendscoders.jpg\"/></a><br/><br/><br/><p>" + txtAbout + "</p>" + "<p>" + txtAbout1 + "</p>" + "<p>" + txtAbout2 + "</p>" + "</body>" + "</html>", "text/html", "UTF-8", "");
        ((Button) findViewById(R.id.btnBack)).setOnClickListener(this.backListener);
    }
}
