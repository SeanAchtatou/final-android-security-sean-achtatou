package com.friendscoders.android.funbattery;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.friendscoders.android.funbattery.util.Utils;
import java.util.List;

public class LazyAdapter extends BaseAdapter {
    private static LayoutInflater inflater = null;
    private Activity activity;
    public ImageLoader imageLoader = new ImageLoader(this.activity.getApplicationContext());
    private List<Skin> skins;

    public static class ViewHolder {
        public ImageView image;
        public TextView text;
    }

    public LazyAdapter(Activity a, List<Skin> s) {
        this.activity = a;
        this.skins = s;
        inflater = (LayoutInflater) this.activity.getSystemService("layout_inflater");
    }

    public int getCount() {
        if (this.skins != null) {
            return this.skins.size();
        }
        return 0;
    }

    public Object getItem(int position) {
        return Integer.valueOf(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View vi = convertView;
        if (convertView == null) {
            vi = inflater.inflate((int) R.layout.item, (ViewGroup) null);
            holder = new ViewHolder();
            holder.text = (TextView) vi.findViewById(R.id.text);
            holder.image = (ImageView) vi.findViewById(R.id.image);
            vi.setTag(holder);
        } else {
            holder = (ViewHolder) vi.getTag();
        }
        Skin skin = this.skins.get(position);
        holder.text.setText(skin.getName());
        holder.image.setTag(Utils.URL + skin.getFolder() + Utils.ICON);
        this.imageLoader.DisplayImage(Utils.URL + skin.getFolder() + Utils.ICON, this.activity, holder.image);
        return vi;
    }
}
