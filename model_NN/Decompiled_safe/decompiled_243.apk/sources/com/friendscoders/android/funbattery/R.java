package com.friendscoders.android.funbattery;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int btn_normal = 2130837504;
        public static final int btn_normal_disabled = 2130837505;
        public static final int btn_pressed = 2130837506;
        public static final int btn_selected = 2130837507;
        public static final int btn_settings = 2130837508;
        public static final int charging = 2130837509;
        public static final int friendscoders = 2130837510;
        public static final int ic_launcher = 2130837511;
        public static final int icon = 2130837512;
        public static final int icon_about = 2130837513;
        public static final int icon_close = 2130837514;
        public static final int icon_credits = 2130837515;
        public static final int icon_display = 2130837516;
        public static final int icon_skins = 2130837517;
        public static final int icon_usage = 2130837518;
        public static final int level0 = 2130837519;
        public static final int level100 = 2130837520;
        public static final int level20 = 2130837521;
        public static final int level40 = 2130837522;
        public static final int level60 = 2130837523;
        public static final int level80 = 2130837524;
        public static final int min_friendscoders = 2130837525;
        public static final int rounded_edges = 2130837526;
        public static final int stub = 2130837527;
    }

    public static final class id {
        public static final int adView = 2131165197;
        public static final int batterytext = 2131165216;
        public static final int btnAbout = 2131165193;
        public static final int btnBack = 2131165185;
        public static final int btnClose = 2131165195;
        public static final int btnCredits = 2131165194;
        public static final int btnDisplay = 2131165191;
        public static final int btnDownload = 2131165211;
        public static final int btnRefresh = 2131165213;
        public static final int btnSkins = 2131165192;
        public static final int btnUsage = 2131165190;
        public static final int charging = 2131165217;
        public static final int icon = 2131165202;
        public static final int image = 2131165186;
        public static final int imgFC = 2131165196;
        public static final int layoutMenu = 2131165188;
        public static final int level = 2131165215;
        public static final int list = 2131165212;
        public static final int menu = 2131165198;
        public static final int menuLayout = 2131165189;
        public static final int selection = 2131165199;
        public static final int skinDetailsLayout = 2131165203;
        public static final int skin_icon = 2131165200;
        public static final int skin_name = 2131165201;
        public static final int text = 2131165187;
        public static final int txtAuthor = 2131165208;
        public static final int txtDownloads = 2131165206;
        public static final int txtMsg = 2131165210;
        public static final int txtName = 2131165204;
        public static final int txtSkinAuthor = 2131165209;
        public static final int txtSkinDownloads = 2131165207;
        public static final int txtSkinName = 2131165205;
        public static final int webkit = 2131165184;
        public static final int widget = 2131165214;
    }

    public static final class layout {
        public static final int about = 2130903040;
        public static final int credits = 2130903041;
        public static final int item = 2130903042;
        public static final int menu = 2130903043;
        public static final int settings = 2130903044;
        public static final int skin_data_row = 2130903045;
        public static final int skin_details = 2130903046;
        public static final int skins_list = 2130903047;
        public static final int widget = 2130903048;
    }

    public static final class string {
        public static final int app_name = 2131034112;
        public static final int author = 2131034128;
        public static final int back = 2131034131;
        public static final int close = 2131034132;
        public static final int download = 2131034129;
        public static final int downloads = 2131034126;
        public static final int menu = 2131034113;
        public static final int mnuAbout = 2131034117;
        public static final int mnuClose = 2131034119;
        public static final int mnuCredits = 2131034118;
        public static final int mnuDisplay = 2131034115;
        public static final int mnuSkins = 2131034116;
        public static final int mnuUsage = 2131034114;
        public static final int msgInternet = 2131034120;
        public static final int msgSkinAlreadyInstalled = 2131034122;
        public static final int msgSkinDownload = 2131034121;
        public static final int name = 2131034125;
        public static final int refresh = 2131034130;
        public static final int skinDetails = 2131034124;
        public static final int skinsList = 2131034123;
        public static final int txtAbout = 2131034133;
        public static final int txtAbout1 = 2131034134;
        public static final int txtAbout2 = 2131034135;
        public static final int votes = 2131034127;
    }

    public static final class style {
        public static final int btnStyle = 2131099648;
    }

    public static final class xml {
        public static final int skins = 2130968576;
        public static final int widget_def = 2130968577;
    }
}
