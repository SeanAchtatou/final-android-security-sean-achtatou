package com.friendscoders.android.funbattery;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import com.friendscoders.android.funbattery.FunBatteryWidget;

public class FunBatteryInfo extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        try {
            if (intent.getAction().equals("android.intent.action.BATTERY_CHANGED")) {
                SharedPreferences settings = context.getSharedPreferences(FunBatteryWidget.PREFS_NAME, 0);
                if (settings != null) {
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putInt(FunBatteryWidget.KEY_LEVEL, intent.getIntExtra("level", 0));
                    editor.putInt(FunBatteryWidget.KEY_CHARGING, intent.getIntExtra("status", 1));
                    editor.putInt(FunBatteryWidget.KEY_VOLTAGE, intent.getIntExtra("voltage", 0));
                    editor.commit();
                }
                context.startService(new Intent(context, FunBatteryWidget.UpdateService.class));
            }
        } catch (Exception e) {
        }
    }
}
