package com.friendscoders.android.funbattery;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

public class HTTPPostActivity extends Activity {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://www.friendscoders.com/android/funbattery/adddownload.php");
        try {
            HttpParams params = new BasicHttpParams();
            params.setParameter("idSkin", "1");
            httppost.setParams(params);
            ArrayList<NameValuePair> postParameters = new ArrayList<>();
            postParameters.add(new BasicNameValuePair("idskin", "11"));
            httppost.setEntity(new UrlEncodedFormEntity(postParameters));
            Log.i("POST", EntityUtils.toString(httpclient.execute(httppost).getEntity()));
        } catch (ClientProtocolException e) {
            Log.e("POST", e.getMessage());
        } catch (IOException e2) {
            Log.e("POST", e2.getMessage());
        }
    }
}
