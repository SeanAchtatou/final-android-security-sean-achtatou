package com.friendscoders.android.funbattery;

import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;
import com.friendscoders.android.funbattery.util.Utils;
import java.io.File;

public class FunBatteryWidget extends AppWidgetProvider {
    public static String KEY_CHARGING = "BATWIDG_CHARGING";
    public static String KEY_LEVEL = "BATWIDG_LEVEL";
    public static String KEY_SKIN = "SKIN";
    public static String KEY_VOLTAGE = "BATWIDG_VOLTAGE";
    public static String PREFS_NAME = "BATWIDG_PREFS";
    public static String SKIN_DEFAULT = "";

    public void onEnabled(Context context) {
        super.onEnabled(context);
    }

    public void onDisabled(Context context) {
        super.onDisabled(context);
        try {
            context.stopService(new Intent(context, UpdateService.class));
        } catch (Exception e) {
            Log.d("BatteryWidget", "Exception on disnable: ", e);
        }
    }

    public void onDeleted(Context context, int[] appWidgetIds) {
        super.onDeleted(context, appWidgetIds);
        try {
            context.stopService(new Intent(context, UpdateService.class));
        } catch (Exception e) {
            Log.d("BatteryWidget", "Exception on delete: ", e);
        }
    }

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        context.startService(new Intent(context, UpdateService.class));
    }

    public static class UpdateService extends Service {
        FunBatteryInfo mBI = null;

        public void onStart(Intent intent, int startId) {
            if (this.mBI == null) {
                this.mBI = new FunBatteryInfo();
                IntentFilter mIntentFilter = new IntentFilter();
                mIntentFilter.addAction("android.intent.action.BATTERY_CHANGED");
                registerReceiver(this.mBI, mIntentFilter);
            }
            RemoteViews updateViews = buildUpdate(this);
            if (updateViews != null) {
                try {
                    ComponentName thisWidget = new ComponentName(this, FunBatteryWidget.class);
                    AppWidgetManager manager = AppWidgetManager.getInstance(this);
                    if (manager != null && updateViews != null) {
                        manager.updateAppWidget(thisWidget, updateViews);
                    }
                } catch (Exception e) {
                    Log.e("Widget", "Update Service Failed to Start", e);
                }
            }
        }

        public void onDestroy() {
            super.onDestroy();
            try {
                if (this.mBI != null) {
                    unregisterReceiver(this.mBI);
                }
            } catch (Exception e) {
                Log.e("Widget", "Failed to unregister", e);
            }
        }

        public RemoteViews buildUpdate(Context context) {
            String levelText;
            RemoteViews updateViews = new RemoteViews(context.getPackageName(), (int) R.layout.widget);
            int level = 0;
            boolean charging = false;
            try {
                SharedPreferences settings = getSharedPreferences(FunBatteryWidget.PREFS_NAME, 0);
                String skin_folder = "";
                if (settings != null) {
                    skin_folder = settings.getString(FunBatteryWidget.KEY_SKIN, FunBatteryWidget.SKIN_DEFAULT);
                    level = settings.getInt(FunBatteryWidget.KEY_LEVEL, 0);
                    charging = settings.getInt(FunBatteryWidget.KEY_CHARGING, 1) == 2;
                }
                boolean skin_default = skin_folder.equalsIgnoreCase(FunBatteryWidget.SKIN_DEFAULT);
                if (level < 11) {
                    if (skin_default) {
                        updateViews.setImageViewResource(R.id.level, R.drawable.level0);
                    } else {
                        updateViews.setImageViewUri(R.id.level, Uri.parse(new File(Utils.PATH_SD + skin_folder + "level0.png").toString()));
                    }
                } else if (level < 21) {
                    if (skin_default) {
                        updateViews.setImageViewResource(R.id.level, R.drawable.level20);
                    } else {
                        updateViews.setImageViewUri(R.id.level, Uri.parse(new File(Utils.PATH_SD + skin_folder + "level20.png").toString()));
                    }
                } else if (level < 41) {
                    if (skin_default) {
                        updateViews.setImageViewResource(R.id.level, R.drawable.level40);
                    } else {
                        updateViews.setImageViewUri(R.id.level, Uri.parse(new File(Utils.PATH_SD + skin_folder + "level40.png").toString()));
                    }
                } else if (level < 61) {
                    if (skin_default) {
                        updateViews.setImageViewResource(R.id.level, R.drawable.level60);
                    } else {
                        updateViews.setImageViewUri(R.id.level, Uri.parse(new File(Utils.PATH_SD + skin_folder + "level60.png").toString()));
                    }
                } else if (level < 81) {
                    if (skin_default) {
                        updateViews.setImageViewResource(R.id.level, R.drawable.level80);
                    } else {
                        updateViews.setImageViewUri(R.id.level, Uri.parse(new File(Utils.PATH_SD + skin_folder + "level80.png").toString()));
                    }
                } else if (level < 101) {
                    if (skin_default) {
                        updateViews.setImageViewResource(R.id.level, R.drawable.level100);
                    } else {
                        updateViews.setImageViewUri(R.id.level, Uri.parse(new File(Utils.PATH_SD + skin_folder + "level100.png").toString()));
                    }
                }
                updateViews.setViewVisibility(R.id.batterytext, 0);
                updateViews.setViewVisibility(R.id.charging, charging ? 0 : 4);
                if (level == 100) {
                    levelText = "100";
                } else {
                    levelText = String.valueOf(level) + "%";
                }
                if (level == 0) {
                    levelText = " 0%";
                }
                updateViews.setTextViewText(R.id.batterytext, levelText);
            } catch (Exception e) {
                Log.e("BatteryWidget", "Error Updating Views", e);
            }
            try {
                updateViews.setOnClickPendingIntent(R.id.widget, PendingIntent.getActivity(context, 0, new Intent(context, MenuActivity.class), 0));
            } catch (Exception e2) {
                Log.e("FunBatteryWidget", "Error Settings Intents", e2);
            }
            return updateViews;
        }

        public IBinder onBind(Intent intent) {
            return null;
        }
    }
}
