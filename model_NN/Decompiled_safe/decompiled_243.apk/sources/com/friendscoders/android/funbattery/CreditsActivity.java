package com.friendscoders.android.funbattery;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

public class CreditsActivity extends Activity {
    public View.OnClickListener backListener = new View.OnClickListener() {
        public void onClick(View arg0) {
            CreditsActivity.this.finish();
        }
    };
    WebView browser;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.credits);
        this.browser = (WebView) findViewById(R.id.webkit);
        this.browser.getSettings().setJavaScriptEnabled(true);
        this.browser.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
        this.browser.getSettings().setPluginsEnabled(false);
        this.browser.getSettings().setSupportMultipleWindows(false);
        this.browser.getSettings().setSupportZoom(false);
        this.browser.setVerticalScrollBarEnabled(true);
        this.browser.setHorizontalScrollBarEnabled(false);
        this.browser.loadDataWithBaseURL("fake://not/needed", "<html><body style=\"text-align:center; background: #FFF; color: #000000;\"><a href=\"http://www.friendscoders.com\"><img src=\"file:///android_asset/friendscoders.jpg\"/></a><br/><br/><br/><a href=\"http://www.friendscoders.com\">www.friendscoders.com</a><br/><br/><br/><a href=\"mailto:info@.friendscoders.com\">info@friendscoders.com</a></body></html>", "text/html", "UTF-8", "");
        ((Button) findViewById(R.id.btnBack)).setOnClickListener(this.backListener);
    }
}
