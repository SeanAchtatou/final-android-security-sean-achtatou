package defpackage;

import android.os.Bundle;
import java.io.Serializable;
import java.util.HashMap;

/* renamed from: e  reason: default package */
public final class e {

    /* renamed from: a  reason: collision with root package name */
    private String f136a;
    private HashMap<String, String> b;

    public e(Bundle bundle) {
        this.f136a = bundle.getString("action");
        Serializable serializable = bundle.getSerializable("params");
        this.b = serializable instanceof HashMap ? (HashMap) serializable : null;
    }

    public e(String str) {
        this.f136a = str;
    }

    public e(String str, HashMap<String, String> hashMap) {
        this(str);
        this.b = hashMap;
    }

    public final Bundle a() {
        Bundle bundle = new Bundle();
        bundle.putString("action", this.f136a);
        bundle.putSerializable("params", this.b);
        return bundle;
    }

    public final String b() {
        return this.f136a;
    }

    public final HashMap<String, String> c() {
        return this.b;
    }
}
