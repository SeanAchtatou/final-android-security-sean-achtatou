package com.ballgame.android.sldemocore;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.TextView;

public class Bubble extends Activity {
    public static final int CBGMUSIC = 2;
    public static final int CCKMUSIC = 4;
    public static final int OBGMUSIC = 1;
    public static final int OCKMUSIC = 3;
    private BBView mBBView;
    public MediaPlayer mBurstMP;
    public MediaPlayer mClickMP;
    public boolean mIsMusic = true;
    public MediaPlayer mMusicMP;
    private Window window;

    public void PlayClick() {
        if (this.mClickMP == null) {
            this.mClickMP = MediaPlayer.create(getBaseContext(), (int) R.raw.click);
        }
        if (this.mClickMP != null && this.mClickMP.isPlaying()) {
            this.mClickMP.stop();
        }
        if (this.mClickMP != null) {
            this.mClickMP.start();
        }
    }

    public void PlayBurst() {
        if (this.mBurstMP == null) {
            this.mBurstMP = MediaPlayer.create(getBaseContext(), (int) R.raw.lclick);
        }
        if (this.mBurstMP != null && this.mBurstMP.isPlaying()) {
            this.mBurstMP.stop();
        }
        if (this.mBurstMP != null) {
            this.mBurstMP.start();
        }
    }

    public void InitSounds() {
        this.mClickMP = MediaPlayer.create(getBaseContext(), (int) R.raw.click);
        this.mBurstMP = MediaPlayer.create(getBaseContext(), (int) R.raw.lclick);
        int streamVolume = ((AudioManager) getBaseContext().getSystemService("audio")).getStreamVolume(3);
        this.mClickMP.setVolume((float) streamVolume, (float) streamVolume);
        this.mBurstMP.setVolume((float) streamVolume, (float) streamVolume);
    }

    public void ExitSounds() {
        if (this.mClickMP != null) {
            if (this.mClickMP.isPlaying()) {
                this.mClickMP.stop();
            }
            this.mClickMP.release();
            this.mClickMP = null;
        }
        if (this.mBurstMP != null) {
            if (this.mBurstMP.isPlaying()) {
                this.mBurstMP.stop();
            }
            this.mBurstMP.release();
            this.mBurstMP = null;
        }
    }

    public void PlaySounds(int num) {
        if (this.mBBView.bSound) {
            if (num < 6 && num > 0) {
                PlayClick();
            } else if (num >= 6) {
                PlayBurst();
            }
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mMusicMP = null;
        this.mClickMP = null;
        this.mBurstMP = null;
        this.mIsMusic = ReadMusicSet();
        requestWindowFeature(1);
        this.window = getWindow();
        this.window.setFlags(1024, 1024);
        setContentView((int) R.layout.bb_layout);
        this.mBBView = (BBView) findViewById(R.id.bubblebreaker);
        this.mBBView.bSound = ReadSoundSet();
        this.mBBView.setTextView((TextView) findViewById(R.id.text));
        this.mBBView.setMain(this);
        this.mBBView.setMode(1);
    }

    public void finish() {
        try {
            this.mBBView.mMode = 0;
            this.mBBView.mTimer.stopTimer();
            stopMusic();
            this.mBBView.clearsound();
            this.mBBView.freeAlloc();
        } finally {
            super.finish();
        }
    }

    public boolean ReadSoundSet() {
        return getSharedPreferences("monkeyfly_Bubble", 0).getBoolean("cb_bgsound", true);
    }

    public boolean ReadMusicSet() {
        return getSharedPreferences("monkeyfly_Bubble", 0).getBoolean("cb_bgmusic", true);
    }

    public void WriteMusicSet(boolean btemp) {
        SharedPreferences.Editor mye = getSharedPreferences("monkeyfly_Bubble", 0).edit();
        mye.putBoolean("cb_bgmusic", btemp);
        mye.commit();
    }

    public void WriteSoundSet(boolean btemp) {
        SharedPreferences.Editor mye = getSharedPreferences("monkeyfly_Bubble", 0).edit();
        mye.putBoolean("cb_bgsound", btemp);
        mye.commit();
    }

    public void ToMainView(int score) {
        Intent intent = new Intent(this, GameResultActivity.class);
        intent.putExtra("GAME_RESULT", score);
        intent.putExtra("GAME_MODE", 0);
        startActivity(intent);
        finish();
    }

    public void ToFirstView() {
        finish();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 1, 0, (int) R.string.Menu_Open_Bg);
        menu.add(0, 2, 0, (int) R.string.Menu_Close_Bg);
        menu.add(0, 3, 1, (int) R.string.Menu_Open_Ck);
        menu.add(0, 4, 1, (int) R.string.Menu_Close_Ck);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                this.mIsMusic = true;
                playMusic();
                WriteMusicSet(true);
                break;
            case 2:
                stopMusic();
                WriteMusicSet(false);
                break;
            case 3:
                this.mBBView.bSound = true;
                WriteSoundSet(true);
                break;
            case 4:
                this.mBBView.bSound = false;
                WriteSoundSet(false);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void playMusic() {
        if (this.mIsMusic) {
            stopMusic();
            if (this.mMusicMP == null) {
                this.mMusicMP = MediaPlayer.create(getBaseContext(), (int) R.raw.bg_02);
            }
            this.mMusicMP.start();
            this.mMusicMP.setLooping(true);
        }
    }

    public void stopMusic() {
        if (this.mMusicMP != null) {
            if (this.mMusicMP.isPlaying()) {
                this.mMusicMP.stop();
            }
            this.mMusicMP.release();
            this.mMusicMP = null;
        }
    }
}
