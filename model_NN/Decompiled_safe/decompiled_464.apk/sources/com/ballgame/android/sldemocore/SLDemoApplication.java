package com.ballgame.android.sldemocore;

import android.app.Application;

public class SLDemoApplication extends Application {
    private static final String GAME_ID = "b71e140a-89a6-4d87-8506-63222236ff42";
    static final int GAME_MODE_COUNT = 1;
    static final int GAME_MODE_MIN = 0;
    private static final String GAME_SECRET = "/7QkixLf2r9yrnC7TT5adTzLx0InfmMGWZOCPiOKGei2QO6pAcRyVg==";

    public void onCreate() {
        super.onCreate();
        ScoreloopManager.init(this, GAME_ID, GAME_SECRET);
    }
}
