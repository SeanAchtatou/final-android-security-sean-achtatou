package com.ballgame.android.sldemocore;

public final class R {

    public static final class array {
        public static final int game_modes = 2131034112;
    }

    public static final class attr {
        public static final int adSize = 2130771974;
        public static final int adUnitId = 2130771975;
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771971;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771972;
        public static final int secondaryTextColor = 2130771970;
        public static final int testing = 2130771973;
    }

    public static final class color {
        public static final int default_error_text = 2131165186;
        public static final int default_highlight_background = 2131165184;
        public static final int user_info_background = 2131165185;
    }

    public static final class drawable {
        public static final int bg_menu = 2130837504;
        public static final int btn_default = 2130837505;
        public static final int btn_focus = 2130837506;
        public static final int btn_press = 2130837507;
        public static final int btn_press_bottom = 2130837508;
        public static final int btn_pressed = 2130837509;
        public static final int bubble_blue = 2130837510;
        public static final int bubble_cyan = 2130837511;
        public static final int bubble_green = 2130837512;
        public static final int bubble_magenta = 2130837513;
        public static final int bubble_red = 2130837514;
        public static final int bubble_yellow = 2130837515;
        public static final int button_background_focus = 2130837516;
        public static final int button_background_focus2 = 2130837517;
        public static final int button_background_normal = 2130837518;
        public static final int button_background_pressed = 2130837519;
        public static final int dialog_divider_horizontal_light = 2130837520;
        public static final int gad_bg_splash = 2130837521;
        public static final int gad_btn_close = 2130837522;
        public static final int gad_btn_download = 2130837523;
        public static final int gad_btn_moregame = 2130837524;
        public static final int gad_close_down = 2130837525;
        public static final int gad_close_up = 2130837526;
        public static final int gad_download_down = 2130837527;
        public static final int gad_download_up = 2130837528;
        public static final int gad_more_down = 2130837529;
        public static final int gad_more_up = 2130837530;
        public static final int game_logo = 2130837531;
        public static final int gmbk = 2130837532;
        public static final int icon = 2130837533;
        public static final int menu_bkg = 2130837534;
        public static final int menu_my = 2130837535;
        public static final int select = 2130837536;
        public static final int title2 = 2130837537;
        public static final int top = 2130837538;
        public static final int topold = 2130837539;
    }

    public static final class id {
        public static final int BANNER = 2131099648;
        public static final int Button01 = 2131099703;
        public static final int Button02 = 2131099704;
        public static final int IAB_BANNER = 2131099650;
        public static final int IAB_LEADERBOARD = 2131099651;
        public static final int IAB_MRECT = 2131099649;
        public static final int TextView01 = 2131099710;
        public static final int TextView02 = 2131099689;
        public static final int TextView03 = 2131099720;
        public static final int accept_button = 2131099662;
        public static final int ad2 = 2131099658;
        public static final int ad_splash_layout = 2131099652;
        public static final int btn_close = 2131099655;
        public static final int btn_download = 2131099653;
        public static final int btn_exit = 2131099707;
        public static final int btn_load_next = 2131099696;
        public static final int btn_load_prev = 2131099694;
        public static final int btn_more = 2131099654;
        public static final int btn_show_me = 2131099695;
        public static final int bubblebreaker = 2131099656;
        public static final int buttons = 2131099661;
        public static final int challenge1_controls = 2131099687;
        public static final int challenge2_controls = 2131099690;
        public static final int challenge_button = 2131099699;
        public static final int challenge_info = 2131099659;
        public static final int challenges_list = 2131099665;
        public static final int choose_opponent_button = 2131099676;
        public static final int email = 2131099724;
        public static final int exit = 2131099729;
        public static final int facebook_connection_checkbox = 2131099713;
        public static final int facebook_receiver_checkbox = 2131099718;
        public static final int facebook_status = 2131099714;
        public static final int game_mode = 2131099678;
        public static final int game_over_button = 2131099681;
        public static final int img_icon = 2131099708;
        public static final int label = 2131099705;
        public static final int list_view = 2131099697;
        public static final int lv = 2131099706;
        public static final int menu = 2131099670;
        public static final int message_text = 2131099721;
        public static final int myspace_connection_checkbox = 2131099711;
        public static final int myspace_receiver_checkbox = 2131099717;
        public static final int myspace_status = 2131099712;
        public static final int name = 2131099666;
        public static final int new_challenge_balance = 2131099671;
        public static final int new_challenge_balance_too_low = 2131099675;
        public static final int new_challenge_play_button = 2131099677;
        public static final int new_challenge_stake_display = 2131099674;
        public static final int new_challenge_stake_header = 2131099672;
        public static final int new_challenge_stake_slider = 2131099673;
        public static final int normal_controls = 2131099685;
        public static final int opponent_info = 2131099660;
        public static final int player_name = 2131099701;
        public static final int post_message_button = 2131099722;
        public static final int post_status = 2131099723;
        public static final int price = 2131099693;
        public static final int profile = 2131099728;
        public static final int random_score_button = 2131099680;
        public static final int rank_button = 2131099683;
        public static final int rank_info = 2131099684;
        public static final int reject_button = 2131099663;
        public static final int rootChallengesOpen = 2131099669;
        public static final int score_board = 2131099727;
        public static final int score_edit = 2131099679;
        public static final int score_info = 2131099702;
        public static final int score_rank = 2131099700;
        public static final int score_results = 2131099692;
        public static final int search_list_spinner = 2131099682;
        public static final int stake = 2131099667;
        public static final int start_game = 2131099726;
        public static final int status = 2131099668;
        public static final int submitted_challenge_score = 2131099688;
        public static final int submitted_score = 2131099686;
        public static final int tabhost = 2131099664;
        public static final int text = 2131099657;
        public static final int tv_name = 2131099709;
        public static final int twitter_connection_checkbox = 2131099715;
        public static final int twitter_receiver_checkbox = 2131099719;
        public static final int twitter_status = 2131099716;
        public static final int update_profile_button = 2131099725;
        public static final int user_info = 2131099698;
        public static final int won_lost = 2131099691;
    }

    public static final class layout {
        public static final int ad_splash = 2130903040;
        public static final int bb_layout = 2130903041;
        public static final int challenge_confirm = 2130903042;
        public static final int challenges = 2130903043;
        public static final int challenges_history_list = 2130903044;
        public static final int challenges_history_list_item = 2130903045;
        public static final int challenges_open_list = 2130903046;
        public static final int challenges_open_list_item = 2130903047;
        public static final int chmenu = 2130903048;
        public static final int direct_challenge = 2130903049;
        public static final int game_play = 2130903050;
        public static final int game_result = 2130903051;
        public static final int highscores = 2130903052;
        public static final int highscores_action = 2130903053;
        public static final int highscores_list_item = 2130903054;
        public static final int main = 2130903055;
        public static final int menu = 2130903056;
        public static final int menu_item = 2130903057;
        public static final int menuold = 2130903058;
        public static final int open_challenge = 2130903059;
        public static final int popup_lv_app = 2130903060;
        public static final int popup_lv_item_app = 2130903061;
        public static final int post_message = 2130903062;
        public static final int profile = 2130903063;
        public static final int splash = 2130903064;
    }

    public static final class raw {
        public static final int bg_02 = 2130968576;
        public static final int click = 2130968577;
        public static final int lclick = 2130968578;
    }

    public static final class string {
        public static final int Menu_Close_Bg = 2131230821;
        public static final int Menu_Close_Ck = 2131230823;
        public static final int Menu_Open_Bg = 2131230820;
        public static final int Menu_Open_Ck = 2131230822;
        public static final int app_name = 2131230720;
        public static final int bb_layout_text_text = 2131230815;
        public static final int btn_accept_challenge = 2131230740;
        public static final int btn_check_rank = 2131230732;
        public static final int btn_choose_opponent = 2131230739;
        public static final int btn_directly_challenge_this_user = 2131230737;
        public static final int btn_game_over = 2131230731;
        public static final int btn_load_next = 2131230734;
        public static final int btn_load_prev = 2131230733;
        public static final int btn_open_url = 2131230728;
        public static final int btn_play_challenge = 2131230738;
        public static final int btn_random_score = 2131230730;
        public static final int btn_reject_challenge = 2131230741;
        public static final int btn_show_me = 2131230735;
        public static final int btn_update_profile = 2131230736;
        public static final int bubble_breaker = 2131230814;
        public static final int chalenge_stake_cap = 2131230747;
        public static final int challenge_anyone = 2131230750;
        public static final int challenge_anyone_cap = 2131230749;
        public static final int challenge_assigned = 2131230755;
        public static final int challenge_complete = 2131230756;
        public static final int challenge_confirmation_format = 2131230769;
        public static final int challenge_invalid_cap = 2131230748;
        public static final int challenge_new_menu_item_direct = 2131230746;
        public static final int challenge_new_menu_item_open = 2131230745;
        public static final int challenge_open = 2131230757;
        public static final int challenge_open_cap = 2131230752;
        public static final int challenge_opponent_format = 2131230770;
        public static final int challenge_other_cap = 2131230753;
        public static final int challenge_pending_cap = 2131230754;
        public static final int challenge_rejected_cap = 2131230751;
        public static final int challenge_score_result_format = 2131230761;
        public static final int challenge_stake_lost_format = 2131230760;
        public static final int challenge_stake_won_format = 2131230759;
        public static final int challenge_tab_history = 2131230743;
        public static final int challenge_tab_new = 2131230744;
        public static final int challenge_tab_open = 2131230742;
        public static final int challenge_won_lost_format = 2131230758;
        public static final int error_balance_to_low = 2131230784;
        public static final int error_message_cannot_accept_challenge = 2131230773;
        public static final int error_message_cannot_reject_challenge = 2131230774;
        public static final int error_message_challenge_upload = 2131230776;
        public static final int error_message_email_already_taken = 2131230782;
        public static final int error_message_insufficient_balance = 2131230775;
        public static final int error_message_invalid_email_format = 2131230783;
        public static final int error_message_name_already_taken = 2131230781;
        public static final int error_message_network = 2131230780;
        public static final int error_message_not_on_highscore_list = 2131230779;
        public static final int error_message_request_cancelled = 2131230777;
        public static final int error_message_self_challenge = 2131230772;
        public static final int error_message_user_info_update_failed = 2131230778;
        public static final int facebook_comm_cancelled = 2131230804;
        public static final int facebook_comm_failed = 2131230803;
        public static final int facebook_comm_ok = 2131230802;
        public static final int facebook_invalid_credentials = 2131230805;
        public static final int game_mode_label = 2131230785;
        public static final int game_score_label = 2131230786;
        public static final int history_challenge_info_format = 2131230771;
        public static final int lost = 2131230798;
        public static final int mExit = 2131230825;
        public static final int mScore = 2131230824;
        public static final int menu_item_challenges = 2131230723;
        public static final int menu_item_highscores = 2131230722;
        public static final int menu_item_payment = 2131230727;
        public static final int menu_item_play = 2131230721;
        public static final int menu_item_post_message = 2131230726;
        public static final int menu_item_profile = 2131230725;
        public static final int menu_item_users = 2131230724;
        public static final int message_no_auth = 2131230729;
        public static final int message_post_failed = 2131230801;
        public static final int message_post_ok = 2131230800;
        public static final int mode_lose_prefix = 2131230818;
        public static final int mode_lose_suffix = 2131230819;
        public static final int mode_pause = 2131230817;
        public static final int mode_ready = 2131230816;
        public static final int money_format = 2131230799;
        public static final int myspace_comm_cancelled = 2131230808;
        public static final int myspace_comm_failed = 2131230807;
        public static final int myspace_comm_ok = 2131230806;
        public static final int myspace_invalid_credentials = 2131230809;
        public static final int name_cap = 2131230795;
        public static final int new_challenge_balance = 2131230767;
        public static final int new_challenge_stake_header = 2131230768;
        public static final int new_direct_challenge = 2131230765;
        public static final int new_open_challenge = 2131230766;
        public static final int play_for_random_score = 2131230793;
        public static final int profile_email_label = 2131230764;
        public static final int profile_name_label = 2131230763;
        public static final int progress_message_default = 2131230794;
        public static final int ranking_check_failed = 2131230791;
        public static final int ranking_check_label = 2131230792;
        public static final int score_submit_failed = 2131230790;
        public static final int status_cap = 2131230796;
        public static final int submitted_challenge_score_info = 2131230789;
        public static final int submitted_challenge_score_label = 2131230788;
        public static final int submitted_score_label = 2131230787;
        public static final int twitter_comm_cancelled = 2131230812;
        public static final int twitter_comm_failed = 2131230811;
        public static final int twitter_comm_ok = 2131230810;
        public static final int twitter_invalid_credentials = 2131230813;
        public static final int user_info_format = 2131230762;
        public static final int won = 2131230797;
    }

    public static final class styleable {
        public static final int[] com_google_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval, R.attr.testing, R.attr.adSize, R.attr.adUnitId};
        public static final int com_google_ads_AdView_adSize = 6;
        public static final int com_google_ads_AdView_adUnitId = 7;
        public static final int com_google_ads_AdView_backgroundColor = 0;
        public static final int com_google_ads_AdView_keywords = 3;
        public static final int com_google_ads_AdView_primaryTextColor = 1;
        public static final int com_google_ads_AdView_refreshInterval = 4;
        public static final int com_google_ads_AdView_secondaryTextColor = 2;
        public static final int com_google_ads_AdView_testing = 5;
    }
}
