package com.ballgame.android.sldemocore;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import java.util.Vector;

public class ScoreBubble {
    private final int CONST_SHOW_INTERVAL = 50;
    private final int CONST_SHOW_TIME = 1200;
    private final int FONT_COLOR = Color.rgb(255, 255, 255);
    private final int FRONT_FONT_COLOR = Color.rgb(34, 138, 34);
    private final int MAX_TEXT_SIZE = 30;
    private final int MIN_TEXT_SIZE = 22;
    private int height;
    private Typeface mFont;
    private Typeface mOldFont;
    private Vector<Point> vector = new Vector<>();
    private int width;

    public ScoreBubble(int w, int h) {
        this.width = w;
        this.height = h;
    }

    public void setFont(Typeface font) {
        this.mFont = font;
    }

    public void Add(int x, int y, int count, int score) {
        long t = System.currentTimeMillis();
        if (x > this.width - 100) {
            x -= 130;
        }
        if (x < 20) {
            x += 15;
        }
        if (y > this.height - 10) {
            y -= 10;
        }
        this.vector.addElement(new Point(x, y, count, score, t));
    }

    public void show(Canvas c, Paint p) {
        c.save();
        c.clipRect(0, 0, this.width, this.height);
        this.mOldFont = p.getTypeface();
        int fontColor = p.getColor();
        float fontSize = p.getTextSize();
        int fontFlags = p.getFlags();
        if (this.mFont != null) {
            p.setTypeface(this.mFont);
        }
        for (int i = this.vector.size() - 1; i >= 0; i--) {
            Point point = this.vector.get(i);
            String str = String.valueOf(Integer.toString(point.count)) + "HIT +" + Integer.toString(point.score);
            if (System.currentTimeMillis() - point.t <= 1200) {
                int currentTimeMillis = ((int) (System.currentTimeMillis() - point.t)) / 50;
                if (point.curTextSize < 30) {
                    point.curTextSize++;
                }
                float nowFontSize = (float) point.curTextSize;
                point.x = point.x;
                point.y -= 7;
                int x = point.x;
                int y = point.y;
                p.setColor(this.FRONT_FONT_COLOR);
                p.setTextSize(1.0f + nowFontSize);
                p.setFlags(1);
                c.drawText(str, (float) (x - 1), (float) (y - 1), p);
                c.drawText(str, (float) x, (float) (y - 1), p);
                c.drawText(str, (float) (x + 1), (float) y, p);
                c.drawText(str, (float) x, (float) (y + 1), p);
                p.setColor(this.FONT_COLOR);
                p.setTextSize(nowFontSize);
                c.drawText(str, (float) x, (float) y, p);
            } else {
                this.vector.remove(i);
            }
        }
        c.restore();
        p.setColor(fontColor);
        p.setTextSize(fontSize);
        p.setFlags(fontFlags);
        p.setTypeface(this.mOldFont);
    }

    class Point {
        int count;
        int curTextSize = 22;
        int score;
        long t;
        int x;
        int y;

        Point(int x2, int y2, int count2, int score2, long t2) {
            this.x = x2;
            this.y = y2;
            this.t = t2;
            this.count = count2;
            this.score = score2;
        }
    }
}
