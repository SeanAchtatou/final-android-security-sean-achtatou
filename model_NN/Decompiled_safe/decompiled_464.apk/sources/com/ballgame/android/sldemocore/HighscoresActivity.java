package com.ballgame.android.sldemocore;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.ballgame.android.sldemocore.ChallengesActivity;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.controller.ScoresController;
import com.scoreloop.client.android.core.model.Range;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.SearchList;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import java.util.Iterator;
import java.util.List;

public class HighscoresActivity extends BaseActivity {
    private static final int FIXED_OFFSET = 3;
    private static final int RANGE_LENGTH = 25;
    private static final int RANGE_POSITION = 0;
    /* access modifiers changed from: private */
    public ListView highScoresListView;
    /* access modifiers changed from: private */
    public boolean initialLoadDone;
    /* access modifiers changed from: private */
    public LoadingType loadingType;
    private Button meButton;
    /* access modifiers changed from: private */
    public Button nextRangeButton;
    /* access modifiers changed from: private */
    public Button prevRangeButton;
    /* access modifiers changed from: private */
    public ScoresController scoresController;

    enum LoadingType {
        ME,
        OTHER
    }

    private final class ChallengeUserOnClickListener implements AdapterView.OnItemClickListener {
        private ChallengeUserOnClickListener() {
        }

        /* synthetic */ ChallengeUserOnClickListener(HighscoresActivity highscoresActivity, ChallengeUserOnClickListener challengeUserOnClickListener) {
            this();
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        }
    }

    private class ScoresControllerObserver implements RequestControllerObserver {
        private ScoresControllerObserver() {
        }

        /* synthetic */ ScoresControllerObserver(HighscoresActivity highscoresActivity, ScoresControllerObserver scoresControllerObserver) {
            this();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ballgame.android.sldemocore.HighscoresActivity.access$0(com.ballgame.android.sldemocore.HighscoresActivity, boolean):void
         arg types: [com.ballgame.android.sldemocore.HighscoresActivity, int]
         candidates:
          com.ballgame.android.sldemocore.BaseActivity.access$0(com.ballgame.android.sldemocore.BaseActivity, int):void
          com.ballgame.android.sldemocore.HighscoresActivity.access$0(com.ballgame.android.sldemocore.HighscoresActivity, boolean):void */
        public void requestControllerDidFail(RequestController requestController, Exception exception) {
            HighscoresActivity.this.hideProgressIndicator();
            if (!HighscoresActivity.this.isRequestCancellation(exception)) {
                HighscoresActivity.this.initialLoadDone = true;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ballgame.android.sldemocore.HighscoresActivity.access$0(com.ballgame.android.sldemocore.HighscoresActivity, boolean):void
         arg types: [com.ballgame.android.sldemocore.HighscoresActivity, int]
         candidates:
          com.ballgame.android.sldemocore.BaseActivity.access$0(com.ballgame.android.sldemocore.BaseActivity, int):void
          com.ballgame.android.sldemocore.HighscoresActivity.access$0(com.ballgame.android.sldemocore.HighscoresActivity, boolean):void */
        public void requestControllerDidReceiveResponse(RequestController aRequestController) {
            List<Score> scores = HighscoresActivity.this.scoresController.getScores();
            HighscoresActivity.this.highScoresListView.setAdapter((ListAdapter) new ScoreViewAdapter(HighscoresActivity.this, R.layout.highscores, scores));
            HighscoresActivity.this.prevRangeButton.setEnabled(HighscoresActivity.this.scoresController.hasPreviousRange());
            HighscoresActivity.this.nextRangeButton.setEnabled(HighscoresActivity.this.scoresController.hasNextRange());
            HighscoresActivity.this.hideProgressIndicator();
            if (HighscoresActivity.this.loadingType == LoadingType.ME) {
                boolean loginFound = false;
                User currentUser = Session.getCurrentSession().getUser();
                int idx = 0;
                Iterator it = scores.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    } else if (((Score) it.next()).getUser().equals(currentUser)) {
                        loginFound = true;
                        break;
                    } else {
                        idx++;
                    }
                }
                if (loginFound) {
                    HighscoresActivity.this.highScoresListView.setSelection(HighscoresActivity.this.calculateUserScorePosition(idx));
                } else {
                    HighscoresActivity.this.showDialog(1);
                }
            }
            HighscoresActivity.this.initialLoadDone = true;
        }
    }

    private class ScoreViewAdapter extends ArrayAdapter<Score> {
        public ScoreViewAdapter(Context context, int resource, List<Score> objects) {
            super(context, resource, objects);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            int c;
            View view = convertView;
            if (view == null) {
                view = HighscoresActivity.this.getLayoutInflater().inflate((int) R.layout.highscores_list_item, (ViewGroup) null);
            }
            Score score = (Score) getItem(position);
            ((TextView) view.findViewById(R.id.score_rank)).setText(new StringBuilder().append(HighscoresActivity.this.scoresController.getLoadedRange().getLocation() + position + 1).toString(), (TextView.BufferType) null);
            ((TextView) view.findViewById(R.id.player_name)).setText(score.getUser().getLogin(), (TextView.BufferType) null);
            ((TextView) view.findViewById(R.id.score_info)).setText(new StringBuilder().append(score.getResult().intValue()).toString(), (TextView.BufferType) null);
            if (score.getUser().equals(Session.getCurrentSession().getUser())) {
                c = HighscoresActivity.this.getResources().getColor(R.color.default_highlight_background);
            } else {
                c = 0;
            }
            view.setBackgroundColor(c);
            return view;
        }
    }

    private final class SelectOpponentOnClickListener implements AdapterView.OnItemClickListener {
        static final /* synthetic */ boolean $assertionsDisabled = (!HighscoresActivity.class.desiredAssertionStatus());

        private SelectOpponentOnClickListener() {
        }

        /* synthetic */ SelectOpponentOnClickListener(HighscoresActivity highscoresActivity, SelectOpponentOnClickListener selectOpponentOnClickListener) {
            this();
        }

        public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
            Score tappedScore = (Score) adapter.getItemAtPosition(position);
            if ($assertionsDisabled || tappedScore != null) {
                User opponent = tappedScore.getUser();
                if (opponent.equals(Session.getCurrentSession().getUser())) {
                    HighscoresActivity.this.showDialog(4);
                    return;
                }
                ScoreloopManager.setPossibleOpponent(opponent);
                Intent intent = new Intent(HighscoresActivity.this, DirectChallengeActivity.class);
                intent.setFlags(67108864);
                HighscoresActivity.this.startActivity(intent);
                return;
            }
            throw new AssertionError();
        }
    }

    /* access modifiers changed from: package-private */
    public void onSearchListsAvailable() {
        initSearchListChooser(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapter, View view, int position, long id) {
                HighscoresActivity.this.scoresController.setSearchList((SearchList) adapter.getItemAtPosition(position));
                HighscoresActivity.this.scoresController.setRange(new Range(0, HighscoresActivity.RANGE_LENGTH));
                HighscoresActivity.this.loadRange(false, LoadingType.OTHER);
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    /* access modifiers changed from: private */
    public int calculateUserScorePosition(int index) {
        if (index < 3) {
            return 0;
        }
        return index - 3;
    }

    /* access modifiers changed from: private */
    public void loadRange(boolean isInitialLoad, LoadingType loadingType2) {
        if (isInitialLoad || this.initialLoadDone) {
            this.loadingType = loadingType2;
            this.scoresController.loadRange();
            showProgressIndicator();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.highscores);
        this.scoresController = new ScoresController(new ScoresControllerObserver(this, null));
        if (!Session.getCurrentSession().isAuthenticated()) {
            requestSearchLists();
        } else {
            onSearchListsAvailable();
        }
        getGameModeChooser(null, true).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                HighscoresActivity.this.scoresController.setMode(Integer.valueOf(position));
                HighscoresActivity.this.loadRange(false, LoadingType.OTHER);
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.prevRangeButton = (Button) findViewById(R.id.btn_load_prev);
        this.prevRangeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HighscoresActivity.this.loadingType = LoadingType.OTHER;
                HighscoresActivity.this.scoresController.loadPreviousRange();
                HighscoresActivity.this.showProgressIndicator();
            }
        });
        this.nextRangeButton = (Button) findViewById(R.id.btn_load_next);
        this.nextRangeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HighscoresActivity.this.loadingType = LoadingType.OTHER;
                HighscoresActivity.this.scoresController.loadNextRange();
                HighscoresActivity.this.showProgressIndicator();
            }
        });
        this.meButton = (Button) findViewById(R.id.btn_show_me);
        this.meButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HighscoresActivity.this.loadingType = LoadingType.ME;
                HighscoresActivity.this.scoresController.loadRangeForUser(Session.getCurrentSession().getUser());
                HighscoresActivity.this.showProgressIndicator();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        AdapterView.OnItemClickListener listener;
        super.onStart();
        this.highScoresListView = (ListView) findViewById(R.id.list_view);
        if (getIntent().getIntExtra("HIGH_SCORES_SELECTION_MODE", ChallengesActivity.NewChallengeModes.NONE.ordinal()) == ChallengesActivity.NewChallengeModes.DIRECT.ordinal()) {
            listener = new SelectOpponentOnClickListener(this, null);
        } else {
            listener = new ChallengeUserOnClickListener(this, null);
        }
        this.highScoresListView.setOnItemClickListener(listener);
        loadRange(true, LoadingType.OTHER);
    }
}
