package com.ballgame.android.sldemocore;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import com.scoreloop.client.android.core.controller.MessageController;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.controller.SocialProviderController;
import com.scoreloop.client.android.core.controller.SocialProviderControllerObserver;
import com.scoreloop.client.android.core.model.FacebookSocialProvider;
import com.scoreloop.client.android.core.model.MySpaceSocialProvider;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.SocialProvider;
import com.scoreloop.client.android.core.model.TwitterSocialProvider;

public class PostMessageActivity extends BaseActivity {
    CheckBox facebookConnectionCheckBox;
    /* access modifiers changed from: private */
    public SocialProviderController facebookController;
    /* access modifiers changed from: private */
    public SocialProvider facebookProvider;
    CheckBox facebookReceiverCheckBox;
    /* access modifiers changed from: private */
    public TextView facebookStatusText;
    /* access modifiers changed from: private */
    public MessageController messageController;
    CheckBox myspaceConnectionCheckBox;
    /* access modifiers changed from: private */
    public SocialProviderController myspaceController;
    /* access modifiers changed from: private */
    public SocialProvider myspaceProvider;
    CheckBox myspaceReceiverCheckBox;
    /* access modifiers changed from: private */
    public TextView myspaceStatusText;
    Button postButton;
    /* access modifiers changed from: private */
    public TextView postingStatusText;
    /* access modifiers changed from: private */
    public CheckBox twitterConnectionCheckBox;
    /* access modifiers changed from: private */
    public SocialProviderController twitterController;
    /* access modifiers changed from: private */
    public SocialProvider twitterProvider;
    /* access modifiers changed from: private */
    public CheckBox twitterReceiverCheckBox;
    /* access modifiers changed from: private */
    public TextView twitterStatusText;

    private final class MessageControllerObserver implements RequestControllerObserver {
        private MessageControllerObserver() {
        }

        /* synthetic */ MessageControllerObserver(PostMessageActivity postMessageActivity, MessageControllerObserver messageControllerObserver) {
            this();
        }

        public void requestControllerDidReceiveResponse(RequestController requestcontroller) {
            PostMessageActivity.this.hideProgressIndicator();
            PostMessageActivity.this.postingStatusText.setText(PostMessageActivity.this.getString(R.string.message_post_ok));
        }

        public void requestControllerDidFail(RequestController requestcontroller, Exception exception) {
            PostMessageActivity.this.hideProgressIndicator();
            PostMessageActivity.this.postingStatusText.setText(String.valueOf(PostMessageActivity.this.getString(R.string.message_post_failed)) + exception.getMessage());
        }
    }

    private class FacebookObserver implements SocialProviderControllerObserver {
        private FacebookObserver() {
        }

        /* synthetic */ FacebookObserver(PostMessageActivity postMessageActivity, FacebookObserver facebookObserver) {
            this();
        }

        public void didFail(Throwable exception) {
            PostMessageActivity.this.facebookConnectionCheckBox.setEnabled(true);
            PostMessageActivity.this.facebookConnectionCheckBox.setChecked(false);
            PostMessageActivity.this.facebookReceiverCheckBox.setEnabled(false);
            PostMessageActivity.this.facebookStatusText.setText(String.valueOf(PostMessageActivity.this.getString(R.string.facebook_comm_failed)) + exception.getMessage());
            exception.printStackTrace();
        }

        public void didSucceed() {
            PostMessageActivity.this.facebookReceiverCheckBox.setEnabled(true);
            PostMessageActivity.this.facebookStatusText.setText(PostMessageActivity.this.getString(R.string.facebook_comm_ok));
        }

        public void userDidCancel() {
            PostMessageActivity.this.facebookConnectionCheckBox.setEnabled(true);
            PostMessageActivity.this.facebookConnectionCheckBox.setChecked(false);
            PostMessageActivity.this.facebookReceiverCheckBox.setEnabled(false);
            PostMessageActivity.this.facebookStatusText.setText(PostMessageActivity.this.getString(R.string.facebook_comm_cancelled));
        }

        public void didEnterInvalidCredentials() {
            PostMessageActivity.this.facebookConnectionCheckBox.setEnabled(true);
            PostMessageActivity.this.facebookConnectionCheckBox.setChecked(false);
            PostMessageActivity.this.facebookReceiverCheckBox.setEnabled(false);
            PostMessageActivity.this.facebookStatusText.setText(PostMessageActivity.this.getString(R.string.facebook_invalid_credentials));
        }
    }

    private class TwitterObserver implements SocialProviderControllerObserver {
        private TwitterObserver() {
        }

        /* synthetic */ TwitterObserver(PostMessageActivity postMessageActivity, TwitterObserver twitterObserver) {
            this();
        }

        public void didFail(Throwable exception) {
            PostMessageActivity.this.twitterConnectionCheckBox.setEnabled(true);
            PostMessageActivity.this.twitterConnectionCheckBox.setChecked(false);
            PostMessageActivity.this.twitterReceiverCheckBox.setEnabled(false);
            PostMessageActivity.this.twitterStatusText.setText(String.valueOf(PostMessageActivity.this.getString(R.string.twitter_comm_failed)) + exception.getMessage());
            exception.printStackTrace();
        }

        public void didSucceed() {
            PostMessageActivity.this.twitterReceiverCheckBox.setEnabled(true);
            PostMessageActivity.this.twitterStatusText.setText(PostMessageActivity.this.getString(R.string.twitter_comm_ok));
        }

        public void userDidCancel() {
            PostMessageActivity.this.twitterConnectionCheckBox.setEnabled(true);
            PostMessageActivity.this.twitterConnectionCheckBox.setChecked(false);
            PostMessageActivity.this.twitterReceiverCheckBox.setEnabled(false);
            PostMessageActivity.this.twitterStatusText.setText(PostMessageActivity.this.getString(R.string.twitter_comm_cancelled));
        }

        public void didEnterInvalidCredentials() {
            PostMessageActivity.this.twitterConnectionCheckBox.setEnabled(true);
            PostMessageActivity.this.twitterConnectionCheckBox.setChecked(false);
            PostMessageActivity.this.twitterReceiverCheckBox.setEnabled(false);
            PostMessageActivity.this.twitterStatusText.setText(PostMessageActivity.this.getString(R.string.twitter_invalid_credentials));
        }
    }

    private class MyspaceObserver implements SocialProviderControllerObserver {
        private MyspaceObserver() {
        }

        /* synthetic */ MyspaceObserver(PostMessageActivity postMessageActivity, MyspaceObserver myspaceObserver) {
            this();
        }

        public void didFail(Throwable exception) {
            PostMessageActivity.this.myspaceConnectionCheckBox.setEnabled(true);
            PostMessageActivity.this.myspaceConnectionCheckBox.setChecked(false);
            PostMessageActivity.this.myspaceReceiverCheckBox.setEnabled(false);
            PostMessageActivity.this.myspaceStatusText.setText(String.valueOf(PostMessageActivity.this.getString(R.string.myspace_comm_failed)) + exception.getMessage());
            exception.printStackTrace();
        }

        public void didSucceed() {
            PostMessageActivity.this.myspaceReceiverCheckBox.setEnabled(true);
            PostMessageActivity.this.myspaceStatusText.setText(PostMessageActivity.this.getString(R.string.myspace_comm_ok));
        }

        public void userDidCancel() {
            PostMessageActivity.this.myspaceConnectionCheckBox.setEnabled(true);
            PostMessageActivity.this.myspaceConnectionCheckBox.setChecked(false);
            PostMessageActivity.this.myspaceReceiverCheckBox.setEnabled(false);
            PostMessageActivity.this.myspaceStatusText.setText(PostMessageActivity.this.getString(R.string.myspace_comm_cancelled));
        }

        public void didEnterInvalidCredentials() {
            PostMessageActivity.this.myspaceConnectionCheckBox.setEnabled(true);
            PostMessageActivity.this.myspaceConnectionCheckBox.setChecked(false);
            PostMessageActivity.this.myspaceReceiverCheckBox.setEnabled(false);
            PostMessageActivity.this.myspaceStatusText.setText(PostMessageActivity.this.getString(R.string.myspace_invalid_credentials));
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.post_message);
        this.facebookProvider = SocialProvider.getSocialProviderForIdentifier(FacebookSocialProvider.IDENTIFIER);
        this.myspaceProvider = SocialProvider.getSocialProviderForIdentifier(MySpaceSocialProvider.IDENTIFIER);
        this.twitterProvider = SocialProvider.getSocialProviderForIdentifier(TwitterSocialProvider.IDENTIFIER);
        this.facebookController = SocialProviderController.getSocialProviderController(Session.getCurrentSession(), new FacebookObserver(this, null), this.facebookProvider);
        this.myspaceController = SocialProviderController.getSocialProviderController(Session.getCurrentSession(), new MyspaceObserver(this, null), this.myspaceProvider);
        this.twitterController = SocialProviderController.getSocialProviderController(Session.getCurrentSession(), new TwitterObserver(this, null), this.twitterProvider);
        this.messageController = new MessageController(new MessageControllerObserver(this, null));
        this.myspaceStatusText = (TextView) findViewById(R.id.myspace_status);
        this.facebookStatusText = (TextView) findViewById(R.id.facebook_status);
        this.twitterStatusText = (TextView) findViewById(R.id.twitter_status);
        this.postingStatusText = (TextView) findViewById(R.id.post_status);
        this.postButton = (Button) findViewById(R.id.post_message_button);
        this.postButton.setEnabled(false);
        this.postButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                PostMessageActivity.this.messageController.setTarget(Session.getCurrentSession().getGame());
                PostMessageActivity.this.messageController.setText(((EditText) PostMessageActivity.this.findViewById(R.id.message_text)).getText().toString());
                PostMessageActivity.this.messageController.postMessage();
                PostMessageActivity.this.postingStatusText.setText("");
                PostMessageActivity.this.showProgressIndicator();
            }
        });
        this.myspaceConnectionCheckBox = (CheckBox) findViewById(R.id.myspace_connection_checkbox);
        this.myspaceConnectionCheckBox.setChecked(Session.getCurrentSession().getUser().isConnectedToSocialProviderWithIdentifier(MySpaceSocialProvider.IDENTIFIER));
        this.myspaceConnectionCheckBox.setEnabled(!Session.getCurrentSession().getUser().isConnectedToSocialProviderWithIdentifier(MySpaceSocialProvider.IDENTIFIER));
        this.myspaceConnectionCheckBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (PostMessageActivity.this.myspaceConnectionCheckBox.isChecked()) {
                    PostMessageActivity.this.myspaceConnectionCheckBox.setEnabled(false);
                    PostMessageActivity.this.myspaceController.connect(PostMessageActivity.this);
                }
            }
        });
        this.myspaceReceiverCheckBox = (CheckBox) findViewById(R.id.myspace_receiver_checkbox);
        this.myspaceReceiverCheckBox.setEnabled(this.myspaceConnectionCheckBox.isChecked());
        this.myspaceReceiverCheckBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (PostMessageActivity.this.myspaceReceiverCheckBox.isChecked()) {
                    PostMessageActivity.this.messageController.addReceiverWithUsers(PostMessageActivity.this.myspaceProvider, null);
                } else {
                    PostMessageActivity.this.messageController.removeAllReceiversOfType(MySpaceSocialProvider.class);
                }
                PostMessageActivity.this.postButton.setEnabled(PostMessageActivity.this.myspaceReceiverCheckBox.isChecked() || PostMessageActivity.this.facebookReceiverCheckBox.isChecked());
            }
        });
        this.facebookConnectionCheckBox = (CheckBox) findViewById(R.id.facebook_connection_checkbox);
        this.facebookConnectionCheckBox.setChecked(Session.getCurrentSession().getUser().isConnectedToSocialProviderWithIdentifier(FacebookSocialProvider.IDENTIFIER));
        this.facebookConnectionCheckBox.setEnabled(!Session.getCurrentSession().getUser().isConnectedToSocialProviderWithIdentifier(FacebookSocialProvider.IDENTIFIER));
        this.facebookConnectionCheckBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (PostMessageActivity.this.facebookConnectionCheckBox.isChecked()) {
                    PostMessageActivity.this.facebookConnectionCheckBox.setEnabled(false);
                    PostMessageActivity.this.facebookController.connect(PostMessageActivity.this);
                }
            }
        });
        this.facebookReceiverCheckBox = (CheckBox) findViewById(R.id.facebook_receiver_checkbox);
        this.facebookReceiverCheckBox.setEnabled(this.facebookConnectionCheckBox.isChecked());
        this.facebookReceiverCheckBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (PostMessageActivity.this.facebookReceiverCheckBox.isChecked()) {
                    PostMessageActivity.this.messageController.addReceiverWithUsers(PostMessageActivity.this.facebookProvider, null);
                } else {
                    PostMessageActivity.this.messageController.removeAllReceiversOfType(FacebookSocialProvider.class);
                }
                PostMessageActivity.this.postButton.setEnabled(PostMessageActivity.this.myspaceReceiverCheckBox.isChecked() || PostMessageActivity.this.facebookReceiverCheckBox.isChecked());
            }
        });
        this.twitterConnectionCheckBox = (CheckBox) findViewById(R.id.twitter_connection_checkbox);
        this.twitterConnectionCheckBox.setChecked(Session.getCurrentSession().getUser().isConnectedToSocialProviderWithIdentifier(TwitterSocialProvider.IDENTIFIER));
        this.twitterConnectionCheckBox.setEnabled(!Session.getCurrentSession().getUser().isConnectedToSocialProviderWithIdentifier(TwitterSocialProvider.IDENTIFIER));
        this.twitterConnectionCheckBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (PostMessageActivity.this.twitterConnectionCheckBox.isChecked()) {
                    PostMessageActivity.this.twitterConnectionCheckBox.setEnabled(false);
                    PostMessageActivity.this.twitterController.connect(PostMessageActivity.this);
                }
            }
        });
        this.twitterReceiverCheckBox = (CheckBox) findViewById(R.id.twitter_receiver_checkbox);
        this.twitterReceiverCheckBox.setEnabled(this.twitterConnectionCheckBox.isChecked());
        this.twitterReceiverCheckBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (PostMessageActivity.this.twitterReceiverCheckBox.isChecked()) {
                    PostMessageActivity.this.messageController.addReceiverWithUsers(PostMessageActivity.this.twitterProvider, null);
                } else {
                    PostMessageActivity.this.messageController.removeAllReceiversOfType(TwitterSocialProvider.class);
                }
                PostMessageActivity.this.postButton.setEnabled(PostMessageActivity.this.myspaceReceiverCheckBox.isChecked() || PostMessageActivity.this.twitterReceiverCheckBox.isChecked());
            }
        });
    }
}
