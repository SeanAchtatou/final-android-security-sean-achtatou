package com.ballgame.android.sldemocore;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.ballgame.android.sldemocore.ChallengesActivity;
import com.scoreloop.client.android.core.model.Challenge;
import com.scoreloop.client.android.core.model.Session;

public class DirectChallengeActivity extends NewChallengeActivity {
    private Button chooseOpponentButton;

    /* access modifiers changed from: protected */
    public void initButtons() {
        this.playChallengeButton = (Button) findViewById(R.id.new_challenge_play_button);
        this.playChallengeButton.setEnabled(false);
        this.playChallengeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Challenge challenge = new Challenge(DirectChallengeActivity.this.getSelectedStake());
                challenge.setContender(Session.getCurrentSession().getUser());
                challenge.setContestant(DirectChallengeActivity.this.opponent);
                ScoreloopManager.setCurrentChallenge(challenge);
                DirectChallengeActivity.this.startActivity(new Intent(DirectChallengeActivity.this, GamePlayActivity.class));
                DirectChallengeActivity.this.finish();
            }
        });
        this.chooseOpponentButton = (Button) findViewById(R.id.choose_opponent_button);
        this.chooseOpponentButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(DirectChallengeActivity.this, HighscoresActivity.class);
                intent.putExtra("HIGH_SCORES_SELECTION_MODE", ChallengesActivity.NewChallengeModes.DIRECT.ordinal());
                intent.setFlags(1073741824);
                DirectChallengeActivity.this.startActivity(intent);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void initChallengeInfo() {
        TextView challengeInfo = (TextView) findViewById(R.id.challenge_info);
        if (this.opponent != null) {
            challengeInfo.setText(String.format(getString(R.string.new_direct_challenge), this.opponent.getLogin()));
        }
    }

    /* access modifiers changed from: protected */
    public boolean isPlayChallengeButtonEnabled(boolean suggestion) {
        return super.isPlayChallengeButtonEnabled(suggestion) && this.opponent != null;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.direct_challenge);
    }
}
