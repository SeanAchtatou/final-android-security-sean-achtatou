package com.scoreloop.client.android.core.ui;

import com.scoreloop.client.android.core.controller.SocialProviderControllerObserver;

class f implements SocialProviderControllerObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AuthViewController f121a;

    f(AuthViewController authViewController) {
        this.f121a = authViewController;
    }

    public void didEnterInvalidCredentials() {
        throw new IllegalStateException("");
    }

    public void didFail(Throwable th) {
        throw new IllegalStateException(th);
    }

    public void didSucceed() {
    }

    public void userDidCancel() {
        throw new IllegalStateException();
    }
}
