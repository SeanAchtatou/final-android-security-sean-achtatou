package com.scoreloop.client.android.core.controller;

import java.util.Collection;
import org.json.JSONArray;

/* renamed from: com.scoreloop.client.android.core.controller.p  reason: case insensitive filesystem */
class C0017p {

    /* renamed from: a  reason: collision with root package name */
    private final String f62a;
    private final C0003b b;
    private final Object c;

    public C0017p(String str, C0003b bVar, String str2) {
        this.f62a = str;
        this.b = bVar;
        this.c = str2;
    }

    public C0017p(String str, C0003b bVar, Collection collection) {
        this.f62a = str;
        this.b = bVar;
        this.c = new JSONArray(collection);
    }

    public String a() {
        String a2 = this.b.a();
        if (a2 == null) {
            return this.f62a;
        }
        return String.format("%s_%s", this.f62a, a2);
    }

    public Object b() {
        return this.c;
    }
}
