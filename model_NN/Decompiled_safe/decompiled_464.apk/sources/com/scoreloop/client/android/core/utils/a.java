package com.scoreloop.client.android.core.utils;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

class a extends FilterOutputStream {

    /* renamed from: a  reason: collision with root package name */
    private final byte[] f134a;
    private final boolean b;
    private byte[] c;
    private final int d;
    private final byte[] e;
    private final boolean f;
    private int g;
    private final int h;
    private int i;
    private boolean j;

    public a(OutputStream outputStream) {
        this(outputStream, 1);
    }

    public a(OutputStream outputStream, int i2) {
        super(outputStream);
        this.b = (i2 & 8) != 0;
        this.f = (i2 & 1) != 0;
        this.d = this.f ? 3 : 4;
        this.c = new byte[this.d];
        this.i = 0;
        this.g = 0;
        this.j = false;
        this.f134a = new byte[4];
        this.h = i2;
        this.e = Base64.c(i2);
    }

    public void a() {
        if (this.i <= 0) {
            return;
        }
        if (this.f) {
            this.out.write(Base64.b(this.f134a, this.c, this.i, this.h));
            this.i = 0;
            return;
        }
        throw new IOException("Base64 input not properly padded.");
    }

    public void close() {
        a();
        super.close();
        this.c = null;
        this.out = null;
    }

    public void write(int i2) {
        if (this.j) {
            this.out.write(i2);
        } else if (this.f) {
            byte[] bArr = this.c;
            int i3 = this.i;
            this.i = i3 + 1;
            bArr[i3] = (byte) i2;
            if (this.i >= this.d) {
                this.out.write(Base64.b(this.f134a, this.c, this.d, this.h));
                this.g += 4;
                if (this.b && this.g >= 76) {
                    this.out.write(10);
                    this.g = 0;
                }
                this.i = 0;
            }
        } else if (this.e[i2 & 127] > -5) {
            byte[] bArr2 = this.c;
            int i4 = this.i;
            this.i = i4 + 1;
            bArr2[i4] = (byte) i2;
            if (this.i >= this.d) {
                this.out.write(this.f134a, 0, Base64.b(this.c, 0, this.f134a, 0, this.h));
                this.i = 0;
            }
        } else if (this.e[i2 & 127] != -5) {
            throw new IOException("Invalid character in Base64 data.");
        }
    }

    public void write(byte[] bArr, int i2, int i3) {
        if (this.j) {
            this.out.write(bArr, i2, i3);
            return;
        }
        for (int i4 = 0; i4 < i3; i4++) {
            write(bArr[i2 + i4]);
        }
    }
}
