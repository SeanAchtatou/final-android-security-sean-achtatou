package com.scoreloop.client.android.core.ui;

import android.app.Activity;
import com.scoreloop.client.android.core.controller.SocialProviderControllerObserver;

public abstract class AuthViewController {

    /* renamed from: a  reason: collision with root package name */
    private SocialProviderControllerObserver f105a;

    public AuthViewController(SocialProviderControllerObserver socialProviderControllerObserver) {
        this.f105a = socialProviderControllerObserver;
        if (this.f105a == null) {
            this.f105a = new f(this);
        }
    }

    public SocialProviderControllerObserver a() {
        return this.f105a;
    }

    public abstract void a(Activity activity);
}
