package com.scoreloop.client.android.core.controller;

public interface ChallengeControllerObserver extends RequestControllerObserver {
    void onCannotAcceptChallenge(ChallengeController challengeController);

    void onCannotRejectChallenge(ChallengeController challengeController);

    void onInsufficientBalance(ChallengeController challengeController);
}
