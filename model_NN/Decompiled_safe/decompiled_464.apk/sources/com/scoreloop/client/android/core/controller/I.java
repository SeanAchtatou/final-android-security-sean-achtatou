package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import org.json.JSONObject;

class I extends Request {

    /* renamed from: a  reason: collision with root package name */
    protected Game f36a;
    protected User b;

    public I(RequestCompletionCallback requestCompletionCallback, Game game, User user) {
        super(requestCompletionCallback);
        this.b = user;
        this.f36a = game;
    }

    public JSONObject a() {
        return null;
    }

    public RequestMethod b() {
        return RequestMethod.GET;
    }

    public String c() {
        if (this.f36a == null || this.f36a.getIdentifier() == null) {
            return String.format("/service/users/%s", this.b.getIdentifier());
        }
        return String.format("/service/games/%s/users/%s", this.f36a.getIdentifier(), this.b.getIdentifier());
    }
}
