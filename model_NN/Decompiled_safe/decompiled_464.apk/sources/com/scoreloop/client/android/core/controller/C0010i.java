package com.scoreloop.client.android.core.controller;

/* renamed from: com.scoreloop.client.android.core.controller.i  reason: case insensitive filesystem */
class C0010i {

    /* renamed from: a  reason: collision with root package name */
    private final String f57a;
    private final S b;

    public C0010i(String str, S s) {
        this.f57a = str;
        this.b = s;
    }

    public String a() {
        return this.f57a;
    }

    public String b() {
        return this.b.toString();
    }
}
