package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.scoreloop.client.android.core.controller.q  reason: case insensitive filesystem */
class C0018q extends K {
    public C0018q(RequestCompletionCallback requestCompletionCallback, User user, User user2) {
        super(requestCompletionCallback, null, user, user2);
    }

    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        JSONObject jSONObject2 = new JSONObject();
        try {
            jSONObject2.put("buddy_id", this.b.getIdentifier());
            jSONObject.put("buddyhood", jSONObject2);
            return jSONObject;
        } catch (JSONException e) {
            throw new IllegalStateException();
        }
    }

    public RequestMethod b() {
        return RequestMethod.POST;
    }

    public String c() {
        return String.format("/service/users/%s/buddies", this.c.getIdentifier());
    }
}
