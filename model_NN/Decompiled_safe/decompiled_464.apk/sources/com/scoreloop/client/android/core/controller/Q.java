package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.SearchList;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import org.json.JSONException;
import org.json.JSONObject;

class Q extends Request {

    /* renamed from: a  reason: collision with root package name */
    private final Game f42a;
    private final Integer b;
    private final Score c;
    private final SearchList e;
    private final User f;

    public Q(RequestCompletionCallback requestCompletionCallback, Game game, SearchList searchList, User user, Score score, Integer num) {
        super(requestCompletionCallback);
        this.f42a = game;
        this.e = searchList;
        this.f = user;
        this.c = score;
        this.b = num;
    }

    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            if (this.e != null) {
                jSONObject.putOpt("search_list_id", this.e.getIdentifier());
            }
            if (this.c != null) {
                jSONObject.put("score", this.c.a());
            } else {
                jSONObject.put("user_id", this.f.getIdentifier());
                jSONObject.put("mode", this.b);
            }
            return jSONObject;
        } catch (JSONException e2) {
            throw new IllegalStateException("Invalid challenge data", e2);
        }
    }

    public RequestMethod b() {
        return RequestMethod.GET;
    }

    public String c() {
        return String.format("/service/games/%s/scores/rankings", this.f42a.getIdentifier());
    }
}
