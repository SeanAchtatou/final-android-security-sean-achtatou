package com.scoreloop.client.android.core.model;

import com.google.ads.AdActivity;
import com.scoreloop.client.android.core.utils.Formats;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class User {

    /* renamed from: a  reason: collision with root package name */
    private static Map f89a = new HashMap();
    private List A;
    private List B;
    private Map C;
    private a b;
    private boolean c;
    private String d;
    private String e;
    private String f;
    private Integer g;
    private String h;
    private List i;
    private Map j;
    private String k;
    private Details l;
    private Date m;
    private String n;
    private String o;
    private Gender p;
    private String q;
    private a r;
    private Date s;
    private Score t;
    private String u;
    private String v;
    private Double w;
    private Integer x;
    private b y;
    private a z;

    public class Details {
        private Double b;
        private Integer c;
        private Integer d;

        public Details() {
        }

        /* access modifiers changed from: package-private */
        public void a(JSONObject jSONObject) {
            if (jSONObject.has("winning_probability")) {
                this.b = Double.valueOf(jSONObject.optDouble("winning_probability"));
            }
            if (jSONObject.has("challenges_lost")) {
                this.d = Integer.valueOf(jSONObject.optInt("challenges_lost"));
            }
            if (jSONObject.has("challenges_won")) {
                this.c = Integer.valueOf(jSONObject.optInt("challenges_won"));
            }
        }

        public Integer getChallengesLost() {
            return this.d;
        }

        public Integer getChallengesWon() {
            return this.c;
        }

        public Double getWinningProbability() {
            return this.b;
        }
    }

    public enum Gender {
        FEMALE("f"),
        MALE(AdActivity.TYPE_PARAM),
        UNKNOWN("?");
        
        private String d;

        private Gender(String str) {
            this.d = str;
        }

        public String a() {
            return this.d;
        }
    }

    static {
        f89a.put("anonymous", b.anonymous);
        f89a.put("active", b.active);
        f89a.put("deleted", b.deleted);
        f89a.put("passive", b.passive);
        f89a.put("pending", b.pending);
        f89a.put("suspended", b.suspended);
    }

    public User() {
        this.p = Gender.UNKNOWN;
        this.C = new HashMap();
        this.l = new Details();
    }

    public User(JSONObject jSONObject) {
        this();
        a(jSONObject);
    }

    private Integer j() {
        return this.g;
    }

    private String k() {
        return this.h;
    }

    private b l() {
        return this.y;
    }

    private boolean m() {
        return Session.getCurrentSession().getUser().equals(this);
    }

    public Money a() {
        return (j() == null || k() == null) ? new Money("SLD", new BigDecimal(0)) : new Money(k(), new BigDecimal(j().intValue()));
    }

    public void a(String str) {
        this.n = str;
    }

    public void a(JSONObject jSONObject) {
        if (jSONObject.has("id")) {
            this.q = jSONObject.getString("id");
        }
        if (jSONObject.has("login")) {
            this.u = jSONObject.optString("login");
        }
        if (jSONObject.has("email")) {
            this.o = jSONObject.optString("email");
        }
        if (jSONObject.has("state")) {
            String lowerCase = jSONObject.optString("state").toLowerCase();
            if (!f89a.containsKey(lowerCase)) {
                throw new IllegalStateException("could not parse json representation of User due to unknown state given: '" + lowerCase + "'");
            }
            this.y = (b) f89a.get(lowerCase);
        }
        if (jSONObject.has("device_id")) {
            this.n = jSONObject.optString("device_id");
        }
        if (jSONObject.has("gender")) {
            String string = jSONObject.getString("gender");
            if (AdActivity.TYPE_PARAM.equalsIgnoreCase(string)) {
                this.p = Gender.MALE;
            } else if ("f".equalsIgnoreCase(string)) {
                this.p = Gender.FEMALE;
            } else {
                this.p = Gender.UNKNOWN;
            }
        }
        if (jSONObject.has("date_of_birth")) {
            if (jSONObject.isNull("date_of_birth")) {
                this.m = null;
            } else {
                try {
                    this.m = Formats.b.parse(jSONObject.getString("date_of_birth"));
                } catch (ParseException e2) {
                    throw new JSONException("Invalid format of the birth date");
                }
            }
        }
        JSONObject optJSONObject = jSONObject.optJSONObject("balance");
        if (optJSONObject != null) {
            this.g = Integer.valueOf(optJSONObject.getInt("amount"));
            this.h = optJSONObject.getString("currency");
        }
        JSONObject optJSONObject2 = jSONObject.optJSONObject("avatar");
        if (optJSONObject2 != null) {
            this.f = optJSONObject2.getString("head");
            this.e = optJSONObject2.getString("hair");
            this.d = optJSONObject2.getString("body");
        }
        SocialProvider.b(this, jSONObject);
        JSONObject optJSONObject3 = jSONObject.optJSONObject("skill");
        if (optJSONObject3 != null) {
            this.x = Integer.valueOf(optJSONObject3.getInt("value"));
            this.w = Double.valueOf(optJSONObject3.getDouble("deviation"));
        }
        JSONObject optJSONObject4 = jSONObject.optJSONObject("agility");
        if (optJSONObject4 != null) {
            this.b = new a(optJSONObject4);
        }
        JSONObject optJSONObject5 = jSONObject.optJSONObject("strategy");
        if (optJSONObject5 != null) {
            this.z = new a(optJSONObject5);
        }
        JSONObject optJSONObject6 = jSONObject.optJSONObject("knowledge");
        if (optJSONObject6 != null) {
            this.r = new a(optJSONObject6);
        }
        JSONArray optJSONArray = jSONObject.optJSONArray("score_lists");
        if (optJSONArray != null) {
            ArrayList arrayList = new ArrayList();
            for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                arrayList.add(new SearchList(optJSONArray.getJSONObject(i2)));
            }
            this.A = arrayList;
        }
        JSONArray optJSONArray2 = jSONObject.optJSONArray("challenge_lists");
        if (optJSONArray2 != null) {
            ArrayList arrayList2 = new ArrayList();
            for (int i3 = 0; i3 < optJSONArray2.length(); i3++) {
                arrayList2.add(new SearchList(optJSONArray2.getJSONObject(i3)));
            }
            this.B = arrayList2;
        }
        if (jSONObject.has("last_active_at")) {
            try {
                this.s = Formats.f131a.parse(jSONObject.getString("last_active_at"));
            } catch (ParseException e3) {
                throw new JSONException("Invalid format of the 'last active at' date");
            }
        }
        JSONArray optJSONArray3 = jSONObject.optJSONArray("buddies");
        if (optJSONArray3 != null) {
            ArrayList arrayList3 = new ArrayList();
            for (int i4 = 0; i4 < optJSONArray3.length(); i4++) {
                arrayList3.add(new User(optJSONArray3.getJSONObject(i4)));
            }
            this.i = arrayList3;
        }
        this.l.a(jSONObject);
    }

    public void a(JSONObject jSONObject, String str) {
        this.C.put(str, jSONObject);
    }

    public void a(boolean z2) {
        this.c = z2;
    }

    public String b() {
        return this.n;
    }

    public boolean b(String str) {
        return this.C.containsKey(str);
    }

    /* access modifiers changed from: package-private */
    public List c() {
        return this.A;
    }

    public JSONObject c(String str) {
        return (JSONObject) this.C.get(str);
    }

    public JSONObject d() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("id", this.q);
        jSONObject.put("login", this.u);
        jSONObject.put("device_id", this.n);
        jSONObject.put("password", this.v);
        jSONObject.put("password_confirmation", this.v);
        jSONObject.put("email", this.o);
        if (this.p != Gender.UNKNOWN) {
            jSONObject.put("gender", this.p.a());
        }
        if (this.m != null) {
            jSONObject.put("date_of_birth", Formats.b.format(this.m));
        }
        if (!(this.d == null && this.e == null && this.d == null)) {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject.put("avatar", jSONObject2);
            jSONObject2.put("hair", this.e);
            jSONObject2.put("head", this.f);
            jSONObject2.put("body", this.d);
        }
        SocialProvider.a(this, jSONObject);
        return jSONObject;
    }

    public void d(String str) {
        this.q = str;
    }

    public JSONObject e() {
        return (JSONObject) this.C.get("facebook");
    }

    public void e(String str) {
        this.k = str;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof User)) {
            return super.equals(obj);
        }
        User user = (User) obj;
        if (getIdentifier() != null && user.getIdentifier() != null) {
            return getIdentifier().equalsIgnoreCase(user.getIdentifier());
        }
        String login = getLogin();
        String login2 = user.getLogin();
        return (login == null || login2 == null || !login.equalsIgnoreCase(login2)) ? false : true;
    }

    public boolean f() {
        return Session.getCurrentSession().getUser().equals(this);
    }

    public String g() {
        return this.k;
    }

    public List getBuddyUsers() {
        return this.i;
    }

    public Map getContext() {
        return this.j;
    }

    public Date getDateOfBirth() {
        return this.m;
    }

    public Details getDetail() {
        return this.l;
    }

    public String getDisplayName() {
        if (this.u != null && !this.u.equals("")) {
            return this.u;
        }
        if (e() == null) {
            return (this.o == null || this.o.equals("")) ? "somebody" : this.o;
        }
        try {
            return String.format("%s %s", e().getString("first_name"), e().getString("last_name"));
        } catch (JSONException e2) {
            return "somebody";
        }
    }

    public String getEmailAddress() {
        if (!m()) {
            return null;
        }
        return this.o;
    }

    public String getIdentifier() {
        return this.q;
    }

    public Date getLastActiveAt() {
        return this.s;
    }

    public Score getLastScore() {
        return this.t;
    }

    public String getLogin() {
        return this.u;
    }

    public Integer getSkillValue() {
        return this.x;
    }

    public JSONObject h() {
        return (JSONObject) this.C.get("twitter");
    }

    public int hashCode() {
        return getLogin() == null ? "".hashCode() : getLogin().hashCode();
    }

    public JSONObject i() {
        return (JSONObject) this.C.get("myspace");
    }

    public boolean isActive() {
        return b.active.equals(l());
    }

    public boolean isAnonymous() {
        return b.anonymous.equals(l());
    }

    public boolean isAuthenticated() {
        return this.c;
    }

    public boolean isChallengable() {
        return isAnonymous() || isPassive() || isPending() || isActive();
    }

    public boolean isConnectedToSocialProviderWithIdentifier(String str) {
        SocialProvider socialProviderForIdentifier = SocialProvider.getSocialProviderForIdentifier(str);
        if (socialProviderForIdentifier != null) {
            return socialProviderForIdentifier.isUserConnected(this);
        }
        throw new IllegalArgumentException("could not find provider for id: '" + str + "'");
    }

    public boolean isPassive() {
        return b.passive.equals(l());
    }

    public boolean isPending() {
        return b.pending.equals(l());
    }

    public void setContext(Map map) {
        this.j = map;
    }

    public void setDateOfBirth(Date date) {
        this.m = date;
    }

    public void setEmailAddress(String str) {
        if (m()) {
            this.o = str;
        }
    }

    public void setLogin(String str) {
        this.u = str;
    }

    public void setPassword(String str) {
        this.v = str;
    }

    public String toString() {
        return getLogin() == null ? getIdentifier() == null ? "[empty user]" : getIdentifier() : getLogin();
    }
}
