package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.SearchList;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.scoreloop.client.android.core.controller.d  reason: case insensitive filesystem */
class C0005d extends Request {

    /* renamed from: a  reason: collision with root package name */
    private final Game f53a;
    private final Integer b;
    private final Integer c;
    private final Integer e;
    private final SearchList f;
    private final User g;

    public C0005d(RequestCompletionCallback requestCompletionCallback, Game game, SearchList searchList, User user, Integer num, Integer num2, Integer num3) {
        super(requestCompletionCallback);
        this.f53a = game;
        this.f = searchList;
        this.g = user;
        this.b = num;
        this.e = num2;
        this.c = num3;
    }

    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            if (this.f != null) {
                jSONObject.putOpt("search_list_id", this.f.getIdentifier());
            }
            jSONObject.put("user_id", this.g.getIdentifier());
            jSONObject.put("offset", this.c);
            jSONObject.put("per_page", this.e);
            jSONObject.put("mode", this.b);
            return jSONObject;
        } catch (JSONException e2) {
            throw new IllegalStateException("Invalid challenge data", e2);
        }
    }

    public RequestMethod b() {
        return RequestMethod.GET;
    }

    public String c() {
        return String.format("/service/games/%s/scores", this.f53a.getIdentifier());
    }
}
