package com.scoreloop.client.android.core.controller;

public interface RequestControllerObserver {
    void requestControllerDidFail(RequestController requestController, Exception exc);

    void requestControllerDidReceiveResponse(RequestController requestController);
}
