package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Device;
import com.scoreloop.client.android.core.model.Money;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.Response;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

class U extends RequestController {
    /* access modifiers changed from: private */
    public C0022u c;
    private final RequestControllerObserver d = new N(this);

    U(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
    }

    private List a(Money money) {
        String d2 = money.d();
        return money.compareTo(new Money(d2, new BigDecimal(10000))) < 0 ? a(d2) : money.compareTo(new Money(d2, new BigDecimal(100000))) < 0 ? c(d2) : b(d2);
    }

    private List a(String str) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new Money(str, new BigDecimal(100)));
        arrayList.add(new Money(str, new BigDecimal(200)));
        arrayList.add(new Money(str, new BigDecimal(500)));
        arrayList.add(new Money(str, new BigDecimal(1000)));
        arrayList.add(new Money(str, new BigDecimal(2000)));
        arrayList.add(new Money(str, new BigDecimal(5000)));
        return arrayList;
    }

    private List b(String str) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new Money(str, new BigDecimal(1000)));
        arrayList.add(new Money(str, new BigDecimal(2500)));
        arrayList.add(new Money(str, new BigDecimal(5000)));
        arrayList.add(new Money(str, new BigDecimal(10000)));
        arrayList.add(new Money(str, new BigDecimal(50000)));
        arrayList.add(new Money(str, new BigDecimal(100000)));
        return arrayList;
    }

    private List c(String str) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new Money(str, new BigDecimal(500)));
        arrayList.add(new Money(str, new BigDecimal(1000)));
        arrayList.add(new Money(str, new BigDecimal(2500)));
        arrayList.add(new Money(str, new BigDecimal(5000)));
        arrayList.add(new Money(str, new BigDecimal(10000)));
        arrayList.add(new Money(str, new BigDecimal(20000)));
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) {
        Session e = e();
        int f = response.f();
        JSONObject e2 = response.e();
        JSONObject optJSONObject = e2.optJSONObject("user");
        if ((f == 200 || f == 201) && optJSONObject != null) {
            User user = e.getUser();
            user.a(optJSONObject);
            user.a(true);
            String optString = e2.optString("characteristic");
            if (optString != null) {
                e.getGame().a(optString);
            }
            e.a(a(user.a()));
            e.a(e2);
            e.a(Session.State.AUTHENTICATED);
            return true;
        }
        e.a(Session.State.FAILED);
        throw new Exception("Session authentication request failed with status: " + f);
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public void i() {
        Session e = e();
        Device a2 = e.a();
        if (e.c() == Session.State.FAILED) {
            a2.a((String) null);
        }
        C0004c cVar = new C0004c(d(), b(), a2);
        if (a2.a() == null && this.c == null) {
            this.c = new C0022u(e(), this.d);
        }
        h();
        e.a(Session.State.AUTHENTICATING);
        if (a2.a() == null) {
            this.c.j();
        }
        a(cVar);
    }
}
