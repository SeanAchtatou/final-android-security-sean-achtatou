package com.novell.sasl.client;

import java.util.Iterator;
import org.apache.harmony.javax.security.sasl.SaslException;

class ResponseAuth {
    private String m_responseValue = null;

    ResponseAuth(byte[] responseAuth) throws SaslException {
        DirectiveList dirList = new DirectiveList(responseAuth);
        try {
            dirList.parseDirectives();
            checkSemantics(dirList);
        } catch (SaslException e) {
        }
    }

    /* access modifiers changed from: package-private */
    public void checkSemantics(DirectiveList dirList) throws SaslException {
        Iterator directives = dirList.getIterator();
        while (directives.hasNext()) {
            ParsedDirective directive = (ParsedDirective) directives.next();
            if (directive.getName().equals("rspauth")) {
                this.m_responseValue = directive.getValue();
            }
        }
        if (this.m_responseValue == null) {
            throw new SaslException("Missing response-auth directive.");
        }
    }

    public String getResponseValue() {
        return this.m_responseValue;
    }
}
