package com.novell.sasl.client;

class ParsedDirective {
    public static final int QUOTED_STRING_VALUE = 1;
    public static final int TOKEN_VALUE = 2;
    private String m_name;
    private String m_value;
    private int m_valueType;

    ParsedDirective(String name, String value, int type) {
        this.m_name = name;
        this.m_value = value;
        this.m_valueType = type;
    }

    /* access modifiers changed from: package-private */
    public String getValue() {
        return this.m_value;
    }

    /* access modifiers changed from: package-private */
    public String getName() {
        return this.m_name;
    }

    /* access modifiers changed from: package-private */
    public int getValueType() {
        return this.m_valueType;
    }
}
