package com.novell.sasl.client;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Map;
import org.apache.harmony.javax.security.auth.callback.Callback;
import org.apache.harmony.javax.security.auth.callback.CallbackHandler;
import org.apache.harmony.javax.security.auth.callback.NameCallback;
import org.apache.harmony.javax.security.auth.callback.PasswordCallback;
import org.apache.harmony.javax.security.auth.callback.UnsupportedCallbackException;
import org.apache.harmony.javax.security.sasl.RealmCallback;
import org.apache.harmony.javax.security.sasl.RealmChoiceCallback;
import org.apache.harmony.javax.security.sasl.SaslClient;
import org.apache.harmony.javax.security.sasl.SaslException;

public class DigestMD5SaslClient implements SaslClient {
    private static final String DIGEST_METHOD = "AUTHENTICATE";
    private static final int NONCE_BYTE_COUNT = 32;
    private static final int NONCE_HEX_COUNT = 64;
    private static final int STATE_DIGEST_RESPONSE_SENT = 1;
    private static final int STATE_DISPOSED = 4;
    private static final int STATE_INITIAL = 0;
    private static final int STATE_INVALID_SERVER_RESPONSE = 3;
    private static final int STATE_VALID_SERVER_RESPONSE = 2;
    private char[] m_HA1 = null;
    private String m_authorizationId = "";
    private CallbackHandler m_cbh;
    private String m_clientNonce = "";
    private DigestChallenge m_dc;
    private String m_digestURI;
    private String m_name = "";
    private Map m_props;
    private String m_protocol = "";
    private String m_qopValue = "";
    private String m_realm = "";
    private String m_serverName = "";
    private int m_state;

    public static SaslClient getClient(String authorizationId, String protocol, String serverName, Map props, CallbackHandler cbh) {
        String desiredQOP = (String) props.get("javax.security.sasl.qop");
        String str = (String) props.get("javax.security.sasl.strength");
        String serverAuth = (String) props.get("javax.security.sasl.server.authentication");
        if (desiredQOP != null && !"auth".equals(desiredQOP)) {
            return null;
        }
        if ((serverAuth == null || "false".equals(serverAuth)) && cbh != null) {
            return new DigestMD5SaslClient(authorizationId, protocol, serverName, props, cbh);
        }
        return null;
    }

    private DigestMD5SaslClient(String authorizationId, String protocol, String serverName, Map props, CallbackHandler cbh) {
        this.m_authorizationId = authorizationId;
        this.m_protocol = protocol;
        this.m_serverName = serverName;
        this.m_props = props;
        this.m_cbh = cbh;
        this.m_state = 0;
    }

    public boolean hasInitialResponse() {
        return false;
    }

    public boolean isComplete() {
        if (this.m_state == 2 || this.m_state == 3 || this.m_state == 4) {
            return true;
        }
        return false;
    }

    public byte[] unwrap(byte[] incoming, int offset, int len) throws SaslException {
        throw new IllegalStateException("unwrap: QOP has neither integrity nor privacy>");
    }

    public byte[] wrap(byte[] outgoing, int offset, int len) throws SaslException {
        throw new IllegalStateException("wrap: QOP has neither integrity nor privacy>");
    }

    public Object getNegotiatedProperty(String propName) {
        if (this.m_state != 2) {
            throw new IllegalStateException("getNegotiatedProperty: authentication exchange not complete.");
        } else if ("javax.security.sasl.qop".equals(propName)) {
            return "auth";
        } else {
            return null;
        }
    }

    public void dispose() throws SaslException {
        if (this.m_state != 4) {
            this.m_state = 4;
        }
    }

    public byte[] evaluateChallenge(byte[] challenge) throws SaslException {
        byte[] response = null;
        switch (this.m_state) {
            case 0:
                if (challenge.length == 0) {
                    throw new SaslException("response = byte[0]");
                }
                try {
                    byte[] response2 = createDigestResponse(challenge).getBytes("UTF-8");
                    this.m_state = 1;
                    return response2;
                } catch (UnsupportedEncodingException e) {
                    throw new SaslException("UTF-8 encoding not suppported by platform", e);
                }
            case 1:
                if (checkServerResponseAuth(challenge)) {
                    this.m_state = 2;
                    return response;
                }
                this.m_state = 3;
                throw new SaslException("Could not validate response-auth value from server");
            case 2:
            case 3:
                throw new SaslException("Authentication sequence is complete");
            case 4:
                throw new SaslException("Client has been disposed");
            default:
                throw new SaslException("Unknown client state.");
        }
    }

    /* access modifiers changed from: package-private */
    public char[] convertToHex(byte[] hash) {
        char[] hex = new char[32];
        for (int i = 0; i < 16; i++) {
            hex[i * 2] = getHexChar((byte) ((hash[i] & 240) >> 4));
            hex[(i * 2) + 1] = getHexChar((byte) (hash[i] & 15));
        }
        return hex;
    }

    /* access modifiers changed from: package-private */
    public char[] DigestCalcHA1(String algorithm, String userName, String realm, String password, String nonce, String clientNonce) throws SaslException {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(userName.getBytes("UTF-8"));
            md.update(":".getBytes("UTF-8"));
            md.update(realm.getBytes("UTF-8"));
            md.update(":".getBytes("UTF-8"));
            md.update(password.getBytes("UTF-8"));
            byte[] hash = md.digest();
            if ("md5-sess".equals(algorithm)) {
                md.update(hash);
                md.update(":".getBytes("UTF-8"));
                md.update(nonce.getBytes("UTF-8"));
                md.update(":".getBytes("UTF-8"));
                md.update(clientNonce.getBytes("UTF-8"));
                hash = md.digest();
            }
            return convertToHex(hash);
        } catch (NoSuchAlgorithmException e) {
            throw new SaslException("No provider found for MD5 hash", e);
        } catch (UnsupportedEncodingException e2) {
            throw new SaslException("UTF-8 encoding not supported by platform.", e2);
        }
    }

    /* access modifiers changed from: package-private */
    public char[] DigestCalcResponse(char[] HA1, String serverNonce, String nonceCount, String clientNonce, String qop, String method, String digestUri, boolean clientResponseFlag) throws SaslException {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            if (clientResponseFlag) {
                md.update(method.getBytes("UTF-8"));
            }
            md.update(":".getBytes("UTF-8"));
            md.update(digestUri.getBytes("UTF-8"));
            if ("auth-int".equals(qop)) {
                md.update(":".getBytes("UTF-8"));
                md.update("00000000000000000000000000000000".getBytes("UTF-8"));
            }
            char[] HA2Hex = convertToHex(md.digest());
            md.update(new String(HA1).getBytes("UTF-8"));
            md.update(":".getBytes("UTF-8"));
            md.update(serverNonce.getBytes("UTF-8"));
            md.update(":".getBytes("UTF-8"));
            if (qop.length() > 0) {
                md.update(nonceCount.getBytes("UTF-8"));
                md.update(":".getBytes("UTF-8"));
                md.update(clientNonce.getBytes("UTF-8"));
                md.update(":".getBytes("UTF-8"));
                md.update(qop.getBytes("UTF-8"));
                md.update(":".getBytes("UTF-8"));
            }
            md.update(new String(HA2Hex).getBytes("UTF-8"));
            return convertToHex(md.digest());
        } catch (NoSuchAlgorithmException e) {
            throw new SaslException("No provider found for MD5 hash", e);
        } catch (UnsupportedEncodingException e2) {
            throw new SaslException("UTF-8 encoding not supported by platform.", e2);
        }
    }

    private String createDigestResponse(byte[] challenge) throws SaslException {
        StringBuffer digestResponse = new StringBuffer(512);
        this.m_dc = new DigestChallenge(challenge);
        this.m_digestURI = String.valueOf(this.m_protocol) + "/" + this.m_serverName;
        if ((this.m_dc.getQop() & 1) == 1) {
            this.m_qopValue = "auth";
            Callback[] callbacks = new Callback[3];
            ArrayList realms = this.m_dc.getRealms();
            int realmSize = realms.size();
            if (realmSize == 0) {
                callbacks[0] = new RealmCallback("Realm");
            } else if (realmSize == 1) {
                callbacks[0] = new RealmCallback("Realm", (String) realms.get(0));
            } else {
                callbacks[0] = new RealmChoiceCallback("Realm", (String[]) realms.toArray(new String[realmSize]), 0, false);
            }
            callbacks[1] = new PasswordCallback("Password", false);
            if (this.m_authorizationId == null || this.m_authorizationId.length() == 0) {
                callbacks[2] = new NameCallback("Name");
            } else {
                callbacks[2] = new NameCallback("Name", this.m_authorizationId);
            }
            try {
                this.m_cbh.handle(callbacks);
                if (realmSize > 1) {
                    int[] selections = ((RealmChoiceCallback) callbacks[0]).getSelectedIndexes();
                    if (selections.length > 0) {
                        this.m_realm = ((RealmChoiceCallback) callbacks[0]).getChoices()[selections[0]];
                    } else {
                        this.m_realm = ((RealmChoiceCallback) callbacks[0]).getChoices()[0];
                    }
                } else {
                    this.m_realm = ((RealmCallback) callbacks[0]).getText();
                }
                this.m_clientNonce = getClientNonce();
                this.m_name = ((NameCallback) callbacks[2]).getName();
                if (this.m_name == null) {
                    this.m_name = ((NameCallback) callbacks[2]).getDefaultName();
                }
                if (this.m_name == null) {
                    throw new SaslException("No user name was specified.");
                }
                this.m_HA1 = DigestCalcHA1(this.m_dc.getAlgorithm(), this.m_name, this.m_realm, new String(((PasswordCallback) callbacks[1]).getPassword()), this.m_dc.getNonce(), this.m_clientNonce);
                char[] response = DigestCalcResponse(this.m_HA1, this.m_dc.getNonce(), "00000001", this.m_clientNonce, this.m_qopValue, DIGEST_METHOD, this.m_digestURI, true);
                digestResponse.append("username=\"");
                digestResponse.append(this.m_authorizationId);
                if (this.m_realm.length() != 0) {
                    digestResponse.append("\",realm=\"");
                    digestResponse.append(this.m_realm);
                }
                digestResponse.append("\",cnonce=\"");
                digestResponse.append(this.m_clientNonce);
                digestResponse.append("\",nc=");
                digestResponse.append("00000001");
                digestResponse.append(",qop=");
                digestResponse.append(this.m_qopValue);
                digestResponse.append(",digest-uri=\"");
                digestResponse.append(this.m_digestURI);
                digestResponse.append("\",response=");
                digestResponse.append(response);
                digestResponse.append(",charset=utf-8,nonce=\"");
                digestResponse.append(this.m_dc.getNonce());
                digestResponse.append("\"");
                return digestResponse.toString();
            } catch (UnsupportedCallbackException e) {
                throw new SaslException("Handler does not support necessary callbacks", e);
            } catch (IOException e2) {
                throw new SaslException("IO exception in CallbackHandler.", e2);
            }
        } else {
            throw new SaslException("Client only supports qop of 'auth'");
        }
    }

    /* access modifiers changed from: package-private */
    public boolean checkServerResponseAuth(byte[] serverResponse) throws SaslException {
        return new String(DigestCalcResponse(this.m_HA1, this.m_dc.getNonce(), "00000001", this.m_clientNonce, this.m_qopValue, DIGEST_METHOD, this.m_digestURI, false)).equals(new ResponseAuth(serverResponse).getResponseValue());
    }

    private static char getHexChar(byte value) {
        switch (value) {
            case 0:
                return '0';
            case 1:
                return '1';
            case 2:
                return '2';
            case 3:
                return '3';
            case 4:
                return '4';
            case 5:
                return '5';
            case 6:
                return '6';
            case 7:
                return '7';
            case 8:
                return '8';
            case 9:
                return '9';
            case 10:
                return 'a';
            case 11:
                return 'b';
            case 12:
                return 'c';
            case 13:
                return 'd';
            case 14:
                return 'e';
            case 15:
                return 'f';
            default:
                return 'Z';
        }
    }

    /* access modifiers changed from: package-private */
    public String getClientNonce() throws SaslException {
        byte[] nonceBytes = new byte[32];
        char[] hexNonce = new char[64];
        try {
            SecureRandom.getInstance("SHA1PRNG").nextBytes(nonceBytes);
            for (int i = 0; i < 32; i++) {
                hexNonce[i * 2] = getHexChar((byte) (nonceBytes[i] & 15));
                hexNonce[(i * 2) + 1] = getHexChar((byte) ((nonceBytes[i] & 240) >> 4));
            }
            return new String(hexNonce);
        } catch (NoSuchAlgorithmException e) {
            throw new SaslException("No random number generator available", e);
        }
    }

    public String getMechanismName() {
        return "DIGEST-MD5";
    }
}
