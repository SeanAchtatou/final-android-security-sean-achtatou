package com.kenai.jbosh;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

final class ZLIBCodec {
    private static final int BUFFER_SIZE = 512;

    private ZLIBCodec() {
    }

    public static String getID() {
        return "deflate";
    }

    public static byte[] encode(byte[] data) throws IOException {
        ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
        DeflaterOutputStream deflateOut = null;
        try {
            DeflaterOutputStream deflateOut2 = new DeflaterOutputStream(byteOut);
            try {
                deflateOut2.write(data);
                deflateOut2.close();
                byteOut.close();
                byte[] byteArray = byteOut.toByteArray();
                deflateOut2.close();
                byteOut.close();
                return byteArray;
            } catch (Throwable th) {
                th = th;
                deflateOut = deflateOut2;
                deflateOut.close();
                byteOut.close();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            deflateOut.close();
            byteOut.close();
            throw th;
        }
    }

    public static byte[] decode(byte[] compressed) throws IOException {
        int read;
        ByteArrayInputStream byteIn = new ByteArrayInputStream(compressed);
        ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
        InflaterInputStream inflaterIn = null;
        try {
            InflaterInputStream inflaterIn2 = new InflaterInputStream(byteIn);
            try {
                byte[] buffer = new byte[512];
                do {
                    read = inflaterIn2.read(buffer);
                    if (read > 0) {
                        byteOut.write(buffer, 0, read);
                        continue;
                    }
                } while (read >= 0);
                byte[] byteArray = byteOut.toByteArray();
                inflaterIn2.close();
                byteOut.close();
                return byteArray;
            } catch (Throwable th) {
                th = th;
                inflaterIn = inflaterIn2;
                inflaterIn.close();
                byteOut.close();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            inflaterIn.close();
            byteOut.close();
            throw th;
        }
    }
}
