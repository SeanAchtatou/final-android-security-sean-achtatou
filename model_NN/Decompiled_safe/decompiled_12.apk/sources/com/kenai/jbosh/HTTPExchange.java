package com.kenai.jbosh;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

final class HTTPExchange {
    private static final Logger LOG = Logger.getLogger(HTTPExchange.class.getName());
    private final Lock lock = new ReentrantLock();
    private final Condition ready = this.lock.newCondition();
    private final AbstractBody request;
    private HTTPResponse response;

    HTTPExchange(AbstractBody req) {
        if (req == null) {
            throw new IllegalArgumentException("Request body cannot be null");
        }
        this.request = req;
    }

    /* access modifiers changed from: package-private */
    public AbstractBody getRequest() {
        return this.request;
    }

    /* access modifiers changed from: package-private */
    public void setHTTPResponse(HTTPResponse resp) {
        this.lock.lock();
        try {
            if (this.response != null) {
                throw new IllegalStateException("HTTPResponse was already set");
            }
            this.response = resp;
            this.ready.signalAll();
        } finally {
            this.lock.unlock();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.InterruptedException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* access modifiers changed from: package-private */
    public HTTPResponse getHTTPResponse() {
        this.lock.lock();
        while (this.response == null) {
            try {
                this.ready.await();
            } catch (InterruptedException intx) {
                LOG.log(Level.FINEST, "Interrupted", (Throwable) intx);
            } catch (Throwable th) {
                this.lock.unlock();
                throw th;
            }
        }
        HTTPResponse hTTPResponse = this.response;
        this.lock.unlock();
        return hTTPResponse;
    }
}
