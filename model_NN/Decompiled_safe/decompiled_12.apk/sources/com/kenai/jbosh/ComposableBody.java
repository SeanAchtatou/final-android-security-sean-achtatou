package com.kenai.jbosh;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class ComposableBody extends AbstractBody {
    private static final Pattern BOSH_START = Pattern.compile("<body(?:[\t\n\r ][^>]*?)?(/>|>)", 64);
    private final Map<BodyQName, String> attrs;
    private final AtomicReference<String> computed;
    /* access modifiers changed from: private */
    public final String payload;

    public static final class Builder {
        private boolean doMapCopy;
        private Map<BodyQName, String> map;
        private String payloadXML;

        private Builder() {
        }

        /* synthetic */ Builder(Builder builder) {
            this();
        }

        /* access modifiers changed from: private */
        public static Builder fromBody(ComposableBody source) {
            Builder result = new Builder();
            result.map = source.getAttributes();
            result.doMapCopy = true;
            result.payloadXML = source.payload;
            return result;
        }

        public Builder setPayloadXML(String xml) {
            if (xml == null) {
                throw new IllegalArgumentException("payload XML argument cannot be null");
            }
            this.payloadXML = xml;
            return this;
        }

        public Builder setAttribute(BodyQName name, String value) {
            if (this.map == null) {
                this.map = new HashMap();
            } else if (this.doMapCopy) {
                this.map = new HashMap(this.map);
                this.doMapCopy = false;
            }
            if (value == null) {
                this.map.remove(name);
            } else {
                this.map.put(name, value);
            }
            return this;
        }

        public Builder setNamespaceDefinition(String prefix, String uri) {
            return setAttribute(BodyQName.createWithPrefix("http://www.w3.org/XML/1998/namespace", prefix, "xmlns"), uri);
        }

        public ComposableBody build() {
            if (this.map == null) {
                this.map = new HashMap();
            }
            if (this.payloadXML == null) {
                this.payloadXML = "";
            }
            return new ComposableBody(this.map, this.payloadXML, null);
        }
    }

    /* synthetic */ ComposableBody(Map map, String str, ComposableBody composableBody) {
        this(map, str);
    }

    private ComposableBody(Map<BodyQName, String> attrMap, String payloadXML) {
        this.computed = new AtomicReference<>();
        this.attrs = attrMap;
        this.payload = payloadXML;
    }

    static ComposableBody fromStaticBody(StaticBody body) throws BOSHException {
        String payload2;
        String raw = body.toXML();
        Matcher matcher = BOSH_START.matcher(raw);
        if (!matcher.find()) {
            throw new BOSHException("Could not locate 'body' element in XML.  The raw XML did not match the pattern: " + BOSH_START);
        }
        if (">".equals(matcher.group(1))) {
            int first = matcher.end();
            int last = raw.lastIndexOf("</");
            if (last < first) {
                last = first;
            }
            payload2 = raw.substring(first, last);
        } else {
            payload2 = "";
        }
        return new ComposableBody(body.getAttributes(), payload2);
    }

    public static Builder builder() {
        return new Builder(null);
    }

    public Builder rebuild() {
        return Builder.fromBody(this);
    }

    public Map<BodyQName, String> getAttributes() {
        return Collections.unmodifiableMap(this.attrs);
    }

    public String toXML() {
        String comp = this.computed.get();
        if (comp != null) {
            return comp;
        }
        String comp2 = computeXML();
        this.computed.set(comp2);
        return comp2;
    }

    public String getPayloadXML() {
        return this.payload;
    }

    private String escape(String value) {
        return value.replace("'", "&apos;");
    }

    private String computeXML() {
        BodyQName bodyName = getBodyQName();
        StringBuilder builder = new StringBuilder();
        builder.append("<");
        builder.append(bodyName.getLocalPart());
        for (Map.Entry<BodyQName, String> entry : this.attrs.entrySet()) {
            builder.append(" ");
            BodyQName name = (BodyQName) entry.getKey();
            String prefix = name.getPrefix();
            if (prefix != null && prefix.length() > 0) {
                builder.append(prefix);
                builder.append(":");
            }
            builder.append(name.getLocalPart());
            builder.append("='");
            builder.append(escape((String) entry.getValue()));
            builder.append("'");
        }
        builder.append(" ");
        builder.append("xmlns");
        builder.append("='");
        builder.append(bodyName.getNamespaceURI());
        builder.append("'>");
        if (this.payload != null) {
            builder.append(this.payload);
        }
        builder.append("</body>");
        return builder.toString();
    }
}
