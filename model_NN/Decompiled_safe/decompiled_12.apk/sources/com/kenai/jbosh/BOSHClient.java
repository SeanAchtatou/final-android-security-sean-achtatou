package com.kenai.jbosh;

import com.kenai.jbosh.ComposableBody;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class BOSHClient {
    static final /* synthetic */ boolean $assertionsDisabled = (!BOSHClient.class.desiredAssertionStatus());
    private static final boolean ASSERTIONS;
    private static final int DEFAULT_EMPTY_REQUEST_DELAY = 100;
    private static final int DEFAULT_PAUSE_MARGIN = 500;
    private static final int EMPTY_REQUEST_DELAY = Integer.getInteger(String.valueOf(BOSHClient.class.getName()) + ".emptyRequestDelay", 100).intValue();
    private static final String ERROR = "error";
    private static final String INTERRUPTED = "Interrupted";
    private static final Logger LOG = Logger.getLogger(BOSHClient.class.getName());
    private static final String NULL_LISTENER = "Listener may not be null";
    private static final int PAUSE_MARGIN = Integer.getInteger(String.valueOf(BOSHClient.class.getName()) + ".pauseMargin", (int) DEFAULT_PAUSE_MARGIN).intValue();
    private static final String TERMINATE = "terminate";
    private static final String UNHANDLED = "Unhandled Exception";
    private final BOSHClientConfig cfg;
    private CMSessionParams cmParams;
    private final Set<BOSHClientConnListener> connListeners = new CopyOnWriteArraySet();
    private final Condition drained = this.lock.newCondition();
    private ScheduledFuture emptyRequestFuture;
    private final Runnable emptyRequestRunnable = new Runnable() {
        public void run() {
            BOSHClient.this.sendEmptyRequest();
        }
    };
    private final AtomicReference<ExchangeInterceptor> exchInterceptor = new AtomicReference<>();
    private Queue<HTTPExchange> exchanges = new LinkedList();
    private final HTTPSender httpSender = new ApacheHTTPSender();
    private final ReentrantLock lock = new ReentrantLock();
    private final Condition notEmpty = this.lock.newCondition();
    private final Condition notFull = this.lock.newCondition();
    private List<ComposableBody> pendingRequestAcks = new ArrayList();
    private SortedSet<Long> pendingResponseAcks = new TreeSet();
    private final Runnable procRunnable = new Runnable() {
        public void run() {
            BOSHClient.this.processMessages();
        }
    };
    private Thread procThread;
    private final RequestIDSequence requestIDSeq = new RequestIDSequence();
    private final Set<BOSHClientRequestListener> requestListeners = new CopyOnWriteArraySet();
    private Long responseAck = -1L;
    private final Set<BOSHClientResponseListener> responseListeners = new CopyOnWriteArraySet();
    private final ScheduledExecutorService schedExec = Executors.newSingleThreadScheduledExecutor();

    static {
        String prop = String.valueOf(BOSHClient.class.getSimpleName()) + ".assertionsEnabled";
        boolean enabled = false;
        if (System.getProperty(prop) != null) {
            enabled = Boolean.getBoolean(prop);
        } else if (!$assertionsDisabled) {
            enabled = true;
            if (1 == 0) {
                throw new AssertionError();
            }
        }
        ASSERTIONS = enabled;
    }

    static abstract class ExchangeInterceptor {
        /* access modifiers changed from: package-private */
        public abstract HTTPExchange interceptExchange(HTTPExchange hTTPExchange);

        ExchangeInterceptor() {
        }
    }

    private BOSHClient(BOSHClientConfig sessCfg) {
        this.cfg = sessCfg;
        init();
    }

    public static BOSHClient create(BOSHClientConfig clientCfg) {
        if (clientCfg != null) {
            return new BOSHClient(clientCfg);
        }
        throw new IllegalArgumentException("Client configuration may not be null");
    }

    public BOSHClientConfig getBOSHClientConfig() {
        return this.cfg;
    }

    public void addBOSHClientConnListener(BOSHClientConnListener listener) {
        if (listener == null) {
            throw new IllegalArgumentException(NULL_LISTENER);
        }
        this.connListeners.add(listener);
    }

    public void removeBOSHClientConnListener(BOSHClientConnListener listener) {
        if (listener == null) {
            throw new IllegalArgumentException(NULL_LISTENER);
        }
        this.connListeners.remove(listener);
    }

    public void addBOSHClientRequestListener(BOSHClientRequestListener listener) {
        if (listener == null) {
            throw new IllegalArgumentException(NULL_LISTENER);
        }
        this.requestListeners.add(listener);
    }

    public void removeBOSHClientRequestListener(BOSHClientRequestListener listener) {
        if (listener == null) {
            throw new IllegalArgumentException(NULL_LISTENER);
        }
        this.requestListeners.remove(listener);
    }

    public void addBOSHClientResponseListener(BOSHClientResponseListener listener) {
        if (listener == null) {
            throw new IllegalArgumentException(NULL_LISTENER);
        }
        this.responseListeners.add(listener);
    }

    public void removeBOSHClientResponseListener(BOSHClientResponseListener listener) {
        if (listener == null) {
            throw new IllegalArgumentException(NULL_LISTENER);
        }
        this.responseListeners.remove(listener);
    }

    /* JADX INFO: finally extract failed */
    public void send(ComposableBody body) throws BOSHException {
        ComposableBody request;
        assertUnlocked();
        if (body == null) {
            throw new IllegalArgumentException("Message body may not be null");
        }
        this.lock.lock();
        blockUntilSendable(body);
        if (isWorking() || isTermination(body)) {
            try {
                long rid = this.requestIDSeq.getNextRID();
                ComposableBody composableBody = body;
                CMSessionParams params = this.cmParams;
                if (params != null || !this.exchanges.isEmpty()) {
                    request = applySessionData(rid, body);
                    if (this.cmParams.isAckingRequests()) {
                        this.pendingRequestAcks.add(request);
                    }
                } else {
                    request = applySessionCreationRequest(rid, body);
                }
                HTTPExchange exch = new HTTPExchange(request);
                this.exchanges.add(exch);
                this.notEmpty.signalAll();
                clearEmptyRequest();
                this.lock.unlock();
                AbstractBody finalReq = exch.getRequest();
                exch.setHTTPResponse(this.httpSender.send(params, finalReq));
                fireRequestSent(finalReq);
            } catch (Throwable th) {
                this.lock.unlock();
                throw th;
            }
        } else {
            throw new BOSHException("Cannot send message when session is closed");
        }
    }

    /* JADX INFO: finally extract failed */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, com.kenai.jbosh.BOSHException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public boolean pause() {
        assertUnlocked();
        this.lock.lock();
        try {
            if (this.cmParams != null) {
                AttrMaxPause maxPause = this.cmParams.getMaxPause();
                if (maxPause != null) {
                    this.lock.unlock();
                    try {
                        send(ComposableBody.builder().setAttribute(Attributes.PAUSE, maxPause.toString()).build());
                    } catch (BOSHException boshx) {
                        LOG.log(Level.FINEST, "Could not send pause", (Throwable) boshx);
                    }
                    return true;
                }
            }
            this.lock.unlock();
            return false;
        } catch (Throwable th) {
            this.lock.unlock();
            throw th;
        }
    }

    public void disconnect() throws BOSHException {
        disconnect(ComposableBody.builder().build());
    }

    public void disconnect(ComposableBody msg) throws BOSHException {
        if (msg == null) {
            throw new IllegalArgumentException("Message body may not be null");
        }
        ComposableBody.Builder builder = msg.rebuild();
        builder.setAttribute(Attributes.TYPE, TERMINATE);
        send(builder.build());
    }

    public void close() {
        dispose(new BOSHException("Session explicitly closed by caller"));
    }

    /* access modifiers changed from: package-private */
    public CMSessionParams getCMSessionParams() {
        this.lock.lock();
        try {
            return this.cmParams;
        } finally {
            this.lock.unlock();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.InterruptedException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* access modifiers changed from: package-private */
    public void drain() {
        this.lock.lock();
        try {
            LOG.finest("Waiting while draining...");
            while (isWorking() && (this.emptyRequestFuture == null || this.emptyRequestFuture.isDone())) {
                this.drained.await();
            }
            LOG.finest("Drained");
            this.lock.unlock();
        } catch (InterruptedException intx) {
            LOG.log(Level.FINEST, INTERRUPTED, (Throwable) intx);
        } catch (Throwable th) {
            this.lock.unlock();
            throw th;
        }
    }

    /* access modifiers changed from: package-private */
    public void setExchangeInterceptor(ExchangeInterceptor interceptor) {
        this.exchInterceptor.set(interceptor);
    }

    private void init() {
        assertUnlocked();
        this.lock.lock();
        try {
            this.httpSender.init(this.cfg);
            this.procThread = new Thread(this.procRunnable);
            this.procThread.setDaemon(true);
            this.procThread.setName(String.valueOf(BOSHClient.class.getSimpleName()) + "[" + System.identityHashCode(this) + "]: Receive thread");
            this.procThread.start();
        } finally {
            this.lock.unlock();
        }
    }

    /* JADX INFO: finally extract failed */
    private void dispose(Throwable cause) {
        assertUnlocked();
        this.lock.lock();
        try {
            if (this.procThread != null) {
                this.procThread = null;
                this.lock.unlock();
                if (cause == null) {
                    fireConnectionClosed();
                } else {
                    fireConnectionClosedOnError(cause);
                }
                this.lock.lock();
                try {
                    clearEmptyRequest();
                    this.exchanges = null;
                    this.cmParams = null;
                    this.pendingResponseAcks = null;
                    this.pendingRequestAcks = null;
                    this.notEmpty.signalAll();
                    this.notFull.signalAll();
                    this.drained.signalAll();
                    this.lock.unlock();
                    this.httpSender.destroy();
                    this.schedExec.shutdownNow();
                } catch (Throwable th) {
                    this.lock.unlock();
                    throw th;
                }
            }
        } finally {
            this.lock.unlock();
        }
    }

    private static boolean isPause(AbstractBody msg) {
        return msg.getAttribute(Attributes.PAUSE) != null;
    }

    private static boolean isTermination(AbstractBody msg) {
        return TERMINATE.equals(msg.getAttribute(Attributes.TYPE));
    }

    private TerminalBindingCondition getTerminalBindingCondition(int respCode, AbstractBody respBody) {
        assertLocked();
        if (isTermination(respBody)) {
            return TerminalBindingCondition.forString(respBody.getAttribute(Attributes.CONDITION));
        }
        if (this.cmParams == null || this.cmParams.getVersion() != null) {
            return null;
        }
        return TerminalBindingCondition.forHTTPResponseCode(respCode);
    }

    private boolean isImmediatelySendable(AbstractBody msg) {
        int maxRequests;
        assertLocked();
        if (this.cmParams == null) {
            return this.exchanges.isEmpty();
        }
        AttrRequests requests = this.cmParams.getRequests();
        if (requests == null || this.exchanges.size() < (maxRequests = requests.intValue())) {
            return true;
        }
        if (this.exchanges.size() != maxRequests || (!isTermination(msg) && !isPause(msg))) {
            return false;
        }
        return true;
    }

    private boolean isWorking() {
        assertLocked();
        return this.procThread != null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.InterruptedException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private void blockUntilSendable(AbstractBody msg) {
        assertLocked();
        while (isWorking() && !isImmediatelySendable(msg)) {
            try {
                this.notFull.await();
            } catch (InterruptedException intx) {
                LOG.log(Level.FINEST, INTERRUPTED, (Throwable) intx);
            }
        }
    }

    private ComposableBody applySessionCreationRequest(long rid, ComposableBody orig) throws BOSHException {
        assertLocked();
        ComposableBody.Builder builder = orig.rebuild();
        builder.setAttribute(Attributes.TO, this.cfg.getTo());
        builder.setAttribute(Attributes.XML_LANG, this.cfg.getLang());
        builder.setAttribute(Attributes.VER, AttrVersion.getSupportedVersion().toString());
        builder.setAttribute(Attributes.WAIT, "60");
        builder.setAttribute(Attributes.HOLD, "1");
        builder.setAttribute(Attributes.RID, Long.toString(rid));
        applyRoute(builder);
        applyFrom(builder);
        builder.setAttribute(Attributes.ACK, "1");
        builder.setAttribute(Attributes.SID, null);
        return builder.build();
    }

    private void applyRoute(ComposableBody.Builder builder) {
        assertLocked();
        String route = this.cfg.getRoute();
        if (route != null) {
            builder.setAttribute(Attributes.ROUTE, route);
        }
    }

    private void applyFrom(ComposableBody.Builder builder) {
        assertLocked();
        String from = this.cfg.getFrom();
        if (from != null) {
            builder.setAttribute(Attributes.FROM, from);
        }
    }

    private ComposableBody applySessionData(long rid, ComposableBody orig) throws BOSHException {
        assertLocked();
        ComposableBody.Builder builder = orig.rebuild();
        builder.setAttribute(Attributes.SID, this.cmParams.getSessionID().toString());
        builder.setAttribute(Attributes.RID, Long.toString(rid));
        applyResponseAcknowledgement(builder, rid);
        return builder.build();
    }

    private void applyResponseAcknowledgement(ComposableBody.Builder builder, long rid) {
        assertLocked();
        if (!this.responseAck.equals(-1L)) {
            if (!this.responseAck.equals(Long.valueOf(rid - 1))) {
                builder.setAttribute(Attributes.ACK, this.responseAck.toString());
            }
        }
    }

    /* access modifiers changed from: private */
    public void processMessages() {
        LOG.log(Level.FINEST, "Processing thread starting");
        while (true) {
            try {
                HTTPExchange exch = nextExchange();
                if (exch == null) {
                    LOG.log(Level.FINEST, "Processing thread exiting");
                    return;
                }
                ExchangeInterceptor interceptor = this.exchInterceptor.get();
                if (interceptor != null) {
                    HTTPExchange newExch = interceptor.interceptExchange(exch);
                    if (newExch == null) {
                        LOG.log(Level.FINE, "Discarding exchange on request of test hook: RID=" + exch.getRequest().getAttribute(Attributes.RID));
                        this.lock.lock();
                        this.exchanges.remove(exch);
                        this.lock.unlock();
                    } else {
                        exch = newExch;
                    }
                }
                processExchange(exch);
            } catch (Throwable th) {
                LOG.log(Level.FINEST, "Processing thread exiting");
                throw th;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.InterruptedException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private HTTPExchange nextExchange() {
        assertUnlocked();
        Thread thread = Thread.currentThread();
        HTTPExchange exch = null;
        this.lock.lock();
        do {
            try {
                if (!thread.equals(this.procThread)) {
                    break;
                }
                exch = this.exchanges.peek();
                if (exch == null) {
                    this.notEmpty.await();
                    continue;
                }
            } catch (InterruptedException intx) {
                LOG.log(Level.FINEST, INTERRUPTED, (Throwable) intx);
                continue;
            } catch (Throwable th) {
                this.lock.unlock();
                throw th;
            }
        } while (exch == null);
        this.lock.unlock();
        return exch;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, com.kenai.jbosh.BOSHException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.InterruptedException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0277  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void processExchange(com.kenai.jbosh.HTTPExchange r20) {
        /*
            r19 = this;
            r19.assertUnlocked()
            com.kenai.jbosh.HTTPResponse r11 = r20.getHTTPResponse()     // Catch:{ BOSHException -> 0x0076, InterruptedException -> 0x008c }
            com.kenai.jbosh.AbstractBody r3 = r11.getBody()     // Catch:{ BOSHException -> 0x0076, InterruptedException -> 0x008c }
            int r12 = r11.getHTTPStatus()     // Catch:{ BOSHException -> 0x0076, InterruptedException -> 0x008c }
            r0 = r19
            r0.fireResponseReceived(r3)
            com.kenai.jbosh.AbstractBody r8 = r20.getRequest()
            r14 = 0
            r0 = r19
            java.util.concurrent.locks.ReentrantLock r0 = r0.lock
            r16 = r0
            r16.lock()
            boolean r16 = r19.isWorking()     // Catch:{ BOSHException -> 0x01f3 }
            if (r16 != 0) goto L_0x00ad
            r0 = r19
            java.util.concurrent.locks.ReentrantLock r0 = r0.lock     // Catch:{ BOSHException -> 0x01f3 }
            r16 = r0
            r16.unlock()     // Catch:{ BOSHException -> 0x01f3 }
            r0 = r19
            java.util.concurrent.locks.ReentrantLock r0 = r0.lock
            r16 = r0
            boolean r16 = r16.isHeldByCurrentThread()
            if (r16 == 0) goto L_0x0075
            r0 = r19
            java.util.Queue<com.kenai.jbosh.HTTPExchange> r0 = r0.exchanges     // Catch:{ all -> 0x00a2 }
            r16 = r0
            r0 = r16
            r1 = r20
            r0.remove(r1)     // Catch:{ all -> 0x00a2 }
            r0 = r19
            java.util.Queue<com.kenai.jbosh.HTTPExchange> r0 = r0.exchanges     // Catch:{ all -> 0x00a2 }
            r16 = r0
            boolean r16 = r16.isEmpty()     // Catch:{ all -> 0x00a2 }
            if (r16 == 0) goto L_0x0063
            r0 = r19
            long r16 = r0.processPauseRequest(r8)     // Catch:{ all -> 0x00a2 }
            r0 = r19
            r1 = r16
            r0.scheduleEmptyRequest(r1)     // Catch:{ all -> 0x00a2 }
        L_0x0063:
            r0 = r19
            java.util.concurrent.locks.Condition r0 = r0.notFull     // Catch:{ all -> 0x00a2 }
            r16 = r0
            r16.signalAll()     // Catch:{ all -> 0x00a2 }
            r0 = r19
            java.util.concurrent.locks.ReentrantLock r0 = r0.lock
            r16 = r0
            r16.unlock()
        L_0x0075:
            return
        L_0x0076:
            r4 = move-exception
            java.util.logging.Logger r16 = com.kenai.jbosh.BOSHClient.LOG
            java.util.logging.Level r17 = java.util.logging.Level.FINEST
            java.lang.String r18 = "Could not obtain response"
            r0 = r16
            r1 = r17
            r2 = r18
            r0.log(r1, r2, r4)
            r0 = r19
            r0.dispose(r4)
            goto L_0x0075
        L_0x008c:
            r6 = move-exception
            java.util.logging.Logger r16 = com.kenai.jbosh.BOSHClient.LOG
            java.util.logging.Level r17 = java.util.logging.Level.FINEST
            java.lang.String r18 = "Interrupted"
            r0 = r16
            r1 = r17
            r2 = r18
            r0.log(r1, r2, r6)
            r0 = r19
            r0.dispose(r6)
            goto L_0x0075
        L_0x00a2:
            r16 = move-exception
            r0 = r19
            java.util.concurrent.locks.ReentrantLock r0 = r0.lock
            r17 = r0
            r17.unlock()
            throw r16
        L_0x00ad:
            r0 = r19
            com.kenai.jbosh.CMSessionParams r0 = r0.cmParams     // Catch:{ BOSHException -> 0x01f3 }
            r16 = r0
            if (r16 != 0) goto L_0x00c2
            com.kenai.jbosh.CMSessionParams r16 = com.kenai.jbosh.CMSessionParams.fromSessionInit(r8, r3)     // Catch:{ BOSHException -> 0x01f3 }
            r0 = r16
            r1 = r19
            r1.cmParams = r0     // Catch:{ BOSHException -> 0x01f3 }
            r19.fireConnectionEstablished()     // Catch:{ BOSHException -> 0x01f3 }
        L_0x00c2:
            r0 = r19
            com.kenai.jbosh.CMSessionParams r7 = r0.cmParams     // Catch:{ BOSHException -> 0x01f3 }
            r0 = r19
            r0.checkForTerminalBindingConditions(r3, r12)     // Catch:{ BOSHException -> 0x01f3 }
            boolean r16 = isTermination(r3)     // Catch:{ BOSHException -> 0x01f3 }
            if (r16 == 0) goto L_0x0134
            r0 = r19
            java.util.concurrent.locks.ReentrantLock r0 = r0.lock     // Catch:{ BOSHException -> 0x01f3 }
            r16 = r0
            r16.unlock()     // Catch:{ BOSHException -> 0x01f3 }
            r16 = 0
            r0 = r19
            r1 = r16
            r0.dispose(r1)     // Catch:{ BOSHException -> 0x01f3 }
            r0 = r19
            java.util.concurrent.locks.ReentrantLock r0 = r0.lock
            r16 = r0
            boolean r16 = r16.isHeldByCurrentThread()
            if (r16 == 0) goto L_0x0075
            r0 = r19
            java.util.Queue<com.kenai.jbosh.HTTPExchange> r0 = r0.exchanges     // Catch:{ all -> 0x0129 }
            r16 = r0
            r0 = r16
            r1 = r20
            r0.remove(r1)     // Catch:{ all -> 0x0129 }
            r0 = r19
            java.util.Queue<com.kenai.jbosh.HTTPExchange> r0 = r0.exchanges     // Catch:{ all -> 0x0129 }
            r16 = r0
            boolean r16 = r16.isEmpty()     // Catch:{ all -> 0x0129 }
            if (r16 == 0) goto L_0x0115
            r0 = r19
            long r16 = r0.processPauseRequest(r8)     // Catch:{ all -> 0x0129 }
            r0 = r19
            r1 = r16
            r0.scheduleEmptyRequest(r1)     // Catch:{ all -> 0x0129 }
        L_0x0115:
            r0 = r19
            java.util.concurrent.locks.Condition r0 = r0.notFull     // Catch:{ all -> 0x0129 }
            r16 = r0
            r16.signalAll()     // Catch:{ all -> 0x0129 }
            r0 = r19
            java.util.concurrent.locks.ReentrantLock r0 = r0.lock
            r16 = r0
            r16.unlock()
            goto L_0x0075
        L_0x0129:
            r16 = move-exception
            r0 = r19
            java.util.concurrent.locks.ReentrantLock r0 = r0.lock
            r17 = r0
            r17.unlock()
            throw r16
        L_0x0134:
            boolean r16 = isRecoverableBindingCondition(r3)     // Catch:{ BOSHException -> 0x01f3 }
            if (r16 == 0) goto L_0x02b0
            if (r14 != 0) goto L_0x014c
            java.util.ArrayList r15 = new java.util.ArrayList     // Catch:{ BOSHException -> 0x01f3 }
            r0 = r19
            java.util.Queue<com.kenai.jbosh.HTTPExchange> r0 = r0.exchanges     // Catch:{ BOSHException -> 0x01f3 }
            r16 = r0
            int r16 = r16.size()     // Catch:{ BOSHException -> 0x01f3 }
            r15.<init>(r16)     // Catch:{ BOSHException -> 0x01f3 }
            r14 = r15
        L_0x014c:
            r0 = r19
            java.util.Queue<com.kenai.jbosh.HTTPExchange> r0 = r0.exchanges     // Catch:{ BOSHException -> 0x01f3 }
            r16 = r0
            java.util.Iterator r16 = r16.iterator()     // Catch:{ BOSHException -> 0x01f3 }
        L_0x0156:
            boolean r17 = r16.hasNext()     // Catch:{ BOSHException -> 0x01f3 }
            if (r17 != 0) goto L_0x01dd
            java.util.Iterator r16 = r14.iterator()     // Catch:{ BOSHException -> 0x01f3 }
        L_0x0160:
            boolean r17 = r16.hasNext()     // Catch:{ BOSHException -> 0x01f3 }
            if (r17 != 0) goto L_0x0257
        L_0x0166:
            r0 = r19
            java.util.concurrent.locks.ReentrantLock r0 = r0.lock
            r16 = r0
            boolean r16 = r16.isHeldByCurrentThread()
            if (r16 == 0) goto L_0x01aa
            r0 = r19
            java.util.Queue<com.kenai.jbosh.HTTPExchange> r0 = r0.exchanges     // Catch:{ all -> 0x02f2 }
            r16 = r0
            r0 = r16
            r1 = r20
            r0.remove(r1)     // Catch:{ all -> 0x02f2 }
            r0 = r19
            java.util.Queue<com.kenai.jbosh.HTTPExchange> r0 = r0.exchanges     // Catch:{ all -> 0x02f2 }
            r16 = r0
            boolean r16 = r16.isEmpty()     // Catch:{ all -> 0x02f2 }
            if (r16 == 0) goto L_0x0198
            r0 = r19
            long r16 = r0.processPauseRequest(r8)     // Catch:{ all -> 0x02f2 }
            r0 = r19
            r1 = r16
            r0.scheduleEmptyRequest(r1)     // Catch:{ all -> 0x02f2 }
        L_0x0198:
            r0 = r19
            java.util.concurrent.locks.Condition r0 = r0.notFull     // Catch:{ all -> 0x02f2 }
            r16 = r0
            r16.signalAll()     // Catch:{ all -> 0x02f2 }
            r0 = r19
            java.util.concurrent.locks.ReentrantLock r0 = r0.lock
            r16 = r0
            r16.unlock()
        L_0x01aa:
            if (r14 == 0) goto L_0x0075
            java.util.Iterator r16 = r14.iterator()
        L_0x01b0:
            boolean r17 = r16.hasNext()
            if (r17 == 0) goto L_0x0075
            java.lang.Object r9 = r16.next()
            com.kenai.jbosh.HTTPExchange r9 = (com.kenai.jbosh.HTTPExchange) r9
            r0 = r19
            com.kenai.jbosh.HTTPSender r0 = r0.httpSender
            r17 = r0
            com.kenai.jbosh.AbstractBody r18 = r9.getRequest()
            r0 = r17
            r1 = r18
            com.kenai.jbosh.HTTPResponse r13 = r0.send(r7, r1)
            r9.setHTTPResponse(r13)
            com.kenai.jbosh.AbstractBody r17 = r9.getRequest()
            r0 = r19
            r1 = r17
            r0.fireRequestSent(r1)
            goto L_0x01b0
        L_0x01dd:
            java.lang.Object r5 = r16.next()     // Catch:{ BOSHException -> 0x01f3 }
            com.kenai.jbosh.HTTPExchange r5 = (com.kenai.jbosh.HTTPExchange) r5     // Catch:{ BOSHException -> 0x01f3 }
            com.kenai.jbosh.HTTPExchange r10 = new com.kenai.jbosh.HTTPExchange     // Catch:{ BOSHException -> 0x01f3 }
            com.kenai.jbosh.AbstractBody r17 = r5.getRequest()     // Catch:{ BOSHException -> 0x01f3 }
            r0 = r17
            r10.<init>(r0)     // Catch:{ BOSHException -> 0x01f3 }
            r14.add(r10)     // Catch:{ BOSHException -> 0x01f3 }
            goto L_0x0156
        L_0x01f3:
            r4 = move-exception
        L_0x01f4:
            java.util.logging.Logger r16 = com.kenai.jbosh.BOSHClient.LOG     // Catch:{ all -> 0x026a }
            java.util.logging.Level r17 = java.util.logging.Level.FINEST     // Catch:{ all -> 0x026a }
            java.lang.String r18 = "Could not process response"
            r0 = r16
            r1 = r17
            r2 = r18
            r0.log(r1, r2, r4)     // Catch:{ all -> 0x026a }
            r0 = r19
            java.util.concurrent.locks.ReentrantLock r0 = r0.lock     // Catch:{ all -> 0x026a }
            r16 = r0
            r16.unlock()     // Catch:{ all -> 0x026a }
            r0 = r19
            r0.dispose(r4)     // Catch:{ all -> 0x026a }
            r0 = r19
            java.util.concurrent.locks.ReentrantLock r0 = r0.lock
            r16 = r0
            boolean r16 = r16.isHeldByCurrentThread()
            if (r16 == 0) goto L_0x0075
            r0 = r19
            java.util.Queue<com.kenai.jbosh.HTTPExchange> r0 = r0.exchanges     // Catch:{ all -> 0x02dc }
            r16 = r0
            r0 = r16
            r1 = r20
            r0.remove(r1)     // Catch:{ all -> 0x02dc }
            r0 = r19
            java.util.Queue<com.kenai.jbosh.HTTPExchange> r0 = r0.exchanges     // Catch:{ all -> 0x02dc }
            r16 = r0
            boolean r16 = r16.isEmpty()     // Catch:{ all -> 0x02dc }
            if (r16 == 0) goto L_0x0243
            r0 = r19
            long r16 = r0.processPauseRequest(r8)     // Catch:{ all -> 0x02dc }
            r0 = r19
            r1 = r16
            r0.scheduleEmptyRequest(r1)     // Catch:{ all -> 0x02dc }
        L_0x0243:
            r0 = r19
            java.util.concurrent.locks.Condition r0 = r0.notFull     // Catch:{ all -> 0x02dc }
            r16 = r0
            r16.signalAll()     // Catch:{ all -> 0x02dc }
            r0 = r19
            java.util.concurrent.locks.ReentrantLock r0 = r0.lock
            r16 = r0
            r16.unlock()
            goto L_0x0075
        L_0x0257:
            java.lang.Object r5 = r16.next()     // Catch:{ BOSHException -> 0x01f3 }
            com.kenai.jbosh.HTTPExchange r5 = (com.kenai.jbosh.HTTPExchange) r5     // Catch:{ BOSHException -> 0x01f3 }
            r0 = r19
            java.util.Queue<com.kenai.jbosh.HTTPExchange> r0 = r0.exchanges     // Catch:{ BOSHException -> 0x01f3 }
            r17 = r0
            r0 = r17
            r0.add(r5)     // Catch:{ BOSHException -> 0x01f3 }
            goto L_0x0160
        L_0x026a:
            r16 = move-exception
        L_0x026b:
            r0 = r19
            java.util.concurrent.locks.ReentrantLock r0 = r0.lock
            r17 = r0
            boolean r17 = r17.isHeldByCurrentThread()
            if (r17 == 0) goto L_0x02af
            r0 = r19
            java.util.Queue<com.kenai.jbosh.HTTPExchange> r0 = r0.exchanges     // Catch:{ all -> 0x02e7 }
            r17 = r0
            r0 = r17
            r1 = r20
            r0.remove(r1)     // Catch:{ all -> 0x02e7 }
            r0 = r19
            java.util.Queue<com.kenai.jbosh.HTTPExchange> r0 = r0.exchanges     // Catch:{ all -> 0x02e7 }
            r17 = r0
            boolean r17 = r17.isEmpty()     // Catch:{ all -> 0x02e7 }
            if (r17 == 0) goto L_0x029d
            r0 = r19
            long r17 = r0.processPauseRequest(r8)     // Catch:{ all -> 0x02e7 }
            r0 = r19
            r1 = r17
            r0.scheduleEmptyRequest(r1)     // Catch:{ all -> 0x02e7 }
        L_0x029d:
            r0 = r19
            java.util.concurrent.locks.Condition r0 = r0.notFull     // Catch:{ all -> 0x02e7 }
            r17 = r0
            r17.signalAll()     // Catch:{ all -> 0x02e7 }
            r0 = r19
            java.util.concurrent.locks.ReentrantLock r0 = r0.lock
            r17 = r0
            r17.unlock()
        L_0x02af:
            throw r16
        L_0x02b0:
            r0 = r19
            r0.processRequestAcknowledgements(r8, r3)     // Catch:{ BOSHException -> 0x01f3 }
            r0 = r19
            r0.processResponseAcknowledgementData(r8)     // Catch:{ BOSHException -> 0x01f3 }
            r0 = r19
            com.kenai.jbosh.HTTPExchange r10 = r0.processResponseAcknowledgementReport(r3)     // Catch:{ BOSHException -> 0x01f3 }
            if (r10 == 0) goto L_0x0166
            if (r14 != 0) goto L_0x0166
            java.util.ArrayList r15 = new java.util.ArrayList     // Catch:{ BOSHException -> 0x01f3 }
            r16 = 1
            r15.<init>(r16)     // Catch:{ BOSHException -> 0x01f3 }
            r15.add(r10)     // Catch:{ BOSHException -> 0x0301, all -> 0x02fd }
            r0 = r19
            java.util.Queue<com.kenai.jbosh.HTTPExchange> r0 = r0.exchanges     // Catch:{ BOSHException -> 0x0301, all -> 0x02fd }
            r16 = r0
            r0 = r16
            r0.add(r10)     // Catch:{ BOSHException -> 0x0301, all -> 0x02fd }
            r14 = r15
            goto L_0x0166
        L_0x02dc:
            r16 = move-exception
            r0 = r19
            java.util.concurrent.locks.ReentrantLock r0 = r0.lock
            r17 = r0
            r17.unlock()
            throw r16
        L_0x02e7:
            r16 = move-exception
            r0 = r19
            java.util.concurrent.locks.ReentrantLock r0 = r0.lock
            r17 = r0
            r17.unlock()
            throw r16
        L_0x02f2:
            r16 = move-exception
            r0 = r19
            java.util.concurrent.locks.ReentrantLock r0 = r0.lock
            r17 = r0
            r17.unlock()
            throw r16
        L_0x02fd:
            r16 = move-exception
            r14 = r15
            goto L_0x026b
        L_0x0301:
            r4 = move-exception
            r14 = r15
            goto L_0x01f4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenai.jbosh.BOSHClient.processExchange(com.kenai.jbosh.HTTPExchange):void");
    }

    private void clearEmptyRequest() {
        assertLocked();
        if (this.emptyRequestFuture != null) {
            this.emptyRequestFuture.cancel(false);
            this.emptyRequestFuture = null;
        }
    }

    private long getDefaultEmptyRequestDelay() {
        assertLocked();
        AttrPolling polling = this.cmParams.getPollingInterval();
        if (polling == null) {
            return (long) EMPTY_REQUEST_DELAY;
        }
        return (long) polling.getInMilliseconds();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.util.concurrent.RejectedExecutionException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private void scheduleEmptyRequest(long delay) {
        assertLocked();
        if (delay < 0) {
            throw new IllegalArgumentException("Empty request delay must be >= 0 (was: " + delay + ")");
        }
        clearEmptyRequest();
        if (isWorking()) {
            if (LOG.isLoggable(Level.FINER)) {
                LOG.finer("Scheduling empty request in " + delay + "ms");
            }
            try {
                this.emptyRequestFuture = this.schedExec.schedule(this.emptyRequestRunnable, delay, TimeUnit.MILLISECONDS);
            } catch (RejectedExecutionException rex) {
                LOG.log(Level.FINEST, "Could not schedule empty request", (Throwable) rex);
            }
            this.drained.signalAll();
        }
    }

    /* access modifiers changed from: private */
    public void sendEmptyRequest() {
        assertUnlocked();
        LOG.finest("Sending empty request");
        try {
            send(ComposableBody.builder().build());
        } catch (BOSHException boshx) {
            dispose(boshx);
        }
    }

    private void assertLocked() {
        if (ASSERTIONS && !this.lock.isHeldByCurrentThread()) {
            throw new AssertionError("Lock is not held by current thread");
        }
    }

    private void assertUnlocked() {
        if (ASSERTIONS && this.lock.isHeldByCurrentThread()) {
            throw new AssertionError("Lock is held by current thread");
        }
    }

    private void checkForTerminalBindingConditions(AbstractBody body, int code) throws BOSHException {
        TerminalBindingCondition cond = getTerminalBindingCondition(code, body);
        if (cond != null) {
            throw new BOSHException("Terminal binding condition encountered: " + cond.getCondition() + "  (" + cond.getMessage() + ")");
        }
    }

    private static boolean isRecoverableBindingCondition(AbstractBody resp) {
        return ERROR.equals(resp.getAttribute(Attributes.TYPE));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, com.kenai.jbosh.BOSHException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private long processPauseRequest(AbstractBody req) {
        assertLocked();
        if (!(this.cmParams == null || this.cmParams.getMaxPause() == null)) {
            try {
                AttrPause pause = AttrPause.createFromString(req.getAttribute(Attributes.PAUSE));
                if (pause != null) {
                    long delay = (long) (pause.getInMilliseconds() - PAUSE_MARGIN);
                    if (delay < 0) {
                        return (long) EMPTY_REQUEST_DELAY;
                    }
                    return delay;
                }
            } catch (BOSHException boshx) {
                LOG.log(Level.FINEST, "Could not extract", (Throwable) boshx);
            }
        }
        return getDefaultEmptyRequestDelay();
    }

    private void processRequestAcknowledgements(AbstractBody req, AbstractBody resp) {
        Long ackUpTo;
        assertLocked();
        if (this.cmParams.isAckingRequests() && resp.getAttribute(Attributes.REPORT) == null) {
            String acked = resp.getAttribute(Attributes.ACK);
            if (acked == null) {
                ackUpTo = Long.valueOf(Long.parseLong(req.getAttribute(Attributes.RID)));
            } else {
                ackUpTo = Long.valueOf(Long.parseLong(acked));
            }
            if (LOG.isLoggable(Level.FINEST)) {
                LOG.finest("Removing pending acks up to: " + ackUpTo);
            }
            Iterator<ComposableBody> iter = this.pendingRequestAcks.iterator();
            while (iter.hasNext()) {
                if (Long.valueOf(Long.parseLong(iter.next().getAttribute(Attributes.RID))).compareTo(ackUpTo) <= 0) {
                    iter.remove();
                }
            }
        }
    }

    private void processResponseAcknowledgementData(AbstractBody req) {
        assertLocked();
        Long rid = Long.valueOf(Long.parseLong(req.getAttribute(Attributes.RID)));
        if (this.responseAck.equals(-1L)) {
            this.responseAck = rid;
            return;
        }
        this.pendingResponseAcks.add(rid);
        Long whileVal = Long.valueOf(this.responseAck.longValue() + 1);
        while (!this.pendingResponseAcks.isEmpty() && whileVal.equals(this.pendingResponseAcks.first())) {
            this.responseAck = whileVal;
            this.pendingResponseAcks.remove(whileVal);
            whileVal = Long.valueOf(whileVal.longValue() + 1);
        }
    }

    private HTTPExchange processResponseAcknowledgementReport(AbstractBody resp) throws BOSHException {
        assertLocked();
        String reportStr = resp.getAttribute(Attributes.REPORT);
        if (reportStr == null) {
            return null;
        }
        Long report = Long.valueOf(Long.parseLong(reportStr));
        Long time = Long.valueOf(Long.parseLong(resp.getAttribute(Attributes.TIME)));
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("Received report of missing request (RID=" + report + ", time=" + time + "ms)");
        }
        Iterator<ComposableBody> iter = this.pendingRequestAcks.iterator();
        AbstractBody req = null;
        while (iter.hasNext() && req == null) {
            AbstractBody pending = iter.next();
            if (report.equals(Long.valueOf(Long.parseLong(pending.getAttribute(Attributes.RID))))) {
                req = pending;
            }
        }
        if (req == null) {
            throw new BOSHException("Report of missing message with RID '" + reportStr + "' but local copy of that request was not found");
        }
        HTTPExchange exch = new HTTPExchange(req);
        this.exchanges.add(exch);
        this.notEmpty.signalAll();
        return exch;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private void fireRequestSent(AbstractBody request) {
        assertUnlocked();
        BOSHMessageEvent event = null;
        for (BOSHClientRequestListener listener : this.requestListeners) {
            if (event == null) {
                event = BOSHMessageEvent.createRequestSentEvent(this, request);
            }
            try {
                listener.requestSent(event);
            } catch (Exception ex) {
                LOG.log(Level.WARNING, UNHANDLED, (Throwable) ex);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private void fireResponseReceived(AbstractBody response) {
        assertUnlocked();
        BOSHMessageEvent event = null;
        for (BOSHClientResponseListener listener : this.responseListeners) {
            if (event == null) {
                event = BOSHMessageEvent.createResponseReceivedEvent(this, response);
            }
            try {
                listener.responseReceived(event);
            } catch (Exception ex) {
                LOG.log(Level.WARNING, UNHANDLED, (Throwable) ex);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private void fireConnectionEstablished() {
        boolean hadLock = this.lock.isHeldByCurrentThread();
        if (hadLock) {
            this.lock.unlock();
        }
        BOSHClientConnEvent event = null;
        try {
            for (BOSHClientConnListener listener : this.connListeners) {
                if (event == null) {
                    event = BOSHClientConnEvent.createConnectionEstablishedEvent(this);
                }
                listener.connectionEvent(event);
            }
            if (hadLock) {
                this.lock.lock();
            }
        } catch (Exception ex) {
            LOG.log(Level.WARNING, UNHANDLED, (Throwable) ex);
        } catch (Throwable th) {
            if (hadLock) {
                this.lock.lock();
            }
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private void fireConnectionClosed() {
        assertUnlocked();
        BOSHClientConnEvent event = null;
        for (BOSHClientConnListener listener : this.connListeners) {
            if (event == null) {
                event = BOSHClientConnEvent.createConnectionClosedEvent(this);
            }
            try {
                listener.connectionEvent(event);
            } catch (Exception ex) {
                LOG.log(Level.WARNING, UNHANDLED, (Throwable) ex);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private void fireConnectionClosedOnError(Throwable cause) {
        assertUnlocked();
        BOSHClientConnEvent event = null;
        for (BOSHClientConnListener listener : this.connListeners) {
            if (event == null) {
                event = BOSHClientConnEvent.createConnectionClosedOnErrorEvent(this, this.pendingRequestAcks, cause);
            }
            try {
                listener.connectionEvent(event);
            } catch (Exception ex) {
                LOG.log(Level.WARNING, UNHANDLED, (Throwable) ex);
            }
        }
    }
}
