package com.kenai.jbosh;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

final class ServiceLib {
    private static final Logger LOG = Logger.getLogger(ServiceLib.class.getName());

    private ServiceLib() {
    }

    static <T> T loadService(Class<T> ofType) {
        for (String implClass : loadServicesImplementations(ofType)) {
            T result = attemptLoad(ofType, implClass);
            if (result != null) {
                if (LOG.isLoggable(Level.FINEST)) {
                    LOG.finest("Selected " + ofType.getSimpleName() + " implementation: " + result.getClass().getName());
                }
                return result;
            }
        }
        throw new IllegalStateException("Could not load " + ofType.getName() + " implementation");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.io.IOException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private static List<String> loadServicesImplementations(Class ofClass) {
        List<String> result = new ArrayList<>();
        String override = System.getProperty(ofClass.getName());
        if (override != null) {
            result.add(override);
        }
        URL url = ServiceLib.class.getClassLoader().getResource("META-INF/services/" + ofClass.getName());
        if (url != null) {
            InputStream inStream = null;
            InputStreamReader reader = null;
            BufferedReader bReader = null;
            try {
                inStream = url.openStream();
                InputStreamReader reader2 = new InputStreamReader(inStream);
                try {
                    BufferedReader bReader2 = new BufferedReader(reader2);
                    while (true) {
                        try {
                            String line = bReader2.readLine();
                            if (line == null) {
                                break;
                            } else if (!line.matches("\\s*(#.*)?")) {
                                result.add(line.trim());
                            }
                        } catch (IOException e) {
                            iox = e;
                            bReader = bReader2;
                            reader = reader2;
                        } catch (Throwable th) {
                            th = th;
                            bReader = bReader2;
                            reader = reader2;
                            finalClose(bReader);
                            finalClose(reader);
                            finalClose(inStream);
                            throw th;
                        }
                    }
                    finalClose(bReader2);
                    finalClose(reader2);
                    finalClose(inStream);
                } catch (IOException e2) {
                    iox = e2;
                    reader = reader2;
                    try {
                        LOG.log(Level.WARNING, "Could not load services descriptor: " + url.toString(), (Throwable) iox);
                        finalClose(bReader);
                        finalClose(reader);
                        finalClose(inStream);
                        return result;
                    } catch (Throwable th2) {
                        th = th2;
                        finalClose(bReader);
                        finalClose(reader);
                        finalClose(inStream);
                        throw th;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    reader = reader2;
                    finalClose(bReader);
                    finalClose(reader);
                    finalClose(inStream);
                    throw th;
                }
            } catch (IOException e3) {
                iox = e3;
                LOG.log(Level.WARNING, "Could not load services descriptor: " + url.toString(), (Throwable) iox);
                finalClose(bReader);
                finalClose(reader);
                finalClose(inStream);
                return result;
            }
        }
        return result;
    }

    private static <T> T attemptLoad(Class<T> ofClass, String className) {
        Level level;
        Throwable thrown;
        if (LOG.isLoggable(Level.FINEST)) {
            LOG.finest("Attempting service load: " + className);
        }
        try {
            Class clazz = Class.forName(className);
            if (ofClass.isAssignableFrom(clazz)) {
                return ofClass.cast(clazz.newInstance());
            }
            if (!LOG.isLoggable(Level.WARNING)) {
                return null;
            }
            LOG.warning(String.valueOf(clazz.getName()) + " is not assignable to " + ofClass.getName());
            return null;
        } catch (LinkageError ex) {
            level = Level.FINEST;
            thrown = ex;
            LOG.log(level, "Could not load " + ofClass.getSimpleName() + " instance: " + className, thrown);
            return null;
        } catch (ClassNotFoundException ex2) {
            level = Level.FINEST;
            thrown = ex2;
            LOG.log(level, "Could not load " + ofClass.getSimpleName() + " instance: " + className, thrown);
            return null;
        } catch (InstantiationException ex3) {
            level = Level.WARNING;
            thrown = ex3;
            LOG.log(level, "Could not load " + ofClass.getSimpleName() + " instance: " + className, thrown);
            return null;
        } catch (IllegalAccessException ex4) {
            level = Level.WARNING;
            thrown = ex4;
            LOG.log(level, "Could not load " + ofClass.getSimpleName() + " instance: " + className, thrown);
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.io.IOException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private static void finalClose(Closeable closeMe) {
        if (closeMe != null) {
            try {
                closeMe.close();
            } catch (IOException iox) {
                LOG.log(Level.FINEST, "Could not close: " + closeMe, (Throwable) iox);
            }
        }
    }
}
