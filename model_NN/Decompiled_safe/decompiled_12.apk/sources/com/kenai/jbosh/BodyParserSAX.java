package com.kenai.jbosh;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

final class BodyParserSAX implements BodyParser {
    /* access modifiers changed from: private */
    public static final Logger LOG = Logger.getLogger(BodyParserSAX.class.getName());
    private static final ThreadLocal<SoftReference<SAXParser>> PARSER = new ThreadLocal<SoftReference<SAXParser>>() {
        /* access modifiers changed from: protected */
        public SoftReference<SAXParser> initialValue() {
            return new SoftReference<>(null);
        }
    };
    private static final SAXParserFactory SAX_FACTORY = SAXParserFactory.newInstance();

    BodyParserSAX() {
    }

    static {
        SAX_FACTORY.setNamespaceAware(true);
        SAX_FACTORY.setValidating(false);
    }

    private static final class Handler extends DefaultHandler {
        private String defaultNS;
        private final SAXParser parser;
        private final BodyParserResults result;

        private Handler(SAXParser theParser, BodyParserResults results) {
            this.defaultNS = null;
            this.parser = theParser;
            this.result = results;
        }

        /* synthetic */ Handler(SAXParser sAXParser, BodyParserResults bodyParserResults, Handler handler) {
            this(sAXParser, bodyParserResults);
        }

        public void startElement(String uri, String localName, String qName, Attributes attributes) {
            if (BodyParserSAX.LOG.isLoggable(Level.FINEST)) {
                BodyParserSAX.LOG.finest("Start element: " + qName);
                BodyParserSAX.LOG.finest("    URI: " + uri);
                BodyParserSAX.LOG.finest("    local: " + localName);
            }
            BodyQName bodyName = AbstractBody.getBodyQName();
            if (!bodyName.getNamespaceURI().equals(uri) || !bodyName.getLocalPart().equals(localName)) {
                throw new IllegalStateException("Root element was not '" + bodyName.getLocalPart() + "' in the '" + bodyName.getNamespaceURI() + "' namespace.  (Was '" + localName + "' in '" + uri + "')");
            }
            for (int idx = 0; idx < attributes.getLength(); idx++) {
                String attrURI = attributes.getURI(idx);
                if (attrURI.length() == 0) {
                    attrURI = this.defaultNS;
                }
                String attrLN = attributes.getLocalName(idx);
                String attrVal = attributes.getValue(idx);
                if (BodyParserSAX.LOG.isLoggable(Level.FINEST)) {
                    BodyParserSAX.LOG.finest("    Attribute: {" + attrURI + "}" + attrLN + " = '" + attrVal + "'");
                }
                this.result.addBodyAttributeValue(BodyQName.create(attrURI, attrLN), attrVal);
            }
            this.parser.reset();
        }

        public void startPrefixMapping(String prefix, String uri) {
            if (prefix.length() == 0) {
                if (BodyParserSAX.LOG.isLoggable(Level.FINEST)) {
                    BodyParserSAX.LOG.finest("Prefix mapping: <DEFAULT> => " + uri);
                }
                this.defaultNS = uri;
            } else if (BodyParserSAX.LOG.isLoggable(Level.FINEST)) {
                BodyParserSAX.LOG.info("Prefix mapping: " + prefix + " => " + uri);
            }
        }
    }

    public BodyParserResults parse(String xml) throws BOSHException {
        Exception thrown;
        BodyParserResults result = new BodyParserResults();
        try {
            InputStream inStream = new ByteArrayInputStream(xml.getBytes());
            SAXParser parser = getSAXParser();
            parser.parse(inStream, new Handler(parser, result, null));
            return result;
        } catch (SAXException saxx) {
            thrown = saxx;
            throw new BOSHException("Could not parse body:\n" + xml, thrown);
        } catch (IOException iox) {
            thrown = iox;
            throw new BOSHException("Could not parse body:\n" + xml, thrown);
        }
    }

    private static SAXParser getSAXParser() {
        Exception thrown;
        SAXParser result = (SAXParser) PARSER.get().get();
        if (result == null) {
            try {
                SAXParser result2 = SAX_FACTORY.newSAXParser();
                try {
                    PARSER.set(new SoftReference<>(result2));
                    return result2;
                } catch (ParserConfigurationException e) {
                    ex = e;
                    thrown = ex;
                    throw new IllegalStateException("Could not create SAX parser", thrown);
                } catch (SAXException e2) {
                    ex = e2;
                    thrown = ex;
                    throw new IllegalStateException("Could not create SAX parser", thrown);
                }
            } catch (ParserConfigurationException e3) {
                ex = e3;
                thrown = ex;
                throw new IllegalStateException("Could not create SAX parser", thrown);
            } catch (SAXException e4) {
                ex = e4;
                thrown = ex;
                throw new IllegalStateException("Could not create SAX parser", thrown);
            }
        } else {
            result.reset();
            return result;
        }
    }
}
