package com.kenai.jbosh;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

final class GZIPCodec {
    private static final int BUFFER_SIZE = 512;

    private GZIPCodec() {
    }

    public static String getID() {
        return "gzip";
    }

    public static byte[] encode(byte[] data) throws IOException {
        ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
        GZIPOutputStream gzOut = null;
        try {
            GZIPOutputStream gzOut2 = new GZIPOutputStream(byteOut);
            try {
                gzOut2.write(data);
                gzOut2.close();
                byteOut.close();
                byte[] byteArray = byteOut.toByteArray();
                gzOut2.close();
                byteOut.close();
                return byteArray;
            } catch (Throwable th) {
                th = th;
                gzOut = gzOut2;
                gzOut.close();
                byteOut.close();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            gzOut.close();
            byteOut.close();
            throw th;
        }
    }

    public static byte[] decode(byte[] compressed) throws IOException {
        int read;
        ByteArrayInputStream byteIn = new ByteArrayInputStream(compressed);
        ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
        GZIPInputStream gzIn = null;
        try {
            GZIPInputStream gzIn2 = new GZIPInputStream(byteIn);
            try {
                byte[] buffer = new byte[512];
                do {
                    read = gzIn2.read(buffer);
                    if (read > 0) {
                        byteOut.write(buffer, 0, read);
                        continue;
                    }
                } while (read >= 0);
                byte[] byteArray = byteOut.toByteArray();
                gzIn2.close();
                byteOut.close();
                return byteArray;
            } catch (Throwable th) {
                th = th;
                gzIn = gzIn2;
                gzIn.close();
                byteOut.close();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            gzIn.close();
            byteOut.close();
            throw th;
        }
    }
}
