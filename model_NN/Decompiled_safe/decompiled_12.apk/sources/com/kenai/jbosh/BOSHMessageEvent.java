package com.kenai.jbosh;

import java.util.EventObject;

public final class BOSHMessageEvent extends EventObject {
    private static final long serialVersionUID = 1;
    private final AbstractBody body;

    private BOSHMessageEvent(Object source, AbstractBody cBody) {
        super(source);
        if (cBody == null) {
            throw new IllegalArgumentException("message body may not be null");
        }
        this.body = cBody;
    }

    static BOSHMessageEvent createRequestSentEvent(BOSHClient source, AbstractBody body2) {
        return new BOSHMessageEvent(source, body2);
    }

    static BOSHMessageEvent createResponseReceivedEvent(BOSHClient source, AbstractBody body2) {
        return new BOSHMessageEvent(source, body2);
    }

    public AbstractBody getBody() {
        return this.body;
    }
}
