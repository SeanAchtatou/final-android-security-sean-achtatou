package com.kenai.jbosh;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.http.HttpHost;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

final class ApacheHTTPSender implements HTTPSender {
    private BOSHClientConfig cfg;
    private HttpClient httpClient;
    private final Lock lock = new ReentrantLock();

    ApacheHTTPSender() {
        HttpClient.class.getName();
    }

    public void init(BOSHClientConfig session) {
        this.lock.lock();
        try {
            this.cfg = session;
            this.httpClient = initHttpClient(session);
        } finally {
            this.lock.unlock();
        }
    }

    public void destroy() {
        this.lock.lock();
        try {
            if (this.httpClient != null) {
                this.httpClient.getConnectionManager().shutdown();
            }
        } finally {
            this.cfg = null;
            this.httpClient = null;
            this.lock.unlock();
        }
    }

    /* JADX INFO: finally extract failed */
    public HTTPResponse send(CMSessionParams params, AbstractBody body) {
        this.lock.lock();
        try {
            if (this.httpClient == null) {
                this.httpClient = initHttpClient(this.cfg);
            }
            HttpClient mClient = this.httpClient;
            BOSHClientConfig mCfg = this.cfg;
            this.lock.unlock();
            return new ApacheHTTPResponse(mClient, mCfg, params, body);
        } catch (Throwable th) {
            this.lock.unlock();
            throw th;
        }
    }

    private synchronized HttpClient initHttpClient(BOSHClientConfig config) {
        HttpParams params;
        SchemeRegistry schemeRegistry;
        params = new BasicHttpParams();
        ConnManagerParams.setMaxTotalConnections(params, 100);
        HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setUseExpectContinue(params, false);
        if (!(config == null || config.getProxyHost() == null || config.getProxyPort() == 0)) {
            params.setParameter("http.route.default-proxy", new HttpHost(config.getProxyHost(), config.getProxyPort()));
        }
        schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        SSLSocketFactory sslFactory = SSLSocketFactory.getSocketFactory();
        sslFactory.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        schemeRegistry.register(new Scheme("https", sslFactory, 443));
        return new DefaultHttpClient(new ThreadSafeClientConnManager(params, schemeRegistry), params);
    }
}
