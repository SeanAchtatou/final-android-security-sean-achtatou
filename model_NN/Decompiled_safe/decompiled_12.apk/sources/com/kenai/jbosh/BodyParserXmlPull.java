package com.kenai.jbosh;

import java.io.IOException;
import java.io.StringReader;
import java.lang.ref.SoftReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

final class BodyParserXmlPull implements BodyParser {
    private static final Logger LOG = Logger.getLogger(BodyParserXmlPull.class.getName());
    private static final ThreadLocal<SoftReference<XmlPullParser>> XPP_PARSER = new ThreadLocal<SoftReference<XmlPullParser>>() {
        /* access modifiers changed from: protected */
        public SoftReference<XmlPullParser> initialValue() {
            return new SoftReference<>(null);
        }
    };

    BodyParserXmlPull() {
    }

    public BodyParserResults parse(String xml) throws BOSHException {
        Exception thrown;
        BodyParserResults result = new BodyParserResults();
        try {
            XmlPullParser xpp = getXmlPullParser();
            xpp.setInput(new StringReader(xml));
            int eventType = xpp.getEventType();
            while (true) {
                if (eventType == 1) {
                    break;
                } else if (eventType == 2) {
                    if (LOG.isLoggable(Level.FINEST)) {
                        LOG.finest("Start tag: " + xpp.getName());
                    }
                    String prefix = xpp.getPrefix();
                    if (prefix == null) {
                        prefix = "";
                    }
                    String uri = xpp.getNamespace();
                    String localName = xpp.getName();
                    QName name = new QName(uri, localName, prefix);
                    if (LOG.isLoggable(Level.FINEST)) {
                        LOG.finest("Start element: ");
                        LOG.finest("    prefix: " + prefix);
                        LOG.finest("    URI: " + uri);
                        LOG.finest("    local: " + localName);
                    }
                    BodyQName bodyName = AbstractBody.getBodyQName();
                    if (!bodyName.equalsQName(name)) {
                        throw new IllegalStateException("Root element was not '" + bodyName.getLocalPart() + "' in the '" + bodyName.getNamespaceURI() + "' namespace.  (Was '" + localName + "' in '" + uri + "')");
                    }
                    for (int idx = 0; idx < xpp.getAttributeCount(); idx++) {
                        String attrURI = xpp.getAttributeNamespace(idx);
                        if (attrURI.length() == 0) {
                            attrURI = xpp.getNamespace(null);
                        }
                        String attrPrefix = xpp.getAttributePrefix(idx);
                        if (attrPrefix == null) {
                            attrPrefix = "";
                        }
                        String attrLN = xpp.getAttributeName(idx);
                        String attrVal = xpp.getAttributeValue(idx);
                        BodyQName aqn = BodyQName.createWithPrefix(attrURI, attrLN, attrPrefix);
                        if (LOG.isLoggable(Level.FINEST)) {
                            LOG.finest("        Attribute: {" + attrURI + "}" + attrLN + " = '" + attrVal + "'");
                        }
                        result.addBodyAttributeValue(aqn, attrVal);
                    }
                } else {
                    eventType = xpp.next();
                }
            }
            return result;
        } catch (RuntimeException rtx) {
            thrown = rtx;
            throw new BOSHException("Could not parse body:\n" + xml, thrown);
        } catch (XmlPullParserException xmlppx) {
            thrown = xmlppx;
            throw new BOSHException("Could not parse body:\n" + xml, thrown);
        } catch (IOException iox) {
            thrown = iox;
            throw new BOSHException("Could not parse body:\n" + xml, thrown);
        }
    }

    private static XmlPullParser getXmlPullParser() {
        XmlPullParser result = (XmlPullParser) XPP_PARSER.get().get();
        if (result != null) {
            return result;
        }
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            factory.setValidating(false);
            XmlPullParser result2 = factory.newPullParser();
            try {
                XPP_PARSER.set(new SoftReference<>(result2));
                return result2;
            } catch (Exception e) {
                ex = e;
                throw new IllegalStateException("Could not create XmlPull parser", ex);
            }
        } catch (Exception e2) {
            ex = e2;
            throw new IllegalStateException("Could not create XmlPull parser", ex);
        }
    }
}
