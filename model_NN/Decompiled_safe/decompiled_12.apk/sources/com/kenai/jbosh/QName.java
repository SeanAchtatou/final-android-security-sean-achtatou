package com.kenai.jbosh;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import org.xbill.DNS.WKSRecord;

public class QName implements Serializable {
    private static final String emptyString = "".intern();
    private String localPart;
    private String namespaceURI;
    private String prefix;

    public QName(String localPart2) {
        this(emptyString, localPart2, emptyString);
    }

    public QName(String namespaceURI2, String localPart2) {
        this(namespaceURI2, localPart2, emptyString);
    }

    public QName(String namespaceURI2, String localPart2, String prefix2) {
        String intern;
        if (namespaceURI2 == null) {
            intern = emptyString;
        } else {
            intern = namespaceURI2.intern();
        }
        this.namespaceURI = intern;
        if (localPart2 == null) {
            throw new IllegalArgumentException("invalid QName local part");
        }
        this.localPart = localPart2.intern();
        if (prefix2 == null) {
            throw new IllegalArgumentException("invalid QName prefix");
        }
        this.prefix = prefix2.intern();
    }

    public String getNamespaceURI() {
        return this.namespaceURI;
    }

    public String getLocalPart() {
        return this.localPart;
    }

    public String getPrefix() {
        return this.prefix;
    }

    public String toString() {
        if (this.namespaceURI == emptyString) {
            return this.localPart;
        }
        return String.valueOf('{') + this.namespaceURI + '}' + this.localPart;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof QName)) {
            return false;
        }
        if (this.namespaceURI == ((QName) obj).namespaceURI && this.localPart == ((QName) obj).localPart) {
            return true;
        }
        return false;
    }

    public static QName valueOf(String s) {
        if (s == null || s.equals("")) {
            throw new IllegalArgumentException("invalid QName literal");
        } else if (s.charAt(0) != '{') {
            return new QName(s);
        } else {
            int i = s.indexOf((int) WKSRecord.Service.LOCUS_MAP);
            if (i == -1) {
                throw new IllegalArgumentException("invalid QName literal");
            } else if (i != s.length() - 1) {
                return new QName(s.substring(1, i), s.substring(i + 1));
            } else {
                throw new IllegalArgumentException("invalid QName literal");
            }
        }
    }

    public final int hashCode() {
        return this.namespaceURI.hashCode() ^ this.localPart.hashCode();
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        this.namespaceURI = this.namespaceURI.intern();
        this.localPart = this.localPart.intern();
        this.prefix = this.prefix.intern();
    }
}
