package com.kenai.jbosh;

import java.lang.Comparable;

abstract class AbstractAttr<T extends Comparable> implements Comparable {
    private final T value;

    protected AbstractAttr(T aValue) {
        this.value = aValue;
    }

    public final T getValue() {
        return this.value;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean equals(java.lang.Object r4) {
        /*
            r3 = this;
            r1 = 0
            if (r4 != 0) goto L_0x0004
        L_0x0003:
            return r1
        L_0x0004:
            boolean r2 = r4 instanceof com.kenai.jbosh.AbstractAttr
            if (r2 == 0) goto L_0x0003
            r0 = r4
            com.kenai.jbosh.AbstractAttr r0 = (com.kenai.jbosh.AbstractAttr) r0
            T r1 = r3.value
            T r2 = r0.value
            boolean r1 = r1.equals(r2)
            goto L_0x0003
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenai.jbosh.AbstractAttr.equals(java.lang.Object):boolean");
    }

    public int hashCode() {
        return this.value.hashCode();
    }

    public String toString() {
        return this.value.toString();
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public int compareTo(java.lang.Object r2) {
        /*
            r1 = this;
            if (r2 != 0) goto L_0x0004
            r0 = 1
        L_0x0003:
            return r0
        L_0x0004:
            T r0 = r1.value
            int r0 = r0.compareTo(r2)
            goto L_0x0003
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenai.jbosh.AbstractAttr.compareTo(java.lang.Object):int");
    }
}
