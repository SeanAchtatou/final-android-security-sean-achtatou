package com.kenai.jbosh;

import java.io.IOException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

final class ApacheHTTPResponse implements HTTPResponse {
    private static final String ACCEPT_ENCODING = "Accept-Encoding";
    private static final String ACCEPT_ENCODING_VAL = (String.valueOf(ZLIBCodec.getID()) + ", " + GZIPCodec.getID());
    private static final String CHARSET = "UTF-8";
    private static final String CONTENT_TYPE = "text/xml; charset=utf-8";
    private AbstractBody body;
    private final HttpClient client;
    private final HttpContext context;
    private final Lock lock = new ReentrantLock();
    private final HttpPost post;
    private boolean sent;
    private int statusCode;
    private BOSHException toThrow;

    ApacheHTTPResponse(HttpClient client2, BOSHClientConfig cfg, CMSessionParams params, AbstractBody request) {
        AttrAccept accept;
        this.client = client2;
        this.context = new BasicHttpContext();
        this.post = new HttpPost(cfg.getURI().toString());
        this.sent = false;
        try {
            byte[] data = request.toXML().getBytes(CHARSET);
            String encoding = null;
            if (!(!cfg.isCompressionEnabled() || params == null || (accept = params.getAccept()) == null)) {
                if (accept.isAccepted(ZLIBCodec.getID())) {
                    encoding = ZLIBCodec.getID();
                    data = ZLIBCodec.encode(data);
                } else if (accept.isAccepted(GZIPCodec.getID())) {
                    encoding = GZIPCodec.getID();
                    data = GZIPCodec.encode(data);
                }
            }
            ByteArrayEntity entity = new ByteArrayEntity(data);
            entity.setContentType(CONTENT_TYPE);
            if (encoding != null) {
                entity.setContentEncoding(encoding);
            }
            this.post.setEntity(entity);
            if (cfg.isCompressionEnabled()) {
                this.post.setHeader(ACCEPT_ENCODING, ACCEPT_ENCODING_VAL);
            }
        } catch (Exception e) {
            this.toThrow = new BOSHException("Could not generate request", e);
        }
    }

    public void abort() {
        if (this.post != null) {
            this.post.abort();
            this.toThrow = new BOSHException("HTTP request aborted");
        }
    }

    /* JADX INFO: finally extract failed */
    public AbstractBody getBody() throws InterruptedException, BOSHException {
        if (this.toThrow != null) {
            throw this.toThrow;
        }
        this.lock.lock();
        try {
            if (!this.sent) {
                awaitResponse();
            }
            this.lock.unlock();
            return this.body;
        } catch (Throwable th) {
            this.lock.unlock();
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public int getHTTPStatus() throws InterruptedException, BOSHException {
        if (this.toThrow != null) {
            throw this.toThrow;
        }
        this.lock.lock();
        try {
            if (!this.sent) {
                awaitResponse();
            }
            this.lock.unlock();
            return this.statusCode;
        } catch (Throwable th) {
            this.lock.unlock();
            throw th;
        }
    }

    private synchronized void awaitResponse() throws BOSHException {
        String encoding;
        try {
            HttpResponse httpResp = this.client.execute(this.post, this.context);
            HttpEntity entity = httpResp.getEntity();
            byte[] data = EntityUtils.toByteArray(entity);
            if (entity.getContentEncoding() != null) {
                encoding = entity.getContentEncoding().getValue();
            } else {
                encoding = null;
            }
            if (ZLIBCodec.getID().equalsIgnoreCase(encoding)) {
                data = ZLIBCodec.decode(data);
            } else if (GZIPCodec.getID().equalsIgnoreCase(encoding)) {
                data = GZIPCodec.decode(data);
            }
            this.body = StaticBody.fromString(new String(data, CHARSET));
            this.statusCode = httpResp.getStatusLine().getStatusCode();
            this.sent = true;
        } catch (IOException iox) {
            abort();
            this.toThrow = new BOSHException("Could not obtain response", iox);
            throw this.toThrow;
        } catch (RuntimeException ex) {
            abort();
            throw ex;
        }
    }
}
