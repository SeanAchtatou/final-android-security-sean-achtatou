package com.kenfor.exutil;

import javax.servlet.ServletContext;
import org.apache.struts.action.ActionServlet;

public class InitXmlConstant {
    protected String path = null;
    protected String realPath = "/WEB-INF/classes/";
    private xmlConstant xmlCon = null;

    public void setRealPath(String newRealPath) {
        this.realPath = newRealPath;
    }

    public String getRealPath() {
        return this.realPath;
    }

    public xmlConstant getXmlConstant() {
        return this.xmlCon;
    }

    public InitXmlConstant(ServletContext servletContext) {
        this.path = servletContext.getRealPath("/WEB-INF/classes/");
        String trade_id = (String) servletContext.getAttribute("trade_id");
        this.xmlCon = (xmlConstant) servletContext.getAttribute("xmlConstant");
        boolean newXml = false;
        if (this.xmlCon == null) {
            try {
                this.xmlCon = new xmlConstant(this.path, trade_id);
                newXml = true;
            } catch (Exception e) {
                this.xmlCon = null;
            }
        }
        if (newXml) {
            servletContext.setAttribute("xmlConstant", this.xmlCon);
        }
    }

    public InitXmlConstant(ActionServlet servlet, String xmlName) {
        this.path = servlet.getServletContext().getRealPath("/WEB-INF/classes/");
        String str = (String) servlet.getServletContext().getAttribute("trade_id");
        this.xmlCon = (xmlConstant) servlet.getServletContext().getAttribute("xmlConstant");
        boolean newXml = false;
        if (this.xmlCon == null) {
            try {
                this.xmlCon = new xmlConstant(this.path, xmlName);
                newXml = true;
            } catch (Exception e) {
                this.xmlCon = null;
            }
        }
        if (newXml) {
            servlet.getServletContext().setAttribute("xmlConstant", this.xmlCon);
        }
    }

    public InitXmlConstant(ActionServlet servlet) {
        this.path = servlet.getServletContext().getRealPath("/WEB-INF/classes/");
        String str = (String) servlet.getServletContext().getAttribute("trade_id");
        this.xmlCon = (xmlConstant) servlet.getServletContext().getAttribute("xmlConstant");
        boolean newXml = false;
        if (this.xmlCon == null) {
            try {
                this.xmlCon = new xmlConstant(this.path, "pro-config.xml");
                newXml = true;
            } catch (Exception e) {
                this.xmlCon = null;
            }
        }
        if (newXml) {
            servlet.getServletContext().setAttribute("xmlConstant", this.xmlCon);
        }
    }

    public InitXmlConstant(String path2, String trade_id) {
        try {
            this.xmlCon = new xmlConstant(path2, trade_id);
        } catch (Exception e) {
            this.xmlCon = null;
        }
    }
}
