package com.kenfor.exutil;

import com.sun.image.codec.jpeg.JPEGCodec;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class checkImage extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=GBK";
    String bkColor = null;
    private Color[] color = {Color.red, Color.black, Color.white, Color.yellow, Color.orange, Color.green};
    private String[] color_font = {"ECF410", "FBFBFB", "351878", "ECF410", "FBFBFB", "351878"};
    private String[] color_line = {"6699CC", "FFCC99", "66CC66", "99CCCC", "CC99FF", "99CCFF"};
    String ftColor = null;
    String high = null;
    int ibkColor = 2;
    int iftColor = 1;
    int ihigh = 20;
    int iwidth = 55;
    String width = null;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.width = request.getParameter("width");
        this.high = request.getParameter("high");
        this.bkColor = request.getParameter("bkColor");
        this.ftColor = request.getParameter("ftColor");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.setContentType("image/jpeg");
        ServletOutputStream out = response.getOutputStream();
        BufferedImage image = new BufferedImage(this.iwidth, this.ihigh, 1);
        Graphics2D g = image.getGraphics();
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setColor(this.color[this.ibkColor]);
        g.fillRect(0, 0, this.iwidth, this.ihigh);
        g.setColor(this.color[this.iftColor]);
        String check_no = getRandStr();
        g.drawImage(ImageIO.read(new FileInputStream(new File(new StringBuffer().append(request.getRealPath("/WEB-INF/")).append(System.getProperty("file.separator")).append("ck_image.jpg").toString()))), (BufferedImageOp) null, 0 - ((int) (Math.random() * 20.0d)), 0);
        g.setFont(new Font((String) null, 1, 15));
        String check_no_1 = check_no.substring(0, 2);
        String check_no_2 = check_no.substring(2, 4);
        String check_no_3 = check_no.substring(4);
        g.setColor(ColorUtil.getColor(this.color_font[(int) ((Math.random() * 5.0d) + 1.0d)]));
        g.drawString(check_no_1, 4, 15);
        g.setColor(ColorUtil.getColor(this.color_font[(int) ((Math.random() * 5.0d) + 1.0d)]));
        g.drawString(check_no_2, 24, 15);
        g.setColor(ColorUtil.getColor(this.color_font[(int) ((Math.random() * 5.0d) + 1.0d)]));
        g.drawString(check_no_3, 43, 15);
        request.getSession().removeAttribute("check_no");
        request.getSession().setAttribute("check_no", check_no);
        JPEGCodec.createJPEGEncoder(out).encode(image);
        out.close();
    }

    public void destroy() {
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    private String getRandStr() {
        String result = "";
        for (int i = 0; i < 5; i++) {
            int ind = (int) ((Math.random() * ((double) ("23456789abcde".length() - 1))) + 1.0d);
            result = new StringBuffer().append(result).append("23456789abcde".substring(ind, ind + 1)).toString();
        }
        return result;
    }

    private void initParam() {
        if (this.width != null) {
            try {
                this.iwidth = Integer.valueOf(this.width).intValue();
            } catch (Exception e) {
                this.iwidth = 100;
            }
        }
        if (this.high != null) {
            try {
                this.ihigh = Integer.valueOf(this.high).intValue();
            } catch (Exception e2) {
                this.ihigh = 100;
            }
        }
        if (this.bkColor != null) {
            try {
                this.ibkColor = Integer.valueOf(this.bkColor).intValue();
            } catch (Exception e3) {
                this.ibkColor = 100;
            }
        }
        if (this.ftColor != null) {
            try {
                this.iftColor = Integer.valueOf(this.ftColor).intValue();
            } catch (Exception e4) {
                this.iftColor = 100;
            }
        }
    }
}
