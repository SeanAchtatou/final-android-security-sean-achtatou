package com.kenfor.exutil;

import com.kenfor.util.CodeTransfer;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class writeMessage {
    private static String createErrorMessage(String show_message, String charset) {
        StringBuffer mesBuf = new StringBuffer();
        if (show_message == null) {
            show_message = "";
        }
        mesBuf.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=");
        mesBuf.append(charset);
        mesBuf.append("\"><BODY>\n");
        mesBuf.append("<SCRIPT LANGUAGE=\"JavaScript\">\n");
        mesBuf.append("<!--\n");
        mesBuf.append("alert(\"");
        if (show_message == null) {
            mesBuf.append("出错，请返回");
        } else {
            mesBuf.append(show_message);
        }
        mesBuf.append("\");\n");
        mesBuf.append("history.go(-1);\n");
        mesBuf.append("//--></SCRIPT>\n</BODY>");
        return mesBuf.toString();
    }

    public static void writeErrorMessage(HttpServletResponse httpServletResponse, String show_message, String charset) {
        Log log = LogFactory.getLog("writeMessage");
        try {
            httpServletResponse.setHeader("accept-language", "UTF-8");
            PrintWriter write = httpServletResponse.getWriter();
            write.write(CodeTransfer.UnicodeToISO(createErrorMessage(show_message, charset)));
            write.flush();
            write.close();
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }
}
