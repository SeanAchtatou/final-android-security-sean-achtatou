package com.kenfor.exutil;

import com.kenfor.database.DatabaseInfo;
import com.kenfor.database.InitDatabase;
import com.kenfor.database.dbMaxID;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionServlet;

public class uploadFileAdmin {
    protected static String fileDir = "db_picture";
    protected static Log log = LogFactory.getLog("uploadFileAdmin");
    protected static String tableName = "bas_uppic_infor";

    private uploadFileAdmin() {
    }

    public static boolean doFileDelete(ActionServlet servlet, String fileName) {
        if (fileName == null || "null".equals(fileName)) {
            return false;
        }
        xmlConstant xmlCon = new InitXmlConstant(servlet).getXmlConstant();
        try {
            String upload_dir = xmlCon.upload_pic_dir;
            String temp = fileName;
            String file_sep = System.getProperty("file.separator");
            if ("\\".equals(file_sep)) {
                char[] temps = temp.toCharArray();
                for (int i = 0; i < temps.length; i++) {
                    if (temps[i] == '/') {
                        temps[i] = '\\';
                    }
                }
                temp = String.valueOf(temps);
            }
            String upload_dir2 = strTrim(upload_dir);
            String temp2 = strTrim(temp);
            if (!upload_dir2.endsWith(file_sep)) {
                upload_dir2 = new StringBuffer().append(upload_dir2).append(file_sep).toString();
            }
            String file_name = new StringBuffer().append(upload_dir2).append(temp2).toString();
            if (log.isDebugEnabled()) {
                log.debug(new StringBuffer().append("file_name:").append(file_name).toString());
            }
            File file = new File(file_name);
            if (file.exists()) {
                file.delete();
            } else {
                log.warn(new StringBuffer().append("file is not exist,file_name:").append(file_name).toString());
            }
            return true;
        } catch (Exception e) {
            log.error(e.getMessage());
            log.error(new StringBuffer().append("xmlCon:").append(xmlCon).toString());
            log.error(new StringBuffer().append("file_name:").append((String) null).toString());
            log.error(new StringBuffer().append("upload_dir:").append((String) null).toString());
            return false;
        }
    }

    private static boolean doFileDelete(HttpServletRequest request, String fileName) {
        if (fileName == null) {
            return false;
        }
        try {
            String upload_dir = new xmlConstant(request.getRealPath("/WEB-INF/classes/"), "pro-config.xml").upload_pic_dir;
            String temp = fileName;
            if ("\\".equals(System.getProperty("file.separator"))) {
                char[] temps = temp.toCharArray();
                for (int i = 0; i < temps.length; i++) {
                    if (temps[i] == '/') {
                        temps[i] = '\\';
                    }
                }
                temp = String.valueOf(temps);
            }
            String upload_dir2 = strTrim(upload_dir);
            File file = new File(new StringBuffer().append(upload_dir2).append(strTrim(temp)).toString());
            if (file.exists()) {
                file.delete();
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean doFileDelete(String fileName) {
        if (fileName == null || "null".equals(fileName)) {
            return false;
        }
        try {
            File file = new File(strTrim(fileName));
            if (file.exists()) {
                file.delete();
            }
            return true;
        } catch (Exception e) {
            log.error(e.getMessage());
            return false;
        }
    }

    private static String getCurFilePath(String str) {
        String temp = str;
        if (!"\\".equals(System.getProperty("file.separator"))) {
            return temp;
        }
        char[] temps = temp.toCharArray();
        for (int i = 0; i < temps.length; i++) {
            if (temps[i] == '/') {
                temps[i] = '\\';
            }
        }
        return String.valueOf(temps);
    }

    private static String strTrim(String str) {
        return str == null ? str : str.trim();
    }

    public static boolean doFileDelete(String file_dir, String fileName) {
        String file_path;
        if (fileName == null || "null".equals(fileName) || file_dir == null || "null".equals(file_dir)) {
            return false;
        }
        String upload_dir = file_dir;
        String temp = fileName;
        try {
            String file_sep = System.getProperty("file.separator");
            if ("\\".equals(file_sep)) {
                char[] temps = temp.toCharArray();
                for (int i = 0; i < temps.length; i++) {
                    if (temps[i] == '/') {
                        temps[i] = '\\';
                    }
                }
                temp = String.valueOf(temps);
            }
            String upload_dir2 = strTrim(upload_dir);
            String temp2 = strTrim(temp);
            if (upload_dir2.endsWith(file_sep)) {
                file_path = new StringBuffer().append(upload_dir2).append(temp2).toString();
            } else {
                file_path = new StringBuffer().append(upload_dir2).append(file_sep).append(temp2).toString();
            }
            File file = new File(file_path);
            if (file.exists()) {
                file.delete();
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:44:0x0192  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean doFileAdmin(org.apache.struts.action.ActionServlet r24, java.lang.String r25, java.lang.String r26) {
        /*
            r12 = 0
            r8 = 0
            r4 = 0
            r14 = 0
            r11 = 0
            com.kenfor.exutil.InitAction r9 = new com.kenfor.exutil.InitAction     // Catch:{ Exception -> 0x01b7 }
            r0 = r24
            r9.<init>(r0)     // Catch:{ Exception -> 0x01b7 }
            java.sql.Connection r4 = r9.getCon()     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            java.sql.Statement r14 = r4.createStatement()     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            java.lang.StringBuffer r21 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            r21.<init>()     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            java.lang.String r22 = "select use_time from bas_uppic_infor where pic_name='"
            java.lang.StringBuffer r21 = r21.append(r22)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            r0 = r21
            r1 = r25
            java.lang.StringBuffer r21 = r0.append(r1)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            java.lang.String r22 = "'"
            java.lang.StringBuffer r21 = r21.append(r22)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            java.lang.String r15 = r21.toString()     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            java.sql.ResultSet r13 = r14.executeQuery(r15)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            r19 = -1
            boolean r21 = r13.next()     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            if (r21 == 0) goto L_0x0045
            java.lang.String r21 = "use_time"
            r0 = r21
            int r19 = r13.getInt(r0)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
        L_0x0045:
            java.lang.String r21 = "add"
            r0 = r21
            r1 = r26
            boolean r21 = r0.equalsIgnoreCase(r1)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            if (r21 == 0) goto L_0x00b9
            long r2 = com.kenfor.database.dbMaxID.getMaxID()     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            r21 = -1
            r0 = r19
            r1 = r21
            if (r0 != r1) goto L_0x009b
            java.lang.StringBuffer r21 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            r21.<init>()     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            java.lang.String r22 = "insert into bas_uppic_infor (up_pic_id,pic_name,use_time) values("
            java.lang.StringBuffer r21 = r21.append(r22)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            java.lang.String r22 = java.lang.String.valueOf(r2)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            java.lang.StringBuffer r21 = r21.append(r22)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            java.lang.String r22 = ",'"
            java.lang.StringBuffer r21 = r21.append(r22)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            r0 = r21
            r1 = r25
            java.lang.StringBuffer r21 = r0.append(r1)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            java.lang.String r22 = "',1)"
            java.lang.StringBuffer r21 = r21.append(r22)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            java.lang.String r15 = r21.toString()     // Catch:{ Exception -> 0x013c, all -> 0x018e }
        L_0x0088:
            r14.execute(r15)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            r14.close()     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            r4.close()     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            r11 = 1
            if (r11 != 0) goto L_0x0097
            com.kenfor.util.CloseCon.Close(r4)
        L_0x0097:
            r21 = 1
            r8 = r9
        L_0x009a:
            return r21
        L_0x009b:
            java.lang.StringBuffer r21 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            r21.<init>()     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            java.lang.String r22 = "update bas_uppic_infor set use_time=use_time+1 where pic_name='"
            java.lang.StringBuffer r21 = r21.append(r22)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            r0 = r21
            r1 = r25
            java.lang.StringBuffer r21 = r0.append(r1)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            java.lang.String r22 = "'"
            java.lang.StringBuffer r21 = r21.append(r22)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            java.lang.String r15 = r21.toString()     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            goto L_0x0088
        L_0x00b9:
            java.lang.String r21 = "delete"
            r0 = r21
            r1 = r26
            boolean r21 = r0.equalsIgnoreCase(r1)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            if (r21 == 0) goto L_0x0088
            r21 = 1
            r0 = r19
            r1 = r21
            if (r0 > r1) goto L_0x0196
            if (r19 < 0) goto L_0x00ec
            java.lang.StringBuffer r21 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            r21.<init>()     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            java.lang.String r22 = "delete from bas_uppic_infor where pic_name='"
            java.lang.StringBuffer r21 = r21.append(r22)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            r0 = r21
            r1 = r25
            java.lang.StringBuffer r21 = r0.append(r1)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            java.lang.String r22 = "'"
            java.lang.StringBuffer r21 = r21.append(r22)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            java.lang.String r15 = r21.toString()     // Catch:{ Exception -> 0x013c, all -> 0x018e }
        L_0x00ec:
            java.lang.String r21 = "file.separator"
            java.lang.String r7 = java.lang.System.getProperty(r21)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            com.kenfor.exutil.InitXmlConstant r10 = new com.kenfor.exutil.InitXmlConstant     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            r0 = r24
            r10.<init>(r0)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            com.kenfor.exutil.xmlConstant r20 = r10.getXmlConstant()     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            r0 = r20
            java.lang.String r0 = r0.upload_pic_dir     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            r18 = r0
            java.lang.String r16 = getCurFilePath(r25)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            r17 = 0
            r0 = r18
            boolean r21 = r0.endsWith(r7)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            if (r21 == 0) goto L_0x0150
            java.lang.StringBuffer r21 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            r21.<init>()     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            r0 = r21
            r1 = r18
            java.lang.StringBuffer r21 = r0.append(r1)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            r0 = r21
            r1 = r16
            java.lang.StringBuffer r21 = r0.append(r1)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            java.lang.String r17 = r21.toString()     // Catch:{ Exception -> 0x013c, all -> 0x018e }
        L_0x012a:
            java.io.File r6 = new java.io.File     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            r0 = r17
            r6.<init>(r0)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            boolean r21 = r6.exists()     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            if (r21 == 0) goto L_0x0170
            r6.delete()     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            goto L_0x0088
        L_0x013c:
            r5 = move-exception
            r8 = r9
        L_0x013e:
            org.apache.commons.logging.Log r21 = com.kenfor.exutil.uploadFileAdmin.log     // Catch:{ all -> 0x01b5 }
            java.lang.String r22 = r5.getMessage()     // Catch:{ all -> 0x01b5 }
            r21.error(r22)     // Catch:{ all -> 0x01b5 }
            r21 = 0
            if (r11 != 0) goto L_0x009a
            com.kenfor.util.CloseCon.Close(r4)
            goto L_0x009a
        L_0x0150:
            java.lang.StringBuffer r21 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            r21.<init>()     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            r0 = r21
            r1 = r18
            java.lang.StringBuffer r21 = r0.append(r1)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            r0 = r21
            java.lang.StringBuffer r21 = r0.append(r7)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            r0 = r21
            r1 = r16
            java.lang.StringBuffer r21 = r0.append(r1)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            java.lang.String r17 = r21.toString()     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            goto L_0x012a
        L_0x0170:
            org.apache.commons.logging.Log r21 = com.kenfor.exutil.uploadFileAdmin.log     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            java.lang.StringBuffer r22 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            r22.<init>()     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            java.lang.String r23 = "file not exist,file_name:"
            java.lang.StringBuffer r22 = r22.append(r23)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            r0 = r22
            r1 = r17
            java.lang.StringBuffer r22 = r0.append(r1)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            java.lang.String r22 = r22.toString()     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            r21.warn(r22)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            goto L_0x0088
        L_0x018e:
            r21 = move-exception
            r8 = r9
        L_0x0190:
            if (r11 != 0) goto L_0x0195
            com.kenfor.util.CloseCon.Close(r4)
        L_0x0195:
            throw r21
        L_0x0196:
            java.lang.StringBuffer r21 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            r21.<init>()     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            java.lang.String r22 = "update bas_uppic_infor set use_time=use_time-1  where pic_name='"
            java.lang.StringBuffer r21 = r21.append(r22)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            r0 = r21
            r1 = r25
            java.lang.StringBuffer r21 = r0.append(r1)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            java.lang.String r22 = "'"
            java.lang.StringBuffer r21 = r21.append(r22)     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            java.lang.String r15 = r21.toString()     // Catch:{ Exception -> 0x013c, all -> 0x018e }
            goto L_0x0088
        L_0x01b5:
            r21 = move-exception
            goto L_0x0190
        L_0x01b7:
            r5 = move-exception
            goto L_0x013e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenfor.exutil.uploadFileAdmin.doFileAdmin(org.apache.struts.action.ActionServlet, java.lang.String, java.lang.String):boolean");
    }

    private static boolean doFileAdmin(HttpServletRequest request, String fileName, String actionType) {
        if (fileName == null) {
            return false;
        }
        String path = request.getRealPath("/WEB-INF/classes");
        try {
            DatabaseInfo dbInfo = new InitDatabase(path).getDatabaseInfo();
            if (dbInfo != null) {
                Class.forName(dbInfo.getDriver());
                Connection con = DriverManager.getConnection(dbInfo.getUrl(), dbInfo.getUsername(), dbInfo.getPassword());
                Statement stmt = con.createStatement();
                String str_sql = new StringBuffer().append("select use_time from bas_uppic_infor where pic_name='").append(fileName).append("'").toString();
                ResultSet rs = stmt.executeQuery(str_sql);
                int use_time = -1;
                if (rs.next()) {
                    use_time = rs.getInt("use_time");
                }
                if ("add".equalsIgnoreCase(actionType)) {
                    long bas_id = dbMaxID.getMaxID();
                    if (use_time == -1) {
                        str_sql = new StringBuffer().append("insert into bas_uppic_infor (up_pic_id,pic_name,use_time) values(").append(String.valueOf(bas_id)).append(",'").append(fileName).append("',1)").toString();
                    } else {
                        str_sql = new StringBuffer().append("update bas_uppic_infor set use_time=use_time+1 where pic_name='").append(fileName).append("'").toString();
                    }
                } else if ("delete".equalsIgnoreCase(actionType)) {
                    if (use_time <= 1) {
                        if (use_time >= 0) {
                            str_sql = new StringBuffer().append("delete from bas_uppic_infor where pic_name='").append(fileName).append("'").toString();
                        }
                        String realPath = request.getRealPath(fileDir);
                        String upload_dir = new xmlConstant(path, "pro-config.xml").upload_pic_dir;
                        File file = new File(new StringBuffer().append(upload_dir).append(getCurFilePath(fileName)).toString());
                        if (file.exists()) {
                            file.delete();
                        }
                    } else {
                        str_sql = new StringBuffer().append("update bas_uppic_infor set use_time=use_time-1  where pic_name='").append(fileName).append("'").toString();
                    }
                }
                stmt.execute(str_sql);
                stmt.close();
                con.close();
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
