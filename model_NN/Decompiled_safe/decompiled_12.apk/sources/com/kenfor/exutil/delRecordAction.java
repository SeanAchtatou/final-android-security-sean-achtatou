package com.kenfor.exutil;

import com.kenfor.taglib.db.dbPresentTag;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class delRecordAction extends Action {
    public ActionForward perform(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        String r_file_name;
        String r_file_name2;
        new ActionErrors();
        pageMapping pageM = (pageMapping) actionMapping;
        String back_url = (String) httpServletRequest.getSession().getAttribute("back_url");
        InitAction initAction = new InitAction(this.servlet);
        Connection con = null;
        ActionErrors errors = initAction.getErrors();
        if (!errors.isEmpty()) {
            saveErrors(httpServletRequest, errors);
            return actionMapping.findForward("failure");
        }
        String table_name = pageM.getTable_name();
        String field_id_name = pageM.getField_id_name();
        String file_field_list = pageM.getFile_field_list();
        String field_list = "";
        HashMap fieldMap = new HashMap();
        ArrayList fieldList = new ArrayList();
        if (file_field_list != null && file_field_list.length() > 0) {
            String[] field = file_field_list.split(dbPresentTag.ROLE_DELIMITER);
            for (int i = 0; i < field.length; i++) {
                String p_value = strTrim(getParamStr(field[i]));
                fieldList.add(p_value);
                if (p_value != null && p_value.length() > 0) {
                    fieldMap.put(p_value, field[i]);
                    if (i == field.length - 1) {
                        field_list = new StringBuffer().append(field_list).append(p_value).toString();
                    } else {
                        field_list = new StringBuffer().append(field_list).append(p_value).append(dbPresentTag.ROLE_DELIMITER).toString();
                    }
                }
            }
        }
        if (table_name == null || field_id_name == null) {
            errors.add("org.apache.struts.action.GLOBAL_ERROR", new ActionError("common.parameter.setting.error"));
            saveErrors(httpServletRequest, errors);
            return actionMapping.findForward("failure");
        }
        String[] ckboxvalue = httpServletRequest.getParameterValues("selcheck");
        String realPath = this.servlet.getServletContext().getRealPath("/WEB-INF/classes/");
        String field_id_name_value = httpServletRequest.getParameter(field_id_name);
        ArrayList sql_list = new ArrayList();
        ArrayList file_list = new ArrayList();
        if ((ckboxvalue != null && ckboxvalue.length > 0) || field_id_name_value != null) {
            if (ckboxvalue == null || ckboxvalue.length <= 0) {
                try {
                    con = initAction.getCon();
                    Statement t_ment = con.createStatement();
                    ResultSet t_rs = t_ment.executeQuery(new StringBuffer().append("select ").append(file_field_list).append(" from ").append(table_name).append(" where ").append(field_id_name).append(" = ").append(field_id_name_value).toString());
                    if (t_rs.next()) {
                        for (int j = 0; j < fieldList.size(); j++) {
                            String field_str = (String) fieldList.get(j);
                            String t_file_name = t_rs.getString(field_str);
                            if (t_file_name != null && t_file_name.length() > 0 && (r_file_name = createValue((String) fieldMap.get(field_str), t_file_name)) != null && r_file_name.length() > 0) {
                                file_list.add(r_file_name);
                            }
                        }
                    }
                    t_rs.close();
                    t_ment.close();
                    con.close();
                } catch (Exception e) {
                    System.out.println(new StringBuffer().append("获取需删除的数据时出错：").append(e.getMessage()).toString());
                }
                sql_list.add(new StringBuffer().append("delete from ").append(table_name).append(" where ").append(field_id_name).append(" = ").append(field_id_name_value).toString());
            } else {
                for (int i2 = 0; i2 < ckboxvalue.length; i2++) {
                    if (field_list != null && field_list.length() > 0) {
                        try {
                            con = initAction.getCon();
                            Statement t_ment2 = con.createStatement();
                            ResultSet t_rs2 = t_ment2.executeQuery(new StringBuffer().append("select ").append(field_list).append(" from ").append(table_name).append(" where ").append(field_id_name).append(" = ").append(ckboxvalue[i2]).toString());
                            if (t_rs2.next()) {
                                for (int j2 = 0; j2 < fieldList.size(); j2++) {
                                    String field_str2 = (String) fieldList.get(j2);
                                    String t_file_name2 = t_rs2.getString(field_str2);
                                    if (t_file_name2 != null && t_file_name2.length() > 0 && (r_file_name2 = createValue((String) fieldMap.get(field_str2), t_file_name2)) != null && r_file_name2.length() > 0) {
                                        file_list.add(r_file_name2);
                                    }
                                }
                            }
                            t_rs2.close();
                            t_ment2.close();
                            con.close();
                        } catch (SQLException e2) {
                            System.out.println(new StringBuffer().append("获取需删除的数据时出错：").append(e2.getMessage()).toString());
                        }
                    }
                    sql_list.add(new StringBuffer().append("delete from ").append(table_name).append(" where ").append(field_id_name).append(" = ").append(ckboxvalue[i2]).toString());
                }
            }
            try {
                con = initAction.getCon();
                con.setAutoCommit(false);
                Statement stmt = con.createStatement();
                for (int i3 = 0; i3 < sql_list.size(); i3++) {
                    stmt.addBatch((String) sql_list.get(i3));
                }
                stmt.executeBatch();
                con.commit();
                con.setAutoCommit(true);
                stmt.close();
                con.close();
            } catch (SQLException e3) {
                System.out.print(e3);
                try {
                    con.rollback();
                    con.close();
                } catch (SQLException e4) {
                    errors.add("org.apache.struts.action.GLOBAL_ERROR", new ActionError("database.rollback.error"));
                    saveErrors(httpServletRequest, errors);
                }
                errors.add("org.apache.struts.action.GLOBAL_ERROR", new ActionError("database.delete.error.reason", e3.getMessage()));
                saveErrors(httpServletRequest, errors);
                return actionMapping.findForward("failure");
            }
        }
        for (int i4 = 0; i4 < file_list.size(); i4++) {
            uploadFileAdmin.doFileDelete((String) file_list.get(i4));
        }
        if (back_url == null || back_url.length() <= 0) {
            return actionMapping.findForward("success");
        }
        try {
            httpServletResponse.sendRedirect("back_url");
        } catch (Exception e5) {
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public String getParamStr(String param_field) {
        int len = param_field.indexOf("[");
        int len2 = param_field.indexOf("]");
        if (len < 0 || len2 < 0 || len > len2) {
            return null;
        }
        return param_field.substring(len + 1, len2);
    }

    /* access modifiers changed from: protected */
    public String strTrim(String str) {
        if (str != null) {
            return str.trim();
        }
        return str;
    }

    /* access modifiers changed from: protected */
    public String createValue(String param_field, String param_value) {
        int len = param_field.indexOf("[");
        int len2 = param_field.indexOf("]");
        if (len < 0 || len2 < 0 || len > len2) {
            return null;
        }
        String l_str = param_field.substring(0, len);
        return new StringBuffer().append(l_str).append(param_value).append(param_field.substring(len2 + 1)).toString();
    }
}
