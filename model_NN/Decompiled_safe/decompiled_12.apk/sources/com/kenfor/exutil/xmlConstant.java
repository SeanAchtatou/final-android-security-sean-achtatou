package com.kenfor.exutil;

import java.io.FileInputStream;
import java.io.InputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

public class xmlConstant {
    protected String filename;
    protected InputStream in;
    Log log;
    public String masterMail;
    protected String path;
    public String picUrl;
    protected String realPath;
    public String secWeb;
    public String smtp;
    public String smtpP;
    public String smtpU;
    protected String sys_separator;
    public String upload_pic_dir;
    public String url_base;

    public xmlConstant() throws Exception {
        this(System.getProperty("user.dir"));
    }

    public xmlConstant(String path2) throws Exception {
        this(path2, "pro-config.xml");
    }

    public xmlConstant(String path2, String filename2) throws Exception {
        this.path = null;
        this.filename = "pro-config.xml";
        this.realPath = null;
        this.sys_separator = "/";
        this.upload_pic_dir = null;
        this.url_base = null;
        this.smtp = null;
        this.smtpU = null;
        this.smtpP = null;
        this.masterMail = null;
        this.secWeb = null;
        this.picUrl = null;
        this.log = LogFactory.getLog(getClass().getName());
        this.sys_separator = System.getProperty("file.separator");
        this.path = path2;
        this.filename = filename2;
        try {
            iniXmlConstant();
        } catch (Exception e) {
        }
    }

    public xmlConstant(String path2, String filename2, boolean auto) throws Exception {
        this.path = null;
        this.filename = "pro-config.xml";
        this.realPath = null;
        this.sys_separator = "/";
        this.upload_pic_dir = null;
        this.url_base = null;
        this.smtp = null;
        this.smtpU = null;
        this.smtpP = null;
        this.masterMail = null;
        this.secWeb = null;
        this.picUrl = null;
        this.log = LogFactory.getLog(getClass().getName());
        this.sys_separator = System.getProperty("file.separator");
        this.path = path2;
        this.filename = filename2;
        if (auto) {
            iniXmlConstant();
        }
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:15:0x0093=Splitter:B:15:0x0093, B:22:0x00aa=Splitter:B:22:0x00aa} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void iniXmlConstant() throws java.lang.Exception {
        /*
            r9 = this;
            java.lang.String r7 = r9.filename
            if (r7 == 0) goto L_0x0017
            java.lang.String r7 = r9.filename
            int r7 = r7.length()
            r8 = 1
            if (r7 < r8) goto L_0x0017
            java.lang.String r7 = r9.filename
            java.lang.String r8 = "null"
            boolean r7 = r7.equalsIgnoreCase(r8)
            if (r7 == 0) goto L_0x001b
        L_0x0017:
            java.lang.String r7 = "pro-config.xml"
            r9.filename = r7
        L_0x001b:
            java.lang.StringBuffer r7 = new java.lang.StringBuffer
            r7.<init>()
            java.lang.String r8 = r9.path
            java.lang.StringBuffer r7 = r7.append(r8)
            java.lang.String r8 = r9.sys_separator
            java.lang.StringBuffer r7 = r7.append(r8)
            java.lang.String r8 = r9.filename
            java.lang.StringBuffer r7 = r7.append(r8)
            java.lang.String r7 = r7.toString()
            r9.realPath = r7
            r3 = 0
            r5 = 0
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ JDOMException -> 0x0092, NullPointerException -> 0x00a9, Exception -> 0x00bb }
            java.lang.String r7 = r9.realPath     // Catch:{ JDOMException -> 0x0092, NullPointerException -> 0x00a9, Exception -> 0x00bb }
            r4.<init>(r7)     // Catch:{ JDOMException -> 0x0092, NullPointerException -> 0x00a9, Exception -> 0x00bb }
            org.jdom.input.SAXBuilder r1 = new org.jdom.input.SAXBuilder     // Catch:{ JDOMException -> 0x00d6, NullPointerException -> 0x00d3, Exception -> 0x00d0, all -> 0x00cd }
            r1.<init>()     // Catch:{ JDOMException -> 0x00d6, NullPointerException -> 0x00d3, Exception -> 0x00d0, all -> 0x00cd }
            org.jdom.Document r0 = r1.build(r4)     // Catch:{ JDOMException -> 0x00d6, NullPointerException -> 0x00d3, Exception -> 0x00d0, all -> 0x00cd }
            org.jdom.Element r6 = r0.getRootElement()     // Catch:{ JDOMException -> 0x00d6, NullPointerException -> 0x00d3, Exception -> 0x00d0, all -> 0x00cd }
            java.lang.String r7 = "uploadPicDir"
            java.lang.String r7 = r6.getChildTextTrim(r7)     // Catch:{ JDOMException -> 0x00d6, NullPointerException -> 0x00d3, Exception -> 0x00d0, all -> 0x00cd }
            r9.upload_pic_dir = r7     // Catch:{ JDOMException -> 0x00d6, NullPointerException -> 0x00d3, Exception -> 0x00d0, all -> 0x00cd }
            java.lang.String r7 = "urlBase"
            java.lang.String r7 = r6.getChildTextTrim(r7)     // Catch:{ JDOMException -> 0x00d6, NullPointerException -> 0x00d3, Exception -> 0x00d0, all -> 0x00cd }
            r9.url_base = r7     // Catch:{ JDOMException -> 0x00d6, NullPointerException -> 0x00d3, Exception -> 0x00d0, all -> 0x00cd }
            java.lang.String r7 = "smtp"
            java.lang.String r7 = r6.getChildTextTrim(r7)     // Catch:{ JDOMException -> 0x00d6, NullPointerException -> 0x00d3, Exception -> 0x00d0, all -> 0x00cd }
            r9.smtp = r7     // Catch:{ JDOMException -> 0x00d6, NullPointerException -> 0x00d3, Exception -> 0x00d0, all -> 0x00cd }
            java.lang.String r7 = "smtpU"
            java.lang.String r7 = r6.getChildTextTrim(r7)     // Catch:{ JDOMException -> 0x00d6, NullPointerException -> 0x00d3, Exception -> 0x00d0, all -> 0x00cd }
            r9.smtpU = r7     // Catch:{ JDOMException -> 0x00d6, NullPointerException -> 0x00d3, Exception -> 0x00d0, all -> 0x00cd }
            java.lang.String r7 = "smtpP"
            java.lang.String r7 = r6.getChildTextTrim(r7)     // Catch:{ JDOMException -> 0x00d6, NullPointerException -> 0x00d3, Exception -> 0x00d0, all -> 0x00cd }
            r9.smtpP = r7     // Catch:{ JDOMException -> 0x00d6, NullPointerException -> 0x00d3, Exception -> 0x00d0, all -> 0x00cd }
            java.lang.String r7 = "masterMail"
            java.lang.String r7 = r6.getChildTextTrim(r7)     // Catch:{ JDOMException -> 0x00d6, NullPointerException -> 0x00d3, Exception -> 0x00d0, all -> 0x00cd }
            r9.masterMail = r7     // Catch:{ JDOMException -> 0x00d6, NullPointerException -> 0x00d3, Exception -> 0x00d0, all -> 0x00cd }
            java.lang.String r7 = "secWeb"
            java.lang.String r7 = r6.getChildTextTrim(r7)     // Catch:{ JDOMException -> 0x00d6, NullPointerException -> 0x00d3, Exception -> 0x00d0, all -> 0x00cd }
            r9.secWeb = r7     // Catch:{ JDOMException -> 0x00d6, NullPointerException -> 0x00d3, Exception -> 0x00d0, all -> 0x00cd }
            java.lang.String r7 = "picUrl"
            java.lang.String r7 = r6.getChildTextTrim(r7)     // Catch:{ JDOMException -> 0x00d6, NullPointerException -> 0x00d3, Exception -> 0x00d0, all -> 0x00cd }
            r9.picUrl = r7     // Catch:{ JDOMException -> 0x00d6, NullPointerException -> 0x00d3, Exception -> 0x00d0, all -> 0x00cd }
            r4.close()
            return
        L_0x0092:
            r2 = move-exception
        L_0x0093:
            org.apache.commons.logging.Log r7 = r9.log     // Catch:{ all -> 0x00a4 }
            java.lang.String r8 = r2.getMessage()     // Catch:{ all -> 0x00a4 }
            r7.error(r8)     // Catch:{ all -> 0x00a4 }
            java.lang.Exception r7 = new java.lang.Exception     // Catch:{ all -> 0x00a4 }
            java.lang.String r8 = "JDOM 出现异常"
            r7.<init>(r8)     // Catch:{ all -> 0x00a4 }
            throw r7     // Catch:{ all -> 0x00a4 }
        L_0x00a4:
            r7 = move-exception
        L_0x00a5:
            r3.close()
            throw r7
        L_0x00a9:
            r2 = move-exception
        L_0x00aa:
            org.apache.commons.logging.Log r7 = r9.log     // Catch:{ all -> 0x00a4 }
            java.lang.String r8 = r2.getMessage()     // Catch:{ all -> 0x00a4 }
            r7.error(r8)     // Catch:{ all -> 0x00a4 }
            java.lang.Exception r7 = new java.lang.Exception     // Catch:{ all -> 0x00a4 }
            java.lang.String r8 = "处理JDOM时出现异常"
            r7.<init>(r8)     // Catch:{ all -> 0x00a4 }
            throw r7     // Catch:{ all -> 0x00a4 }
        L_0x00bb:
            r2 = move-exception
        L_0x00bc:
            org.apache.commons.logging.Log r7 = r9.log     // Catch:{ all -> 0x00a4 }
            java.lang.String r8 = r2.getMessage()     // Catch:{ all -> 0x00a4 }
            r7.error(r8)     // Catch:{ all -> 0x00a4 }
            java.lang.Exception r7 = new java.lang.Exception     // Catch:{ all -> 0x00a4 }
            java.lang.String r8 = "处理JDOM时出现异常"
            r7.<init>(r8)     // Catch:{ all -> 0x00a4 }
            throw r7     // Catch:{ all -> 0x00a4 }
        L_0x00cd:
            r7 = move-exception
            r3 = r4
            goto L_0x00a5
        L_0x00d0:
            r2 = move-exception
            r3 = r4
            goto L_0x00bc
        L_0x00d3:
            r2 = move-exception
            r3 = r4
            goto L_0x00aa
        L_0x00d6:
            r2 = move-exception
            r3 = r4
            goto L_0x0093
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenfor.exutil.xmlConstant.iniXmlConstant():void");
    }

    public String getString(String element) throws Exception {
        this.realPath = new StringBuffer().append(this.path).append(this.sys_separator).append(this.filename).toString();
        try {
            this.in = new FileInputStream(this.realPath);
            String resultStr = new SAXBuilder().build(this.in).getRootElement().getChildTextTrim(element);
            this.in.close();
            return resultStr;
        } catch (JDOMException e) {
            throw new Exception("JDOM 出现异常");
        } catch (NullPointerException e2) {
            throw new Exception("处理JDOM时出现异常");
        } catch (Throwable th) {
            this.in.close();
            throw th;
        }
    }
}
