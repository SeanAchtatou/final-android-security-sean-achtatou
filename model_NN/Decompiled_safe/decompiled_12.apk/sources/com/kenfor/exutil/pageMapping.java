package com.kenfor.exutil;

import org.apache.struts.action.ActionMapping;

public class pageMapping extends ActionMapping {
    private String control_type;
    private String field_id_name;
    private String field_name;
    private String file_field_list;
    private String list_no;
    private String page_no;
    private String param_field_list;
    private String param_list;
    private String static_no;
    private String table_name;
    private String trade_version;

    public void setField_name(String field_name2) {
        this.field_name = field_name2;
    }

    public String getField_name() {
        return this.field_name;
    }

    public void setTrade_version(String newTrade_version) {
        this.trade_version = newTrade_version;
    }

    public String getTrade_version() {
        return this.trade_version;
    }

    public void setField_id_name(String id) {
        this.field_id_name = id;
    }

    public String getField_id_name() {
        return this.field_id_name;
    }

    public void setPage_no(String page_no2) {
        this.page_no = page_no2;
    }

    public String getPage_no() {
        return this.page_no;
    }

    public void setControl_type(String control_type2) {
        this.control_type = control_type2;
    }

    public String getControl_type() {
        return this.control_type;
    }

    public void setTable_name(String table_name2) {
        this.table_name = table_name2;
    }

    public String getTable_name() {
        return this.table_name;
    }

    public String getFile_field_list() {
        return this.file_field_list;
    }

    public void setFile_field_list(String file_field_list2) {
        this.file_field_list = file_field_list2;
    }

    public String getStatic_no() {
        return this.static_no;
    }

    public void setStatic_no(String static_no2) {
        this.static_no = static_no2;
    }

    public String getList_no() {
        return this.list_no;
    }

    public void setList_no(String list_no2) {
        this.list_no = list_no2;
    }

    public String getParam_list() {
        return this.param_list;
    }

    public void setParam_list(String param_list2) {
        this.param_list = param_list2;
    }

    public String getParam_field_list() {
        return this.param_field_list;
    }

    public void setParam_field_list(String param_field_list2) {
        this.param_field_list = param_field_list2;
    }
}
