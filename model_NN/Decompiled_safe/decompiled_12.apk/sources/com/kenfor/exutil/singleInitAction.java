package com.kenfor.exutil;

import javax.servlet.ServletContext;
import javax.servlet.jsp.PageContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionServlet;

public class singleInitAction {
    private static InitAction initAction = null;
    private static Log log = LogFactory.getLog("singleInitAction");

    private singleInitAction() {
    }

    public static synchronized InitAction getInitAction(ActionServlet servlet) {
        InitAction initAction2;
        synchronized (singleInitAction.class) {
            if (initAction == null) {
                if (log.isInfoEnabled()) {
                    log.info("initAction is null,get it ");
                }
                initAction = new InitAction(servlet);
            }
            initAction2 = initAction;
        }
        return initAction2;
    }

    public static synchronized InitAction getInitAction(PageContext pageContext) {
        InitAction initAction2;
        synchronized (singleInitAction.class) {
            if (initAction == null) {
                if (log.isInfoEnabled()) {
                    log.info("initAction is null,get it ");
                }
                initAction = new InitAction(pageContext);
            }
            initAction2 = initAction;
        }
        return initAction2;
    }

    public static synchronized InitAction getInitAction(ServletContext servletContext) {
        InitAction initAction2;
        synchronized (singleInitAction.class) {
            if (initAction == null) {
                if (log.isInfoEnabled()) {
                    log.info("initAction is null,get it ");
                }
                initAction = new InitAction(servletContext);
            }
            initAction2 = initAction;
        }
        return initAction2;
    }
}
