package com.kenfor.exutil;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Properties;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.dialect.Dialect;
import net.sf.hibernate.engine.SessionImplementor;
import net.sf.hibernate.id.Configurable;
import net.sf.hibernate.id.IdentifierGenerator;
import net.sf.hibernate.type.Type;

public class idGenerator implements IdentifierGenerator, Configurable {
    static Class class$java$lang$Long;
    private Class returnClass;

    public void configure(Type type, Properties params, Dialect dialect) {
        this.returnClass = type.getReturnedClass();
    }

    public Serializable generate(SessionImplementor parm1, Object parm2) throws SQLException, HibernateException {
        Class cls;
        long result = (1000 * System.currentTimeMillis()) + ((long) ((int) ((Math.random() * 1000.0d) + 1.0d)));
        Class cls2 = this.returnClass;
        if (class$java$lang$Long == null) {
            cls = class$("java.lang.Long");
            class$java$lang$Long = cls;
        } else {
            cls = class$java$lang$Long;
        }
        if (cls2 == cls) {
            return new Long(result);
        }
        if (this.returnClass.getName() == "java.lang.String") {
            return String.valueOf(result);
        }
        throw new HibernateException("this id generator generates long, string only.");
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError(x1.getMessage());
        }
    }
}
