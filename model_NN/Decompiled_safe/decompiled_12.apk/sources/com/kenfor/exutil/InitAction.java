package com.kenfor.exutil;

import com.kenfor.database.PoolBean;
import com.kenfor.util.MyUtil;
import java.sql.Connection;
import java.sql.SQLException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;
import javax.sql.DataSource;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.SessionFactory;
import net.sf.hibernate.cfg.Configuration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionServlet;

public class InitAction {
    protected Connection con;
    private Context ctx;
    protected ActionErrors errors;
    private String jndi_name;
    Log log;
    protected String path;
    protected PoolBean pool;
    String poolType;
    protected String realPath;
    private SessionFactory sessionFactory;
    String sys_name;

    public void releaseInit() {
        this.pool.realsePool();
        this.pool = null;
        this.con = null;
        this.path = null;
        this.realPath = null;
        this.errors.clear();
        this.errors = null;
    }

    public void setCon(Connection newCon) {
        this.con = newCon;
    }

    public Connection getCon() throws SQLException {
        if (this.pool == null) {
            return null;
        }
        this.con = this.pool.getConnection();
        return this.con;
    }

    public void setPool(PoolBean newPool) {
        this.pool = newPool;
    }

    public PoolBean getPool() {
        return this.pool;
    }

    public void setPath(String newPath) {
        this.path = newPath;
    }

    public String getPath() {
        return this.path;
    }

    public void setRealPath(String newRealPath) {
        this.realPath = newRealPath;
    }

    public String getRealPath() {
        return this.realPath;
    }

    public void setErrors(ActionErrors newErrors) {
        this.errors = newErrors;
    }

    public ActionErrors getErrors() {
        return this.errors;
    }

    public String getTrade_id() {
        return this.pool.getTrade_id();
    }

    public InitAction(ServletContext servletContext) {
        this.con = null;
        this.pool = null;
        this.path = null;
        this.ctx = null;
        this.sys_name = null;
        this.poolType = null;
        this.realPath = "/WEB-INF/classes/";
        this.errors = null;
        this.log = LogFactory.getLog(getClass().getName());
        this.errors = new ActionErrors();
        try {
            this.path = servletContext.getRealPath("/WEB-INF/classes/");
            this.pool = (PoolBean) servletContext.getAttribute("pool");
            if (this.pool == null) {
                this.log.info("pool is null ,initDB");
                initDB(servletContext);
                return;
            }
            this.log.info("pool is not null,return ");
        } catch (Exception e) {
            this.log.error(e.getMessage());
            this.errors.add("org.apache.struts.action.GLOBAL_ERROR", new ActionError("error.blank", "初始化有误，请重试"));
        }
    }

    public InitAction(PageContext pageContext) {
        this.con = null;
        this.pool = null;
        this.path = null;
        this.ctx = null;
        this.sys_name = null;
        this.poolType = null;
        this.realPath = "/WEB-INF/classes/";
        this.errors = null;
        this.log = LogFactory.getLog(getClass().getName());
        this.errors = new ActionErrors();
        try {
            this.path = pageContext.getRequest().getRealPath("/WEB-INF/classes/");
            this.pool = (PoolBean) pageContext.getServletContext().getAttribute("pool");
            if (this.pool == null) {
                initDB(pageContext.getServletContext());
            }
        } catch (Exception e) {
            this.log.error(e.getMessage());
        }
    }

    public InitAction(ActionServlet servlet) {
        this.con = null;
        this.pool = null;
        this.path = null;
        this.ctx = null;
        this.sys_name = null;
        this.poolType = null;
        this.realPath = "/WEB-INF/classes/";
        this.errors = null;
        this.log = LogFactory.getLog(getClass().getName());
        this.errors = new ActionErrors();
        try {
            this.path = servlet.getServletContext().getRealPath("/WEB-INF/classes/");
            this.pool = (PoolBean) servlet.getServletContext().getAttribute("pool");
            if (this.pool == null) {
                initDB(servlet.getServletContext());
            }
        } catch (Exception e) {
            this.log.error(e.getMessage());
            this.errors.add("org.apache.struts.action.GLOBAL_ERROR", new ActionError("error.blank", "初始化有误，请重试"));
        }
    }

    public synchronized boolean initDB(ServletContext servletContext) {
        boolean z;
        if (this.log.isInfoEnabled()) {
            this.log.info("start initDB");
        }
        String path2 = servletContext.getRealPath("/WEB-INF/classes/");
        this.pool = (PoolBean) servletContext.getAttribute("pool");
        if (this.pool == null) {
            this.pool = new PoolBean();
            String t_id = (String) servletContext.getAttribute("trade_id");
            String webapp_config = (String) servletContext.getAttribute("webapp_config");
            this.jndi_name = (String) servletContext.getAttribute("jndi_name");
            String initHiberante = (String) servletContext.getAttribute("initHiberante");
            String h_config_name = (String) servletContext.getAttribute("h_config_name");
            this.poolType = (String) servletContext.getAttribute("poolType");
            String poolName = (String) servletContext.getAttribute("poolName");
            String html_flag = (String) servletContext.getAttribute("html_flag");
            String systemName = (String) servletContext.getAttribute("systemName");
            String version = (String) servletContext.getAttribute("version");
            if (this.log.isInfoEnabled()) {
                this.log.info(new StringBuffer().append("version:").append(version).toString());
            }
            int pool_type = MyUtil.getStringToInt(this.poolType, 1);
            if (this.log.isDebugEnabled()) {
                this.log.debug(new StringBuffer().append("pool_type:").append(pool_type).toString());
            }
            this.pool.setTrade_id(t_id);
            if (this.log.isDebugEnabled()) {
                this.log.debug(new StringBuffer().append("set trade_id at initDatabase,trade_id:").append(t_id).toString());
            }
            if ("true".equalsIgnoreCase(webapp_config)) {
                try {
                    Context ctx2 = (Context) new InitialContext().lookup("java:comp/env");
                    if (ctx2 == null) {
                        throw new Exception("Boom - No Context");
                    }
                    DataSource ds = (DataSource) ctx2.lookup(this.jndi_name);
                    if (ds == null) {
                        this.log.error("get datasource is null");
                        throw new ServletException("get datasource is null");
                    }
                    this.pool.setBDS(ds);
                    this.log.info("set datasource to pool at initDatabase");
                } catch (Exception e) {
                    this.log.error(new StringBuffer().append("get datasource occur exception").append(e.getMessage()).toString());
                    z = false;
                }
            } else {
                try {
                    if (!this.pool.isStarted()) {
                        this.pool.setPath(path2);
                        if (poolName != null) {
                            this.pool.setPoolName(poolName);
                        }
                        this.pool.setPoolType(pool_type);
                        this.pool.setVersion(version);
                        if (html_flag != null) {
                            this.pool.setHtml_flag(html_flag);
                        }
                        if (systemName != null) {
                            this.pool.setSystemName(systemName);
                        }
                        this.pool.setJndiName(this.jndi_name);
                        this.pool.initializePool();
                        this.log.info("init pool ok");
                    }
                } catch (Exception e2) {
                    this.log.error(e2.getMessage());
                    z = false;
                }
            }
            servletContext.setAttribute("pool", this.pool);
            if ("true".equalsIgnoreCase(initHiberante)) {
                initHibernate(h_config_name);
            }
            z = true;
        } else {
            if (this.log.isInfoEnabled()) {
                this.log.info("pool is not null,return");
            }
            z = true;
        }
        return z;
    }

    public void initHibernate(String h_config_name) {
        if (this.log.isInfoEnabled()) {
            this.log.info(new StringBuffer().append("start config hibernate,h_config_name:").append(h_config_name).toString());
        }
        if (h_config_name != null) {
            try {
                this.sessionFactory = new Configuration().configure(h_config_name).buildSessionFactory();
            } catch (HibernateException ex) {
                this.log.error(new StringBuffer().append("HibernateException building SessionFactory: ").append(ex.getMessage()).toString());
                throw new RuntimeException(new StringBuffer().append("HibernateException building SessionFactory: ").append(ex.getMessage()).toString(), ex);
            } catch (Exception e) {
                this.log.error(new StringBuffer().append("Exception building SessionFactory: ").append(e.getMessage()).toString());
                throw new RuntimeException(new StringBuffer().append("Exception building SessionFactory: ").append(e.getMessage()).toString());
            }
        } else {
            this.sessionFactory = new Configuration().configure().buildSessionFactory();
        }
        if (this.log.isDebugEnabled()) {
            this.log.debug("end config hibernate");
        }
        try {
            this.ctx = new InitialContext();
            if (this.log.isInfoEnabled()) {
                this.log.info("bind sessionFactory to context by :HibSessionFactory");
            }
            this.ctx.rebind("HibSessionFactory", this.sessionFactory);
        } catch (NamingException ex2) {
            this.log.error(new StringBuffer().append("Exception binding SessionFactory to JNDI: ").append(ex2.getMessage()).toString());
            throw new RuntimeException(new StringBuffer().append("Exception binding SessionFactory to JNDI: ").append(ex2.getMessage()).toString(), ex2);
        }
    }
}
