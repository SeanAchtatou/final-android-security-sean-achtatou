package com.kenfor.exutil;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.SessionFactory;
import net.sf.hibernate.cfg.Configuration;

public class InitHibernate extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=GBK";
    private Context ctx;
    private SessionFactory sessionFactory;

    public void InitHibernate() {
    }

    public void init() throws ServletException {
        try {
            this.sessionFactory = new Configuration().configure(new xmlConstant(getServletContext().getRealPath("/WEB-INF/classes/"), "pro-config.xml", false).getString("hibernateName")).buildSessionFactory();
            try {
                this.ctx = new InitialContext();
                this.ctx.bind("HibernateSessionFactory", this.sessionFactory);
            } catch (NamingException ex) {
                throw new RuntimeException(new StringBuffer().append("Exception binding SessionFactory to JNDI: ").append(ex.getMessage()).toString(), ex);
            }
        } catch (HibernateException ex2) {
            throw new RuntimeException(new StringBuffer().append("Exception building SessionFactory: ").append(ex2.getMessage()).toString(), ex2);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public void destroy() {
        if (this.ctx != null) {
            try {
                this.ctx.unbind("HibernateSessionFactory");
            } catch (NamingException e) {
                e.printStackTrace();
            }
        }
        if (this.sessionFactory != null) {
            try {
                this.sessionFactory.close();
            } catch (HibernateException e2) {
                e2.printStackTrace();
            }
            this.sessionFactory = null;
        }
    }
}
