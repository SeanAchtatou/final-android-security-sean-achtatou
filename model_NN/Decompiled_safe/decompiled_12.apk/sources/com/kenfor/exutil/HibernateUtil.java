package com.kenfor.exutil;

import net.sf.hibernate.HibernateException;
import net.sf.hibernate.Session;
import net.sf.hibernate.SessionFactory;
import net.sf.hibernate.cfg.Configuration;

public class HibernateUtil {
    public static final ThreadLocal session = new ThreadLocal();
    private static final SessionFactory sessionFactory;

    static {
        try {
            sessionFactory = new Configuration().configure().buildSessionFactory();
        } catch (HibernateException ex) {
            throw new RuntimeException(new StringBuffer().append("Exception building SessionFactory: ").append(ex.getMessage()).toString(), ex);
        }
    }

    public static Session currentSession() throws HibernateException {
        Session s = (Session) session.get();
        if (s != null) {
            return s;
        }
        Session s2 = sessionFactory.openSession();
        session.set(s2);
        return s2;
    }

    public static void closeSession() throws HibernateException {
        Session s = (Session) session.get();
        session.set(null);
        if (s != null) {
            s.close();
        }
    }
}
