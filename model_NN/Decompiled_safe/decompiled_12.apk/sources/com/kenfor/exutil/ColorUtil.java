package com.kenfor.exutil;

import java.awt.Color;

public class ColorUtil {
    public static Color getColor(String color) {
        String temp = color;
        int pos = temp.indexOf("#");
        int len = temp.length();
        if (len != 7 && len != 6) {
            return null;
        }
        if (pos == 0) {
            temp = temp.substring(1);
        }
        return new Color(HexConverter.getIntValue(temp.substring(0, 2)), HexConverter.getIntValue(temp.substring(2, 4)), HexConverter.getIntValue(temp.substring(4)));
    }
}
