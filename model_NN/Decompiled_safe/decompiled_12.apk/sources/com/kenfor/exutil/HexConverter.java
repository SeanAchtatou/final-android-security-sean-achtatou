package com.kenfor.exutil;

public class HexConverter {
    public static void main(String[] args) {
        new HexConverter();
        System.out.println(new StringBuffer().append("1f").append(" = ").append(String.valueOf(scanhex("1f"))).toString());
    }

    public static int scanhex(String s) {
        new HexConverter();
        return getIntValue(s);
    }

    public static int getIntValue(String hex) {
        int value = 0;
        int power = 1;
        for (int i = hex.length() - 1; i >= 0; i--) {
            if (hexToInteger(hex.charAt(i)) != -1) {
                value += hexToInteger(hex.charAt(i)) * power;
            }
            power *= 16;
        }
        return value;
    }

    public static long getLongValue(String hex) {
        long value = 0;
        int power = 1;
        for (int i = hex.length() - 1; i >= 0; i--) {
            if (hexToInteger(hex.charAt(i)) != -1) {
                value += (long) (hexToInteger(hex.charAt(i)) * power);
            }
            power *= 16;
        }
        return value;
    }

    protected static boolean isAHexCharacter(char c) {
        String s = String.valueOf(c).toUpperCase();
        if (c == '0' || c == '1' || c == '2' || c == '3' || c == '4' || c == '5' || c == '6' || c == '7' || c == '8' || c == '9' || c == 'a' || c == 'A' || c == 'b' || c == 'B' || c == 'c' || c == 'C' || c == 'd' || c == 'D' || c == 'e' || c == 'E' || c == 'f' || c == 'F') {
            return true;
        }
        return false;
    }

    protected static int hexToInteger(char c) {
        int i = -1;
        if (c == '0') {
            i = 0;
        }
        if (c == '1') {
            i = 1;
        }
        if (c == '2') {
            i = 2;
        }
        if (c == '3') {
            i = 3;
        }
        if (c == '4') {
            i = 4;
        }
        if (c == '5') {
            i = 5;
        }
        if (c == '6') {
            i = 6;
        }
        if (c == '7') {
            i = 7;
        }
        if (c == '8') {
            i = 8;
        }
        if (c == '9') {
            i = 9;
        }
        if (c == 'A') {
            i = 10;
        }
        if (c == 'a') {
            i = 10;
        }
        if (c == 'B') {
            i = 11;
        }
        if (c == 'b') {
            i = 11;
        }
        if (c == 'C') {
            i = 12;
        }
        if (c == 'c') {
            i = 12;
        }
        if (c == 'D') {
            i = 13;
        }
        if (c == 'd') {
            i = 13;
        }
        if (c == 'E') {
            i = 14;
        }
        if (c == 'e') {
            i = 14;
        }
        if (c == 'F') {
            i = 15;
        }
        if (c == 'f') {
            return 15;
        }
        return i;
    }
}
