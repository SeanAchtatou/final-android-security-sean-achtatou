package com.kenfor.exutil;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.Session;
import net.sf.hibernate.SessionFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class HibernateUtilPlus {
    static Class class$com$kenfor$exutil$HibernateUtilPlus;
    private static Log log;
    public static final ThreadLocal session = new ThreadLocal();
    private static SessionFactory sessionFactory = null;
    static String sys_name = null;

    static {
        Class cls;
        if (class$com$kenfor$exutil$HibernateUtilPlus == null) {
            cls = class$("com.kenfor.exutil.HibernateUtilPlus");
            class$com$kenfor$exutil$HibernateUtilPlus = cls;
        } else {
            cls = class$com$kenfor$exutil$HibernateUtilPlus;
        }
        log = LogFactory.getLog(cls.getName());
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError(x1.getMessage());
        }
    }

    public static Session currentSession(ServletContext servletContext) throws HibernateException {
        log.info("start currentSession");
        if (sessionFactory != null || getSystemSessionFactory(servletContext)) {
            log.info("start get session");
            Session s = (Session) session.get();
            log.info("get session ok");
            if (s != null) {
                return s;
            }
            log.info("start set session");
            Session s2 = sessionFactory.openSession();
            log.info(new StringBuffer().append("session :").append(s2).toString());
            session.set(s2);
            log.info("set session ok");
            return s2;
        }
        throw new HibernateException("Exception geting SessionFactory from JNDI ");
    }

    public static Session currentSession(String t_sys_name, ServletContext servletContext) throws HibernateException {
        sys_name = t_sys_name;
        return currentSession(servletContext);
    }

    public static void closeSession() throws HibernateException {
        Session s = (Session) session.get();
        session.set(null);
        if (s != null) {
            s.close();
        }
    }

    private static boolean getSystemSessionFactory(ServletContext servletContext) {
        log.info("star getSystemSessionFactory");
        if (sessionFactory == null) {
            log.info("star InitAction");
            try {
                Context inttex = new InitialContext();
                if (log.isDebugEnabled()) {
                    log.debug("get sessionFactory to context by :HibSessionFactory");
                }
                log.info(new StringBuffer().append("inttex:").append(inttex).toString());
                sessionFactory = (SessionFactory) inttex.lookup("HibSessionFactory");
                log.info("get sessioinFactory ok");
            } catch (NamingException e) {
                return false;
            }
        }
        return true;
    }
}
