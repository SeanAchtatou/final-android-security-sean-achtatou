package com.kenfor.exutil;

import com.kenfor.coolmenu.menu.displayer.MenuDisplayer;

public class htmlFilter {
    private static String filter_block_key = "<script <style";
    private static String filter_line_key = "<table </table <span </span <font </font  <tr </tr <div </div <tbody </tbody <td </td <a </a <img </img ";

    public static String filter(String initString, int length, boolean skidbr) {
        if (initString == null) {
            return null;
        }
        String resultStr = "";
        char[] str_chars = initString.toCharArray();
        String temp_str = "";
        int cur_pos = 0;
        int temp_len = 0;
        boolean is_start_map = false;
        int blank_len = 0;
        int i = 0;
        while (i < str_chars.length && resultStr.length() < length + blank_len) {
            if (!(str_chars[i] == 10 || str_chars[i] == 9 || str_chars[i] == 13)) {
                if (str_chars[i] == '<' || str_chars[i] == '&' || is_start_map) {
                    if (!(str_chars[i] == '>' || str_chars[i] == ' ' || temp_len >= 20)) {
                        temp_str = new StringBuffer().append(temp_str).append(str_chars[i]).toString();
                        temp_len++;
                        is_start_map = true;
                    }
                    if (str_chars[i] == '>') {
                        if (skidbr) {
                            if ("<br".equalsIgnoreCase(temp_str)) {
                                resultStr = new StringBuffer().append(resultStr).append("<br>").toString();
                            }
                            if ("<p".equalsIgnoreCase(temp_str)) {
                                resultStr = new StringBuffer().append(resultStr).append("<p>").toString();
                            }
                            if ("</p".equalsIgnoreCase(temp_str)) {
                                resultStr = new StringBuffer().append(resultStr).append("</p>").toString();
                            }
                        }
                        temp_str = "";
                        temp_len = 0;
                        is_start_map = false;
                    }
                    if (str_chars[i] != ';') {
                        int l_pos = -1;
                        int b_pos = -1;
                        if (str_chars[i] == ' ' && temp_len < 20) {
                            blank_len++;
                            l_pos = filter_line_key.indexOf(temp_str.toLowerCase());
                            b_pos = filter_block_key.indexOf(temp_str.toLowerCase());
                            if (l_pos == -1 && b_pos == -1) {
                                resultStr = new StringBuffer().append(resultStr).append(temp_str).toString();
                                temp_str = "";
                                temp_len = 0;
                                is_start_map = false;
                            } else {
                                is_start_map = false;
                            }
                        }
                        if (l_pos >= 0) {
                            int t_pos = initString.indexOf(">", i);
                            if (t_pos >= 0) {
                                i = t_pos;
                                is_start_map = false;
                                temp_str = "";
                                temp_len = 0;
                            }
                        } else if (b_pos >= 0) {
                            String t_str = temp_str.replaceAll("<", "</");
                            int t_pos2 = initString.indexOf(t_str.trim());
                            if (t_pos2 >= 0) {
                                i = t_pos2 + t_str.length();
                                is_start_map = false;
                                temp_str = "";
                                temp_len = 0;
                            }
                        }
                    } else if (filter_line_key.indexOf(temp_str.toLowerCase()) > 0) {
                        temp_str = "";
                        temp_len = 0;
                        is_start_map = false;
                    } else {
                        if (MenuDisplayer.NBSP.equals(temp_str)) {
                            resultStr = new StringBuffer().append(resultStr).append(" ").toString();
                        }
                        temp_str = "";
                        is_start_map = false;
                        temp_len = 0;
                    }
                } else {
                    if (str_chars[i] == ' ') {
                        blank_len++;
                    }
                    resultStr = new StringBuffer().append(resultStr).append(str_chars[i]).toString();
                    cur_pos++;
                    is_start_map = false;
                    temp_len = 0;
                }
            }
            i++;
        }
        if (length == -1 || resultStr == null || i >= str_chars.length) {
            return resultStr;
        }
        return new StringBuffer().append(resultStr).append("...").toString();
    }

    public static String filter(String initString, int length) {
        return filter(initString, length, true);
    }

    public static String filterNewLine(String initString) {
        char[] str_chars = initString.toCharArray();
        StringBuffer str_buf = new StringBuffer();
        for (int i = 0; i < str_chars.length; i++) {
            if (!(str_chars[i] == 10 || str_chars[i] == 9 || str_chars[i] == 13)) {
                if (str_chars[i] == '\'') {
                    str_buf.append("`");
                } else {
                    str_buf.append(str_chars[i]);
                }
            }
        }
        return str_buf.toString();
    }
}
