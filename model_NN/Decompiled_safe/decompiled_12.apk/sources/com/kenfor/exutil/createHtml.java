package com.kenfor.exutil;

import com.kenfor.database.PoolBean;
import com.kenfor.taglib.db.dbPresentTag;
import com.kenfor.util.CloseCon;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionServlet;

public class createHtml {
    protected String TABLE_NAME = "bill_static_info_v";
    protected String error_mes = null;
    protected String file_name = null;
    protected String file_path = null;
    protected boolean get_old_path_ok = false;
    protected String html_url_base = null;
    private boolean isDebug = false;
    Log log = LogFactory.getLog(getClass().getName());
    protected String path = null;
    protected PoolBean pool = null;

    public String getError_mes() {
        return this.error_mes;
    }

    public boolean doCreate(ActionServlet servlet, String html_no) {
        return doCreate(servlet, html_no, (HashMap) null, 1);
    }

    public boolean doCreate(ActionServlet servlet, String html_no, HashMap paramMap) {
        return doCreate(servlet, html_no, paramMap, 1);
    }

    public boolean doCreate(ActionServlet servlet, String html_no, HashMap paramMap, int create_type) {
        if (this.log.isDebugEnabled()) {
            this.log.debug("start init doCreate");
        }
        try {
            this.path = servlet.getServletContext().getRealPath("/WEB-INF/classes/");
            this.pool = (PoolBean) servlet.getServletContext().getAttribute("pool");
            if (this.pool == null || !this.pool.isStarted()) {
                this.log.error("pool is null or did not started");
            }
            if ("1".equals(this.pool.getHtml_flag())) {
                return doCreate(this.pool, html_no, paramMap, create_type);
            }
            if (this.log.isDebugEnabled()) {
                this.log.debug("无需产生静态文件，返回");
            }
            return true;
        } catch (Exception e) {
            this.error_mes = "初始化路径出错";
            return false;
        }
    }

    private String getOldPath(PoolBean pool2, String get_sql) {
        String result = null;
        try {
            Connection con = pool2.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(get_sql);
            if (rs.next()) {
                result = rs.getString(1);
            }
            rs.close();
            stmt.close();
            con.close();
            CloseCon.Close(con);
            return result;
        } catch (SQLException e) {
            this.error_mes = "获取原路径时出错";
            CloseCon.Close(null);
            return null;
        } catch (Throwable th) {
            CloseCon.Close(null);
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public boolean doCreate(PoolBean pool2, String html_no, HashMap paramMap, int create_type) {
        if (this.log.isInfoEnabled()) {
            this.log.info("start");
        }
        Connection con_t = null;
        if (pool2 == null || html_no == null || html_no.trim().length() < 1) {
            return false;
        }
        String str_sql = new StringBuffer().append("select * from ").append(this.TABLE_NAME).append(" where bas_no = '").append(html_no.trim()).append("' order by order_num").toString();
        if (this.log.isInfoEnabled()) {
            this.log.info(new StringBuffer().append("str_sql:").append(str_sql).toString());
        }
        ArrayList staticList = new ArrayList();
        try {
            con_t = pool2.getConnection();
            Statement stmt = con_t.createStatement();
            ResultSet rs = stmt.executeQuery(str_sql);
            while (rs.next()) {
                String str_url = strTrim(rs.getString("url"));
                this.file_path = strTrim(rs.getString("file_path"));
                this.file_name = strTrim(rs.getString("file_name"));
                String charset = strTrim(rs.getString("charset"));
                String param_list = strTrim(rs.getString("param_list"));
                int is_list = rs.getInt("is_list");
                this.html_url_base = strTrim(rs.getString("html_url_base"));
                String update_sql = strTrim(rs.getString("update_sql"));
                String get_sql = strTrim(rs.getString("get_sql"));
                int is_need_update = rs.getInt("is_need_update");
                int is_del_update = rs.getInt("is_del_update");
                HashMap staticMap = new HashMap();
                staticMap.put("str_url", str_url);
                staticMap.put("file_path", this.file_path);
                staticMap.put("file_name", this.file_name);
                staticMap.put("charset", charset);
                staticMap.put("param_list", param_list);
                staticMap.put("is_list", String.valueOf(is_list));
                staticMap.put("html_url_base", this.html_url_base);
                staticMap.put("update_sql", update_sql);
                staticMap.put("get_sql", get_sql);
                staticMap.put("is_need_update", String.valueOf(is_need_update));
                staticMap.put("is_del_update", String.valueOf(is_del_update));
                staticList.add(staticMap);
            }
            rs.close();
            stmt.close();
            CloseCon.Close(con_t);
            CloseCon.Close(con_t);
            if (this.log.isInfoEnabled()) {
                this.log.info(new StringBuffer().append("staticList.size():").append(staticList.size()).toString());
            }
            for (int i = 0; i < staticList.size(); i++) {
                try {
                    HashMap staticMap2 = (HashMap) staticList.get(i);
                    if (this.log.isInfoEnabled()) {
                        this.log.info(new StringBuffer().append("start doRealCreate :").append(staticMap2).toString());
                        this.log.info(paramMap.toString());
                    }
                    if (!doRealCreate(pool2, staticMap2, paramMap, create_type) && this.log.isInfoEnabled()) {
                        this.log.info(this.error_mes);
                    }
                } catch (Exception e) {
                    this.log.error(e.getMessage());
                }
            }
            clearList(staticList);
            try {
                Thread.sleep(100);
            } catch (Exception e2) {
                this.log.error("Thread.sleep at createHtml occur exception");
            }
            return true;
        } catch (Exception e3) {
            CloseCon.Close(con_t);
            this.error_mes = new StringBuffer().append("数据库操作出错，信息：").append(e3.getMessage()).toString();
            this.log.error(this.error_mes);
            this.log.error(str_sql);
            CloseCon.Close(con_t);
            return false;
        } catch (Throwable th) {
            CloseCon.Close(con_t);
            throw th;
        }
    }

    private void clearList(ArrayList list) {
        if (list != null && list.size() > 0) {
            Iterator iterator = list.iterator();
            if (iterator.hasNext()) {
                ((HashMap) iterator.next()).clear();
                iterator.remove();
            }
        }
    }

    private boolean doRealCreate(PoolBean pool2, HashMap staticMap, HashMap paramMap, int create_type) {
        boolean z;
        String update_sql;
        if (this.log.isInfoEnabled()) {
            this.log.info("start doRealCreate");
        }
        if (staticMap == null) {
            return false;
        }
        if ((0 == 1 && 0 == 0) || (create_type == 2 && 0 == 0)) {
            z = true;
            staticMap.clear();
        } else {
            try {
                String str_url = (String) staticMap.get("str_url");
                this.html_url_base = (String) staticMap.get("html_url_base");
                this.file_name = (String) staticMap.get("file_name");
                this.file_path = (String) staticMap.get("file_path");
                String update_sql2 = (String) staticMap.get("update_sql");
                String param_list = (String) staticMap.get("param_list");
                String charset = (String) staticMap.get("charset");
                int is_list = getIntValue((String) staticMap.get("is_list"), 0);
                String get_sql = (String) staticMap.get("get_sql");
                int is_need_update = getIntValue((String) staticMap.get("is_need_update"), 0);
                int is_del_update = getIntValue((String) staticMap.get("is_del_update"), 0);
                if (this.log.isInfoEnabled()) {
                    this.log.info("get value from staticMap ok");
                }
                String get_sql2 = getRealValue(get_sql, paramMap);
                boolean need_update = true;
                if (create_type == 3) {
                    if (this.log.isDebugEnabled()) {
                        this.log.debug(get_sql2);
                    }
                    String t_file_path = null;
                    if (is_list != 1) {
                        t_file_path = getOldPath(pool2, get_sql2);
                    }
                    if (this.log.isDebugEnabled()) {
                        this.log.debug(t_file_path);
                    }
                    if (t_file_path == null || t_file_path.length() < 1) {
                        this.get_old_path_ok = false;
                        t_file_path = getRealValue(this.file_path, paramMap);
                        need_update = true;
                    } else {
                        this.get_old_path_ok = true;
                        need_update = false;
                    }
                    if (this.log.isDebugEnabled()) {
                        this.log.debug(t_file_path);
                    }
                    this.file_path = t_file_path;
                } else {
                    this.file_path = getRealValue(this.file_path, paramMap);
                }
                if (this.log.isDebugEnabled()) {
                    this.log.debug(this.file_path);
                    this.log.debug(new StringBuffer().append("need_update:").append(need_update).toString());
                }
                this.html_url_base = getRealValue(this.html_url_base, paramMap);
                if (need_update) {
                    this.file_name = getRealValue(this.file_name, paramMap);
                }
                if (need_update) {
                    update_sql = getRealValue(update_sql2, paramMap);
                    if (this.log.isDebugEnabled()) {
                        this.log.debug(new StringBuffer().append("update_sql:").append(update_sql).toString());
                    }
                } else {
                    update_sql = getUpdateSql(update_sql2, paramMap);
                }
                if (this.log.isDebugEnabled()) {
                    this.log.debug(update_sql);
                }
                String str_url2 = getRealURL(str_url, param_list, paramMap);
                if (str_url2 == null) {
                    z = false;
                    staticMap.clear();
                } else {
                    if (this.log.isInfoEnabled()) {
                        this.log.info("start create html at createHtml");
                        this.log.info(new StringBuffer().append("str_url：").append(str_url2).toString());
                        this.log.info(new StringBuffer().append("file_path：").append(this.file_path).toString());
                        this.log.info(new StringBuffer().append("file_name：").append(this.file_name).toString());
                        this.log.info(new StringBuffer().append("is_list：").append(is_list).toString());
                    }
                    doCreateHtml(str_url2, this.file_path, this.file_name, charset, is_list, create_type, need_update);
                    if (this.log.isInfoEnabled()) {
                        this.log.info("end create html at createHtml");
                        this.log.info(new StringBuffer().append("update_sql:").append(update_sql).append(",need_update:").append(need_update).toString());
                    }
                    if (update_sql != null && update_sql.length() > 0) {
                        try {
                            if (this.log.isInfoEnabled()) {
                                this.log.info("start update_sql at createHtml");
                            }
                            Connection con = pool2.getConnection();
                            Statement stmt2 = con.createStatement();
                            stmt2.execute(update_sql);
                            stmt2.close();
                            if (this.log.isInfoEnabled()) {
                                this.log.info("end update_sql at createHtml");
                            }
                            con.close();
                            CloseCon.Close(con);
                        } catch (SQLException ee) {
                            CloseCon.Close(null);
                            this.error_mes = new StringBuffer().append("更新状态时出错，信息：").append(ee.getMessage()).toString();
                            this.log.error(new StringBuffer().append("SQLException at createHtml--").append(this.error_mes).toString());
                            this.log.error(new StringBuffer().append("SQLException at createHtml--").append(update_sql).toString());
                            z = false;
                            CloseCon.Close(null);
                            staticMap.clear();
                        } catch (Throwable th) {
                            CloseCon.Close(null);
                            throw th;
                        }
                    }
                    staticMap.clear();
                    return true;
                }
            } catch (Exception e2) {
                this.error_mes = new StringBuffer().append("产生静态文件时出错，信息：").append(e2.getMessage()).toString();
                this.log.error(new StringBuffer().append("Exception at createHtml--").append(this.error_mes).toString());
                z = false;
                staticMap.clear();
            } catch (Throwable th2) {
                staticMap.clear();
                throw th2;
            }
        }
        return z;
    }

    /* JADX WARNING: Removed duplicated region for block: B:120:0x0363 A[SYNTHETIC, Splitter:B:120:0x0363] */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x0368 A[Catch:{ Exception -> 0x037b }] */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x036d A[Catch:{ Exception -> 0x037b }] */
    /* JADX WARNING: Removed duplicated region for block: B:127:0x0372 A[Catch:{ Exception -> 0x037b }] */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x038e A[SYNTHETIC, Splitter:B:133:0x038e] */
    /* JADX WARNING: Removed duplicated region for block: B:136:0x0393 A[Catch:{ Exception -> 0x03a5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:138:0x0398 A[Catch:{ Exception -> 0x03a5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:140:0x039d A[Catch:{ Exception -> 0x03a5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:165:0x0110 A[EDGE_INSN: B:165:0x0110->B:37:0x0110 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:174:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00b7 A[Catch:{ IOException -> 0x0353 }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00ff A[Catch:{ IOException -> 0x03f7, all -> 0x03e0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0127 A[SYNTHETIC, Splitter:B:42:0x0127] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x012c A[Catch:{ Exception -> 0x0343 }] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0131 A[Catch:{ Exception -> 0x0343 }] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0136 A[Catch:{ Exception -> 0x0343 }] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0149  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x01b3  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x01d0  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x01e2  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x023a  */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x02b3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void doCreateHtml(java.lang.String r34, java.lang.String r35, java.lang.String r36, java.lang.String r37, int r38, int r39, boolean r40) {
        /*
            r33 = this;
            r0 = r33
            org.apache.commons.logging.Log r0 = r0.log
            r30 = r0
            boolean r30 = r30.isInfoEnabled()
            if (r30 == 0) goto L_0x003c
            r0 = r33
            org.apache.commons.logging.Log r0 = r0.log
            r30 = r0
            java.lang.StringBuffer r31 = new java.lang.StringBuffer
            r31.<init>()
            java.lang.String r32 = "start doCreateHtml 1 ,create_type:"
            java.lang.StringBuffer r31 = r31.append(r32)
            r0 = r31
            r1 = r39
            java.lang.StringBuffer r31 = r0.append(r1)
            java.lang.String r32 = ",get_old_path_ok:"
            java.lang.StringBuffer r31 = r31.append(r32)
            r0 = r33
            boolean r0 = r0.get_old_path_ok
            r32 = r0
            java.lang.StringBuffer r31 = r31.append(r32)
            java.lang.String r31 = r31.toString()
            r30.info(r31)
        L_0x003c:
            r8 = 0
            r30 = 3
            r0 = r39
            r1 = r30
            if (r0 != r1) goto L_0x0246
            r0 = r33
            boolean r0 = r0.get_old_path_ok     // Catch:{ IOException -> 0x0283 }
            r30 = r0
            if (r30 == 0) goto L_0x0246
            java.io.File r9 = new java.io.File     // Catch:{ IOException -> 0x0283 }
            r0 = r35
            r9.<init>(r0)     // Catch:{ IOException -> 0x0283 }
            boolean r30 = r9.exists()     // Catch:{ IOException -> 0x03ff }
            if (r30 != 0) goto L_0x0280
            r9.createNewFile()     // Catch:{ IOException -> 0x03ff }
            r8 = r9
        L_0x005e:
            r0 = r33
            org.apache.commons.logging.Log r0 = r0.log
            r30 = r0
            boolean r30 = r30.isInfoEnabled()
            if (r30 == 0) goto L_0x0098
            r0 = r33
            org.apache.commons.logging.Log r0 = r0.log
            r30 = r0
            java.lang.StringBuffer r31 = new java.lang.StringBuffer
            r31.<init>()
            java.lang.String r32 = "file:"
            java.lang.StringBuffer r31 = r31.append(r32)
            r0 = r31
            r1 = r35
            java.lang.StringBuffer r31 = r0.append(r1)
            java.lang.String r32 = " ,str_url:"
            java.lang.StringBuffer r31 = r31.append(r32)
            r0 = r31
            r1 = r34
            java.lang.StringBuffer r31 = r0.append(r1)
            java.lang.String r31 = r31.toString()
            r30.info(r31)
        L_0x0098:
            r21 = 0
            r14 = 0
            r10 = 0
            r28 = 0
            r3 = 0
            r12 = 0
            java.lang.String r17 = ""
            java.net.URL r27 = new java.net.URL     // Catch:{ IOException -> 0x0353 }
            r0 = r27
            r1 = r34
            r0.<init>(r1)     // Catch:{ IOException -> 0x0353 }
            r0 = r33
            org.apache.commons.logging.Log r0 = r0.log     // Catch:{ IOException -> 0x0353 }
            r30 = r0
            boolean r30 = r30.isInfoEnabled()     // Catch:{ IOException -> 0x0353 }
            if (r30 == 0) goto L_0x00c2
            r0 = r33
            org.apache.commons.logging.Log r0 = r0.log     // Catch:{ IOException -> 0x0353 }
            r30 = r0
            java.lang.String r31 = "creat url ok"
            r30.info(r31)     // Catch:{ IOException -> 0x0353 }
        L_0x00c2:
            java.io.InputStream r12 = r27.openStream()     // Catch:{ IOException -> 0x0353 }
            java.io.BufferedReader r4 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0353 }
            java.io.InputStreamReader r30 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0353 }
            java.io.InputStream r31 = r27.openStream()     // Catch:{ IOException -> 0x0353 }
            r0 = r30
            r1 = r31
            r2 = r37
            r0.<init>(r1, r2)     // Catch:{ IOException -> 0x0353 }
            r0 = r30
            r4.<init>(r0)     // Catch:{ IOException -> 0x0353 }
            java.io.FileOutputStream r15 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x03e7, all -> 0x03d3 }
            r15.<init>(r8)     // Catch:{ IOException -> 0x03e7, all -> 0x03d3 }
            java.io.OutputStreamWriter r29 = new java.io.OutputStreamWriter     // Catch:{ IOException -> 0x03eb, all -> 0x03d6 }
            r0 = r29
            r1 = r37
            r0.<init>(r15, r1)     // Catch:{ IOException -> 0x03eb, all -> 0x03d6 }
            java.io.PrintWriter r11 = new java.io.PrintWriter     // Catch:{ IOException -> 0x03f0, all -> 0x03da }
            r0 = r29
            r11.<init>(r0)     // Catch:{ IOException -> 0x03f0, all -> 0x03da }
            r16 = -1
            r0 = r33
            org.apache.commons.logging.Log r0 = r0.log     // Catch:{ IOException -> 0x03f7, all -> 0x03e0 }
            r30 = r0
            boolean r30 = r30.isInfoEnabled()     // Catch:{ IOException -> 0x03f7, all -> 0x03e0 }
            if (r30 == 0) goto L_0x010a
            r0 = r33
            org.apache.commons.logging.Log r0 = r0.log     // Catch:{ IOException -> 0x03f7, all -> 0x03e0 }
            r30 = r0
            java.lang.String r31 = "start read line and write info"
            r30.info(r31)     // Catch:{ IOException -> 0x03f7, all -> 0x03e0 }
        L_0x010a:
            java.lang.String r21 = r4.readLine()     // Catch:{ IOException -> 0x03f7, all -> 0x03e0 }
            if (r21 != 0) goto L_0x02b3
            r10 = 0
            r29.close()     // Catch:{ IOException -> 0x03f0, all -> 0x03da }
            r15.close()     // Catch:{ IOException -> 0x03f0, all -> 0x03da }
            r4.close()     // Catch:{ IOException -> 0x03f0, all -> 0x03da }
            r12.close()     // Catch:{ IOException -> 0x03f0, all -> 0x03da }
            r28 = 0
            r14 = 0
            r3 = 0
            r12 = 0
            r27 = 0
            r8 = 0
            if (r28 == 0) goto L_0x012a
            r28.close()     // Catch:{ Exception -> 0x0343 }
        L_0x012a:
            if (r14 == 0) goto L_0x012f
            r14.close()     // Catch:{ Exception -> 0x0343 }
        L_0x012f:
            if (r3 == 0) goto L_0x0134
            r3.close()     // Catch:{ Exception -> 0x0343 }
        L_0x0134:
            if (r12 == 0) goto L_0x0139
            r12.close()     // Catch:{ Exception -> 0x0343 }
        L_0x0139:
            r28 = 0
            r14 = 0
            r3 = 0
        L_0x013d:
            r0 = r33
            org.apache.commons.logging.Log r0 = r0.log
            r30 = r0
            boolean r30 = r30.isInfoEnabled()
            if (r30 == 0) goto L_0x0154
            r0 = r33
            org.apache.commons.logging.Log r0 = r0.log
            r30 = r0
            java.lang.String r31 = "end doCreateHtml and then page "
            r30.info(r31)
        L_0x0154:
            r5 = 0
            r13 = 0
            r30 = 1
            r0 = r38
            r1 = r30
            if (r0 != r1) goto L_0x022e
            if (r17 == 0) goto L_0x022e
            int r30 = r17.length()
            if (r30 <= 0) goto L_0x022e
            java.lang.String r30 = "["
            r0 = r17
            r1 = r30
            int r19 = r0.indexOf(r1)
            java.lang.String r30 = "]"
            r0 = r17
            r1 = r30
            int r20 = r0.indexOf(r1)
            if (r19 < 0) goto L_0x022e
            if (r20 < 0) goto L_0x022e
            r0 = r20
            r1 = r19
            if (r0 <= r1) goto L_0x022e
            int r30 = r17.length()
            r0 = r20
            r1 = r30
            if (r0 > r1) goto L_0x022e
            int r30 = r19 + 1
            r0 = r17
            r1 = r30
            r2 = r20
            java.lang.String r26 = r0.substring(r1, r2)
            if (r26 == 0) goto L_0x022e
            int r30 = r26.length()
            if (r30 <= 0) goto L_0x022e
            java.lang.String r30 = ","
            r0 = r26
            r1 = r30
            java.lang.String[] r25 = r0.split(r1)
            r0 = r25
            int r0 = r0.length
            r30 = r0
            if (r30 <= 0) goto L_0x01c3
            r30 = 0
            r30 = r25[r30]
            r31 = 0
            r0 = r33
            r1 = r30
            r2 = r31
            int r5 = r0.getIntValue(r1, r2)
        L_0x01c3:
            r0 = r25
            int r0 = r0.length
            r30 = r0
            r31 = 1
            r0 = r30
            r1 = r31
            if (r0 <= r1) goto L_0x01e0
            r30 = 1
            r30 = r25[r30]
            r31 = 0
            r0 = r33
            r1 = r30
            r2 = r31
            int r13 = r0.getIntValue(r1, r2)
        L_0x01e0:
            if (r5 >= r13) goto L_0x022e
            java.lang.String r30 = "?"
            r0 = r34
            r1 = r30
            int r18 = r0.indexOf(r1)
            if (r18 < 0) goto L_0x01fa
            java.lang.String r30 = "page"
            r0 = r33
            r1 = r34
            r2 = r30
            java.lang.String r34 = r0.delURLParam(r1, r2)
        L_0x01fa:
            java.lang.String r30 = "?"
            r0 = r34
            r1 = r30
            int r18 = r0.indexOf(r1)
            if (r18 < 0) goto L_0x03b4
            java.lang.StringBuffer r30 = new java.lang.StringBuffer
            r30.<init>()
            r0 = r30
            r1 = r34
            java.lang.StringBuffer r30 = r0.append(r1)
            java.lang.String r31 = "&page="
            java.lang.StringBuffer r30 = r30.append(r31)
            r0 = r30
            java.lang.StringBuffer r30 = r0.append(r5)
            java.lang.String r34 = r30.toString()
        L_0x0223:
            r0 = r33
            r1 = r36
            java.lang.String r36 = r0.getPageFileName(r1, r5)
            r33.doCreateHtml(r34, r35, r36, r37, r38, r39, r40)
        L_0x022e:
            r0 = r33
            org.apache.commons.logging.Log r0 = r0.log
            r30 = r0
            boolean r30 = r30.isInfoEnabled()
            if (r30 == 0) goto L_0x0245
            r0 = r33
            org.apache.commons.logging.Log r0 = r0.log
            r30 = r0
            java.lang.String r31 = "end doCreateHtml and then page "
            r30.info(r31)
        L_0x0245:
            return
        L_0x0246:
            java.io.File r7 = new java.io.File     // Catch:{ IOException -> 0x0283 }
            r0 = r35
            r7.<init>(r0)     // Catch:{ IOException -> 0x0283 }
            boolean r30 = r7.exists()     // Catch:{ IOException -> 0x0283 }
            if (r30 != 0) goto L_0x0256
            r7.mkdirs()     // Catch:{ IOException -> 0x0283 }
        L_0x0256:
            r7 = 0
            java.io.File r9 = new java.io.File     // Catch:{ IOException -> 0x0283 }
            java.lang.StringBuffer r30 = new java.lang.StringBuffer     // Catch:{ IOException -> 0x0283 }
            r30.<init>()     // Catch:{ IOException -> 0x0283 }
            r0 = r30
            r1 = r36
            java.lang.StringBuffer r30 = r0.append(r1)     // Catch:{ IOException -> 0x0283 }
            java.lang.String r31 = ".html"
            java.lang.StringBuffer r30 = r30.append(r31)     // Catch:{ IOException -> 0x0283 }
            java.lang.String r30 = r30.toString()     // Catch:{ IOException -> 0x0283 }
            r0 = r35
            r1 = r30
            r9.<init>(r0, r1)     // Catch:{ IOException -> 0x0283 }
            boolean r30 = r9.exists()     // Catch:{ IOException -> 0x03ff }
            if (r30 != 0) goto L_0x0280
            r9.createNewFile()     // Catch:{ IOException -> 0x03ff }
        L_0x0280:
            r8 = r9
            goto L_0x005e
        L_0x0283:
            r6 = move-exception
        L_0x0284:
            r0 = r33
            org.apache.commons.logging.Log r0 = r0.log
            r30 = r0
            java.lang.String r31 = r6.getMessage()
            r30.error(r31)
            r0 = r33
            org.apache.commons.logging.Log r0 = r0.log
            r30 = r0
            java.lang.StringBuffer r31 = new java.lang.StringBuffer
            r31.<init>()
            java.lang.String r32 = "file_path : "
            java.lang.StringBuffer r31 = r31.append(r32)
            r0 = r31
            r1 = r35
            java.lang.StringBuffer r31 = r0.append(r1)
            java.lang.String r31 = r31.toString()
            r30.error(r31)
            r8 = 0
            goto L_0x0245
        L_0x02b3:
            r23 = r21
            int r22 = r23.length()     // Catch:{ IOException -> 0x03f7, all -> 0x03e0 }
            if (r22 <= 0) goto L_0x010a
            r0 = r23
            r11.println(r0)     // Catch:{ IOException -> 0x03f7, all -> 0x03e0 }
            r11.flush()     // Catch:{ IOException -> 0x03f7, all -> 0x03e0 }
            r30 = 1
            r0 = r38
            r1 = r30
            if (r0 != r1) goto L_0x010a
            java.lang.String r30 = "<!--page"
            r0 = r23
            r1 = r30
            int r16 = r0.indexOf(r1)     // Catch:{ IOException -> 0x03f7, all -> 0x03e0 }
            if (r16 < 0) goto L_0x0314
            int r30 = r16 + 8
            r0 = r23
            r1 = r30
            java.lang.String r24 = r0.substring(r1)     // Catch:{ IOException -> 0x03f7, all -> 0x03e0 }
            java.lang.StringBuffer r30 = new java.lang.StringBuffer     // Catch:{ IOException -> 0x03f7, all -> 0x03e0 }
            r30.<init>()     // Catch:{ IOException -> 0x03f7, all -> 0x03e0 }
            r0 = r30
            r1 = r17
            java.lang.StringBuffer r30 = r0.append(r1)     // Catch:{ IOException -> 0x03f7, all -> 0x03e0 }
            r0 = r30
            r1 = r24
            java.lang.StringBuffer r30 = r0.append(r1)     // Catch:{ IOException -> 0x03f7, all -> 0x03e0 }
            java.lang.String r17 = r30.toString()     // Catch:{ IOException -> 0x03f7, all -> 0x03e0 }
            java.lang.String r30 = "page-->"
            r0 = r17
            r1 = r30
            int r16 = r0.indexOf(r1)     // Catch:{ IOException -> 0x03f7, all -> 0x03e0 }
            if (r16 < 0) goto L_0x010a
            r30 = 0
            r0 = r17
            r1 = r30
            r2 = r16
            java.lang.String r17 = r0.substring(r1, r2)     // Catch:{ IOException -> 0x03f7, all -> 0x03e0 }
            goto L_0x010a
        L_0x0314:
            java.lang.String r30 = "page-->"
            r0 = r23
            r1 = r30
            int r16 = r0.indexOf(r1)     // Catch:{ IOException -> 0x03f7, all -> 0x03e0 }
            if (r16 < 0) goto L_0x010a
            java.lang.StringBuffer r30 = new java.lang.StringBuffer     // Catch:{ IOException -> 0x03f7, all -> 0x03e0 }
            r30.<init>()     // Catch:{ IOException -> 0x03f7, all -> 0x03e0 }
            r0 = r30
            r1 = r17
            java.lang.StringBuffer r30 = r0.append(r1)     // Catch:{ IOException -> 0x03f7, all -> 0x03e0 }
            r31 = 0
            r0 = r23
            r1 = r31
            r2 = r16
            java.lang.String r31 = r0.substring(r1, r2)     // Catch:{ IOException -> 0x03f7, all -> 0x03e0 }
            java.lang.StringBuffer r30 = r30.append(r31)     // Catch:{ IOException -> 0x03f7, all -> 0x03e0 }
            java.lang.String r17 = r30.toString()     // Catch:{ IOException -> 0x03f7, all -> 0x03e0 }
            goto L_0x010a
        L_0x0343:
            r6 = move-exception
            r0 = r33
            org.apache.commons.logging.Log r0 = r0.log
            r30 = r0
            java.lang.String r31 = r6.getMessage()
            r30.error(r31)
            goto L_0x013d
        L_0x0353:
            r6 = move-exception
        L_0x0354:
            r0 = r33
            org.apache.commons.logging.Log r0 = r0.log     // Catch:{ all -> 0x038b }
            r30 = r0
            java.lang.String r31 = r6.getMessage()     // Catch:{ all -> 0x038b }
            r30.error(r31)     // Catch:{ all -> 0x038b }
            if (r28 == 0) goto L_0x0366
            r28.close()     // Catch:{ Exception -> 0x037b }
        L_0x0366:
            if (r14 == 0) goto L_0x036b
            r14.close()     // Catch:{ Exception -> 0x037b }
        L_0x036b:
            if (r3 == 0) goto L_0x0370
            r3.close()     // Catch:{ Exception -> 0x037b }
        L_0x0370:
            if (r12 == 0) goto L_0x0375
            r12.close()     // Catch:{ Exception -> 0x037b }
        L_0x0375:
            r28 = 0
            r14 = 0
            r3 = 0
            goto L_0x0245
        L_0x037b:
            r6 = move-exception
            r0 = r33
            org.apache.commons.logging.Log r0 = r0.log
            r30 = r0
            java.lang.String r31 = r6.getMessage()
            r30.error(r31)
            goto L_0x0245
        L_0x038b:
            r30 = move-exception
        L_0x038c:
            if (r28 == 0) goto L_0x0391
            r28.close()     // Catch:{ Exception -> 0x03a5 }
        L_0x0391:
            if (r14 == 0) goto L_0x0396
            r14.close()     // Catch:{ Exception -> 0x03a5 }
        L_0x0396:
            if (r3 == 0) goto L_0x039b
            r3.close()     // Catch:{ Exception -> 0x03a5 }
        L_0x039b:
            if (r12 == 0) goto L_0x03a0
            r12.close()     // Catch:{ Exception -> 0x03a5 }
        L_0x03a0:
            r28 = 0
            r14 = 0
            r3 = 0
        L_0x03a4:
            throw r30
        L_0x03a5:
            r6 = move-exception
            r0 = r33
            org.apache.commons.logging.Log r0 = r0.log
            r31 = r0
            java.lang.String r32 = r6.getMessage()
            r31.error(r32)
            goto L_0x03a4
        L_0x03b4:
            java.lang.StringBuffer r30 = new java.lang.StringBuffer
            r30.<init>()
            r0 = r30
            r1 = r34
            java.lang.StringBuffer r30 = r0.append(r1)
            java.lang.String r31 = "?page="
            java.lang.StringBuffer r30 = r30.append(r31)
            r0 = r30
            java.lang.StringBuffer r30 = r0.append(r5)
            java.lang.String r34 = r30.toString()
            goto L_0x0223
        L_0x03d3:
            r30 = move-exception
            r3 = r4
            goto L_0x038c
        L_0x03d6:
            r30 = move-exception
            r3 = r4
            r14 = r15
            goto L_0x038c
        L_0x03da:
            r30 = move-exception
            r3 = r4
            r28 = r29
            r14 = r15
            goto L_0x038c
        L_0x03e0:
            r30 = move-exception
            r3 = r4
            r28 = r29
            r10 = r11
            r14 = r15
            goto L_0x038c
        L_0x03e7:
            r6 = move-exception
            r3 = r4
            goto L_0x0354
        L_0x03eb:
            r6 = move-exception
            r3 = r4
            r14 = r15
            goto L_0x0354
        L_0x03f0:
            r6 = move-exception
            r3 = r4
            r28 = r29
            r14 = r15
            goto L_0x0354
        L_0x03f7:
            r6 = move-exception
            r3 = r4
            r28 = r29
            r10 = r11
            r14 = r15
            goto L_0x0354
        L_0x03ff:
            r6 = move-exception
            r8 = r9
            goto L_0x0284
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenfor.exutil.createHtml.doCreateHtml(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, int, boolean):void");
    }

    /* access modifiers changed from: protected */
    public String getPageFileName(String file_name2, int cur_page) {
        String result;
        int pos = file_name2.lastIndexOf("_p");
        if (pos > 0) {
            result = file_name2.substring(0, pos);
        } else {
            result = file_name2;
        }
        return new StringBuffer().append(result).append("_p").append(cur_page).toString();
    }

    /* access modifiers changed from: protected */
    public String delURLParam(String str_url, String param_str) {
        String result;
        String t_url = str_url;
        int pos = t_url.indexOf("?");
        if (pos <= 0) {
            return t_url;
        }
        String l_url = t_url.substring(0, pos);
        String r_url = delQueryStr(t_url.substring(pos + 1), param_str);
        if (r_url == null || r_url.length() <= 0) {
            result = l_url;
        } else {
            result = new StringBuffer().append(l_url).append("?").append(r_url).toString();
        }
        return result;
    }

    /* access modifiers changed from: protected */
    public String delQueryStr(String querystr, String exceptStr) {
        if (querystr == null || querystr.trim().length() == 0) {
            return null;
        }
        String t_str = querystr;
        String str = querystr;
        int p_index = t_str.indexOf(exceptStr);
        if (p_index > 0) {
            String result = t_str.substring(0, p_index - 1);
            String t_str2 = t_str.substring(p_index);
            int s_index = t_str2.indexOf("&");
            if (s_index < 0) {
                return result;
            }
            String result2 = new StringBuffer().append(result).append(t_str2.substring(s_index)).toString();
            String t_str3 = t_str2.substring(0, s_index);
            return result2;
        } else if (p_index != 0) {
            return str;
        } else {
            int t_index = t_str.indexOf("&");
            if (t_index > 0) {
                return t_str.substring(t_index + 1);
            }
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public int getIntValue(String str_int_value, int default_value) {
        try {
            return Integer.valueOf(str_int_value).intValue();
        } catch (Exception e) {
            return default_value;
        }
    }

    private String strTrim(String str) {
        if (str != null) {
            return str.trim();
        }
        return str;
    }

    private String getUpdateSql(String update_sql, HashMap paramMap) {
        if (update_sql == null || update_sql.length() < 1) {
            return null;
        }
        int pos1 = update_sql.indexOf("set");
        String str1 = update_sql.substring(0, pos1 + 3);
        int pos2 = update_sql.indexOf("where");
        String str2 = update_sql.substring(pos2);
        String str3 = update_sql.substring(pos1 + 3, pos2);
        String new_set = "";
        if (str3 != null && str3.length() > 0) {
            String[] str = str3.split(dbPresentTag.ROLE_DELIMITER);
            for (String t_str : str) {
                if (t_str != null && t_str.indexOf("]") < 0) {
                    new_set = new StringBuffer().append(new_set).append(t_str).append(dbPresentTag.ROLE_DELIMITER).toString();
                }
            }
        }
        if (new_set.endsWith(dbPresentTag.ROLE_DELIMITER)) {
            new_set = new_set.substring(0, new_set.length() - 1);
        }
        return getRealValue(new StringBuffer().append(str1).append(" ").append(new_set).append(" ").append(str2).toString(), paramMap);
    }

    private String getRealValue(String p_value, HashMap paramMap) {
        if (p_value == null) {
            return null;
        }
        String result = "";
        String left_result = p_value;
        while (true) {
            int pos = left_result.indexOf("[");
            if (pos < 0) {
                break;
            }
            String t_result = left_result.substring(0, pos);
            String t_left_result = left_result.substring(pos + 1);
            int pos2 = t_left_result.indexOf("]");
            if (pos2 > 0) {
                String p_str = t_left_result.substring(0, pos2);
                if (pos2 < t_left_result.length() - 1) {
                    left_result = t_left_result.substring(pos2 + 1);
                } else {
                    left_result = "";
                }
                if ("date".equals(p_str)) {
                    result = new StringBuffer().append(result).append(t_result).append(new SimpleDateFormat("yyMMdd").format(new Date())).toString();
                } else if ("year".equals(p_str)) {
                    result = new StringBuffer().append(result).append(t_result).append(new SimpleDateFormat("yyyy").format(new Date())).toString();
                } else if ("html_url".equals(p_str)) {
                    if (!this.html_url_base.endsWith("/")) {
                        this.html_url_base = new StringBuffer().append(this.html_url_base).append("/").toString();
                    }
                    result = new StringBuffer().append(result).append(t_result).append(this.html_url_base).append(this.file_name).append(".html").toString();
                } else if ("html_path".equals(p_str)) {
                    String path_sep = System.getProperty("file.separator");
                    if (!this.file_path.endsWith(path_sep)) {
                        this.file_path = new StringBuffer().append(this.file_path).append(path_sep).toString();
                    }
                    result = new StringBuffer().append(result).append(t_result).append(this.file_path).append(this.file_name).append(".html").toString();
                } else {
                    String p_value2 = String.valueOf(paramMap.get(p_str));
                    if (p_value2 != null) {
                        result = new StringBuffer().append(result).append(t_result).append(strTrim(p_value2)).toString();
                    } else {
                        result = new StringBuffer().append(result).append(t_result).toString();
                    }
                }
            }
        }
        if (left_result == null || left_result.length() <= 0) {
            return result;
        }
        return new StringBuffer().append(result).append(left_result).toString();
    }

    private String getRealURL(String str_url, String param_list, HashMap paramMap) {
        if (param_list == null || param_list.length() < 1) {
            return str_url;
        }
        String[] param = param_list.split(dbPresentTag.ROLE_DELIMITER);
        int j = 0;
        String url_result = str_url;
        for (int i = 0; i < param.length; i++) {
            String p = param[i];
            if (p != null && p.trim().length() >= 1) {
                String t_param = (String) paramMap.get(p);
                if (t_param != null) {
                    if (j == 0) {
                        url_result = new StringBuffer().append(url_result).append("?").append(strTrim(param[i])).append("=").append(strTrim(t_param)).toString();
                    } else {
                        url_result = new StringBuffer().append(url_result).append("&").append(strTrim(param[i])).append("=").append(strTrim(t_param)).toString();
                    }
                    j++;
                } else {
                    this.error_mes = new StringBuffer().append("URL参数设置不正确，在paramMap中找不到 ").append(p).append(" 对应的参数").toString();
                    return null;
                }
            }
        }
        return url_result;
    }

    public boolean isIsDebug() {
        return this.isDebug;
    }

    public void setIsDebug(boolean isDebug2) {
        this.isDebug = isDebug2;
    }
}
