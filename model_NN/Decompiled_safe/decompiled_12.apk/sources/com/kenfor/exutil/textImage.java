package com.kenfor.exutil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

public class textImage extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=GBK";

    public void init() throws ServletException {
    }

    /* JADX WARNING: Removed duplicated region for block: B:101:0x04ef  */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x051e  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x01d2  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x01e0  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x01f2  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x02ee  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x031f  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x033c  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0357  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0372  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x038d  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x03a5  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void doGet(javax.servlet.http.HttpServletRequest r59, javax.servlet.http.HttpServletResponse r60) throws javax.servlet.ServletException, java.io.IOException {
        /*
            r58 = this;
            java.lang.String r55 = "width"
            r0 = r59
            r1 = r55
            java.lang.String r51 = r0.getParameter(r1)
            java.lang.String r55 = "high"
            r0 = r59
            r1 = r55
            java.lang.String r25 = r0.getParameter(r1)
            java.lang.String r55 = "xaxis"
            r0 = r59
            r1 = r55
            java.lang.String r52 = r0.getParameter(r1)
            java.lang.String r55 = "yaxis"
            r0 = r59
            r1 = r55
            java.lang.String r54 = r0.getParameter(r1)
            java.lang.String r55 = "bkColor"
            r0 = r59
            r1 = r55
            java.lang.String r46 = r0.getParameter(r1)
            java.lang.String r55 = "ftColor"
            r0 = r59
            r1 = r55
            java.lang.String r47 = r0.getParameter(r1)
            java.lang.String r55 = "font_size"
            r0 = r59
            r1 = r55
            java.lang.String r19 = r0.getParameter(r1)
            java.lang.String r55 = "fontName"
            r0 = r59
            r1 = r55
            java.lang.String r17 = r0.getParameter(r1)
            r0 = r17
            r1 = r59
            java.lang.String r17 = com.kenfor.util.MultiLanguage.getUnicode(r0, r1)
            java.lang.String r55 = "fontStyle"
            r0 = r59
            r1 = r55
            java.lang.String r18 = r0.getParameter(r1)
            java.lang.String r55 = "picName"
            r0 = r59
            r1 = r55
            java.lang.String r40 = r0.getParameter(r1)
            java.lang.String r55 = "drawText"
            r0 = r59
            r1 = r55
            java.lang.String r14 = r0.getParameter(r1)
            java.lang.String r55 = "shadow"
            r0 = r59
            r1 = r55
            java.lang.String r44 = r0.getParameter(r1)
            r0 = r59
            java.lang.String r14 = com.kenfor.util.MultiLanguage.getUnicode(r14, r0)
            java.lang.String r55 = "isDebug"
            r0 = r59
            r1 = r55
            java.lang.String r30 = r0.getParameter(r1)
            java.lang.String r55 = "is_align_right"
            r0 = r59
            r1 = r55
            java.lang.String r31 = r0.getParameter(r1)
            java.lang.String r55 = "getRealURL"
            r0 = r59
            r1 = r55
            java.lang.String r24 = r0.getParameter(r1)
            java.lang.String r55 = "true"
            r0 = r55
            r1 = r30
            boolean r55 = r0.equals(r1)
            if (r55 == 0) goto L_0x00b8
            java.lang.String r55 = "start textImage"
            com.kenfor.util.ProDebug.addDebugLog(r55)
            com.kenfor.util.ProDebug.saveToFile()
        L_0x00b8:
            r32 = 100
            r27 = 30
            r33 = 20
            r34 = 20
            java.awt.Color r5 = new java.awt.Color
            java.lang.String r55 = "ffffff"
            r56 = 16
            int r55 = java.lang.Integer.parseInt(r55, r56)
            r0 = r55
            r5.<init>(r0)
            java.awt.Color r20 = new java.awt.Color
            java.lang.String r55 = "000000"
            r56 = 16
            int r55 = java.lang.Integer.parseInt(r55, r56)
            r0 = r20
            r1 = r55
            r0.<init>(r1)
            java.awt.Color r22 = new java.awt.Color
            java.lang.String r55 = "7D7D7D"
            r56 = 16
            int r55 = java.lang.Integer.parseInt(r55, r56)
            r0 = r22
            r1 = r55
            r0.<init>(r1)
            r26 = 18
            java.lang.Integer r55 = java.lang.Integer.valueOf(r19)     // Catch:{ Exception -> 0x03e0 }
            int r26 = r55.intValue()     // Catch:{ Exception -> 0x03e0 }
        L_0x00fb:
            java.lang.Integer r55 = java.lang.Integer.valueOf(r52)     // Catch:{ Exception -> 0x03e5 }
            int r33 = r55.intValue()     // Catch:{ Exception -> 0x03e5 }
        L_0x0103:
            java.lang.Integer r55 = java.lang.Integer.valueOf(r54)     // Catch:{ Exception -> 0x03ea }
            int r34 = r55.intValue()     // Catch:{ Exception -> 0x03ea }
        L_0x010b:
            java.lang.Integer r55 = java.lang.Integer.valueOf(r51)     // Catch:{ Exception -> 0x03ef }
            int r32 = r55.intValue()     // Catch:{ Exception -> 0x03ef }
        L_0x0113:
            java.lang.Integer r55 = java.lang.Integer.valueOf(r25)     // Catch:{ Exception -> 0x03f4 }
            int r27 = r55.intValue()     // Catch:{ Exception -> 0x03f4 }
        L_0x011b:
            java.lang.String r55 = "#"
            java.lang.String r56 = ""
            r0 = r46
            r1 = r55
            r2 = r56
            r0.replaceFirst(r1, r2)     // Catch:{ Exception -> 0x03f9 }
            java.awt.Color r5 = new java.awt.Color     // Catch:{ Exception -> 0x03f9 }
            r55 = 16
            r0 = r46
            r1 = r55
            int r55 = java.lang.Integer.parseInt(r0, r1)     // Catch:{ Exception -> 0x03f9 }
            r0 = r55
            r5.<init>(r0)     // Catch:{ Exception -> 0x03f9 }
        L_0x0139:
            java.lang.String r55 = "#"
            java.lang.String r56 = ""
            r0 = r47
            r1 = r55
            r2 = r56
            r0.replaceFirst(r1, r2)     // Catch:{ Exception -> 0x040b }
            r55 = 0
            r56 = 2
            r0 = r47
            r1 = r55
            r2 = r56
            java.lang.String r11 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x040b }
            r55 = 2
            r56 = 4
            r0 = r47
            r1 = r55
            r2 = r56
            java.lang.String r10 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x040b }
            r55 = 4
            r56 = 6
            r0 = r47
            r1 = r55
            r2 = r56
            java.lang.String r9 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x040b }
            r55 = 16
            r0 = r55
            int r55 = java.lang.Integer.parseInt(r11, r0)     // Catch:{ Exception -> 0x040b }
            r0 = r55
            int r0 = r0 + 255
            r55 = r0
            int r8 = r55 / 2
            r55 = 16
            r0 = r55
            int r55 = java.lang.Integer.parseInt(r10, r0)     // Catch:{ Exception -> 0x040b }
            r0 = r55
            int r0 = r0 + 255
            r55 = r0
            int r7 = r55 / 2
            r55 = 16
            r0 = r55
            int r55 = java.lang.Integer.parseInt(r9, r0)     // Catch:{ Exception -> 0x040b }
            r0 = r55
            int r0 = r0 + 255
            r55 = r0
            int r6 = r55 / 2
            int r0 = r8 * 256
            r55 = r0
            r0 = r55
            int r0 = r0 * 256
            r55 = r0
            int r0 = r7 * 256
            r56 = r0
            int r55 = r55 + r56
            int r41 = r55 + r6
            java.awt.Color r21 = new java.awt.Color     // Catch:{ Exception -> 0x040b }
            r55 = 16
            r0 = r47
            r1 = r55
            int r55 = java.lang.Integer.parseInt(r0, r1)     // Catch:{ Exception -> 0x040b }
            r0 = r21
            r1 = r55
            r0.<init>(r1)     // Catch:{ Exception -> 0x040b }
            java.awt.Color r22 = new java.awt.Color     // Catch:{ Exception -> 0x054b }
            r0 = r22
            r1 = r41
            r0.<init>(r1)     // Catch:{ Exception -> 0x054b }
            r20 = r21
        L_0x01d0:
            if (r17 != 0) goto L_0x01d4
            java.lang.String r17 = ""
        L_0x01d4:
            java.lang.String r55 = "true"
            r0 = r55
            r1 = r30
            boolean r55 = r0.equals(r1)
            if (r55 == 0) goto L_0x01e8
            java.lang.String r55 = "test 1 at  textImage"
            com.kenfor.util.ProDebug.addDebugLog(r55)
            com.kenfor.util.ProDebug.saveToFile()
        L_0x01e8:
            r45 = 0
            r28 = 0
            r23 = 0
            r29 = 0
            if (r40 == 0) goto L_0x04ef
            java.lang.String r42 = r59.getServerName()
            int r43 = r59.getServerPort()
            java.lang.String r12 = r59.getContextPath()
            r39 = 0
            java.lang.String r55 = "true"
            r0 = r55
            r1 = r24
            boolean r55 = r0.equalsIgnoreCase(r1)
            if (r55 == 0) goto L_0x044f
            r55 = 80
            r0 = r43
            r1 = r55
            if (r0 == r1) goto L_0x0430
            java.lang.StringBuffer r55 = new java.lang.StringBuffer
            r55.<init>()
            java.lang.String r56 = "http://"
            java.lang.StringBuffer r55 = r55.append(r56)
            r0 = r55
            r1 = r42
            java.lang.StringBuffer r55 = r0.append(r1)
            java.lang.String r56 = ":"
            java.lang.StringBuffer r55 = r55.append(r56)
            r0 = r55
            r1 = r43
            java.lang.StringBuffer r55 = r0.append(r1)
            java.lang.String r56 = "/"
            java.lang.StringBuffer r55 = r55.append(r56)
            java.lang.String r39 = r55.toString()
        L_0x023f:
            if (r12 == 0) goto L_0x0264
            int r55 = r12.length()
            if (r55 <= 0) goto L_0x0264
            java.lang.StringBuffer r55 = new java.lang.StringBuffer
            r55.<init>()
            r0 = r55
            r1 = r39
            java.lang.StringBuffer r55 = r0.append(r1)
            r0 = r55
            java.lang.StringBuffer r55 = r0.append(r12)
            java.lang.String r56 = "/"
            java.lang.StringBuffer r55 = r55.append(r56)
            java.lang.String r39 = r55.toString()
        L_0x0264:
            java.lang.StringBuffer r55 = new java.lang.StringBuffer
            r55.<init>()
            r0 = r55
            r1 = r39
            java.lang.StringBuffer r55 = r0.append(r1)
            r0 = r55
            r1 = r40
            java.lang.StringBuffer r55 = r0.append(r1)
            java.lang.String r39 = r55.toString()
            java.io.PrintStream r55 = java.lang.System.out
            r0 = r55
            r1 = r39
            r0.println(r1)
        L_0x0286:
            java.lang.String r55 = "true"
            r0 = r55
            r1 = r30
            boolean r55 = r0.equals(r1)
            if (r55 == 0) goto L_0x029d
            java.lang.String r55 = "realPath at textImage:"
            com.kenfor.util.ProDebug.addDebugLog(r55)
            com.kenfor.util.ProDebug.addDebugLog(r39)
            com.kenfor.util.ProDebug.saveToFile()
        L_0x029d:
            java.net.URL r50 = new java.net.URL     // Catch:{ Exception -> 0x04ac }
            r0 = r50
            r1 = r39
            r0.<init>(r1)     // Catch:{ Exception -> 0x04ac }
            java.io.InputStream r55 = r50.openStream()     // Catch:{ Exception -> 0x04ac }
            com.sun.image.codec.jpeg.JPEGImageDecoder r13 = com.sun.image.codec.jpeg.JPEGCodec.createJPEGDecoder(r55)     // Catch:{ Exception -> 0x04ac }
            java.awt.image.BufferedImage r28 = r13.decodeAsBufferedImage()     // Catch:{ Exception -> 0x04ac }
            int r29 = r28.getWidth()     // Catch:{ Exception -> 0x04ac }
            java.awt.Graphics r23 = r28.getGraphics()     // Catch:{ Exception -> 0x04ac }
            java.lang.String r55 = "true"
            r0 = r55
            r1 = r30
            boolean r55 = r0.equals(r1)     // Catch:{ Exception -> 0x04ac }
            if (r55 == 0) goto L_0x02ce
            java.lang.String r55 = "test 12 textImage"
            com.kenfor.util.ProDebug.addDebugLog(r55)     // Catch:{ Exception -> 0x04ac }
            com.kenfor.util.ProDebug.saveToFile()     // Catch:{ Exception -> 0x04ac }
        L_0x02ce:
            java.lang.String r55 = "true"
            r0 = r55
            r1 = r30
            boolean r55 = r0.equals(r1)
            if (r55 == 0) goto L_0x02e2
            java.lang.String r55 = "test 13 textImage"
            com.kenfor.util.ProDebug.addDebugLog(r55)
            com.kenfor.util.ProDebug.saveToFile()
        L_0x02e2:
            java.lang.String r55 = "true"
            r0 = r55
            r1 = r30
            boolean r55 = r0.equals(r1)
            if (r55 == 0) goto L_0x02f6
            java.lang.String r55 = "test 14 textImage"
            com.kenfor.util.ProDebug.addDebugLog(r55)
            com.kenfor.util.ProDebug.saveToFile()
        L_0x02f6:
            java.lang.String r55 = "Cache-Control"
            java.lang.String r56 = "no-store"
            r0 = r60
            r1 = r55
            r2 = r56
            r0.setHeader(r1, r2)
            java.lang.String r55 = "Expires"
            r56 = 0
            r0 = r60
            r1 = r55
            r2 = r56
            r0.setDateHeader(r1, r2)
            java.lang.String r55 = "image/jpeg"
            r0 = r60
            r1 = r55
            r0.setContentType(r1)
            javax.servlet.ServletOutputStream r38 = r60.getOutputStream()
            if (r14 != 0) goto L_0x0321
            java.lang.String r14 = "测试文字"
        L_0x0321:
            java.awt.Font r37 = new java.awt.Font
            r55 = 0
            r0 = r37
            r1 = r17
            r2 = r55
            r3 = r26
            r0.<init>(r1, r2, r3)
            java.lang.String r55 = "italic"
            r0 = r55
            r1 = r18
            boolean r55 = r0.equalsIgnoreCase(r1)
            if (r55 == 0) goto L_0x034b
            java.awt.Font r37 = new java.awt.Font
            r55 = 2
            r0 = r37
            r1 = r17
            r2 = r55
            r3 = r26
            r0.<init>(r1, r2, r3)
        L_0x034b:
            java.lang.String r55 = "bold"
            r0 = r55
            r1 = r18
            boolean r55 = r0.equalsIgnoreCase(r1)
            if (r55 == 0) goto L_0x0366
            java.awt.Font r37 = new java.awt.Font
            r55 = 1
            r0 = r37
            r1 = r17
            r2 = r55
            r3 = r26
            r0.<init>(r1, r2, r3)
        L_0x0366:
            java.lang.String r55 = "plain"
            r0 = r55
            r1 = r18
            boolean r55 = r0.equalsIgnoreCase(r1)
            if (r55 == 0) goto L_0x0381
            java.awt.Font r37 = new java.awt.Font
            r55 = 0
            r0 = r37
            r1 = r17
            r2 = r55
            r3 = r26
            r0.<init>(r1, r2, r3)
        L_0x0381:
            java.lang.String r55 = "true"
            r0 = r55
            r1 = r31
            boolean r55 = r0.equalsIgnoreCase(r1)
            if (r55 == 0) goto L_0x0399
            int r36 = r14.length()
            int r55 = r26 + 1
            int r55 = r55 * r36
            int r55 = r29 - r55
            int r33 = r55 - r33
        L_0x0399:
            java.lang.String r55 = "false"
            r0 = r55
            r1 = r44
            boolean r55 = r0.equals(r1)
            if (r55 == 0) goto L_0x051e
            r0 = r23
            r1 = r37
            r0.setFont(r1)
            r0 = r23
            r1 = r20
            r0.setColor(r1)
            r0 = r23
            r1 = r33
            r2 = r34
            r0.drawString(r14, r1, r2)
        L_0x03bc:
            com.sun.image.codec.jpeg.JPEGEncodeParam r35 = com.sun.image.codec.jpeg.JPEGCodec.getDefaultJPEGEncodeParam(r28)
            r55 = 1065353216(0x3f800000, float:1.0)
            r56 = 1
            r0 = r35
            r1 = r55
            r2 = r56
            r0.setQuality(r1, r2)
            r0 = r38
            r1 = r35
            com.sun.image.codec.jpeg.JPEGImageEncoder r16 = com.sun.image.codec.jpeg.JPEGCodec.createJPEGEncoder(r0, r1)
            r0 = r16
            r1 = r28
            r0.encode(r1)
            r38.close()
            return
        L_0x03e0:
            r15 = move-exception
            r26 = 18
            goto L_0x00fb
        L_0x03e5:
            r15 = move-exception
            r33 = 20
            goto L_0x0103
        L_0x03ea:
            r15 = move-exception
            r34 = r26
            goto L_0x010b
        L_0x03ef:
            r15 = move-exception
            r32 = 100
            goto L_0x0113
        L_0x03f4:
            r15 = move-exception
            int r27 = r26 + 10
            goto L_0x011b
        L_0x03f9:
            r15 = move-exception
            java.awt.Color r5 = new java.awt.Color
            java.lang.String r55 = "ffffff"
            r56 = 16
            int r55 = java.lang.Integer.parseInt(r55, r56)
            r0 = r55
            r5.<init>(r0)
            goto L_0x0139
        L_0x040b:
            r15 = move-exception
        L_0x040c:
            java.awt.Color r20 = new java.awt.Color
            java.lang.String r55 = "000000"
            r56 = 16
            int r55 = java.lang.Integer.parseInt(r55, r56)
            r0 = r20
            r1 = r55
            r0.<init>(r1)
            java.awt.Color r22 = new java.awt.Color
            java.lang.String r55 = "7D7D7D"
            r56 = 16
            int r55 = java.lang.Integer.parseInt(r55, r56)
            r0 = r22
            r1 = r55
            r0.<init>(r1)
            goto L_0x01d0
        L_0x0430:
            java.lang.StringBuffer r55 = new java.lang.StringBuffer
            r55.<init>()
            java.lang.String r56 = "http://"
            java.lang.StringBuffer r55 = r55.append(r56)
            r0 = r55
            r1 = r42
            java.lang.StringBuffer r55 = r0.append(r1)
            java.lang.String r56 = "/"
            java.lang.StringBuffer r55 = r55.append(r56)
            java.lang.String r39 = r55.toString()
            goto L_0x023f
        L_0x044f:
            java.lang.String r55 = "/WEB-INF/classes"
            r0 = r59
            r1 = r55
            java.lang.String r48 = r0.getRealPath(r1)
            java.lang.String r49 = ""
            com.kenfor.exutil.xmlConstant r53 = new com.kenfor.exutil.xmlConstant     // Catch:{ Exception -> 0x04a8 }
            r0 = r53
            r1 = r48
            r0.<init>(r1)     // Catch:{ Exception -> 0x04a8 }
            r0 = r53
            java.lang.String r0 = r0.url_base     // Catch:{ Exception -> 0x04a8 }
            r49 = r0
            java.lang.String r55 = "/"
            r0 = r49
            r1 = r55
            boolean r55 = r0.endsWith(r1)     // Catch:{ Exception -> 0x04a8 }
            if (r55 != 0) goto L_0x048d
            java.lang.StringBuffer r55 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x04a8 }
            r55.<init>()     // Catch:{ Exception -> 0x04a8 }
            r0 = r55
            r1 = r49
            java.lang.StringBuffer r55 = r0.append(r1)     // Catch:{ Exception -> 0x04a8 }
            java.lang.String r56 = "/"
            java.lang.StringBuffer r55 = r55.append(r56)     // Catch:{ Exception -> 0x04a8 }
            java.lang.String r49 = r55.toString()     // Catch:{ Exception -> 0x04a8 }
        L_0x048d:
            java.lang.StringBuffer r55 = new java.lang.StringBuffer
            r55.<init>()
            r0 = r55
            r1 = r49
            java.lang.StringBuffer r55 = r0.append(r1)
            r0 = r55
            r1 = r40
            java.lang.StringBuffer r55 = r0.append(r1)
            java.lang.String r39 = r55.toString()
            goto L_0x0286
        L_0x04a8:
            r15 = move-exception
            java.lang.String r49 = "http://indexpro.lighting86.com/"
            goto L_0x048d
        L_0x04ac:
            r15 = move-exception
            java.lang.String r55 = "true"
            r0 = r55
            r1 = r30
            boolean r55 = r0.equals(r1)
            if (r55 == 0) goto L_0x04d6
            java.lang.StringBuffer r55 = new java.lang.StringBuffer
            r55.<init>()
            java.lang.String r56 = "some exception : "
            java.lang.StringBuffer r55 = r55.append(r56)
            java.lang.String r56 = r15.getMessage()
            java.lang.StringBuffer r55 = r55.append(r56)
            java.lang.String r55 = r55.toString()
            com.kenfor.util.ProDebug.addDebugLog(r55)
            com.kenfor.util.ProDebug.saveToFile()
        L_0x04d6:
            r29 = r32
            java.awt.image.BufferedImage r28 = new java.awt.image.BufferedImage
            r55 = 13
            r0 = r28
            r1 = r32
            r2 = r27
            r3 = r55
            r0.<init>(r1, r2, r3)
            java.awt.Graphics r23 = r28.getGraphics()
            java.awt.Graphics2D r23 = (java.awt.Graphics2D) r23
            goto L_0x02ce
        L_0x04ef:
            r29 = r32
            java.awt.image.BufferedImage r28 = new java.awt.image.BufferedImage
            r55 = 13
            r0 = r28
            r1 = r32
            r2 = r27
            r3 = r55
            r0.<init>(r1, r2, r3)
            java.awt.Graphics r23 = r28.getGraphics()
            java.awt.Graphics2D r23 = (java.awt.Graphics2D) r23
            r0 = r23
            r0.setColor(r5)
            r55 = 0
            r56 = 0
            r0 = r23
            r1 = r55
            r2 = r56
            r3 = r32
            r4 = r27
            r0.fillRect(r1, r2, r3, r4)
            goto L_0x02e2
        L_0x051e:
            r0 = r23
            r1 = r37
            r0.setFont(r1)
            r0 = r23
            r1 = r22
            r0.setColor(r1)
            int r55 = r33 + 2
            int r56 = r34 + 1
            r0 = r23
            r1 = r55
            r2 = r56
            r0.drawString(r14, r1, r2)
            r0 = r23
            r1 = r20
            r0.setColor(r1)
            r0 = r23
            r1 = r33
            r2 = r34
            r0.drawString(r14, r1, r2)
            goto L_0x03bc
        L_0x054b:
            r15 = move-exception
            r20 = r21
            goto L_0x040c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenfor.exutil.textImage.doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse):void");
    }

    public void destroy() {
    }
}
