package com.kenfor.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Dictionary;
import java.util.Hashtable;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import org.xbill.DNS.KEYRecord;

public class FileUploadBean {
    private String contentType;
    private Dictionary fields;
    private String filename;
    private String filepath;
    private String savePath;

    public String getFilename() {
        return this.filename;
    }

    public String getFilepath() {
        return this.filepath;
    }

    public void setSavePath(String savePath2) {
        this.savePath = savePath2;
    }

    public String getContentType() {
        return this.contentType;
    }

    public String getFieldValue(String fieldName) {
        if (this.fields == null || fieldName == null) {
            return null;
        }
        return (String) this.fields.get(fieldName);
    }

    private void setFilename(String s) {
        int pos;
        if (s != null && (pos = s.indexOf("filename=\"")) != -1) {
            this.filepath = s.substring(pos + 10, s.length() - 1);
            int pos2 = this.filepath.lastIndexOf("\\");
            if (pos2 != -1) {
                this.filename = this.filepath.substring(pos2 + 1);
            } else {
                this.filename = this.filepath;
            }
        }
    }

    private void setContentType(String s) {
        int pos;
        if (s != null && (pos = s.indexOf(": ")) != -1) {
            this.contentType = s.substring(pos + 2, s.length());
        }
    }

    private void cpyBytes(byte[] bytes1, byte[] bytes2, int len) {
        for (int i = 0; i < len; i++) {
            bytes2[i] = bytes1[i];
        }
    }

    public void doUpload(HttpServletRequest request) throws IOException {
        ServletInputStream in = request.getInputStream();
        byte[] line = new byte[KEYRecord.Flags.FLAG5];
        byte[] line_temp = new byte[KEYRecord.Flags.FLAG5];
        int i = in.readLine(line, 0, (int) KEYRecord.Flags.FLAG5);
        if (i >= 3) {
            int boundaryLength = i - 2;
            String boundary = new String(line, 0, boundaryLength);
            this.fields = new Hashtable();
            while (i != -1) {
                String newLine = new String(line, 0, i);
                if (newLine.startsWith("Content-Disposition: form-data; name=\"")) {
                    if (newLine.indexOf("filename=\"") != -1) {
                        setFilename(new String(line, 0, i - 2));
                        if (this.filename != null) {
                            setContentType(new String(line, 0, in.readLine(line, 0, 128) - 2));
                            int i2 = in.readLine(line, 0, 128);
                            int i3 = in.readLine(line, 0, 128);
                            String newLine2 = new String(line, 0, i3);
                            int l_len = i3;
                            cpyBytes(line, line_temp, i3);
                            FileOutputStream fout = new FileOutputStream(new File(new StringBuffer().append(this.savePath == null ? "" : this.savePath).append(this.filename).toString()));
                            while (i3 != -1 && !newLine2.startsWith(boundary)) {
                                i3 = in.readLine(line, 0, 128);
                                if ((i3 == boundaryLength + 2 || i3 == boundaryLength + 4) && new String(line, 0, i3).startsWith(boundary)) {
                                    fout.write(line_temp, 0, l_len - 2);
                                } else {
                                    fout.write(line_temp);
                                }
                                newLine2 = new String(line, 0, i3);
                                cpyBytes(line, line_temp, i3);
                                l_len = i3;
                            }
                            fout.close();
                        } else {
                            return;
                        }
                    } else {
                        String fieldName = newLine.substring(newLine.indexOf("name=\"") + 6, newLine.length() - 3);
                        int i4 = in.readLine(line, 0, 128);
                        int i5 = in.readLine(line, 0, 128);
                        String newLine3 = new String(line, 0, i5);
                        StringBuffer fieldValue = new StringBuffer(128);
                        while (i5 != -1 && !newLine3.startsWith(boundary)) {
                            i5 = in.readLine(line, 0, 128);
                            if ((i5 == boundaryLength + 2 || i5 == boundaryLength + 4) && new String(line, 0, i5).startsWith(boundary)) {
                                fieldValue.append(newLine3.substring(0, newLine3.length() - 2));
                            } else {
                                fieldValue.append(newLine3);
                            }
                            newLine3 = new String(line, 0, i5);
                        }
                        this.fields.put(fieldName, fieldValue.toString());
                    }
                }
                i = in.readLine(line, 0, 128);
            }
        }
    }
}
