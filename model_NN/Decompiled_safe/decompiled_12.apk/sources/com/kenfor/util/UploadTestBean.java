package com.kenfor.util;

import com.kenfor.util.upload.MultipartElement;
import com.kenfor.util.upload.MultipartIterator;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;

public class UploadTestBean {
    public static String HEADER_CONTENT_TYPE = "Content-Type";
    private static final String PARAMETER_BOUNDARY = "boundary=";
    private String boundary = null;
    private String contentType = null;
    private HashMap map = null;
    protected HttpServletRequest request = null;

    public void doUpload(HttpServletRequest request2) throws IOException {
        new ProDebug();
        ProDebug.addDebugLog("start do upload");
        ProDebug.saveToFile();
        this.map = new HashMap();
        MultipartIterator iterator = new MultipartIterator(request2);
        while (true) {
            MultipartElement element = iterator.getNextElement();
            if (element == null) {
                break;
            }
            if (element.isFile()) {
                this.map.put(element.getName(), element.getFileName());
            } else {
                this.map.put(element.getName(), element.getValue());
            }
            ProDebug.addDebugLog(new StringBuffer().append("name :").append(element.getName()).toString());
            ProDebug.saveToFile();
        }
        this.request = request2;
        getContentTypeOfRequest();
        getBoundaryFromContentType();
        PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter("Demo.out")));
        ServletInputStream in = request2.getInputStream();
        for (int i = in.read(); i != -1; i = in.read()) {
            pw.print((char) i);
        }
        pw.close();
    }

    public String getContentType() {
        return this.contentType;
    }

    public String getBoundary() {
        return this.boundary;
    }

    public HashMap getMap() {
        return this.map;
    }

    private final void getContentTypeOfRequest() {
        this.contentType = this.request.getContentType();
        if (this.contentType == null) {
            this.contentType = this.request.getHeader(HEADER_CONTENT_TYPE);
        }
    }

    private final void getBoundaryFromContentType() throws IOException {
        if (this.contentType.lastIndexOf(PARAMETER_BOUNDARY) != -1) {
            String _boundary = this.contentType.substring(this.contentType.lastIndexOf(PARAMETER_BOUNDARY) + 9);
            if (_boundary.endsWith("\n")) {
                this.boundary = _boundary.substring(0, _boundary.length() - 1);
            }
            this.boundary = _boundary;
        } else {
            this.boundary = null;
        }
        if (this.boundary == null || this.boundary.length() < 1) {
            throw new IOException("io exception");
        }
    }
}
