package com.kenfor.util;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public final class Helper {
    private static Log log = LogFactory.getLog("Helper");

    public static final void cleanup(Statement stmt, Connection conn) {
        if (conn != null) {
            try {
                if (!conn.isClosed() && stmt != null) {
                }
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        }
        if (conn != null) {
            try {
                if (!conn.isClosed()) {
                    conn.close();
                }
            } catch (Exception e2) {
                log.error("有数据库连接没有关闭的情况，试关闭");
                log.error(e2.getMessage());
                e2.printStackTrace();
            }
        }
    }

    public static final void cleanup(PreparedStatement pstmt, Connection conn) {
        if (conn != null) {
            try {
                if (!conn.isClosed() && pstmt != null) {
                }
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        }
        if (conn != null) {
            try {
                if (!conn.isClosed()) {
                    conn.close();
                }
            } catch (Exception e2) {
                log.error("有数据库连接没有关闭的情况，试关闭");
                log.error(new StringBuffer().append("exception: con isn't closed again").append(e2.getMessage()).toString());
                e2.printStackTrace();
            }
        }
    }

    public static final void cleanup(Statement stmt, Connection conn, ResultSet rs) {
        if (conn != null) {
            try {
                if (!conn.isClosed() && rs != null) {
                }
            } catch (Exception ex) {
                log.error(ex.getMessage());
            }
        }
        if (conn != null) {
            try {
                if (!conn.isClosed() && stmt != null) {
                }
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        }
        if (conn != null) {
            try {
                if (!conn.isClosed()) {
                    conn.close();
                }
            } catch (Exception e2) {
                log.error("有数据库连接没有关闭的情况，试关闭");
                log.error(new StringBuffer().append("exception: con isn't closed again").append(e2.getMessage()).toString());
                e2.printStackTrace();
            }
        }
    }

    public static final void cleanup(PreparedStatement pstmt, Connection conn, ResultSet rs) {
        if (conn != null) {
            try {
                if (!conn.isClosed() && rs != null) {
                }
            } catch (Exception ex) {
                log.error(ex.getMessage());
            }
        }
        if (conn != null) {
            try {
                if (!conn.isClosed() && pstmt != null) {
                }
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        }
        if (conn != null) {
            try {
                if (!conn.isClosed()) {
                    conn.close();
                }
            } catch (Exception e2) {
                log.error("有数据库连接没有关闭的情况，试关闭");
                log.error(new StringBuffer().append("exception: con isn't closed again").append(e2.getMessage()).toString());
                e2.printStackTrace();
            }
        }
    }

    public static final void cleanup(CallableStatement castmt, Connection conn, ResultSet rs) {
        if (conn != null) {
            try {
                if (!conn.isClosed() && rs != null) {
                }
            } catch (Exception ex) {
                log.error(ex.getMessage());
            }
        }
        if (conn != null) {
            try {
                if (!conn.isClosed() && castmt != null) {
                }
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        }
        if (conn != null) {
            try {
                if (!conn.isClosed()) {
                    conn.close();
                }
            } catch (Exception e2) {
                log.error("有数据库连接没有关闭的情况，试关闭");
                log.error(new StringBuffer().append("exception: con isn't closed again").append(e2.getMessage()).toString());
                e2.printStackTrace();
            }
        }
    }

    public static final void cleanup(CallableStatement castmt, Connection conn) {
        if (conn != null) {
            try {
                if (!conn.isClosed() && castmt != null) {
                }
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        }
        if (conn != null) {
            try {
                if (!conn.isClosed()) {
                    conn.close();
                }
            } catch (Exception e2) {
                log.error("有数据库连接没有关闭的情况，试关闭");
                log.error(new StringBuffer().append("exception: con isn't closed again").append(e2.getMessage()).toString());
                e2.printStackTrace();
            }
        }
    }
}
