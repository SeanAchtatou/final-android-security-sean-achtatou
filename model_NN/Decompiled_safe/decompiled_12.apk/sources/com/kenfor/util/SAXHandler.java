package com.kenfor.util;

import java.util.Hashtable;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SAXHandler extends DefaultHandler {
    private String currentElement = null;
    private String currentValue = null;
    private Hashtable table = new Hashtable();

    public void setTable(Hashtable table2) {
        this.table = table2;
    }

    public Hashtable getTable() {
        return this.table;
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        this.currentElement = qName;
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        this.currentValue = new String(ch, start, length);
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (this.currentElement.equals(qName)) {
            this.table.put(this.currentElement, this.currentValue);
        }
    }
}
