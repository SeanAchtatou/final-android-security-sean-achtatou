package com.kenfor.util.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class InvalidSQLinjectFilter implements Filter {
    private FilterConfig filterConfig;
    private Log log = LogFactory.getLog(getClass().getName());

    public void init(FilterConfig filterConfig2) throws ServletException {
        System.out.println("InvalidSQLinjectFilter initialized");
        this.filterConfig = filterConfig2;
    }

    public void destroy() {
        System.out.println("InvalidSQLinjectFilter destroyed");
        this.log.info("ReplaceFilter destroyed");
        this.filterConfig = null;
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        InvalidSQLParmWrapper invalidSQLParmWrapper = new InvalidSQLParmWrapper((HttpServletRequest) request);
        if (invalidSQLParmWrapper.CheckValue()) {
            chain.doFilter(invalidSQLParmWrapper, response);
            return;
        }
        try {
            HttpServletResponse httpServletResponse = (HttpServletResponse) response;
            this.log.warn(new StringBuffer().append("发现非法的sql注入:").append(((HttpServletRequest) request).getRequestURI()).toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
