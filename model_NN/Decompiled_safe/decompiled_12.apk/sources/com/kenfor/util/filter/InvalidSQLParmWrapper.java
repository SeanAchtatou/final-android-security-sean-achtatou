package com.kenfor.util.filter;

import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class InvalidSQLParmWrapper extends HttpServletRequestWrapper {
    String check_sql;
    private Log log;

    public InvalidSQLParmWrapper(HttpServletRequest servletRequest) {
        super(servletRequest);
        this.log = LogFactory.getLog(getClass().getName());
        this.check_sql = null;
        this.check_sql = getQueryString();
    }

    public boolean CheckValue() {
        String tempStr = this.check_sql;
        if (tempStr == null || "".equals(tempStr)) {
            return true;
        }
        String tempStr2 = URLDecoder.decode(tempStr).toLowerCase();
        if (tempStr2.indexOf(";") != -1) {
            System.out.println(new StringBuffer().append("; : ").append(tempStr2).toString());
            return false;
        } else if (tempStr2.indexOf("exec") != -1) {
            System.out.println(new StringBuffer().append("EXEC: ").append(tempStr2).toString());
            return false;
        } else if (tempStr2.indexOf("xp_") != -1) {
            System.out.println(new StringBuffer().append("xp_: ").append(tempStr2).toString());
            return false;
        } else if (tempStr2.indexOf("select") != -1) {
            System.out.println(new StringBuffer().append("select : ").append(tempStr2).toString());
            return false;
        } else if (tempStr2.indexOf(" select ") != -1) {
            System.out.println(new StringBuffer().append(" select2 : ").append(tempStr2).toString());
            return false;
        } else if (tempStr2.indexOf(" delete ") != -1) {
            System.out.println(new StringBuffer().append("delete : ").append(tempStr2).toString());
            return false;
        } else if (tempStr2.indexOf(" update ") != -1) {
            System.out.println(new StringBuffer().append(" update: ").append(tempStr2).toString());
            return false;
        } else if (tempStr2.indexOf("drop") != -1) {
            System.out.println(new StringBuffer().append("drop: ").append(tempStr2).toString());
            return false;
        } else if (tempStr2.indexOf("alter") != -1) {
            System.out.println(new StringBuffer().append("alter: ").append(tempStr2).toString());
            return false;
        } else if (tempStr2.indexOf("is_srvrolemember") != -1) {
            System.out.println(new StringBuffer().append("is_srvrolemember: ").append(tempStr2).toString());
            return false;
        } else if (tempStr2.indexOf("is_member") != -1) {
            System.out.println(new StringBuffer().append("is_member: ").append(tempStr2).toString());
            return false;
        } else if (tempStr2.indexOf("convert") != -1) {
            System.out.println(new StringBuffer().append("convert: ").append(tempStr2).toString());
            return false;
        } else if (tempStr2.indexOf("delclare") != -1) {
            System.out.println(new StringBuffer().append("delclare: ").append(tempStr2).toString());
            return false;
        } else if (tempStr2.indexOf(" count ") != -1) {
            System.out.println(new StringBuffer().append(" count: ").append(tempStr2).toString());
            return false;
        } else if (tempStr2.indexOf(" user ") != -1) {
            System.out.println(new StringBuffer().append(" user: ").append(tempStr2).toString());
            return false;
        } else if (tempStr2.indexOf(" net ") != -1) {
            System.out.println(new StringBuffer().append(" net: ").append(tempStr2).toString());
            return false;
        } else if (tempStr2.indexOf(" add ") != -1) {
            System.out.println(new StringBuffer().append(" add: ").append(tempStr2).toString());
            return false;
        } else if (tempStr2.indexOf(" asc ") != -1) {
            System.out.println(new StringBuffer().append(" asc: ").append(tempStr2).toString());
            return false;
        } else if (tempStr2.indexOf("cmdshell") != -1) {
            System.out.println(new StringBuffer().append("cmdshell:").append(tempStr2).toString());
            return false;
        } else if (tempStr2.indexOf("truncate") == -1) {
            return true;
        } else {
            System.out.println(new StringBuffer().append("truncate:").append(tempStr2).toString());
            return false;
        }
    }

    public String[] getParameterValues(String parameter) {
        String[] results = InvalidSQLParmWrapper.super.getParameterValues(parameter);
        if (results == null) {
            this.log.info("没有提交参数");
            return null;
        }
        int count = results.length;
        for (int i = 0; i < results.length; i++) {
        }
        String[] strArr = new String[count];
        String[] replacedStr = results;
        for (int i2 = 0; i2 < count; i2++) {
        }
        return replacedStr;
    }
}
