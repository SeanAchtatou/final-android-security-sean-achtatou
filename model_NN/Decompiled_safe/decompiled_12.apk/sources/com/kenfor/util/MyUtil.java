package com.kenfor.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MyUtil {
    public static String gettime() {
        try {
            return new SimpleDateFormat("yyyy-MM-dd HH").format(new Date());
        } catch (Exception e) {
            return "";
        }
    }

    public static String getHour() {
        try {
            return new SimpleDateFormat("H").format(new Date());
        } catch (Exception e) {
            return "";
        }
    }

    public static String getMonth() {
        try {
            return new SimpleDateFormat("yyyy-M").format(new Date());
        } catch (Exception e) {
            return "";
        }
    }

    public static String getStrMonth() {
        try {
            return new SimpleDateFormat("M").format(new Date());
        } catch (Exception e) {
            return "";
        }
    }

    public static String getStrDay() {
        try {
            return new SimpleDateFormat("d").format(new Date());
        } catch (Exception e) {
            return "";
        }
    }

    public static String getDay() {
        try {
            return new SimpleDateFormat("yyyy-M-d").format(new Date());
        } catch (Exception e) {
            return "";
        }
    }

    public static String getWeek() {
        try {
            return new SimpleDateFormat("E").format(new Date());
        } catch (Exception e) {
            return "";
        }
    }

    public static int getStringToInt(String str, int defalut_value) {
        try {
            return Integer.valueOf(str.trim()).intValue();
        } catch (Exception e) {
            return defalut_value;
        }
    }

    public static long getStringToLong(String str, long default_value) {
        long result;
        if (!isNumber(str)) {
            return default_value;
        }
        try {
            result = Long.valueOf(str.trim()).longValue();
        } catch (Exception e) {
            result = default_value;
        }
        return result;
    }

    public static boolean isNumber(String str) {
        if (str == null) {
            return false;
        }
        boolean result = true;
        char[] t_char = str.toCharArray();
        for (char t : t_char) {
            if ((t < '0' || t > '9') && t != '.') {
                result = false;
            }
        }
        return result;
    }

    public static boolean isRealNumber(String str) {
        if (str == null) {
            return false;
        }
        boolean result = true;
        char[] t_char = str.toCharArray();
        for (char t : t_char) {
            if (t < '0' || t > '9') {
                result = false;
            }
        }
        return result;
    }
}
