package com.kenfor.util;

import java.util.Locale;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;

public class MultiLanguage {
    public static String getLocaleStr(String key, String resourceName) {
        return ResourceBundle.getBundle(resourceName, Locale.getDefault()).getString(key);
    }

    public static String getLocaleStr(String key, String resourceName, Locale cur_locale) {
        return ResourceBundle.getBundle(resourceName, cur_locale).getString(key);
    }

    public static String getUnicode(String init_str, HttpServletRequest request) {
        return init_str;
    }
}
