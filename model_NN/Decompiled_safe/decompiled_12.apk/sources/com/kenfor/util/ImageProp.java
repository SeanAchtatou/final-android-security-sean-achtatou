package com.kenfor.util;

import com.sun.image.codec.jpeg.JPEGCodec;
import java.awt.image.BufferedImage;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStream;

public class ImageProp {
    private String fileName;
    private int fileSize;
    private int imgHeight;
    private int imgWidth;
    private String path;
    private String realFileName;
    private String sys_separator;

    public int getImgHeight() {
        return this.imgHeight;
    }

    public int getImgWidth() {
        return this.imgWidth;
    }

    public int getFileSize() {
        return this.fileSize;
    }

    public ImageProp(String realFileName2) throws Exception {
        this.imgHeight = 0;
        this.imgWidth = 0;
        this.fileSize = 0;
        this.path = null;
        this.fileName = null;
        this.realFileName = null;
        this.sys_separator = "/";
        this.realFileName = realFileName2;
        Init(realFileName2);
    }

    public ImageProp(String path2, String fileName2) throws Exception {
        this.imgHeight = 0;
        this.imgWidth = 0;
        this.fileSize = 0;
        this.path = null;
        this.fileName = null;
        this.realFileName = null;
        this.sys_separator = "/";
        this.sys_separator = System.getProperty("file.separator");
        this.realFileName = new StringBuffer().append(path2).append(this.sys_separator).append(fileName2).toString();
        Init(this.realFileName);
    }

    private String getExt(String realFileName2) {
        return realFileName2.substring(realFileName2.lastIndexOf(".") + 1, realFileName2.length());
    }

    private void Init(String realFileName2) throws Exception {
        if (realFileName2 == null) {
            throw new Exception("file name and path can not be null");
        }
        String file_ext = getExt(realFileName2);
        if (file_ext.compareToIgnoreCase("BMP") == 0) {
            InputStream imgIn = new FileInputStream(realFileName2);
            DataInputStream in = new DataInputStream(imgIn);
            if (in.read() != 66) {
                throw new Exception("Not a .BMP file");
            } else if (in.read() != 77) {
                throw new Exception("Not a .BMP file");
            } else {
                this.fileSize = intelInt(in.readInt());
                in.readUnsignedShort();
                in.readUnsignedShort();
                int intelInt = intelInt(in.readInt());
                int intelInt2 = intelInt(in.readInt());
                this.imgWidth = intelInt(in.readInt());
                this.imgHeight = intelInt(in.readInt());
                in.close();
                imgIn.close();
            }
        } else if (file_ext.compareToIgnoreCase("JPEG") == 0 || file_ext.compareToIgnoreCase("JPG") == 0) {
            InputStream imgIn2 = new FileInputStream(realFileName2);
            BufferedImage image = JPEGCodec.createJPEGDecoder(imgIn2).decodeAsBufferedImage();
            this.imgHeight = image.getHeight();
            this.imgWidth = image.getWidth();
            imgIn2.close();
        } else {
            throw new Exception("the file format is not support");
        }
    }

    private static int intelInt(int i) {
        return ((i & 255) << 24) + ((65280 & i) << 8) + ((16711680 & i) >> 8) + ((i >> 24) & 255);
    }
}
