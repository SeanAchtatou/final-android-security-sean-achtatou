package com.kenfor.util.upload;

import com.kenfor.util.ProDebug;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.servlet.http.HttpServletRequest;

public class MultipartIterator {
    private static final String DEFAULT_ENCODING = "iso-8859-1";
    private static final String FILE_PREFIX = "strts";
    public static final String HEADER_CONTENT_DISPOSITION = "Content-Disposition";
    public static String HEADER_CONTENT_TYPE = "Content-Type";
    public static final String MESSAGE_CANNOT_RETRIEVE_BOUNDARY = "MultipartIterator: cannot retrieve boundary for multipart request";
    private static final String PARAMETER_BOUNDARY = "boundary=";
    private static final int TEXT_BUFFER_SIZE = 1000;
    protected String boundary;
    protected int bufferSize;
    protected int contentLength;
    protected String contentType;
    protected int diskBufferSize;
    protected long fileSize;
    protected MultipartBoundaryInputStream inputStream;
    protected boolean maxLengthExceeded;
    protected long maxSize;
    ProDebug myDebug;
    protected HttpServletRequest request;
    protected String tempDir;

    public MultipartIterator(HttpServletRequest request2) throws IOException {
        this(request2, -1);
    }

    public MultipartIterator(HttpServletRequest request2, int bufferSize2) throws IOException {
        this(request2, bufferSize2, -1);
    }

    public MultipartIterator(HttpServletRequest request2, int bufferSize2, long maxSize2) throws IOException {
        this(request2, bufferSize2, maxSize2, null);
    }

    public MultipartIterator(HttpServletRequest request2, int bufferSize2, long maxSize2, String tempDir2) throws IOException {
        this.myDebug = new ProDebug();
        this.maxSize = -1;
        this.fileSize = 0;
        this.diskBufferSize = 20480;
        this.bufferSize = 4096;
        this.request = request2;
        this.maxSize = maxSize2;
        if (bufferSize2 > -1) {
            this.bufferSize = bufferSize2;
        }
        if (tempDir2 != null) {
            this.tempDir = tempDir2;
        } else {
            this.tempDir = System.getProperty("java.io.tmpdir");
        }
        this.maxLengthExceeded = false;
        this.inputStream = new MultipartBoundaryInputStream();
        parseRequest();
    }

    /* access modifiers changed from: protected */
    public void parseRequest() throws IOException {
        getContentTypeOfRequest();
        this.contentLength = this.request.getContentLength();
        getBoundaryFromContentType();
        this.inputStream.setMaxLength((long) (this.contentLength + 1));
        if (((long) this.contentLength) > this.maxSize) {
            this.maxLengthExceeded = true;
            return;
        }
        InputStream requestInputStream = this.request.getInputStream();
        if (requestInputStream.markSupported()) {
            requestInputStream.mark(this.contentLength + 1);
        }
        this.inputStream.setBoundary(this.boundary);
        this.inputStream.setInputStream(requestInputStream);
    }

    public MultipartElement getNextElement() throws IOException {
        MultipartElement element = null;
        if (!isMaxLengthExceeded() && !this.inputStream.isFinalBoundaryEncountered()) {
            if (this.inputStream.isElementFile()) {
                element = createFileMultipartElement();
            } else {
                element = createTextMultipartElement(getElementEncoding());
            }
            this.inputStream.resetForNextBoundary();
        }
        return element;
    }

    /* access modifiers changed from: protected */
    public String getElementEncoding() {
        String encoding = this.inputStream.getElementCharset();
        if (encoding != null) {
            return encoding;
        }
        String encoding2 = this.request.getCharacterEncoding();
        if (encoding2 == null) {
            return DEFAULT_ENCODING;
        }
        return encoding2;
    }

    /* access modifiers changed from: protected */
    public MultipartElement createTextMultipartElement(String encoding) throws IOException {
        byte[] buffer = new byte[TEXT_BUFFER_SIZE];
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        while (true) {
            int read = this.inputStream.read(buffer, 0, TEXT_BUFFER_SIZE);
            if (read <= 0) {
                return new MultipartElement(this.inputStream.getElementName(), baos.toString(encoding));
            }
            baos.write(buffer, 0, read);
        }
    }

    /* access modifiers changed from: protected */
    public MultipartElement createFileMultipartElement() throws IOException {
        this.fileSize = 0;
        MultipartElement element = new MultipartElement(this.inputStream.getElementName(), this.inputStream.getElementFileName(), this.inputStream.getElementContentType(), createLocalFile());
        element.setFileSize(this.fileSize);
        return element;
    }

    public void setBufferSize(int bufferSize2) {
        this.bufferSize = bufferSize2;
    }

    public int getBufferSize() {
        return this.bufferSize;
    }

    public void setMaxSize(long maxSize2) {
        this.maxSize = maxSize2;
    }

    public long getMaxSize() {
        return this.maxSize;
    }

    public boolean isMaxLengthExceeded() {
        return this.maxLengthExceeded || this.inputStream.isMaxLengthMet();
    }

    /* access modifiers changed from: protected */
    public final void getBoundaryFromContentType() throws IOException {
        if (this.contentType.lastIndexOf(PARAMETER_BOUNDARY) != -1) {
            String _boundary = this.contentType.substring(this.contentType.lastIndexOf(PARAMETER_BOUNDARY) + 9);
            if (_boundary.endsWith("\n")) {
                this.boundary = _boundary.substring(0, _boundary.length() - 1);
            }
            this.boundary = _boundary;
        } else {
            this.boundary = null;
        }
        if (this.boundary == null || this.boundary.length() < 1) {
            throw new IOException(MESSAGE_CANNOT_RETRIEVE_BOUNDARY);
        }
    }

    /* access modifiers changed from: protected */
    public final void getContentTypeOfRequest() {
        this.contentType = this.request.getContentType();
        if (this.contentType == null) {
            this.contentType = this.request.getHeader(HEADER_CONTENT_TYPE);
        }
    }

    /* access modifiers changed from: protected */
    public File createLocalFile() throws IOException {
        File tempFile = File.createTempFile(FILE_PREFIX, null, new File(this.tempDir));
        BufferedOutputStream fos = new BufferedOutputStream(new FileOutputStream(tempFile), this.diskBufferSize);
        byte[] buffer = new byte[this.diskBufferSize];
        while (true) {
            int read = this.inputStream.read(buffer, 0, this.diskBufferSize);
            if (read <= 0) {
                fos.flush();
                fos.close();
                return tempFile;
            }
            fos.write(buffer, 0, read);
            this.fileSize += (long) read;
        }
    }

    public String getContentType() {
        return this.contentType;
    }

    public String getBoundary() {
        return this.boundary;
    }

    public int getContentLenght() {
        return this.contentLength;
    }
}
