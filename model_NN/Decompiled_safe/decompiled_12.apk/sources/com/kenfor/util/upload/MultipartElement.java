package com.kenfor.util.upload;

import java.io.File;

public class MultipartElement {
    protected String contentType;
    protected byte[] data;
    protected File file;
    protected String fileName;
    protected long fileSize;
    protected boolean isFile = false;
    protected String name;
    protected String value;

    public MultipartElement(String name2, String fileName2, String contentType2, byte[] data2) {
        this.name = name2;
        this.fileName = fileName2;
        this.contentType = contentType2;
        this.data = data2;
        if (fileName2 != null) {
            this.isFile = true;
        }
    }

    public MultipartElement(String name2, String fileName2, String contentType2, File file2) {
        this.name = name2;
        this.fileName = fileName2;
        this.contentType = contentType2;
        this.file = file2;
        this.isFile = true;
    }

    public MultipartElement(String name2, String value2) {
        this.name = name2;
        this.value = value2;
        this.isFile = false;
    }

    public String getContentType() {
        return this.contentType;
    }

    public byte[] getData() {
        return this.data;
    }

    public File getFile() {
        return this.file;
    }

    public String getName() {
        return this.name;
    }

    public String getFileName() {
        return this.fileName;
    }

    public String getValue() {
        return this.value;
    }

    public void setFile(File file2) {
        this.file = file2;
    }

    public void setFileName(String fileName2) {
        this.fileName = fileName2;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public void setContentType(String contentType2) {
        this.contentType = contentType2;
    }

    public boolean isFile() {
        if (this.file == null) {
            return false;
        }
        return true;
    }

    public void setValue(String value2) {
        this.value = value2;
    }

    public void setData(byte[] data2) {
        this.data = data2;
    }

    public void setFileSize(long fileSize2) {
        this.fileSize = fileSize2;
    }

    public long getFileSize() {
        return this.fileSize;
    }
}
