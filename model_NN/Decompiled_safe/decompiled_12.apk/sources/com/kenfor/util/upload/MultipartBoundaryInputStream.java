package com.kenfor.util.upload;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class MultipartBoundaryInputStream extends InputStream {
    private static final byte CARRIAGE_RETURN = 13;
    private static final String CONTENT_TYPE_APPLICATION_OCTET_STREAM = "application/octet-stream";
    private static final String CONTENT_TYPE_TEXT_PLAIN = "text/plain";
    private static final byte[] CRLF = {CARRIAGE_RETURN, 10};
    private static final String DEFAULT_CONTENT_DISPOSITION = "form-data";
    private static final int DEFAULT_LINE_SIZE = 4096;
    private static final String DOUBLE_DASH_STRING = "--";
    private static final String MESSAGE_INVALID_START = "Multipart data doesn't start with boundary";
    private static final byte NEWLINE_BYTE = 10;
    private static final String PARAMETER_CHARSET = "charset";
    private static final String PARAMETER_FILENAME = "filename";
    private static final String PARAMETER_NAME = "name";
    private static final char TOKEN_COLON = ':';
    private static final String TOKEN_EQUALS = "=";
    private static final char TOKEN_QUOTE = '\"';
    private static final char TOKEN_SEMI_COLON = ';';
    private static final char TOKEN_SPACE = ' ';
    protected String boundary;
    private byte[] boundaryBytes;
    protected boolean boundaryEncountered;
    protected long bytesRead;
    protected String elementCharset;
    protected String elementContentDisposition;
    protected String elementContentType;
    protected String elementFileName;
    protected String elementName;
    protected boolean endOfStream;
    private byte[] finalBoundaryBytes;
    protected boolean finalBoundaryEncountered;
    protected InputStream inputStream;
    private byte[] line;
    private boolean lineHasCarriage;
    private boolean lineHasNewline;
    private int lineIndex;
    private int lineLength;
    private int lineSize = 4096;
    protected long maxLength = -1;
    protected boolean maxLengthMet;

    public MultipartBoundaryInputStream() {
        resetStream();
    }

    public void setBoundary(String boundary2) {
        this.boundary = new StringBuffer().append(DOUBLE_DASH_STRING).append(boundary2).toString();
        this.boundaryBytes = this.boundary.getBytes();
        this.finalBoundaryBytes = new StringBuffer().append(this.boundary).append(DOUBLE_DASH_STRING).toString().getBytes();
    }

    public void resetForNextBoundary() throws IOException {
        if (!this.finalBoundaryEncountered) {
            this.boundaryEncountered = false;
            resetCrlf();
            fillLine();
            readElementHeaders();
        }
    }

    public void setInputStream(InputStream stream) throws IOException {
        this.inputStream = stream;
        resetStream();
        readFirstElement();
    }

    public int read() throws IOException {
        if (this.maxLengthMet || this.boundaryEncountered) {
            return -1;
        }
        return readFromLine();
    }

    public int read(byte[] buffer) throws IOException {
        return read(buffer, 0, buffer.length);
    }

    public int read(byte[] buffer, int offset, int length) throws IOException {
        if (length <= 0) {
            return -1;
        }
        int read = read();
        if (read == -1 && (this.endOfStream || this.boundaryEncountered)) {
            return -1;
        }
        int bytesRead2 = 1;
        int offset2 = offset + 1;
        buffer[offset] = (byte) read;
        while (bytesRead2 < length) {
            int read2 = read();
            if ((read2 == -1 && (read2 != -1 || this.boundaryEncountered)) || this.maxLengthMet) {
                break;
            }
            buffer[offset2] = (byte) read2;
            bytesRead2++;
            offset2++;
        }
        return bytesRead2;
    }

    public synchronized void mark(int i) {
        this.inputStream.mark(i);
    }

    public synchronized void reset() throws IOException {
        this.inputStream.reset();
    }

    public void setMaxLength(long maxLength2) {
        this.maxLength = maxLength2;
    }

    public long getMaxLength() {
        return this.maxLength;
    }

    public boolean isMaxLengthMet() {
        return this.maxLengthMet;
    }

    public String getElementContentDisposition() {
        return this.elementContentDisposition;
    }

    public String getElementName() {
        return this.elementName;
    }

    public String getElementCharset() {
        return this.elementCharset;
    }

    public String getElementContentType() {
        return this.elementContentType;
    }

    public String getElementFileName() {
        return this.elementFileName;
    }

    public boolean isElementFile() {
        return this.elementFileName != null;
    }

    public boolean isBoundaryEncountered() {
        return this.boundaryEncountered;
    }

    public boolean isFinalBoundaryEncountered() {
        return this.finalBoundaryEncountered;
    }

    public boolean isEndOfStream() {
        return this.endOfStream;
    }

    public void setLineSize(int size) {
        this.lineSize = size;
    }

    public long getBytesRead() {
        return this.bytesRead;
    }

    private final void readFirstElement() throws IOException {
        fillLine();
        if (!this.boundaryEncountered) {
            throw new IOException(MESSAGE_INVALID_START);
        }
        fillLine();
        readElementHeaders();
    }

    private final void readElementHeaders() throws IOException {
        readContentDisposition();
        resetCrlf();
        boolean hadContentType = readContentType();
        resetCrlf();
        if (hadContentType) {
            skipCurrentLineIfBlank();
        }
    }

    private final void readContentDisposition() throws IOException {
        String line2 = readLine();
        if (line2 != null) {
            int colonIndex = line2.indexOf(58);
            if (colonIndex != -1) {
                int firstSemiColonIndex = line2.indexOf(59);
                if (firstSemiColonIndex != -1) {
                    this.elementContentDisposition = line2.substring(colonIndex + 1, firstSemiColonIndex).trim();
                }
            } else {
                this.elementContentDisposition = DEFAULT_CONTENT_DISPOSITION;
            }
            this.elementName = parseForParameter(PARAMETER_NAME, line2);
            this.elementFileName = parseForParameter(PARAMETER_FILENAME, line2);
            if (this.elementFileName != null) {
                this.elementFileName = checkAndFixFilename(this.elementFileName);
            }
        }
    }

    private final String checkAndFixFilename(String filename) {
        String filename2 = new File(filename).getName();
        int colonIndex = filename2.indexOf(":");
        if (colonIndex == -1) {
            colonIndex = filename2.indexOf("\\\\");
        }
        int slashIndex = filename2.lastIndexOf("\\");
        if (colonIndex <= -1 || slashIndex <= -1) {
            return filename2;
        }
        return filename2.substring(slashIndex + 1, filename2.length());
    }

    private final String parseForParameter(String parameter, String parseString) {
        int startIndex;
        int nameIndex = parseString.indexOf(new StringBuffer().append(parameter).append(TOKEN_EQUALS).toString());
        if (nameIndex != -1) {
            int nameIndex2 = nameIndex + parameter.length() + 1;
            int endIndex = -1;
            if (parseString.charAt(nameIndex2) == '\"') {
                startIndex = nameIndex2 + 1;
                int endQuoteIndex = parseString.indexOf(34, startIndex);
                if (endQuoteIndex != -1) {
                    endIndex = endQuoteIndex;
                }
            } else {
                startIndex = nameIndex2;
                int spaceIndex = parseString.indexOf(32, startIndex);
                if (spaceIndex != -1) {
                    endIndex = spaceIndex;
                } else {
                    int carriageIndex = parseString.indexOf(13, startIndex);
                    if (carriageIndex != -1) {
                        endIndex = carriageIndex;
                    } else {
                        endIndex = parseString.length();
                    }
                }
            }
            if (!(startIndex == -1 || endIndex == -1)) {
                return parseString.substring(startIndex, endIndex);
            }
        }
        return null;
    }

    private final boolean readContentType() throws IOException {
        String line2 = readLine();
        if (line2 != null) {
            if (line2.length() > 2) {
                this.elementContentType = parseHeaderValue(line2);
                if (this.elementContentType == null) {
                    this.elementContentType = CONTENT_TYPE_APPLICATION_OCTET_STREAM;
                }
                this.elementCharset = parseForParameter(PARAMETER_CHARSET, line2);
                return true;
            }
            this.elementContentType = CONTENT_TYPE_TEXT_PLAIN;
        }
        return false;
    }

    private final String parseHeaderValue(String headerLine) {
        int endLineIndex;
        int colonIndex = headerLine.indexOf(58);
        if (colonIndex == -1) {
            return null;
        }
        int semiColonIndex = headerLine.indexOf(59, colonIndex);
        if (semiColonIndex != -1) {
            endLineIndex = semiColonIndex;
        } else {
            endLineIndex = headerLine.indexOf(13, colonIndex);
        }
        if (endLineIndex == -1) {
            endLineIndex = headerLine.length();
        }
        return headerLine.substring(colonIndex + 1, endLineIndex).trim();
    }

    private final void skipCurrentLineIfBlank() throws IOException {
        boolean fill = false;
        if (this.lineLength == 1) {
            if (this.line[0] == 10) {
                fill = true;
            }
        } else if (this.lineLength == 2 && equals(this.line, 0, 2, CRLF)) {
            fill = true;
        }
        if (fill && !this.endOfStream) {
            fillLine();
        }
    }

    private final void resetCrlf() {
        this.lineHasCarriage = false;
        this.lineHasNewline = false;
    }

    private final void resetStream() {
        this.line = new byte[this.lineSize];
        this.lineIndex = 0;
        this.lineLength = 0;
        this.lineHasCarriage = false;
        this.lineHasNewline = false;
        this.boundaryEncountered = false;
        this.finalBoundaryEncountered = false;
        this.endOfStream = false;
        this.maxLengthMet = false;
        this.bytesRead = 0;
    }

    private final String readLine() throws IOException {
        if (availableInLine() > 0) {
            String line2 = new String(this.line, 0, this.lineLength);
            if (this.endOfStream) {
                return line2;
            }
            fillLine();
            return line2;
        } else if (this.endOfStream) {
            return null;
        } else {
            fillLine();
            return readLine();
        }
    }

    private final int readFromLine() throws IOException {
        if (!this.boundaryEncountered) {
            if (availableInLine() > 0) {
                byte[] bArr = this.line;
                int i = this.lineIndex;
                this.lineIndex = i + 1;
                return bArr[i];
            } else if (!this.endOfStream) {
                fillLine();
                return readFromLine();
            }
        }
        return -1;
    }

    private final int availableInLine() {
        return this.lineLength - this.lineIndex;
    }

    private final void fillLine() throws IOException {
        resetLine();
        if (!this.finalBoundaryEncountered && !this.endOfStream) {
            fillLineBuffer();
            checkForBoundary();
        }
    }

    private final void resetLine() {
        this.lineIndex = 0;
    }

    private final void fillLineBuffer() throws IOException {
        int index = 0;
        if (this.lineHasCarriage) {
            this.line[0] = CARRIAGE_RETURN;
            this.lineHasCarriage = false;
            index = 0 + 1;
        }
        if (this.lineHasNewline) {
            this.line[index] = 10;
            this.lineHasNewline = false;
            index++;
        }
        while (true) {
            if (index >= this.line.length || this.maxLengthMet) {
                break;
            }
            int read = this.inputStream.read();
            byteRead();
            if (read != -1 || (read == -1 && this.inputStream.available() > 0 && !this.maxLengthMet)) {
                int index2 = index + 1;
                this.line[index] = (byte) read;
                if (read == 10) {
                    this.lineHasNewline = true;
                    index = index2 - 1;
                    if (index > 0 && this.line[index - 1] == 13) {
                        this.lineHasCarriage = true;
                        index--;
                    }
                } else {
                    index = index2;
                }
            }
        }
        this.endOfStream = true;
        this.lineLength = index;
    }

    private final void byteRead() {
        this.bytesRead++;
        if (this.maxLength > -1 && this.bytesRead >= this.maxLength) {
            this.maxLengthMet = true;
            this.endOfStream = true;
        }
    }

    private final void checkForBoundary() {
        this.boundaryEncountered = false;
        int actualLength = this.lineLength;
        if (this.line[0] == 13 || this.line[0] == 10) {
            actualLength--;
        }
        if (this.line[1] == 10) {
            actualLength--;
        }
        int startIndex = this.lineLength - actualLength;
        if (actualLength == this.boundaryBytes.length) {
            if (equals(this.line, startIndex, this.boundaryBytes.length, this.boundaryBytes)) {
                this.boundaryEncountered = true;
            }
        } else if (actualLength == this.boundaryBytes.length + 2 && equals(this.line, startIndex, this.finalBoundaryBytes.length, this.finalBoundaryBytes)) {
            this.boundaryEncountered = true;
            this.finalBoundaryEncountered = true;
            this.endOfStream = true;
        }
    }

    private final boolean equals(byte[] comp, int offset, int length, byte[] source) {
        if (length != source.length || comp.length - offset < length) {
            return false;
        }
        for (int i = 0; i < length; i++) {
            if (comp[offset + i] != source[i]) {
                return false;
            }
        }
        return true;
    }
}
