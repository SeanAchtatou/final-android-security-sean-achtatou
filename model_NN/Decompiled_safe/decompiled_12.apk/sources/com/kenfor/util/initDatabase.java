package com.kenfor.util;

import com.kenfor.database.PoolBean;
import java.io.File;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.sql.DataSource;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.SessionFactory;
import net.sf.hibernate.cfg.Configuration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.logicalcobwebs.proxool.ProxoolFacade;

public class initDatabase extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=GBK";
    private Context ctx = null;
    private String jndi_name;
    Log log = LogFactory.getLog(getClass().getName());
    String poolType = null;
    private SessionFactory sessionFactory;
    String sys_name = null;

    public void init() throws ServletException {
        String real_path;
        String real_path2;
        if (this.log.isInfoEnabled()) {
            this.log.info("start");
        }
        this.poolType = getServletConfig().getInitParameter("poolType");
        String poolName = getServletConfig().getInitParameter("poolName");
        String path = getServletContext().getRealPath("/WEB-INF/classes/");
        if (this.poolType == null) {
            this.poolType = "1";
        }
        if (this.poolType != null && "0".equals(this.poolType)) {
            String sep = System.getProperty("file.separator");
            if (path.endsWith("/") || path.endsWith("\\")) {
                real_path = new StringBuffer().append(path).append("proxool.xml").toString();
            } else {
                real_path = new StringBuffer().append(path).append(sep).append("proxool.xml").toString();
            }
            if (!new File(real_path).exists()) {
                path = getServletContext().getRealPath("/WEB-INF/");
                String sep2 = System.getProperty("file.separator");
                if (path.endsWith("/") || path.endsWith("\\")) {
                    real_path2 = new StringBuffer().append(path).append("proxool.xml").toString();
                } else {
                    real_path2 = new StringBuffer().append(path).append(sep2).append("proxool.xml").toString();
                }
                if (!new File(real_path2).exists()) {
                }
            }
        }
        PoolBean pool = (PoolBean) getServletContext().getAttribute("pool");
        boolean newPool = false;
        if (pool == null) {
            pool = new PoolBean();
            newPool = true;
        }
        String t_id = getServletConfig().getInitParameter("trade_id");
        String webapp_config = getServletConfig().getInitParameter("webapp_config");
        this.jndi_name = getServletConfig().getInitParameter("jndi_name");
        String initHiberante = getServletConfig().getInitParameter("initHiberante");
        String h_config_name = getServletConfig().getInitParameter("h_config_name");
        String html_flag = getServletConfig().getInitParameter("html_flag");
        String systemName = getServletConfig().getInitParameter("systemName");
        String version = getServletConfig().getInitParameter("version");
        if (this.log.isInfoEnabled()) {
            this.log.info(new StringBuffer().append("version:").append(version).toString());
        }
        int pool_type = MyUtil.getStringToInt(this.poolType, 1);
        if (this.log.isDebugEnabled()) {
            this.log.debug(new StringBuffer().append("pool_type:").append(pool_type).toString());
        }
        pool.setTrade_id(t_id);
        this.log.warn("this is initDatabase");
        if (this.log.isDebugEnabled()) {
            this.log.debug(new StringBuffer().append("set trade_id at initDatabase,trade_id:").append(t_id).toString());
        }
        if ("true".equalsIgnoreCase(webapp_config)) {
            try {
                Context ctx2 = (Context) new InitialContext().lookup("java:comp/env");
                if (ctx2 == null) {
                    throw new Exception("Boom - No Context");
                }
                DataSource ds = (DataSource) ctx2.lookup(this.jndi_name);
                if (ds == null) {
                    this.log.error("get datasource is null");
                    throw new ServletException("get datasource is null");
                } else {
                    pool.setBDS(ds);
                    this.log.info("set datasource to pool at initDatabase");
                }
            } catch (Exception e) {
                this.log.error(new StringBuffer().append("get datasource occur exception").append(e.getMessage()).toString());
                throw new ServletException("get datasource occur exception");
            }
        } else {
            try {
                if (!pool.isStarted()) {
                    if (this.log.isInfoEnabled()) {
                        this.log.info(new StringBuffer().append("pool_type:").append(pool_type).toString());
                    }
                    pool.setPoolType(pool_type);
                    System.out.println(new StringBuffer().append("Path: ").append(path).toString());
                    this.log.info(new StringBuffer().append("Path : ").append(path).toString());
                    pool.setPath(path);
                    if (this.log.isInfoEnabled()) {
                        this.log.info(new StringBuffer().append("poolName:").append(poolName).toString());
                    }
                    if (poolName != null) {
                        pool.setPoolName(poolName);
                    }
                    pool.setVersion(version);
                    if (this.log.isInfoEnabled()) {
                        this.log.info(new StringBuffer().append("html_flag:").append(html_flag).toString());
                    }
                    if (html_flag != null) {
                        pool.setHtml_flag(html_flag);
                    }
                    if (this.log.isInfoEnabled()) {
                        this.log.info(new StringBuffer().append("systemName:").append(systemName).toString());
                    }
                    if (systemName != null) {
                        pool.setSystemName(systemName);
                    }
                    if (this.log.isInfoEnabled()) {
                        this.log.info(new StringBuffer().append("jndi_name:").append(this.jndi_name).toString());
                    }
                    pool.setJndiName(this.jndi_name);
                    if (this.log.isInfoEnabled()) {
                        this.log.info("start initializePool");
                    }
                    pool.initializePool();
                    this.log.info("init pool ok");
                } else if (this.log.isInfoEnabled()) {
                    this.log.info("pool is started");
                }
            } catch (Exception e2) {
                e2.printStackTrace();
                this.log.error(e2.getMessage());
                throw new ServletException(new StringBuffer().append("initDatabase exception when initializePool():").append(e2.getMessage()).toString());
            }
        }
        if (newPool) {
            getServletContext().setAttribute("pool", pool);
        }
        if ("true".equalsIgnoreCase(initHiberante)) {
            if (this.log.isDebugEnabled()) {
                this.log.debug(new StringBuffer().append("start config hibernate,h_config_name:").append(h_config_name).toString());
            }
            if (h_config_name != null) {
                try {
                    this.sessionFactory = new Configuration().configure(h_config_name).buildSessionFactory();
                } catch (HibernateException ex) {
                    this.log.error(new StringBuffer().append("HibernateException building SessionFactory: ").append(ex.getMessage()).toString());
                    throw new RuntimeException(new StringBuffer().append("HibernateException building SessionFactory: ").append(ex.getMessage()).toString(), ex);
                } catch (Exception e3) {
                    this.log.error(new StringBuffer().append("Exception building SessionFactory: ").append(e3.getMessage()).toString());
                    throw new RuntimeException(new StringBuffer().append("Exception building SessionFactory: ").append(e3.getMessage()).toString());
                }
            } else {
                this.sessionFactory = new Configuration().configure().buildSessionFactory();
            }
            if (this.log.isDebugEnabled()) {
                this.log.debug("end config hibernate");
            }
            try {
                this.ctx = new InitialContext();
                if (this.log.isDebugEnabled()) {
                    this.log.debug("bind sessionFactory to context by :HibSessionFactory");
                }
                this.ctx.rebind("HibSessionFactory", this.sessionFactory);
            } catch (NamingException ex2) {
                this.log.error(new StringBuffer().append("Exception binding SessionFactory to JNDI: ").append(ex2.getMessage()).toString());
                throw new RuntimeException(new StringBuffer().append("Exception binding SessionFactory to JNDI: ").append(ex2.getMessage()).toString(), ex2);
            }
        }
    }

    public void destroy() {
        getServletContext().removeAttribute("pool");
        this.log.info("destroy");
        if (this.ctx != null) {
            try {
                this.ctx.unbind("HibSessionFactory");
                if ("1".equals(this.poolType)) {
                    this.ctx.unbind(this.jndi_name);
                }
            } catch (NamingException e) {
                e.printStackTrace();
            }
        }
        if (this.sessionFactory != null) {
            try {
                this.sessionFactory.close();
            } catch (HibernateException e2) {
                e2.printStackTrace();
            }
            this.sessionFactory = null;
        }
        this.ctx = null;
        new ProxoolFacade();
        ProxoolFacade.shutdown(1);
        initDatabase.super.destroy();
    }
}
