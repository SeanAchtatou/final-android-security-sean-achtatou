package com.kenfor.util;

public class Constants {
    public static final String DATABASE_KEY = "database";
    public static final String ERROR_KEY = "kf_error";
    public static final String USER_KEY = "user";
}
