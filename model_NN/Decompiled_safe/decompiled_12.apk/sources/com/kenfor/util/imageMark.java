package com.kenfor.util;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGEncodeParam;
import com.sun.image.codec.jpeg.JPEGImageEncoder;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class imageMark {
    private static Color[] color = {Color.red, Color.black, Color.white, Color.yellow, Color.orange, Color.green};
    private static int stat_X = 0;
    private static int stat_Y = 0;
    String ext = "png";

    private imageMark() {
    }

    private static Image compress(String file_path, int width, int height) {
        FileInputStream fin;
        BufferedImage Bi = null;
        if (width < 400) {
            try {
                try {
                    fin = new FileInputStream(file_path.replaceFirst(".png", "-s.png"));
                } catch (Exception e) {
                    fin = new FileInputStream(file_path);
                }
            } catch (Exception e2) {
            }
        } else {
            fin = new FileInputStream(file_path);
        }
        Bi = ImageIO.read(fin);
        fin.close();
        int in_h = Bi.getHeight();
        int in_w = Bi.getWidth();
        float Ratio = getRatio(width, height, in_w, in_h);
        int int_s_w = (int) ((double) (((float) in_w) * Ratio));
        Bi.createGraphics().rotate(Math.toRadians(30.0d));
        Image scaledInstance = Bi.getScaledInstance(int_s_w, (int) ((double) (((float) in_h) * Ratio)), 4);
        return new AffineTransformOp(AffineTransform.getScaleInstance((double) Ratio, (double) Ratio), (RenderingHints) null).filter(Bi, (BufferedImage) null);
    }

    private static Image compress(BufferedImage bufImage, float Ratio) {
        if (bufImage == null) {
            return null;
        }
        BufferedImage Bi = bufImage;
        int in_h = Bi.getHeight();
        int in_w = Bi.getWidth();
        int int_s_h = getIntValue(String.valueOf((double) (((float) in_h) * Ratio)), in_h);
        Image scaledInstance = Bi.getScaledInstance(getIntValue(String.valueOf((double) (((float) in_w) * Ratio)), in_w), int_s_h, 4);
        return new AffineTransformOp(AffineTransform.getScaleInstance((double) Ratio, (double) Ratio), (RenderingHints) null).filter(Bi, (BufferedImage) null);
    }

    private static int getIntValue(String int_value, int default_value) {
        try {
            return Integer.valueOf(int_value).intValue();
        } catch (Exception e) {
            return default_value;
        }
    }

    private static int[] getMidPels(int X, int Y, int Xw, int Yw) {
        int[] stat_cood = {0, 0};
        if (X >= Xw && Y >= Yw) {
            stat_cood[0] = (X / 2) - (Xw / 2);
            stat_cood[1] = (Y / 2) - (Yw / 2);
        }
        return stat_cood;
    }

    private static int[] getMidPels(int X, int Y, Image img) {
        int[] stat_cood = {0, 0};
        int Xw = img.getWidth((ImageObserver) null);
        int Yw = img.getHeight((ImageObserver) null);
        if (X >= Xw) {
            stat_cood[0] = (X / 2) - (Xw / 2);
        }
        if (Y >= Yw) {
            stat_cood[1] = (Y / 2) - (Yw / 2);
        }
        return stat_cood;
    }

    private static int[] getMidDownPels(int X, int Y, int X1, int Y1, Image img) {
        int[] stat_cood = {0, 0};
        int Xw = img.getWidth((ImageObserver) null);
        int Yw = img.getHeight((ImageObserver) null);
        if (X >= Xw) {
            stat_cood[0] = (X / 2) - (Xw / 2);
        }
        if (Y >= Yw) {
            stat_cood[1] = ((Y + Y1) / 2) - Yw;
        }
        return stat_cood;
    }

    private static int[] getMidDownPels(int X, int Y, Image img) {
        int[] stat_cood = {0, 0};
        int Xw = img.getWidth((ImageObserver) null);
        int Yw = img.getHeight((ImageObserver) null);
        if (X >= Xw) {
            stat_cood[0] = (X / 2) - (Xw / 2);
        }
        if (Y >= Yw) {
            stat_cood[1] = Y - Yw;
        }
        return stat_cood;
    }

    private static int[] getLeftDownPels(int X, int Y, Image img) {
        int[] stat_cood = {0, 0};
        int Xw = img.getWidth((ImageObserver) null);
        int Yw = img.getHeight((ImageObserver) null);
        if (X >= Xw) {
            stat_cood[0] = (int) (0.0d + (((double) X) * 0.02345d));
        }
        if (Y >= Yw) {
            stat_cood[1] = (int) (((double) (Y - Yw)) - (((double) Y) * 0.02345d));
        }
        return stat_cood;
    }

    private static int[] getRightDownPels(int X, int Y, Image img) {
        int Xend;
        int Yend;
        int[] stat_cood = {0, 0};
        int Xw = img.getWidth((ImageObserver) null);
        int Yw = img.getHeight((ImageObserver) null);
        if (X >= Xw && Y >= Yw) {
            if (X - Xw <= ((int) (((double) X) * 0.02345d)) || Y - Yw <= ((int) (((double) Y) * 0.02345d))) {
                Xend = X - Xw;
                Yend = Y - Yw;
            } else {
                Xend = (int) (((double) (X - Xw)) - (((double) X) * 0.02345d));
                Yend = (int) (((double) (Y - Yw)) - (((double) Y) * 0.02345d));
            }
            stat_cood[0] = Xend;
            stat_cood[1] = Yend;
        }
        return stat_cood;
    }

    private static float getRatio(int X, int Y, int Xw, int Yw) {
        float x_r = ((float) ((X * 4) / 5)) / ((float) Xw);
        float y_r = ((float) ((Y * 4) / 5)) / ((float) Yw);
        if (x_r > 1.0f && y_r > 1.0f) {
            return 1.0f;
        }
        if (x_r >= y_r) {
            return y_r;
        }
        return x_r;
    }

    public static boolean createMark(BufferedImage sour_image, int set_width, int set_height, int bk_color, String sour_filepath, String watermark_path, String position) throws IOException {
        int width = sour_image.getWidth();
        int height = sour_image.getHeight();
        BufferedImage bimage = new BufferedImage(width, height, 1);
        Image Itemp = compress(watermark_path, set_width, set_height);
        Graphics2D g = bimage.createGraphics();
        g.setBackground(color[bk_color]);
        g.drawImage(sour_image, 0, 0, (ImageObserver) null);
        int[] iArr = new int[2];
        int Xcoord = 0;
        int Ycoord = 0;
        if ("mid".equalsIgnoreCase(position)) {
            int[] pits = getMidPels(width, height, Itemp);
            Xcoord = pits[0];
            Ycoord = pits[1];
        }
        if ("left".equalsIgnoreCase(position)) {
            int[] pits2 = getLeftDownPels(width, height, Itemp);
            Xcoord = pits2[0];
            Ycoord = pits2[1];
        }
        if ("right".equalsIgnoreCase(position)) {
            int[] pits3 = getRightDownPels(width, height, Itemp);
            Xcoord = pits3[0];
            Ycoord = pits3[1];
        }
        if ("down".equalsIgnoreCase(position)) {
            int[] pits4 = getMidDownPels(width, height, set_width, set_height, Itemp);
            Xcoord = pits4[0];
            Ycoord = pits4[1];
        }
        g.drawImage(Itemp, Xcoord, Ycoord, (ImageObserver) null);
        g.dispose();
        FileOutputStream out = new FileOutputStream(sour_filepath);
        JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
        JPEGEncodeParam param = encoder.getDefaultJPEGEncodeParam(bimage);
        param.setQuality(0.95f, true);
        encoder.encode(bimage, param);
        out.close();
        Itemp.flush();
        bimage.flush();
        return true;
    }

    public static boolean createMark(BufferedImage sour_image, String sour_filepath, String watermark_path, String position) throws IOException {
        return createMark(sour_image, sour_image.getWidth(), sour_image.getHeight(), 2, sour_filepath, watermark_path, position);
    }

    public static boolean createMark(String filePath, String watermark, String position) {
        Image theImg = new ImageIcon(filePath).getImage();
        int width = theImg.getWidth((ImageObserver) null);
        int height = theImg.getHeight((ImageObserver) null);
        BufferedImage bimage = new BufferedImage(width, height, 1);
        Image Itemp = compress(watermark, width, height);
        Graphics2D g = bimage.createGraphics();
        g.setBackground(Color.white);
        g.drawImage(theImg, 0, 0, (ImageObserver) null);
        int[] iArr = new int[2];
        int Xcoord = 0;
        int Ycoord = 0;
        if ("mid".equalsIgnoreCase(position)) {
            int[] pits = getMidPels(width, height, Itemp);
            Xcoord = pits[0];
            Ycoord = pits[1];
        }
        if ("left".equalsIgnoreCase(position)) {
            int[] pits2 = getLeftDownPels(width, height, Itemp);
            Xcoord = pits2[0];
            Ycoord = pits2[1];
        }
        if ("right".equalsIgnoreCase(position)) {
            int[] pits3 = getRightDownPels(width, height, Itemp);
            Xcoord = pits3[0];
            Ycoord = pits3[1];
        }
        g.drawImage(Itemp, Xcoord, Ycoord, (ImageObserver) null);
        g.rotate(Math.toRadians(30.0d));
        g.dispose();
        try {
            FileOutputStream out = new FileOutputStream(filePath);
            JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
            JPEGEncodeParam param = encoder.getDefaultJPEGEncodeParam(bimage);
            param.setQuality(0.5f, true);
            encoder.encode(bimage, param);
            out.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
