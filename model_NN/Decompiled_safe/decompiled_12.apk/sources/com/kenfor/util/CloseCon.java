package com.kenfor.util;

import java.sql.Connection;
import java.sql.SQLException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class CloseCon {
    public static void Close(Connection con) {
        Log log = LogFactory.getLog("CloseCon");
        if (con != null) {
            try {
                if (!con.isClosed()) {
                    con.close();
                }
            } catch (SQLException ee) {
                log.debug(ee.getMessage());
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        }
    }
}
