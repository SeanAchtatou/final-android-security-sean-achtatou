package com.kenfor.util;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

public class SoftHashMap extends AbstractMap {
    private final int HARD_SIZE;
    private final LinkedList hardCache;
    private final Map hash;
    private final ReferenceQueue queue;

    /* renamed from: com.kenfor.util.SoftHashMap$1  reason: invalid class name */
    class AnonymousClass1 {
    }

    public SoftHashMap() {
        this(100);
    }

    public SoftHashMap(int hardSize) {
        this.hash = new HashMap();
        this.hardCache = new LinkedList();
        this.queue = new ReferenceQueue();
        this.HARD_SIZE = hardSize;
    }

    public Object get(Object key) {
        Object result = null;
        SoftReference soft_ref = (SoftReference) this.hash.get(key);
        if (soft_ref != null) {
            result = soft_ref.get();
            if (result == null) {
                this.hash.remove(key);
            } else {
                this.hardCache.addFirst(result);
                if (this.hardCache.size() > this.HARD_SIZE) {
                    this.hardCache.removeLast();
                }
            }
        }
        return result;
    }

    private static class SoftValue extends SoftReference {
        private final Object key;

        SoftValue(Object x0, Object x1, ReferenceQueue x2, AnonymousClass1 x3) {
            this(x0, x1, x2);
        }

        static Object access$000(SoftValue x0) {
            return x0.key;
        }

        private SoftValue(Object k, Object key2, ReferenceQueue q) {
            super(k, q);
            this.key = key2;
        }
    }

    private void processQueue() {
        while (true) {
            SoftValue sv = (SoftValue) this.queue.poll();
            if (sv != null) {
                this.hash.remove(SoftValue.access$000(sv));
            } else {
                return;
            }
        }
    }

    public Object put(Object key, Object value) {
        processQueue();
        return this.hash.put(key, new SoftValue(value, key, this.queue, null));
    }

    public Object remove(Object key) {
        processQueue();
        return this.hash.remove(key);
    }

    public void clear() {
        this.hardCache.clear();
        processQueue();
        this.hash.clear();
    }

    public int size() {
        processQueue();
        return this.hash.size();
    }

    public Set entrySet() {
        return null;
    }
}
