package com.kenfor.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.upload.FormFile;

public class strutsUploadFile {
    protected boolean autoDir = true;
    protected int diskBufferSize = 10240;
    protected String error_msg = null;
    protected String fileName = null;
    protected String file_path;
    protected String isDebug = "false";
    Log log = LogFactory.getLog(getClass().getName());
    protected long maxSize = 204800;
    protected String newDir = "";
    protected String oldFileName;
    protected String realPath = null;
    protected String thisFileName = null;
    protected FormFile upFormFile;

    public void setMaxSize(long maxSize2) {
        this.maxSize = maxSize2;
    }

    public long getMaxSize() {
        return this.maxSize;
    }

    public void setAutoDir(boolean autoDir2) {
        this.autoDir = autoDir2;
    }

    public boolean getAutoDir() {
        return this.autoDir;
    }

    public String getError_msg() {
        return this.error_msg;
    }

    public strutsUploadFile(FormFile newFile, String file_path2, String fileName2) throws IOException {
        initUploadFile(newFile, file_path2, fileName2);
    }

    public strutsUploadFile(FormFile newFile, String file_path2, String fileName2, String isDebug2) throws IOException {
        initUploadFile(newFile, file_path2, fileName2);
        this.isDebug = isDebug2;
    }

    public strutsUploadFile(FormFile newFile, String file_path2) throws IOException {
        initUploadFile(newFile, file_path2, null);
    }

    public strutsUploadFile(FormFile newFile) throws IOException {
        initUploadFile(newFile, System.getProperty("java.io.tmpdir"), null);
    }

    public strutsUploadFile() {
    }

    public String getFileName() {
        char[] temps = this.fileName.toCharArray();
        for (int i = 0; i < temps.length; i++) {
            if (temps[i] == '\\') {
                temps[i] = '/';
            }
        }
        return String.valueOf(temps);
    }

    public String getRealPath() {
        return this.realPath;
    }

    public void doUploadFile() throws IOException {
        if (this.upFormFile == null) {
            throw new IOException("upFormFile can not be null,please set it first!");
        } else if (this.file_path == null) {
            throw new IOException("file_path can not be null,please set it first!");
        } else {
            initUploadFile(this.upFormFile, this.file_path, this.fileName);
        }
    }

    /* access modifiers changed from: protected */
    public String initUploadFile(FormFile newFile, String file_path2, String fileName2) throws IOException {
        if (newFile == null) {
            return null;
        }
        long fileSize = (long) newFile.getFileSize();
        if (fileSize <= 0) {
            return null;
        }
        if (fileSize > this.maxSize) {
            this.error_msg = new StringBuffer().append("上传的文件的大小超过最大值，允许上传的最大值为：").append(this.maxSize / 1024).append("K").toString();
            throw new IOException(this.error_msg);
        }
        this.oldFileName = newFile.getFileName();
        this.realPath = getRealPath(file_path2, fileName2, this.oldFileName);
        InputStream in = newFile.getInputStream();
        BufferedOutputStream fos = new BufferedOutputStream(new FileOutputStream(new File(this.realPath)), this.diskBufferSize);
        byte[] buffer = new byte[this.diskBufferSize];
        while (true) {
            int read = in.read(buffer, 0, this.diskBufferSize);
            if (read <= 0) {
                fos.flush();
                fos.close();
                this.thisFileName = this.fileName;
                in.close();
                newFile.destroy();
                return this.fileName;
            }
            fos.write(buffer, 0, read);
        }
    }

    /* access modifiers changed from: protected */
    public String getExt(String fileName2) {
        return fileName2.substring(fileName2.indexOf(".") + 1, fileName2.length());
    }

    /* access modifiers changed from: protected */
    public String createAutoDir() {
        String str_date = new SimpleDateFormat("yyyyMMdd").format(new Date());
        int len = str_date.length();
        String str_dd = "01";
        if (len == 8) {
            str_dd = str_date.substring(6, 8);
        }
        if (len > 6) {
            str_date = str_date.substring(0, 6);
        }
        String str_file = new StringBuffer().append(str_date).append(System.getProperty("file.separator")).append(str_dd).toString();
        this.newDir = str_file;
        return str_file;
    }

    /* access modifiers changed from: protected */
    public String getRealPath(String file_path2, String fileName2, String oldFileName2) {
        String tempFileName;
        String tempRealPath;
        String newPath;
        String extName = ".jpg";
        if (oldFileName2 != null) {
            try {
                if (oldFileName2.length() > 0) {
                    extName = oldFileName2.substring(oldFileName2.lastIndexOf("."), oldFileName2.length());
                }
            } catch (Exception e) {
                this.log.error(new StringBuffer().append("oldFileName:").append(oldFileName2).toString());
            }
        }
        if (".jpeg".equalsIgnoreCase(extName) || ".jpg".equalsIgnoreCase(extName)) {
            extName = ".jpg";
        } else if (".gif".equalsIgnoreCase(extName)) {
            extName = ".gif";
        } else if (".png".equalsIgnoreCase(extName)) {
            extName = ".png";
        }
        if (fileName2 == null || fileName2.length() < 1) {
            tempFileName = new StringBuffer().append(String.valueOf((1000 * System.currentTimeMillis()) + ((long) ((int) ((Math.random() * 1000.0d) + 1.0d))))).append(extName).toString();
        } else {
            tempFileName = fileName2;
        }
        this.fileName = tempFileName;
        String sys_separator = System.getProperty("file.separator");
        if (this.autoDir) {
            String tempPath = createAutoDir();
            if ("\\".equals(file_path2.substring(file_path2.length() - 1, file_path2.length())) || "/".equals(file_path2.substring(file_path2.length() - 1, file_path2.length()))) {
                newPath = new StringBuffer().append(file_path2).append(tempPath).toString();
            } else {
                newPath = new StringBuffer().append(file_path2).append(sys_separator).append(tempPath).toString();
            }
            if (newPath != null) {
                File f = new File(newPath);
                if (!f.exists()) {
                    f.mkdirs();
                }
                file_path2 = newPath;
            }
        }
        if ("\\".equals(file_path2.substring(file_path2.length() - 1, file_path2.length())) || "/".equals(file_path2.substring(file_path2.length() - 1, file_path2.length()))) {
            tempRealPath = new StringBuffer().append(file_path2).append(tempFileName).toString();
        } else {
            tempRealPath = new StringBuffer().append(file_path2).append(sys_separator).append(tempFileName).toString();
        }
        this.fileName = new StringBuffer().append(this.newDir).append(sys_separator).append(this.fileName).toString();
        return tempRealPath;
    }

    public void setRealPath(String realPath2) {
        this.realPath = realPath2;
    }

    public void setFileName(String fileName2) {
        this.fileName = fileName2;
    }

    public FormFile getUpFormFile() {
        return this.upFormFile;
    }

    public void setUpFormFile(FormFile upFormFile2) {
        this.upFormFile = upFormFile2;
    }

    public String getFile_path() {
        return this.file_path;
    }

    public void setFile_path(String file_path2) {
        this.file_path = file_path2;
    }

    public String getOldFileName() {
        return this.oldFileName;
    }
}
