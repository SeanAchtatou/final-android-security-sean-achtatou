package com.kenfor.util;

import java.io.UnsupportedEncodingException;
import org.xbill.DNS.Type;
import org.xbill.DNS.WKSRecord;

public class CodeTransfer {
    private String ENCODE_KEY1 = "zxcvbnm,./asdfg";
    private String ENCODE_KEY2 = "hjkl;'qwertyuiop";
    private String ENCODE_KEY3 = "[]\\1234567890-";
    private String ENCODE_KEY4 = "=` ZXCVBNM<>?:LKJ";
    private String ENCODE_KEY5 = "HGUAQWSRTFYEDI";
    private String ENCODE_KEY6 = "OP%$}|#_)(*{+^&@!~";
    private String ENCODE_KEY_A = "cjk;";
    private String ENCODE_KEY_B = "cai2";
    private String ENCODE_KEY_C = "%^@#";
    private String ENCODE_KEY_D = "*(N";
    private String ENCODE_KEY_E = "%^HJ";

    public String decryption(String varCode) throws Exception {
        String DecodeStr;
        try {
            if (varCode.length() == 0) {
                return "";
            }
            String strKey = new StringBuffer().append(this.ENCODE_KEY1).append(this.ENCODE_KEY2).append(this.ENCODE_KEY3).append(this.ENCODE_KEY4).append(this.ENCODE_KEY5).append(this.ENCODE_KEY6).toString();
            if (varCode.length() % 2 == 1) {
                varCode = new StringBuffer().append(varCode).append("?").toString();
            }
            String des = "";
            int midNum = Math.round((float) ((varCode.length() / 2) - 1));
            for (int n = 0; n <= midNum; n++) {
                StringBuffer append = new StringBuffer().append(des);
                des = append.append((char) (varCode.charAt(n * 2) ^ strKey.indexOf(varCode.charAt((n * 2) + 1)))).toString();
            }
            int n2 = des.indexOf((char) 1);
            if (n2 >= 0) {
                DecodeStr = des.substring(0, n2);
            } else {
                DecodeStr = des;
            }
            return DecodeStr;
        } catch (Exception ex) {
            DecodeStr = new StringBuffer().append("Exception/Message:").append(ex.getMessage()).toString();
        }
    }

    public String encryption(String sSource) throws Exception {
        String EncodeStr;
        int code;
        try {
            if (sSource.length() == 0) {
                return "";
            }
            String strKey = new StringBuffer().append(this.ENCODE_KEY1).append(this.ENCODE_KEY2).append(this.ENCODE_KEY3).append(this.ENCODE_KEY4).append(this.ENCODE_KEY5).append(this.ENCODE_KEY6).toString();
            char onestr = (char) 1;
            while (sSource.length() < 8) {
                sSource = new StringBuffer().append(sSource).append(onestr).toString();
            }
            String des = "";
            for (int n = 1; n <= sSource.length(); n++) {
                while (true) {
                    code = (int) Math.round(100.0d * Math.random());
                    int num1 = sSource.charAt(n - 1);
                    while (code > 0 && ((code ^ num1) < 0 || (code ^ num1) > 90)) {
                        code--;
                    }
                    char charcode = (char) code;
                    if (code > 35 && code < 122 && charcode != '|' && charcode != '\'' && charcode != ',' && strKey.charAt(code ^ num1) != '|' && strKey.charAt(code ^ num1) != '\'' && strKey.charAt(code ^ num1) != ',') {
                        break;
                    }
                }
                des = new StringBuffer().append(des).append((char) code).append(strKey.charAt(code ^ sSource.charAt(n - 1))).toString();
            }
            EncodeStr = des;
            return EncodeStr;
        } catch (Exception ex) {
            EncodeStr = new StringBuffer().append("Exception/Message:").append(ex.getMessage()).toString();
        }
    }

    public static String gbToUnicode(String s) {
        if (s == null) {
            return s;
        }
        try {
            return new String(s.getBytes("ISO8859_1"), "gb2312");
        } catch (UnsupportedEncodingException e) {
            return s;
        }
    }

    public static String unicodeTogb(String s) {
        if (s == null) {
            return s;
        }
        try {
            return new String(s.getBytes("gb2312"), "ISO8859_1");
        } catch (UnsupportedEncodingException e) {
            return s;
        }
    }

    public static String gbkToUnicode(String s) {
        if (s == null) {
            return s;
        }
        try {
            return new String(s.getBytes("ISO8859_1"), "GBK");
        } catch (UnsupportedEncodingException e) {
            return s;
        }
    }

    public static String unicodeTogbk(String s) {
        if (s == null) {
            return s;
        }
        try {
            return new String(s.getBytes("GBK"), "ISO8859_1");
        } catch (UnsupportedEncodingException e) {
            return s;
        }
    }

    public static String UnicodeToISO(String s) {
        if (s == null) {
            return s;
        }
        try {
            return new String(s.getBytes("UTF-8"), "ISO8859_1");
        } catch (UnsupportedEncodingException e) {
            return s;
        }
    }

    public static String ISOToUnicode(String s) {
        if (s == null) {
            return s;
        }
        try {
            return new String(s.getBytes("ISO8859_1"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return s;
        }
    }

    public static String big5ToUnicode(String s) {
        if (s == null) {
            return s;
        }
        try {
            return new String(s.getBytes("ISO8859_1"), "BIG5");
        } catch (UnsupportedEncodingException e) {
            return s;
        }
    }

    public static String unicodeTobig5(String s) {
        if (s == null) {
            return s;
        }
        try {
            return new String(s.getBytes("BIG5"), "ISO8859_1");
        } catch (UnsupportedEncodingException e) {
            return s;
        }
    }

    public static String toHexString(String s) {
        if (s == null) {
            return s;
        }
        String str = "";
        for (int i = 0; i < s.length(); i++) {
            String s4 = new StringBuffer().append("0000").append(Integer.toHexString(s.charAt(i))).toString();
            str = new StringBuffer().append(str).append(s4.substring(s4.length() - 4)).append(" ").toString();
        }
        return str;
    }

    public static String toHtmlString(String value) {
        if (value == null) {
            return null;
        }
        char[] content = new char[value.length()];
        value.getChars(0, value.length(), content, 0);
        StringBuffer result = new StringBuffer(content.length + 50);
        for (int i = 0; i < content.length; i++) {
            switch (content[i]) {
                case 10:
                    result.append("<br>");
                    break;
                case Type.ATMA /*34*/:
                    result.append("&quot;");
                    break;
                case Type.A6 /*38*/:
                    result.append("&amp;");
                    break;
                case '\'':
                    result.append("&#39;");
                    break;
                case '<':
                    result.append("&lt;");
                    break;
                case WKSRecord.Protocol.CFTP /*62*/:
                    result.append("&gt;");
                    break;
                default:
                    result.append(content[i]);
                    break;
            }
        }
        return result.toString();
    }
}
