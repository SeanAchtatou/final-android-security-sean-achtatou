package com.kenfor.util;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.xbill.DNS.KEYRecord;

public class SetCharacterEncodingFilter implements Filter {
    protected String encoding = null;
    protected int fileMaxLength = 204800;
    protected FilterConfig filterConfig = null;
    protected boolean ignore = true;
    Log log = LogFactory.getLog(getClass().getName());

    public void destroy() {
        this.encoding = null;
        this.filterConfig = null;
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String encoding2;
        HttpServletRequest t_re = (HttpServletRequest) request;
        String re_uri = trimString(t_re.getRequestURI());
        String re_host = trimString(t_re.getServerName());
        String qu_str = trimString(t_re.getQueryString());
        if (re_uri.indexOf(".do") > 0 && this.log.isDebugEnabled()) {
            this.log.debug(new StringBuffer().append(re_host).append(re_uri).append("?").append(qu_str).toString());
        }
        try {
            if ((this.ignore || request.getCharacterEncoding() == null) && (encoding2 = selectEncoding(request)) != null) {
                request.setCharacterEncoding(encoding2);
            }
            chain.doFilter(request, response);
        } catch (Exception e) {
            String error_mes = e.getMessage();
            String strServerName = null;
            String strQueryString = null;
            String requestUrl = null;
            String remoteAddr = null;
            if (error_mes.indexOf("reset") < 0) {
                String referer = "";
                try {
                    strServerName = t_re.getServerName();
                    strQueryString = t_re.getQueryString();
                    requestUrl = t_re.getRequestURI();
                    referer = t_re.getHeader("referer");
                    remoteAddr = t_re.getRemoteAddr();
                } catch (Exception e2) {
                }
                if (error_mes != null && !"null".equals(error_mes) && error_mes.length() > 0) {
                    this.log.error(new StringBuffer().append(remoteAddr).append(",exception:").append(error_mes).toString());
                }
                if (referer != null && referer.length() > 0) {
                    this.log.error(new StringBuffer().append("referer:").append(referer).toString());
                }
                if (strServerName != null && strQueryString != null) {
                    this.log.error(new StringBuffer().append("filter ,").append(strServerName).append(" , ").append(strQueryString).append(" , ").append(requestUrl).toString());
                }
            }
        }
    }

    public void init(FilterConfig filterConfig2) throws ServletException {
        this.filterConfig = filterConfig2;
        this.encoding = filterConfig2.getInitParameter("encoding");
        String value = filterConfig2.getInitParameter("ignore");
        String file_max_length = filterConfig2.getInitParameter("file_max_length");
        if (file_max_length != null && file_max_length.length() > 0) {
            this.fileMaxLength = MyUtil.getStringToInt(file_max_length, 200) * KEYRecord.Flags.FLAG5;
        }
        if (value == null) {
            this.ignore = true;
        } else if (value.equalsIgnoreCase("true")) {
            this.ignore = true;
        } else if (value.equalsIgnoreCase("yes")) {
            this.ignore = true;
        } else {
            this.ignore = false;
        }
    }

    /* access modifiers changed from: protected */
    public String selectEncoding(ServletRequest request) {
        return this.encoding;
    }

    private String trimString(String value) {
        if (value != null) {
            return value.trim();
        }
        return "";
    }

    /* access modifiers changed from: package-private */
    public String createErrorMessage(String message, String referer) {
        if (message == null) {
            message = "";
        }
        return new StringBuffer().append(new StringBuffer().append(new StringBuffer().append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"><BODY>\n<SCRIPT LANGUAGE=\"JavaScript\">\n<!--\nalert(\"").append(message).append("\");\n").toString()).append("history.go(-1);\n").toString()).append("//--></SCRIPT>\n</BODY>").toString();
    }

    /* access modifiers changed from: protected */
    public void writeErrorMessage(HttpServletResponse httpServletResponse, String message) {
        try {
            httpServletResponse.setHeader("accept-language", "UTF-8");
            PrintWriter write = httpServletResponse.getWriter();
            write.write(CodeTransfer.UnicodeToISO(message));
            write.flush();
            write.close();
        } catch (Exception e) {
            this.log.error(e.getMessage());
        }
    }
}
