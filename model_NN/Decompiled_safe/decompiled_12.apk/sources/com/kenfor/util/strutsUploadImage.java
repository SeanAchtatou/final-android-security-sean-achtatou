package com.kenfor.util;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGEncodeParam;
import com.sun.image.codec.jpeg.JPEGImageEncoder;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.struts.upload.FormFile;
import org.ksoap2.SoapEnvelope;

public class strutsUploadImage extends strutsUploadFile {
    protected boolean auto_small_pic = false;
    private int bk_color_index = 2;
    private Color[] color = {Color.red, Color.black, Color.white, Color.yellow, Color.orange, Color.green};
    private boolean fixed_flag = true;
    private boolean logit = false;
    private String markString;
    protected int max_height = 300;
    protected int max_width = 300;
    protected int min_height = 0;
    protected int min_width = 0;
    protected String realFileName = null;
    private int real_height_big;
    private int real_height_small;
    private int real_width_big;
    private int real_width_small;
    private int run_step = 0;
    protected String smallFileName = null;
    protected int small_pic_height = SoapEnvelope.VER12;
    protected int small_pic_width = SoapEnvelope.VER12;
    private String str_pos;
    private String watermark_path;
    private int watermark_pos;

    public void setHeightLimit(int max_height2, int min_height2) {
        this.max_height = max_height2;
        this.min_height = min_height2;
    }

    public void setWidthLimit(int max_width2, int min_width2) {
        this.max_width = max_width2;
        this.min_width = min_width2;
    }

    public void setAutoSmallPic(boolean auto_small_pic2, int small_pic_height2, int small_pic_width2) {
        this.auto_small_pic = auto_small_pic2;
        this.small_pic_height = small_pic_height2;
        this.small_pic_width = small_pic_width2;
    }

    public strutsUploadImage() {
    }

    public String getRealFileName() {
        String temp = this.realFileName;
        if (temp == null || temp.length() < 1) {
            return null;
        }
        char[] temps = temp.toCharArray();
        for (int i = 0; i < temps.length; i++) {
            if (temps[i] == '\\') {
                temps[i] = '/';
            }
        }
        return String.valueOf(temps);
    }

    public void setRealFileName(String realFileName2) {
        this.realFileName = realFileName2;
    }

    public String getSmallFileName() throws IOException, Exception {
        String temp = this.smallFileName;
        if (temp == null) {
            temp = this.realFileName;
        }
        if (temp == null || temp.length() <= 0) {
            return temp;
        }
        char[] temps = temp.toCharArray();
        for (int i = 0; i < temps.length; i++) {
            if (temps[i] == '\\') {
                temps[i] = '/';
            }
        }
        return String.valueOf(temps);
    }

    public void setSmallFileName(String smallFileName2) {
        this.smallFileName = smallFileName2;
    }

    public strutsUploadImage(FormFile upFormFile, String file_path) {
        this.upFormFile = upFormFile;
        this.file_path = file_path;
    }

    public strutsUploadImage(FormFile upFormFile, String file_path, boolean autoSmallPic) {
        this.upFormFile = upFormFile;
        this.file_path = file_path;
        this.auto_small_pic = autoSmallPic;
    }

    private int getFontSize(int width, int str_len) {
        int result = ((width * 5) / 6) / str_len;
        if (result < 5) {
            return -1;
        }
        if (result <= 10) {
            result = 10;
        }
        if (result < 25) {
            return result;
        }
        return 25;
    }

    public void uploadFile(boolean logit2) throws IOException {
        this.logit = false;
        try {
            uploadFile();
        } catch (Exception e) {
            this.log.warn(new StringBuffer().append("run step:").append(this.run_step).toString());
            this.log.warn(e.getMessage());
            e.printStackTrace();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:293:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:294:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0181  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0192  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x02ee  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:145:0x0421=Splitter:B:145:0x0421, B:156:0x043f=Splitter:B:156:0x043f} */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:252:0x05f1=Splitter:B:252:0x05f1, B:246:0x05e3=Splitter:B:246:0x05e3} */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:34:0x010c=Splitter:B:34:0x010c, B:50:0x016e=Splitter:B:50:0x016e} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void uploadFile() throws java.io.IOException {
        /*
            r45 = this;
            java.lang.String r4 = "file.separator"
            java.lang.String r42 = java.lang.System.getProperty(r4)
            r0 = r45
            java.lang.String r4 = r0.str_pos
            if (r4 != 0) goto L_0x0012
            java.lang.String r4 = "down"
            r0 = r45
            r0.str_pos = r4
        L_0x0012:
            r0 = r45
            org.apache.struts.upload.FormFile r4 = r0.upFormFile
            if (r4 != 0) goto L_0x0019
        L_0x0018:
            return
        L_0x0019:
            r0 = r45
            org.apache.struts.upload.FormFile r4 = r0.upFormFile
            int r4 = r4.getFileSize()
            long r0 = (long) r4
            r27 = r0
            r4 = 0
            int r4 = (r27 > r4 ? 1 : (r27 == r4 ? 0 : -1))
            if (r4 <= 0) goto L_0x0018
            r0 = r45
            long r4 = r0.maxSize
            int r4 = (r27 > r4 ? 1 : (r27 == r4 ? 0 : -1))
            if (r4 <= 0) goto L_0x0083
            r0 = r45
            org.apache.commons.logging.Log r4 = r0.log
            java.lang.StringBuffer r5 = new java.lang.StringBuffer
            r5.<init>()
            java.lang.String r8 = "上传文件大小超过最大值，当前大小为："
            java.lang.StringBuffer r5 = r5.append(r8)
            r0 = r27
            java.lang.StringBuffer r5 = r5.append(r0)
            java.lang.String r5 = r5.toString()
            r4.warn(r5)
            java.lang.StringBuffer r4 = new java.lang.StringBuffer
            r4.<init>()
            java.lang.String r5 = "上传的文件的大小超过最大值，允许上传的最大值为："
            java.lang.StringBuffer r4 = r4.append(r5)
            r0 = r45
            long r8 = r0.maxSize
            r11 = 1024(0x400, double:5.06E-321)
            long r8 = r8 / r11
            java.lang.StringBuffer r4 = r4.append(r8)
            java.lang.String r5 = "K"
            java.lang.StringBuffer r4 = r4.append(r5)
            java.lang.String r4 = r4.toString()
            r0 = r45
            r0.error_msg = r4
            r0 = r45
            org.apache.struts.upload.FormFile r4 = r0.upFormFile
            r4.destroy()
            java.io.IOException r4 = new java.io.IOException
            r0 = r45
            java.lang.String r5 = r0.error_msg
            r4.<init>(r5)
            throw r4
        L_0x0083:
            r0 = r45
            org.apache.struts.upload.FormFile r4 = r0.upFormFile     // Catch:{ Exception -> 0x0130 }
            java.lang.String r4 = r4.getFileName()     // Catch:{ Exception -> 0x0130 }
            r0 = r45
            r0.oldFileName = r4     // Catch:{ Exception -> 0x0130 }
        L_0x008f:
            r0 = r45
            java.lang.String r4 = r0.file_path
            r5 = 0
            r0 = r45
            java.lang.String r8 = r0.oldFileName
            r0 = r45
            java.lang.String r4 = r0.getRealPath(r4, r5, r8)
            r0 = r45
            r0.realPath = r4
            r0 = r45
            java.lang.String r4 = r0.fileName
            r0 = r45
            r0.realFileName = r4
            r0 = r45
            java.lang.String r4 = r0.fileName
            r0 = r45
            java.lang.String r26 = r0.getExt(r4)
            r0 = r45
            org.apache.commons.logging.Log r4 = r0.log
            boolean r4 = r4.isInfoEnabled()
            if (r4 == 0) goto L_0x00c7
            r0 = r45
            org.apache.commons.logging.Log r4 = r0.log
            java.lang.String r5 = "开始上传原始图片"
            r4.info(r5)
        L_0x00c7:
            r0 = r45
            boolean r4 = r0.auto_small_pic
            if (r4 == 0) goto L_0x0463
            java.lang.String r4 = "jpg"
            r0 = r26
            boolean r4 = r4.equalsIgnoreCase(r0)
            if (r4 != 0) goto L_0x00f5
            java.lang.String r4 = "jpeg"
            r0 = r26
            boolean r4 = r4.equalsIgnoreCase(r0)
            if (r4 != 0) goto L_0x00f5
            java.lang.String r4 = "png"
            r0 = r26
            boolean r4 = r4.equalsIgnoreCase(r0)
            if (r4 != 0) goto L_0x00f5
            java.lang.String r4 = "gif"
            r0 = r26
            boolean r4 = r4.equalsIgnoreCase(r0)
            if (r4 == 0) goto L_0x0463
        L_0x00f5:
            r31 = 0
            r15 = 0
            r20 = 0
            r32 = 0
            r7 = 0
            r0 = r45
            org.apache.struts.upload.FormFile r4 = r0.upFormFile     // Catch:{ IOException -> 0x010b, Exception -> 0x016d }
            if (r4 != 0) goto L_0x0154
            java.io.IOException r4 = new java.io.IOException     // Catch:{ IOException -> 0x010b, Exception -> 0x016d }
            java.lang.String r5 = "upFormFile is null , errors"
            r4.<init>(r5)     // Catch:{ IOException -> 0x010b, Exception -> 0x016d }
            throw r4     // Catch:{ IOException -> 0x010b, Exception -> 0x016d }
        L_0x010b:
            r25 = move-exception
        L_0x010c:
            r25.printStackTrace()     // Catch:{ all -> 0x0128 }
            r0 = r45
            org.apache.commons.logging.Log r4 = r0.log     // Catch:{ all -> 0x0128 }
            java.lang.String r5 = r25.getMessage()     // Catch:{ all -> 0x0128 }
            r4.error(r5)     // Catch:{ all -> 0x0128 }
            java.lang.String r4 = "写文件时出错 "
            r0 = r45
            r0.error_msg = r4     // Catch:{ all -> 0x0128 }
            java.io.IOException r4 = new java.io.IOException     // Catch:{ all -> 0x0128 }
            java.lang.String r5 = "写文件时出错 "
            r4.<init>(r5)     // Catch:{ all -> 0x0128 }
            throw r4     // Catch:{ all -> 0x0128 }
        L_0x0128:
            r4 = move-exception
        L_0x0129:
            r20.close()     // Catch:{ Exception -> 0x0606 }
            r31.close()     // Catch:{ Exception -> 0x0606 }
        L_0x012f:
            throw r4
        L_0x0130:
            r24 = move-exception
            r24.printStackTrace()
            r0 = r45
            org.apache.commons.logging.Log r4 = r0.log
            java.lang.StringBuffer r5 = new java.lang.StringBuffer
            r5.<init>()
            java.lang.String r8 = "获取文件名出错:"
            java.lang.StringBuffer r5 = r5.append(r8)
            java.lang.String r8 = r24.getMessage()
            java.lang.StringBuffer r5 = r5.append(r8)
            java.lang.String r5 = r5.toString()
            r4.error(r5)
            goto L_0x008f
        L_0x0154:
            r0 = r45
            org.apache.struts.upload.FormFile r4 = r0.upFormFile     // Catch:{ IOException -> 0x010b, Exception -> 0x016d }
            java.io.InputStream r31 = r4.getInputStream()     // Catch:{ IOException -> 0x010b, Exception -> 0x016d }
            java.awt.image.BufferedImage r15 = javax.imageio.ImageIO.read(r31)     // Catch:{ IOException -> 0x010b, Exception -> 0x016d }
            if (r15 != 0) goto L_0x02f9
            r31.close()     // Catch:{ IOException -> 0x010b, Exception -> 0x016d }
            java.io.IOException r4 = new java.io.IOException     // Catch:{ IOException -> 0x010b, Exception -> 0x016d }
            java.lang.String r5 = "BufferedImage is null , errors"
            r4.<init>(r5)     // Catch:{ IOException -> 0x010b, Exception -> 0x016d }
            throw r4     // Catch:{ IOException -> 0x010b, Exception -> 0x016d }
        L_0x016d:
            r24 = move-exception
        L_0x016e:
            r24.printStackTrace()     // Catch:{ all -> 0x0128 }
            r20.close()     // Catch:{ Exception -> 0x060c }
            r31.close()     // Catch:{ Exception -> 0x060c }
        L_0x0177:
            r0 = r45
            org.apache.commons.logging.Log r4 = r0.log
            boolean r4 = r4.isInfoEnabled()
            if (r4 == 0) goto L_0x018a
            r0 = r45
            org.apache.commons.logging.Log r4 = r0.log
            java.lang.String r5 = "完成原始图片上传，开始处理缩略图"
            r4.info(r5)
        L_0x018a:
            r17 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            r0 = r45
            boolean r4 = r0.auto_small_pic
            if (r4 == 0) goto L_0x0018
            r0 = r45
            int r4 = r0.small_pic_height
            r0 = r32
            if (r0 > r4) goto L_0x01a0
            r0 = r45
            int r4 = r0.small_pic_width
            if (r7 <= r4) goto L_0x01af
        L_0x01a0:
            r0 = r32
            if (r0 >= r7) goto L_0x03f5
            r0 = r45
            int r4 = r0.small_pic_width
            double r4 = (double) r4
            r8 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r4 = r4 * r8
            double r8 = (double) r7
            double r17 = r4 / r8
        L_0x01af:
            r0 = r45
            int r4 = r0.real_height_big
            double r4 = (double) r4
            double r38 = r4 * r17
            r0 = r45
            int r4 = r0.real_width_big
            double r4 = (double) r4
            double r40 = r4 * r17
            r0 = r38
            int r4 = (int) r0
            r0 = r45
            r0.real_height_small = r4     // Catch:{ Exception -> 0x0404 }
            r0 = r40
            int r4 = (int) r0     // Catch:{ Exception -> 0x0404 }
            r0 = r45
            r0.real_width_small = r4     // Catch:{ Exception -> 0x0404 }
        L_0x01cb:
            r0 = r45
            java.lang.String r4 = r0.file_path
            r5 = 0
            r0 = r45
            java.lang.String r8 = r0.oldFileName
            r0 = r45
            java.lang.String r4 = r0.getRealPath(r4, r5, r8)
            r0 = r45
            r0.realPath = r4
            r0 = r45
            java.lang.String r4 = r0.fileName
            r0 = r45
            r0.smallFileName = r4
            r19 = 0
            r0 = r45
            int r4 = r0.small_pic_width
            r0 = r45
            int r5 = r0.small_pic_height
            r8 = 4
            java.awt.Image r16 = r15.getScaledInstance(r4, r5, r8)
            java.awt.image.AffineTransformOp r34 = new java.awt.image.AffineTransformOp
            r0 = r17
            r2 = r17
            java.awt.geom.AffineTransform r4 = java.awt.geom.AffineTransform.getScaleInstance(r0, r2)
            r5 = 0
            r0 = r34
            r0.<init>(r4, r5)
            r4 = 0
            r0 = r34
            java.awt.image.BufferedImage r16 = r0.filter(r15, r4)
            r4 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            int r4 = (r17 > r4 ? 1 : (r17 == r4 ? 0 : -1))
            if (r4 >= 0) goto L_0x044e
            java.awt.image.BufferedImage r22 = new java.awt.image.BufferedImage
            r0 = r45
            int r4 = r0.small_pic_width
            r0 = r45
            int r5 = r0.small_pic_height
            r8 = 1
            r0 = r22
            r0.<init>(r4, r5, r8)
            r4 = r16
            java.awt.image.BufferedImage r4 = (java.awt.image.BufferedImage) r4
            int r43 = r4.getWidth()
            r4 = r16
            java.awt.image.BufferedImage r4 = (java.awt.image.BufferedImage) r4
            int r10 = r4.getHeight()
            r35 = 0
            java.io.FileOutputStream r36 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0645, Exception -> 0x0642 }
            r0 = r45
            java.lang.String r4 = r0.realPath     // Catch:{ IOException -> 0x0645, Exception -> 0x0642 }
            r0 = r36
            r0.<init>(r4)     // Catch:{ IOException -> 0x0645, Exception -> 0x0642 }
            java.awt.Graphics2D r6 = r22.createGraphics()     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
            r4 = 0
            r5 = 0
            r0 = r45
            int r8 = r0.small_pic_width     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
            r0 = r45
            int r9 = r0.small_pic_height     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
            r6.fillRect(r4, r5, r8, r9)     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
            r0 = r45
            int r4 = r0.small_pic_width     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
            int r4 = r4 - r43
            int r4 = r4 / 2
            r0 = r45
            int r5 = r0.small_pic_height     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
            int r5 = r5 - r10
            int r5 = r5 / 2
            r8 = 0
            r0 = r16
            r6.drawImage(r0, r4, r5, r8)     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
            r0 = r45
            java.lang.String r4 = r0.markString     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
            if (r4 == 0) goto L_0x029f
            r0 = r45
            java.lang.String r4 = r0.markString     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
            int r4 = r4.length()     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
            if (r4 <= 0) goto L_0x029f
            r4 = 150(0x96, float:2.1E-43)
            r0 = r43
            if (r0 <= r4) goto L_0x029f
            r0 = r45
            org.apache.commons.logging.Log r4 = r0.log     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
            boolean r4 = r4.isDebugEnabled()     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
            if (r4 == 0) goto L_0x028e
            r0 = r45
            org.apache.commons.logging.Log r4 = r0.log     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
            java.lang.String r5 = "给缩略图加字符串"
            r4.debug(r5)     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
        L_0x028e:
            r0 = r45
            java.lang.String r5 = r0.markString     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
            r0 = r45
            int r8 = r0.small_pic_width     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
            r0 = r45
            int r9 = r0.small_pic_height     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
            r4 = r45
            r4.markString(r5, r6, r7, r8, r9)     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
        L_0x029f:
            r0 = r45
            java.lang.String r4 = r0.watermark_path     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
            if (r4 == 0) goto L_0x042f
            java.io.File r44 = new java.io.File     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
            r0 = r45
            java.lang.String r4 = r0.watermark_path     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
            r0 = r44
            r0.<init>(r4)     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
            boolean r4 = r44.exists()     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
            if (r4 == 0) goto L_0x0411
            r0 = r45
            boolean r4 = r0.fixed_flag     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
            if (r4 == 0) goto L_0x02dc
            r0 = r45
            org.apache.commons.logging.Log r4 = r0.log     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
            java.lang.String r5 = "给缩略图加水印"
            r4.debug(r5)     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
            r0 = r45
            int r11 = r0.bk_color_index     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
            r0 = r45
            java.lang.String r12 = r0.realPath     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
            r0 = r45
            java.lang.String r13 = r0.watermark_path     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
            r0 = r45
            java.lang.String r14 = r0.str_pos     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
            r8 = r22
            r9 = r43
            com.kenfor.util.imageMark.createMark(r8, r9, r10, r11, r12, r13, r14)     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
        L_0x02dc:
            r36.close()     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
            r36.close()     // Catch:{ Exception -> 0x0618 }
        L_0x02e2:
            r35 = r36
        L_0x02e4:
            r0 = r45
            org.apache.commons.logging.Log r4 = r0.log
            boolean r4 = r4.isInfoEnabled()
            if (r4 == 0) goto L_0x0018
            r0 = r45
            org.apache.commons.logging.Log r4 = r0.log
            java.lang.String r5 = "缩略图处理完成"
            r4.info(r5)
            goto L_0x0018
        L_0x02f9:
            int r32 = r15.getHeight()     // Catch:{ IOException -> 0x010b, Exception -> 0x016d }
            int r7 = r15.getWidth()     // Catch:{ IOException -> 0x010b, Exception -> 0x016d }
            r0 = r32
            r1 = r45
            r1.real_height_big = r0     // Catch:{ IOException -> 0x010b, Exception -> 0x016d }
            r0 = r45
            r0.real_width_big = r7     // Catch:{ IOException -> 0x010b, Exception -> 0x016d }
            r0 = r45
            boolean r4 = r0.logit     // Catch:{ IOException -> 0x010b, Exception -> 0x016d }
            if (r4 == 0) goto L_0x031a
            r0 = r45
            org.apache.commons.logging.Log r4 = r0.log     // Catch:{ IOException -> 0x010b, Exception -> 0x016d }
            java.lang.String r5 = "开始了生成原始图片"
            r4.warn(r5)     // Catch:{ IOException -> 0x010b, Exception -> 0x016d }
        L_0x031a:
            java.io.FileOutputStream r21 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x010b, Exception -> 0x016d }
            r0 = r45
            java.lang.String r4 = r0.realPath     // Catch:{ IOException -> 0x010b, Exception -> 0x016d }
            r0 = r21
            r0.<init>(r4)     // Catch:{ IOException -> 0x010b, Exception -> 0x016d }
            java.awt.image.BufferedImage r22 = new java.awt.image.BufferedImage     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
            r0 = r45
            int r4 = r0.real_width_big     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
            r0 = r45
            int r5 = r0.real_height_big     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
            r8 = 1
            r0 = r22
            r0.<init>(r4, r5, r8)     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
            java.awt.Graphics2D r6 = r22.createGraphics()     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
            r4 = 0
            r5 = 0
            r8 = 0
            r6.drawImage(r15, r4, r5, r8)     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
            r0 = r45
            java.lang.String r4 = r0.markString     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
            if (r4 == 0) goto L_0x037b
            r0 = r45
            java.lang.String r4 = r0.markString     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
            int r4 = r4.length()     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
            if (r4 <= 0) goto L_0x037b
            r0 = r45
            int r4 = r0.real_width_big     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
            r5 = 150(0x96, float:2.1E-43)
            if (r4 <= r5) goto L_0x037b
            r0 = r45
            org.apache.commons.logging.Log r4 = r0.log     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
            boolean r4 = r4.isDebugEnabled()     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
            if (r4 == 0) goto L_0x036a
            r0 = r45
            org.apache.commons.logging.Log r4 = r0.log     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
            java.lang.String r5 = "在图片中加字符串"
            r4.debug(r5)     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
        L_0x036a:
            r0 = r45
            java.lang.String r5 = r0.markString     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
            r0 = r45
            int r8 = r0.real_width_big     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
            r0 = r45
            int r9 = r0.real_height_big     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
            r4 = r45
            r4.markString(r5, r6, r7, r8, r9)     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
        L_0x037b:
            r0 = r45
            java.lang.String r4 = r0.watermark_path     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
            if (r4 == 0) goto L_0x03e4
            java.io.File r44 = new java.io.File     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
            r0 = r45
            java.lang.String r4 = r0.watermark_path     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
            r0 = r44
            r0.<init>(r4)     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
            boolean r4 = r44.exists()     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
            if (r4 == 0) goto L_0x03d3
            r0 = r45
            org.apache.commons.logging.Log r4 = r0.log     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
            boolean r4 = r4.isDebugEnabled()     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
            if (r4 == 0) goto L_0x03a5
            r0 = r45
            org.apache.commons.logging.Log r4 = r0.log     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
            java.lang.String r5 = "给图片加水印"
            r4.debug(r5)     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
        L_0x03a5:
            r0 = r45
            java.lang.String r4 = r0.realPath     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
            r0 = r45
            java.lang.String r5 = r0.watermark_path     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
            r0 = r45
            java.lang.String r8 = r0.str_pos     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
            r0 = r22
            com.kenfor.util.imageMark.createMark(r0, r4, r5, r8)     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
        L_0x03b6:
            r0 = r45
            org.apache.commons.logging.Log r4 = r0.log     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
            boolean r4 = r4.isInfoEnabled()     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
            if (r4 == 0) goto L_0x03c9
            r0 = r45
            org.apache.commons.logging.Log r4 = r0.log     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
            java.lang.String r5 = "原始图片处理完成"
            r4.info(r5)     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
        L_0x03c9:
            r21.close()     // Catch:{ Exception -> 0x061e }
            r31.close()     // Catch:{ Exception -> 0x061e }
        L_0x03cf:
            r20 = r21
            goto L_0x0177
        L_0x03d3:
            r0 = r45
            java.lang.String r4 = r0.realPath     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
            r0 = r45
            r1 = r22
            r0.jpegEncode(r4, r1)     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
            goto L_0x03b6
        L_0x03df:
            r25 = move-exception
            r20 = r21
            goto L_0x010c
        L_0x03e4:
            r0 = r45
            java.lang.String r4 = r0.realPath     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
            r0 = r45
            r1 = r22
            r0.jpegEncode(r4, r1)     // Catch:{ IOException -> 0x03df, Exception -> 0x03f0, all -> 0x0648 }
            goto L_0x03b6
        L_0x03f0:
            r24 = move-exception
            r20 = r21
            goto L_0x016e
        L_0x03f5:
            r0 = r45
            int r4 = r0.small_pic_height
            double r4 = (double) r4
            r8 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r4 = r4 * r8
            r0 = r32
            double r8 = (double) r0
            double r17 = r4 / r8
            goto L_0x01af
        L_0x0404:
            r24 = move-exception
            r4 = 0
            r0 = r45
            r0.real_height_small = r4
            r4 = 0
            r0 = r45
            r0.real_height_small = r4
            goto L_0x01cb
        L_0x0411:
            r0 = r45
            java.lang.String r4 = r0.realPath     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
            r0 = r45
            r1 = r22
            r0.jpegEncode(r4, r1)     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
            goto L_0x02dc
        L_0x041e:
            r24 = move-exception
            r35 = r36
        L_0x0421:
            r24.printStackTrace()     // Catch:{ all -> 0x0449 }
            r35.close()     // Catch:{ Exception -> 0x0429 }
            goto L_0x02e4
        L_0x0429:
            r24 = move-exception
        L_0x042a:
            r24.printStackTrace()
            goto L_0x02e4
        L_0x042f:
            r0 = r45
            java.lang.String r4 = r0.realPath     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
            r0 = r45
            r1 = r22
            r0.jpegEncode(r4, r1)     // Catch:{ IOException -> 0x041e, Exception -> 0x043c, all -> 0x063d }
            goto L_0x02dc
        L_0x043c:
            r24 = move-exception
            r35 = r36
        L_0x043f:
            r24.printStackTrace()     // Catch:{ all -> 0x0449 }
            r35.close()     // Catch:{ Exception -> 0x0447 }
            goto L_0x02e4
        L_0x0447:
            r24 = move-exception
            goto L_0x042a
        L_0x0449:
            r4 = move-exception
        L_0x044a:
            r35.close()     // Catch:{ Exception -> 0x0612 }
        L_0x044d:
            throw r4
        L_0x044e:
            r0 = r45
            java.lang.String r4 = r0.realPath     // Catch:{ Exception -> 0x045d }
            java.awt.image.BufferedImage r16 = (java.awt.image.BufferedImage) r16     // Catch:{ Exception -> 0x045d }
            r0 = r45
            r1 = r16
            r0.jpegEncode(r4, r1)     // Catch:{ Exception -> 0x045d }
            goto L_0x0018
        L_0x045d:
            r24 = move-exception
            r24.printStackTrace()
            goto L_0x0018
        L_0x0463:
            java.lang.String r4 = "jpg"
            r0 = r26
            boolean r4 = r4.equalsIgnoreCase(r0)
            if (r4 != 0) goto L_0x048b
            java.lang.String r4 = "jpeg"
            r0 = r26
            boolean r4 = r4.equalsIgnoreCase(r0)
            if (r4 != 0) goto L_0x048b
            java.lang.String r4 = "png"
            r0 = r26
            boolean r4 = r4.equalsIgnoreCase(r0)
            if (r4 != 0) goto L_0x048b
            java.lang.String r4 = "gif"
            r0 = r26
            boolean r4 = r4.equalsIgnoreCase(r0)
            if (r4 == 0) goto L_0x0571
        L_0x048b:
            r0 = r45
            java.lang.String r4 = r0.watermark_path
            if (r4 == 0) goto L_0x0571
            r0 = r45
            org.apache.commons.logging.Log r4 = r0.log
            boolean r4 = r4.isDebugEnabled()
            if (r4 == 0) goto L_0x04a4
            r0 = r45
            org.apache.commons.logging.Log r4 = r0.log
            java.lang.String r5 = "开始上传须加水印但不须要缩略的图片"
            r4.debug(r5)
        L_0x04a4:
            r31 = 0
            r15 = 0
            r0 = r45
            org.apache.struts.upload.FormFile r4 = r0.upFormFile     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
            java.io.InputStream r31 = r4.getInputStream()     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
            java.awt.image.BufferedImage r15 = javax.imageio.ImageIO.read(r31)     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
            int r32 = r15.getHeight()     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
            int r7 = r15.getWidth()     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
            r0 = r32
            r1 = r45
            r1.real_height_big = r0     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
            r0 = r45
            r0.real_width_big = r7     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
            java.awt.image.BufferedImage r22 = new java.awt.image.BufferedImage     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
            r0 = r45
            int r4 = r0.real_width_big     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
            r0 = r45
            int r5 = r0.real_height_big     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
            r8 = 1
            r0 = r22
            r0.<init>(r4, r5, r8)     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
            java.awt.Graphics2D r6 = r22.createGraphics()     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
            r4 = 0
            r5 = 0
            r8 = 0
            r6.drawImage(r15, r4, r5, r8)     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
            r0 = r45
            java.lang.String r4 = r0.markString     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
            if (r4 == 0) goto L_0x0517
            r0 = r45
            java.lang.String r4 = r0.markString     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
            int r4 = r4.length()     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
            if (r4 <= 0) goto L_0x0517
            r4 = 150(0x96, float:2.1E-43)
            if (r7 <= r4) goto L_0x0517
            r0 = r45
            org.apache.commons.logging.Log r4 = r0.log     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
            boolean r4 = r4.isDebugEnabled()     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
            if (r4 == 0) goto L_0x0506
            r0 = r45
            org.apache.commons.logging.Log r4 = r0.log     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
            java.lang.String r5 = "给缩略图加字符串"
            r4.debug(r5)     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
        L_0x0506:
            r0 = r45
            java.lang.String r5 = r0.markString     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
            r0 = r45
            int r8 = r0.real_width_big     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
            r0 = r45
            int r9 = r0.real_height_big     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
            r4 = r45
            r4.markString(r5, r6, r7, r8, r9)     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
        L_0x0517:
            r0 = r45
            java.lang.String r4 = r0.watermark_path     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
            if (r4 == 0) goto L_0x053f
            java.io.File r44 = new java.io.File     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
            r0 = r45
            java.lang.String r4 = r0.watermark_path     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
            r0 = r44
            r0.<init>(r4)     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
            boolean r4 = r44.exists()     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
            if (r4 == 0) goto L_0x054a
            r0 = r45
            java.lang.String r4 = r0.realPath     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
            r0 = r45
            java.lang.String r5 = r0.watermark_path     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
            r0 = r45
            java.lang.String r8 = r0.str_pos     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
            r0 = r22
            com.kenfor.util.imageMark.createMark(r0, r4, r5, r8)     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
        L_0x053f:
            r31.close()     // Catch:{ Exception -> 0x0544 }
            goto L_0x0018
        L_0x0544:
            r24 = move-exception
        L_0x0545:
            r24.printStackTrace()
            goto L_0x0018
        L_0x054a:
            r0 = r45
            java.lang.String r4 = r0.realPath     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
            r0 = r45
            r1 = r22
            r0.jpegEncode(r4, r1)     // Catch:{ IOException -> 0x0556, Exception -> 0x0561 }
            goto L_0x053f
        L_0x0556:
            r33 = move-exception
            r33.printStackTrace()     // Catch:{ all -> 0x056c }
            r31.close()     // Catch:{ Exception -> 0x055f }
            goto L_0x0018
        L_0x055f:
            r24 = move-exception
            goto L_0x0545
        L_0x0561:
            r24 = move-exception
            r24.printStackTrace()     // Catch:{ all -> 0x056c }
            r31.close()     // Catch:{ Exception -> 0x056a }
            goto L_0x0018
        L_0x056a:
            r24 = move-exception
            goto L_0x0545
        L_0x056c:
            r4 = move-exception
            r31.close()     // Catch:{ Exception -> 0x0624 }
        L_0x0570:
            throw r4
        L_0x0571:
            r0 = r45
            org.apache.commons.logging.Log r4 = r0.log
            boolean r4 = r4.isDebugEnabled()
            if (r4 == 0) goto L_0x0584
            r0 = r45
            org.apache.commons.logging.Log r4 = r0.log
            java.lang.String r5 = "开始上传BMP图片，或其它文件，不能进行缩小等处理"
            r4.debug(r5)
        L_0x0584:
            r31 = 0
            r29 = 0
            r0 = r45
            org.apache.struts.upload.FormFile r4 = r0.upFormFile     // Catch:{ IOException -> 0x063b, Exception -> 0x05f0 }
            java.io.InputStream r31 = r4.getInputStream()     // Catch:{ IOException -> 0x063b, Exception -> 0x05f0 }
            java.io.BufferedOutputStream r30 = new java.io.BufferedOutputStream     // Catch:{ IOException -> 0x063b, Exception -> 0x05f0 }
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x063b, Exception -> 0x05f0 }
            java.io.File r5 = new java.io.File     // Catch:{ IOException -> 0x063b, Exception -> 0x05f0 }
            r0 = r45
            java.lang.String r8 = r0.realPath     // Catch:{ IOException -> 0x063b, Exception -> 0x05f0 }
            r5.<init>(r8)     // Catch:{ IOException -> 0x063b, Exception -> 0x05f0 }
            r4.<init>(r5)     // Catch:{ IOException -> 0x063b, Exception -> 0x05f0 }
            r0 = r45
            int r5 = r0.diskBufferSize     // Catch:{ IOException -> 0x063b, Exception -> 0x05f0 }
            r0 = r30
            r0.<init>(r4, r5)     // Catch:{ IOException -> 0x063b, Exception -> 0x05f0 }
            r37 = 0
            r0 = r45
            int r4 = r0.diskBufferSize     // Catch:{ IOException -> 0x05e0, Exception -> 0x0637, all -> 0x0633 }
            byte[] r0 = new byte[r4]     // Catch:{ IOException -> 0x05e0, Exception -> 0x0637, all -> 0x0633 }
            r23 = r0
        L_0x05b3:
            r4 = 0
            r0 = r45
            int r5 = r0.diskBufferSize     // Catch:{ IOException -> 0x05e0, Exception -> 0x0637, all -> 0x0633 }
            r0 = r31
            r1 = r23
            int r37 = r0.read(r1, r4, r5)     // Catch:{ IOException -> 0x05e0, Exception -> 0x0637, all -> 0x0633 }
            if (r37 > 0) goto L_0x05d5
            r23 = 0
            r30.flush()     // Catch:{ IOException -> 0x05e0, Exception -> 0x0637, all -> 0x0633 }
            r31.close()     // Catch:{ Exception -> 0x062f }
            r30.close()     // Catch:{ Exception -> 0x05cf }
            goto L_0x0018
        L_0x05cf:
            r24 = move-exception
        L_0x05d0:
            r24.printStackTrace()
            goto L_0x0018
        L_0x05d5:
            r4 = 0
            r0 = r30
            r1 = r23
            r2 = r37
            r0.write(r1, r4, r2)     // Catch:{ IOException -> 0x05e0, Exception -> 0x0637, all -> 0x0633 }
            goto L_0x05b3
        L_0x05e0:
            r24 = move-exception
            r29 = r30
        L_0x05e3:
            r24.printStackTrace()     // Catch:{ all -> 0x05fe }
            r31.close()     // Catch:{ Exception -> 0x05ee }
            r29.close()     // Catch:{ Exception -> 0x05ee }
            goto L_0x0018
        L_0x05ee:
            r24 = move-exception
            goto L_0x05d0
        L_0x05f0:
            r24 = move-exception
        L_0x05f1:
            r24.printStackTrace()     // Catch:{ all -> 0x05fe }
            r31.close()     // Catch:{ Exception -> 0x0631 }
            r29.close()     // Catch:{ Exception -> 0x05fc }
            goto L_0x0018
        L_0x05fc:
            r24 = move-exception
            goto L_0x05d0
        L_0x05fe:
            r4 = move-exception
        L_0x05ff:
            r31.close()     // Catch:{ Exception -> 0x062a }
            r29.close()     // Catch:{ Exception -> 0x062a }
        L_0x0605:
            throw r4
        L_0x0606:
            r24 = move-exception
            r24.printStackTrace()
            goto L_0x012f
        L_0x060c:
            r24 = move-exception
            r24.printStackTrace()
            goto L_0x0177
        L_0x0612:
            r24 = move-exception
            r24.printStackTrace()
            goto L_0x044d
        L_0x0618:
            r24 = move-exception
            r24.printStackTrace()
            goto L_0x02e2
        L_0x061e:
            r24 = move-exception
            r24.printStackTrace()
            goto L_0x03cf
        L_0x0624:
            r24 = move-exception
            r24.printStackTrace()
            goto L_0x0570
        L_0x062a:
            r24 = move-exception
            r24.printStackTrace()
            goto L_0x0605
        L_0x062f:
            r24 = move-exception
            goto L_0x05d0
        L_0x0631:
            r24 = move-exception
            goto L_0x05d0
        L_0x0633:
            r4 = move-exception
            r29 = r30
            goto L_0x05ff
        L_0x0637:
            r24 = move-exception
            r29 = r30
            goto L_0x05f1
        L_0x063b:
            r24 = move-exception
            goto L_0x05e3
        L_0x063d:
            r4 = move-exception
            r35 = r36
            goto L_0x044a
        L_0x0642:
            r24 = move-exception
            goto L_0x043f
        L_0x0645:
            r24 = move-exception
            goto L_0x0421
        L_0x0648:
            r4 = move-exception
            r20 = r21
            goto L_0x0129
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenfor.util.strutsUploadImage.uploadFile():void");
    }

    /* access modifiers changed from: protected */
    public void markString(String markString2, Graphics2D g, int in_w, int img_width, int img_height) {
        int font_size = getFontSize(in_w, markString2.getBytes().length);
        if (font_size > 0) {
            this.str_pos = "down";
            Font mFont = new Font("宋体", 1, font_size);
            Color mClor = new Color(0, 0, 0);
            int str_len = markString2.length();
            g.setFont(mFont);
            g.setColor(mClor);
            g.drawString(markString2, (img_width - (str_len * font_size)) / 2 > 0 ? (img_width - (str_len * font_size)) / 2 : 10, (img_height - font_size) / 2);
        }
    }

    /* access modifiers changed from: protected */
    public void jpegEncode(String realPath, BufferedImage image) throws IOException {
        FileOutputStream ThF = null;
        try {
            FileOutputStream ThF2 = new FileOutputStream(realPath);
            try {
                JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(ThF2);
                JPEGEncodeParam param = encoder.getDefaultJPEGEncodeParam(image);
                param.setQuality(0.95f, true);
                encoder.encode(image, param);
                ThF2.close();
            } catch (Exception e) {
                ex = e;
                ThF = ThF2;
                try {
                    ex.printStackTrace();
                    this.log.error(ex.getMessage());
                    this.error_msg = "写文件时出错 ";
                    throw new IOException("写文件时出错 ");
                } catch (Throwable th) {
                    th = th;
                    ThF.close();
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                ThF = ThF2;
                ThF.close();
                throw th;
            }
        } catch (Exception e2) {
            ex = e2;
            ex.printStackTrace();
            this.log.error(ex.getMessage());
            this.error_msg = "写文件时出错 ";
            throw new IOException("写文件时出错 ");
        }
    }

    public int getReal_height_big() {
        return this.real_height_big;
    }

    public int getReal_width_big() {
        return this.real_width_big;
    }

    public int getReal_height_small() {
        return this.real_height_small;
    }

    public int getReal_width_small() {
        return this.real_width_small;
    }

    public String getWatermark_path() {
        return this.watermark_path;
    }

    public void setWatermark_path(String watermark_path2) {
        this.watermark_path = watermark_path2;
    }

    public boolean isFixed_flag() {
        return this.fixed_flag;
    }

    public void setFixed_flag(boolean fixed_flag2) {
        this.fixed_flag = fixed_flag2;
    }

    public int getBk_color_index() {
        return this.bk_color_index;
    }

    public void setBk_color_index(int bk_color_index2) {
        this.bk_color_index = bk_color_index2;
    }

    public String getMarkString() {
        return this.markString;
    }

    public void setMarkString(String markString2) {
        this.markString = markString2;
        this.str_pos = "down";
    }

    public int getWatermark_pos() {
        return this.watermark_pos;
    }

    public void setWatermark_pos(int watermark_pos2) {
        this.watermark_pos = watermark_pos2;
        switch (watermark_pos2) {
            case 1:
                this.str_pos = "mid";
                return;
            case 2:
                this.str_pos = "left";
                return;
            case 3:
                this.str_pos = "right";
                return;
            case 4:
                this.str_pos = "down";
                return;
            default:
                this.str_pos = "mid";
                return;
        }
    }
}
