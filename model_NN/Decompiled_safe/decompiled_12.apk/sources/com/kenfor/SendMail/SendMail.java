package com.kenfor.SendMail;

import com.kenfor.taglib.db.dbPresentTag;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class SendMail {
    protected static String body_charset = "text/html;charset=gb2312";
    protected static Log log = LogFactory.getLog("SendMail");
    protected ArrayList attach = new ArrayList();
    protected String body;
    protected String from;
    private String fromName = null;
    protected String password;
    private String replyto;
    protected String smtpHost;
    protected String subject;
    protected String to;
    protected String username;

    public void setBody_charset(String body_charset2) {
        body_charset = body_charset2;
    }

    public String getBody_charset() {
        return body_charset;
    }

    public void addAttach(String newAttach) {
        this.attach.add(newAttach);
    }

    public void removeAttach() {
        this.attach.remove(this.attach.size());
    }

    public void removeAttach(int i) {
        this.attach.remove(i);
    }

    public void setSmtpHost(String newStmpHost) {
        this.smtpHost = newStmpHost;
    }

    public String getSmtpHost() {
        return this.smtpHost;
    }

    public void setTo(String newTo) {
        this.to = newTo;
    }

    public String getTo() {
        return this.to;
    }

    public void setFrom(String newTo) {
        this.from = newTo;
    }

    public String getFrom() {
        return this.from;
    }

    public void setSubject(String newTo) {
        this.subject = newTo;
    }

    public String getSubject() {
        return this.subject;
    }

    public void setBody(String newTo) {
        this.body = newTo;
    }

    public String getBody() {
        return this.body;
    }

    public void setUsername(String newTo) {
        this.username = newTo;
    }

    public String getUsername() {
        return this.username;
    }

    public void setPassword(String newTo) {
        this.password = newTo;
    }

    public String getPassword() {
        return this.password;
    }

    public synchronized void send() {
        try {
            Properties props = System.getProperties();
            props.put("mail.smtp.host", this.smtpHost);
            props.put("mail.smtp.auth", "true");
            MimeMessage msg = new MimeMessage(Session.getInstance(props, new Email_Autherticatorbean(this.username, this.password)));
            if (this.fromName == null || this.fromName.trim().length() <= 0) {
                msg.setFrom(new InternetAddress(this.from));
            } else {
                msg.setFrom(new InternetAddress(this.from, this.fromName));
            }
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(this.to, false));
            msg.setSubject(this.subject);
            msg.setHeader("X-Mailer", "KenforMail");
            msg.setSentDate(new Date());
            if (this.replyto != null) {
                msg.setReplyTo(InternetAddress.parse(this.replyto));
            }
            MimeBodyPart mbp1 = new MimeBodyPart();
            mbp1.setContent(this.body, body_charset);
            MimeMultipart mp = new MimeMultipart();
            mp.addBodyPart(mbp1);
            if (!this.attach.isEmpty()) {
                MimeBodyPart[] mbpAttach = new MimeBodyPart[this.attach.size()];
                for (int i = 0; i < this.attach.size(); i++) {
                    FileDataSource fds = new FileDataSource(this.attach.get(i).toString());
                    mbpAttach[i] = new MimeBodyPart();
                    mbpAttach[i].setDataHandler(new DataHandler(fds));
                    mbpAttach[i].setFileName(new String(fds.getName().getBytes("GBK"), "iso8859-1"));
                    mp.addBodyPart(mbpAttach[i]);
                }
            }
            msg.setContent(mp);
            Transport.send(msg);
        } catch (SendFailedException e1) {
            log.error(new StringBuffer().append("to:").append(this.to).append(",smtp:").append(this.smtpHost).append(dbPresentTag.ROLE_DELIMITER).append(e1.getMessage()).toString());
        } catch (AddressException ee) {
            log.error(new StringBuffer().append("to:").append(this.to).append(",smtp:").append(this.smtpHost).append(dbPresentTag.ROLE_DELIMITER).append(ee.getMessage()).toString());
        } catch (Exception e) {
            log.error(new StringBuffer().append("to:").append(this.to).append(",smtp:").append(this.smtpHost).append(dbPresentTag.ROLE_DELIMITER).append(e.getMessage()).toString());
        }
        return;
    }

    public static synchronized boolean simpleSend(String smtpServer, String to2, String from2, String subject2, String body2, String username2, String password2, String fromName2) {
        boolean simpleSend;
        synchronized (SendMail.class) {
            simpleSend = simpleSend(smtpServer, to2, from2, subject2, body2, username2, password2, fromName2, "text/html;charset=gb2312");
        }
        return simpleSend;
    }

    public static synchronized boolean simpleSend(String smtpServer, String to2, String from2, String subject2, String body2, String username2, String password2, String fromName2, String body_charset2) {
        boolean z;
        synchronized (SendMail.class) {
            try {
                Properties props = System.getProperties();
                props.put("mail.smtp.host", smtpServer);
                props.put("mail.smtp.auth", "true");
                MimeMessage msg = new MimeMessage(Session.getInstance(props, new Email_Autherticatorbean(username2, password2)));
                if (fromName2 == null || fromName2.trim().length() <= 0) {
                    msg.setFrom(new InternetAddress(from2));
                } else {
                    msg.setFrom(new InternetAddress(from2, fromName2));
                }
                msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to2, false));
                msg.setSubject(subject2);
                msg.setHeader("X-Mailer", "KenforMail");
                msg.setSentDate(new Date());
                MimeBodyPart mbp1 = new MimeBodyPart();
                mbp1.setContent(body2, body_charset2);
                MimeMultipart mp = new MimeMultipart();
                mp.addBodyPart(mbp1);
                msg.setContent(mp);
                Transport.send(msg);
            } catch (SendFailedException e1) {
                log.error(new StringBuffer().append("to:").append(to2).append(",smtp:").append(smtpServer).append(dbPresentTag.ROLE_DELIMITER).append(e1.getMessage()).toString());
            } catch (AddressException ee) {
                log.error(new StringBuffer().append("to:").append(to2).append(",smtp:").append(smtpServer).append(dbPresentTag.ROLE_DELIMITER).append(ee.getMessage()).toString());
            } catch (Exception e) {
                log.error(new StringBuffer().append("to:").append(to2).append(",smtp:").append(smtpServer).toString());
                log.error(e.getMessage());
                z = false;
            }
            z = true;
        }
        return z;
    }

    public static synchronized boolean simpleSend(String smtpServer, String to2, String from2, String subject2, String body2, String username2, String password2) {
        boolean z;
        synchronized (SendMail.class) {
            try {
                Properties props = System.getProperties();
                props.put("mail.smtp.host", smtpServer);
                props.put("mail.smtp.auth", "true");
                MimeMessage msg = new MimeMessage(Session.getInstance(props, new Email_Autherticatorbean(username2, password2)));
                msg.setFrom(new InternetAddress(from2));
                msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to2, false));
                msg.setSubject(subject2);
                msg.setHeader("X-Mailer", "KenforMail");
                msg.setSentDate(new Date());
                MimeBodyPart mbp1 = new MimeBodyPart();
                mbp1.setContent(body2, body_charset);
                MimeMultipart mp = new MimeMultipart();
                mp.addBodyPart(mbp1);
                msg.setContent(mp);
                Transport.send(msg);
            } catch (SendFailedException e1) {
                log.error(new StringBuffer().append("to:").append(to2).append(",smtp:").append(smtpServer).append(dbPresentTag.ROLE_DELIMITER).append(e1.getMessage()).toString());
            } catch (AddressException ee) {
                log.error(new StringBuffer().append("to:").append(to2).append(",smtp:").append(smtpServer).append(dbPresentTag.ROLE_DELIMITER).append(ee.getMessage()).toString());
            } catch (Exception e) {
                log.error(new StringBuffer().append("to:").append(to2).append(",smtp:").append(smtpServer).toString());
                log.error(e.getMessage());
                z = false;
            }
            z = true;
        }
        return z;
    }

    public static void main(String[] args) {
        if (args.length == 4) {
            String host = args[0];
            String user_name = args[1];
            String password2 = args[2];
            String to2 = args[3];
            SendMail sendMail = new SendMail();
            sendMail.setSmtpHost(host);
            sendMail.setBody("hello肖");
            sendMail.setFrom("guiping.zhang@kenfor.com");
            sendMail.setReplyto("guiping.zhang@kenfor.com");
            sendMail.setPassword(password2);
            sendMail.setUsername(user_name);
            sendMail.setTo(to2);
            sendMail.setSubject("my jony mail");
            sendMail.send();
            System.out.println("ok");
            return;
        }
        System.out.println("usage: SendMail host user_name password to");
    }

    public String getFromName() {
        return this.fromName;
    }

    public void setFromName(String fromName2) {
        this.fromName = fromName2;
    }

    public String getReplyto() {
        return this.replyto;
    }

    public void setReplyto(String replyto2) {
        this.replyto = replyto2;
    }
}
