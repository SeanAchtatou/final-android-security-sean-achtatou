package com.kenfor.SendMail;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

public class Email_Autherticatorbean extends Authenticator {
    private String m_username = null;
    private String m_userpass = null;

    public void setUsername(String username) {
        this.m_username = username;
    }

    public void setUserpass(String userpass) {
        this.m_userpass = userpass;
    }

    public Email_Autherticatorbean(String username, String userpass) {
        setUsername(username);
        setUserpass(userpass);
    }

    public PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(this.m_username, this.m_userpass);
    }
}
