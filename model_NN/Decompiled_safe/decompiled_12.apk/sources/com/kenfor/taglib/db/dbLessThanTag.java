package com.kenfor.taglib.db;

import javax.servlet.jsp.JspException;

public class dbLessThanTag extends dbCompareTagBase {
    /* access modifiers changed from: protected */
    public boolean condition() throws JspException {
        return condition(-1, -1);
    }
}
