package com.kenfor.taglib.db;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.WeakHashMap;
import javax.servlet.jsp.JspException;

public class dbEmptyTag extends dbConditionalTagBase {
    /* access modifiers changed from: protected */
    public boolean condition() throws JspException {
        return condition(true);
    }

    /* access modifiers changed from: protected */
    public boolean condition(boolean desired) throws JspException {
        boolean empty;
        boolean empty2 = true;
        if (this.name != null) {
            Object bean = this.pageContext.getAttribute(this.name);
            String value = null;
            if (bean instanceof ResultSet) {
                ResultSet rs = (ResultSet) bean;
                if (this.property != null) {
                    if (rs != null) {
                        try {
                            value = rs.getString(this.property);
                        } catch (Exception e) {
                            value = null;
                        }
                        if (value != null) {
                            empty2 = value.length() < 1;
                        }
                    } else {
                        empty2 = true;
                    }
                } else if (rs == null) {
                    empty2 = true;
                } else {
                    try {
                        empty2 = rs.next();
                    } catch (Exception e2) {
                        empty2 = true;
                    }
                }
            }
            if (bean instanceof ArrayList) {
                ArrayList al = (ArrayList) bean;
                if (al == null) {
                    empty2 = true;
                } else {
                    empty2 = al.isEmpty();
                }
            }
            if (bean instanceof HashMap) {
                HashMap hm = (HashMap) bean;
                if (this.property != null) {
                    if (hm != null) {
                        value = String.valueOf(hm.get(this.property));
                    }
                    if (value != null) {
                        if (value.trim().length() < 1) {
                            empty2 = true;
                        } else {
                            empty2 = false;
                        }
                        if (!empty2 && "null".equalsIgnoreCase(value.trim())) {
                            empty2 = true;
                        }
                    }
                } else {
                    empty2 = hm.isEmpty();
                }
            }
            if (bean instanceof WeakHashMap) {
                WeakHashMap hm2 = (WeakHashMap) bean;
                if (this.property != null) {
                    if (hm2 != null) {
                        value = String.valueOf(hm2.get(this.property));
                    }
                    if (value != null) {
                        if (value.trim().length() < 1) {
                            empty = true;
                        } else {
                            empty = false;
                        }
                        if (!empty2 && "null".equalsIgnoreCase(value.trim())) {
                            empty2 = true;
                        }
                    }
                } else {
                    empty2 = hm2.isEmpty();
                }
            } else if (bean == null) {
                empty2 = true;
            }
        }
        if (empty2 == desired) {
            return true;
        }
        return false;
    }
}
