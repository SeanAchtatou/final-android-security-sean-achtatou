package com.kenfor.taglib.db;

import com.kenfor.database.DatabaseInfo;
import com.kenfor.database.PoolBean;
import com.kenfor.database.weakDbSpecifyValueBean;
import com.kenfor.exutil.InitAction;
import com.kenfor.util.CloseCon;
import com.kenfor.util.MyUtil;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.WeakHashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import org.apache.commons.collections.OrderedMap;
import org.apache.struts.util.RequestUtils;

public class dbListArrayTag extends dbTag {
    private boolean body_start = false;
    protected String classFieldName = "class_id";
    protected String comName = null;
    private boolean count_hotkey = false;
    private int cur_month = 0;
    private int cur_year = 0;
    protected int dbType = 0;
    private String isKeyword = "false";
    protected String isMysql = "false";
    protected int maxPageIndex = 0;
    protected int maxRecord = 0;
    protected String maxSQL = null;
    private int max_month = 6;
    private int max_year = 2005;
    protected String pageControl = "false";
    protected int pageIndex = 0;
    protected int pageMax = 100;
    private String page_flag = "false";
    protected String relateField = null;
    protected String rowMax = null;
    protected String rowStart = "0";
    private String topNum = null;

    public void release() {
        this.rowStart = "0";
        this.rowMax = null;
        this.relateField = null;
        this.isMysql = "false";
        this.maxSQL = null;
        this.dbType = 0;
        this.pageControl = "true";
        this.comName = null;
        this.classFieldName = "class_id";
        this.maxSQL = null;
        this.topNum = null;
        this.page_flag = "false";
        this.isKeyword = "false";
        super.release();
    }

    public void setIsKeyword(String isKeyword2) {
        this.isKeyword = isKeyword2;
    }

    public String getIsKeyword() {
        return this.isKeyword;
    }

    public void setPage_flag(String page_flag2) {
        this.page_flag = page_flag2;
    }

    public String getPage_flag() {
        return this.page_flag;
    }

    public void setPageControl(String pageControl2) {
        this.pageControl = pageControl2;
    }

    public String getPageControl() {
        return this.pageControl;
    }

    public void setMaxSQL(String maxSQL2) {
        this.maxSQL = maxSQL2;
    }

    public String getMaxSQL(String maxSQL2) {
        return this.maxSQL;
    }

    public void setRowStart(String rowStart2) {
        this.rowStart = rowStart2;
    }

    public String getRowStart() {
        return this.rowStart;
    }

    public void setRowMax(String rowMax2) {
        this.rowMax = rowMax2;
        if (this.log.isDebugEnabled()) {
            this.log.debug(new StringBuffer().append("set rowMax : ").append(this.rowMax).toString());
        }
        this.pageMax = MyUtil.getStringToInt(rowMax2, 0);
        if (this.pageMax <= 0) {
            this.pageMax = 3;
        }
    }

    public String getRowMax() {
        return this.rowMax;
    }

    public void setRelateField(String relateField2) {
        this.relateField = relateField2;
    }

    public String getRelateField() {
        return this.relateField;
    }

    public void setIsMysql(String isMysql2) {
        this.isMysql = isMysql2;
        if (isMysql2.compareToIgnoreCase("true") == 0) {
            this.dbType = 1;
        }
    }

    public String getIsMysql() {
        return this.isMysql;
    }

    public void setComName(String comName2) {
        this.comName = comName2;
    }

    public String getComName() {
        return this.comName;
    }

    public void setClassFieldName(String classFieldName2) {
        this.classFieldName = classFieldName2;
    }

    public String getClassFieldName() {
        return this.classFieldName;
    }

    /* access modifiers changed from: protected */
    public String createMaxSQL() {
        if (this.maxSQL != null) {
            return this.maxSQL;
        }
        if (this.sqlTablename == null) {
            return null;
        }
        String sql_where = createWhereSQL();
        String temp_sql = new StringBuffer().append("select count(*) from ").append(this.sqlTablename).toString();
        if (sql_where == null || sql_where.length() <= 0) {
            return temp_sql;
        }
        return new StringBuffer().append(temp_sql).append(" where ").append(sql_where).toString();
    }

    /* access modifiers changed from: protected */
    public String createWhereSQL() {
        String result;
        if (this.new_flag == null || this.new_flag.equalsIgnoreCase("false")) {
            result = createWhereSQL(true);
        } else {
            result = createNewWhereSQL();
        }
        return filterChar(result, '`');
    }

    /* access modifiers changed from: protected */
    public String filterChar(String value, char bec) {
        if (value == null || value.length() < 1) {
            return value;
        }
        char[] v_char = value.toCharArray();
        StringBuffer sb = new StringBuffer();
        int char_len = v_char.length;
        for (int i = 0; i < char_len; i++) {
            if (v_char[i] != bec) {
                sb.append(v_char[i]);
            }
        }
        if (sb.length() > 0) {
            value = sb.toString();
        }
        return value;
    }

    /* access modifiers changed from: protected */
    public String createNewWhereSQL() {
        String result;
        String t_result;
        String temp_sql = null;
        int t_index = -1;
        if (this.body_start && this.bodySqlWhere != null && this.bodySqlWhere.length() > 0) {
            temp_sql = this.bodySqlWhere;
        } else if (this.sqlWhere != null && this.sqlWhere.length() > 0) {
            temp_sql = this.sqlWhere;
        }
        ArrayList paramPre = new ArrayList();
        ArrayList paramNex = new ArrayList();
        ArrayList paramTotal = new ArrayList();
        StringBuffer resultBuf = new StringBuffer();
        String t_sql = temp_sql;
        if (t_sql != null) {
            t_index = t_sql.indexOf("[");
        }
        if (t_index > 0) {
            while (t_index > 0) {
                int t_index2 = t_sql.indexOf("[");
                int t_index22 = t_sql.indexOf("]");
                if (t_index22 < 0) {
                    return null;
                }
                String total_str = t_sql.substring(0, t_index2);
                if (total_str != null && total_str.trim().length() > 0) {
                    paramTotal.add(total_str);
                }
                String deal_str = dealParam(t_sql.substring(t_index2 + 1, t_index22));
                if (deal_str != null && deal_str.length() > 0) {
                    paramTotal.add(deal_str);
                }
                t_sql = t_sql.substring(t_index22 + 1);
                t_index = t_sql.indexOf("[");
            }
            int p_size = paramPre.size();
            for (int i = 0; i < p_size; i++) {
                String str2 = null;
                boolean three_flag = false;
                String str1 = (String) paramPre.get(i);
                if (str1 != null) {
                    str1 = str1.trim();
                }
                if (paramNex.size() > i) {
                    str2 = (String) paramNex.get(i);
                }
                if (str2 != null) {
                    str2 = str2.trim();
                }
                String value = null;
                String b_str = null;
                String e_str = null;
                if (str2 != null) {
                    if (str2.length() > 1) {
                        b_str = str2.substring(0, 1);
                        e_str = str2.substring(str2.length() - 1, str2.length());
                    }
                    if ("%".equals(b_str) && str2.length() >= 1) {
                        str2 = str2.substring(1, str2.length());
                    }
                    if ("%".equals(e_str) && str2.length() >= 1) {
                        str2 = str2.substring(0, str2.length() - 1);
                    }
                    if ("keyword".equals(str2.trim())) {
                        this.count_hotkey = true;
                    }
                    if (str2.indexOf(":") == 0) {
                        str2 = str2.substring(1);
                        three_flag = true;
                    }
                    value = this.pageContext.getRequest().getParameter(str2.trim());
                    if (value == null || value.equalsIgnoreCase("null")) {
                        value = (String) this.pageContext.getRequest().getAttribute(str2.trim());
                    }
                    if (value == null || value.equalsIgnoreCase("null")) {
                        value = (String) this.pageContext.getAttribute(str2.trim());
                    }
                    if (value == null || value.equalsIgnoreCase("null")) {
                        value = (String) this.pageContext.getSession().getAttribute(str2.trim());
                    }
                    if ((value == null || value.equalsIgnoreCase("null") || value.trim().length() <= 0) && this.propName != null && this.propName.length() > 1) {
                        try {
                            Object obj_value = RequestUtils.lookup(this.pageContext, this.propName, str2.trim(), (String) null);
                            if (obj_value != null) {
                                value = String.valueOf(obj_value);
                            }
                        } catch (Exception e) {
                            value = null;
                        }
                    }
                }
                String value2 = getFilterString(value);
                if ("errorwhere".equals(value2)) {
                    this.is_value_ok = false;
                    return null;
                }
                if (value2 == null || value2.equalsIgnoreCase("null") || value2.trim().length() <= 0) {
                    if (this.log.isDebugEnabled()) {
                        this.log.debug(new StringBuffer().append("value is null,").append(str2).toString());
                    }
                    if (this.isSubSQL.equals("true")) {
                        temp_sql = temp_sql.replaceFirst(str2, "0");
                    } else if (!this.isIgnore.equalsIgnoreCase("true")) {
                        paramTotal.add(new StringBuffer().append(str1).append(" 0").toString());
                    }
                } else {
                    if (this.log.isDebugEnabled()) {
                        this.log.debug(new StringBuffer().append("value is ok,").append(value2).toString());
                    }
                    this.pageContext.setAttribute(str2.trim(), value2.trim());
                    if (this.isSubSQL.equals("true")) {
                        temp_sql = temp_sql.replaceFirst(str2, value2.trim());
                    } else {
                        String value3 = value2.trim();
                        if ("%".equals(b_str)) {
                            value3 = new StringBuffer().append("%").append(value3).toString();
                        }
                        if ("%".equals(e_str)) {
                            value3 = new StringBuffer().append(value3).append("%").toString();
                        }
                        if (three_flag) {
                            t_result = new StringBuffer().append(str1).append(" ").append(value3).toString();
                        } else {
                            int str1_len = str1.indexOf("==");
                            if (str1_len > 0) {
                                long lng_value = MyUtil.getStringToLong(value3, 0);
                                if (lng_value == 0) {
                                    System.out.println(new StringBuffer().append("value:").append(value3).toString());
                                }
                                t_result = new StringBuffer().append(str1.substring(0, str1_len + 1)).append(lng_value).toString();
                            } else {
                                t_result = new StringBuffer().append(str1).append(" '").append(value3).append("' ").toString();
                            }
                        }
                        paramTotal.add(t_result);
                    }
                }
            }
            if (!this.isSubSQL.equals("true")) {
                int pt_size = paramTotal.size();
                for (int j = 0; j < pt_size; j++) {
                    String t_result2 = (String) paramTotal.get(j);
                    if (t_result2 != null && t_result2.trim().length() >= 1) {
                        resultBuf.append(t_result2);
                    }
                }
            } else if (temp_sql != null) {
                resultBuf.append(temp_sql);
            }
        } else if (t_sql != null) {
            resultBuf.append(t_sql);
        }
        if (resultBuf == null) {
            result = null;
        } else {
            result = resultBuf.toString();
        }
        paramPre.clear();
        paramNex.clear();
        paramTotal.clear();
        return result;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: CFG modification limit reached, blocks count: 157 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String dealParam(java.lang.String r15) {
        /*
            r14 = this;
            r13 = 0
            r12 = 1
            java.lang.String r5 = ""
            java.lang.String r10 = "::"
            int r2 = r15.indexOf(r10)
            if (r2 >= 0) goto L_0x00fc
            r10 = r15
        L_0x000d:
            return r10
        L_0x000e:
            r8 = 0
            if (r2 <= 0) goto L_0x0015
            java.lang.String r8 = r15.substring(r13, r2)
        L_0x0015:
            int r10 = r2 + 2
            java.lang.String r6 = r15.substring(r10)
            java.lang.String r10 = " "
            int r4 = r6.indexOf(r10)
            r7 = 0
            if (r4 <= 0) goto L_0x0101
            java.lang.String r7 = r6.substring(r13, r4)
            java.lang.String r15 = r6.substring(r4)
        L_0x002c:
            r9 = 0
            r0 = 0
            r1 = 0
            int r10 = r7.length()
            if (r10 <= r12) goto L_0x0047
            java.lang.String r0 = r7.substring(r13, r12)
            int r10 = r7.length()
            int r10 = r10 + -1
            int r11 = r7.length()
            java.lang.String r1 = r7.substring(r10, r11)
        L_0x0047:
            java.lang.String r10 = "%"
            boolean r10 = r10.equals(r0)
            if (r10 == 0) goto L_0x005d
            int r10 = r7.length()
            if (r10 < r12) goto L_0x005d
            int r10 = r7.length()
            java.lang.String r7 = r7.substring(r12, r10)
        L_0x005d:
            java.lang.String r10 = "%"
            boolean r10 = r10.equals(r1)
            if (r10 == 0) goto L_0x0075
            int r10 = r7.length()
            if (r10 < r12) goto L_0x0075
            int r10 = r7.length()
            int r10 = r10 + -1
            java.lang.String r7 = r7.substring(r13, r10)
        L_0x0075:
            java.lang.String r10 = ":"
            boolean r10 = r10.equals(r0)
            if (r10 == 0) goto L_0x008b
            int r10 = r7.length()
            if (r10 < r12) goto L_0x008b
            int r10 = r7.length()
            java.lang.String r7 = r7.substring(r12, r10)
        L_0x008b:
            java.lang.String r3 = r14.getParamValue(r7)
            if (r3 == 0) goto L_0x00d5
            java.lang.String r10 = "null"
            boolean r10 = r3.equalsIgnoreCase(r10)
            if (r10 != 0) goto L_0x00d5
            int r10 = r3.length()
            if (r10 <= 0) goto L_0x00d5
            java.lang.String r10 = "%"
            boolean r10 = r10.equals(r0)
            if (r10 == 0) goto L_0x0106
            java.lang.StringBuffer r10 = new java.lang.StringBuffer
            r10.<init>()
            java.lang.StringBuffer r10 = r10.append(r5)
            java.lang.StringBuffer r10 = r10.append(r8)
            java.lang.String r5 = r10.toString()
            java.lang.StringBuffer r10 = new java.lang.StringBuffer
            r10.<init>()
            java.lang.StringBuffer r10 = r10.append(r5)
            java.lang.String r11 = " '%"
            java.lang.StringBuffer r10 = r10.append(r11)
            java.lang.StringBuffer r10 = r10.append(r3)
            java.lang.String r11 = "%' "
            java.lang.StringBuffer r10 = r10.append(r11)
            java.lang.String r5 = r10.toString()
        L_0x00d5:
            java.lang.String r10 = "::"
            int r2 = r15.indexOf(r10)
            if (r2 >= 0) goto L_0x00fc
            if (r5 == 0) goto L_0x00fc
            int r10 = r5.length()
            if (r10 <= 0) goto L_0x00fc
            java.lang.StringBuffer r10 = new java.lang.StringBuffer
            r10.<init>()
            java.lang.StringBuffer r10 = r10.append(r5)
            java.lang.StringBuffer r10 = r10.append(r15)
            java.lang.String r11 = " "
            java.lang.StringBuffer r10 = r10.append(r11)
            java.lang.String r5 = r10.toString()
        L_0x00fc:
            if (r2 > 0) goto L_0x000e
            r10 = r5
            goto L_0x000d
        L_0x0101:
            r7 = r6
            java.lang.String r15 = ""
            goto L_0x002c
        L_0x0106:
            java.lang.String r10 = ":"
            boolean r10 = r10.equals(r0)
            if (r10 == 0) goto L_0x0137
            java.lang.StringBuffer r10 = new java.lang.StringBuffer
            r10.<init>()
            java.lang.StringBuffer r10 = r10.append(r5)
            java.lang.StringBuffer r10 = r10.append(r8)
            java.lang.String r5 = r10.toString()
            java.lang.StringBuffer r10 = new java.lang.StringBuffer
            r10.<init>()
            java.lang.StringBuffer r10 = r10.append(r5)
            java.lang.StringBuffer r10 = r10.append(r3)
            java.lang.String r11 = " "
            java.lang.StringBuffer r10 = r10.append(r11)
            java.lang.String r5 = r10.toString()
            goto L_0x00d5
        L_0x0137:
            if (r8 == 0) goto L_0x00d5
            java.lang.String r10 = "=="
            int r10 = r8.indexOf(r10)
            if (r10 <= 0) goto L_0x0173
            java.lang.String r10 = "=="
            java.lang.String r11 = "="
            java.lang.String r8 = r8.replaceFirst(r10, r11)
            java.lang.StringBuffer r10 = new java.lang.StringBuffer
            r10.<init>()
            java.lang.StringBuffer r10 = r10.append(r5)
            java.lang.StringBuffer r10 = r10.append(r8)
            java.lang.String r5 = r10.toString()
            java.lang.StringBuffer r10 = new java.lang.StringBuffer
            r10.<init>()
            java.lang.StringBuffer r10 = r10.append(r5)
            java.lang.StringBuffer r10 = r10.append(r3)
            java.lang.String r11 = " "
            java.lang.StringBuffer r10 = r10.append(r11)
            java.lang.String r5 = r10.toString()
            goto L_0x00d5
        L_0x0173:
            java.lang.StringBuffer r10 = new java.lang.StringBuffer
            r10.<init>()
            java.lang.StringBuffer r10 = r10.append(r5)
            java.lang.StringBuffer r10 = r10.append(r8)
            java.lang.String r5 = r10.toString()
            java.lang.StringBuffer r10 = new java.lang.StringBuffer
            r10.<init>()
            java.lang.StringBuffer r10 = r10.append(r5)
            java.lang.String r11 = " '"
            java.lang.StringBuffer r10 = r10.append(r11)
            java.lang.StringBuffer r10 = r10.append(r3)
            java.lang.String r11 = "' "
            java.lang.StringBuffer r10 = r10.append(r11)
            java.lang.String r5 = r10.toString()
            goto L_0x00d5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenfor.taglib.db.dbListArrayTag.dealParam(java.lang.String):java.lang.String");
    }

    /* access modifiers changed from: protected */
    public String getParamValue(String str2) {
        String value = this.pageContext.getRequest().getParameter(str2.trim());
        if (value == null || value.equalsIgnoreCase("null")) {
            value = (String) this.pageContext.getRequest().getAttribute(str2.trim());
        }
        if (value == null || value.equalsIgnoreCase("null")) {
            value = (String) this.pageContext.getAttribute(str2.trim());
        }
        if ((value != null && !value.equalsIgnoreCase("null") && value.trim().length() > 0) || this.propName == null || this.propName.length() <= 1) {
            return value;
        }
        try {
            Object obj_value = RequestUtils.lookup(this.pageContext, this.propName, str2.trim(), (String) null);
            if (obj_value != null) {
                return String.valueOf(obj_value);
            }
            return value;
        } catch (Exception e) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public String createWhereSQL(boolean bdb_sql_where) {
        int t_index;
        String result;
        String t_result;
        String temp_sql = null;
        int t_index2 = -1;
        boolean three_flag = false;
        if (this.body_start && this.bodySqlWhere != null && this.bodySqlWhere.length() > 0) {
            temp_sql = this.bodySqlWhere;
        } else if (this.sqlWhere != null && this.sqlWhere.length() > 0) {
            temp_sql = this.sqlWhere;
        }
        if (bdb_sql_where && this.db_sql_where != null) {
            if (temp_sql != null) {
                temp_sql = new StringBuffer().append(temp_sql).append(this.db_sql_where).toString();
            } else {
                temp_sql = this.db_sql_where;
            }
        }
        ArrayList paramPre = new ArrayList();
        ArrayList paramNex = new ArrayList();
        ArrayList paramTotal = new ArrayList();
        StringBuffer resultBuf = new StringBuffer();
        String t_sql = temp_sql;
        if (t_sql != null) {
            t_index2 = t_sql.indexOf("::");
        }
        if (t_index > 0) {
            if (this.log.isDebugEnabled()) {
                this.log.debug(new StringBuffer().append("t_index : ").append(t_index).toString());
            }
            while (t_index > 0) {
                int t_index3 = t_sql.indexOf("::");
                String sub_str1 = null;
                if (t_index3 > 0) {
                    sub_str1 = t_sql.substring(0, t_index3);
                }
                int r_index = -1;
                if (sub_str1 != null) {
                    r_index = sub_str1.lastIndexOf("and");
                }
                if (r_index > 0) {
                    paramTotal.add(sub_str1.substring(0, r_index));
                    sub_str1 = sub_str1.substring(r_index + 3);
                }
                paramPre.add(sub_str1);
                String sub_str2 = t_sql.substring(t_index3 + 2);
                int tt_index = sub_str2.indexOf("and");
                if (tt_index > 0) {
                    paramNex.add(sub_str2.substring(0, tt_index));
                    t_sql = sub_str2.substring(tt_index + 3);
                    t_index = 1;
                } else {
                    paramNex.add(sub_str2);
                    t_index = -1;
                }
            }
            int pp_size = paramPre.size();
            for (int i = 0; i < pp_size; i++) {
                String str2 = null;
                String str1 = (String) paramPre.get(i);
                if (str1 != null) {
                    str1 = str1.trim();
                }
                if (paramNex.size() > i) {
                    str2 = (String) paramNex.get(i);
                }
                if (str2 != null) {
                    str2 = str2.trim();
                }
                String value = null;
                String b_str = null;
                String e_str = null;
                if (str1 != null || str2 == null) {
                    if (str2 != null) {
                        if (str2.length() > 1) {
                            b_str = str2.substring(0, 1);
                            e_str = str2.substring(str2.length() - 1, str2.length());
                        }
                        if ("%".equals(b_str) && str2.length() >= 1) {
                            str2 = str2.substring(1, str2.length());
                        }
                        if ("%".equals(e_str) && str2.length() >= 1) {
                            str2 = str2.substring(0, str2.length() - 1);
                        }
                        if ("keyword".equals(str2.trim())) {
                            this.count_hotkey = true;
                        }
                        if (str2.indexOf(":") == 0) {
                            str2 = str2.substring(1);
                            three_flag = true;
                        }
                        value = getParamValue(str2);
                    }
                    String value2 = getFilterString(value);
                    if ("errorwhere".equals(value2)) {
                        this.is_value_ok = false;
                        return null;
                    } else if (value2 == null || value2.equalsIgnoreCase("null") || value2.trim().length() <= 0) {
                        if (this.log.isDebugEnabled()) {
                            this.log.debug(new StringBuffer().append("value is null,").append(value2).toString());
                        }
                        if (this.isSubSQL.equals("true")) {
                            temp_sql = temp_sql.replaceFirst(str2, "0");
                        } else if (!this.isIgnore.equalsIgnoreCase("true")) {
                            paramTotal.add(new StringBuffer().append(str1).append(" 0").toString());
                        }
                    } else {
                        String value3 = getFilterString(value2);
                        if ("errorwhere".equals(value3)) {
                            this.is_value_ok = false;
                            return null;
                        }
                        String value4 = value3.trim();
                        if (value4.endsWith("`")) {
                            value4 = value4.substring(0, value4.length() - 1);
                        }
                        if (value4.endsWith("/")) {
                            value4 = value4.substring(0, value4.length() - 1);
                        }
                        if (this.isSubSQL.equals("true")) {
                            temp_sql = temp_sql.replaceFirst(str2, value4.trim());
                        } else {
                            String value5 = value4.trim().replaceAll("'", "''");
                            if ("%".equals(b_str)) {
                                value5 = new StringBuffer().append("%").append(value5).toString();
                            }
                            if ("%".equals(e_str)) {
                                value5 = new StringBuffer().append(value5).append("%").toString();
                            }
                            if (three_flag) {
                                t_result = new StringBuffer().append(str1).append(" ").append(value5).toString();
                            } else {
                                int str1_len = str1.indexOf("==");
                                if (str1_len <= 0) {
                                    t_result = new StringBuffer().append(str1).append(" '").append(value5).append("' ").toString();
                                } else if (isValidNumber(value5)) {
                                    t_result = new StringBuffer().append(str1.substring(0, str1_len + 1)).append(value5).toString();
                                } else {
                                    this.is_value_ok = false;
                                    this.log.warn("value is not number");
                                    return null;
                                }
                            }
                            paramTotal.add(t_result);
                        }
                    }
                } else {
                    paramTotal.add(str2);
                }
            }
            if (!this.isSubSQL.equals("true")) {
                int pt_size = paramTotal.size();
                for (int j = 0; j < pt_size; j++) {
                    resultBuf.append((String) paramTotal.get(j));
                    if (j < paramTotal.size() - 1) {
                        resultBuf.append(" and ");
                    }
                }
            } else if (temp_sql != null) {
                resultBuf.append(temp_sql);
            }
        } else if (t_sql != null) {
            resultBuf.append(t_sql);
        }
        if (resultBuf == null) {
            result = null;
        } else {
            result = resultBuf.toString();
        }
        if (this.comName != null) {
            if (this.log.isDebugEnabled()) {
                this.log.debug("comName is not null");
            }
            String comValue = this.pageContext.getRequest().getParameter(this.comName);
            String path = this.pageContext.getServletContext().getRealPath("WEB-INF/classes/");
            if (comValue == null || comValue.equalsIgnoreCase("null")) {
                weakDbSpecifyValueBean db = new weakDbSpecifyValueBean();
                String sql = null;
                if (this.dbType == 0) {
                    sql = new StringBuffer().append("select top 1 ").append(this.classFieldName).append(" from ").append(this.sqlTablename).toString();
                } else if (this.dbType == 1) {
                    sql = new StringBuffer().append("select ").append(this.classFieldName).append(" from ").append(this.sqlTablename).append(" limit 0,1").toString();
                } else if (this.dbType == 2) {
                    sql = new StringBuffer().append("select ").append(this.classFieldName).append(" from ").append(this.sqlTablename).append(" limit 1").toString();
                }
                db.setSql(sql);
                try {
                    WeakHashMap result_map = db.getdbValue(path);
                    if (result_map != null) {
                        comValue = String.valueOf(result_map.get(this.classFieldName.toLowerCase()));
                        this.pageContext.setAttribute(this.comName, comValue);
                    }
                    result_map.clear();
                } catch (Exception e) {
                }
            }
            if (comValue != null && comValue.length() > 0) {
                if (result == null || result.length() <= 0) {
                    result = new StringBuffer().append(this.comName).append("=").append(comValue).toString();
                } else {
                    result = new StringBuffer().append(result).append(" and ").append(this.comName).append(" = ").append(comValue).toString();
                }
            }
        }
        if (result == null || result.length() == 0) {
            result = null;
        }
        paramPre.clear();
        paramNex.clear();
        paramTotal.clear();
        return result;
    }

    /* access modifiers changed from: protected */
    public String createOrderSQL() {
        String temp_orderby = this.sqlOrderby;
        if (this.sqlOrderby == null || this.sqlOrderby.length() <= 0 || this.sqlOrderby.indexOf("::") < 0) {
            return temp_orderby;
        }
        String temp = temp_orderby.substring(temp_orderby.indexOf("::") + 2).trim();
        int t2_index = temp.indexOf("[");
        int t3_index = temp.indexOf("]");
        String default_order = null;
        if (t2_index > 0) {
            default_order = temp.substring(t2_index + 1, t3_index);
            temp = temp.substring(0, t2_index);
        }
        String value = null;
        if (temp != null && ((value = this.pageContext.getRequest().getParameter(temp.trim())) == null || value.equalsIgnoreCase("null"))) {
            value = (String) this.pageContext.getAttribute(temp.trim());
        }
        if (value != null) {
            return value;
        }
        if (default_order != null) {
            return default_order;
        }
        return "";
    }

    /* access modifiers changed from: protected */
    public String createSQL() {
        StringBuffer temp_sql_buf = new StringBuffer();
        String sql_where = null;
        if (this.sql == null && (sql_where = createWhereSQL()) != null && sql_where.length() == 0) {
            sql_where = null;
        }
        if (this.sql != null) {
            if (this.log.isDebugEnabled()) {
                this.log.debug(new StringBuffer().append("sql :").append(this.sql).toString());
            }
            return this.sql;
        }
        String this_orderby = createOrderSQL();
        if (this.log.isDebugEnabled()) {
            this.log.debug(new StringBuffer().append("pageMax = ").append(this.pageMax).toString());
        }
        if (this.dbType == 0) {
            if (this.rowMax != null && this.relateField != null && this.pageMax >= 0) {
                temp_sql_buf.append(new StringBuffer().append("select top ").append(this.rowMax).append(" ").append(this.sqlFields).append(" from ").append(this.sqlTablename).toString());
                temp_sql_buf.append(" with (nolock) ");
                if (sql_where != null) {
                    if (this_orderby != null) {
                        if (this.pageIndex == 0) {
                            temp_sql_buf.append(new StringBuffer().append(" where ").append(sql_where).append(" order by ").append(this_orderby).toString());
                        } else {
                            temp_sql_buf.append(new StringBuffer().append(" where ").append(sql_where).append(" and ").append(this.relateField).append(" not in ( select top ").append(String.valueOf(this.pageMax * this.pageIndex)).append(" ").append(this.relateField).append(" from ").append(this.sqlTablename).append(" where ").append(sql_where).append(" order by ").append(this_orderby).append(" ) order by ").append(this_orderby).toString());
                        }
                    } else if (this.pageIndex == 0) {
                        temp_sql_buf.append(new StringBuffer().append(" where ").append(sql_where).toString());
                    } else {
                        temp_sql_buf.append(new StringBuffer().append(" where ").append(sql_where).append(" and ").append(this.relateField).append(" not in ( select top ").append(String.valueOf(this.pageMax * this.pageIndex)).append(" ").append(this.relateField).append(" from ").append(this.sqlTablename).append(" where ").append(sql_where).append(" ) ").toString());
                    }
                }
                if (sql_where == null) {
                    if (this_orderby != null) {
                        if (this.pageIndex == 0) {
                            temp_sql_buf.append(new StringBuffer().append(" order by ").append(this_orderby).toString());
                        } else {
                            temp_sql_buf.append(new StringBuffer().append(" where ").append(this.relateField).append(" not in ( select top ").append(String.valueOf(this.pageMax * this.pageIndex)).append(" ").append(this.relateField).append(" from ").append(this.sqlTablename).append(" order by ").append(this_orderby).append(" ) order by ").append(this_orderby).toString());
                        }
                    } else if (this.pageIndex > 0) {
                        temp_sql_buf.append(new StringBuffer().append(" where ").append(this.relateField).append(" not in (select top ").append(String.valueOf(this.pageMax * this.pageIndex)).append(" ").append(this.relateField).append(" from ").append(this.sqlTablename).append(")").toString());
                    }
                }
            } else if (this.topNum != null) {
                temp_sql_buf.append(new StringBuffer().append("select top ").append(this.topNum).append(" ").append(this.sqlFields).append(" from ").append(this.sqlTablename).toString());
                temp_sql_buf.append(" with (nolock) ");
                if (sql_where != null) {
                    temp_sql_buf.append(new StringBuffer().append(" where ").append(sql_where).toString());
                }
                if (this_orderby != null) {
                    temp_sql_buf.append(new StringBuffer().append(" order by ").append(this_orderby).toString());
                }
            } else {
                temp_sql_buf.append(new StringBuffer().append("select top 200 ").append(this.sqlFields).append(" from ").append(this.sqlTablename).toString());
                temp_sql_buf.append(" with (nolock) ");
                if (sql_where != null) {
                    temp_sql_buf.append(new StringBuffer().append(" where ").append(sql_where).toString());
                }
                if (this_orderby != null) {
                    temp_sql_buf.append(new StringBuffer().append(" order by ").append(this_orderby).toString());
                }
            }
        }
        if (this.dbType == 1) {
            temp_sql_buf.append(new StringBuffer().append("select ").append(this.sqlFields).append(" from ").append(this.sqlTablename).toString());
            if (sql_where != null) {
                temp_sql_buf.append(new StringBuffer().append(" where ").append(sql_where).toString());
            }
            if (this_orderby != null) {
                temp_sql_buf.append(new StringBuffer().append(" order by ").append(this_orderby).toString());
            }
            if (this.pageMax > 0) {
                temp_sql_buf.append(new StringBuffer().append(" limit ").append(String.valueOf(this.pageMax * this.pageIndex)).append(dbPresentTag.ROLE_DELIMITER).append(String.valueOf(this.pageMax)).toString());
            }
        }
        if (this.dbType == 2) {
            temp_sql_buf.append(new StringBuffer().append("select ").append(this.sqlFields).append(" from ").append(this.sqlTablename).toString());
            if (sql_where != null) {
                temp_sql_buf.append(new StringBuffer().append(" where ").append(sql_where).toString());
            }
            if (this_orderby != null) {
                temp_sql_buf.append(new StringBuffer().append(" order by ").append(this_orderby).toString());
            }
            if (this.pageMax > 0) {
                temp_sql_buf.append(new StringBuffer().append(" limit ").append(String.valueOf(this.pageMax)).append(" offset ").append(String.valueOf(this.pageMax * this.pageIndex)).toString());
            }
        }
        return temp_sql_buf.toString();
    }

    public int doStartTag() throws JspException {
        return 2;
    }

    public int doAfterBody() throws JspException {
        if (this.bodyContent != null) {
            this.bodySqlWhere = this.bodyContent.getString();
            if (this.bodySqlWhere != null) {
                this.bodySqlWhere = this.bodySqlWhere.trim();
            }
            if (this.bodySqlWhere.length() < 1) {
                this.bodySqlWhere = null;
            }
        } else {
            this.bodySqlWhere = null;
        }
        this.body_start = true;
        this.bodySqlWhere = getFilterString(this.bodySqlWhere, false);
        if ("errorwhere".equals(this.bodySqlWhere)) {
            this.is_value_ok = false;
        }
        return 0;
    }

    private void saveBackUrl() {
        String result;
        HttpServletRequest request = this.pageContext.getRequest();
        String ser_name = request.getServerName();
        long ser_port = (long) request.getServerPort();
        String query_str = request.getQueryString();
        String request_url = request.getRequestURI();
        if (ser_port != 80) {
            result = new StringBuffer().append("http://").append(ser_name).append(":").append(String.valueOf(ser_port)).append(request_url).append("?").append(query_str).toString();
        } else {
            result = new StringBuffer().append("http://").append(ser_name).append(request_url).append("?").append(query_str).toString();
        }
        this.pageContext.getSession().setAttribute("back_url", result);
    }

    private long getPageMax(String sqlTablename, String tt_where, PoolBean pool, OrderedMap max_map, String system_name) {
        Object max_obj;
        long p_max = 0;
        if (0 == 0) {
            try {
                long currentTimeMillis = System.currentTimeMillis();
                if ("{call sp_getTableCount (?,?)}" != 0) {
                    Connection con = pool.getNoInfoConnection();
                    CallableStatement stmt2 = con.prepareCall("{call sp_getTableCount (?,?)}");
                    stmt2.setString(1, sqlTablename);
                    stmt2.setString(2, tt_where);
                    ResultSet rs2 = stmt2.executeQuery();
                    if (rs2.next() && (max_obj = rs2.getObject(1)) != null) {
                        this.maxRecord = MyUtil.getStringToInt(String.valueOf(max_obj), 0);
                    }
                    rs2.close();
                    stmt2.close();
                    con.close();
                    p_max = Math.round(Math.ceil((((double) this.maxRecord) * 1.0d) / ((double) this.pageMax)));
                }
            } catch (SQLException e) {
                System.out.println(new StringBuffer().append(" -- ").append(system_name).append(",getPageMax SQLException : ").append(e.getMessage()).toString());
                System.out.println(new StringBuffer().append("sql:").append((String) null).toString());
                System.out.println(new StringBuffer().append("tt_where at dbListArrayTag:").append(tt_where).toString());
                System.out.println(new StringBuffer().append("sqlTablename at dbListArrayTag:").append(sqlTablename).toString());
                CloseCon.Close(null);
                this.pageContext.setAttribute(this.name, new ArrayList());
                return 6;
            } catch (Exception e2) {
                System.out.println(new StringBuffer().append(" -- ").append(system_name).append(",getPageMax Exception : ").append(e2.getMessage()).toString());
                System.out.println(new StringBuffer().append("sql:").append((String) null).toString());
                System.out.println(new StringBuffer().append("tt_where at dbListArrayTag:").append(tt_where).toString());
                System.out.println(new StringBuffer().append("sqlTablename at dbListArrayTag:").append(sqlTablename).toString());
                CloseCon.Close(null);
                this.pageContext.setAttribute(this.name, new ArrayList());
                return 6;
            }
        } else {
            this.maxRecord = 0;
            p_max = Math.round(Math.ceil((((double) this.maxRecord) * 1.0d) / ((double) this.pageMax)));
        }
        return p_max;
    }

    private void setHotkey(PoolBean pool) {
        String keyword = this.pageContext.getRequest().getParameter("keyword");
        if (keyword != null) {
            String[] key = null;
            if (keyword != null) {
                key = keyword.split(" ");
            }
            int is_en = 0;
            if ("en".equals(pool.getVersion())) {
                is_en = 1;
            }
            try {
                Connection con = pool.getNoInfoConnection();
                Statement t_stmt = con.createStatement();
                for (String str_key : key) {
                    if (str_key != null && str_key.trim().length() > 1) {
                        t_stmt.execute(new StringBuffer().append("exec insert_hotkey_new '").append(str_key.trim()).append("', ").append(pool.getTrade_id()).append(dbPresentTag.ROLE_DELIMITER).append(is_en).toString());
                    }
                }
                t_stmt.close();
                con.close();
            } catch (Exception e) {
                CloseCon.Close(null);
                this.log.error(e.getMessage());
                this.log.error(new StringBuffer().append("str_sql:").append((String) null).toString());
            }
        }
    }

    public int doEndTag() throws JspException {
        Connection con;
        int i;
        ResultSet rs;
        String t_str;
        String tt_str;
        long currentTimeMillis = System.currentTimeMillis();
        if (!this.is_value_ok) {
            return 6;
        }
        con = null;
        this.count_hotkey = false;
        this.pageIndex = 0;
        this.maxPageIndex = 0;
        this.maxRecord = 0;
        this.pageMax = 10;
        String realPath = this.pageContext.getServletContext().getRealPath("/WEB-INF/classes/");
        String pageObj = this.pageContext.getRequest().getParameter("page");
        Object maxPageObj = this.pageContext.getRequest().getAttribute("maxPage");
        Object maxRecordObj = this.pageContext.getRequest().getAttribute("maxRecord");
        if (pageObj != null) {
            this.pageIndex = MyUtil.getStringToInt(String.valueOf(pageObj), 0);
        }
        if (maxPageObj != null) {
            this.maxPageIndex = MyUtil.getStringToInt(String.valueOf(maxPageObj), 0);
        }
        if (maxRecordObj != null) {
            this.maxRecord = MyUtil.getStringToInt(String.valueOf(maxRecordObj), 0);
        }
        String parameter = this.pageContext.getRequest().getParameter("class_id");
        if (this.rowMax == null || this.rowMax.length() <= 0 || this.relateField == null) {
            this.pageMax = 10;
        } else {
            this.pageMax = MyUtil.getStringToInt(this.rowMax, 0);
        }
        if (this.log.isDebugEnabled()) {
            this.log.debug(new StringBuffer().append("rowMax = ").append(this.rowMax).append(" , pageMax = ").append(this.pageMax).toString());
        }
        PoolBean pool = (PoolBean) this.pageContext.getAttribute("pool", this.scope_type);
        if (pool == null) {
            pool = new InitAction(this.pageContext).getPool();
            this.log.error("pool is null,create new");
        }
        String system_name = pool.getDatabaseInfo().getSystemName();
        String tt_where = createWhereSQL();
        String tt_order = createOrderSQL();
        if (tt_where == null) {
            tt_where = "";
        }
        if (tt_order == null) {
            tt_order = "";
        }
        if (!this.is_value_ok) {
            return 6;
        }
        if (this.log.isDebugEnabled()) {
            this.log.debug(new StringBuffer().append("tt_where : ").append(tt_where).toString());
        }
        long currentTimeMillis2 = System.currentTimeMillis();
        ArrayList rows = new ArrayList();
        ArrayList curFieldList = new ArrayList();
        this.dbType = pool.getDbType();
        DatabaseInfo databaseInfo = pool.getDatabaseInfo();
        long p_max = 0;
        if (this.log.isDebugEnabled()) {
            this.log.debug(new StringBuffer().append("tt_where : ").append(tt_where).toString());
        }
        if (!(this.rowMax == null || this.rowMax.length() <= 0 || this.relateField == null)) {
            p_max = getPageMax(this.sqlTablename, tt_where, pool, null, system_name);
        }
        if (this.log.isDebugEnabled()) {
            this.log.debug(new StringBuffer().append("tt_where : ").append(tt_where).toString());
        }
        String this_sql = createSQL();
        if (this.log.isDebugEnabled()) {
            this.log.debug(new StringBuffer().append(system_name).append(",this_sql:").append(this_sql).toString());
        }
        if (!this.is_value_ok) {
            return 6;
        }
        if ("-1".equals(this.topNum)) {
            this.pageContext.getRequest().setAttribute("sqlWhere", tt_where);
            this.pageContext.getRequest().setAttribute("sql", this_sql);
            this.pageContext.getRequest().setAttribute("orderby", tt_order);
            return 6;
        }
        if (pool.getTrade_id() != null && (this.count_hotkey || "true".equals(this.isKeyword))) {
            setHotkey(pool);
        }
        try {
            con = pool.getNoInfoConnection();
            con.setReadOnly(true);
            if (this.relateField == null) {
                this.relateField = "";
            }
            if (this.sql == null) {
                rs = con.createStatement().executeQuery(this_sql);
            } else {
                rs = con.createStatement().executeQuery(createSQL());
            }
            ResultSetMetaData rsmd = rs.getMetaData();
            int colCount = rsmd.getColumnCount();
            this.pageContext.getRequest().setAttribute("colCount", String.valueOf(colCount));
            int index = 0;
            int index_1 = 1;
            while (rs.next()) {
                HashMap row = new HashMap();
                row.put("index", String.valueOf(index));
                row.put("index_1", String.valueOf(index_1));
                long t_index = (long) ((this.pageIndex * this.pageMax) + index);
                row.put("t_index", String.valueOf(t_index));
                row.put("t_index_1", String.valueOf(1 + t_index));
                for (int i2 = 1; i2 <= colCount; i2++) {
                    String name = rsmd.getColumnName(i2).toLowerCase();
                    if ("text".equals(rsmd.getColumnTypeName(i2).toLowerCase())) {
                        String tt_str2 = rs.getString(i2);
                        if (tt_str2 == null) {
                            tt_str = "";
                        } else {
                            tt_str = tt_str2.trim();
                        }
                        row.put(name.trim(), tt_str);
                    } else {
                        Object obj = rs.getObject(i2);
                        if (obj instanceof String) {
                            if (obj == null) {
                                t_str = "";
                            } else {
                                t_str = String.valueOf(obj).trim();
                            }
                            row.put(name.trim(), t_str);
                        } else {
                            row.put(name.trim(), obj);
                        }
                        if (this.rec_field_name != null && this.rec_field_name.equalsIgnoreCase(name)) {
                            curFieldList.add(obj);
                        }
                    }
                }
                index++;
                index_1++;
                rows.add(row);
            }
            this.pageContext.getRequest().setAttribute("rowCount", String.valueOf(index));
            con.setReadOnly(false);
            con.close();
            CloseCon.Close(null);
            this.pageContext.setAttribute(this.name, rows);
            if (this.rowMax != null && this.rowMax.length() > 0) {
                this.pageContext.getRequest().setAttribute("maxPage", String.valueOf(p_max));
                if (this.log.isDebugEnabled()) {
                    this.log.debug("pageMax is disable");
                }
                this.pageContext.getRequest().setAttribute("maxRecord", String.valueOf(this.maxRecord));
            }
            if (curFieldList != null && curFieldList.size() > 0) {
                this.pageContext.setAttribute("curFieldList", curFieldList);
            }
            this.pageContext.removeAttribute("nokey");
            if (this.topNum != null && this.topNum.length() > 0) {
                this.pageContext.setAttribute("nokey", "true");
            }
            this.body_start = false;
            return 6;
        } catch (SQLException e) {
            CloseCon.Close(con);
            HttpServletRequest request = this.pageContext.getRequest();
            String strServerName = request.getServerName();
            String strRequestURI = request.getRequestURI();
            String strQueryString = request.getQueryString();
            String contextPath = request.getContextPath();
            String referer = request.getHeader("referer");
            this.log.error(new StringBuffer().append(request.getRemoteAddr()).append(" , ").append(strServerName).append(" , ").append(strRequestURI).append(" , ").append(strQueryString).toString());
            if (this.log.isInfoEnabled()) {
                this.log.info(new StringBuffer().append("referer:").append(referer).toString());
            }
            this.log.error(new StringBuffer().append("this_sql:").append(this_sql).toString());
            i = 6;
        } catch (Exception ee) {
            CloseCon.Close(con);
            this.log.error(ee.getMessage());
            i = 6;
        } catch (Throwable th) {
            CloseCon.Close(con);
            throw th;
        }
        CloseCon.Close(con);
        return i;
    }

    public String getTopNum() {
        return this.topNum;
    }

    public void setTopNum(String topNum2) {
        this.topNum = topNum2;
    }
}
