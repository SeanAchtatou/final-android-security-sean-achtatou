package com.kenfor.taglib.db;

import com.kenfor.database.PoolBean;
import com.kenfor.exutil.InitAction;
import com.kenfor.taglib.util.ClassItems;
import java.io.IOException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import javax.servlet.jsp.JspException;
import org.apache.struts.util.RequestUtils;

public class dbSelectTag extends dbTag {
    private boolean bFieldMsg = false;
    private boolean bFirst = true;
    private String blankMsg = null;
    private String blankValue = "";
    private String bodySqlWhere = null;
    private boolean bonlyL = false;
    private boolean bonlySL = false;
    private String className = null;
    private String disFieldName = "NAME";
    private String hasBlank = "false";
    private String hasMultiple = "false";
    private String hasParent = "true";
    private String id = null;
    protected String id_field_name = "bas_id";
    protected String isDebug = "false";
    private String isFieldMsg = "false";
    private int layer = 0;
    private Locale locale = null;
    private String msgResources = "com.kenfor.resources.ApplicationResources";
    private String name = "BAS_ID";
    private String new_flag = "false";
    private String onChange = null;
    private String onlyLeaf = "false";
    private String onlyShowLeaf = "false";
    private String parentIdName = "PARENT_ID";
    private PoolBean pool = null;
    protected String scopeType = "4";
    protected int scope_type = 4;
    private String selectedName = null;
    private String selectedProperty = null;
    private String selectedValue = null;
    private String size = null;
    private Statement stmt = null;
    private StringBuffer strBuf = null;
    private String tableName = "PRD_CLS";
    private String valueFieldName = "BAS_ID";

    public void setId_field_name(String id_field_name2) {
        this.id_field_name = id_field_name2;
    }

    public String getId_field_name() {
        return this.id_field_name;
    }

    public void setIsDebug(String isDebug2) {
        this.isDebug = isDebug2;
    }

    public String getIsDebug() {
        return this.isDebug;
    }

    public void setMsgResources(String msgResources2) {
        this.msgResources = msgResources2;
    }

    public String getMsgResources() {
        return this.msgResources;
    }

    public void setIsFieldMsg(String isFieldMsg2) {
        this.isFieldMsg = isFieldMsg2;
        if (isFieldMsg2.equalsIgnoreCase("true")) {
            this.bFieldMsg = true;
        } else {
            this.bFieldMsg = false;
        }
    }

    public String getIsFieldMsg() {
        return this.isFieldMsg;
    }

    public void setScopeType(String scopeType2) {
        this.scopeType = scopeType2;
        if (scopeType2 != null) {
            this.scope_type = Integer.valueOf(scopeType2).intValue();
            if (this.scope_type > 4) {
                this.scope_type = 4;
            }
            if (this.scope_type < 1) {
                this.scope_type = 1;
            }
        }
    }

    public String getScopeType() {
        return this.scopeType;
    }

    public void setOnlyLeaf(String onlyLeaf2) {
        this.onlyLeaf = onlyLeaf2;
        if (this.onlyLeaf.compareToIgnoreCase("true") == 0) {
            this.bonlyL = true;
        }
    }

    public String getOnlyLeaf() {
        return this.onlyLeaf;
    }

    public void setHasParent(String hasParent2) {
        this.hasParent = hasParent2;
    }

    public String getHasParent() {
        return this.hasParent;
    }

    public void setParentIdName(String parentIdName2) {
        this.parentIdName = parentIdName2;
    }

    public String getParentIdName() {
        return this.parentIdName;
    }

    public void setTableName(String tableName2) {
        this.tableName = tableName2;
        this.sqlTablename = tableName2;
    }

    public String getTableName() {
        return this.tableName;
    }

    public void setDisFieldName(String disFieldName2) {
        this.disFieldName = disFieldName2;
    }

    public String getDisFieldName() {
        return this.disFieldName;
    }

    public void setValueFieldName(String valueFieldName2) {
        this.valueFieldName = valueFieldName2;
    }

    public String getValueFieldName() {
        return this.valueFieldName;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getName() {
        return this.name;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getId() {
        return this.id;
    }

    public void setClassName(String className2) {
        this.className = className2;
    }

    public String getClassName() {
        return this.className;
    }

    public void setHasBlank(String hasBlank2) {
        this.hasBlank = hasBlank2;
    }

    public String getHasBlank() {
        return this.hasBlank;
    }

    public void setBlankMsg(String blankMsg2) {
        this.blankMsg = blankMsg2;
    }

    public String getBlankMsg() {
        return this.blankMsg;
    }

    public void setBlankValue(String blankValue2) {
        this.blankValue = blankValue2;
    }

    public String getBlankValue() {
        return this.blankValue;
    }

    public void setOnChange(String onChange2) {
        this.onChange = onChange2;
    }

    public String getOnChange() {
        return this.onChange;
    }

    public void setSize(String size2) {
        this.size = size2;
    }

    public String getSize() {
        return this.size;
    }

    public void setHasMultiple(String hasMultiple2) {
        this.hasMultiple = hasMultiple2;
    }

    public String getHasMultiple() {
        return this.hasMultiple;
    }

    public void setSelectedName(String selectedName2) {
        this.selectedName = selectedName2;
    }

    public String getSelectName() {
        return this.selectedName;
    }

    public void setSelectedProperty(String selectedProperty2) {
        this.selectedProperty = selectedProperty2;
    }

    public String getSelectedProperty() {
        return this.selectedProperty;
    }

    public void setSelectedValue(String selectedValue2) {
        this.selectedValue = selectedValue2;
    }

    public String getSelectedValue() {
        return this.selectedValue;
    }

    public void setOnlyShowLeaf(String onlyShowLeaf2) {
        this.onlyShowLeaf = onlyShowLeaf2;
        if (this.onlyShowLeaf.compareToIgnoreCase("true") == 0) {
            this.bonlySL = true;
        }
    }

    public String getOnlyShowLeaf() {
        return this.onlyShowLeaf;
    }

    public int doStartTag() throws JspException {
        return 2;
    }

    public int doAfterBody() throws JspException {
        if (this.bodyContent == null) {
            return 0;
        }
        this.bodySqlWhere = this.bodyContent.getString();
        if (this.bodySqlWhere != null) {
            this.bodySqlWhere = this.bodySqlWhere.trim();
        }
        if (this.bodySqlWhere.length() >= 1) {
            return 0;
        }
        this.bodySqlWhere = null;
        return 0;
    }

    /* JADX INFO: Multiple debug info for r3v2 java.lang.Object: [D('selObj' java.lang.String), D('selObj' java.lang.Object)] */
    public int doEndTag() throws JspException {
        Object value;
        String tt_sqlWhere = createWhereSQL();
        String tt_sqlOrder = createOrderSQL();
        String tt_sql = new StringBuffer().append("select ").append(this.sqlFields).append(" from ").append(this.sqlTablename).toString();
        if (tt_sqlWhere != null) {
            tt_sql = new StringBuffer().append(tt_sql).append(" where ").append(tt_sqlWhere).toString();
        }
        if (tt_sqlOrder != null) {
            tt_sql = new StringBuffer().append(tt_sql).append(" order by ").append(tt_sqlOrder).toString();
        }
        this.strBuf = new StringBuffer();
        this.locale = this.pageContext.getRequest().getLocale();
        String path = this.pageContext.getServletContext().getRealPath("/WEB-INF/classes/");
        if (this.selectedName != null) {
            if (this.selectedProperty != null) {
                try {
                    value = RequestUtils.lookup(this.pageContext, this.selectedName, this.selectedProperty, (String) null);
                } catch (Exception e) {
                    value = null;
                }
                if (value != null) {
                    this.selectedValue = String.valueOf(value);
                }
            }
            if (this.selectedProperty == null) {
                Object selObj = this.pageContext.getRequest().getParameter(this.selectedName);
                if (selObj == null) {
                    selObj = this.pageContext.getAttribute(this.selectedName);
                }
                if (selObj != null) {
                    this.selectedValue = String.valueOf(selObj);
                }
            }
        }
        this.bFirst = true;
        try {
            initMenu(path, tt_sql);
            StringBuffer out_str_buf = new StringBuffer();
            out_str_buf.append(new StringBuffer().append("<select name=\"").append(this.name).append("\"").toString());
            if (this.id != null) {
                out_str_buf.append(new StringBuffer().append(" id = \"").append(this.id).append("\"").toString());
            }
            if (this.className != null) {
                out_str_buf.append(new StringBuffer().append(" class = '").append(this.className).append("'").toString());
            }
            if (this.onChange != null) {
                out_str_buf.append(new StringBuffer().append(" onchange = \"").append(this.onChange).append("\"").toString());
            }
            if (this.size != null) {
                out_str_buf.append(new StringBuffer().append(" size = \"").append(this.size).append("\"").toString());
            }
            if (this.hasMultiple.compareToIgnoreCase("true") == 0) {
                out_str_buf.append(" multiple ");
            }
            out_str_buf.append(" >");
            if (this.hasBlank.compareToIgnoreCase("true") == 0) {
                if (this.blankMsg != null) {
                    out_str_buf.append(new StringBuffer().append("<option value=\"").append(this.blankValue).append("\">").append(this.blankMsg).append("</option>").toString());
                } else {
                    out_str_buf.append(new StringBuffer().append("<option value=\"").append(this.blankValue).append("\"></option>").toString());
                }
            }
            out_str_buf.append(this.strBuf);
            out_str_buf.append("</select>");
            try {
                this.pageContext.getOut().write(out_str_buf.toString());
                this.pageContext.getOut().flush();
            } catch (IOException e2) {
                throw new JspException(new StringBuffer().append("IO Error: ").append(e2.getMessage()).toString());
            }
        } catch (Exception e3) {
            this.log.error(e3.getMessage());
            this.log.error(new StringBuffer().append("tt_sql:").append(tt_sql).toString());
        }
        return 6;
    }

    public void initMenu(String path, String sql) {
        int i_level;
        String str;
        this.pool = (PoolBean) this.pageContext.getServletContext().getAttribute("pool");
        if (this.pool == null) {
            this.pool = new InitAction(this.pageContext).getPool();
            this.log.error("pool is null,create new");
        }
        if (!this.pool.isStarted()) {
            this.log.error("pool is not started");
            return;
        }
        ClassItems c_item = new ClassItems();
        c_item.setIdFieldName(this.id_field_name);
        c_item.setParentFieldName(this.parentIdName);
        c_item.initData(this.pool, sql);
        while (true) {
            HashMap t_map = c_item.getDataItem("0", "0", this.hasParent);
            if (t_map != null) {
                Object disObj_sub = t_map.get(this.disFieldName);
                Object valueObj_sub = t_map.get(this.valueFieldName);
                String str_value = String.valueOf(disObj_sub);
                String level = String.valueOf(t_map.get("level"));
                String valueOf = String.valueOf(t_map.get("isLeaf"));
                try {
                    i_level = Integer.valueOf(level).intValue();
                } catch (Exception e) {
                    i_level = 1;
                }
                String beforeStr = "";
                for (int m = 1; m < i_level; m++) {
                    beforeStr = new StringBuffer().append(beforeStr).append("--").toString();
                }
                String str_valueObj = String.valueOf(valueObj_sub);
                if (str_valueObj != null) {
                    str_valueObj = str_valueObj.trim();
                }
                if (this.selectedValue == null || this.selectedValue.compareToIgnoreCase(String.valueOf(valueObj_sub)) != 0) {
                    if (this.selectedName != null && ((this.selectedValue == null || "null".equalsIgnoreCase(this.selectedValue)) && this.bFirst)) {
                        this.bFirst = false;
                    }
                    str = new StringBuffer().append("<option value=\"").append(str_valueObj).append("\">").append(beforeStr).append(str_value).append("</option>").toString();
                } else {
                    str = new StringBuffer().append("<option value=\"").append(str_valueObj).append("\" selected>").append(beforeStr).append(str_value).append("</option>").toString();
                }
                this.strBuf.append(str);
            } else {
                c_item.clearData();
                return;
            }
        }
    }

    public void release() {
        this.hasParent = "true";
        this.parentIdName = "PARENT_ID";
        this.tableName = "PRD_CLS";
        this.disFieldName = "NAME";
        this.valueFieldName = "BAS_ID";
        this.name = "BAS_ID";
        this.id = null;
        this.className = null;
        this.hasBlank = "false";
        this.blankMsg = null;
        this.blankValue = "0";
        this.onChange = null;
        this.size = null;
        this.hasMultiple = "false";
        this.selectedName = null;
        this.selectedProperty = null;
        this.sqlWhere = null;
        this.isDebug = "false";
        this.scopeType = "4";
        this.scope_type = 4;
        this.propName = null;
        this.id_field_name = "bas_id";
    }

    public String getNew_flag() {
        return this.new_flag;
    }

    public void setNew_flag(String new_flag2) {
        this.new_flag = new_flag2;
    }

    /* access modifiers changed from: protected */
    public String createWhereSQL() {
        if (this.new_flag == null || this.new_flag.equalsIgnoreCase("false")) {
            return createWhereSQL(true);
        }
        return createNewWhereSQL();
    }

    /* access modifiers changed from: protected */
    public String createNewWhereSQL() {
        String result;
        String t_result;
        String temp_sql = null;
        int t_index = -1;
        if (this.bodySqlWhere != null && this.bodySqlWhere.length() > 0) {
            temp_sql = this.bodySqlWhere;
        } else if (this.sqlWhere != null && this.sqlWhere.length() > 0) {
            temp_sql = this.sqlWhere;
        }
        ArrayList paramPre = new ArrayList();
        ArrayList paramNex = new ArrayList();
        ArrayList paramTotal = new ArrayList();
        StringBuffer resultBuf = new StringBuffer();
        String t_sql = temp_sql;
        if (t_sql != null) {
            t_index = t_sql.indexOf("[");
        }
        if (t_index > 0) {
            while (t_index > 0) {
                int t_index2 = t_sql.indexOf("[");
                int t_index22 = t_sql.indexOf("]");
                if (t_index22 < 0) {
                    return null;
                }
                String total_str = t_sql.substring(0, t_index2);
                if (total_str != null && total_str.trim().length() > 0) {
                    paramTotal.add(total_str);
                }
                String p_str = t_sql.substring(t_index2 + 1, t_index22);
                t_sql = t_sql.substring(t_index22 + 1);
                int p_index = p_str.indexOf("::");
                String p_str_b = p_str.substring(0, p_index - 1);
                String p_str_a = p_str.substring(p_index + 2);
                paramPre.add(p_str_b);
                paramNex.add(p_str_a);
                t_index = t_sql.indexOf("[");
            }
            int pp_size = paramPre.size();
            for (int i = 0; i < pp_size; i++) {
                String str2 = null;
                String str1 = (String) paramPre.get(i);
                if (str1 != null) {
                    str1 = str1.trim();
                }
                if (paramNex.size() > i) {
                    str2 = (String) paramNex.get(i);
                }
                if (str2 != null) {
                    str2 = str2.trim();
                }
                String value = null;
                String b_str = null;
                String e_str = null;
                if (str2 != null) {
                    if (str2.length() > 1) {
                        b_str = str2.substring(0, 1);
                        e_str = str2.substring(str2.length() - 1, str2.length());
                    }
                    if ("%".equals(b_str) && str2.length() >= 1) {
                        str2 = str2.substring(1, str2.length());
                    }
                    if ("%".equals(e_str) && str2.length() >= 1) {
                        str2 = str2.substring(0, str2.length() - 1);
                    }
                    value = this.pageContext.getRequest().getParameter(str2.trim());
                    if (value == null || value.equalsIgnoreCase("null")) {
                        value = (String) this.pageContext.getRequest().getAttribute(str2.trim());
                    }
                    if (value == null || value.equalsIgnoreCase("null")) {
                        value = (String) this.pageContext.getAttribute(str2.trim());
                    }
                    if (value == null || value.equalsIgnoreCase("null")) {
                        value = (String) this.pageContext.getSession().getAttribute(str2.trim());
                    }
                    if ((value == null || value.equalsIgnoreCase("null") || value.trim().length() <= 0) && this.propName != null && this.propName.length() > 1) {
                        try {
                            Object obj_value = RequestUtils.lookup(this.pageContext, this.propName, str2.trim(), (String) null);
                            if (obj_value != null) {
                                value = String.valueOf(obj_value);
                            }
                        } catch (Exception e) {
                            value = null;
                        }
                    }
                }
                if (value != null && !value.equalsIgnoreCase("null") && value.trim().length() > 0) {
                    this.pageContext.setAttribute(str2.trim(), value.trim());
                    if (this.isSubSQL.equals("true")) {
                        temp_sql = temp_sql.replaceFirst(str2, value.trim());
                    } else {
                        String value2 = value.trim();
                        if ("%".equals(b_str)) {
                            value2 = new StringBuffer().append("%").append(value2).toString();
                        }
                        if ("%".equals(e_str)) {
                            value2 = new StringBuffer().append(value2).append("%").toString();
                        }
                        int str1_len = str1.indexOf("==");
                        if (str1_len > 0) {
                            t_result = new StringBuffer().append(str1.substring(0, str1_len + 1)).append(value2).toString();
                        } else {
                            t_result = new StringBuffer().append(str1).append(" '").append(value2).append("' ").toString();
                        }
                        paramTotal.add(t_result);
                    }
                } else if (this.isSubSQL.equals("true")) {
                    temp_sql = temp_sql.replaceFirst(str2, "0");
                } else if (!this.isIgnore.equalsIgnoreCase("true")) {
                    paramTotal.add(new StringBuffer().append(str1).append(" 0").toString());
                }
            }
            if (!this.isSubSQL.equals("true")) {
                int pt_size = paramTotal.size();
                for (int j = 0; j < pt_size; j++) {
                    String t_result2 = (String) paramTotal.get(j);
                    if (t_result2 != null && t_result2.trim().length() >= 1) {
                        resultBuf.append(t_result2);
                    }
                }
            } else if (temp_sql != null) {
                resultBuf.append(temp_sql);
            }
        } else if (t_sql != null) {
            resultBuf.append(t_sql);
        }
        if (resultBuf == null) {
            result = null;
        } else {
            result = resultBuf.toString();
        }
        paramPre.clear();
        paramNex.clear();
        paramTotal.clear();
        return result;
    }

    /* access modifiers changed from: protected */
    public String createWhereSQL(boolean bdb_sql_where) {
        int t_index;
        String result;
        String t_result;
        String temp_sql = null;
        int t_index2 = -1;
        if (this.bodySqlWhere != null && this.bodySqlWhere.length() > 0) {
            temp_sql = this.bodySqlWhere;
        } else if (this.sqlWhere != null && this.sqlWhere.length() > 0) {
            temp_sql = this.sqlWhere;
        }
        if (bdb_sql_where && this.db_sql_where != null) {
            if (temp_sql != null) {
                temp_sql = new StringBuffer().append(temp_sql).append(this.db_sql_where).toString();
            } else {
                temp_sql = this.db_sql_where;
            }
        }
        ArrayList paramPre = new ArrayList();
        ArrayList paramNex = new ArrayList();
        ArrayList paramTotal = new ArrayList();
        StringBuffer resultBuf = new StringBuffer();
        String t_sql = temp_sql;
        if (t_sql != null) {
            t_index2 = t_sql.indexOf("::");
        }
        if (t_index > 0) {
            while (t_index > 0) {
                int t_index3 = t_sql.indexOf("::");
                String sub_str1 = null;
                if (t_index3 > 0) {
                    sub_str1 = t_sql.substring(0, t_index3);
                }
                int r_index = -1;
                if (sub_str1 != null) {
                    r_index = sub_str1.lastIndexOf("and");
                }
                if (r_index > 0) {
                    paramTotal.add(sub_str1.substring(0, r_index));
                    sub_str1 = sub_str1.substring(r_index + 3);
                }
                paramPre.add(sub_str1);
                String sub_str2 = t_sql.substring(t_index3 + 2);
                int tt_index = sub_str2.indexOf("and");
                if (tt_index > 0) {
                    paramNex.add(sub_str2.substring(0, tt_index));
                    t_sql = sub_str2.substring(tt_index + 3);
                    t_index = 1;
                } else {
                    paramNex.add(sub_str2);
                    t_index = -1;
                }
            }
            int pp_size = paramPre.size();
            for (int i = 0; i < pp_size; i++) {
                String str2 = null;
                String str1 = (String) paramPre.get(i);
                if (str1 != null) {
                    str1 = str1.trim();
                }
                if (paramNex.size() > i) {
                    str2 = (String) paramNex.get(i);
                }
                if (str2 != null) {
                    str2 = str2.trim();
                }
                String value = null;
                String b_str = null;
                String e_str = null;
                if (str2 != null) {
                    if (str2.length() > 1) {
                        b_str = str2.substring(0, 1);
                        e_str = str2.substring(str2.length() - 1, str2.length());
                    }
                    if ("%".equals(b_str) && str2.length() >= 1) {
                        str2 = str2.substring(1, str2.length());
                    }
                    if ("%".equals(e_str) && str2.length() >= 1) {
                        str2 = str2.substring(0, str2.length() - 1);
                    }
                    value = this.pageContext.getRequest().getParameter(str2.trim());
                    if (value == null || value.equalsIgnoreCase("null")) {
                        value = (String) this.pageContext.getRequest().getAttribute(str2.trim());
                    }
                    if (value == null || value.equalsIgnoreCase("null")) {
                        value = (String) this.pageContext.getAttribute(str2.trim());
                    }
                    if ((value == null || value.equalsIgnoreCase("null") || value.trim().length() <= 0) && this.propName != null && this.propName.length() > 1) {
                        try {
                            Object obj_value = RequestUtils.lookup(this.pageContext, this.propName, str2.trim(), (String) null);
                            if (obj_value != null) {
                                value = String.valueOf(obj_value);
                            }
                        } catch (Exception e) {
                            System.out.println(new StringBuffer().append("get value by RequestUtils exceptione:").append(e.getMessage()).toString());
                            value = null;
                        }
                    }
                }
                if (value != null && !value.equalsIgnoreCase("null") && value.trim().length() > 0) {
                    this.pageContext.setAttribute(str2.trim(), value.trim());
                    if (this.isSubSQL.equals("true")) {
                        temp_sql = temp_sql.replaceFirst(str2, value.trim());
                    } else {
                        String value2 = value.trim().replaceAll("'", "''");
                        if ("%".equals(b_str)) {
                            value2 = new StringBuffer().append("%").append(value2).toString();
                        }
                        if ("%".equals(e_str)) {
                            value2 = new StringBuffer().append(value2).append("%").toString();
                        }
                        int str1_len = str1.indexOf("==");
                        if (str1_len > 0) {
                            t_result = new StringBuffer().append(str1.substring(0, str1_len + 1)).append(value2).toString();
                        } else {
                            t_result = new StringBuffer().append(str1).append(" '").append(value2).append("' ").toString();
                        }
                        paramTotal.add(t_result);
                    }
                } else if (this.isSubSQL.equals("true")) {
                    temp_sql = temp_sql.replaceFirst(str2, "0");
                } else if (!this.isIgnore.equalsIgnoreCase("true")) {
                    paramTotal.add(new StringBuffer().append(str1).append(" 0").toString());
                }
            }
            if (!this.isSubSQL.equals("true")) {
                int pt_size = paramTotal.size();
                for (int j = 0; j < pt_size; j++) {
                    resultBuf.append((String) paramTotal.get(j));
                    if (j < paramTotal.size() - 1) {
                        resultBuf.append(" and ");
                    }
                }
            } else if (temp_sql != null) {
                resultBuf.append(temp_sql);
            }
        } else if (t_sql != null) {
            resultBuf.append(t_sql);
        }
        if (resultBuf == null) {
            result = null;
        } else {
            result = resultBuf.toString();
        }
        if (result == null || result.length() == 0) {
            result = null;
        }
        paramPre.clear();
        paramNex.clear();
        paramTotal.clear();
        return result;
    }

    /* access modifiers changed from: protected */
    public String createOrderSQL() {
        String temp_orderby = this.sqlOrderby;
        if (this.sqlOrderby == null || this.sqlOrderby.length() <= 0 || this.sqlOrderby.indexOf("::") < 0) {
            return temp_orderby;
        }
        String temp = temp_orderby.substring(temp_orderby.indexOf("::") + 2).trim();
        int t2_index = temp.indexOf("[");
        int t3_index = temp.indexOf("]");
        String default_order = null;
        if (t2_index > 0) {
            default_order = temp.substring(t2_index + 1, t3_index);
            temp = temp.substring(0, t2_index);
        }
        String value = null;
        if (temp != null && ((value = this.pageContext.getRequest().getParameter(temp.trim())) == null || value.equalsIgnoreCase("null"))) {
            value = (String) this.pageContext.getAttribute(temp.trim());
        }
        if (value != null) {
            return value;
        }
        if (default_order != null) {
            return default_order;
        }
        return "";
    }
}
