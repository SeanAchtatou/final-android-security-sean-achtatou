package com.kenfor.taglib.db;

import com.kenfor.util.MyUtil;
import java.io.IOException;
import java.lang.reflect.Array;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.BodyTagSupport;
import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.util.RequestUtils;
import org.apache.struts.util.ResponseUtils;
import org.jivesoftware.smackx.workgroup.packet.SessionID;

public class dbIterateTag extends BodyTagSupport {
    private ResultSet _rset = null;
    private String id = "row";
    private int index = 0;
    private int index_1 = 1;
    protected Iterator iterator = null;
    Log log = LogFactory.getLog(getClass().getName());
    private String name = "rows";

    public void removeAttri(PageContext pageContext, String name2, String scope) throws JspTagException {
        if (scope == null) {
            pageContext.removeAttribute(name2);
            pageContext.removeAttribute(name2, 2);
            pageContext.removeAttribute(name2, 3);
            pageContext.removeAttribute(name2, 4);
        } else if (scope.equalsIgnoreCase("page")) {
            pageContext.removeAttribute(name2);
        } else if (scope.equalsIgnoreCase("request")) {
            pageContext.removeAttribute(name2, 2);
        } else if (scope.equalsIgnoreCase(SessionID.ELEMENT_NAME)) {
            pageContext.removeAttribute(name2, 3);
        } else if (scope.equalsIgnoreCase("application")) {
            pageContext.removeAttribute(name2, 4);
        }
    }

    /* access modifiers changed from: protected */
    public Object lookup(PageContext pageContext, String name2, String scope) throws JspTagException {
        if (scope == null) {
            Object bean = pageContext.findAttribute(name2);
            if (bean == null) {
                bean = pageContext.getAttribute(name2, 2);
            }
            if (bean == null) {
                bean = pageContext.getAttribute(name2, 3);
            }
            if (bean == null) {
                return pageContext.getAttribute(name2, 4);
            }
            return bean;
        } else if (scope.equalsIgnoreCase("page")) {
            return pageContext.getAttribute(name2, 1);
        } else {
            if (scope.equalsIgnoreCase("request")) {
                return pageContext.getAttribute(name2, 2);
            }
            if (scope.equalsIgnoreCase(SessionID.ELEMENT_NAME)) {
                return pageContext.getAttribute(name2, 3);
            }
            if (scope.equalsIgnoreCase("application")) {
                return pageContext.getAttribute(name2, 4);
            }
            throw new JspTagException(new StringBuffer().append("Invalid scope ").append(scope).toString());
        }
    }

    public int doStartTag() throws JspException {
        long currentTimeMillis = System.currentTimeMillis();
        this.index = 0;
        this.index_1 = 1;
        this.bodyContent = null;
        int maxPageIndex = 0;
        int pageIndex = 0;
        String pageObj = this.pageContext.getRequest().getParameter("page");
        Object maxPageObj = this.pageContext.getAttribute("maxPage");
        Object maxRecordObj = this.pageContext.getAttribute("maxRecord");
        if (pageObj != null) {
            pageIndex = MyUtil.getStringToInt(String.valueOf(pageObj), 0);
        }
        if (maxPageObj != null) {
            maxPageIndex = MyUtil.getStringToInt(String.valueOf(maxPageObj), 0);
        }
        if (maxRecordObj != null) {
            int maxRecord = MyUtil.getStringToInt(String.valueOf(maxRecordObj), 0);
        }
        Object collection = lookup(this.pageContext, this.name, null);
        if (collection == null) {
            this.log.info("数据集为空，可能是查询出了错误");
            return 0;
        }
        if (collection instanceof ArrayList) {
            this.iterator = ((ArrayList) collection).iterator();
        } else if (collection instanceof ResultSet) {
            this._rset = (ResultSet) collection;
            this.pageContext.setAttribute(getId(), this._rset);
            try {
                if (!this._rset.next()) {
                    return 0;
                }
            } catch (SQLException e) {
                throw new JspTagException(new StringBuffer().append("same error at dbIterateTag :").append(e.toString()).toString());
            }
        } else if (collection.getClass().isArray()) {
            try {
                this.iterator = Arrays.asList((Object[]) collection).iterator();
            } catch (ClassCastException e2) {
                int length = Array.getLength(collection);
                ArrayList c = new ArrayList(length);
                for (int i = 0; i < length; i++) {
                    c.add(Array.get(collection, i));
                }
                this.iterator = c.iterator();
            }
        } else if (collection instanceof Collection) {
            this.iterator = ((Collection) collection).iterator();
        } else if (collection instanceof Iterator) {
            this.iterator = (Iterator) collection;
        } else if (collection instanceof Enumeration) {
            this.iterator = IteratorUtils.asIterator((Enumeration) collection);
        } else {
            JspException e3 = new JspException("can not get collection at dbIterateTag ");
            RequestUtils.saveException(this.pageContext, e3);
            throw e3;
        }
        if (this._rset == null && this.iterator != null) {
            if (!this.iterator.hasNext()) {
                return 0;
            }
            Object element = this.iterator.next();
            if (element == null) {
                this.pageContext.removeAttribute(this.id);
            } else {
                this.pageContext.setAttribute(this.id, element);
            }
            this.iterator.remove();
        }
        if (this._rset != null) {
            int t_index = (pageIndex * maxPageIndex) + this.index_1;
            setRowCount(new StringBuffer().append(this.id).append("_index").toString(), this.index);
            setRowCount(new StringBuffer().append(this.id).append("_index_1").toString(), this.index_1);
            setRowCount(new StringBuffer().append(this.id).append("_t_index").toString(), t_index);
            this.index = this.index + 1;
            this.index_1 = this.index_1 + 1;
        }
        return 2;
    }

    public int doEndTag() throws JspTagException {
        this.pageContext.removeAttribute(this.id);
        this.pageContext.removeAttribute(this.name);
        try {
            if (!(getBodyContent() == null || getPreviousOut() == null)) {
                getPreviousOut().write(getBodyContent().getString());
            }
            if (this._rset != null) {
                try {
                    this._rset.close();
                    Statement stmt = (Statement) this.pageContext.getAttribute(new StringBuffer().append(this.name).append("_stmt").toString());
                    if (stmt != null) {
                        stmt.close();
                    }
                    Connection con = (Connection) this.pageContext.getAttribute(new StringBuffer().append(this.name).append("_con").toString());
                    if (con != null) {
                        con.close();
                    }
                } catch (SQLException e) {
                }
            }
            if (this.iterator != null) {
                try {
                    this.iterator.remove();
                } catch (Exception e2) {
                }
                this.iterator = null;
            }
            this._rset = null;
            long currentTimeMillis = System.currentTimeMillis();
            return 6;
        } catch (IOException e3) {
            throw new JspTagException(e3.toString());
        }
    }

    public int doAfterBody() throws JspException {
        if (this.bodyContent != null) {
            ResponseUtils.writePrevious(this.pageContext, this.bodyContent.getString());
            this.bodyContent.clearBody();
        }
        int maxPageIndex = 0;
        int pageIndex = 0;
        String pageObj = this.pageContext.getRequest().getParameter("page");
        Object maxPageObj = this.pageContext.getAttribute("maxPage");
        Object maxRecordObj = this.pageContext.getAttribute("maxRecord");
        if (pageObj != null) {
            pageIndex = MyUtil.getStringToInt(String.valueOf(pageObj), 0);
        }
        if (maxPageObj != null) {
            maxPageIndex = MyUtil.getStringToInt(String.valueOf(maxPageObj), 0);
        }
        if (maxRecordObj != null) {
            int maxRecord = MyUtil.getStringToInt(String.valueOf(maxRecordObj), 0);
        }
        if (this._rset != null) {
            try {
                if (this._rset.next()) {
                    ResultSet resultSet = (ResultSet) this.pageContext.getAttribute(this.id);
                    int t_index = (pageIndex * maxPageIndex) + this.index_1;
                    setRowCount(new StringBuffer().append(this.id).append("_index").toString(), this.index);
                    setRowCount(new StringBuffer().append(this.id).append("_index_1").toString(), this.index_1);
                    setRowCount(new StringBuffer().append(this.id).append("_t_index").toString(), t_index);
                    this.index++;
                    this.index_1++;
                    return 2;
                }
            } catch (SQLException e) {
                throw new JspTagException(e.toString());
            }
        } else if (this.iterator != null) {
            if (!this.iterator.hasNext()) {
                return 0;
            }
            Object element = this.iterator.next();
            if (element == null) {
                this.pageContext.removeAttribute(this.id);
            } else {
                this.pageContext.setAttribute(this.id, element);
            }
            this.iterator.remove();
            return 2;
        }
        return 6;
    }

    public void release() {
        dbIterateTag.super.release();
        this._rset = null;
        this.name = "rows";
        this.id = "row";
        this.index = 0;
        this.index_1 = 1;
    }

    /* access modifiers changed from: protected */
    public void setRowCount(String name2, int rowCount) {
        this.pageContext.setAttribute(name2, String.valueOf(rowCount));
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }
}
