package com.kenfor.taglib.db;

import com.kenfor.database.PoolBean;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class dbProcListArrayTag extends TagSupport {
    private String[] param = new String[5];
    private String param1 = null;
    private String param2 = null;
    private String param3 = null;
    private String param4 = null;
    private String param5 = null;
    private String paramCount = "0";
    private String procName = null;
    protected String scopeType = "4";
    protected int scope_type = 4;

    public void release() {
        this.paramCount = "0";
        this.procName = null;
        this.scopeType = "4";
        this.scope_type = 4;
        this.param1 = null;
        this.param2 = null;
        this.param3 = null;
        this.param4 = null;
        this.param5 = null;
    }

    public void setScopeType(String scopeType2) {
        this.scopeType = scopeType2;
        if (scopeType2 != null) {
            this.scope_type = Integer.valueOf(scopeType2).intValue();
            if (this.scope_type > 4) {
                this.scope_type = 4;
            }
            if (this.scope_type < 1) {
                this.scope_type = 1;
            }
        }
    }

    public String getScopeType() {
        return this.scopeType;
    }

    public void setParamCount(String paramCount2) {
        this.paramCount = paramCount2;
    }

    public String getParamCount() {
        return this.paramCount;
    }

    public void setProcName(String procName2) {
        this.procName = procName2;
    }

    public String getProcName() {
        return this.procName;
    }

    public void setParam1(String param12) {
        this.param1 = param12;
        this.param[0] = param12;
    }

    public String getParam1() {
        return this.param1;
    }

    public void setParam2(String param22) {
        this.param2 = param22;
        this.param[1] = param22;
    }

    public String getParam2() {
        return this.param2;
    }

    public void setParam3(String param32) {
        this.param3 = param32;
        this.param[2] = param32;
    }

    public String getParam3() {
        return this.param3;
    }

    public void setParam4(String param42) {
        this.param4 = param42;
        this.param[3] = param42;
    }

    public String getParam4() {
        return this.param4;
    }

    public void setParam5(String param52) {
        this.param5 = param52;
        this.param[4] = param52;
    }

    public String getParam5() {
        return this.param5;
    }

    public int doStartTag() throws JspException {
        return 0;
    }

    public int doEndTag() throws JspException {
        if (Integer.valueOf(this.paramCount).intValue() > 5) {
            throw new JspException("parameter too much,5 is most");
        }
        String path = this.pageContext.getServletContext().getRealPath("/WEB-INF/classes/");
        boolean newPool = false;
        PoolBean pool = (PoolBean) this.pageContext.getAttribute("pool", this.scope_type);
        if (pool == null) {
            pool = new PoolBean();
            newPool = true;
        }
        ArrayList rows = new ArrayList();
        try {
            if (!pool.isStarted()) {
                pool.setPath(path);
                pool.initializePool();
            }
            Connection con = pool.getConnection();
            if (newPool) {
                this.pageContext.setAttribute("pool", pool, this.scope_type);
            }
            try {
                String sql = new StringBuffer().append("{call ").append(this.procName).toString();
                int pCount = Integer.valueOf(this.paramCount).intValue();
                if (pCount > 0) {
                    String sql2 = new StringBuffer().append(sql).append("(").toString();
                    for (int i = 1; i < pCount; i++) {
                        sql2 = new StringBuffer().append(sql2).append("?,").toString();
                    }
                    sql = new StringBuffer().append(sql2).append("?)").toString();
                }
                CallableStatement stmt = con.prepareCall(new StringBuffer().append(sql).append("}").toString());
                for (int j = 1; j <= pCount; j++) {
                    stmt.setString(j, this.param[j - 1]);
                }
                ResultSet rs = stmt.executeQuery();
                ResultSetMetaData rsmd = rs.getMetaData();
                int colCount = rsmd.getColumnCount();
                int index = 0;
                while (rs.next()) {
                    HashMap row = new HashMap();
                    row.put("INDEX", String.valueOf(index));
                    for (int i2 = 1; i2 <= colCount; i2++) {
                        row.put(rsmd.getColumnName(i2), rs.getObject(i2));
                    }
                    index++;
                    rows.add(row);
                }
                rs.close();
                stmt.close();
                pool.releaseConnection(con);
                this.pageContext.getRequest().setAttribute("rows", rows);
                return 6;
            } catch (SQLException e) {
                throw new JspException(new StringBuffer().append("database perform error : ").append(e.getMessage()).toString());
            }
        } catch (Exception e2) {
            throw new JspException("initializePool error!");
        }
    }
}
