package com.kenfor.taglib.db;

import com.kenfor.database.PoolBean;
import com.kenfor.exutil.InitAction;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.jsp.JspException;

public class dbListArrayPTag extends dbListArrayTag {
    private Connection con = null;
    protected int index = 0;
    protected int level = 0;
    protected String parentIdName = "PARENT_ID";
    private PoolBean pool = null;
    protected ArrayList rows = null;
    protected String temp_sqlWhere = null;

    public void setParentIdName(String parentIdName2) {
        this.parentIdName = parentIdName2;
    }

    public String getParentIdName() {
        return this.parentIdName;
    }

    public int doEndTag() throws JspException {
        Object max_obj;
        String path = this.pageContext.getServletContext().getRealPath("/WEB-INF/classes/");
        String pageObj = this.pageContext.getRequest().getParameter("page");
        Object maxPageObj = this.pageContext.getAttribute("maxPage");
        Object maxRecordObj = this.pageContext.getAttribute("maxRecord");
        if (pageObj != null) {
            this.pageIndex = Integer.valueOf(String.valueOf(pageObj)).intValue();
        }
        if (maxPageObj != null) {
            this.maxPageIndex = Integer.valueOf(String.valueOf(maxPageObj)).intValue();
        }
        if (maxRecordObj != null) {
            this.maxRecord = Integer.valueOf(String.valueOf(maxRecordObj)).intValue();
        }
        this.db_sql_where = new StringBuffer().append(this.parentIdName).append("=0").toString();
        this.pool = (PoolBean) this.pageContext.getAttribute("pool", this.scope_type);
        if (this.pool == null) {
            this.pool = new InitAction(this.pageContext).getPool();
            this.log.error("pool is null,create new");
        }
        this.rows = new ArrayList();
        new ArrayList();
        try {
            if (!this.pool.isStarted()) {
                this.pool.setPath(path);
                this.pool.initializePool();
            }
            this.con = this.pool.getConnection();
            if (0 != 0) {
                this.pageContext.setAttribute("pool", this.pool, this.scope_type);
            }
            try {
                String max_sql = createMaxSQL();
                Statement stmt2 = this.con.createStatement();
                ResultSet rs2 = stmt2.executeQuery(max_sql);
                if (rs2.next() && (max_obj = rs2.getObject(1)) != null) {
                    this.maxPageIndex = Integer.valueOf(String.valueOf(max_obj)).intValue();
                }
                rs2.close();
                stmt2.close();
                long p_max = Math.round(Math.ceil((((double) this.maxPageIndex) * 1.0d) / ((double) this.pageMax)));
                if (this.maxPageIndex < this.pageIndex) {
                    this.pageIndex = 0;
                }
                String this_sql = createSQL();
                try {
                    Statement stmt = this.con.createStatement(1004, 1007);
                    ResultSet rs = stmt.executeQuery(this_sql);
                    int columnCount = rs.getMetaData().getColumnCount();
                    this.index = 0;
                    rs.last();
                    int rowCount = rs.getRow();
                    rs.beforeFirst();
                    int[] b_id = new int[rowCount];
                    int[] p_id = new int[rowCount];
                    int thisRow = 0;
                    while (rs.next()) {
                        b_id[thisRow] = rs.getInt("BAS_ID");
                        p_id[thisRow] = rs.getInt(this.parentIdName);
                        thisRow++;
                    }
                    rs.close();
                    stmt.close();
                    showMenuTree(b_id, rowCount);
                    this.pool.releaseConnection(this.con);
                    this.pageContext.getRequest().setAttribute(this.name, this.rows);
                    this.pageContext.setAttribute("maxPage", String.valueOf(p_max));
                    this.pageContext.setAttribute("maxRecord", String.valueOf(this.maxPageIndex));
                    return 6;
                } catch (SQLException e) {
                    throw new JspException(new StringBuffer().append("database perform error : ").append(e.getMessage()).toString());
                }
            } catch (SQLException e2) {
                this.log.error(e2.getMessage());
                this.log.error(new StringBuffer().append("sql:").append((String) null).toString());
                throw new JspException(new StringBuffer().append("database perform error : ").append(e2.getMessage()).toString());
            }
        } catch (Exception e3) {
            this.log.error(e3.getMessage());
            throw new JspException(new StringBuffer().append("initializePool error!").append(e3.getMessage()).toString());
        }
    }

    public void showMenuTree(int[] bas_id, int rowCount) throws SQLException {
        String sql1;
        String sql2;
        String test = "";
        for (int ii = 0; ii < rowCount; ii++) {
            test = new StringBuffer().append(test).append("bas_id=").append(bas_id[ii]).append(dbPresentTag.ROLE_DELIMITER).toString();
        }
        int j = 0;
        while (j < rowCount) {
            try {
                if (this.con == null) {
                    this.con = this.pool.getConnection();
                }
                Statement stmt_sub = this.con.createStatement();
                String t_where = createWhereSQL(false);
                if (t_where != null) {
                    sql1 = new StringBuffer().append("select * from ").append(this.sqlTablename).append(" where BAS_ID=").append(String.valueOf(bas_id[j])).append(" and ").append(t_where).append(" order by BAS_ID").toString();
                } else {
                    sql1 = new StringBuffer().append("select * from ").append(this.sqlTablename).append(" where BAS_ID=").append(String.valueOf(bas_id[j])).append(" order by BAS_ID").toString();
                }
                ResultSet rs_sub = stmt_sub.executeQuery(sql1);
                ResultSetMetaData rsmd = rs_sub.getMetaData();
                int colCount = rsmd.getColumnCount();
                if (rs_sub.next()) {
                    HashMap row = new HashMap();
                    row.put("INDEX", String.valueOf(this.index));
                    row.put("LEVEL", String.valueOf(this.level));
                    for (int i = 1; i <= colCount; i++) {
                        row.put(rsmd.getColumnName(i).toLowerCase().trim(), rs_sub.getObject(i));
                    }
                    this.index = this.index + 1;
                    this.rows.add(row);
                }
                rs_sub.close();
                stmt_sub.close();
                Statement stmt_sub2 = this.con.createStatement(1004, 1007);
                String t_where2 = createWhereSQL(false);
                if (t_where2 != null) {
                    sql2 = new StringBuffer().append("select * from ").append(this.sqlTablename).append(" where ").append(this.parentIdName).append("=").append(String.valueOf(bas_id[j])).append(" and ").append(t_where2).append(" order by BAS_ID").toString();
                } else {
                    sql2 = new StringBuffer().append("select * from ").append(this.sqlTablename).append(" where ").append(this.parentIdName).append("=").append(String.valueOf(bas_id[j])).append(" order by BAS_ID").toString();
                }
                ResultSet rs_sub2 = stmt_sub2.executeQuery(sql2);
                rs_sub2.last();
                int childCount = rs_sub2.getRow();
                rs_sub2.beforeFirst();
                int childRowCount = 0;
                int[] bas_id2 = new int[childCount];
                while (rs_sub2.next()) {
                    bas_id2[childRowCount] = rs_sub2.getInt("BAS_ID");
                    childRowCount++;
                }
                rs_sub2.close();
                stmt_sub2.close();
                if (childCount > 0) {
                    this.level = this.level + 1;
                    showMenuTree(bas_id2, childCount);
                    this.level = this.level - 1;
                }
                this.pool.releaseConnection(this.con);
                j++;
            } catch (SQLException e) {
                throw new SQLException(new StringBuffer().append("show menu tree error:\n").append(e.getMessage()).toString());
            } catch (Exception e1) {
                throw new SQLException(e1.getMessage());
            }
        }
    }
}
