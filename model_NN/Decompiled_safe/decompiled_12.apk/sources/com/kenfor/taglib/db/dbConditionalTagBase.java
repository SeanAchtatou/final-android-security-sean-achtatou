package com.kenfor.taglib.db;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import org.apache.struts.util.MessageResources;

public abstract class dbConditionalTagBase extends TagSupport {
    protected static MessageResources messages = MessageResources.getMessageResources("org.apache.struts.taglib.logic.LocalStrings");
    protected String cookie = null;
    protected String header = null;
    protected String name = null;
    protected String parameter = null;
    protected String property = null;
    protected String role = null;
    protected String scope = null;
    protected String user = null;

    /* access modifiers changed from: protected */
    public abstract boolean condition() throws JspException;

    public String getCookie() {
        return this.cookie;
    }

    public void setCookie(String cookie2) {
        this.cookie = cookie2;
    }

    public String getHeader() {
        return this.header;
    }

    public void setHeader(String header2) {
        this.header = header2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getParameter() {
        return this.parameter;
    }

    public void setParameter(String parameter2) {
        this.parameter = parameter2;
    }

    public String getProperty() {
        return this.property;
    }

    public void setProperty(String property2) {
        this.property = property2;
    }

    public String getRole() {
        return this.role;
    }

    public void setRole(String role2) {
        this.role = role2;
    }

    public String getScope() {
        return this.scope;
    }

    public void setScope(String scope2) {
        this.scope = scope2;
    }

    public String getUser() {
        return this.user;
    }

    public void setUser(String user2) {
        this.user = user2;
    }

    public int doStartTag() throws JspException {
        if (condition()) {
            return 1;
        }
        return 0;
    }

    public int doEndTag() throws JspException {
        return 6;
    }

    public void release() {
        dbConditionalTagBase.super.release();
        this.cookie = null;
        this.header = null;
        this.name = null;
        this.parameter = null;
        this.property = null;
        this.role = null;
        this.scope = null;
        this.user = null;
    }
}
