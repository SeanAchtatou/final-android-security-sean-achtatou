package com.kenfor.taglib.db;

import javax.servlet.jsp.tagext.TagSupport;

public class dbReleaseTag extends TagSupport {
    private String name = null;

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }
}
