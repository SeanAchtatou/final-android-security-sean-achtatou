package com.kenfor.taglib.db;

import com.kenfor.database.PoolBean;
import com.kenfor.database.dbSpecifyValueBean;
import com.kenfor.exutil.InitAction;
import com.kenfor.util.MyUtil;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import org.apache.struts.util.RequestUtils;
import org.apache.struts.util.ResponseUtils;

public class dbSpecifyValueTag extends dbTag {
    protected String isCount = "false";
    protected String isSum = "false";

    public void setIsCount(String isCount2) {
        this.isCount = isCount2;
    }

    public String getIsCount() {
        return this.isCount;
    }

    public void setIsSum(String isSum2) {
        this.isSum = isSum2;
    }

    public String getIsSum() {
        return this.isSum;
    }

    /* access modifiers changed from: protected */
    public String createMaxSQL() {
        String sql_where = createWhereSQL();
        String temp_sql = new StringBuffer().append("select count(*) from ").append(this.sqlTablename).toString();
        if (sql_where != null) {
            return new StringBuffer().append(temp_sql).append(" where ").append(sql_where).toString();
        }
        return temp_sql;
    }

    /* access modifiers changed from: protected */
    public String createWhereSQL() {
        int t_index;
        String result;
        boolean is_int_value;
        HttpSession session;
        String temp_sql = null;
        int t_index2 = -1;
        if (this.bodySqlWhere != null && this.bodySqlWhere.length() > 0) {
            temp_sql = this.bodySqlWhere;
        } else if (this.sqlWhere != null && this.sqlWhere.length() > 0) {
            temp_sql = this.sqlWhere;
        }
        String result2 = temp_sql;
        ArrayList paramPre = new ArrayList();
        ArrayList paramNex = new ArrayList();
        ArrayList paramTotal = new ArrayList();
        String t_sql = temp_sql;
        boolean is_int_value2 = false;
        StringBuffer resultBuf = new StringBuffer();
        if (t_sql != null) {
            t_index2 = t_sql.indexOf("::");
        }
        if (t_index > 0) {
            while (t_index > 0) {
                String sub_str1 = null;
                t_index = t_sql.indexOf("::");
                if (t_index > 0) {
                    sub_str1 = t_sql.substring(0, t_index);
                }
                int r_index = -1;
                if (sub_str1 != null) {
                    r_index = sub_str1.indexOf("and");
                }
                if (r_index > 0) {
                    paramTotal.add(sub_str1.substring(0, r_index));
                    sub_str1 = sub_str1.substring(r_index + 3);
                }
                if (sub_str1 != null) {
                    paramPre.add(sub_str1);
                }
                if (t_index > 0) {
                    String sub_str2 = t_sql.substring(t_index + 2);
                    int tt_index = sub_str2.indexOf("and");
                    if (tt_index > 0) {
                        paramNex.add(sub_str2.substring(0, tt_index));
                        t_sql = sub_str2.substring(tt_index + 3);
                        t_index = 1;
                    } else {
                        paramNex.add(sub_str2);
                        t_index = -1;
                    }
                } else if (t_sql != null) {
                    paramTotal.add(t_sql);
                }
            }
            int pp_size = paramPre.size();
            for (int i = 0; i < pp_size; i++) {
                String str1 = ((String) paramPre.get(i)).trim();
                String str2 = ((String) paramNex.get(i)).trim();
                String value = null;
                String b_str = null;
                String e_str = null;
                if (str2 != null) {
                    if (str2.length() > 1) {
                        b_str = str2.substring(0, 1);
                        e_str = str2.substring(str2.length() - 1, str2.length());
                    }
                    if ("%".equals(b_str) && str2.length() >= 1) {
                        str2 = str2.substring(1, str2.length());
                    }
                    if ("%".equals(e_str) && str2.length() >= 1) {
                        str2 = str2.substring(0, str2.length() - 1);
                    }
                    str2 = str2.trim();
                    if (str2.startsWith(":")) {
                        is_int_value = true;
                        str2 = str2.substring(1);
                    } else {
                        is_int_value = false;
                    }
                    value = this.pageContext.getRequest().getParameter(str2);
                    if (value == null || value.equalsIgnoreCase("null")) {
                        value = (String) this.pageContext.getAttribute(str2);
                    }
                    if ((value == null || value.equalsIgnoreCase("null")) && (session = this.pageContext.getSession()) != null) {
                        value = (String) session.getAttribute(str2);
                    }
                    if (this.log.isDebugEnabled()) {
                        this.log.debug("start just propName");
                    }
                    if ((value == null || value.equalsIgnoreCase("null") || value.trim().length() <= 0) && this.propName != null && this.propName.length() > 1) {
                        try {
                            value = (String) RequestUtils.lookup(this.pageContext, this.propName, str2, (String) null);
                        } catch (Exception e) {
                            value = null;
                            this.log.debug(e.getMessage());
                        }
                    }
                }
                if (value == null || value.equalsIgnoreCase("null") || value.trim().length() <= 0) {
                    if (this.log.isDebugEnabled()) {
                        this.log.debug(new StringBuffer().append("value is null,").append(value).toString());
                    }
                    if (!this.isIgnore.equalsIgnoreCase("true")) {
                        paramTotal.add(new StringBuffer().append(str1).append(" 0").toString());
                    }
                } else {
                    if (this.log.isDebugEnabled()) {
                        this.log.debug(new StringBuffer().append("value is ok,").append(value).toString());
                    }
                    this.pageContext.setAttribute(str2.trim(), value.trim());
                    String t_result = null;
                    String value2 = value.trim();
                    if ("%".equals(b_str)) {
                        value2 = new StringBuffer().append("%").append(value2).toString();
                    }
                    if ("%".equals(e_str)) {
                        value2 = new StringBuffer().append(value2).append("%").toString();
                    }
                    int e_len = str1.indexOf("==");
                    if (e_len > 0) {
                        if (MyUtil.isNumber(value2)) {
                            t_result = new StringBuffer().append(" ").append(str1.substring(0, e_len + 1)).append(value2).toString();
                        } else {
                            this.is_value_ok = false;
                            HttpServletRequest t_re = this.pageContext.getRequest();
                            String strServerName = t_re.getServerName();
                            this.log.warn(new StringBuffer().append(strServerName).append(" , ").append(t_re.getQueryString()).append(" , ").append(t_re.getRequestURI()).toString());
                        }
                    } else if (is_int_value) {
                        t_result = new StringBuffer().append(" ").append(str1).append(value2).toString();
                    } else {
                        t_result = new StringBuffer().append(" ").append(str1).append(" '").append(value2).append("' ").toString();
                    }
                    paramTotal.add(t_result);
                }
                is_int_value2 = false;
            }
            int pt_size = paramTotal.size();
            for (int j = 0; j < pt_size; j++) {
                resultBuf.append((String) paramTotal.get(j));
                if (j < paramTotal.size() - 1) {
                    resultBuf.append(" and ");
                }
            }
        } else if (result2 != null) {
            resultBuf.append(result2);
        }
        if (resultBuf == null) {
            result = null;
        } else {
            result = resultBuf.toString();
        }
        if (paramPre != null) {
            paramPre.clear();
        }
        if (paramNex != null) {
            paramNex.clear();
        }
        if (paramTotal != null) {
            paramTotal.clear();
        }
        String result3 = getFilterString(result);
        if ("errorwhere".equals(result3)) {
            this.is_value_ok = false;
            return null;
        } else if (isValidWhere(result3)) {
            return result3;
        } else {
            this.is_value_ok = false;
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public String createSql() {
        String sql_where = createWhereSQL();
        if (this.sql != null) {
            return this.sql;
        }
        if (this.sqlTablename == null) {
            return null;
        }
        if (this.isCount == null || this.isCount.compareToIgnoreCase("true") != 0) {
            if (this.sqlFields == null) {
                return null;
            }
            if (this.isSum == null || this.isSum.compareToIgnoreCase("true") != 0) {
                if (sql_where == null || sql_where.length() <= 0) {
                    return new StringBuffer().append("select top 1 ").append(this.sqlFields).append(" from ").append(this.sqlTablename).toString();
                }
                return new StringBuffer().append("select top 1 ").append(this.sqlFields).append(" from ").append(this.sqlTablename).append(" where ").append(sql_where).toString();
            } else if (sql_where == null || sql_where.length() <= 0) {
                return new StringBuffer().append("select sum(").append(this.sqlFields).append(") as SUM from ").append(this.sqlTablename).toString();
            } else {
                return new StringBuffer().append("select sum(").append(this.sqlFields).append(") as SUM from ").append(this.sqlTablename).append(" where ").append(sql_where).toString();
            }
        } else if (sql_where == null || sql_where.length() <= 0) {
            return new StringBuffer().append("select count(*) as COUNT from ").append(this.sqlTablename).toString();
        } else {
            return new StringBuffer().append("select count(*) as COUNT from ").append(this.sqlTablename).append(" where ").append(sql_where).toString();
        }
    }

    public int doStartTag() throws JspException {
        return 2;
    }

    public int doAfterBody() throws JspException {
        if (this.bodyContent != null) {
            this.bodySqlWhere = this.bodyContent.getString();
            if (this.bodySqlWhere != null) {
                this.bodySqlWhere = this.bodySqlWhere.trim();
            }
            if (this.bodySqlWhere.length() < 1) {
                this.bodySqlWhere = null;
            }
        }
        if (this.log.isDebugEnabled()) {
            this.log.debug(new StringBuffer().append("bodySqlWhere init:").append(this.bodySqlWhere).toString());
        }
        this.bodySqlWhere = getFilterString(this.bodySqlWhere);
        if ("errorwhere".equals(this.bodySqlWhere)) {
            this.is_value_ok = false;
        }
        return 0;
    }

    public int doEndTag() throws JspException {
        if (!this.is_value_ok) {
            return 6;
        }
        String temp_sql = createSql();
        if (!this.is_value_ok) {
            return 6;
        }
        if (this.log.isDebugEnabled()) {
            this.log.debug(new StringBuffer().append("temp_sql:").append(temp_sql).toString());
        }
        PoolBean pool = (PoolBean) this.pageContext.getAttribute("pool", this.scope_type);
        if (pool == null) {
            pool = (PoolBean) this.pageContext.getServletContext().getAttribute("pool");
        }
        String realPath = this.pageContext.getServletContext().getRealPath("/WEB-INF/classes/");
        if (pool == null) {
            pool = new InitAction(this.pageContext).getPool();
            this.log.error("pool is null,create new");
        }
        if (temp_sql == null) {
            throw new JspException("gived sql statement error");
        }
        dbSpecifyValueBean dbsv = new dbSpecifyValueBean();
        dbsv.setSql(temp_sql);
        try {
            HashMap result = dbsv.getdbValue(pool);
            if (result == null) {
                result = new HashMap();
            }
            try {
                if ("true".equalsIgnoreCase(this.isSave)) {
                    if (this.name == null || !"true".equalsIgnoreCase(this.isSave)) {
                        result.clear();
                    } else {
                        this.pageContext.setAttribute(this.name, result);
                        HttpServletRequest request = this.pageContext.getRequest();
                    }
                } else if (result != null) {
                    if (this.isSum.compareToIgnoreCase("true") == 0) {
                        ResponseUtils.write(this.pageContext, String.valueOf(result.get("sum")));
                    } else if (this.isCount.compareToIgnoreCase("true") == 0) {
                        ResponseUtils.write(this.pageContext, String.valueOf(result.get("count")));
                    } else if (!(this.sqlFields == null || this.sqlFields.indexOf(dbPresentTag.ROLE_DELIMITER) != -1 || this.sqlFields.compareTo("*") == 0)) {
                        ResponseUtils.write(this.pageContext, String.valueOf(result.get(this.sqlFields)));
                    }
                    result.clear();
                }
                return 6;
            } catch (Exception e) {
                this.log.error(e.getMessage());
                HttpServletRequest request2 = this.pageContext.getRequest();
                String strServerName = request2.getServerName();
                String strRequestURI = request2.getRequestURI();
                String strQueryString = request2.getQueryString();
                String context = request2.getContextPath();
                String header = request2.getHeader("referer");
                this.log.error(new StringBuffer().append(request2.getRemoteAddr()).append(" , ").append(strServerName).append(" , ").append(strRequestURI).append(" , ").append(strQueryString).append(" , ").append(context).toString());
                return 6;
            }
        } catch (Exception e2) {
            HttpServletRequest request3 = this.pageContext.getRequest();
            String strServerName2 = request3.getServerName();
            String strRequestURI2 = request3.getRequestURI();
            String strQueryString2 = request3.getQueryString();
            String contextPath = request3.getContextPath();
            String header2 = request3.getHeader("referer");
            this.log.error(new StringBuffer().append(request3.getRemoteAddr()).append(" , ").append(strServerName2).append(" , ").append(strRequestURI2).append(" , ").append(strQueryString2).append(" , ").append(e2.getMessage()).toString());
            return 6;
        }
    }

    public void release() {
        super.release();
        this.sqlTablename = null;
        this.sqlFields = null;
        this.sqlWhere = null;
        this.isCount = "false";
        this.isSum = "false";
        this.sql = null;
        this.bodySqlWhere = null;
        this.isSave = "false";
        this.name = null;
    }
}
