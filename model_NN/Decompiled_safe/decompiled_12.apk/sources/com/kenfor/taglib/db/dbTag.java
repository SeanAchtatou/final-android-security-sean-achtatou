package com.kenfor.taglib.db;

import javax.servlet.jsp.tagext.BodyTagSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class dbTag extends BodyTagSupport {
    protected String bodySqlWhere = null;
    protected String db_sql_where = null;
    protected String isDebug = "false";
    protected String isIgnore = "true";
    protected String isSave = "false";
    protected String isSubSQL = "false";
    protected boolean is_value_ok = true;
    protected Log log = LogFactory.getLog(getClass().getName());
    protected String name = "rows";
    protected String new_flag = "false";
    protected String propName = null;
    protected String rec_field_name = null;
    protected String scopeType = "4";
    protected int scope_type = 4;
    protected String sql = null;
    protected String sqlFields = "*";
    protected String sqlOrderby = null;
    protected String sqlTablename = null;
    protected String sqlWhere = null;

    public void release() {
        this.sqlTablename = null;
        this.sqlFields = "*";
        this.sqlWhere = null;
        this.sqlOrderby = null;
        this.sql = null;
        this.bodySqlWhere = null;
        this.isSave = "false";
        this.isDebug = "false";
        this.name = "rows";
        this.scopeType = "4";
        this.scope_type = 4;
        this.isIgnore = "true";
        this.isSubSQL = "false";
        this.rec_field_name = null;
        this.propName = null;
        dbTag.super.release();
    }

    public void setPropName(String propName2) {
        this.propName = propName2;
    }

    public String getPropName() {
        return this.propName;
    }

    public void setRec_field_name(String rec_field_name2) {
        this.rec_field_name = rec_field_name2;
    }

    public String getRec_field_name() {
        return this.rec_field_name;
    }

    public void setIsSubSQL(String isSubSQL2) {
        this.isSubSQL = isSubSQL2;
    }

    public String getIsSubSQL() {
        return this.isSubSQL;
    }

    public void setIsIgnore(String isIgnore2) {
        this.isIgnore = isIgnore2;
    }

    public String getIsIgnore() {
        return this.isIgnore;
    }

    public void setIsDebug(String isDebug2) {
        this.isDebug = isDebug2;
    }

    public String getIsDebug() {
        return this.isDebug;
    }

    /* access modifiers changed from: protected */
    public int getIntValue(String str_int_value, int default_value) {
        try {
            return Integer.valueOf(str_int_value).intValue();
        } catch (Exception e) {
            return default_value;
        }
    }

    /* access modifiers changed from: protected */
    public boolean isValidNumber(String value) {
        if (value == null || value.length() < 1) {
            return false;
        }
        char[] tt = value.toCharArray();
        for (char t : tt) {
            if (t < '0') {
            }
            if (t > '9') {
            }
            if ((t < '0' || t > '9') && t != '.') {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean isValidWhere(String value) {
        if (value == null || value.trim().length() < 1) {
            return true;
        }
        boolean result = true;
        String[] tt = value.split("and");
        int i = 0;
        while (true) {
            if (i >= tt.length) {
                break;
            }
            String t_str = tt[i];
            if (t_str != null && t_str.trim().length() > 0 && t_str.indexOf("=") > 0) {
                String[] t2 = t_str.split("=");
                if (t2.length < 2) {
                    result = false;
                    break;
                }
                int j = 0;
                while (true) {
                    if (j >= t2.length) {
                        break;
                    }
                    String t_t = t2[j];
                    if (t_t == null || t_t.trim().length() < 1) {
                        result = false;
                    } else {
                        j++;
                    }
                }
                result = false;
            }
            i++;
        }
        if (result || !this.log.isDebugEnabled()) {
            return result;
        }
        this.log.debug(new StringBuffer().append("where is invalid,where:").append(value).toString());
        return result;
    }

    /* access modifiers changed from: protected */
    public String filterQuotation(String value) {
        if (value == null || value.trim().length() <= 0) {
            return value;
        }
        return value.replaceAll("'", "''");
    }

    /* access modifiers changed from: protected */
    public String getFilterString(String value, boolean q_ok) {
        if (value != null) {
            value = value.trim();
            if (value.indexOf(" select ") >= 0 || value.indexOf("(select ") >= 0 || value.indexOf(" declare ") >= 0 || value.indexOf("(declare ") >= 0 || value.indexOf("(update ") >= 0 || value.indexOf(" update ") >= 0 || value.indexOf(" exists ") >= 0 || value.indexOf("(exists ") >= 0 || value.indexOf(" from ") >= 0 || value.indexOf("(from ") >= 0 || value.indexOf(" delete ") >= 0 || value.indexOf("(delete ") >= 0 || (q_ok && (value.indexOf("[") >= 0 || value.indexOf("]") >= 0))) {
                this.log.warn(new StringBuffer().append("value is errro:").append(value).toString());
                value = "errorwhere";
            }
            if (value != null && (value.indexOf(" user ") >= 0 || value.indexOf("(user ") >= 0 || value.indexOf("user)") >= 0 || value.indexOf("user+") >= 0)) {
                this.log.warn(new StringBuffer().append("can not use user for select,value:").append(value).toString());
                value = "errorwhere";
            }
        }
        filterQuotation(value);
        return value;
    }

    /* access modifiers changed from: protected */
    public String getFilterString(String value) {
        return getFilterString(value, true);
    }

    public void setScopeType(String scopeType2) {
        this.scopeType = scopeType2;
        if (scopeType2 != null) {
            this.scope_type = Integer.valueOf(scopeType2).intValue();
            if (this.scope_type > 4) {
                this.scope_type = 4;
            }
            if (this.scope_type < 1) {
                this.scope_type = 1;
            }
        }
    }

    public String getScopeType() {
        return this.scopeType;
    }

    public void setSqlOrderby(String sqlOrderby2) {
        this.sqlOrderby = sqlOrderby2;
    }

    public String getSqlOrderby() {
        return this.sqlOrderby;
    }

    public void setSqlTablename(String sqlTablename2) {
        this.sqlTablename = sqlTablename2;
    }

    public String getSqlTablename() {
        return this.sqlTablename;
    }

    public void setSqlFields(String sqlFields2) {
        this.sqlFields = sqlFields2;
    }

    public String getSqlFields() {
        return this.sqlFields;
    }

    public void setSqlWhere(String sqlWhere2) {
        this.sqlWhere = sqlWhere2;
    }

    public String getSqlWhere() {
        return this.sqlWhere;
    }

    public void setSql(String sql2) {
        this.sql = sql2;
    }

    public String getSql() {
        return this.sql;
    }

    public void setIsSave(String isSave2) {
        this.isSave = isSave2;
    }

    public String getIsSave() {
        return this.isSave;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getName() {
        return this.name;
    }

    public String getNew_flag() {
        return this.new_flag;
    }

    public void setNew_flag(String new_flag2) {
        this.new_flag = new_flag2;
    }
}
