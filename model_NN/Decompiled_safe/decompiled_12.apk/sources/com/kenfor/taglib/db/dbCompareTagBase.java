package com.kenfor.taglib.db;

import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.WeakHashMap;
import javax.servlet.http.Cookie;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.PageContext;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.struts.util.MessageResources;
import org.apache.struts.util.RequestUtils;
import org.jivesoftware.smackx.workgroup.packet.SessionID;

public abstract class dbCompareTagBase extends dbConditionalTagBase {
    protected static final int DOUBLE_COMPARE = 0;
    protected static final int LONG_COMPARE = 1;
    protected static final int STRING_COMPARE = 2;
    protected static MessageResources messages = MessageResources.getMessageResources("org.apache.struts.taglib.logic.LocalStrings");
    public String value = null;

    /* access modifiers changed from: protected */
    public abstract boolean condition() throws JspException;

    public String getValue() {
        return this.value;
    }

    public void setValue(String value2) {
        this.value = value2;
    }

    public void release() {
        super.release();
        this.value = null;
    }

    /* access modifiers changed from: protected */
    public boolean condition(int desired1, int desired2) throws JspException {
        int type = -1;
        double doubleValue = 0.0d;
        long longValue = 0;
        if (-1 < 0 && this.value.length() > 0) {
            try {
                doubleValue = Double.parseDouble(this.value);
                type = 0;
            } catch (NumberFormatException e) {
            }
        }
        if (type < 0 && this.value.length() > 0) {
            try {
                longValue = Long.parseLong(this.value);
                type = 1;
            } catch (NumberFormatException e2) {
            }
        }
        if (type < 0) {
            type = 2;
        }
        Object variable = null;
        if (this.cookie != null) {
            Cookie[] cookies = this.pageContext.getRequest().getCookies();
            if (cookies == null) {
                cookies = new Cookie[0];
            }
            int i = 0;
            while (true) {
                if (i >= cookies.length) {
                    break;
                } else if (this.cookie.equals(cookies[i].getName())) {
                    variable = cookies[i].getValue();
                    break;
                } else {
                    i++;
                }
            }
        } else if (this.header != null) {
            variable = this.pageContext.getRequest().getHeader(this.header);
        } else if (this.name != null) {
            Object bean = lookup(this.pageContext, this.name, null);
            if (this.property == null) {
                variable = bean;
            } else if (bean == null) {
                return false;
            } else {
                if (bean instanceof ResultSet) {
                    try {
                        variable = ((ResultSet) bean).getObject(this.property);
                    } catch (Exception e3) {
                        throw new JspException("exception at dbCompareTagBase");
                    }
                } else if (bean instanceof HashMap) {
                    variable = ((HashMap) bean).get(this.property);
                } else if (bean instanceof WeakHashMap) {
                    variable = ((WeakHashMap) bean).get(this.property);
                } else if (bean instanceof String) {
                    variable = String.valueOf(bean);
                } else {
                    try {
                        variable = PropertyUtils.getProperty(bean, this.property);
                    } catch (InvocationTargetException e4) {
                        throw new JspException(new StringBuffer().append("error at dbCompareTagBase InvocationTargetException :").append(e4.getMessage()).toString());
                    } catch (Throwable t) {
                        throw new JspException(new StringBuffer().append("error at dbCompareTagBase Throwable :").append(t.getMessage()).toString());
                    }
                }
            }
        } else if (this.parameter != null) {
            variable = this.pageContext.getRequest().getParameter(this.parameter);
        } else {
            JspException e5 = new JspException(messages.getMessage("logic.selector"));
            RequestUtils.saveException(this.pageContext, e5);
            throw e5;
        }
        if (variable == null) {
            variable = "";
        }
        int result = 0;
        if (type == 0) {
            try {
                double doubleVariable = Double.parseDouble(variable.toString());
                if (doubleVariable < doubleValue) {
                    result = -1;
                } else if (doubleVariable > doubleValue) {
                    result = 1;
                }
            } catch (NumberFormatException e6) {
                result = variable.toString().compareTo(this.value);
            }
        } else if (type == 1) {
            try {
                long longVariable = Long.parseLong(variable.toString());
                if (longVariable < longValue) {
                    result = -1;
                } else if (longVariable > longValue) {
                    result = 1;
                }
            } catch (NumberFormatException e7) {
                result = variable.toString().compareTo(this.value);
            }
        } else {
            result = variable.toString().compareTo(this.value);
        }
        if (result < 0) {
            result = -1;
        } else if (result > 0) {
            result = 1;
        }
        return result == desired1 || result == desired2;
    }

    /* access modifiers changed from: protected */
    public Object lookup(PageContext pageContext, String name, String scope) throws JspTagException {
        if (scope == null) {
            Object bean = pageContext.findAttribute(name);
            if (bean == null) {
                bean = pageContext.getAttribute(name, 2);
            }
            if (bean == null) {
                bean = pageContext.getAttribute(name, 3);
            }
            if (bean == null) {
                return pageContext.getAttribute(name, 4);
            }
            return bean;
        } else if (scope.equalsIgnoreCase("page")) {
            return pageContext.getAttribute(name, 1);
        } else {
            if (scope.equalsIgnoreCase("request")) {
                return pageContext.getAttribute(name, 2);
            }
            if (scope.equalsIgnoreCase(SessionID.ELEMENT_NAME)) {
                return pageContext.getAttribute(name, 3);
            }
            if (scope.equalsIgnoreCase("application")) {
                return pageContext.getAttribute(name, 4);
            }
            throw new JspTagException(new StringBuffer().append("Invalid scope ").append(scope).toString());
        }
    }
}
