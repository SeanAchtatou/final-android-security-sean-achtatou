package com.kenfor.taglib.db;

import java.security.Principal;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.WeakHashMap;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.PageContext;
import org.apache.struts.util.RequestUtils;
import org.jivesoftware.smackx.workgroup.packet.SessionID;

public class dbPresentTag extends dbConditionalTagBase {
    public static final String ROLE_DELIMITER = ",";

    /* access modifiers changed from: protected */
    public boolean condition() throws JspException {
        return condition(true);
    }

    /* access modifiers changed from: protected */
    public boolean condition(boolean desired) throws JspException {
        boolean present = false;
        if (this.cookie != null) {
            Cookie[] cookies = this.pageContext.getRequest().getCookies();
            if (cookies == null) {
                cookies = new Cookie[0];
            }
            int i = 0;
            while (true) {
                if (i >= cookies.length) {
                    break;
                } else if (this.cookie.equals(cookies[i].getName())) {
                    present = true;
                    break;
                } else {
                    i++;
                }
            }
        } else if (this.header != null) {
            present = this.pageContext.getRequest().getHeader(this.header) != null;
        } else if (this.name != null) {
            Object value = null;
            try {
                if (this.property != null) {
                    Object bean = lookup(this.pageContext, this.name, null);
                    if (0 != 0) {
                        if (bean instanceof HashMap) {
                            value = (String) ((HashMap) bean).get(this.property);
                        }
                        if (bean instanceof WeakHashMap) {
                            value = (String) ((WeakHashMap) bean).get(this.property);
                        }
                        if (bean instanceof ResultSet) {
                            try {
                                value = ((ResultSet) bean).getString(this.property);
                            } catch (Exception e) {
                                value = null;
                            }
                        }
                    }
                } else {
                    value = lookup(this.pageContext, this.name, null);
                }
            } catch (JspException e2) {
                value = null;
            }
            present = value != null;
        } else if (this.parameter != null) {
            present = this.pageContext.getRequest().getParameter(this.parameter) != null;
        } else if (this.role != null) {
            HttpServletRequest request = this.pageContext.getRequest();
            StringTokenizer st = new StringTokenizer(this.role, ROLE_DELIMITER, false);
            while (!present && st.hasMoreTokens()) {
                present = request.isUserInRole(st.nextToken());
            }
        } else if (this.user != null) {
            Principal principal = this.pageContext.getRequest().getUserPrincipal();
            present = principal != null && this.user.equals(principal.getName());
        } else {
            JspException e3 = new JspException(dbConditionalTagBase.messages.getMessage("logic.selector"));
            RequestUtils.saveException(this.pageContext, e3);
            throw e3;
        }
        if (present == desired) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public Object lookup(PageContext pageContext, String name, String scope) throws JspTagException {
        if (scope == null) {
            Object bean = pageContext.findAttribute(name);
            if (bean == null) {
                bean = pageContext.getAttribute(name, 2);
            }
            if (bean == null) {
                bean = pageContext.getAttribute(name, 3);
            }
            if (bean == null) {
                return pageContext.getAttribute(name, 4);
            }
            return bean;
        } else if (scope.equalsIgnoreCase("page")) {
            return pageContext.getAttribute(name, 1);
        } else {
            if (scope.equalsIgnoreCase("request")) {
                return pageContext.getAttribute(name, 2);
            }
            if (scope.equalsIgnoreCase(SessionID.ELEMENT_NAME)) {
                return pageContext.getAttribute(name, 3);
            }
            if (scope.equalsIgnoreCase("application")) {
                return pageContext.getAttribute(name, 4);
            }
            throw new JspTagException(new StringBuffer().append("Invalid scope ").append(scope).toString());
        }
    }
}
