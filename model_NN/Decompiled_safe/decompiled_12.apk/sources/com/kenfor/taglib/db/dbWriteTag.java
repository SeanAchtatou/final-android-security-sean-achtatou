package com.kenfor.taglib.db;

import com.kenfor.exutil.htmlFilter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.WeakHashMap;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.TagSupport;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.struts.util.MessageResources;
import org.apache.struts.util.RequestUtils;
import org.apache.struts.util.ResponseUtils;
import org.jivesoftware.smackx.workgroup.packet.SessionID;

public class dbWriteTag extends TagSupport {
    public static final String DATE_FORMAT_KEY = "org.apache.struts.taglib.bean.format.date";
    public static final String FLOAT_FORMAT_KEY = "org.apache.struts.taglib.bean.format.float";
    public static final String INT_FORMAT_KEY = "org.apache.struts.taglib.bean.format.int";
    public static final String SQL_DATE_FORMAT_KEY = "org.apache.struts.taglib.bean.format.sql.date";
    public static final String SQL_TIMESTAMP_FORMAT_KEY = "org.apache.struts.taglib.bean.format.sql.timestamp";
    public static final String SQL_TIME_FORMAT_KEY = "org.apache.struts.taglib.bean.format.sql.time";
    protected static MessageResources messages = MessageResources.getMessageResources("org.apache.struts.taglib.bean.LocalStrings");
    protected String bundle = null;
    protected boolean filter = true;
    protected String formatKey = null;
    protected String formatStr = null;
    private String highlightKey;
    protected boolean ignore = false;
    protected String localeKey = null;
    private String maxLength = null;
    protected String name = null;
    protected String property = null;
    protected String scope = null;

    public boolean getFilter() {
        return this.filter;
    }

    public void setFilter(boolean filter2) {
        this.filter = filter2;
    }

    public boolean getIgnore() {
        return this.ignore;
    }

    public void setIgnore(boolean ignore2) {
        this.ignore = ignore2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getProperty() {
        return this.property;
    }

    public void setProperty(String property2) {
        this.property = property2;
    }

    public String getScope() {
        return this.scope;
    }

    public void setScope(String scope2) {
        this.scope = scope2;
    }

    public String getFormat() {
        return this.formatStr;
    }

    public void setFormat(String formatStr2) {
        this.formatStr = formatStr2;
    }

    public String getFormatKey() {
        return this.formatKey;
    }

    public void setFormatKey(String formatKey2) {
        this.formatKey = formatKey2;
    }

    public String getLocale() {
        return this.localeKey;
    }

    public void setLocale(String localeKey2) {
        this.localeKey = localeKey2;
    }

    public String getBundle() {
        return this.bundle;
    }

    public void setBundle(String bundle2) {
        this.bundle = bundle2;
    }

    public int doStartTag() throws JspException {
        Object bean = lookup(this.pageContext, this.name, null);
        if (bean != null) {
            Object value = null;
            if (this.property != null) {
                if (bean instanceof ResultSet) {
                    if (this.property.indexOf("index") < 0) {
                        value = getValue(bean);
                    } else if ("index".equalsIgnoreCase(this.property)) {
                        value = this.pageContext.getAttribute(new StringBuffer().append(this.name).append("_index").toString());
                    } else if ("index_1".equalsIgnoreCase(this.property)) {
                        value = this.pageContext.getAttribute(new StringBuffer().append(this.name).append("_index_1").toString());
                    } else if ("t_index".equalsIgnoreCase(this.property)) {
                        value = this.pageContext.getAttribute(new StringBuffer().append(this.name).append("_t_index").toString());
                    } else {
                        value = getValue(bean);
                    }
                } else if (bean instanceof HashMap) {
                    value = ((HashMap) bean).get(this.property);
                } else if (bean instanceof WeakHashMap) {
                    value = ((WeakHashMap) bean).get(this.property);
                } else {
                    try {
                        value = PropertyUtils.getProperty(bean, this.property);
                    } catch (Exception t) {
                        throw new JspException(new StringBuffer().append("error at dbWrite :").append(t.getMessage()).toString());
                    }
                }
            }
            if (value != null) {
                String output = formatValue(value);
                String trimString = trimString(output);
                if (this.filter) {
                    output = htmlFilter.filterNewLine(output);
                }
                String temp_out = output;
                if (this.maxLength != null) {
                    try {
                        int max_length = Integer.valueOf(this.maxLength).intValue();
                        if (max_length > 0) {
                            output = htmlFilter.filter(output, max_length);
                        }
                    } catch (Exception e) {
                        output = temp_out;
                    }
                }
                if (!"true".equals((String) this.pageContext.getAttribute("nokey"))) {
                    if (this.highlightKey == null) {
                        output = highlightKey("keyword", output);
                    } else if (!"nokey".equals(this.highlightKey)) {
                        output = highlightKey(this.highlightKey, output);
                    }
                }
                if ("null".equalsIgnoreCase(output)) {
                    output = "";
                }
                ResponseUtils.write(this.pageContext, output);
            }
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public String trimString(String value) {
        if (value != null) {
            return value.trim();
        }
        return value;
    }

    /* access modifiers changed from: protected */
    public String cutString(String output, int max_length) {
        String result;
        double d;
        if (output == null) {
            return null;
        }
        int len = output.length();
        if (len <= max_length) {
            result = output;
        } else {
            char[] str_chars = output.toCharArray();
            double c_len = 0.0d;
            int point = 0;
            int i = 0;
            while (i < str_chars.length && c_len < ((double) max_length)) {
                if (i + 9 < str_chars.length && str_chars[i] == '&' && str_chars[i + 9] == ';') {
                    i += 9;
                    point += 9;
                }
                if (str_chars[i] <= 0 || str_chars[i] >= 245) {
                    d = 1.0d;
                } else {
                    d = 0.5d;
                }
                c_len += d;
                i++;
                point++;
            }
            if (len <= point) {
                result = output;
            } else {
                result = new StringBuffer().append(output.substring(0, point)).append("...").toString();
            }
        }
        return result;
    }

    /* access modifiers changed from: protected */
    public String doWithValue(Object value) throws JspException {
        String output = formatValue(value);
        Object value2 = trimString(output);
        if (this.filter) {
            output = htmlFilter.filterNewLine(output);
        }
        String temp_out = output;
        if (this.maxLength != null) {
            try {
                int max_length = Integer.valueOf(this.maxLength).intValue();
                if (max_length > 0) {
                    if (this.filter) {
                        output = htmlFilter.filter(output, max_length);
                    } else if (output != null) {
                        if (output.length() > max_length) {
                            output = new StringBuffer().append(output.substring(0, max_length)).append("...").toString();
                        }
                    }
                }
            } catch (Exception e) {
                output = temp_out;
            }
        }
        if (!"true".equals((String) this.pageContext.getAttribute("nokey"))) {
            if (this.highlightKey == null) {
                output = highlightKey("keyword", output);
            } else if (!"nokey".equals(this.highlightKey)) {
                output = highlightKey(this.highlightKey, output);
            }
        }
        if ("null".equalsIgnoreCase(output)) {
            return "";
        }
        return output;
    }

    /* access modifiers changed from: protected */
    public Object getValue(Object bean, String prop) throws JspException {
        if (prop == null) {
            return null;
        }
        if (bean instanceof ResultSet) {
            if (prop.indexOf("index") < 0) {
                return getRSValue(bean);
            }
            if ("index".equalsIgnoreCase(prop)) {
                return this.pageContext.getAttribute(new StringBuffer().append(this.name).append("_index").toString());
            }
            if ("index_1".equalsIgnoreCase(prop)) {
                return this.pageContext.getAttribute(new StringBuffer().append(this.name).append("_index_1").toString());
            }
            if ("t_index".equalsIgnoreCase(prop)) {
                return this.pageContext.getAttribute(new StringBuffer().append(this.name).append("_t_index").toString());
            }
            return getRSValue(bean);
        } else if (bean instanceof HashMap) {
            return ((HashMap) bean).get(prop);
        } else {
            if (bean instanceof WeakHashMap) {
                return ((WeakHashMap) bean).get(prop);
            }
            try {
                return PropertyUtils.getProperty(bean, prop);
            } catch (Exception t) {
                throw new JspException(new StringBuffer().append("error at dbWrite :").append(t.getMessage()).toString());
            }
        }
    }

    /* access modifiers changed from: protected */
    public Object getRSValue(Object bean) throws JspException {
        try {
            return ((ResultSet) bean).getObject(this.property);
        } catch (SQLException e) {
            throw new JspException(new StringBuffer().append("error at dbWriteTag SQLException: ").append(e.getMessage()).toString());
        } catch (Exception e2) {
            throw new JspException(new StringBuffer().append("error at dbWriteTag Exception:").append(e2.getMessage()).toString());
        }
    }

    /* access modifiers changed from: protected */
    public Object getValue(Object bean) throws JspException {
        try {
            return ((ResultSet) bean).getObject(this.property);
        } catch (SQLException e) {
            throw new JspException(new StringBuffer().append("error at dbWriteTag SQLException: ").append(e.getMessage()).toString());
        } catch (Exception e2) {
            throw new JspException(new StringBuffer().append("error at dbWriteTag Exception:").append(e2.getMessage()).toString());
        }
    }

    /* access modifiers changed from: protected */
    public String highlightKey(String key, String content) throws JspException {
        String key_value = this.pageContext.getRequest().getParameter(key);
        if (key_value == null) {
            key_value = (String) this.pageContext.getRequest().getAttribute(key);
        }
        if (key_value == null || "null".equalsIgnoreCase(key_value) || key_value.length() < 1) {
            return content;
        }
        String[] key_t = null;
        if (key_value != null) {
            key_t = key_value.split(" ");
        }
        for (String str_key : key_t) {
            if (str_key != null && str_key.trim().length() > 1) {
                String str_key2 = str_key.trim();
                content = content.replaceAll(str_key2, new StringBuffer().append("<font color=\"cc0033\" >").append(str_key2).append("</font>").toString());
            }
        }
        return content;
    }

    /* access modifiers changed from: protected */
    public String retrieveFormatString(String formatKey2) throws JspException {
        String result = RequestUtils.message(this.pageContext, this.bundle, this.localeKey, formatKey2);
        if (result == null || (result.startsWith("???") && result.endsWith("???"))) {
            return null;
        }
        return result;
    }

    /* access modifiers changed from: protected */
    public String formatValue(Object valueToFormat) throws JspException {
        Format format = null;
        Object value = valueToFormat;
        Locale locale = RequestUtils.retrieveUserLocale(this.pageContext, this.localeKey);
        boolean formatStrFromResources = false;
        String formatString = this.formatStr;
        if (value instanceof String) {
            return (String) value;
        }
        if (!(formatString != null || this.formatKey == null || (formatString = retrieveFormatString(this.formatKey)) == null)) {
            formatStrFromResources = true;
        }
        if (value instanceof Number) {
            if (formatString == null) {
                if ((value instanceof Byte) || (value instanceof Short) || (value instanceof Integer) || (value instanceof Long) || (value instanceof BigInteger)) {
                    formatString = retrieveFormatString(INT_FORMAT_KEY);
                } else if ((value instanceof Float) || (value instanceof Double) || (value instanceof BigDecimal)) {
                    formatString = retrieveFormatString(FLOAT_FORMAT_KEY);
                }
                if (formatString != null) {
                    formatStrFromResources = true;
                }
            }
            if (formatString != null) {
                try {
                    format = NumberFormat.getNumberInstance(locale);
                    if (formatStrFromResources) {
                        ((DecimalFormat) format).applyLocalizedPattern(formatString);
                    } else {
                        ((DecimalFormat) format).applyPattern(formatString);
                    }
                } catch (IllegalArgumentException e) {
                    JspException e2 = new JspException(messages.getMessage("write.format", formatString));
                    RequestUtils.saveException(this.pageContext, e2);
                    throw e2;
                }
            }
        } else if (value instanceof Date) {
            if (formatString == null) {
                if (value instanceof Timestamp) {
                    formatString = retrieveFormatString(SQL_TIMESTAMP_FORMAT_KEY);
                } else if (value instanceof java.sql.Date) {
                    formatString = retrieveFormatString(SQL_DATE_FORMAT_KEY);
                } else if (value instanceof Time) {
                    formatString = retrieveFormatString(SQL_TIME_FORMAT_KEY);
                } else if (value instanceof Date) {
                    formatString = retrieveFormatString(DATE_FORMAT_KEY);
                }
                if (formatString != null) {
                    formatStrFromResources = true;
                }
            }
            if (formatString != null) {
                format = formatStrFromResources ? new SimpleDateFormat(formatString, locale) : new SimpleDateFormat(formatString);
            }
        }
        if (format != null) {
            return format.format(value);
        }
        return value.toString();
    }

    public void release() {
        dbWriteTag.super.release();
        this.filter = true;
        this.ignore = false;
        this.name = null;
        this.property = null;
        this.scope = null;
        this.formatStr = null;
        this.formatKey = null;
        this.localeKey = null;
        this.bundle = null;
    }

    /* access modifiers changed from: protected */
    public Object lookup(PageContext pageContext, String name2, String scope2) throws JspTagException {
        if (scope2 == null) {
            Object bean = pageContext.findAttribute(name2);
            if (bean == null) {
                bean = pageContext.getAttribute(name2, 2);
            }
            if (bean == null) {
                bean = pageContext.getAttribute(name2, 3);
            }
            if (bean == null) {
                return pageContext.getAttribute(name2, 4);
            }
            return bean;
        } else if (scope2.equalsIgnoreCase("page")) {
            return pageContext.getAttribute(name2, 1);
        } else {
            if (scope2.equalsIgnoreCase("request")) {
                return pageContext.getAttribute(name2, 2);
            }
            if (scope2.equalsIgnoreCase(SessionID.ELEMENT_NAME)) {
                return pageContext.getAttribute(name2, 3);
            }
            if (scope2.equalsIgnoreCase("application")) {
                return pageContext.getAttribute(name2, 4);
            }
            throw new JspTagException(new StringBuffer().append("Invalid scope ").append(scope2).toString());
        }
    }

    public String getHighlightKey() {
        return this.highlightKey;
    }

    public void setHighlightKey(String highlightKey2) {
        this.highlightKey = highlightKey2;
    }

    public String getMaxLength() {
        return this.maxLength;
    }

    public void setMaxLength(String maxLength2) {
        this.maxLength = maxLength2;
    }
}
