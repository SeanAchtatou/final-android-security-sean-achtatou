package com.kenfor.taglib.page;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;

public class lastPageTag extends pageTag {
    public int doStartTag() throws JspException {
        String queryString;
        String result_url;
        HttpServletRequest request = this.pageContext.getRequest();
        StringBuffer this_url = new StringBuffer();
        if (this.url == null) {
            this_url.append(request.getRequestURI());
        } else {
            this_url.append(this.url);
        }
        String queryString2 = request.getQueryString();
        StringBuffer results = new StringBuffer();
        Object maxp = this.pageContext.getAttribute("maxPage");
        if (maxp == null) {
            maxp = this.pageContext.getRequest().getAttribute("maxPage");
        }
        if (maxp != null) {
            this.maxpage = getIntValue(String.valueOf(maxp), 0) - 1;
        }
        this.cur_page = String.valueOf(this.maxpage);
        int n_cur_page = getIntValue(this.cur_page, 1);
        int n_max_html_page = getIntValue(this.maxHtmlPage, 5);
        if (this.comName != null && (!"true".equals(this.createHtml) || ("true".equals(this.createHtml) && n_cur_page > n_max_html_page - 1))) {
            queryString2 = dealQueryString();
        }
        if (getIntValue(this.cur_page, 0) <= 0) {
            return 1;
        }
        if (!"true".equals(this.createHtml) || n_cur_page >= n_max_html_page) {
            String queryString3 = delQueryStr(queryString2, "page");
            if (queryString3 == null || queryString3.length() <= 0) {
                queryString = new StringBuffer().append("page=").append(this.cur_page).toString();
            } else {
                queryString = new StringBuffer().append(queryString3).append("&page=").append(this.cur_page).toString();
            }
            this_url.append("?");
            this_url.append(queryString);
            result_url = this_url.toString();
        } else {
            result_url = new StringBuffer().append(getRealHtmlFileName()).append("_p").append(n_cur_page).append(".html").toString();
        }
        HttpServletResponse response = this.pageContext.getResponse();
        results.append("<a href=\"");
        results.append(result_url);
        results.append("\">");
        try {
            this.pageContext.getOut().print(results.toString());
            return 1;
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
    }
}
