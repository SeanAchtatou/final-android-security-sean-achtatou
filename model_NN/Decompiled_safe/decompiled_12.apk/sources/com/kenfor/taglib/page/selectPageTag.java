package com.kenfor.taglib.page;

import com.kenfor.util.MyUtil;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;

public class selectPageTag extends pageTag {
    protected String onchange = null;

    public void setOnchange(String onchange2) {
        this.onchange = onchange2;
    }

    public String getOnchange() {
        return this.onchange;
    }

    public int doStartTag() throws JspException {
        return 0;
    }

    public int doEndTag() throws JspException {
        HttpServletRequest request = this.pageContext.getRequest();
        StringBuffer this_url = new StringBuffer();
        int maxpage = 0;
        String queryString = request.getQueryString();
        StringBuffer results = new StringBuffer();
        Object maxp = this.pageContext.getAttribute("maxPage");
        if (maxp == null) {
            maxp = this.pageContext.getRequest().getAttribute("maxPage");
        }
        if (maxp != null) {
            maxpage = getIntValue(String.valueOf(maxp), 0);
        }
        String t_url = request.getRequestURI();
        if (this.url == null) {
            this_url.append(t_url);
        } else {
            this_url.append(this.url);
        }
        if (queryString == null || queryString.indexOf("page") < 0) {
            this.cur_page = "0";
        } else {
            this.cur_page = String.valueOf(MyUtil.getStringToInt(String.valueOf(request.getParameter("page")), 1));
        }
        if (this.comName != null) {
            queryString = dealQueryString();
        }
        String queryString2 = delQueryStr(queryString, "page");
        if (queryString2 == null) {
            queryString2 = "pSel=true";
        }
        this_url.append("?");
        this_url.append(queryString2);
        HttpServletResponse response = this.pageContext.getResponse();
        int n_cur_page = getIntValue(this.cur_page, 1);
        int intValue = getIntValue(this.maxHtmlPage, 5);
        if ("true".equals(this.createHtml)) {
            String result_file_name = getRealHtmlFileName();
            results.append(new StringBuffer().append("<script>function changeUrl(JsName){ if(JsName==0) location.replace(\"").append(result_file_name).append(".html\");  if(JsName>0 && JsName<").append(this.maxHtmlPage).append(") location.replace(\"").append(result_file_name).append("_p\"+JsName+\".html\"); if(JsName>=").append(this.maxHtmlPage).append(") location.replace(\"").append(this_url.toString()).append("&page=\"+JsName);}</script>").toString());
        } else {
            results.append(new StringBuffer().append("<script>function changeUrl(JsName){ location.replace(\"").append(this_url.toString()).append("&page=\"+JsName);}</script>").toString());
        }
        results.append("<select name=\"pageSelected\"  onChange=\"changeUrl(this.value)\">");
        int start_pos = n_cur_page + -100 < 0 ? 0 : n_cur_page - 100;
        int end_pos = n_cur_page + 100 > maxpage ? maxpage : n_cur_page + 100;
        for (int i = start_pos; i < end_pos; i++) {
            results.append(new StringBuffer().append("<option value=\"").append(String.valueOf(i)).append("\"").toString());
            if (i == getIntValue(this.cur_page, 0)) {
                results.append("selected");
            }
            results.append(new StringBuffer().append(" >  ").append(String.valueOf(i + 1)).append("  </option>").toString());
        }
        results.append("</select>");
        try {
            this.pageContext.getOut().print(results.toString());
            return 6;
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
    }
}
