package com.kenfor.taglib.page;

import com.kenfor.util.MyUtil;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;

public class nextPageTag extends pageTag {
    public int doStartTag() throws JspException {
        String queryString;
        String result_url;
        HttpServletRequest request = this.pageContext.getRequest();
        StringBuffer this_url = new StringBuffer();
        if (this.url == null) {
            this_url.append(request.getRequestURI());
        } else {
            this_url.append(this.url);
        }
        String queryString2 = request.getQueryString();
        StringBuffer results = new StringBuffer();
        Object maxp = this.pageContext.getAttribute("maxPage");
        if (maxp == null) {
            maxp = this.pageContext.getRequest().getAttribute("maxPage");
        }
        if (maxp != null) {
            this.maxpage = getIntValue(String.valueOf(maxp), 0);
        }
        if (queryString2 == null || queryString2.indexOf("page") < 0) {
            this.cur_page = "1";
        } else {
            this.cur_page = String.valueOf(MyUtil.getStringToInt(String.valueOf(request.getParameter("page")), 1) + 1);
        }
        int n_cur_page = getIntValue(this.cur_page, 1);
        int n_max_html_page = getIntValue(this.maxHtmlPage, 5);
        if (this.comName != null && (!"true".equals(this.createHtml) || ("true".equals(this.createHtml) && n_cur_page > n_max_html_page - 1))) {
            queryString2 = dealQueryString();
        }
        if (getIntValue(this.cur_page, 0) < this.maxpage) {
            if (!"true".equals(this.createHtml) || n_cur_page >= n_max_html_page) {
                String queryString3 = delQueryStr(queryString2, "page");
                if (queryString3 == null || queryString3.length() <= 0) {
                    queryString = new StringBuffer().append("page=").append(this.cur_page).toString();
                } else {
                    queryString = new StringBuffer().append(queryString3).append("&page=").append(this.cur_page).toString();
                }
                this_url.append("?");
                this_url.append(queryString);
                result_url = this_url.toString();
            } else {
                result_url = new StringBuffer().append(getRealHtmlFileName()).append("_p").append(n_cur_page).append(".html").toString();
            }
            results.append("<a href=\"");
            results.append(this.pageContext.getResponse().encodeURL(result_url));
            results.append("\">");
            try {
                this.pageContext.getOut().print(results.toString());
            } catch (IOException e) {
                throw new JspException(e.getMessage());
            }
        }
        return 1;
    }
}
