package com.kenfor.taglib.page;

import java.io.IOException;
import java.net.URLEncoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class pageTag extends TagSupport {
    protected boolean bFirst = true;
    protected String comName = null;
    protected String createHtml;
    protected String cur_page = null;
    protected String htmlFileName;
    Log log = LogFactory.getLog(getClass().getName());
    protected String maxHtmlPage = "5";
    protected int maxpage = 0;
    protected String tempComName = null;
    protected String url = null;

    public void release() {
        pageTag.super.release();
        this.url = null;
        this.comName = null;
        this.cur_page = null;
        this.tempComName = null;
        this.bFirst = true;
        this.maxHtmlPage = "5";
    }

    /* access modifiers changed from: protected */
    public int getIntValue(String str_int_value, int default_value) {
        try {
            return Integer.valueOf(str_int_value).intValue();
        } catch (Exception e) {
            return default_value;
        }
    }

    /* access modifiers changed from: protected */
    public String getRealHtmlFileName() {
        if (this.htmlFileName == null) {
            return null;
        }
        String result = "";
        String left_result = this.htmlFileName;
        while (true) {
            int pos = left_result.indexOf("[");
            if (pos < 0) {
                break;
            }
            String t_result = left_result.substring(0, pos);
            String t_left_result = left_result.substring(pos + 1);
            int pos2 = t_left_result.indexOf("]");
            if (pos2 > 0) {
                String p_str = t_left_result.substring(0, pos2);
                left_result = t_left_result.substring(pos2 + 1);
                String p_value = this.pageContext.getRequest().getParameter(p_str);
                if (p_value == null) {
                    p_value = (String) this.pageContext.getRequest().getAttribute(p_str);
                }
                if (p_value == null) {
                    p_value = (String) this.pageContext.getSession().getAttribute(p_str);
                }
                if (p_value != null) {
                    result = new StringBuffer().append(result).append(t_result).append(strTrim(p_value)).toString();
                } else {
                    result = new StringBuffer().append(result).append(t_result).toString();
                }
            }
        }
        if (left_result == null || left_result.length() <= 0) {
            return result;
        }
        return new StringBuffer().append(result).append(left_result).toString();
    }

    /* access modifiers changed from: protected */
    public String strTrim(String str) {
        if (str != null) {
            return str.trim();
        }
        return str;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public String getUrl() {
        return this.url;
    }

    public void setComName(String comName2) {
        this.comName = comName2;
    }

    public String getComName() {
        return this.comName;
    }

    public int doStartTag() throws JspException {
        return 1;
    }

    public int doEndTag() throws JspException {
        if (getIntValue(this.cur_page, 0) < 0) {
            return 6;
        }
        try {
            this.pageContext.getOut().print("</a>");
            return 6;
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
    }

    /* access modifiers changed from: protected */
    public String dealQueryString() {
        String res = this.comName;
        String queryString = null;
        String[] comNameList = null;
        HttpServletRequest request = this.pageContext.getRequest();
        if (res != null) {
            comNameList = res.split("::");
        }
        for (String t_comName : comNameList) {
            if (t_comName != null && t_comName.length() >= 1) {
                String comValue = request.getParameter(t_comName);
                if (comValue == null) {
                    comValue = String.valueOf(this.pageContext.getAttribute(t_comName));
                }
                if (comValue != null && !"null".equalsIgnoreCase(comValue) && comValue.length() > 0) {
                    try {
                        comValue = URLEncoder.encode(comValue, request.getCharacterEncoding());
                    } catch (Exception e) {
                        this.log.error(e.getMessage());
                    }
                    queryString = addQueryStr(delQueryStr(queryString, t_comName), t_comName, comValue);
                }
                if (this.comName != null && this.comName.indexOf("::isDebug") >= 0) {
                    this.log.debug(new StringBuffer().append(t_comName).append("=").append(comValue).toString());
                }
            }
        }
        if (this.comName != null && this.comName.indexOf("::isDebug") >= 0) {
            this.log.debug(new StringBuffer().append("queryString=").append(queryString).toString());
        }
        return queryString;
    }

    /* access modifiers changed from: protected */
    public String addUnderLine(String value) {
        String result = value;
        if (!result.endsWith("`")) {
            return new StringBuffer().append(result).append("`").toString();
        }
        return result;
    }

    /* access modifiers changed from: protected */
    public String addQueryStr(String querystr, String addName, String addValue) {
        String result;
        String result2 = querystr;
        if (addName == null || addValue == null) {
            return result2;
        }
        if (addValue.trim().length() < 1 || "null".equals(addValue.trim())) {
            return result2;
        }
        if (result2 == null || result2.length() < 1) {
            result = new StringBuffer().append(addName).append("=").append(addValue).toString();
        } else {
            result = new StringBuffer().append(result2).append("&").append(addName).append("=").append(addValue).toString();
        }
        return result;
    }

    /* access modifiers changed from: protected */
    public String delQueryStr(String querystr, String exceptStr) {
        if (querystr == null || querystr.trim().length() == 0) {
            return null;
        }
        String t_str = querystr;
        String str = querystr;
        int p_index = t_str.indexOf(exceptStr);
        if (p_index > 0) {
            String result = t_str.substring(0, p_index - 1);
            String t_str2 = t_str.substring(p_index);
            int s_index = t_str2.indexOf("&");
            if (s_index < 0) {
                return result;
            }
            String result2 = new StringBuffer().append(result).append(t_str2.substring(s_index)).toString();
            String t_str3 = t_str2.substring(0, s_index);
            return result2;
        } else if (p_index != 0) {
            return str;
        } else {
            int t_index = t_str.indexOf("&");
            if (t_index > 0) {
                return t_str.substring(t_index + 1);
            }
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public String getNextComName() {
        if (this.bFirst) {
            this.bFirst = false;
            this.tempComName = this.comName;
        }
        if (this.tempComName == null || this.tempComName.length() < 1) {
            return null;
        }
        int pos = this.tempComName.indexOf("::");
        if (pos < 0) {
            String result = this.tempComName;
            this.tempComName = null;
            return result;
        }
        String result2 = this.tempComName.substring(0, pos);
        this.tempComName = this.tempComName.substring(pos + 2);
        return result2;
    }

    public String getCreateHtml() {
        return this.createHtml;
    }

    public void setCreateHtml(String createHtml2) {
        this.createHtml = createHtml2;
    }

    public String getMaxHtmlPage() {
        return this.maxHtmlPage;
    }

    public void setMaxHtmlPage(String maxHtmlPage2) {
        this.maxHtmlPage = maxHtmlPage2;
    }

    public String getHtmlFileName() {
        return this.htmlFileName;
    }

    public void setHtmlFileName(String htmlFileName2) {
        this.htmlFileName = htmlFileName2;
    }
}
