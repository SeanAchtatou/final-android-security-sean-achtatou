package com.kenfor.taglib.page;

import com.kenfor.util.MyUtil;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;

public class listPageTag extends pageTag {
    protected String listCount = "5";
    protected int nlistCount = 5;

    public void release() {
        super.release();
        this.listCount = "5";
    }

    public void setListCount(String listCount2) {
        this.listCount = listCount2;
        try {
            this.nlistCount = Integer.valueOf(listCount2).intValue();
        } catch (Exception e) {
            this.nlistCount = 5;
        }
    }

    public String getListCount() {
        return this.listCount;
    }

    public int doEndTag() throws JspException {
        return 6;
    }

    public int doStartTag() throws JspException {
        String tempQuery;
        String result_url;
        HttpServletRequest request = this.pageContext.getRequest();
        StringBuffer this_url = new StringBuffer();
        if (this.url == null) {
            this_url.append(request.getRequestURI());
        } else {
            this_url.append(this.url);
        }
        String queryString = request.getQueryString();
        StringBuffer results = new StringBuffer();
        Object maxp = this.pageContext.getAttribute("maxPage");
        if (maxp == null) {
            maxp = this.pageContext.getRequest().getAttribute("maxPage");
        }
        if (maxp != null) {
            this.maxpage = getIntValue(String.valueOf(maxp), 0);
        }
        if (queryString == null || queryString.indexOf("page") < 0) {
            this.cur_page = "1";
        } else {
            this.cur_page = String.valueOf(MyUtil.getStringToInt(String.valueOf(request.getParameter("page")), 1) + 1);
        }
        int n_cur_page = getIntValue(this.cur_page, 0);
        if (this.comName != null && this.comName.indexOf("::isDebug") > 0) {
            System.out.println(new StringBuffer().append("debug at ").append(request.getServerName()).append("/").append(request.getRequestURI()).toString());
        }
        if (this.comName != null) {
            queryString = dealQueryString();
        }
        int startPage = 0;
        int t_page = this.nlistCount;
        if (this.nlistCount < this.maxpage) {
            int mid_page = (this.nlistCount / 2) + 1;
            if (n_cur_page > mid_page) {
                startPage = n_cur_page - mid_page;
            }
            if (mid_page > this.maxpage - n_cur_page) {
                startPage = this.maxpage - this.nlistCount;
            }
        } else {
            t_page = this.maxpage;
        }
        for (int i = startPage; i < startPage + t_page; i++) {
            if (i == 0 && "true".equals(this.createHtml)) {
                result_url = new StringBuffer().append(getRealHtmlFileName()).append(".html").toString();
            } else if (!"true".equals(this.createHtml) || i >= getIntValue(this.maxHtmlPage, 5)) {
                StringBuffer temp_url = new StringBuffer();
                String tempQuery2 = delQueryStr(queryString, "page");
                if (tempQuery2 == null || tempQuery2.length() <= 0) {
                    tempQuery = new StringBuffer().append("page=").append(i).toString();
                } else {
                    tempQuery = new StringBuffer().append(tempQuery2).append("&page=").append(i).toString();
                }
                temp_url.append(this_url);
                temp_url.append("?");
                temp_url.append(tempQuery);
                result_url = temp_url.toString();
            } else {
                result_url = new StringBuffer().append(getRealHtmlFileName()).append("_p").append(i).append(".html").toString();
            }
            HttpServletResponse response = this.pageContext.getResponse();
            if (i == n_cur_page - 1) {
                results.append("<font color=\"#FF3333\">[");
                results.append(i + 1);
                results.append("]</font>&nbsp;");
            } else {
                results.append("<a href=\"");
                results.append(result_url);
                results.append("\">[");
                results.append(i + 1);
                results.append("]</a>&nbsp;");
            }
        }
        try {
            this.pageContext.getOut().print(results.toString());
            return 1;
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
    }
}
