package com.kenfor.taglib.page;

import com.kenfor.taglib.db.dbPresentTag;
import com.kenfor.util.MyUtil;
import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class pageInfoTag extends TagSupport {
    private String createHtml;
    private String maxHtmlPage;
    int nType = 0;
    String type = "0";

    public void setType(String type2) {
        this.type = type2;
        if (type2 != null && type2.length() > 0) {
            this.nType = MyUtil.getStringToInt(type2, 0);
        }
    }

    public String getType() {
        return this.type;
    }

    public int doStartTag() throws JspException {
        return 0;
    }

    public int doEndTag() throws JspException {
        int cur_page = 1;
        String str_cur_page = this.pageContext.getRequest().getParameter("page");
        String str_max_record = (String) this.pageContext.getAttribute("maxRecord");
        if (str_max_record == null) {
            str_max_record = (String) this.pageContext.getRequest().getAttribute("maxRecord");
        }
        String str_max_page = (String) this.pageContext.getAttribute("maxPage");
        if (str_max_page == null) {
            str_max_page = (String) this.pageContext.getRequest().getAttribute("maxPage");
        }
        if (str_cur_page != null && str_cur_page.length() > 0) {
            cur_page = MyUtil.getStringToInt(str_cur_page, 0) + 1;
        }
        String str_cur_page2 = String.valueOf(cur_page);
        if (str_max_record == null) {
            str_max_record = "0";
        }
        if (str_max_page == null) {
            str_max_page = "0";
        }
        int page_max = MyUtil.getStringToInt(String.valueOf((String) this.pageContext.getRequest().getAttribute("pageMax")), 0);
        int cur_count = MyUtil.getStringToInt((String) this.pageContext.getRequest().getAttribute("rowCount"), 0);
        if (this.maxHtmlPage == null) {
            this.maxHtmlPage = "5";
        }
        int max_html_page = MyUtil.getStringToInt(this.maxHtmlPage, 10);
        int start_num = ((cur_page - 1) * page_max) + 1;
        int end_num = cur_page * page_max;
        if (cur_count != page_max) {
            end_num = ((cur_page - 1) * page_max) + 1 + cur_count;
        }
        int i_max_record = MyUtil.getStringToInt(str_max_page, 0);
        if (MyUtil.getStringToInt(str_max_page, 0) == 0 && i_max_record > 0) {
            str_max_page = "1";
        }
        String result = null;
        if (this.nType == 0) {
            result = str_cur_page2;
        } else if (this.nType == 1) {
            result = str_max_page;
        } else if (this.nType == 2) {
            result = str_max_record;
        } else if (this.nType == 3) {
            result = String.valueOf(start_num);
        } else if (this.nType == 4) {
            result = String.valueOf(end_num);
        } else if (this.nType != 5) {
            result = "0";
        } else if ("true".equals(this.createHtml) && cur_page < max_html_page) {
            result = new StringBuffer().append("<!--page [").append(str_cur_page2).append(dbPresentTag.ROLE_DELIMITER).append(str_max_page).append("]  page--> ").toString();
        }
        if (result == null) {
            result = "";
        }
        try {
            this.pageContext.getOut().write(result);
            return 6;
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
    }

    public String getMaxHtmlPage() {
        return this.maxHtmlPage;
    }

    public void setMaxHtmlPage(String maxHtmlPage2) {
        this.maxHtmlPage = maxHtmlPage2;
    }

    public String getCreateHtml() {
        return this.createHtml;
    }

    public void setCreateHtml(String createHtml2) {
        this.createHtml = createHtml2;
    }
}
