package com.kenfor.taglib.page;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;

public class firstPageTag extends pageTag {
    public int doStartTag() throws JspException {
        String queryString;
        HttpServletRequest request = this.pageContext.getRequest();
        StringBuffer this_url = new StringBuffer();
        String queryString2 = request.getQueryString();
        StringBuffer results = new StringBuffer();
        Object maxp = this.pageContext.getAttribute("maxPage");
        if (maxp == null) {
            maxp = this.pageContext.getRequest().getAttribute("maxPage");
        }
        if (maxp != null) {
            this.maxpage = getIntValue(String.valueOf(maxp), 0);
        }
        this.cur_page = "0";
        if (this.comName != null && !"true".equals(this.createHtml)) {
            queryString2 = dealQueryString();
        }
        if (getIntValue(this.cur_page, 0) >= this.maxpage) {
            return 1;
        }
        if ("true".equals(this.createHtml)) {
            this_url.append(new StringBuffer().append(getRealHtmlFileName()).append(".html").toString());
        } else {
            if (this.url == null) {
                this_url.append(request.getRequestURI());
            } else {
                this_url.append(this.url);
            }
            String queryString3 = delQueryStr(queryString2, "page");
            if (queryString3 == null || queryString3.length() <= 0) {
                queryString = new StringBuffer().append("page=").append(this.cur_page).toString();
            } else {
                queryString = new StringBuffer().append(queryString3).append("&page=").append(this.cur_page).toString();
            }
            this_url.append("?");
            this_url.append(queryString);
        }
        HttpServletResponse response = this.pageContext.getResponse();
        results.append("<a href=\"");
        results.append(this_url.toString());
        results.append("\">");
        try {
            this.pageContext.getOut().print(results.toString());
            return 1;
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
    }
}
