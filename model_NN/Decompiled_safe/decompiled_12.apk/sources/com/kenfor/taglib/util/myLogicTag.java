package com.kenfor.taglib.util;

import java.io.IOException;
import java.sql.ResultSet;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.BodyTagSupport;
import org.jivesoftware.smackx.workgroup.packet.SessionID;

public class myLogicTag extends BodyTagSupport {
    private String bodyStr = null;
    private String divResult = "0";
    private String divValue = "4";
    private int div_result = 0;
    private int div_value = 4;
    private String isValueEqual = null;
    private String name = "row";
    private String property = "INDEX";
    private String scope = null;

    public void release() {
        this.name = "row";
        this.property = "INDEX";
        this.divValue = "4";
        this.divResult = "0";
        this.isValueEqual = null;
    }

    public void setDivResult(String divResult2) {
        this.divResult = divResult2;
        if (divResult2 != null) {
            this.div_result = Integer.valueOf(divResult2).intValue();
        }
    }

    public String getDivResult() {
        return this.divResult;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getName() {
        return this.name;
    }

    public void setProperty(String property2) {
        this.property = property2;
    }

    public String getProperty() {
        return this.property;
    }

    public void setDivValue(String divValue2) {
        this.divValue = divValue2;
        this.div_value = Integer.valueOf(divValue2).intValue();
    }

    public String getDivValue() {
        return this.divValue;
    }

    public void setScope(String scope2) {
        this.scope = scope2;
    }

    public String getScope() {
        return this.scope;
    }

    public void setIsValueEqual(String isValueEqual2) {
        this.isValueEqual = isValueEqual2;
    }

    public String getIsValueEqual() {
        return this.isValueEqual;
    }

    public int doStartTag() throws JspTagException {
        return 2;
    }

    public int doAfterBody() throws JspException {
        if (this.bodyContent == null) {
            return 0;
        }
        this.bodyStr = this.bodyContent.getString();
        if (this.bodyStr != null) {
            this.bodyStr = this.bodyStr.trim();
        }
        if (this.bodyStr.length() >= 1) {
            return 0;
        }
        this.bodyStr = null;
        return 0;
    }

    public int doEndTag() throws JspException {
        HttpServletRequest request = this.pageContext.getRequest();
        Object value = null;
        Object hm_row = lookup(this.pageContext, this.name, null);
        if (hm_row != null) {
            if (this.property != null) {
                if (hm_row instanceof ResultSet) {
                    if ("index".equalsIgnoreCase(this.property)) {
                        value = this.pageContext.getAttribute(new StringBuffer().append(this.name).append("_index").toString());
                    } else if ("index_1".equalsIgnoreCase(this.property)) {
                        value = this.pageContext.getAttribute(new StringBuffer().append(this.name).append("_index_1").toString());
                    } else if ("t_index".equalsIgnoreCase(this.property)) {
                        value = this.pageContext.getAttribute(new StringBuffer().append(this.name).append("_t_index").toString());
                    } else {
                        try {
                            value = ((ResultSet) hm_row).getString(this.property.trim().toLowerCase());
                        } catch (Exception e) {
                            value = null;
                        }
                    }
                }
                if (hm_row instanceof HashMap) {
                    value = ((HashMap) hm_row).get(this.property.trim());
                }
            } else {
                value = String.valueOf(hm_row);
            }
            if (hm_row instanceof String) {
                value = (String) hm_row;
            }
        }
        if (!isOk(value)) {
            return 6;
        }
        try {
            this.pageContext.getOut().print(this.bodyStr);
            return 6;
        } catch (IOException e2) {
            throw new JspException(e2.getMessage());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0048, code lost:
        if ("null".equalsIgnoreCase(r4) != false) goto L_0x004a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean isOk(java.lang.Object r9) {
        /*
            r8 = this;
            r3 = 0
            r5 = 0
            r6 = r9
            r2 = -1
            r4 = 0
            boolean r7 = r6 instanceof java.lang.String
            if (r7 == 0) goto L_0x000c
            r4 = r6
            java.lang.String r4 = (java.lang.String) r4
        L_0x000c:
            boolean r7 = r6 instanceof java.lang.Number
            if (r7 == 0) goto L_0x0014
            java.lang.String r4 = java.lang.String.valueOf(r6)
        L_0x0014:
            boolean r7 = r6 instanceof java.lang.Boolean
            if (r7 == 0) goto L_0x0039
            java.lang.String r7 = java.lang.String.valueOf(r6)
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r7)
            boolean r0 = r7.booleanValue()
            if (r0 == 0) goto L_0x0036
            java.lang.String r4 = "1"
        L_0x0028:
            java.lang.String r7 = r8.isValueEqual
            if (r7 == 0) goto L_0x0040
            java.lang.String r7 = r8.isValueEqual
            boolean r7 = r7.equals(r4)
            if (r7 == 0) goto L_0x003e
            r7 = 1
        L_0x0035:
            return r7
        L_0x0036:
            java.lang.String r4 = "0"
            goto L_0x0028
        L_0x0039:
            java.lang.String r4 = java.lang.String.valueOf(r6)
            goto L_0x0028
        L_0x003e:
            r7 = 0
            goto L_0x0035
        L_0x0040:
            if (r4 == 0) goto L_0x004a
            java.lang.String r7 = "null"
            boolean r7 = r7.equalsIgnoreCase(r4)     // Catch:{ Exception -> 0x005f }
            if (r7 == 0) goto L_0x004c
        L_0x004a:
            java.lang.String r4 = "0"
        L_0x004c:
            java.lang.Integer r7 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x005f }
            int r2 = r7.intValue()     // Catch:{ Exception -> 0x005f }
            int r7 = r8.div_value     // Catch:{ Exception -> 0x005f }
            int r5 = r2 % r7
            int r7 = r8.div_result     // Catch:{ Exception -> 0x005f }
            if (r5 != r7) goto L_0x005d
            r3 = 1
        L_0x005d:
            r7 = r3
            goto L_0x0035
        L_0x005f:
            r1 = move-exception
            r3 = 0
            goto L_0x005d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenfor.taglib.util.myLogicTag.isOk(java.lang.Object):boolean");
    }

    /* access modifiers changed from: protected */
    public Object lookup(PageContext pageContext, String name2, String scope2) throws JspException {
        if (scope2 == null) {
            Object bean = pageContext.findAttribute(name2);
            if (bean == null) {
                bean = pageContext.getAttribute(name2, 2);
            }
            if (bean == null) {
                bean = pageContext.getAttribute(name2, 3);
            }
            if (bean == null) {
                return pageContext.getAttribute(name2, 4);
            }
            return bean;
        } else if (scope2.equalsIgnoreCase("page")) {
            return pageContext.getAttribute(name2, 1);
        } else {
            if (scope2.equalsIgnoreCase("request")) {
                return pageContext.getAttribute(name2, 2);
            }
            if (scope2.equalsIgnoreCase(SessionID.ELEMENT_NAME)) {
                return pageContext.getAttribute(name2, 3);
            }
            if (scope2.equalsIgnoreCase("application")) {
                return pageContext.getAttribute(name2, 4);
            }
            throw new JspException(new StringBuffer().append("Invalid scope ").append(scope2).toString());
        }
    }
}
