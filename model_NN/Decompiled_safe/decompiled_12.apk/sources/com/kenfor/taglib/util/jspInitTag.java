package com.kenfor.taglib.util;

import com.kenfor.util.ProDebug;
import java.io.UnsupportedEncodingException;
import java.util.Locale;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class jspInitTag extends TagSupport {
    protected String isDebug = "false";
    protected String judgeLocale = null;

    public void setIsDebug(String isDebug2) {
        this.isDebug = isDebug2;
    }

    public String getIsDebug() {
        return this.isDebug;
    }

    public void setJudgeLocale(String judgeLocale2) {
        this.judgeLocale = judgeLocale2;
    }

    public String getJudgeLocale() {
        return this.judgeLocale;
    }

    public void release() {
        this.judgeLocale = null;
    }

    public int doStartTag() throws JspException {
        return 0;
    }

    public int doEndTag() throws JspException {
        HttpServletRequest request = this.pageContext.getRequest();
        HttpServletResponse response = this.pageContext.getResponse();
        String clientLanguage = request.getHeader("Accept-Language");
        if (this.isDebug.equalsIgnoreCase("true")) {
            ProDebug.addDebugLog(new StringBuffer().append("jspInitTag --  clientLanguage:").append(clientLanguage).toString());
            ProDebug.saveToFile();
        }
        if (clientLanguage != null) {
            try {
                if (clientLanguage.equals("zh-cn")) {
                    request.setCharacterEncoding("GBK");
                    response.setContentType("text/html; charset=GBK");
                } else if (clientLanguage.equals("zh-tw")) {
                    request.setCharacterEncoding("BIG5");
                    response.setContentType("text/html; charset=BIG5");
                } else {
                    request.setCharacterEncoding("ISO-8859-1");
                    response.setContentType("text/html; charset=ISO-8859-1");
                }
            } catch (UnsupportedEncodingException e) {
                throw new JspException(new StringBuffer().append("request setCharacterEncoding exception:").append(e.getMessage()).toString());
            }
        }
        if (this.judgeLocale == null || !this.judgeLocale.equalsIgnoreCase("true")) {
            return 6;
        }
        Cookie[] cookies = request.getCookies();
        String str_locale = null;
        if (cookies != null) {
            int i = 0;
            while (true) {
                if (i >= cookies.length) {
                    break;
                }
                Cookie cookie = cookies[i];
                if (cookie.getName().equalsIgnoreCase("locale_prefer")) {
                    str_locale = cookie.getValue();
                    break;
                }
                i++;
            }
        }
        if (this.isDebug.equalsIgnoreCase("true")) {
            ProDebug.addDebugLog(new StringBuffer().append("jspInitTag --  str_locale:").append(str_locale).toString());
            ProDebug.saveToFile();
        }
        if (str_locale == null) {
            return 6;
        }
        if (str_locale.equalsIgnoreCase("en")) {
            request.getSession().setAttribute("org.apache.struts.action.LOCALE", Locale.ENGLISH);
        }
        if (str_locale.equalsIgnoreCase("en_US")) {
            request.getSession().setAttribute("org.apache.struts.action.LOCALE", Locale.US);
        }
        if (str_locale.equalsIgnoreCase("zh_CN")) {
            request.getSession().setAttribute("org.apache.struts.action.LOCALE", Locale.SIMPLIFIED_CHINESE);
        }
        if (str_locale.equalsIgnoreCase("zh_TW") || str_locale.equalsIgnoreCase("zh_HK")) {
            request.getSession().setAttribute("org.apache.struts.action.LOCALE", Locale.SIMPLIFIED_CHINESE);
        }
        if (!str_locale.equalsIgnoreCase("ja_JP")) {
            return 6;
        }
        request.getSession().setAttribute("org.apache.struts.action.LOCALE", Locale.JAPANESE);
        return 6;
    }
}
