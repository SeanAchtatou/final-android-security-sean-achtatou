package com.kenfor.taglib.util;

import com.kenfor.User;
import com.kenfor.exutil.InitAction;
import com.kenfor.util.MyUtil;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class CheckLogonTag extends TagSupport {
    private String ck_menu;
    private String isrefer = "true";
    Log log = LogFactory.getLog(getClass().getName());
    private String menu_id;
    private String name = "user";
    private String nop_page = "/nop_page.jsp";
    private String page = "/logon.jsp";
    private String return_mes;
    private String showMessage;

    public String getIsrefer() {
        return this.isrefer;
    }

    public void setIsrefer(String isrefer2) {
        this.isrefer = isrefer2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getPage() {
        return this.page;
    }

    public void setPage(String page2) {
        this.page = page2;
    }

    public int doStartTag() throws JspException {
        return 0;
    }

    public int doEndTag() throws JspException {
        boolean readable;
        long user_id;
        HttpServletRequest request = this.pageContext.getRequest();
        HttpServletResponse response = this.pageContext.getResponse();
        boolean valid = false;
        HttpSession session = this.pageContext.getSession();
        if (!(session == null || session.getAttribute(this.name) == null)) {
            valid = true;
        }
        session.removeAttribute("refer");
        String t_menu_id = request.getParameter("bas_id");
        long lng_menu_id = MyUtil.getStringToLong(this.menu_id, 0);
        if (lng_menu_id == 0) {
            lng_menu_id = MyUtil.getStringToLong(t_menu_id, 0);
        }
        if (valid) {
            if (this.log.isDebugEnabled()) {
                this.log.debug("valid is true");
            }
            if ("true".equals(this.ck_menu)) {
                if (this.log.isDebugEnabled()) {
                    this.log.debug("star check with ck_menu is true");
                }
                try {
                    User user = (User) session.getAttribute(this.name);
                    long t_id_1 = user.getUser_id();
                    long t_id_2 = user.getUserId();
                    if (t_id_1 > t_id_2) {
                        user_id = t_id_1;
                    } else {
                        user_id = t_id_2;
                    }
                    String str_sql = new StringBuffer().append("select * from bas_menu_control where menu_id = ").append(lng_menu_id).append(" and sysopt_id = ").append(user_id).append(" and selected='true'").toString();
                    Connection con = new InitAction(this.pageContext).getCon();
                    Statement stmt = con.createStatement();
                    ResultSet rs = stmt.executeQuery(str_sql);
                    if (rs.next()) {
                        readable = true;
                    } else {
                        readable = false;
                    }
                    rs.close();
                    stmt.close();
                    con.close();
                } catch (Exception e) {
                    System.out.println(new StringBuffer().append("处理浏览权限时出错(CheckLogonTag),").append(e.getMessage()).toString());
                    readable = false;
                }
                if (readable) {
                    return 6;
                }
                try {
                    response.sendRedirect(this.nop_page);
                    return 5;
                } catch (Exception e2) {
                    throw new JspException(e2.toString());
                }
            } else {
                if (this.log.isDebugEnabled()) {
                    this.log.debug("no check ,just forward");
                }
                return 6;
            }
        } else {
            if (this.log.isDebugEnabled()) {
                this.log.debug(new StringBuffer().append("valid is false,name:").append(this.name).toString());
            }
            String str1 = request.getQueryString();
            String refer = request.getRequestURI();
            if (str1 != null) {
                refer = new StringBuffer().append(refer).append("?").append(str1).toString();
            }
            if ("true".equalsIgnoreCase(this.isrefer)) {
                session.setAttribute("refer", refer);
            }
            writeErrorMessage(response, this.page);
            return 5;
        }
    }

    /* access modifiers changed from: protected */
    public void writeErrorMessage(HttpServletResponse httpServletResponse, String f_page) {
        String str_mes;
        String str;
        try {
            httpServletResponse.setHeader("accept-language", "UTF-8");
            PrintWriter write = httpServletResponse.getWriter();
            InitAction initAction = new InitAction(this.pageContext.getServletContext());
            String version = initAction.getPool().getVersion();
            initAction.releaseInit();
            if (this.log.isDebugEnabled()) {
                this.log.debug(new StringBuffer().append("return_mes:").append(this.return_mes).toString());
            }
            if (this.return_mes != null && this.return_mes.length() > 0) {
                str_mes = this.return_mes;
            } else if ("en".equalsIgnoreCase(version)) {
                str_mes = "you do not log in or you log in session is timeout ,please log in again!";
            } else {
                str_mes = "您没有登录，或登录状态已过期，请重新登录！";
            }
            if (this.log.isDebugEnabled()) {
                this.log.debug(new StringBuffer().append("str_mes:").append(str_mes).toString());
            }
            String str2 = "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" /><script LANGUAGE='javascript'>";
            if ("true".equalsIgnoreCase(this.showMessage)) {
                str2 = new StringBuffer().append(str2).append("alert('  ").append(str_mes).append("  ');").toString();
            }
            if (f_page == null || f_page.length() < 1) {
                str = new StringBuffer().append(str2).append(" history.go(-1);</script>").toString();
            } else {
                str = new StringBuffer().append(str2).append(" location.href=\"").append(f_page).append("\";</script>").toString();
            }
            if (this.log.isDebugEnabled()) {
                this.log.debug(new StringBuffer().append("str:").append(str).toString());
            }
            write.write(str);
            write.flush();
            write.close();
        } catch (Exception e) {
            System.out.println("exception at buypostAction :打印信息时出错");
        }
    }

    public void release() {
        CheckLogonTag.super.release();
        this.name = "user";
        this.page = "/logon.jsp";
    }

    public String getCk_menu() {
        return this.ck_menu;
    }

    public void setCk_menu(String ck_menu2) {
        this.ck_menu = ck_menu2;
    }

    public String getNop_page() {
        return this.nop_page;
    }

    public void setNop_page(String nop_page2) {
        this.nop_page = nop_page2;
    }

    public String getMenu_id() {
        return this.menu_id;
    }

    public void setMenu_id(String menu_id2) {
        this.menu_id = menu_id2;
    }

    public String getShowMessage() {
        return this.showMessage;
    }

    public void setShowMessage(String showMessage2) {
        this.showMessage = showMessage2;
    }

    public String getReturn_mes() {
        return this.return_mes;
    }

    public void setReturn_mes(String return_mes2) {
        this.return_mes = return_mes2;
    }
}
