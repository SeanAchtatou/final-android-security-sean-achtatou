package com.kenfor.taglib.util;

import com.kenfor.exutil.InitAction;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import net.Strong.cookie.OpCookies;

public class viewCountTagBean extends TagSupport {
    private String count_field_name;
    private String id_field_name;
    private String id_value;
    private String showCount;
    private String table_name;
    private String version = "cn";

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String version2) {
        this.version = version2;
    }

    public int doStartTag() throws JspException {
        return 0;
    }

    public int doEndTag() throws JspException {
        HttpServletRequest request = this.pageContext.getRequest();
        String t_id_value = this.id_value;
        if (t_id_value == null) {
            return 6;
        }
        InitAction initAction = new InitAction(this.pageContext);
        String new_count = "0";
        if (t_id_value.indexOf("::") == 0) {
            String tp_id_value = t_id_value.substring(2);
            String id_result = null;
            if (tp_id_value != null && tp_id_value.length() > 0) {
                id_result = request.getParameter(tp_id_value);
            }
            if (id_result != null && id_result.trim().length() > 0) {
                this.id_value = id_result;
            }
        }
        boolean viewed = false;
        try {
            HttpServletResponse response = this.pageContext.getResponse();
            OpCookies op = new OpCookies();
            if (op.Read_Cookies(this.id_value, request) != null) {
                viewed = true;
            } else {
                op.Write_Cookies(this.id_value, this.id_value, 0, "/", response);
            }
        } catch (Exception e) {
            System.out.println(new StringBuffer().append("exception at viewCountTagBean:").append(e.getMessage()).toString());
        }
        if (viewed) {
            try {
                Connection con = initAction.getCon();
                String str_sql = new StringBuffer().append("select ").append(this.count_field_name).append(" from ").append(this.table_name).append(" where ").append(this.id_field_name).append(" = ").append(this.id_value).toString();
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(str_sql);
                if (rs.next()) {
                    new_count = rs.getString(1);
                }
                rs.close();
                stmt.close();
                con.close();
            } catch (Exception e2) {
                CloseCon.Close(null);
                return 6;
            }
        } else if ("cn".equalsIgnoreCase(this.version)) {
            try {
                Connection con2 = initAction.getCon();
                CallableStatement stmt2 = con2.prepareCall("{call update_table_count ( ?,?,?, ? )}");
                stmt2.setString(1, this.table_name);
                stmt2.setString(2, this.count_field_name);
                stmt2.setString(3, this.id_field_name);
                stmt2.setString(4, this.id_value);
                ResultSet rs2 = stmt2.executeQuery();
                if (rs2.next()) {
                    new_count = rs2.getString(1);
                }
                stmt2.close();
                con2.close();
            } catch (Exception e3) {
                CloseCon.Close(null);
                return 6;
            }
        }
        if ("true".equals(this.showCount)) {
            try {
                this.pageContext.getOut().write(new_count);
            } catch (IOException e4) {
                throw new JspException(new StringBuffer().append("IO Error: ").append(e4.getMessage()).toString());
            }
        }
        return 6;
    }

    public void release() {
        viewCountTagBean.super.release();
        this.version = "cn";
    }

    private String strTrim(String trim_value) {
        if (trim_value == null) {
            return "";
        }
        try {
            return trim_value.trim();
        } catch (Exception e) {
            return trim_value;
        }
    }

    public String getTable_name() {
        return this.table_name;
    }

    public void setTable_name(String table_name2) {
        this.table_name = table_name2;
    }

    public String getId_field_name() {
        return this.id_field_name;
    }

    public void setId_field_name(String id_field_name2) {
        this.id_field_name = id_field_name2;
    }

    public String getId_value() {
        return this.id_value;
    }

    public void setId_value(String id_value2) {
        this.id_value = id_value2;
    }

    public String getCount_field_name() {
        return this.count_field_name;
    }

    public void setCount_field_name(String count_field_name2) {
        this.count_field_name = count_field_name2;
    }

    public String getShowCount() {
        return this.showCount;
    }

    public void setShowCount(String showCount2) {
        this.showCount = showCount2;
    }
}
