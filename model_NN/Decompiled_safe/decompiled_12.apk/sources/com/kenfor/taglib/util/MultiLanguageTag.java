package com.kenfor.taglib.util;

import com.kenfor.util.ProDebug;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class MultiLanguageTag extends TagSupport {
    private String key = null;
    private String resourceName = null;

    public void setKey(String key2) {
        this.key = key2;
    }

    public String getKey() {
        return this.key;
    }

    public void setResourceName(String resourceName2) {
        this.resourceName = resourceName2;
    }

    public String getResourceName() {
        return this.resourceName;
    }

    public int doStartTag() throws JspException {
        return 0;
    }

    public int doEndTag() throws JspException {
        if (this.key == null) {
            throw new JspException("key can not be null");
        } else if (this.resourceName == null) {
            throw new JspException("sourceName can not be null");
        } else {
            Locale cur_locale = null;
            HttpSession session = this.pageContext.getSession();
            if (session != null) {
                cur_locale = (Locale) session.getAttribute("userLocale");
            }
            if (cur_locale == null) {
                cur_locale = Locale.getDefault();
            }
            ProDebug.addDebugLog(new StringBuffer().append("source_name:").append(this.pageContext.getServletConfig().getInitParameter("config")).toString());
            ProDebug.saveToFile();
            try {
                this.pageContext.getOut().print(ResourceBundle.getBundle(this.resourceName, cur_locale).getString(this.key));
                return 6;
            } catch (IOException e) {
                throw new JspException(e.getMessage());
            }
        }
    }

    public void release() {
        this.key = null;
        this.resourceName = null;
        MultiLanguageTag.super.release();
    }
}
