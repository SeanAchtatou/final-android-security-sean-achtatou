package com.kenfor.taglib.util;

import com.kenfor.User;
import com.kenfor.database.PoolBean;
import com.kenfor.database.weakDbSpecifyValueBean;
import com.kenfor.exutil.InitAction;
import com.kenfor.util.ProDebug;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Locale;
import java.util.WeakHashMap;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.util.MessageResources;

public class menuTag extends TagSupport {
    protected int ID = 0;
    protected boolean bMsgFlag = true;
    protected Connection con = null;
    protected weakDbSpecifyValueBean dbsBean = null;
    protected String dispFieldName = "title_cn";
    protected String isDebug = "false";
    protected Locale locale = null;
    Log log = LogFactory.getLog(getClass().getName());
    protected ArrayList menuList = null;
    protected String msgFlag = "true";
    protected String msgName = "msg_name";
    protected String msgResources = "com.kenfor.resources.ApplicationResources";
    protected String orderBy = "bas_id";
    protected String pageControl = "false";
    protected String parentIdName = "PARENT_ID";
    protected String pathFieldName = "location";
    protected PoolBean pool = null;
    protected StringBuffer resultBuf = null;
    protected String scopeType = "4";
    protected int scope_type = 4;
    protected String sqlWhere = null;
    protected String sql_where = null;
    protected Statement stmt = null;
    protected String tableName = "PRD_CLS";
    protected String userControl = null;
    protected String xmlPath = null;

    public void release() {
        this.tableName = "PRD_CLS";
        this.parentIdName = "PARENT_ID";
        this.sqlWhere = null;
        this.userControl = null;
        this.dispFieldName = "title_en";
        this.pathFieldName = "location";
        this.msgFlag = "true";
        this.msgName = "msg_name";
        this.msgResources = "com.kenfor.resources.ApplicationResources";
        this.pageControl = "false";
        this.isDebug = "false";
        this.bMsgFlag = true;
        this.scopeType = "4";
        this.scope_type = 4;
        this.dbsBean = null;
        this.resultBuf = null;
        this.menuList = null;
    }

    public void setIsDebug(String isDebug2) {
        this.isDebug = isDebug2;
    }

    public String getIsDebug() {
        return this.isDebug;
    }

    public void setPageControl(String pageControl2) {
        this.pageControl = pageControl2;
    }

    public String getPageControl() {
        return this.pageControl;
    }

    public void setMsgResources(String msgResources2) {
        this.msgResources = msgResources2;
    }

    public String getMsgResources() {
        return this.msgResources;
    }

    public void setMsgName(String msgName2) {
        this.msgName = msgName2;
    }

    public String getMsgName() {
        return this.msgName;
    }

    public void setMsgFlag(String msgFlag2) {
        this.msgFlag = msgFlag2;
        if ("false".equalsIgnoreCase(msgFlag2)) {
            this.bMsgFlag = false;
        }
    }

    public String getMsgFlag() {
        return this.msgFlag;
    }

    public void setScopeType(String scopeType2) {
        this.scopeType = scopeType2;
        if (scopeType2 != null) {
            this.scope_type = Integer.valueOf(scopeType2).intValue();
            if (this.scope_type > 4) {
                this.scope_type = 4;
            }
            if (this.scope_type < 1) {
                this.scope_type = 1;
            }
        }
    }

    public String getScopeType() {
        return this.scopeType;
    }

    public void setTableName(String tableName2) {
        this.tableName = tableName2;
    }

    public String getTableName() {
        return this.tableName;
    }

    public void setParentIdame(String parentIdName2) {
        this.parentIdName = parentIdName2;
    }

    public String getParentIdName() {
        return this.parentIdName;
    }

    public void setSqlWhere(String sqlWhere2) {
        this.sqlWhere = sqlWhere2;
    }

    public String getSqlWhere() {
        return this.sqlWhere;
    }

    public void setUserControl(String userControl2) {
        this.userControl = userControl2;
    }

    public String getUserControl() {
        return this.userControl;
    }

    public int doStartTag() throws JspException {
        return 0;
    }

    public int doEndTag() throws JspException {
        String sql;
        this.menuList = new ArrayList();
        this.sql_where = null;
        this.locale = this.pageContext.getRequest().getLocale();
        this.xmlPath = this.pageContext.getServletContext().getRealPath("/WEB-INF/classes/");
        PoolBean pool2 = (PoolBean) this.pageContext.getAttribute("pool", this.scope_type);
        if (pool2 == null) {
            pool2 = new InitAction(this.pageContext).getPool();
            this.log.error("pool is null,create new");
        }
        if (this.sqlWhere != null) {
            this.sql_where = this.sqlWhere;
        }
        User user = (User) this.pageContext.getSession().getAttribute("user");
        if (user == null) {
            try {
                this.pageContext.getOut().write("<br><b>you have not login<br>please login again!<br>");
                return 6;
            } catch (IOException e) {
                throw new JspException(new StringBuffer().append("IO Error: ").append(e.getMessage()).toString());
            }
        } else {
            long user_id = user.getUserId();
            long dept_id = user.getDeptId();
            long role_id = user.getRoleId();
            if (this.userControl != null && this.userControl.equalsIgnoreCase("true")) {
                String user_where = new StringBuffer().append("(user_id=").append(user_id).toString();
                if (dept_id != 0) {
                    user_where = new StringBuffer().append(user_where).append(" or dept_id = ").append(dept_id).toString();
                }
                if (role_id != 0) {
                    user_where = new StringBuffer().append(user_where).append(" or role_id = ").append(role_id).toString();
                }
                String user_where2 = new StringBuffer().append(user_where).append(") ").toString();
                if (this.sql_where != null) {
                    this.sql_where = new StringBuffer().append(this.sql_where).append(" and ").append(user_where2).toString();
                } else {
                    this.sql_where = user_where2;
                }
            }
            this.resultBuf = new StringBuffer();
            this.dbsBean = new weakDbSpecifyValueBean();
            try {
                if (!pool2.isStarted()) {
                    pool2.setPath(this.xmlPath);
                    pool2.initializePool();
                }
                this.con = pool2.getConnection();
                if (this.pageControl.equalsIgnoreCase("true")) {
                    if (user_id <= 0) {
                        throw new JspException("没有登录，不能使用");
                    }
                    Statement t_stmt = this.con.createStatement(1004, 1007);
                    ResultSet t_rs = t_stmt.executeQuery(new StringBuffer().append(new StringBuffer().append(new StringBuffer().append(new StringBuffer().append(new StringBuffer().append("select distinct menu_id from bas_page where bas_id not in ( ").append("select distinct p.bas_id from bas_page p,bas_page_control pc ").toString()).append("where pc.page_id=p.bas_id and pc.readflag=1 and ").toString()).append("(pc.user_id=").append(String.valueOf(user_id)).toString()).append(" or (pc.user_id IS NULL and pc.department_id = ").append(String.valueOf(dept_id)).append(") ").toString()).append(" or (pc.user_id IS NULL and pc.department_id IS NULL) ) )").toString());
                    while (t_rs.next()) {
                        this.menuList.add(t_rs.getObject(1));
                    }
                    t_rs.close();
                    t_stmt.close();
                }
                this.stmt = this.con.createStatement(1004, 1007);
                if (this.sql_where != null) {
                    sql = new StringBuffer().append("select * from ").append(this.tableName).append(" where ").append(this.sql_where).append(" and ").append(this.parentIdName).append("=0 order by ").append(this.orderBy).toString();
                } else {
                    sql = new StringBuffer().append("select * from ").append(this.tableName).append(" where ").append(this.parentIdName).append(" = 0 order by ").append(this.orderBy).toString();
                }
                if (this.isDebug.equalsIgnoreCase("true")) {
                    ProDebug.addDebugLog(new StringBuffer().append("sql at menuTag : ").append(sql).toString());
                    ProDebug.saveToFile();
                }
                ResultSet rs = this.stmt.executeQuery(sql);
                rs.last();
                int parentRowCount = rs.getRow();
                rs.beforeFirst();
                int[] bas_id = new int[parentRowCount];
                int rownum = 0;
                while (rs.next()) {
                    bas_id[rownum] = rs.getInt("BAS_ID");
                    rownum++;
                }
                rs.close();
                this.stmt.close();
                this.stmt = null;
                showMenuTree(bas_id, parentRowCount);
                this.con.close();
                if (0 != 0) {
                    this.pageContext.setAttribute("pool", pool2, this.scope_type);
                }
                if (this.isDebug.equalsIgnoreCase("true")) {
                    ProDebug.addDebugLog("step 4 at menuTag");
                    ProDebug.saveToFile();
                }
                try {
                    this.pageContext.getOut().write(this.resultBuf.toString());
                    this.resultBuf = null;
                    return 6;
                } catch (IOException e2) {
                    this.resultBuf = null;
                    throw new JspException(new StringBuffer().append("IO Error: ").append(e2.getMessage()).toString());
                }
            } catch (SQLException e3) {
                CloseCon.Close(this.con);
                throw new JspException(e3.getMessage());
            } catch (Exception e1) {
                CloseCon.Close(this.con);
                throw new JspException(e1.getMessage());
            }
        }
    }

    public void showMenuTree(int[] bas_id, int rowCount) throws Exception {
        String sql2;
        for (int j = 0; j < rowCount; j++) {
            Statement stmt_sub2 = this.con.createStatement(1004, 1007);
            if (this.sql_where != null) {
                sql2 = new StringBuffer().append("select BAS_ID from ").append(this.tableName).append(" where ").append(this.parentIdName).append("=").append(String.valueOf(bas_id[j])).append(" and ").append(this.sql_where).append(" order by BAS_ID").toString();
            } else {
                sql2 = new StringBuffer().append("select BAS_ID from ").append(this.tableName).append(" where ").append(this.parentIdName).append("=").append(String.valueOf(bas_id[j])).append(" order by BAS_ID").toString();
            }
            ResultSet rs_sub2 = stmt_sub2.executeQuery(sql2);
            rs_sub2.last();
            int childCount = rs_sub2.getRow();
            rs_sub2.beforeFirst();
            int childRowCount = 0;
            int[] bas_id2 = new int[childCount];
            while (rs_sub2.next()) {
                bas_id2[childRowCount] = rs_sub2.getInt("BAS_ID");
                childRowCount++;
            }
            rs_sub2.close();
            stmt_sub2.close();
            if (childRowCount > 0) {
                this.ID = this.ID + 1;
                this.dbsBean.setSql(new StringBuffer().append("select * from ").append(this.tableName).append(" where BAS_ID = ").append(bas_id[j]).toString());
                WeakHashMap hm = this.dbsBean.getdbValue(this.xmlPath);
                Object ttObj = null;
                String str_msg = null;
                String str_disp = null;
                if (hm != null) {
                    ttObj = hm.get("tooltips");
                    Object targetObj = hm.get("target");
                    Object hfObj = hm.get(this.pathFieldName);
                    str_msg = ((String) hm.get(this.msgName)).trim();
                    str_disp = ((String) hm.get(this.dispFieldName)).trim();
                    hm.clear();
                }
                this.resultBuf.append(new StringBuffer().append("<div id=\"main").append(this.ID).append("\" class = \"menu\"").toString());
                this.resultBuf.append(new StringBuffer().append(" onclick = \"expandIt('").append(this.ID).append("');return false\">").toString());
                this.resultBuf.append("<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                this.resultBuf.append("<tr><td width='35'");
                if (ttObj != null) {
                    this.resultBuf.append(new StringBuffer().append(" title='").append(ttObj.toString().trim()).append("' ").toString());
                }
                this.resultBuf.append("><img src='images/icon-folder1-close.gif' width='15' height='13'>");
                this.resultBuf.append("<img src='images/icon-folder-close.gif' width='16' height='15' align='absmiddle'></td>");
                this.resultBuf.append("<td>");
                if (this.bMsgFlag) {
                    this.resultBuf.append(MessageResources.getMessageResources(this.msgResources).getMessage(this.locale, str_msg));
                } else {
                    this.resultBuf.append(str_disp);
                }
                this.resultBuf.append("</td>");
                this.resultBuf.append("</tr></table></div>");
                StringBuffer stringBuffer = this.resultBuf;
                StringBuffer append = new StringBuffer().append("<div id='page");
                int i = this.ID;
                this.ID = i + 1;
                stringBuffer.append(append.append(i).append("' class='child' style='padding-left:15px'>").toString());
                this.ID = this.ID + 1;
                showMenuTree(bas_id2, childCount);
            } else {
                boolean bexp = false;
                if (this.menuList != null) {
                    int i2 = 0;
                    while (true) {
                        if (i2 >= this.menuList.size()) {
                            break;
                        } else if (Integer.valueOf(String.valueOf(this.menuList.get(i2))).intValue() == bas_id[j]) {
                            bexp = true;
                            break;
                        } else {
                            i2++;
                        }
                    }
                    if (bexp) {
                    }
                }
                this.dbsBean.setSql(new StringBuffer().append("select * from ").append(this.tableName).append(" where BAS_ID = ").append(bas_id[j]).toString());
                WeakHashMap hm2 = this.dbsBean.getdbValue(this.xmlPath);
                Object ttObj2 = null;
                Object targetObj2 = null;
                Object hfObj2 = null;
                String str_msg2 = null;
                String str_disp2 = null;
                if (hm2 != null) {
                    ttObj2 = hm2.get("tooltips");
                    targetObj2 = hm2.get("target");
                    hfObj2 = hm2.get(this.pathFieldName);
                    str_msg2 = ((String) hm2.get(this.msgName)).trim();
                    str_disp2 = ((String) hm2.get(this.dispFieldName)).trim();
                    hm2.clear();
                }
                String str_target = "main";
                if (targetObj2 != null) {
                    str_target = targetObj2.toString().trim();
                }
                this.resultBuf.append("<table width='100%' border='0' cellspacing='0' cellpadding='0'> <tr> <td></td>");
                this.resultBuf.append("<td height='20' ");
                if (ttObj2 != null) {
                    this.resultBuf.append(new StringBuffer().append(" title = '").append(ttObj2.toString().trim()).append("'").toString());
                }
                this.resultBuf.append("><img src='images/icon-folder1-open.gif' width='15' height='13'>");
                this.resultBuf.append("<img src='images/icon-page.gif' width='16' height='15' align='absmiddle' hspace='4'>");
                String hf = "";
                if (hfObj2 != null) {
                    hf = hfObj2.toString().trim();
                }
                this.resultBuf.append(new StringBuffer().append("<a href='").append(hf).toString());
                this.resultBuf.append(new StringBuffer().append("?bas_id=").append(bas_id[j]).append("' target='").append(str_target).append("'>").toString());
                if (this.bMsgFlag) {
                    this.resultBuf.append(MessageResources.getMessageResources(this.msgResources).getMessage(this.locale, str_msg2));
                } else {
                    this.resultBuf.append(str_disp2);
                }
                this.resultBuf.append("</a></td> </tr></table>");
            }
        }
        this.resultBuf.append("</div>");
    }

    public String getOrderBy() {
        return this.orderBy;
    }

    public void setOrderBy(String orderBy2) {
        this.orderBy = orderBy2;
    }
}
