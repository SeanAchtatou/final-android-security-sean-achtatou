package com.kenfor.taglib.util;

import com.kenfor.util.ProDebug;
import java.sql.Connection;

public class CloseCon {
    public static void Close(Connection con) {
        try {
            if (!con.isClosed()) {
                con.close();
            }
        } catch (Exception e) {
            ProDebug.addDebugLog("can not close con ");
            ProDebug.saveToFile();
        }
    }
}
