package com.kenfor.taglib.util;

import javax.servlet.jsp.JspException;
import org.apache.struts.taglib.html.BaseHandlerTag;
import org.apache.struts.util.MessageResources;
import org.apache.struts.util.RequestUtils;
import org.apache.struts.util.ResponseUtils;

public class CheckboxTag extends BaseHandlerTag {
    protected static MessageResources messages = MessageResources.getMessageResources("org.apache.struts.taglib.html.LocalStrings");
    protected String name = "org.apache.struts.taglib.html.BEAN";
    protected String p_value = null;
    protected String property = null;
    protected String propertyValue = null;
    protected String text = null;
    protected String value = null;

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getProperty() {
        return this.property;
    }

    public void setProperty(String property2) {
        this.property = property2;
    }

    public String getPropertyValue() {
        return this.propertyValue;
    }

    public void setPropertyValue(String propertyValue2) {
        this.propertyValue = propertyValue2;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value2) {
        this.value = value2;
    }

    public int doStartTag() throws JspException {
        String str;
        StringBuffer results = new StringBuffer("<input type=\"checkbox\"");
        results.append(" name=\"");
        if (this.indexed) {
            prepareIndex(results, this.name);
        }
        results.append(this.property);
        results.append("\"");
        if (this.accesskey != null) {
            results.append(" accesskey=\"");
            results.append(this.accesskey);
            results.append("\"");
        }
        if (this.tabindex != null) {
            results.append(" tabindex=\"");
            results.append(this.tabindex);
            results.append("\"");
        }
        results.append(" value=\"");
        this.p_value = null;
        if (this.value == null) {
            Object result = RequestUtils.lookup(this.pageContext, this.name, this.propertyValue, (String) null);
            if (result == null) {
                results.append("on");
            } else {
                if (!(result instanceof String)) {
                    result = result.toString();
                }
                this.p_value = (String) result;
                results.append(this.p_value);
            }
        } else {
            results.append(this.value);
        }
        results.append("\"");
        Object result2 = RequestUtils.lookup(this.pageContext, this.name, this.property, (String) null);
        if (result2 == null) {
            result2 = "";
        }
        if (!(result2 instanceof String)) {
            str = result2.toString();
        } else {
            str = result2;
        }
        String checked = (String) str;
        if (checked.equalsIgnoreCase(this.value) || checked.equalsIgnoreCase(this.p_value) || checked.equalsIgnoreCase("true") || checked.equalsIgnoreCase("yes") || checked.equalsIgnoreCase("on")) {
            results.append(" checked=\"checked\"");
        }
        results.append(prepareEventHandlers());
        results.append(prepareStyles());
        results.append(">");
        ResponseUtils.write(this.pageContext, results.toString());
        this.text = null;
        return 2;
    }

    public int doAfterBody() throws JspException {
        if (this.bodyContent == null) {
            return 0;
        }
        String value2 = this.bodyContent.getString().trim();
        if (value2.length() <= 0) {
            return 0;
        }
        this.text = value2;
        return 0;
    }

    public int doEndTag() throws JspException {
        if (this.text == null) {
            return 6;
        }
        ResponseUtils.write(this.pageContext, this.text);
        return 6;
    }

    public void release() {
        CheckboxTag.super.release();
        this.name = "org.apache.struts.taglib.html.BEAN";
        this.property = null;
        this.text = null;
        this.value = null;
    }
}
