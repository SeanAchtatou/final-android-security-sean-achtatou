package com.kenfor.taglib.util;

import com.kenfor.User;
import com.kenfor.database.PoolBean;
import com.kenfor.database.weakDbSpecifyValueBean;
import com.kenfor.exutil.InitAction;
import com.kenfor.util.ProDebug;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.WeakHashMap;
import javax.servlet.jsp.JspException;
import org.apache.struts.util.MessageResources;

public class menuPageControlTag extends menuTag {
    protected ArrayList menuList = null;

    public int doEndTag() throws JspException {
        String sql;
        this.menuList = new ArrayList();
        this.sql_where = null;
        this.locale = this.pageContext.getRequest().getLocale();
        this.xmlPath = this.pageContext.getServletContext().getRealPath("/WEB-INF/classes/");
        PoolBean pool = (PoolBean) this.pageContext.getAttribute("pool", this.scope_type);
        if (pool == null) {
            pool = new InitAction(this.pageContext).getPool();
            this.log.error("pool is null,create new");
        }
        if (this.sqlWhere != null) {
            this.sql_where = this.sqlWhere;
        }
        if (this.isDebug.equalsIgnoreCase("true")) {
            ProDebug.addDebugLog("step 2 at menuTag");
            ProDebug.saveToFile();
        }
        User user = (User) this.pageContext.getSession().getAttribute("user");
        if (user == null) {
            try {
                this.pageContext.getOut().write("<br><b>you have not login<br>please login again!<br>");
                return 6;
            } catch (IOException e) {
                throw new JspException(new StringBuffer().append("IO Error: ").append(e.getMessage()).toString());
            }
        } else {
            long user_id = user.getUserId();
            long dept_id = user.getDeptId();
            long roleId = user.getRoleId();
            this.resultBuf = new StringBuffer();
            this.dbsBean = new weakDbSpecifyValueBean();
            try {
                if (!pool.isStarted()) {
                    pool.setPath(this.xmlPath);
                    pool.initializePool();
                }
                this.con = pool.getConnection();
                if (this.pageControl.equalsIgnoreCase("true")) {
                    if (user_id <= 0) {
                        throw new JspException("没有登录，不能使用");
                    }
                    Statement t_stmt = this.con.createStatement(1004, 1007);
                    ResultSet t_rs = t_stmt.executeQuery(new StringBuffer().append(new StringBuffer().append(new StringBuffer().append(new StringBuffer().append(new StringBuffer().append("select distinct menu_id from bas_page where bas_id not in ( ").append("select distinct p.bas_id from bas_page p,bas_page_control pc ").toString()).append("where pc.page_id=p.bas_id and pc.readflag=1 and ").toString()).append("(pc.user_id=").append(String.valueOf(user_id)).toString()).append(" or (pc.user_id IS NULL and pc.department_id = ").append(String.valueOf(dept_id)).append(") ").toString()).append(" or (pc.user_id IS NULL and pc.department_id IS NULL) ) )").toString());
                    while (t_rs.next()) {
                        this.menuList.add(t_rs.getObject(1));
                    }
                    t_rs.close();
                    t_stmt.close();
                }
                this.stmt = this.con.createStatement(1004, 1007);
                if (this.sql_where != null) {
                    sql = new StringBuffer().append("select * from ").append(this.tableName).append(" where ").append(this.sql_where).append(" and ").append(this.parentIdName).append("=0 order by BAS_ID").toString();
                } else {
                    sql = new StringBuffer().append("select * from ").append(this.tableName).append(" where ").append(this.parentIdName).append(" = 0 order by BAS_ID").toString();
                }
                if (this.isDebug.equalsIgnoreCase("true")) {
                    ProDebug.addDebugLog(new StringBuffer().append("sql at menuTag : ").append(sql).toString());
                    ProDebug.saveToFile();
                }
                ResultSet rs = this.stmt.executeQuery(sql);
                rs.last();
                int parentRowCount = rs.getRow();
                rs.beforeFirst();
                int[] bas_id = new int[parentRowCount];
                int rownum = 0;
                while (rs.next()) {
                    bas_id[rownum] = rs.getInt("BAS_ID");
                    rownum++;
                }
                rs.close();
                this.stmt.close();
                this.stmt = null;
                showMenuTree(bas_id, parentRowCount);
                this.con.close();
                if (0 != 0) {
                    this.pageContext.setAttribute("pool", pool, this.scope_type);
                }
                if (this.isDebug.equalsIgnoreCase("true")) {
                    ProDebug.addDebugLog("step 4 at menuTag");
                    ProDebug.saveToFile();
                }
                try {
                    this.pageContext.getOut().write(this.resultBuf.toString());
                    this.resultBuf = null;
                    return 6;
                } catch (IOException e2) {
                    throw new JspException(new StringBuffer().append("IO Error: ").append(e2.getMessage()).toString());
                }
            } catch (SQLException e3) {
                CloseCon.Close(this.con);
                throw new JspException(e3.getMessage());
            } catch (Exception e1) {
                CloseCon.Close(this.con);
                throw new JspException(e1.getMessage());
            }
        }
    }

    public void showMenuTree(int[] bas_id, int rowCount) throws Exception {
        String sql2;
        if (!this.con.isClosed()) {
            this.con.close();
            this.con = null;
            this.con = this.pool.getConnection();
        }
        for (int j = 0; j < rowCount; j++) {
            Statement stmt_sub2 = this.con.createStatement(1004, 1007);
            if (this.sql_where != null) {
                sql2 = new StringBuffer().append("select BAS_ID from ").append(this.tableName).append(" where ").append(this.parentIdName).append("=").append(String.valueOf(bas_id[j])).append(" and ").append(this.sql_where).append(" order by BAS_ID").toString();
            } else {
                sql2 = new StringBuffer().append("select BAS_ID from ").append(this.tableName).append(" where ").append(this.parentIdName).append("=").append(String.valueOf(bas_id[j])).append(" order by BAS_ID").toString();
            }
            ResultSet rs_sub2 = stmt_sub2.executeQuery(sql2);
            rs_sub2.last();
            int childCount = rs_sub2.getRow();
            rs_sub2.beforeFirst();
            int childRowCount = 0;
            int[] bas_id2 = new int[childCount];
            while (rs_sub2.next()) {
                bas_id2[childRowCount] = rs_sub2.getInt("BAS_ID");
                childRowCount++;
            }
            rs_sub2.close();
            stmt_sub2.close();
            if (childRowCount > 0) {
                this.ID = this.ID + 1;
                this.dbsBean.setSql(new StringBuffer().append("select * from ").append(this.tableName).append(" where BAS_ID = ").append(bas_id[j]).toString());
                WeakHashMap hm = this.dbsBean.getdbValue(this.xmlPath);
                Object ttObj = null;
                String str_msg = null;
                String str_disp = null;
                if (hm != null) {
                    ttObj = hm.get("tooltips");
                    Object targetObj = hm.get("target");
                    Object hfObj = hm.get(this.pathFieldName);
                    str_msg = ((String) hm.get(this.msgName)).trim();
                    str_disp = ((String) hm.get(this.dispFieldName)).trim();
                    hm.clear();
                }
                this.resultBuf.append(new StringBuffer().append("<div id=\"main").append(this.ID).append("\" class = \"menu\"").toString());
                this.resultBuf.append(new StringBuffer().append(" onclick = \"expandIt('").append(this.ID).append("');return false\">").toString());
                this.resultBuf.append("<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                this.resultBuf.append("<tr><td width='35'");
                if (ttObj != null) {
                    this.resultBuf.append(new StringBuffer().append(" title='").append(ttObj.toString().trim()).append("' ").toString());
                }
                this.resultBuf.append("><img src='images/icon-folder1-close.gif' width='15' height='13'>");
                this.resultBuf.append("<img src='images/icon-folder-close.gif' width='16' height='15' align='absmiddle'></td>");
                this.resultBuf.append("<td>");
                if (this.bMsgFlag) {
                    this.resultBuf.append(MessageResources.getMessageResources(this.msgResources).getMessage(this.locale, str_msg));
                } else {
                    this.resultBuf.append(str_disp);
                }
                this.resultBuf.append("</td>");
                this.resultBuf.append("</tr></table></div>");
                StringBuffer stringBuffer = this.resultBuf;
                StringBuffer append = new StringBuffer().append("<div id='page");
                int i = this.ID;
                this.ID = i + 1;
                stringBuffer.append(append.append(i).append("' class='child' style='padding-left:15px'>").toString());
                this.ID = this.ID + 1;
                showMenuTree(bas_id2, childCount);
            } else {
                boolean bexp = false;
                if (this.menuList != null) {
                    int i2 = 0;
                    while (true) {
                        if (i2 >= this.menuList.size()) {
                            break;
                        } else if (Integer.valueOf(String.valueOf(this.menuList.get(i2))).intValue() == bas_id[j]) {
                            bexp = true;
                            break;
                        } else {
                            i2++;
                        }
                    }
                    if (bexp) {
                    }
                }
                this.dbsBean.setSql(new StringBuffer().append("select * from ").append(this.tableName).append(" where BAS_ID = ").append(bas_id[j]).toString());
                WeakHashMap hm2 = this.dbsBean.getdbValue(this.xmlPath);
                Object ttObj2 = null;
                Object targetObj2 = null;
                Object hfObj2 = null;
                String str_msg2 = null;
                String str_disp2 = null;
                if (hm2 != null) {
                    ttObj2 = hm2.get("tooltips");
                    targetObj2 = hm2.get("target");
                    hfObj2 = hm2.get(this.pathFieldName);
                    str_msg2 = ((String) hm2.get(this.msgName)).trim();
                    str_disp2 = ((String) hm2.get(this.dispFieldName)).trim();
                    hm2.clear();
                }
                String str_target = "main";
                if (targetObj2 != null) {
                    str_target = targetObj2.toString().trim();
                }
                this.resultBuf.append("<table width='100%' border='0' cellspacing='0' cellpadding='0'> <tr> <td></td>");
                this.resultBuf.append("<td height='20' ");
                if (ttObj2 != null) {
                    this.resultBuf.append(new StringBuffer().append(" title = '").append(ttObj2.toString().trim()).append("'").toString());
                }
                this.resultBuf.append("><img src='images/icon-folder1-open.gif' width='15' height='13'>");
                this.resultBuf.append("<img src='images/icon-page.gif' width='16' height='15' align='absmiddle' hspace='4'>");
                String hf = "";
                if (hfObj2 != null) {
                    hf = hfObj2.toString().trim();
                }
                this.resultBuf.append(new StringBuffer().append("<a href='").append(hf).toString());
                this.resultBuf.append(new StringBuffer().append("?bas_id=").append(bas_id[j]).append("' target='").append(str_target).append("'>").toString());
                if (this.bMsgFlag) {
                    this.resultBuf.append(MessageResources.getMessageResources(this.msgResources).getMessage(this.locale, str_msg2));
                } else {
                    this.resultBuf.append(str_disp2);
                }
                this.resultBuf.append("</a></td> </tr></table>");
            }
        }
        this.resultBuf.append("</div>");
    }
}
