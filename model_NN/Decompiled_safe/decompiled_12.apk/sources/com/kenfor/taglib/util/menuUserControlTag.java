package com.kenfor.taglib.util;

import com.kenfor.User;
import com.kenfor.database.PoolBean;
import com.kenfor.exutil.InitAction;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.jsp.JspException;

public class menuUserControlTag extends menuTag {
    public int doEndTag() throws JspException {
        long start_time = System.currentTimeMillis();
        this.sql_where = null;
        this.locale = this.pageContext.getRequest().getLocale();
        this.xmlPath = this.pageContext.getServletContext().getRealPath("/WEB-INF/classes/");
        if (this.sqlWhere != null) {
            this.sql_where = this.sqlWhere;
        }
        User user = (User) this.pageContext.getSession().getAttribute("user");
        if (user == null) {
            try {
                this.pageContext.getOut().write("<br><b>you have not login<br>please login again!<br>");
                this.pageContext.getOut().flush();
                return 6;
            } catch (IOException e) {
                this.log.error(e.getMessage());
                return 6;
            }
        } else {
            String user_where = new StringBuffer().append(new StringBuffer().append("(user_id=").append(user.getUserId()).toString()).append(") ").toString();
            if (this.sql_where != null) {
                this.sql_where = new StringBuffer().append(this.sql_where).append(" and ").append(user_where).toString();
            } else {
                this.sql_where = user_where;
            }
            String sql = new StringBuffer().append("select * from ").append(this.tableName).toString();
            if (this.sql_where != null) {
                sql = new StringBuffer().append(sql).append(" where ").append(this.sql_where).toString();
            }
            if (this.orderBy != null) {
                sql = new StringBuffer().append(sql).append(" order by ").append(this.orderBy).toString();
            }
            try {
                showMenuTree(this.xmlPath, sql);
                try {
                    this.pageContext.getOut().write(this.resultBuf.toString());
                    this.pageContext.getOut().flush();
                    this.resultBuf = null;
                    long take_time = System.currentTimeMillis() - start_time;
                    if (this.log.isDebugEnabled()) {
                        this.log.debug(new StringBuffer().append("menu list spend time:").append(take_time).toString());
                    }
                    return 6;
                } catch (IOException e2) {
                    this.resultBuf = null;
                    this.log.error(e2.getMessage());
                    return 6;
                }
            } catch (Exception e3) {
                this.log.error(e3.getMessage());
                return 6;
            }
        }
    }

    public void showMenuTree(String path, String sql) throws Exception {
        String t_id;
        this.pool = (PoolBean) this.pageContext.getAttribute("pool", this.scope_type);
        if (this.pool == null) {
            this.pool = new InitAction(this.pageContext).getPool();
            this.log.error("pool is null,create new");
        }
        if (!this.pool.isStarted()) {
            this.log.error("pool is not started");
        }
        ClassItems c_item = new ClassItems();
        c_item.setIdFieldName("bas_id");
        c_item.setParentFieldName(this.parentIdName);
        c_item.initData(this.pool, sql);
        if (this.resultBuf == null) {
            this.resultBuf = new StringBuffer();
        }
        ArrayList id_list = new ArrayList();
        while (true) {
            HashMap t_map = c_item.getDataItem("0", "0");
            if (t_map == null) {
                this.resultBuf.append("</div>");
                c_item.clearData();
                CloseCon.Close(this.con);
                return;
            }
            Object hfObj = t_map.get(this.pathFieldName);
            String str_disp = (String) t_map.get(this.dispFieldName);
            Object targetObj = t_map.get("target");
            String valueOf = String.valueOf(hfObj);
            String valueOf2 = String.valueOf(t_map.get("level"));
            String isLeaf = String.valueOf(t_map.get("isLeaf"));
            String str_id = String.valueOf(t_map.get("bas_id"));
            String str_p_id = String.valueOf(t_map.get(this.parentIdName.toLowerCase()));
            if ("false".equals(isLeaf)) {
                if (id_list.size() > 0 && (t_id = (String) id_list.get(id_list.size() - 1)) != null && !t_id.equals(str_p_id)) {
                    this.resultBuf.append("</div>");
                    id_list.remove(id_list.size() - 1);
                }
                id_list.add(str_id);
                this.ID = this.ID + 1;
                this.resultBuf.append(new StringBuffer().append("<div id=\"main").append(this.ID).append("\" class = \"menu\"").toString());
                this.resultBuf.append(new StringBuffer().append(" onclick = \"expandIt('").append(this.ID).append("');return false\">").toString());
                this.resultBuf.append("<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                this.resultBuf.append("<tr><td width='35'");
                this.resultBuf.append("><img src='images/icon-folder1-close.gif' width='15' height='13'>");
                this.resultBuf.append("<img src='images/icon-folder-close.gif' width='16' height='15' align='absmiddle'></td>");
                this.resultBuf.append("<td>");
                this.resultBuf.append(str_disp);
                this.resultBuf.append("</td>");
                this.resultBuf.append("</tr></table></div>");
                StringBuffer stringBuffer = this.resultBuf;
                StringBuffer append = new StringBuffer().append("<div id='page");
                int i = this.ID;
                this.ID = i + 1;
                stringBuffer.append(append.append(i).append("' class='child' style='padding-left:15px'>").toString());
            } else {
                this.resultBuf.append("<table width='100%' border='0' cellspacing='0' cellpadding='0'> <tr> <td></td>");
                this.resultBuf.append("<td height='20' ");
                this.resultBuf.append("><img src='images/icon-folder1-open.gif' width='15' height='13'>");
                this.resultBuf.append("<img src='images/icon-page.gif' width='16' height='15' align='absmiddle' hspace='4'>");
                String hf = "";
                if (hfObj != null) {
                    hf = hfObj.toString().trim();
                }
                String str_target = "main";
                if (targetObj != null) {
                    str_target = targetObj.toString().trim();
                }
                this.resultBuf.append(new StringBuffer().append("<a href='").append(hf).toString());
                this.resultBuf.append(new StringBuffer().append("?bas_id=").append(str_id).append("&ck_menu=true' target='").append(str_target).append("'>").toString());
                this.resultBuf.append(str_disp);
                this.resultBuf.append("</a></td> </tr></table>");
            }
        }
    }
}
