package com.kenfor.taglib.util;

import com.kenfor.database.PoolBean;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class ClassItems {
    public String idFieldName = "bas_id";
    public boolean isLeaf = false;
    public boolean isNext = false;
    private ArrayList itemList = null;
    private int level = 0;
    public HashMap nextMap = null;
    public String parentFieldName = "parent_id";

    public void clearData() {
        this.nextMap = null;
        Iterator iterator = this.itemList.iterator();
        if (iterator.hasNext()) {
            ((HashMap) iterator.next()).clear();
            iterator.remove();
        }
        this.itemList = null;
    }

    public void initData(PoolBean pool, String sql) {
        String t_str;
        try {
            Connection con = pool.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            ResultSetMetaData rsmd = rs.getMetaData();
            int colCount = rsmd.getColumnCount();
            if (this.itemList == null) {
                this.itemList = new ArrayList();
            }
            while (rs.next()) {
                HashMap row = new HashMap();
                for (int i = 1; i <= colCount; i++) {
                    String name = rsmd.getColumnName(i).toLowerCase();
                    Object obj = rs.getObject(i);
                    if (obj instanceof String) {
                        if (obj == null) {
                            t_str = "";
                        } else {
                            t_str = String.valueOf(obj).trim();
                        }
                        row.put(name.trim(), t_str);
                    } else {
                        row.put(name.trim(), obj);
                    }
                }
                this.itemList.add(row);
            }
            rs.close();
            stmt.close();
            con.close();
        } catch (Exception e) {
            System.out.println(new StringBuffer().append("get classItem exception:").append(e.getMessage()).toString());
        }
    }

    private void initData(Connection con, String sql) throws SQLException {
        String t_str;
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        ResultSetMetaData rsmd = rs.getMetaData();
        int colCount = rsmd.getColumnCount();
        if (this.itemList == null) {
            this.itemList = new ArrayList();
        }
        while (rs.next()) {
            HashMap row = new HashMap();
            for (int i = 1; i <= colCount; i++) {
                String name = rsmd.getColumnName(i).toLowerCase();
                Object obj = rs.getObject(i);
                if (obj instanceof String) {
                    if (obj == null) {
                        t_str = "";
                    } else {
                        t_str = String.valueOf(obj).trim();
                    }
                    row.put(name.trim(), t_str);
                } else {
                    row.put(name.trim(), obj);
                }
            }
            this.itemList.add(row);
        }
        rs.close();
        stmt.close();
        con.close();
    }

    public HashMap getDataItem(String p_id, String p_p_id, String hasParent) {
        this.isNext = false;
        int list_count = this.itemList.size();
        HashMap result_map = null;
        if ("true".equalsIgnoreCase(hasParent)) {
            return getDataItem(p_id, p_p_id);
        }
        int id_remove = -1;
        int i = 0;
        while (true) {
            if (i < list_count) {
                HashMap t_map = (HashMap) this.itemList.get(i);
                if (!"true".equals(String.valueOf(t_map.get("del")))) {
                    id_remove = i;
                    result_map = t_map;
                    break;
                }
                i++;
            } else {
                break;
            }
        }
        if (id_remove >= 0) {
            this.itemList.remove(id_remove);
            result_map.put("del", "true");
            this.itemList.add(id_remove, result_map);
        }
        return result_map;
    }

    public HashMap getDataItem(String p_id, String p_p_id) {
        HashMap result_map;
        HashMap t_map;
        this.isNext = false;
        int size = this.itemList.size();
        if (this.nextMap == null) {
            result_map = getItem(p_id, p_p_id);
        } else {
            result_map = this.nextMap;
        }
        if (result_map == null) {
            return null;
        }
        String t_id = String.valueOf(result_map.get(this.idFieldName.toLowerCase()));
        String t_p_id = String.valueOf(result_map.get(this.parentFieldName.toLowerCase()));
        String str_isleaf = String.valueOf(result_map.get("isLeaf"));
        this.isNext = true;
        if ("true".equals(str_isleaf)) {
            String tt_p_id = getPID(t_p_id);
            this.level--;
            int while_level = 0;
            do {
                t_map = getItem(t_p_id, tt_p_id);
                if (t_map != null) {
                    break;
                }
                tt_p_id = getPID(tt_p_id);
                while_level++;
                this.level--;
                t_map = null;
            } while (while_level <= 10);
            this.nextMap = t_map;
            return result_map;
        }
        this.nextMap = getItem(t_id, t_p_id);
        return result_map;
    }

    private HashMap getItem(String p_id, String p_p_id) {
        int size = this.itemList.size();
        HashMap result_map = getItem(p_id);
        if (result_map == null) {
            result_map = getItem(p_p_id);
            if (result_map != null) {
                String valueOf = String.valueOf(result_map.get(this.idFieldName.toLowerCase()));
                if ("0".equals(p_p_id)) {
                    this.level = 1;
                }
                result_map.put("level", String.valueOf(this.level));
            }
        } else {
            String valueOf2 = String.valueOf(result_map.get(this.idFieldName.toLowerCase()));
            String t_p_id = String.valueOf(result_map.get(this.parentFieldName.toLowerCase()));
            if (p_id != null && p_id.equals(t_p_id)) {
                this.level++;
            }
            result_map.put("level", String.valueOf(this.level));
        }
        return result_map;
    }

    private boolean isLeaf(String id) {
        boolean result = true;
        int list_count = this.itemList.size();
        int i = 0;
        while (true) {
            if (i >= list_count) {
                break;
            }
            String t_id = String.valueOf(((HashMap) this.itemList.get(i)).get(this.parentFieldName.toLowerCase()));
            if (t_id != null && t_id.equals(id)) {
                result = false;
                break;
            }
            i++;
        }
        return result;
    }

    private String getPID(String id) {
        if (id == null) {
            return "0";
        }
        int list_count = this.itemList.size();
        String result = "0";
        int i = 0;
        while (true) {
            if (i < list_count) {
                HashMap t_map = (HashMap) this.itemList.get(i);
                String p_value = String.valueOf(t_map.get(this.idFieldName.toLowerCase()));
                if (p_value != null && id.equals(p_value.trim())) {
                    result = String.valueOf(t_map.get(this.parentFieldName.toLowerCase()));
                    break;
                }
                i++;
            } else {
                break;
            }
        }
        return result;
    }

    private int getItemIndex(String id) {
        String p_value;
        if (id == null) {
            return -1;
        }
        int list_count = this.itemList.size();
        int result = -1;
        int i = 0;
        while (true) {
            if (i >= list_count) {
                break;
            }
            HashMap t_map = (HashMap) this.itemList.get(i);
            if (!"true".equals(String.valueOf(t_map.get("del"))) && (p_value = String.valueOf(t_map.get(this.idFieldName.toLowerCase()))) != null && id.equals(p_value.trim())) {
                result = i;
                break;
            }
            i++;
        }
        return result;
    }

    private HashMap getItem(String p_id) {
        String p_value;
        if (p_id == null) {
            return null;
        }
        int list_count = this.itemList.size();
        HashMap result_map = null;
        int id_remove = -1;
        int i = 0;
        while (true) {
            if (i >= list_count) {
                break;
            }
            HashMap t_map = (HashMap) this.itemList.get(i);
            if (!"true".equals(String.valueOf(t_map.get("del"))) && (p_value = String.valueOf(t_map.get(this.parentFieldName.toLowerCase()))) != null && p_id.equals(p_value.trim())) {
                result_map = t_map;
                id_remove = i;
                result_map.put("isLeaf", Boolean.valueOf(isLeaf(String.valueOf(t_map.get(this.idFieldName.toLowerCase())))));
                break;
            }
            i++;
        }
        if (id_remove >= 0) {
            this.itemList.remove(id_remove);
            result_map.put("del", "true");
            this.itemList.add(id_remove, result_map);
        }
        return result_map;
    }

    public void setIdFieldName(String idFieldName2) {
        this.idFieldName = idFieldName2;
    }

    public void setParentFieldName(String parentFieldName2) {
        this.parentFieldName = parentFieldName2;
    }
}
