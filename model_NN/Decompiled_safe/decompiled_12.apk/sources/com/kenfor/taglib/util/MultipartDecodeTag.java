package com.kenfor.taglib.util;

import com.kenfor.util.ProDebug;
import com.kenfor.util.upload.MultipartIterator;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import org.xbill.DNS.KEYRecord;

public class MultipartDecodeTag extends TagSupport {
    private HashMap customFieldTypeMap = null;
    private int encodingIndex = 1;
    private ArrayList etNameList = null;
    private ArrayList fList = null;
    private HashMap fMap = null;
    private HashMap fMapParam = null;
    private HashMap fSizeMap = null;
    private int fileCount = 0;
    private HashMap fileMap = null;
    private ArrayList fileNameList = null;
    private String imgPath = "upload_pic";
    private String isFileCanNull = "true";
    private String isOnlyImage = "true";
    private MultipartIterator iterator = null;
    private ProDebug myDebug = new ProDebug();
    private String noChangeFileName = "false";
    private StringBuffer sqlBuf = new StringBuffer();
    private String subImgPath = null;
    private String sys_separator = null;
    private String thisImgPath = null;
    private String thisSubImgPath = null;

    public void release() {
        this.imgPath = "upload_pic";
        this.subImgPath = null;
        this.isFileCanNull = "true";
        this.isOnlyImage = "true";
        this.noChangeFileName = "false";
    }

    public void setIsOnlyImage(String isOnlyImage2) {
        this.isOnlyImage = isOnlyImage2;
    }

    public String getIsOnlyImage() {
        return this.isOnlyImage;
    }

    public void setImgPath(String imgPath2) {
        this.imgPath = imgPath2;
    }

    public String getImgPath() {
        return this.imgPath;
    }

    public void setSubImgPath(String subImgPath2) {
        this.subImgPath = subImgPath2;
    }

    public String getSubImgPath() {
        return this.subImgPath;
    }

    public void setIsFileCanNull(String isFileCanNull2) {
        this.isFileCanNull = isFileCanNull2;
    }

    public String getIsFileCanNull() {
        return this.isFileCanNull;
    }

    public void setNoChangeFileName(String noChangeFileName2) {
        this.noChangeFileName = noChangeFileName2;
    }

    public String getNoChangeFileName() {
        return this.noChangeFileName;
    }

    public String getSqlResult() {
        return this.sqlBuf.toString();
    }

    public int doStartTag() throws JspException {
        return 0;
    }

    public int doEndTag() throws JspException {
        HttpServletRequest request = this.pageContext.getRequest();
        try {
            validate(request);
            this.iterator = new MultipartIterator(request, KEYRecord.Flags.FLAG5, 4194304, null);
            if (this.imgPath != null) {
                this.thisImgPath = this.pageContext.getServletContext().getRealPath(new StringBuffer().append("/").append(this.imgPath).append("/").toString());
            } else {
                this.thisImgPath = this.pageContext.getServletContext().getRealPath("/");
            }
            this.fileMap = new HashMap();
            this.fileNameList = new ArrayList();
            this.etNameList = new ArrayList();
            this.fileCount = 0;
            initMap(request);
            analyseSubImgPath();
            uploadFile();
            this.pageContext.setAttribute("paramMap", this.fMap);
            this.pageContext.setAttribute("paramList", this.fList);
            return 6;
        } catch (Exception e) {
            throw new JspException(new StringBuffer().append("some exception occur : ").append(e.getMessage()).toString());
        }
    }

    private void uploadFile() throws Exception {
        String realFileName;
        for (int i = 0; i < this.fileNameList.size(); i++) {
            String fileName = (String) this.fileNameList.get(i);
            String e_name = (String) this.etNameList.get(i);
            Date nowDate = new Date();
            if (this.noChangeFileName.compareToIgnoreCase("false") == 0) {
                int pIndex = fileName.indexOf(".");
                int fLen = fileName.length();
                String fileExt = ".bmp";
                if (pIndex > 0) {
                    fileExt = fileName.substring(pIndex, fLen);
                }
                fileName = new StringBuffer().append(String.valueOf(nowDate.getTime())).append(fileExt).toString();
            }
            if (this.thisSubImgPath != null) {
                realFileName = new StringBuffer().append(this.thisImgPath).append(this.sys_separator).append(this.thisSubImgPath).append(this.sys_separator).append(fileName).toString();
            } else {
                realFileName = new StringBuffer().append(this.thisImgPath).append(this.sys_separator).append(fileName).toString();
            }
            FileInputStream fin = new FileInputStream((File) this.fileMap.get(e_name.trim()));
            FileOutputStream fout = new FileOutputStream(realFileName);
            byte[] buffer = new byte[KEYRecord.Flags.FLAG2];
            while (true) {
                int bytesRead = fin.read(buffer, 0, KEYRecord.Flags.FLAG2);
                if (bytesRead == -1) {
                    break;
                }
                fout.write(buffer, 0, bytesRead);
            }
            fout.close();
            fin.close();
            this.fMap.put(e_name.trim(), fileName);
        }
    }

    private void validate(HttpServletRequest request) throws Exception {
        this.sys_separator = System.getProperty("file.separator");
        if (request == null) {
            throw new Exception("request is null");
        }
    }

    private void analyseSubImgPath() throws Exception {
        String temp = this.subImgPath;
        if (temp != null) {
            String temp2 = temp.trim();
            if (temp2.length() > 0) {
                int ii = temp2.indexOf(":");
                if (ii >= 0) {
                    this.thisSubImgPath = (String) this.fMap.get(temp2.substring(ii + 1).trim());
                    if (this.thisSubImgPath != null) {
                        this.thisSubImgPath = this.thisSubImgPath.trim();
                        return;
                    }
                    return;
                }
                this.thisSubImgPath = temp2;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x006e, code lost:
        throw new java.lang.Exception("there are some com name is null");
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void initMap(javax.servlet.http.HttpServletRequest r12) throws java.lang.Exception {
        /*
            r11 = this;
            r5 = 0
            java.util.HashMap r8 = new java.util.HashMap
            r8.<init>()
            r11.fMap = r8
            java.util.HashMap r8 = new java.util.HashMap
            r8.<init>()
            r11.fSizeMap = r8
            java.util.ArrayList r8 = new java.util.ArrayList
            r8.<init>()
            r11.fList = r8
            r3 = 0
        L_0x0018:
            com.kenfor.util.upload.MultipartIterator r8 = r11.iterator
            com.kenfor.util.upload.MultipartElement r3 = r8.getNextElement()
            if (r3 != 0) goto L_0x0021
            return
        L_0x0021:
            boolean r8 = r3.isFile()
            if (r8 == 0) goto L_0x00a8
            long r5 = r3.getFileSize()
            r8 = 0
            int r8 = (r5 > r8 ? 1 : (r5 == r8 ? 0 : -1))
            if (r8 <= 0) goto L_0x0096
            java.lang.String r8 = r11.isOnlyImage
            java.lang.String r9 = "true"
            int r8 = r8.compareToIgnoreCase(r9)
            if (r8 != 0) goto L_0x004f
            java.lang.String r0 = r3.getContentType()
            java.lang.String r8 = "image"
            int r8 = r0.indexOf(r8)
            if (r8 >= 0) goto L_0x004f
            java.lang.Exception r8 = new java.lang.Exception
            java.lang.String r9 = "you can only upload image file "
            r8.<init>(r9)
            throw r8
        L_0x004f:
            java.io.File r7 = r3.getFile()
            java.lang.String r4 = r3.getFileName()
            java.lang.String r2 = r3.getName()
            if (r2 == 0) goto L_0x0067
            java.lang.String r8 = r2.trim()
            int r8 = r8.length()
            if (r8 != 0) goto L_0x006f
        L_0x0067:
            java.lang.Exception r8 = new java.lang.Exception
            java.lang.String r9 = "there are some com name is null"
            r8.<init>(r9)
            throw r8
        L_0x006f:
            int r8 = r11.fileCount
            int r8 = r8 + 1
            r11.fileCount = r8
            java.util.HashMap r8 = r11.fileMap
            java.lang.String r9 = r2.trim()
            r8.put(r9, r7)
            java.util.ArrayList r8 = r11.fileNameList
            r8.add(r4)
            java.util.ArrayList r8 = r11.etNameList
            r8.add(r2)
            java.util.HashMap r8 = r11.fSizeMap
            java.lang.String r9 = r2.trim()
            java.lang.String r10 = java.lang.String.valueOf(r5)
            r8.put(r9, r10)
            goto L_0x0018
        L_0x0096:
            java.lang.String r8 = r11.isFileCanNull
            java.lang.String r9 = "false"
            int r8 = r8.compareToIgnoreCase(r9)
            if (r8 != 0) goto L_0x0018
            java.lang.Exception r8 = new java.lang.Exception
            java.lang.String r9 = "file can not be null or empty"
            r8.<init>(r9)
            throw r8
        L_0x00a8:
            java.lang.String r2 = r3.getName()
            java.lang.String r1 = r3.getFileName()
            if (r2 == 0) goto L_0x00bc
            java.lang.String r8 = r2.trim()
            int r8 = r8.length()
            if (r8 != 0) goto L_0x00c4
        L_0x00bc:
            java.lang.Exception r8 = new java.lang.Exception
            java.lang.String r9 = "there are some com name is null"
            r8.<init>(r9)
            throw r8
        L_0x00c4:
            java.util.HashMap r8 = r11.fMap
            java.lang.String r9 = r2.trim()
            java.lang.String r10 = r3.getValue()
            r8.put(r9, r10)
            java.util.ArrayList r8 = r11.fList
            java.lang.String r9 = r2.trim()
            r8.add(r9)
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenfor.taglib.util.MultipartDecodeTag.initMap(javax.servlet.http.HttpServletRequest):void");
    }
}
