package com.kenfor.taglib;

import com.kenfor.util.ProDebug;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.BodyTagSupport;

public class tagTest extends BodyTagSupport {
    private String body = null;
    private String name = null;

    public void setName(String name2) {
        this.name = name2;
    }

    public String getName() {
        return this.name;
    }

    public int doStartTag() throws JspTagException {
        return 2;
    }

    public int doAfterBody() throws JspException {
        if (this.bodyContent == null) {
            return 0;
        }
        this.body = this.bodyContent.getString();
        if (this.body != null) {
            this.body = this.body.trim();
        }
        if (this.body.length() >= 1) {
            return 0;
        }
        this.body = null;
        return 0;
    }

    public int doEndTag() throws JspException {
        HttpServletRequest request = this.pageContext.getRequest();
        ProDebug.addDebugLog("tagTest");
        ProDebug.saveToFile();
        String result = null;
        if (0 == 0) {
            result = "result is null";
        }
        if (this.body != null) {
            result = new StringBuffer().append(result).append(this.body).toString();
        }
        try {
            this.pageContext.getOut().print(result);
            return 6;
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
    }
}
