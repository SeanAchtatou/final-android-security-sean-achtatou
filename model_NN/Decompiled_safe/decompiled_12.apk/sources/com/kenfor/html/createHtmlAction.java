package com.kenfor.html;

import com.kenfor.exutil.InitAction;
import com.kenfor.exutil.createHtml;
import com.kenfor.taglib.db.dbPresentTag;
import com.kenfor.util.CloseCon;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class createHtmlAction extends Action {
    public ActionForward perform(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        Log log = LogFactory.getLog(getClass().getName());
        createHtmlActionForm form = (createHtmlActionForm) actionForm;
        int create_info_type = form.getCreate_info_type();
        int create_list_type = form.getCreate_list_type();
        String info_type = form.getInfo_type();
        String start_date = form.getStart_date();
        String end_date = form.getEnddate();
        ActionErrors errors = null;
        String param_list = form.getParam_list();
        String static_no = form.getStatic_no();
        String list_no = form.getList_no();
        String table_name = form.getTable_name();
        String param_field_list = form.getParam_field_list();
        String back_url = form.getBack_url();
        String sql_where = form.getSql_where();
        String date_field_name = form.getDate_field_name();
        String isDebug = form.getIsDebug();
        ArrayList pl_list = new ArrayList();
        ArrayList pfl_list = new ArrayList();
        String ref = httpServletRequest.getHeader("referer");
        String retStr = null;
        if (ref != null) {
            int i = ref.indexOf("?");
            if (i != -1) {
                retStr = ref.substring(0, i);
            } else {
                retStr = ref;
            }
        }
        String success_url = new StringBuffer().append(retStr).append("?result=ok").toString();
        String failure_url = new StringBuffer().append(retStr).append("?result=error").toString();
        if (!(param_list == null || param_field_list == null)) {
            String[] t_p_list = param_list.split(dbPresentTag.ROLE_DELIMITER);
            String[] t_pf_list = param_field_list.split(dbPresentTag.ROLE_DELIMITER);
            if (t_p_list.length == t_pf_list.length) {
                int i2 = 0;
                while (i2 < t_p_list.length) {
                    String str1 = t_p_list[i2];
                    String str2 = t_pf_list[i2];
                    if (str1.length() < 1 || str2.length() < 1) {
                        errors.add("org.apache.struts.action.GLOBAL_ERROR", new ActionError("error.blank", "设置的参数格式不对"));
                        saveErrors(httpServletRequest, errors);
                        try {
                            httpServletResponse.sendRedirect(failure_url);
                            return null;
                        } catch (Exception e) {
                            return null;
                        }
                    } else {
                        pl_list.add(t_p_list[i2]);
                        pfl_list.add(t_pf_list[i2]);
                        i2++;
                    }
                }
            }
        }
        if (create_info_type == 0 && create_list_type == 0) {
            errors.add("org.apache.struts.action.GLOBAL_ERROR", new ActionError("error.blank", "未选定范围"));
            saveErrors(httpServletRequest, errors);
            return actionMapping.findForward("failture");
        }
        Connection con = null;
        InitAction initAction = new InitAction(this.servlet);
        if (create_info_type == 1 && static_no != null) {
            String str_sql = null;
            if (info_type != null && "1".equals(info_type)) {
                str_sql = new StringBuffer().append("select ").append(param_field_list).append(" from ").append(table_name).append(" where (is_updated_html = 0 or is_updated_html is null)").toString();
                if (sql_where != null && sql_where.length() > 0) {
                    str_sql = new StringBuffer().append(str_sql).append(" and ").append(sql_where).toString();
                }
            } else if (info_type != null && "2".equals(info_type) && start_date != null) {
                if (date_field_name == null || date_field_name.length() < 1) {
                    date_field_name = "pub_time";
                }
                str_sql = new StringBuffer().append("select ").append(param_field_list).append(" from ").append(table_name).append(" where ").append(date_field_name).append(" >'").append(start_date).append("'").toString();
                if (end_date != null) {
                    str_sql = new StringBuffer().append(str_sql).append(" and ").append(date_field_name).append("-1<'").append(end_date).append("'").toString();
                }
                if (sql_where != null && sql_where.length() > 0) {
                    str_sql = new StringBuffer().append(str_sql).append(" and ").append(sql_where).toString();
                }
            } else if (info_type != null && "3".equals(info_type)) {
                str_sql = new StringBuffer().append("select ").append(param_field_list).append(" from ").append(table_name).toString();
                if (sql_where != null && sql_where.length() > 0) {
                    str_sql = new StringBuffer().append(str_sql).append(" where ").append(sql_where).toString();
                }
            }
            if (log.isDebugEnabled()) {
                log.debug(new StringBuffer().append("str_sql:").append(str_sql).toString());
            }
            if (str_sql != null) {
                try {
                    con = initAction.getCon();
                    Statement stmt = con.createStatement();
                    ResultSet rs = stmt.executeQuery(str_sql);
                    while (rs.next()) {
                        HashMap pmap = null;
                        int i3 = 0;
                        while (pl_list != null) {
                            if (i3 >= pl_list.size()) {
                                break;
                            }
                            String value = rs.getString((String) pfl_list.get(i3));
                            if (pmap == null) {
                                pmap = new HashMap();
                            }
                            pmap.put(pl_list.get(i3), value);
                            i3++;
                        }
                        createHtml html = new createHtml();
                        int create_type = 3;
                        if ("1".equals(info_type)) {
                            create_type = 1;
                        }
                        if (log.isDebugEnabled()) {
                            log.debug("start doCreate to create single page at creatHtmlAction");
                        }
                        if (!html.doCreate(this.servlet, static_no, pmap, create_type)) {
                            System.out.println(new StringBuffer().append("create html message:").append(html.getError_mes()).toString());
                        }
                        pmap.clear();
                    }
                    rs.close();
                    stmt.close();
                    con.close();
                } catch (Exception e2) {
                    CloseCon.Close(con);
                    errors.add("org.apache.struts.action.GLOBAL_ERROR", new ActionError("error.blank", "查询需处理的信息时出错"));
                    saveErrors(httpServletRequest, errors);
                    try {
                        httpServletResponse.sendRedirect(failure_url);
                        return null;
                    } catch (Exception e3) {
                        return null;
                    }
                }
            }
        }
        if (create_list_type == 1 && list_no != null) {
            createHtml html2 = new createHtml();
            if (log.isDebugEnabled()) {
                log.debug("start doCreate to create list page at creatHtmlAction");
            }
            if (!html2.doCreate(this.servlet, list_no)) {
                System.out.println(new StringBuffer().append("create list html message:").append(html2.getError_mes()).toString());
            }
        }
        try {
            httpServletResponse.sendRedirect(success_url);
            return null;
        } catch (Exception e4) {
            return null;
        }
    }
}
