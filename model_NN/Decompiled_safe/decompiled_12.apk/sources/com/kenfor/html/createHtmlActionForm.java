package com.kenfor.html;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class createHtmlActionForm extends ActionForm {
    private String back_url;
    private int create_info_type;
    private int create_list_type;
    private String date_field_name;
    private String enddate;
    private String info_type;
    private String isDebug;
    private String list_no;
    private String param_field_list;
    private String param_list;
    private String sql_where;
    private String start_date;
    private String static_no;
    private String table_name;

    public int getCreate_info_type() {
        return this.create_info_type;
    }

    public void setCreate_info_type(int create_info_type2) {
        this.create_info_type = create_info_type2;
    }

    public int getCreate_list_type() {
        return this.create_list_type;
    }

    public void setCreate_list_type(int create_list_type2) {
        this.create_list_type = create_list_type2;
    }

    public String getEnddate() {
        return this.enddate;
    }

    public void setEnddate(String enddate2) {
        this.enddate = enddate2;
    }

    public String getInfo_type() {
        return this.info_type;
    }

    public void setInfo_type(String info_type2) {
        this.info_type = info_type2;
    }

    public String getStart_date() {
        return this.start_date;
    }

    public void setStart_date(String start_date2) {
        this.start_date = start_date2;
    }

    public ActionErrors validate(ActionMapping actionMapping, HttpServletRequest httpServletRequest) {
        return null;
    }

    public void reset(ActionMapping actionMapping, HttpServletRequest httpServletRequest) {
    }

    public String getParam_list() {
        return this.param_list;
    }

    public void setParam_list(String param_list2) {
        this.param_list = param_list2;
    }

    public String getStatic_no() {
        return this.static_no;
    }

    public void setStatic_no(String static_no2) {
        this.static_no = static_no2;
    }

    public String getList_no() {
        return this.list_no;
    }

    public void setList_no(String list_no2) {
        this.list_no = list_no2;
    }

    public String getTable_name() {
        return this.table_name;
    }

    public void setTable_name(String table_name2) {
        this.table_name = table_name2;
    }

    public String getParam_field_list() {
        return this.param_field_list;
    }

    public void setParam_field_list(String param_field_list2) {
        this.param_field_list = param_field_list2;
    }

    public String getBack_url() {
        return this.back_url;
    }

    public void setBack_url(String back_url2) {
        this.back_url = back_url2;
    }

    public String getSql_where() {
        return this.sql_where;
    }

    public void setSql_where(String sql_where2) {
        this.sql_where = sql_where2;
    }

    public String getDate_field_name() {
        return this.date_field_name;
    }

    public void setDate_field_name(String date_field_name2) {
        this.date_field_name = date_field_name2;
    }

    public String getIsDebug() {
        return this.isDebug;
    }

    public void setIsDebug(String isDebug2) {
        this.isDebug = isDebug2;
    }
}
