package com.kenfor.test;

import com.kenfor.client3g.util.Constant;
import com.kenfor.database.PoolBean;
import java.sql.Connection;

public class pooltest {
    public static void main(String[] args) {
        try {
            PoolBean pool = new PoolBean();
            pool.setPath("/home/tradenet/lighting86/WEB-INF/classes/");
            pool.initializePool();
            long i = 0;
            while (true) {
                Connection con = pool.getConnection();
                Thread.sleep(Constant.DELAY_TIME);
                i++;
                if (i % 1000 == 0) {
                    System.out.println(new StringBuffer().append("i=").append(i).toString());
                }
            }
        } catch (Exception e1) {
            System.out.print(e1.getMessage());
        }
    }
}
