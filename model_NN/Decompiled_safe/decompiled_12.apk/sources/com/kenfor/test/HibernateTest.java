package com.kenfor.test;

import net.sf.hibernate.HibernateException;
import net.sf.hibernate.SessionFactory;
import net.sf.hibernate.cfg.Configuration;

public class HibernateTest {
    public static void main(String[] args) {
        if ("/hibernate.cfg.xml" != 0) {
            try {
                SessionFactory buildSessionFactory = new Configuration().configure("/hibernate.cfg.xml").buildSessionFactory();
            } catch (HibernateException ex) {
                throw new RuntimeException(new StringBuffer().append("Exception building SessionFactory: ").append(ex.getMessage()).toString(), ex);
            } catch (Exception e) {
                throw new RuntimeException(e.getMessage());
            }
        } else {
            SessionFactory buildSessionFactory2 = new Configuration().configure().buildSessionFactory();
        }
    }
}
