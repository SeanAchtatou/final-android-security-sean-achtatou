package com.kenfor.test;

import java.io.Serializable;
import java.util.Calendar;
import org.apache.struts.action.ActionForm;

public class SysUser extends ActionForm implements Serializable {
    private Calendar lastlogin;
    private long userid;
    private String username;
    private String userpasword;

    public long getUserid() {
        return this.userid;
    }

    public void setUserid(long userid2) {
        this.userid = userid2;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username2) {
        this.username = username2;
    }

    public String getUserpasword() {
        return this.userpasword;
    }

    public void setUserpasword(String userpasword2) {
        this.userpasword = userpasword2;
    }

    public Calendar getLastlogin() {
        return this.lastlogin;
    }

    public void setLastlogin(Calendar lastlogin2) {
        this.lastlogin = lastlogin2;
    }
}
