package com.kenfor.test;

import com.kenfor.util.ImageProp;

public class ImgTest {
    public static void main(String[] args) {
        try {
            ImageProp img = new ImageProp("E:\\source\\bmp\\Food19.jpg");
            int w = img.getImgWidth();
            int h = img.getImgHeight();
            System.out.print(new StringBuffer().append("width : ").append(w).toString());
            System.out.print(new StringBuffer().append("height : ").append(h).toString());
        } catch (Exception e) {
            System.out.print(new StringBuffer().append("error ").append(e.getMessage()).toString());
        }
    }

    protected static int intelInt(int i) {
        return ((i & 255) << 24) + ((65280 & i) << 8) + ((16711680 & i) >> 8) + ((i >> 24) & 255);
    }
}
