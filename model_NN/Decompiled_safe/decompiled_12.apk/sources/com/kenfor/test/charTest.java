package com.kenfor.test;

import com.kenfor.exutil.htmlFilter;
import java.lang.Character;

public class charTest {
    public static void main(String[] args) {
        String str2 = htmlFilter.filter("<span style='FONT-SIZE: 10.5pt; COLOR: black; FONT-FAMILY: 宋体; mso-ascii-font-family: 'Times New Roman'; mso-hansi-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; mso-font-kerning: 1.0pt; mso-ansi-language: EN-US; mso-fareast-language: ZH-CN; mso-bidi-language: AR-SA'>中山国旅推出的旅游界首张&ldquo;</span><strong><span style='FONT-SIZE: 10.5pt; COLOR: red; FONT-FAMILY: 宋体; mso-ascii-font-family: 'Times New Roman'; mso-hansi-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; mso-font-kerning: 1.0pt; mso-ansi-language: EN-US; mso-fareast-language: ZH-CN; mso-bidi-language: AR-SA'>旅游智能卡</span></strong><span style='FONT-SIZE: 10.5pt; COLOR: black; FONT-FAMILY: 宋体; mso-ascii-font-family: 'Times New Roman'; mso-hansi-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; mso-font-kerning: 1.0pt; mso-ansi-language: EN-US; mso-fareast-language: ZH-CN; mso-bidi-language: AR-SA'>&rdquo;，经过近</span><span lang='EN-US' style='FONT-SIZE: 10.5pt; COLOR: black; FONT-FAMILY: &quot;Times New Roman&quot;; mso-font-kerning: 1.0pt; mso-ansi-language: EN-US; mso-fareast-language: ZH-CN; mso-bidi-language: AR-SA; mso-fareast-font-family: 宋体'>2</span><span style='FONT-SIZE: 10.5pt; COLOR: black; FONT-FAMILY: 宋体; mso-ascii-font-family: 'Times New Roman'; mso-hansi-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; mso-font-kerning: 1.0pt; mso-ansi-language: EN-US; mso-fareast-language: ZH-CN; mso-bidi-language: AR-SA'>年的运作，如今智能卡会员已遍布中山城乡各地。近期，为答谢中山旅游消费者，中山国旅特向智能卡会员推出&ldquo;</span><strong><span style='FONT-SIZE: 10.5pt; COLOR: red; FONT-FAMILY: 宋体; mso-ascii-font-family: 'Times New Roman'; mso-hansi-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; mso-font-kerning: 1.0pt; mso-ansi-language: EN-US; mso-fareast-language: ZH-CN; mso-bidi-language: AR-SA'>积分百万金大回赠</span></strong><span style='FONT-SIZE: 10.5pt; COLOR: black; FONT-FAMILY: 宋体; mso-ascii-font-family: 'Times New Roman'; mso-hansi-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; mso-font-kerning: 1.0pt; mso-ansi-language: EN-US; mso-fareast-language: ZH-CN; mso-bidi-language: AR-SA'>&rdquo;活动，受到了市民的普遍欢迎。</span></p>';", 30);
        System.out.println(new StringBuffer().append(str2).append(",  len:").append(str2.length()).toString());
    }

    public static String getCharArray(String inStr) {
        if (inStr == null) {
            return null;
        }
        char[] myBuffer = inStr.toCharArray();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < inStr.length(); i++) {
            byte b = (byte) myBuffer[i];
            short s = (short) myBuffer[i];
            String hexB = Integer.toHexString(b).toUpperCase();
            String hexS = Integer.toHexString(s).toUpperCase();
            sb.append("char[");
            sb.append(i);
            sb.append("]='");
            sb.append(myBuffer[i]);
            sb.append("'\t");
            sb.append("byte=");
            sb.append((int) b);
            sb.append(" \\u");
            sb.append(hexB);
            sb.append(9);
            sb.append("short=");
            sb.append((int) s);
            sb.append(" \\u");
            sb.append(hexS);
            sb.append(9);
            sb.append(Character.UnicodeBlock.of(myBuffer[i]));
            sb.append("<br />");
        }
        return sb.toString();
    }
}
