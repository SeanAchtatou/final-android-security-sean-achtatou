package com.kenfor.test;

import java.sql.ResultSet;
import java.util.ArrayList;
import org.apache.commons.dbcp.BasicDataSource;

public class dbsJspTest {
    public void doDbsJspTest() {
        BasicDataSource bds = new BasicDataSource();
        bds.setDriverClassName("net.sourceforge.jtds.jdbc.Driver");
        bds.setUrl("jdbc:jtds:sqlserver://localhost:1433/tradenet;charset=gb2312");
        bds.setUsername("sa");
        bds.setPassword("1234");
        bds.setMaxActive(50);
        bds.setMaxIdle(14);
        bds.setMaxWait(10);
        bds.setMinEvictableIdleTimeMillis(18000);
        bds.setTimeBetweenEvictionRunsMillis(8000);
        bds.setMinIdle(5);
        new ArrayList();
        try {
            try {
                ResultSet rs = bds.getConnection().createStatement().executeQuery("select top 10 * from bas_member");
                while (rs.next()) {
                    String bas_no = rs.getString("bas_no");
                    String address = rs.getString("address");
                    System.out.println(new StringBuffer().append("bas_no:").append(bas_no).toString());
                    System.out.println(new StringBuffer().append("address:").append(address).toString());
                }
            } catch (Exception e) {
            }
            bds.close();
            System.out.println(new StringBuffer().append("bds start colose,cur active:").append(bds.getNumActive()).toString());
        } catch (Exception e2) {
            System.out.println(e2.getMessage());
        }
    }
}
