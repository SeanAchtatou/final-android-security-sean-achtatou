package com.kenfor.test;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

public class uploadTestActionForm extends ActionForm {
    private String fileName;
    private FormFile imgFile;

    public String getFileName() {
        return this.fileName;
    }

    public void setFileName(String fileName2) {
        this.fileName = fileName2;
    }

    public FormFile getImgFile() {
        return this.imgFile;
    }

    public void setImgFile(FormFile imgFile2) {
        this.imgFile = imgFile2;
    }

    public ActionErrors validate(ActionMapping actionMapping, HttpServletRequest httpServletRequest) {
        return null;
    }

    public void reset(ActionMapping actionMapping, HttpServletRequest httpServletRequest) {
        this.imgFile = null;
        this.fileName = null;
    }
}
