package com.kenfor.test;

import java.sql.CallableStatement;
import java.sql.DriverManager;
import java.sql.ResultSet;

public class sqlTest {
    public static void main(String[] args) {
        try {
            Class.forName("com.microsoft.jdbc.sqlserver.SQLServerDriver");
            CallableStatement stmt = DriverManager.getConnection("jdbc:microsoft:sqlserver://192.168.4.120:1433;DatabaseName=tournet", "tournet", "tournet123").prepareCall("select top 10 * from bas_article where article_title like ?");
            stmt.setString(1, "%森林%");
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                System.out.println(rs.getString("article_title"));
            }
        } catch (Exception e1) {
            System.out.print(e1.getMessage());
        }
    }
}
