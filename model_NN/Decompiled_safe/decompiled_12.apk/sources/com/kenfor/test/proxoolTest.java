package com.kenfor.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.logicalcobwebs.proxool.ProxoolFacade;
import org.logicalcobwebs.proxool.configuration.JAXPConfigurator;

public class proxoolTest {
    static Class class$com$kenfor$test$proxoolTest;

    public static void main(String[] args) {
        Class cls;
        if (class$com$kenfor$test$proxoolTest == null) {
            cls = class$("com.kenfor.test.proxoolTest");
            class$com$kenfor$test$proxoolTest = cls;
        } else {
            cls = class$com$kenfor$test$proxoolTest;
        }
        Log log = LogFactory.getLog(cls.getName());
        try {
            log.debug("start");
            JAXPConfigurator.configure("E:\\bjb_project\\java_class\\web\\WEB-INF\\classes\\proxool.xml", false);
            Connection con = DriverManager.getConnection("proxool.test");
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select top 10 * from bas_member");
            while (rs.next()) {
                System.out.println(rs.getString("bas_no"));
            }
            rs.close();
            stmt.close();
            con.close();
            log.debug("end");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        new ProxoolFacade();
        ProxoolFacade.shutdown(1);
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError(x1.getMessage());
        }
    }
}
