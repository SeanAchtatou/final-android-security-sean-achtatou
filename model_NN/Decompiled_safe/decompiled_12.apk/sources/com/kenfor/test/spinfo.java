package com.kenfor.test;

public class spinfo {
    private boolean release_ok;
    private String url;

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public boolean isRelease_ok() {
        return this.release_ok;
    }

    public void setRelease_ok(boolean release_ok2) {
        this.release_ok = release_ok2;
    }
}
