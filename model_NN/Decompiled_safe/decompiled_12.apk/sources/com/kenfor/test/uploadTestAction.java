package com.kenfor.test;

import com.kenfor.exutil.HibernateUtilPlus;
import com.kenfor.util.CodeTransfer;
import com.kenfor.util.strutsUploadImage;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

public class uploadTestAction extends Action {
    public ActionForward perform(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        uploadTestActionForm uploadtestactionform = (uploadTestActionForm) actionForm;
        try {
            HibernateUtilPlus.currentSession(this.servlet.getServletContext());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return actionMapping.findForward("success");
    }

    private ActionForward test(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        FormFile logo_File = ((uploadTestActionForm) actionForm).getImgFile();
        System.out.println("start uploadTestAction");
        try {
            int fileSize = logo_File.getFileSize();
            strutsUploadImage suFile = new strutsUploadImage(logo_File, "f:\\pic");
            try {
                suFile.setMaxSize(10485760);
                suFile.uploadFile();
                String realFileName = suFile.getRealFileName();
                String smallFileName = suFile.getSmallFileName();
                return actionMapping.findForward("success");
            } catch (Exception e) {
                return actionMapping.findForward("failure");
            }
        } catch (Exception e2) {
            return actionMapping.findForward("failure");
        }
    }

    /* access modifiers changed from: package-private */
    public String createErrorMessage(String message) {
        if (message == null) {
            message = "";
        }
        return new StringBuffer().append("<body leftmargin=0 topmargin=0>&nbsp;&nbsp;<font style='font-size:12px'>").append(message).append("</font> <input type=button value='返回上传界面' onclick='javascript:history.go(-1)' style='font-size:12px'></body>").toString();
    }

    /* access modifiers changed from: protected */
    public void writeErrorMessage(HttpServletResponse httpServletResponse, String message) {
        try {
            httpServletResponse.setHeader("accept-language", "UTF-8");
            PrintWriter write = httpServletResponse.getWriter();
            write.write(CodeTransfer.UnicodeToISO(message));
            write.flush();
            write.close();
        } catch (Exception e) {
            System.out.println("exception at buypostAction :打印信息时出错");
        }
    }
}
