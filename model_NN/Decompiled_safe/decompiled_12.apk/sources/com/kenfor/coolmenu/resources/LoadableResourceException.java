package com.kenfor.coolmenu.resources;

public class LoadableResourceException extends Exception {
    public LoadableResourceException() {
    }

    public LoadableResourceException(String msg) {
        super(msg);
    }
}
