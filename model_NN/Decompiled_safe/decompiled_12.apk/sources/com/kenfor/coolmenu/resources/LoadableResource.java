package com.kenfor.coolmenu.resources;

import javax.servlet.http.HttpServlet;

public interface LoadableResource {
    String getLoadParam();

    String getName();

    HttpServlet getServlet();

    void load() throws LoadableResourceException;

    void reload() throws LoadableResourceException;

    void setLoadParam(String str);

    void setName(String str);

    void setServlet(HttpServlet httpServlet);
}
