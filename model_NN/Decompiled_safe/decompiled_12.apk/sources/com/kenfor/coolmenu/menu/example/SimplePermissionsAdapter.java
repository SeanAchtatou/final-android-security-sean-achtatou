package com.kenfor.coolmenu.menu.example;

import com.kenfor.coolmenu.menu.MenuComponent;
import com.kenfor.coolmenu.menu.PermissionsAdapter;
import java.util.ArrayList;

public class SimplePermissionsAdapter implements PermissionsAdapter {
    private ArrayList menuNames = new ArrayList();

    public SimplePermissionsAdapter(String[] theMenuNames) {
        if (theMenuNames != null) {
            for (String add : theMenuNames) {
                this.menuNames.add(add);
            }
        }
    }

    public boolean isAllowed(MenuComponent menu) {
        return !this.menuNames.contains(menu.getName());
    }
}
