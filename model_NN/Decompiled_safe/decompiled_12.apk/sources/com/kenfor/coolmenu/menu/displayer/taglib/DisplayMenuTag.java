package com.kenfor.coolmenu.menu.displayer.taglib;

import com.kenfor.coolmenu.menu.MenuComponent;
import com.kenfor.coolmenu.menu.MenuRepository;
import com.kenfor.coolmenu.menu.displayer.MenuDisplayer;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class DisplayMenuTag extends TagSupport {
    private String name;
    private String target;

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getTarget() {
        return this.target;
    }

    public void setTarget(String target2) {
        this.target = target2;
    }

    public int doStartTag() throws JspException {
        MenuDisplayer displayer = (MenuDisplayer) this.pageContext.getAttribute(UseMenuDisplayerTag.DISPLAYER_KEY);
        if (displayer == null) {
            throw new JspException("Could not retrieve the menu displayer.");
        }
        MenuRepository repository = (MenuRepository) this.pageContext.findAttribute(UseMenuDisplayerTag.MENU_REPOSITORY_KEY);
        if (repository == null) {
            throw new JspException("Could not obtain the menu repository");
        }
        try {
            MenuComponent menu = repository.getMenu(this.name);
            if (menu == null) {
                return 0;
            }
            try {
                if (this.target != null) {
                    displayer.setTarget(this.target);
                }
                setPageLocation(menu);
                displayer.display(menu);
                displayer.setTarget(null);
                return 0;
            } catch (Exception e) {
                return 0;
            }
        } catch (Exception e2) {
            throw new JspException(new StringBuffer().append("get Menu erro : ").append(e2.getMessage()).toString());
        }
    }

    /* access modifiers changed from: protected */
    public void setPageLocation(MenuComponent menu) {
        if (menu.getLocation() == null && menu.getPage() != null) {
            menu.setLocation(new StringBuffer().append(this.pageContext.getRequest().getContextPath()).append(getPage(menu.getPage())).toString());
        }
        MenuComponent[] subMenus = menu.getMenuComponents();
        if (subMenus.length > 0) {
            for (MenuComponent pageLocation : subMenus) {
                setPageLocation(pageLocation);
            }
        }
    }

    private String getPage(String page) {
        if (page.startsWith("/")) {
            return page;
        }
        return new StringBuffer().append("/").append(page).toString();
    }

    public void release() {
        this.name = null;
        this.target = null;
    }
}
