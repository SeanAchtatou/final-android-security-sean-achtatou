package com.kenfor.coolmenu.menu.displayer;

import com.kenfor.coolmenu.menu.MenuComponent;
import java.io.IOException;
import java.text.MessageFormat;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

public class CoolMenuDisplayer4 extends MessageResourcesMenuDisplayer {
    private static final String END_STATEMENT = "\n\noCMenu.construct();\n";
    private static final String SCRIPT_END = "//-->\n</script>\n";
    private static final String SCRIPT_START = "\n<script type=\"text/javascript\">\n<!--\n";
    private static final String SUB_IMAGE = "cmSubMenuImage";
    private static final String TOP_IMAGE = "cmTopMenuImage";
    private static MessageFormat menuMessage = new MessageFormat("oCMenu.makeMenu(''{0}'',''{1}'',''{2}'',''{3}'',''{4}'','''','''','''','''','''','''','''','''',0,''{5}'',''{6}'',''{7}'');");

    public void init(PageContext context, MenuDisplayerMapping mapping) {
        super.init(context, mapping);
        try {
            this.out.print(SCRIPT_START);
        } catch (Exception e) {
        }
    }

    public void display(MenuComponent menu) throws JspException, IOException {
        StringBuffer sb = new StringBuffer();
        buildMenuString(menu, sb, isAllowed(menu));
        this.out.print(new StringBuffer().append("\n\t").append((Object) sb).toString());
    }

    public void end(PageContext context) {
        try {
            this.out.print(END_STATEMENT);
            this.out.print(SCRIPT_END);
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void buildMenuString(MenuComponent menu, StringBuffer sb, boolean allowed) {
        boolean z;
        if (allowed) {
            sb.append(new StringBuffer().append(menuMessage.format(getArgs(menu))).append("\n").toString());
            MenuComponent[] subMenus = menu.getMenuComponents();
            if (subMenus.length > 0) {
                for (int i = 0; i < subMenus.length; i++) {
                    MenuComponent menuComponent = subMenus[i];
                    if (allowed) {
                        z = isAllowed(subMenus[i]);
                    } else {
                        z = allowed;
                    }
                    buildMenuString(menuComponent, sb, z);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public String[] getArgs(MenuComponent menu) {
        String[] args = new String[8];
        args[0] = menu.getName();
        args[1] = getParentName(menu);
        args[2] = getTitle(menu);
        args[3] = menu.getLocation() == null ? "" : menu.getLocation();
        args[4] = getTarget(menu);
        args[5] = menu.getOnClick() == null ? "" : menu.getOnClick();
        args[6] = menu.getOnMouseOver() == null ? "" : menu.getOnMouseOver();
        args[7] = menu.getOnMouseOut() == null ? "" : menu.getOnMouseOut();
        return args;
    }

    /* access modifiers changed from: protected */
    public String getTitle(MenuComponent menu) {
        String title = getMessage(menu.getTitle());
        if (menu.getMenuComponents().length <= 0) {
            return title;
        }
        if (menu.getParent() == null) {
            return new StringBuffer().append(title).append("'+").append(TOP_IMAGE).append("+'").toString();
        }
        return new StringBuffer().append(title).append("'+").append(SUB_IMAGE).append("+'").toString();
    }

    /* access modifiers changed from: protected */
    public String getParentName(MenuComponent menu) {
        if (menu.getParent() == null) {
            return "";
        }
        return menu.getParent().getName();
    }

    /* access modifiers changed from: protected */
    public String getTarget(MenuComponent menu) {
        String theTarget = super.getTarget(menu);
        if (this.target == null) {
            return "";
        }
        return theTarget;
    }
}
