package com.kenfor.coolmenu.menu;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import javax.servlet.ServletException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionServlet;
import org.apache.struts.action.PlugIn;
import org.apache.struts.config.ModuleConfig;

public class MenuPlugIn implements PlugIn {
    private Log log = LogFactory.getLog(getClass().getName());
    private String menuConfig;
    private MenuRepository repository;
    private ActionServlet servlet;

    public String getMenuConfig() {
        return this.menuConfig;
    }

    public void setMenuConfig(String menuConfig2) {
        this.menuConfig = menuConfig2;
    }

    public void init(ActionServlet servlet2, ModuleConfig config) throws ServletException {
        if (this.log.isDebugEnabled()) {
            this.log.debug("Starting struts-menu initialization");
        }
        try {
            new DataOutputStream(new BufferedOutputStream(new FileOutputStream("menuPlugIn.log"))).writeChars(this.repository.getMenu());
        } catch (Exception e) {
        }
    }

    public void destroy() {
        this.repository = null;
        this.servlet.getServletContext().removeAttribute(MenuRepository.MENU_REPOSITORY_KEY);
        this.menuConfig = null;
        this.servlet = null;
    }
}
