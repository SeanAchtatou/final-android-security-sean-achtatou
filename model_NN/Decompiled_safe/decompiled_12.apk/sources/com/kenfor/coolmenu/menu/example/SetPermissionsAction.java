package com.kenfor.coolmenu.menu.example;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public final class SetPermissionsAction extends Action {
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        request.getSession().setAttribute("exampleAdapter", new SimplePermissionsAdapter(request.getParameterValues("menus")));
        request.getSession().setAttribute("displayer", request.getParameter("displayer"));
        return mapping.findForward("success");
    }
}
