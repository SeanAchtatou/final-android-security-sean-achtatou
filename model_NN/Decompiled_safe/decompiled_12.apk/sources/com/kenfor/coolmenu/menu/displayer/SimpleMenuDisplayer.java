package com.kenfor.coolmenu.menu.displayer;

import com.kenfor.coolmenu.menu.MenuComponent;
import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

public class SimpleMenuDisplayer extends MessageResourcesMenuDisplayer {
    protected static final String nbsp = "&nbsp;";

    public void init(PageContext pageContext, MenuDisplayerMapping mapping) {
        super.init(pageContext, mapping);
        try {
            this.out.println(this.displayStrings.getMessage("smd.style"));
        } catch (Exception e) {
        }
    }

    public void display(MenuComponent menu) throws JspException, IOException {
        this.out.println(this.displayStrings.getMessage("smd.menu.top"));
        displayComponents(menu, 0);
        this.out.println(this.displayStrings.getMessage("smd.menu.bottom"));
    }

    /* access modifiers changed from: protected */
    public void displayComponents(MenuComponent menu, int level) throws JspException, IOException {
        String message = super.getMessage(menu.getTitle());
        MenuComponent[] components = menu.getMenuComponents();
        if (components.length > 0) {
            this.out.println(this.displayStrings.getMessage("smd.menu.item.top", new StringBuffer().append(getSpace(level)).append(this.displayStrings.getMessage("smd.menu.item.image.bullet")).append(getMessage(menu.getTitle())).toString()));
            for (int i = 0; i < components.length; i++) {
                if (components[i].getMenuComponents().length > 0) {
                    displayComponents(components[i], level + 1);
                } else {
                    this.out.println(this.displayStrings.getMessage("smd.menu.item", components[i].getLocation(), super.getMenuTarget(components[i]), super.getMenuToolTip(components[i]), new StringBuffer().append(getSpace(level + 1)).append(getImage(components[i])).append(getMessage(components[i].getTitle())).toString()));
                }
            }
            return;
        }
        this.out.println(this.displayStrings.getMessage("smd.menu.item", menu.getLocation(), super.getMenuTarget(menu), super.getMenuToolTip(menu), new StringBuffer().append(getSpace(level)).append(getImage(menu)).append(getMessage(menu.getTitle())).toString()));
    }

    /* access modifiers changed from: protected */
    public String getSpace(int length) {
        String space = "";
        for (int i = 0; i < length; i++) {
            space = new StringBuffer().append(space).append("&nbsp;").append("&nbsp;").toString();
        }
        return space;
    }

    /* access modifiers changed from: protected */
    public String getImage(MenuComponent menu) {
        if (menu.getImage() == null || menu.getImage() == "") {
            return "";
        }
        return this.displayStrings.getMessage("smd.menu.item.image", menu.getImage(), super.getMenuToolTip(menu));
    }
}
