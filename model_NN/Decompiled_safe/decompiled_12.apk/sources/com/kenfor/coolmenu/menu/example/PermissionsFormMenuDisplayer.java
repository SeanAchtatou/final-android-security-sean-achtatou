package com.kenfor.coolmenu.menu.example;

import com.kenfor.coolmenu.menu.MenuComponent;
import com.kenfor.coolmenu.menu.displayer.SimpleMenuDisplayer;
import java.io.IOException;
import java.text.MessageFormat;
import javax.servlet.jsp.JspException;

public class PermissionsFormMenuDisplayer extends SimpleMenuDisplayer {
    private static final MessageFormat inputMessage = new MessageFormat("<input type=\"checkbox\" name=\"menus\" value=\"{0}\"/>");
    private static final MessageFormat itemMessage = new MessageFormat("<tr><td class=\"smd-menu-item\">{0} {1} {2} {3}</td></tr>");

    /* access modifiers changed from: protected */
    public void displayComponents(MenuComponent menu, int level) throws JspException, IOException {
        String message = getMessage(menu.getTitle());
        MenuComponent[] components = menu.getMenuComponents();
        if (components.length > 0) {
            this.out.println(this.displayStrings.getMessage("smd.menu.item.top", new StringBuffer().append(getSpace(level)).append(this.displayStrings.getMessage("smd.menu.item.image.bullet")).append(getMenuInput(menu)).append(getMessage(menu.getTitle())).toString()));
            for (int i = 0; i < components.length; i++) {
                if (components[i].getMenuComponents().length > 0) {
                    displayComponents(components[i], level + 1);
                } else {
                    this.out.println(getMenuItem(components[i], getSpace(level + 1)));
                }
            }
            return;
        }
        this.out.println(getMenuItem(menu, ""));
    }

    private String getMenuItem(MenuComponent menu, String space) {
        return itemMessage.format(new String[]{space, getMenuInput(menu), getImage(menu), getMessage(menu.getTitle())});
    }

    private String getMenuInput(MenuComponent menu) {
        return inputMessage.format(new String[]{menu.getName()});
    }
}
