package com.kenfor.coolmenu.menu.displayer;

import com.kenfor.coolmenu.menu.MenuComponent;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import org.apache.commons.lang.StringUtils;

public class ListMenuDisplayer extends MessageResourcesMenuDisplayer {
    private Map jsMap;

    public void init(PageContext pageContext, MenuDisplayerMapping mapping) {
        super.init(pageContext, mapping);
        this.jsMap = new HashMap();
        try {
            this.out.println(this.displayStrings.getMessage("lmd.begin"));
        } catch (Exception e) {
        }
    }

    public void display(MenuComponent menu) throws JspException, IOException {
        if (isAllowed(menu)) {
            this.out.println(this.displayStrings.getMessage("lmd.menu.top"));
            displayComponents(menu, 0);
            this.out.println(this.displayStrings.getMessage("lmd.menu.bottom"));
        }
    }

    /* access modifiers changed from: protected */
    public void displayComponents(MenuComponent menu, int level) throws JspException, IOException {
        String message = super.getMessage(menu.getTitle());
        MenuComponent[] components = menu.getMenuComponents();
        if (components.length > 0) {
            String domId = StringUtils.deleteSpaces(getMessage(menu.getTitle()));
            if (this.jsMap.containsKey(new StringBuffer().append(domId).append("Menu").toString())) {
                domId = new StringBuffer().append(domId).append(Math.round(Math.random() * 100.0d)).toString();
            }
            String menuClass = "menu";
            if (level >= 1) {
                menuClass = "submenu";
            }
            this.out.println(this.displayStrings.getMessage("lmd.menu.actuator.top", domId, getMessage(menu.getTitle()), menuClass));
            this.jsMap.put(new StringBuffer().append(domId).append("Menu").toString(), new StringBuffer().append(domId).append("Actuator").toString());
            for (int i = 0; i < components.length; i++) {
                if (isAllowed(components[i])) {
                    if (components[i].getMenuComponents().length > 0) {
                        if (menuClass.equals("menu")) {
                            this.out.println("<li>");
                        }
                        displayComponents(components[i], level + 1);
                        this.out.println(this.displayStrings.getMessage("lmd.menu.actuator.bottom"));
                        if (i == components[i].getMenuComponents().length - 1) {
                            this.out.println("</li>");
                        }
                    } else {
                        this.out.println(this.displayStrings.getMessage("lmd.menu.item", components[i].getLocation(), super.getMenuToolTip(components[i]), getMessage(components[i].getTitle())));
                    }
                }
            }
            if (menuClass.equals("menu")) {
                this.out.println("</ul>");
                return;
            }
            return;
        }
        this.out.println(this.displayStrings.getMessage("lmd.menu.item", menu.getLocation(), super.getMenuToolTip(menu), getMessage(menu.getTitle())));
    }

    public void end(PageContext context) {
        try {
            this.out.print(this.displayStrings.getMessage("lmd.end"));
            this.out.print(this.displayStrings.getMessage("lmd.js.start"));
            for (Object obj : this.jsMap.keySet()) {
                String id = obj.toString();
                this.out.print(this.displayStrings.getMessage("lmd.js", id, this.jsMap.get(id)));
            }
            this.out.print(this.displayStrings.getMessage("lmd.js.stop"));
        } catch (Exception e) {
        }
    }
}
