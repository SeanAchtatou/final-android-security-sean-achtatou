package com.kenfor.coolmenu.menu;

public interface PermissionsAdapter {
    boolean isAllowed(MenuComponent menuComponent);
}
