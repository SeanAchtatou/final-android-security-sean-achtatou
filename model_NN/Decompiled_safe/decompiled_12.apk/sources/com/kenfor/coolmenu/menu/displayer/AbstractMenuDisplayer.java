package com.kenfor.coolmenu.menu.displayer;

import com.kenfor.coolmenu.menu.MenuComponent;
import com.kenfor.coolmenu.menu.PermissionsAdapter;
import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import org.apache.struts.util.MessageResources;

public abstract class AbstractMenuDisplayer implements MenuDisplayer {
    protected MessageResources displayStrings;
    protected MenuDisplayerMapping mapping;
    protected String name;
    protected JspWriter out;
    protected PermissionsAdapter permissionsAdapter;
    protected String target;

    public abstract void display(MenuComponent menuComponent) throws JspException, IOException;

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getConfig() {
        if (this.displayStrings != null) {
            return this.displayStrings.getConfig();
        }
        return null;
    }

    public void setConfig(String config) {
        this.displayStrings = MessageResources.getMessageResources(config);
    }

    public void setConfig(MessageResources displayStrings2) {
        this.displayStrings = displayStrings2;
    }

    public String getTarget() {
        return this.target;
    }

    /* access modifiers changed from: protected */
    public String getTarget(MenuComponent menu) {
        if (this.target != null) {
            return this.target;
        }
        if (menu.getTarget() != null) {
            return menu.getTarget();
        }
        return null;
    }

    public void setTarget(String target2) {
        this.target = target2;
    }

    public PermissionsAdapter getPermissionsAdapter() {
        return this.permissionsAdapter;
    }

    public void setPermissionsAdapter(PermissionsAdapter permissionsAdapter2) {
        this.permissionsAdapter = permissionsAdapter2;
    }

    public void init(PageContext pageContext, MenuDisplayerMapping mapping2) {
        this.out = pageContext.getOut();
        this.mapping = mapping2;
    }

    public void end(PageContext pageContext) {
    }

    /* access modifiers changed from: protected */
    public boolean isAllowed(MenuComponent menu) {
        if (this.permissionsAdapter == null) {
            return true;
        }
        return this.permissionsAdapter.isAllowed(menu);
    }
}
