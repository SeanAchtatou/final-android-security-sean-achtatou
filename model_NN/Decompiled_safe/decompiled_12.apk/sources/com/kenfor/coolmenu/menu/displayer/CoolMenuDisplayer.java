package com.kenfor.coolmenu.menu.displayer;

import com.kenfor.coolmenu.menu.MenuComponent;
import java.io.IOException;
import java.text.MessageFormat;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

public class CoolMenuDisplayer extends MessageResourcesMenuDisplayer {
    private static final String BGCOL_OFF = "cmBGColorOff";
    private static final String BGCOL_ON = "cmBGColorOn";
    private static final String DIS_BGCOL_OFF = "cmDisBGColorOff";
    private static final String DIS_BGCOL_ON = "cmDisBGColorOn";
    private static final String DIS_HOVER = "cmDisHoverColor";
    private static final String DIS_TXTCOL = "cmDisTxtColor";
    private static final String END_STATEMENT = "\noCMenu.makeStyle(); oCMenu.construct()\n";
    private static final String HOVER = "cmHoverColor";
    private static final String SCRIPT_END = "</script>\n";
    private static final String SCRIPT_START = "<script language=\"javascript\">\n";
    private static final String SUB_IMAGE = "cmSubMenuImage";
    private static final String TOP_IMAGE = "cmTopMenuImage";
    private static final String TXTCOL = "cmTxtColor";
    private static MessageFormat menuMessage = new MessageFormat("oCMenu.makeMenu(''{0}'',''{1}'',''{2}'',''{3}'',''{4}'','''','''',''{5}'',''{6}'',{7},{8},{9},");

    public void init(PageContext context, MenuDisplayerMapping mapping) {
        super.init(context, mapping);
        try {
            this.out.print(SCRIPT_START);
        } catch (Exception e) {
        }
    }

    public void display(MenuComponent menu) throws JspException, IOException {
        StringBuffer sb = new StringBuffer();
        buildMenuString(menu, sb, isAllowed(menu));
        this.out.print(sb);
    }

    public void end(PageContext context) {
        try {
            this.out.print(END_STATEMENT);
            this.out.print(SCRIPT_END);
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void buildMenuString(MenuComponent menu, StringBuffer sb, boolean allowed) {
        boolean z;
        sb.append(menuMessage.format(getArgs(menu, allowed)));
        sb.append(allowed ? HOVER : DIS_HOVER);
        sb.append(",'");
        sb.append(menu.getOnClick() == null ? "" : menu.getOnClick());
        sb.append("')\n");
        MenuComponent[] subMenus = menu.getMenuComponents();
        if (subMenus.length > 0) {
            for (int i = 0; i < subMenus.length; i++) {
                MenuComponent menuComponent = subMenus[i];
                if (allowed) {
                    z = isAllowed(subMenus[i]);
                } else {
                    z = allowed;
                }
                buildMenuString(menuComponent, sb, z);
            }
        }
    }

    /* access modifiers changed from: protected */
    public String[] getArgs(MenuComponent menu, boolean allowed) {
        String[] args = new String[10];
        args[0] = menu.getName();
        args[1] = getParentName(menu);
        args[2] = getTitle(menu);
        args[3] = menu.getLocation() == null ? "" : allowed ? menu.getLocation() : "";
        args[4] = getTarget(menu);
        args[5] = "";
        args[6] = "";
        args[7] = allowed ? BGCOL_OFF : DIS_BGCOL_OFF;
        args[8] = allowed ? BGCOL_ON : DIS_BGCOL_ON;
        args[9] = allowed ? TXTCOL : DIS_TXTCOL;
        return args;
    }

    /* access modifiers changed from: protected */
    public String getTitle(MenuComponent menu) {
        String title = getMessage(menu.getTitle());
        if (menu.getMenuComponents().length <= 0) {
            return title;
        }
        if (menu.getParent() == null) {
            return new StringBuffer().append(title).append("'+").append(TOP_IMAGE).append("+'").toString();
        }
        return new StringBuffer().append(title).append("'+").append(SUB_IMAGE).append("+'").toString();
    }

    /* access modifiers changed from: protected */
    public String getParentName(MenuComponent menu) {
        if (menu.getParent() == null) {
            return "";
        }
        return menu.getParent().getName();
    }

    /* access modifiers changed from: protected */
    public String getTarget(MenuComponent menu) {
        String theTarget = super.getTarget(menu);
        if (this.target == null) {
            return "";
        }
        return theTarget;
    }
}
