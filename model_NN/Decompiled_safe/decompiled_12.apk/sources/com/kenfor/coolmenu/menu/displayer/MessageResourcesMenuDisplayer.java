package com.kenfor.coolmenu.menu.displayer;

import com.kenfor.coolmenu.menu.MenuComponent;
import java.io.IOException;
import java.util.Locale;
import javax.servlet.jsp.JspException;
import org.apache.struts.util.MessageResources;

public abstract class MessageResourcesMenuDisplayer extends AbstractMenuDisplayer {
    protected Locale locale = null;
    protected MessageResources messages = null;

    public abstract void display(MenuComponent menuComponent) throws JspException, IOException;

    public Locale getLocale() {
        return this.locale;
    }

    public void setLocale(Locale locale2) {
        this.locale = locale2;
    }

    public MessageResources getMessageResources() {
        return this.messages;
    }

    public void setMessageResources(MessageResources messages2) {
        this.messages = messages2;
    }

    /* access modifiers changed from: protected */
    public String getMessage(String key) {
        String message;
        if (this.messages != null) {
            try {
                if (this.locale != null) {
                    message = this.messages.getMessage(this.locale, key);
                } else {
                    message = this.messages.getMessage(key);
                }
            } catch (Throwable th) {
                message = null;
            }
        } else {
            message = key;
        }
        if (message == null) {
            return key;
        }
        return message;
    }

    /* access modifiers changed from: protected */
    public String getMenuTarget(MenuComponent menu) {
        String str = this.target;
        if (this.target != null) {
            return this.target;
        }
        if (menu.getTarget() != null) {
            return menu.getTarget();
        }
        return MenuDisplayer._SELF;
    }

    /* access modifiers changed from: protected */
    public String getMenuToolTip(MenuComponent menu) {
        if (menu.getToolTip() != null) {
            return getMessage(menu.getToolTip());
        }
        return getMessage(menu.getTitle());
    }
}
