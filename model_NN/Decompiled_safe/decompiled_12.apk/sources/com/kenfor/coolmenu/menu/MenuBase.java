package com.kenfor.coolmenu.menu;

public abstract class MenuBase {
    private String altImage;
    protected String description;
    private String image;
    protected String location;
    protected String name;
    protected String onClick;
    protected String onMouseOut;
    protected String onMouseOver;
    private String page;
    private String roles;
    protected String target;
    protected String title;
    private String toolTip;

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public String getLocation() {
        return this.location;
    }

    public void setLocation(String location2) {
        this.location = location2;
    }

    public String getTarget() {
        return this.target;
    }

    public void setTarget(String target2) {
        this.target = target2;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description2) {
        this.description = description2;
    }

    public String getOnClick() {
        return this.onClick;
    }

    public void setOnClick(String onClick2) {
        this.onClick = onClick2;
    }

    public String getOnMouseOver() {
        return this.onMouseOver;
    }

    public void setOnMouseOver(String onMouseOver2) {
        this.onMouseOver = onMouseOver2;
    }

    public String getOnMouseOut() {
        return this.onMouseOut;
    }

    public void setOnMouseOut(String onMouseOut2) {
        this.onMouseOut = onMouseOut2;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image2) {
        this.image = image2;
    }

    public String getAltImage() {
        return this.altImage;
    }

    public void setAltImage(String altImage2) {
        this.altImage = altImage2;
    }

    public String getToolTip() {
        return this.toolTip;
    }

    public void setToolTip(String toolTip2) {
        this.toolTip = toolTip2;
    }

    public String getRoles() {
        return this.roles;
    }

    public void setRoles(String roles2) {
        this.roles = roles2;
    }

    public String getPage() {
        return this.page;
    }

    public void setPage(String page2) {
        this.page = page2;
    }
}
