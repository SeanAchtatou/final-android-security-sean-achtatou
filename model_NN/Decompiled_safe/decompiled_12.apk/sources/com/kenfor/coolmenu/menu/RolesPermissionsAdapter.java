package com.kenfor.coolmenu.menu;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;

public class RolesPermissionsAdapter implements PermissionsAdapter {
    private HttpServletRequest request;

    public RolesPermissionsAdapter(HttpServletRequest request2) {
        this.request = request2;
    }

    public boolean isAllowed(MenuComponent menu) {
        if (menu.getRoles() == null) {
            return true;
        }
        String[] allowedRoles = StringUtils.split(menu.getRoles());
        for (String isUserInRole : allowedRoles) {
            if (this.request.isUserInRole(isUserInRole)) {
                return true;
            }
        }
        return false;
    }
}
