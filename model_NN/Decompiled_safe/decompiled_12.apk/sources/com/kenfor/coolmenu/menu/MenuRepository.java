package com.kenfor.coolmenu.menu;

import com.kenfor.coolmenu.menu.displayer.MenuDisplayerMapping;
import com.kenfor.coolmenu.resources.LoadableResource;
import com.kenfor.coolmenu.resources.LoadableResourceException;
import com.kenfor.database.DatabaseInfo;
import com.kenfor.database.InitDatabase;
import com.kenfor.util.ProDebug;
import java.awt.List;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Set;
import javax.servlet.http.HttpServlet;
import javax.servlet.jsp.PageContext;
import org.apache.commons.collections.FastHashMap;

public class MenuRepository implements LoadableResource {
    public static final String MENU_REPOSITORY_KEY = "com.kenfor.coolmenu.menu.MENU_REPOSITORY";
    protected Connection con = null;
    protected String config = null;
    protected FastHashMap displayers = new FastHashMap();
    protected int layer = 0;
    private List list = new List();
    protected FastHashMap menus = new FastHashMap();
    private ProDebug myDebug = new ProDebug();
    protected String name = null;
    protected PageContext pageContext = null;
    protected HttpServlet servlet = null;
    protected Statement stmt = null;
    protected String tableName = "BAS_MENU";

    public void setTableName(String table_name) {
        this.tableName = table_name;
    }

    public String getTableName() {
        return this.tableName;
    }

    public Set getMenuNames() {
        return this.menus.keySet();
    }

    public MenuComponent getMenu(String menuName) throws Exception {
        try {
            return (MenuComponent) this.menus.get(menuName);
        } catch (Exception e) {
            throw new Exception(new StringBuffer().append("get menu error : ").append(e.getMessage()).toString());
        }
    }

    public int getMenuIndex(String menuName) {
        int rIndex = 0;
        boolean getit = false;
        while (!getit && rIndex < this.list.getItemCount()) {
            if (this.list.getItem(rIndex).trim().compareTo(menuName) == 0) {
                getit = true;
            }
            if (!getit) {
                rIndex++;
            }
        }
        if (!getit) {
            return 0;
        }
        return rIndex;
    }

    public List getMenuList() {
        return this.list;
    }

    public MenuComponent getMenu(int menuIndex) throws Exception {
        try {
            return (MenuComponent) this.menus.get(this.list.getItem(menuIndex));
        } catch (Exception e) {
            throw new Exception(new StringBuffer().append("get menu error : ").append(e.getMessage()).toString());
        }
    }

    public String getMenu() {
        return this.menus.toString();
    }

    public MenuDisplayerMapping getMenuDisplayerMapping(String displayerName) {
        MenuDisplayerMapping mapping = new MenuDisplayerMapping();
        mapping.setName("CoolMenu");
        mapping.setType("com.fgm.web.menu.displayer.CoolMenuDisplayer");
        return mapping;
    }

    public void addMenu(MenuComponent menu) {
        this.list.add(menu.getName());
        this.menus.put(menu.getName().trim(), menu);
    }

    public void addMenuDisplayerMapping(MenuDisplayerMapping displayerMapping) {
        this.displayers.put(displayerMapping.getName(), displayerMapping);
    }

    public void InitMenu(DatabaseInfo dbInfo) {
        boolean bEof;
        boolean bEof2;
        ResultSet rs = null;
        try {
            Class.forName(dbInfo.getDriver());
        } catch (Exception e) {
        }
        try {
            this.con = DriverManager.getConnection(dbInfo.getUrl(), dbInfo.getUsername(), dbInfo.getPassword());
            this.stmt = this.con.createStatement(1004, 1007);
            String sql = new StringBuffer().append("select * from ").append(this.tableName).append(" where PARENT_ID=0 order by BAS_ID").toString();
            ProDebug.addDebugLog(new StringBuffer().append("sql at menuRepository:").append(sql).toString());
            ProDebug.saveToFile();
            rs = this.stmt.executeQuery(sql);
        } catch (SQLException e2) {
            ProDebug proDebug = this.myDebug;
            ProDebug.addDebugLog(new StringBuffer().append("database error:\n ").append(e2.getMessage()).toString());
            ProDebug proDebug2 = this.myDebug;
            ProDebug.saveToFile();
        }
        try {
            rs.last();
            int parentRowCount = rs.getRow();
            rs.first();
            if (parentRowCount > 0) {
                bEof = true;
            } else {
                bEof = false;
            }
            int[] bas_id = new int[parentRowCount];
            String[] bas_name = new String[parentRowCount];
            int rownum = 0;
            while (bEof) {
                this.layer = 1;
                bas_id[rownum] = rs.getInt("BAS_ID");
                bas_name[rownum] = rs.getString("TITLE_CN").trim();
                MenuComponent menuCom = new MenuComponent();
                menuCom.setName(bas_name[rownum]);
                menuCom.setTitle(rs.getString("TITLE_CN").trim());
                menuCom.setLocation(rs.getString("LOCATION"));
                menuCom.setImage(rs.getString("IMAGE"));
                menuCom.setDescription(rs.getString("DESCRIPTION"));
                addMenu(menuCom);
                bEof = rs.next();
                rownum++;
            }
            rs.close();
            this.stmt.close();
            for (int i = 0; i < parentRowCount; i++) {
                Statement stmt2 = this.con.createStatement(1004, 1007);
                ResultSet rs2 = stmt2.executeQuery(new StringBuffer().append("select * from ").append(this.tableName).append(" where PARENT_ID=").append(String.valueOf(bas_id[i])).append(" order by BAS_ID").toString());
                rs2.last();
                int thisRowCount = rs2.getRow();
                rs2.first();
                if (thisRowCount > 0) {
                    bEof2 = true;
                } else {
                    bEof2 = false;
                }
                int[] bas_id2 = new int[thisRowCount];
                int thisRow = 0;
                while (bEof2) {
                    bas_id2[thisRow] = rs2.getInt("BAS_ID");
                    bEof2 = rs2.next();
                    thisRow++;
                }
                rs2.close();
                stmt2.close();
                if (thisRowCount > 0) {
                    showMenuTree(bas_id2, thisRowCount, getMenu(bas_name[i]));
                }
            }
        } catch (Exception e3) {
            ProDebug proDebug3 = this.myDebug;
            ProDebug.addDebugLog(new StringBuffer().append("init menu error:\n ").append(e3.getMessage()).toString());
            ProDebug proDebug4 = this.myDebug;
            ProDebug.saveToFile();
        }
    }

    public void showMenuTree(int[] bas_id, int rowCount, MenuComponent menuCom) throws Exception {
        boolean childEof;
        int j = 0;
        while (j < rowCount) {
            MenuComponent childMenuCom = new MenuComponent();
            try {
                Statement stmt_sub = this.con.createStatement(1004, 1007);
                ResultSet rs_sub = stmt_sub.executeQuery(new StringBuffer().append("select * from ").append(this.tableName).append(" where BAS_ID=").append(String.valueOf(bas_id[j])).append(" order by BAS_ID").toString());
                if (rs_sub.next()) {
                    childMenuCom.setName(rs_sub.getString("TITLE_CN").trim());
                    childMenuCom.setTitle(rs_sub.getString("TITLE_CN").trim());
                    childMenuCom.setDescription(rs_sub.getString("DESCRIPTION"));
                    childMenuCom.setImage(rs_sub.getString("IMAGE"));
                    childMenuCom.setLocation(rs_sub.getString("LOCATION"));
                }
                menuCom.addMenuComponent(childMenuCom);
                rs_sub.close();
                stmt_sub.close();
                ResultSet rs_sub2 = this.con.createStatement(1004, 1007).executeQuery(new StringBuffer().append("select * from ").append(this.tableName).append(" where PARENT_ID=").append(String.valueOf(bas_id[j])).append(" order by BAS_ID").toString());
                rs_sub2.last();
                int childCount = rs_sub2.getRow();
                rs_sub2.first();
                if (childCount > 0) {
                    childEof = true;
                } else {
                    childEof = false;
                }
                int childRowCount = 0;
                int[] bas_id2 = new int[childCount];
                while (childEof) {
                    bas_id2[childRowCount] = rs_sub2.getInt("BAS_ID");
                    childEof = rs_sub2.next();
                    childRowCount++;
                }
                rs_sub.close();
                stmt_sub.close();
                if (childCount > 0) {
                    showMenuTree(bas_id2, childCount, childMenuCom);
                }
                j++;
            } catch (Exception e) {
                throw new Exception(new StringBuffer().append("show menu tree error:\n").append(e.getMessage()).toString());
            }
        }
    }

    public void load() throws LoadableResourceException {
        String path;
        if (this.servlet == null && this.pageContext == null) {
            throw new LoadableResourceException("no reference to servlet or pageContext founf");
        }
        if (this.servlet == null) {
            path = this.pageContext.getServletContext().getRealPath("/WEB-INF/classes/");
        } else {
            path = this.servlet.getServletContext().getRealPath("/WEB-INF/classes/");
        }
        try {
            try {
                DatabaseInfo dbInfo = new InitDatabase(path).getDatabaseInfo();
                InitMenu(dbInfo);
                if (this.servlet == null) {
                    this.pageContext.getServletContext().setAttribute("database", dbInfo);
                } else {
                    this.servlet.getServletContext().setAttribute("database", dbInfo);
                }
                this.menus.setFast(true);
            } catch (Exception e) {
                e = e;
            } catch (Throwable th) {
                th = th;
                throw th;
            }
        } catch (Exception e2) {
            e = e2;
            try {
                e.printStackTrace();
                throw new LoadableResourceException(new StringBuffer().append("error in initDatabase or initMenu nested exception is: ").append(e.getMessage()).toString());
            } catch (Throwable th2) {
                th = th2;
                throw th;
            }
        }
    }

    public void reload() throws LoadableResourceException {
        this.menus.setFast(false);
        this.menus.clear();
        this.displayers.setFast(false);
        this.displayers.clear();
        load();
    }

    public void setLoadParam(String loadParam) {
        this.config = loadParam;
    }

    public String getLoadParam() {
        return this.config;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getName() {
        return this.name;
    }

    public HttpServlet getServlet() {
        return this.servlet;
    }

    public void setServlet(HttpServlet servlet2) {
        this.servlet = servlet2;
    }

    public PageContext getPageContext() {
        return this.pageContext;
    }

    public void setPageContext(PageContext pageContext2) {
        this.pageContext = pageContext2;
    }
}
