package com.kenfor.coolmenu.menu.displayer.taglib;

import com.kenfor.coolmenu.menu.MenuComponent;
import com.kenfor.coolmenu.menu.MenuRepository;
import com.kenfor.coolmenu.menu.PermissionsAdapter;
import com.kenfor.coolmenu.menu.RolesPermissionsAdapter;
import com.kenfor.coolmenu.menu.displayer.MenuDisplayer;
import com.kenfor.coolmenu.menu.displayer.MenuDisplayerMapping;
import com.kenfor.coolmenu.menu.displayer.MessageResourcesMenuDisplayer;
import com.kenfor.util.ProDebug;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import org.apache.struts.util.MessageResources;

public class UseMenuDisplayerTag extends TagSupport {
    public static final String DISPLAYER_KEY = "com.kenfor.coolmenu.menu.displayer.taglib.DISPLAYER";
    public static final String MENU_REPOSITORY_KEY = "com.kenfor.coolmenu.menu.displayer.taglibs.MENU_REPOSITORY";
    public static final String ROLES_ADAPTER = "rolesAdapter";
    protected static ResourceBundle messages = ResourceBundle.getBundle("com.kenfor.coolmenu.menu.displayer.taglib.LocalStrings");
    private String bundleKey;
    private String config = MenuDisplayer.DEFAULT_CONFIG;
    protected String localeKey;
    protected MenuDisplayer menuDisplayer = null;
    private String menuType = "com.kenfor.coolmenu.menu.displayer.CoolMenuDisplayer4";
    ProDebug myDebug = new ProDebug();
    protected String name;
    private String permissions;
    private String repository = MenuRepository.MENU_REPOSITORY_KEY;

    public String getBundle() {
        return this.bundleKey;
    }

    public void setBundle(String bundle) {
        this.bundleKey = bundle;
    }

    public String getMenutype() {
        return this.menuType;
    }

    public void setMenutype(String menutype) {
        this.menuType = menutype;
    }

    public String getConfig() {
        return this.config;
    }

    public void setConfig(String config2) {
        this.config = config2;
    }

    public String getLocale() {
        return this.localeKey;
    }

    public void setLocale(String locale) {
        this.localeKey = locale;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getRepository() {
        return this.repository;
    }

    public void setRepository(String repository2) {
        this.repository = repository2;
    }

    public String getPermissions() {
        return this.permissions;
    }

    public void setPermissions(String permissions2) {
        this.permissions = permissions2;
    }

    public int doStartTag() throws JspException {
        Locale locale;
        MenuRepository menuRep = new MenuRepository();
        menuRep.setTableName("bas_menu");
        menuRep.setPageContext(this.pageContext);
        try {
            menuRep.load();
            if (menuRep == null) {
                throw new JspException(messages.getString("menurepository.not.found"));
            }
            MenuDisplayerMapping displayerMapping = new MenuDisplayerMapping();
            displayerMapping.setName(this.menuType);
            displayerMapping.setType(this.menuType);
            if (displayerMapping == null) {
                throw new JspException(messages.getString("displayer.mapping.not.found"));
            }
            PermissionsAdapter permissions2 = getPermissionsAdapter();
            try {
                MenuDisplayer displayerInstance = (MenuDisplayer) Class.forName(this.menuType).newInstance();
                this.menuDisplayer = displayerInstance;
                if (this.bundleKey != null && (displayerInstance instanceof MessageResourcesMenuDisplayer)) {
                    MessageResourcesMenuDisplayer mrDisplayerInstance = (MessageResourcesMenuDisplayer) displayerInstance;
                    MessageResources resources = (MessageResources) this.pageContext.findAttribute(this.bundleKey);
                    if (this.localeKey == null) {
                        locale = this.pageContext.getRequest().getLocale();
                    } else {
                        locale = (Locale) this.pageContext.findAttribute(this.localeKey);
                    }
                    mrDisplayerInstance.setMessageResources(resources);
                    mrDisplayerInstance.setLocale(locale);
                }
                displayerInstance.setConfig(this.config);
                displayerInstance.init(this.pageContext, displayerMapping);
                displayerInstance.setPermissionsAdapter(permissions2);
                int menu_count = menuRep.getMenuList().getItemCount();
                int i = 0;
                while (i < menu_count) {
                    try {
                        MenuComponent menu = menuRep.getMenu(i);
                        if (menu != null) {
                            try {
                                setPageLocation(menu);
                                displayerInstance.display(menu);
                                displayerInstance.setTarget(null);
                            } catch (Exception e) {
                            }
                        }
                        i++;
                    } catch (Exception e2) {
                        ProDebug proDebug = this.myDebug;
                        ProDebug.addDebugLog(new StringBuffer().append("get Menu error ").append(e2.getMessage()).toString());
                        ProDebug proDebug2 = this.myDebug;
                        ProDebug.saveToFile();
                        throw new JspException(new StringBuffer().append("get menu error: ").append(e2.getMessage()).toString());
                    }
                }
                return 1;
            } catch (Exception e3) {
                throw new JspException(e3.getMessage());
            }
        } catch (Exception lre) {
            ProDebug proDebug3 = this.myDebug;
            ProDebug.addDebugLog(new StringBuffer().append("Failure initializing struts-menu: ").append(lre.getMessage()).toString());
            ProDebug proDebug4 = this.myDebug;
            ProDebug.saveToFile();
            throw new JspException(new StringBuffer().append("Failure initializing struts-menu: ").append(lre.getMessage()).toString());
        }
    }

    /* access modifiers changed from: protected */
    public void setPageLocation(MenuComponent menu) {
        if (menu.getLocation() == null && menu.getPage() != null) {
            menu.setLocation(new StringBuffer().append(this.pageContext.getRequest().getContextPath()).append(getPage(menu.getPage())).toString());
        }
        MenuComponent[] subMenus = menu.getMenuComponents();
        if (subMenus.length > 0) {
            for (MenuComponent pageLocation : subMenus) {
                setPageLocation(pageLocation);
            }
        }
    }

    private String getPage(String page) {
        if (page.startsWith("/")) {
            return page;
        }
        return new StringBuffer().append("/").append(page).toString();
    }

    /* access modifiers changed from: protected */
    public PermissionsAdapter getPermissionsAdapter() throws JspException {
        if (this.permissions == null) {
            return null;
        }
        if (this.permissions.equalsIgnoreCase(ROLES_ADAPTER)) {
            return new RolesPermissionsAdapter(this.pageContext.getRequest());
        }
        PermissionsAdapter adapter = (PermissionsAdapter) this.pageContext.findAttribute(this.permissions);
        if (adapter != null) {
            return adapter;
        }
        throw new JspException(messages.getString("permissions.not.found"));
    }

    public int doEndTag() throws JspException {
        this.menuDisplayer.end(this.pageContext);
        return 6;
    }

    public void release() {
        this.bundleKey = null;
        this.config = MenuDisplayer.DEFAULT_CONFIG;
        this.localeKey = null;
        this.name = null;
        this.repository = MenuRepository.MENU_REPOSITORY_KEY;
        this.permissions = null;
    }
}
