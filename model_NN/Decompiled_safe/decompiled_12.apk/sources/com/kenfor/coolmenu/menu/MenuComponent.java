package com.kenfor.coolmenu.menu;

import java.util.ArrayList;

public class MenuComponent extends MenuBase {
    protected static MenuComponent[] _menuComponent = new MenuComponent[0];
    protected ArrayList menuComponents = new ArrayList();
    protected MenuComponent parentMenu = null;

    public void addMenuComponent(MenuComponent menuComponent) {
        this.menuComponents.add(menuComponent);
        menuComponent.setParent(this);
        if (menuComponent.getName() == null || menuComponent.getName().equals("")) {
            menuComponent.setName(new StringBuffer().append(this.name).append(this.menuComponents.size()).toString());
        }
    }

    public MenuComponent[] getMenuComponents() {
        return (MenuComponent[]) this.menuComponents.toArray(_menuComponent);
    }

    public void setParent(MenuComponent parentMenu2) {
        this.parentMenu = parentMenu2;
    }

    public MenuComponent getParent() {
        return this.parentMenu;
    }
}
