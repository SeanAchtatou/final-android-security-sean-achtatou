package com.kenfor.coolmenu.menu.displayer;

import com.kenfor.coolmenu.menu.MenuComponent;
import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

public class DropDownMenuDisplayer extends MessageResourcesMenuDisplayer {
    public void init(PageContext pageContext, MenuDisplayerMapping mapping) {
        super.init(pageContext, mapping);
        StringBuffer sb = new StringBuffer();
        sb.append(this.displayStrings.getMessage("dd.js.start"));
        sb.append(this.displayStrings.getMessage("dd.js.image.src.expand", this.displayStrings.getMessage("dd.image.src.expand")));
        sb.append(this.displayStrings.getMessage("dd.js.image.src.expanded", this.displayStrings.getMessage("dd.image.src.expanded")));
        sb.append(this.displayStrings.getMessage("dd.js.toggle.display"));
        sb.append(this.displayStrings.getMessage("dd.js.end"));
        try {
            this.out.print(sb.toString());
        } catch (Exception e) {
        }
    }

    public void display(MenuComponent menu) throws JspException, IOException {
        String title = super.getMessage(menu.getTitle());
        StringBuffer sb = new StringBuffer();
        String img = "";
        if (menu.getImage() != null) {
            img = this.displayStrings.getMessage("dd.image", menu.getImage());
        }
        MenuComponent[] components = menu.getMenuComponents();
        sb.append(this.displayStrings.getMessage("dd.menu.top"));
        if (components.length > 0) {
            sb.append(this.displayStrings.getMessage("dd.menu.expander", menu.getName(), new StringBuffer().append(menu.getName()).append("_img").toString(), new StringBuffer().append(this.displayStrings.getMessage("dd.image.expander", new StringBuffer().append(menu.getName()).append("_img").toString(), this.displayStrings.getMessage("dd.image.src.expand"))).append(MenuDisplayer.NBSP).append(img).append(title).toString()));
            displayComponents(menu, sb);
        } else {
            sb.append(title);
        }
        sb.append(this.displayStrings.getMessage("dd.menu.bottom"));
        this.out.println(sb.toString());
    }

    private void displayComponents(MenuComponent menu, StringBuffer sb) throws JspException, IOException {
        String img;
        String name = menu.getName();
        MenuComponent[] components = menu.getMenuComponents();
        sb.append(this.displayStrings.getMessage("dd.menu.item.top", name));
        for (int i = 0; i < components.length; i++) {
            String title = super.getMessage(components[i].getTitle());
            if (components[i].getImage() != null) {
                img = this.displayStrings.getMessage("dd.image", components[i].getImage());
            } else {
                img = "";
            }
            String href = components[i].getLocation();
            sb.append(this.displayStrings.getMessage("dd.menu.item.row.start"));
            if (components[i].getMenuComponents().length > 0) {
                sb.append(this.displayStrings.getMessage("dd.menu.expander", components[i].getName(), new StringBuffer().append(components[i].getName()).append("_img").toString(), new StringBuffer().append(this.displayStrings.getMessage("dd.image.expander", new StringBuffer().append(components[i].getName()).append("_img").toString(), this.displayStrings.getMessage("dd.image.src.expand"))).append(MenuDisplayer.NBSP).append(img).append(title).toString()));
                displayComponents(components[i], sb);
            } else {
                sb.append(this.displayStrings.getMessage("dd.link.start", href, super.getMenuTarget(components[i]), super.getMenuToolTip(components[i])));
                sb.append(MenuDisplayer.NBSP);
                sb.append(MenuDisplayer.NBSP);
                sb.append(img);
                sb.append(title);
                sb.append(this.displayStrings.getMessage("dd.link.end"));
            }
            sb.append(this.displayStrings.getMessage("dd.menu.item.row.end"));
        }
        sb.append(this.displayStrings.getMessage("dd.menu.item.bottom"));
    }
}
