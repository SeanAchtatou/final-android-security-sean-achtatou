package com.kenfor.coolmenu.menu.displayer;

public class MenuDisplayerMapping {
    private String config;
    private String name;
    private String type;

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type2) {
        this.type = type2;
    }

    public String getConfig() {
        return this.config;
    }

    public void setConfig(String config2) {
        this.config = config2;
    }
}
