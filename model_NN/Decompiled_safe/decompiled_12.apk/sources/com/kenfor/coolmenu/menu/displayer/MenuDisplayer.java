package com.kenfor.coolmenu.menu.displayer;

import com.kenfor.coolmenu.menu.MenuComponent;
import com.kenfor.coolmenu.menu.PermissionsAdapter;
import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

public interface MenuDisplayer {
    public static final String DEFAULT_CONFIG = "com.fgm.web.menu.displayer.DisplayerStrings";
    public static final String EMPTY = "";
    public static final String NBSP = "&nbsp;";
    public static final String _SELF = "_self";

    void display(MenuComponent menuComponent) throws JspException, IOException;

    void end(PageContext pageContext);

    String getConfig();

    String getName();

    PermissionsAdapter getPermissionsAdapter();

    String getTarget();

    void init(PageContext pageContext, MenuDisplayerMapping menuDisplayerMapping);

    void setConfig(String str);

    void setName(String str);

    void setPermissionsAdapter(PermissionsAdapter permissionsAdapter);

    void setTarget(String str);
}
