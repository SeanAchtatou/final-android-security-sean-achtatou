package com.kenfor.tools.str;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.jivesoftware.smackx.packet.CapsExtension;

public class BuildRandomString {
    public static String buildRandomString(String inputString) {
        String[][] str = {new String[]{"0", "a"}, new String[]{"1", "b"}, new String[]{"2", CapsExtension.NODE_NAME}, new String[]{"3", "d"}, new String[]{"4", "e"}, new String[]{"5", "f"}, new String[]{"6", "g"}, new String[]{"7", "h"}, new String[]{"8", "i"}, new String[]{"9", "j"}};
        String jym = String.valueOf(inputString) + new SimpleDateFormat("mmssSSS").format(new Date()).substring(1);
        String jym_new = "";
        List<String> jym_list = new ArrayList<>();
        for (int i = 0; i < jym.length(); i++) {
            jym_list.add(jym.substring(i, i + 1));
        }
        while (jym_list.size() > 0) {
            int i2 = ((int) (1.0d / Math.random())) % jym_list.size();
            int random = ((int) (1.0d / Math.random())) % 2;
            if (isNumberic((String) jym_list.get(i2))) {
                jym_new = String.valueOf(jym_new) + str[Integer.parseInt((String) jym_list.get(i2))][random];
            } else {
                jym_new = String.valueOf(jym_new) + ((String) jym_list.get(i2)).toLowerCase();
            }
            jym_list.remove(i2);
        }
        return isContainNum(jym_new) ? jym_new : String.valueOf(jym_new.substring(1)) + (((int) Math.random()) * 10);
    }

    private static boolean isNumberic(String string) {
        try {
            Double.parseDouble(string);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean isContainNum(String string) {
        byte[] bytes = string.getBytes();
        int i = 0;
        while (i < bytes.length && (bytes[i] < 48 || bytes[i] > 57)) {
            i++;
        }
        if (i == bytes.length) {
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println(buildRandomString(null));
    }
}
