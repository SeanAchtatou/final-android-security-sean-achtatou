package com.kenfor.tools.str;

import java.util.LinkedList;
import java.util.List;

public class StringTool {
    public static int getLengthAsLetter(String str) {
        int len = 0;
        int strLength = str.length();
        for (int i = 0; i < strLength; i++) {
            int code = str.charAt(i);
            if (code <= 0 || code >= 257) {
                len += 2;
            } else {
                len++;
            }
        }
        return len;
    }

    public static List<String> uniqueList(String[] str) {
        List<String> list = new LinkedList<>();
        for (int i = 0; i < str.length; i++) {
            if (!list.contains(str[i])) {
                list.add(str[i]);
            }
        }
        return list;
    }

    public static String[] uniqueArray(String[] str) {
        List<String> list = new LinkedList<>();
        for (int i = 0; i < str.length; i++) {
            if (!list.contains(str[i])) {
                list.add(str[i]);
            }
        }
        return (String[]) list.toArray(new String[list.size()]);
    }

    public static long getIp_compare(String p_ip) {
        long j;
        long ip_compare = 0;
        String[] ips = p_ip.replace(".", ":").split(":");
        for (int i = 0; i < ips.length; i++) {
            switch (i) {
                case 0:
                    j = 16777216;
                    break;
                case 1:
                    j = 65536;
                    break;
                case 2:
                    j = 256;
                    break;
                default:
                    j = 1;
                    break;
            }
            ip_compare += ((long) Integer.parseInt(ips[i])) * j;
        }
        return ip_compare;
    }

    public static long ipToLong(String strIp) {
        int position1 = strIp.indexOf(".");
        int position2 = strIp.indexOf(".", position1 + 1);
        int position3 = strIp.indexOf(".", position2 + 1);
        long[] ip = {Long.parseLong(strIp.substring(0, position1)), Long.parseLong(strIp.substring(position1 + 1, position2)), Long.parseLong(strIp.substring(position2 + 1, position3)), Long.parseLong(strIp.substring(position3 + 1))};
        return (ip[0] << 24) + (ip[1] << 16) + (ip[2] << 8) + ip[3];
    }

    public static String longToIP(long longIp) {
        StringBuffer sb = new StringBuffer("");
        sb.append(String.valueOf(longIp >>> 24));
        sb.append(".");
        sb.append(String.valueOf((16777215 & longIp) >>> 16));
        sb.append(".");
        sb.append(String.valueOf((65535 & longIp) >>> 8));
        sb.append(".");
        sb.append(String.valueOf(255 & longIp));
        return sb.toString();
    }

    public static void main(String[] args) {
        long longIp = ipToLong("192.168.0.1");
        System.out.println("192.168.0.1 的整数形式为：" + getIp_compare("192.168.0.1"));
        System.out.println("192.168.0.1 的整数形式为：" + longIp);
        System.out.println("整数" + longIp + "转化成字符串IP地址：" + longToIP(getIp_compare("192.168.0.1")));
        System.out.println("192.168.0.1 的二进制形式为：" + Long.toBinaryString(longIp));
    }
}
