package com.kenfor.tools.excel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class ExcelCreator {
    final short encode = 1;

    public HSSFWorkbook CreateSimpleReportForm(String[][] title, String[] sheetName, Object[][][] records) throws ExcelCreatorException {
        HSSFWorkbook wb = new HSSFWorkbook();
        if (records.length < sheetName.length || title.length < sheetName.length) {
            throw new ExcelCreatorException();
        }
        for (int i = 0; i < sheetName.length; i++) {
            wb = createSheet(wb, sheetName[i], title[i], records[i]);
        }
        return wb;
    }

    public HSSFWorkbook createSheet(HSSFWorkbook wb, String sheetName, String[] title, Object[][] records) {
        HSSFSheet sheet = wb.createSheet();
        wb.setSheetName(0, sheetName);
        HSSFRow row = sheet.createRow(0);
        HSSFFont createFont = wb.createFont();
        HSSFCellStyle style = wb.createCellStyle();
        for (int i = 0; i < title.length; i++) {
            HSSFCell cell = row.createCell((short) i);
            sheet.setColumnWidth((short) i, 4500);
            style.setAlignment(2);
            HSSFFont titleFont = wb.createFont();
            titleFont.setFontHeightInPoints(12);
            titleFont.setFontName("黑体");
            titleFont.setBoldweight(700);
            style.setFont(titleFont);
            cell.setCellStyle(style);
            cell.setCellValue(title[i]);
        }
        for (int j = 0; j < records.length; j++) {
            System.out.println("j===" + j);
            HSSFRow row2 = sheet.createRow(((short) j) + 1);
            for (int i2 = 0; i2 < title.length; i2++) {
                HSSFCell cell2 = row2.createCell((short) i2);
                if (records[j][i2] != null) {
                    if (records[j][i2] instanceof Double) {
                        HSSFCellStyle cellStyle = wb.createCellStyle();
                        cellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.00"));
                        cell2.setCellValue(((Double) records[j][i2]).doubleValue());
                        cell2.setCellStyle(cellStyle);
                    } else if (records[j][i2] instanceof Calendar) {
                        cell2.setCellValue((Calendar) records[j][i2]);
                    } else if (records[j][i2] instanceof Boolean) {
                        cell2.setCellValue(((Boolean) records[j][i2]).booleanValue());
                    } else if (records[j][i2] instanceof Date) {
                        HSSFCellStyle cellStyle2 = wb.createCellStyle();
                        cellStyle2.setDataFormat(HSSFDataFormat.getBuiltinFormat("m/d/yy h:mm"));
                        cell2.setCellValue((Date) records[j][i2]);
                        cell2.setCellStyle(cellStyle2);
                    } else if (records[j][i2] instanceof Integer) {
                        cell2.setCellValue((double) ((Integer) records[j][i2]).intValue());
                    } else {
                        cell2.setCellValue(records[j][i2].toString());
                    }
                }
            }
        }
        return wb;
    }

    public HSSFWorkbook createSheet(String sheetName, ArrayList list) {
        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet();
        wb.setSheetName(0, sheetName);
        HSSFRow row = sheet.createRow(0);
        HSSFCellStyle style = wb.createCellStyle();
        HSSFFont f = wb.createFont();
        style.setFillBackgroundColor(10);
        for (int i = 0; i < list.size(); i++) {
            HSSFCell cell = row.createCell((short) i);
            cell.setCellValue(list.get(i).toString());
            f.setBoldweight(400);
            style.setFont(f);
            cell.setCellStyle(style);
        }
        return wb;
    }

    public HSSFWorkbook createRow(HSSFWorkbook wb, String sheetName, Object[][] records, int row_index) {
        HSSFCellStyle createCellStyle = wb.createCellStyle();
        HSSFSheet sheet = wb.getSheet(sheetName);
        for (int i = 0; i < records.length; i++) {
            HSSFRow row = sheet.createRow(row_index + i);
            for (int j = 0; j < records[i].length; j++) {
                HSSFCell cell = row.createCell((short) j);
                if (records[i][j] instanceof Double) {
                    HSSFCellStyle cellStyle = wb.createCellStyle();
                    cellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.00"));
                    cell.setCellValue(((Double) records[i][j]).doubleValue());
                    cell.setCellStyle(cellStyle);
                } else if (records[i][j] instanceof Calendar) {
                    cell.setCellValue((Calendar) records[i][j]);
                } else if (records[i][j] instanceof Boolean) {
                    cell.setCellValue(((Boolean) records[i][j]).booleanValue());
                } else if (records[i][j] instanceof Date) {
                    HSSFCellStyle cellStyle2 = wb.createCellStyle();
                    cellStyle2.setDataFormat(HSSFDataFormat.getBuiltinFormat("m/d/yy h:mm"));
                    cell.setCellValue((Date) records[i][j]);
                    cell.setCellStyle(cellStyle2);
                } else {
                    cell.setCellValue(records[i][j].toString().trim());
                }
            }
        }
        return wb;
    }

    class ExcelCreatorException extends Exception {
        ExcelCreatorException() {
        }
    }
}
