package com.kenfor.tools.excel;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

public abstract class HSSFHelper {
    public abstract Object oneExcelRowToObject(HSSFRow hSSFRow, String str, String str2) throws Exception;

    public abstract Object oneExcelRowToObjectValidate(HSSFRow hSSFRow) throws Exception;

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0037, code lost:
        r4 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0038, code lost:
        r6 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0075, code lost:
        r17 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0076, code lost:
        r6 = r7;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0075 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:3:0x000d] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0079 A[SYNTHETIC, Splitter:B:42:0x0079] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List coverSingleSheetExcelToList(java.lang.String r21, java.lang.String r22, java.lang.String r23) {
        /*
            r20 = this;
            java.util.ArrayList r11 = new java.util.ArrayList
            r11.<init>()
            r6 = 0
            java.io.FileInputStream r7 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0125 }
            r0 = r21
            r7.<init>(r0)     // Catch:{ Exception -> 0x0125 }
            org.apache.poi.poifs.filesystem.POIFSFileSystem r8 = new org.apache.poi.poifs.filesystem.POIFSFileSystem     // Catch:{ Exception -> 0x0037, all -> 0x0075 }
            r8.<init>(r7)     // Catch:{ Exception -> 0x0037, all -> 0x0075 }
            org.apache.poi.hssf.usermodel.HSSFWorkbook r16 = new org.apache.poi.hssf.usermodel.HSSFWorkbook     // Catch:{ Exception -> 0x0037, all -> 0x0075 }
            r0 = r16
            r0.<init>(r8)     // Catch:{ Exception -> 0x0037, all -> 0x0075 }
            r17 = 0
            org.apache.poi.hssf.usermodel.HSSFSheet r15 = r16.getSheetAt(r17)     // Catch:{ Exception -> 0x0037, all -> 0x0075 }
            int r14 = r15.getPhysicalNumberOfRows()     // Catch:{ Exception -> 0x0037, all -> 0x0075 }
            if (r14 != 0) goto L_0x004c
            if (r7 == 0) goto L_0x002a
            r7.close()     // Catch:{ IOException -> 0x0032 }
        L_0x002a:
            if (r7 == 0) goto L_0x002f
            r7.close()     // Catch:{ IOException -> 0x0047 }
        L_0x002f:
            r11 = 0
            r6 = r7
        L_0x0031:
            return r11
        L_0x0032:
            r3 = move-exception
            r3.printStackTrace()     // Catch:{ Exception -> 0x0037, all -> 0x0075 }
            goto L_0x002a
        L_0x0037:
            r4 = move-exception
            r6 = r7
        L_0x0039:
            r4.printStackTrace()     // Catch:{ all -> 0x0122 }
            if (r6 == 0) goto L_0x0031
            r6.close()     // Catch:{ IOException -> 0x0042 }
            goto L_0x0031
        L_0x0042:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x0031
        L_0x0047:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x002f
        L_0x004c:
            r17 = 0
            r0 = r17
            org.apache.poi.hssf.usermodel.HSSFRow r17 = r15.getRow(r0)     // Catch:{ Exception -> 0x0037, all -> 0x0075 }
            int r10 = r17.getPhysicalNumberOfCells()     // Catch:{ Exception -> 0x0037, all -> 0x0075 }
            java.lang.String r17 = "3"
            r0 = r22
            r1 = r17
            boolean r17 = r0.equals(r1)     // Catch:{ Exception -> 0x0037, all -> 0x0075 }
            if (r17 == 0) goto L_0x007d
            r17 = 4
            r0 = r17
            if (r10 == r0) goto L_0x007d
            if (r7 == 0) goto L_0x002a
            r7.close()     // Catch:{ IOException -> 0x0070 }
            goto L_0x002a
        L_0x0070:
            r3 = move-exception
            r3.printStackTrace()     // Catch:{ Exception -> 0x0037, all -> 0x0075 }
            goto L_0x002a
        L_0x0075:
            r17 = move-exception
            r6 = r7
        L_0x0077:
            if (r6 == 0) goto L_0x007c
            r6.close()     // Catch:{ IOException -> 0x0115 }
        L_0x007c:
            throw r17
        L_0x007d:
            java.lang.String r17 = "1"
            r0 = r22
            r1 = r17
            boolean r17 = r0.equals(r1)     // Catch:{ Exception -> 0x0037, all -> 0x0075 }
            if (r17 == 0) goto L_0x00a6
            java.lang.String r17 = "1"
            r0 = r17
            r1 = r23
            boolean r17 = r0.equals(r1)     // Catch:{ Exception -> 0x0037, all -> 0x0075 }
            if (r17 == 0) goto L_0x00a6
            r17 = 5
            r0 = r17
            if (r10 == r0) goto L_0x00a6
            if (r7 == 0) goto L_0x002a
            r7.close()     // Catch:{ IOException -> 0x00a1 }
            goto L_0x002a
        L_0x00a1:
            r3 = move-exception
            r3.printStackTrace()     // Catch:{ Exception -> 0x0037, all -> 0x0075 }
            goto L_0x002a
        L_0x00a6:
            java.lang.String r17 = "1"
            r0 = r22
            r1 = r17
            boolean r17 = r0.equals(r1)     // Catch:{ Exception -> 0x0037, all -> 0x0075 }
            if (r17 == 0) goto L_0x00d1
            java.lang.String r17 = "2"
            r0 = r17
            r1 = r23
            boolean r17 = r0.equals(r1)     // Catch:{ Exception -> 0x0037, all -> 0x0075 }
            if (r17 == 0) goto L_0x00d1
            r17 = 7
            r0 = r17
            if (r10 == r0) goto L_0x00d1
            if (r7 == 0) goto L_0x002a
            r7.close()     // Catch:{ IOException -> 0x00cb }
            goto L_0x002a
        L_0x00cb:
            r3 = move-exception
            r3.printStackTrace()     // Catch:{ Exception -> 0x0037, all -> 0x0075 }
            goto L_0x002a
        L_0x00d1:
            r9 = 1
        L_0x00d2:
            if (r9 < r14) goto L_0x00dc
            if (r7 == 0) goto L_0x011f
            r7.close()     // Catch:{ IOException -> 0x011b }
            r6 = r7
            goto L_0x0031
        L_0x00dc:
            org.apache.poi.hssf.usermodel.HSSFRow r13 = r15.getRow(r9)     // Catch:{ Exception -> 0x0037, all -> 0x0075 }
            if (r13 == 0) goto L_0x00f1
            r0 = r20
            r1 = r22
            r2 = r23
            java.lang.Object r12 = r0.oneExcelRowToObject(r13, r1, r2)     // Catch:{ Exception -> 0x00f4, all -> 0x0075 }
            if (r12 == 0) goto L_0x00f1
            r11.add(r12)     // Catch:{ Exception -> 0x00f4, all -> 0x0075 }
        L_0x00f1:
            int r9 = r9 + 1
            goto L_0x00d2
        L_0x00f4:
            r5 = move-exception
            java.io.PrintStream r17 = java.lang.System.out     // Catch:{ Exception -> 0x0037, all -> 0x0075 }
            java.lang.StringBuilder r18 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0037, all -> 0x0075 }
            java.lang.String r19 = "第"
            r18.<init>(r19)     // Catch:{ Exception -> 0x0037, all -> 0x0075 }
            r0 = r18
            java.lang.StringBuilder r18 = r0.append(r9)     // Catch:{ Exception -> 0x0037, all -> 0x0075 }
            java.lang.String r19 = "行出错"
            java.lang.StringBuilder r18 = r18.append(r19)     // Catch:{ Exception -> 0x0037, all -> 0x0075 }
            java.lang.String r18 = r18.toString()     // Catch:{ Exception -> 0x0037, all -> 0x0075 }
            r17.println(r18)     // Catch:{ Exception -> 0x0037, all -> 0x0075 }
            r5.printStackTrace()     // Catch:{ Exception -> 0x0037, all -> 0x0075 }
            goto L_0x00f1
        L_0x0115:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x007c
        L_0x011b:
            r3 = move-exception
            r3.printStackTrace()
        L_0x011f:
            r6 = r7
            goto L_0x0031
        L_0x0122:
            r17 = move-exception
            goto L_0x0077
        L_0x0125:
            r4 = move-exception
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenfor.tools.excel.HSSFHelper.coverSingleSheetExcelToList(java.lang.String, java.lang.String, java.lang.String):java.util.List");
    }

    public List coverSingleSheetExcelToList(String p_filePath) {
        ArrayList objList = new ArrayList();
        try {
            HSSFSheet sheet = new HSSFWorkbook(new POIFSFileSystem(new FileInputStream(p_filePath))).getSheetAt(0);
            int rowsCount = sheet.getPhysicalNumberOfRows();
            for (int i = 1; i < rowsCount; i++) {
                try {
                    objList.add(oneExcelRowToObjectValidate(sheet.getRow(i)));
                } catch (Exception exx) {
                    exx.printStackTrace();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return objList;
    }

    public static String getHSSFCellValue(HSSFCell p_hssfCell) {
        if (p_hssfCell == null) {
            return "";
        }
        return p_hssfCell.toString();
    }
}
