package com.kenfor.tools.excel;

import java.util.ArrayList;
import java.util.List;

public abstract class ValidateHelper {
    private String errorMessage = null;
    private String prettyErrorMessage = null;
    protected List validateFalseList = new ArrayList();
    protected List validateSuccessList = new ArrayList();

    public abstract Object oneRowToFalseObject(Object obj) throws Exception;

    public abstract Object oneRowToSuccessObject(Object obj) throws Exception;

    public abstract Object oneRowToSuccessObject(Object obj, String str) throws Exception;

    /* access modifiers changed from: protected */
    public String getErrorMessage() {
        return this.errorMessage;
    }

    private void setErrorMessage(String errorMessage2) {
        this.errorMessage = errorMessage2;
    }

    /* access modifiers changed from: protected */
    public String getPrettyErrorMessage() {
        return this.prettyErrorMessage;
    }

    /* access modifiers changed from: protected */
    public void setPrettyErrorMessage(String prettyErrorMessage2) {
        this.prettyErrorMessage = prettyErrorMessage2;
    }

    /* access modifiers changed from: protected */
    public void setPrettyErrorMessageAndThrowsException(String prettyErrorMessage2) throws Exception {
        setPrettyErrorMessage(prettyErrorMessage2);
        throw new Exception("I know this error, please ignore!");
    }

    public List getValidateFalseList() {
        return this.validateFalseList;
    }

    public List getValidateSuccessList() {
        return this.validateSuccessList;
    }

    public void processValidateAndSplit(List p_list, String use_type) {
        int i = 0;
        while (i < p_list.size()) {
            try {
                Object originalObject = p_list.get(i);
                try {
                    this.validateSuccessList.add(oneRowToSuccessObject(originalObject, use_type));
                } catch (Exception ex) {
                    ex.printStackTrace();
                    setErrorMessage(ex.getMessage());
                    this.validateFalseList.add(oneRowToFalseObject(originalObject));
                }
                i++;
            } catch (Exception ex2) {
                ex2.printStackTrace();
                return;
            }
        }
    }

    public void processValidateAndSplit(List p_list) {
        int i = 0;
        while (i < p_list.size()) {
            try {
                Object originalObject = p_list.get(i);
                try {
                    this.validateSuccessList.add(oneRowToSuccessObject(originalObject));
                } catch (Exception ex) {
                    ex.printStackTrace();
                    String message = ex.getMessage();
                    String obj = ex.getStackTrace().toString();
                    setErrorMessage(message);
                    this.validateFalseList.add(oneRowToFalseObject(originalObject));
                }
                i++;
            } catch (Exception ex2) {
                ex2.printStackTrace();
                return;
            }
        }
    }
}
