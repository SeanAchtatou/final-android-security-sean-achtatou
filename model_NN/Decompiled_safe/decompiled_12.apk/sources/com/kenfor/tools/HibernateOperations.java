package com.kenfor.tools;

import com.kenfor.tools.hibernate.HibernateSessionFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class HibernateOperations {
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0024  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x001b A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List processAListToDBRecords(java.util.List r6) {
        /*
            r3 = 0
            r1 = 0
        L_0x0002:
            int r5 = r6.size()
            if (r1 < r5) goto L_0x0009
            return r6
        L_0x0009:
            java.lang.Object r2 = r6.get(r1)
            com.kenfor.tools.HibernateOperations$1 r4 = new com.kenfor.tools.HibernateOperations$1     // Catch:{ Exception -> 0x001e }
            r4.<init>(r2)     // Catch:{ Exception -> 0x001e }
            r4.execute()     // Catch:{ Exception -> 0x0034, all -> 0x0031 }
            if (r4 == 0) goto L_0x001a
            r4.release()
        L_0x001a:
            r3 = 0
        L_0x001b:
            int r1 = r1 + 1
            goto L_0x0002
        L_0x001e:
            r0 = move-exception
        L_0x001f:
            r0.printStackTrace()     // Catch:{ all -> 0x0029 }
            if (r3 == 0) goto L_0x0027
            r3.release()
        L_0x0027:
            r3 = 0
            goto L_0x001b
        L_0x0029:
            r5 = move-exception
        L_0x002a:
            if (r3 == 0) goto L_0x002f
            r3.release()
        L_0x002f:
            r3 = 0
            throw r5
        L_0x0031:
            r5 = move-exception
            r3 = r4
            goto L_0x002a
        L_0x0034:
            r0 = move-exception
            r3 = r4
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenfor.tools.HibernateOperations.processAListToDBRecords(java.util.List):java.util.List");
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x001d  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void processOneToDBRecord(java.lang.Object r5) {
        /*
            r1 = 0
            com.kenfor.tools.HibernateOperations$2 r2 = new com.kenfor.tools.HibernateOperations$2     // Catch:{ Exception -> 0x0010 }
            r2.<init>(r5)     // Catch:{ Exception -> 0x0010 }
            r2.execute()     // Catch:{ Exception -> 0x002d, all -> 0x002a }
            if (r2 == 0) goto L_0x000e
            r2.release()
        L_0x000e:
            r1 = 0
        L_0x000f:
            return
        L_0x0010:
            r0 = move-exception
        L_0x0011:
            r0.printStackTrace()     // Catch:{ all -> 0x0022 }
            java.io.PrintStream r3 = java.lang.System.out     // Catch:{ all -> 0x0022 }
            java.lang.String r4 = "Trace 1\t"
            r3.println(r4)     // Catch:{ all -> 0x0022 }
            if (r1 == 0) goto L_0x0020
            r1.release()
        L_0x0020:
            r1 = 0
            goto L_0x000f
        L_0x0022:
            r3 = move-exception
        L_0x0023:
            if (r1 == 0) goto L_0x0028
            r1.release()
        L_0x0028:
            r1 = 0
            throw r3
        L_0x002a:
            r3 = move-exception
            r1 = r2
            goto L_0x0023
        L_0x002d:
            r0 = move-exception
            r1 = r2
            goto L_0x0011
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenfor.tools.HibernateOperations.processOneToDBRecord(java.lang.Object):void");
    }

    public static Object getSingleDBRecord(Class p_class, String p_where) {
        String whereStr;
        Transaction trans = null;
        Session session = HibernateSessionFactory.getCurrentSession();
        try {
            trans = session.beginTransaction();
            if (p_where == null || p_where.trim().equals("")) {
                whereStr = "";
            } else {
                whereStr = " where " + p_where;
            }
            Query query = session.createQuery("from " + p_class.getSimpleName() + whereStr);
            if (query != null && query.list().size() > 0) {
                Object obj = query.list().get(0);
                trans.commit();
                HibernateSessionFactory.closeCurrentSession();
                return obj;
            }
        } catch (Exception e) {
            if (trans != null) {
                trans.rollback();
            }
            e.printStackTrace();
        } catch (Throwable th) {
            HibernateSessionFactory.closeCurrentSession();
            throw th;
        }
        HibernateSessionFactory.closeCurrentSession();
        return null;
    }

    public static boolean fExistDBRecord(Class p_class, String p_where) {
        if (getSingleDBRecord(p_class, p_where) != null) {
            return true;
        }
        return false;
    }
}
