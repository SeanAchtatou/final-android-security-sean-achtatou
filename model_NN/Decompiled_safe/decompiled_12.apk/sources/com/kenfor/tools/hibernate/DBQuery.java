package com.kenfor.tools.hibernate;

import com.kenfor.tools.file.ClassLoaderUtil;
import com.kenfor.tools.xml.UseXml;
import java.lang.reflect.Array;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class DBQuery {
    public static Connection getConnection() {
        try {
            return DriverManager.getConnection("proxool." + new UseXml(ClassLoaderUtil.getClassLoader().getResource("proxool.xml").getPath().toString()).getElementValue("proxool.alias"));
        } catch (Exception e) {
            System.out.println("%%%% create connection failure. %%%");
            e.printStackTrace();
            return null;
        }
    }

    public static Connection getConnection(String poolName) {
        try {
            return DriverManager.getConnection("proxool." + poolName);
        } catch (Exception e) {
            System.out.println("%%%% create connection failure. %%%");
            e.printStackTrace();
            return null;
        }
    }

    private static void freeConnection(Connection p_con) {
        if (p_con != null) {
            try {
                p_con.close();
            } catch (SQLException e) {
                System.out.println("%%% free the database connection failure. %%%");
                e.printStackTrace();
            }
        }
    }

    public static Object findByProperty(String p_sql, String p_property) throws SQLException {
        ArrayList list = new ArrayList();
        PreparedStatement psmt = null;
        ResultSet rs = null;
        try {
            psmt = getConnection().prepareStatement(p_sql);
            rs = psmt.executeQuery();
            ResultSetMetaData metaData = rs.getMetaData();
            int numberOfColumns = metaData.getColumnCount();
            while (rs.next()) {
                HashMap map = new HashMap();
                for (int i = 0; i < numberOfColumns; i++) {
                    map.put(metaData.getColumnName(i + 1).trim().toLowerCase(), rs.getString(i + 1) == null ? "" : rs.getString(i + 1).trim());
                }
                list.add(map);
            }
            if (rs != null) {
                rs.close();
            }
            if (psmt != null) {
                psmt.close();
            }
        } catch (SQLException e1) {
            System.out.println("%%% Query failure. %%%");
            e1.printStackTrace();
            if (rs != null) {
                rs.close();
            }
            if (psmt != null) {
                psmt.close();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            if (rs != null) {
                rs.close();
            }
            if (psmt != null) {
                psmt.close();
            }
        } catch (Throwable th) {
            if (rs != null) {
                rs.close();
            }
            if (psmt != null) {
                psmt.close();
            }
            throw th;
        }
        return ((HashMap) list.get(0)).get(p_property);
    }

    public static Object findByProperty(String p_sql, String p_property, String poolName) throws SQLException {
        ArrayList list = new ArrayList();
        PreparedStatement psmt = null;
        ResultSet rs = null;
        try {
            psmt = getConnection(poolName).prepareStatement(p_sql);
            rs = psmt.executeQuery();
            ResultSetMetaData metaData = rs.getMetaData();
            int numberOfColumns = metaData.getColumnCount();
            while (rs.next()) {
                HashMap map = new HashMap();
                for (int i = 0; i < numberOfColumns; i++) {
                    map.put(metaData.getColumnName(i + 1).trim().toLowerCase(), rs.getString(i + 1) == null ? "" : rs.getString(i + 1).trim());
                }
                list.add(map);
            }
            if (rs != null) {
                rs.close();
            }
            if (psmt != null) {
                psmt.close();
            }
        } catch (SQLException e1) {
            System.out.println("%%% Query failure. %%%");
            e1.printStackTrace();
            if (rs != null) {
                rs.close();
            }
            if (psmt != null) {
                psmt.close();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            if (rs != null) {
                rs.close();
            }
            if (psmt != null) {
                psmt.close();
            }
        } catch (Throwable th) {
            if (rs != null) {
                rs.close();
            }
            if (psmt != null) {
                psmt.close();
            }
            throw th;
        }
        return ((HashMap) list.get(0)).get(p_property);
    }

    public static Object findByProperty(Connection p_con, String p_sql) {
        return null;
    }

    public static ArrayList executeQuery(String p_sql) throws SQLException {
        ArrayList list = new ArrayList();
        Connection con = getConnection();
        try {
            list = executeQuery(con, p_sql);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            freeConnection(con);
        }
        return list;
    }

    public static ArrayList executeQuery(Connection p_con, String p_sql) throws SQLException {
        ArrayList dataSetArray = new ArrayList();
        PreparedStatement psmt = null;
        ResultSet rs = null;
        try {
            psmt = p_con.prepareStatement(p_sql);
            rs = psmt.executeQuery();
            ResultSetMetaData metaData = rs.getMetaData();
            int numberOfColumns = metaData.getColumnCount();
            while (rs.next()) {
                HashMap map = new HashMap();
                for (int i = 0; i < numberOfColumns; i++) {
                    map.put(metaData.getColumnName(i + 1).trim().toLowerCase(), rs.getString(i + 1) == null ? "" : rs.getString(i + 1).trim());
                }
                dataSetArray.add(map);
            }
            if (rs != null) {
                rs.close();
            }
            if (psmt != null) {
                psmt.close();
            }
        } catch (SQLException e1) {
            System.out.println("%%% Query failure. %%%");
            e1.printStackTrace();
            if (rs != null) {
                rs.close();
            }
            if (psmt != null) {
                psmt.close();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            if (rs != null) {
                rs.close();
            }
            if (psmt != null) {
                psmt.close();
            }
        } catch (Throwable th) {
            if (rs != null) {
                rs.close();
            }
            if (psmt != null) {
                psmt.close();
            }
            throw th;
        }
        return dataSetArray;
    }

    public static ArrayList executeQuery(Connection p_con, String p_sql, String[] p_parameters) throws SQLException {
        String trim;
        ArrayList list = new ArrayList();
        PreparedStatement psmt = null;
        ResultSet rs = null;
        try {
            psmt = p_con.prepareStatement(p_sql);
            for (int i = 0; i < p_parameters.length; i++) {
                psmt.setString(i + 1, p_parameters[i]);
            }
            rs = psmt.executeQuery();
            ResultSetMetaData metaData = rs.getMetaData();
            int numberOfColumns = metaData.getColumnCount();
            while (rs.next()) {
                HashMap map = new HashMap();
                for (int i2 = 0; i2 < numberOfColumns; i2++) {
                    String lowerCase = metaData.getColumnName(i2 + 1).trim().toLowerCase();
                    if (rs.getString(i2 + 1) == null) {
                        trim = "";
                    } else {
                        trim = rs.getString(i2 + 1).trim();
                    }
                    map.put(lowerCase, trim);
                }
                list.add(map);
            }
            if (rs != null) {
                rs.close();
            }
            if (psmt != null) {
                psmt.close();
            }
        } catch (SQLException e1) {
            System.out.println("%%% Query failure. %%%");
            e1.printStackTrace();
            if (rs != null) {
                rs.close();
            }
            if (psmt != null) {
                psmt.close();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            if (rs != null) {
                rs.close();
            }
            if (psmt != null) {
                psmt.close();
            }
        } catch (Throwable th) {
            if (rs != null) {
                rs.close();
            }
            if (psmt != null) {
                psmt.close();
            }
            throw th;
        }
        return list;
    }

    public static ArrayList executeQuery(Connection conn, String sql, Object[] paramList, boolean toString) throws SQLException {
        PreparedStatement stmt = null;
        ArrayList list = new ArrayList();
        try {
            stmt = conn.prepareStatement(sql);
            for (int i = 0; i < paramList.length; i++) {
                stmt.setObject(i + 1, paramList[i]);
            }
            ResultSet rs = stmt.executeQuery();
            ResultSetMetaData rsMetaData = rs.getMetaData();
            int columnCount = rsMetaData.getColumnCount();
            while (rs.next()) {
                HashMap hashMap = new HashMap();
                for (int i2 = 1; i2 <= columnCount; i2++) {
                    if (!toString) {
                        hashMap.put(rsMetaData.getColumnName(i2), rs.getObject(rsMetaData.getColumnName(i2)));
                    } else if (rs.getObject(rsMetaData.getColumnName(i2)) == null) {
                        hashMap.put(rsMetaData.getColumnName(i2), "");
                    } else {
                        hashMap.put(rsMetaData.getColumnName(i2), rs.getObject(rsMetaData.getColumnName(i2)).toString());
                    }
                }
                list.add(hashMap);
            }
            return list;
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v4, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: java.lang.Object[][]} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Object[][] executeSQLQuery(java.sql.Connection r14, java.lang.String r15) throws java.sql.SQLException {
        /*
            r4 = 0
            java.lang.Object[][] r4 = (java.lang.Object[][]) r4
            r8 = 0
            r10 = 0
            r5 = 0
            r11 = 1004(0x3ec, float:1.407E-42)
            r12 = 1007(0x3ef, float:1.411E-42)
            java.sql.PreparedStatement r8 = r14.prepareStatement(r15, r11, r12)     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
            java.sql.ResultSet r10 = r8.executeQuery()     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
            java.sql.ResultSetMetaData r5 = r10.getMetaData()     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
            r10.last()     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
            int r7 = r10.getRow()     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
            int r6 = r5.getColumnCount()     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
            r9 = 0
            int[] r11 = new int[]{r7, r6}     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
            java.lang.Class<java.lang.Object> r12 = java.lang.Object.class
            java.lang.Object r11 = java.lang.reflect.Array.newInstance(r12, r11)     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
            r0 = r11
            java.lang.Object[][] r0 = (java.lang.Object[][]) r0     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
            r4 = r0
            r10.beforeFirst()     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
        L_0x0033:
            boolean r11 = r10.next()     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
            if (r11 != 0) goto L_0x0044
            if (r10 == 0) goto L_0x003e
            r10.close()
        L_0x003e:
            if (r8 == 0) goto L_0x0043
            r8.close()
        L_0x0043:
            return r4
        L_0x0044:
            r1 = 0
        L_0x0045:
            if (r1 < r6) goto L_0x004a
            int r9 = r9 + 1
            goto L_0x0033
        L_0x004a:
            r11 = r4[r9]     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
            r11 = r11[r1]     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
            if (r11 == 0) goto L_0x0057
            r11 = r4[r9]     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
            r11 = r11[r1]     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
            r11.toString()     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
        L_0x0057:
            int r11 = r1 + 1
            java.lang.String r11 = r5.getColumnTypeName(r11)     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
            java.lang.String r12 = "NUMBER"
            boolean r11 = r11.equals(r12)     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
            if (r11 == 0) goto L_0x0089
            int r11 = r1 + 1
            int r11 = r5.getScale(r11)     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
            if (r11 <= 0) goto L_0x0089
            r13 = r4[r9]     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
            int r11 = r1 + 1
            java.lang.Object r11 = r10.getObject(r11)     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
            if (r11 != 0) goto L_0x0082
            r11 = 0
        L_0x0079:
            java.lang.Double r11 = java.lang.Double.valueOf(r11)     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
            r13[r1] = r11     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
        L_0x007f:
            int r1 = r1 + 1
            goto L_0x0045
        L_0x0082:
            int r11 = r1 + 1
            double r11 = r10.getDouble(r11)     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
            goto L_0x0079
        L_0x0089:
            int r11 = r1 + 1
            java.lang.String r11 = r5.getColumnTypeName(r11)     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
            java.lang.String r12 = "NUMBER"
            boolean r11 = r11.equals(r12)     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
            if (r11 == 0) goto L_0x00cf
            int r11 = r1 + 1
            int r11 = r5.getScale(r11)     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
            if (r11 != 0) goto L_0x00cf
            r12 = r4[r9]     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
            int r11 = r1 + 1
            java.lang.Object r11 = r10.getObject(r11)     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
            if (r11 != 0) goto L_0x00c8
            r11 = 0
        L_0x00aa:
            java.lang.Integer r11 = java.lang.Integer.valueOf(r11)     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
            r12[r1] = r11     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
            goto L_0x007f
        L_0x00b1:
            r2 = move-exception
            java.io.PrintStream r11 = java.lang.System.out     // Catch:{ all -> 0x00f5 }
            java.lang.String r12 = "%%% Query failure. %%%"
            r11.println(r12)     // Catch:{ all -> 0x00f5 }
            r2.printStackTrace()     // Catch:{ all -> 0x00f5 }
            if (r10 == 0) goto L_0x00c1
            r10.close()
        L_0x00c1:
            if (r8 == 0) goto L_0x0043
            r8.close()
            goto L_0x0043
        L_0x00c8:
            int r11 = r1 + 1
            int r11 = r10.getInt(r11)     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
            goto L_0x00aa
        L_0x00cf:
            r12 = r4[r9]     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
            int r11 = r1 + 1
            java.lang.Object r11 = r10.getObject(r11)     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
            if (r11 != 0) goto L_0x00ee
            java.lang.String r11 = ""
        L_0x00db:
            r12[r1] = r11     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
            goto L_0x007f
        L_0x00de:
            r3 = move-exception
            r3.printStackTrace()     // Catch:{ all -> 0x00f5 }
            if (r10 == 0) goto L_0x00e7
            r10.close()
        L_0x00e7:
            if (r8 == 0) goto L_0x0043
            r8.close()
            goto L_0x0043
        L_0x00ee:
            int r11 = r1 + 1
            java.lang.Object r11 = r10.getObject(r11)     // Catch:{ SQLException -> 0x00b1, Exception -> 0x00de }
            goto L_0x00db
        L_0x00f5:
            r11 = move-exception
            if (r10 == 0) goto L_0x00fb
            r10.close()
        L_0x00fb:
            if (r8 == 0) goto L_0x0100
            r8.close()
        L_0x0100:
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenfor.tools.hibernate.DBQuery.executeSQLQuery(java.sql.Connection, java.lang.String):java.lang.Object[][]");
    }

    public static Object[] executeSQLQueryForSort(Connection p_con, String sql) throws SQLException {
        Object[] list = null;
        PreparedStatement psmt = null;
        ResultSet rs = null;
        try {
            psmt = p_con.prepareStatement(sql, 1004, 1007);
            rs = psmt.executeQuery();
            ResultSetMetaData metaData = rs.getMetaData();
            rs.last();
            int numberOfRows = rs.getRow();
            int columnCount = metaData.getColumnCount();
            int row = 0;
            list = new Object[numberOfRows];
            rs.beforeFirst();
            while (rs.next()) {
                list[row] = rs.getObject(1);
                row++;
            }
            if (rs != null) {
                rs.close();
            }
            if (psmt != null) {
                psmt.close();
            }
        } catch (SQLException e1) {
            System.out.println("%%% Query failure. %%%");
            e1.printStackTrace();
            if (rs != null) {
                rs.close();
            }
            if (psmt != null) {
                psmt.close();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            if (rs != null) {
                rs.close();
            }
            if (psmt != null) {
                psmt.close();
            }
        } catch (Throwable th) {
            if (rs != null) {
                rs.close();
            }
            if (psmt != null) {
                psmt.close();
            }
            throw th;
        }
        return list;
    }

    public static Object[][] executeSQLQueryAddStat(Object[][] p_list) {
        int a = p_list.length;
        Object[][] copylist = (Object[][]) Array.newInstance(Object.class, a + 1, p_list[0].length + 1);
        System.arraycopy(p_list, 0, copylist, 0, p_list.length);
        for (int i = 0; i < copylist[a].length; i++) {
            copylist[a][i] = "合计";
        }
        return copylist;
    }
}
