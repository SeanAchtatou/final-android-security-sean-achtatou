package com.kenfor.tools.hibernate;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.jivesoftware.smackx.GroupChatInvitation;
import org.jivesoftware.smackx.packet.CapsExtension;

public class CreateNO {
    /* access modifiers changed from: private */
    public static final String[] noes = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", CapsExtension.NODE_NAME, "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", GroupChatInvitation.ELEMENT_NAME, "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

    public static void main(String[] args) {
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x005e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getNO1(final int r9, final java.lang.String r10) {
        /*
            r2 = 0
            r5 = 0
            java.lang.String r0 = ""
            java.lang.String r4 = ""
            com.kenfor.tools.hibernate.CreateNO$1 r3 = new com.kenfor.tools.hibernate.CreateNO$1     // Catch:{ Exception -> 0x004e }
            r3.<init>(r9, r10)     // Catch:{ Exception -> 0x004e }
            r3.execute()     // Catch:{ Exception -> 0x0076, all -> 0x0073 }
            java.lang.Object r7 = r3.parameter     // Catch:{ Exception -> 0x0076, all -> 0x0073 }
            java.lang.Integer r7 = (java.lang.Integer) r7     // Catch:{ Exception -> 0x0076, all -> 0x0073 }
            int r5 = r7.intValue()     // Catch:{ Exception -> 0x0076, all -> 0x0073 }
            if (r3 == 0) goto L_0x0079
            r3.release()
            r2 = 0
        L_0x001c:
            java.lang.Integer r7 = java.lang.Integer.valueOf(r5)
            java.lang.String r6 = r7.toString()
        L_0x0024:
            int r7 = r6.length()
            r8 = 4
            if (r7 < r8) goto L_0x0063
            java.lang.String r7 = "yyyyMMdd"
            java.lang.String r0 = com.kenfor.tools.hibernate.DBRecord.get_format_db_dt(r7)
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            java.lang.String r8 = java.lang.String.valueOf(r10)
            r7.<init>(r8)
            java.lang.String r8 = "-"
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.StringBuilder r7 = r7.append(r0)
            java.lang.StringBuilder r7 = r7.append(r6)
            java.lang.String r4 = r7.toString()
            r7 = r4
        L_0x004d:
            return r7
        L_0x004e:
            r1 = move-exception
        L_0x004f:
            r1.printStackTrace()     // Catch:{ all -> 0x005b }
            if (r2 == 0) goto L_0x0058
            r2.release()
            r2 = 0
        L_0x0058:
            java.lang.String r7 = ""
            goto L_0x004d
        L_0x005b:
            r7 = move-exception
        L_0x005c:
            if (r2 == 0) goto L_0x0062
            r2.release()
            r2 = 0
        L_0x0062:
            throw r7
        L_0x0063:
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            java.lang.String r8 = "0"
            r7.<init>(r8)
            java.lang.StringBuilder r7 = r7.append(r6)
            java.lang.String r6 = r7.toString()
            goto L_0x0024
        L_0x0073:
            r7 = move-exception
            r2 = r3
            goto L_0x005c
        L_0x0076:
            r1 = move-exception
            r2 = r3
            goto L_0x004f
        L_0x0079:
            r2 = r3
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenfor.tools.hibernate.CreateNO.getNO1(int, java.lang.String):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0067  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getNO(final java.lang.String r11, final int r12, final java.lang.String r13) {
        /*
            r3 = 0
            r6 = 0
            java.lang.String r0 = ""
            java.lang.String r5 = ""
            com.kenfor.tools.hibernate.CreateNO$2 r4 = new com.kenfor.tools.hibernate.CreateNO$2     // Catch:{ Exception -> 0x0057 }
            r4.<init>(r12, r13, r11)     // Catch:{ Exception -> 0x0057 }
            r4.execute()     // Catch:{ Exception -> 0x007f, all -> 0x007c }
            java.lang.Object r9 = r4.parameter     // Catch:{ Exception -> 0x007f, all -> 0x007c }
            java.lang.Integer r9 = (java.lang.Integer) r9     // Catch:{ Exception -> 0x007f, all -> 0x007c }
            int r6 = r9.intValue()     // Catch:{ Exception -> 0x007f, all -> 0x007c }
            if (r4 == 0) goto L_0x0082
            r4.release()
            r3 = 0
        L_0x001c:
            java.lang.Integer r9 = java.lang.Integer.valueOf(r6)
            java.lang.String r7 = r9.toString()
        L_0x0024:
            int r9 = r7.length()
            r10 = 4
            if (r9 < r10) goto L_0x006c
            java.text.SimpleDateFormat r8 = new java.text.SimpleDateFormat
            java.lang.String r9 = "yyyyMMdd"
            r8.<init>(r9)
            java.sql.Date r1 = java.sql.Date.valueOf(r11)
            java.lang.String r0 = r8.format(r1)
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            java.lang.String r10 = java.lang.String.valueOf(r13)
            r9.<init>(r10)
            java.lang.String r10 = "-"
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.StringBuilder r9 = r9.append(r0)
            java.lang.StringBuilder r9 = r9.append(r7)
            java.lang.String r5 = r9.toString()
            r9 = r5
        L_0x0056:
            return r9
        L_0x0057:
            r2 = move-exception
        L_0x0058:
            r2.printStackTrace()     // Catch:{ all -> 0x0064 }
            if (r3 == 0) goto L_0x0061
            r3.release()
            r3 = 0
        L_0x0061:
            java.lang.String r9 = ""
            goto L_0x0056
        L_0x0064:
            r9 = move-exception
        L_0x0065:
            if (r3 == 0) goto L_0x006b
            r3.release()
            r3 = 0
        L_0x006b:
            throw r9
        L_0x006c:
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            java.lang.String r10 = "0"
            r9.<init>(r10)
            java.lang.StringBuilder r9 = r9.append(r7)
            java.lang.String r7 = r9.toString()
            goto L_0x0024
        L_0x007c:
            r9 = move-exception
            r3 = r4
            goto L_0x0065
        L_0x007f:
            r2 = move-exception
            r3 = r4
            goto L_0x0058
        L_0x0082:
            r3 = r4
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenfor.tools.hibernate.CreateNO.getNO(java.lang.String, int, java.lang.String):java.lang.String");
    }

    public static String getCoopNO(final int account_id, final int p_employee_id, final String p_no_type) {
        try {
            ConnectionManager getValue = new ConnectionManager() {
                public void process(Connection con) throws Exception {
                    PreparedStatement pstmt = con.prepareStatement("select count(*) as num from remark where account_id=? and remark_name=? and employee_id=?");
                    pstmt.setInt(1, account_id);
                    pstmt.setString(2, p_no_type);
                    pstmt.setInt(3, p_employee_id);
                    ResultSet rs = pstmt.executeQuery();
                    rs.next();
                    if (rs.getInt("num") > 0) {
                        PreparedStatement pstmt2 = con.prepareStatement("select remark_int+1 as order_id from remark where  account_id=? and remark_name=? and employee_id=?");
                        pstmt2.setInt(1, account_id);
                        pstmt2.setString(2, p_no_type);
                        pstmt2.setInt(3, p_employee_id);
                        ResultSet rs2 = pstmt2.executeQuery();
                        rs2.next();
                        this.parameter = Integer.valueOf(rs2.getInt("order_id"));
                        PreparedStatement pstmt3 = con.prepareStatement("update remark set remark_int=remark_int+1,remark_dt=convert(varchar(10),getdate(),120) where account_id=? and remark_name=? and employee_id=?");
                        pstmt3.setInt(1, account_id);
                        pstmt3.setString(2, p_no_type);
                        pstmt3.setInt(3, p_employee_id);
                        pstmt3.execute();
                        return;
                    }
                    this.parameter = 1;
                    PreparedStatement pstmt4 = con.prepareStatement("insert into remark (remark_name,remark_int,account_id,remark_dt,employee_id) values (?,1,?,convert(varchar(10),getdate(),120),?)");
                    pstmt4.setString(1, p_no_type);
                    pstmt4.setInt(2, account_id);
                    pstmt4.setInt(3, p_employee_id);
                    pstmt4.execute();
                }
            };
            getValue.execute();
            String order_no = ((Integer) getValue.parameter).toString();
            while (order_no.length() < 6) {
                order_no = "0" + order_no;
            }
            return String.valueOf(p_no_type) + ":" + order_no;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getAllNO(final int account_id, final String p_no_type) {
        try {
            ConnectionManager getValue = new ConnectionManager() {
                public void process(Connection con) throws Exception {
                    PreparedStatement pstmt = con.prepareStatement("select count(*) as num from remark where account_id=? and remark_name=? and employee_id=?");
                    pstmt.setInt(1, account_id);
                    pstmt.setString(2, p_no_type);
                    pstmt.setInt(3, 0);
                    ResultSet rs = pstmt.executeQuery();
                    rs.next();
                    if (rs.getInt("num") > 0) {
                        PreparedStatement pstmt2 = con.prepareStatement("select remark_int+1 as order_id from remark where  account_id=? and remark_name=? and employee_id=?");
                        pstmt2.setInt(1, account_id);
                        pstmt2.setString(2, p_no_type);
                        pstmt2.setInt(3, 0);
                        ResultSet rs2 = pstmt2.executeQuery();
                        rs2.next();
                        this.parameter = Integer.valueOf(rs2.getInt("order_id"));
                        PreparedStatement pstmt3 = con.prepareStatement("update remark set remark_int=remark_int+1,remark_dt=convert(varchar(10),getdate(),120) where account_id=? and remark_name=? and employee_id=?");
                        pstmt3.setInt(1, account_id);
                        pstmt3.setString(2, p_no_type);
                        pstmt3.setInt(3, 0);
                        pstmt3.execute();
                        return;
                    }
                    this.parameter = 1;
                    PreparedStatement pstmt4 = con.prepareStatement("insert into remark (remark_name,remark_int,account_id,remark_dt,employee_id) values (?,1,?,convert(varchar(10),getdate(),120),?)");
                    pstmt4.setString(1, p_no_type);
                    pstmt4.setInt(2, account_id);
                    pstmt4.setInt(3, 0);
                    pstmt4.execute();
                }
            };
            getValue.execute();
            String order_no = ((Integer) getValue.parameter).toString();
            while (order_no.length() < 6) {
                order_no = "0" + order_no;
            }
            return String.valueOf(p_no_type) + ":" + order_no;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getLevelNo(int father_id, String table_name, String column_name, String id_column_name) {
        String father_level_no;
        String level_no;
        String level_no2 = "error";
        Session session = HibernateSessionFactory.getCurrentSession();
        Transaction trans = null;
        try {
            trans = session.beginTransaction();
            Connection con = session.connection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select " + column_name + " from " + table_name + " where " + id_column_name + "=" + String.valueOf(father_id));
            if (rs.next()) {
                father_level_no = rs.getString(column_name);
            } else {
                father_level_no = "";
            }
            rs.close();
            ResultSet rs2 = stmt.executeQuery("select max(right(" + column_name + ",4)) as level_no from " + table_name + " where father_id=" + String.valueOf(father_id));
            if (rs2.next()) {
                String level_no3 = rs2.getString("level_no") == null ? "0000" : rs2.getString("level_no");
                if (level_no3.trim().length() <= 0 || Integer.parseInt(level_no3) >= 9999) {
                    level_no = "out";
                } else {
                    level_no = String.valueOf(Integer.parseInt(level_no3) + 10001).substring(1, 5);
                }
            } else {
                level_no = "0001";
            }
            level_no2 = String.valueOf(father_level_no) + level_no;
            trans.commit();
            stmt.close();
            con.close();
        } catch (Exception e) {
            if (trans != null) {
                trans.rollback();
            }
            e.printStackTrace();
        } finally {
            HibernateSessionFactory.closeCurrentSession();
        }
        return level_no2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0023  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x002b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getLevelNo(final java.lang.String r6, final java.lang.String r7, final java.lang.String r8, final java.lang.String r9) {
        /*
            java.lang.String r1 = ""
            r3 = 0
            com.kenfor.tools.hibernate.CreateNO$5 r4 = new com.kenfor.tools.hibernate.CreateNO$5     // Catch:{ Exception -> 0x001d }
            r4.<init>(r8, r7, r9, r6)     // Catch:{ Exception -> 0x001d }
            r4.execute()     // Catch:{ Exception -> 0x0032, all -> 0x002f }
            java.lang.Object r5 = r4.parameter     // Catch:{ Exception -> 0x0032, all -> 0x002f }
            if (r5 == 0) goto L_0x0015
            java.lang.Object r5 = r4.parameter     // Catch:{ Exception -> 0x0032, all -> 0x002f }
            java.lang.String r1 = r5.toString()     // Catch:{ Exception -> 0x0032, all -> 0x002f }
        L_0x0015:
            if (r4 == 0) goto L_0x001a
            r4.release()
        L_0x001a:
            r3 = r4
            r2 = r1
        L_0x001c:
            return r2
        L_0x001d:
            r0 = move-exception
        L_0x001e:
            r0.printStackTrace()     // Catch:{ all -> 0x0028 }
            if (r3 == 0) goto L_0x0026
            r3.release()
        L_0x0026:
            r2 = r1
            goto L_0x001c
        L_0x0028:
            r5 = move-exception
        L_0x0029:
            if (r3 == 0) goto L_0x002e
            r3.release()
        L_0x002e:
            throw r5
        L_0x002f:
            r5 = move-exception
            r3 = r4
            goto L_0x0029
        L_0x0032:
            r0 = move-exception
            r3 = r4
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenfor.tools.hibernate.CreateNO.getLevelNo(java.lang.String, java.lang.String, java.lang.String, java.lang.String):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0023  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x002b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getLevelNoNew(final java.lang.String r6, final java.lang.String r7, final java.lang.String r8, final java.lang.String r9) {
        /*
            java.lang.String r1 = ""
            r3 = 0
            com.kenfor.tools.hibernate.CreateNO$6 r4 = new com.kenfor.tools.hibernate.CreateNO$6     // Catch:{ Exception -> 0x001d }
            r4.<init>(r8, r7, r9, r6)     // Catch:{ Exception -> 0x001d }
            r4.execute()     // Catch:{ Exception -> 0x0032, all -> 0x002f }
            java.lang.Object r5 = r4.parameter     // Catch:{ Exception -> 0x0032, all -> 0x002f }
            if (r5 == 0) goto L_0x0015
            java.lang.Object r5 = r4.parameter     // Catch:{ Exception -> 0x0032, all -> 0x002f }
            java.lang.String r1 = r5.toString()     // Catch:{ Exception -> 0x0032, all -> 0x002f }
        L_0x0015:
            if (r4 == 0) goto L_0x001a
            r4.release()
        L_0x001a:
            r3 = r4
            r2 = r1
        L_0x001c:
            return r2
        L_0x001d:
            r0 = move-exception
        L_0x001e:
            r0.printStackTrace()     // Catch:{ all -> 0x0028 }
            if (r3 == 0) goto L_0x0026
            r3.release()
        L_0x0026:
            r2 = r1
            goto L_0x001c
        L_0x0028:
            r5 = move-exception
        L_0x0029:
            if (r3 == 0) goto L_0x002e
            r3.release()
        L_0x002e:
            throw r5
        L_0x002f:
            r5 = move-exception
            r3 = r4
            goto L_0x0029
        L_0x0032:
            r0 = move-exception
            r3 = r4
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenfor.tools.hibernate.CreateNO.getLevelNoNew(java.lang.String, java.lang.String, java.lang.String, java.lang.String):java.lang.String");
    }
}
