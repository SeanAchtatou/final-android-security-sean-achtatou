package com.kenfor.tools.hibernate;

import org.hibernate.Session;
import org.hibernate.Transaction;

public abstract class SessionManager {
    public boolean flag;
    public Object form;
    public Object form2;
    public Object[] forms;
    public Object parameter;
    public Object parameter1;

    public abstract void process(Session session) throws Exception;

    public SessionManager() {
    }

    public void release() {
        this.form = null;
        this.form2 = null;
        this.parameter = null;
        this.parameter1 = null;
    }

    public SessionManager(Object form3) {
        this.form = form3;
    }

    public SessionManager(Object form1, Object form22) {
        this.form = form1;
        this.form2 = form22;
    }

    public SessionManager(Object form1, Object[] p_forms) {
        this.form = form1;
        this.forms = p_forms;
    }

    public void execute() throws Exception {
        Transaction trans = null;
        Session session = HibernateSessionFactory.getCurrentSession();
        try {
            trans = session.beginTransaction();
            process(session);
            trans.commit();
            HibernateSessionFactory.closeCurrentSession();
        } catch (Exception e) {
            if (trans != null) {
                trans.rollback();
            }
            e.printStackTrace();
            throw e;
        } catch (Throwable th) {
            HibernateSessionFactory.closeCurrentSession();
            throw th;
        }
    }

    public void execute(int config_index) throws Exception {
        Session session;
        Transaction trans = null;
        if (config_index == 1) {
            session = HibernateSessionFactory.getCurrentSession();
        } else if (config_index == 2) {
            session = HibernateSessionFactory2.getCurrentSession();
        } else {
            session = HibernateSessionFactory.getCurrentSession();
        }
        try {
            trans = session.beginTransaction();
            process(session);
            trans.commit();
            if (config_index == 1) {
                HibernateSessionFactory.closeCurrentSession();
            } else if (config_index == 2) {
                HibernateSessionFactory2.closeCurrentSession();
            } else {
                HibernateSessionFactory.closeCurrentSession();
            }
        } catch (Exception e) {
            if (trans != null) {
                trans.rollback();
            }
            e.printStackTrace();
            throw e;
        } catch (Throwable th) {
            if (config_index == 1) {
                HibernateSessionFactory.closeCurrentSession();
            } else if (config_index == 2) {
                HibernateSessionFactory2.closeCurrentSession();
            } else {
                HibernateSessionFactory.closeCurrentSession();
            }
            throw th;
        }
    }
}
