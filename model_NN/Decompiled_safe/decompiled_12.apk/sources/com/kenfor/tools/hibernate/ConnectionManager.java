package com.kenfor.tools.hibernate;

import com.kenfor.tools.file.ClassLoaderUtil;
import com.kenfor.tools.xml.UseXml;
import java.sql.Connection;
import java.sql.DriverManager;
import org.apache.struts.action.ActionServlet;

public abstract class ConnectionManager {
    public boolean flag;
    public Object import_obj;
    public Object parameter;
    public Object parameter1;
    public ActionServlet servlet;

    public abstract void process(Connection connection) throws Exception;

    public ConnectionManager() {
    }

    public ConnectionManager(Object import_obj2) {
        this.import_obj = import_obj2;
    }

    public ConnectionManager(ActionServlet servlet2) {
        this.servlet = servlet2;
    }

    public void release() {
        this.parameter = null;
        this.parameter1 = null;
        this.servlet = null;
    }

    public void execute() throws Exception {
        String url = ClassLoaderUtil.getClassLoader().getResource("proxool.xml").getPath().toString();
        System.out.println(url);
        Connection con = DriverManager.getConnection("proxool." + new UseXml(url).getElementValue("proxool.alias"));
        try {
            con.setAutoCommit(false);
            process(con);
            con.commit();
            Recycle.cleanup(con);
        } catch (Exception e) {
            if (con != null) {
                con.rollback();
            }
            e.printStackTrace();
            throw e;
        } catch (Throwable th) {
            Recycle.cleanup(con);
            throw th;
        }
    }

    public void execute(String poolName) throws Exception {
        Connection con = DriverManager.getConnection("proxool." + poolName);
        try {
            con.setAutoCommit(false);
            process(con);
            con.commit();
            Recycle.cleanup(con);
        } catch (Exception e) {
            if (con != null) {
                con.rollback();
            }
            e.printStackTrace();
            throw e;
        } catch (Throwable th) {
            Recycle.cleanup(con);
            throw th;
        }
    }
}
