package com.kenfor.tools.hibernate;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForward;

public class DBRecord {
    public static boolean findSameRecord(final String table_name, final String sql_where) {
        ConnectionManager findSameRec = null;
        try {
            ConnectionManager findSameRec2 = new ConnectionManager() {
                public void process(Connection con) throws Exception {
                    ResultSet rs = con.createStatement().executeQuery("select count(*) as rec_num from " + table_name + " where " + sql_where);
                    if (rs.next()) {
                        this.parameter = Integer.valueOf(rs.getInt("rec_num"));
                    } else {
                        this.parameter = null;
                    }
                }
            };
            try {
                findSameRec2.execute();
                if (findSameRec2.parameter == null || ((Integer) findSameRec2.parameter).intValue() <= 0) {
                    findSameRec2.release();
                    return false;
                }
                findSameRec = findSameRec2;
                findSameRec.release();
                return true;
            } catch (Exception e) {
                e = e;
                findSameRec = findSameRec2;
                try {
                    e.printStackTrace();
                    findSameRec.release();
                    return true;
                } catch (Throwable th) {
                    th = th;
                }
            } catch (Throwable th2) {
                th = th2;
                findSameRec = findSameRec2;
                findSameRec.release();
                throw th;
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            findSameRec.release();
            return true;
        }
    }

    public static boolean findSameRecord(final String table_name, final String sql_where, String poolName) {
        ConnectionManager findSameRec = null;
        try {
            ConnectionManager findSameRec2 = new ConnectionManager() {
                public void process(Connection con) throws Exception {
                    ResultSet rs = con.createStatement().executeQuery("select count(*) as rec_num from " + table_name + " where " + sql_where);
                    if (rs.next()) {
                        this.parameter = Integer.valueOf(rs.getInt("rec_num"));
                    } else {
                        this.parameter = null;
                    }
                }
            };
            try {
                findSameRec2.execute(poolName);
                if (findSameRec2.parameter == null || ((Integer) findSameRec2.parameter).intValue() <= 0) {
                    findSameRec2.release();
                    return false;
                }
                findSameRec = findSameRec2;
                findSameRec.release();
                return true;
            } catch (Exception e) {
                e = e;
                findSameRec = findSameRec2;
                try {
                    e.printStackTrace();
                    findSameRec.release();
                    return true;
                } catch (Throwable th) {
                    th = th;
                }
            } catch (Throwable th2) {
                th = th2;
                findSameRec = findSameRec2;
                findSameRec.release();
                throw th;
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            findSameRec.release();
            return true;
        }
    }

    public static Object getTableValue(final String table_name, final String getColumn_name, final String sql_where, final String value_type) {
        try {
            ConnectionManager getValue = new ConnectionManager() {
                public void process(Connection con) throws Exception {
                    ResultSet rs = con.createStatement().executeQuery("select " + getColumn_name + " as column_value from " + table_name + " where " + sql_where);
                    System.out.println("select " + getColumn_name + " as column_value from " + table_name + " where " + sql_where);
                    if (rs.next()) {
                        if ("str".equals(value_type)) {
                            this.parameter = rs.getString("column_value");
                        } else if ("int".equals(value_type)) {
                            this.parameter = Integer.valueOf(rs.getInt("column_value"));
                        } else if ("float".equals(value_type)) {
                            this.parameter = Float.valueOf(rs.getFloat("column_value"));
                        } else if (value_type.equals("double")) {
                            this.parameter = Double.valueOf(rs.getDouble("column_value"));
                        } else {
                            this.parameter = rs.getObject("column_value");
                        }
                    } else if ("str".equals(value_type)) {
                        this.parameter = "";
                    } else if ("int".equals(value_type)) {
                        this.parameter = 0;
                    } else if ("float".equals(value_type)) {
                        this.parameter = Double.valueOf(0.0d);
                    } else if (value_type.equals("double")) {
                        this.parameter = Double.valueOf(0.0d);
                    } else {
                        this.parameter = null;
                    }
                }
            };
            getValue.execute();
            return getValue.parameter;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static Object getTableValue(final String table_name, final String getColumn_name, final String sql_where, final String value_type, String poolName) {
        try {
            ConnectionManager getValue = new ConnectionManager() {
                public void process(Connection con) throws Exception {
                    ResultSet rs = con.createStatement().executeQuery("select " + getColumn_name + " as column_value from " + table_name + " where " + sql_where);
                    System.out.println("select " + getColumn_name + " as column_value from " + table_name + " where " + sql_where);
                    if (rs.next()) {
                        if ("str".equals(value_type)) {
                            this.parameter = rs.getString("column_value");
                        } else if ("int".equals(value_type)) {
                            this.parameter = Integer.valueOf(rs.getInt("column_value"));
                        } else if ("float".equals(value_type)) {
                            this.parameter = Float.valueOf(rs.getFloat("column_value"));
                        } else if (value_type.equals("double")) {
                            this.parameter = Double.valueOf(rs.getDouble("column_value"));
                        } else {
                            this.parameter = rs.getObject("column_value");
                        }
                    } else if ("str".equals(value_type)) {
                        this.parameter = "";
                    } else if ("int".equals(value_type)) {
                        this.parameter = 0;
                    } else if ("float".equals(value_type)) {
                        this.parameter = Double.valueOf(0.0d);
                    } else if (value_type.equals("double")) {
                        this.parameter = Double.valueOf(0.0d);
                    } else {
                        this.parameter = null;
                    }
                }
            };
            getValue.execute(poolName);
            return getValue.parameter;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getServerDate() {
        try {
            ConnectionManager getDate = new ConnectionManager() {
                public void process(Connection con) throws Exception {
                    ResultSet rs = con.createStatement().executeQuery("select to_char(sysdate,'yyyy-MM-dd') as server_date from dual");
                    if (rs.next()) {
                        this.parameter = rs.getString("server_date");
                    } else {
                        this.parameter = "";
                    }
                }
            };
            getDate.execute();
            return (String) getDate.parameter;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getServerDate(String poolName) {
        try {
            ConnectionManager getDate = new ConnectionManager() {
                public void process(Connection con) throws Exception {
                    ResultSet rs = con.createStatement().executeQuery("select to_char(sysdate,'yyyy-MM-dd') as server_date from dual");
                    if (rs.next()) {
                        this.parameter = rs.getString("server_date");
                    } else {
                        this.parameter = "";
                    }
                }
            };
            getDate.execute(poolName);
            return (String) getDate.parameter;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getServerDateTime() {
        try {
            ConnectionManager getDateTime = new ConnectionManager() {
                public void process(Connection con) throws Exception {
                    ResultSet rs = con.createStatement().executeQuery("select to_char(sysdate,'yyyy-MM-dd hh:mm:ss') as server_datetime from dual");
                    if (rs.next()) {
                        this.parameter = rs.getString("server_datetime");
                    } else {
                        this.parameter = "";
                    }
                }
            };
            getDateTime.execute();
            return (String) getDateTime.parameter;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getServerDateTime(String poolName) {
        try {
            ConnectionManager getDateTime = new ConnectionManager() {
                public void process(Connection con) throws Exception {
                    ResultSet rs = con.createStatement().executeQuery("select to_char(sysdate,'yyyy-MM-dd hh:mm:ss') as server_datetime from dual");
                    if (rs.next()) {
                        this.parameter = rs.getString("server_datetime");
                    } else {
                        this.parameter = "";
                    }
                }
            };
            getDateTime.execute(poolName);
            return (String) getDateTime.parameter;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String get_format_server_dt(String dt_format) {
        return new SimpleDateFormat(dt_format).format(new Date());
    }

    public static String get_format_db_dt(String date_format) {
        return new SimpleDateFormat(date_format).format(new Date());
    }

    public static String convertTimestampToString(Timestamp timestamp, String pattern) {
        return new SimpleDateFormat(pattern).format(timestamp);
    }

    public static String convert_dt_format(String dt, String old_format, String new_format) {
        try {
            return new SimpleDateFormat(new_format).format(new SimpleDateFormat(old_format).parse(dt));
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getFormatDatetime(String p_format, String p_datetime) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(p_format);
        return sdf.format(sdf.parse(p_datetime)).toString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0033  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int get_date_order(final int r5) {
        /*
            r2 = 0
            com.kenfor.tools.hibernate.DBRecord$9 r3 = new com.kenfor.tools.hibernate.DBRecord$9     // Catch:{ Exception -> 0x0024 }
            r3.<init>(r5)     // Catch:{ Exception -> 0x0024 }
            r3.execute()     // Catch:{ Exception -> 0x003b, all -> 0x0038 }
            java.lang.Object r4 = r3.parameter     // Catch:{ Exception -> 0x003b, all -> 0x0038 }
            if (r4 != 0) goto L_0x0015
            if (r3 == 0) goto L_0x0040
            r3.release()
            r2 = 0
        L_0x0013:
            r0 = 1
        L_0x0014:
            return r0
        L_0x0015:
            java.lang.Object r4 = r3.parameter     // Catch:{ Exception -> 0x003b, all -> 0x0038 }
            java.lang.Integer r4 = (java.lang.Integer) r4     // Catch:{ Exception -> 0x003b, all -> 0x0038 }
            int r0 = r4.intValue()     // Catch:{ Exception -> 0x003b, all -> 0x0038 }
            if (r3 == 0) goto L_0x003e
            r3.release()
            r2 = 0
            goto L_0x0014
        L_0x0024:
            r1 = move-exception
        L_0x0025:
            r1.printStackTrace()     // Catch:{ all -> 0x0030 }
            if (r2 == 0) goto L_0x002e
            r2.release()
            r2 = 0
        L_0x002e:
            r0 = 0
            goto L_0x0014
        L_0x0030:
            r4 = move-exception
        L_0x0031:
            if (r2 == 0) goto L_0x0037
            r2.release()
            r2 = 0
        L_0x0037:
            throw r4
        L_0x0038:
            r4 = move-exception
            r2 = r3
            goto L_0x0031
        L_0x003b:
            r1 = move-exception
            r2 = r3
            goto L_0x0025
        L_0x003e:
            r2 = r3
            goto L_0x0014
        L_0x0040:
            r2 = r3
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenfor.tools.hibernate.DBRecord.get_date_order(int):int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0033  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int get_date_order(final int r5, java.lang.String r6) {
        /*
            r2 = 0
            com.kenfor.tools.hibernate.DBRecord$10 r3 = new com.kenfor.tools.hibernate.DBRecord$10     // Catch:{ Exception -> 0x0024 }
            r3.<init>(r5)     // Catch:{ Exception -> 0x0024 }
            r3.execute(r6)     // Catch:{ Exception -> 0x003b, all -> 0x0038 }
            java.lang.Object r4 = r3.parameter     // Catch:{ Exception -> 0x003b, all -> 0x0038 }
            if (r4 != 0) goto L_0x0015
            if (r3 == 0) goto L_0x0040
            r3.release()
            r2 = 0
        L_0x0013:
            r0 = 1
        L_0x0014:
            return r0
        L_0x0015:
            java.lang.Object r4 = r3.parameter     // Catch:{ Exception -> 0x003b, all -> 0x0038 }
            java.lang.Integer r4 = (java.lang.Integer) r4     // Catch:{ Exception -> 0x003b, all -> 0x0038 }
            int r0 = r4.intValue()     // Catch:{ Exception -> 0x003b, all -> 0x0038 }
            if (r3 == 0) goto L_0x003e
            r3.release()
            r2 = 0
            goto L_0x0014
        L_0x0024:
            r1 = move-exception
        L_0x0025:
            r1.printStackTrace()     // Catch:{ all -> 0x0030 }
            if (r2 == 0) goto L_0x002e
            r2.release()
            r2 = 0
        L_0x002e:
            r0 = 0
            goto L_0x0014
        L_0x0030:
            r4 = move-exception
        L_0x0031:
            if (r2 == 0) goto L_0x0037
            r2.release()
            r2 = 0
        L_0x0037:
            throw r4
        L_0x0038:
            r4 = move-exception
            r2 = r3
            goto L_0x0031
        L_0x003b:
            r1 = move-exception
            r2 = r3
            goto L_0x0025
        L_0x003e:
            r2 = r3
            goto L_0x0014
        L_0x0040:
            r2 = r3
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenfor.tools.hibernate.DBRecord.get_date_order(int, java.lang.String):int");
    }

    public static String get_urlencode_str(String old_str) {
        if (old_str == null || old_str.length() == 0) {
            return "";
        }
        try {
            return URLEncoder.encode(old_str, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static int get_url_integer(String id) {
        if (id == null || id.length() == 0) {
            return 0;
        }
        try {
            return Integer.parseInt(id);
        } catch (Exception e) {
            return 0;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0017  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean delRecord(final java.lang.String r4, final java.lang.String r5, final java.lang.String r6) {
        /*
            r0 = 0
            com.kenfor.tools.hibernate.DBRecord$11 r1 = new com.kenfor.tools.hibernate.DBRecord$11     // Catch:{ Exception -> 0x0011 }
            r1.<init>(r4, r5, r6)     // Catch:{ Exception -> 0x0011 }
            r1.execute()     // Catch:{ Exception -> 0x0028, all -> 0x0025 }
            if (r1 == 0) goto L_0x002b
            r1.release()
            r0 = 0
        L_0x000f:
            r3 = 1
        L_0x0010:
            return r3
        L_0x0011:
            r2 = move-exception
        L_0x0012:
            r2.printStackTrace()     // Catch:{ all -> 0x001d }
            if (r0 == 0) goto L_0x001b
            r0.release()
            r0 = 0
        L_0x001b:
            r3 = 0
            goto L_0x0010
        L_0x001d:
            r3 = move-exception
        L_0x001e:
            if (r0 == 0) goto L_0x0024
            r0.release()
            r0 = 0
        L_0x0024:
            throw r3
        L_0x0025:
            r3 = move-exception
            r0 = r1
            goto L_0x001e
        L_0x0028:
            r2 = move-exception
            r0 = r1
            goto L_0x0012
        L_0x002b:
            r0 = r1
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenfor.tools.hibernate.DBRecord.delRecord(java.lang.String, java.lang.String, java.lang.String):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0017  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean delRecord(final java.lang.String r4, final java.lang.String r5, final java.lang.String r6, java.lang.String r7) {
        /*
            r0 = 0
            com.kenfor.tools.hibernate.DBRecord$12 r1 = new com.kenfor.tools.hibernate.DBRecord$12     // Catch:{ Exception -> 0x0011 }
            r1.<init>(r4, r5, r6)     // Catch:{ Exception -> 0x0011 }
            r1.execute(r7)     // Catch:{ Exception -> 0x0028, all -> 0x0025 }
            if (r1 == 0) goto L_0x002b
            r1.release()
            r0 = 0
        L_0x000f:
            r3 = 1
        L_0x0010:
            return r3
        L_0x0011:
            r2 = move-exception
        L_0x0012:
            r2.printStackTrace()     // Catch:{ all -> 0x001d }
            if (r0 == 0) goto L_0x001b
            r0.release()
            r0 = 0
        L_0x001b:
            r3 = 0
            goto L_0x0010
        L_0x001d:
            r3 = move-exception
        L_0x001e:
            if (r0 == 0) goto L_0x0024
            r0.release()
            r0 = 0
        L_0x0024:
            throw r3
        L_0x0025:
            r3 = move-exception
            r0 = r1
            goto L_0x001e
        L_0x0028:
            r2 = move-exception
            r0 = r1
            goto L_0x0012
        L_0x002b:
            r0 = r1
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenfor.tools.hibernate.DBRecord.delRecord(java.lang.String, java.lang.String, java.lang.String, java.lang.String):boolean");
    }

    public static String get_url_uri(String url) {
        return get_urlencode_str(url.substring(url.indexOf("/", 8), url.length()));
    }

    public static boolean checkNUM(String num_str) {
        if (num_str == null || num_str.length() == 0) {
            return false;
        }
        try {
            Float.parseFloat(num_str);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static ActionForward showInfo(HttpServletResponse response, String info_str, String url) {
        PrintWriter pout = null;
        try {
            response.setContentType("text/html;charset=UTF-8");
            pout = response.getWriter();
            pout.print("<script charset=\"UTF-8\" >alert('" + info_str + "');location.href='" + url + "';</script>");
            if (pout == null) {
                return null;
            }
            pout.close();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            if (pout == null) {
                return null;
            }
            pout.close();
            return null;
        } catch (Throwable th) {
            if (pout != null) {
                pout.close();
            }
            throw th;
        }
    }

    public static String round(String num_str, int scale, int round_mode) {
        if (scale >= 0) {
            return new BigDecimal(num_str).setScale(scale, round_mode).toString();
        }
        throw new IllegalArgumentException("The scale must be a positive integer or zero");
    }

    public static double round2(double d, int scale) {
        long temp = 1;
        for (int i = scale; i > 0; i--) {
            temp *= 10;
        }
        return ((double) Math.round(d * ((double) temp))) / ((double) temp);
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:15:0x0059=Splitter:B:15:0x0059, B:8:0x004b=Splitter:B:8:0x004b} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String iniXmlConstant(java.lang.String r10, java.lang.String r11, org.apache.struts.action.ActionServlet r12) throws java.lang.Exception {
        /*
            javax.servlet.ServletContext r8 = r12.getServletContext()
            java.lang.String r9 = "/"
            java.lang.String r5 = r8.getRealPath(r9)
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            java.lang.String r9 = java.lang.String.valueOf(r5)
            r8.<init>(r9)
            java.lang.String r9 = "WEB-INF/classes/"
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r5 = r8.toString()
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            java.lang.String r9 = java.lang.String.valueOf(r5)
            r8.<init>(r9)
            java.lang.StringBuilder r8 = r8.append(r10)
            java.lang.String r5 = r8.toString()
            r3 = 0
            r6 = 0
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ JDOMException -> 0x004a, NullPointerException -> 0x0058, Exception -> 0x0061 }
            r4.<init>(r5)     // Catch:{ JDOMException -> 0x004a, NullPointerException -> 0x0058, Exception -> 0x0061 }
            org.jdom.input.SAXBuilder r1 = new org.jdom.input.SAXBuilder     // Catch:{ JDOMException -> 0x0073, NullPointerException -> 0x0070, Exception -> 0x006d, all -> 0x006a }
            r1.<init>()     // Catch:{ JDOMException -> 0x0073, NullPointerException -> 0x0070, Exception -> 0x006d, all -> 0x006a }
            org.jdom.Document r0 = r1.build(r4)     // Catch:{ JDOMException -> 0x0073, NullPointerException -> 0x0070, Exception -> 0x006d, all -> 0x006a }
            org.jdom.Element r7 = r0.getRootElement()     // Catch:{ JDOMException -> 0x0073, NullPointerException -> 0x0070, Exception -> 0x006d, all -> 0x006a }
            java.lang.String r6 = r7.getChildTextTrim(r11)     // Catch:{ JDOMException -> 0x0073, NullPointerException -> 0x0070, Exception -> 0x006d, all -> 0x006a }
            r4.close()
            return r6
        L_0x004a:
            r2 = move-exception
        L_0x004b:
            java.lang.Exception r8 = new java.lang.Exception     // Catch:{ all -> 0x0053 }
            java.lang.String r9 = "JDOM 鍑虹幇寮傚父"
            r8.<init>(r9)     // Catch:{ all -> 0x0053 }
            throw r8     // Catch:{ all -> 0x0053 }
        L_0x0053:
            r8 = move-exception
        L_0x0054:
            r3.close()
            throw r8
        L_0x0058:
            r2 = move-exception
        L_0x0059:
            java.lang.Exception r8 = new java.lang.Exception     // Catch:{ all -> 0x0053 }
            java.lang.String r9 = "澶勭悊JDOM鏃跺嚭鐜板紓甯�"
            r8.<init>(r9)     // Catch:{ all -> 0x0053 }
            throw r8     // Catch:{ all -> 0x0053 }
        L_0x0061:
            r2 = move-exception
        L_0x0062:
            java.lang.Exception r8 = new java.lang.Exception     // Catch:{ all -> 0x0053 }
            java.lang.String r9 = "澶勭悊JDOM鏃跺嚭鐜板紓甯�"
            r8.<init>(r9)     // Catch:{ all -> 0x0053 }
            throw r8     // Catch:{ all -> 0x0053 }
        L_0x006a:
            r8 = move-exception
            r3 = r4
            goto L_0x0054
        L_0x006d:
            r2 = move-exception
            r3 = r4
            goto L_0x0062
        L_0x0070:
            r2 = move-exception
            r3 = r4
            goto L_0x0059
        L_0x0073:
            r2 = move-exception
            r3 = r4
            goto L_0x004b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenfor.tools.hibernate.DBRecord.iniXmlConstant(java.lang.String, java.lang.String, org.apache.struts.action.ActionServlet):java.lang.String");
    }

    public static String getPassword(String old_str) {
        int length = old_str.length();
        char[] str_char = old_str.toCharArray();
        for (int i = 0; i < length; i++) {
            str_char[i] = (char) ((((str_char[i] * 2) + i) - length) % 128);
        }
        return String.valueOf(str_char);
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:15:0x003a=Splitter:B:15:0x003a, B:8:0x002c=Splitter:B:8:0x002c} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String iniXmlConstant(java.lang.String r10, java.lang.String r11) throws java.lang.Exception {
        /*
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            java.lang.String r9 = "/WEB-INF/classes/"
            r8.<init>(r9)
            java.lang.StringBuilder r8 = r8.append(r10)
            java.lang.String r5 = r8.toString()
            r3 = 0
            r6 = 0
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ JDOMException -> 0x002b, NullPointerException -> 0x0039, Exception -> 0x0042 }
            r4.<init>(r5)     // Catch:{ JDOMException -> 0x002b, NullPointerException -> 0x0039, Exception -> 0x0042 }
            org.jdom.input.SAXBuilder r1 = new org.jdom.input.SAXBuilder     // Catch:{ JDOMException -> 0x0054, NullPointerException -> 0x0051, Exception -> 0x004e, all -> 0x004b }
            r1.<init>()     // Catch:{ JDOMException -> 0x0054, NullPointerException -> 0x0051, Exception -> 0x004e, all -> 0x004b }
            org.jdom.Document r0 = r1.build(r4)     // Catch:{ JDOMException -> 0x0054, NullPointerException -> 0x0051, Exception -> 0x004e, all -> 0x004b }
            org.jdom.Element r7 = r0.getRootElement()     // Catch:{ JDOMException -> 0x0054, NullPointerException -> 0x0051, Exception -> 0x004e, all -> 0x004b }
            java.lang.String r6 = r7.getChildTextTrim(r11)     // Catch:{ JDOMException -> 0x0054, NullPointerException -> 0x0051, Exception -> 0x004e, all -> 0x004b }
            r4.close()
            return r6
        L_0x002b:
            r2 = move-exception
        L_0x002c:
            java.lang.Exception r8 = new java.lang.Exception     // Catch:{ all -> 0x0034 }
            java.lang.String r9 = "JDOM 出现异常"
            r8.<init>(r9)     // Catch:{ all -> 0x0034 }
            throw r8     // Catch:{ all -> 0x0034 }
        L_0x0034:
            r8 = move-exception
        L_0x0035:
            r3.close()
            throw r8
        L_0x0039:
            r2 = move-exception
        L_0x003a:
            java.lang.Exception r8 = new java.lang.Exception     // Catch:{ all -> 0x0034 }
            java.lang.String r9 = "处理JDOM时出现异常"
            r8.<init>(r9)     // Catch:{ all -> 0x0034 }
            throw r8     // Catch:{ all -> 0x0034 }
        L_0x0042:
            r2 = move-exception
        L_0x0043:
            java.lang.Exception r8 = new java.lang.Exception     // Catch:{ all -> 0x0034 }
            java.lang.String r9 = "处理JDOM时出现异常"
            r8.<init>(r9)     // Catch:{ all -> 0x0034 }
            throw r8     // Catch:{ all -> 0x0034 }
        L_0x004b:
            r8 = move-exception
            r3 = r4
            goto L_0x0035
        L_0x004e:
            r2 = move-exception
            r3 = r4
            goto L_0x0043
        L_0x0051:
            r2 = move-exception
            r3 = r4
            goto L_0x003a
        L_0x0054:
            r2 = move-exception
            r3 = r4
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenfor.tools.hibernate.DBRecord.iniXmlConstant(java.lang.String, java.lang.String):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0036  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:9:0x002b=Splitter:B:9:0x002b, B:17:0x003b=Splitter:B:17:0x003b} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getDBPool(java.lang.String r10, java.lang.String r11) throws java.lang.Exception {
        /*
            r5 = r10
            r3 = 0
            r6 = 0
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ JDOMException -> 0x002a, NullPointerException -> 0x003a, Exception -> 0x0043 }
            r4.<init>(r5)     // Catch:{ JDOMException -> 0x002a, NullPointerException -> 0x003a, Exception -> 0x0043 }
            org.jdom.input.SAXBuilder r1 = new org.jdom.input.SAXBuilder     // Catch:{ JDOMException -> 0x0055, NullPointerException -> 0x0052, Exception -> 0x004f, all -> 0x004c }
            r1.<init>()     // Catch:{ JDOMException -> 0x0055, NullPointerException -> 0x0052, Exception -> 0x004f, all -> 0x004c }
            org.jdom.Document r0 = r1.build(r4)     // Catch:{ JDOMException -> 0x0055, NullPointerException -> 0x0052, Exception -> 0x004f, all -> 0x004c }
            org.jdom.Element r7 = r0.getRootElement()     // Catch:{ JDOMException -> 0x0055, NullPointerException -> 0x0052, Exception -> 0x004f, all -> 0x004c }
            java.util.List r8 = r7.getChildren()     // Catch:{ JDOMException -> 0x0055, NullPointerException -> 0x0052, Exception -> 0x004f, all -> 0x004c }
            r9 = 0
            java.lang.Object r8 = r8.get(r9)     // Catch:{ JDOMException -> 0x0055, NullPointerException -> 0x0052, Exception -> 0x004f, all -> 0x004c }
            org.jdom.Element r8 = (org.jdom.Element) r8     // Catch:{ JDOMException -> 0x0055, NullPointerException -> 0x0052, Exception -> 0x004f, all -> 0x004c }
            java.lang.String r6 = r8.getChildTextTrim(r11)     // Catch:{ JDOMException -> 0x0055, NullPointerException -> 0x0052, Exception -> 0x004f, all -> 0x004c }
            if (r4 == 0) goto L_0x0029
            r4.close()
        L_0x0029:
            return r6
        L_0x002a:
            r2 = move-exception
        L_0x002b:
            java.lang.Exception r8 = new java.lang.Exception     // Catch:{ all -> 0x0033 }
            java.lang.String r9 = "JDOM 出现异常"
            r8.<init>(r9)     // Catch:{ all -> 0x0033 }
            throw r8     // Catch:{ all -> 0x0033 }
        L_0x0033:
            r8 = move-exception
        L_0x0034:
            if (r3 == 0) goto L_0x0039
            r3.close()
        L_0x0039:
            throw r8
        L_0x003a:
            r2 = move-exception
        L_0x003b:
            java.lang.Exception r8 = new java.lang.Exception     // Catch:{ all -> 0x0033 }
            java.lang.String r9 = "处理JDOM时出现异常"
            r8.<init>(r9)     // Catch:{ all -> 0x0033 }
            throw r8     // Catch:{ all -> 0x0033 }
        L_0x0043:
            r2 = move-exception
        L_0x0044:
            java.lang.Exception r8 = new java.lang.Exception     // Catch:{ all -> 0x0033 }
            java.lang.String r9 = "处理JDOM时出现异常"
            r8.<init>(r9)     // Catch:{ all -> 0x0033 }
            throw r8     // Catch:{ all -> 0x0033 }
        L_0x004c:
            r8 = move-exception
            r3 = r4
            goto L_0x0034
        L_0x004f:
            r2 = move-exception
            r3 = r4
            goto L_0x0044
        L_0x0052:
            r2 = move-exception
            r3 = r4
            goto L_0x003b
        L_0x0055:
            r2 = move-exception
            r3 = r4
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenfor.tools.hibernate.DBRecord.getDBPool(java.lang.String, java.lang.String):java.lang.String");
    }

    public static String get_tax_office_no(String old_tax_office_no) {
        if (old_tax_office_no == null || old_tax_office_no.length() == 0) {
            return "";
        }
        String new_str = old_tax_office_no;
        if (old_tax_office_no.length() == 9) {
            while (new_str.substring(new_str.length() - 2).equals("00")) {
                new_str = new_str.substring(0, new_str.length() - 2);
            }
            return new_str;
        } else if (old_tax_office_no.length() != 11) {
            return new_str;
        } else {
            if (!old_tax_office_no.substring(old_tax_office_no.length() - 4).substring(0, 2).equals("00")) {
                return old_tax_office_no;
            }
            String new_str2 = old_tax_office_no.substring(0, old_tax_office_no.length() - 4);
            while (new_str2.substring(new_str2.length() - 2).equals("00")) {
                new_str2 = new_str2.substring(0, new_str2.length() - 2);
            }
            return new_str2;
        }
    }

    public static String getMonthFirstDate() {
        return String.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(new Date()).substring(0, 8)) + "01";
    }

    public static String getMonthCurrentDate() {
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    }

    public static double formatBigDouble(double num, int format_int) {
        return Double.valueOf(new BigDecimal(num).setScale(format_int, 4).doubleValue()).doubleValue();
    }

    public static double formatDouble(double num) {
        return Double.valueOf(new DecimalFormat("#.00").format(num)).doubleValue();
    }

    public static String convertDoubleToString(double num) {
        return new DecimalFormat("###,###,###,###,##0.00").format(num);
    }

    public static String convertIntToString(double num) {
        return new DecimalFormat("###,###,###,###,##0").format(num);
    }
}
