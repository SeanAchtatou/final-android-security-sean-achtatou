package com.kenfor.tools.hibernate;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import org.apache.log4j.Logger;

public class Recycle {
    private static Logger log = Logger.getLogger("Recyle");

    public static final void cleanup(Statement stmt) {
        if (stmt != null) {
            try {
                stmt.close();
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        }
    }

    private static final void cleanup(PreparedStatement pstmt) {
        if (pstmt != null) {
            try {
                pstmt.close();
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        }
    }

    public static final void cleanup(Connection conn) {
        if (conn != null) {
            try {
                if (!conn.isClosed()) {
                    conn.close();
                    if (!conn.isClosed()) {
                        log.error("��ݿ⻹��û�йر�");
                    }
                }
            } catch (Exception e) {
                log.error(e.getMessage());
                e.printStackTrace();
            }
        }
    }

    private static final void cleanup(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (Exception ex) {
                log.error(ex.getMessage());
            }
        }
    }

    public static final void cleanup(Statement stmt, ResultSet rs) {
        cleanup(rs);
        cleanup(stmt);
    }

    public static final void cleanup(Statement stmt, Connection conn) {
        cleanup(stmt);
        cleanup(conn);
    }

    public static final void cleanup(PreparedStatement pstmt, Connection conn) {
        cleanup(pstmt);
        cleanup(conn);
    }

    public static final void cleanup(Statement stmt, Connection conn, ResultSet rs) {
        cleanup(rs);
        cleanup(stmt, conn);
    }

    public static final void cleanup(PreparedStatement pstmt, Connection conn, ResultSet rs) {
        cleanup(rs);
        cleanup(pstmt, conn);
    }
}
