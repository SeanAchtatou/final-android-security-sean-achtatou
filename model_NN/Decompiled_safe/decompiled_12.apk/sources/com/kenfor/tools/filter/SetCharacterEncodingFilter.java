package com.kenfor.tools.filter;

import com.kenfor.util.CodeTransfer;
import com.kenfor.util.MyUtil;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.xbill.DNS.KEYRecord;

public class SetCharacterEncodingFilter implements Filter {
    protected String encoding = null;
    protected int fileMaxLength = 204800;
    protected FilterConfig filterConfig = null;
    protected boolean ignore = true;
    Log log = LogFactory.getLog(super.getClass().getName());

    public void destroy() {
        this.encoding = null;
        this.filterConfig = null;
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String encoding2;
        HttpServletRequest t_re = (HttpServletRequest) request;
        String re_uri = trimString(t_re.getRequestURI());
        String re_host = trimString(t_re.getServerName());
        String qu_str = trimString(t_re.getQueryString());
        if (re_uri.indexOf(".do") > 0 && this.log.isInfoEnabled()) {
            this.log.info(String.valueOf(re_host) + re_uri + "?" + qu_str);
        }
        if ((this.ignore || request.getCharacterEncoding() == null) && (encoding2 = selectEncoding(request)) != null) {
            request.setCharacterEncoding(encoding2);
        }
        chain.doFilter(request, response);
    }

    public void init(FilterConfig filterConfig2) throws ServletException {
        this.filterConfig = filterConfig2;
        this.encoding = filterConfig2.getInitParameter("encoding");
        String value = filterConfig2.getInitParameter("ignore");
        String file_max_length = filterConfig2.getInitParameter("file_max_length");
        if (file_max_length != null && file_max_length.length() > 0) {
            this.fileMaxLength = MyUtil.getStringToInt(file_max_length, 200) * KEYRecord.Flags.FLAG5;
        }
        if (value == null) {
            this.ignore = true;
        } else if (value.equalsIgnoreCase("true")) {
            this.ignore = true;
        } else if (value.equalsIgnoreCase("yes")) {
            this.ignore = true;
        } else {
            this.ignore = false;
        }
    }

    /* access modifiers changed from: protected */
    public String selectEncoding(ServletRequest request) {
        return this.encoding;
    }

    private String trimString(String value) {
        if (value != null) {
            return value.trim();
        }
        return "";
    }

    /* access modifiers changed from: package-private */
    public String createErrorMessage(String message, String referer) {
        if (message == null) {
            message = "";
        }
        return String.valueOf(String.valueOf("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"><BODY>\n<SCRIPT LANGUAGE=\"JavaScript\">\n<!--\nalert(\"" + message + "\");\n") + "history.go(-1);\n") + "//--></SCRIPT>\n</BODY>";
    }

    /* access modifiers changed from: protected */
    public void writeErrorMessage(HttpServletResponse httpServletResponse, String message) {
        try {
            httpServletResponse.setHeader("accept-language", "UTF-8");
            PrintWriter write = httpServletResponse.getWriter();
            write.write(CodeTransfer.UnicodeToISO(message));
            write.flush();
            write.close();
        } catch (Exception e) {
            this.log.error(e.getMessage());
        }
    }
}
