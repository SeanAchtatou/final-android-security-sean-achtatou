package com.kenfor.tools.datetime;

/* compiled from: SolarLunar */
class Calendar {
    private static final char[] iLunarLeapMonthTable;
    private static final int[] iLunarMonthDaysTable = {19168, 42352, 21096, 53856, 55632, 27304, 22176, 39632, 19176, 19168, 42200, 42192, 53840, 54600, 46416, 22176, 38608, 38320, 18872, 18864, 42160, 45656, 27216, 27968, 44456, 11104, 38256, 18808, 18800, 25776, 54432, 59984, 27976, 23248, 11104, 37744, 37600, 51560, 51536, 54432, 55888, 46416, 22176, 43736, 9680, 37584, 51544, 43344, 46248, 27808, 46416, 21928, 19872, 42416, 21176, 21168, 43344, 59728, 27296, 44368, 43856, 19296, 42352, 42352, 21088, 59696, 55632, 23208, 22176, 38608, 19176, 19152, 42192, 53864, 53840, 54568, 46400, 46752, 38608, 38320, 18864, 42168, 42160, 45656, 27216, 27968, 44448, 43872, 37744, 18808, 18800, 25776, 27216, 59984, 27432, 23232, 43872, 37736, 37600, 51552, 54440, 54432, 55888, 23208, 22176, 43736, 9680, 37584, 51544, 43344, 46240, 46416, 46416, 21928, 19360, 42416, 21176, 21168, 43312, 29864, 27296, 44368, 19880, 19296, 38256, 42208, 53856, 59696, 54576, 23200, 27472, 38608, 19176, 19152, 42192, 53848, 53840, 54560, 55968, 46496, 22224, 19160, 18864, 42168, 42160, 43600, 46376, 27936, 44448, 21936};
    private static final char[] iSolarLunarOffsetTable = {'1', '&', 28, '.', '\"', 24, '+', ' ', 21, '(', 29, '0', '$', 25, ',', '\"', 22, ')', 31, '2', '&', 27, '.', '#', 23, '+', ' ', 22, '(', 29, '/', '$', 25, ',', '\"', 23, ')', 30, '1', '&', 26, '-', '#', 24, '+', ' ', 21, '(', 28, '/', '$', 26, ',', '!', 23, '*', 30, '0', '&', 27, '-', '#', 24, '+', ' ', 20, '\'', 29, '/', '$', 26, '-', '!', 22, ')', 30, '0', '%', 27, '.', '#', 24, '+', ' ', '2', '\'', 28, '/', '$', 26, '-', '\"', 22, '(', 30, '1', '%', 27, '.', '#', 23, '*', 31, 21, '\'', 28, '0', '%', 25, ',', '!', 23, ')', 31, '2', '\'', 28, '/', '#', 24, '*', 30, 21, '(', 28, '/', '$', 25, '+', '!', 22, ')', 30, '1', '%', 26, ',', '!', 23, '*', 31, 21, '(', 29, '/', '$', 25, ',', ' ', 22};

    Calendar() {
    }

    static {
        char[] cArr = new char[75];
        cArr[1] = 'P';
        cArr[2] = 4;
        cArr[4] = ' ';
        cArr[5] = '`';
        cArr[6] = 5;
        cArr[8] = ' ';
        cArr[9] = 'p';
        cArr[10] = 5;
        cArr[12] = '@';
        cArr[13] = 2;
        cArr[14] = 6;
        cArr[16] = 'P';
        cArr[17] = 3;
        cArr[18] = 7;
        cArr[20] = '`';
        cArr[21] = 4;
        cArr[23] = ' ';
        cArr[24] = 'p';
        cArr[25] = 5;
        cArr[27] = '0';
        cArr[28] = 128;
        cArr[29] = 6;
        cArr[31] = '@';
        cArr[32] = 3;
        cArr[33] = 7;
        cArr[35] = 'P';
        cArr[36] = 4;
        cArr[37] = 8;
        cArr[39] = '`';
        cArr[40] = 4;
        cArr[41] = 10;
        cArr[43] = '`';
        cArr[44] = 5;
        cArr[46] = '0';
        cArr[47] = 128;
        cArr[48] = 5;
        cArr[50] = '@';
        cArr[51] = 2;
        cArr[52] = 7;
        cArr[54] = 'P';
        cArr[55] = 4;
        cArr[56] = 9;
        cArr[58] = '`';
        cArr[59] = 4;
        cArr[61] = ' ';
        cArr[62] = '`';
        cArr[63] = 5;
        cArr[65] = '0';
        cArr[66] = 176;
        cArr[67] = 6;
        cArr[69] = 'P';
        cArr[70] = 2;
        cArr[71] = 7;
        cArr[73] = 'P';
        cArr[74] = 3;
        iLunarLeapMonthTable = cArr;
    }

    static boolean bIsSolarLeapYear(int iYear) {
        return (iYear % 4 == 0 && iYear % 100 != 0) || iYear % 400 == 0;
    }

    static int iGetSYearMonthDays(int iYear, int iMonth) {
        if (iMonth == 1 || iMonth == 3 || iMonth == 5 || iMonth == 7 || iMonth == 8 || iMonth == 10 || iMonth == 12) {
            return 31;
        }
        if (iMonth == 4 || iMonth == 6 || iMonth == 9 || iMonth == 11) {
            return 30;
        }
        if (iMonth != 2) {
            return 0;
        }
        if (bIsSolarLeapYear(iYear)) {
            return 29;
        }
        return 28;
    }

    static int iGetSNewYearOffsetDays(int iYear, int iMonth, int iDay) {
        int iOffsetDays = 0;
        for (int i = 1; i < iMonth; i++) {
            iOffsetDays += iGetSYearMonthDays(iYear, i);
        }
        return iOffsetDays + (iDay - 1);
    }

    static int iGetLLeapMonth(int iYear) {
        char iMonth = iLunarLeapMonthTable[(iYear - 1901) / 2];
        if (iYear % 2 == 0) {
            return iMonth & 15;
        }
        return (iMonth & 240) >> 4;
    }

    static int iGetLMonthDays(int iYear, int iMonth) {
        int iLeapMonth = iGetLLeapMonth(iYear);
        if ((iMonth > 12 && iMonth - 12 != iLeapMonth) || iMonth < 0) {
            System.out.println("Wrong month, ^_^ , i think you are want a -1, go to death!");
            return -1;
        } else if (iMonth - 12 != iLeapMonth) {
            if (iLeapMonth > 0 && iMonth > iLeapMonth) {
                iMonth++;
            }
            if ((iLunarMonthDaysTable[iYear - 1901] & (32768 >> (iMonth - 1))) != 0) {
                return 30;
            }
            return 29;
        } else if ((iLunarMonthDaysTable[iYear - 1901] & (32768 >> iLeapMonth)) != 0) {
            return 30;
        } else {
            return 29;
        }
    }

    static int iGetLYearDays(int iYear) {
        int iYearDays = 0;
        int iLeapMonth = iGetLLeapMonth(iYear);
        for (int i = 1; i < 13; i++) {
            iYearDays += iGetLMonthDays(iYear, i);
        }
        if (iLeapMonth > 0) {
            return iYearDays + iGetLMonthDays(iYear, iLeapMonth + 12);
        }
        return iYearDays;
    }

    static int iGetLNewYearOffsetDays(int iYear, int iMonth, int iDay) {
        int iOffsetDays;
        int iOffsetDays2 = 0;
        int iLeapMonth = iGetLLeapMonth(iYear);
        if (iLeapMonth > 0 && iLeapMonth == iMonth - 12) {
            iMonth = iLeapMonth;
            iOffsetDays2 = 0 + iGetLMonthDays(iYear, iMonth);
        }
        for (int i = 1; i < iMonth; i++) {
            iOffsetDays += iGetLMonthDays(iYear, i);
            if (i == iLeapMonth) {
                iOffsetDays += iGetLMonthDays(iYear, iLeapMonth + 12);
            }
        }
        return iOffsetDays + (iDay - 1);
    }

    static String sCalendarSolarToLundar(int iYear, int iMonth, int iDay) {
        int iLYear;
        int iLDay;
        int iLMonth;
        int iOffsetDays = iGetSNewYearOffsetDays(iYear, iMonth, iDay);
        int iLeapMonth = iGetLLeapMonth(iYear);
        if (iOffsetDays < iSolarLunarOffsetTable[iYear - 1901]) {
            iLYear = iYear - 1;
            int iOffsetDays2 = iSolarLunarOffsetTable[iYear - 1901] - iOffsetDays;
            int iLDay2 = iOffsetDays2;
            iLMonth = 12;
            while (iOffsetDays2 > iGetLMonthDays(iLYear, iLMonth)) {
                iLDay2 = iOffsetDays2;
                iOffsetDays2 -= iGetLMonthDays(iLYear, iLMonth);
                iLMonth--;
            }
            if (iLDay2 == 0) {
                iLDay = 1;
            } else {
                iLDay = (iGetLMonthDays(iLYear, iLMonth) - iOffsetDays2) + 1;
            }
        } else {
            iLYear = iYear;
            int iOffsetDays3 = iOffsetDays - iSolarLunarOffsetTable[iYear - 1901];
            iLDay = iOffsetDays3 + 1;
            int iLMonth2 = 1;
            while (true) {
                if (iOffsetDays3 < 0) {
                    break;
                }
                iLDay = iOffsetDays3 + 1;
                iOffsetDays3 -= iGetLMonthDays(iLYear, iLMonth2);
                if (iLeapMonth == iLMonth2 && iOffsetDays3 > 0) {
                    iLDay = iOffsetDays3;
                    iOffsetDays3 -= iGetLMonthDays(iLYear, iLMonth2 + 12);
                    if (iOffsetDays3 <= 0) {
                        iLMonth2 += 13;
                        break;
                    }
                }
                iLMonth2++;
            }
            iLMonth = iLMonth2 - 1;
        }
        return iLYear + (iLMonth > 9 ? new StringBuilder().append(iLMonth).toString() : "0" + iLMonth) + (iLDay > 9 ? new StringBuilder().append(iLDay).toString() : "0" + iLDay);
    }

    static String sCalendarLundarToSolar(int iYear, int iMonth, int iDay) {
        int iSYear;
        int iOffsetDays = iGetLNewYearOffsetDays(iYear, iMonth, iDay) + iSolarLunarOffsetTable[iYear - 1901];
        int iYearDays = bIsSolarLeapYear(iYear) ? 366 : 365;
        if (iOffsetDays >= iYearDays) {
            iSYear = iYear + 1;
            iOffsetDays -= iYearDays;
        } else {
            iSYear = iYear;
        }
        int iSDay = iOffsetDays + 1;
        int iSMonth = 1;
        while (iOffsetDays >= 0) {
            iSDay = iOffsetDays + 1;
            iOffsetDays -= iGetSYearMonthDays(iSYear, iSMonth);
            iSMonth++;
        }
        int iSMonth2 = iSMonth - 1;
        return iSYear + (iSMonth2 > 9 ? new StringBuilder(String.valueOf(iSMonth2)).toString() : "0" + iSMonth2) + (iSDay > 9 ? new StringBuilder(String.valueOf(iSDay)).toString() : "0" + iSDay);
    }
}
