package com.kenfor.tools.datetime;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateTime {
    private int MaxDate;
    private int MaxYear;
    private int weeks = 0;

    public static String getTwoDay(String sj1, String sj2) {
        SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return new StringBuilder(String.valueOf((myFormatter.parse(sj1).getTime() - myFormatter.parse(sj2).getTime()) / 86400000)).toString();
        } catch (Exception e) {
            return "";
        }
    }

    public static String getWeek(String sdate) {
        Date date = strToDate(sdate);
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return new SimpleDateFormat("EEEE").format(c.getTime());
    }

    public static Date strToDate(String strDate) {
        return new SimpleDateFormat("yyyy-MM-dd").parse(strDate, new ParsePosition(0));
    }

    public static long getDays(String date1, String date2) {
        if (date1 == null || date1.equals("") || date2 == null || date2.equals("")) {
            return 0;
        }
        SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        Date mydate = null;
        try {
            date = myFormatter.parse(date1);
            mydate = myFormatter.parse(date2);
        } catch (Exception e) {
        }
        return (date.getTime() - mydate.getTime()) / 86400000;
    }

    public String getDefaultDay() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar lastDate = Calendar.getInstance();
        lastDate.set(5, 1);
        lastDate.add(2, 1);
        lastDate.add(5, -1);
        return sdf.format(lastDate.getTime());
    }

    public String getPreviousMonthFirst() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar lastDate = Calendar.getInstance();
        lastDate.set(5, 1);
        lastDate.add(2, -1);
        return sdf.format(lastDate.getTime());
    }

    public String getFirstDayOfMonth() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar lastDate = Calendar.getInstance();
        lastDate.set(5, 1);
        return sdf.format(lastDate.getTime());
    }

    public String getCurrentWeekday() {
        this.weeks = 0;
        int mondayPlus = getMondayPlus();
        GregorianCalendar currentDate = new GregorianCalendar();
        currentDate.add(5, mondayPlus + 6);
        return DateFormat.getDateInstance().format(currentDate.getTime());
    }

    public static String getNowTime(String dateformat) {
        return new SimpleDateFormat(dateformat).format(new Date());
    }

    private int getMondayPlus() {
        int dayOfWeek = Calendar.getInstance().get(7) - 1;
        if (dayOfWeek == 1) {
            return 0;
        }
        return 1 - dayOfWeek;
    }

    public String getMondayOFWeek() {
        this.weeks = 0;
        int mondayPlus = getMondayPlus();
        GregorianCalendar currentDate = new GregorianCalendar();
        currentDate.add(5, mondayPlus);
        return DateFormat.getDateInstance().format(currentDate.getTime());
    }

    public String getSaturday() {
        int mondayPlus = getMondayPlus();
        GregorianCalendar currentDate = new GregorianCalendar();
        currentDate.add(5, (this.weeks * 7) + mondayPlus + 6);
        return DateFormat.getDateInstance().format(currentDate.getTime());
    }

    public String getPreviousWeekSunday() {
        this.weeks = 0;
        this.weeks--;
        int mondayPlus = getMondayPlus();
        GregorianCalendar currentDate = new GregorianCalendar();
        currentDate.add(5, this.weeks + mondayPlus);
        return DateFormat.getDateInstance().format(currentDate.getTime());
    }

    public String getPreviousWeekday() {
        this.weeks--;
        int mondayPlus = getMondayPlus();
        GregorianCalendar currentDate = new GregorianCalendar();
        currentDate.add(5, (this.weeks * 7) + mondayPlus);
        return DateFormat.getDateInstance().format(currentDate.getTime());
    }

    public String getNextMonday() {
        this.weeks++;
        int mondayPlus = getMondayPlus();
        GregorianCalendar currentDate = new GregorianCalendar();
        currentDate.add(5, mondayPlus + 7);
        return DateFormat.getDateInstance().format(currentDate.getTime());
    }

    public String getNextSunday() {
        int mondayPlus = getMondayPlus();
        GregorianCalendar currentDate = new GregorianCalendar();
        currentDate.add(5, mondayPlus + 7 + 6);
        return DateFormat.getDateInstance().format(currentDate.getTime());
    }

    public static String getPreviousMonthBegin() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar firstDate = Calendar.getInstance();
        firstDate.set(5, 1);
        firstDate.add(2, -1);
        return sdf.format(firstDate.getTime());
    }

    public static String getPreviousMonthEnd() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar lastDate = Calendar.getInstance();
        lastDate.set(5, 1);
        lastDate.add(5, -1);
        return sdf.format(lastDate.getTime());
    }

    public static String getPreviousMonthBegin(int month_add) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar firstDate = Calendar.getInstance();
        firstDate.set(5, 1);
        firstDate.add(2, month_add * -1);
        return sdf.format(firstDate.getTime());
    }

    public static String getPreviousMonthEnd(int month_add) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar lastDate = Calendar.getInstance();
        lastDate.set(5, 1);
        lastDate.add(2, (month_add - 1) * -1);
        lastDate.add(5, -1);
        return sdf.format(lastDate.getTime());
    }

    public static int getDaysBetween(String str1, String str2) {
        SimpleDateFormat dfs = new SimpleDateFormat("yyyy-MM-dd");
        Date begin = null;
        Date end = null;
        try {
            begin = dfs.parse(str1);
            end = dfs.parse(str2);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return ((int) ((end.getTime() - begin.getTime()) / 1000)) / 86400;
    }

    public String getNextMonthFirst() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar lastDate = Calendar.getInstance();
        lastDate.add(2, 1);
        lastDate.set(5, 1);
        return sdf.format(lastDate.getTime());
    }

    public String getNextMonthEnd() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar lastDate = Calendar.getInstance();
        lastDate.add(2, 1);
        lastDate.set(5, 1);
        lastDate.roll(5, -1);
        return sdf.format(lastDate.getTime());
    }

    public String getNextYearEnd() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar lastDate = Calendar.getInstance();
        lastDate.add(1, 1);
        lastDate.set(6, 1);
        lastDate.roll(6, -1);
        return sdf.format(lastDate.getTime());
    }

    public String getNextYearFirst() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar lastDate = Calendar.getInstance();
        lastDate.add(1, 1);
        lastDate.set(6, 1);
        return sdf.format(lastDate.getTime());
    }

    public String getLastYearSameDay(String strdate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar lastDate = Calendar.getInstance();
        lastDate.setTime(getDateByStr(strdate));
        lastDate.add(1, -1);
        return sdf.format(lastDate.getTime());
    }

    private int getMaxYear() {
        Calendar cd = Calendar.getInstance();
        cd.set(6, 1);
        cd.roll(6, -1);
        return cd.get(6);
    }

    private int getYearPlus() {
        Calendar cd = Calendar.getInstance();
        int yearOfNumber = cd.get(6);
        cd.set(6, 1);
        cd.roll(6, -1);
        int MaxYear2 = cd.get(6);
        if (yearOfNumber == 1) {
            return -MaxYear2;
        }
        return 1 - yearOfNumber;
    }

    public String getCurrentYearFirst() {
        return String.valueOf(new SimpleDateFormat("yyyy").format(new Date())) + "-01-01";
    }

    public String getCurrentYearEnd() {
        return String.valueOf(new SimpleDateFormat("yyyy").format(new Date())) + "-12-31";
    }

    public String getYearEnd(Date date) {
        return String.valueOf(new SimpleDateFormat("yyyy").format(date)) + "-12-31";
    }

    public String getPreviousYearFirst() {
        return String.valueOf(Integer.parseInt(new SimpleDateFormat("yyyy").format(new Date())) - 1) + "-01-01";
    }

    public String getPreviousYearEnd() {
        this.weeks--;
        int yearPlus = getYearPlus();
        GregorianCalendar currentDate = new GregorianCalendar();
        currentDate.add(5, (this.MaxYear * this.weeks) + yearPlus + (this.MaxYear - 1));
        String preYearDay = DateFormat.getDateInstance().format(currentDate.getTime());
        getThisSeasonTime(11);
        return preYearDay;
    }

    public String getThisSeasonTime(int month) {
        int[][] array = {new int[]{1, 2, 3}, new int[]{4, 5, 6}, new int[]{7, 8, 9}, new int[]{10, 11, 12}};
        int season = 1;
        if (month >= 1 && month <= 3) {
            season = 1;
        }
        if (month >= 4 && month <= 6) {
            season = 2;
        }
        if (month >= 7 && month <= 9) {
            season = 3;
        }
        if (month >= 10 && month <= 12) {
            season = 4;
        }
        int start_month = array[season - 1][0];
        int end_month = array[season - 1][2];
        int years_value = Integer.parseInt(new SimpleDateFormat("yyyy").format(new Date()));
        return String.valueOf(years_value) + "-" + start_month + "-" + 1 + ";" + years_value + "-" + end_month + "-" + getLastDayOfMonth(years_value, end_month);
    }

    private int getLastDayOfMonth(int year, int month) {
        if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
            return 31;
        }
        if (month == 4 || month == 6 || month == 9 || month == 11) {
            return 30;
        }
        if (month != 2) {
            return 0;
        }
        if (isLeapYear(year)) {
            return 29;
        }
        return 28;
    }

    public boolean isLeapYear(int year) {
        return (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
    }

    public String getFullTime() {
        return DateFormat.getDateInstance(0).format(new Date());
    }

    public Date getDateByStr(String strdate) {
        try {
            return parseFormatDate(strdate, "yyyy-MM-dd");
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getMonthBegin(String strdate) {
        Date date = null;
        try {
            date = parseFormatDate(strdate, "yyyy-MM-dd");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return String.valueOf(formatDateByFormat(date, "yyyy-MM")) + "-01";
    }

    public String getMonthEnd(String strdate) {
        Date date = null;
        try {
            date = parseFormatDate(getMonthBegin(strdate), "yyyy-MM-dd");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(2, 1);
        calendar.add(6, -1);
        return formatDate(calendar.getTime());
    }

    public String getMonthBegin(String strdate, int month_add) {
        Date date = null;
        try {
            Date date2 = parseFormatDate(strdate, "yyyy-MM-dd");
            Calendar firstDate = Calendar.getInstance();
            firstDate.setTime(date2);
            firstDate.set(5, 1);
            firstDate.add(2, month_add * -1);
            date = firstDate.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formatDateByFormat(date, "yyyy-MM-dd");
    }

    public String getMonthEnd(String strdate, int month_add) {
        Date date = null;
        try {
            date = parseFormatDate(getMonthBegin(strdate), "yyyy-MM-dd");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(2, month_add * -1);
        calendar.add(2, 1);
        calendar.add(6, -1);
        return formatDate(calendar.getTime());
    }

    public String changeMonth(String strdate, int month) {
        Date date = null;
        try {
            date = parseFormatDate(getMonthBegin(strdate), "yyyy-MM-dd");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(2, month);
        return formatDate(calendar.getTime());
    }

    public String formatDate(Date date) {
        return formatDateByFormat(date, "yyyy-MM-dd");
    }

    public static String formatDateByFormat(Date date, String format) {
        if (date == null) {
            return "";
        }
        try {
            return new SimpleDateFormat(format).format(date);
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }
    }

    public static Date parseFormatDate(String strDate, String formatType) throws ParseException {
        if (strDate.equals("")) {
            return null;
        }
        return new SimpleDateFormat(formatType).parse(strDate);
    }

    public static String getDateStr(String format, int days) {
        return formatDateByFormat(getDate(new Date(), days), format);
    }

    public static Date getDate(Date startdate, int days) {
        try {
            GregorianCalendar cal = new GregorianCalendar();
            cal.setTime(startdate);
            cal.add(2, days);
            return cal.getTime();
        } catch (Exception e) {
            System.out.println("exception" + e.toString());
            return null;
        }
    }

    public static String getNowOfNextMonths(String format, int month) {
        SimpleDateFormat aSimpleDateFormat = new SimpleDateFormat(format);
        GregorianCalendar aGregorianCalendar = new GregorianCalendar();
        aGregorianCalendar.setTime(new Date());
        aGregorianCalendar.set(2, aGregorianCalendar.get(2) + month);
        if (month > 0) {
            aGregorianCalendar.set(5, 1);
        }
        return aSimpleDateFormat.format(aGregorianCalendar.getTime());
    }
}
