package com.kenfor.tools.datetime;

/* compiled from: SolarLunar */
class ChineseEra {
    int iYear;
    String[] sEarthlyBranches = {"子", "丑", "寅", "卯", "辰", "巳", "午", "未", "申", "酉", "戌", "亥"};
    String[] sHeavenlyStems = {"甲", "乙", "丙", "丁", "戊", "己", "庚", "辛", "壬", "癸"};

    public ChineseEra() {
    }

    public ChineseEra(int iYear2) {
        if (iYear2 >= 2050 || iYear2 <= 1901) {
            this.iYear = 1981;
        } else {
            this.iYear = iYear2;
        }
    }

    public String toString() {
        int temp = Math.abs(this.iYear - 1924);
        return String.valueOf(this.sHeavenlyStems[temp % 10]) + this.sEarthlyBranches[temp % 12];
    }
}
