package com.kenfor.tools.datetime;

/* compiled from: SolarLunar */
class Week {
    int iWeek;
    private String[] sWeek;

    public Week() {
        this.sWeek = new String[]{"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
        this.iWeek = 0;
    }

    public Week(int w) {
        this.sWeek = new String[]{"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
        if (w > 6 || w < 0) {
            System.out.println("Week out of range, I think you want Sunday");
            this.iWeek = 0;
            return;
        }
        this.iWeek = w;
    }

    public String toString() {
        return this.sWeek[this.iWeek];
    }
}
