package com.kenfor.tools.datetime;

public class SolarLunar {
    public static void main(String[] args) {
        System.out.println(sLundarToNumberDate("正月十五"));
    }

    public static String sCalendarSolarToLundar(int iYear, int iMonth, int iDay) {
        return Calendar.sCalendarLundarToSolar(iYear, iMonth, iDay);
    }

    public static String sCalendarLundarToSolar(int iYear, int iMonth, int iDay) {
        return Calendar.sCalendarLundarToSolar(iYear, iMonth, iDay);
    }

    public static String sLundarToNumberDate(String lundarStr) {
        String sMonth = "";
        String sDay = "";
        int iday = 0;
        String[] chineseNumber = {"一", "二", "三", "四", "五", "六", "七", "八", "九", "十", "十一", "十二"};
        String[] lundar = lundarStr.split("月");
        if (lundar[0].equals("正")) {
            sMonth = "1";
        } else {
            int i = 0;
            while (true) {
                if (i >= chineseNumber.length) {
                    break;
                } else if (lundar[0].equals(chineseNumber[i])) {
                    sMonth = new StringBuilder(String.valueOf(i + 1)).toString();
                    break;
                } else {
                    i++;
                }
            }
        }
        String[] chineseTen = {"初", "十", "廿", "卅"};
        if (lundar[1].indexOf(chineseTen[3]) > -1) {
            sDay = "30";
        } else if (lundar[1].indexOf(chineseTen[0]) > -1) {
            String tDay = lundar[1].substring(lundar[1].indexOf(chineseTen[0]) + 1, lundar[1].length());
            int i2 = 0;
            while (true) {
                if (i2 >= chineseNumber.length) {
                    break;
                } else if (tDay.equals(chineseNumber[i2])) {
                    iday = i2 + 1;
                    break;
                } else {
                    i2++;
                }
            }
            sDay = new StringBuilder(String.valueOf(iday)).toString();
        } else if (lundar[1].indexOf(chineseTen[2]) > -1) {
            String tDay2 = lundar[1].substring(lundar[1].indexOf(chineseTen[2]) + 1, lundar[1].length());
            int i3 = 0;
            while (true) {
                if (i3 < chineseNumber.length) {
                    if (tDay2.equals(chineseNumber[i3]) && !tDay2.equals("十")) {
                        iday = i3 + 1;
                        break;
                    }
                    i3++;
                } else {
                    break;
                }
            }
            sDay = new StringBuilder(String.valueOf(iday + 20)).toString();
        } else if (lundar[1].lastIndexOf(chineseTen[1]) > -1) {
            String tDay3 = lundar[1].substring(lundar[1].indexOf(chineseTen[1]) + 1, lundar[1].length());
            int i4 = 0;
            while (true) {
                if (i4 >= chineseNumber.length) {
                    break;
                } else if (tDay3.equals(chineseNumber[i4])) {
                    iday = i4 + 1;
                    break;
                } else {
                    i4++;
                }
            }
            sDay = new StringBuilder(String.valueOf(iday + 10)).toString();
        }
        return String.valueOf(sMonth) + "-" + sDay;
    }
}
