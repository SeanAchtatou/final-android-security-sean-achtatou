package com.kenfor.tools.datetime;

/* compiled from: SolarLunar */
class SolarDate extends MyDate {
    private static int checkMonth(int iMonth) {
        if (iMonth > 12) {
            System.out.println("Month out of range, I think you want 12 :)");
            return 12;
        } else if (iMonth >= 1) {
            return iMonth;
        } else {
            System.out.println("Month out of range, I think you want 1 :)");
            return 1;
        }
    }

    private static int checkDay(int iYear, int iMonth, int iDay) {
        int iMonthDays = Calendar.iGetSYearMonthDays(iYear, iMonth);
        if (iDay > iMonthDays) {
            System.out.println("Day out of range, I think you want " + iMonthDays + " :)");
            return iMonthDays;
        } else if (iDay >= 1) {
            return iDay;
        } else {
            System.out.println("Day out of range, I think you want 1 :)");
            return 1;
        }
    }

    public SolarDate(int iYear, int iMonth, int iDay) {
        super(iYear);
        this.iMonth = checkMonth(iMonth);
        this.iDay = checkDay(this.iYear, this.iMonth, iDay);
    }

    public SolarDate(int iYear, int iMonth) {
        super(iYear);
        this.iMonth = checkMonth(iMonth);
    }

    public SolarDate(int iYear) {
        super(iYear);
    }

    public SolarDate() {
    }

    public String toString() {
        return this.iYear + (this.iMonth > 9 ? "-" + this.iMonth : "-0" + this.iMonth) + (this.iDay > 9 ? "-" + this.iDay : "-0" + this.iDay);
    }

    public Week toWeek() {
        int iOffsetDays = 0;
        for (int i = 1901; i < this.iYear; i++) {
            if (Calendar.bIsSolarLeapYear(i)) {
                iOffsetDays += 366;
            } else {
                iOffsetDays += 365;
            }
        }
        return new Week(((iOffsetDays + Calendar.iGetSNewYearOffsetDays(this.iYear, this.iMonth, this.iDay)) + 2) % 7);
    }

    public LunarDate toLunarDate() {
        int iDate = Integer.parseInt(Calendar.sCalendarSolarToLundar(this.iYear, this.iMonth, this.iDay));
        return new LunarDate(iDate / 10000, (iDate % 10000) / 100, iDate % 100);
    }
}
