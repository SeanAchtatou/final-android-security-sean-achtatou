package com.kenfor.tools.datetime;

/* compiled from: SolarLunar */
class LunarDate extends MyDate {
    private String[] sChineseNum = {"零", "一", "二", "三", "四", "五", "六", "七", "八", "九", "十"};

    private static int checkMonth(int iYear, int iMonth) {
        if (iMonth > 12 && iMonth == Calendar.iGetLLeapMonth(iYear) + 12) {
            return iMonth;
        }
        if (iMonth > 12) {
            System.out.println("Month out of range, I think you want 12 :)");
            return 12;
        } else if (iMonth >= 1) {
            return iMonth;
        } else {
            System.out.println("Month out of range, I think you want 1 :)");
            return 1;
        }
    }

    private static int checkDay(int iYear, int iMonth, int iDay) {
        int iMonthDays = Calendar.iGetLMonthDays(iYear, iMonth);
        if (iDay > iMonthDays) {
            System.out.println("Day out of range, I think you want " + iMonthDays + " :)");
            return iMonthDays;
        } else if (iDay >= 1) {
            return iDay;
        } else {
            System.out.println("Day out of range, I think you want 1 :)");
            return 1;
        }
    }

    public LunarDate(int iYear, int iMonth, int iDay) {
        super(iYear);
        this.iMonth = checkMonth(this.iYear, iMonth);
        this.iDay = checkDay(this.iYear, this.iMonth, iDay);
    }

    public LunarDate(int iYear, int iMonth) {
        super(iYear);
        this.iMonth = checkMonth(this.iYear, iMonth);
    }

    public LunarDate(int iYear) {
        super(iYear);
    }

    public LunarDate() {
    }

    public String toChineseString() {
        String sCalendar;
        String sCalendar2 = String.valueOf("农历") + this.sChineseNum[this.iYear / 1000] + this.sChineseNum[(this.iYear % 1000) / 100] + this.sChineseNum[(this.iYear % 100) / 10] + this.sChineseNum[this.iYear % 10] + "(" + toChineseEra() + ")年";
        if (this.iMonth > 12) {
            this.iMonth -= 12;
            sCalendar2 = String.valueOf(sCalendar2) + "闰";
        }
        if (this.iMonth == 12) {
            sCalendar = String.valueOf(sCalendar2) + "腊月";
        } else if (this.iMonth == 11) {
            sCalendar = String.valueOf(sCalendar2) + "冬月";
        } else if (this.iMonth == 1) {
            sCalendar = String.valueOf(sCalendar2) + "正月";
        } else {
            sCalendar = String.valueOf(sCalendar2) + this.sChineseNum[this.iMonth] + "月";
        }
        if (this.iDay > 29) {
            return String.valueOf(sCalendar) + "三十";
        }
        if (this.iDay > 20) {
            return String.valueOf(sCalendar) + "二十" + this.sChineseNum[this.iDay % 20];
        }
        if (this.iDay == 20) {
            return String.valueOf(sCalendar) + "二十";
        }
        if (this.iDay > 10) {
            return String.valueOf(sCalendar) + "十" + this.sChineseNum[this.iDay % 10];
        }
        return String.valueOf(sCalendar) + "初" + this.sChineseNum[this.iDay];
    }

    public CnWeek toWeek() {
        int iOffsetDays = 0;
        for (int i = 1901; i < this.iYear; i++) {
            iOffsetDays += Calendar.iGetLYearDays(i);
        }
        return new CnWeek(((iOffsetDays + Calendar.iGetLNewYearOffsetDays(this.iYear, this.iMonth, this.iDay)) + 2) % 7);
    }

    public ChineseEra toChineseEra() {
        return new ChineseEra(this.iYear);
    }

    public SolarDate toSolarDate() {
        int iDate = Integer.parseInt(Calendar.sCalendarLundarToSolar(this.iYear, this.iMonth, this.iDay));
        return new SolarDate(iDate / 10000, (iDate % 10000) / 100, iDate % 100);
    }
}
