package com.kenfor.tools.datetime;

/* compiled from: SolarLunar */
class MyDate {
    public int iDay;
    public int iMonth;
    public int iYear;

    private static int checkYear(int iYear2) {
        if (iYear2 > 1901 && iYear2 < 2050) {
            return iYear2;
        }
        System.out.println("The Year out of range, I think you want 1981");
        return 1981;
    }

    public MyDate(int iYear2, int iMonth2, int iDay2) {
        this.iYear = checkYear(iYear2);
        this.iMonth = iMonth2;
        this.iDay = iDay2;
    }

    public MyDate(int iYear2, int iMonth2) {
        this.iYear = checkYear(iYear2);
        this.iMonth = iMonth2;
        this.iDay = 1;
    }

    public MyDate(int iYear2) {
        this.iYear = checkYear(iYear2);
        this.iMonth = 1;
        this.iDay = 1;
    }

    public MyDate() {
        this.iYear = 1981;
        this.iMonth = 1;
        this.iDay = 1;
    }

    public String toString() {
        return this.iYear + (this.iMonth > 9 ? new StringBuilder().append(this.iMonth).toString() : "0" + this.iMonth) + (this.iDay > 9 ? new StringBuilder().append(this.iDay).toString() : "0" + this.iDay);
    }

    public boolean equals(MyDate md) {
        return md.iDay == this.iDay && md.iMonth == this.iMonth && md.iYear == this.iYear;
    }
}
