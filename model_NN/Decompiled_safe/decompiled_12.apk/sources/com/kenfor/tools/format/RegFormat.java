package com.kenfor.tools.format;

import java.util.regex.Pattern;
import org.xbill.DNS.WKSRecord;

public class RegFormat {
    public static boolean checkLetterNum(String str) {
        return Pattern.compile("^[A-Za-z0-9]+$").matcher(str).find();
    }

    public static boolean checkLetterNum2(String str) {
        return Pattern.compile("^[a-zA-Z]{1}([a-zA-Z0-9]|[._]){4,19}$").matcher(str).find();
    }

    public static boolean checkLetterOnlyNum(String str) {
        return Pattern.compile("^[0-9]{1,20}$").matcher(str).find();
    }

    public static boolean checkLetterYear(String str) {
        return Pattern.compile("^[0-9]{4}$").matcher(str).find();
    }

    public static boolean checkLetterMonth(String str) {
        return Pattern.compile("^[0-1]{0,1}[0-9]{1}$").matcher(str).find();
    }

    public static boolean checkLetterDay(String str) {
        return Pattern.compile("^[0-3]{0,1}[0-9]{1}$").matcher(str).find();
    }

    public static boolean checkLetterFloat(String str) {
        return Pattern.compile("^[+\\-]?\\d+(.\\d+)?$").matcher(str).find();
    }

    public static int getCardType(String mobile) {
        if (!mobile.matches("^\\d{11}$") || mobile.substring(0, 1) == "0") {
            return mobile.matches("^\\d{3,4}-{0,1}\\d{7,8}$") ? 4 : 0;
        }
        switch (Integer.parseInt(mobile.substring(0, 3))) {
            case 130:
            case 131:
            case 132:
            case 155:
            case 156:
            case 185:
            case 186:
                return 2;
            case WKSRecord.Service.STATSRV /*133*/:
            case 153:
            case 180:
            case 189:
                return 3;
            case WKSRecord.Service.INGRES_NET /*134*/:
            case WKSRecord.Service.LOC_SRV /*135*/:
            case WKSRecord.Service.PROFILE /*136*/:
            case WKSRecord.Service.NETBIOS_NS /*137*/:
            case WKSRecord.Service.NETBIOS_DGM /*138*/:
            case WKSRecord.Service.NETBIOS_SSN /*139*/:
            case 150:
            case 151:
            case 152:
            case 157:
            case 158:
            case 159:
            case 187:
            case 188:
                return 1;
            case WKSRecord.Service.EMFIS_DATA /*140*/:
            case WKSRecord.Service.EMFIS_CNTL /*141*/:
            case WKSRecord.Service.BL_IDM /*142*/:
            case 143:
            case 144:
            case 145:
            case 146:
            case 147:
            case 148:
            case 149:
            case 154:
            case 160:
            case 161:
            case 162:
            case 163:
            case 164:
            case 165:
            case 166:
            case 167:
            case 168:
            case 169:
            case 170:
            case 171:
            case 172:
            case 173:
            case 174:
            case 175:
            case 176:
            case 177:
            case 178:
            case 179:
            case 181:
            case 182:
            case 183:
            case 184:
            default:
                return 0;
        }
    }
}
