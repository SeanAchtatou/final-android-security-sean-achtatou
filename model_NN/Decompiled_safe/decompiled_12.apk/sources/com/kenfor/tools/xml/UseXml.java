package com.kenfor.tools.xml;

import com.kenfor.tools.file.ClassLoaderUtil;
import java.io.FileInputStream;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

public class UseXml {
    private Document another_document = null;
    private SAXBuilder builder = null;
    private FileInputStream file_in_stream = null;
    private String realPath = null;
    private Element root = null;

    public UseXml(String file_name, boolean getRealPath) {
        this.realPath = ClassLoaderUtil.getClassLoader().getResource(file_name).getPath().toString();
    }

    public UseXml(String file_path) {
        this.realPath = file_path;
    }

    public boolean getXMLFile() {
        if (this.another_document != null) {
            return true;
        }
        try {
            this.file_in_stream = new FileInputStream(this.realPath);
            this.builder = new SAXBuilder();
            this.another_document = this.builder.build(this.file_in_stream);
            this.root = this.another_document.getRootElement();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            this.another_document = null;
            return false;
        }
    }

    public String getElementValue(String elementName) throws Exception {
        if (!getXMLFile()) {
            return null;
        }
        Element temp_element = this.root;
        String[] elementNames = elementName.split("\\.");
        for (int i = 0; i < elementNames.length - 1; i++) {
            temp_element = temp_element.getChild(elementNames[i]);
        }
        return temp_element.getChildText(elementNames[elementNames.length - 1]);
    }
}
