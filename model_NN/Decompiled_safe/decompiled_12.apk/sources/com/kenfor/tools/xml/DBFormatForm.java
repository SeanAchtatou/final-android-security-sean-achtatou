package com.kenfor.tools.xml;

public class DBFormatForm {
    String column_name = "";
    String element_name = "";
    String element_no = "";
    Integer father_id = 0;
    String fix_str = "";
    Integer get_data = 0;
    Integer is_repeat;
    Integer order_index;
    String primary_key_column_name = "";
    String table_name = "";
    Integer table_start;
    Integer xml_format_id = 0;

    public Integer getIs_repeat() {
        return this.is_repeat;
    }

    public void setIs_repeat(Integer is_repeat2) {
        this.is_repeat = is_repeat2;
    }

    public Integer getTable_start() {
        return this.table_start;
    }

    public void setTable_start(Integer table_start2) {
        this.table_start = table_start2;
    }

    public Integer getOrder_index() {
        return this.order_index;
    }

    public void setOrder_index(Integer order_index2) {
        this.order_index = order_index2;
    }

    public String getColumn_name() {
        return this.column_name;
    }

    public void setColumn_name(String column_name2) {
        this.column_name = column_name2;
    }

    public String getElement_name() {
        return this.element_name;
    }

    public void setElement_name(String element_name2) {
        this.element_name = element_name2;
    }

    public String getElement_no() {
        return this.element_no;
    }

    public void setElement_no(String element_no2) {
        this.element_no = element_no2;
    }

    public String getFix_str() {
        return this.fix_str;
    }

    public void setFix_str(String fix_str2) {
        this.fix_str = fix_str2;
    }

    public Integer getGet_data() {
        return this.get_data;
    }

    public void setGet_data(Integer get_data2) {
        this.get_data = get_data2;
    }

    public String getPrimary_key_column_name() {
        return this.primary_key_column_name;
    }

    public void setPrimary_key_column_name(String primary_key_column_name2) {
        this.primary_key_column_name = primary_key_column_name2;
    }

    public String getTable_name() {
        return this.table_name;
    }

    public void setTable_name(String table_name2) {
        this.table_name = table_name2;
    }

    public Integer getFather_id() {
        return this.father_id;
    }

    public void setFather_id(Integer father_id2) {
        this.father_id = father_id2;
    }

    public Integer getXml_format_id() {
        return this.xml_format_id;
    }

    public void setXml_format_id(Integer xml_format_id2) {
        this.xml_format_id = xml_format_id2;
    }
}
