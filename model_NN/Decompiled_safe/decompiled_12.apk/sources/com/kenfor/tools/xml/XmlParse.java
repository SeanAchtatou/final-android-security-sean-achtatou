package com.kenfor.tools.xml;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

public class XmlParse {
    public static String getXmlString(Map<String, Object> map) {
        if (map == null || map.isEmpty()) {
            return null;
        }
        Document document = DocumentHelper.createDocument();
        Element elements = document.addElement("params");
        for (String key : map.keySet()) {
            Element subElement = elements.addElement(key);
            subElement.addAttribute("type", map.get(key).getClass().getName());
            subElement.setText(String.valueOf(map.get(key)));
        }
        String returnString = document.asXML();
        System.out.println(returnString);
        return returnString;
    }

    public static Map<String, Object> parseXml(String xmlString) {
        Map<String, Object> map = new HashMap<>();
        try {
            Iterator it = DocumentHelper.parseText(xmlString).getRootElement().elementIterator();
            while (it.hasNext()) {
                Element element = (Element) it.next();
                String type = element.attributeValue("type");
                if ("java.lang.Integer".equals(type)) {
                    map.put(element.getName(), Integer.valueOf(element.getText()));
                }
                if ("java.lang.String".equals(type)) {
                    map.put(element.getName(), element.getText());
                }
                if ("java.lang.Long".equals(type)) {
                    map.put(element.getName(), Long.valueOf(element.getText()));
                }
                if ("java.lang.Short".equals(type)) {
                    map.put(element.getName(), Short.valueOf(element.getText()));
                }
                if ("java.lang.Boolean".equals(type)) {
                    map.put(element.getName(), Boolean.valueOf(element.getText()));
                }
                if ("java.lang.Float".equals(type)) {
                    map.put(element.getName(), Float.valueOf(element.getText()));
                }
                if ("java.lang.Double".equals(type)) {
                    map.put(element.getName(), Double.valueOf(element.getText()));
                }
            }
            return map;
        } catch (DocumentException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void main(String[] args) {
        Map<String, Object> map = new HashMap<>();
        map.put("name", "meiqi");
        map.put("age", 12);
        map.put("haveWife", false);
        String xmlString = getXmlString(map);
        System.out.println(xmlString);
        System.out.println(parseXml(xmlString).get("haveWife"));
    }
}
