package com.kenfor.tools.xml;

import java.io.IOException;
import java.util.List;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

public class XmlHelper {
    private Document doc = null;
    private String encoding = "GBK";
    private Element root = null;
    private SAXBuilder sb = null;

    public void init(String rootName) {
        this.sb = new SAXBuilder();
        this.root = new Element(rootName);
        this.doc = new Document(this.root);
    }

    public void createElement(String[] str, String value) {
        int index = 0;
        if (str[0].equals(this.doc.getRootElement().getName().trim())) {
            index = 1;
        }
        Element temp = this.doc.getRootElement();
        for (int i = index; i < str.length - 1; i++) {
            temp = temp.getChild(str[i]);
        }
        temp.addContent(new Element(str[str.length - 1]).setText(value));
    }

    public void updateElementName(String[] str, String elementName) {
        int index = 0;
        if (str[0].equals(this.doc.getRootElement().getName().trim())) {
            index = 1;
        }
        Element temp = this.doc.getRootElement();
        for (int i = index; i < str.length; i++) {
            temp = temp.getChild(str[i]);
        }
        temp.setName(elementName);
    }

    public void updateElementValue(String[] str, String value) {
        int index = 0;
        if (str[0].equals(this.doc.getRootElement().getName().trim())) {
            index = 1;
        }
        Element temp = this.doc.getRootElement();
        for (int i = index; i < str.length; i++) {
            temp = temp.getChild(str[i]);
        }
        temp.setText(value);
    }

    public void deleteElement(String[] str) {
        int index = 0;
        if (str[0].equals(this.doc.getRootElement().getName().trim())) {
            index = 1;
        }
        Element temp = this.doc.getRootElement();
        for (int i = index; i < str.length - 1; i++) {
            temp = temp.getChild(str[i]);
        }
        temp.removeChild(str[str.length - 1]);
    }

    public void createOrUpdateAttribute(String[] str, String attributeName, String value) {
        int index = 0;
        if (str[0].equals(this.doc.getRootElement().getName().trim())) {
            index = 1;
        }
        Element temp = this.doc.getRootElement();
        for (int i = index; i < str.length; i++) {
            temp = temp.getChild(str[i]);
        }
        temp.setAttribute(new Attribute(attributeName, value));
    }

    public void deleteAttribute(String[] str, String attributeName) {
        int index = 0;
        if (str[0].equals(this.doc.getRootElement().getName().trim())) {
            index = 1;
        }
        Element temp = this.doc.getRootElement();
        for (int i = index; i < str.length; i++) {
            temp = temp.getChild(str[i]);
        }
        temp.removeAttribute(attributeName);
    }

    public String getElementText(String[] str) {
        int index = 0;
        if (str[0].equals(this.doc.getRootElement().getName().trim())) {
            index = 1;
        }
        Element temp = this.doc.getRootElement();
        for (int i = index; i < str.length; i++) {
            temp = temp.getChild(str[i]);
        }
        return temp.getText();
    }

    public String getElementTextNormalize(String[] str) {
        int index = 0;
        if (str[0].equals(this.doc.getRootElement().getName().trim())) {
            index = 1;
        }
        Element temp = this.doc.getRootElement();
        for (int i = index; i < str.length; i++) {
            temp = temp.getChild(str[i]);
        }
        return temp.getTextNormalize();
    }

    public String getElementTextTrim(String[] str) {
        int index = 0;
        if (str[0].equals(this.doc.getRootElement().getName().trim())) {
            index = 1;
        }
        Element temp = this.doc.getRootElement();
        for (int i = index; i < str.length; i++) {
            temp = temp.getChild(str[i]);
        }
        return temp.getTextTrim();
    }

    public String getAttribute(String[] str, String attributeName) {
        int index = 0;
        if (str[0].equals(this.doc.getRootElement().getName().trim())) {
            index = 1;
        }
        Element temp = this.doc.getRootElement();
        for (int i = index; i < str.length; i++) {
            temp = temp.getChild(str[i]);
        }
        return temp.getAttribute(attributeName).getValue();
    }

    public void saveAsFile(String filename) {
        throw new Error("Unresolved compilation problems: \n\tThe method getCompactFormat() is undefined for the type Format\n\tThe method setEncoding(String) is undefined for the type Format\n\tThe method setIndent(String) is undefined for the type Format\n\tThe method setFormat(Format) in the type XMLOutputter is not applicable for the arguments (Format)\n");
    }

    public Element getDesignatedChildElement(String[] str, int index) {
        int n = 0;
        if (str[0].equals(this.doc.getRootElement().getName().trim())) {
            n = 1;
        }
        Element temp = this.doc.getRootElement();
        for (int i = n; i < str.length - 1; i++) {
            temp = temp.getChild(str[i]);
        }
        return (Element) temp.getChildren(str[str.length - 1]).get(index);
    }

    public List getAllChildren(String[] str) {
        int n = 0;
        if (str[0].equals(this.doc.getRootElement().getName().trim())) {
            n = 1;
        }
        Element temp = this.doc.getRootElement();
        for (int i = n; i < str.length; i++) {
            temp = temp.getChild(str[i]);
        }
        return temp.getChildren();
    }

    public void test() {
        this.root.addContent(new Element("id1").addContent(new Element("title").setText("test")));
        createElement(new String[]{"news", "id1", "text"}, "this is text");
        createElement(new String[]{"news", "id2"}, "");
        createElement(new String[]{"news", "id2", "title"}, "test2");
        saveAsFile("a.xml");
    }

    public void testRead() {
        try {
            this.doc = this.sb.build("a.xml");
            System.out.println("+++++++");
            System.out.println(getElementTextTrim(new String[]{"news", "id2", "title"}));
            System.out.println(getElementTextTrim(new String[]{"news", "id1", "text"}));
        } catch (IOException e) {
        } catch (JDOMException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        XmlHelper t = new XmlHelper();
        t.init("news");
        t.test();
        t.testRead();
    }

    public void testAllchild() {
        List l = getAllChildren(new String[]{"stu"});
        System.out.println(l.size());
        for (int i = 0; i < l.size(); i++) {
            System.out.print(((Element) l.get(i)).getName());
        }
    }
}
