package com.kenfor.tools.xml;

import com.kenfor.tools.hibernate.DBRecord;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class CreateXmlByDBFormat {
    public static void createXml(int xml_type_id, int id1, int id2) {
        HashMap hs = new HashMap();
        StringBuffer str_buffer = new StringBuffer();
        str_buffer.append("<?xml version=\"1.0\" encoding=\"GBK\"?>");
        str_buffer.append(getWrap());
        int element_index = 1;
        try {
            PreparedStatement pstmt = DriverManager.getConnection("proxool.poolcrm").prepareStatement("select element_no,element_name,table_name,column_name,fix_str,get_data,primary_key_column_name from xml_format where xml_type_id=? order by element_no");
            pstmt.setInt(1, xml_type_id);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                if (rs.getString("element_no").length() <= "".length()) {
                    if (rs.getString("element_no").length() == "".length()) {
                        str_buffer.append("</" + rs.getString("element_name") + ">");
                    } else {
                        rs.getString("element_no").length();
                        "".length();
                    }
                }
                hs.put(Integer.valueOf(element_index), rs.getString("element_name"));
                element_index++;
                if (rs.getInt("get_data") == 0) {
                    str_buffer.append(getSpace(rs.getString("element_no")));
                    str_buffer.append("<" + rs.getString("element_name") + ">");
                } else if (rs.getInt("get_data") == 1) {
                    str_buffer.append(getSpace(rs.getString("element_no")));
                    str_buffer.append("<" + rs.getString("element_name") + ">");
                    str_buffer.append((String) DBRecord.getTableValue(rs.getString("table_name"), rs.getString("column_name"), String.valueOf(rs.getString("primary_key_column_name")) + "=" + String.valueOf(id1), "str"));
                } else if (rs.getInt("get_data") == 2) {
                    str_buffer.append(getSpace(rs.getString("element_no")));
                    str_buffer.append(rs.getString("fix_str"));
                }
                str_buffer.append("<" + rs.getString("element_name") + ">");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String getSpace(String element_no) {
        String new_space_str = "";
        for (int i = 0; i < element_no.length(); i++) {
            new_space_str = String.valueOf(new_space_str) + " ";
        }
        return new_space_str;
    }

    public static String getWrap() {
        return "\\n";
    }
}
