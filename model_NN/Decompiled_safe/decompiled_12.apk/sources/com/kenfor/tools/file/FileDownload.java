package com.kenfor.tools.file;

import java.math.BigDecimal;

public class FileDownload {
    /* JADX WARNING: Removed duplicated region for block: B:58:0x015d A[SYNTHETIC, Splitter:B:58:0x015d] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0162 A[SYNTHETIC, Splitter:B:61:0x0162] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean downloadFile(java.lang.String r22, java.lang.String r23, javax.servlet.http.HttpServletRequest r24, javax.servlet.http.HttpServletResponse r25, java.lang.String r26, java.lang.String r27) {
        /*
            r12 = 0
            r8 = 0
            java.io.File r8 = new java.io.File
            r0 = r22
            r8.<init>(r0)
            r9 = 0
            r16 = 0
            boolean r17 = r8.exists()
            if (r17 == 0) goto L_0x0014
            r12 = 1
        L_0x0014:
            int r17 = r27.length()
            if (r17 != 0) goto L_0x004f
            long r9 = r8.length()
            r17 = 1048576(0x100000, double:5.180654E-318)
            int r17 = (r9 > r17 ? 1 : (r9 == r17 ? 0 : -1))
            if (r17 < 0) goto L_0x00c6
            java.lang.StringBuilder r17 = new java.lang.StringBuilder
            double r0 = (double) r9
            r18 = r0
            r20 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r18 = r18 * r20
            r20 = 4697254411347427328(0x4130000000000000, double:1048576.0)
            double r18 = r18 / r20
            java.lang.String r18 = java.lang.String.valueOf(r18)
            r19 = 2
            r20 = 4
            java.lang.String r18 = round(r18, r19, r20)
            java.lang.String r18 = java.lang.String.valueOf(r18)
            r17.<init>(r18)
            java.lang.String r18 = "M"
            java.lang.StringBuilder r17 = r17.append(r18)
            java.lang.String r27 = r17.toString()
        L_0x004f:
            if (r12 == 0) goto L_0x0178
            java.lang.String r11 = ""
            java.lang.String r17 = "UTF-8"
            r0 = r23
            r1 = r17
            java.lang.String r11 = java.net.URLEncoder.encode(r0, r1)     // Catch:{ UnsupportedEncodingException -> 0x010d }
        L_0x005d:
            java.lang.String r17 = "Content-disposition"
            java.lang.StringBuilder r18 = new java.lang.StringBuilder
            java.lang.String r19 = "attachment;filename="
            r18.<init>(r19)
            r0 = r18
            java.lang.StringBuilder r18 = r0.append(r11)
            java.lang.String r18 = r18.toString()
            r0 = r25
            r1 = r17
            r2 = r18
            r0.setHeader(r1, r2)
            java.lang.String r17 = "Content-Length"
            java.lang.String r18 = java.lang.String.valueOf(r9)
            r0 = r25
            r1 = r17
            r2 = r18
            r0.setHeader(r1, r2)
            r4 = 0
            r15 = 0
            javax.servlet.ServletOutputStream r15 = r25.getOutputStream()     // Catch:{ Exception -> 0x01ac }
            java.io.BufferedInputStream r5 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x01ac }
            java.io.FileInputStream r17 = new java.io.FileInputStream     // Catch:{ Exception -> 0x01ac }
            r0 = r17
            r1 = r22
            r0.<init>(r1)     // Catch:{ Exception -> 0x01ac }
            r0 = r17
            r5.<init>(r0)     // Catch:{ Exception -> 0x01ac }
            r6 = 0
            r14 = 8192(0x2000, float:1.14794E-41)
            r17 = 8192(0x2000, float:1.14794E-41)
            r0 = r17
            byte[] r3 = new byte[r0]     // Catch:{ Exception -> 0x011f, all -> 0x01a9 }
        L_0x00a7:
            r17 = 0
            r18 = 8192(0x2000, float:1.14794E-41)
            r0 = r17
            r1 = r18
            int r6 = r5.read(r3, r0, r1)     // Catch:{ Exception -> 0x011f, all -> 0x01a9 }
            r17 = -1
            r0 = r17
            if (r6 != r0) goto L_0x0117
            if (r5 == 0) goto L_0x00be
            r5.close()     // Catch:{ IOException -> 0x0135 }
        L_0x00be:
            if (r15 == 0) goto L_0x00c3
            r15.close()     // Catch:{ IOException -> 0x013e }
        L_0x00c3:
            r17 = 1
        L_0x00c5:
            return r17
        L_0x00c6:
            r17 = 1024(0x400, double:5.06E-321)
            int r17 = (r9 > r17 ? 1 : (r9 == r17 ? 0 : -1))
            if (r17 < 0) goto L_0x00f8
            java.lang.StringBuilder r17 = new java.lang.StringBuilder
            double r0 = (double) r9
            r18 = r0
            r20 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r18 = r18 * r20
            r20 = 4652218415073722368(0x4090000000000000, double:1024.0)
            double r18 = r18 / r20
            java.lang.String r18 = java.lang.String.valueOf(r18)
            r19 = 2
            r20 = 4
            java.lang.String r18 = round(r18, r19, r20)
            java.lang.String r18 = java.lang.String.valueOf(r18)
            r17.<init>(r18)
            java.lang.String r18 = "K"
            java.lang.StringBuilder r17 = r17.append(r18)
            java.lang.String r27 = r17.toString()
            goto L_0x004f
        L_0x00f8:
            java.lang.StringBuilder r17 = new java.lang.StringBuilder
            java.lang.String r18 = java.lang.String.valueOf(r9)
            r17.<init>(r18)
            java.lang.String r18 = "B"
            java.lang.StringBuilder r17 = r17.append(r18)
            java.lang.String r27 = r17.toString()
            goto L_0x004f
        L_0x010d:
            r7 = move-exception
            java.io.PrintStream r17 = java.lang.System.out
            java.lang.String r18 = "进行字符集转换时抛出异常:"
            r17.println(r18)
            goto L_0x005d
        L_0x0117:
            r17 = 0
            r0 = r17
            r15.write(r3, r0, r6)     // Catch:{ Exception -> 0x011f, all -> 0x01a9 }
            goto L_0x00a7
        L_0x011f:
            r7 = move-exception
            r4 = r5
        L_0x0121:
            java.io.PrintStream r17 = java.lang.System.out     // Catch:{ all -> 0x015a }
            java.lang.String r18 = "取消也会抛出异常"
            r17.println(r18)     // Catch:{ all -> 0x015a }
            if (r4 == 0) goto L_0x012d
            r4.close()     // Catch:{ IOException -> 0x0148 }
        L_0x012d:
            if (r15 == 0) goto L_0x0132
            r15.close()     // Catch:{ IOException -> 0x0151 }
        L_0x0132:
            r17 = 0
            goto L_0x00c5
        L_0x0135:
            r13 = move-exception
            java.io.PrintStream r17 = java.lang.System.out
            java.lang.String r18 = "关闭转入流FileinputStream时抛出异常:"
            r17.println(r18)
            goto L_0x00be
        L_0x013e:
            r13 = move-exception
            java.io.PrintStream r17 = java.lang.System.out
            java.lang.String r18 = "关闭转出流OutputStream时抛出异常:"
            r17.println(r18)
            goto L_0x00c3
        L_0x0148:
            r13 = move-exception
            java.io.PrintStream r17 = java.lang.System.out
            java.lang.String r18 = "关闭转入流FileinputStream时抛出异常:"
            r17.println(r18)
            goto L_0x012d
        L_0x0151:
            r13 = move-exception
            java.io.PrintStream r17 = java.lang.System.out
            java.lang.String r18 = "关闭转出流OutputStream时抛出异常:"
            r17.println(r18)
            goto L_0x0132
        L_0x015a:
            r17 = move-exception
        L_0x015b:
            if (r4 == 0) goto L_0x0160
            r4.close()     // Catch:{ IOException -> 0x0166 }
        L_0x0160:
            if (r15 == 0) goto L_0x0165
            r15.close()     // Catch:{ IOException -> 0x016f }
        L_0x0165:
            throw r17
        L_0x0166:
            r13 = move-exception
            java.io.PrintStream r18 = java.lang.System.out
            java.lang.String r19 = "关闭转入流FileinputStream时抛出异常:"
            r18.println(r19)
            goto L_0x0160
        L_0x016f:
            r13 = move-exception
            java.io.PrintStream r18 = java.lang.System.out
            java.lang.String r19 = "关闭转出流OutputStream时抛出异常:"
            r18.println(r19)
            goto L_0x0165
        L_0x0178:
            java.io.PrintStream r17 = java.lang.System.out
            java.lang.String r18 = "文件下载路径地址错误!"
            r17.println(r18)
            java.lang.String r17 = "text/html; charset=utf-8"
            r0 = r25
            r1 = r17
            r0.setContentType(r1)
            java.io.PrintWriter r16 = r25.getWriter()     // Catch:{ IOException -> 0x0198 }
            java.lang.String r17 = "<script language=\"javascript\">alert(\"文件下载路径地址错误\");history.back(-1);</script>"
            r16.print(r17)     // Catch:{ IOException -> 0x0198 }
            r16.close()
        L_0x0194:
            r17 = 0
            goto L_0x00c5
        L_0x0198:
            r7 = move-exception
            java.io.PrintStream r17 = java.lang.System.out     // Catch:{ all -> 0x01a4 }
            java.lang.String r18 = "IO输出异常"
            r17.println(r18)     // Catch:{ all -> 0x01a4 }
            r16.close()
            goto L_0x0194
        L_0x01a4:
            r17 = move-exception
            r16.close()
            throw r17
        L_0x01a9:
            r17 = move-exception
            r4 = r5
            goto L_0x015b
        L_0x01ac:
            r7 = move-exception
            goto L_0x0121
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenfor.tools.file.FileDownload.downloadFile(java.lang.String, java.lang.String, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.String, java.lang.String):boolean");
    }

    public static String round(String num_str, int scale, int round_mode) {
        if (scale >= 0) {
            return new BigDecimal(num_str).setScale(scale, round_mode).toString();
        }
        throw new IllegalArgumentException("The scale must be a positive integer or zero");
    }
}
