package com.kenfor.tools.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import org.xbill.DNS.KEYRecord;

public class FileCopy {
    public static boolean copyFile(String filefrom_str, String fileto_str, boolean rewrite) {
        File filefrom = new File(filefrom_str);
        File fileto = new File(fileto_str);
        if (!filefrom.exists()) {
            System.out.println("文件不存在");
            return false;
        } else if (!filefrom.isFile()) {
            System.out.println("不能够复制文件夹");
            return false;
        } else if (!filefrom.canRead()) {
            System.out.println("不能够读取需要复制的文件");
            return false;
        } else {
            if (!fileto.getParentFile().exists()) {
                fileto.getParentFile().mkdirs();
            }
            if (fileto.exists() && rewrite) {
                fileto.delete();
            }
            try {
                FileInputStream fosfrom = new FileInputStream(filefrom);
                FileOutputStream fosto = new FileOutputStream(fileto);
                byte[] bt = new byte[KEYRecord.Flags.FLAG5];
                while (true) {
                    int c = fosfrom.read(bt);
                    if (c <= 0) {
                        fosfrom.close();
                        fosto.close();
                        return true;
                    }
                    fosto.write(bt, 0, c);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                return false;
            }
        }
    }
}
