package com.kenfor.tools.file;

public class ExportTxtFile {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException} */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00f0 A[SYNTHETIC, Splitter:B:43:0x00f0] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00fe A[SYNTHETIC, Splitter:B:49:0x00fe] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void exportTxt(java.lang.Object[][] r16, java.lang.String r17, java.lang.String[] r18, java.lang.String r19, boolean r20) {
        /*
            java.util.Date r1 = new java.util.Date
            r1.<init>()
            r4 = 0
            r8 = 0
            if (r20 == 0) goto L_0x0096
            java.io.FileWriter r5 = new java.io.FileWriter     // Catch:{ Exception -> 0x00ea }
            r10 = 1
            r0 = r17
            r5.<init>(r0, r10)     // Catch:{ Exception -> 0x00ea }
            r4 = r5
        L_0x0012:
            java.lang.StringBuffer r9 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x00ea }
            r9.<init>()     // Catch:{ Exception -> 0x00ea }
            if (r20 != 0) goto L_0x0041
            r6 = 0
        L_0x001a:
            r0 = r18
            int r10 = r0.length     // Catch:{ Exception -> 0x0111, all -> 0x010e }
            if (r6 < r10) goto L_0x00a0
            int r10 = r9.length()     // Catch:{ Exception -> 0x0111, all -> 0x010e }
            if (r10 <= 0) goto L_0x0035
            int r10 = r9.length()     // Catch:{ Exception -> 0x0111, all -> 0x010e }
            int r11 = r19.length()     // Catch:{ Exception -> 0x0111, all -> 0x010e }
            int r10 = r10 - r11
            int r11 = r9.length()     // Catch:{ Exception -> 0x0111, all -> 0x010e }
            r9.delete(r10, r11)     // Catch:{ Exception -> 0x0111, all -> 0x010e }
        L_0x0035:
            java.lang.String r10 = "\r\n"
            r9.append(r10)     // Catch:{ Exception -> 0x0111, all -> 0x010e }
            java.lang.String r10 = r9.toString()     // Catch:{ Exception -> 0x0111, all -> 0x010e }
            r4.write(r10)     // Catch:{ Exception -> 0x0111, all -> 0x010e }
        L_0x0041:
            java.io.PrintStream r10 = java.lang.System.out     // Catch:{ Exception -> 0x0111, all -> 0x010e }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0111, all -> 0x010e }
            java.lang.String r12 = "本次导出："
            r11.<init>(r12)     // Catch:{ Exception -> 0x0111, all -> 0x010e }
            r0 = r16
            int r12 = r0.length     // Catch:{ Exception -> 0x0111, all -> 0x010e }
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ Exception -> 0x0111, all -> 0x010e }
            java.lang.String r12 = "条"
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ Exception -> 0x0111, all -> 0x010e }
            java.lang.String r11 = r11.toString()     // Catch:{ Exception -> 0x0111, all -> 0x010e }
            r10.println(r11)     // Catch:{ Exception -> 0x0111, all -> 0x010e }
            r6 = 0
        L_0x005f:
            r0 = r16
            int r10 = r0.length     // Catch:{ Exception -> 0x0111, all -> 0x010e }
            if (r6 < r10) goto L_0x00af
            r4.flush()     // Catch:{ Exception -> 0x0111, all -> 0x010e }
            if (r4 == 0) goto L_0x010b
            r4.close()     // Catch:{ IOException -> 0x0107 }
            r8 = r9
        L_0x006d:
            java.util.Date r3 = new java.util.Date
            r3.<init>()
            java.io.PrintStream r10 = java.lang.System.out
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            java.lang.String r12 = "运行时间："
            r11.<init>(r12)
            long r12 = r3.getTime()
            long r14 = r1.getTime()
            long r12 = r12 - r14
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.String r12 = "ms"
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.String r11 = r11.toString()
            r10.println(r11)
            return
        L_0x0096:
            java.io.FileWriter r5 = new java.io.FileWriter     // Catch:{ Exception -> 0x00ea }
            r0 = r17
            r5.<init>(r0)     // Catch:{ Exception -> 0x00ea }
            r4 = r5
            goto L_0x0012
        L_0x00a0:
            r10 = r18[r6]     // Catch:{ Exception -> 0x0111, all -> 0x010e }
            java.lang.StringBuffer r10 = r9.append(r10)     // Catch:{ Exception -> 0x0111, all -> 0x010e }
            r0 = r19
            r10.append(r0)     // Catch:{ Exception -> 0x0111, all -> 0x010e }
            int r6 = r6 + 1
            goto L_0x001a
        L_0x00af:
            java.lang.StringBuffer r8 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x0111, all -> 0x010e }
            r8.<init>()     // Catch:{ Exception -> 0x0111, all -> 0x010e }
            r7 = 0
        L_0x00b5:
            r0 = r18
            int r10 = r0.length     // Catch:{ Exception -> 0x00ea }
            if (r7 < r10) goto L_0x00da
            int r10 = r8.length()     // Catch:{ Exception -> 0x00ea }
            int r11 = r19.length()     // Catch:{ Exception -> 0x00ea }
            int r10 = r10 - r11
            int r11 = r8.length()     // Catch:{ Exception -> 0x00ea }
            r8.delete(r10, r11)     // Catch:{ Exception -> 0x00ea }
            java.lang.String r10 = "\r\n"
            r8.append(r10)     // Catch:{ Exception -> 0x00ea }
            java.lang.String r10 = r8.toString()     // Catch:{ Exception -> 0x00ea }
            r4.write(r10)     // Catch:{ Exception -> 0x00ea }
            int r6 = r6 + 1
            r9 = r8
            goto L_0x005f
        L_0x00da:
            r10 = r16[r6]     // Catch:{ Exception -> 0x00ea }
            r10 = r10[r7]     // Catch:{ Exception -> 0x00ea }
            java.lang.StringBuffer r10 = r8.append(r10)     // Catch:{ Exception -> 0x00ea }
            r0 = r19
            r10.append(r0)     // Catch:{ Exception -> 0x00ea }
            int r7 = r7 + 1
            goto L_0x00b5
        L_0x00ea:
            r2 = move-exception
        L_0x00eb:
            r2.printStackTrace()     // Catch:{ all -> 0x00fb }
            if (r4 == 0) goto L_0x006d
            r4.close()     // Catch:{ IOException -> 0x00f5 }
            goto L_0x006d
        L_0x00f5:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x006d
        L_0x00fb:
            r10 = move-exception
        L_0x00fc:
            if (r4 == 0) goto L_0x0101
            r4.close()     // Catch:{ IOException -> 0x0102 }
        L_0x0101:
            throw r10
        L_0x0102:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0101
        L_0x0107:
            r2 = move-exception
            r2.printStackTrace()
        L_0x010b:
            r8 = r9
            goto L_0x006d
        L_0x010e:
            r10 = move-exception
            r8 = r9
            goto L_0x00fc
        L_0x0111:
            r2 = move-exception
            r8 = r9
            goto L_0x00eb
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenfor.tools.file.ExportTxtFile.exportTxt(java.lang.Object[][], java.lang.String, java.lang.String[], java.lang.String, boolean):void");
    }
}
