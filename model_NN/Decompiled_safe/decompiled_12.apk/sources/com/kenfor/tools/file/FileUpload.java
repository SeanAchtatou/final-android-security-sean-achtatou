package com.kenfor.tools.file;

import com.kenfor.tools.xml.UseXml;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;
import org.apache.struts.upload.FormFile;

public class FileUpload {
    private static Logger log = Logger.getLogger("SecureFileUploader");

    public static String uploadFile(FormFile file) throws IOException {
        String new_file_name = getRamdomFileName(file.getFileName());
        String path = createAutoDir();
        String full_path = null;
        try {
            full_path = String.valueOf(new UseXml("kenfor_tools.xml", true).getElementValue("uploadFileDir")) + path;
            System.out.println(full_path);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (full_path == null || full_path.length() == 0) {
            return null;
        }
        File f = new File(full_path);
        if (!f.exists()) {
            f.mkdirs();
        }
        String full_path2 = String.valueOf(full_path) + new_file_name;
        new File(full_path2);
        InputStream in = null;
        try {
            in = file.getInputStream();
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
        BufferedOutputStream fos = null;
        try {
            fos = new BufferedOutputStream(new FileOutputStream(new File(full_path2)), 10240);
        } catch (FileNotFoundException e4) {
            e4.printStackTrace();
        }
        byte[] buffer = new byte[10240];
        while (true) {
            int read = in.read(buffer, 0, 10240);
            if (read <= 0) {
                fos.flush();
                fos.close();
                in.close();
                file.destroy();
                return String.valueOf(path) + new_file_name;
            }
            fos.write(buffer, 0, read);
        }
    }

    public static String getExt(String fileName) {
        int pos = fileName.lastIndexOf(".");
        String extName = ".txt";
        if (pos > -1) {
            extName = fileName.substring(pos + 1, fileName.length());
        }
        return extName.toLowerCase();
    }

    public static String createAutoDir() {
        String str_date = new SimpleDateFormat("yyyyMMdd").format(new Date());
        int len = str_date.length();
        String str_dd = "01";
        if (len == 8) {
            str_dd = str_date.substring(6, 8);
        }
        if (len > 6) {
            str_date = str_date.substring(0, 6);
        }
        return String.valueOf(File.separator) + str_date + File.separator + str_dd + File.separator;
    }

    public static String getRamdomFileName(String fileName) {
        return String.valueOf(String.valueOf((1000 * System.currentTimeMillis()) + ((long) ((int) ((Math.random() * 1000.0d) + 1.0d))))) + "." + getExt(fileName);
    }
}
