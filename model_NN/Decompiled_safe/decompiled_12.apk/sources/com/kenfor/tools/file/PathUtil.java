package com.kenfor.tools.file;

import java.io.File;
import java.net.URL;

public class PathUtil {
    public String getWebClassesPath() {
        System.out.println(Thread.currentThread().getContextClassLoader().getResource(""));
        System.out.println(PathUtil.class.getClassLoader().getResource(""));
        System.out.println(ClassLoader.getSystemResource(""));
        System.out.println(PathUtil.class.getResource(""));
        System.out.println(PathUtil.class.getResource("/"));
        System.out.println(new File("/").getAbsolutePath());
        System.out.println(System.getProperty("user.dir"));
        URL temp_path = PathUtil.class.getResource("");
        if (temp_path == null) {
            System.out.println("temp_path is null");
        } else {
            System.out.println("temp_path=" + temp_path);
        }
        return PathUtil.class.getResource("").toString();
    }

    public String getWebInfPath() throws IllegalAccessException {
        String path = getWebClassesPath();
        if (path.indexOf("WEB-INF") > 0) {
            return path.substring(6, path.indexOf("WEB-INF") + 8);
        }
        throw new IllegalAccessException("路径获取错误");
    }

    public String getWebRoot() throws IllegalAccessException {
        String path = getWebClassesPath();
        if (path == null) {
            System.out.println("path is null");
        } else {
            System.out.println("path=" + path);
        }
        if (path.indexOf("WEB-INF") > 0) {
            return path.substring(6, path.indexOf("WEB-INF/classes"));
        }
        throw new IllegalAccessException("路径获取错误");
    }
}
