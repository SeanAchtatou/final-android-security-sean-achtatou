package com.kenfor.tools;

import org.jdom.Element;

public class XmlParser {
    private Element rootElement = null;

    /* JADX WARNING: Unknown top exception splitter block from list: {B:7:0x0021=Splitter:B:7:0x0021, B:15:0x0031=Splitter:B:15:0x0031, B:11:0x0029=Splitter:B:11:0x0029} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public XmlParser(java.lang.String r7) throws java.lang.Exception {
        /*
            r6 = this;
            r6.<init>()
            r5 = 0
            r6.rootElement = r5
            r2 = 0
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ JDOMException -> 0x0020, NullPointerException -> 0x0028, Exception -> 0x0030 }
            r3.<init>(r7)     // Catch:{ JDOMException -> 0x0020, NullPointerException -> 0x0028, Exception -> 0x0030 }
            org.jdom.input.SAXBuilder r0 = new org.jdom.input.SAXBuilder     // Catch:{ JDOMException -> 0x0046, NullPointerException -> 0x0043, Exception -> 0x0040, all -> 0x003d }
            r0.<init>()     // Catch:{ JDOMException -> 0x0046, NullPointerException -> 0x0043, Exception -> 0x0040, all -> 0x003d }
            org.jdom.Document r4 = r0.build(r3)     // Catch:{ JDOMException -> 0x0046, NullPointerException -> 0x0043, Exception -> 0x0040, all -> 0x003d }
            org.jdom.Element r5 = r4.getRootElement()     // Catch:{ JDOMException -> 0x0046, NullPointerException -> 0x0043, Exception -> 0x0040, all -> 0x003d }
            r6.rootElement = r5     // Catch:{ JDOMException -> 0x0046, NullPointerException -> 0x0043, Exception -> 0x0040, all -> 0x003d }
            r3.close()
            r2 = r3
        L_0x001f:
            return
        L_0x0020:
            r1 = move-exception
        L_0x0021:
            r1.printStackTrace()     // Catch:{ all -> 0x0038 }
            r2.close()
            goto L_0x001f
        L_0x0028:
            r1 = move-exception
        L_0x0029:
            r1.printStackTrace()     // Catch:{ all -> 0x0038 }
            r2.close()
            goto L_0x001f
        L_0x0030:
            r1 = move-exception
        L_0x0031:
            r1.printStackTrace()     // Catch:{ all -> 0x0038 }
            r2.close()
            goto L_0x001f
        L_0x0038:
            r5 = move-exception
        L_0x0039:
            r2.close()
            throw r5
        L_0x003d:
            r5 = move-exception
            r2 = r3
            goto L_0x0039
        L_0x0040:
            r1 = move-exception
            r2 = r3
            goto L_0x0031
        L_0x0043:
            r1 = move-exception
            r2 = r3
            goto L_0x0029
        L_0x0046:
            r1 = move-exception
            r2 = r3
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenfor.tools.XmlParser.<init>(java.lang.String):void");
    }

    public String getProperty(String p_property) {
        return this.rootElement.getChildTextTrim(p_property);
    }
}
