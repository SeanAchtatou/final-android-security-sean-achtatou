package com.kenfor.tools.encrypt;

import com.kenfor.util.MD5;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class AzDGCrypt {
    public static byte[] encrypt(byte[] txt, byte[] key) {
        byte[] encrypt_key = new MD5().getMD5ofStr(new StringBuilder(String.valueOf(new Double(Math.random() * 32000.0d).intValue())).toString()).toLowerCase().getBytes();
        byte ctr = 0;
        ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
        int i = 0;
        int j = 0;
        while (i < txt.length) {
            if (ctr == encrypt_key.length) {
                ctr = 0;
            }
            byteOut.write(encrypt_key[ctr]);
            byteOut.write(txt[i] ^ encrypt_key[ctr]);
            i++;
            j += 2;
            ctr = (byte) (ctr + 1);
        }
        return Base64.encode(encodeKey(byteOut.toByteArray(), key)).getBytes();
    }

    public static String encrypt(String txt, String key) {
        return new String(encrypt(txt.getBytes(), key.getBytes()));
    }

    public static String encrypt(String txt, String key, String encoding) {
        String str = null;
        try {
            String str2 = new String(encrypt(txt.getBytes(encoding), key.getBytes()));
            if (str2 == null) {
                return "";
            }
            try {
                return URLEncoder.encode(str2, "utf-8");
            } catch (UnsupportedEncodingException e) {
                e = e;
                str = str2;
                e.printStackTrace();
                return str;
            }
        } catch (UnsupportedEncodingException e2) {
            e = e2;
            e.printStackTrace();
            return str;
        }
    }

    public static byte[] decrypt(byte[] txt, byte[] key) {
        byte[] txt2 = encodeKey(Base64.decode(new String(txt)), key);
        ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
        int i = 0;
        while (i < txt2.length) {
            byte md5 = txt2[i];
            int i2 = i + 1;
            byteOut.write(txt2[i2] ^ md5);
            i = i2 + 1;
        }
        return byteOut.toByteArray();
    }

    public static String decrypt(String txt, String key) {
        return new String(decrypt(txt.getBytes(), key.getBytes()));
    }

    public static byte[] encodeKey(byte[] txt, byte[] encrypt_key) {
        byte[] encrypt_key2 = new MD5().getMD5ofStr(new String(encrypt_key)).toLowerCase().getBytes();
        byte ctr = 0;
        byte[] tmp = new byte[txt.length];
        int i = 0;
        while (i < txt.length) {
            if (ctr == encrypt_key2.length) {
                ctr = 0;
            }
            tmp[i] = (byte) (txt[i] ^ encrypt_key2[ctr]);
            i++;
            ctr = (byte) (ctr + 1);
        }
        return tmp;
    }
}
