package com.kenfor.tools.encrypt;

import org.kxml2.wap.Wbxml;
import org.xbill.DNS.Flags;

public class Base64 {
    private static byte[] base64DecodeChars;
    private static char[] base64EncodeChars = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

    static {
        byte[] bArr = new byte[128];
        bArr[0] = -1;
        bArr[1] = -1;
        bArr[2] = -1;
        bArr[3] = -1;
        bArr[4] = -1;
        bArr[5] = -1;
        bArr[6] = -1;
        bArr[7] = -1;
        bArr[8] = -1;
        bArr[9] = -1;
        bArr[10] = -1;
        bArr[11] = -1;
        bArr[12] = -1;
        bArr[13] = -1;
        bArr[14] = -1;
        bArr[15] = -1;
        bArr[16] = -1;
        bArr[17] = -1;
        bArr[18] = -1;
        bArr[19] = -1;
        bArr[20] = -1;
        bArr[21] = -1;
        bArr[22] = -1;
        bArr[23] = -1;
        bArr[24] = -1;
        bArr[25] = -1;
        bArr[26] = -1;
        bArr[27] = -1;
        bArr[28] = -1;
        bArr[29] = -1;
        bArr[30] = -1;
        bArr[31] = -1;
        bArr[32] = -1;
        bArr[33] = -1;
        bArr[34] = -1;
        bArr[35] = -1;
        bArr[36] = -1;
        bArr[37] = -1;
        bArr[38] = -1;
        bArr[39] = -1;
        bArr[40] = -1;
        bArr[41] = -1;
        bArr[42] = -1;
        bArr[43] = 62;
        bArr[44] = -1;
        bArr[45] = -1;
        bArr[46] = -1;
        bArr[47] = 63;
        bArr[48] = 52;
        bArr[49] = 53;
        bArr[50] = 54;
        bArr[51] = 55;
        bArr[52] = 56;
        bArr[53] = 57;
        bArr[54] = 58;
        bArr[55] = 59;
        bArr[56] = 60;
        bArr[57] = 61;
        bArr[58] = -1;
        bArr[59] = -1;
        bArr[60] = -1;
        bArr[61] = -1;
        bArr[62] = -1;
        bArr[63] = -1;
        bArr[64] = -1;
        bArr[66] = 1;
        bArr[67] = 2;
        bArr[68] = 3;
        bArr[69] = 4;
        bArr[70] = 5;
        bArr[71] = 6;
        bArr[72] = 7;
        bArr[73] = 8;
        bArr[74] = 9;
        bArr[75] = 10;
        bArr[76] = Flags.CD;
        bArr[77] = 12;
        bArr[78] = 13;
        bArr[79] = 14;
        bArr[80] = 15;
        bArr[81] = 16;
        bArr[82] = 17;
        bArr[83] = 18;
        bArr[84] = 19;
        bArr[85] = 20;
        bArr[86] = 21;
        bArr[87] = 22;
        bArr[88] = 23;
        bArr[89] = 24;
        bArr[90] = 25;
        bArr[91] = -1;
        bArr[92] = -1;
        bArr[93] = -1;
        bArr[94] = -1;
        bArr[95] = -1;
        bArr[96] = -1;
        bArr[97] = 26;
        bArr[98] = 27;
        bArr[99] = 28;
        bArr[100] = 29;
        bArr[101] = 30;
        bArr[102] = 31;
        bArr[103] = 32;
        bArr[104] = 33;
        bArr[105] = 34;
        bArr[106] = 35;
        bArr[107] = 36;
        bArr[108] = 37;
        bArr[109] = 38;
        bArr[110] = 39;
        bArr[111] = 40;
        bArr[112] = 41;
        bArr[113] = 42;
        bArr[114] = 43;
        bArr[115] = 44;
        bArr[116] = 45;
        bArr[117] = 46;
        bArr[118] = 47;
        bArr[119] = 48;
        bArr[120] = 49;
        bArr[121] = 50;
        bArr[122] = 51;
        bArr[123] = -1;
        bArr[124] = -1;
        bArr[125] = -1;
        bArr[126] = -1;
        bArr[127] = -1;
        base64DecodeChars = bArr;
    }

    private Base64() {
    }

    public static String encode(byte[] data) {
        StringBuffer sb = new StringBuffer();
        int len = data.length;
        int i = 0;
        while (true) {
            if (i >= len) {
                break;
            }
            int i2 = i + 1;
            int b1 = data[i] & 255;
            if (i2 == len) {
                sb.append(base64EncodeChars[b1 >>> 2]);
                sb.append(base64EncodeChars[(b1 & 3) << 4]);
                sb.append("==");
                break;
            }
            int i3 = i2 + 1;
            int b2 = data[i2] & 255;
            if (i3 == len) {
                sb.append(base64EncodeChars[b1 >>> 2]);
                sb.append(base64EncodeChars[((b1 & 3) << 4) | ((b2 & 240) >>> 4)]);
                sb.append(base64EncodeChars[(b2 & 15) << 2]);
                sb.append("=");
                break;
            }
            int b3 = data[i3] & 255;
            sb.append(base64EncodeChars[b1 >>> 2]);
            sb.append(base64EncodeChars[((b1 & 3) << 4) | ((b2 & 240) >>> 4)]);
            sb.append(base64EncodeChars[((b2 & 15) << 2) | ((b3 & Wbxml.EXT_0) >>> 6)]);
            sb.append(base64EncodeChars[b3 & 63]);
            i = i3 + 1;
        }
        return sb.toString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x0024 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0026 A[LOOP:2: B:9:0x0026->B:11:0x0031, LOOP_START, PHI: r7 
      PHI: (r7v1 'i' int) = (r7v0 'i' int), (r7v2 'i' int) binds: [B:7:0x0022, B:11:0x0031] A[DONT_GENERATE, DONT_INLINE]] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] decode(java.lang.String r13) {
        /*
            r12 = 61
            r11 = -1
            byte[] r5 = r13.getBytes()
            int r8 = r5.length
            java.io.ByteArrayOutputStream r4 = new java.io.ByteArrayOutputStream
            r4.<init>(r8)
            r6 = 0
        L_0x000e:
            if (r6 < r8) goto L_0x0016
        L_0x0010:
            byte[] r9 = r4.toByteArray()
        L_0x0014:
            return r9
        L_0x0015:
            r6 = r7
        L_0x0016:
            byte[] r9 = com.kenfor.tools.encrypt.Base64.base64DecodeChars
            int r7 = r6 + 1
            byte r10 = r5[r6]
            byte r0 = r9[r10]
            if (r7 >= r8) goto L_0x0022
            if (r0 == r11) goto L_0x0015
        L_0x0022:
            if (r0 != r11) goto L_0x0026
            r6 = r7
            goto L_0x0010
        L_0x0026:
            r6 = r7
            byte[] r9 = com.kenfor.tools.encrypt.Base64.base64DecodeChars
            int r7 = r6 + 1
            byte r10 = r5[r6]
            byte r1 = r9[r10]
            if (r7 >= r8) goto L_0x0033
            if (r1 == r11) goto L_0x0026
        L_0x0033:
            if (r1 != r11) goto L_0x0037
            r6 = r7
            goto L_0x0010
        L_0x0037:
            int r9 = r0 << 2
            r10 = r1 & 48
            int r10 = r10 >>> 4
            r9 = r9 | r10
            r4.write(r9)
        L_0x0041:
            r6 = r7
            int r7 = r6 + 1
            byte r2 = r5[r6]
            if (r2 != r12) goto L_0x004e
            byte[] r9 = r4.toByteArray()
            r6 = r7
            goto L_0x0014
        L_0x004e:
            byte[] r9 = com.kenfor.tools.encrypt.Base64.base64DecodeChars
            byte r2 = r9[r2]
            if (r7 >= r8) goto L_0x0056
            if (r2 == r11) goto L_0x0041
        L_0x0056:
            if (r2 != r11) goto L_0x005a
            r6 = r7
            goto L_0x0010
        L_0x005a:
            r9 = r1 & 15
            int r9 = r9 << 4
            r10 = r2 & 60
            int r10 = r10 >>> 2
            r9 = r9 | r10
            r4.write(r9)
        L_0x0066:
            r6 = r7
            int r7 = r6 + 1
            byte r3 = r5[r6]
            if (r3 != r12) goto L_0x0073
            byte[] r9 = r4.toByteArray()
            r6 = r7
            goto L_0x0014
        L_0x0073:
            byte[] r9 = com.kenfor.tools.encrypt.Base64.base64DecodeChars
            byte r3 = r9[r3]
            if (r7 >= r8) goto L_0x007b
            if (r3 == r11) goto L_0x0066
        L_0x007b:
            if (r3 != r11) goto L_0x007f
            r6 = r7
            goto L_0x0010
        L_0x007f:
            r9 = r2 & 3
            int r9 = r9 << 6
            r9 = r9 | r3
            r4.write(r9)
            r6 = r7
            goto L_0x000e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenfor.tools.encrypt.Base64.decode(java.lang.String):byte[]");
    }
}
