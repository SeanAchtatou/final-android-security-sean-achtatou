package com.kenfor.tools.encrypt;

import org.xbill.DNS.Flags;

public class TAXEncrypt {
    static int FKEY = 1119;
    static int FSEEDA = 1119;
    static int FSEEDB = 1119;

    /* JADX WARN: Type inference failed for: r6v3, types: [byte] */
    public static String TAXEncrypt(String str) {
        int iKey = FKEY;
        byte[] strGet = str.getBytes();
        byte[] result = new byte[strGet.length];
        StringBuffer last_result = new StringBuffer();
        for (int i = 0; i < strGet.length; i++) {
            result[i] = (byte) (strGet[i] ^ (iKey >> 8));
            iKey = (((result[i] > 0 ? result[i] : result[i] + Flags.QR) + iKey) * FSEEDA) + FSEEDB;
        }
        byte[] strGet2 = result;
        for (byte b : strGet2) {
            int j = Integer.valueOf(b & 255).intValue();
            last_result.append((char) ((j / 26) + 65));
            last_result.append((char) ((j % 26) + 65));
        }
        return last_result.toString();
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [byte[]] */
    /* JADX WARN: Type inference failed for: r7v7, types: [byte] */
    /* JADX WARN: Type inference failed for: r7v11, types: [byte] */
    /* JADX WARN: Type inference failed for: r7v12, types: [int, byte] */
    /* JADX WARN: Type inference failed for: r6v3, types: [byte] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String TAXDecrypt(java.lang.String r10) {
        /*
            int r1 = com.kenfor.tools.encrypt.TAXEncrypt.FKEY
            byte[] r5 = r10.getBytes()
            int r7 = r5.length
            int r7 = r7 / 2
            byte[] r4 = new byte[r7]
            java.lang.StringBuffer r3 = new java.lang.StringBuffer
            r3.<init>()
            r2 = 0
            r6 = 0
            r0 = 0
        L_0x0013:
            int r7 = r5.length
            int r7 = r7 / 2
            if (r0 < r7) goto L_0x0036
            java.io.PrintStream r7 = java.lang.System.out
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            java.lang.String r9 = "de result_length"
            r8.<init>(r9)
            int r9 = r4.length
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r8 = r8.toString()
            r7.println(r8)
            r0 = 0
        L_0x002e:
            int r7 = r4.length
            if (r0 < r7) goto L_0x0061
            java.lang.String r7 = r3.toString()
            return r7
        L_0x0036:
            int r7 = r0 * 2
            byte r7 = r5[r7]
            r7 = r7 & 255(0xff, float:3.57E-43)
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)
            int r7 = r7.intValue()
            int r7 = r7 + -65
            int r2 = r7 * 26
            int r7 = r0 * 2
            int r7 = r7 + 1
            byte r7 = r5[r7]
            r7 = r7 & 255(0xff, float:3.57E-43)
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)
            int r7 = r7.intValue()
            int r7 = r7 + -65
            int r2 = r2 + r7
            byte r7 = (byte) r2
            r4[r0] = r7
            int r0 = r0 + 1
            goto L_0x0013
        L_0x0061:
            byte r7 = r4[r0]
            int r8 = r1 >> 8
            r7 = r7 ^ r8
            byte r7 = (byte) r7
            char r7 = (char) r7
            r3.append(r7)
            byte r7 = r4[r0]
            if (r7 <= 0) goto L_0x007d
            byte r6 = r4[r0]
        L_0x0071:
            int r7 = r6 + r1
            int r8 = com.kenfor.tools.encrypt.TAXEncrypt.FSEEDA
            int r7 = r7 * r8
            int r8 = com.kenfor.tools.encrypt.TAXEncrypt.FSEEDB
            int r1 = r7 + r8
            int r0 = r0 + 1
            goto L_0x002e
        L_0x007d:
            byte r7 = r4[r0]
            int r6 = r7 + 256
            goto L_0x0071
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenfor.tools.encrypt.TAXEncrypt.TAXDecrypt(java.lang.String):java.lang.String");
    }
}
