package com.kenfor.tools.remotelogin;

import com.kenfor.tools.encrypt.AzDGCrypt;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class RemoteLoginTool {
    public static Map<String, String> decodeLoginParam(String param, String key, String licenseKey) {
        String keys = AzDGCrypt.decrypt(key, licenseKey);
        System.out.println("key===" + keys);
        String auth = AzDGCrypt.decrypt(param, keys);
        Map<String, String> map = new HashMap<>();
        int index1 = auth.indexOf("=");
        int indexOf = auth.indexOf("&");
        int index = 0;
        while (index1 > 0) {
            int index2 = auth.indexOf("&");
            if (index2 > 0) {
                map.put(auth.substring(0, index1), auth.substring(index1 + 1, index2));
                auth = auth.substring(index2 + 1);
            } else {
                map.put(auth.substring(0, index1), auth.substring(index1 + 1));
                auth = "";
            }
            index++;
            index1 = auth.indexOf("=");
        }
        return map;
    }

    public static String getLoginParam(Map<String, String> paramMaps, String licenseKey) {
        String auth = "";
        String keys = String.valueOf(new Double(Math.random() * 1000.0d).intValue());
        for (String key : paramMaps.keySet()) {
            auth = String.valueOf(auth) + key + "=" + paramMaps.get(key) + "&";
        }
        if (auth.length() > 0) {
            auth = auth.substring(0, auth.length() - 1);
        }
        return "?auth=" + AzDGCrypt.encrypt(auth, keys, "UTF-8") + "&key=" + AzDGCrypt.encrypt(keys, licenseKey, "UTF-8");
    }

    public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();
        map.put("kenfor_ypt", "test.kenfor.com");
        map.put("dt", new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
        map.put("back_url", "www.kenfor.com");
        System.out.println(getLoginParam(map, "kfypt"));
    }
}
