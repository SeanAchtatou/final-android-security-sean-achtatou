package com.kenfor.tools;

import java.io.InputStream;
import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

public class Configurate {
    private static Configurate config = null;
    private Document doc = null;

    private Configurate() {
    }

    public static Configurate getInstance() {
        config = new Configurate();
        try {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            if (classLoader != null) {
                InputStream stream = classLoader.getResourceAsStream("pro-config.xml");
                try {
                    config.doc = new SAXBuilder().build(stream);
                    stream.close();
                } catch (JDOMException e) {
                    e.printStackTrace();
                    stream.close();
                } catch (NullPointerException e2) {
                    e2.printStackTrace();
                    stream.close();
                } catch (Exception e3) {
                    e3.printStackTrace();
                    stream.close();
                } catch (Throwable th) {
                    stream.close();
                    throw th;
                }
            }
            return config;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public String getProperty(String p_propertyName) {
        return this.doc.getRootElement().getChildTextTrim(p_propertyName);
    }
}
