package com.kenfor.client;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.kenfor.client.NotificationService;
import com.kenfor.client3g.util.Constant;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Future;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.AndFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketIDFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Registration;
import org.jivesoftware.smack.provider.ProviderManager;

public class XmppManager {
    /* access modifiers changed from: private */
    public static final String LOGTAG = LogUtil.makeLogTag(XmppManager.class);
    private static final String XMPP_RESOURCE_NAME = "AndroidpnClient";
    /* access modifiers changed from: private */
    public XMPPConnection connection;
    private ConnectionListener connectionListener;
    private Context context;
    /* access modifiers changed from: private */
    public String deviceId;
    private Future<?> futureTask;
    private Handler handler;
    private PacketListener notificationPacketListener;
    /* access modifiers changed from: private */
    public String password;
    private Thread reconnection;
    private boolean running = false;
    /* access modifiers changed from: private */
    public SharedPreferences sharedPrefs;
    /* access modifiers changed from: private */
    public String systemCode;
    private List<Runnable> taskList;
    private NotificationService.TaskSubmitter taskSubmitter;
    private NotificationService.TaskTracker taskTracker;
    /* access modifiers changed from: private */
    public String username;
    /* access modifiers changed from: private */
    public String xmppHost;
    /* access modifiers changed from: private */
    public int xmppPort;

    public XmppManager(NotificationService notificationService) {
        this.context = notificationService;
        this.taskSubmitter = notificationService.getTaskSubmitter();
        this.taskTracker = notificationService.getTaskTracker();
        this.sharedPrefs = notificationService.getSharedPreferences();
        this.xmppHost = this.sharedPrefs.getString(Constants.XMPP_HOST, "localhost");
        this.xmppPort = this.sharedPrefs.getInt(Constants.XMPP_PORT, 5222);
        this.deviceId = this.sharedPrefs.getString(Constants.DEVICE_ID, "");
        if ("".equals(this.deviceId)) {
            this.deviceId = getUuID();
            this.sharedPrefs.edit().putString(Constants.DEVICE_ID, this.deviceId).commit();
        }
        this.systemCode = this.sharedPrefs.getString(Constants.SYSTEM_CODE, "ypt");
        this.username = this.sharedPrefs.getString(Constants.XMPP_USERNAME, "");
        this.password = this.sharedPrefs.getString(Constants.XMPP_PASSWORD, "");
        this.connectionListener = new PersistentConnectionListener(this);
        this.notificationPacketListener = new NotificationPacketListener(this);
        this.handler = new Handler();
        this.taskList = new ArrayList();
        this.reconnection = new ReconnectionThread(this);
    }

    public Context getContext() {
        return this.context;
    }

    public void connect() {
        Log.d(LOGTAG, "connect()...");
        submitLoginTask();
    }

    public void disconnect() {
        Log.d(LOGTAG, "disconnect()...");
        terminatePersistentConnection();
    }

    public void terminatePersistentConnection() {
        Log.d(LOGTAG, "terminatePersistentConnection()...");
        addTask(new Runnable() {
            final XmppManager xmppManager;

            {
                this.xmppManager = XmppManager.this;
            }

            public void run() {
                if (this.xmppManager.isConnected()) {
                    Log.d(XmppManager.LOGTAG, "terminatePersistentConnection()... run()");
                    this.xmppManager.getConnection().removePacketListener(this.xmppManager.getNotificationPacketListener());
                    this.xmppManager.getConnection().disconnect();
                }
                this.xmppManager.runTask();
            }
        });
    }

    public XMPPConnection getConnection() {
        return this.connection;
    }

    public void setConnection(XMPPConnection connection2) {
        this.connection = connection2;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username2) {
        this.username = username2;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password2) {
        this.password = password2;
    }

    public ConnectionListener getConnectionListener() {
        return this.connectionListener;
    }

    public PacketListener getNotificationPacketListener() {
        return this.notificationPacketListener;
    }

    public void startReconnectionThread() {
        synchronized (this.reconnection) {
            if (!this.reconnection.isAlive()) {
                this.reconnection.setName("Xmpp Reconnection Thread");
                this.reconnection.start();
            }
        }
    }

    public Handler getHandler() {
        return this.handler;
    }

    public void reregisterAccount() {
        removeAccount();
        submitLoginTask();
        runTask();
    }

    public List<Runnable> getTaskList() {
        return this.taskList;
    }

    public Future<?> getFutureTask() {
        return this.futureTask;
    }

    public void runTask() {
        Log.d(LOGTAG, "runTask()...");
        synchronized (this.taskList) {
            this.running = false;
            this.futureTask = null;
            if (!this.taskList.isEmpty()) {
                this.taskList.remove(0);
                this.running = true;
                this.futureTask = this.taskSubmitter.submit(this.taskList.get(0));
                if (this.futureTask == null) {
                    this.taskTracker.decrease();
                }
            }
        }
        this.taskTracker.decrease();
        Log.d(LOGTAG, "runTask()...done");
    }

    /* access modifiers changed from: private */
    public String newRandomUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    public boolean isConnected() {
        return this.connection != null && this.connection.isConnected();
    }

    /* access modifiers changed from: private */
    public boolean isAuthenticated() {
        return this.connection != null && this.connection.isConnected() && this.connection.isAuthenticated();
    }

    /* access modifiers changed from: private */
    public boolean isRegistered() {
        return false;
    }

    private void submitConnectTask() {
        Log.d(LOGTAG, "submitConnectTask()...");
        addTask(new ConnectTask(this, null));
    }

    private void submitRegisterTask() {
        Log.d(LOGTAG, "submitRegisterTask()...");
        submitConnectTask();
        addTask(new RegisterTask(this, null));
    }

    private void submitLoginTask() {
        Log.d(LOGTAG, "submitLoginTask()...");
        submitRegisterTask();
        addTask(new LoginTask(this, null));
    }

    private void addTask(Runnable runnable) {
        Log.d(LOGTAG, "addTask(runnable)...");
        this.taskTracker.increase();
        synchronized (this.taskList) {
            if (!this.taskList.isEmpty() || this.running) {
                this.taskList.add(runnable);
                runTask();
            } else {
                this.running = true;
                this.futureTask = this.taskSubmitter.submit(runnable);
                if (this.futureTask == null) {
                    this.taskTracker.decrease();
                }
            }
        }
        Log.d(LOGTAG, "addTask(runnable)... done");
    }

    private void removeAccount() {
        SharedPreferences.Editor editor = this.sharedPrefs.edit();
        editor.remove(Constants.XMPP_USERNAME);
        editor.remove(Constants.XMPP_PASSWORD);
        editor.commit();
    }

    private class ConnectTask implements Runnable {
        final XmppManager xmppManager;

        private ConnectTask() {
            this.xmppManager = XmppManager.this;
        }

        /* synthetic */ ConnectTask(XmppManager xmppManager2, ConnectTask connectTask) {
            this();
        }

        public void run() {
            Log.i(XmppManager.LOGTAG, "ConnectTask.run()...");
            if (!this.xmppManager.isConnected()) {
                ConnectionConfiguration connConfig = new ConnectionConfiguration(XmppManager.this.xmppHost, XmppManager.this.xmppPort);
                connConfig.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
                connConfig.setSASLAuthenticationEnabled(false);
                connConfig.setCompressionEnabled(false);
                XMPPConnection connection = new XMPPConnection(connConfig);
                this.xmppManager.setConnection(connection);
                try {
                    connection.connect();
                    Log.i(XmppManager.LOGTAG, "XMPP connected successfully");
                    ProviderManager.getInstance().addIQProvider("notification", "androidpn:iq:notification", new NotificationIQProvider());
                } catch (XMPPException e) {
                    Log.e(XmppManager.LOGTAG, "XMPP connection failed", e);
                }
                this.xmppManager.runTask();
                return;
            }
            Log.i(XmppManager.LOGTAG, "XMPP connected already");
            this.xmppManager.runTask();
        }
    }

    private class RegisterTask implements Runnable {
        final XmppManager xmppManager;

        private RegisterTask() {
            this.xmppManager = XmppManager.this;
        }

        /* synthetic */ RegisterTask(XmppManager xmppManager2, RegisterTask registerTask) {
            this();
        }

        public void run() {
            Log.i(XmppManager.LOGTAG, "RegisterTask.run()...");
            if (!this.xmppManager.isRegistered()) {
                final String newPassword = XmppManager.this.newRandomUUID();
                Registration registration = new Registration();
                PacketFilter packetFilter = new AndFilter(new PacketIDFilter(registration.getPacketID()), new PacketTypeFilter(IQ.class));
                XmppManager.this.connection.addPacketListener(new PacketListener() {
                    public void processPacket(Packet packet) {
                        Log.d("RegisterTask.PacketListener", "processPacket().....");
                        Log.d("RegisterTask.PacketListener", "packet=" + packet.toXML());
                        if (packet instanceof IQ) {
                            IQ response = (IQ) packet;
                            if (response.getType() == IQ.Type.ERROR) {
                                if (!response.getError().toString().contains("409")) {
                                    Log.e(XmppManager.LOGTAG, "Unknown error while registering XMPP account! " + response.getError().getCondition());
                                }
                            } else if (response.getType() == IQ.Type.RESULT) {
                                String name = null;
                                try {
                                    name = URLEncoder.encode("admin@test.kenfor.com", "utf-8");
                                } catch (Exception e) {
                                }
                                RegisterTask.this.xmppManager.setUsername(name);
                                RegisterTask.this.xmppManager.setPassword(newPassword);
                                Log.d(XmppManager.LOGTAG, "username=" + name);
                                Log.d(XmppManager.LOGTAG, "password=" + newPassword);
                                SharedPreferences.Editor editor = XmppManager.this.sharedPrefs.edit();
                                editor.putString(Constants.XMPP_USERNAME, "admin@test.kenfor.com");
                                editor.putString(Constants.XMPP_PASSWORD, newPassword);
                                editor.commit();
                                Log.i(XmppManager.LOGTAG, "Account registered successfully");
                                RegisterTask.this.xmppManager.runTask();
                            }
                        }
                    }
                }, packetFilter);
                registration.setType(IQ.Type.SET);
                registration.addAttribute("username", "admin@test.kenfor.com");
                registration.addAttribute(Constant.PASSWORD, newPassword);
                registration.addAttribute("deviceId", XmppManager.this.deviceId);
                registration.addAttribute(Constants.SYSTEM_CODE, XmppManager.this.systemCode);
                XmppManager.this.connection.sendPacket(registration);
                return;
            }
            Log.i(XmppManager.LOGTAG, "Account registered already");
            this.xmppManager.runTask();
        }
    }

    private class LoginTask implements Runnable {
        final XmppManager xmppManager;

        private LoginTask() {
            this.xmppManager = XmppManager.this;
        }

        /* synthetic */ LoginTask(XmppManager xmppManager2, LoginTask loginTask) {
            this();
        }

        public void run() {
            Log.i(XmppManager.LOGTAG, "LoginTask.run()...");
            if (!this.xmppManager.isAuthenticated()) {
                Log.d(XmppManager.LOGTAG, "username=" + XmppManager.this.username);
                Log.d(XmppManager.LOGTAG, "password=" + XmppManager.this.password);
                try {
                    XMPPConnection connection = this.xmppManager.getConnection();
                    connection.login(String.valueOf(this.xmppManager.getUsername()) + URLEncoder.encode("$" + XmppManager.this.deviceId, "utf-8"), this.xmppManager.getPassword(), XmppManager.XMPP_RESOURCE_NAME);
                    Log.d(XmppManager.LOGTAG, "Loggedn in successfully");
                    if (this.xmppManager.getConnectionListener() != null) {
                        this.xmppManager.getConnection().addConnectionListener(this.xmppManager.getConnectionListener());
                    }
                    connection.addPacketListener(this.xmppManager.getNotificationPacketListener(), new PacketTypeFilter(NotificationIQ.class));
                    this.xmppManager.runTask();
                } catch (XMPPException e) {
                    e.printStackTrace();
                    e.printStackTrace(System.out);
                    Log.e("sss", "sss", e.fillInStackTrace());
                    Log.e(XmppManager.LOGTAG, "LoginTask.run()... xmpp error");
                    Log.e(XmppManager.LOGTAG, "Failed to login to xmpp server. Caused by: " + e.getMessage());
                    String errorMessage = e.getMessage();
                    if (errorMessage == null || !errorMessage.contains("401")) {
                        this.xmppManager.startReconnectionThread();
                    } else {
                        this.xmppManager.reregisterAccount();
                    }
                } catch (Exception e2) {
                    Log.e(XmppManager.LOGTAG, "LoginTask.run()... other error");
                    Log.e(XmppManager.LOGTAG, "Failed to login to xmpp server. Caused by: " + e2.getMessage());
                    this.xmppManager.startReconnectionThread();
                }
            } else {
                Log.i(XmppManager.LOGTAG, "Logged in already");
                this.xmppManager.runTask();
            }
        }
    }

    private String getUuID() {
        TelephonyManager tm = (TelephonyManager) getContext().getSystemService("phone");
        return new UUID((long) "kenfor".hashCode(), (((long) (tm.getDeviceId()).hashCode()) << 32) | ((long) (tm.getSimSerialNumber()).hashCode())).toString();
    }
}
