package com.kenfor.client;

import org.jivesoftware.smack.packet.IQ;

public class NotificationIQ extends IQ {
    private String apiKey;
    private String broadcast;
    private String id;
    private String info_type;
    private String message;
    private String retention_value1;
    private String system_code;
    private String time;
    private String title;
    private String token;
    private String uri;

    public String getTime() {
        return this.time;
    }

    public void setTime(String time2) {
        this.time = time2;
    }

    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append("notification").append(" xmlns=\"").append("androidpn:iq:notification").append("\">");
        if (this.id != null) {
            buf.append("<id>").append(this.id).append("</id>");
        }
        if (this.time != null) {
            buf.append("<time>").append(this.time).append("</time>");
        }
        if (this.info_type != null) {
            buf.append("<info_type>").append(this.info_type).append("</info_type>");
        }
        if (this.token != null) {
            buf.append("<token>").append(this.token).append("</token>");
        }
        if (this.broadcast != null) {
            buf.append("<broadcast>").append(this.broadcast).append("</broadcast>");
        }
        if (this.system_code != null) {
            buf.append("<system_code>").append(this.system_code).append("</system_code>");
        }
        if (this.retention_value1 != null) {
            buf.append("<retention_value1>").append(this.retention_value1).append("</retention_value1>");
        }
        buf.append("</").append("notification").append("> ");
        return buf.toString();
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getApiKey() {
        return this.apiKey;
    }

    public void setApiKey(String apiKey2) {
        this.apiKey = apiKey2;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message2) {
        this.message = message2;
    }

    public String getUri() {
        return this.uri;
    }

    public void setUri(String url) {
        this.uri = url;
    }

    public String getInfo_type() {
        return this.info_type;
    }

    public void setInfo_type(String infoType) {
        this.info_type = infoType;
    }

    public String getToken() {
        return this.token;
    }

    public void setToken(String token2) {
        this.token = token2;
    }

    public String getBroadcast() {
        return this.broadcast;
    }

    public void setBroadcast(String broadcast2) {
        this.broadcast = broadcast2;
    }

    public String getSystem_code() {
        return this.system_code;
    }

    public void setSystem_code(String systemCode) {
        this.system_code = systemCode;
    }

    public String getRetention_value1() {
        return this.retention_value1;
    }

    public void setRetention_value1(String retentionValue1) {
        this.retention_value1 = retentionValue1;
    }
}
