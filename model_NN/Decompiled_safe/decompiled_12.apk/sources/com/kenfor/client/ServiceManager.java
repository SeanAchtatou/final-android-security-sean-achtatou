package com.kenfor.client;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import java.util.Properties;

public final class ServiceManager {
    private static final String LOGTAG = LogUtil.makeLogTag(ServiceManager.class);
    private String apiKey;
    private String callbackActivityClassName;
    private String callbackActivityPackageName;
    /* access modifiers changed from: private */
    public Context context;
    private Properties props;
    private SharedPreferences sharedPrefs;
    private String shared_preference_name;
    private String systemCode;
    private String version = "0.5.0";
    private String xmppHost;
    private String xmppPort;

    public ServiceManager(Context context2, String shared_preference_name2) {
        this.context = context2;
        this.shared_preference_name = shared_preference_name2;
        if (context2 instanceof Activity) {
            Log.i(LOGTAG, "Callback Activity...");
            Activity callbackActivity = (Activity) context2;
            this.callbackActivityPackageName = callbackActivity.getPackageName();
            this.callbackActivityClassName = callbackActivity.getClass().getName();
        }
        this.props = loadProperties();
        this.apiKey = this.props.getProperty("apiKey", "");
        this.xmppHost = this.props.getProperty("xmppHost", "127.0.0.1");
        this.xmppPort = this.props.getProperty("xmppPort", "5222");
        this.systemCode = this.props.getProperty(Constants.SYSTEM_CODE, "ypt");
        Log.i(LOGTAG, "apiKey=" + this.apiKey);
        Log.i(LOGTAG, "xmppHost=" + this.xmppHost);
        Log.i(LOGTAG, "xmppPort=" + this.xmppPort);
        this.sharedPrefs = context2.getSharedPreferences(shared_preference_name2, 0);
        SharedPreferences.Editor editor = this.sharedPrefs.edit();
        editor.putString(Constants.API_KEY, this.apiKey);
        editor.putString(Constants.VERSION, this.version);
        editor.putString(Constants.XMPP_HOST, this.xmppHost);
        editor.putInt(Constants.XMPP_PORT, Integer.parseInt(this.xmppPort));
        editor.putString(Constants.SYSTEM_CODE, this.systemCode);
        editor.putString(Constants.CALLBACK_ACTIVITY_PACKAGE_NAME, this.callbackActivityPackageName);
        editor.putString(Constants.CALLBACK_ACTIVITY_CLASS_NAME, this.callbackActivityClassName);
        editor.commit();
    }

    public void startService() {
        new Thread(new Runnable() {
            public void run() {
                ServiceManager.this.context.startService(NotificationService.getIntent());
            }
        }).start();
    }

    public void stopService() {
        this.context.stopService(NotificationService.getIntent());
    }

    private Properties loadProperties() {
        Properties props2 = new Properties();
        try {
            int id = this.context.getResources().getIdentifier("androidpn", "raw", this.context.getPackageName());
            System.out.println("id===" + id);
            props2.load(this.context.getResources().openRawResource(id));
        } catch (Exception e) {
            Log.e(LOGTAG, "Could not find the properties file.", e);
        }
        return props2;
    }

    public void setNotificationIcon(int iconId) {
        SharedPreferences.Editor editor = this.sharedPrefs.edit();
        editor.putInt(Constants.NOTIFICATION_ICON, iconId);
        editor.commit();
    }

    public static void viewNotificationSettings(Context context2) {
        context2.startActivity(new Intent().setClass(context2, NotificationSettingsActivity.class));
    }
}
