package com.kenfor.client;

import com.kenfor.client3g.util.Constant;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;

public class NotificationIQProvider implements IQProvider {
    public IQ parseIQ(XmlPullParser parser) throws Exception {
        NotificationIQ notification = new NotificationIQ();
        boolean done = false;
        while (!done) {
            int eventType = parser.next();
            if (eventType == 2) {
                if ("id".equals(parser.getName())) {
                    notification.setId(parser.nextText());
                }
                if ("apiKey".equals(parser.getName())) {
                    notification.setApiKey(parser.nextText());
                }
                if ("title".equals(parser.getName())) {
                    notification.setTitle(parser.nextText());
                }
                if ("message".equals(parser.getName())) {
                    notification.setMessage(parser.nextText());
                }
                if ("system_code".equals(parser.getName())) {
                    notification.setSystem_code(parser.nextText());
                }
                if ("uri".equals(parser.getName())) {
                    notification.setUri(parser.nextText());
                }
                if ("time".equals(parser.getName())) {
                    notification.setTime(parser.nextText());
                }
                if ("info_type".equals(parser.getName())) {
                    notification.setInfo_type(parser.nextText());
                }
                if ("token".equals(parser.getName())) {
                    notification.setToken(parser.nextText());
                }
                if ("broadcast".equals(parser.getName())) {
                    notification.setBroadcast(parser.nextText());
                }
                if (Constant.RETENTION_VALUE1.equals(parser.getName())) {
                    notification.setRetention_value1(parser.nextText());
                }
            } else if (eventType == 3 && "notification".equals(parser.getName())) {
                done = true;
            }
        }
        return notification;
    }
}
