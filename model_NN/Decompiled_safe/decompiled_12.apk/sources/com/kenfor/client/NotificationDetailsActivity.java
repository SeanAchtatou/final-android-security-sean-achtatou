package com.kenfor.client;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class NotificationDetailsActivity extends Activity {
    private static final String LOGTAG = LogUtil.makeLogTag(NotificationDetailsActivity.class);
    /* access modifiers changed from: private */
    public String callbackActivityClassName;
    /* access modifiers changed from: private */
    public String callbackActivityPackageName;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedPrefs = getSharedPreferences("yjk_data", 0);
        this.callbackActivityPackageName = sharedPrefs.getString(Constants.CALLBACK_ACTIVITY_PACKAGE_NAME, "");
        this.callbackActivityClassName = sharedPrefs.getString(Constants.CALLBACK_ACTIVITY_CLASS_NAME, "");
        Intent intent = getIntent();
        String notificationId = intent.getStringExtra(Constants.NOTIFICATION_ID);
        String notificationApiKey = intent.getStringExtra(Constants.NOTIFICATION_API_KEY);
        String notificationTitle = intent.getStringExtra(Constants.NOTIFICATION_TITLE);
        String notificationMessage = intent.getStringExtra(Constants.NOTIFICATION_MESSAGE);
        String notificationUri = intent.getStringExtra(Constants.NOTIFICATION_URI);
        Log.d(LOGTAG, "notificationId=" + notificationId);
        Log.d(LOGTAG, "notificationApiKey=" + notificationApiKey);
        Log.d(LOGTAG, "notificationTitle=" + notificationTitle);
        Log.d(LOGTAG, "notificationMessage=" + notificationMessage);
        Log.d(LOGTAG, "notificationUri=" + notificationUri);
        setContentView(createView(notificationTitle, notificationMessage, notificationUri));
    }

    private View createView(String title, String message, final String uri) {
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setBackgroundColor(-1118482);
        linearLayout.setOrientation(1);
        linearLayout.setPadding(5, 5, 5, 5);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        TextView textTitle = new TextView(this);
        textTitle.setText(title);
        textTitle.setTextSize(18.0f);
        textTitle.setTypeface(Typeface.DEFAULT, 1);
        textTitle.setTextColor(-16777216);
        textTitle.setGravity(17);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.setMargins(30, 30, 30, 0);
        textTitle.setLayoutParams(layoutParams);
        linearLayout.addView(textTitle);
        TextView textDetails = new TextView(this);
        textDetails.setText(message);
        textDetails.setTextSize(14.0f);
        textDetails.setTextColor(-13421773);
        textDetails.setGravity(17);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams2.setMargins(30, 10, 30, 20);
        textDetails.setLayoutParams(layoutParams2);
        linearLayout.addView(textDetails);
        Button okButton = new Button(this);
        okButton.setText("Ok");
        okButton.setWidth(100);
        okButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent;
                if (uri == null || uri.length() <= 0 || (!uri.startsWith("http:") && !uri.startsWith("https:") && !uri.startsWith("tel:") && !uri.startsWith("geo:"))) {
                    intent = new Intent().setClassName(NotificationDetailsActivity.this.callbackActivityPackageName, NotificationDetailsActivity.this.callbackActivityClassName);
                    intent.setFlags(268435456);
                    intent.setFlags(536870912);
                    intent.setFlags(67108864);
                } else {
                    intent = new Intent("android.intent.action.VIEW", Uri.parse(uri));
                }
                NotificationDetailsActivity.this.startActivity(intent);
                NotificationDetailsActivity.this.finish();
            }
        });
        LinearLayout innerLayout = new LinearLayout(this);
        innerLayout.setGravity(17);
        innerLayout.addView(okButton);
        linearLayout.addView(innerLayout);
        return linearLayout;
    }
}
