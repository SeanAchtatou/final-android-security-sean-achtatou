package com.kenfor.client;

import android.telephony.PhoneStateListener;
import android.util.Log;

public class PhoneStateChangeListener extends PhoneStateListener {
    private static final String LOGTAG = LogUtil.makeLogTag(PhoneStateChangeListener.class);
    private final NotificationService notificationService;

    public PhoneStateChangeListener(NotificationService notificationService2) {
        this.notificationService = notificationService2;
    }

    public void onDataConnectionStateChanged(int state) {
        super.onDataConnectionStateChanged(state);
        Log.d(LOGTAG, "onDataConnectionStateChanged()...");
        Log.d(LOGTAG, "Data Connection State = " + getState(state));
        if (state == 2) {
            this.notificationService.connect();
        }
    }

    private String getState(int state) {
        switch (state) {
            case 0:
                return "DATA_DISCONNECTED";
            case 1:
                return "DATA_CONNECTING";
            case 2:
                return "DATA_CONNECTED";
            case 3:
                return "DATA_SUSPENDED";
            default:
                return "DATA_<UNKNOWN>";
        }
    }
}
