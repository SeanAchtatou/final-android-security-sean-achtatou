package com.kenfor.client;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.kenfor.util2.Installation;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class NotificationService extends Service {
    /* access modifiers changed from: private */
    public static final String LOGTAG = LogUtil.makeLogTag(NotificationService.class);
    public static final String SERVICE_NAME = "org.androidpn.client.NotificationService";
    private BroadcastReceiver connectivityReceiver = new ConnectivityReceiver(this);
    private String deviceId;
    private ExecutorService executorService = Executors.newSingleThreadExecutor();
    private BroadcastReceiver notificationReceiver = new NotificationReceiver();
    private PhoneStateListener phoneStateListener = new PhoneStateChangeListener(this);
    private SharedPreferences sharedPrefs;
    private TaskSubmitter taskSubmitter = new TaskSubmitter(this);
    private TaskTracker taskTracker = new TaskTracker(this);
    private TelephonyManager telephonyManager;
    private XmppManager xmppManager;

    public NotificationService() {
        Log.e("NotificationService", "init");
    }

    public void onCreate() {
        Log.d(LOGTAG, "onCreate()...");
        this.telephonyManager = (TelephonyManager) getSystemService("phone");
        this.sharedPrefs = getSharedPreferences("yjk_data", 0);
        String device_id = Installation.id(this);
        this.deviceId = this.telephonyManager.getDeviceId();
        SharedPreferences.Editor editor = this.sharedPrefs.edit();
        editor.putString(Constants.DEVICE_ID, device_id);
        editor.commit();
        if (this.deviceId == null || this.deviceId.trim().length() == 0 || this.deviceId.matches("0+")) {
            if (this.sharedPrefs.contains(Constants.EMULATOR_DEVICE_ID)) {
                this.deviceId = this.sharedPrefs.getString(Constants.EMULATOR_DEVICE_ID, "");
            } else {
                this.deviceId = "EMU" + new Random(System.currentTimeMillis()).nextLong();
                editor.putString(Constants.EMULATOR_DEVICE_ID, this.deviceId);
                editor.commit();
            }
        }
        Log.d(LOGTAG, "deviceId=" + this.deviceId);
        this.xmppManager = new XmppManager(this);
        this.taskSubmitter.submit(new Runnable() {
            public void run() {
                NotificationService.this.start();
            }
        });
    }

    public void onStart(Intent intent, int startId) {
        Log.d(LOGTAG, "onStart()...");
    }

    public void onDestroy() {
        Log.d(LOGTAG, "onDestroy()...");
        stop();
    }

    public IBinder onBind(Intent intent) {
        Log.d(LOGTAG, "onBind()...");
        return null;
    }

    public void onRebind(Intent intent) {
        Log.d(LOGTAG, "onRebind()...");
    }

    public boolean onUnbind(Intent intent) {
        Log.d(LOGTAG, "onUnbind()...");
        return true;
    }

    public static Intent getIntent() {
        return new Intent(SERVICE_NAME);
    }

    public ExecutorService getExecutorService() {
        return this.executorService;
    }

    public TaskSubmitter getTaskSubmitter() {
        return this.taskSubmitter;
    }

    public TaskTracker getTaskTracker() {
        return this.taskTracker;
    }

    public XmppManager getXmppManager() {
        return this.xmppManager;
    }

    public SharedPreferences getSharedPreferences() {
        return this.sharedPrefs;
    }

    public String getDeviceId() {
        return this.deviceId;
    }

    public void connect() {
        Log.d(LOGTAG, "connect()...");
        this.taskSubmitter.submit(new Runnable() {
            public void run() {
                NotificationService.this.getXmppManager().connect();
            }
        });
    }

    public void disconnect() {
        Log.d(LOGTAG, "disconnect()...");
        this.taskSubmitter.submit(new Runnable() {
            public void run() {
                NotificationService.this.getXmppManager().disconnect();
            }
        });
    }

    private void registerNotificationReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.ACTION_SHOW_NOTIFICATION);
        filter.addAction(Constants.ACTION_NOTIFICATION_CLICKED);
        filter.addAction(Constants.ACTION_NOTIFICATION_CLEARED);
        registerReceiver(this.notificationReceiver, filter);
    }

    private void unregisterNotificationReceiver() {
        unregisterReceiver(this.notificationReceiver);
    }

    private void registerConnectivityReceiver() {
        Log.d(LOGTAG, "registerConnectivityReceiver()...");
        this.telephonyManager.listen(this.phoneStateListener, 64);
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(this.connectivityReceiver, filter);
    }

    private void unregisterConnectivityReceiver() {
        Log.d(LOGTAG, "unregisterConnectivityReceiver()...");
        this.telephonyManager.listen(this.phoneStateListener, 0);
        unregisterReceiver(this.connectivityReceiver);
    }

    /* access modifiers changed from: private */
    public void start() {
        Log.d(LOGTAG, "start()...");
        registerNotificationReceiver();
        registerConnectivityReceiver();
        this.xmppManager.connect();
    }

    private void stop() {
        Log.d(LOGTAG, "stop()...");
        unregisterNotificationReceiver();
        unregisterConnectivityReceiver();
        this.xmppManager.disconnect();
        this.executorService.shutdown();
    }

    public class TaskSubmitter {
        final NotificationService notificationService;

        public TaskSubmitter(NotificationService notificationService2) {
            this.notificationService = notificationService2;
        }

        public Future submit(Runnable task) {
            if (this.notificationService.getExecutorService().isTerminated() || this.notificationService.getExecutorService().isShutdown() || task == null) {
                return null;
            }
            return this.notificationService.getExecutorService().submit(task);
        }
    }

    public class TaskTracker {
        public int count = 0;
        final NotificationService notificationService;

        public TaskTracker(NotificationService notificationService2) {
            this.notificationService = notificationService2;
        }

        public void increase() {
            synchronized (this.notificationService.getTaskTracker()) {
                this.notificationService.getTaskTracker().count++;
                Log.d(NotificationService.LOGTAG, "Incremented task count to " + this.count);
            }
        }

        public void decrease() {
            synchronized (this.notificationService.getTaskTracker()) {
                TaskTracker taskTracker = this.notificationService.getTaskTracker();
                taskTracker.count--;
                Log.d(NotificationService.LOGTAG, "Decremented task count to " + this.count);
            }
        }
    }
}
