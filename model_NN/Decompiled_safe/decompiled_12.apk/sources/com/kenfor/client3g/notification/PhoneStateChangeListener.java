package com.kenfor.client3g.notification;

import android.telephony.PhoneStateListener;
import com.kenfor.client.LogUtil;

public class PhoneStateChangeListener extends PhoneStateListener {
    private static final String LOGTAG = LogUtil.makeLogTag(PhoneStateChangeListener.class);
    private final BroadCastService notificationService;

    public PhoneStateChangeListener(BroadCastService notificationService2) {
        this.notificationService = notificationService2;
    }

    public void onDataConnectionStateChanged(int state) {
        super.onDataConnectionStateChanged(state);
        if (state == 2) {
            this.notificationService.connect();
        }
    }

    private String getState(int state) {
        switch (state) {
            case 0:
                return "DATA_DISCONNECTED";
            case 1:
                return "DATA_CONNECTING";
            case 2:
                return "DATA_CONNECTED";
            case 3:
                return "DATA_SUSPENDED";
            default:
                return "DATA_<UNKNOWN>";
        }
    }
}
