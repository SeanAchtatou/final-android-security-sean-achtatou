package com.kenfor.client3g.notification;

import org.jivesoftware.smack.packet.IQ;

public class FeedbackIQ extends IQ {
    private String broadcast;
    private String id;
    private String time;
    private String token;

    public String getTime() {
        return this.time;
    }

    public void setTime(String time2) {
        this.time = time2;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append("feedback").append(" xmlns=\"").append("androidpn:iq:feedback").append("\">");
        if (this.id != null) {
            buf.append("<id>").append(this.id).append("</id>");
        }
        if (this.time != null) {
            buf.append("<time>").append(this.time).append("</time>");
        }
        if (this.token != null) {
            buf.append("<token>").append(this.token).append("</token>");
        }
        if (this.broadcast != null) {
            buf.append("<broadcast>").append(this.broadcast).append("</broadcast>");
        }
        buf.append("</").append("feedback").append("> ");
        return buf.toString();
    }

    public String getToken() {
        return this.token;
    }

    public void setToken(String token2) {
        this.token = token2;
    }

    public String getBroadcast() {
        return this.broadcast;
    }

    public void setBroadcast(String broadcast2) {
        this.broadcast = broadcast2;
    }
}
