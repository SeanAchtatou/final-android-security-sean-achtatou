package com.kenfor.client3g.notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.kenfor.client3g.util.Constants;
import com.kenfor.client3g.util.DataApplication;
import com.kenfor.client3g.wwwqtcmcccom.R;
import java.util.ArrayList;

public class BootBroadcastReceiver extends BroadcastReceiver {
    private DataApplication dataApplication = null;

    public void onReceive(Context context, Intent intent) {
        Log.v("TAG", "开机自动服务自动启动.....");
        initData(context);
        ServiceManager serviceManager = new ServiceManager(context, "yjk_data");
        serviceManager.setNotificationIcon(R.drawable.yjk_logo);
        serviceManager.startService();
    }

    public void initData(Context context) {
        this.dataApplication = (DataApplication) context.getApplicationContext();
        this.dataApplication.setDomain(context.getResources().getString(R.string.client_domain));
        this.dataApplication.setLangid(context.getResources().getString(R.string.client_langid));
        this.dataApplication.setCurrentUrl(context.getResources().getString(R.string.client_home));
        ServiceManager serviceManager = new ServiceManager(context, "yjk_data");
        serviceManager.setNotificationIcon(R.drawable.yjk_logo);
        this.dataApplication.setServiceManager(serviceManager);
        this.dataApplication.setPrefs(context.getSharedPreferences(Constants.SETTINGS, 0));
        this.dataApplication.setEditor(this.dataApplication.getPrefs().edit());
        this.dataApplication.setPushInfoSymbol(context.getResources().getString(R.string.push_info_symbol));
        this.dataApplication.setPushInfoSystemCode(context.getResources().getString(R.string.push_info_systemCode));
        this.dataApplication.setServiceIntent(new Intent(this.dataApplication.getPushInfoSymbol()));
        this.dataApplication.setReceivePushinfoList(new ArrayList());
    }
}
