package com.kenfor.client3g.notification;

import android.content.Intent;
import com.kenfor.client.Constants;
import com.kenfor.client.LogUtil;
import com.kenfor.client.NotificationIQ;
import com.kenfor.client3g.util.Constant;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;

public class NotificationPacketListener implements PacketListener {
    private static final String LOGTAG = LogUtil.makeLogTag(NotificationPacketListener.class);
    private final XmppManager xmppManager;

    public NotificationPacketListener(XmppManager xmppManager2) {
        this.xmppManager = xmppManager2;
    }

    public void processPacket(Packet packet) {
        if (packet instanceof NotificationIQ) {
            NotificationIQ notification = (NotificationIQ) packet;
            if (notification.getChildElementXML().contains("androidpn:iq:notification")) {
                String notificationId = notification.getId();
                String notificationTime = notification.getTime();
                String notificationApiKey = notification.getApiKey();
                String notificationTitle = notification.getTitle();
                String notificationMessage = notification.getMessage();
                String notificationUri = notification.getUri();
                String info_type = notification.getInfo_type();
                String broadcast = notification.getBroadcast();
                String token = notification.getToken();
                String systemCode = notification.getSystem_code();
                String retention_value1 = notification.getRetention_value1();
                FeedbackIQ feedback = new FeedbackIQ();
                feedback.setType(IQ.Type.SET);
                feedback.setId(notificationId);
                feedback.setTime(notificationTime);
                if (broadcast != null) {
                    feedback.setBroadcast(broadcast);
                    feedback.setToken(token);
                }
                this.xmppManager.getConnection().sendPacket(feedback);
                Intent intent = new Intent(Constants.ACTION_SHOW_NOTIFICATION);
                intent.putExtra(Constants.NOTIFICATION_ID, notificationId);
                intent.putExtra(Constants.NOTIFICATION_API_KEY, notificationApiKey);
                intent.putExtra(Constants.NOTIFICATION_TITLE, notificationTitle);
                intent.putExtra(Constants.SYSTEM_CODE, systemCode);
                intent.putExtra(Constants.NOTIFICATION_MESSAGE, notificationMessage);
                intent.putExtra(Constants.NOTIFICATION_URI, notificationUri);
                intent.putExtra(Constant.RETENTION_VALUE1, retention_value1);
                intent.putExtra("info_type", info_type);
                this.xmppManager.getContext().sendBroadcast(intent);
            }
        }
    }
}
