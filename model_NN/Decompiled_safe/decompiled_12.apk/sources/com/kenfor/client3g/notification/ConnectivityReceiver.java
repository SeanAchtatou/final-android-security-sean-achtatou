package com.kenfor.client3g.notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.kenfor.client.LogUtil;

public class ConnectivityReceiver extends BroadcastReceiver {
    private static final String LOGTAG = LogUtil.makeLogTag(ConnectivityReceiver.class);
    private BroadCastService notificationService;

    public ConnectivityReceiver(BroadCastService notificationService2) {
        this.notificationService = notificationService2;
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        NetworkInfo networkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (networkInfo == null) {
            this.notificationService.disconnect();
        } else if (networkInfo.isConnected()) {
            this.notificationService.connect();
        }
    }
}
