package com.kenfor.client3g.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;
import com.kenfor.client.Constants;
import com.kenfor.client.LogUtil;
import com.kenfor.client3g.NotificationDetailActivity;
import com.kenfor.client3g.util.Constant;
import java.util.Random;

public class Notifier {
    private static final String LOGTAG = LogUtil.makeLogTag(Notifier.class);
    private static final Random random = new Random(System.currentTimeMillis());
    private Context context;
    private NotificationManager notificationManager;
    private SharedPreferences sharedPrefs;

    public Notifier(Context context2) {
        this.context = context2;
        this.sharedPrefs = context2.getSharedPreferences("yjk_data", 0);
        this.notificationManager = (NotificationManager) context2.getSystemService("notification");
    }

    public void notify(String notificationId, String title, String message, String pushInfoId) {
        if (isNotificationEnabled()) {
            if (isNotificationToastEnabled()) {
                Toast.makeText(this.context, message, 1).show();
            }
            Notification notification = new Notification();
            notification.icon = getNotificationIcon();
            notification.defaults = 4;
            if (isNotificationSoundEnabled()) {
                notification.defaults |= 1;
            }
            if (isNotificationVibrateEnabled()) {
                notification.defaults |= 2;
            }
            notification.flags |= 16;
            notification.when = System.currentTimeMillis();
            String message2 = message.replaceAll("<br>", "\n");
            Intent intent = new Intent(this.context, NotificationDetailActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.NOTIFICATION_TITLE, title);
            bundle.putString(Constants.NOTIFICATION_MESSAGE, message2);
            bundle.putString(Constant.RETENTION_VALUE1, pushInfoId);
            intent.putExtras(bundle);
            notification.setLatestEventInfo(this.context, title, message2, PendingIntent.getActivity(this.context, Integer.valueOf(notificationId).intValue(), intent, 134217728));
            this.notificationManager.notify(random.nextInt(), notification);
        }
    }

    private int getNotificationIcon() {
        return this.sharedPrefs.getInt(Constants.NOTIFICATION_ICON, 0);
    }

    private boolean isNotificationEnabled() {
        return this.sharedPrefs.getBoolean(Constants.SETTINGS_NOTIFICATION_ENABLED, true);
    }

    private boolean isNotificationSoundEnabled() {
        return this.sharedPrefs.getBoolean(Constants.SETTINGS_SOUND_ENABLED, true);
    }

    private boolean isNotificationVibrateEnabled() {
        return this.sharedPrefs.getBoolean(Constants.SETTINGS_VIBRATE_ENABLED, true);
    }

    private boolean isNotificationToastEnabled() {
        return this.sharedPrefs.getBoolean(Constants.SETTINGS_TOAST_ENABLED, false);
    }
}
