package com.kenfor.client3g.notification;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import com.kenfor.client.Constants;
import com.kenfor.client.LogUtil;
import com.kenfor.client3g.util.DataApplication;
import java.util.Properties;

public final class ServiceManager {
    private static final String LOGTAG = LogUtil.makeLogTag(ServiceManager.class);
    private String apiKey;
    private String callbackActivityClassName;
    private String callbackActivityPackageName;
    private Context context;
    private DataApplication dataApplication = null;
    private Properties props;
    private SharedPreferences sharedPrefs;
    private String shared_preference_name;
    private String systemCode;
    private String version = "0.5.0";
    private String xmppHost;
    private String xmppPort;

    public ServiceManager(Context context2, String shared_preference_name2) {
        this.context = context2;
        this.dataApplication = (DataApplication) context2.getApplicationContext();
        this.shared_preference_name = shared_preference_name2;
        if (context2 instanceof Activity) {
            Activity callbackActivity = (Activity) context2;
            this.callbackActivityPackageName = callbackActivity.getPackageName();
            this.callbackActivityClassName = callbackActivity.getClass().getName();
        }
        this.props = loadProperties();
        this.apiKey = this.props.getProperty("apiKey", "");
        this.xmppHost = this.props.getProperty("xmppHost", "127.0.0.1");
        this.xmppPort = this.props.getProperty("xmppPort", "5222");
        this.systemCode = this.props.getProperty(Constants.SYSTEM_CODE, "ypt");
        this.sharedPrefs = context2.getSharedPreferences(shared_preference_name2, 0);
        SharedPreferences.Editor editor = this.sharedPrefs.edit();
        editor.putString(Constants.API_KEY, this.apiKey);
        editor.putString(Constants.VERSION, this.version);
        editor.putString(Constants.XMPP_HOST, this.xmppHost);
        editor.putInt(Constants.XMPP_PORT, Integer.parseInt(this.xmppPort));
        editor.putString(Constants.SYSTEM_CODE, this.systemCode);
        editor.putString(Constants.CALLBACK_ACTIVITY_PACKAGE_NAME, this.callbackActivityPackageName);
        editor.putString(Constants.CALLBACK_ACTIVITY_CLASS_NAME, this.callbackActivityClassName);
        editor.commit();
    }

    public void startService() {
        this.context.startService(this.dataApplication.getServiceIntent());
    }

    public void stopService() {
        this.context.stopService(this.dataApplication.getServiceIntent());
    }

    private Properties loadProperties() {
        Properties props2 = new Properties();
        try {
            props2.load(this.context.getResources().openRawResource(this.context.getResources().getIdentifier("androidpn", "raw", this.context.getPackageName())));
        } catch (Exception e) {
        }
        return props2;
    }

    public void setNotificationIcon(int iconId) {
        SharedPreferences.Editor editor = this.sharedPrefs.edit();
        editor.putInt(Constants.NOTIFICATION_ICON, iconId);
        editor.commit();
    }
}
