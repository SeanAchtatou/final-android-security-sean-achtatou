package com.kenfor.client3g.notification;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import com.kenfor.client3g.util.Constants;
import com.kenfor.client3g.util.DataApplication;

public class ServiceTool {
    public Context context = null;
    public DataApplication dataApplication = null;
    public SharedPreferences prefs = null;
    public ServiceManager serviceManager = null;
    public Handler startHandler = null;
    public Runnable startRunnable = null;

    public ServiceTool(Context context2) {
        this.context = context2;
        this.dataApplication = (DataApplication) context2.getApplicationContext();
    }

    public void start() {
        if (!(this.startHandler == null || this.startRunnable == null)) {
            this.startHandler.removeCallbacks(this.startRunnable);
        }
        if (this.dataApplication.getPrefs().getInt(Constants.RECEIVE_PUSH_INFO, 1) == 1) {
            this.startHandler = new Handler();
            this.startRunnable = new Runnable() {
                public void run() {
                    ServiceTool.this.dataApplication.getServiceManager().startService();
                }
            };
            this.startHandler.postDelayed(this.startRunnable, 15000);
        }
    }

    public void stop() {
        this.dataApplication.getServiceManager().stopService();
    }

    public void restart() {
        if (this.dataApplication.getPrefs().getInt(Constants.RECEIVE_PUSH_INFO, 1) == 1) {
            stop();
            start();
        }
    }
}
