package com.kenfor.client3g.notification;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.kenfor.client.Constants;
import com.kenfor.client.LogUtil;
import com.kenfor.client3g.util.DataApplication;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class BroadCastService extends Service {
    private static final String LOGTAG = LogUtil.makeLogTag(BroadCastService.class);
    private BroadcastReceiver connectivityReceiver;
    private DataApplication dataApplication;
    private String deviceId;
    private ExecutorService executorService;
    /* access modifiers changed from: private */
    public Handler handler;
    private BroadcastReceiver notificationReceiver;
    private PhoneStateListener phoneStateListener;
    /* access modifiers changed from: private */
    public int runnableInterval;
    private SharedPreferences sharedPrefs;
    private TaskSubmitter taskSubmitter;
    private TaskTracker taskTracker;
    private TelephonyManager telephonyManager;
    private XmppManager xmppManager;

    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e("service", "onStartCommand()");
        return 1;
    }

    public BroadCastService() {
        this.handler = new Handler();
        this.runnableInterval = 120000;
        this.dataApplication = null;
        this.notificationReceiver = new NotificationReceiver();
        this.connectivityReceiver = new ConnectivityReceiver(this);
        this.phoneStateListener = new PhoneStateChangeListener(this);
        this.executorService = Executors.newSingleThreadExecutor();
        this.taskSubmitter = new TaskSubmitter(this);
        this.taskTracker = new TaskTracker(this);
        this.dataApplication = (DataApplication) getApplication();
    }

    public void onCreate() {
        this.telephonyManager = (TelephonyManager) getSystemService("phone");
        this.sharedPrefs = getSharedPreferences("yjk_data", 0);
        SharedPreferences.Editor editor = this.sharedPrefs.edit();
        editor.commit();
        if (this.deviceId == null || this.deviceId.trim().length() == 0 || this.deviceId.matches("0+")) {
            if (this.sharedPrefs.contains(Constants.EMULATOR_DEVICE_ID)) {
                this.deviceId = this.sharedPrefs.getString(Constants.EMULATOR_DEVICE_ID, "");
            } else {
                this.deviceId = "EMU" + new Random(System.currentTimeMillis()).nextLong();
                editor.putString(Constants.EMULATOR_DEVICE_ID, this.deviceId);
                editor.commit();
            }
        }
        this.xmppManager = new XmppManager(this);
        this.taskSubmitter.submit(new Runnable() {
            public void run() {
                BroadCastService.this.start();
                BroadCastService.this.handler.postDelayed(this, (long) BroadCastService.this.runnableInterval);
            }
        });
    }

    public void onStart(Intent intent, int startId) {
    }

    public void onDestroy() {
        stop();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onRebind(Intent intent) {
    }

    public boolean onUnbind(Intent intent) {
        return true;
    }

    public ExecutorService getExecutorService() {
        return this.executorService;
    }

    public TaskSubmitter getTaskSubmitter() {
        return this.taskSubmitter;
    }

    public TaskTracker getTaskTracker() {
        return this.taskTracker;
    }

    public XmppManager getXmppManager() {
        return this.xmppManager;
    }

    public SharedPreferences getSharedPreferences() {
        return this.sharedPrefs;
    }

    public String getDeviceId() {
        return this.deviceId;
    }

    public void connect() {
        this.taskSubmitter.submit(new Runnable() {
            public void run() {
                BroadCastService.this.getXmppManager().connect();
            }
        });
    }

    public void disconnect() {
        this.taskSubmitter.submit(new Runnable() {
            public void run() {
                BroadCastService.this.getXmppManager().disconnect();
            }
        });
    }

    private void registerNotificationReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.ACTION_SHOW_NOTIFICATION);
        filter.addAction(Constants.ACTION_NOTIFICATION_CLICKED);
        filter.addAction(Constants.ACTION_NOTIFICATION_CLEARED);
        registerReceiver(this.notificationReceiver, filter);
    }

    private void unregisterNotificationReceiver() {
        unregisterReceiver(this.notificationReceiver);
    }

    private void registerConnectivityReceiver() {
        this.telephonyManager.listen(this.phoneStateListener, 64);
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(this.connectivityReceiver, filter);
    }

    private void unregisterConnectivityReceiver() {
        this.telephonyManager.listen(this.phoneStateListener, 0);
        unregisterReceiver(this.connectivityReceiver);
    }

    /* access modifiers changed from: private */
    public void start() {
        registerNotificationReceiver();
        registerConnectivityReceiver();
        this.xmppManager.connect();
    }

    private void stop() {
        unregisterNotificationReceiver();
        unregisterConnectivityReceiver();
        this.xmppManager.disconnect();
        this.executorService.shutdown();
    }

    public class TaskSubmitter {
        final BroadCastService notificationService;

        public TaskSubmitter(BroadCastService notificationService2) {
            this.notificationService = notificationService2;
        }

        public Future submit(Runnable task) {
            if (this.notificationService.getExecutorService().isTerminated() || this.notificationService.getExecutorService().isShutdown() || task == null) {
                return null;
            }
            return this.notificationService.getExecutorService().submit(task);
        }
    }

    public class TaskTracker {
        public int count = 0;
        final BroadCastService notificationService;

        public TaskTracker(BroadCastService notificationService2) {
            this.notificationService = notificationService2;
        }

        public void increase() {
            synchronized (this.notificationService.getTaskTracker()) {
                this.notificationService.getTaskTracker().count++;
            }
        }

        public void decrease() {
            synchronized (this.notificationService.getTaskTracker()) {
                TaskTracker taskTracker = this.notificationService.getTaskTracker();
                taskTracker.count--;
            }
        }
    }
}
