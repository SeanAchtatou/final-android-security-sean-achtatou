package com.kenfor.client3g.notification;

import com.kenfor.client.LogUtil;

public class ReconnectionThread extends Thread {
    private static final String LOGTAG = LogUtil.makeLogTag(ReconnectionThread.class);
    private int waiting = 0;
    /* access modifiers changed from: private */
    public final XmppManager xmppManager;

    ReconnectionThread(XmppManager xmppManager2) {
        this.xmppManager = xmppManager2;
    }

    public void run() {
        while (!isInterrupted() && !this.xmppManager.isConnected()) {
            try {
                Thread.sleep(((long) waiting()) * 1000);
                this.xmppManager.connect();
                this.waiting++;
            } catch (InterruptedException e) {
                this.xmppManager.getHandler().post(new Runnable() {
                    public void run() {
                        ReconnectionThread.this.xmppManager.getConnectionListener().reconnectionFailed(e);
                    }
                });
                return;
            }
        }
    }

    private int waiting() {
        if (this.waiting > 20) {
            return 600;
        }
        if (this.waiting > 13) {
            return 300;
        }
        return this.waiting <= 7 ? 10 : 60;
    }
}
