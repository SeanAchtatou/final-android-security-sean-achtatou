package com.kenfor.client3g.notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.kenfor.client.Constants;
import com.kenfor.client3g.util.Constant;
import com.kenfor.client3g.util.DataApplication;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public final class NotificationReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        DataApplication dataApplication = (DataApplication) context.getApplicationContext();
        String action = intent.getAction();
        String systemCode = intent.getStringExtra(Constants.SYSTEM_CODE);
        String pushInfoSymbol = dataApplication.getPushInfoSystemCode();
        if (Constants.ACTION_SHOW_NOTIFICATION.equals(action) && systemCode != null && systemCode.equals(pushInfoSymbol)) {
            String notificationId = intent.getStringExtra(Constants.NOTIFICATION_ID);
            if (!dataApplication.getReceivePushinfoList().contains(notificationId)) {
                dataApplication.getReceivePushinfoList().add(notificationId);
                String stringExtra = intent.getStringExtra(Constants.NOTIFICATION_API_KEY);
                String notificationTitle = intent.getStringExtra(Constants.NOTIFICATION_TITLE);
                String notificationMessage = intent.getStringExtra(Constants.NOTIFICATION_MESSAGE);
                String stringExtra2 = intent.getStringExtra(Constants.NOTIFICATION_URI);
                String stringExtra3 = intent.getStringExtra("info_type");
                String pushInfoId = intent.getStringExtra(Constant.RETENTION_VALUE1);
                System.out.println("---------NotificationReceiver:id:" + notificationId + "----retention_value1:" + parseJson(pushInfoId, "id"));
                new Notifier(context).notify(notificationId, notificationTitle, notificationMessage, parseJson(pushInfoId, "id"));
            }
        }
    }

    private String parseJson(String jsonStr, String returnValueKey) {
        if (jsonStr == null || "".equals(jsonStr)) {
            return "";
        }
        try {
            return ((JSONObject) new JSONTokener(jsonStr).nextValue()).getString(returnValueKey);
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }
}
