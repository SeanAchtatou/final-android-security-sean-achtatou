package com.kenfor.client3g.notification;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.telephony.TelephonyManager;
import com.kenfor.client.Constants;
import com.kenfor.client.LogUtil;
import com.kenfor.client.NotificationIQ;
import com.kenfor.client.NotificationIQProvider;
import com.kenfor.client3g.notification.BroadCastService;
import com.kenfor.client3g.util.Constant;
import com.kenfor.client3g.util.DataApplication;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Future;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.AndFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketIDFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Registration;
import org.jivesoftware.smack.provider.ProviderManager;

public class XmppManager {
    private static final String LOGTAG = LogUtil.makeLogTag(XmppManager.class);
    private static final String XMPP_RESOURCE_NAME = "AndroidpnClient";
    /* access modifiers changed from: private */
    public XMPPConnection connection;
    private ConnectionListener connectionListener;
    private Context context;
    private long cop_id = 0;
    /* access modifiers changed from: private */
    public DataApplication dataApplication = null;
    private Future<?> futureTask;
    private Handler handler;
    private PacketListener notificationPacketListener;
    private String password;
    /* access modifiers changed from: private */
    public String push_type = "system_code";
    private Thread reconnection;
    private boolean running = false;
    /* access modifiers changed from: private */
    public SharedPreferences sharedPrefs;
    private String systemCode = "yjk";
    private List<Runnable> taskList;
    private BroadCastService.TaskSubmitter taskSubmitter;
    private BroadCastService.TaskTracker taskTracker;
    /* access modifiers changed from: private */
    public String token;
    private String username;
    /* access modifiers changed from: private */
    public String xmppHost;
    /* access modifiers changed from: private */
    public int xmppPort;

    public XmppManager(BroadCastService notificationService) {
        this.context = notificationService;
        this.dataApplication = (DataApplication) this.context.getApplicationContext();
        this.taskSubmitter = notificationService.getTaskSubmitter();
        this.taskTracker = notificationService.getTaskTracker();
        this.sharedPrefs = notificationService.getSharedPreferences();
        this.xmppHost = this.sharedPrefs.getString(Constants.XMPP_HOST, "localhost");
        this.xmppPort = this.sharedPrefs.getInt(Constants.XMPP_PORT, 5222);
        this.token = this.sharedPrefs.getString("token", "");
        this.cop_id = this.sharedPrefs.getLong(Constant.COP_ID, 0);
        if ("".equals(this.token)) {
            this.token = getUuID();
        }
        this.username = this.sharedPrefs.getString(Constant.USER_NAME, "");
        this.password = this.sharedPrefs.getString(Constants.XMPP_PASSWORD, "");
        this.connectionListener = new PersistentConnectionListener(this);
        this.notificationPacketListener = new NotificationPacketListener(this);
        this.handler = new Handler();
        this.taskList = new ArrayList();
        this.reconnection = new ReconnectionThread(this);
    }

    public Context getContext() {
        return this.context;
    }

    public void connect() {
        submitLoginTask();
    }

    public void disconnect() {
        terminatePersistentConnection();
    }

    public void terminatePersistentConnection() {
        addTask(new Runnable() {
            final XmppManager xmppManager;

            {
                this.xmppManager = XmppManager.this;
            }

            public void run() {
                if (this.xmppManager.isConnected()) {
                    this.xmppManager.getConnection().removePacketListener(this.xmppManager.getNotificationPacketListener());
                    this.xmppManager.getConnection().disconnect();
                }
                this.xmppManager.runTask();
            }
        });
    }

    public XMPPConnection getConnection() {
        return this.connection;
    }

    public void setConnection(XMPPConnection connection2) {
        this.connection = connection2;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username2) {
        this.username = username2;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password2) {
        this.password = password2;
    }

    public ConnectionListener getConnectionListener() {
        return this.connectionListener;
    }

    public PacketListener getNotificationPacketListener() {
        return this.notificationPacketListener;
    }

    public void startReconnectionThread() {
        synchronized (this.reconnection) {
            if (!this.reconnection.isAlive()) {
                this.reconnection.setName("Xmpp Reconnection Thread");
                this.reconnection.start();
            }
        }
    }

    public Handler getHandler() {
        return this.handler;
    }

    public void reregisterAccount() {
        removeAccount();
        submitLoginTask();
        runTask();
    }

    public List<Runnable> getTaskList() {
        return this.taskList;
    }

    public Future<?> getFutureTask() {
        return this.futureTask;
    }

    public void runTask() {
        synchronized (this.taskList) {
            this.running = false;
            this.futureTask = null;
            if (!this.taskList.isEmpty()) {
                this.taskList.remove(0);
                this.running = true;
                this.futureTask = this.taskSubmitter.submit(this.taskList.get(0));
                if (this.futureTask == null) {
                    this.taskTracker.decrease();
                }
            }
        }
        this.taskTracker.decrease();
    }

    private String newRandomUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    public boolean isConnected() {
        return this.connection != null && this.connection.isConnected();
    }

    /* access modifiers changed from: private */
    public boolean isAuthenticated() {
        return this.connection != null && this.connection.isConnected() && this.connection.isAuthenticated();
    }

    /* access modifiers changed from: private */
    public boolean isRegistered() {
        return false;
    }

    private void submitConnectTask() {
        addTask(new ConnectTask(this, null));
    }

    private void submitRegisterTask() {
        submitConnectTask();
        addTask(new RegisterTask(this, null));
    }

    private void submitLoginTask() {
        submitRegisterTask();
        addTask(new LoginTask(this, null));
    }

    private void addTask(Runnable runnable) {
        this.taskTracker.increase();
        synchronized (this.taskList) {
            if (!this.taskList.isEmpty() || this.running) {
                this.taskList.add(runnable);
                runTask();
            } else {
                this.running = true;
                this.futureTask = this.taskSubmitter.submit(runnable);
                if (this.futureTask == null) {
                    this.taskTracker.decrease();
                }
            }
        }
    }

    private void removeAccount() {
        SharedPreferences.Editor editor = this.sharedPrefs.edit();
        editor.remove(Constants.XMPP_USERNAME);
        editor.remove(Constants.XMPP_PASSWORD);
        editor.commit();
    }

    private class ConnectTask implements Runnable {
        final XmppManager xmppManager;

        private ConnectTask() {
            this.xmppManager = XmppManager.this;
        }

        /* synthetic */ ConnectTask(XmppManager xmppManager2, ConnectTask connectTask) {
            this();
        }

        public void run() {
            if (!this.xmppManager.isConnected()) {
                ConnectionConfiguration connConfig = new ConnectionConfiguration(XmppManager.this.xmppHost, XmppManager.this.xmppPort);
                connConfig.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
                connConfig.setSASLAuthenticationEnabled(false);
                connConfig.setCompressionEnabled(false);
                XMPPConnection connection = new XMPPConnection(connConfig);
                this.xmppManager.setConnection(connection);
                try {
                    connection.connect();
                    ProviderManager.getInstance().addIQProvider("notification", "androidpn:iq:notification", new NotificationIQProvider());
                } catch (XMPPException e) {
                }
                this.xmppManager.runTask();
                return;
            }
            this.xmppManager.runTask();
        }
    }

    private class RegisterTask implements Runnable {
        final XmppManager xmppManager;

        private RegisterTask() {
            this.xmppManager = XmppManager.this;
        }

        /* synthetic */ RegisterTask(XmppManager xmppManager2, RegisterTask registerTask) {
            this();
        }

        public void run() {
            if (!this.xmppManager.isRegistered()) {
                final String newUsername = XmppManager.this.sharedPrefs.getString(Constant.USER_NAME, "");
                Registration registration = new Registration();
                PacketFilter packetFilter = new AndFilter(new PacketIDFilter(registration.getPacketID()), new PacketTypeFilter(IQ.class));
                XmppManager.this.connection.addPacketListener(new PacketListener() {
                    public void processPacket(Packet packet) {
                        if (packet instanceof IQ) {
                            IQ response = (IQ) packet;
                            if (response.getType() == IQ.Type.ERROR) {
                                response.getError().toString().contains("409");
                            } else if (response.getType() == IQ.Type.RESULT) {
                                try {
                                    String name = URLEncoder.encode(newUsername, "utf-8");
                                } catch (Exception e) {
                                }
                                SharedPreferences.Editor editor = XmppManager.this.sharedPrefs.edit();
                                editor.putString(Constants.XMPP_USERNAME, newUsername);
                                editor.commit();
                                RegisterTask.this.xmppManager.runTask();
                            }
                        }
                    }
                }, packetFilter);
                registration.setType(IQ.Type.SET);
                String memberAccount = XmppManager.this.dataApplication.getPrefs().getString(com.kenfor.client3g.util.Constants.MEMBER_ACCOUNT, "");
                String memberPassword = XmppManager.this.dataApplication.getPrefs().getString(com.kenfor.client3g.util.Constants.MEMBER_PASSWORD, "");
                if ("".equals(memberAccount) || "".equals(memberPassword)) {
                    System.out.println("222222222222222  ---  ");
                    registration.addAttribute("username", newUsername);
                } else {
                    System.out.println("111111111111111  ---  " + memberAccount);
                    registration.addAttribute("username", memberAccount);
                }
                registration.addAttribute(Constants.SYSTEM_CODE, XmppManager.this.dataApplication.getPushInfoSystemCode());
                registration.addAttribute("token", XmppManager.this.token);
                registration.addAttribute("push_type", XmppManager.this.push_type);
                XmppManager.this.connection.sendPacket(registration);
                return;
            }
            this.xmppManager.runTask();
        }
    }

    private class LoginTask implements Runnable {
        final XmppManager xmppManager;

        private LoginTask() {
            this.xmppManager = XmppManager.this;
        }

        /* synthetic */ LoginTask(XmppManager xmppManager2, LoginTask loginTask) {
            this();
        }

        public void run() {
            if (!this.xmppManager.isAuthenticated()) {
                try {
                    XMPPConnection connection = this.xmppManager.getConnection();
                    connection.login(XmppManager.this.token, this.xmppManager.getPassword(), XmppManager.XMPP_RESOURCE_NAME);
                    if (this.xmppManager.getConnectionListener() != null) {
                        this.xmppManager.getConnection().addConnectionListener(this.xmppManager.getConnectionListener());
                    }
                    connection.addPacketListener(this.xmppManager.getNotificationPacketListener(), new PacketTypeFilter(NotificationIQ.class));
                    this.xmppManager.runTask();
                } catch (XMPPException e) {
                    e.printStackTrace();
                    e.printStackTrace(System.out);
                    String errorMessage = e.getMessage();
                    if (errorMessage == null || !errorMessage.contains("401")) {
                        this.xmppManager.startReconnectionThread();
                    } else {
                        this.xmppManager.reregisterAccount();
                    }
                } catch (Exception e2) {
                    this.xmppManager.startReconnectionThread();
                }
            } else {
                this.xmppManager.runTask();
            }
        }
    }

    private String getUuID() {
        TelephonyManager tm = (TelephonyManager) getContext().getSystemService("phone");
        return new UUID((long) "kenfor".hashCode(), (((long) (tm.getDeviceId()).hashCode()) << 32) | ((long) (tm.getSimSerialNumber()).hashCode())).toString().replaceAll("-", "");
    }
}
