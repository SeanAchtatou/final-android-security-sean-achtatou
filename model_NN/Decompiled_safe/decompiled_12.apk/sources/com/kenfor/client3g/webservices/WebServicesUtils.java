package com.kenfor.client3g.webservices;

import android.util.Log;
import java.io.IOException;
import java.util.LinkedHashMap;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

public class WebServicesUtils {
    public static final String GET_APP_CONFIG = "getAppConfig";
    public static final String GET_APP_VERSION = "getAppVersion";
    public static final String GET_VERSION_3G = "getVersion3g";
    public static final String SERVICE_NAME_SPACE = "http://android.webservices.kenfor_3g/";
    public static final String SERVICE_URL = "http://wz.kenfor.com//xfireservices/ypt3gclient";

    public static String executeMethod(String methodName, LinkedHashMap<String, Object> param) {
        try {
            SoapObject request = new SoapObject(SERVICE_NAME_SPACE, methodName);
            if (param != null) {
                for (String key : param.keySet()) {
                    request.addProperty(key, param.get(key));
                }
            }
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.bodyOut = request;
            new MarshalBase64().register(envelope);
            HttpTransportSE transport = new HttpTransportSE(SERVICE_URL);
            transport.debug = true;
            transport.call(SERVICE_NAME_SPACE + methodName, envelope);
            return envelope.getResponse().toString();
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("webService", e.getMessage());
            return null;
        } catch (XmlPullParserException e2) {
            e2.printStackTrace();
            Log.e("webService", e2.getMessage());
            return null;
        } catch (Exception e3) {
            e3.printStackTrace();
            Log.e("webService", e3.getMessage());
            return null;
        }
    }

    public static String getXmlStringValue(String result, String key) {
        if (result == null || "".equals(result) || result.indexOf(key) == -1) {
            return "";
        }
        return result.substring(result.indexOf("<" + key + ">") + key.length() + 2, result.indexOf("</" + key + ">"));
    }
}
