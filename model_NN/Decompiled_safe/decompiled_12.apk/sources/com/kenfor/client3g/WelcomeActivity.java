package com.kenfor.client3g;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.KeyEvent;
import com.kenfor.client3g.util.Activities;
import com.kenfor.client3g.util.Constants;
import com.kenfor.client3g.util.DataApplication;
import com.kenfor.client3g.wwwqtcmcccom.MainActivity;
import com.kenfor.client3g.wwwqtcmcccom.R;
import java.util.Timer;
import java.util.TimerTask;

public class WelcomeActivity extends BaseOuterActivity implements DialogInterface.OnClickListener {
    private AlertDialog dialog = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Activities.getInstance().addActivity(this);
        setContentView((int) R.layout.welcome);
        installShortcut();
    }

    public void installShortcut() {
        DataApplication dataApplication = (DataApplication) getApplicationContext();
        final SharedPreferences.Editor editor = dataApplication.getPrefs().edit();
        if (dataApplication.getPrefs().getBoolean(Constants.IS_INSTALL, false)) {
            redirect();
        } else {
            new AlertDialog.Builder(this).setMessage("是否创建桌面快捷方式").setCancelable(false).setPositiveButton("是", new DialogInterface.OnClickListener() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
                 arg types: [java.lang.String, int]
                 candidates:
                  ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
                public void onClick(DialogInterface dialog, int which) {
                    Intent shortcutintent = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
                    shortcutintent.putExtra("duplicate", false);
                    shortcutintent.putExtra("android.intent.extra.shortcut.NAME", WelcomeActivity.this.getResources().getString(R.string.app_name));
                    shortcutintent.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", Intent.ShortcutIconResource.fromContext(WelcomeActivity.this.getApplicationContext(), R.drawable.client_logo));
                    new Intent("android.intent.action.MAIN").setClassName("com.kenfor.client3g", ".MainActivity");
                    shortcutintent.putExtra("android.intent.extra.shortcut.INTENT", new Intent(WelcomeActivity.this.getApplicationContext(), MainActivity.class).setAction("android.intent.action.MAIN").addCategory("android.intent.category.LAUNCHER"));
                    WelcomeActivity.this.sendBroadcast(shortcutintent);
                    editor.putBoolean(Constants.IS_INSTALL, true);
                    editor.commit();
                    WelcomeActivity.this.redirect();
                }
            }).setNegativeButton("否", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    editor.putBoolean(Constants.IS_INSTALL, true);
                    editor.commit();
                    dialog.cancel();
                    WelcomeActivity.this.redirect();
                }
            }).create().show();
        }
    }

    /* access modifiers changed from: private */
    public void redirect() {
        new Timer().schedule(new TimerTask() {
            public void run() {
                Intent intent = new Intent();
                if (WelcomeActivity.this.isConnection()) {
                    WelcomeActivity.this.setResult(0, intent);
                } else {
                    WelcomeActivity.this.setResult(1, intent);
                }
                WelcomeActivity.this.finish();
            }
        }, 3000);
    }

    /* access modifiers changed from: private */
    public boolean isConnection() {
        NetworkInfo networkInfo = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
        if (networkInfo != null) {
            return networkInfo.isAvailable();
        }
        return false;
    }

    public void onClick(DialogInterface dialog2, int which) {
        switch (which) {
            case -2:
                dialog2.cancel();
                return;
            case -1:
                Activities.getInstance().exit();
                return;
            default:
                return;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        this.dialog = new AlertDialog.Builder(this).create();
        this.dialog.setMessage("确定要退出吗？");
        this.dialog.setButton("确定退出", this);
        this.dialog.setButton2("返回", this);
        this.dialog.show();
        return true;
    }
}
