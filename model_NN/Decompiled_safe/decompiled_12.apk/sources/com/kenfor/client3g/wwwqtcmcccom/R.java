package com.kenfor.client3g.wwwqtcmcccom;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int back = 2130837504;
        public static final int check_network = 2130837505;
        public static final int checkbox = 2130837506;
        public static final int checkbox_false = 2130837507;
        public static final int checkbox_true = 2130837508;
        public static final int client_logo = 2130837509;
        public static final int common_top_bg = 2130837510;
        public static final int ic_launcher = 2130837511;
        public static final int ic_preference_single_normal = 2130837512;
        public static final int ic_preference_single_pressed = 2130837513;
        public static final int icon = 2130837514;
        public static final int kenfor_logo = 2130837515;
        public static final int member_button_001 = 2130837516;
        public static final int memberlogin_checkbox = 2130837517;
        public static final int membermain_button = 2130837518;
        public static final int membermain_button2 = 2130837519;
        public static final int membermain_button2_normal = 2130837520;
        public static final int membermain_button2_pressed = 2130837521;
        public static final int membermain_button3 = 2130837522;
        public static final int membermain_button3_normal = 2130837523;
        public static final int membermain_button3_pressed = 2130837524;
        public static final int membermain_button_normal = 2130837525;
        public static final int membermain_button_pressed = 2130837526;
        public static final int mm_submenu = 2130837527;
        public static final int mm_submenu_normal = 2130837528;
        public static final int mm_submenu_pressed = 2130837529;
        public static final int notification = 2130837530;
        public static final int preference_single_item = 2130837531;
        public static final int shape_bottom_corner_no_top_line = 2130837532;
        public static final int shape_no_corner_without_bottom = 2130837533;
        public static final int shape_top_corner_no_bottom_line = 2130837534;
        public static final int welcome = 2130837535;
        public static final int yjk_logo = 2130837536;
    }

    public static final class id {
        public static final int check_network = 2131165184;
        public static final int memberlogin_account = 2131165188;
        public static final int memberlogin_back = 2131165187;
        public static final int memberlogin_login = 2131165191;
        public static final int memberlogin_password = 2131165189;
        public static final int memberlogin_register = 2131165192;
        public static final int memberlogin_remember_account = 2131165190;
        public static final int memberlogin_top = 2131165186;
        public static final int membermain_account = 2131165195;
        public static final int membermain_back = 2131165194;
        public static final int membermain_logout = 2131165199;
        public static final int membermain_mobile = 2131165197;
        public static final int membermain_note = 2131165198;
        public static final int membermain_registertime = 2131165196;
        public static final int membermain_top = 2131165193;
        public static final int memberregister_account = 2131165202;
        public static final int memberregister_back = 2131165201;
        public static final int memberregister_mobile = 2131165205;
        public static final int memberregister_note = 2131165206;
        public static final int memberregister_password = 2131165203;
        public static final int memberregister_password_confirm = 2131165204;
        public static final int memberregister_register = 2131165207;
        public static final int memberregister_top = 2131165200;
        public static final int mywebview = 2131165185;
        public static final int noWebTryAgain = 2131165211;
        public static final int notification_back = 2131165209;
        public static final int notification_text = 2131165210;
        public static final int notification_top = 2131165208;
        public static final int setting_back = 2131165213;
        public static final int setting_update = 2131165216;
        public static final int setting_update_textView_1 = 2131165217;
        public static final int setting_update_textView_2 = 2131165218;
        public static final int settings_pushinfo_checkBox = 2131165215;
        public static final int settings_pushinfo_relativeLayout = 2131165214;
        public static final int settintg_top = 2131165212;
        public static final int update_progress = 2131165219;
    }

    public static final class layout {
        public static final int check_network = 2130903040;
        public static final int main = 2130903041;
        public static final int memberlogin = 2130903042;
        public static final int membermain = 2130903043;
        public static final int memberregister = 2130903044;
        public static final int notification_dialog = 2130903045;
        public static final int noweb = 2130903046;
        public static final int settings = 2130903047;
        public static final int test = 2130903048;
        public static final int update_progress = 2130903049;
        public static final int welcome = 2130903050;
    }

    public static final class raw {
        public static final int androidpn = 2130968576;
    }

    public static final class string {
        public static final int app_name = 2131034112;
        public static final int check_update = 2131034121;
        public static final int client_domain = 2131034115;
        public static final int client_home = 2131034116;
        public static final int client_langid = 2131034117;
        public static final int config_label = 2131034119;
        public static final int hello = 2131034113;
        public static final int push_info_enable = 2131034120;
        public static final int push_info_label = 2131034118;
        public static final int push_info_symbol = 2131034127;
        public static final int push_info_systemCode = 2131034128;
        public static final int settings = 2131034114;
        public static final int update_cancel = 2131034126;
        public static final int update_info = 2131034123;
        public static final int update_later = 2131034125;
        public static final int update_now = 2131034124;
        public static final int update_title = 2131034122;
    }

    public static final class style {
        public static final int memberloginCheckbox = 2131099649;
        public static final int myCheckBox = 2131099648;
    }
}
