package com.kenfor.client3g.wwwqtcmcccom;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.kenfor.client3g.BaseInnerActivity;
import com.kenfor.client3g.CheckNetworkActivity;
import com.kenfor.client3g.WelcomeActivity;
import com.kenfor.client3g.member.MemberTool;
import com.kenfor.client3g.notification.ServiceManager;
import com.kenfor.client3g.notification.ServiceTool;
import com.kenfor.client3g.setting.UpdateApp;
import com.kenfor.client3g.util.Activities;
import com.kenfor.client3g.util.Constant;
import com.kenfor.client3g.util.Constants;
import com.kenfor.client3g.util.DataApplication;
import com.kenfor.client3g.webservices.WebServicesUtils;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.UUID;

public class MainActivity extends BaseInnerActivity {
    /* access modifiers changed from: private */
    public int clickBackTimes = 0;
    private WebViewClient client = new WebViewClient() {
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (MainActivity.this.isConnection()) {
                view.loadUrl(url);
                return true;
            }
            MainActivity.this.startActivityForResult(new Intent(MainActivity.this, CheckNetworkActivity.class), 0);
            return true;
        }

        public void onPageFinished(WebView view, String url) {
            if (MainActivity.this.dataApplication.getVisitTimes() == 0) {
                MainActivity.this.dataApplication.setVisitTimes(MainActivity.this.dataApplication.getVisitTimes() + 1);
            } else if (MainActivity.this.loadingDialog != null && MainActivity.this.loadingDialog.isShowing()) {
                MainActivity.this.loadingDialog.hide();
            }
            MainActivity.this.myWebView.loadUrl("javascript:changeTelAndSms()");
            super.onPageFinished(view, url);
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            MainActivity.this.clickBackTimes = 0;
            MainActivity.this.dataApplication.setCurrentUrl(url);
            if (MainActivity.this.dataApplication.getVisitTimes() != 0) {
                if (MainActivity.this.loadingDialog != null && MainActivity.this.loadingDialog.isShowing()) {
                    MainActivity.this.loadingDialog.hide();
                }
                MainActivity.this.loadingDialog.show();
            }
            super.onPageStarted(view, url, favicon);
        }

        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            if (!MainActivity.this.isConnection()) {
                MainActivity.this.startActivityForResult(new Intent(MainActivity.this, CheckNetworkActivity.class), 0);
            }
            super.onReceivedError(view, errorCode, description, failingUrl);
        }

        public void onLoadResource(WebView view, String url) {
            super.onLoadResource(view, url);
        }
    };
    /* access modifiers changed from: private */
    public DataApplication dataApplication = null;
    /* access modifiers changed from: private */
    public Dialog loadingDialog = null;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    public WebView myWebView = null;
    private Object smsJs = new Object() {
        public void clickOnAndroid(final String param) {
            MainActivity.this.mHandler.post(new Runnable() {
                public void run() {
                    MainActivity.this.startActivity(new Intent("android.intent.action.CALL", Uri.parse("sms:" + param)));
                }
            });
        }
    };
    private Object telJs = new Object() {
        public void clickOnAndroid(final String param) {
            MainActivity.this.mHandler.post(new Runnable() {
                public void run() {
                    MainActivity.this.startActivity(new Intent("android.intent.action.DIAL", Uri.parse("tel:" + param)));
                }
            });
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Activities.getInstance().addActivity(this);
        initData();
        this.loadingDialog = new Dialog(this);
        this.loadingDialog.requestWindowFeature(1);
        this.loadingDialog.addContentView(new ProgressBar(this), new WindowManager.LayoutParams(90, 90));
        setContentView((int) R.layout.main);
        this.myWebView = (WebView) findViewById(R.id.mywebview);
        this.myWebView.setWebViewClient(this.client);
        this.myWebView.getSettings().setUserAgentString(this.myWebView.getSettings().getUserAgentString().replace("Mobile Safari", "Mobile Safari Kenfor"));
        this.myWebView.getSettings().setJavaScriptEnabled(true);
        this.myWebView.setScrollBarStyle(0);
        this.myWebView.requestFocus(130);
        if (isConnection()) {
            this.myWebView.loadUrl(this.dataApplication.getCurrentUrl());
        }
        this.myWebView.addJavascriptInterface(this.telJs, "tel");
        this.myWebView.addJavascriptInterface(this.smsJs, "sms");
        startActivityForResult(new Intent(this, WelcomeActivity.class), 0);
    }

    public void initData() {
        this.dataApplication = (DataApplication) getApplicationContext();
        this.dataApplication.setDomain(getResources().getString(R.string.client_domain));
        this.dataApplication.setLangid(getResources().getString(R.string.client_langid));
        this.dataApplication.setCurrentUrl(getResources().getString(R.string.client_home));
        ServiceManager serviceManager = new ServiceManager(this, "yjk_data");
        serviceManager.setNotificationIcon(R.drawable.yjk_logo);
        this.dataApplication.setServiceManager(serviceManager);
        this.dataApplication.setPrefs(getSharedPreferences(Constants.SETTINGS, 0));
        this.dataApplication.setEditor(this.dataApplication.getPrefs().edit());
        this.dataApplication.setPushInfoSymbol(getResources().getString(R.string.push_info_symbol));
        this.dataApplication.setPushInfoSystemCode(getResources().getString(R.string.push_info_systemCode));
        this.dataApplication.setServiceIntent(new Intent(this.dataApplication.getPushInfoSymbol()));
        this.dataApplication.setReceivePushinfoList(new ArrayList());
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == 0) {
            new Thread(new UpdateHandler()).start();
            if (this.myWebView.getUrl() == null || this.myWebView.getUrl().trim().length() == 0) {
                this.myWebView.loadUrl(this.dataApplication.getCurrentUrl());
            } else {
                this.myWebView.reload();
            }
        } else if (resultCode == 1) {
            startActivityForResult(new Intent(this, CheckNetworkActivity.class), 0);
        } else if (resultCode == 2) {
            this.dataApplication.setVisitTimes(1);
            this.myWebView.loadUrl(this.dataApplication.getCurrentUrl());
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    class UpdateHandler implements Runnable {
        UpdateHandler() {
        }

        public void run() {
            Looper.prepare();
            MainActivity.this.receivePushInfo();
            MainActivity.this.checkIsMember();
            UpdateApp updateApp = new UpdateApp(MainActivity.this);
            if (updateApp.checkUpdate()) {
                updateApp.showNoticeDialog();
            }
            Looper.loop();
        }
    }

    public void receivePushInfo() {
        if (this.dataApplication.getPrefs() != null && this.dataApplication.getPrefs().getInt(Constants.RECEIVE_PUSH_INFO, 1) == 1 && this.dataApplication.getServiceManager() != null) {
            if (!"1".equals(WebServicesUtils.getXmlStringValue(new MemberTool(this).checkLoginInfoValid(this.dataApplication.getPrefs().getString(Constants.MEMBER_ACCOUNT, ""), this.dataApplication.getPrefs().getString(Constants.MEMBER_PASSWORD, ""), getResources().getString(R.string.client_domain)), "status"))) {
                this.dataApplication.getEditor().putString(Constants.MEMBER_ACCOUNT, "");
                this.dataApplication.getEditor().putString(Constants.MEMBER_PASSWORD, "");
                this.dataApplication.getEditor().commit();
            }
            TelephonyManager tm = (TelephonyManager) getBaseContext().getSystemService("phone");
            String deviceId = new UUID((long) (String.valueOf(this.dataApplication.getPushInfoSymbol()) + Settings.Secure.getString(getContentResolver(), "android_id")).hashCode(), (((long) (tm.getDeviceId()).hashCode()) << 32) | ((long) (tm.getSimSerialNumber()).hashCode())).toString().replaceAll("-", "");
            Log.e("id", deviceId);
            getSharedPreferences("yjk_data", 0).edit().putString("token", deviceId).commit();
            new ServiceTool(this).start();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4 && this.myWebView.canGoBack() && this.myWebView != null) {
            this.myWebView.goBack();
            return true;
        } else if (keyCode == 4 && !this.myWebView.canGoBack() && this.clickBackTimes == 0) {
            this.clickBackTimes++;
            Toast.makeText(this, "再按一次退出客户端", 0).show();
            return true;
        } else if (keyCode != 4 || this.myWebView.canGoBack() || this.clickBackTimes != 1) {
            return super.onKeyDown(keyCode, event);
        } else {
            Activities.getInstance().exit();
            return true;
        }
    }

    /* access modifiers changed from: private */
    public boolean isConnection() {
        NetworkInfo networkInfo = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
        if (networkInfo != null) {
            return networkInfo.isAvailable();
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.loadingDialog.dismiss();
        super.onPause();
    }

    public void checkIsMember() {
        new Thread() {
            public void run() {
                LinkedHashMap<String, Object> param = new LinkedHashMap<>();
                param.put(Constant.DOMAIN, MainActivity.this.dataApplication.getDomain());
                param.put("langid", MainActivity.this.dataApplication.getLangid());
                String result = WebServicesUtils.executeMethod(WebServicesUtils.GET_APP_CONFIG, param);
                if (result != null && result.trim().length() != 0) {
                    String hasMember = WebServicesUtils.getXmlStringValue(result, "hasMember");
                    if ("1".equals(hasMember)) {
                        MainActivity.this.dataApplication.getEditor().putInt(Constants.HAS_MEMBER, 1);
                    } else if ("0".equals(hasMember)) {
                        MainActivity.this.dataApplication.getEditor().putInt(Constants.HAS_MEMBER, 0);
                        MainActivity.this.dataApplication.getEditor().putString(Constants.MEMBER_ACCOUNT, "");
                        MainActivity.this.dataApplication.getEditor().putString(Constants.MEMBER_PASSWORD, "");
                    }
                    String themeAppColor = WebServicesUtils.getXmlStringValue(result, "themeAppColor");
                    if (!"".equals(themeAppColor)) {
                        MainActivity.this.dataApplication.getEditor().putString(Constants.THEME_APP_COLOR, themeAppColor.trim());
                    }
                    MainActivity.this.dataApplication.getEditor().commit();
                }
            }
        }.start();
    }
}
