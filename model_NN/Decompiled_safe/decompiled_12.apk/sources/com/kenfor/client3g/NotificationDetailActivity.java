package com.kenfor.client3g;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.kenfor.client3g.member.MemberActivity;
import com.kenfor.client3g.util.Activities;
import com.kenfor.client3g.util.Constant;
import com.kenfor.client3g.util.Constants;
import com.kenfor.client3g.util.DataApplication;
import com.kenfor.client3g.wwwqtcmcccom.R;
import java.net.URL;
import java.util.regex.Pattern;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.jivesoftware.smackx.packet.IBBExtensions;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class NotificationDetailActivity extends BaseInnerActivity {
    Html.ImageGetter imgGetter = new Html.ImageGetter() {
        public Drawable getDrawable(String source) {
            try {
                Drawable drawable = Drawable.createFromStream(new URL(source).openStream(), "");
                drawable.setBounds(0, 0, drawable.getIntrinsicWidth() * 2, drawable.getIntrinsicHeight() * 2);
                return drawable;
            } catch (Exception e) {
                return null;
            }
        }
    };
    private ImageView notificationBack = null;
    private View.OnClickListener notificationBackListener = new View.OnClickListener() {
        public void onClick(View v) {
            NotificationDetailActivity.this.finish();
        }
    };
    private LinearLayout notificationTop = null;
    private String pushInfoUrl = "http://wz.kenfor.com/front/app/appDataAction.do";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Activities.getInstance().addActivity(this);
        setContentView((int) R.layout.notification_dialog);
        this.notificationTop = (LinearLayout) findViewById(R.id.notification_top);
        this.notificationBack = (ImageView) findViewById(R.id.notification_back);
        Bundle bundle = getIntent().getExtras();
        this.notificationBack.setOnClickListener(this.notificationBackListener);
        String themeAppColor = getSharedPreferences(Constants.SETTINGS, 0).getString(Constants.THEME_APP_COLOR, "");
        if (!"".equals(themeAppColor)) {
            this.notificationTop.setBackgroundColor(Color.parseColor(themeAppColor));
        }
        final String pushInfoId = bundle.getString(Constant.RETENTION_VALUE1);
        final MemberActivity mActivity = new MemberActivity(this);
        mActivity.showProgressDialog("查看中...");
        new Handler().post(new Runnable() {
            public void run() {
                String result = NotificationDetailActivity.this.getPushInfo(pushInfoId, "pushinfo");
                mActivity.closeProgressDialog();
                if (result != null && result.length() > 0) {
                    ((TextView) NotificationDetailActivity.this.findViewById(R.id.notification_text)).setText(Html.fromHtml(NotificationDetailActivity.this.parseJson(result, "content_rich"), NotificationDetailActivity.this.imgGetter, null));
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public String parseJson(String jsonStr, String returnValueKey) {
        if (jsonStr == null || "".equals(jsonStr)) {
            return "";
        }
        try {
            return new JSONObject(new JSONArray(new JSONObject(jsonStr).getString(IBBExtensions.Data.ELEMENT_NAME)).getString(0)).getString(returnValueKey);
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    /* access modifiers changed from: private */
    public String getPushInfo(String pushInfoId, String system) {
        if (pushInfoId == null || "".equals(pushInfoId) || !Pattern.compile("[0-9]*").matcher(pushInfoId).find()) {
            return null;
        }
        DataApplication dataApplication = (DataApplication) getApplicationContext();
        DefaultHttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(String.valueOf(this.pushInfoUrl) + "?domain=" + dataApplication.getDomain() + "&langid=" + dataApplication.getLangid() + "&key=" + Constant.KEY + "&system=" + system + "&id=" + pushInfoId);
        httpPost.setHeader("Cookie", "JSESSIONID=" + dataApplication.getJsessionid());
        HttpContext context = new BasicHttpContext();
        context.setAttribute("http.cookie-store", new BasicCookieStore());
        try {
            return EntityUtils.toString(client.execute(httpPost, context).getEntity(), "utf-8");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
