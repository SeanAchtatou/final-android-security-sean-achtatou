package com.kenfor.client3g;

import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import com.kenfor.client3g.member.MemberMainActivity;
import com.kenfor.client3g.setting.SettingsActivity;
import com.kenfor.client3g.util.Activities;
import com.kenfor.client3g.util.Constants;
import com.kenfor.client3g.util.DataApplication;

public class BaseInnerActivity extends Activity {
    private DataApplication dataApplication = null;

    public boolean onCreateOptionsMenu(Menu menu) {
        this.dataApplication = (DataApplication) getApplicationContext();
        if (this.dataApplication.getPrefs().getInt(Constants.HAS_MEMBER, 0) == 1) {
            menu.add(0, 1, 1, "会员").setIcon(17301576);
        }
        menu.add(0, 2, 2, "设置").setIcon(17301569);
        menu.add(0, 3, 3, "退出").setIcon(17301560);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == 1) {
            startActivity(new Intent(this, MemberMainActivity.class));
        }
        if (item.getItemId() == 2) {
            startActivity(new Intent(this, SettingsActivity.class));
        }
        if (item.getItemId() == 3) {
            Activities.getInstance().exit();
        }
        return super.onOptionsItemSelected(item);
    }
}
