package com.kenfor.client3g;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import com.kenfor.client3g.util.Activities;
import com.kenfor.client3g.wwwqtcmcccom.R;

public class CheckNetworkActivity extends BaseOuterActivity implements View.OnClickListener, DialogInterface.OnClickListener {
    private AlertDialog dialog = null;
    private ImageView imageView = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Activities.getInstance().addActivity(this);
        setContentView((int) R.layout.check_network);
        this.imageView = (ImageView) findViewById(R.id.check_network);
        this.imageView.setOnClickListener(this);
        Toast.makeText(getApplicationContext(), "网络不给力!", 0).show();
    }

    private boolean isConnection() {
        NetworkInfo networkInfo = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
        if (networkInfo != null) {
            return networkInfo.isAvailable();
        }
        return false;
    }

    public void onClick(View v) {
        if (v.getId() != R.id.check_network) {
            return;
        }
        if (!isConnection()) {
            Toast.makeText(getApplicationContext(), "网络不给力!", 0).show();
            return;
        }
        setResult(2, new Intent());
        finish();
    }

    public void onClick(DialogInterface dialog2, int which) {
        switch (which) {
            case -2:
                dialog2.cancel();
                return;
            case -1:
                Activities.getInstance().exit();
                return;
            default:
                return;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        this.dialog = new AlertDialog.Builder(this).create();
        this.dialog.setMessage("确定要退出吗？");
        this.dialog.setButton("确定退出", this);
        this.dialog.setButton2("返回", this);
        this.dialog.show();
        return true;
    }
}
