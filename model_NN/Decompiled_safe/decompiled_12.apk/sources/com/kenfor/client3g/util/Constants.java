package com.kenfor.client3g.util;

public class Constants {
    public static final String HAS_MEMBER = "has_member";
    public static final String IS_INSTALL = "is_install";
    public static final String LICENSEKEY = "kenfor_app";
    public static final String MEMBERLOGIN_REMEMBER_ACCOUNT = "memberlogin_remember_account";
    public static final String MEMBER_ACCOUNT = "member_account";
    public static final String MEMBER_PASSWORD = "member_password";
    public static final String RECEIVE_PUSH_INFO = "receive_push_info";
    public static final String SETTINGS = "settings";
    public static final String THEME_APP_COLOR = "theme_app_color";
}
