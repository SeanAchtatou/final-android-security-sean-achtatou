package com.kenfor.client3g.util;

public class Constant {
    public static final String COP_ID = "cop_id";
    public static final long DELAY_TIME = 10000;
    public static final String DOMAIN = "domain";
    public static final int FAILURE = -1;
    public static final int FILESIZE = 4096;
    public static final int G_NETWORK = 2;
    public static final int JASON_ERROR = 3;
    public static final String JOB_AWOKE = "http://zmht.kenfor.com/servlet/JobAwokeForMobileServlet";
    public static final String KEY = "kenfor_app_4008307686";
    public static final String LAST_LOGIN_DT = "last_login_dt";
    public static final int NETWORK_ERROR = 2;
    public static final int NO_AVIABLE_NETWORK = 0;
    public static final int NO_DATA = 1;
    public static final String PACKAGE_NAME = "com.kenfor.cordova.reporter";
    public static final String PASSPORT_ID = "passport_id";
    public static final String PASSWORD = "password";
    public static final long PERIOD_TIME = 18000000;
    public static final String PHONE = "pt";
    public static final String RETENTION_VALUE1 = "retention_value1";
    public static final String SHARED_PREFERENCE_NAME = "yjk_data";
    public static final String SHUT_DOWN_STATUS = "SHUT_DOWN_STATUS";
    public static final int SUCCESS = 0;
    public static final String SUFFIX = ".yjk";
    public static final String SWE = "swe";
    public static final String USER_NAME = "userName";
    public static final int WIFI_NETWORK = 1;
    public static final String WLT = "wlt";
    public static final String YYJB = "http://zmht.kenfor.com/servlet/YyjbServlet";
}
