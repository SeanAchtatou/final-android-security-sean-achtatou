package com.kenfor.client3g.util;

import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;
import com.kenfor.client3g.notification.ServiceManager;
import java.util.List;

public class DataApplication extends Application {
    public String apkName = "kenfor_3g";
    public String currentUrl = "";
    public String domain = "";
    public String downloadUrl = "";
    public SharedPreferences.Editor editor = null;
    public String jsessionid = "";
    public String langid = "";
    public int localVersion3gCode = 0;
    public String localVersion3gName = "";
    public SharedPreferences prefs = null;
    public String pushInfoSymbol = "";
    public String pushInfoSystemCode = "";
    public List<String> receivePushinfoList = null;
    public int remoteVersion3gCode = 0;
    public String remoteVersion3gDate = "";
    public String remoteVersion3gName = "";
    public String remoteVersion3gNote = "";
    public Intent serviceIntent = null;
    public ServiceManager serviceManager = null;
    public String version = "1.0";
    public int visitTimes = 0;

    public String getPushInfoSystemCode() {
        return this.pushInfoSystemCode;
    }

    public void setPushInfoSystemCode(String pushInfoSystemCode2) {
        this.pushInfoSystemCode = pushInfoSystemCode2;
    }

    public String getPushInfoSymbol() {
        return this.pushInfoSymbol;
    }

    public void setPushInfoSymbol(String pushInfoSymbol2) {
        this.pushInfoSymbol = pushInfoSymbol2;
    }

    public List<String> getReceivePushinfoList() {
        return this.receivePushinfoList;
    }

    public void setReceivePushinfoList(List<String> receivePushinfoList2) {
        this.receivePushinfoList = receivePushinfoList2;
    }

    public Intent getServiceIntent() {
        return this.serviceIntent;
    }

    public void setServiceIntent(Intent serviceIntent2) {
        this.serviceIntent = serviceIntent2;
    }

    public SharedPreferences.Editor getEditor() {
        return this.editor;
    }

    public void setEditor(SharedPreferences.Editor editor2) {
        this.editor = editor2;
    }

    public SharedPreferences getPrefs() {
        return this.prefs;
    }

    public void setPrefs(SharedPreferences prefs2) {
        this.prefs = prefs2;
    }

    public String getCurrentUrl() {
        return this.currentUrl;
    }

    public void setCurrentUrl(String currentUrl2) {
        this.currentUrl = currentUrl2;
    }

    public int getVisitTimes() {
        return this.visitTimes;
    }

    public void setVisitTimes(int visitTimes2) {
        this.visitTimes = visitTimes2;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String version2) {
        this.version = version2;
    }

    public int getLocalVersion3gCode() {
        return this.localVersion3gCode;
    }

    public void setLocalVersion3gCode(int localVersion3gCode2) {
        this.localVersion3gCode = localVersion3gCode2;
    }

    public String getLocalVersion3gName() {
        return this.localVersion3gName;
    }

    public void setLocalVersion3gName(String localVersion3gName2) {
        this.localVersion3gName = localVersion3gName2;
    }

    public int getRemoteVersion3gCode() {
        return this.remoteVersion3gCode;
    }

    public void setRemoteVersion3gCode(int remoteVersion3gCode2) {
        this.remoteVersion3gCode = remoteVersion3gCode2;
    }

    public String getRemoteVersion3gName() {
        return this.remoteVersion3gName;
    }

    public void setRemoteVersion3gName(String remoteVersion3gName2) {
        this.remoteVersion3gName = remoteVersion3gName2;
    }

    public String getRemoteVersion3gDate() {
        return this.remoteVersion3gDate;
    }

    public void setRemoteVersion3gDate(String remoteVersion3gDate2) {
        this.remoteVersion3gDate = remoteVersion3gDate2;
    }

    public String getRemoteVersion3gNote() {
        return this.remoteVersion3gNote;
    }

    public void setRemoteVersion3gNote(String remoteVersion3gNote2) {
        this.remoteVersion3gNote = remoteVersion3gNote2;
    }

    public String getDownloadUrl() {
        return this.downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl2) {
        this.downloadUrl = downloadUrl2;
    }

    public String getApkName() {
        return this.apkName;
    }

    public void setApkName(String apkName2) {
        this.apkName = apkName2;
    }

    public String getDomain() {
        return this.domain;
    }

    public void setDomain(String domain2) {
        this.domain = domain2;
    }

    public String getLangid() {
        return this.langid;
    }

    public void setLangid(String langid2) {
        this.langid = langid2;
    }

    public String getJsessionid() {
        return this.jsessionid;
    }

    public void setJsessionid(String jsessionid2) {
        this.jsessionid = jsessionid2;
    }

    public ServiceManager getServiceManager() {
        return this.serviceManager;
    }

    public void setServiceManager(ServiceManager serviceManager2) {
        this.serviceManager = serviceManager2;
    }
}
