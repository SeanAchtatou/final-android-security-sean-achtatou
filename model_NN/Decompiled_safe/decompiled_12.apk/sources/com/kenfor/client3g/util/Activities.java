package com.kenfor.client3g.util;

import android.app.Activity;
import android.app.Application;
import java.util.LinkedList;
import java.util.List;

public class Activities extends Application {
    private static Activities instance;
    private List<Activity> activities = new LinkedList();

    private Activities() {
    }

    public static Activities getInstance() {
        if (instance == null) {
            instance = new Activities();
        }
        return instance;
    }

    public void addActivity(Activity activity) {
        this.activities.add(activity);
    }

    public List<Activity> getActivities() {
        return this.activities;
    }

    public void exit() {
        for (Activity activity : this.activities) {
            activity.finish();
        }
    }
}
