package com.kenfor.client3g.member;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.kenfor.client3g.notification.ServiceTool;
import com.kenfor.client3g.util.Activities;
import com.kenfor.client3g.util.Constants;
import com.kenfor.client3g.util.DataApplication;
import com.kenfor.client3g.webservices.WebServicesUtils;
import com.kenfor.client3g.wwwqtcmcccom.R;

public class MemberMainActivity extends MemberActivity {
    Runnable accountUI = new Runnable() {
        public void run() {
            MemberMainActivity.this.membermainAccount.setText(MemberMainActivity.this.memberAccountText);
        }
    };
    /* access modifiers changed from: private */
    public DataApplication dataApplication = null;
    /* access modifiers changed from: private */
    public String domain = null;
    /* access modifiers changed from: private */
    public Handler mHandler = null;
    /* access modifiers changed from: private */
    public String memberAccount = null;
    /* access modifiers changed from: private */
    public String memberAccountText = null;
    /* access modifiers changed from: private */
    public String memberMobile = "";
    /* access modifiers changed from: private */
    public String memberNote = "";
    /* access modifiers changed from: private */
    public String memberPassword = null;
    /* access modifiers changed from: private */
    public String memberRegistertimeText = null;
    /* access modifiers changed from: private */
    public MemberTool memberTool = null;
    /* access modifiers changed from: private */
    public TextView membermainAccount = null;
    /* access modifiers changed from: private */
    public ImageView membermainBack = null;
    /* access modifiers changed from: private */
    public View.OnClickListener membermainBackListener = new View.OnClickListener() {
        public void onClick(View v) {
            MemberMainActivity.this.closeAllMemberActivity();
        }
    };
    /* access modifiers changed from: private */
    public Button membermainLogout = null;
    /* access modifiers changed from: private */
    public View.OnClickListener membermainLogoutListener = new View.OnClickListener() {
        public void onClick(View v) {
            MemberMainActivity.this.dataApplication.getEditor().putString(Constants.MEMBER_ACCOUNT, "");
            MemberMainActivity.this.dataApplication.getEditor().putString(Constants.MEMBER_PASSWORD, "");
            MemberMainActivity.this.dataApplication.getEditor().commit();
            new ServiceTool(MemberMainActivity.this).restart();
            new Thread() {
                public void run() {
                    MemberMainActivity.this.memberTool.logout();
                }
            }.start();
            MemberMainActivity.this.finish();
            MemberMainActivity.this.startActivity(new Intent(MemberMainActivity.this, MemberLoginActivity.class));
        }
    };
    /* access modifiers changed from: private */
    public TextView membermainMobile = null;
    /* access modifiers changed from: private */
    public TextView membermainNote = null;
    /* access modifiers changed from: private */
    public TextView membermainRegistertime = null;
    private LinearLayout membermainTop = null;
    Runnable registerUI = new Runnable() {
        public void run() {
            MemberMainActivity.this.membermainRegistertime.setText(MemberMainActivity.this.memberRegistertimeText);
            MemberMainActivity.this.membermainMobile.setText(MemberMainActivity.this.memberMobile);
            MemberMainActivity.this.membermainNote.setText(MemberMainActivity.this.memberNote);
        }
    };

    public MemberMainActivity() {
    }

    public MemberMainActivity(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Activities.getInstance().addActivity(this);
        this.dataApplication = (DataApplication) getApplicationContext();
        this.memberTool = new MemberTool(this);
        this.mHandler = new Handler();
        getSessionId();
        this.memberAccount = this.dataApplication.getPrefs().getString(Constants.MEMBER_ACCOUNT, "");
        this.memberPassword = this.dataApplication.getPrefs().getString(Constants.MEMBER_PASSWORD, "");
        this.domain = getResources().getString(R.string.client_domain);
        if ("".equals(this.memberAccount) || "".equals(this.memberPassword)) {
            finish();
            startActivity(new Intent(this, MemberLoginActivity.class));
            return;
        }
        setContentView((int) R.layout.membermain);
        this.membermainTop = (LinearLayout) findViewById(R.id.membermain_top);
        String themeAppColor = this.dataApplication.getPrefs().getString(Constants.THEME_APP_COLOR, "");
        if (!"".equals(themeAppColor)) {
            this.membermainTop.setBackgroundColor(Color.parseColor(themeAppColor));
        }
        final MemberActivity mActivity = new MemberActivity(this);
        mActivity.showProgressDialog("获取会员信息中...");
        new Thread() {
            public void run() {
                Looper.prepare();
                String result = MemberMainActivity.this.memberTool.getLoginInfo(MemberMainActivity.this.memberAccount, MemberMainActivity.this.memberPassword, MemberMainActivity.this.domain);
                mActivity.closeProgressDialog();
                if (result == null || "".equals(result) || !"1".equals(WebServicesUtils.getXmlStringValue(result, "status")) || "".equals(WebServicesUtils.getXmlStringValue(result, "account"))) {
                    MemberMainActivity.this.dataApplication.getEditor().putString(Constants.MEMBER_ACCOUNT, "");
                    MemberMainActivity.this.dataApplication.getEditor().putString(Constants.MEMBER_PASSWORD, "");
                    MemberMainActivity.this.dataApplication.getEditor().commit();
                    MemberMainActivity.this.finish();
                    MemberMainActivity.this.startActivity(new Intent(MemberMainActivity.this, MemberLoginActivity.class));
                } else {
                    MemberMainActivity.this.memberAccount = WebServicesUtils.getXmlStringValue(result, "account");
                    MemberMainActivity.this.membermainBack = (ImageView) MemberMainActivity.this.findViewById(R.id.membermain_back);
                    MemberMainActivity.this.membermainAccount = (TextView) MemberMainActivity.this.findViewById(R.id.membermain_account);
                    MemberMainActivity.this.membermainRegistertime = (TextView) MemberMainActivity.this.findViewById(R.id.membermain_registertime);
                    MemberMainActivity.this.membermainMobile = (TextView) MemberMainActivity.this.findViewById(R.id.membermain_mobile);
                    MemberMainActivity.this.membermainNote = (TextView) MemberMainActivity.this.findViewById(R.id.membermain_note);
                    MemberMainActivity.this.membermainLogout = (Button) MemberMainActivity.this.findViewById(R.id.membermain_logout);
                    MemberMainActivity.this.memberAccountText = WebServicesUtils.getXmlStringValue(result, "account");
                    MemberMainActivity.this.memberRegistertimeText = WebServicesUtils.getXmlStringValue(result, "registertime");
                    MemberMainActivity.this.memberNote = WebServicesUtils.getXmlStringValue(result, "note");
                    MemberMainActivity.this.memberMobile = WebServicesUtils.getXmlStringValue(result, "mobile");
                    MemberMainActivity.this.mHandler.post(MemberMainActivity.this.accountUI);
                    MemberMainActivity.this.mHandler.post(MemberMainActivity.this.registerUI);
                    MemberMainActivity.this.membermainBack.setOnClickListener(MemberMainActivity.this.membermainBackListener);
                    MemberMainActivity.this.membermainLogout.setOnClickListener(MemberMainActivity.this.membermainLogoutListener);
                }
                Looper.loop();
            }
        }.start();
    }

    public void getSessionId() {
        CookieSyncManager.createInstance(this);
        String cookieStr = CookieManager.getInstance().getCookie(this.dataApplication.getDomain());
        if (cookieStr != null) {
            String[] cookies = cookieStr.split(";");
            for (String cookie : cookies) {
                if ("JSESSIONID".equals(cookie.split("=")[0].toUpperCase())) {
                    this.dataApplication.setJsessionid(cookie.split("=")[1]);
                }
            }
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        closeAllMemberActivity();
        return true;
    }

    public void closeAllMemberActivity() {
        for (Activity activity : Activities.getInstance().getActivities()) {
            if (activity.getClass().getName().indexOf("Member") != -1) {
                activity.finish();
            }
        }
    }
}
