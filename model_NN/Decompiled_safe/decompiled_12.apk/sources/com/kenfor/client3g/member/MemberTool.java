package com.kenfor.client3g.member;

import android.content.Context;
import com.kenfor.client3g.util.Constant;
import com.kenfor.client3g.util.Constants;
import com.kenfor.client3g.util.DataApplication;
import com.kenfor.tools.remotelogin.RemoteLoginTool;
import java.util.HashMap;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

public class MemberTool {
    private final String checkLoginInfoValidUrl = "http://wz.kenfor.com/front/memberCheckLoginFromAppAction.do";
    private DataApplication dataApplication = null;
    private final String memberGetLoginInfoUrl = "http://wz.kenfor.com/front/memberGetLoginInfoFromAppAction.do";
    private final String memberLoginUrl = "http://wz.kenfor.com/front/memberLoginFromAppAction.do";
    private final String memberLogoutUrl = "http://wz.kenfor.com/front/memberLogoutFromAppAction.do";
    private final String memberRegisterUrl = "http://wz.kenfor.com/front/memberRegisterFromAppAction.do";

    public MemberTool(Context context) {
        this.dataApplication = (DataApplication) context.getApplicationContext();
    }

    public String checkLoginInfoValid(String memberAccount, String memberPassword, String domain) {
        HashMap<String, String> map = new HashMap<>();
        map.put("memberAccount", memberAccount);
        map.put("memberPassword", memberPassword);
        map.put(Constant.DOMAIN, domain);
        String tempUrl = "http://wz.kenfor.com/front/memberCheckLoginFromAppAction.do" + RemoteLoginTool.getLoginParam(map, Constants.LICENSEKEY);
        System.out.println(tempUrl);
        DefaultHttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(tempUrl);
        httpPost.setHeader("Cookie", "JSESSIONID=" + this.dataApplication.getJsessionid());
        HttpContext context = new BasicHttpContext();
        context.setAttribute("http.cookie-store", new BasicCookieStore());
        try {
            return EntityUtils.toString(client.execute(httpPost, context).getEntity(), "utf-8");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String login(String memberAccount, String memberPassword, String domain) {
        HashMap<String, String> map = new HashMap<>();
        map.put("memberAccount", memberAccount);
        map.put("memberPassword", memberPassword);
        map.put(Constant.DOMAIN, domain);
        DefaultHttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost("http://wz.kenfor.com/front/memberLoginFromAppAction.do" + RemoteLoginTool.getLoginParam(map, Constants.LICENSEKEY));
        httpPost.setHeader("Cookie", "JSESSIONID=" + this.dataApplication.getJsessionid());
        HttpContext context = new BasicHttpContext();
        context.setAttribute("http.cookie-store", new BasicCookieStore());
        try {
            return EntityUtils.toString(client.execute(httpPost, context).getEntity(), "utf-8");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getLoginInfo(String memberAccount, String memberPassword, String domain) {
        HashMap<String, String> map = new HashMap<>();
        map.put("memberAccount", memberAccount);
        map.put("memberPassword", memberPassword);
        map.put(Constant.DOMAIN, domain);
        DefaultHttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost("http://wz.kenfor.com/front/memberGetLoginInfoFromAppAction.do" + RemoteLoginTool.getLoginParam(map, Constants.LICENSEKEY));
        httpPost.setHeader("Cookie", "JSESSIONID=" + this.dataApplication.getJsessionid());
        HttpContext context = new BasicHttpContext();
        context.setAttribute("http.cookie-store", new BasicCookieStore());
        try {
            return EntityUtils.toString(client.execute(httpPost, context).getEntity(), "utf-8");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void logout() {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost("http://wz.kenfor.com/front/memberLogoutFromAppAction.do");
        httpPost.setHeader("Cookie", "JSESSIONID=" + this.dataApplication.getJsessionid());
        HttpContext context = new BasicHttpContext();
        context.setAttribute("http.cookie-store", new BasicCookieStore());
        try {
            client.execute(httpPost, context);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String register(String memberAccount, String memberPassword, String domain, String langid, String note, String mobile) {
        HashMap<String, String> map = new HashMap<>();
        map.put("memberAccount", memberAccount);
        map.put("memberPassword", memberPassword);
        map.put(Constant.DOMAIN, domain);
        map.put("langid", langid);
        map.put("note", note);
        map.put("mobile", mobile);
        DefaultHttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost("http://wz.kenfor.com/front/memberRegisterFromAppAction.do" + RemoteLoginTool.getLoginParam(map, Constants.LICENSEKEY));
        httpPost.setHeader("Cookie", "JSESSIONID=" + this.dataApplication.getJsessionid());
        HttpContext context = new BasicHttpContext();
        context.setAttribute("http.cookie-store", new BasicCookieStore());
        try {
            return EntityUtils.toString(client.execute(httpPost, context).getEntity(), "utf-8");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
