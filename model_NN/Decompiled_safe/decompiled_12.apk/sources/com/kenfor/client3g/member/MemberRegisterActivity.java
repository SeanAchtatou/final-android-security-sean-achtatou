package com.kenfor.client3g.member;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.kenfor.client3g.notification.ServiceTool;
import com.kenfor.client3g.util.Constants;
import com.kenfor.client3g.util.DataApplication;
import com.kenfor.client3g.webservices.WebServicesUtils;
import com.kenfor.client3g.wwwqtcmcccom.R;
import java.util.regex.Pattern;

public class MemberRegisterActivity extends MemberActivity {
    /* access modifiers changed from: private */
    public DataApplication dataApplication;
    /* access modifiers changed from: private */
    public String inputAccount = null;
    /* access modifiers changed from: private */
    public String inputMobile = null;
    /* access modifiers changed from: private */
    public String inputNote = null;
    /* access modifiers changed from: private */
    public String inputPassword = null;
    /* access modifiers changed from: private */
    public String inputPasswordConfirm = null;
    /* access modifiers changed from: private */
    public MemberTool memberTool = null;
    /* access modifiers changed from: private */
    public EditText memberregisterAccount = null;
    private ImageView memberregisterBack = null;
    private View.OnClickListener memberregisterBackListener = new View.OnClickListener() {
        public void onClick(View v) {
            MemberRegisterActivity.this.finish();
        }
    };
    /* access modifiers changed from: private */
    public EditText memberregisterMobile = null;
    /* access modifiers changed from: private */
    public EditText memberregisterNote = null;
    /* access modifiers changed from: private */
    public EditText memberregisterPassword = null;
    /* access modifiers changed from: private */
    public EditText memberregisterPasswordConfirm = null;
    private Button memberregisterRegister = null;
    private View.OnClickListener memberregisterRegisterListener = new View.OnClickListener() {
        public void onClick(View v) {
            MemberRegisterActivity.this.inputAccount = MemberRegisterActivity.this.memberregisterAccount.getText().toString();
            MemberRegisterActivity.this.inputPassword = MemberRegisterActivity.this.memberregisterPassword.getText().toString();
            MemberRegisterActivity.this.inputPasswordConfirm = MemberRegisterActivity.this.memberregisterPasswordConfirm.getText().toString();
            MemberRegisterActivity.this.inputNote = MemberRegisterActivity.this.memberregisterNote.getText().toString();
            MemberRegisterActivity.this.inputMobile = MemberRegisterActivity.this.memberregisterMobile.getText().toString();
            if ("".equals(MemberRegisterActivity.this.inputAccount)) {
                Toast.makeText(MemberRegisterActivity.this, "请填写账号", 0).show();
            } else if (!Pattern.compile("[a-zA-Z0-9]{4,16}").matcher(MemberRegisterActivity.this.inputAccount).find()) {
                Toast.makeText(MemberRegisterActivity.this, "账号必须由4-16位字母或数字组成", 0).show();
            } else if ("".equals(MemberRegisterActivity.this.inputPassword)) {
                Toast.makeText(MemberRegisterActivity.this, "请填写密码", 0).show();
            } else if (!Pattern.compile("[a-zA-Z0-9]{4,16}").matcher(MemberRegisterActivity.this.inputPassword).find()) {
                Toast.makeText(MemberRegisterActivity.this, "密码必须由4-16位字母或数字组成", 0).show();
            } else if (!MemberRegisterActivity.this.inputPasswordConfirm.equals(MemberRegisterActivity.this.inputPassword)) {
                Toast.makeText(MemberRegisterActivity.this, "两次输入的密码不一致", 0).show();
            } else {
                final MemberActivity mActivity = new MemberActivity(MemberRegisterActivity.this);
                mActivity.showProgressDialog("注册中...");
                new Thread() {
                    public void run() {
                        Looper.prepare();
                        String result = MemberRegisterActivity.this.memberTool.register(MemberRegisterActivity.this.inputAccount, MemberRegisterActivity.this.inputPassword, MemberRegisterActivity.this.dataApplication.getDomain(), MemberRegisterActivity.this.dataApplication.getLangid(), MemberRegisterActivity.this.inputNote, MemberRegisterActivity.this.inputMobile);
                        mActivity.closeProgressDialog();
                        if (result == null || "".equals(result)) {
                            Toast.makeText(MemberRegisterActivity.this, "服务器连接失败，请稍候再试", 0).show();
                        } else if ("0".equals(WebServicesUtils.getXmlStringValue(result, "status"))) {
                            Toast.makeText(MemberRegisterActivity.this, WebServicesUtils.getXmlStringValue(result, "message"), 0).show();
                        } else if ("1".equals(WebServicesUtils.getXmlStringValue(result, "status"))) {
                            Toast.makeText(MemberRegisterActivity.this, WebServicesUtils.getXmlStringValue(result, "message"), 0).show();
                            MemberRegisterActivity.this.finish();
                        } else if ("2".equals(WebServicesUtils.getXmlStringValue(result, "status"))) {
                            Toast.makeText(MemberRegisterActivity.this, "注册成功", 0).show();
                            MemberRegisterActivity.this.dataApplication.getEditor().putString(Constants.MEMBER_ACCOUNT, MemberRegisterActivity.this.inputAccount);
                            MemberRegisterActivity.this.dataApplication.getEditor().putString(Constants.MEMBER_PASSWORD, MemberRegisterActivity.this.inputPassword);
                            MemberRegisterActivity.this.dataApplication.getEditor().commit();
                            MemberRegisterActivity.this.finish();
                            MemberRegisterActivity.this.startActivity(new Intent(MemberRegisterActivity.this, MemberMainActivity.class));
                            new ServiceTool(MemberRegisterActivity.this).restart();
                        }
                        Looper.loop();
                    }
                }.start();
            }
        }
    };
    private LinearLayout memberregisterTop = null;

    public MemberRegisterActivity() {
    }

    public MemberRegisterActivity(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.memberregister);
        this.dataApplication = (DataApplication) getApplicationContext();
        this.memberTool = new MemberTool(this);
        this.memberregisterTop = (LinearLayout) findViewById(R.id.memberregister_top);
        this.memberregisterBack = (ImageView) findViewById(R.id.memberregister_back);
        this.memberregisterAccount = (EditText) findViewById(R.id.memberregister_account);
        this.memberregisterPassword = (EditText) findViewById(R.id.memberregister_password);
        this.memberregisterPasswordConfirm = (EditText) findViewById(R.id.memberregister_password_confirm);
        this.memberregisterNote = (EditText) findViewById(R.id.memberregister_note);
        this.memberregisterMobile = (EditText) findViewById(R.id.memberregister_mobile);
        this.memberregisterRegister = (Button) findViewById(R.id.memberregister_register);
        this.memberregisterBack.setOnClickListener(this.memberregisterBackListener);
        this.memberregisterRegister.setOnClickListener(this.memberregisterRegisterListener);
        String themeAppColor = this.dataApplication.getPrefs().getString(Constants.THEME_APP_COLOR, "");
        if (!"".equals(themeAppColor)) {
            this.memberregisterTop.setBackgroundColor(Color.parseColor(themeAppColor));
        }
    }
}
