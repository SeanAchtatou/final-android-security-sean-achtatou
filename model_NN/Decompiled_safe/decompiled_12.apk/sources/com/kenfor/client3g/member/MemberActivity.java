package com.kenfor.client3g.member;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;

public class MemberActivity extends Activity {
    private Context context;
    public ProgressDialog progressDialog = null;

    public MemberActivity() {
    }

    public MemberActivity(Context context2) {
        this.context = context2;
    }

    public void closeProgressDialog() {
        if (this.progressDialog != null && this.progressDialog.isShowing()) {
            this.progressDialog.dismiss();
        }
    }

    public void showProgressDialog(String message) {
        this.progressDialog = new ProgressDialog(this.context);
        this.progressDialog.setTitle("请稍候");
        this.progressDialog.setMessage(message);
        this.progressDialog.setIndeterminate(true);
        this.progressDialog.setCancelable(true);
        this.progressDialog.show();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.progressDialog != null && this.progressDialog.isShowing()) {
            this.progressDialog.dismiss();
        }
        super.onDestroy();
    }
}
