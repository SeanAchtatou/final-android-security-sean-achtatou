package com.kenfor.client3g.member;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.kenfor.client3g.notification.ServiceTool;
import com.kenfor.client3g.util.Activities;
import com.kenfor.client3g.util.Constants;
import com.kenfor.client3g.util.DataApplication;
import com.kenfor.client3g.webservices.WebServicesUtils;
import com.kenfor.client3g.wwwqtcmcccom.R;
import java.util.regex.Pattern;

public class MemberLoginActivity extends MemberActivity {
    /* access modifiers changed from: private */
    public DataApplication dataApplication = null;
    /* access modifiers changed from: private */
    public String inputAccount = null;
    /* access modifiers changed from: private */
    public String inputPassword = null;
    /* access modifiers changed from: private */
    public MemberTool memberTool = null;
    /* access modifiers changed from: private */
    public EditText memberloginAccount = null;
    private ImageView memberloginBack = null;
    private View.OnClickListener memberloginBackListener = new View.OnClickListener() {
        public void onClick(View v) {
            MemberLoginActivity.this.finish();
        }
    };
    private Button memberloginLogin = null;
    private View.OnClickListener memberloginLoginListener = new View.OnClickListener() {
        public void onClick(View v) {
            MemberLoginActivity.this.inputAccount = MemberLoginActivity.this.memberloginAccount.getText().toString();
            MemberLoginActivity.this.inputPassword = MemberLoginActivity.this.memberloginPassword.getText().toString();
            if ("".equals(MemberLoginActivity.this.inputAccount)) {
                Toast.makeText(MemberLoginActivity.this, "请填写账号", 0).show();
            } else if (!Pattern.compile("[a-zA-Z0-9]{4,16}").matcher(MemberLoginActivity.this.inputAccount).find()) {
                Toast.makeText(MemberLoginActivity.this, "账号必须由4-16位字母或数字组成", 0).show();
            } else if ("".equals(MemberLoginActivity.this.inputPassword)) {
                Toast.makeText(MemberLoginActivity.this, "请填写密码", 0).show();
            } else if (!Pattern.compile("[a-zA-Z0-9]{4,16}").matcher(MemberLoginActivity.this.inputPassword).find()) {
                Toast.makeText(MemberLoginActivity.this, "密码必须由4-16位字母或数字组成", 0).show();
            } else {
                final MemberActivity mActivity = new MemberActivity(MemberLoginActivity.this);
                mActivity.showProgressDialog("登录中...");
                new Thread() {
                    public void run() {
                        Looper.prepare();
                        String result = MemberLoginActivity.this.memberTool.login(MemberLoginActivity.this.inputAccount, MemberLoginActivity.this.inputPassword, MemberLoginActivity.this.dataApplication.getDomain());
                        mActivity.closeProgressDialog();
                        System.out.println("------1------");
                        if (result == null || "".equals(result)) {
                            System.out.println("------2------");
                            Toast.makeText(MemberLoginActivity.this, "服务器连接失败，请稍候再试", 0).show();
                        } else if (!"1".equals(WebServicesUtils.getXmlStringValue(result, "status"))) {
                            System.out.println("------4------" + WebServicesUtils.getXmlStringValue(result, "message"));
                            Toast.makeText(MemberLoginActivity.this, WebServicesUtils.getXmlStringValue(result, "message"), 0).show();
                        } else {
                            System.out.println("------5------");
                            if (MemberLoginActivity.this.memberloginRememberAccount.isChecked()) {
                                MemberLoginActivity.this.dataApplication.getEditor().putString(Constants.MEMBERLOGIN_REMEMBER_ACCOUNT, MemberLoginActivity.this.inputAccount);
                            } else {
                                MemberLoginActivity.this.dataApplication.getEditor().putString(Constants.MEMBERLOGIN_REMEMBER_ACCOUNT, "");
                            }
                            System.out.println("------6------");
                            MemberLoginActivity.this.dataApplication.getEditor().putString(Constants.MEMBER_ACCOUNT, MemberLoginActivity.this.inputAccount);
                            MemberLoginActivity.this.dataApplication.getEditor().putString(Constants.MEMBER_PASSWORD, MemberLoginActivity.this.inputPassword);
                            MemberLoginActivity.this.dataApplication.getEditor().commit();
                            System.out.println("------7------");
                            new ServiceTool(MemberLoginActivity.this).restart();
                            System.out.println("------8------");
                            Toast.makeText(MemberLoginActivity.this, "登录成功", 0).show();
                            MemberLoginActivity.this.finish();
                            MemberLoginActivity.this.startActivity(new Intent(MemberLoginActivity.this, MemberMainActivity.class));
                        }
                        Looper.loop();
                    }
                }.start();
            }
        }
    };
    /* access modifiers changed from: private */
    public EditText memberloginPassword = null;
    private Button memberloginRegister = null;
    private View.OnClickListener memberloginRegisterListener = new View.OnClickListener() {
        public void onClick(View v) {
            MemberLoginActivity.this.startActivity(new Intent(MemberLoginActivity.this, MemberRegisterActivity.class));
        }
    };
    /* access modifiers changed from: private */
    public CheckBox memberloginRememberAccount = null;
    private LinearLayout memberloginTop = null;

    public MemberLoginActivity() {
    }

    public MemberLoginActivity(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Activities.getInstance().addActivity(this);
        setContentView((int) R.layout.memberlogin);
        this.dataApplication = (DataApplication) getApplicationContext();
        this.memberTool = new MemberTool(this);
        this.memberloginTop = (LinearLayout) findViewById(R.id.memberlogin_top);
        this.memberloginBack = (ImageView) findViewById(R.id.memberlogin_back);
        this.memberloginAccount = (EditText) findViewById(R.id.memberlogin_account);
        this.memberloginPassword = (EditText) findViewById(R.id.memberlogin_password);
        this.memberloginRememberAccount = (CheckBox) findViewById(R.id.memberlogin_remember_account);
        this.memberloginLogin = (Button) findViewById(R.id.memberlogin_login);
        this.memberloginRegister = (Button) findViewById(R.id.memberlogin_register);
        String rememberAccount = this.dataApplication.getPrefs().getString(Constants.MEMBERLOGIN_REMEMBER_ACCOUNT, "");
        if (!"".equals(rememberAccount)) {
            this.memberloginAccount.setText(rememberAccount);
            this.memberloginRememberAccount.setChecked(true);
        }
        String themeAppColor = this.dataApplication.getPrefs().getString(Constants.THEME_APP_COLOR, "");
        if (!"".equals(themeAppColor)) {
            this.memberloginTop.setBackgroundColor(Color.parseColor(themeAppColor));
        }
        this.memberloginBack.setOnClickListener(this.memberloginBackListener);
        this.memberloginLogin.setOnClickListener(this.memberloginLoginListener);
        this.memberloginRegister.setOnClickListener(this.memberloginRegisterListener);
    }
}
