package com.kenfor.client3g.setting;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import com.kenfor.client3g.util.Constant;
import com.kenfor.client3g.util.DataApplication;
import com.kenfor.client3g.webservices.WebServicesUtils;
import com.kenfor.client3g.wwwqtcmcccom.R;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.regex.Pattern;
import org.xbill.DNS.KEYRecord;

public class UpdateApp {
    private static final int DOWNLOAD = 1;
    private static final int DOWNLOAD_FINISH = 2;
    /* access modifiers changed from: private */
    public boolean cancelUpdate = false;
    /* access modifiers changed from: private */
    public DataApplication dataApplication;
    private Context mContext;
    /* access modifiers changed from: private */
    public Dialog mDownloadDialog;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    UpdateApp.this.mProgress.setProgress(UpdateApp.this.progress);
                    return;
                case 2:
                    UpdateApp.this.installApk();
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public ProgressBar mProgress;
    /* access modifiers changed from: private */
    public String mSavePath;
    /* access modifiers changed from: private */
    public int progress;

    public UpdateApp(Context context) {
        this.mContext = context;
        this.dataApplication = (DataApplication) this.mContext.getApplicationContext();
    }

    public boolean checkUpdate() {
        try {
            PackageInfo packInfo = this.mContext.getPackageManager().getPackageInfo(this.mContext.getPackageName(), 0);
            this.dataApplication.setLocalVersion3gCode(packInfo.versionCode);
            this.dataApplication.setLocalVersion3gName(packInfo.versionName);
        } catch (Exception e) {
            this.dataApplication.setLocalVersion3gCode(0);
            this.dataApplication.setLocalVersion3gName("未知");
        }
        LinkedHashMap<String, Object> param = new LinkedHashMap<>();
        param.put(Constant.DOMAIN, this.dataApplication.getDomain());
        param.put("langid", this.dataApplication.getLangid());
        String result = WebServicesUtils.executeMethod(WebServicesUtils.GET_APP_VERSION, param);
        if (!(result == null || result.trim().length() == 0)) {
            String version3gCode = getXmlStringValue(result, "version3gCode");
            String version3gName = getXmlStringValue(result, "version3gName");
            String version3gUrl = getXmlStringValue(result, "version3gUrl");
            String version3gDate = getXmlStringValue(result, "version3gDate");
            String version3gNote = getXmlStringValue(result, "version3gNote");
            if (checkisNumeric(version3gCode)) {
                this.dataApplication.setRemoteVersion3gCode(Integer.parseInt(version3gCode));
            }
            this.dataApplication.setRemoteVersion3gName(version3gName);
            this.dataApplication.setDownloadUrl(version3gUrl);
            this.dataApplication.setRemoteVersion3gDate(version3gDate);
            this.dataApplication.setRemoteVersion3gNote(version3gNote);
        }
        if (this.dataApplication.getLocalVersion3gCode() == 0 || this.dataApplication.getRemoteVersion3gCode() == 0 || this.dataApplication.getLocalVersion3gCode() >= this.dataApplication.getRemoteVersion3gCode()) {
            return false;
        }
        return true;
    }

    public void showNoticeDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.mContext);
        builder.setTitle((int) R.string.update_title);
        if (this.dataApplication.getRemoteVersion3gNote().trim().length() == 0) {
            builder.setMessage((int) R.string.update_info);
        } else {
            builder.setMessage(this.dataApplication.getRemoteVersion3gNote().replaceAll("\\n", "\n"));
        }
        builder.setPositiveButton((int) R.string.update_now, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                UpdateApp.this.showDownloadDialog();
            }
        });
        builder.setNegativeButton((int) R.string.update_later, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    /* access modifiers changed from: private */
    public void showDownloadDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.mContext);
        builder.setTitle((int) R.string.update_title);
        View v = LayoutInflater.from(this.mContext).inflate((int) R.layout.update_progress, (ViewGroup) null);
        this.mProgress = (ProgressBar) v.findViewById(R.id.update_progress);
        builder.setView(v);
        builder.setNegativeButton((int) R.string.update_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                UpdateApp.this.cancelUpdate = true;
            }
        });
        this.mDownloadDialog = builder.create();
        this.mDownloadDialog.show();
        downloadApk();
    }

    private void downloadApk() {
        new downloadApkThread(this, null).start();
    }

    private class downloadApkThread extends Thread {
        private downloadApkThread() {
        }

        /* synthetic */ downloadApkThread(UpdateApp updateApp, downloadApkThread downloadapkthread) {
            this();
        }

        public void run() {
            try {
                if (Environment.getExternalStorageState().equals("mounted")) {
                    UpdateApp.this.mSavePath = String.valueOf(Environment.getExternalStorageDirectory() + "/") + "download";
                    HttpURLConnection conn = (HttpURLConnection) new URL(UpdateApp.this.dataApplication.getDownloadUrl()).openConnection();
                    conn.connect();
                    int length = conn.getContentLength();
                    InputStream is = conn.getInputStream();
                    File file = new File(UpdateApp.this.mSavePath);
                    if (!file.exists()) {
                        file.mkdir();
                    }
                    FileOutputStream fos = new FileOutputStream(new File(UpdateApp.this.mSavePath, String.valueOf(UpdateApp.this.dataApplication.getApkName()) + "-" + UpdateApp.this.dataApplication.getRemoteVersion3gName() + ".apk"));
                    int count = 0;
                    byte[] buf = new byte[KEYRecord.Flags.FLAG5];
                    while (true) {
                        int numread = is.read(buf);
                        count += numread;
                        UpdateApp.this.progress = (int) ((((float) count) / ((float) length)) * 100.0f);
                        UpdateApp.this.mHandler.sendEmptyMessage(1);
                        if (numread > 0) {
                            fos.write(buf, 0, numread);
                            if (UpdateApp.this.cancelUpdate) {
                                break;
                            }
                        } else {
                            UpdateApp.this.mHandler.sendEmptyMessage(2);
                            break;
                        }
                    }
                    fos.close();
                    is.close();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            UpdateApp.this.mDownloadDialog.dismiss();
        }
    }

    /* access modifiers changed from: private */
    public void installApk() {
        File apkfile = new File(this.mSavePath, String.valueOf(this.dataApplication.getApkName()) + "-" + this.dataApplication.getRemoteVersion3gName() + ".apk");
        if (apkfile.exists()) {
            Intent i = new Intent("android.intent.action.VIEW");
            i.setDataAndType(Uri.parse("file://" + apkfile.toString()), "application/vnd.android.package-archive");
            this.mContext.startActivity(i);
        }
    }

    public boolean checkisNumeric(String str) {
        if (str == null) {
            return false;
        }
        return Pattern.compile("[0-9]+").matcher(str.trim()).matches();
    }

    public String getXmlStringValue(String result, String key) {
        return result.substring(result.indexOf("<" + key + ">") + key.length() + 2, result.indexOf("</" + key + ">"));
    }
}
