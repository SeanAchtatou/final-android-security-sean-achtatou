package com.kenfor.client3g.setting;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.kenfor.client3g.BaseOuterActivity;
import com.kenfor.client3g.notification.ServiceTool;
import com.kenfor.client3g.util.Activities;
import com.kenfor.client3g.util.Constants;
import com.kenfor.client3g.util.DataApplication;
import com.kenfor.client3g.wwwqtcmcccom.R;

public class SettingsActivity extends BaseOuterActivity implements DialogInterface.OnClickListener {
    private CheckBox checkBox = null;
    /* access modifiers changed from: private */
    public DataApplication dataApplication = null;
    /* access modifiers changed from: private */
    public AlertDialog dialog = null;
    private CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton btn, boolean value) {
            if (!value) {
                SettingsActivity.this.dialog = new AlertDialog.Builder(SettingsActivity.this).create();
                SettingsActivity.this.dialog.setMessage("确定要关闭吗？");
                SettingsActivity.this.dialog.setButton("确定", SettingsActivity.this);
                SettingsActivity.this.dialog.setButton2("返回", SettingsActivity.this);
                SettingsActivity.this.dialog.show();
            } else if (SettingsActivity.this.dataApplication.getPrefs().getInt(Constants.RECEIVE_PUSH_INFO, 1) == 0) {
                SettingsActivity.this.start();
            }
        }
    };
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            UpdateApp updateApp = new UpdateApp(SettingsActivity.this);
            if (updateApp.checkUpdate()) {
                updateApp.showNoticeDialog();
            } else {
                Toast.makeText(SettingsActivity.this, "已经是最新的版本", 0).show();
            }
        }
    };
    private RelativeLayout relativeLayout = null;
    private ImageView settingBack = null;
    private View.OnClickListener settingBackListener = new View.OnClickListener() {
        public void onClick(View v) {
            SettingsActivity.this.finish();
        }
    };
    private LinearLayout settingTop = null;
    private TextView versionTextView = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Activities.getInstance().addActivity(this);
        this.dataApplication = (DataApplication) getApplicationContext();
        setContentView((int) R.layout.settings);
        this.settingTop = (LinearLayout) findViewById(R.id.settintg_top);
        this.settingBack = (ImageView) findViewById(R.id.setting_back);
        this.checkBox = (CheckBox) findViewById(R.id.settings_pushinfo_checkBox);
        this.relativeLayout = (RelativeLayout) findViewById(R.id.setting_update);
        this.versionTextView = (TextView) findViewById(R.id.setting_update_textView_2);
        this.versionTextView.setText("(当前版本:" + this.dataApplication.getLocalVersion3gName() + ")");
        if (this.dataApplication.getPrefs().getInt(Constants.RECEIVE_PUSH_INFO, 1) == 1) {
            this.checkBox.setChecked(true);
        } else {
            this.checkBox.setChecked(false);
        }
        this.checkBox.setOnCheckedChangeListener(this.onCheckedChangeListener);
        this.relativeLayout.setOnClickListener(this.onClickListener);
        this.settingBack.setOnClickListener(this.settingBackListener);
        String themeAppColor = this.dataApplication.getPrefs().getString(Constants.THEME_APP_COLOR, "");
        if (!"".equals(themeAppColor)) {
            this.settingTop.setBackgroundColor(Color.parseColor(themeAppColor));
        }
    }

    public void start() {
        this.dataApplication.getEditor().putInt(Constants.RECEIVE_PUSH_INFO, 1);
        this.dataApplication.getEditor().commit();
        new ServiceTool(this).start();
        Toast.makeText(this, "成功开启接收推送信息", 0).show();
    }

    public void close() {
        this.dataApplication.getEditor().putInt(Constants.RECEIVE_PUSH_INFO, 0);
        this.dataApplication.getEditor().commit();
        new ServiceTool(this).stop();
    }

    public void onClick(DialogInterface dialog2, int which) {
        switch (which) {
            case -2:
                this.checkBox.setChecked(true);
                return;
            case -1:
                close();
                return;
            default:
                return;
        }
    }
}
