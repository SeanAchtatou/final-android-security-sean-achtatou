package com.kenfor.client3g;

import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import com.kenfor.client3g.util.Activities;

public class BaseOuterActivity extends Activity {
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 1, 1, "退出").setIcon(17301560);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == 1) {
            Activities.getInstance().exit();
        }
        return super.onOptionsItemSelected(item);
    }
}
