package com.kenfor;

public class User {
    private String alias_name;
    private String bas_no;
    private Integer com_parent_id;
    private String com_parent_name;
    private Integer com_type_id;
    private String com_type_name;
    protected long dept_id = 0;
    protected String email = null;
    protected long level = 0;
    protected String localePrefer = null;
    private long member_id;
    private int member_type;
    private String member_type_name;
    private String password;
    protected long role_id = 0;
    private float ser_money;
    protected String sex = null;
    protected String str_dept_id = null;
    protected String str_user_id = null;
    protected String truename = null;
    protected long user_id = 0;
    private int user_level;
    private int user_locked;
    private String user_template = "/bbs/templates/default/";
    protected String username = null;
    private int vip_flag;

    public void setStr_user_id(String str_user_id2) {
        this.str_user_id = str_user_id2;
        try {
            this.user_id = Long.valueOf(str_user_id2).longValue();
            this.member_id = this.user_id;
        } catch (Exception e) {
            this.user_id = 0;
        }
    }

    public String getStr_user_id() {
        return this.str_user_id;
    }

    public void setStr_dept_id(String str_dept_id2) {
        this.str_dept_id = str_dept_id2;
        try {
            this.dept_id = Long.valueOf(str_dept_id2).longValue();
        } catch (Exception e) {
            this.dept_id = 0;
        }
    }

    public String getStr_dept_id() {
        return this.str_dept_id;
    }

    public void setUsername(String username2) {
        this.username = username2;
    }

    public String getUsername() {
        return this.username;
    }

    public void setTruename(String truename2) {
        this.truename = truename2;
    }

    public String getTruename() {
        return this.truename;
    }

    public void setSex(String sex2) {
        this.sex = sex2;
    }

    public String getSex() {
        return this.sex;
    }

    public void setEmail(String email2) {
        this.email = email2;
    }

    public String getEmail() {
        return this.email;
    }

    public void setLocalePrefer(String localePrefer2) {
        this.localePrefer = localePrefer2;
    }

    public String getLocalePrefer() {
        return this.localePrefer;
    }

    public void setLevel(int level2) {
        this.level = (long) level2;
    }

    public long getLevel() {
        return this.level;
    }

    public void setUser_id(long user_id2) {
        this.user_id = user_id2;
        this.str_user_id = String.valueOf(user_id2);
        this.member_id = user_id2;
    }

    public long getUser_id() {
        return this.user_id;
    }

    public void setUserId(long user_id2) {
        this.user_id = user_id2;
        this.str_user_id = String.valueOf(user_id2);
        this.member_id = user_id2;
    }

    public long getUserId() {
        return this.user_id;
    }

    public void setDept_id(long dept_id2) {
        this.dept_id = dept_id2;
        this.str_dept_id = String.valueOf(dept_id2);
    }

    public long getDept_id() {
        return this.dept_id;
    }

    public void setDeptId(long dept_id2) {
        this.dept_id = dept_id2;
    }

    public long getDeptId() {
        return this.dept_id;
    }

    public void setRole_id(long role_id2) {
        this.role_id = role_id2;
    }

    public long getRole_id() {
        return this.role_id;
    }

    public void setRoleId(long role_id2) {
        this.role_id = role_id2;
    }

    public long getRoleId() {
        return this.role_id;
    }

    public long getMember_id() {
        return this.member_id;
    }

    public void setMember_id(long member_id2) {
        this.member_id = member_id2;
        this.user_id = this.user_id;
        this.str_user_id = String.valueOf(this.user_id);
    }

    public String getAlias_name() {
        return this.alias_name;
    }

    public void setAlias_name(String alias_name2) {
        this.alias_name = alias_name2;
    }

    public String getBas_no() {
        return this.bas_no;
    }

    public void setBas_no(String bas_no2) {
        this.bas_no = bas_no2;
    }

    public String getMember_type_name() {
        return this.member_type_name;
    }

    public void setMember_type_name(String member_type_name2) {
        this.member_type_name = member_type_name2;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password2) {
        this.password = password2;
    }

    public String getCom_parent_name() {
        return this.com_parent_name;
    }

    public void setCom_parent_name(String com_parent_name2) {
        this.com_parent_name = com_parent_name2;
    }

    public String getCom_type_name() {
        return this.com_type_name;
    }

    public void setCom_type_name(String com_type_name2) {
        this.com_type_name = com_type_name2;
    }

    public String getUser_template() {
        return this.user_template;
    }

    public void setUser_template(String user_template2) {
        this.user_template = user_template2;
    }

    public int getMember_type() {
        return this.member_type;
    }

    public void setMember_type(int member_type2) {
        this.member_type = member_type2;
    }

    public int getVip_flag() {
        return this.vip_flag;
    }

    public void setVip_flag(int vip_flag2) {
        this.vip_flag = vip_flag2;
    }

    public int getUser_level() {
        return this.user_level;
    }

    public void setUser_level(int user_level2) {
        this.user_level = user_level2;
    }

    public int getUser_locked() {
        return this.user_locked;
    }

    public void setUser_locked(int user_locked2) {
        this.user_locked = user_locked2;
    }

    public float getSer_money() {
        return this.ser_money;
    }

    public void setSer_money(float ser_money2) {
        this.ser_money = ser_money2;
    }

    public Integer getCom_parent_id() {
        return this.com_parent_id;
    }

    public void setCom_parent_id(Integer com_parent_id2) {
        this.com_parent_id = com_parent_id2;
    }

    public Integer getCom_type_id() {
        return this.com_type_id;
    }

    public void setCom_type_id(Integer com_type_id2) {
        this.com_type_id = com_type_id2;
    }
}
