package com.kenfor.util2;

import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class DES {
    private static String strDefaultKey = "national";
    private Cipher decryptCipher;
    private Cipher encryptCipher;

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v3, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v4, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String byteArr2HexStr(byte[] r6) throws java.lang.Exception {
        /*
            r5 = 16
            int r1 = r6.length
            java.lang.StringBuffer r3 = new java.lang.StringBuffer
            int r4 = r1 * 2
            r3.<init>(r4)
            r0 = 0
        L_0x000b:
            if (r0 < r1) goto L_0x0012
            java.lang.String r4 = r3.toString()
            return r4
        L_0x0012:
            byte r2 = r6[r0]
        L_0x0014:
            if (r2 < 0) goto L_0x0027
            if (r2 >= r5) goto L_0x001d
            java.lang.String r4 = "0"
            r3.append(r4)
        L_0x001d:
            java.lang.String r4 = java.lang.Integer.toString(r2, r5)
            r3.append(r4)
            int r0 = r0 + 1
            goto L_0x000b
        L_0x0027:
            int r2 = r2 + 256
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenfor.util2.DES.byteArr2HexStr(byte[]):java.lang.String");
    }

    public static byte[] hexStr2ByteArr(String strIn) throws Exception {
        byte[] arrB = strIn.getBytes();
        int iLen = arrB.length;
        byte[] arrOut = new byte[(iLen / 2)];
        for (int i = 0; i < iLen; i += 2) {
            arrOut[i / 2] = (byte) Integer.parseInt(new String(arrB, i, 2), 16);
        }
        return arrOut;
    }

    public DES() throws Exception {
        this(strDefaultKey);
    }

    public DES(String strKey) throws Exception {
        this.encryptCipher = null;
        this.decryptCipher = null;
        Key key = getKey(strKey.getBytes());
        this.encryptCipher = Cipher.getInstance("DES");
        this.encryptCipher.init(1, key);
        this.decryptCipher = Cipher.getInstance("DES");
        this.decryptCipher.init(2, key);
    }

    public byte[] encrypt(byte[] arrB) throws Exception {
        return this.encryptCipher.doFinal(arrB);
    }

    public String encrypt(String strIn) throws Exception {
        return byteArr2HexStr(encrypt(strIn.getBytes()));
    }

    public byte[] decrypt(byte[] arrB) throws Exception {
        return this.decryptCipher.doFinal(arrB);
    }

    public String decrypt(String strIn) throws Exception {
        return new String(decrypt(hexStr2ByteArr(strIn)));
    }

    private Key getKey(byte[] arrBTmp) throws Exception {
        byte[] arrB = new byte[8];
        int i = 0;
        while (i < arrBTmp.length && i < arrB.length) {
            arrB[i] = arrBTmp[i];
            i++;
        }
        return new SecretKeySpec(arrB, "DES");
    }

    public static void main(String[] args) throws Exception {
        DES des = new DES();
        String result = des.encrypt("admin@test.kenfor.com");
        String result2 = des.decrypt(result);
        System.out.println("result=====" + result);
        System.out.println("result2====" + result2);
    }
}
