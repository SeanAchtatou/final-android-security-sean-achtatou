package com.kenfor.util2;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5 {
    public static String getMD5(String text) throws NoSuchAlgorithmException {
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        md5.update(text.getBytes());
        return new String(md5.digest());
    }

    public static void main(String[] args) {
        try {
            System.out.println(getMD5("test"));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
}
