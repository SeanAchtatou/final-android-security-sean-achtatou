package com.kenfor.demoapp;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import com.kenfor.client.ServiceManager;
import com.kenfor.util2.DES;
import org.androidpn.demoapp.R;

public class DemoAppActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        Log.d("DemoAppActivity", "onCreate()...");
        try {
            DES des = new DES();
            String encode = des.encrypt("admin@test.kenfor.com");
            String decode = des.decrypt(encode);
            Log.d("DemoAppActivity", "encode==" + encode);
            Log.d("DemoAppActivity", "decode==" + decode);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        ((Button) findViewById(R.id.btn_settings)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ServiceManager.viewNotificationSettings(DemoAppActivity.this);
            }
        });
        ServiceManager serviceManager = new ServiceManager(this, "yjk_data");
        serviceManager.setNotificationIcon(R.drawable.notification);
        serviceManager.startService();
    }
}
