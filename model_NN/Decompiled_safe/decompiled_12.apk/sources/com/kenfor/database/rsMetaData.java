package com.kenfor.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

public class rsMetaData {
    ArrayList fList;
    String fieldName;
    String path;
    String tableName;

    public rsMetaData() {
        this.path = null;
        this.tableName = null;
        this.fieldName = null;
        this.fList = null;
        this.fieldName = "BAS_ID";
    }

    public rsMetaData(String path2, String tableName2) {
        this.path = null;
        this.tableName = null;
        this.fieldName = null;
        this.fList = null;
        this.path = path2;
        this.tableName = tableName2;
        this.fieldName = "BAS_ID";
    }

    public rsMetaData(String path2, String tableName2, String fieldName2) {
        this.path = null;
        this.tableName = null;
        this.fieldName = null;
        this.fList = null;
        this.path = path2;
        this.tableName = tableName2;
        this.fieldName = fieldName2;
    }

    public void setPath(String path2) {
        this.path = path2;
    }

    public String getPath() {
        return this.path;
    }

    public void setTableName(String tableName2) {
        this.tableName = tableName2;
    }

    public String getTableName() {
        return this.tableName;
    }

    public void setFieldName(String fieldName2) {
        this.fieldName = fieldName2;
    }

    public String getFieldName() {
        return this.fieldName;
    }

    public HashMap getRsMetaDataMap() throws Exception {
        if (this.path == null) {
            throw new Exception("path can not be null");
        } else if (this.tableName == null) {
            throw new Exception("table name can not be null");
        } else {
            DatabaseInfo dbInfo = new InitDatabase(this.path).getDatabaseInfo();
            HashMap rsmdMap = new HashMap();
            this.fList = new ArrayList();
            try {
                Class.forName(dbInfo.getDriver());
                Connection con = DriverManager.getConnection(dbInfo.getUrl(), dbInfo.getUsername(), dbInfo.getPassword());
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(new StringBuffer().append("select * from ").append(this.tableName).append(" where ").append(this.fieldName).append(" =0").toString());
                ResultSetMetaData rsmd = rs.getMetaData();
                int colCount = rsmd.getColumnCount();
                for (int i = 1; i <= colCount; i++) {
                    boolean bAuto = rsmd.isAutoIncrement(i);
                    String fName = rsmd.getColumnName(i).toLowerCase();
                    String fType = rsmd.getColumnTypeName(i);
                    this.fList.add(fName);
                    if (bAuto) {
                        rsmdMap.put(fName, "auto");
                    } else {
                        rsmdMap.put(fName, fType);
                    }
                }
                rs.close();
                stmt.close();
                con.close();
                return rsmdMap;
            } catch (SQLException e) {
                throw new Exception(e.getMessage());
            }
        }
    }

    public ArrayList getFieldList() {
        return this.fList;
    }
}
