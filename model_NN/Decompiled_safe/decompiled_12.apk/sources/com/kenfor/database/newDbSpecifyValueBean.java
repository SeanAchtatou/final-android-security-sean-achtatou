package com.kenfor.database;

import com.kenfor.util.CloseCon;
import com.kenfor.util.ProDebug;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class newDbSpecifyValueBean {
    private Connection con = null;
    private String isCount = "false";
    private String isSum = "false";
    private String sql = null;
    private String sqlWhere = null;
    private String tableName = null;
    private String valueField = null;

    public Connection getCon() {
        return this.con;
    }

    public void setTableName(String tableName2) {
        this.tableName = tableName2;
    }

    public String getTableName() {
        return this.tableName;
    }

    public void setValueField(String valueField2) {
        this.valueField = valueField2;
    }

    public String getValueField() {
        return this.valueField;
    }

    public void setSqlWhere(String sqlWhere2) {
        this.sqlWhere = sqlWhere2;
    }

    public String getSqlWhere() {
        return this.sqlWhere;
    }

    public void setIsCount(String isCount2) {
        this.isCount = isCount2;
    }

    public String getIsCount() {
        return this.isCount;
    }

    public void setIsSum(String isSum2) {
        this.isSum = isSum2;
    }

    public String getIsSum() {
        return this.isSum;
    }

    public void setSql(String sql2) {
        this.sql = sql2;
    }

    public String getSql() {
        return this.sql;
    }

    public String createSql() {
        String temp_sql = null;
        if (this.sql != null) {
            return this.sql;
        }
        if (this.tableName == null) {
            return null;
        }
        if (this.isCount != null && this.isCount.compareToIgnoreCase("true") == 0) {
            temp_sql = this.sqlWhere != null ? new StringBuffer().append("select count(*) as COUNT from ").append(this.tableName).append(" where ").append(this.sqlWhere).toString() : new StringBuffer().append("select count(*) as COUNT from ").append(this.tableName).toString();
        }
        if (this.valueField == null) {
            return temp_sql;
        }
        if (this.isSum == null || this.isSum.compareToIgnoreCase("true") != 0) {
            if (this.sqlWhere != null) {
                return new StringBuffer().append("select ").append(this.valueField).append(" from ").append(this.tableName).append(" where ").append(this.sqlWhere).toString();
            }
            return new StringBuffer().append("select ").append(this.valueField).append(" from ").append(this.tableName).toString();
        } else if (this.sqlWhere != null) {
            return new StringBuffer().append("select sum(").append(this.valueField).append(") as SUM from ").append(this.tableName).append(" where ").append(this.sqlWhere).toString();
        } else {
            return new StringBuffer().append("select sum(").append(this.valueField).append(") as SUM from ").append(this.tableName).toString();
        }
    }

    public ResultSet getdbValue(Connection con2) throws Exception {
        this.con = con2;
        String temp_sql = createSql();
        if (temp_sql == null) {
            throw new SQLException("not sql statement");
        }
        try {
            return con2.createStatement().executeQuery(temp_sql);
        } catch (SQLException e) {
            ProDebug.addDebugLog(new StringBuffer().append("database perform error : ").append(e.getMessage()).toString());
            ProDebug.addDebugLog(new StringBuffer().append("sql:").append(temp_sql).toString());
            ProDebug.saveToFile();
            CloseCon.Close(con2);
            throw new SQLException(new StringBuffer().append("database perform error : ").append(e.getMessage()).toString());
        }
    }

    public ResultSet getdbValue(String path) throws Exception {
        String temp_sql = createSql();
        if (temp_sql == null) {
            throw new SQLException("not sql statement");
        }
        DatabaseInfo dbInfo = new InitDatabase(path).getDatabaseInfo();
        try {
            Class.forName(dbInfo.getDriver());
            this.con = DriverManager.getConnection(dbInfo.getUrl(), dbInfo.getUsername(), dbInfo.getPassword());
            return this.con.createStatement().executeQuery(temp_sql);
        } catch (SQLException e) {
            ProDebug.addDebugLog(new StringBuffer().append("database perform error : ").append(e.getMessage()).toString());
            ProDebug.addDebugLog(new StringBuffer().append("sql:").append(temp_sql).toString());
            ProDebug.saveToFile();
            CloseCon.Close(this.con);
            throw new SQLException(new StringBuffer().append("database perform error : ").append(e.getMessage()).toString());
        }
    }

    public ResultSet getdbValue(DatabaseInfo dbInfo) throws Exception {
        String temp_sql = createSql();
        if (temp_sql == null) {
            throw new SQLException("not sql statement");
        }
        try {
            Class.forName(dbInfo.getDriver());
            this.con = DriverManager.getConnection(dbInfo.getUrl(), dbInfo.getUsername(), dbInfo.getPassword());
            return this.con.createStatement().executeQuery(temp_sql);
        } catch (SQLException e) {
            ProDebug.addDebugLog(new StringBuffer().append("database perform error : ").append(e.getMessage()).toString());
            ProDebug.addDebugLog(new StringBuffer().append("sql:").append(temp_sql).toString());
            ProDebug.saveToFile();
            CloseCon.Close(this.con);
            throw new SQLException(new StringBuffer().append("database perform error : ").append(e.getMessage()).toString());
        }
    }
}
