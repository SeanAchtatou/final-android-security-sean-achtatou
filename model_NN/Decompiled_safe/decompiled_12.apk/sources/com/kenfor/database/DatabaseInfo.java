package com.kenfor.database;

public class DatabaseInfo {
    private String closeFilePath = "/home/closeTime.log";
    private long closeTime = 600;
    private int dbType = 0;
    private String driver = null;
    private String isDebug;
    private int maxActive = 20;
    private int maxIdle = 0;
    private int maxWait = -1;
    private String password = null;
    private String systemName = "";
    private String url = null;
    private String username = null;
    private long waitTime = 2000;

    public void setCloseFilePath(String closeFilePath2) {
        this.closeFilePath = closeFilePath2;
    }

    public String getCloseFilePath() {
        return this.closeFilePath;
    }

    public void setWaitTime(long waitTime2) {
        this.waitTime = waitTime2;
    }

    public long getWaitTime() {
        return this.waitTime;
    }

    public void setSystemName(String systemName2) {
        this.systemName = systemName2;
    }

    public String getSystemName() {
        return this.systemName;
    }

    public void setCloseTime(long closeTime2) {
        this.closeTime = closeTime2;
    }

    public long getCloseTime() {
        return this.closeTime;
    }

    public void setMaxActive(int maxActive2) {
        if (maxActive2 <= 0) {
            this.maxActive = 5;
        }
        if (maxActive2 >= 500) {
            this.maxActive = 500;
        } else {
            this.maxActive = maxActive2;
        }
    }

    public int getMaxActive() {
        return this.maxActive;
    }

    public void setMaxIdle(int maxIdle2) {
        if (maxIdle2 < 0) {
            this.maxIdle = 0;
        } else {
            this.maxIdle = maxIdle2;
        }
    }

    public int getMaxIdle() {
        return this.maxIdle;
    }

    public void setMaxWait(int maxWait2) {
        this.maxWait = maxWait2;
    }

    public int getMaxWait() {
        return this.maxWait;
    }

    public void setDbType(int dbType2) {
        if (dbType2 < 0) {
            this.dbType = 0;
        }
        if (dbType2 > 2) {
            this.dbType = 2;
        } else {
            this.dbType = dbType2;
        }
    }

    public int getDbType() {
        return this.dbType;
    }

    public void setDriver(String driver2) {
        this.driver = driver2;
    }

    public String getDriver() {
        return this.driver;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUsername(String username2) {
        this.username = username2;
    }

    public String getUsername() {
        return this.username;
    }

    public void setPassword(String password2) {
        this.password = password2;
    }

    public String getPassword() {
        return this.password;
    }

    public String toString() {
        return new StringBuffer().append("driver:").append(this.driver).append("\nurl:").append(this.url).append("\nuser:").append(this.username).append("\npassword:").append(this.password).append("\n").toString();
    }

    public String getIsDebug() {
        return this.isDebug;
    }

    public void setIsDebug(String isDebug2) {
        this.isDebug = isDebug2;
    }
}
