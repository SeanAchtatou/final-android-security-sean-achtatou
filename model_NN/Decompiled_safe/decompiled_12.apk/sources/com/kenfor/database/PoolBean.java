package com.kenfor.database;

import com.kenfor.util.ProDebug;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.logicalcobwebs.proxool.configuration.JAXPConfigurator;

public class PoolBean {
    private ArrayList bdsList = null;
    private int bds_close_times = 0;
    int big_con_count = 0;
    long c_count = 0;
    long clear_time = 0;
    int close_times = 0;
    private ArrayList conList = null;
    private DatabaseInfo dbInfo = null;
    private DataSource g_bds = null;
    private String html_flag = "1";
    private int info_flag = 1;
    private Context initCtx = null;
    private String jndi_name = null;
    Log log = null;
    int manual_close_times = 0;
    int over_time_times = 0;
    private String path = null;
    private String poolName = "pool";
    private int poolType = 1;
    boolean run_test = false;
    boolean start_clear = false;
    private boolean started = false;
    private String systemName;
    private String trade_id = null;
    int un_c_count = 0;
    private String version;

    public void setTrade_id(String trade_id2) {
        this.trade_id = trade_id2;
    }

    public String getTrade_id() {
        return this.trade_id;
    }

    public void realsePool() {
    }

    public void setJndiName(String jndi_name2) {
        this.jndi_name = jndi_name2;
    }

    public void setRun_test(boolean run_test2) {
        this.run_test = run_test2;
    }

    public void setPoolType(int poolType2) {
        this.poolType = poolType2;
    }

    public boolean isStarted() {
        return this.started;
    }

    public int getPoolType() {
        return this.poolType;
    }

    public String getPoolName() {
        return this.poolName;
    }

    public Log getLog() {
        return this.log;
    }

    public int getDbType() {
        return this.dbInfo.getDbType();
    }

    public void setDatabaseInfo(DatabaseInfo db) {
        this.dbInfo = db;
    }

    public DatabaseInfo getDatabaseInfo() {
        return this.dbInfo;
    }

    public synchronized void setPath(String path2) throws Exception {
        this.path = path2;
        if (this.poolType == 1) {
            InitDatabase initDb = new InitDatabase(path2);
            this.dbInfo = initDb.getDatabaseInfo();
            initDb.releaseInit();
        } else {
            this.dbInfo = new DatabaseInfo();
        }
    }

    public void setPoolName(String poolName2) {
        this.poolName = poolName2;
    }

    public synchronized void initializePool() throws Exception {
        String real_path;
        this.log = LogFactory.getLog(getClass().getName());
        if (this.log.isInfoEnabled()) {
            this.log.info("start");
        }
        if (this.poolType == 0) {
            if (this.log.isDebugEnabled()) {
                this.log.debug("start initializePool at pool by proxool");
            }
            String sep = System.getProperty("file.separator");
            if (this.path.endsWith("/") || this.path.endsWith("\\")) {
                real_path = new StringBuffer().append(this.path).append("proxool.xml").toString();
            } else {
                real_path = new StringBuffer().append(this.path).append(sep).append("proxool.xml").toString();
            }
            JAXPConfigurator.configure(real_path, false);
        } else {
            if (this.log.isDebugEnabled()) {
                this.log.debug("start initializePool at pool by dbcp");
            }
            if (this.dbInfo == null) {
                throw new Exception("dbInfo is null");
            } else if (!this.started) {
                if (this.bdsList == null) {
                    this.bdsList = new ArrayList();
                }
                BasicDataSource t_bds = new BasicDataSource();
                t_bds.setDriverClassName(this.dbInfo.getDriver());
                t_bds.setUrl(this.dbInfo.getUrl());
                t_bds.setUsername(this.dbInfo.getUsername());
                t_bds.setPassword(this.dbInfo.getPassword());
                t_bds.setMaxActive(this.dbInfo.getMaxActive());
                t_bds.setMaxIdle(this.dbInfo.getMaxIdle());
                t_bds.setMaxWait(3000);
                t_bds.setRemoveAbandoned(true);
                t_bds.setRemoveAbandonedTimeout(90);
                t_bds.setLogAbandoned(true);
                t_bds.setMinIdle(3);
                this.bdsList.add(t_bds);
                if (this.jndi_name != null) {
                    this.initCtx = new InitialContext();
                    this.initCtx.rebind(this.jndi_name, t_bds);
                }
                if (this.log.isDebugEnabled()) {
                    this.log.debug(new StringBuffer().append("end initializePool at PoolBean ,system name:").append(this.dbInfo.getSystemName()).toString());
                }
            }
        }
        this.started = true;
    }

    public void setBDS(DataSource bds) {
        this.g_bds = bds;
    }

    public synchronized BasicDataSource getValidBds() {
        BasicDataSource basicDataSource;
        int[] active_nums = {0, 0, 0, 0, 0};
        int[] idle_nums = {0, 0, 0, 0, 0};
        int bds_num = this.bdsList.size();
        int i = 0;
        while (i < bds_num && i < 5) {
            BasicDataSource t_bds = (BasicDataSource) this.bdsList.get(i);
            active_nums[i] = t_bds.getNumActive();
            idle_nums[i] = t_bds.getNumIdle();
            i++;
        }
        String str_active = "";
        String str_idle = "";
        int i2 = 0;
        while (i2 < bds_num && i2 < 5) {
            str_active = new StringBuffer().append(str_active).append("acitve_num[").append(i2).append("]=").append(active_nums[i2]).append(";").toString();
            str_idle = new StringBuffer().append(str_idle).append("idle_num[").append(i2).append("]=").append(idle_nums[i2]).append(";").toString();
            i2++;
        }
        String format = new SimpleDateFormat("yyyy_MM_dd hh:mm:ss ").format(new Date());
        if (active_nums[bds_num - 1] > this.dbInfo.getMaxActive() - 1) {
            System.out.println("PoolBean:产生一个新的数据库连接");
            BasicDataSource t_bds2 = new BasicDataSource();
            t_bds2.setDriverClassName(this.dbInfo.getDriver());
            t_bds2.setUrl(this.dbInfo.getUrl());
            t_bds2.setUsername(this.dbInfo.getUsername());
            t_bds2.setPassword(this.dbInfo.getPassword());
            t_bds2.setMaxActive(this.dbInfo.getMaxActive());
            t_bds2.setRemoveAbandoned(true);
            t_bds2.setRemoveAbandonedTimeout(900);
            t_bds2.setLogAbandoned(true);
            t_bds2.setMaxIdle(this.dbInfo.getMaxIdle());
            t_bds2.setMaxWait(3000);
            t_bds2.setMinIdle(3);
            this.bdsList.add(t_bds2);
            if (this.jndi_name != null) {
                try {
                    this.initCtx.rebind(this.jndi_name, t_bds2);
                } catch (Exception e) {
                    System.out.println(new StringBuffer().append("bind bds to context occur exception:").append(e.getMessage()).toString());
                }
            }
        }
        int bds_num2 = this.bdsList.size();
        if (active_nums[0] > this.dbInfo.getMaxActive() - 1) {
            try {
                ((BasicDataSource) this.bdsList.get(0)).close();
            } catch (Exception e2) {
                System.out.println("t_bds close exception");
            }
            this.bdsList.remove(0);
        }
        int bds_num3 = this.bdsList.size();
        int i3 = 0;
        while (true) {
            if (i3 >= bds_num3) {
                basicDataSource = (BasicDataSource) this.bdsList.get(0);
                break;
            } else if (active_nums[i3] < this.dbInfo.getMaxActive() - 1) {
                basicDataSource = (BasicDataSource) this.bdsList.get(i3);
                break;
            } else {
                i3++;
            }
        }
        return basicDataSource;
    }

    public synchronized void initializePool(DatabaseInfo db) throws Exception {
        this.dbInfo = db;
        initializePool();
    }

    public int getConnectedCount() {
        return 0;
    }

    public int getTotalConnection() {
        return 0;
    }

    public synchronized void releaseConnection(Connection con) {
        try {
            if (!con.isClosed()) {
                con.close();
            }
        } catch (SQLException e) {
            ProDebug.addDebugLog(new StringBuffer().append("can not close connection at PoolBean :").append(e.getMessage()).toString());
        }
        return;
    }

    public synchronized Connection getNoInfoConnection() throws SQLException {
        this.info_flag = 0;
        return getConnection();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0053, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r14.log.error(new java.lang.StringBuffer().append("poolName:").append(r14.poolName).toString());
        r14.log.error(new java.lang.StringBuffer().append("get connection error,").append(r2.getMessage()).toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        r8 = r14.dbInfo.getSystemName();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        r1 = r9.getConnection();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00a9, code lost:
        r4 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00ab, code lost:
        if (r4 == false) goto L_0x00ad;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00b4, code lost:
        throw new java.sql.SQLException("连接不上数据库");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00b5, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00b6, code lost:
        java.lang.System.out.println(new java.lang.StringBuffer().append("--host:").append(r14.dbInfo.getSystemName()).append(", 连不上数据库").append(r3.getMessage()).toString());
        r4 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00e4, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0101, code lost:
        throw new java.sql.SQLException(new java.lang.StringBuffer().append("获取数据库连接时出错!").append(r3.getMessage()).toString());
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:5:0x0006, B:25:0x0099] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.sql.Connection getConnection() throws java.sql.SQLException {
        /*
            r14 = this;
            monitor-enter(r14)
            r1 = 0
            int r11 = r14.poolType     // Catch:{ all -> 0x008b }
            if (r11 != 0) goto L_0x008e
            org.apache.commons.logging.Log r11 = r14.log     // Catch:{ Exception -> 0x0053 }
            boolean r11 = r11.isDebugEnabled()     // Catch:{ Exception -> 0x0053 }
            if (r11 == 0) goto L_0x0015
            org.apache.commons.logging.Log r11 = r14.log     // Catch:{ Exception -> 0x0053 }
            java.lang.String r12 = "start getConnection"
            r11.debug(r12)     // Catch:{ Exception -> 0x0053 }
        L_0x0015:
            java.lang.StringBuffer r11 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x0053 }
            r11.<init>()     // Catch:{ Exception -> 0x0053 }
            java.lang.String r12 = "proxool."
            java.lang.StringBuffer r11 = r11.append(r12)     // Catch:{ Exception -> 0x0053 }
            java.lang.String r12 = r14.poolName     // Catch:{ Exception -> 0x0053 }
            java.lang.StringBuffer r11 = r11.append(r12)     // Catch:{ Exception -> 0x0053 }
            java.lang.String r11 = r11.toString()     // Catch:{ Exception -> 0x0053 }
            java.sql.Connection r1 = java.sql.DriverManager.getConnection(r11)     // Catch:{ Exception -> 0x0053 }
            org.apache.commons.logging.Log r11 = r14.log     // Catch:{ Exception -> 0x0053 }
            boolean r11 = r11.isDebugEnabled()     // Catch:{ Exception -> 0x0053 }
            if (r11 == 0) goto L_0x004e
            org.apache.commons.logging.Log r11 = r14.log     // Catch:{ Exception -> 0x0053 }
            java.lang.StringBuffer r12 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x0053 }
            r12.<init>()     // Catch:{ Exception -> 0x0053 }
            java.lang.String r13 = "get con ok:"
            java.lang.StringBuffer r12 = r12.append(r13)     // Catch:{ Exception -> 0x0053 }
            java.lang.StringBuffer r12 = r12.append(r1)     // Catch:{ Exception -> 0x0053 }
            java.lang.String r12 = r12.toString()     // Catch:{ Exception -> 0x0053 }
            r11.debug(r12)     // Catch:{ Exception -> 0x0053 }
        L_0x004e:
            r11 = 1
            r14.info_flag = r11     // Catch:{ all -> 0x008b }
            monitor-exit(r14)
            return r1
        L_0x0053:
            r2 = move-exception
            org.apache.commons.logging.Log r11 = r14.log     // Catch:{ all -> 0x008b }
            java.lang.StringBuffer r12 = new java.lang.StringBuffer     // Catch:{ all -> 0x008b }
            r12.<init>()     // Catch:{ all -> 0x008b }
            java.lang.String r13 = "poolName:"
            java.lang.StringBuffer r12 = r12.append(r13)     // Catch:{ all -> 0x008b }
            java.lang.String r13 = r14.poolName     // Catch:{ all -> 0x008b }
            java.lang.StringBuffer r12 = r12.append(r13)     // Catch:{ all -> 0x008b }
            java.lang.String r12 = r12.toString()     // Catch:{ all -> 0x008b }
            r11.error(r12)     // Catch:{ all -> 0x008b }
            org.apache.commons.logging.Log r11 = r14.log     // Catch:{ all -> 0x008b }
            java.lang.StringBuffer r12 = new java.lang.StringBuffer     // Catch:{ all -> 0x008b }
            r12.<init>()     // Catch:{ all -> 0x008b }
            java.lang.String r13 = "get connection error,"
            java.lang.StringBuffer r12 = r12.append(r13)     // Catch:{ all -> 0x008b }
            java.lang.String r13 = r2.getMessage()     // Catch:{ all -> 0x008b }
            java.lang.StringBuffer r12 = r12.append(r13)     // Catch:{ all -> 0x008b }
            java.lang.String r12 = r12.toString()     // Catch:{ all -> 0x008b }
            r11.error(r12)     // Catch:{ all -> 0x008b }
            goto L_0x004e
        L_0x008b:
            r11 = move-exception
            monitor-exit(r14)
            throw r11
        L_0x008e:
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x008b }
            r0 = 0
            r5 = 0
            r10 = 0
            org.apache.commons.dbcp.BasicDataSource r9 = r14.getValidBds()     // Catch:{ all -> 0x008b }
            java.sql.Connection r1 = r9.getConnection()     // Catch:{ SQLException -> 0x009e, Exception -> 0x00e4 }
            goto L_0x004e
        L_0x009e:
            r2 = move-exception
            com.kenfor.database.DatabaseInfo r11 = r14.dbInfo     // Catch:{ all -> 0x008b }
            java.lang.String r8 = r11.getSystemName()     // Catch:{ all -> 0x008b }
            r4 = 0
            java.sql.Connection r1 = r9.getConnection()     // Catch:{ Exception -> 0x00b5 }
            r4 = 1
        L_0x00ab:
            if (r4 != 0) goto L_0x004e
            java.sql.SQLException r11 = new java.sql.SQLException     // Catch:{ all -> 0x008b }
            java.lang.String r12 = "连接不上数据库"
            r11.<init>(r12)     // Catch:{ all -> 0x008b }
            throw r11     // Catch:{ all -> 0x008b }
        L_0x00b5:
            r3 = move-exception
            java.io.PrintStream r11 = java.lang.System.out     // Catch:{ all -> 0x008b }
            java.lang.StringBuffer r12 = new java.lang.StringBuffer     // Catch:{ all -> 0x008b }
            r12.<init>()     // Catch:{ all -> 0x008b }
            java.lang.String r13 = "--host:"
            java.lang.StringBuffer r12 = r12.append(r13)     // Catch:{ all -> 0x008b }
            com.kenfor.database.DatabaseInfo r13 = r14.dbInfo     // Catch:{ all -> 0x008b }
            java.lang.String r13 = r13.getSystemName()     // Catch:{ all -> 0x008b }
            java.lang.StringBuffer r12 = r12.append(r13)     // Catch:{ all -> 0x008b }
            java.lang.String r13 = ", 连不上数据库"
            java.lang.StringBuffer r12 = r12.append(r13)     // Catch:{ all -> 0x008b }
            java.lang.String r13 = r3.getMessage()     // Catch:{ all -> 0x008b }
            java.lang.StringBuffer r12 = r12.append(r13)     // Catch:{ all -> 0x008b }
            java.lang.String r12 = r12.toString()     // Catch:{ all -> 0x008b }
            r11.println(r12)     // Catch:{ all -> 0x008b }
            r4 = 0
            goto L_0x00ab
        L_0x00e4:
            r3 = move-exception
            java.sql.SQLException r11 = new java.sql.SQLException     // Catch:{ all -> 0x008b }
            java.lang.StringBuffer r12 = new java.lang.StringBuffer     // Catch:{ all -> 0x008b }
            r12.<init>()     // Catch:{ all -> 0x008b }
            java.lang.String r13 = "获取数据库连接时出错!"
            java.lang.StringBuffer r12 = r12.append(r13)     // Catch:{ all -> 0x008b }
            java.lang.String r13 = r3.getMessage()     // Catch:{ all -> 0x008b }
            java.lang.StringBuffer r12 = r12.append(r13)     // Catch:{ all -> 0x008b }
            java.lang.String r12 = r12.toString()     // Catch:{ all -> 0x008b }
            r11.<init>(r12)     // Catch:{ all -> 0x008b }
            throw r11     // Catch:{ all -> 0x008b }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenfor.database.PoolBean.getConnection():java.sql.Connection");
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String version2) {
        this.version = version2;
    }

    public String getHtml_flag() {
        return this.html_flag;
    }

    public void setHtml_flag(String html_flag2) {
        this.html_flag = html_flag2;
    }

    public String getSystemName() {
        return this.systemName;
    }

    public void setSystemName(String systemName2) {
        this.systemName = systemName2;
    }
}
