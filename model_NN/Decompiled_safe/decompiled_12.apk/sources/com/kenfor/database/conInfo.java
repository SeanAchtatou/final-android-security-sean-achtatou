package com.kenfor.database;

import java.sql.Connection;

public class conInfo {
    private Connection con;
    private long startTime;

    public void setStartTime(long startTime2) {
        this.startTime = startTime2;
    }

    public long getStartTime() {
        return this.startTime;
    }

    public void setCon(Connection con2) {
        this.con = con2;
    }

    public Connection getCon() {
        return this.con;
    }
}
