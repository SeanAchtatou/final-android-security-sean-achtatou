package com.kenfor.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class PoolStatement {
    private int RowCount = 0;
    private Connection con = null;
    private Statement stmt = null;

    public void setConnection(Connection con2) throws SQLException {
        this.con = con2;
        this.stmt = con2.createStatement(1005, 1008);
    }

    public Connection getConnection() {
        return this.con;
    }

    public void close() throws SQLException {
        this.stmt.close();
    }

    public int executeUpdate(String sql) throws SQLException {
        if (this.con != null) {
            return this.stmt.executeUpdate(sql);
        }
        throw new SQLException("did not set connection ");
    }

    public ResultSet executeQuery(String sql) throws SQLException {
        if (this.con == null) {
            throw new SQLException("did not set connection ");
        }
        ResultSet rs = this.stmt.executeQuery(sql);
        this.RowCount = getRowCount(rs);
        return rs;
    }

    public int getRowCount() {
        return this.RowCount;
    }

    public int getRowCount(ResultSet rs) throws SQLException {
        rs.last();
        int result = rs.getRow();
        rs.beforeFirst();
        return result;
    }
}
