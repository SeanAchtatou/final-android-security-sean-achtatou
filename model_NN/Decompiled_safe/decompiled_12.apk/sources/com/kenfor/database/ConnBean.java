package com.kenfor.database;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;

public class ConnBean implements Serializable {
    private Connection conn = null;
    private boolean inuse = false;

    public ConnBean() {
    }

    public ConnBean(Connection con) {
        if (con != null) {
            this.conn = con;
        }
    }

    public Connection getConnection() {
        return this.conn;
    }

    public void setConnection(Connection con) {
        this.conn = con;
    }

    public void setInuse(boolean inuse2) {
        this.inuse = inuse2;
    }

    public boolean getInuse() {
        return this.inuse;
    }

    public void close() {
        try {
            this.conn.close();
        } catch (SQLException sqle) {
            System.err.println(new StringBuffer().append("SQLException at ConnBean:").append(sqle.getMessage()).toString());
        }
    }
}
