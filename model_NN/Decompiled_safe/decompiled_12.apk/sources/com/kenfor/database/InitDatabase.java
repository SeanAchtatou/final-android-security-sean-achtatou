package com.kenfor.database;

import com.kenfor.client3g.util.Constant;
import com.kenfor.util.ProDebug;
import java.io.InputStream;
import java.util.Hashtable;
import javax.servlet.http.HttpServlet;

public class InitDatabase {
    private DatabaseInfo dbInfo;
    private String errorMessage;
    private String filename;
    private Hashtable hashTable;
    private ProDebug myDebug;
    private String path;
    private String realPath;
    private HttpServlet servlet;
    private String sys_separator;

    public void releaseInit() {
        this.path = null;
        this.filename = null;
        this.realPath = null;
        this.errorMessage = null;
        this.sys_separator = null;
        this.dbInfo = null;
        this.servlet = null;
        this.hashTable = null;
        this.myDebug = null;
    }

    public InitDatabase() {
        this.path = null;
        this.filename = "database-config.xml";
        this.realPath = null;
        this.errorMessage = null;
        this.sys_separator = "/";
        this.dbInfo = null;
        this.hashTable = null;
        this.myDebug = new ProDebug();
        this.sys_separator = System.getProperty("file.separator");
        this.path = System.getProperty("user.dir");
    }

    public InitDatabase(String path2) {
        this.path = null;
        this.filename = "database-config.xml";
        this.realPath = null;
        this.errorMessage = null;
        this.sys_separator = "/";
        this.dbInfo = null;
        this.hashTable = null;
        this.myDebug = new ProDebug();
        this.sys_separator = System.getProperty("file.separator");
        this.path = path2;
    }

    public InitDatabase(String path2, String filename2) {
        this.path = null;
        this.filename = "database-config.xml";
        this.realPath = null;
        this.errorMessage = null;
        this.sys_separator = "/";
        this.dbInfo = null;
        this.hashTable = null;
        this.myDebug = new ProDebug();
        this.sys_separator = System.getProperty("file.separator");
        this.path = path2;
        this.filename = filename2;
    }

    public InitDatabase(InputStream in) {
        this.path = null;
        this.filename = "database-config.xml";
        this.realPath = null;
        this.errorMessage = null;
        this.sys_separator = "/";
        this.dbInfo = null;
        this.hashTable = null;
        this.myDebug = new ProDebug();
        this.sys_separator = System.getProperty("file.separator");
        this.path = System.getProperty("user.dir");
    }

    public InitDatabase(HttpServlet servlet2) {
        this.path = null;
        this.filename = "database-config.xml";
        this.realPath = null;
        this.errorMessage = null;
        this.sys_separator = "/";
        this.dbInfo = null;
        this.hashTable = null;
        this.myDebug = new ProDebug();
        this.sys_separator = System.getProperty("file.separator");
        this.path = System.getProperty("user.dir");
        this.servlet = servlet2;
    }

    public void setPath(String path2) {
        this.path = path2;
    }

    public String getPath() {
        return this.path;
    }

    public void setFileName(String filename2) {
        this.filename = filename2;
    }

    public String getFileName() {
        return this.filename;
    }

    public String getRealPath() {
        if (this.path != null) {
            return new StringBuffer().append(this.path).append(this.sys_separator).append(this.filename).toString();
        }
        return this.filename;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:14:0x01a6=Splitter:B:14:0x01a6, B:10:0x019e=Splitter:B:10:0x019e} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void readXml() throws java.lang.Exception {
        /*
            r25 = this;
            java.lang.StringBuffer r23 = new java.lang.StringBuffer
            r23.<init>()
            r0 = r25
            java.lang.String r0 = r0.path
            r24 = r0
            java.lang.StringBuffer r23 = r23.append(r24)
            r0 = r25
            java.lang.String r0 = r0.sys_separator
            r24 = r0
            java.lang.StringBuffer r23 = r23.append(r24)
            r0 = r25
            java.lang.String r0 = r0.filename
            r24 = r0
            java.lang.StringBuffer r23 = r23.append(r24)
            java.lang.String r23 = r23.toString()
            r0 = r23
            r1 = r25
            r1.realPath = r0
            r8 = 0
            r11 = 0
            java.io.FileInputStream r9 = new java.io.FileInputStream     // Catch:{ JDOMException -> 0x019d, NullPointerException -> 0x01a5 }
            r0 = r25
            java.lang.String r0 = r0.realPath     // Catch:{ JDOMException -> 0x019d, NullPointerException -> 0x01a5 }
            r23 = r0
            r0 = r23
            r9.<init>(r0)     // Catch:{ JDOMException -> 0x019d, NullPointerException -> 0x01a5 }
            org.jdom.input.SAXBuilder r4 = new org.jdom.input.SAXBuilder     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            r4.<init>()     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            org.jdom.Document r3 = r4.build(r9)     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            org.jdom.Element r12 = r3.getRootElement()     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            java.lang.String r23 = "driver"
            r0 = r23
            java.lang.String r14 = r12.getChildTextTrim(r0)     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            java.lang.String r23 = "url"
            r0 = r23
            java.lang.String r19 = r12.getChildTextTrim(r0)     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            java.lang.String r23 = "username"
            r0 = r23
            java.lang.String r20 = r12.getChildTextTrim(r0)     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            java.lang.String r23 = "password"
            r0 = r23
            java.lang.String r18 = r12.getChildTextTrim(r0)     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            java.lang.String r23 = "dbType"
            r0 = r23
            java.lang.String r13 = r12.getChildTextTrim(r0)     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            java.lang.String r23 = "maxActive"
            r0 = r23
            java.lang.String r15 = r12.getChildTextTrim(r0)     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            java.lang.String r23 = "maxIdle"
            r0 = r23
            java.lang.String r16 = r12.getChildTextTrim(r0)     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            java.lang.String r23 = "maxWait"
            r0 = r23
            java.lang.String r17 = r12.getChildTextTrim(r0)     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            java.lang.String r23 = "isDebug"
            r0 = r23
            java.lang.String r10 = r12.getChildTextTrim(r0)     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            java.lang.String r23 = "waitTime"
            r0 = r23
            java.lang.String r22 = r12.getChildTextTrim(r0)     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            java.lang.String r23 = "systemName"
            r0 = r23
            java.lang.String r21 = r12.getChildTextTrim(r0)     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            java.lang.String r23 = "closeTime"
            r0 = r23
            java.lang.String r6 = r12.getChildTextTrim(r0)     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            java.lang.String r23 = "closeFilePath"
            r0 = r23
            java.lang.String r5 = r12.getChildTextTrim(r0)     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            r0 = r25
            java.util.Hashtable r0 = r0.hashTable     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            r23 = r0
            if (r23 != 0) goto L_0x00c4
            java.util.Hashtable r23 = new java.util.Hashtable     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            r23.<init>()     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            r0 = r23
            r1 = r25
            r1.hashTable = r0     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
        L_0x00c4:
            r0 = r25
            java.util.Hashtable r0 = r0.hashTable     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            r23 = r0
            java.lang.String r24 = "driver"
            r0 = r23
            r1 = r24
            r0.put(r1, r14)     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            r0 = r25
            java.util.Hashtable r0 = r0.hashTable     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            r23 = r0
            java.lang.String r24 = "url"
            r0 = r23
            r1 = r24
            r2 = r19
            r0.put(r1, r2)     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            r0 = r25
            java.util.Hashtable r0 = r0.hashTable     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            r23 = r0
            java.lang.String r24 = "username"
            r0 = r23
            r1 = r24
            r2 = r20
            r0.put(r1, r2)     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            r0 = r25
            java.util.Hashtable r0 = r0.hashTable     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            r23 = r0
            java.lang.String r24 = "password"
            r0 = r23
            r1 = r24
            r2 = r18
            r0.put(r1, r2)     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            r0 = r25
            java.util.Hashtable r0 = r0.hashTable     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            r23 = r0
            java.lang.String r24 = "dbType"
            r0 = r23
            r1 = r24
            r0.put(r1, r13)     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            r0 = r25
            java.util.Hashtable r0 = r0.hashTable     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            r23 = r0
            java.lang.String r24 = "maxActive"
            r0 = r23
            r1 = r24
            r0.put(r1, r15)     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            r0 = r25
            java.util.Hashtable r0 = r0.hashTable     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            r23 = r0
            java.lang.String r24 = "maxIdle"
            r0 = r23
            r1 = r24
            r2 = r16
            r0.put(r1, r2)     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            r0 = r25
            java.util.Hashtable r0 = r0.hashTable     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            r23 = r0
            java.lang.String r24 = "maxWait"
            r0 = r23
            r1 = r24
            r2 = r17
            r0.put(r1, r2)     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            r0 = r25
            java.util.Hashtable r0 = r0.hashTable     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            r23 = r0
            java.lang.String r24 = "isDebug"
            r0 = r23
            r1 = r24
            r0.put(r1, r10)     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            r0 = r25
            java.util.Hashtable r0 = r0.hashTable     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            r23 = r0
            java.lang.String r24 = "waitTime"
            r0 = r23
            r1 = r24
            r2 = r22
            r0.put(r1, r2)     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            r0 = r25
            java.util.Hashtable r0 = r0.hashTable     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            r23 = r0
            java.lang.String r24 = "systemName"
            r0 = r23
            r1 = r24
            r2 = r21
            r0.put(r1, r2)     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            r0 = r25
            java.util.Hashtable r0 = r0.hashTable     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            r23 = r0
            java.lang.String r24 = "closeTime"
            r0 = r23
            r1 = r24
            r0.put(r1, r6)     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            r0 = r25
            java.util.Hashtable r0 = r0.hashTable     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            r23 = r0
            java.lang.String r24 = "closeFilePath"
            r0 = r23
            r1 = r24
            r0.put(r1, r5)     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            r9.close()     // Catch:{ JDOMException -> 0x01b5, NullPointerException -> 0x01b2, all -> 0x01af }
            r9.close()
            r8 = r9
        L_0x019c:
            return
        L_0x019d:
            r7 = move-exception
        L_0x019e:
            r7.printStackTrace()     // Catch:{ all -> 0x01aa }
        L_0x01a1:
            r8.close()
            goto L_0x019c
        L_0x01a5:
            r7 = move-exception
        L_0x01a6:
            r7.printStackTrace()     // Catch:{ all -> 0x01aa }
            goto L_0x01a1
        L_0x01aa:
            r23 = move-exception
        L_0x01ab:
            r8.close()
            throw r23
        L_0x01af:
            r23 = move-exception
            r8 = r9
            goto L_0x01ab
        L_0x01b2:
            r7 = move-exception
            r8 = r9
            goto L_0x01a6
        L_0x01b5:
            r7 = move-exception
            r8 = r9
            goto L_0x019e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kenfor.database.InitDatabase.readXml():void");
    }

    public DatabaseInfo getDatabaseInfo() throws Exception {
        if (this.hashTable == null) {
            try {
                readXml();
            } catch (Exception e) {
                throw new Exception(e.getMessage());
            }
        }
        if (this.hashTable != null && this.dbInfo == null) {
            this.dbInfo = new DatabaseInfo();
            this.dbInfo.setDriver((String) this.hashTable.get(new String("driver")));
            this.dbInfo.setUrl((String) this.hashTable.get(new String("url")));
            this.dbInfo.setUsername((String) this.hashTable.get(new String("username")));
            this.dbInfo.setPassword((String) this.hashTable.get(new String(Constant.PASSWORD)));
            Object Obj_db = this.hashTable.get(new String("dbType"));
            Object obj = this.hashTable.get(new String("scopeType"));
            Object Obj_ma = this.hashTable.get(new String("maxActive"));
            Object Obj_mi = this.hashTable.get(new String("maxIdle"));
            Object Obj_mw = this.hashTable.get(new String("maxWait"));
            Object Obj_wTime = this.hashTable.get("waitTime");
            Object Obj_cT = this.hashTable.get("closeTime");
            String systemName = (String) this.hashTable.get("systemName");
            String is_debug = (String) this.hashTable.get(new String("isDebug"));
            String closeFilePath = (String) this.hashTable.get("closeFilePath");
            int db_type = 0;
            int max_active = 20;
            int max_idle = 0;
            int max_wait = -1;
            long waitTime = 3000;
            long closeTime = 600;
            if (Obj_db != null) {
                try {
                    db_type = Integer.valueOf((String) Obj_db).intValue();
                } catch (NumberFormatException e2) {
                }
            }
            if (Obj_ma != null) {
                try {
                    max_active = Integer.valueOf((String) Obj_ma).intValue();
                } catch (NumberFormatException e3) {
                }
            }
            if (Obj_mi != null) {
                try {
                    max_idle = Integer.valueOf((String) Obj_mi).intValue();
                } catch (NumberFormatException e4) {
                }
            }
            if (Obj_ma != null) {
                try {
                    max_wait = Integer.valueOf((String) Obj_mw).intValue();
                } catch (NumberFormatException e5) {
                }
            }
            if (Obj_wTime != null) {
                try {
                    waitTime = Long.valueOf((String) Obj_wTime).longValue();
                } catch (NumberFormatException e6) {
                }
            }
            if (Obj_cT != null) {
                try {
                    closeTime = Long.valueOf((String) Obj_cT).longValue();
                } catch (NumberFormatException e7) {
                }
            }
            this.dbInfo.setDbType(db_type);
            this.dbInfo.setMaxActive(max_active);
            this.dbInfo.setMaxIdle(max_idle);
            this.dbInfo.setMaxWait(max_wait);
            this.dbInfo.setIsDebug(is_debug);
            this.dbInfo.setWaitTime(waitTime);
            this.dbInfo.setCloseTime(closeTime);
            this.dbInfo.setSystemName(systemName);
            this.dbInfo.setCloseFilePath(closeFilePath);
        }
        return this.dbInfo;
    }

    public Hashtable getHashtable() throws Exception {
        if (this.hashTable == null) {
            try {
                readXml();
            } catch (Exception e) {
                ProDebug proDebug = this.myDebug;
                ProDebug.addDebugLog(new StringBuffer().append("readXml error : ").append(e.getMessage()).toString());
                ProDebug proDebug2 = this.myDebug;
                ProDebug.saveToFile();
                throw new Exception(e.getMessage());
            }
        }
        return this.hashTable;
    }
}
