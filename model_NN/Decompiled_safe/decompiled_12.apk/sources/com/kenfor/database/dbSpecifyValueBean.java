package com.kenfor.database;

import com.kenfor.taglib.db.dbPresentTag;
import com.kenfor.util.CloseCon;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

public class dbSpecifyValueBean {
    private String isCount = "false";
    private String isSum = "false";
    private String sql = null;
    private String sqlWhere = null;
    private String tableName = null;
    private String valueField = null;

    public void setTableName(String tableName2) {
        this.tableName = tableName2;
    }

    public String getTableName() {
        return this.tableName;
    }

    public void setValueField(String valueField2) {
        this.valueField = valueField2;
    }

    public String getValueField() {
        return this.valueField;
    }

    public void setSqlWhere(String sqlWhere2) {
        this.sqlWhere = sqlWhere2;
    }

    public String getSqlWhere() {
        return this.sqlWhere;
    }

    public void setIsCount(String isCount2) {
        this.isCount = isCount2;
    }

    public String getIsCount() {
        return this.isCount;
    }

    public void setIsSum(String isSum2) {
        this.isSum = isSum2;
    }

    public String getIsSum() {
        return this.isSum;
    }

    public void setSql(String sql2) {
        this.sql = sql2;
    }

    public String getSql() {
        return this.sql;
    }

    public String createSql() {
        String temp_sql = null;
        if (this.sql != null) {
            return this.sql;
        }
        if (this.tableName == null) {
            return null;
        }
        if (this.isCount != null && this.isCount.compareToIgnoreCase("true") == 0) {
            temp_sql = this.sqlWhere != null ? new StringBuffer().append("select count(*) as COUNT from ").append(this.tableName).append(" where ").append(this.sqlWhere).toString() : new StringBuffer().append("select count(*) as COUNT from ").append(this.tableName).toString();
        }
        if (this.valueField == null) {
            return temp_sql;
        }
        if (this.isSum == null || this.isSum.compareToIgnoreCase("true") != 0) {
            if (this.sqlWhere != null) {
                return new StringBuffer().append("select ").append(this.valueField).append(" from ").append(this.tableName).append(" where ").append(this.sqlWhere).toString();
            }
            return new StringBuffer().append("select ").append(this.valueField).append(" from ").append(this.tableName).toString();
        } else if (this.sqlWhere != null) {
            return new StringBuffer().append("select sum(").append(this.valueField).append(") as SUM from ").append(this.tableName).append(" where ").append(this.sqlWhere).toString();
        } else {
            return new StringBuffer().append("select sum(").append(this.valueField).append(") as SUM from ").append(this.tableName).toString();
        }
    }

    public HashMap getdbValue(PoolBean pool) throws Exception {
        String t_str;
        String tt_str;
        if (pool.getLog().isDebugEnabled()) {
            pool.getLog().debug("star getdbValue");
        }
        String system_name = pool.getDatabaseInfo().getSystemName();
        HashMap result = null;
        String temp_sql = createSql();
        if (temp_sql == null) {
            throw new SQLException("not sql statement");
        }
        Connection con = null;
        try {
            con = pool.getNoInfoConnection();
            con.setReadOnly(true);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(temp_sql);
            if (rs != null) {
                ResultSetMetaData rsmd = rs.getMetaData();
                int colCount = rsmd.getColumnCount();
                if (rs.next()) {
                    HashMap result2 = new HashMap();
                    int i = 1;
                    while (i <= colCount) {
                        try {
                            String name = rsmd.getColumnName(i).toLowerCase();
                            if ("text".equals(rsmd.getColumnTypeName(i).toLowerCase())) {
                                String tt_str2 = rs.getString(i);
                                if (tt_str2 == null) {
                                    tt_str = "";
                                } else {
                                    tt_str = tt_str2.trim();
                                }
                                result2.put(name.trim(), tt_str);
                            } else {
                                Object obj_value = rs.getObject(i);
                                if (obj_value instanceof String) {
                                    if (obj_value == null) {
                                        t_str = "";
                                    } else {
                                        t_str = String.valueOf(obj_value).trim();
                                    }
                                    result2.put(name.trim(), t_str);
                                } else {
                                    result2.put(name.trim(), obj_value);
                                }
                            }
                            i++;
                        } catch (SQLException e) {
                            e = e;
                            try {
                                CloseCon.Close(con);
                                pool.getLog().error(new StringBuffer().append(system_name).append(dbPresentTag.ROLE_DELIMITER).append(e.getMessage()).append(",sql:").append(temp_sql).toString());
                                throw new SQLException(new StringBuffer().append("database perform error : ").append(e.getMessage()).toString());
                            } catch (Throwable th) {
                                th = th;
                                CloseCon.Close(con);
                                pool.realsePool();
                                throw th;
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            CloseCon.Close(con);
                            pool.realsePool();
                            throw th;
                        }
                    }
                    result = result2;
                }
            }
            rs.close();
            stmt.close();
            con.setReadOnly(false);
            con.close();
            CloseCon.Close(null);
            pool.realsePool();
            return result;
        } catch (SQLException e2) {
            e = e2;
            CloseCon.Close(con);
            pool.getLog().error(new StringBuffer().append(system_name).append(dbPresentTag.ROLE_DELIMITER).append(e.getMessage()).append(",sql:").append(temp_sql).toString());
            throw new SQLException(new StringBuffer().append("database perform error : ").append(e.getMessage()).toString());
        }
    }

    private HashMap getdbValue(Connection con) throws Exception {
        String t_str;
        String tt_str;
        HashMap result = null;
        String temp_sql = createSql();
        if (temp_sql == null) {
            throw new SQLException("not sql statement");
        }
        try {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(temp_sql);
            if (rs != null) {
                ResultSetMetaData rsmd = rs.getMetaData();
                int colCount = rsmd.getColumnCount();
                if (rs.next()) {
                    HashMap result2 = new HashMap();
                    int i = 1;
                    while (i <= colCount) {
                        try {
                            String name = rsmd.getColumnName(i).toLowerCase();
                            if ("text".equals(rsmd.getColumnTypeName(i).toLowerCase())) {
                                String tt_str2 = rs.getString(i);
                                if (tt_str2 == null) {
                                    tt_str = "";
                                } else {
                                    tt_str = tt_str2.trim();
                                }
                                result2.put(name.trim(), tt_str);
                            } else {
                                Object obj_value = rs.getObject(i);
                                if (obj_value instanceof String) {
                                    if (obj_value == null) {
                                        t_str = "";
                                    } else {
                                        t_str = String.valueOf(obj_value).trim();
                                    }
                                    result2.put(name.trim(), t_str);
                                } else {
                                    result2.put(name.trim(), obj_value);
                                }
                            }
                            i++;
                        } catch (SQLException e) {
                            e = e;
                            try {
                                throw new SQLException(new StringBuffer().append("database perform error : ").append(e.getMessage()).toString());
                            } catch (Throwable th) {
                                th = th;
                                CloseCon.Close(con);
                                throw th;
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            CloseCon.Close(con);
                            throw th;
                        }
                    }
                    result = result2;
                }
            }
            rs.close();
            stmt.close();
            CloseCon.Close(con);
            return result;
        } catch (SQLException e2) {
            e = e2;
            throw new SQLException(new StringBuffer().append("database perform error : ").append(e.getMessage()).toString());
        }
    }

    private HashMap getdbValue(String path) throws Exception {
        String t_str;
        String tt_str;
        System.out.println("此函数须要进行 IO操作，请尽量不用");
        HashMap result = null;
        String temp_sql = createSql();
        if (temp_sql == null) {
            throw new SQLException("not sql statement");
        }
        Connection con = null;
        DatabaseInfo dbInfo = new InitDatabase(path).getDatabaseInfo();
        try {
            Class.forName(dbInfo.getDriver());
            con = DriverManager.getConnection(dbInfo.getUrl(), dbInfo.getUsername(), dbInfo.getPassword());
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(temp_sql);
            if (rs != null) {
                ResultSetMetaData rsmd = rs.getMetaData();
                int colCount = rsmd.getColumnCount();
                if (rs.next()) {
                    HashMap result2 = new HashMap();
                    int i = 1;
                    while (i <= colCount) {
                        try {
                            String name = rsmd.getColumnName(i).toLowerCase();
                            if ("text".equals(rsmd.getColumnTypeName(i).toLowerCase())) {
                                String tt_str2 = rs.getString(i);
                                if (tt_str2 == null) {
                                    tt_str = "";
                                } else {
                                    tt_str = tt_str2.trim();
                                }
                                result2.put(name.trim(), tt_str);
                            } else {
                                Object obj_value = rs.getObject(i);
                                if (obj_value instanceof String) {
                                    if (obj_value == null) {
                                        t_str = "";
                                    } else {
                                        t_str = String.valueOf(obj_value).trim();
                                    }
                                    result2.put(name.trim(), t_str);
                                } else {
                                    result2.put(name.trim(), obj_value);
                                }
                            }
                            i++;
                        } catch (SQLException e) {
                            e = e;
                            try {
                                throw new SQLException(new StringBuffer().append("database perform error : ").append(e.getMessage()).toString());
                            } catch (Throwable th) {
                                th = th;
                                CloseCon.Close(con);
                                throw th;
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            CloseCon.Close(con);
                            throw th;
                        }
                    }
                    result = result2;
                }
                rs.close();
            }
            stmt.close();
            CloseCon.Close(con);
            return result;
        } catch (SQLException e2) {
            e = e2;
            throw new SQLException(new StringBuffer().append("database perform error : ").append(e.getMessage()).toString());
        }
    }

    public HashMap getdbValue(DatabaseInfo dbInfo) throws Exception {
        String t_str;
        String tt_str;
        HashMap result = null;
        String temp_sql = createSql();
        if (temp_sql == null) {
            throw new SQLException("not sql statement");
        }
        Connection con = null;
        try {
            Class.forName(dbInfo.getDriver());
            con = DriverManager.getConnection(dbInfo.getUrl(), dbInfo.getUsername(), dbInfo.getPassword());
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(temp_sql);
            ResultSetMetaData rsmd = rs.getMetaData();
            int colCount = rsmd.getColumnCount();
            if (rs.next()) {
                HashMap result2 = new HashMap();
                int i = 1;
                while (i <= colCount) {
                    try {
                        String name = rsmd.getColumnName(i).toLowerCase();
                        if ("text".equals(rsmd.getColumnTypeName(i).toLowerCase())) {
                            String tt_str2 = rs.getString(i);
                            if (tt_str2 == null) {
                                tt_str = "";
                            } else {
                                tt_str = tt_str2.trim();
                            }
                            result2.put(name.trim(), tt_str);
                        } else {
                            Object obj_value = rs.getObject(i);
                            if (obj_value instanceof String) {
                                if (obj_value == null) {
                                    t_str = "";
                                } else {
                                    t_str = String.valueOf(obj_value).trim();
                                }
                                result2.put(name.trim(), t_str);
                            } else {
                                result2.put(name.trim(), obj_value);
                            }
                        }
                        i++;
                    } catch (SQLException e) {
                        e = e;
                        try {
                            throw new SQLException(new StringBuffer().append("database perform error : ").append(e.getMessage()).toString());
                        } catch (Throwable th) {
                            th = th;
                            CloseCon.Close(con);
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        CloseCon.Close(con);
                        throw th;
                    }
                }
                result = result2;
            }
            rs.close();
            stmt.close();
            CloseCon.Close(con);
            return result;
        } catch (SQLException e2) {
            e = e2;
            throw new SQLException(new StringBuffer().append("database perform error : ").append(e.getMessage()).toString());
        }
    }
}
