package com.kenfor.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class dbMaxID {
    private dbMaxID() {
    }

    public static Object getMaxValue(String path, String tableName, String valueFieldName) throws Exception {
        if (path != null) {
            return getMaxValue(new InitDatabase(path).getDatabaseInfo(), tableName, valueFieldName);
        }
        throw new SQLException("path can not be null");
    }

    public static Object getMaxValue(DatabaseInfo dbInfo, String tableName, String valueFieldName) throws Exception {
        if (dbInfo == null) {
            throw new SQLException("path can not be null");
        }
        Object result = null;
        try {
            Class.forName(dbInfo.getDriver());
            Connection con = DriverManager.getConnection(dbInfo.getUrl(), dbInfo.getUsername(), dbInfo.getPassword());
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(new StringBuffer().append("select ").append(valueFieldName).append(" from MAG_MAX_ID where TABLE_NAME='").append(tableName).append("'").toString());
            if (rs.next()) {
                result = rs.getObject(1);
            }
            rs.close();
            stmt.close();
            con.close();
            return result;
        } catch (SQLException e) {
            throw new Exception(e.getMessage());
        }
    }

    public static long getMaxID() {
        return (1000 * System.currentTimeMillis()) + ((long) ((int) ((Math.random() * 1000.0d) + 1.0d)));
    }

    public static int getMaxID(String path, String tableName) throws Exception {
        if (path != null) {
            return getMaxID(new InitDatabase(path).getDatabaseInfo(), tableName);
        }
        throw new SQLException("path can not be null");
    }

    public static int getMaxID(DatabaseInfo dbInfo, String tableName) throws Exception {
        int result;
        boolean geted;
        String sql;
        if (dbInfo == null) {
            throw new SQLException("dbInfo can not be null");
        }
        Connection con = null;
        try {
            Class.forName(dbInfo.getDriver());
            con = DriverManager.getConnection(dbInfo.getUrl(), dbInfo.getUsername(), dbInfo.getPassword());
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(new StringBuffer().append("select TABLE_ID from MAG_MAX_ID where TABLE_NAME='").append(tableName).append("'").toString());
            if (rs.next()) {
                result = rs.getInt("TABLE_ID");
                geted = true;
            } else {
                result = 1;
                geted = false;
            }
            rs.close();
            if (geted) {
                sql = new StringBuffer().append("update MAG_MAX_ID set TABLE_ID = ").append(result + 1).append(" where TABLE_NAME='").append(tableName).append("'").toString();
            } else {
                sql = new StringBuffer().append("insert into MAG_MAX_ID (TABLE_NAME,TABLE_ID) values('").append(tableName).append("',1)").toString();
            }
            stmt.execute(sql);
            con.close();
            try {
                con.close();
            } catch (Exception e) {
            }
            return result;
        } catch (SQLException e2) {
            throw new Exception(e2.getMessage());
        } catch (Throwable th) {
            try {
                con.close();
            } catch (Exception e3) {
            }
            throw th;
        }
    }
}
