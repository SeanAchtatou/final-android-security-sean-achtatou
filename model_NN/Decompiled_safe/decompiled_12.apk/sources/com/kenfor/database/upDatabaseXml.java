package com.kenfor.database;

import com.kenfor.client3g.util.Constant;
import java.io.FileOutputStream;
import java.io.IOException;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

public class upDatabaseXml {
    protected String dbType = "2";
    protected String driver = null;
    protected String maxActive = "20";
    protected String maxIdle = "0";
    protected String maxWait = "-1";
    protected String password = null;
    protected String path = null;
    protected String url = null;
    protected String username = null;

    public void setPath(String path2) {
        this.path = path2;
    }

    public void setDriver(String driver2) {
        this.driver = driver2;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public void setUsername(String username2) {
        this.username = username2;
    }

    public void setPassword(String password2) {
        this.password = password2;
    }

    public void setDbType(String dbType2) {
        this.dbType = dbType2;
    }

    public void setMaxActive(String maxActive2) {
        this.maxActive = maxActive2;
    }

    public void setMaxIdle(String maxIdle2) {
        this.maxIdle = maxIdle2;
    }

    public void setMaxWait(String maxWait2) {
        this.maxWait = maxWait2;
    }

    public void doUpdate() throws Exception {
        if (this.driver == null) {
            throw new Exception("driver 不能为空");
        } else if (this.url == null) {
            throw new Exception("url 不能为空");
        } else if (this.username == null) {
            throw new Exception("username 不能为空");
        } else if (this.password == null) {
            throw new Exception("password 不能为空");
        } else {
            Element element = new Element("database");
            Element edriver = new Element("driver");
            edriver.setText(this.driver);
            element.addContent(edriver);
            Element eurl = new Element("url");
            eurl.setText(this.url);
            element.addContent(eurl);
            Element eusername = new Element("username");
            eusername.setText(this.username);
            element.addContent(eusername);
            Element epassword = new Element(Constant.PASSWORD);
            epassword.setText(this.password);
            element.addContent(epassword);
            Element edbType = new Element("dbType");
            edbType.setText(this.dbType);
            element.addContent(edbType);
            Element emaxActive = new Element("maxActive");
            emaxActive.setText(this.maxActive);
            element.addContent(emaxActive);
            Element maxIdle2 = new Element("maxIdle");
            maxIdle2.setText(this.maxIdle);
            element.addContent(maxIdle2);
            Element emaxWait = new Element("maxWait");
            emaxWait.setText(this.maxWait);
            element.addContent(emaxWait);
            Document doc = new Document(element);
            FileOutputStream out = null;
            String fileName = "database-config.xml";
            try {
                if (this.path != null) {
                    fileName = new StringBuffer().append(this.path).append(System.getProperty("file.separator")).append("database-config.xml").toString();
                }
                XMLOutputter xMLOutputter = new XMLOutputter("", true, "gb2312");
                FileOutputStream fileOutputStream = new FileOutputStream(fileName);
                try {
                    xMLOutputter.output(doc, fileOutputStream);
                    fileOutputStream.close();
                } catch (IOException e) {
                    out = fileOutputStream;
                    out.close();
                    throw new Exception("更新database-config.xml出现异常");
                }
            } catch (IOException e2) {
                out.close();
                throw new Exception("更新database-config.xml出现异常");
            }
        }
    }
}
