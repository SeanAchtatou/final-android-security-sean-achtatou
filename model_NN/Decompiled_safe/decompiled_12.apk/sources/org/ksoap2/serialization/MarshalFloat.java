package org.ksoap2.serialization;

import java.io.IOException;
import java.math.BigDecimal;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public class MarshalFloat implements Marshal {
    static Class class$java$lang$Double;
    static Class class$java$lang$Float;
    static Class class$java$math$BigDecimal;

    public Object readInstance(XmlPullParser parser, String namespace, String name, PropertyInfo propertyInfo) throws IOException, XmlPullParserException {
        String stringValue = parser.nextText();
        if (name.equals("float")) {
            return new Float(stringValue);
        }
        if (name.equals("double")) {
            return new Double(stringValue);
        }
        if (name.equals("decimal")) {
            return new BigDecimal(stringValue);
        }
        throw new RuntimeException("float, double, or decimal expected");
    }

    public void writeInstance(XmlSerializer writer, Object instance) throws IOException {
        writer.text(instance.toString());
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError(x1.getMessage());
        }
    }

    public void register(SoapSerializationEnvelope cm) {
        Class cls;
        Class cls2;
        Class cls3;
        String str = cm.xsd;
        if (class$java$lang$Float == null) {
            cls = class$("java.lang.Float");
            class$java$lang$Float = cls;
        } else {
            cls = class$java$lang$Float;
        }
        cm.addMapping(str, "float", cls, this);
        String str2 = cm.xsd;
        if (class$java$lang$Double == null) {
            cls2 = class$("java.lang.Double");
            class$java$lang$Double = cls2;
        } else {
            cls2 = class$java$lang$Double;
        }
        cm.addMapping(str2, "double", cls2, this);
        String str3 = cm.xsd;
        if (class$java$math$BigDecimal == null) {
            cls3 = class$("java.math.BigDecimal");
            class$java$math$BigDecimal = cls3;
        } else {
            cls3 = class$java$math$BigDecimal;
        }
        cm.addMapping(str3, "decimal", cls3, this);
    }
}
