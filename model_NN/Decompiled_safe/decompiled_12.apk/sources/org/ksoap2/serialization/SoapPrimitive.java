package org.ksoap2.serialization;

public class SoapPrimitive extends AttributeContainer {
    String name;
    String namespace;
    String value;

    public SoapPrimitive(String namespace2, String name2, String value2) {
        this.namespace = namespace2;
        this.name = name2;
        this.value = value2;
    }

    public boolean equals(Object o) {
        boolean z = true;
        if (!(o instanceof SoapPrimitive)) {
            return false;
        }
        SoapPrimitive p = (SoapPrimitive) o;
        if (!(this.name.equals(p.name) && (this.namespace != null ? this.namespace.equals(p.namespace) : p.namespace == null) && (this.value != null ? this.value.equals(p.value) : p.value == null)) || !attributesAreEqual(p)) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        return (this.namespace == null ? 0 : this.namespace.hashCode()) ^ this.name.hashCode();
    }

    public String toString() {
        return this.value;
    }

    public String getNamespace() {
        return this.namespace;
    }

    public String getName() {
        return this.name;
    }
}
