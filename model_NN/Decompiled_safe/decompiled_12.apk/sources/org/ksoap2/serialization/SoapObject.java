package org.ksoap2.serialization;

import java.util.Hashtable;
import java.util.Vector;

public class SoapObject extends AttributeContainer implements KvmSerializable {
    private static final String EMPTY_STRING = "";
    static Class class$java$lang$String;
    static Class class$org$ksoap2$serialization$SoapObject;
    protected String name;
    protected String namespace;
    protected Vector properties = new Vector();

    public SoapObject(String namespace2, String name2) {
        this.namespace = namespace2;
        this.name = name2;
    }

    public boolean equals(Object obj) {
        int numProperties;
        if (!(obj instanceof SoapObject)) {
            return false;
        }
        SoapObject otherSoapObject = (SoapObject) obj;
        if (!this.name.equals(otherSoapObject.name) || !this.namespace.equals(otherSoapObject.namespace) || (numProperties = this.properties.size()) != otherSoapObject.properties.size()) {
            return false;
        }
        for (int propIndex = 0; propIndex < numProperties; propIndex++) {
            if (!otherSoapObject.isPropertyEqual(this.properties.elementAt(propIndex), propIndex)) {
                return false;
            }
        }
        return attributesAreEqual(otherSoapObject);
    }

    public boolean isPropertyEqual(Object otherProp, int index) {
        if (index >= getPropertyCount()) {
            return false;
        }
        Object thisProp = this.properties.elementAt(index);
        if ((otherProp instanceof PropertyInfo) && (thisProp instanceof PropertyInfo)) {
            PropertyInfo otherPropInfo = (PropertyInfo) otherProp;
            PropertyInfo thisPropInfo = (PropertyInfo) thisProp;
            if (!otherPropInfo.getName().equals(thisPropInfo.getName()) || !otherPropInfo.getValue().equals(thisPropInfo.getValue())) {
                return false;
            }
            return true;
        } else if (!(otherProp instanceof SoapObject) || !(thisProp instanceof SoapObject)) {
            return false;
        } else {
            return ((SoapObject) otherProp).equals((SoapObject) thisProp);
        }
    }

    public String getName() {
        return this.name;
    }

    public String getNamespace() {
        return this.namespace;
    }

    public Object getNestedSoap(int index) {
        return getProperty(index);
    }

    public Object getProperty(int index) {
        Object prop = this.properties.elementAt(index);
        if (prop instanceof PropertyInfo) {
            return ((PropertyInfo) prop).getValue();
        }
        return (SoapObject) prop;
    }

    public String getPropertyAsString(int index) {
        return ((PropertyInfo) this.properties.elementAt(index)).getValue().toString();
    }

    public Object getProperty(String name2) {
        Integer index = propertyIndex(name2);
        if (index != null) {
            return getProperty(index.intValue());
        }
        throw new RuntimeException(new StringBuffer().append("illegal property: ").append(name2).toString());
    }

    public String getPropertyAsString(String name2) {
        Integer index = propertyIndex(name2);
        if (index != null) {
            return getProperty(index.intValue()).toString();
        }
        throw new RuntimeException(new StringBuffer().append("illegal property: ").append(name2).toString());
    }

    public boolean hasProperty(String name2) {
        if (propertyIndex(name2) != null) {
            return true;
        }
        return false;
    }

    public Object getPropertySafely(String name2) {
        Integer i = propertyIndex(name2);
        if (i != null) {
            return getProperty(i.intValue());
        }
        return new NullSoapObject();
    }

    public String getPropertySafelyAsString(String name2) {
        Object foo;
        Integer i = propertyIndex(name2);
        if (i == null || (foo = getProperty(i.intValue())) == null) {
            return "";
        }
        return foo.toString();
    }

    public Object safeGetProperty(String name2) {
        return getPropertySafely(name2);
    }

    public Object getPropertySafely(String name2, Object defaultThing) {
        Integer i = propertyIndex(name2);
        if (i != null) {
            return getProperty(i.intValue());
        }
        return defaultThing;
    }

    public String getPropertySafelyAsString(String name2, Object defaultThing) {
        Integer i = propertyIndex(name2);
        if (i != null) {
            Object property = getProperty(i.intValue());
            if (property != null) {
                return property.toString();
            }
            return "";
        } else if (defaultThing != null) {
            return defaultThing.toString();
        } else {
            return "";
        }
    }

    public Object safeGetProperty(String name2, Object defaultThing) {
        return getPropertySafely(name2, defaultThing);
    }

    public Object getPrimitiveProperty(String name2) {
        Class cls;
        Class cls2;
        Integer index = propertyIndex(name2);
        if (index != null) {
            PropertyInfo propertyInfo = (PropertyInfo) this.properties.elementAt(index.intValue());
            Object type = propertyInfo.getType();
            if (class$org$ksoap2$serialization$SoapObject == null) {
                cls = class$("org.ksoap2.serialization.SoapObject");
                class$org$ksoap2$serialization$SoapObject = cls;
            } else {
                cls = class$org$ksoap2$serialization$SoapObject;
            }
            if (type != cls) {
                return propertyInfo.getValue();
            }
            PropertyInfo propertyInfo2 = new PropertyInfo();
            if (class$java$lang$String == null) {
                cls2 = class$("java.lang.String");
                class$java$lang$String = cls2;
            } else {
                cls2 = class$java$lang$String;
            }
            propertyInfo2.setType(cls2);
            propertyInfo2.setValue("");
            propertyInfo2.setName(name2);
            return propertyInfo2.getValue();
        }
        throw new RuntimeException(new StringBuffer().append("illegal property: ").append(name2).toString());
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError(x1.getMessage());
        }
    }

    public String getPrimitivePropertyAsString(String name2) {
        Class cls;
        Integer index = propertyIndex(name2);
        if (index != null) {
            PropertyInfo propertyInfo = (PropertyInfo) this.properties.elementAt(index.intValue());
            Object type = propertyInfo.getType();
            if (class$org$ksoap2$serialization$SoapObject == null) {
                cls = class$("org.ksoap2.serialization.SoapObject");
                class$org$ksoap2$serialization$SoapObject = cls;
            } else {
                cls = class$org$ksoap2$serialization$SoapObject;
            }
            if (type != cls) {
                return propertyInfo.getValue().toString();
            }
            return "";
        }
        throw new RuntimeException(new StringBuffer().append("illegal property: ").append(name2).toString());
    }

    public Object getPrimitivePropertySafely(String name2) {
        Class cls;
        Class cls2;
        Integer index = propertyIndex(name2);
        if (index == null) {
            return new NullSoapObject();
        }
        PropertyInfo propertyInfo = (PropertyInfo) this.properties.elementAt(index.intValue());
        Object type = propertyInfo.getType();
        if (class$org$ksoap2$serialization$SoapObject == null) {
            cls = class$("org.ksoap2.serialization.SoapObject");
            class$org$ksoap2$serialization$SoapObject = cls;
        } else {
            cls = class$org$ksoap2$serialization$SoapObject;
        }
        if (type != cls) {
            return propertyInfo.getValue().toString();
        }
        PropertyInfo propertyInfo2 = new PropertyInfo();
        if (class$java$lang$String == null) {
            cls2 = class$("java.lang.String");
            class$java$lang$String = cls2;
        } else {
            cls2 = class$java$lang$String;
        }
        propertyInfo2.setType(cls2);
        propertyInfo2.setValue("");
        propertyInfo2.setName(name2);
        return propertyInfo2.getValue();
    }

    public String getPrimitivePropertySafelyAsString(String name2) {
        Class cls;
        Integer index = propertyIndex(name2);
        if (index == null) {
            return "";
        }
        PropertyInfo propertyInfo = (PropertyInfo) this.properties.elementAt(index.intValue());
        Object type = propertyInfo.getType();
        if (class$org$ksoap2$serialization$SoapObject == null) {
            cls = class$("org.ksoap2.serialization.SoapObject");
            class$org$ksoap2$serialization$SoapObject = cls;
        } else {
            cls = class$org$ksoap2$serialization$SoapObject;
        }
        if (type != cls) {
            return propertyInfo.getValue().toString();
        }
        return "";
    }

    private Integer propertyIndex(String name2) {
        if (name2 != null) {
            for (int i = 0; i < this.properties.size(); i++) {
                if (name2.equals(((PropertyInfo) this.properties.elementAt(i)).getName())) {
                    return new Integer(i);
                }
            }
        }
        return null;
    }

    public int getPropertyCount() {
        return this.properties.size();
    }

    public int getNestedSoapCount() {
        int count = 0;
        for (int i = 0; i < this.properties.size(); i++) {
            if (this.properties.elementAt(i) instanceof SoapObject) {
                count++;
            }
        }
        return count;
    }

    public void getPropertyInfo(int index, Hashtable properties2, PropertyInfo propertyInfo) {
        getPropertyInfo(index, propertyInfo);
    }

    public void getPropertyInfo(int index, PropertyInfo propertyInfo) {
        Object element = this.properties.elementAt(index);
        if (element instanceof PropertyInfo) {
            PropertyInfo p = (PropertyInfo) element;
            propertyInfo.name = p.name;
            propertyInfo.namespace = p.namespace;
            propertyInfo.flags = p.flags;
            propertyInfo.type = p.type;
            propertyInfo.elementType = p.elementType;
            propertyInfo.value = p.value;
            propertyInfo.multiRef = p.multiRef;
            return;
        }
        propertyInfo.name = null;
        propertyInfo.namespace = null;
        propertyInfo.flags = 0;
        propertyInfo.type = null;
        propertyInfo.elementType = null;
        propertyInfo.value = element;
        propertyInfo.multiRef = false;
    }

    public SoapObject newInstance() {
        SoapObject o = new SoapObject(this.namespace, this.name);
        for (int propIndex = 0; propIndex < this.properties.size(); propIndex++) {
            Object prop = this.properties.elementAt(propIndex);
            if (prop instanceof PropertyInfo) {
                o.addProperty((PropertyInfo) this.properties.elementAt(propIndex));
            } else if (prop instanceof SoapObject) {
                o.addSoapObject(((SoapObject) prop).newInstance());
            }
        }
        for (int attribIndex = 0; attribIndex < getAttributeCount(); attribIndex++) {
            AttributeInfo newAI = new AttributeInfo();
            getAttributeInfo(attribIndex, newAI);
            o.addAttribute(newAI);
        }
        return o;
    }

    public void setProperty(int index, Object value) {
        Object prop = this.properties.elementAt(index);
        if (prop instanceof PropertyInfo) {
            ((PropertyInfo) prop).setValue(value);
        }
    }

    public SoapObject addProperty(String name2, Object value) {
        PropertyInfo propertyInfo = new PropertyInfo();
        propertyInfo.name = name2;
        propertyInfo.type = value == null ? PropertyInfo.OBJECT_CLASS : value.getClass();
        propertyInfo.value = value;
        return addProperty(propertyInfo);
    }

    public SoapObject addPropertyIfValue(String name2, Object value) {
        if (value != null) {
            return addProperty(name2, value);
        }
        return this;
    }

    public SoapObject addProperty(PropertyInfo propertyInfo, Object value) {
        propertyInfo.setValue(value);
        addProperty(propertyInfo);
        return this;
    }

    public SoapObject addPropertyIfValue(PropertyInfo propertyInfo, Object value) {
        if (value != null) {
            return addProperty(propertyInfo, value);
        }
        return this;
    }

    public SoapObject addProperty(PropertyInfo propertyInfo) {
        this.properties.addElement(propertyInfo);
        return this;
    }

    public SoapObject addPropertyIfValue(PropertyInfo propertyInfo) {
        if (propertyInfo.value != null) {
            this.properties.addElement(propertyInfo);
        }
        return this;
    }

    public SoapObject addSoapObject(SoapObject soapObject) {
        this.properties.addElement(soapObject);
        return this;
    }

    public String toString() {
        StringBuffer buf = new StringBuffer(new StringBuffer().append("").append(this.name).append("{").toString());
        for (int i = 0; i < getPropertyCount(); i++) {
            Object prop = this.properties.elementAt(i);
            if (prop instanceof PropertyInfo) {
                buf.append(new StringBuffer().append("").append(((PropertyInfo) prop).getName()).append("=").append(getProperty(i)).append("; ").toString());
            } else {
                buf.append(((SoapObject) prop).toString());
            }
        }
        buf.append("}");
        return buf.toString();
    }
}
