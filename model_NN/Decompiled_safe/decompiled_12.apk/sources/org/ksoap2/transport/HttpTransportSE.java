package org.ksoap2.transport;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.util.List;
import org.jivesoftware.smackx.packet.IBBExtensions;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.xmlpull.v1.XmlPullParserException;

public class HttpTransportSE extends Transport {
    private ServiceConnection connection;

    public HttpTransportSE(String url) {
        super((Proxy) null, url);
    }

    public HttpTransportSE(Proxy proxy, String url) {
        super(proxy, url);
    }

    public HttpTransportSE(String url, int timeout) {
        super(url, timeout);
    }

    public HttpTransportSE(Proxy proxy, String url, int timeout) {
        super(proxy, url, timeout);
    }

    public void call(String soapAction, SoapEnvelope envelope) throws IOException, XmlPullParserException {
        call(soapAction, envelope, null);
    }

    public List call(String soapAction, SoapEnvelope envelope, List headers) throws IOException, XmlPullParserException {
        InputStream is;
        if (soapAction == null) {
            soapAction = "\"\"";
        }
        byte[] requestData = createRequestData(envelope);
        this.requestDump = this.debug ? new String(requestData) : null;
        this.responseDump = null;
        this.connection = getServiceConnection();
        this.connection.setRequestProperty("User-Agent", "ksoap2-android/2.6.0+");
        if (envelope.version != 120) {
            this.connection.setRequestProperty("SOAPAction", soapAction);
        }
        if (envelope.version == 120) {
            this.connection.setRequestProperty("Content-Type", "application/soap+xml;charset=utf-8");
        } else {
            this.connection.setRequestProperty("Content-Type", "text/xml;charset=utf-8");
        }
        this.connection.setRequestProperty("Connection", IBBExtensions.Close.ELEMENT_NAME);
        this.connection.setRequestProperty("Content-Length", new StringBuffer().append("").append(requestData.length).toString());
        if (headers != null) {
            for (int i = 0; i < headers.size(); i++) {
                HeaderProperty hp = (HeaderProperty) headers.get(i);
                this.connection.setRequestProperty(hp.getKey(), hp.getValue());
            }
        }
        this.connection.setRequestMethod("POST");
        this.connection.connect();
        OutputStream os = this.connection.openOutputStream();
        os.write(requestData, 0, requestData.length);
        os.flush();
        os.close();
        List retHeaders = null;
        try {
            this.connection.connect();
            is = this.connection.openInputStream();
            retHeaders = this.connection.getResponseProperties();
        } catch (IOException e) {
            is = this.connection.getErrorStream();
            if (is == null) {
                this.connection.disconnect();
                throw e;
            }
        }
        if (this.debug) {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] buf = new byte[256];
            while (true) {
                int rd = is.read(buf, 0, 256);
                if (rd == -1) {
                    break;
                }
                bos.write(buf, 0, rd);
            }
            bos.flush();
            byte[] buf2 = bos.toByteArray();
            this.responseDump = new String(buf2);
            is.close();
            is = new ByteArrayInputStream(buf2);
        }
        parseResponse(envelope, is);
        return retHeaders;
    }

    public ServiceConnection getConnection() {
        return (ServiceConnectionSE) this.connection;
    }

    /* access modifiers changed from: protected */
    public ServiceConnection getServiceConnection() throws IOException {
        return new ServiceConnectionSE(this.proxy, this.url, this.timeout);
    }

    public String getHost() {
        try {
            return new URL(this.url).getHost();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public int getPort() {
        try {
            return new URL(this.url).getPort();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public String getPath() {
        try {
            return new URL(this.url).getPath();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
