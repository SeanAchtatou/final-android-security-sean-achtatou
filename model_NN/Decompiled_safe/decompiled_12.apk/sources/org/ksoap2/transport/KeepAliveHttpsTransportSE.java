package org.ksoap2.transport;

import java.io.IOException;

public class KeepAliveHttpsTransportSE extends HttpsTransportSE {
    private HttpsServiceConnectionSE conn = null;
    private final String file;
    private final String host;
    private final int port;
    private final int timeout;

    public KeepAliveHttpsTransportSE(String host2, int port2, String file2, int timeout2) {
        super(host2, port2, file2, timeout2);
        this.host = host2;
        this.port = port2;
        this.file = file2;
        this.timeout = timeout2;
    }

    /* access modifiers changed from: protected */
    public ServiceConnection getServiceConnection() throws IOException {
        this.conn = new HttpsServiceConnectionSEIgnoringConnectionClose(this.host, this.port, this.file, this.timeout);
        this.conn.setRequestProperty("Connection", "keep-alive");
        return this.conn;
    }
}
