package org.ksoap2.transport;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Proxy;
import java.util.List;
import org.ksoap2.SoapEnvelope;
import org.kxml2.io.KXmlParser;
import org.kxml2.io.KXmlSerializer;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public abstract class Transport {
    protected static final String CONTENT_TYPE_SOAP_XML_CHARSET_UTF_8 = "application/soap+xml;charset=utf-8";
    protected static final String CONTENT_TYPE_XML_CHARSET_UTF_8 = "text/xml;charset=utf-8";
    protected static final String USER_AGENT = "ksoap2-android/2.6.0+";
    public boolean debug;
    protected Proxy proxy;
    public String requestDump;
    public String responseDump;
    protected int timeout;
    protected String url;
    private String xmlVersionTag;

    public abstract List call(String str, SoapEnvelope soapEnvelope, List list) throws IOException, XmlPullParserException;

    public abstract String getHost();

    public abstract String getPath();

    public abstract int getPort();

    public Transport() {
        this.timeout = ServiceConnection.DEFAULT_TIMEOUT;
        this.xmlVersionTag = "";
    }

    public Transport(String url2) {
        this((Proxy) null, url2);
    }

    public Transport(String url2, int timeout2) {
        this.timeout = ServiceConnection.DEFAULT_TIMEOUT;
        this.xmlVersionTag = "";
        this.url = url2;
        this.timeout = timeout2;
    }

    public Transport(Proxy proxy2, String url2) {
        this.timeout = ServiceConnection.DEFAULT_TIMEOUT;
        this.xmlVersionTag = "";
        this.proxy = proxy2;
        this.url = url2;
    }

    public Transport(Proxy proxy2, String url2, int timeout2) {
        this.timeout = ServiceConnection.DEFAULT_TIMEOUT;
        this.xmlVersionTag = "";
        this.proxy = proxy2;
        this.url = url2;
        this.timeout = timeout2;
    }

    /* access modifiers changed from: protected */
    public void parseResponse(SoapEnvelope envelope, InputStream is) throws XmlPullParserException, IOException {
        XmlPullParser xp = new KXmlParser();
        xp.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, true);
        xp.setInput(is, null);
        envelope.parse(xp);
    }

    /* access modifiers changed from: protected */
    public byte[] createRequestData(SoapEnvelope envelope) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bos.write(this.xmlVersionTag.getBytes());
        XmlSerializer xw = new KXmlSerializer();
        xw.setOutput(bos, null);
        envelope.write(xw);
        xw.flush();
        bos.write(13);
        bos.write(10);
        bos.flush();
        return bos.toByteArray();
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public void setXmlVersionTag(String tag) {
        this.xmlVersionTag = tag;
    }

    public void reset() {
    }

    public void call(String targetNamespace, SoapEnvelope envelope) throws IOException, XmlPullParserException {
        call(targetNamespace, envelope, null);
    }
}
