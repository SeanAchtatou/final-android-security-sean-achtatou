package org.ksoap2.transport;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class HttpsTransportSE extends HttpTransportSE {
    static final String PROTOCOL = "https";
    private HttpsServiceConnectionSE conn = null;
    private final String file;
    private final String host;
    private final int port;
    private final int timeout;

    public HttpsTransportSE(String host2, int port2, String file2, int timeout2) {
        super(new StringBuffer().append("https://").append(host2).append(":").append(port2).append(file2).toString());
        this.host = host2;
        this.port = port2;
        this.file = file2;
        this.timeout = timeout2;
    }

    public ServiceConnection getConnection() {
        return this.conn;
    }

    /* access modifiers changed from: protected */
    public ServiceConnection getServiceConnection() throws IOException {
        this.conn = new HttpsServiceConnectionSE(this.host, this.port, this.file, this.timeout);
        return this.conn;
    }

    public String getHost() {
        try {
            return new URL(this.url).getHost();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public int getPort() {
        try {
            return new URL(this.url).getPort();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public String getPath() {
        try {
            return new URL(this.url).getPath();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
