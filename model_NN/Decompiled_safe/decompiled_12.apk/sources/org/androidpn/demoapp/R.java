package org.androidpn.demoapp;

public final class R {

    public static final class drawable {
        public static final int icon = 2130837514;
        public static final int notification = 2130837530;
    }

    public static final class id {
    }

    public static final class layout {
        public static final int main = 2130903041;
    }

    public static final class raw {
        public static final int androidpn = 2130968576;
    }

    public static final class string {
        public static final int app_name = 2131034112;
        public static final int hello = 2131034113;
        public static final int settings = 2131034114;
    }
}
