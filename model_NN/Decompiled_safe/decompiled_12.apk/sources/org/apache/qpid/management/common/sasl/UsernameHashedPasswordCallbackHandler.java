package org.apache.qpid.management.common.sasl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.harmony.javax.security.auth.callback.Callback;
import org.apache.harmony.javax.security.auth.callback.CallbackHandler;
import org.apache.harmony.javax.security.auth.callback.NameCallback;
import org.apache.harmony.javax.security.auth.callback.PasswordCallback;
import org.apache.harmony.javax.security.auth.callback.UnsupportedCallbackException;

public class UsernameHashedPasswordCallbackHandler implements CallbackHandler {
    private char[] pwchars;
    private String user;

    public UsernameHashedPasswordCallbackHandler(String user2, String password) throws Exception {
        this.user = user2;
        this.pwchars = getHash(password);
    }

    public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
        for (int i = 0; i < callbacks.length; i++) {
            if (callbacks[i] instanceof NameCallback) {
                ((NameCallback) callbacks[i]).setName(this.user);
            } else if (callbacks[i] instanceof PasswordCallback) {
                ((PasswordCallback) callbacks[i]).setPassword(this.pwchars);
            } else {
                throw new UnsupportedCallbackException(callbacks[i]);
            }
        }
    }

    private void clearPassword() {
        if (this.pwchars != null) {
            for (int i = 0; i < this.pwchars.length; i++) {
                this.pwchars[i] = 0;
            }
            this.pwchars = null;
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        clearPassword();
    }

    public static char[] getHash(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        int i = 0;
        byte[] data = text.getBytes("utf-8");
        MessageDigest md = MessageDigest.getInstance("MD5");
        for (byte b : data) {
            md.update(b);
        }
        byte[] digest = md.digest();
        char[] hash = new char[digest.length];
        int length = digest.length;
        int index = 0;
        while (i < length) {
            hash[index] = (char) digest[i];
            i++;
            index++;
        }
        return hash;
    }
}
