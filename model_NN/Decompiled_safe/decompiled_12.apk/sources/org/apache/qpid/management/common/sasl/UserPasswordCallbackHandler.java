package org.apache.qpid.management.common.sasl;

import java.io.IOException;
import org.apache.harmony.javax.security.auth.callback.Callback;
import org.apache.harmony.javax.security.auth.callback.CallbackHandler;
import org.apache.harmony.javax.security.auth.callback.NameCallback;
import org.apache.harmony.javax.security.auth.callback.PasswordCallback;
import org.apache.harmony.javax.security.auth.callback.UnsupportedCallbackException;

public class UserPasswordCallbackHandler implements CallbackHandler {
    private char[] pwchars;
    private String user;

    public UserPasswordCallbackHandler(String user2, String password) {
        this.user = user2;
        this.pwchars = password.toCharArray();
    }

    public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
        for (int i = 0; i < callbacks.length; i++) {
            if (callbacks[i] instanceof NameCallback) {
                ((NameCallback) callbacks[i]).setName(this.user);
            } else if (callbacks[i] instanceof PasswordCallback) {
                ((PasswordCallback) callbacks[i]).setPassword(this.pwchars);
            } else {
                throw new UnsupportedCallbackException(callbacks[i]);
            }
        }
    }

    private void clearPassword() {
        if (this.pwchars != null) {
            for (int i = 0; i < this.pwchars.length; i++) {
                this.pwchars[i] = 0;
            }
            this.pwchars = null;
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        clearPassword();
    }
}
