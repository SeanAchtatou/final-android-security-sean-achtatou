package org.apache.qpid.management.common.sasl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import org.apache.harmony.javax.security.auth.callback.Callback;
import org.apache.harmony.javax.security.auth.callback.CallbackHandler;
import org.apache.harmony.javax.security.auth.callback.NameCallback;
import org.apache.harmony.javax.security.auth.callback.PasswordCallback;
import org.apache.harmony.javax.security.auth.callback.UnsupportedCallbackException;
import org.apache.harmony.javax.security.sasl.SaslClient;
import org.apache.harmony.javax.security.sasl.SaslException;

public class PlainSaslClient implements SaslClient {
    private static byte SEPARATOR = 0;
    private String authenticationID;
    private String authorizationID;
    private CallbackHandler cbh;
    private boolean completed = false;
    private byte[] password;

    public PlainSaslClient(String authorizationID2, CallbackHandler cbh2) throws SaslException {
        this.cbh = cbh2;
        Object[] userInfo = getUserInfo();
        this.authorizationID = authorizationID2;
        this.authenticationID = (String) userInfo[0];
        this.password = (byte[]) userInfo[1];
        if (this.authenticationID == null || this.password == null) {
            throw new SaslException("PLAIN: authenticationID and password must be specified");
        }
    }

    public byte[] evaluateChallenge(byte[] challenge) throws SaslException {
        byte[] authzid;
        int size;
        int i = 0;
        if (this.completed) {
            throw new IllegalStateException("PLAIN: authentication already completed");
        }
        this.completed = true;
        try {
            if (this.authorizationID == null) {
                authzid = null;
            } else {
                authzid = this.authorizationID.getBytes("UTF8");
            }
            byte[] authnid = this.authenticationID.getBytes("UTF8");
            int length = this.password.length + authnid.length + 2;
            if (authzid != null) {
                i = authzid.length;
            }
            byte[] response = new byte[(i + length)];
            if (authzid != null) {
                System.arraycopy(authzid, 0, response, 0, authzid.length);
                size = authzid.length;
            } else {
                size = 0;
            }
            int size2 = size + 1;
            response[size] = SEPARATOR;
            System.arraycopy(authnid, 0, response, size2, authnid.length);
            int size3 = size2 + authnid.length;
            response[size3] = SEPARATOR;
            System.arraycopy(this.password, 0, response, size3 + 1, this.password.length);
            clearPassword();
            return response;
        } catch (UnsupportedEncodingException e) {
            throw new SaslException("PLAIN: Cannot get UTF-8 encoding of ids", e);
        }
    }

    public String getMechanismName() {
        return Constants.MECH_PLAIN;
    }

    public boolean hasInitialResponse() {
        return true;
    }

    public boolean isComplete() {
        return this.completed;
    }

    public byte[] unwrap(byte[] incoming, int offset, int len) throws SaslException {
        if (this.completed) {
            throw new IllegalStateException("PLAIN: this mechanism supports neither integrity nor privacy");
        }
        throw new IllegalStateException("PLAIN: authentication not completed");
    }

    public byte[] wrap(byte[] outgoing, int offset, int len) throws SaslException {
        if (this.completed) {
            throw new IllegalStateException("PLAIN: this mechanism supports neither integrity nor privacy");
        }
        throw new IllegalStateException("PLAIN: authentication not completed");
    }

    public Object getNegotiatedProperty(String propName) {
        if (!this.completed) {
            throw new IllegalStateException("PLAIN: authentication not completed");
        } else if (propName.equals("javax.security.sasl.qop")) {
            return "auth";
        } else {
            return null;
        }
    }

    private void clearPassword() {
        if (this.password != null) {
            for (int i = 0; i < this.password.length; i++) {
                this.password[i] = 0;
            }
            this.password = null;
        }
    }

    public void dispose() throws SaslException {
        clearPassword();
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        clearPassword();
    }

    private Object[] getUserInfo() throws SaslException {
        byte[] pwbytes;
        try {
            NameCallback nameCb = new NameCallback("PLAIN authentication id: ");
            PasswordCallback passwordCb = new PasswordCallback("PLAIN password: ", false);
            this.cbh.handle(new Callback[]{nameCb, passwordCb});
            String userid = nameCb.getName();
            char[] pwchars = passwordCb.getPassword();
            if (pwchars != null) {
                pwbytes = new String(pwchars).getBytes("UTF8");
                passwordCb.clearPassword();
            } else {
                pwbytes = null;
            }
            return new Object[]{userid, pwbytes};
        } catch (IOException e) {
            throw new SaslException("Cannot get password", e);
        } catch (UnsupportedCallbackException e2) {
            throw new SaslException("Cannot get userid/password", e2);
        }
    }
}
