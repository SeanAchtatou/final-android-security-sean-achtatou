package org.apache.qpid.management.common.sasl;

import de.measite.smack.Sasl;
import java.util.Map;
import org.apache.harmony.javax.security.auth.callback.CallbackHandler;
import org.apache.harmony.javax.security.sasl.SaslClient;
import org.apache.harmony.javax.security.sasl.SaslClientFactory;
import org.apache.harmony.javax.security.sasl.SaslException;

public class CRAMMD5HashedSaslClientFactory implements SaslClientFactory {
    public static final String MECHANISM = "CRAM-MD5-HASHED";

    public SaslClient createSaslClient(String[] mechanisms, String authorizationId, String protocol, String serverName, Map<String, ?> props, CallbackHandler cbh) throws SaslException {
        int i = 0;
        while (i < mechanisms.length) {
            if (!mechanisms[i].equals(MECHANISM)) {
                i++;
            } else if (cbh == null) {
                throw new SaslException("CallbackHandler must not be null");
            } else {
                return Sasl.createSaslClient(new String[]{Constants.MECH_CRAMMD5}, authorizationId, protocol, serverName, props, cbh);
            }
        }
        return null;
    }

    public String[] getMechanismNames(Map props) {
        return new String[]{MECHANISM};
    }
}
