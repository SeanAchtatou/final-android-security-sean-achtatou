package org.apache.qpid.management.common.sasl;

import java.security.Provider;
import java.util.Map;
import org.apache.harmony.javax.security.sasl.SaslClientFactory;

public class JCAProvider extends Provider {
    private static final long serialVersionUID = 1;

    public JCAProvider(Map<String, Class<? extends SaslClientFactory>> providerMap) {
        super("AMQSASLProvider", 1.0d, "A JCA provider that registers all AMQ SASL providers that want to be registered");
        register(providerMap);
    }

    private void register(Map<String, Class<? extends SaslClientFactory>> providerMap) {
        for (Map.Entry<String, Class<? extends SaslClientFactory>> me : providerMap.entrySet()) {
            put("SaslClientFactory." + ((String) me.getKey()), ((Class) me.getValue()).getName());
        }
    }
}
