package org.apache.harmony.javax.security.sasl;

import java.io.Serializable;
import org.apache.harmony.javax.security.auth.callback.Callback;

public class AuthorizeCallback implements Callback, Serializable {
    private static final long serialVersionUID = -2353344186490470805L;
    private final String authenticationID;
    private final String authorizationID;
    private boolean authorized;
    private String authorizedID;

    public AuthorizeCallback(String authnID, String authzID) {
        this.authenticationID = authnID;
        this.authorizationID = authzID;
        this.authorizedID = authzID;
    }

    public String getAuthenticationID() {
        return this.authenticationID;
    }

    public String getAuthorizationID() {
        return this.authorizationID;
    }

    public String getAuthorizedID() {
        if (this.authorized) {
            return this.authorizedID;
        }
        return null;
    }

    public boolean isAuthorized() {
        return this.authorized;
    }

    public void setAuthorized(boolean ok) {
        this.authorized = ok;
    }

    public void setAuthorizedID(String id) {
        if (id != null) {
            this.authorizedID = id;
        }
    }
}
