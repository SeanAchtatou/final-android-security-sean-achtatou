package org.apache.harmony.javax.security.auth;

import java.security.BasicPermission;

public final class AuthPermission extends BasicPermission {
    private static final String CREATE_LOGIN_CONTEXT = "createLoginContext";
    private static final String CREATE_LOGIN_CONTEXT_ANY = "createLoginContext.*";
    private static final long serialVersionUID = 5806031445061587174L;

    private static String init(String name) {
        if (name == null) {
            throw new NullPointerException("auth.13");
        } else if (CREATE_LOGIN_CONTEXT.equals(name)) {
            return CREATE_LOGIN_CONTEXT_ANY;
        } else {
            return name;
        }
    }

    public AuthPermission(String name) {
        super(init(name));
    }

    public AuthPermission(String name, String actions) {
        super(init(name), actions);
    }
}
