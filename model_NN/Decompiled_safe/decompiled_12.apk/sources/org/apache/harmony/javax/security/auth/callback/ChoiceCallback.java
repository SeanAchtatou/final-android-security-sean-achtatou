package org.apache.harmony.javax.security.auth.callback;

import java.io.Serializable;

public class ChoiceCallback implements Callback, Serializable {
    private static final long serialVersionUID = -3975664071579892167L;
    private String[] choices;
    private int defaultChoice;
    private boolean multipleSelectionsAllowed;
    private String prompt;
    private int[] selections;

    private void setChoices(String[] choices2) {
        if (choices2 == null || choices2.length == 0) {
            throw new IllegalArgumentException("auth.1C");
        }
        for (int i = 0; i < choices2.length; i++) {
            if (choices2[i] == null || choices2[i].length() == 0) {
                throw new IllegalArgumentException("auth.1C");
            }
        }
        this.choices = choices2;
    }

    private void setPrompt(String prompt2) {
        if (prompt2 == null || prompt2.length() == 0) {
            throw new IllegalArgumentException("auth.14");
        }
        this.prompt = prompt2;
    }

    private void setDefaultChoice(int defaultChoice2) {
        if (defaultChoice2 < 0 || defaultChoice2 >= this.choices.length) {
            throw new IllegalArgumentException("auth.1D");
        }
        this.defaultChoice = defaultChoice2;
    }

    public ChoiceCallback(String prompt2, String[] choices2, int defaultChoice2, boolean multipleSelectionsAllowed2) {
        setPrompt(prompt2);
        setChoices(choices2);
        setDefaultChoice(defaultChoice2);
        this.multipleSelectionsAllowed = multipleSelectionsAllowed2;
    }

    public boolean allowMultipleSelections() {
        return this.multipleSelectionsAllowed;
    }

    public String[] getChoices() {
        return this.choices;
    }

    public int getDefaultChoice() {
        return this.defaultChoice;
    }

    public String getPrompt() {
        return this.prompt;
    }

    public int[] getSelectedIndexes() {
        return this.selections;
    }

    public void setSelectedIndex(int selection) {
        this.selections = new int[1];
        this.selections[0] = selection;
    }

    public void setSelectedIndexes(int[] selections2) {
        if (!this.multipleSelectionsAllowed) {
            throw new UnsupportedOperationException();
        }
        this.selections = selections2;
    }
}
