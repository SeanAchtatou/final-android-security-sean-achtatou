package org.apache.harmony.javax.security.auth.callback;

import java.io.Serializable;

public class NameCallback implements Callback, Serializable {
    private static final long serialVersionUID = 3770938795909392253L;
    private String defaultName;
    private String inputName;
    private String prompt;

    private void setPrompt(String prompt2) {
        if (prompt2 == null || prompt2.length() == 0) {
            throw new IllegalArgumentException("auth.14");
        }
        this.prompt = prompt2;
    }

    private void setDefaultName(String defaultName2) {
        if (defaultName2 == null || defaultName2.length() == 0) {
            throw new IllegalArgumentException("auth.1E");
        }
        this.defaultName = defaultName2;
    }

    public NameCallback(String prompt2) {
        setPrompt(prompt2);
    }

    public NameCallback(String prompt2, String defaultName2) {
        setPrompt(prompt2);
        setDefaultName(defaultName2);
    }

    public String getPrompt() {
        return this.prompt;
    }

    public String getDefaultName() {
        return this.defaultName;
    }

    public void setName(String name) {
        this.inputName = name;
    }

    public String getName() {
        return this.inputName;
    }
}
