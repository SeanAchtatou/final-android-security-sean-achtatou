package org.apache.harmony.javax.security.auth;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.security.AccessControlContext;
import java.security.AccessController;
import java.security.DomainCombiner;
import java.security.Permission;
import java.security.Principal;
import java.security.PrivilegedAction;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.security.ProtectionDomain;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

public final class Subject implements Serializable {
    private static final AuthPermission _AS = new AuthPermission("doAs");
    private static final AuthPermission _AS_PRIVILEGED = new AuthPermission("doAsPrivileged");
    /* access modifiers changed from: private */
    public static final AuthPermission _PRINCIPALS = new AuthPermission("modifyPrincipals");
    /* access modifiers changed from: private */
    public static final AuthPermission _PRIVATE_CREDENTIALS = new AuthPermission("modifyPrivateCredentials");
    /* access modifiers changed from: private */
    public static final AuthPermission _PUBLIC_CREDENTIALS = new AuthPermission("modifyPublicCredentials");
    private static final AuthPermission _READ_ONLY = new AuthPermission("setReadOnly");
    private static final AuthPermission _SUBJECT = new AuthPermission("getSubject");
    private static final long serialVersionUID = -8308522755600156056L;
    /* access modifiers changed from: private */
    public final Set<Principal> principals;
    private transient SecureSet<Object> privateCredentials;
    private transient SecureSet<Object> publicCredentials;
    private boolean readOnly;

    public Subject() {
        this.principals = new SecureSet(_PRINCIPALS);
        this.publicCredentials = new SecureSet<>(_PUBLIC_CREDENTIALS);
        this.privateCredentials = new SecureSet<>(_PRIVATE_CREDENTIALS);
        this.readOnly = false;
    }

    public Subject(boolean readOnly2, Set<? extends Principal> subjPrincipals, Set<?> pubCredentials, Set<?> privCredentials) {
        if (subjPrincipals == null || pubCredentials == null || privCredentials == null) {
            throw new NullPointerException();
        }
        this.principals = new SecureSet(this, _PRINCIPALS, subjPrincipals);
        this.publicCredentials = new SecureSet<>(this, _PUBLIC_CREDENTIALS, pubCredentials);
        this.privateCredentials = new SecureSet<>(this, _PRIVATE_CREDENTIALS, privCredentials);
        this.readOnly = readOnly2;
    }

    public static Object doAs(Subject subject, PrivilegedAction action) {
        checkPermission(_AS);
        return doAs_PrivilegedAction(subject, action, AccessController.getContext());
    }

    public static Object doAsPrivileged(Subject subject, PrivilegedAction action, AccessControlContext context) {
        checkPermission(_AS_PRIVILEGED);
        if (context == null) {
            return doAs_PrivilegedAction(subject, action, new AccessControlContext(new ProtectionDomain[0]));
        }
        return doAs_PrivilegedAction(subject, action, context);
    }

    private static Object doAs_PrivilegedAction(Subject subject, PrivilegedAction action, final AccessControlContext context) {
        final SubjectDomainCombiner combiner;
        if (subject == null) {
            combiner = null;
        } else {
            combiner = new SubjectDomainCombiner(subject);
        }
        return AccessController.doPrivileged(action, (AccessControlContext) AccessController.doPrivileged(new PrivilegedAction() {
            public Object run() {
                return new AccessControlContext(context, combiner);
            }
        }));
    }

    public static Object doAs(Subject subject, PrivilegedExceptionAction action) throws PrivilegedActionException {
        checkPermission(_AS);
        return doAs_PrivilegedExceptionAction(subject, action, AccessController.getContext());
    }

    public static Object doAsPrivileged(Subject subject, PrivilegedExceptionAction action, AccessControlContext context) throws PrivilegedActionException {
        checkPermission(_AS_PRIVILEGED);
        if (context == null) {
            return doAs_PrivilegedExceptionAction(subject, action, new AccessControlContext(new ProtectionDomain[0]));
        }
        return doAs_PrivilegedExceptionAction(subject, action, context);
    }

    private static Object doAs_PrivilegedExceptionAction(Subject subject, PrivilegedExceptionAction action, final AccessControlContext context) throws PrivilegedActionException {
        final SubjectDomainCombiner combiner;
        if (subject == null) {
            combiner = null;
        } else {
            combiner = new SubjectDomainCombiner(subject);
        }
        return AccessController.doPrivileged(action, (AccessControlContext) AccessController.doPrivileged(new PrivilegedAction<AccessControlContext>() {
            public AccessControlContext run() {
                return new AccessControlContext(context, combiner);
            }
        }));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Subject that = (Subject) obj;
        if (!this.principals.equals(that.principals) || !this.publicCredentials.equals(that.publicCredentials) || !this.privateCredentials.equals(that.privateCredentials)) {
            return false;
        }
        return true;
    }

    public Set<Principal> getPrincipals() {
        return this.principals;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [java.lang.Class, java.lang.Class<T>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <T extends java.security.Principal> java.util.Set<T> getPrincipals(java.lang.Class<T> r2) {
        /*
            r1 = this;
            java.util.Set<java.security.Principal> r0 = r1.principals
            org.apache.harmony.javax.security.auth.Subject$SecureSet r0 = (org.apache.harmony.javax.security.auth.Subject.SecureSet) r0
            java.util.Set r0 = r0.get(r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.harmony.javax.security.auth.Subject.getPrincipals(java.lang.Class):java.util.Set");
    }

    public Set<Object> getPrivateCredentials() {
        return this.privateCredentials;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [java.lang.Class, java.lang.Class<T>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <T> java.util.Set<T> getPrivateCredentials(java.lang.Class<T> r2) {
        /*
            r1 = this;
            org.apache.harmony.javax.security.auth.Subject$SecureSet<java.lang.Object> r0 = r1.privateCredentials
            java.util.Set r0 = r0.get(r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.harmony.javax.security.auth.Subject.getPrivateCredentials(java.lang.Class):java.util.Set");
    }

    public Set<Object> getPublicCredentials() {
        return this.publicCredentials;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [java.lang.Class, java.lang.Class<T>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <T> java.util.Set<T> getPublicCredentials(java.lang.Class<T> r2) {
        /*
            r1 = this;
            org.apache.harmony.javax.security.auth.Subject$SecureSet<java.lang.Object> r0 = r1.publicCredentials
            java.util.Set r0 = r0.get(r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.harmony.javax.security.auth.Subject.getPublicCredentials(java.lang.Class):java.util.Set");
    }

    public int hashCode() {
        return this.principals.hashCode() + this.privateCredentials.hashCode() + this.publicCredentials.hashCode();
    }

    public void setReadOnly() {
        checkPermission(_READ_ONLY);
        this.readOnly = true;
    }

    public boolean isReadOnly() {
        return this.readOnly;
    }

    public String toString() {
        StringBuilder buf = new StringBuilder("Subject:\n");
        for (Principal append : this.principals) {
            buf.append("\tPrincipal: ");
            buf.append(append);
            buf.append(10);
        }
        Iterator<?> it = this.publicCredentials.iterator();
        while (it.hasNext()) {
            buf.append("\tPublic Credential: ");
            buf.append(it.next());
            buf.append(10);
        }
        int offset = buf.length() - 1;
        Iterator<?> it2 = this.privateCredentials.iterator();
        while (it2.hasNext()) {
            try {
                buf.append("\tPrivate Credential: ");
                buf.append(it2.next());
                buf.append(10);
            } catch (SecurityException e) {
                buf.delete(offset, buf.length());
                buf.append("\tPrivate Credentials: no accessible information\n");
            }
        }
        return buf.toString();
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        this.publicCredentials = new SecureSet<>(_PUBLIC_CREDENTIALS);
        this.privateCredentials = new SecureSet<>(_PRIVATE_CREDENTIALS);
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();
    }

    public static Subject getSubject(final AccessControlContext context) {
        checkPermission(_SUBJECT);
        if (context == null) {
            throw new NullPointerException("auth.09");
        }
        DomainCombiner combiner = (DomainCombiner) AccessController.doPrivileged(new PrivilegedAction<DomainCombiner>() {
            public DomainCombiner run() {
                return context.getDomainCombiner();
            }
        });
        if (combiner == null || !(combiner instanceof SubjectDomainCombiner)) {
            return null;
        }
        return ((SubjectDomainCombiner) combiner).getSubject();
    }

    /* access modifiers changed from: private */
    public static void checkPermission(Permission p) {
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            sm.checkPermission(p);
        }
    }

    /* access modifiers changed from: private */
    public void checkState() {
        if (this.readOnly) {
            throw new IllegalStateException("auth.0A");
        }
    }

    private final class SecureSet<SST> extends AbstractSet<SST> implements Serializable {
        private static final int SET_Principal = 0;
        private static final int SET_PrivCred = 1;
        private static final int SET_PubCred = 2;
        private static final long serialVersionUID = 7911754171111800359L;
        private LinkedList<SST> elements;
        /* access modifiers changed from: private */
        public transient AuthPermission permission;
        private int setType;

        protected SecureSet(AuthPermission perm) {
            this.permission = perm;
            this.elements = new LinkedList<>();
        }

        protected SecureSet(Subject subject, AuthPermission perm, Collection<? extends SST> s) {
            this(perm);
            boolean trust = s.getClass().getClassLoader() == null;
            for (SST o : s) {
                verifyElement(o);
                if (trust || !this.elements.contains(o)) {
                    this.elements.add(o);
                }
            }
        }

        private void verifyElement(Object o) {
            if (o == null) {
                throw new NullPointerException();
            } else if (this.permission == Subject._PRINCIPALS && !Principal.class.isAssignableFrom(o.getClass())) {
                throw new IllegalArgumentException("auth.0B");
            }
        }

        public boolean add(SST o) {
            verifyElement(o);
            Subject.this.checkState();
            Subject.checkPermission(this.permission);
            if (this.elements.contains(o)) {
                return false;
            }
            this.elements.add(o);
            return true;
        }

        public Iterator<SST> iterator() {
            return this.permission == Subject._PRIVATE_CREDENTIALS ? new SecureSet<SST>.SecureIterator(this.elements.iterator()) {
                public SST next() {
                    SST obj = this.iterator.next();
                    Subject.checkPermission(new PrivateCredentialPermission(obj.getClass().getName(), Subject.this.principals));
                    return obj;
                }
            } : new SecureIterator(this.elements.iterator());
        }

        public boolean retainAll(Collection<?> c) {
            if (c != null) {
                return super.retainAll(c);
            }
            throw new NullPointerException();
        }

        public int size() {
            return this.elements.size();
        }

        /* access modifiers changed from: protected */
        public final <E> Set<E> get(final Class<E> c) {
            if (c == null) {
                throw new NullPointerException();
            }
            AbstractSet<E> s = new AbstractSet<E>() {
                private LinkedList<E> elements = new LinkedList<>();

                public boolean add(E o) {
                    if (!c.isAssignableFrom(o.getClass())) {
                        throw new IllegalArgumentException("auth.0C " + c.getName());
                    } else if (this.elements.contains(o)) {
                        return false;
                    } else {
                        this.elements.add(o);
                        return true;
                    }
                }

                public Iterator<E> iterator() {
                    return this.elements.iterator();
                }

                public boolean retainAll(Collection<?> c) {
                    if (c != null) {
                        return super.retainAll(c);
                    }
                    throw new NullPointerException();
                }

                public int size() {
                    return this.elements.size();
                }
            };
            Iterator<SST> it = iterator();
            while (it.hasNext()) {
                SST o = it.next();
                if (c.isAssignableFrom(o.getClass())) {
                    s.add(c.cast(o));
                }
            }
            return s;
        }

        private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
            in.defaultReadObject();
            switch (this.setType) {
                case 0:
                    this.permission = Subject._PRINCIPALS;
                    break;
                case 1:
                    this.permission = Subject._PRIVATE_CREDENTIALS;
                    break;
                case 2:
                    this.permission = Subject._PUBLIC_CREDENTIALS;
                    break;
                default:
                    throw new IllegalArgumentException();
            }
            Iterator<SST> it = this.elements.iterator();
            while (it.hasNext()) {
                verifyElement(it.next());
            }
        }

        private void writeObject(ObjectOutputStream out) throws IOException {
            if (this.permission == Subject._PRIVATE_CREDENTIALS) {
                Iterator<SST> it = iterator();
                while (it.hasNext()) {
                    it.next();
                }
                this.setType = 1;
            } else if (this.permission == Subject._PRINCIPALS) {
                this.setType = 0;
            } else {
                this.setType = 2;
            }
            out.defaultWriteObject();
        }

        private class SecureIterator implements Iterator<SST> {
            protected Iterator<SST> iterator;

            protected SecureIterator(Iterator<SST> iterator2) {
                this.iterator = iterator2;
            }

            public boolean hasNext() {
                return this.iterator.hasNext();
            }

            public SST next() {
                return this.iterator.next();
            }

            public void remove() {
                Subject.this.checkState();
                Subject.checkPermission(SecureSet.this.permission);
                this.iterator.remove();
            }
        }
    }
}
