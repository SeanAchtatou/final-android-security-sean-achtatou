package org.apache.harmony.javax.security.auth;

import java.security.DomainCombiner;
import java.security.Principal;
import java.security.ProtectionDomain;
import java.util.Set;

public class SubjectDomainCombiner implements DomainCombiner {
    private static final AuthPermission _GET = new AuthPermission("getSubjectFromDomainCombiner");
    private Subject subject;

    public SubjectDomainCombiner(Subject subject2) {
        if (subject2 == null) {
            throw new NullPointerException();
        }
        this.subject = subject2;
    }

    public Subject getSubject() {
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            sm.checkPermission(_GET);
        }
        return this.subject;
    }

    public ProtectionDomain[] combine(ProtectionDomain[] currentDomains, ProtectionDomain[] assignedDomains) {
        int len = 0;
        if (currentDomains != null) {
            len = 0 + currentDomains.length;
        }
        if (assignedDomains != null) {
            len += assignedDomains.length;
        }
        if (len == 0) {
            return null;
        }
        ProtectionDomain[] pd = new ProtectionDomain[len];
        int cur = 0;
        if (currentDomains != null) {
            Set<Principal> s = this.subject.getPrincipals();
            Principal[] p = (Principal[]) s.toArray(new Principal[s.size()]);
            cur = 0;
            while (cur < currentDomains.length) {
                if (currentDomains[cur] != null) {
                    pd[cur] = new ProtectionDomain(currentDomains[cur].getCodeSource(), currentDomains[cur].getPermissions(), currentDomains[cur].getClassLoader(), p);
                }
                cur++;
            }
        }
        if (assignedDomains == null) {
            return pd;
        }
        System.arraycopy(assignedDomains, 0, pd, cur, assignedDomains.length);
        return pd;
    }
}
