package org.jivesoftware.smackx.filetransfer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.PacketCollector;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.PacketIDFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.util.Cache;
import org.jivesoftware.smackx.ServiceDiscoveryManager;
import org.jivesoftware.smackx.packet.Bytestream;
import org.jivesoftware.smackx.packet.DiscoverInfo;
import org.jivesoftware.smackx.packet.DiscoverItems;

public class Socks5TransferNegotiatorManager implements FileTransferNegotiatorManager {
    private static final long BLACKLIST_LIFETIME = 7200000;
    private static ProxyProcess proxyProcess;
    private final Cache<String, Integer> addressBlacklist = new Cache<>(100, BLACKLIST_LIFETIME);
    private Connection connection;
    private final Object processLock = new Object();
    private List<String> proxies;
    private final Object proxyLock = new Object();
    private List<Bytestream.StreamHost> streamHosts;

    public Socks5TransferNegotiatorManager(Connection connection2) {
        this.connection = connection2;
    }

    public StreamNegotiator createNegotiator() {
        return new Socks5TransferNegotiator(this, this.connection);
    }

    public void incrementConnectionFailures(String address) {
        int count;
        Integer count2 = this.addressBlacklist.get(address);
        if (count2 == null) {
            count = 1;
        } else {
            count = Integer.valueOf(count2.intValue() + 1);
        }
        this.addressBlacklist.put(address, count);
    }

    public int getConnectionFailures(String address) {
        Integer count = this.addressBlacklist.get(address);
        if (count != null) {
            return count.intValue();
        }
        return 0;
    }

    public ProxyProcess addTransfer() throws IOException {
        synchronized (this.processLock) {
            if (proxyProcess == null) {
                proxyProcess = new ProxyProcess(new ServerSocket(7777));
                proxyProcess.start();
            }
        }
        proxyProcess.addTransfer();
        return proxyProcess;
    }

    public void removeTransfer() {
        if (proxyProcess != null) {
            proxyProcess.removeTransfer();
        }
    }

    public Collection<Bytestream.StreamHost> getStreamHosts() {
        synchronized (this.proxyLock) {
            if (this.proxies == null) {
                initProxies();
            }
        }
        return Collections.unmodifiableCollection(this.streamHosts);
    }

    private String checkIsProxy(ServiceDiscoveryManager manager, DiscoverItems.Item item) {
        try {
            DiscoverInfo info = manager.discoverInfo(item.getEntityID());
            Iterator<DiscoverInfo.Identity> itx = info.getIdentities();
            while (itx.hasNext()) {
                DiscoverInfo.Identity identity = itx.next();
                if ("proxy".equalsIgnoreCase(identity.getCategory()) && "bytestreams".equalsIgnoreCase(identity.getType())) {
                    return info.getFrom();
                }
            }
            return null;
        } catch (XMPPException e) {
            return null;
        }
    }

    private void initProxies() {
        this.proxies = new ArrayList();
        ServiceDiscoveryManager manager = ServiceDiscoveryManager.getInstanceFor(this.connection);
        try {
            Iterator<DiscoverItems.Item> it = manager.discoverItems(this.connection.getServiceName()).getItems();
            while (it.hasNext()) {
                String proxy = checkIsProxy(manager, it.next());
                if (proxy != null) {
                    this.proxies.add(proxy);
                }
            }
            if (this.proxies.size() > 0) {
                initStreamHosts();
            }
        } catch (XMPPException e) {
        }
    }

    private void initStreamHosts() {
        List<Bytestream.StreamHost> streamHosts2 = new ArrayList<>();
        for (String jid : this.proxies) {
            IQ query = new IQ() {
                public String getChildElementXML() {
                    return "<query xmlns=\"http://jabber.org/protocol/bytestreams\"/>";
                }
            };
            query.setType(IQ.Type.GET);
            query.setTo(jid);
            PacketCollector collector = this.connection.createPacketCollector(new PacketIDFilter(query.getPacketID()));
            this.connection.sendPacket(query);
            Bytestream response = (Bytestream) collector.nextResult((long) SmackConfiguration.getPacketReplyTimeout());
            if (response != null) {
                streamHosts2.addAll(response.getStreamHosts());
            }
            collector.cancel();
        }
        this.streamHosts = streamHosts2;
    }

    public void cleanup() {
        synchronized (this.processLock) {
            if (proxyProcess != null) {
                proxyProcess.stop();
                proxyProcess = null;
            }
        }
    }

    class ProxyProcess implements Runnable {
        private final Map<String, Socket> connectionMap = new HashMap();
        private boolean done = false;
        private final ServerSocket listeningSocket;
        private Thread thread = new Thread(this, "File Transfer Connection Listener");
        private int transfers;

        public void run() {
            try {
                this.listeningSocket.setSoTimeout(10000);
                while (!this.done) {
                    Socket conn = null;
                    synchronized (this) {
                        while (this.transfers <= 0 && !this.done) {
                            this.transfers = -1;
                            try {
                                wait();
                            } catch (InterruptedException e) {
                            }
                        }
                    }
                    if (!this.done) {
                        try {
                            synchronized (this.listeningSocket) {
                                conn = this.listeningSocket.accept();
                            }
                            if (conn != null) {
                                String digest = establishSocks5UploadConnection(conn);
                                synchronized (this.connectionMap) {
                                    this.connectionMap.put(digest, conn);
                                }
                            }
                        } catch (IOException | SocketTimeoutException e2) {
                        } catch (XMPPException e3) {
                            e3.printStackTrace();
                            if (conn != null) {
                                try {
                                    conn.close();
                                } catch (IOException e4) {
                                }
                            }
                        }
                    }
                }
                try {
                    this.listeningSocket.close();
                } catch (IOException e5) {
                }
            } catch (SocketException e6) {
                e6.printStackTrace();
                try {
                    this.listeningSocket.close();
                } catch (IOException e7) {
                }
            } catch (Throwable th) {
                try {
                    this.listeningSocket.close();
                } catch (IOException e8) {
                }
                throw th;
            }
        }

        private String establishSocks5UploadConnection(Socket connection) throws XMPPException, IOException {
            OutputStream out = new DataOutputStream(connection.getOutputStream());
            InputStream in = new DataInputStream(connection.getInputStream());
            if (in.read() != 5) {
                throw new XMPPException("Only SOCKS5 supported");
            }
            int b = in.read();
            int[] auth = new int[b];
            for (int i = 0; i < b; i++) {
                auth[i] = in.read();
            }
            int authMethod = -1;
            int length = auth.length;
            for (int i2 = 0; i2 < length; i2++) {
                authMethod = auth[i2] == 0 ? 0 : -1;
                if (authMethod == 0) {
                    break;
                }
            }
            if (authMethod != 0) {
                throw new XMPPException("Authentication method not supported");
            }
            out.write(new byte[]{5, 0});
            String responseDigest = Socks5TransferNegotiator.createIncomingSocks5Message(in);
            byte[] cmd = Socks5TransferNegotiator.createOutgoingSocks5Message(0, responseDigest);
            if (!connection.isConnected()) {
                throw new XMPPException("Socket closed by remote user");
            }
            out.write(cmd);
            return responseDigest;
        }

        public void start() {
            this.thread.start();
        }

        public void stop() {
            this.done = true;
            synchronized (this) {
                notify();
            }
            synchronized (this.listeningSocket) {
                this.listeningSocket.notify();
            }
        }

        public int getPort() {
            return this.listeningSocket.getLocalPort();
        }

        ProxyProcess(ServerSocket listeningSocket2) {
            this.listeningSocket = listeningSocket2;
        }

        public Socket getSocket(String digest) {
            Socket socket;
            synchronized (this.connectionMap) {
                socket = this.connectionMap.get(digest);
            }
            return socket;
        }

        public void addTransfer() {
            synchronized (this) {
                if (this.transfers == -1) {
                    this.transfers = 1;
                    notify();
                } else {
                    this.transfers++;
                }
            }
        }

        public void removeTransfer() {
            synchronized (this) {
                this.transfers--;
            }
        }
    }
}
