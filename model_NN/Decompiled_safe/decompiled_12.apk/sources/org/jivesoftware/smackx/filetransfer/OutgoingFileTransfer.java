package org.jivesoftware.smackx.filetransfer;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.XMPPError;
import org.jivesoftware.smackx.filetransfer.FileTransfer;

public class OutgoingFileTransfer extends FileTransfer {
    private static int RESPONSE_TIMEOUT = 60000;
    private NegotiationProgress callback;
    private String initiator;
    /* access modifiers changed from: private */
    public OutputStream outputStream;
    private Thread transferThread;

    public interface NegotiationProgress {
        void errorEstablishingStream(Exception exc);

        void outputStreamEstablished(OutputStream outputStream);

        void statusUpdated(FileTransfer.Status status, FileTransfer.Status status2);
    }

    public static int getResponseTimeout() {
        return RESPONSE_TIMEOUT;
    }

    public static void setResponseTimeout(int responseTimeout) {
        RESPONSE_TIMEOUT = responseTimeout;
    }

    protected OutgoingFileTransfer(String initiator2, String target, String streamID, FileTransferNegotiator transferNegotiator) {
        super(target, streamID, transferNegotiator);
        this.initiator = initiator2;
    }

    /* access modifiers changed from: protected */
    public void setOutputStream(OutputStream stream) {
        if (this.outputStream == null) {
            this.outputStream = stream;
        }
    }

    /* access modifiers changed from: protected */
    public OutputStream getOutputStream() {
        if (getStatus().equals(FileTransfer.Status.negotiated)) {
            return this.outputStream;
        }
        return null;
    }

    public synchronized OutputStream sendFile(String fileName, long fileSize, String description) throws XMPPException {
        if (isDone() || this.outputStream != null) {
            throw new IllegalStateException("The negotation process has already been attempted on this file transfer");
        }
        try {
            this.outputStream = negotiateStream(fileName, fileSize, description);
        } catch (XMPPException e) {
            handleXMPPException(e);
            throw e;
        }
        return this.outputStream;
    }

    public synchronized void sendFile(String fileName, long fileSize, String description, NegotiationProgress progress) {
        if (progress == null) {
            throw new IllegalArgumentException("Callback progress cannot be null.");
        }
        checkTransferThread();
        if (isDone() || this.outputStream != null) {
            throw new IllegalStateException("The negotation process has already been attempted for this file transfer");
        }
        this.callback = progress;
        final String str = fileName;
        final long j = fileSize;
        final String str2 = description;
        final NegotiationProgress negotiationProgress = progress;
        this.transferThread = new Thread(new Runnable() {
            public void run() {
                try {
                    OutgoingFileTransfer.this.outputStream = OutgoingFileTransfer.this.negotiateStream(str, j, str2);
                    negotiationProgress.outputStreamEstablished(OutgoingFileTransfer.this.outputStream);
                } catch (XMPPException e) {
                    OutgoingFileTransfer.this.handleXMPPException(e);
                }
            }
        }, "File Transfer Negotiation " + this.streamID);
        this.transferThread.start();
    }

    private void checkTransferThread() {
        if ((this.transferThread != null && this.transferThread.isAlive()) || isDone()) {
            throw new IllegalStateException("File transfer in progress or has already completed.");
        }
    }

    public synchronized void sendFile(final File file, final String description) throws XMPPException {
        checkTransferThread();
        if (file == null || !file.exists() || !file.canRead()) {
            throw new IllegalArgumentException("Could not read file");
        }
        setFileInfo(file.getAbsolutePath(), file.getName(), file.length());
        this.transferThread = new Thread(new Runnable() {
            /* JADX WARNING: Removed duplicated region for block: B:23:0x0080 A[SYNTHETIC, Splitter:B:23:0x0080] */
            /* JADX WARNING: Removed duplicated region for block: B:31:0x00a7 A[SYNTHETIC, Splitter:B:31:0x00a7] */
            /* JADX WARNING: Removed duplicated region for block: B:37:0x00c2 A[SYNTHETIC, Splitter:B:37:0x00c2] */
            /* JADX WARNING: Unknown top exception splitter block from list: {B:28:0x0099=Splitter:B:28:0x0099, B:20:0x006b=Splitter:B:20:0x006b} */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r9 = this;
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r3 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this     // Catch:{ XMPPException -> 0x0022 }
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r4 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this     // Catch:{ XMPPException -> 0x0022 }
                    java.io.File r5 = r5     // Catch:{ XMPPException -> 0x0022 }
                    java.lang.String r5 = r5.getName()     // Catch:{ XMPPException -> 0x0022 }
                    java.io.File r6 = r5     // Catch:{ XMPPException -> 0x0022 }
                    long r6 = r6.length()     // Catch:{ XMPPException -> 0x0022 }
                    java.lang.String r8 = r6     // Catch:{ XMPPException -> 0x0022 }
                    java.io.OutputStream r4 = r4.negotiateStream(r5, r6, r8)     // Catch:{ XMPPException -> 0x0022 }
                    r3.outputStream = r4     // Catch:{ XMPPException -> 0x0022 }
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r3 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this
                    java.io.OutputStream r3 = r3.outputStream
                    if (r3 != 0) goto L_0x0029
                L_0x0021:
                    return
                L_0x0022:
                    r0 = move-exception
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r3 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this
                    r3.handleXMPPException(r0)
                    goto L_0x0021
                L_0x0029:
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r3 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this
                    org.jivesoftware.smackx.filetransfer.FileTransfer$Status r4 = org.jivesoftware.smackx.filetransfer.FileTransfer.Status.negotiated
                    org.jivesoftware.smackx.filetransfer.FileTransfer$Status r5 = org.jivesoftware.smackx.filetransfer.FileTransfer.Status.in_progress
                    boolean r3 = r3.updateStatus(r4, r5)
                    if (r3 == 0) goto L_0x0021
                    r1 = 0
                    java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x006a, XMPPException -> 0x0098 }
                    java.io.File r3 = r5     // Catch:{ FileNotFoundException -> 0x006a, XMPPException -> 0x0098 }
                    r2.<init>(r3)     // Catch:{ FileNotFoundException -> 0x006a, XMPPException -> 0x0098 }
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r3 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this     // Catch:{ FileNotFoundException -> 0x00e3, XMPPException -> 0x00e0, all -> 0x00dd }
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r4 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this     // Catch:{ FileNotFoundException -> 0x00e3, XMPPException -> 0x00e0, all -> 0x00dd }
                    java.io.OutputStream r4 = r4.outputStream     // Catch:{ FileNotFoundException -> 0x00e3, XMPPException -> 0x00e0, all -> 0x00dd }
                    r3.writeToStream(r2, r4)     // Catch:{ FileNotFoundException -> 0x00e3, XMPPException -> 0x00e0, all -> 0x00dd }
                    if (r2 == 0) goto L_0x004d
                    r2.close()     // Catch:{ IOException -> 0x00d8 }
                L_0x004d:
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r3 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this     // Catch:{ IOException -> 0x00d8 }
                    java.io.OutputStream r3 = r3.outputStream     // Catch:{ IOException -> 0x00d8 }
                    r3.flush()     // Catch:{ IOException -> 0x00d8 }
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r3 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this     // Catch:{ IOException -> 0x00d8 }
                    java.io.OutputStream r3 = r3.outputStream     // Catch:{ IOException -> 0x00d8 }
                    r3.close()     // Catch:{ IOException -> 0x00d8 }
                    r1 = r2
                L_0x0060:
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r3 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this
                    org.jivesoftware.smackx.filetransfer.FileTransfer$Status r4 = org.jivesoftware.smackx.filetransfer.FileTransfer.Status.in_progress
                    org.jivesoftware.smackx.filetransfer.FileTransfer$Status r5 = org.jivesoftware.smackx.filetransfer.FileTransfer.Status.complete
                    r3.updateStatus(r4, r5)
                    goto L_0x0021
                L_0x006a:
                    r0 = move-exception
                L_0x006b:
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r3 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this     // Catch:{ all -> 0x00bf }
                    org.jivesoftware.smackx.filetransfer.FileTransfer$Status r4 = org.jivesoftware.smackx.filetransfer.FileTransfer.Status.error     // Catch:{ all -> 0x00bf }
                    r3.setStatus(r4)     // Catch:{ all -> 0x00bf }
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r3 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this     // Catch:{ all -> 0x00bf }
                    org.jivesoftware.smackx.filetransfer.FileTransfer$Error r4 = org.jivesoftware.smackx.filetransfer.FileTransfer.Error.bad_file     // Catch:{ all -> 0x00bf }
                    r3.setError(r4)     // Catch:{ all -> 0x00bf }
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r3 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this     // Catch:{ all -> 0x00bf }
                    r3.setException(r0)     // Catch:{ all -> 0x00bf }
                    if (r1 == 0) goto L_0x0083
                    r1.close()     // Catch:{ IOException -> 0x0096 }
                L_0x0083:
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r3 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this     // Catch:{ IOException -> 0x0096 }
                    java.io.OutputStream r3 = r3.outputStream     // Catch:{ IOException -> 0x0096 }
                    r3.flush()     // Catch:{ IOException -> 0x0096 }
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r3 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this     // Catch:{ IOException -> 0x0096 }
                    java.io.OutputStream r3 = r3.outputStream     // Catch:{ IOException -> 0x0096 }
                    r3.close()     // Catch:{ IOException -> 0x0096 }
                    goto L_0x0060
                L_0x0096:
                    r3 = move-exception
                    goto L_0x0060
                L_0x0098:
                    r0 = move-exception
                L_0x0099:
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r3 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this     // Catch:{ all -> 0x00bf }
                    org.jivesoftware.smackx.filetransfer.FileTransfer$Status r4 = org.jivesoftware.smackx.filetransfer.FileTransfer.Status.error     // Catch:{ all -> 0x00bf }
                    r3.setStatus(r4)     // Catch:{ all -> 0x00bf }
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r3 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this     // Catch:{ all -> 0x00bf }
                    r3.setException(r0)     // Catch:{ all -> 0x00bf }
                    if (r1 == 0) goto L_0x00aa
                    r1.close()     // Catch:{ IOException -> 0x00bd }
                L_0x00aa:
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r3 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this     // Catch:{ IOException -> 0x00bd }
                    java.io.OutputStream r3 = r3.outputStream     // Catch:{ IOException -> 0x00bd }
                    r3.flush()     // Catch:{ IOException -> 0x00bd }
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r3 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this     // Catch:{ IOException -> 0x00bd }
                    java.io.OutputStream r3 = r3.outputStream     // Catch:{ IOException -> 0x00bd }
                    r3.close()     // Catch:{ IOException -> 0x00bd }
                    goto L_0x0060
                L_0x00bd:
                    r3 = move-exception
                    goto L_0x0060
                L_0x00bf:
                    r3 = move-exception
                L_0x00c0:
                    if (r1 == 0) goto L_0x00c5
                    r1.close()     // Catch:{ IOException -> 0x00db }
                L_0x00c5:
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r4 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this     // Catch:{ IOException -> 0x00db }
                    java.io.OutputStream r4 = r4.outputStream     // Catch:{ IOException -> 0x00db }
                    r4.flush()     // Catch:{ IOException -> 0x00db }
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r4 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this     // Catch:{ IOException -> 0x00db }
                    java.io.OutputStream r4 = r4.outputStream     // Catch:{ IOException -> 0x00db }
                    r4.close()     // Catch:{ IOException -> 0x00db }
                L_0x00d7:
                    throw r3
                L_0x00d8:
                    r3 = move-exception
                    r1 = r2
                    goto L_0x0060
                L_0x00db:
                    r4 = move-exception
                    goto L_0x00d7
                L_0x00dd:
                    r3 = move-exception
                    r1 = r2
                    goto L_0x00c0
                L_0x00e0:
                    r0 = move-exception
                    r1 = r2
                    goto L_0x0099
                L_0x00e3:
                    r0 = move-exception
                    r1 = r2
                    goto L_0x006b
                */
                throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.AnonymousClass2.run():void");
            }
        }, "File Transfer " + this.streamID);
        this.transferThread.start();
    }

    public synchronized void sendStream(InputStream in, String fileName, long fileSize, String description) {
        checkTransferThread();
        final String str = fileName;
        final long j = fileSize;
        final String str2 = description;
        final InputStream inputStream = in;
        this.transferThread = new Thread(new Runnable() {
            public void run() {
                try {
                    OutgoingFileTransfer.this.outputStream = OutgoingFileTransfer.this.negotiateStream(str, j, str2);
                    if (OutgoingFileTransfer.this.outputStream != null && OutgoingFileTransfer.this.updateStatus(FileTransfer.Status.negotiated, FileTransfer.Status.in_progress)) {
                        try {
                            OutgoingFileTransfer.this.writeToStream(inputStream, OutgoingFileTransfer.this.outputStream);
                            try {
                                if (inputStream != null) {
                                    inputStream.close();
                                }
                                OutgoingFileTransfer.this.outputStream.flush();
                                OutgoingFileTransfer.this.outputStream.close();
                            } catch (IOException e) {
                            }
                        } catch (XMPPException e2) {
                            OutgoingFileTransfer.this.setStatus(FileTransfer.Status.error);
                            OutgoingFileTransfer.this.setException(e2);
                            try {
                                if (inputStream != null) {
                                    inputStream.close();
                                }
                                OutgoingFileTransfer.this.outputStream.flush();
                                OutgoingFileTransfer.this.outputStream.close();
                            } catch (IOException e3) {
                            }
                        } catch (Throwable th) {
                            try {
                                if (inputStream != null) {
                                    inputStream.close();
                                }
                                OutgoingFileTransfer.this.outputStream.flush();
                                OutgoingFileTransfer.this.outputStream.close();
                            } catch (IOException e4) {
                            }
                            throw th;
                        }
                        OutgoingFileTransfer.this.updateStatus(FileTransfer.Status.in_progress, FileTransfer.Status.complete);
                    }
                } catch (XMPPException e5) {
                    OutgoingFileTransfer.this.handleXMPPException(e5);
                }
            }
        }, "File Transfer " + this.streamID);
        this.transferThread.start();
    }

    /* access modifiers changed from: private */
    public void handleXMPPException(XMPPException e) {
        XMPPError error = e.getXMPPError();
        if (error != null) {
            int code = error.getCode();
            if (code == 403) {
                setStatus(FileTransfer.Status.refused);
                return;
            } else if (code == 400) {
                setStatus(FileTransfer.Status.error);
                setError(FileTransfer.Error.not_acceptable);
            } else {
                setStatus(FileTransfer.Status.error);
            }
        }
        setException(e);
    }

    public long getBytesSent() {
        return this.amountWritten;
    }

    /* access modifiers changed from: private */
    public OutputStream negotiateStream(String fileName, long fileSize, String description) throws XMPPException {
        if (!updateStatus(FileTransfer.Status.initial, FileTransfer.Status.negotiating_transfer)) {
            throw new XMPPException("Illegal state change");
        }
        StreamNegotiator streamNegotiator = this.negotiator.negotiateOutgoingTransfer(getPeer(), this.streamID, fileName, fileSize, description, RESPONSE_TIMEOUT);
        if (streamNegotiator == null) {
            setStatus(FileTransfer.Status.error);
            setError(FileTransfer.Error.no_response);
            return null;
        } else if (!updateStatus(FileTransfer.Status.negotiating_transfer, FileTransfer.Status.negotiating_stream)) {
            throw new XMPPException("Illegal state change");
        } else {
            this.outputStream = streamNegotiator.createOutgoingStream(this.streamID, this.initiator, getPeer());
            if (updateStatus(FileTransfer.Status.negotiating_stream, FileTransfer.Status.negotiated)) {
                return this.outputStream;
            }
            throw new XMPPException("Illegal state change");
        }
    }

    public void cancel() {
        setStatus(FileTransfer.Status.cancelled);
    }

    /* access modifiers changed from: protected */
    public boolean updateStatus(FileTransfer.Status oldStatus, FileTransfer.Status newStatus) {
        boolean isUpdated = super.updateStatus(oldStatus, newStatus);
        if (this.callback != null && isUpdated) {
            this.callback.statusUpdated(oldStatus, newStatus);
        }
        return isUpdated;
    }

    /* access modifiers changed from: protected */
    public void setStatus(FileTransfer.Status status) {
        FileTransfer.Status oldStatus = getStatus();
        super.setStatus(status);
        if (this.callback != null) {
            this.callback.statusUpdated(oldStatus, status);
        }
    }

    /* access modifiers changed from: protected */
    public void setException(Exception exception) {
        super.setException(exception);
        if (this.callback != null) {
            this.callback.errorEstablishingStream(exception);
        }
    }
}
