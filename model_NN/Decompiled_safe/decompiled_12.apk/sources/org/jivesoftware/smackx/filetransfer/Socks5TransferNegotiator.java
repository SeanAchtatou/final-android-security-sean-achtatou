package org.jivesoftware.smackx.filetransfer;

import com.kenfor.client3g.util.Constant;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PushbackInputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.Iterator;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.PacketCollector;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.AndFilter;
import org.jivesoftware.smack.filter.FromMatchesFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketIDFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.XMPPError;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.filetransfer.Socks5TransferNegotiatorManager;
import org.jivesoftware.smackx.packet.Bytestream;
import org.jivesoftware.smackx.packet.StreamInitiation;

public class Socks5TransferNegotiator extends StreamNegotiator {
    private static final int CONNECT_FAILURE_THRESHOLD = 2;
    protected static final String NAMESPACE = "http://jabber.org/protocol/bytestreams";
    public static boolean isAllowLocalProxyHost = true;
    private final Connection connection;
    private Socks5TransferNegotiatorManager transferNegotiatorManager;

    public Socks5TransferNegotiator(Socks5TransferNegotiatorManager transferNegotiatorManager2, Connection connection2) {
        this.connection = connection2;
        this.transferNegotiatorManager = transferNegotiatorManager2;
    }

    public PacketFilter getInitiationPacketFilter(String from, String sessionID) {
        return new AndFilter(new FromMatchesFilter(from), new BytestreamSIDFilter(sessionID));
    }

    /* access modifiers changed from: package-private */
    public InputStream negotiateIncomingStream(Packet streamInitiation) throws XMPPException {
        Bytestream streamHostsInfo = (Bytestream) streamInitiation;
        if (streamHostsInfo.getType().equals(IQ.Type.ERROR)) {
            throw new XMPPException(streamHostsInfo.getError());
        }
        try {
            SelectedHostInfo selectedHost = selectHost(streamHostsInfo);
            this.connection.sendPacket(createUsedHostConfirmation(selectedHost.selectedHost, streamHostsInfo.getFrom(), streamHostsInfo.getTo(), streamHostsInfo.getPacketID()));
            try {
                PushbackInputStream stream = new PushbackInputStream(selectedHost.establishedSocket.getInputStream());
                stream.unread(stream.read());
                return stream;
            } catch (IOException e) {
                throw new XMPPException("Error establishing input stream", e);
            }
        } catch (XMPPException ex) {
            if (ex.getXMPPError() != null) {
                this.connection.sendPacket(super.createError(streamHostsInfo.getTo(), streamHostsInfo.getFrom(), streamHostsInfo.getPacketID(), ex.getXMPPError()));
            }
            throw ex;
        }
    }

    public InputStream createIncomingStream(StreamInitiation initiation) throws XMPPException {
        return negotiateIncomingStream(initiateIncomingStream(this.connection, initiation));
    }

    private Bytestream createUsedHostConfirmation(Bytestream.StreamHost selectedHost, String initiator, String target, String packetID) {
        Bytestream streamResponse = new Bytestream();
        streamResponse.setTo(initiator);
        streamResponse.setFrom(target);
        streamResponse.setType(IQ.Type.RESULT);
        streamResponse.setPacketID(packetID);
        streamResponse.setUsedHost(selectedHost.getJID());
        return streamResponse;
    }

    private SelectedHostInfo selectHost(Bytestream streamHostsInfo) throws XMPPException {
        Iterator<Bytestream.StreamHost> it = streamHostsInfo.getStreamHosts().iterator();
        Bytestream.StreamHost selectedHost = null;
        Socket socket = null;
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            selectedHost = it.next();
            String address = selectedHost.getAddress();
            if (getConnectionFailures(address) < 2) {
                try {
                    Socket socket2 = new Socket(address, selectedHost.getPort());
                    try {
                        establishSOCKS5ConnectionToProxy(socket2, createDigest(streamHostsInfo.getSessionID(), streamHostsInfo.getFrom(), streamHostsInfo.getTo()));
                        socket = socket2;
                        break;
                    } catch (IOException e) {
                        e = e;
                    }
                } catch (IOException e2) {
                    e = e2;
                    e.printStackTrace();
                    incrementConnectionFailures(address);
                    selectedHost = null;
                    socket = null;
                }
            }
        }
        if (selectedHost != null && socket != null && socket.isConnected()) {
            return new SelectedHostInfo(selectedHost, socket);
        }
        throw new XMPPException("Could not establish socket with any provided host", new XMPPError(XMPPError.Condition.no_acceptable, "Could not establish socket with any provided host"));
    }

    private void incrementConnectionFailures(String address) {
        this.transferNegotiatorManager.incrementConnectionFailures(address);
    }

    private int getConnectionFailures(String address) {
        return this.transferNegotiatorManager.getConnectionFailures(address);
    }

    private String createDigest(String sessionID, String initiator, String target) {
        return StringUtils.hash(String.valueOf(sessionID) + StringUtils.parseName(initiator) + "@" + StringUtils.parseServer(initiator) + "/" + StringUtils.parseResource(initiator) + StringUtils.parseName(target) + "@" + StringUtils.parseServer(target) + "/" + StringUtils.parseResource(target));
    }

    public OutputStream createOutgoingStream(String streamID, String initiator, String target) throws XMPPException {
        try {
            Socket socket = initBytestreamSocket(streamID, initiator, target);
            if (socket == null) {
                return null;
            }
            try {
                return new BufferedOutputStream(socket.getOutputStream());
            } catch (IOException e) {
                throw new XMPPException("Error establishing output stream", e);
            }
        } catch (Exception e2) {
            throw new XMPPException("Error establishing transfer socket", e2);
        }
    }

    private Socket initBytestreamSocket(String sessionID, String initiator, String target) throws Exception {
        Socks5TransferNegotiatorManager.ProxyProcess process;
        String localIP;
        int i;
        try {
            process = establishListeningSocket();
        } catch (IOException e) {
            process = null;
        }
        try {
            localIP = discoverLocalIP();
        } catch (UnknownHostException e2) {
            localIP = null;
        }
        if (process != null) {
            try {
                i = process.getPort();
            } catch (Throwable th) {
                cleanupListeningSocket();
                throw th;
            }
        } else {
            i = 0;
        }
        Socket conn = waitForUsedHostResponse(sessionID, process, createDigest(sessionID, initiator, target), createByteStreamInit(initiator, target, sessionID, localIP, i)).establishedSocket;
        cleanupListeningSocket();
        return conn;
    }

    private SelectedHostInfo waitForUsedHostResponse(String sessionID, Socks5TransferNegotiatorManager.ProxyProcess proxy, String digest, Bytestream query) throws XMPPException, IOException {
        SelectedHostInfo info = new SelectedHostInfo();
        PacketCollector collector = this.connection.createPacketCollector(new PacketIDFilter(query.getPacketID()));
        this.connection.sendPacket(query);
        Packet packet = collector.nextResult(Constant.DELAY_TIME);
        collector.cancel();
        if (packet == null || !(packet instanceof Bytestream)) {
            throw new XMPPException("Unexpected response from remote user");
        }
        Bytestream response = (Bytestream) packet;
        if (response.getType().equals(IQ.Type.ERROR)) {
            throw new XMPPException("Remote client returned error, stream hosts expected", response.getError());
        }
        Bytestream.StreamHostUsed used = response.getUsedHost();
        Bytestream.StreamHost usedHost = query.getStreamHost(used.getJID());
        if (usedHost == null) {
            throw new XMPPException("Remote user responded with unknown host");
        } else if (used.getJID().equals(query.getFrom())) {
            info.establishedSocket = proxy.getSocket(digest);
            info.selectedHost = usedHost;
            return info;
        } else {
            info.establishedSocket = new Socket(usedHost.getAddress(), usedHost.getPort());
            establishSOCKS5ConnectionToProxy(info.establishedSocket, digest);
            Bytestream activate = createByteStreamActivate(sessionID, response.getTo(), usedHost.getJID(), response.getFrom());
            PacketCollector collector2 = this.connection.createPacketCollector(new PacketIDFilter(activate.getPacketID()));
            this.connection.sendPacket(activate);
            collector2.cancel();
            if (((IQ) collector2.nextResult((long) SmackConfiguration.getPacketReplyTimeout())).getType().equals(IQ.Type.RESULT)) {
                return info;
            }
            info.establishedSocket.close();
            return null;
        }
    }

    private Socks5TransferNegotiatorManager.ProxyProcess establishListeningSocket() throws IOException {
        return this.transferNegotiatorManager.addTransfer();
    }

    private void cleanupListeningSocket() {
        this.transferNegotiatorManager.removeTransfer();
    }

    private String discoverLocalIP() throws UnknownHostException {
        return InetAddress.getLocalHost().getHostAddress();
    }

    private Bytestream createByteStreamInit(String from, String to, String sid, String localIP, int port) {
        Bytestream bs = new Bytestream();
        bs.setTo(to);
        bs.setFrom(from);
        bs.setSessionID(sid);
        bs.setType(IQ.Type.SET);
        bs.setMode(Bytestream.Mode.tcp);
        if (localIP != null && port > 0) {
            bs.addStreamHost(from, localIP, port);
        }
        Collection<Bytestream.StreamHost> streamHosts = this.transferNegotiatorManager.getStreamHosts();
        if (streamHosts != null) {
            for (Bytestream.StreamHost host : streamHosts) {
                bs.addStreamHost(host);
            }
        }
        return bs;
    }

    private static Bytestream createByteStreamActivate(String sessionID, String from, String to, String target) {
        Bytestream activate = new Bytestream(sessionID);
        activate.setMode(null);
        activate.setToActivate(target);
        activate.setFrom(from);
        activate.setTo(to);
        activate.setType(IQ.Type.SET);
        return activate;
    }

    public String[] getNamespaces() {
        return new String[]{"http://jabber.org/protocol/bytestreams"};
    }

    private void establishSOCKS5ConnectionToProxy(Socket socket, String digest) throws IOException {
        OutputStream out = new DataOutputStream(socket.getOutputStream());
        out.write(new byte[]{5, 1, 0});
        InputStream in = new DataInputStream(socket.getInputStream());
        in.read(new byte[2]);
        out.write(createOutgoingSocks5Message(1, digest));
        createIncomingSocks5Message(in);
    }

    static String createIncomingSocks5Message(InputStream in) throws IOException {
        byte[] cmd = new byte[5];
        in.read(cmd, 0, 5);
        byte[] addr = new byte[cmd[4]];
        in.read(addr, 0, addr.length);
        String digest = new String(addr);
        in.read();
        in.read();
        return digest;
    }

    static byte[] createOutgoingSocks5Message(int cmd, String digest) {
        byte[] addr = digest.getBytes();
        byte[] data = new byte[(addr.length + 7)];
        data[0] = 5;
        data[1] = (byte) cmd;
        data[2] = 0;
        data[3] = 3;
        data[4] = (byte) addr.length;
        System.arraycopy(addr, 0, data, 5, addr.length);
        data[data.length - 2] = 0;
        data[data.length - 1] = 0;
        return data;
    }

    public void cleanup() {
    }

    private static class SelectedHostInfo {
        protected Socket establishedSocket;
        protected XMPPException exception;
        protected Bytestream.StreamHost selectedHost;

        SelectedHostInfo(Bytestream.StreamHost selectedHost2, Socket establishedSocket2) {
            this.selectedHost = selectedHost2;
            this.establishedSocket = establishedSocket2;
        }

        public SelectedHostInfo() {
        }
    }

    private static class BytestreamSIDFilter implements PacketFilter {
        private String sessionID;

        public BytestreamSIDFilter(String sessionID2) {
            if (sessionID2 == null) {
                throw new IllegalArgumentException("StreamID cannot be null");
            }
            this.sessionID = sessionID2;
        }

        public boolean accept(Packet packet) {
            String sessionID2;
            if (Bytestream.class.isInstance(packet) && (sessionID2 = ((Bytestream) packet).getSessionID()) != null && sessionID2.equals(this.sessionID)) {
                return true;
            }
            return false;
        }
    }
}
