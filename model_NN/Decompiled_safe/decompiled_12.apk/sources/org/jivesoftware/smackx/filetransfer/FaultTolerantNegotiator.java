package org.jivesoftware.smackx.filetransfer;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.Callable;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.PacketCollector;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.OrFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.packet.StreamInitiation;

public class FaultTolerantNegotiator extends StreamNegotiator {
    private Connection connection;
    private PacketFilter primaryFilter;
    private StreamNegotiator primaryNegotiator;
    private PacketFilter secondaryFilter;
    private StreamNegotiator secondaryNegotiator;

    public FaultTolerantNegotiator(Connection connection2, StreamNegotiator primary, StreamNegotiator secondary) {
        this.primaryNegotiator = primary;
        this.secondaryNegotiator = secondary;
        this.connection = connection2;
    }

    public PacketFilter getInitiationPacketFilter(String from, String streamID) {
        if (this.primaryFilter == null || this.secondaryFilter == null) {
            this.primaryFilter = this.primaryNegotiator.getInitiationPacketFilter(from, streamID);
            this.secondaryFilter = this.secondaryNegotiator.getInitiationPacketFilter(from, streamID);
        }
        return new OrFilter(this.primaryFilter, this.secondaryFilter);
    }

    /* access modifiers changed from: package-private */
    public InputStream negotiateIncomingStream(Packet streamInitiation) throws XMPPException {
        throw new UnsupportedOperationException("Negotiation only handled by create incoming stream method.");
    }

    /* access modifiers changed from: package-private */
    public final Packet initiateIncomingStream(Connection connection2, StreamInitiation initiation) {
        throw new UnsupportedOperationException("Initiation handled by createIncomingStream method");
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v17, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: java.io.InputStream} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.io.InputStream createIncomingStream(org.jivesoftware.smackx.packet.StreamInitiation r15) throws org.jivesoftware.smack.XMPPException {
        /*
            r14 = this;
            r13 = 1
            org.jivesoftware.smack.Connection r10 = r14.connection
            java.lang.String r11 = r15.getFrom()
            java.lang.String r12 = r15.getSessionID()
            org.jivesoftware.smack.filter.PacketFilter r11 = r14.getInitiationPacketFilter(r11, r12)
            org.jivesoftware.smack.PacketCollector r1 = r10.createPacketCollector(r11)
            org.jivesoftware.smack.Connection r10 = r14.connection
            java.lang.String[] r11 = r14.getNamespaces()
            org.jivesoftware.smackx.packet.StreamInitiation r11 = super.createInitiationAccept(r15, r11)
            r10.sendPacket(r11)
            java.util.concurrent.ExecutorCompletionService r8 = new java.util.concurrent.ExecutorCompletionService
            r10 = 2
            java.util.concurrent.ExecutorService r10 = java.util.concurrent.Executors.newFixedThreadPool(r10)
            r8.<init>(r10)
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            r9 = 0
            r3 = 0
            org.jivesoftware.smackx.filetransfer.FaultTolerantNegotiator$NegotiatorService r10 = new org.jivesoftware.smackx.filetransfer.FaultTolerantNegotiator$NegotiatorService     // Catch:{ all -> 0x0088 }
            r10.<init>(r1)     // Catch:{ all -> 0x0088 }
            java.util.concurrent.Future r10 = r8.submit(r10)     // Catch:{ all -> 0x0088 }
            r6.add(r10)     // Catch:{ all -> 0x0088 }
            org.jivesoftware.smackx.filetransfer.FaultTolerantNegotiator$NegotiatorService r10 = new org.jivesoftware.smackx.filetransfer.FaultTolerantNegotiator$NegotiatorService     // Catch:{ all -> 0x0088 }
            r10.<init>(r1)     // Catch:{ all -> 0x0088 }
            java.util.concurrent.Future r10 = r8.submit(r10)     // Catch:{ all -> 0x0088 }
            r6.add(r10)     // Catch:{ all -> 0x0088 }
            r7 = 0
            r4 = r3
        L_0x004b:
            if (r9 != 0) goto L_0x0053
            int r10 = r6.size()     // Catch:{ all -> 0x00b4 }
            if (r7 < r10) goto L_0x0065
        L_0x0053:
            java.util.Iterator r10 = r6.iterator()
        L_0x0057:
            boolean r11 = r10.hasNext()
            if (r11 != 0) goto L_0x00a1
            r1.cancel()
            if (r9 != 0) goto L_0x00b3
            if (r4 == 0) goto L_0x00ab
            throw r4
        L_0x0065:
            int r7 = r7 + 1
            r10 = 10
            java.util.concurrent.TimeUnit r12 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ InterruptedException -> 0x007a }
            java.util.concurrent.Future r5 = r8.poll(r10, r12)     // Catch:{ InterruptedException -> 0x007a }
            if (r5 == 0) goto L_0x004b
            java.lang.Object r10 = r5.get()     // Catch:{ InterruptedException -> 0x00b7, ExecutionException -> 0x007c }
            r0 = r10
            java.io.InputStream r0 = (java.io.InputStream) r0     // Catch:{ InterruptedException -> 0x00b7, ExecutionException -> 0x007c }
            r9 = r0
            goto L_0x004b
        L_0x007a:
            r2 = move-exception
            goto L_0x004b
        L_0x007c:
            r2 = move-exception
            org.jivesoftware.smack.XMPPException r3 = new org.jivesoftware.smack.XMPPException     // Catch:{ all -> 0x00b4 }
            java.lang.Throwable r10 = r2.getCause()     // Catch:{ all -> 0x00b4 }
            r3.<init>(r10)     // Catch:{ all -> 0x00b4 }
            r4 = r3
            goto L_0x004b
        L_0x0088:
            r10 = move-exception
        L_0x0089:
            java.util.Iterator r11 = r6.iterator()
        L_0x008d:
            boolean r12 = r11.hasNext()
            if (r12 != 0) goto L_0x0097
            r1.cancel()
            throw r10
        L_0x0097:
            java.lang.Object r5 = r11.next()
            java.util.concurrent.Future r5 = (java.util.concurrent.Future) r5
            r5.cancel(r13)
            goto L_0x008d
        L_0x00a1:
            java.lang.Object r5 = r10.next()
            java.util.concurrent.Future r5 = (java.util.concurrent.Future) r5
            r5.cancel(r13)
            goto L_0x0057
        L_0x00ab:
            org.jivesoftware.smack.XMPPException r10 = new org.jivesoftware.smack.XMPPException
            java.lang.String r11 = "File transfer negotiation failed."
            r10.<init>(r11)
            throw r10
        L_0x00b3:
            return r9
        L_0x00b4:
            r10 = move-exception
            r3 = r4
            goto L_0x0089
        L_0x00b7:
            r10 = move-exception
            goto L_0x004b
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smackx.filetransfer.FaultTolerantNegotiator.createIncomingStream(org.jivesoftware.smackx.packet.StreamInitiation):java.io.InputStream");
    }

    /* access modifiers changed from: private */
    public StreamNegotiator determineNegotiator(Packet streamInitiation) {
        return this.primaryFilter.accept(streamInitiation) ? this.primaryNegotiator : this.secondaryNegotiator;
    }

    public OutputStream createOutgoingStream(String streamID, String initiator, String target) throws XMPPException {
        try {
            return this.primaryNegotiator.createOutgoingStream(streamID, initiator, target);
        } catch (XMPPException e) {
            return this.secondaryNegotiator.createOutgoingStream(streamID, initiator, target);
        }
    }

    public String[] getNamespaces() {
        String[] primary = this.primaryNegotiator.getNamespaces();
        String[] secondary = this.secondaryNegotiator.getNamespaces();
        String[] namespaces = new String[(primary.length + secondary.length)];
        System.arraycopy(primary, 0, namespaces, 0, primary.length);
        System.arraycopy(secondary, 0, namespaces, primary.length, secondary.length);
        return namespaces;
    }

    public void cleanup() {
    }

    private class NegotiatorService implements Callable<InputStream> {
        private PacketCollector collector;

        NegotiatorService(PacketCollector collector2) {
            this.collector = collector2;
        }

        public InputStream call() throws Exception {
            Packet streamInitiation = this.collector.nextResult((long) (SmackConfiguration.getPacketReplyTimeout() * 2));
            if (streamInitiation != null) {
                return FaultTolerantNegotiator.this.determineNegotiator(streamInitiation).negotiateIncomingStream(streamInitiation);
            }
            throw new XMPPException("No response from remote client");
        }
    }
}
