package org.jivesoftware.smackx;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.util.Cache;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.packet.MultipleAddresses;

public class MultipleRecipientManager {
    private static Cache services = new Cache(100, 86400000);

    public static void send(Connection connection, Packet packet, List to, List cc, List bcc) throws XMPPException {
        send(connection, packet, to, cc, bcc, null, null, false);
    }

    public static void send(Connection connection, Packet packet, List to, List cc, List bcc, String replyTo, String replyRoom, boolean noReply) throws XMPPException {
        String serviceAddress = getMultipleRecipienServiceAddress(connection);
        if (serviceAddress != null) {
            sendThroughService(connection, packet, to, cc, bcc, replyTo, replyRoom, noReply, serviceAddress);
        } else if (noReply || ((replyTo != null && replyTo.trim().length() > 0) || (replyRoom != null && replyRoom.trim().length() > 0))) {
            throw new XMPPException("Extended Stanza Addressing not supported by server");
        } else {
            sendToIndividualRecipients(connection, packet, to, cc, bcc);
        }
    }

    public static void reply(Connection connection, Message original, Message reply) throws XMPPException {
        MultipleRecipientInfo info = getMultipleRecipientInfo(original);
        if (info == null) {
            throw new XMPPException("Original message does not contain multiple recipient info");
        } else if (info.shouldNotReply()) {
            throw new XMPPException("Original message should not be replied");
        } else if (info.getReplyRoom() != null) {
            throw new XMPPException("Reply should be sent through a room");
        } else {
            if (original.getThread() != null) {
                reply.setThread(original.getThread());
            }
            MultipleAddresses.Address replyAddress = info.getReplyAddress();
            if (replyAddress == null || replyAddress.getJid() == null) {
                List to = new ArrayList();
                List cc = new ArrayList();
                for (MultipleAddresses.Address jid : info.getTOAddresses()) {
                    to.add(jid.getJid());
                }
                for (MultipleAddresses.Address jid2 : info.getCCAddresses()) {
                    cc.add(jid2.getJid());
                }
                if (!to.contains(original.getFrom()) && !cc.contains(original.getFrom())) {
                    to.add(original.getFrom());
                }
                String from = connection.getUser();
                if (!to.remove(from) && !cc.remove(from)) {
                    String bareJID = StringUtils.parseBareAddress(from);
                    to.remove(bareJID);
                    cc.remove(bareJID);
                }
                String serviceAddress = getMultipleRecipienServiceAddress(connection);
                if (serviceAddress != null) {
                    sendThroughService(connection, reply, to, cc, null, null, null, false, serviceAddress);
                } else {
                    sendToIndividualRecipients(connection, reply, to, cc, null);
                }
            } else {
                reply.setTo(replyAddress.getJid());
                connection.sendPacket(reply);
            }
        }
    }

    public static MultipleRecipientInfo getMultipleRecipientInfo(Packet packet) {
        MultipleAddresses extension = (MultipleAddresses) packet.getExtension("addresses", "http://jabber.org/protocol/address");
        if (extension == null) {
            return null;
        }
        return new MultipleRecipientInfo(extension);
    }

    private static void sendToIndividualRecipients(Connection connection, Packet packet, List to, List cc, List bcc) {
        if (to != null) {
            Iterator it = to.iterator();
            while (it.hasNext()) {
                packet.setTo((String) it.next());
                connection.sendPacket(new PacketCopy(packet.toXML()));
            }
        }
        if (cc != null) {
            Iterator it2 = cc.iterator();
            while (it2.hasNext()) {
                packet.setTo((String) it2.next());
                connection.sendPacket(new PacketCopy(packet.toXML()));
            }
        }
        if (bcc != null) {
            Iterator it3 = bcc.iterator();
            while (it3.hasNext()) {
                packet.setTo((String) it3.next());
                connection.sendPacket(new PacketCopy(packet.toXML()));
            }
        }
    }

    private static void sendThroughService(Connection connection, Packet packet, List to, List cc, List bcc, String replyTo, String replyRoom, boolean noReply, String serviceAddress) {
        MultipleAddresses multipleAddresses = new MultipleAddresses();
        if (to != null) {
            Iterator it = to.iterator();
            while (it.hasNext()) {
                multipleAddresses.addAddress("to", (String) it.next(), null, null, false, null);
            }
        }
        if (cc != null) {
            Iterator it2 = cc.iterator();
            while (it2.hasNext()) {
                multipleAddresses.addAddress(MultipleAddresses.CC, (String) it2.next(), null, null, false, null);
            }
        }
        if (bcc != null) {
            Iterator it3 = bcc.iterator();
            while (it3.hasNext()) {
                multipleAddresses.addAddress(MultipleAddresses.BCC, (String) it3.next(), null, null, false, null);
            }
        }
        if (noReply) {
            multipleAddresses.setNoReply();
        } else {
            if (replyTo != null && replyTo.trim().length() > 0) {
                multipleAddresses.addAddress(MultipleAddresses.REPLY_TO, replyTo, null, null, false, null);
            }
            if (replyRoom != null && replyRoom.trim().length() > 0) {
                multipleAddresses.addAddress(MultipleAddresses.REPLY_ROOM, replyRoom, null, null, false, null);
            }
        }
        packet.setTo(serviceAddress);
        packet.addExtension(multipleAddresses);
        connection.sendPacket(packet);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v5, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: java.lang.String} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String getMultipleRecipienServiceAddress(org.jivesoftware.smack.Connection r12) {
        /*
            java.lang.String r7 = r12.getServiceName()
            org.jivesoftware.smack.util.Cache r8 = org.jivesoftware.smackx.MultipleRecipientManager.services
            java.lang.Object r6 = r8.get(r7)
            java.lang.String r6 = (java.lang.String) r6
            if (r6 != 0) goto L_0x0038
            org.jivesoftware.smack.util.Cache r9 = org.jivesoftware.smackx.MultipleRecipientManager.services
            monitor-enter(r9)
            org.jivesoftware.smack.util.Cache r8 = org.jivesoftware.smackx.MultipleRecipientManager.services     // Catch:{ all -> 0x007b }
            java.lang.Object r8 = r8.get(r7)     // Catch:{ all -> 0x007b }
            r0 = r8
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x007b }
            r6 = r0
            if (r6 != 0) goto L_0x0037
            org.jivesoftware.smackx.ServiceDiscoveryManager r8 = org.jivesoftware.smackx.ServiceDiscoveryManager.getInstanceFor(r12)     // Catch:{ XMPPException -> 0x0076 }
            org.jivesoftware.smackx.packet.DiscoverInfo r2 = r8.discoverInfo(r7)     // Catch:{ XMPPException -> 0x0076 }
            java.lang.String r8 = "http://jabber.org/protocol/address"
            boolean r8 = r2.containsFeature(r8)     // Catch:{ XMPPException -> 0x0076 }
            if (r8 == 0) goto L_0x0042
            r6 = r7
        L_0x002e:
            org.jivesoftware.smack.util.Cache r10 = org.jivesoftware.smackx.MultipleRecipientManager.services     // Catch:{ XMPPException -> 0x0076 }
            if (r6 != 0) goto L_0x0074
            java.lang.String r8 = ""
        L_0x0034:
            r10.put(r7, r8)     // Catch:{ XMPPException -> 0x0076 }
        L_0x0037:
            monitor-exit(r9)     // Catch:{ all -> 0x007b }
        L_0x0038:
            java.lang.String r8 = ""
            boolean r8 = r8.equals(r6)
            if (r8 == 0) goto L_0x0041
            r6 = 0
        L_0x0041:
            return r6
        L_0x0042:
            org.jivesoftware.smackx.ServiceDiscoveryManager r8 = org.jivesoftware.smackx.ServiceDiscoveryManager.getInstanceFor(r12)     // Catch:{ XMPPException -> 0x0076 }
            org.jivesoftware.smackx.packet.DiscoverItems r5 = r8.discoverItems(r7)     // Catch:{ XMPPException -> 0x0076 }
            java.util.Iterator r3 = r5.getItems()     // Catch:{ XMPPException -> 0x0076 }
        L_0x004e:
            boolean r8 = r3.hasNext()     // Catch:{ XMPPException -> 0x0076 }
            if (r8 == 0) goto L_0x002e
            java.lang.Object r4 = r3.next()     // Catch:{ XMPPException -> 0x0076 }
            org.jivesoftware.smackx.packet.DiscoverItems$Item r4 = (org.jivesoftware.smackx.packet.DiscoverItems.Item) r4     // Catch:{ XMPPException -> 0x0076 }
            org.jivesoftware.smackx.ServiceDiscoveryManager r8 = org.jivesoftware.smackx.ServiceDiscoveryManager.getInstanceFor(r12)     // Catch:{ XMPPException -> 0x0076 }
            java.lang.String r10 = r4.getEntityID()     // Catch:{ XMPPException -> 0x0076 }
            java.lang.String r11 = r4.getNode()     // Catch:{ XMPPException -> 0x0076 }
            org.jivesoftware.smackx.packet.DiscoverInfo r2 = r8.discoverInfo(r10, r11)     // Catch:{ XMPPException -> 0x0076 }
            java.lang.String r8 = "http://jabber.org/protocol/address"
            boolean r8 = r2.containsFeature(r8)     // Catch:{ XMPPException -> 0x0076 }
            if (r8 == 0) goto L_0x004e
            r6 = r7
            goto L_0x002e
        L_0x0074:
            r8 = r6
            goto L_0x0034
        L_0x0076:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x007b }
            goto L_0x0037
        L_0x007b:
            r8 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x007b }
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smackx.MultipleRecipientManager.getMultipleRecipienServiceAddress(org.jivesoftware.smack.Connection):java.lang.String");
    }

    private static class PacketCopy extends Packet {
        private String text;

        public PacketCopy(String text2) {
            this.text = text2;
        }

        public String toXML() {
            return this.text;
        }
    }
}
