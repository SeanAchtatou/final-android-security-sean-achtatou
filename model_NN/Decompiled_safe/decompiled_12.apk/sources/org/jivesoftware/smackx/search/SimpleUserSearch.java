package org.jivesoftware.smackx.search;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smackx.Form;
import org.jivesoftware.smackx.FormField;
import org.jivesoftware.smackx.ReportedData;
import org.xmlpull.v1.XmlPullParser;

class SimpleUserSearch extends IQ {
    private ReportedData data;
    private Form form;

    SimpleUserSearch() {
    }

    public void setForm(Form form2) {
        this.form = form2;
    }

    public ReportedData getReportedData() {
        return this.data;
    }

    public String getChildElementXML() {
        return "<query xmlns=\"jabber:iq:search\">" + getItemsToSearch() + "</query>";
    }

    private String getItemsToSearch() {
        StringBuilder buf = new StringBuilder();
        if (this.form == null) {
            this.form = Form.getFormFrom(this);
        }
        if (this.form == null) {
            return "";
        }
        Iterator<FormField> fields = this.form.getFields();
        while (fields.hasNext()) {
            FormField field = fields.next();
            String name = field.getVariable();
            String value = getSingleValue(field);
            if (value.trim().length() > 0) {
                buf.append("<").append(name).append(">").append(value).append("</").append(name).append(">");
            }
        }
        return buf.toString();
    }

    private static String getSingleValue(FormField formField) {
        Iterator<String> values = formField.getValues();
        if (values.hasNext()) {
            return values.next();
        }
        return "";
    }

    /* access modifiers changed from: protected */
    public void parseItems(XmlPullParser parser) throws Exception {
        ReportedData data2 = new ReportedData();
        data2.addColumn(new ReportedData.Column("JID", "jid", FormField.TYPE_TEXT_SINGLE));
        boolean done = false;
        List<ReportedData.Field> fields = new ArrayList<>();
        while (!done) {
            if (parser.getAttributeCount() > 0) {
                String jid = parser.getAttributeValue("", "jid");
                List<String> valueList = new ArrayList<>();
                valueList.add(jid);
                fields.add(new ReportedData.Field("jid", valueList));
            }
            int eventType = parser.next();
            if (eventType == 2 && parser.getName().equals("item")) {
                fields = new ArrayList<>();
            } else if (eventType == 3 && parser.getName().equals("item")) {
                data2.addRow(new ReportedData.Row(fields));
            } else if (eventType == 2) {
                String name = parser.getName();
                String value = parser.nextText();
                List<String> valueList2 = new ArrayList<>();
                valueList2.add(value);
                fields.add(new ReportedData.Field(name, valueList2));
                boolean exists = false;
                Iterator cols = data2.getColumns();
                while (cols.hasNext()) {
                    if (cols.next().getVariable().equals(name)) {
                        exists = true;
                    }
                }
                if (!exists) {
                    data2.addColumn(new ReportedData.Column(name, name, FormField.TYPE_TEXT_SINGLE));
                }
            } else if (eventType == 3 && parser.getName().equals("query")) {
                done = true;
            }
        }
        this.data = data2;
    }
}
