package org.jivesoftware.smackx;

import com.kenfor.client3g.util.Constant;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.PacketCollector;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.PacketIDFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Registration;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.packet.DiscoverInfo;

public class Gateway {
    /* access modifiers changed from: private */
    public Connection connection;
    /* access modifiers changed from: private */
    public String entityJID;
    private DiscoverInfo.Identity identity;
    private DiscoverInfo info;
    private Registration registerInfo;
    /* access modifiers changed from: private */
    public Roster roster;
    private ServiceDiscoveryManager sdManager;

    Gateway(Connection connection2, String entityJID2) {
        this.connection = connection2;
        this.roster = connection2.getRoster();
        this.sdManager = ServiceDiscoveryManager.getInstanceFor(connection2);
        this.entityJID = entityJID2;
    }

    Gateway(Connection connection2, String entityJID2, DiscoverInfo info2, DiscoverInfo.Identity identity2) {
        this(connection2, entityJID2);
        this.info = info2;
        this.identity = identity2;
    }

    private void discoverInfo() throws XMPPException {
        this.info = this.sdManager.discoverInfo(this.entityJID);
        Iterator<DiscoverInfo.Identity> iterator = this.info.getIdentities();
        while (iterator.hasNext()) {
            DiscoverInfo.Identity temp = iterator.next();
            if (temp.getCategory().equalsIgnoreCase("gateway")) {
                this.identity = temp;
                return;
            }
        }
    }

    private DiscoverInfo.Identity getIdentity() throws XMPPException {
        if (this.identity == null) {
            discoverInfo();
        }
        return this.identity;
    }

    private Registration getRegisterInfo() {
        if (this.registerInfo == null) {
            refreshRegisterInfo();
        }
        return this.registerInfo;
    }

    private void refreshRegisterInfo() {
        Registration packet = new Registration();
        packet.setFrom(this.connection.getUser());
        packet.setType(IQ.Type.GET);
        packet.setTo(this.entityJID);
        PacketCollector collector = this.connection.createPacketCollector(new PacketIDFilter(packet.getPacketID()));
        this.connection.sendPacket(packet);
        Packet result = collector.nextResult((long) SmackConfiguration.getPacketReplyTimeout());
        collector.cancel();
        if ((result instanceof Registration) && result.getError() == null) {
            this.registerInfo = (Registration) result;
        }
    }

    public boolean canRegister() throws XMPPException {
        if (this.info == null) {
            discoverInfo();
        }
        return this.info.containsFeature("jabber:iq:register");
    }

    public List<String> getRequiredFields() {
        return getRegisterInfo().getRequiredFields();
    }

    public String getName() throws XMPPException {
        if (this.identity == null) {
            discoverInfo();
        }
        return this.identity.getName();
    }

    public String getType() throws XMPPException {
        if (this.identity == null) {
            discoverInfo();
        }
        return this.identity.getType();
    }

    public boolean isRegistered() throws XMPPException {
        return getRegisterInfo().isRegistered();
    }

    public String getField(String fieldName) {
        return getRegisterInfo().getField(fieldName);
    }

    public List<String> getFieldNames() {
        return getRegisterInfo().getFieldNames();
    }

    public String getUsername() {
        return getField("username");
    }

    public String getPassword() {
        return getField(Constant.PASSWORD);
    }

    public String getInstructions() {
        return getRegisterInfo().getInstructions();
    }

    public void register(String username, String password, Map<String, String> fields) throws XMPPException {
        if (getRegisterInfo().isRegistered()) {
            throw new IllegalStateException("You are already registered with this gateway");
        }
        Registration register = new Registration();
        register.setFrom(this.connection.getUser());
        register.setTo(this.entityJID);
        register.setType(IQ.Type.SET);
        register.setUsername(username);
        register.setPassword(password);
        for (String s : fields.keySet()) {
            register.addAttribute(s, fields.get(s));
        }
        PacketCollector resultCollector = this.connection.createPacketCollector(new PacketIDFilter(register.getPacketID()));
        this.connection.sendPacket(register);
        Packet result = resultCollector.nextResult((long) SmackConfiguration.getPacketReplyTimeout());
        resultCollector.cancel();
        if (result == null || !(result instanceof IQ)) {
            throw new XMPPException("Packet reply timeout");
        }
        IQ resultIQ = (IQ) result;
        if (resultIQ.getError() != null) {
            throw new XMPPException(resultIQ.getError());
        } else if (resultIQ.getType() == IQ.Type.ERROR) {
            throw new XMPPException(resultIQ.getError());
        } else {
            this.connection.addPacketListener(new GatewayPresenceListener(this, null), new PacketTypeFilter(Presence.class));
            this.roster.createEntry(this.entityJID, getIdentity().getName(), new String[0]);
        }
    }

    public void register(String username, String password) throws XMPPException {
        register(username, password, new HashMap());
    }

    public void unregister() throws XMPPException {
        Registration register = new Registration();
        register.setFrom(this.connection.getUser());
        register.setTo(this.entityJID);
        register.setType(IQ.Type.SET);
        register.setRemove(true);
        PacketCollector resultCollector = this.connection.createPacketCollector(new PacketIDFilter(register.getPacketID()));
        this.connection.sendPacket(register);
        Packet result = resultCollector.nextResult((long) SmackConfiguration.getPacketReplyTimeout());
        resultCollector.cancel();
        if (result == null || !(result instanceof IQ)) {
            throw new XMPPException("Packet reply timeout");
        }
        IQ resultIQ = (IQ) result;
        if (resultIQ.getError() != null) {
            throw new XMPPException(resultIQ.getError());
        } else if (resultIQ.getType() == IQ.Type.ERROR) {
            throw new XMPPException(resultIQ.getError());
        } else {
            this.roster.removeEntry(this.roster.getEntry(this.entityJID));
        }
    }

    public void login() {
        login(new Presence(Presence.Type.available));
    }

    public void login(Presence presence) {
        presence.setType(Presence.Type.available);
        presence.setTo(this.entityJID);
        presence.setFrom(this.connection.getUser());
        this.connection.sendPacket(presence);
    }

    public void logout() {
        Presence presence = new Presence(Presence.Type.unavailable);
        presence.setTo(this.entityJID);
        presence.setFrom(this.connection.getUser());
        this.connection.sendPacket(presence);
    }

    private class GatewayPresenceListener implements PacketListener {
        private GatewayPresenceListener() {
        }

        /* synthetic */ GatewayPresenceListener(Gateway gateway, GatewayPresenceListener gatewayPresenceListener) {
            this();
        }

        public void processPacket(Packet packet) {
            if (packet instanceof Presence) {
                Presence presence = (Presence) packet;
                if (Gateway.this.entityJID.equals(presence.getFrom()) && Gateway.this.roster.contains(presence.getFrom()) && presence.getType().equals(Presence.Type.subscribe)) {
                    Presence response = new Presence(Presence.Type.subscribed);
                    response.setTo(presence.getFrom());
                    response.setFrom(StringUtils.parseBareAddress(Gateway.this.connection.getUser()));
                    Gateway.this.connection.sendPacket(response);
                }
            }
        }
    }
}
