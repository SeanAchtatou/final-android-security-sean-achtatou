package org.jivesoftware.smackx.bookmark;

public class BookmarkedConference implements SharedBookmark {
    private boolean autoJoin;
    private boolean isShared;
    private final String jid;
    private String name;
    private String nickname;
    private String password;

    protected BookmarkedConference(String jid2) {
        this.jid = jid2;
    }

    protected BookmarkedConference(String name2, String jid2, boolean autoJoin2, String nickname2, String password2) {
        this.name = name2;
        this.jid = jid2;
        this.autoJoin = autoJoin2;
        this.nickname = nickname2;
        this.password = password2;
    }

    public String getName() {
        return this.name;
    }

    /* access modifiers changed from: protected */
    public void setName(String name2) {
        this.name = name2;
    }

    public boolean isAutoJoin() {
        return this.autoJoin;
    }

    /* access modifiers changed from: protected */
    public void setAutoJoin(boolean autoJoin2) {
        this.autoJoin = autoJoin2;
    }

    public String getJid() {
        return this.jid;
    }

    public String getNickname() {
        return this.nickname;
    }

    /* access modifiers changed from: protected */
    public void setNickname(String nickname2) {
        this.nickname = nickname2;
    }

    public String getPassword() {
        return this.password;
    }

    /* access modifiers changed from: protected */
    public void setPassword(String password2) {
        this.password = password2;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof BookmarkedConference)) {
            return false;
        }
        return ((BookmarkedConference) obj).getJid().equalsIgnoreCase(this.jid);
    }

    /* access modifiers changed from: protected */
    public void setShared(boolean isShared2) {
        this.isShared = isShared2;
    }

    public boolean isShared() {
        return this.isShared;
    }
}
