package org.jivesoftware.smackx;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.filter.AndFilter;
import org.jivesoftware.smack.filter.PacketExtensionFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smack.util.Base64;
import org.jivesoftware.smackx.packet.CapsExtension;
import org.jivesoftware.smackx.packet.DataForm;
import org.jivesoftware.smackx.packet.DiscoverInfo;
import org.jivesoftware.smackx.provider.CapsExtensionProvider;

public class EntityCapsManager {
    public static final String HASH_METHOD = "sha-1";
    public static final String HASH_METHOD_CAPS = "SHA-1";
    private static Map<String, DiscoverInfo> caps = new ConcurrentHashMap();
    private static String entityNode = "http://www.igniterealtime.org/projects/smack/";
    private Set<CapsVerListener> capsVerListeners = new CopyOnWriteArraySet();
    private String currentCapsVersion = null;
    private Map<String, String> userCaps = new ConcurrentHashMap();

    static {
        ProviderManager.getInstance().addExtensionProvider(CapsExtension.NODE_NAME, CapsExtension.XMLNS, new CapsExtensionProvider());
    }

    public static void addDiscoverInfoByNode(String node, DiscoverInfo info) {
        cleanupDicsoverInfo(info);
        caps.put(node, info);
    }

    public void addUserCapsNode(String user, String node) {
        if (user != null && node != null) {
            this.userCaps.put(user, node);
        }
    }

    public void removeUserCapsNode(String user) {
        this.userCaps.remove(user);
    }

    public String getNodeVersionByUser(String user) {
        return this.userCaps.get(user);
    }

    public DiscoverInfo getDiscoverInfoByUser(String user) {
        String capsNode = this.userCaps.get(user);
        if (capsNode == null) {
            return null;
        }
        return getDiscoverInfoByNode(capsNode);
    }

    public String getCapsVersion() {
        return this.currentCapsVersion;
    }

    public String getNode() {
        return entityNode;
    }

    public void setNode(String node) {
        entityNode = node;
    }

    public static DiscoverInfo getDiscoverInfoByNode(String node) {
        return caps.get(node);
    }

    private static void cleanupDicsoverInfo(DiscoverInfo info) {
        info.setFrom(null);
        info.setTo(null);
        info.setPacketID(null);
    }

    public void addPacketListener(Connection connection) {
        connection.addPacketListener(new CapsPacketListener(), new AndFilter(new PacketTypeFilter(Presence.class), new PacketExtensionFilter(CapsExtension.NODE_NAME, CapsExtension.XMLNS)));
    }

    public void addCapsVerListener(CapsVerListener listener) {
        this.capsVerListeners.add(listener);
        if (this.currentCapsVersion != null) {
            listener.capsVerUpdated(this.currentCapsVersion);
        }
    }

    public void removeCapsVerListener(CapsVerListener listener) {
        this.capsVerListeners.remove(listener);
    }

    private void notifyCapsVerListeners() {
        for (CapsVerListener listener : this.capsVerListeners) {
            listener.capsVerUpdated(this.currentCapsVersion);
        }
    }

    private static String capsToHash(String capsString) {
        try {
            return Base64.encodeBytes(MessageDigest.getInstance(HASH_METHOD_CAPS).digest(capsString.getBytes()));
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    private static String formFieldValuesToCaps(Iterator<String> i) {
        String s = "";
        SortedSet<String> fvs = new TreeSet<>();
        while (i.hasNext()) {
            fvs.add(i.next());
        }
        for (String fv : fvs) {
            s = String.valueOf(s) + fv + "<";
        }
        return s;
    }

    /* access modifiers changed from: package-private */
    public void calculateEntityCapsVersion(DiscoverInfo discoverInfo, String identityType, String identityName, List<String> features, DataForm extendedInfo) {
        String s = String.valueOf("") + "client/" + identityType + "//" + identityName + "<";
        synchronized (features) {
            SortedSet<String> fs = new TreeSet<>();
            for (String f : features) {
                fs.add(f);
            }
            for (String f2 : fs) {
                s = String.valueOf(s) + f2 + "<";
            }
        }
        if (extendedInfo != null) {
            synchronized (extendedInfo) {
                SortedSet<FormField> fs2 = new TreeSet<>(new Comparator<FormField>() {
                    public int compare(FormField f1, FormField f2) {
                        return f1.getVariable().compareTo(f2.getVariable());
                    }
                });
                FormField ft = null;
                Iterator<FormField> i = extendedInfo.getFields();
                while (i.hasNext()) {
                    FormField f3 = i.next();
                    if (!f3.getVariable().equals("FORM_TYPE")) {
                        fs2.add(f3);
                    } else {
                        ft = f3;
                    }
                }
                if (ft != null) {
                    s = String.valueOf(s) + formFieldValuesToCaps(ft.getValues());
                }
                for (FormField f4 : fs2) {
                    s = String.valueOf(String.valueOf(s) + f4.getVariable() + "<") + formFieldValuesToCaps(f4.getValues());
                }
            }
        }
        setCurrentCapsVersion(discoverInfo, capsToHash(s));
    }

    public void setCurrentCapsVersion(DiscoverInfo discoverInfo, String capsVersion) {
        this.currentCapsVersion = capsVersion;
        addDiscoverInfoByNode(String.valueOf(getNode()) + "#" + capsVersion, discoverInfo);
        notifyCapsVerListeners();
    }

    class CapsPacketListener implements PacketListener {
        CapsPacketListener() {
        }

        public void processPacket(Packet packet) {
            CapsExtension ext = (CapsExtension) packet.getExtension(CapsExtension.NODE_NAME, CapsExtension.XMLNS);
            EntityCapsManager.this.addUserCapsNode(packet.getFrom(), String.valueOf(ext.getNode()) + "#" + ext.getVersion());
        }
    }
}
