package org.jivesoftware.smackx.muc;

import java.util.Date;
import org.jivesoftware.smackx.packet.MUCInitialPresence;

public class DiscussionHistory {
    private int maxChars = -1;
    private int maxStanzas = -1;
    private int seconds = -1;
    private Date since;

    public int getMaxChars() {
        return this.maxChars;
    }

    public int getMaxStanzas() {
        return this.maxStanzas;
    }

    public int getSeconds() {
        return this.seconds;
    }

    public Date getSince() {
        return this.since;
    }

    public void setMaxChars(int maxChars2) {
        this.maxChars = maxChars2;
    }

    public void setMaxStanzas(int maxStanzas2) {
        this.maxStanzas = maxStanzas2;
    }

    public void setSeconds(int seconds2) {
        this.seconds = seconds2;
    }

    public void setSince(Date since2) {
        this.since = since2;
    }

    private boolean isConfigured() {
        return this.maxChars > -1 || this.maxStanzas > -1 || this.seconds > -1 || this.since != null;
    }

    /* access modifiers changed from: package-private */
    public MUCInitialPresence.History getMUCHistory() {
        if (!isConfigured()) {
            return null;
        }
        MUCInitialPresence.History mucHistory = new MUCInitialPresence.History();
        if (this.maxChars > -1) {
            mucHistory.setMaxChars(this.maxChars);
        }
        if (this.maxStanzas > -1) {
            mucHistory.setMaxStanzas(this.maxStanzas);
        }
        if (this.seconds > -1) {
            mucHistory.setSeconds(this.seconds);
        }
        if (this.since == null) {
            return mucHistory;
        }
        mucHistory.setSince(this.since);
        return mucHistory;
    }
}
