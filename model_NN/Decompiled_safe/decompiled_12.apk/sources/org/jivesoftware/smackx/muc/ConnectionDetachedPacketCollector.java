package org.jivesoftware.smackx.muc;

import java.util.LinkedList;
import org.jivesoftware.smack.packet.Packet;

class ConnectionDetachedPacketCollector {
    private static final int MAX_PACKETS = 65536;
    private LinkedList<Packet> resultQueue = new LinkedList<>();

    public synchronized Packet pollResult() {
        Packet removeLast;
        if (this.resultQueue.isEmpty()) {
            removeLast = null;
        } else {
            removeLast = this.resultQueue.removeLast();
        }
        return removeLast;
    }

    public synchronized Packet nextResult() {
        while (this.resultQueue.isEmpty()) {
            try {
                wait();
            } catch (InterruptedException e) {
            }
        }
        return this.resultQueue.removeLast();
    }

    public synchronized Packet nextResult(long timeout) {
        Packet removeLast;
        if (this.resultQueue.isEmpty()) {
            try {
                wait(timeout);
            } catch (InterruptedException e) {
            }
        }
        if (this.resultQueue.isEmpty()) {
            removeLast = null;
        } else {
            removeLast = this.resultQueue.removeLast();
        }
        return removeLast;
    }

    /* access modifiers changed from: protected */
    public synchronized void processPacket(Packet packet) {
        if (packet != null) {
            if (this.resultQueue.size() == MAX_PACKETS) {
                this.resultQueue.removeLast();
            }
            this.resultQueue.addFirst(packet);
            notifyAll();
        }
    }
}
