package org.jivesoftware.smackx.muc;

import org.jivesoftware.smack.PacketInterceptor;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smackx.GroupChatInvitation;

public class DeafOccupantInterceptor implements PacketInterceptor {
    public void interceptPacket(Packet packet) {
        Presence presence = (Presence) packet;
        if (Presence.Type.available == presence.getType() && presence.getExtension(GroupChatInvitation.ELEMENT_NAME, "http://jabber.org/protocol/muc") != null) {
            packet.addExtension(new DeafExtension(null));
        }
    }

    private static class DeafExtension implements PacketExtension {
        private DeafExtension() {
        }

        /* synthetic */ DeafExtension(DeafExtension deafExtension) {
            this();
        }

        public String getElementName() {
            return GroupChatInvitation.ELEMENT_NAME;
        }

        public String getNamespace() {
            return "http://jivesoftware.org/protocol/muc";
        }

        public String toXML() {
            StringBuilder buf = new StringBuilder();
            buf.append("<").append(getElementName()).append(" xmlns=\"").append(getNamespace()).append("\">");
            buf.append("<deaf-occupant/>");
            buf.append("</").append(getElementName()).append(">");
            return buf.toString();
        }
    }
}
