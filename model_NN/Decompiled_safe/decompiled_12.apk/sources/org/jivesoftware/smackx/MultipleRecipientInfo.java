package org.jivesoftware.smackx;

import java.util.List;
import org.jivesoftware.smackx.packet.MultipleAddresses;

public class MultipleRecipientInfo {
    MultipleAddresses extension;

    MultipleRecipientInfo(MultipleAddresses extension2) {
        this.extension = extension2;
    }

    public List getTOAddresses() {
        return this.extension.getAddressesOfType("to");
    }

    public List getCCAddresses() {
        return this.extension.getAddressesOfType(MultipleAddresses.CC);
    }

    public String getReplyRoom() {
        List replyRoom = this.extension.getAddressesOfType(MultipleAddresses.REPLY_ROOM);
        if (replyRoom.isEmpty()) {
            return null;
        }
        return ((MultipleAddresses.Address) replyRoom.get(0)).getJid();
    }

    public boolean shouldNotReply() {
        return !this.extension.getAddressesOfType(MultipleAddresses.NO_REPLY).isEmpty();
    }

    public MultipleAddresses.Address getReplyAddress() {
        List replyTo = this.extension.getAddressesOfType(MultipleAddresses.REPLY_TO);
        if (replyTo.isEmpty()) {
            return null;
        }
        return (MultipleAddresses.Address) replyTo.get(0);
    }
}
