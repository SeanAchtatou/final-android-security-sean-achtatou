package org.jivesoftware.smackx.workgroup.ext.history;

import java.util.Date;

public class AgentChatSession {
    public long duration;
    public String question;
    public String sessionID;
    public Date startDate;
    public String visitorsEmail;
    public String visitorsName;

    public AgentChatSession(Date date, long duration2, String visitorsName2, String visitorsEmail2, String sessionID2, String question2) {
        this.startDate = date;
        this.duration = duration2;
        this.visitorsName = visitorsName2;
        this.visitorsEmail = visitorsEmail2;
        this.sessionID = sessionID2;
        this.question = question2;
    }

    public Date getStartDate() {
        return this.startDate;
    }

    public void setStartDate(Date startDate2) {
        this.startDate = startDate2;
    }

    public long getDuration() {
        return this.duration;
    }

    public void setDuration(long duration2) {
        this.duration = duration2;
    }

    public String getVisitorsName() {
        return this.visitorsName;
    }

    public void setVisitorsName(String visitorsName2) {
        this.visitorsName = visitorsName2;
    }

    public String getVisitorsEmail() {
        return this.visitorsEmail;
    }

    public void setVisitorsEmail(String visitorsEmail2) {
        this.visitorsEmail = visitorsEmail2;
    }

    public String getSessionID() {
        return this.sessionID;
    }

    public void setSessionID(String sessionID2) {
        this.sessionID = sessionID2;
    }

    public void setQuestion(String question2) {
        this.question = question2;
    }

    public String getQuestion() {
        return this.question;
    }
}
