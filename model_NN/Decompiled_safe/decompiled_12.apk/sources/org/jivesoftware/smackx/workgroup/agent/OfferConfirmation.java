package org.jivesoftware.smackx.workgroup.agent;

import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;

public class OfferConfirmation extends IQ {
    private long sessionID;
    private String userJID;

    public String getUserJID() {
        return this.userJID;
    }

    public void setUserJID(String userJID2) {
        this.userJID = userJID2;
    }

    public long getSessionID() {
        return this.sessionID;
    }

    public void setSessionID(long sessionID2) {
        this.sessionID = sessionID2;
    }

    public void notifyService(Connection con, String workgroup, String createdRoomName) {
        con.sendPacket(new NotifyServicePacket(workgroup, createdRoomName));
    }

    public String getChildElementXML() {
        return "<offer-confirmation xmlns=\"http://jabber.org/protocol/workgroup\">" + "</offer-confirmation>";
    }

    public static class Provider implements IQProvider {
        public IQ parseIQ(XmlPullParser parser) throws Exception {
            OfferConfirmation confirmation = new OfferConfirmation();
            boolean done = false;
            while (!done) {
                parser.next();
                String elementName = parser.getName();
                if (parser.getEventType() == 2 && "user-jid".equals(elementName)) {
                    try {
                        confirmation.setUserJID(parser.nextText());
                    } catch (NumberFormatException e) {
                    }
                } else if (parser.getEventType() == 2 && "session-id".equals(elementName)) {
                    try {
                        confirmation.setSessionID(Long.valueOf(parser.nextText()).longValue());
                    } catch (NumberFormatException e2) {
                    }
                } else if (parser.getEventType() == 3 && "offer-confirmation".equals(elementName)) {
                    done = true;
                }
            }
            return confirmation;
        }
    }

    private class NotifyServicePacket extends IQ {
        String roomName;

        NotifyServicePacket(String workgroup, String roomName2) {
            setTo(workgroup);
            setType(IQ.Type.RESULT);
            this.roomName = roomName2;
        }

        public String getChildElementXML() {
            return "<offer-confirmation  roomname=\"" + this.roomName + "\" xmlns=\"http://jabber.org/protocol/workgroup" + "\"/>";
        }
    }
}
