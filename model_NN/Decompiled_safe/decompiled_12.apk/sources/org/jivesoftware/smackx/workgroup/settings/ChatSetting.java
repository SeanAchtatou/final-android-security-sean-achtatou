package org.jivesoftware.smackx.workgroup.settings;

public class ChatSetting {
    private String key;
    private int type;
    private String value;

    public ChatSetting(String key2, String value2, int type2) {
        setKey(key2);
        setValue(value2);
        setType(type2);
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(String key2) {
        this.key = key2;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value2) {
        this.value = value2;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type2) {
        this.type = type2;
    }
}
