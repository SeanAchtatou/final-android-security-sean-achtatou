package org.jivesoftware.smackx.workgroup.agent;

import java.util.Date;
import java.util.Map;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.packet.IQ;

public class Offer {
    private boolean accepted = false;
    private Connection connection;
    private OfferContent content;
    private Date expiresDate;
    private Map metaData;
    private boolean rejected = false;
    private AgentSession session;
    private String sessionID;
    private String userID;
    private String userJID;
    private String workgroupName;

    Offer(Connection conn, AgentSession agentSession, String userID2, String userJID2, String workgroupName2, Date expiresDate2, String sessionID2, Map metaData2, OfferContent content2) {
        this.connection = conn;
        this.session = agentSession;
        this.userID = userID2;
        this.userJID = userJID2;
        this.workgroupName = workgroupName2;
        this.expiresDate = expiresDate2;
        this.sessionID = sessionID2;
        this.metaData = metaData2;
        this.content = content2;
    }

    public void accept() {
        this.connection.sendPacket(new AcceptPacket(this.session.getWorkgroupJID()));
        this.accepted = true;
    }

    public void reject() {
        this.connection.sendPacket(new RejectPacket(this.session.getWorkgroupJID()));
        this.rejected = true;
    }

    public String getUserID() {
        return this.userID;
    }

    public String getUserJID() {
        return this.userJID;
    }

    public String getWorkgroupName() {
        return this.workgroupName;
    }

    public Date getExpiresDate() {
        return this.expiresDate;
    }

    public String getSessionID() {
        return this.sessionID;
    }

    public Map getMetaData() {
        return this.metaData;
    }

    public OfferContent getContent() {
        return this.content;
    }

    public boolean isAccepted() {
        return this.accepted;
    }

    public boolean isRejected() {
        return this.rejected;
    }

    private class RejectPacket extends IQ {
        RejectPacket(String workgroup) {
            setTo(workgroup);
            setType(IQ.Type.SET);
        }

        public String getChildElementXML() {
            return "<offer-reject id=\"" + Offer.this.getSessionID() + "\" xmlns=\"http://jabber.org/protocol/workgroup" + "\"/>";
        }
    }

    private class AcceptPacket extends IQ {
        AcceptPacket(String workgroup) {
            setTo(workgroup);
            setType(IQ.Type.SET);
        }

        public String getChildElementXML() {
            return "<offer-accept id=\"" + Offer.this.getSessionID() + "\" xmlns=\"http://jabber.org/protocol/workgroup" + "\"/>";
        }
    }
}
