package org.jivesoftware.smackx.workgroup.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public final class ModelUtil {
    private ModelUtil() {
    }

    public static final boolean areEqual(Object o1, Object o2) {
        if (o1 == o2) {
            return true;
        }
        if (o1 == null || o2 == null) {
            return false;
        }
        return o1.equals(o2);
    }

    public static final boolean areBooleansEqual(Boolean b1, Boolean b2) {
        return (b1 == Boolean.TRUE && b2 == Boolean.TRUE) || !(b1 == Boolean.TRUE || b2 == Boolean.TRUE);
    }

    public static final boolean areDifferent(Object o1, Object o2) {
        return !areEqual(o1, o2);
    }

    public static final boolean areBooleansDifferent(Boolean b1, Boolean b2) {
        return !areBooleansEqual(b1, b2);
    }

    public static final boolean hasNonNullElement(Object[] array) {
        if (array != null) {
            for (Object obj : array) {
                if (obj != null) {
                    return true;
                }
            }
        }
        return false;
    }

    public static final String concat(String[] strs) {
        return concat(strs, " ");
    }

    public static final String concat(String[] strs, String delim) {
        if (strs == null) {
            return "";
        }
        StringBuilder buf = new StringBuilder();
        for (String str : strs) {
            if (str != null) {
                buf.append(str).append(delim);
            }
        }
        int length = buf.length();
        if (length > 0) {
            buf.setLength(length - 1);
        }
        return buf.toString();
    }

    public static final boolean hasLength(String s) {
        return s != null && s.length() > 0;
    }

    public static final String nullifyIfEmpty(String s) {
        if (hasLength(s)) {
            return s;
        }
        return null;
    }

    public static final String nullifyingToString(Object o) {
        if (o != null) {
            return nullifyIfEmpty(o.toString());
        }
        return null;
    }

    public static boolean hasStringChanged(String oldString, String newString) {
        if (oldString == null && newString == null) {
            return false;
        }
        if ((oldString == null && newString != null) || (oldString != null && newString == null)) {
            return true;
        }
        if (!oldString.equals(newString)) {
            return true;
        }
        return false;
    }

    public static String getTimeFromLong(long diff) {
        new Date();
        long j = diff / 86400000;
        long diff2 = diff % 86400000;
        long numHours = diff2 / 3600000;
        long diff3 = diff2 % 3600000;
        long numMinutes = diff3 / 60000;
        long diff4 = diff3 % 60000;
        long numSeconds = diff4 / 1000;
        long diff5 = diff4 % 1000;
        StringBuilder buf = new StringBuilder();
        if (numHours > 0) {
            buf.append(String.valueOf(numHours) + " " + "h" + ", ");
        }
        if (numMinutes > 0) {
            buf.append(String.valueOf(numMinutes) + " " + "min" + ", ");
        }
        buf.append(String.valueOf(numSeconds) + " " + "sec");
        return buf.toString();
    }

    public static List iteratorAsList(Iterator i) {
        ArrayList list = new ArrayList(10);
        while (i.hasNext()) {
            list.add(i.next());
        }
        return list;
    }

    public static Iterator reverseListIterator(ListIterator i) {
        return new ReverseListIterator(i);
    }
}
