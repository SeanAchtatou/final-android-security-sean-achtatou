package org.jivesoftware.smackx.workgroup.packet;

import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.provider.PacketExtensionProvider;
import org.xmlpull.v1.XmlPullParser;

public class UserID implements PacketExtension {
    public static final String ELEMENT_NAME = "user";
    public static final String NAMESPACE = "http://jivesoftware.com/protocol/workgroup";
    private String userID;

    public UserID(String userID2) {
        this.userID = userID2;
    }

    public String getUserID() {
        return this.userID;
    }

    public String getElementName() {
        return "user";
    }

    public String getNamespace() {
        return "http://jivesoftware.com/protocol/workgroup";
    }

    public String toXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append("user").append(" xmlns=\"").append("http://jivesoftware.com/protocol/workgroup").append("\" ");
        buf.append("id=\"").append(getUserID());
        buf.append("\"/>");
        return buf.toString();
    }

    public static class Provider implements PacketExtensionProvider {
        public PacketExtension parseExtension(XmlPullParser parser) throws Exception {
            String userID = parser.getAttributeValue("", "id");
            parser.next();
            return new UserID(userID);
        }
    }
}
