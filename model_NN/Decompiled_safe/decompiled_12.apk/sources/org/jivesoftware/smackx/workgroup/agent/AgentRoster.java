package org.jivesoftware.smackx.workgroup.agent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.packet.DiscoverItems;
import org.jivesoftware.smackx.workgroup.packet.AgentStatus;
import org.jivesoftware.smackx.workgroup.packet.AgentStatusRequest;

public class AgentRoster {
    private static final int EVENT_AGENT_ADDED = 0;
    private static final int EVENT_AGENT_REMOVED = 1;
    private static final int EVENT_PRESENCE_CHANGED = 2;
    private Connection connection;
    /* access modifiers changed from: private */
    public List<String> entries;
    private List<AgentRosterListener> listeners;
    /* access modifiers changed from: private */
    public Map<String, Map<String, Presence>> presenceMap;
    boolean rosterInitialized = false;
    /* access modifiers changed from: private */
    public String workgroupJID;

    AgentRoster(Connection connection2, String workgroupJID2) {
        this.connection = connection2;
        this.workgroupJID = workgroupJID2;
        this.entries = new ArrayList();
        this.listeners = new ArrayList();
        this.presenceMap = new HashMap();
        connection2.addPacketListener(new AgentStatusListener(this, null), new PacketTypeFilter(AgentStatusRequest.class));
        connection2.addPacketListener(new PresencePacketListener(this, null), new PacketTypeFilter(Presence.class));
        AgentStatusRequest request = new AgentStatusRequest();
        request.setTo(workgroupJID2);
        connection2.sendPacket(request);
    }

    public void reload() {
        AgentStatusRequest request = new AgentStatusRequest();
        request.setTo(this.workgroupJID);
        this.connection.sendPacket(request);
    }

    public void addListener(AgentRosterListener listener) {
        synchronized (this.listeners) {
            if (!this.listeners.contains(listener)) {
                this.listeners.add(listener);
                for (String jid : getAgents()) {
                    if (this.entries.contains(jid)) {
                        listener.agentAdded(jid);
                        Map<String, Presence> userPresences = this.presenceMap.get(jid);
                        if (userPresences != null) {
                            for (Presence presenceChanged : userPresences.values()) {
                                listener.presenceChanged(presenceChanged);
                            }
                        }
                    }
                }
            }
        }
    }

    public void removeListener(AgentRosterListener listener) {
        synchronized (this.listeners) {
            this.listeners.remove(listener);
        }
    }

    public int getAgentCount() {
        return this.entries.size();
    }

    public Set<String> getAgents() {
        Set<String> agents = new HashSet<>();
        synchronized (this.entries) {
            for (String add : this.entries) {
                agents.add(add);
            }
        }
        return Collections.unmodifiableSet(agents);
    }

    public boolean contains(String jid) {
        if (jid == null) {
            return false;
        }
        synchronized (this.entries) {
            for (String entry : this.entries) {
                if (entry.toLowerCase().equals(jid.toLowerCase())) {
                    return true;
                }
            }
            return false;
        }
    }

    public Presence getPresence(String user) {
        Map<String, Presence> userPresences = this.presenceMap.get(getPresenceMapKey(user));
        if (userPresences == null) {
            Presence presence = new Presence(Presence.Type.unavailable);
            presence.setFrom(user);
            return presence;
        }
        Presence presence2 = null;
        for (Object obj : userPresences.keySet()) {
            Presence p = (Presence) userPresences.get(obj);
            if (presence2 == null) {
                presence2 = p;
            } else if (p.getPriority() > presence2.getPriority()) {
                presence2 = p;
            }
        }
        if (presence2 != null) {
            return presence2;
        }
        Presence presence3 = new Presence(Presence.Type.unavailable);
        presence3.setFrom(user);
        return presence3;
    }

    /* access modifiers changed from: private */
    public String getPresenceMapKey(String user) {
        String key = user;
        if (!contains(user)) {
            return StringUtils.parseBareAddress(user).toLowerCase();
        }
        return key;
    }

    /* access modifiers changed from: private */
    public void fireEvent(int eventType, Object eventObject) {
        AgentRosterListener[] listeners2;
        synchronized (this.listeners) {
            listeners2 = new AgentRosterListener[this.listeners.size()];
            this.listeners.toArray(listeners2);
        }
        for (int i = 0; i < listeners2.length; i++) {
            switch (eventType) {
                case 0:
                    listeners2[i].agentAdded((String) eventObject);
                    break;
                case 1:
                    listeners2[i].agentRemoved((String) eventObject);
                    break;
                case 2:
                    listeners2[i].presenceChanged((Presence) eventObject);
                    break;
            }
        }
    }

    private class PresencePacketListener implements PacketListener {
        private PresencePacketListener() {
        }

        /* synthetic */ PresencePacketListener(AgentRoster agentRoster, PresencePacketListener presencePacketListener) {
            this();
        }

        public void processPacket(Packet packet) {
            Map<String, Presence> userPresences;
            Presence presence = (Presence) packet;
            String from = presence.getFrom();
            if (from == null) {
                System.out.println("Presence with no FROM: " + presence.toXML());
                return;
            }
            String key = AgentRoster.this.getPresenceMapKey(from);
            if (presence.getType() == Presence.Type.available) {
                AgentStatus agentStatus = (AgentStatus) presence.getExtension(AgentStatus.ELEMENT_NAME, "http://jabber.org/protocol/workgroup");
                if (agentStatus != null && AgentRoster.this.workgroupJID.equals(agentStatus.getWorkgroupJID())) {
                    if (AgentRoster.this.presenceMap.get(key) == null) {
                        userPresences = new HashMap<>();
                        AgentRoster.this.presenceMap.put(key, userPresences);
                    } else {
                        userPresences = (Map) AgentRoster.this.presenceMap.get(key);
                    }
                    synchronized (userPresences) {
                        userPresences.put(StringUtils.parseResource(from), presence);
                    }
                    synchronized (AgentRoster.this.entries) {
                        for (String entry : AgentRoster.this.entries) {
                            if (entry.toLowerCase().equals(StringUtils.parseBareAddress(key).toLowerCase())) {
                                AgentRoster.this.fireEvent(2, packet);
                            }
                        }
                    }
                }
            } else if (presence.getType() == Presence.Type.unavailable) {
                if (AgentRoster.this.presenceMap.get(key) != null) {
                    Map<String, Presence> userPresences2 = (Map) AgentRoster.this.presenceMap.get(key);
                    synchronized (userPresences2) {
                        userPresences2.remove(StringUtils.parseResource(from));
                    }
                    if (userPresences2.isEmpty()) {
                        AgentRoster.this.presenceMap.remove(key);
                    }
                }
                synchronized (AgentRoster.this.entries) {
                    for (String entry2 : AgentRoster.this.entries) {
                        if (entry2.toLowerCase().equals(StringUtils.parseBareAddress(key).toLowerCase())) {
                            AgentRoster.this.fireEvent(2, packet);
                        }
                    }
                }
            }
        }
    }

    private class AgentStatusListener implements PacketListener {
        private AgentStatusListener() {
        }

        /* synthetic */ AgentStatusListener(AgentRoster agentRoster, AgentStatusListener agentStatusListener) {
            this();
        }

        public void processPacket(Packet packet) {
            if (packet instanceof AgentStatusRequest) {
                for (AgentStatusRequest.Item item : ((AgentStatusRequest) packet).getAgents()) {
                    String agentJID = item.getJID();
                    if (DiscoverItems.Item.REMOVE_ACTION.equals(item.getType())) {
                        AgentRoster.this.presenceMap.remove(StringUtils.parseName(String.valueOf(StringUtils.parseName(agentJID)) + "@" + StringUtils.parseServer(agentJID)));
                        AgentRoster.this.fireEvent(1, agentJID);
                    } else {
                        AgentRoster.this.entries.add(agentJID);
                        AgentRoster.this.fireEvent(0, agentJID);
                    }
                }
                AgentRoster.this.rosterInitialized = true;
            }
        }
    }
}
