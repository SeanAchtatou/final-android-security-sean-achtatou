package org.jivesoftware.smackx.workgroup.agent;

import java.util.Date;

public class RevokedOffer {
    private String reason;
    private String sessionID;
    private Date timestamp;
    private String userID;
    private String userJID;
    private String workgroupName;

    RevokedOffer(String userJID2, String userID2, String workgroupName2, String sessionID2, String reason2, Date timestamp2) {
        this.userJID = userJID2;
        this.userID = userID2;
        this.workgroupName = workgroupName2;
        this.sessionID = sessionID2;
        this.reason = reason2;
        this.timestamp = timestamp2;
    }

    public String getUserJID() {
        return this.userJID;
    }

    public String getUserID() {
        return this.userID;
    }

    public String getWorkgroupName() {
        return this.workgroupName;
    }

    public String getSessionID() {
        return this.sessionID;
    }

    public String getReason() {
        return this.reason;
    }

    public Date getTimestamp() {
        return this.timestamp;
    }
}
