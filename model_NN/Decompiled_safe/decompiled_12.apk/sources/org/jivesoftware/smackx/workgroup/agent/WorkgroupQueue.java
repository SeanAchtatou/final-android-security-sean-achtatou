package org.jivesoftware.smackx.workgroup.agent;

import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;
import org.jivesoftware.smackx.packet.IBBExtensions;

public class WorkgroupQueue {
    private int averageWaitTime = -1;
    private int currentChats = 0;
    private int maxChats = 0;
    private String name;
    private Date oldestEntry = null;
    private Status status = Status.CLOSED;
    private Set users = Collections.EMPTY_SET;

    WorkgroupQueue(String name2) {
        this.name = name2;
    }

    public String getName() {
        return this.name;
    }

    public Status getStatus() {
        return this.status;
    }

    /* access modifiers changed from: package-private */
    public void setStatus(Status status2) {
        this.status = status2;
    }

    public int getUserCount() {
        if (this.users == null) {
            return 0;
        }
        return this.users.size();
    }

    public Iterator getUsers() {
        if (this.users == null) {
            return Collections.EMPTY_SET.iterator();
        }
        return Collections.unmodifiableSet(this.users).iterator();
    }

    /* access modifiers changed from: package-private */
    public void setUsers(Set users2) {
        this.users = users2;
    }

    public int getAverageWaitTime() {
        return this.averageWaitTime;
    }

    /* access modifiers changed from: package-private */
    public void setAverageWaitTime(int averageTime) {
        this.averageWaitTime = averageTime;
    }

    public Date getOldestEntry() {
        return this.oldestEntry;
    }

    /* access modifiers changed from: package-private */
    public void setOldestEntry(Date oldestEntry2) {
        this.oldestEntry = oldestEntry2;
    }

    public int getMaxChats() {
        return this.maxChats;
    }

    /* access modifiers changed from: package-private */
    public void setMaxChats(int maxChats2) {
        this.maxChats = maxChats2;
    }

    public int getCurrentChats() {
        return this.currentChats;
    }

    /* access modifiers changed from: package-private */
    public void setCurrentChats(int currentChats2) {
        this.currentChats = currentChats2;
    }

    public static class Status {
        public static final Status ACTIVE = new Status("active");
        public static final Status CLOSED = new Status("closed");
        public static final Status OPEN = new Status(IBBExtensions.Open.ELEMENT_NAME);
        private String value;

        public static Status fromString(String type) {
            if (type == null) {
                return null;
            }
            String type2 = type.toLowerCase();
            if (OPEN.toString().equals(type2)) {
                return OPEN;
            }
            if (ACTIVE.toString().equals(type2)) {
                return ACTIVE;
            }
            if (CLOSED.toString().equals(type2)) {
                return CLOSED;
            }
            return null;
        }

        private Status(String value2) {
            this.value = value2;
        }

        public String toString() {
            return this.value;
        }
    }
}
