package org.jivesoftware.smackx.workgroup.agent;

public abstract class OfferContent {
    /* access modifiers changed from: package-private */
    public abstract boolean isInvitation();

    /* access modifiers changed from: package-private */
    public abstract boolean isTransfer();

    /* access modifiers changed from: package-private */
    public abstract boolean isUserRequest();
}
