package org.jivesoftware.smackx.workgroup.agent;

public class TransferRequest extends OfferContent {
    private String inviter;
    private String reason;
    private String room;

    public TransferRequest(String inviter2, String room2, String reason2) {
        this.inviter = inviter2;
        this.room = room2;
        this.reason = reason2;
    }

    public String getInviter() {
        return this.inviter;
    }

    public String getRoom() {
        return this.room;
    }

    public String getReason() {
        return this.reason;
    }

    /* access modifiers changed from: package-private */
    public boolean isUserRequest() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean isInvitation() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean isTransfer() {
        return true;
    }
}
