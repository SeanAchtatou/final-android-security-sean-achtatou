package org.jivesoftware.smackx.packet;

import org.jivesoftware.smack.packet.PacketExtension;

public class CapsExtension implements PacketExtension {
    public static final String NODE_NAME = "c";
    public static final String XMLNS = "http://jabber.org/protocol/caps";
    private String hash;
    private String node;
    private String version;

    public CapsExtension() {
    }

    public CapsExtension(String node2, String version2, String hash2) {
        this.node = node2;
        this.version = version2;
        this.hash = hash2;
    }

    public String getElementName() {
        return NODE_NAME;
    }

    public String getNamespace() {
        return XMLNS;
    }

    public String getNode() {
        return this.node;
    }

    public void setNode(String node2) {
        this.node = node2;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String version2) {
        this.version = version2;
    }

    public String getHash() {
        return this.hash;
    }

    public void setHash(String hash2) {
        this.hash = hash2;
    }

    public String toXML() {
        return "<c xmlns='http://jabber.org/protocol/caps' hash='" + this.hash + "' " + "node='" + this.node + "' " + "ver='" + this.version + "'/>";
    }
}
