package org.jivesoftware.smackx.packet;

import org.jivesoftware.smack.packet.PacketExtension;

public class PEPEvent implements PacketExtension {
    PEPItem item;

    public PEPEvent() {
    }

    public PEPEvent(PEPItem item2) {
        this.item = item2;
    }

    public void addPEPItem(PEPItem item2) {
        this.item = item2;
    }

    public String getElementName() {
        return "event";
    }

    public String getNamespace() {
        return "http://jabber.org/protocol/pubsub";
    }

    public String toXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append(getElementName()).append(" xmlns=\"").append(getNamespace()).append("\">");
        buf.append(this.item.toXML());
        buf.append("</").append(getElementName()).append(">");
        return buf.toString();
    }
}
