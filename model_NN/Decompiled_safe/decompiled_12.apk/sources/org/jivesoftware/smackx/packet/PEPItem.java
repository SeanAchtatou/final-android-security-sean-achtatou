package org.jivesoftware.smackx.packet;

import org.jivesoftware.smack.packet.PacketExtension;

public abstract class PEPItem implements PacketExtension {
    String id;

    /* access modifiers changed from: package-private */
    public abstract String getItemDetailsXML();

    /* access modifiers changed from: package-private */
    public abstract String getNode();

    public PEPItem(String id2) {
        this.id = id2;
    }

    public String getElementName() {
        return "item";
    }

    public String getNamespace() {
        return "http://jabber.org/protocol/pubsub";
    }

    public String toXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append(getElementName()).append(" id=\"").append(this.id).append("\">");
        buf.append(getItemDetailsXML());
        buf.append("</").append(getElementName()).append(">");
        return buf.toString();
    }
}
