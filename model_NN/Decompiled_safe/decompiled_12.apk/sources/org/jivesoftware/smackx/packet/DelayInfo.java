package org.jivesoftware.smackx.packet;

import java.util.Date;
import org.jivesoftware.smack.packet.Packet;

public class DelayInfo extends DelayInformation {
    DelayInformation wrappedInfo;

    public DelayInfo(DelayInformation delay) {
        super(delay.getStamp());
        this.wrappedInfo = delay;
    }

    public String getFrom() {
        return this.wrappedInfo.getFrom();
    }

    public String getReason() {
        return this.wrappedInfo.getReason();
    }

    public Date getStamp() {
        return this.wrappedInfo.getStamp();
    }

    public void setFrom(String from) {
        this.wrappedInfo.setFrom(from);
    }

    public void setReason(String reason) {
        this.wrappedInfo.setReason(reason);
    }

    public String getElementName() {
        return "delay";
    }

    public String getNamespace() {
        return "urn:xmpp:delay";
    }

    public String toXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append(getElementName()).append(" xmlns=\"").append(getNamespace()).append("\"");
        buf.append(" stamp=\"");
        synchronized (Packet.XEP_0082_UTC_FORMAT) {
            buf.append(Packet.XEP_0082_UTC_FORMAT.format(getStamp()));
        }
        buf.append("\"");
        if (getFrom() != null && getFrom().length() > 0) {
            buf.append(" from=\"").append(getFrom()).append("\"");
        }
        buf.append(">");
        if (getReason() != null && getReason().length() > 0) {
            buf.append(getReason());
        }
        buf.append("</").append(getElementName()).append(">");
        return buf.toString();
    }
}
