package org.jivesoftware.smackx.packet;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.PacketCollector;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.PacketIDFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.EntityCapsManager;

public class VCard extends IQ {
    private String avatar;
    /* access modifiers changed from: private */
    public String emailHome;
    /* access modifiers changed from: private */
    public String emailWork;
    /* access modifiers changed from: private */
    public String firstName;
    /* access modifiers changed from: private */
    public Map<String, String> homeAddr = new HashMap();
    /* access modifiers changed from: private */
    public Map<String, String> homePhones = new HashMap();
    /* access modifiers changed from: private */
    public String lastName;
    /* access modifiers changed from: private */
    public String middleName;
    /* access modifiers changed from: private */
    public String organization;
    /* access modifiers changed from: private */
    public String organizationUnit;
    /* access modifiers changed from: private */
    public Map<String, String> otherSimpleFields = new HashMap();
    /* access modifiers changed from: private */
    public Map<String, String> otherUnescapableFields = new HashMap();
    /* access modifiers changed from: private */
    public Map<String, String> workAddr = new HashMap();
    /* access modifiers changed from: private */
    public Map<String, String> workPhones = new HashMap();

    private interface ContentBuilder {
        void addTagContent();
    }

    public String getField(String field) {
        return this.otherSimpleFields.get(field);
    }

    public void setField(String field, String value) {
        setField(field, value, false);
    }

    public void setField(String field, String value, boolean isUnescapable) {
        if (!isUnescapable) {
            this.otherSimpleFields.put(field, value);
        } else {
            this.otherUnescapableFields.put(field, value);
        }
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName2) {
        this.firstName = firstName2;
        updateFN();
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName2) {
        this.lastName = lastName2;
        updateFN();
    }

    public String getMiddleName() {
        return this.middleName;
    }

    public void setMiddleName(String middleName2) {
        this.middleName = middleName2;
        updateFN();
    }

    public String getNickName() {
        return this.otherSimpleFields.get("NICKNAME");
    }

    public void setNickName(String nickName) {
        this.otherSimpleFields.put("NICKNAME", nickName);
    }

    public String getEmailHome() {
        return this.emailHome;
    }

    public void setEmailHome(String email) {
        this.emailHome = email;
    }

    public String getEmailWork() {
        return this.emailWork;
    }

    public void setEmailWork(String emailWork2) {
        this.emailWork = emailWork2;
    }

    public String getJabberId() {
        return this.otherSimpleFields.get("JABBERID");
    }

    public void setJabberId(String jabberId) {
        this.otherSimpleFields.put("JABBERID", jabberId);
    }

    public String getOrganization() {
        return this.organization;
    }

    public void setOrganization(String organization2) {
        this.organization = organization2;
    }

    public String getOrganizationUnit() {
        return this.organizationUnit;
    }

    public void setOrganizationUnit(String organizationUnit2) {
        this.organizationUnit = organizationUnit2;
    }

    public String getAddressFieldHome(String addrField) {
        return this.homeAddr.get(addrField);
    }

    public void setAddressFieldHome(String addrField, String value) {
        this.homeAddr.put(addrField, value);
    }

    public String getAddressFieldWork(String addrField) {
        return this.workAddr.get(addrField);
    }

    public void setAddressFieldWork(String addrField, String value) {
        this.workAddr.put(addrField, value);
    }

    public void setPhoneHome(String phoneType, String phoneNum) {
        this.homePhones.put(phoneType, phoneNum);
    }

    public String getPhoneHome(String phoneType) {
        return this.homePhones.get(phoneType);
    }

    public void setPhoneWork(String phoneType, String phoneNum) {
        this.workPhones.put(phoneType, phoneNum);
    }

    public String getPhoneWork(String phoneType) {
        return this.workPhones.get(phoneType);
    }

    public void setAvatar(URL avatarURL) {
        byte[] bytes = new byte[0];
        try {
            bytes = getBytes(avatarURL);
        } catch (IOException e) {
            e.printStackTrace();
        }
        setAvatar(bytes);
    }

    public void setAvatar(byte[] bytes) {
        if (bytes == null) {
            this.otherUnescapableFields.remove("PHOTO");
            return;
        }
        String encodedImage = StringUtils.encodeBase64(bytes);
        this.avatar = encodedImage;
        setField("PHOTO", "<TYPE>image/jpeg</TYPE><BINVAL>" + encodedImage + "</BINVAL>", true);
    }

    public void setAvatar(byte[] bytes, String mimeType) {
        if (bytes == null) {
            this.otherUnescapableFields.remove("PHOTO");
            return;
        }
        String encodedImage = StringUtils.encodeBase64(bytes);
        this.avatar = encodedImage;
        setField("PHOTO", "<TYPE>" + mimeType + "</TYPE><BINVAL>" + encodedImage + "</BINVAL>", true);
    }

    public void setEncodedImage(String encodedAvatar) {
        this.avatar = encodedAvatar;
    }

    public byte[] getAvatar() {
        if (this.avatar == null) {
            return null;
        }
        return StringUtils.decodeBase64(this.avatar);
    }

    public static byte[] getBytes(URL url) throws IOException {
        File file = new File(url.getPath());
        if (file.exists()) {
            return getFileBytes(file);
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static byte[] getFileBytes(java.io.File r7) throws java.io.IOException {
        /*
            r0 = 0
            java.io.BufferedInputStream r1 = new java.io.BufferedInputStream     // Catch:{ all -> 0x002f }
            java.io.FileInputStream r5 = new java.io.FileInputStream     // Catch:{ all -> 0x002f }
            r5.<init>(r7)     // Catch:{ all -> 0x002f }
            r1.<init>(r5)     // Catch:{ all -> 0x002f }
            long r5 = r7.length()     // Catch:{ all -> 0x0021 }
            int r3 = (int) r5     // Catch:{ all -> 0x0021 }
            byte[] r2 = new byte[r3]     // Catch:{ all -> 0x0021 }
            int r4 = r1.read(r2)     // Catch:{ all -> 0x0021 }
            int r5 = r2.length     // Catch:{ all -> 0x0021 }
            if (r4 == r5) goto L_0x0029
            java.io.IOException r5 = new java.io.IOException     // Catch:{ all -> 0x0021 }
            java.lang.String r6 = "Entire file not read"
            r5.<init>(r6)     // Catch:{ all -> 0x0021 }
            throw r5     // Catch:{ all -> 0x0021 }
        L_0x0021:
            r5 = move-exception
            r0 = r1
        L_0x0023:
            if (r0 == 0) goto L_0x0028
            r0.close()
        L_0x0028:
            throw r5
        L_0x0029:
            if (r1 == 0) goto L_0x002e
            r1.close()
        L_0x002e:
            return r2
        L_0x002f:
            r5 = move-exception
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smackx.packet.VCard.getFileBytes(java.io.File):byte[]");
    }

    public String getAvatarHash() {
        byte[] bytes = getAvatar();
        if (bytes == null) {
            return null;
        }
        try {
            MessageDigest digest = MessageDigest.getInstance(EntityCapsManager.HASH_METHOD_CAPS);
            digest.update(bytes);
            return StringUtils.encodeHex(digest.digest());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void updateFN() {
        StringBuilder sb = new StringBuilder();
        if (this.firstName != null) {
            sb.append(StringUtils.escapeForXML(this.firstName)).append(' ');
        }
        if (this.middleName != null) {
            sb.append(StringUtils.escapeForXML(this.middleName)).append(' ');
        }
        if (this.lastName != null) {
            sb.append(StringUtils.escapeForXML(this.lastName));
        }
        setField("FN", sb.toString());
    }

    public void save(Connection connection) throws XMPPException {
        checkAuthenticated(connection, true);
        setType(IQ.Type.SET);
        setFrom(connection.getUser());
        PacketCollector collector = connection.createPacketCollector(new PacketIDFilter(getPacketID()));
        connection.sendPacket(this);
        Packet response = collector.nextResult((long) SmackConfiguration.getPacketReplyTimeout());
        collector.cancel();
        if (response == null) {
            throw new XMPPException("No response from server on status set.");
        } else if (response.getError() != null) {
            throw new XMPPException(response.getError());
        }
    }

    public void load(Connection connection) throws XMPPException {
        checkAuthenticated(connection, true);
        setFrom(connection.getUser());
        doLoad(connection, connection.getUser());
    }

    public void load(Connection connection, String user) throws XMPPException {
        checkAuthenticated(connection, false);
        setTo(user);
        doLoad(connection, user);
    }

    /* JADX WARN: Type inference failed for: r5v5, types: [org.jivesoftware.smack.packet.Packet] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void doLoad(org.jivesoftware.smack.Connection r9, java.lang.String r10) throws org.jivesoftware.smack.XMPPException {
        /*
            r8 = this;
            org.jivesoftware.smack.packet.IQ$Type r5 = org.jivesoftware.smack.packet.IQ.Type.GET
            r8.setType(r5)
            org.jivesoftware.smack.filter.PacketIDFilter r5 = new org.jivesoftware.smack.filter.PacketIDFilter
            java.lang.String r6 = r8.getPacketID()
            r5.<init>(r6)
            org.jivesoftware.smack.PacketCollector r1 = r9.createPacketCollector(r5)
            r9.sendPacket(r8)
            r4 = 0
            int r5 = org.jivesoftware.smack.SmackConfiguration.getPacketReplyTimeout()     // Catch:{ ClassCastException -> 0x0034 }
            long r5 = (long) r5     // Catch:{ ClassCastException -> 0x0034 }
            org.jivesoftware.smack.packet.Packet r5 = r1.nextResult(r5)     // Catch:{ ClassCastException -> 0x0034 }
            r0 = r5
            org.jivesoftware.smackx.packet.VCard r0 = (org.jivesoftware.smackx.packet.VCard) r0     // Catch:{ ClassCastException -> 0x0034 }
            r4 = r0
            if (r4 != 0) goto L_0x004d
            java.lang.String r3 = "Timeout getting VCard information"
            org.jivesoftware.smack.XMPPException r5 = new org.jivesoftware.smack.XMPPException     // Catch:{ ClassCastException -> 0x0034 }
            org.jivesoftware.smack.packet.XMPPError r6 = new org.jivesoftware.smack.packet.XMPPError     // Catch:{ ClassCastException -> 0x0034 }
            org.jivesoftware.smack.packet.XMPPError$Condition r7 = org.jivesoftware.smack.packet.XMPPError.Condition.request_timeout     // Catch:{ ClassCastException -> 0x0034 }
            r6.<init>(r7, r3)     // Catch:{ ClassCastException -> 0x0034 }
            r5.<init>(r3, r6)     // Catch:{ ClassCastException -> 0x0034 }
            throw r5     // Catch:{ ClassCastException -> 0x0034 }
        L_0x0034:
            r2 = move-exception
            java.io.PrintStream r5 = java.lang.System.out
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r7 = "No VCard for "
            r6.<init>(r7)
            java.lang.StringBuilder r6 = r6.append(r10)
            java.lang.String r6 = r6.toString()
            r5.println(r6)
        L_0x0049:
            r8.copyFieldsFrom(r4)
            return
        L_0x004d:
            org.jivesoftware.smack.packet.XMPPError r5 = r4.getError()     // Catch:{ ClassCastException -> 0x0034 }
            if (r5 == 0) goto L_0x0049
            org.jivesoftware.smack.XMPPException r5 = new org.jivesoftware.smack.XMPPException     // Catch:{ ClassCastException -> 0x0034 }
            org.jivesoftware.smack.packet.XMPPError r6 = r4.getError()     // Catch:{ ClassCastException -> 0x0034 }
            r5.<init>(r6)     // Catch:{ ClassCastException -> 0x0034 }
            throw r5     // Catch:{ ClassCastException -> 0x0034 }
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smackx.packet.VCard.doLoad(org.jivesoftware.smack.Connection, java.lang.String):void");
    }

    public String getChildElementXML() {
        StringBuilder sb = new StringBuilder();
        new VCardWriter(sb).write();
        return sb.toString();
    }

    private void copyFieldsFrom(VCard result) {
        if (result == null) {
            result = new VCard();
        }
        for (Field field : VCard.class.getDeclaredFields()) {
            if (field.getDeclaringClass() == VCard.class && !Modifier.isFinal(field.getModifiers())) {
                try {
                    field.setAccessible(true);
                    field.set(this, field.get(result));
                } catch (IllegalAccessException e) {
                    throw new RuntimeException("This cannot happen:" + field, e);
                }
            }
        }
    }

    private void checkAuthenticated(Connection connection, boolean checkForAnonymous) {
        if (connection == null) {
            throw new IllegalArgumentException("No connection was provided");
        } else if (!connection.isAuthenticated()) {
            throw new IllegalArgumentException("Connection is not authenticated");
        } else if (checkForAnonymous && connection.isAnonymous()) {
            throw new IllegalArgumentException("Connection cannot be anonymous");
        }
    }

    /* access modifiers changed from: private */
    public boolean hasContent() {
        return hasNameField() || hasOrganizationFields() || this.emailHome != null || this.emailWork != null || this.otherSimpleFields.size() > 0 || this.otherUnescapableFields.size() > 0 || this.homeAddr.size() > 0 || this.homePhones.size() > 0 || this.workAddr.size() > 0 || this.workPhones.size() > 0;
    }

    /* access modifiers changed from: private */
    public boolean hasNameField() {
        return (this.firstName == null && this.lastName == null && this.middleName == null) ? false : true;
    }

    /* access modifiers changed from: private */
    public boolean hasOrganizationFields() {
        return (this.organization == null && this.organizationUnit == null) ? false : true;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        VCard vCard = (VCard) o;
        if (this.emailHome != null) {
            if (!this.emailHome.equals(vCard.emailHome)) {
                return false;
            }
        } else if (vCard.emailHome != null) {
            return false;
        }
        if (this.emailWork != null) {
            if (!this.emailWork.equals(vCard.emailWork)) {
                return false;
            }
        } else if (vCard.emailWork != null) {
            return false;
        }
        if (this.firstName != null) {
            if (!this.firstName.equals(vCard.firstName)) {
                return false;
            }
        } else if (vCard.firstName != null) {
            return false;
        }
        if (!this.homeAddr.equals(vCard.homeAddr) || !this.homePhones.equals(vCard.homePhones)) {
            return false;
        }
        if (this.lastName != null) {
            if (!this.lastName.equals(vCard.lastName)) {
                return false;
            }
        } else if (vCard.lastName != null) {
            return false;
        }
        if (this.middleName != null) {
            if (!this.middleName.equals(vCard.middleName)) {
                return false;
            }
        } else if (vCard.middleName != null) {
            return false;
        }
        if (this.organization != null) {
            if (!this.organization.equals(vCard.organization)) {
                return false;
            }
        } else if (vCard.organization != null) {
            return false;
        }
        if (this.organizationUnit != null) {
            if (!this.organizationUnit.equals(vCard.organizationUnit)) {
                return false;
            }
        } else if (vCard.organizationUnit != null) {
            return false;
        }
        if (!this.otherSimpleFields.equals(vCard.otherSimpleFields) || !this.workAddr.equals(vCard.workAddr)) {
            return false;
        }
        return this.workPhones.equals(vCard.workPhones);
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6 = 0;
        int hashCode = ((((((((this.homePhones.hashCode() * 29) + this.workPhones.hashCode()) * 29) + this.homeAddr.hashCode()) * 29) + this.workAddr.hashCode()) * 29) + (this.firstName != null ? this.firstName.hashCode() : 0)) * 29;
        if (this.lastName != null) {
            i = this.lastName.hashCode();
        } else {
            i = 0;
        }
        int i7 = (hashCode + i) * 29;
        if (this.middleName != null) {
            i2 = this.middleName.hashCode();
        } else {
            i2 = 0;
        }
        int i8 = (i7 + i2) * 29;
        if (this.emailHome != null) {
            i3 = this.emailHome.hashCode();
        } else {
            i3 = 0;
        }
        int i9 = (i8 + i3) * 29;
        if (this.emailWork != null) {
            i4 = this.emailWork.hashCode();
        } else {
            i4 = 0;
        }
        int i10 = (i9 + i4) * 29;
        if (this.organization != null) {
            i5 = this.organization.hashCode();
        } else {
            i5 = 0;
        }
        int i11 = (i10 + i5) * 29;
        if (this.organizationUnit != null) {
            i6 = this.organizationUnit.hashCode();
        }
        return ((i11 + i6) * 29) + this.otherSimpleFields.hashCode();
    }

    public String toString() {
        return getChildElementXML();
    }

    private class VCardWriter {
        /* access modifiers changed from: private */
        public final StringBuilder sb;

        VCardWriter(StringBuilder sb2) {
            this.sb = sb2;
        }

        public void write() {
            appendTag("vCard", "xmlns", "vcard-temp", VCard.this.hasContent(), new ContentBuilder() {
                public void addTagContent() {
                    VCardWriter.this.buildActualContent();
                }
            });
        }

        /* access modifiers changed from: private */
        public void buildActualContent() {
            if (VCard.this.hasNameField()) {
                appendN();
            }
            appendOrganization();
            appendGenericFields();
            appendEmail(VCard.this.emailWork, "WORK");
            appendEmail(VCard.this.emailHome, "HOME");
            appendPhones(VCard.this.workPhones, "WORK");
            appendPhones(VCard.this.homePhones, "HOME");
            appendAddress(VCard.this.workAddr, "WORK");
            appendAddress(VCard.this.homeAddr, "HOME");
        }

        private void appendEmail(final String email, final String type) {
            if (email != null) {
                appendTag("EMAIL", true, new ContentBuilder() {
                    public void addTagContent() {
                        VCardWriter.this.appendEmptyTag(type);
                        VCardWriter.this.appendEmptyTag("INTERNET");
                        VCardWriter.this.appendEmptyTag("PREF");
                        VCardWriter.this.appendTag("USERID", StringUtils.escapeForXML(email));
                    }
                });
            }
        }

        private void appendPhones(Map<String, String> phones, final String code) {
            for (final Map.Entry entry : phones.entrySet()) {
                appendTag("TEL", true, new ContentBuilder() {
                    public void addTagContent() {
                        VCardWriter.this.appendEmptyTag(entry.getKey());
                        VCardWriter.this.appendEmptyTag(code);
                        VCardWriter.this.appendTag("NUMBER", StringUtils.escapeForXML((String) entry.getValue()));
                    }
                });
            }
        }

        private void appendAddress(final Map<String, String> addr, final String code) {
            if (addr.size() > 0) {
                appendTag("ADR", true, new ContentBuilder() {
                    public void addTagContent() {
                        VCardWriter.this.appendEmptyTag(code);
                        for (Map.Entry entry : addr.entrySet()) {
                            VCardWriter.this.appendTag((String) entry.getKey(), StringUtils.escapeForXML((String) entry.getValue()));
                        }
                    }
                });
            }
        }

        /* access modifiers changed from: private */
        public void appendEmptyTag(Object tag) {
            this.sb.append('<').append(tag).append("/>");
        }

        private void appendGenericFields() {
            for (Map.Entry entry : VCard.this.otherSimpleFields.entrySet()) {
                appendTag(entry.getKey().toString(), StringUtils.escapeForXML((String) entry.getValue()));
            }
            for (Map.Entry entry2 : VCard.this.otherUnescapableFields.entrySet()) {
                appendTag(entry2.getKey().toString(), (String) entry2.getValue());
            }
        }

        private void appendOrganization() {
            if (VCard.this.hasOrganizationFields()) {
                appendTag("ORG", true, new ContentBuilder() {
                    public void addTagContent() {
                        VCardWriter.this.appendTag("ORGNAME", StringUtils.escapeForXML(VCard.this.organization));
                        VCardWriter.this.appendTag("ORGUNIT", StringUtils.escapeForXML(VCard.this.organizationUnit));
                    }
                });
            }
        }

        private void appendN() {
            appendTag("N", true, new ContentBuilder() {
                public void addTagContent() {
                    VCardWriter.this.appendTag("FAMILY", StringUtils.escapeForXML(VCard.this.lastName));
                    VCardWriter.this.appendTag("GIVEN", StringUtils.escapeForXML(VCard.this.firstName));
                    VCardWriter.this.appendTag("MIDDLE", StringUtils.escapeForXML(VCard.this.middleName));
                }
            });
        }

        private void appendTag(String tag, String attr, String attrValue, boolean hasContent, ContentBuilder builder) {
            this.sb.append('<').append(tag);
            if (attr != null) {
                this.sb.append(' ').append(attr).append('=').append('\'').append(attrValue).append('\'');
            }
            if (hasContent) {
                this.sb.append('>');
                builder.addTagContent();
                this.sb.append("</").append(tag).append(">\n");
                return;
            }
            this.sb.append("/>\n");
        }

        private void appendTag(String tag, boolean hasContent, ContentBuilder builder) {
            appendTag(tag, null, null, hasContent, builder);
        }

        /* access modifiers changed from: private */
        public void appendTag(String tag, final String tagText) {
            if (tagText != null) {
                appendTag(tag, true, new ContentBuilder() {
                    public void addTagContent() {
                        VCardWriter.this.sb.append(tagText.trim());
                    }
                });
            }
        }
    }
}
