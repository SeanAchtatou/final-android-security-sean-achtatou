package org.jivesoftware.smackx.packet;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.PacketExtension;

public class IBBExtensions {
    public static final String NAMESPACE = "http://jabber.org/protocol/ibb";

    private static abstract class IBB extends IQ {
        final String sid;

        private IBB(String sid2) {
            this.sid = sid2;
        }

        /* synthetic */ IBB(String str, IBB ibb) {
            this(str);
        }

        public String getSessionID() {
            return this.sid;
        }

        public String getNamespace() {
            return "http://jabber.org/protocol/ibb";
        }
    }

    public static class Open extends IBB {
        public static final String ELEMENT_NAME = "open";
        private final int blockSize;

        public /* bridge */ /* synthetic */ String getNamespace() {
            return super.getNamespace();
        }

        public /* bridge */ /* synthetic */ String getSessionID() {
            return super.getSessionID();
        }

        public Open(String sid, int blockSize2) {
            super(sid, null);
            this.blockSize = blockSize2;
        }

        public int getBlockSize() {
            return this.blockSize;
        }

        public String getElementName() {
            return ELEMENT_NAME;
        }

        public String getChildElementXML() {
            StringBuilder buf = new StringBuilder();
            buf.append("<").append(getElementName()).append(" xmlns=\"").append(getNamespace()).append("\" ");
            buf.append("sid=\"").append(getSessionID()).append("\" ");
            buf.append("block-size=\"").append(getBlockSize()).append("\"");
            buf.append("/>");
            return buf.toString();
        }
    }

    public static class Data implements PacketExtension {
        public static final String ELEMENT_NAME = "data";
        private String data;
        private long seq;
        final String sid;

        public String getSessionID() {
            return this.sid;
        }

        public String getNamespace() {
            return "http://jabber.org/protocol/ibb";
        }

        public Data(String sid2) {
            this.sid = sid2;
        }

        public Data(String sid2, long seq2, String data2) {
            this(sid2);
            this.seq = seq2;
            this.data = data2;
        }

        public String getElementName() {
            return ELEMENT_NAME;
        }

        public String getData() {
            return this.data;
        }

        public void setData(String data2) {
            this.data = data2;
        }

        public long getSeq() {
            return this.seq;
        }

        public void setSeq(long seq2) {
            this.seq = seq2;
        }

        public String toXML() {
            StringBuilder buf = new StringBuilder();
            buf.append("<").append(getElementName()).append(" xmlns=\"").append(getNamespace()).append("\" ");
            buf.append("sid=\"").append(getSessionID()).append("\" ");
            buf.append("seq=\"").append(getSeq()).append("\"");
            buf.append(">");
            buf.append(getData());
            buf.append("</").append(getElementName()).append(">");
            return buf.toString();
        }
    }

    public static class Close extends IBB {
        public static final String ELEMENT_NAME = "close";

        public /* bridge */ /* synthetic */ String getNamespace() {
            return super.getNamespace();
        }

        public /* bridge */ /* synthetic */ String getSessionID() {
            return super.getSessionID();
        }

        public Close(String sid) {
            super(sid, null);
        }

        public String getElementName() {
            return ELEMENT_NAME;
        }

        public String getChildElementXML() {
            StringBuilder buf = new StringBuilder();
            buf.append("<").append(getElementName()).append(" xmlns=\"").append(getNamespace()).append("\" ");
            buf.append("sid=\"").append(getSessionID()).append("\"");
            buf.append("/>");
            return buf.toString();
        }
    }
}
