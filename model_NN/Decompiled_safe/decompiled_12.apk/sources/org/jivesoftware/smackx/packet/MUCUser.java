package org.jivesoftware.smackx.packet;

import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smackx.GroupChatInvitation;

public class MUCUser implements PacketExtension {
    private Decline decline;
    private Destroy destroy;
    private Invite invite;
    private Item item;
    private String password;
    private Status status;

    public String getElementName() {
        return GroupChatInvitation.ELEMENT_NAME;
    }

    public String getNamespace() {
        return "http://jabber.org/protocol/muc#user";
    }

    public String toXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append(getElementName()).append(" xmlns=\"").append(getNamespace()).append("\">");
        if (getInvite() != null) {
            buf.append(getInvite().toXML());
        }
        if (getDecline() != null) {
            buf.append(getDecline().toXML());
        }
        if (getItem() != null) {
            buf.append(getItem().toXML());
        }
        if (getPassword() != null) {
            buf.append("<password>").append(getPassword()).append("</password>");
        }
        if (getStatus() != null) {
            buf.append(getStatus().toXML());
        }
        if (getDestroy() != null) {
            buf.append(getDestroy().toXML());
        }
        buf.append("</").append(getElementName()).append(">");
        return buf.toString();
    }

    public Invite getInvite() {
        return this.invite;
    }

    public Decline getDecline() {
        return this.decline;
    }

    public Item getItem() {
        return this.item;
    }

    public String getPassword() {
        return this.password;
    }

    public Status getStatus() {
        return this.status;
    }

    public Destroy getDestroy() {
        return this.destroy;
    }

    public void setInvite(Invite invite2) {
        this.invite = invite2;
    }

    public void setDecline(Decline decline2) {
        this.decline = decline2;
    }

    public void setItem(Item item2) {
        this.item = item2;
    }

    public void setPassword(String string) {
        this.password = string;
    }

    public void setStatus(Status status2) {
        this.status = status2;
    }

    public void setDestroy(Destroy destroy2) {
        this.destroy = destroy2;
    }

    public static class Invite {
        private String from;
        private String reason;
        private String to;

        public String getFrom() {
            return this.from;
        }

        public String getReason() {
            return this.reason;
        }

        public String getTo() {
            return this.to;
        }

        public void setFrom(String from2) {
            this.from = from2;
        }

        public void setReason(String reason2) {
            this.reason = reason2;
        }

        public void setTo(String to2) {
            this.to = to2;
        }

        public String toXML() {
            StringBuilder buf = new StringBuilder();
            buf.append("<invite ");
            if (getTo() != null) {
                buf.append(" to=\"").append(getTo()).append("\"");
            }
            if (getFrom() != null) {
                buf.append(" from=\"").append(getFrom()).append("\"");
            }
            buf.append(">");
            if (getReason() != null) {
                buf.append("<reason>").append(getReason()).append("</reason>");
            }
            buf.append("</invite>");
            return buf.toString();
        }
    }

    public static class Decline {
        private String from;
        private String reason;
        private String to;

        public String getFrom() {
            return this.from;
        }

        public String getReason() {
            return this.reason;
        }

        public String getTo() {
            return this.to;
        }

        public void setFrom(String from2) {
            this.from = from2;
        }

        public void setReason(String reason2) {
            this.reason = reason2;
        }

        public void setTo(String to2) {
            this.to = to2;
        }

        public String toXML() {
            StringBuilder buf = new StringBuilder();
            buf.append("<decline ");
            if (getTo() != null) {
                buf.append(" to=\"").append(getTo()).append("\"");
            }
            if (getFrom() != null) {
                buf.append(" from=\"").append(getFrom()).append("\"");
            }
            buf.append(">");
            if (getReason() != null) {
                buf.append("<reason>").append(getReason()).append("</reason>");
            }
            buf.append("</decline>");
            return buf.toString();
        }
    }

    public static class Item {
        private String actor;
        private String affiliation;
        private String jid;
        private String nick;
        private String reason;
        private String role;

        public Item(String affiliation2, String role2) {
            this.affiliation = affiliation2;
            this.role = role2;
        }

        public String getActor() {
            return this.actor == null ? "" : this.actor;
        }

        public String getReason() {
            return this.reason == null ? "" : this.reason;
        }

        public String getAffiliation() {
            return this.affiliation;
        }

        public String getJid() {
            return this.jid;
        }

        public String getNick() {
            return this.nick;
        }

        public String getRole() {
            return this.role;
        }

        public void setActor(String actor2) {
            this.actor = actor2;
        }

        public void setReason(String reason2) {
            this.reason = reason2;
        }

        public void setJid(String jid2) {
            this.jid = jid2;
        }

        public void setNick(String nick2) {
            this.nick = nick2;
        }

        public String toXML() {
            StringBuilder buf = new StringBuilder();
            buf.append("<item");
            if (getAffiliation() != null) {
                buf.append(" affiliation=\"").append(getAffiliation()).append("\"");
            }
            if (getJid() != null) {
                buf.append(" jid=\"").append(getJid()).append("\"");
            }
            if (getNick() != null) {
                buf.append(" nick=\"").append(getNick()).append("\"");
            }
            if (getRole() != null) {
                buf.append(" role=\"").append(getRole()).append("\"");
            }
            if (getReason() == null && getActor() == null) {
                buf.append("/>");
            } else {
                buf.append(">");
                if (getReason() != null) {
                    buf.append("<reason>").append(getReason()).append("</reason>");
                }
                if (getActor() != null) {
                    buf.append("<actor jid=\"").append(getActor()).append("\"/>");
                }
                buf.append("</item>");
            }
            return buf.toString();
        }
    }

    public static class Status {
        private String code;

        public Status(String code2) {
            this.code = code2;
        }

        public String getCode() {
            return this.code;
        }

        public String toXML() {
            StringBuilder buf = new StringBuilder();
            buf.append("<status code=\"").append(getCode()).append("\"/>");
            return buf.toString();
        }
    }

    public static class Destroy {
        private String jid;
        private String reason;

        public String getJid() {
            return this.jid;
        }

        public String getReason() {
            return this.reason;
        }

        public void setJid(String jid2) {
            this.jid = jid2;
        }

        public void setReason(String reason2) {
            this.reason = reason2;
        }

        public String toXML() {
            StringBuilder buf = new StringBuilder();
            buf.append("<destroy");
            if (getJid() != null) {
                buf.append(" jid=\"").append(getJid()).append("\"");
            }
            if (getReason() == null) {
                buf.append("/>");
            } else {
                buf.append(">");
                if (getReason() != null) {
                    buf.append("<reason>").append(getReason()).append("</reason>");
                }
                buf.append("</destroy>");
            }
            return buf.toString();
        }
    }
}
