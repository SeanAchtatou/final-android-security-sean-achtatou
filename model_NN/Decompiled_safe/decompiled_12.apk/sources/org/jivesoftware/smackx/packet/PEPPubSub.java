package org.jivesoftware.smackx.packet;

import org.jivesoftware.smack.packet.IQ;

public class PEPPubSub extends IQ {
    PEPItem item;

    public PEPPubSub(PEPItem item2) {
        this.item = item2;
    }

    public String getElementName() {
        return "pubsub";
    }

    public String getNamespace() {
        return "http://jabber.org/protocol/pubsub";
    }

    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append(getElementName()).append(" xmlns=\"").append(getNamespace()).append("\">");
        buf.append("<publish node=\"").append(this.item.getNode()).append("\">");
        buf.append(this.item.toXML());
        buf.append("</publish>");
        buf.append("</").append(getElementName()).append(">");
        return buf.toString();
    }
}
