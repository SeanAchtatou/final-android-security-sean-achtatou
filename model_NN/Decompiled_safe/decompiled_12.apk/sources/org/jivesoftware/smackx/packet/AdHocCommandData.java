package org.jivesoftware.smackx.packet;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smackx.commands.AdHocCommand;
import org.jivesoftware.smackx.commands.AdHocCommandNote;

public class AdHocCommandData extends IQ {
    private AdHocCommand.Action action;
    private ArrayList<AdHocCommand.Action> actions = new ArrayList<>();
    private AdHocCommand.Action executeAction;
    private DataForm form;
    private String id;
    private String lang;
    private String name;
    private String node;
    private List<AdHocCommandNote> notes = new ArrayList();
    private String sessionID;
    private AdHocCommand.Status status;

    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<command xmlns=\"http://jabber.org/protocol/commands\"");
        buf.append(" node=\"").append(this.node).append("\"");
        if (this.sessionID != null && !this.sessionID.equals("")) {
            buf.append(" sessionid=\"").append(this.sessionID).append("\"");
        }
        if (this.status != null) {
            buf.append(" status=\"").append(this.status).append("\"");
        }
        if (this.action != null) {
            buf.append(" action=\"").append(this.action).append("\"");
        }
        if (this.lang != null && !this.lang.equals("")) {
            buf.append(" lang=\"").append(this.lang).append("\"");
        }
        buf.append(">");
        if (getType() == IQ.Type.RESULT) {
            buf.append("<actions");
            if (this.executeAction != null) {
                buf.append(" execute=\"").append(this.executeAction).append("\"");
            }
            if (this.actions.size() == 0) {
                buf.append("/>");
            } else {
                buf.append(">");
                Iterator<AdHocCommand.Action> it = this.actions.iterator();
                while (it.hasNext()) {
                    buf.append("<").append(it.next()).append("/>");
                }
                buf.append("</actions>");
            }
        }
        if (this.form != null) {
            buf.append(this.form.toXML());
        }
        for (AdHocCommandNote note : this.notes) {
            buf.append("<note type=\"").append(note.getType().toString()).append("\">");
            buf.append(note.getValue());
            buf.append("</note>");
        }
        buf.append("</command>");
        return buf.toString();
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getNode() {
        return this.node;
    }

    public void setNode(String node2) {
        this.node = node2;
    }

    public List<AdHocCommandNote> getNotes() {
        return this.notes;
    }

    public void addNote(AdHocCommandNote note) {
        this.notes.add(note);
    }

    public void remveNote(AdHocCommandNote note) {
        this.notes.remove(note);
    }

    public DataForm getForm() {
        return this.form;
    }

    public void setForm(DataForm form2) {
        this.form = form2;
    }

    public AdHocCommand.Action getAction() {
        return this.action;
    }

    public void setAction(AdHocCommand.Action action2) {
        this.action = action2;
    }

    public AdHocCommand.Status getStatus() {
        return this.status;
    }

    public void setStatus(AdHocCommand.Status status2) {
        this.status = status2;
    }

    public List<AdHocCommand.Action> getActions() {
        return this.actions;
    }

    public void addAction(AdHocCommand.Action action2) {
        this.actions.add(action2);
    }

    public void setExecuteAction(AdHocCommand.Action executeAction2) {
        this.executeAction = executeAction2;
    }

    public AdHocCommand.Action getExecuteAction() {
        return this.executeAction;
    }

    public void setSessionID(String sessionID2) {
        this.sessionID = sessionID2;
    }

    public String getSessionID() {
        return this.sessionID;
    }

    public static class SpecificError implements PacketExtension {
        public static final String namespace = "http://jabber.org/protocol/commands";
        public AdHocCommand.SpecificErrorCondition condition;

        public SpecificError(AdHocCommand.SpecificErrorCondition condition2) {
            this.condition = condition2;
        }

        public String getElementName() {
            return this.condition.toString();
        }

        public String getNamespace() {
            return namespace;
        }

        public AdHocCommand.SpecificErrorCondition getCondition() {
            return this.condition;
        }

        public String toXML() {
            StringBuilder buf = new StringBuilder();
            buf.append("<").append(getElementName());
            buf.append(" xmlns=\"").append(getNamespace()).append("\"/>");
            return buf.toString();
        }
    }
}
