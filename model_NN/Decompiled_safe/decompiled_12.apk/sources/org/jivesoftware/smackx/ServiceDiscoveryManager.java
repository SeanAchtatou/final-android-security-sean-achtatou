package org.jivesoftware.smackx;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.ConnectionCreationListener;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.PacketCollector;
import org.jivesoftware.smack.PacketInterceptor;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketIDFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.XMPPError;
import org.jivesoftware.smackx.packet.CapsExtension;
import org.jivesoftware.smackx.packet.DataForm;
import org.jivesoftware.smackx.packet.DiscoverInfo;
import org.jivesoftware.smackx.packet.DiscoverItems;

public class ServiceDiscoveryManager {
    private static boolean cacheNonCaps = true;
    private static String entityNode = "http://www.igniterealtime.org/projects/smack/";
    private static String identityName = "Smack";
    private static String identityType = "pc";
    /* access modifiers changed from: private */
    public static Map<Connection, ServiceDiscoveryManager> instances = new ConcurrentHashMap();
    /* access modifiers changed from: private */
    public EntityCapsManager capsManager;
    /* access modifiers changed from: private */
    public Connection connection;
    private String currentCapsVersion = null;
    private DataForm extendedInfo = null;
    private final List<String> features = new ArrayList();
    private Map<String, NodeInformationProvider> nodeInformationProviders = new ConcurrentHashMap();
    private Map<String, DiscoverInfo> nonCapsCache = new ConcurrentHashMap();
    private boolean sendPresence = false;

    static {
        XMPPConnection.addConnectionCreationListener(new ConnectionCreationListener() {
            public void connectionCreated(Connection connection) {
                new ServiceDiscoveryManager(connection);
            }
        });
    }

    public ServiceDiscoveryManager(Connection connection2) {
        this.connection = connection2;
        if (connection2 instanceof XMPPConnection) {
            setEntityCapsManager(new EntityCapsManager());
            this.capsManager.addCapsVerListener(new CapsPresenceRenewer(this, null));
        }
        renewEntityCapsVersion();
        init();
    }

    public static ServiceDiscoveryManager getInstanceFor(Connection connection2) {
        return instances.get(connection2);
    }

    public static String getIdentityName() {
        return identityName;
    }

    public static void setIdentityName(String name) {
        identityName = name;
    }

    public static String getIdentityType() {
        return identityType;
    }

    public static void setIdentityType(String type) {
        identityType = type;
    }

    public static void setNonCapsCaching(boolean set) {
        cacheNonCaps = true;
    }

    public static boolean isNonCapsCachingEnabled() {
        return cacheNonCaps;
    }

    public void addDiscoverInfoTo(DiscoverInfo response) {
        DiscoverInfo.Identity identity = new DiscoverInfo.Identity("client", getIdentityName());
        identity.setType(getIdentityType());
        response.addIdentity(identity);
        synchronized (this.features) {
            response.addFeature(CapsExtension.XMLNS);
            Iterator<String> it = getFeatures();
            while (it.hasNext()) {
                response.addFeature(it.next());
            }
            if (this.extendedInfo != null) {
                response.addExtension(this.extendedInfo);
            }
        }
    }

    public DiscoverInfo getOwnDiscoverInfo() {
        DiscoverInfo di = new DiscoverInfo();
        di.setType(IQ.Type.RESULT);
        di.setNode(String.valueOf(this.capsManager.getNode()) + "#" + getEntityCapsVersion());
        addDiscoverInfoTo(di);
        return di;
    }

    private void init() {
        instances.put(this.connection, this);
        this.connection.addConnectionListener(new ConnectionListener() {
            public void connectionClosed() {
                ServiceDiscoveryManager.instances.remove(ServiceDiscoveryManager.this.connection);
            }

            public void connectionClosedOnError(Exception e) {
            }

            public void reconnectionFailed(Exception e) {
            }

            public void reconnectingIn(int seconds) {
            }

            public void reconnectionSuccessful() {
            }
        });
        PacketFilter capsPacketFilter = new PacketTypeFilter(Presence.class);
        this.connection.addPacketInterceptor(new PacketInterceptor() {
            public void interceptPacket(Packet packet) {
                if (ServiceDiscoveryManager.this.capsManager != null) {
                    packet.addExtension(new CapsExtension(ServiceDiscoveryManager.this.capsManager.getNode(), ServiceDiscoveryManager.this.getEntityCapsVersion(), EntityCapsManager.HASH_METHOD));
                }
            }
        }, capsPacketFilter);
        PacketFilter packetFilter = new PacketTypeFilter(DiscoverItems.class);
        this.connection.addPacketListener(new PacketListener() {
            public void processPacket(Packet packet) {
                DiscoverItems discoverItems = (DiscoverItems) packet;
                if (discoverItems != null && discoverItems.getType() == IQ.Type.GET) {
                    DiscoverItems response = new DiscoverItems();
                    response.setType(IQ.Type.RESULT);
                    response.setTo(discoverItems.getFrom());
                    response.setPacketID(discoverItems.getPacketID());
                    response.setNode(discoverItems.getNode());
                    NodeInformationProvider nodeInformationProvider = ServiceDiscoveryManager.this.getNodeInformationProvider(discoverItems.getNode());
                    if (nodeInformationProvider != null) {
                        List<DiscoverItems.Item> items = nodeInformationProvider.getNodeItems();
                        if (items != null) {
                            for (DiscoverItems.Item item : items) {
                                response.addItem(item);
                            }
                        }
                    } else if (discoverItems.getNode() != null) {
                        response.setType(IQ.Type.ERROR);
                        response.setError(new XMPPError(XMPPError.Condition.item_not_found));
                    }
                    ServiceDiscoveryManager.this.connection.sendPacket(response);
                }
            }
        }, packetFilter);
        PacketFilter packetFilter2 = new PacketTypeFilter(DiscoverInfo.class);
        this.connection.addPacketListener(new PacketListener() {
            public void processPacket(Packet packet) {
                DiscoverInfo discoverInfo = (DiscoverInfo) packet;
                if (discoverInfo != null && discoverInfo.getType() == IQ.Type.GET) {
                    DiscoverInfo response = new DiscoverInfo();
                    response.setType(IQ.Type.RESULT);
                    response.setTo(discoverInfo.getFrom());
                    response.setPacketID(discoverInfo.getPacketID());
                    response.setNode(discoverInfo.getNode());
                    if (discoverInfo.getNode() == null || ServiceDiscoveryManager.this.capsManager == null || (String.valueOf(ServiceDiscoveryManager.this.capsManager.getNode()) + "#" + ServiceDiscoveryManager.this.getEntityCapsVersion()).equals(discoverInfo.getNode())) {
                        ServiceDiscoveryManager.this.addDiscoverInfoTo(response);
                    } else {
                        NodeInformationProvider nodeInformationProvider = ServiceDiscoveryManager.this.getNodeInformationProvider(discoverInfo.getNode());
                        if (nodeInformationProvider != null) {
                            List<String> features = nodeInformationProvider.getNodeFeatures();
                            if (features != null) {
                                for (String feature : features) {
                                    response.addFeature(feature);
                                }
                            }
                            List<DiscoverInfo.Identity> identities = nodeInformationProvider.getNodeIdentities();
                            if (identities != null) {
                                for (DiscoverInfo.Identity identity : identities) {
                                    response.addIdentity(identity);
                                }
                            }
                        } else {
                            response.setType(IQ.Type.ERROR);
                            response.setError(new XMPPError(XMPPError.Condition.item_not_found));
                        }
                    }
                    ServiceDiscoveryManager.this.connection.sendPacket(response);
                }
            }
        }, packetFilter2);
    }

    /* access modifiers changed from: private */
    public NodeInformationProvider getNodeInformationProvider(String node) {
        if (node == null) {
            return null;
        }
        return this.nodeInformationProviders.get(node);
    }

    public void setNodeInformationProvider(String node, NodeInformationProvider listener) {
        this.nodeInformationProviders.put(node, listener);
    }

    public void removeNodeInformationProvider(String node) {
        this.nodeInformationProviders.remove(node);
    }

    public Iterator<String> getFeatures() {
        Iterator<String> it;
        synchronized (this.features) {
            it = Collections.unmodifiableList(new ArrayList(this.features)).iterator();
        }
        return it;
    }

    public void addFeature(String feature) {
        synchronized (this.features) {
            this.features.add(feature);
            renewEntityCapsVersion();
        }
    }

    public void removeFeature(String feature) {
        synchronized (this.features) {
            this.features.remove(feature);
            renewEntityCapsVersion();
        }
    }

    public boolean includesFeature(String feature) {
        boolean contains;
        synchronized (this.features) {
            contains = this.features.contains(feature);
        }
        return contains;
    }

    public void setExtendedInfo(DataForm info) {
        this.extendedInfo = info;
        renewEntityCapsVersion();
    }

    public void removeExtendedInfo() {
        this.extendedInfo = null;
        renewEntityCapsVersion();
    }

    public DiscoverInfo discoverInfoByCaps(String entityID) throws XMPPException {
        DiscoverInfo info = this.capsManager.getDiscoverInfoByUser(entityID);
        if (info == null) {
            return null;
        }
        DiscoverInfo newInfo = cloneDiscoverInfo(info);
        newInfo.setFrom(entityID);
        return newInfo;
    }

    public DiscoverInfo discoverInfo(String entityID) throws XMPPException {
        DiscoverInfo info = discoverInfoByCaps(entityID);
        if (info != null) {
            return info;
        }
        String node = null;
        if (this.capsManager != null) {
            node = this.capsManager.getNodeVersionByUser(entityID);
        }
        if (cacheNonCaps && node == null && this.nonCapsCache.containsKey(entityID)) {
            return this.nonCapsCache.get(entityID);
        }
        DiscoverInfo info2 = discoverInfo(entityID, node);
        if (node != null && this.capsManager != null) {
            EntityCapsManager.addDiscoverInfoByNode(node, info2);
        } else if (cacheNonCaps && node == null) {
            this.nonCapsCache.put(entityID, info2);
        }
        return info2;
    }

    public DiscoverInfo discoverInfo(String entityID, String node) throws XMPPException {
        DiscoverInfo disco = new DiscoverInfo();
        disco.setType(IQ.Type.GET);
        disco.setTo(entityID);
        disco.setNode(node);
        PacketCollector collector = this.connection.createPacketCollector(new PacketIDFilter(disco.getPacketID()));
        this.connection.sendPacket(disco);
        IQ result = (IQ) collector.nextResult((long) SmackConfiguration.getPacketReplyTimeout());
        collector.cancel();
        if (result == null) {
            throw new XMPPException("No response from the server.");
        } else if (result.getType() != IQ.Type.ERROR) {
            return (DiscoverInfo) result;
        } else {
            throw new XMPPException(result.getError());
        }
    }

    public DiscoverItems discoverItems(String entityID) throws XMPPException {
        return discoverItems(entityID, null);
    }

    public DiscoverItems discoverItems(String entityID, String node) throws XMPPException {
        DiscoverItems disco = new DiscoverItems();
        disco.setType(IQ.Type.GET);
        disco.setTo(entityID);
        disco.setNode(node);
        PacketCollector collector = this.connection.createPacketCollector(new PacketIDFilter(disco.getPacketID()));
        this.connection.sendPacket(disco);
        IQ result = (IQ) collector.nextResult((long) SmackConfiguration.getPacketReplyTimeout());
        collector.cancel();
        if (result == null) {
            throw new XMPPException("No response from the server.");
        } else if (result.getType() != IQ.Type.ERROR) {
            return (DiscoverItems) result;
        } else {
            throw new XMPPException(result.getError());
        }
    }

    public boolean canPublishItems(String entityID) throws XMPPException {
        return canPublishItems(discoverInfo(entityID));
    }

    public static boolean canPublishItems(DiscoverInfo info) {
        return info.containsFeature("http://jabber.org/protocol/disco#publish");
    }

    public void publishItems(String entityID, DiscoverItems discoverItems) throws XMPPException {
        publishItems(entityID, null, discoverItems);
    }

    public void publishItems(String entityID, String node, DiscoverItems discoverItems) throws XMPPException {
        discoverItems.setType(IQ.Type.SET);
        discoverItems.setTo(entityID);
        discoverItems.setNode(node);
        PacketCollector collector = this.connection.createPacketCollector(new PacketIDFilter(discoverItems.getPacketID()));
        this.connection.sendPacket(discoverItems);
        IQ result = (IQ) collector.nextResult((long) SmackConfiguration.getPacketReplyTimeout());
        collector.cancel();
        if (result == null) {
            throw new XMPPException("No response from the server.");
        } else if (result.getType() == IQ.Type.ERROR) {
            throw new XMPPException(result.getError());
        }
    }

    private DiscoverInfo cloneDiscoverInfo(DiscoverInfo disco) {
        return disco.clone();
    }

    public void setEntityCapsManager(EntityCapsManager manager) {
        this.capsManager = manager;
        if (!(this.connection.getCapsNode() == null || this.connection.getHost() == null)) {
            this.capsManager.addUserCapsNode(this.connection.getHost(), this.connection.getCapsNode());
        }
        this.capsManager.addPacketListener(this.connection);
    }

    private void renewEntityCapsVersion() {
        if ((this.connection instanceof XMPPConnection) && this.capsManager != null) {
            this.capsManager.calculateEntityCapsVersion(getOwnDiscoverInfo(), identityType, identityName, this.features, this.extendedInfo);
        }
    }

    /* access modifiers changed from: private */
    public String getEntityCapsVersion() {
        if (this.capsManager != null) {
            return this.capsManager.getCapsVersion();
        }
        return null;
    }

    public EntityCapsManager getEntityCapsManager() {
        return this.capsManager;
    }

    private void setSendPresence() {
        this.sendPresence = true;
    }

    /* access modifiers changed from: private */
    public boolean isSendPresence() {
        return this.sendPresence;
    }

    private class CapsPresenceRenewer implements CapsVerListener {
        private CapsPresenceRenewer() {
        }

        /* synthetic */ CapsPresenceRenewer(ServiceDiscoveryManager serviceDiscoveryManager, CapsPresenceRenewer capsPresenceRenewer) {
            this();
        }

        public void capsVerUpdated(String ver) {
            if (!((XMPPConnection) ServiceDiscoveryManager.this.connection).isAuthenticated()) {
                return;
            }
            if (((XMPPConnection) ServiceDiscoveryManager.this.connection).isSendPresence() || ServiceDiscoveryManager.this.isSendPresence()) {
                ServiceDiscoveryManager.this.connection.sendPacket(new Presence(Presence.Type.available));
            }
        }
    }
}
