package org.jivesoftware.smackx;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.packet.DiscoverInfo;
import org.jivesoftware.smackx.packet.DiscoverItems;

public class GatewayManager {
    private static Map<Connection, GatewayManager> instances = new HashMap();
    private Connection connection;
    private Map<String, Gateway> gateways = new HashMap();
    private Map<String, Gateway> localGateways = new HashMap();
    private Map<String, Gateway> nonLocalGateways = new HashMap();
    private Roster roster;
    private ServiceDiscoveryManager sdManager;

    private GatewayManager() {
    }

    private GatewayManager(Connection connection2) throws XMPPException {
        this.connection = connection2;
        this.roster = connection2.getRoster();
        this.sdManager = ServiceDiscoveryManager.getInstanceFor(connection2);
    }

    private void loadLocalGateways() throws XMPPException {
        Iterator<DiscoverItems.Item> iter = this.sdManager.discoverItems(this.connection.getHost()).getItems();
        while (iter.hasNext()) {
            discoverGateway(iter.next().getEntityID());
        }
    }

    private void discoverGateway(String itemJID) throws XMPPException {
        DiscoverInfo info = this.sdManager.discoverInfo(itemJID);
        Iterator<DiscoverInfo.Identity> i = info.getIdentities();
        while (i.hasNext()) {
            DiscoverInfo.Identity identity = i.next();
            if (identity.getCategory().toLowerCase().equals("gateway")) {
                this.gateways.put(itemJID, new Gateway(this.connection, itemJID));
                if (itemJID.contains(this.connection.getHost())) {
                    this.localGateways.put(itemJID, new Gateway(this.connection, itemJID, info, identity));
                    return;
                } else {
                    this.nonLocalGateways.put(itemJID, new Gateway(this.connection, itemJID, info, identity));
                    return;
                }
            }
        }
    }

    private void loadNonLocalGateways() throws XMPPException {
        if (this.roster != null) {
            for (RosterEntry entry : this.roster.getEntries()) {
                if (entry.getUser().equalsIgnoreCase(StringUtils.parseServer(entry.getUser())) && !entry.getUser().contains(this.connection.getHost())) {
                    discoverGateway(entry.getUser());
                }
            }
        }
    }

    public GatewayManager getInstanceFor(Connection connection2) throws XMPPException {
        synchronized (instances) {
            if (instances.containsKey(connection2)) {
                GatewayManager gatewayManager = instances.get(connection2);
                return gatewayManager;
            }
            GatewayManager instance = new GatewayManager(connection2);
            instances.put(connection2, instance);
            return instance;
        }
    }

    public List<Gateway> getLocalGateways() throws XMPPException {
        if (this.localGateways.size() == 0) {
            loadLocalGateways();
        }
        return new ArrayList(this.localGateways.values());
    }

    public List<Gateway> getNonLocalGateways() throws XMPPException {
        if (this.nonLocalGateways.size() == 0) {
            loadNonLocalGateways();
        }
        return new ArrayList(this.nonLocalGateways.values());
    }

    public void refreshNonLocalGateways() throws XMPPException {
        loadNonLocalGateways();
    }

    public Gateway getGateway(String entityJID) {
        if (this.localGateways.containsKey(entityJID)) {
            return this.localGateways.get(entityJID);
        }
        if (this.nonLocalGateways.containsKey(entityJID)) {
            return this.nonLocalGateways.get(entityJID);
        }
        if (this.gateways.containsKey(entityJID)) {
            return this.gateways.get(entityJID);
        }
        Gateway gateway = new Gateway(this.connection, entityJID);
        if (entityJID.contains(this.connection.getHost())) {
            this.localGateways.put(entityJID, gateway);
        } else {
            this.nonLocalGateways.put(entityJID, gateway);
        }
        this.gateways.put(entityJID, gateway);
        return gateway;
    }
}
