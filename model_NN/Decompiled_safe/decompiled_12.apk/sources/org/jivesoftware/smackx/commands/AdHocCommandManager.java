package org.jivesoftware.smackx.commands;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.ConnectionCreationListener;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.XMPPError;
import org.jivesoftware.smackx.NodeInformationProvider;
import org.jivesoftware.smackx.ServiceDiscoveryManager;
import org.jivesoftware.smackx.commands.AdHocCommand;
import org.jivesoftware.smackx.packet.AdHocCommandData;
import org.jivesoftware.smackx.packet.DiscoverInfo;
import org.jivesoftware.smackx.packet.DiscoverItems;

public class AdHocCommandManager {
    private static final String DISCO_NAMESPACE = "http://jabber.org/protocol/commands";
    private static final int SESSION_TIMEOUT = 120;
    private static final String discoNode = "http://jabber.org/protocol/commands";
    /* access modifiers changed from: private */
    public static Map<Connection, AdHocCommandManager> instances = new ConcurrentHashMap();
    private Map<String, AdHocCommandInfo> commands;
    /* access modifiers changed from: private */
    public Connection connection;
    /* access modifiers changed from: private */
    public Map<String, LocalCommand> executingCommands;
    private Thread sessionsSweeper;

    static {
        Connection.addConnectionCreationListener(new ConnectionCreationListener() {
            public void connectionCreated(Connection connection) {
                new AdHocCommandManager(connection, null);
            }
        });
    }

    public static AdHocCommandManager getAddHocCommandsManager(Connection connection2) {
        return instances.get(connection2);
    }

    /* synthetic */ AdHocCommandManager(Connection connection2, AdHocCommandManager adHocCommandManager) {
        this(connection2);
    }

    private AdHocCommandManager(Connection connection2) {
        this.commands = new ConcurrentHashMap();
        this.executingCommands = new ConcurrentHashMap();
        this.connection = connection2;
        init();
    }

    public void registerCommand(String node, String name, final Class clazz) {
        registerCommand(node, name, new LocalCommandFactory() {
            public LocalCommand getInstance() throws InstantiationException, IllegalAccessException {
                return (LocalCommand) clazz.newInstance();
            }
        });
    }

    public void registerCommand(String node, final String name, LocalCommandFactory factory) {
        this.commands.put(node, new AdHocCommandInfo(node, name, this.connection.getUser(), factory));
        ServiceDiscoveryManager.getInstanceFor(this.connection).setNodeInformationProvider(node, new NodeInformationProvider() {
            public List<DiscoverItems.Item> getNodeItems() {
                return null;
            }

            public List<String> getNodeFeatures() {
                List<String> answer = new ArrayList<>();
                answer.add(AdHocCommandData.SpecificError.namespace);
                answer.add("jabber:x:data");
                return answer;
            }

            public List<DiscoverInfo.Identity> getNodeIdentities() {
                List<DiscoverInfo.Identity> answer = new ArrayList<>();
                DiscoverInfo.Identity identity = new DiscoverInfo.Identity("automation", name);
                identity.setType("command-node");
                answer.add(identity);
                return answer;
            }
        });
    }

    public DiscoverItems discoverCommands(String jid) throws XMPPException {
        return ServiceDiscoveryManager.getInstanceFor(this.connection).discoverItems(jid, AdHocCommandData.SpecificError.namespace);
    }

    public void publishCommands(String jid) throws XMPPException {
        ServiceDiscoveryManager serviceDiscoveryManager = ServiceDiscoveryManager.getInstanceFor(this.connection);
        DiscoverItems discoverItems = new DiscoverItems();
        for (AdHocCommandInfo info : getRegisteredCommands()) {
            DiscoverItems.Item item = new DiscoverItems.Item(info.getOwnerJID());
            item.setName(info.getName());
            item.setNode(info.getNode());
            discoverItems.addItem(item);
        }
        serviceDiscoveryManager.publishItems(jid, AdHocCommandData.SpecificError.namespace, discoverItems);
    }

    public RemoteCommand getRemoteCommand(String jid, String node) {
        return new RemoteCommand(this.connection, node, jid);
    }

    private void init() {
        instances.put(this.connection, this);
        this.connection.addConnectionListener(new ConnectionListener() {
            public void connectionClosed() {
                AdHocCommandManager.instances.remove(AdHocCommandManager.this.connection);
            }

            public void connectionClosedOnError(Exception e) {
                AdHocCommandManager.instances.remove(AdHocCommandManager.this.connection);
            }

            public void reconnectionSuccessful() {
                AdHocCommandManager.instances.put(AdHocCommandManager.this.connection, AdHocCommandManager.this);
            }

            public void reconnectingIn(int seconds) {
            }

            public void reconnectionFailed(Exception e) {
            }
        });
        ServiceDiscoveryManager.getInstanceFor(this.connection).addFeature(AdHocCommandData.SpecificError.namespace);
        ServiceDiscoveryManager.getInstanceFor(this.connection).setNodeInformationProvider(AdHocCommandData.SpecificError.namespace, new NodeInformationProvider() {
            public List<DiscoverItems.Item> getNodeItems() {
                List<DiscoverItems.Item> answer = new ArrayList<>();
                for (AdHocCommandInfo info : AdHocCommandManager.this.getRegisteredCommands()) {
                    DiscoverItems.Item item = new DiscoverItems.Item(info.getOwnerJID());
                    item.setName(info.getName());
                    item.setNode(info.getNode());
                    answer.add(item);
                }
                return answer;
            }

            public List<String> getNodeFeatures() {
                return null;
            }

            public List<DiscoverInfo.Identity> getNodeIdentities() {
                return null;
            }
        });
        this.connection.addPacketListener(new PacketListener() {
            public void processPacket(Packet packet) {
                AdHocCommandManager.this.processAdHocCommand((AdHocCommandData) packet);
            }
        }, new PacketTypeFilter(AdHocCommandData.class));
        this.sessionsSweeper = new Thread(new Runnable() {
            public void run() {
                while (true) {
                    for (String sessionId : AdHocCommandManager.this.executingCommands.keySet()) {
                        LocalCommand command = (LocalCommand) AdHocCommandManager.this.executingCommands.get(sessionId);
                        if (command != null) {
                            if (System.currentTimeMillis() - command.getCreationDate() > 240000) {
                                AdHocCommandManager.this.executingCommands.remove(sessionId);
                            }
                        }
                    }
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                    }
                }
            }
        });
        this.sessionsSweeper.setDaemon(true);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0135, code lost:
        if (org.jivesoftware.smackx.commands.AdHocCommand.Action.execute.equals(r0) != false) goto L_0x0137;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void processAdHocCommand(org.jivesoftware.smackx.packet.AdHocCommandData r14) {
        /*
            r13 = this;
            org.jivesoftware.smack.packet.IQ$Type r9 = r14.getType()
            org.jivesoftware.smack.packet.IQ$Type r10 = org.jivesoftware.smack.packet.IQ.Type.SET
            if (r9 == r10) goto L_0x0009
        L_0x0008:
            return
        L_0x0009:
            org.jivesoftware.smackx.packet.AdHocCommandData r7 = new org.jivesoftware.smackx.packet.AdHocCommandData
            r7.<init>()
            java.lang.String r9 = r14.getFrom()
            r7.setTo(r9)
            java.lang.String r9 = r14.getPacketID()
            r7.setPacketID(r9)
            java.lang.String r9 = r14.getNode()
            r7.setNode(r9)
            java.lang.String r9 = r14.getTo()
            r7.setId(r9)
            java.lang.String r8 = r14.getSessionID()
            java.lang.String r2 = r14.getNode()
            if (r8 != 0) goto L_0x00e0
            java.util.Map<java.lang.String, org.jivesoftware.smackx.commands.AdHocCommandManager$AdHocCommandInfo> r9 = r13.commands
            boolean r9 = r9.containsKey(r2)
            if (r9 != 0) goto L_0x0042
            org.jivesoftware.smack.packet.XMPPError$Condition r9 = org.jivesoftware.smack.packet.XMPPError.Condition.item_not_found
            r13.respondError(r7, r9)
            goto L_0x0008
        L_0x0042:
            r9 = 15
            java.lang.String r8 = org.jivesoftware.smack.util.StringUtils.randomString(r9)
            org.jivesoftware.smackx.commands.LocalCommand r1 = r13.newInstanceOfCmd(r2, r8)     // Catch:{ XMPPException -> 0x0064 }
            org.jivesoftware.smack.packet.IQ$Type r9 = org.jivesoftware.smack.packet.IQ.Type.RESULT     // Catch:{ XMPPException -> 0x0064 }
            r7.setType(r9)     // Catch:{ XMPPException -> 0x0064 }
            r1.setData(r7)     // Catch:{ XMPPException -> 0x0064 }
            java.lang.String r9 = r14.getFrom()     // Catch:{ XMPPException -> 0x0064 }
            boolean r9 = r1.hasPermission(r9)     // Catch:{ XMPPException -> 0x0064 }
            if (r9 != 0) goto L_0x0086
            org.jivesoftware.smack.packet.XMPPError$Condition r9 = org.jivesoftware.smack.packet.XMPPError.Condition.forbidden     // Catch:{ XMPPException -> 0x0064 }
            r13.respondError(r7, r9)     // Catch:{ XMPPException -> 0x0064 }
            goto L_0x0008
        L_0x0064:
            r5 = move-exception
            org.jivesoftware.smack.packet.XMPPError r6 = r5.getXMPPError()
            org.jivesoftware.smack.packet.XMPPError$Type r9 = org.jivesoftware.smack.packet.XMPPError.Type.CANCEL
            org.jivesoftware.smack.packet.XMPPError$Type r10 = r6.getType()
            boolean r9 = r9.equals(r10)
            if (r9 == 0) goto L_0x007f
            org.jivesoftware.smackx.commands.AdHocCommand$Status r9 = org.jivesoftware.smackx.commands.AdHocCommand.Status.canceled
            r7.setStatus(r9)
            java.util.Map<java.lang.String, org.jivesoftware.smackx.commands.LocalCommand> r9 = r13.executingCommands
            r9.remove(r8)
        L_0x007f:
            r13.respondError(r7, r6)
            r5.printStackTrace()
            goto L_0x0008
        L_0x0086:
            org.jivesoftware.smackx.commands.AdHocCommand$Action r0 = r14.getAction()     // Catch:{ XMPPException -> 0x0064 }
            if (r0 == 0) goto L_0x009d
            org.jivesoftware.smackx.commands.AdHocCommand$Action r9 = org.jivesoftware.smackx.commands.AdHocCommand.Action.unknown     // Catch:{ XMPPException -> 0x0064 }
            boolean r9 = r0.equals(r9)     // Catch:{ XMPPException -> 0x0064 }
            if (r9 == 0) goto L_0x009d
            org.jivesoftware.smack.packet.XMPPError$Condition r9 = org.jivesoftware.smack.packet.XMPPError.Condition.bad_request     // Catch:{ XMPPException -> 0x0064 }
            org.jivesoftware.smackx.commands.AdHocCommand$SpecificErrorCondition r10 = org.jivesoftware.smackx.commands.AdHocCommand.SpecificErrorCondition.malformedAction     // Catch:{ XMPPException -> 0x0064 }
            r13.respondError(r7, r9, r10)     // Catch:{ XMPPException -> 0x0064 }
            goto L_0x0008
        L_0x009d:
            if (r0 == 0) goto L_0x00b0
            org.jivesoftware.smackx.commands.AdHocCommand$Action r9 = org.jivesoftware.smackx.commands.AdHocCommand.Action.execute     // Catch:{ XMPPException -> 0x0064 }
            boolean r9 = r0.equals(r9)     // Catch:{ XMPPException -> 0x0064 }
            if (r9 != 0) goto L_0x00b0
            org.jivesoftware.smack.packet.XMPPError$Condition r9 = org.jivesoftware.smack.packet.XMPPError.Condition.bad_request     // Catch:{ XMPPException -> 0x0064 }
            org.jivesoftware.smackx.commands.AdHocCommand$SpecificErrorCondition r10 = org.jivesoftware.smackx.commands.AdHocCommand.SpecificErrorCondition.badAction     // Catch:{ XMPPException -> 0x0064 }
            r13.respondError(r7, r9, r10)     // Catch:{ XMPPException -> 0x0064 }
            goto L_0x0008
        L_0x00b0:
            r1.incrementStage()     // Catch:{ XMPPException -> 0x0064 }
            r1.execute()     // Catch:{ XMPPException -> 0x0064 }
            boolean r9 = r1.isLastStage()     // Catch:{ XMPPException -> 0x0064 }
            if (r9 == 0) goto L_0x00c8
            org.jivesoftware.smackx.commands.AdHocCommand$Status r9 = org.jivesoftware.smackx.commands.AdHocCommand.Status.completed     // Catch:{ XMPPException -> 0x0064 }
            r7.setStatus(r9)     // Catch:{ XMPPException -> 0x0064 }
        L_0x00c1:
            org.jivesoftware.smack.Connection r9 = r13.connection     // Catch:{ XMPPException -> 0x0064 }
            r9.sendPacket(r7)     // Catch:{ XMPPException -> 0x0064 }
            goto L_0x0008
        L_0x00c8:
            org.jivesoftware.smackx.commands.AdHocCommand$Status r9 = org.jivesoftware.smackx.commands.AdHocCommand.Status.executing     // Catch:{ XMPPException -> 0x0064 }
            r7.setStatus(r9)     // Catch:{ XMPPException -> 0x0064 }
            java.util.Map<java.lang.String, org.jivesoftware.smackx.commands.LocalCommand> r9 = r13.executingCommands     // Catch:{ XMPPException -> 0x0064 }
            r9.put(r8, r1)     // Catch:{ XMPPException -> 0x0064 }
            java.lang.Thread r9 = r13.sessionsSweeper     // Catch:{ XMPPException -> 0x0064 }
            boolean r9 = r9.isAlive()     // Catch:{ XMPPException -> 0x0064 }
            if (r9 != 0) goto L_0x00c1
            java.lang.Thread r9 = r13.sessionsSweeper     // Catch:{ XMPPException -> 0x0064 }
            r9.start()     // Catch:{ XMPPException -> 0x0064 }
            goto L_0x00c1
        L_0x00e0:
            java.util.Map<java.lang.String, org.jivesoftware.smackx.commands.LocalCommand> r9 = r13.executingCommands
            java.lang.Object r1 = r9.get(r8)
            org.jivesoftware.smackx.commands.LocalCommand r1 = (org.jivesoftware.smackx.commands.LocalCommand) r1
            if (r1 != 0) goto L_0x00f3
            org.jivesoftware.smack.packet.XMPPError$Condition r9 = org.jivesoftware.smack.packet.XMPPError.Condition.bad_request
            org.jivesoftware.smackx.commands.AdHocCommand$SpecificErrorCondition r10 = org.jivesoftware.smackx.commands.AdHocCommand.SpecificErrorCondition.badSessionid
            r13.respondError(r7, r9, r10)
            goto L_0x0008
        L_0x00f3:
            long r3 = r1.getCreationDate()
            long r9 = java.lang.System.currentTimeMillis()
            long r9 = r9 - r3
            r11 = 120000(0x1d4c0, double:5.9288E-319)
            int r9 = (r9 > r11 ? 1 : (r9 == r11 ? 0 : -1))
            if (r9 <= 0) goto L_0x0111
            java.util.Map<java.lang.String, org.jivesoftware.smackx.commands.LocalCommand> r9 = r13.executingCommands
            r9.remove(r8)
            org.jivesoftware.smack.packet.XMPPError$Condition r9 = org.jivesoftware.smack.packet.XMPPError.Condition.not_allowed
            org.jivesoftware.smackx.commands.AdHocCommand$SpecificErrorCondition r10 = org.jivesoftware.smackx.commands.AdHocCommand.SpecificErrorCondition.sessionExpired
            r13.respondError(r7, r9, r10)
            goto L_0x0008
        L_0x0111:
            monitor-enter(r1)
            org.jivesoftware.smackx.commands.AdHocCommand$Action r0 = r14.getAction()     // Catch:{ all -> 0x012a }
            if (r0 == 0) goto L_0x012d
            org.jivesoftware.smackx.commands.AdHocCommand$Action r9 = org.jivesoftware.smackx.commands.AdHocCommand.Action.unknown     // Catch:{ all -> 0x012a }
            boolean r9 = r0.equals(r9)     // Catch:{ all -> 0x012a }
            if (r9 == 0) goto L_0x012d
            org.jivesoftware.smack.packet.XMPPError$Condition r9 = org.jivesoftware.smack.packet.XMPPError.Condition.bad_request     // Catch:{ all -> 0x012a }
            org.jivesoftware.smackx.commands.AdHocCommand$SpecificErrorCondition r10 = org.jivesoftware.smackx.commands.AdHocCommand.SpecificErrorCondition.malformedAction     // Catch:{ all -> 0x012a }
            r13.respondError(r7, r9, r10)     // Catch:{ all -> 0x012a }
            monitor-exit(r1)     // Catch:{ all -> 0x012a }
            goto L_0x0008
        L_0x012a:
            r9 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x012a }
            throw r9
        L_0x012d:
            if (r0 == 0) goto L_0x0137
            org.jivesoftware.smackx.commands.AdHocCommand$Action r9 = org.jivesoftware.smackx.commands.AdHocCommand.Action.execute     // Catch:{ all -> 0x012a }
            boolean r9 = r9.equals(r0)     // Catch:{ all -> 0x012a }
            if (r9 == 0) goto L_0x013b
        L_0x0137:
            org.jivesoftware.smackx.commands.AdHocCommand$Action r0 = r1.getExecuteAction()     // Catch:{ all -> 0x012a }
        L_0x013b:
            boolean r9 = r1.isValidAction(r0)     // Catch:{ all -> 0x012a }
            if (r9 != 0) goto L_0x014b
            org.jivesoftware.smack.packet.XMPPError$Condition r9 = org.jivesoftware.smack.packet.XMPPError.Condition.bad_request     // Catch:{ all -> 0x012a }
            org.jivesoftware.smackx.commands.AdHocCommand$SpecificErrorCondition r10 = org.jivesoftware.smackx.commands.AdHocCommand.SpecificErrorCondition.badAction     // Catch:{ all -> 0x012a }
            r13.respondError(r7, r9, r10)     // Catch:{ all -> 0x012a }
            monitor-exit(r1)     // Catch:{ all -> 0x012a }
            goto L_0x0008
        L_0x014b:
            org.jivesoftware.smack.packet.IQ$Type r9 = org.jivesoftware.smack.packet.IQ.Type.RESULT     // Catch:{ XMPPException -> 0x0183 }
            r7.setType(r9)     // Catch:{ XMPPException -> 0x0183 }
            r1.setData(r7)     // Catch:{ XMPPException -> 0x0183 }
            org.jivesoftware.smackx.commands.AdHocCommand$Action r9 = org.jivesoftware.smackx.commands.AdHocCommand.Action.next     // Catch:{ XMPPException -> 0x0183 }
            boolean r9 = r9.equals(r0)     // Catch:{ XMPPException -> 0x0183 }
            if (r9 == 0) goto L_0x01a5
            r1.incrementStage()     // Catch:{ XMPPException -> 0x0183 }
            org.jivesoftware.smackx.Form r9 = new org.jivesoftware.smackx.Form     // Catch:{ XMPPException -> 0x0183 }
            org.jivesoftware.smackx.packet.DataForm r10 = r14.getForm()     // Catch:{ XMPPException -> 0x0183 }
            r9.<init>(r10)     // Catch:{ XMPPException -> 0x0183 }
            r1.next(r9)     // Catch:{ XMPPException -> 0x0183 }
            boolean r9 = r1.isLastStage()     // Catch:{ XMPPException -> 0x0183 }
            if (r9 == 0) goto L_0x017d
            org.jivesoftware.smackx.commands.AdHocCommand$Status r9 = org.jivesoftware.smackx.commands.AdHocCommand.Status.completed     // Catch:{ XMPPException -> 0x0183 }
            r7.setStatus(r9)     // Catch:{ XMPPException -> 0x0183 }
        L_0x0175:
            org.jivesoftware.smack.Connection r9 = r13.connection     // Catch:{ XMPPException -> 0x0183 }
            r9.sendPacket(r7)     // Catch:{ XMPPException -> 0x0183 }
        L_0x017a:
            monitor-exit(r1)     // Catch:{ all -> 0x012a }
            goto L_0x0008
        L_0x017d:
            org.jivesoftware.smackx.commands.AdHocCommand$Status r9 = org.jivesoftware.smackx.commands.AdHocCommand.Status.executing     // Catch:{ XMPPException -> 0x0183 }
            r7.setStatus(r9)     // Catch:{ XMPPException -> 0x0183 }
            goto L_0x0175
        L_0x0183:
            r5 = move-exception
            org.jivesoftware.smack.packet.XMPPError r6 = r5.getXMPPError()     // Catch:{ all -> 0x012a }
            org.jivesoftware.smack.packet.XMPPError$Type r9 = org.jivesoftware.smack.packet.XMPPError.Type.CANCEL     // Catch:{ all -> 0x012a }
            org.jivesoftware.smack.packet.XMPPError$Type r10 = r6.getType()     // Catch:{ all -> 0x012a }
            boolean r9 = r9.equals(r10)     // Catch:{ all -> 0x012a }
            if (r9 == 0) goto L_0x019e
            org.jivesoftware.smackx.commands.AdHocCommand$Status r9 = org.jivesoftware.smackx.commands.AdHocCommand.Status.canceled     // Catch:{ all -> 0x012a }
            r7.setStatus(r9)     // Catch:{ all -> 0x012a }
            java.util.Map<java.lang.String, org.jivesoftware.smackx.commands.LocalCommand> r9 = r13.executingCommands     // Catch:{ all -> 0x012a }
            r9.remove(r8)     // Catch:{ all -> 0x012a }
        L_0x019e:
            r13.respondError(r7, r6)     // Catch:{ all -> 0x012a }
            r5.printStackTrace()     // Catch:{ all -> 0x012a }
            goto L_0x017a
        L_0x01a5:
            org.jivesoftware.smackx.commands.AdHocCommand$Action r9 = org.jivesoftware.smackx.commands.AdHocCommand.Action.complete     // Catch:{ XMPPException -> 0x0183 }
            boolean r9 = r9.equals(r0)     // Catch:{ XMPPException -> 0x0183 }
            if (r9 == 0) goto L_0x01c7
            r1.incrementStage()     // Catch:{ XMPPException -> 0x0183 }
            org.jivesoftware.smackx.Form r9 = new org.jivesoftware.smackx.Form     // Catch:{ XMPPException -> 0x0183 }
            org.jivesoftware.smackx.packet.DataForm r10 = r14.getForm()     // Catch:{ XMPPException -> 0x0183 }
            r9.<init>(r10)     // Catch:{ XMPPException -> 0x0183 }
            r1.complete(r9)     // Catch:{ XMPPException -> 0x0183 }
            org.jivesoftware.smackx.commands.AdHocCommand$Status r9 = org.jivesoftware.smackx.commands.AdHocCommand.Status.completed     // Catch:{ XMPPException -> 0x0183 }
            r7.setStatus(r9)     // Catch:{ XMPPException -> 0x0183 }
            java.util.Map<java.lang.String, org.jivesoftware.smackx.commands.LocalCommand> r9 = r13.executingCommands     // Catch:{ XMPPException -> 0x0183 }
            r9.remove(r8)     // Catch:{ XMPPException -> 0x0183 }
            goto L_0x0175
        L_0x01c7:
            org.jivesoftware.smackx.commands.AdHocCommand$Action r9 = org.jivesoftware.smackx.commands.AdHocCommand.Action.prev     // Catch:{ XMPPException -> 0x0183 }
            boolean r9 = r9.equals(r0)     // Catch:{ XMPPException -> 0x0183 }
            if (r9 == 0) goto L_0x01d6
            r1.decrementStage()     // Catch:{ XMPPException -> 0x0183 }
            r1.prev()     // Catch:{ XMPPException -> 0x0183 }
            goto L_0x0175
        L_0x01d6:
            org.jivesoftware.smackx.commands.AdHocCommand$Action r9 = org.jivesoftware.smackx.commands.AdHocCommand.Action.cancel     // Catch:{ XMPPException -> 0x0183 }
            boolean r9 = r9.equals(r0)     // Catch:{ XMPPException -> 0x0183 }
            if (r9 == 0) goto L_0x0175
            r1.cancel()     // Catch:{ XMPPException -> 0x0183 }
            org.jivesoftware.smackx.commands.AdHocCommand$Status r9 = org.jivesoftware.smackx.commands.AdHocCommand.Status.canceled     // Catch:{ XMPPException -> 0x0183 }
            r7.setStatus(r9)     // Catch:{ XMPPException -> 0x0183 }
            java.util.Map<java.lang.String, org.jivesoftware.smackx.commands.LocalCommand> r9 = r13.executingCommands     // Catch:{ XMPPException -> 0x0183 }
            r9.remove(r8)     // Catch:{ XMPPException -> 0x0183 }
            goto L_0x0175
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smackx.commands.AdHocCommandManager.processAdHocCommand(org.jivesoftware.smackx.packet.AdHocCommandData):void");
    }

    private void respondError(AdHocCommandData response, XMPPError.Condition condition) {
        respondError(response, new XMPPError(condition));
    }

    private void respondError(AdHocCommandData response, XMPPError.Condition condition, AdHocCommand.SpecificErrorCondition specificCondition) {
        XMPPError error = new XMPPError(condition);
        error.addExtension(new AdHocCommandData.SpecificError(specificCondition));
        respondError(response, error);
    }

    private void respondError(AdHocCommandData response, XMPPError error) {
        response.setType(IQ.Type.ERROR);
        response.setError(error);
        this.connection.sendPacket(response);
    }

    private LocalCommand newInstanceOfCmd(String commandNode, String sessionID) throws XMPPException {
        AdHocCommandInfo commandInfo = this.commands.get(commandNode);
        try {
            LocalCommand command = commandInfo.getCommandInstance();
            command.setSessionID(sessionID);
            command.setName(commandInfo.getName());
            command.setNode(commandInfo.getNode());
            return command;
        } catch (InstantiationException e) {
            e.printStackTrace();
            throw new XMPPException(new XMPPError(XMPPError.Condition.interna_server_error));
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
            throw new XMPPException(new XMPPError(XMPPError.Condition.interna_server_error));
        }
    }

    /* access modifiers changed from: private */
    public Collection<AdHocCommandInfo> getRegisteredCommands() {
        return this.commands.values();
    }

    private static class AdHocCommandInfo {
        private LocalCommandFactory factory;
        private String name;
        private String node;
        private String ownerJID;

        public AdHocCommandInfo(String node2, String name2, String ownerJID2, LocalCommandFactory factory2) {
            this.node = node2;
            this.name = name2;
            this.ownerJID = ownerJID2;
            this.factory = factory2;
        }

        public LocalCommand getCommandInstance() throws InstantiationException, IllegalAccessException {
            return this.factory.getInstance();
        }

        public String getName() {
            return this.name;
        }

        public String getNode() {
            return this.node;
        }

        public String getOwnerJID() {
            return this.ownerJID;
        }
    }
}
