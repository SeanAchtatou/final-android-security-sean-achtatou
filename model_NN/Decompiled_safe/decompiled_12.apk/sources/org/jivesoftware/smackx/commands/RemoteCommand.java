package org.jivesoftware.smackx.commands;

import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.PacketCollector;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.PacketIDFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.Form;
import org.jivesoftware.smackx.commands.AdHocCommand;
import org.jivesoftware.smackx.packet.AdHocCommandData;

public class RemoteCommand extends AdHocCommand {
    private Connection connection;
    private String jid;
    private long packetReplyTimeout = ((long) SmackConfiguration.getPacketReplyTimeout());
    private String sessionID;

    protected RemoteCommand(Connection connection2, String node, String jid2) {
        this.connection = connection2;
        this.jid = jid2;
        setNode(node);
    }

    public void cancel() throws XMPPException {
        executeAction(AdHocCommand.Action.cancel, this.packetReplyTimeout);
    }

    public void complete(Form form) throws XMPPException {
        executeAction(AdHocCommand.Action.complete, form, this.packetReplyTimeout);
    }

    public void execute() throws XMPPException {
        executeAction(AdHocCommand.Action.execute, this.packetReplyTimeout);
    }

    public void execute(Form form) throws XMPPException {
        executeAction(AdHocCommand.Action.execute, form, this.packetReplyTimeout);
    }

    public void next(Form form) throws XMPPException {
        executeAction(AdHocCommand.Action.next, form, this.packetReplyTimeout);
    }

    public void prev() throws XMPPException {
        executeAction(AdHocCommand.Action.prev, this.packetReplyTimeout);
    }

    private void executeAction(AdHocCommand.Action action, long packetReplyTimeout2) throws XMPPException {
        executeAction(action, null, packetReplyTimeout2);
    }

    private void executeAction(AdHocCommand.Action action, Form form, long timeout) throws XMPPException {
        AdHocCommandData data = new AdHocCommandData();
        data.setType(IQ.Type.SET);
        data.setTo(getOwnerJID());
        data.setNode(getNode());
        data.setSessionID(this.sessionID);
        data.setAction(action);
        if (form != null) {
            data.setForm(form.getDataFormToSend());
        }
        PacketCollector collector = this.connection.createPacketCollector(new PacketIDFilter(data.getPacketID()));
        this.connection.sendPacket(data);
        Packet response = collector.nextResult(timeout);
        collector.cancel();
        if (response == null) {
            throw new XMPPException("No response from server on status set.");
        } else if (response.getError() != null) {
            throw new XMPPException(response.getError());
        } else {
            AdHocCommandData responseData = (AdHocCommandData) response;
            this.sessionID = responseData.getSessionID();
            super.setData(responseData);
        }
    }

    public String getOwnerJID() {
        return this.jid;
    }

    public long getPacketReplyTimeout() {
        return this.packetReplyTimeout;
    }

    public void setPacketReplyTimeout(long packetReplyTimeout2) {
        this.packetReplyTimeout = packetReplyTimeout2;
    }
}
