package org.jivesoftware.smackx.provider;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.provider.IQProvider;
import org.jivesoftware.smack.provider.PacketExtensionProvider;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.jivesoftware.smackx.GroupChatInvitation;
import org.jivesoftware.smackx.commands.AdHocCommand;
import org.jivesoftware.smackx.commands.AdHocCommandNote;
import org.jivesoftware.smackx.packet.AdHocCommandData;
import org.jivesoftware.smackx.packet.DataForm;
import org.xmlpull.v1.XmlPullParser;

public class AdHocCommandDataProvider implements IQProvider {
    public IQ parseIQ(XmlPullParser parser) throws Exception {
        boolean done = false;
        AdHocCommandData adHocCommandData = new AdHocCommandData();
        DataFormProvider dataFormProvider = new DataFormProvider();
        adHocCommandData.setSessionID(parser.getAttributeValue("", "sessionid"));
        adHocCommandData.setNode(parser.getAttributeValue("", "node"));
        String status = parser.getAttributeValue("", "status");
        if (AdHocCommand.Status.executing.toString().equalsIgnoreCase(status)) {
            adHocCommandData.setStatus(AdHocCommand.Status.executing);
        } else if (AdHocCommand.Status.completed.toString().equalsIgnoreCase(status)) {
            adHocCommandData.setStatus(AdHocCommand.Status.completed);
        } else if (AdHocCommand.Status.canceled.toString().equalsIgnoreCase(status)) {
            adHocCommandData.setStatus(AdHocCommand.Status.canceled);
        }
        String action = parser.getAttributeValue("", "action");
        if (action != null) {
            AdHocCommand.Action realAction = AdHocCommand.Action.valueOf(action);
            if (realAction == null || realAction.equals(AdHocCommand.Action.unknown)) {
                adHocCommandData.setAction(AdHocCommand.Action.unknown);
            } else {
                adHocCommandData.setAction(realAction);
            }
        }
        while (!done) {
            int eventType = parser.next();
            String elementName = parser.getName();
            String namespace = parser.getNamespace();
            if (eventType == 2) {
                if (parser.getName().equals("actions")) {
                    String execute = parser.getAttributeValue("", "execute");
                    if (execute != null) {
                        adHocCommandData.setExecuteAction(AdHocCommand.Action.valueOf(execute));
                    }
                } else if (parser.getName().equals("next")) {
                    adHocCommandData.addAction(AdHocCommand.Action.next);
                } else if (parser.getName().equals("complete")) {
                    adHocCommandData.addAction(AdHocCommand.Action.complete);
                } else if (parser.getName().equals("prev")) {
                    adHocCommandData.addAction(AdHocCommand.Action.prev);
                } else if (elementName.equals(GroupChatInvitation.ELEMENT_NAME) && namespace.equals("jabber:x:data")) {
                    adHocCommandData.setForm((DataForm) dataFormProvider.parseExtension(parser));
                } else if (parser.getName().equals("note")) {
                    adHocCommandData.addNote(new AdHocCommandNote(AdHocCommandNote.Type.valueOf(parser.getAttributeValue("", "type")), parser.nextText()));
                } else if (parser.getName().equals("error")) {
                    adHocCommandData.setError(PacketParserUtils.parseError(parser));
                }
            } else if (eventType == 3 && parser.getName().equals("command")) {
                done = true;
            }
        }
        return adHocCommandData;
    }

    public static class BadActionError implements PacketExtensionProvider {
        public PacketExtension parseExtension(XmlPullParser parser) throws Exception {
            return new AdHocCommandData.SpecificError(AdHocCommand.SpecificErrorCondition.badAction);
        }
    }

    public static class MalformedActionError implements PacketExtensionProvider {
        public PacketExtension parseExtension(XmlPullParser parser) throws Exception {
            return new AdHocCommandData.SpecificError(AdHocCommand.SpecificErrorCondition.malformedAction);
        }
    }

    public static class BadLocaleError implements PacketExtensionProvider {
        public PacketExtension parseExtension(XmlPullParser parser) throws Exception {
            return new AdHocCommandData.SpecificError(AdHocCommand.SpecificErrorCondition.badLocale);
        }
    }

    public static class BadPayloadError implements PacketExtensionProvider {
        public PacketExtension parseExtension(XmlPullParser parser) throws Exception {
            return new AdHocCommandData.SpecificError(AdHocCommand.SpecificErrorCondition.badPayload);
        }
    }

    public static class BadSessionIDError implements PacketExtensionProvider {
        public PacketExtension parseExtension(XmlPullParser parser) throws Exception {
            return new AdHocCommandData.SpecificError(AdHocCommand.SpecificErrorCondition.badSessionid);
        }
    }

    public static class SessionExpiredError implements PacketExtensionProvider {
        public PacketExtension parseExtension(XmlPullParser parser) throws Exception {
            return new AdHocCommandData.SpecificError(AdHocCommand.SpecificErrorCondition.sessionExpired);
        }
    }
}
