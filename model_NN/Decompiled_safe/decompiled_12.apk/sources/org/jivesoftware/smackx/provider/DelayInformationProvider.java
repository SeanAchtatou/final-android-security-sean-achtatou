package org.jivesoftware.smackx.provider;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.packet.PrivacyItem;
import org.jivesoftware.smack.provider.PacketExtensionProvider;
import org.jivesoftware.smackx.packet.DelayInformation;
import org.xmlpull.v1.XmlPullParser;

public class DelayInformationProvider implements PacketExtensionProvider {
    private static final SimpleDateFormat XEP_0082_UTC_FORMAT_WITHOUT_MILLIS = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    private static final SimpleDateFormat XEP_0091_UTC_FALLBACK_FORMAT = new SimpleDateFormat("yyyyMd'T'HH:mm:ss");
    private static Map<String, DateFormat> formats = new HashMap();

    static {
        XEP_0091_UTC_FALLBACK_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
        XEP_0082_UTC_FORMAT_WITHOUT_MILLIS.setTimeZone(TimeZone.getTimeZone("UTC"));
        formats.put("^\\d+T\\d+:\\d+:\\d+$", DelayInformation.XEP_0091_UTC_FORMAT);
        formats.put("^\\d+-\\d+-\\d+T\\d+:\\d+:\\d+\\.\\d+Z$", Packet.XEP_0082_UTC_FORMAT);
        formats.put("^\\d+-\\d+-\\d+T\\d+:\\d+:\\d+Z$", XEP_0082_UTC_FORMAT_WITHOUT_MILLIS);
    }

    public PacketExtension parseExtension(XmlPullParser parser) throws Exception {
        String stampString = parser.getAttributeValue("", "stamp");
        Date stamp = null;
        DateFormat format = null;
        Iterator<String> it = formats.keySet().iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            String regexp = it.next();
            if (stampString.matches(regexp)) {
                try {
                    format = formats.get(regexp);
                    synchronized (format) {
                        stamp = format.parse(stampString);
                    }
                    break;
                } catch (ParseException e) {
                }
            }
        }
        if (format == DelayInformation.XEP_0091_UTC_FORMAT && stampString.split("T")[0].length() < 8) {
            stamp = handleDateWithMissingLeadingZeros(stampString);
        }
        if (stamp == null) {
            stamp = new Date();
        }
        DelayInformation delayInformation = new DelayInformation(stamp);
        delayInformation.setFrom(parser.getAttributeValue("", PrivacyItem.PrivacyRule.SUBSCRIPTION_FROM));
        String reason = parser.nextText();
        if ("".equals(reason)) {
            reason = null;
        }
        delayInformation.setReason(reason);
        return delayInformation;
    }

    private Date handleDateWithMissingLeadingZeros(String stampString) {
        Calendar now = new GregorianCalendar();
        List<Calendar> dates = filterDatesBefore(now, parseXEP91Date(stampString, DelayInformation.XEP_0091_UTC_FORMAT), parseXEP91Date(stampString, XEP_0091_UTC_FALLBACK_FORMAT));
        if (!dates.isEmpty()) {
            return determineNearestDate(now, dates).getTime();
        }
        return null;
    }

    private Calendar parseXEP91Date(String stampString, DateFormat dateFormat) {
        Calendar calendar;
        try {
            synchronized (dateFormat) {
                dateFormat.parse(stampString);
                calendar = dateFormat.getCalendar();
            }
            return calendar;
        } catch (ParseException e) {
            return null;
        }
    }

    private List<Calendar> filterDatesBefore(Calendar now, Calendar... dates) {
        List<Calendar> result = new ArrayList<>();
        for (Calendar calendar : dates) {
            if (calendar != null && calendar.before(now)) {
                result.add(calendar);
            }
        }
        return result;
    }

    private Calendar determineNearestDate(final Calendar now, List<Calendar> dates) {
        Collections.sort(dates, new Comparator<Calendar>() {
            public int compare(Calendar o1, Calendar o2) {
                return new Long(now.getTimeInMillis() - o1.getTimeInMillis()).compareTo(new Long(now.getTimeInMillis() - o2.getTimeInMillis()));
            }
        });
        return dates.get(0);
    }
}
