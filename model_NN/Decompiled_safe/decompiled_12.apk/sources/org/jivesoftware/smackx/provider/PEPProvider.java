package org.jivesoftware.smackx.provider;

import java.util.HashMap;
import java.util.Map;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.provider.PacketExtensionProvider;
import org.xmlpull.v1.XmlPullParser;

public class PEPProvider implements PacketExtensionProvider {
    Map<String, PacketExtensionProvider> nodeParsers = new HashMap();
    PacketExtension pepItem;

    public void registerPEPParserExtension(String node, PacketExtensionProvider pepItemParser) {
        this.nodeParsers.put(node, pepItemParser);
    }

    public PacketExtension parseExtension(XmlPullParser parser) throws Exception {
        PacketExtensionProvider nodeParser;
        boolean done = false;
        while (!done) {
            int eventType = parser.next();
            if (eventType == 2) {
                if (!parser.getName().equals("event") && parser.getName().equals("items") && (nodeParser = this.nodeParsers.get(parser.getAttributeValue("", "node"))) != null) {
                    this.pepItem = nodeParser.parseExtension(parser);
                }
            } else if (eventType == 3 && parser.getName().equals("event")) {
                done = true;
            }
        }
        return this.pepItem;
    }
}
