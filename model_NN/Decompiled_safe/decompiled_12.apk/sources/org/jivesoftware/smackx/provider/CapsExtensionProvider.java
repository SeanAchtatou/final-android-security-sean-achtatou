package org.jivesoftware.smackx.provider;

import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.provider.PacketExtensionProvider;
import org.jivesoftware.smackx.packet.CapsExtension;
import org.xmlpull.v1.XmlPullParser;

public class CapsExtensionProvider implements PacketExtensionProvider {
    public PacketExtension parseExtension(XmlPullParser parser) throws Exception {
        boolean done = false;
        String hash = null;
        String version = null;
        String node = null;
        while (!done) {
            if (parser.getEventType() == 2 && parser.getName().equalsIgnoreCase(CapsExtension.NODE_NAME)) {
                hash = parser.getAttributeValue(null, "hash");
                version = parser.getAttributeValue(null, "ver");
                node = parser.getAttributeValue(null, "node");
            }
            if (parser.getEventType() != 3 || !parser.getName().equalsIgnoreCase(CapsExtension.NODE_NAME)) {
                parser.next();
            } else {
                done = true;
            }
        }
        return new CapsExtension(node, version, hash);
    }
}
