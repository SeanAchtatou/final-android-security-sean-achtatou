package org.jivesoftware.smackx.provider;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.provider.IQProvider;
import org.jivesoftware.smack.provider.PacketExtensionProvider;
import org.jivesoftware.smackx.packet.IBBExtensions;
import org.xmlpull.v1.XmlPullParser;

public class IBBProviders {

    public static class Open implements IQProvider {
        public IQ parseIQ(XmlPullParser parser) throws Exception {
            return new IBBExtensions.Open(parser.getAttributeValue("", "sid"), Integer.parseInt(parser.getAttributeValue("", "block-size")));
        }
    }

    public static class Data implements PacketExtensionProvider {
        public PacketExtension parseExtension(XmlPullParser parser) throws Exception {
            return new IBBExtensions.Data(parser.getAttributeValue("", "sid"), Long.parseLong(parser.getAttributeValue("", "seq")), parser.nextText());
        }
    }

    public static class Close implements IQProvider {
        public IQ parseIQ(XmlPullParser parser) throws Exception {
            return new IBBExtensions.Close(parser.getAttributeValue("", "sid"));
        }
    }
}
