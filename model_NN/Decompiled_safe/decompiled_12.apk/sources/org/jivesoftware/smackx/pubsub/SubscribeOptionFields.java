package org.jivesoftware.smackx.pubsub;

public enum SubscribeOptionFields {
    deliver,
    digest,
    digest_frequency,
    expire,
    include_body,
    show_values,
    subscription_type,
    subscription_depth;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public String getFieldName() {
        if (this == show_values) {
            return "pubsub#" + toString().replace('_', '-');
        }
        return "pubsub#" + toString();
    }

    public static SubscribeOptionFields valueOfFromElement(String elementName) {
        String portion = elementName.substring(elementName.lastIndexOf(36));
        if ("show-values".equals(portion)) {
            return show_values;
        }
        return valueOf(portion);
    }
}
