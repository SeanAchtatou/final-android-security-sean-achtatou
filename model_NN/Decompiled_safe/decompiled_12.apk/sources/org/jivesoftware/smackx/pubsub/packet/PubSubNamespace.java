package org.jivesoftware.smackx.pubsub.packet;

public enum PubSubNamespace {
    BASIC(null),
    ERROR("errors"),
    EVENT("event"),
    OWNER("owner");
    
    private String fragment;

    private PubSubNamespace(String fragment2) {
        this.fragment = fragment2;
    }

    public String getXmlns() {
        if (this.fragment != null) {
            return String.valueOf("http://jabber.org/protocol/pubsub") + '#' + this.fragment;
        }
        return "http://jabber.org/protocol/pubsub";
    }

    public String getFragment() {
        return this.fragment;
    }

    public static PubSubNamespace valueOfFromXmlns(String ns) {
        if (ns.lastIndexOf(35) != -1) {
            return valueOf(ns.substring(ns.lastIndexOf(35) + 1).toUpperCase());
        }
        return BASIC;
    }
}
