package org.jivesoftware.smackx.pubsub;

import org.jivesoftware.smack.packet.PacketExtension;

public class Item implements PacketExtension {
    private String id;

    public Item() {
    }

    public Item(String itemId) {
        this.id = itemId;
    }

    public String getId() {
        return this.id;
    }

    public String getElementName() {
        return "item";
    }

    public String getNamespace() {
        return null;
    }

    public String toXML() {
        StringBuilder builder = new StringBuilder("<item");
        if (this.id != null) {
            builder.append(" id='");
            builder.append(this.id);
            builder.append("'");
        }
        builder.append("/>");
        return builder.toString();
    }

    public String toString() {
        return String.valueOf(getClass().getName()) + " | Content [" + toXML() + "]";
    }
}
