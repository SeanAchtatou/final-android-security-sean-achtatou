package org.jivesoftware.smackx.pubsub.provider;

import org.jivesoftware.smack.provider.PacketExtensionProvider;

public class SubscriptionProvider implements PacketExtensionProvider {
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0047  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.jivesoftware.smack.packet.PacketExtension parseExtension(org.xmlpull.v1.XmlPullParser r12) throws java.lang.Exception {
        /*
            r11 = this;
            r10 = 3
            r9 = 2
            r4 = 0
            java.lang.String r0 = "jid"
            java.lang.String r1 = r12.getAttributeValue(r4, r0)
            java.lang.String r0 = "node"
            java.lang.String r2 = r12.getAttributeValue(r4, r0)
            java.lang.String r0 = "subid"
            java.lang.String r3 = r12.getAttributeValue(r4, r0)
            java.lang.String r0 = "subscription"
            java.lang.String r6 = r12.getAttributeValue(r4, r0)
            r5 = 0
            int r7 = r12.next()
            if (r7 != r9) goto L_0x004f
            java.lang.String r0 = r12.getName()
            java.lang.String r8 = "subscribe-options"
            boolean r0 = r0.equals(r8)
            if (r0 == 0) goto L_0x004f
            int r7 = r12.next()
            if (r7 != r9) goto L_0x0041
            java.lang.String r0 = r12.getName()
            java.lang.String r8 = "required"
            boolean r0 = r0.equals(r8)
            if (r0 == 0) goto L_0x0041
            r5 = 1
        L_0x0041:
            int r0 = r12.next()
            if (r0 == r10) goto L_0x004f
            java.lang.String r0 = r12.getName()
            java.lang.String r8 = "subscribe-options"
            if (r0 != r8) goto L_0x0041
        L_0x004f:
            int r0 = r12.getEventType()
            if (r0 != r10) goto L_0x005d
            org.jivesoftware.smackx.pubsub.Subscription r0 = new org.jivesoftware.smackx.pubsub.Subscription
            if (r6 != 0) goto L_0x0061
        L_0x0059:
            r0.<init>(r1, r2, r3, r4, r5)
            return r0
        L_0x005d:
            r12.next()
            goto L_0x004f
        L_0x0061:
            org.jivesoftware.smackx.pubsub.Subscription$State r4 = org.jivesoftware.smackx.pubsub.Subscription.State.valueOf(r6)
            goto L_0x0059
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smackx.pubsub.provider.SubscriptionProvider.parseExtension(org.xmlpull.v1.XmlPullParser):org.jivesoftware.smack.packet.PacketExtension");
    }
}
