package org.jivesoftware.smack;

import com.kenai.jbosh.AbstractBody;
import com.kenai.jbosh.BOSHClientResponseListener;
import com.kenai.jbosh.BOSHMessageEvent;
import com.kenai.jbosh.BodyQName;
import com.kenai.jbosh.ComposableBody;
import java.io.StringReader;
import org.jivesoftware.smack.sasl.SASLMechanism;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.jivesoftware.smackx.workgroup.packet.SessionID;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

public class BOSHPacketReader implements BOSHClientResponseListener {
    private BOSHConnection connection;

    public BOSHPacketReader(BOSHConnection connection2) {
        this.connection = connection2;
    }

    public void responseReceived(BOSHMessageEvent event) {
        int eventType;
        AbstractBody body = event.getBody();
        if (body != null) {
            try {
                if (this.connection.sessionID == null) {
                    this.connection.sessionID = body.getAttribute(BodyQName.create(BOSHConnection.BOSH_URI, "sid"));
                }
                if (this.connection.authID == null) {
                    this.connection.authID = body.getAttribute(BodyQName.create(BOSHConnection.BOSH_URI, "authid"));
                }
                XmlPullParser parser = XmlPullParserFactory.newInstance().newPullParser();
                parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, true);
                parser.setInput(new StringReader(body.toXML()));
                int eventType2 = parser.getEventType();
                do {
                    eventType = parser.next();
                    if (eventType == 2 && !parser.getName().equals("body")) {
                        if (parser.getName().equals("message")) {
                            this.connection.processPacket(PacketParserUtils.parseMessage(parser));
                            continue;
                        } else if (parser.getName().equals("iq")) {
                            this.connection.processPacket(PacketParserUtils.parseIQ(parser, this.connection));
                            continue;
                        } else if (parser.getName().equals("presence")) {
                            this.connection.processPacket(PacketParserUtils.parsePresence(parser));
                            continue;
                        } else if (parser.getName().equals("challenge")) {
                            String challengeData = parser.nextText();
                            this.connection.getSASLAuthentication().challengeReceived(challengeData);
                            this.connection.processPacket(new SASLMechanism.Challenge(challengeData));
                            continue;
                        } else if (parser.getName().equals("success")) {
                            this.connection.send(ComposableBody.builder().setNamespaceDefinition("xmpp", BOSHConnection.XMPP_BOSH_NS).setAttribute(BodyQName.createWithPrefix(BOSHConnection.XMPP_BOSH_NS, "restart", "xmpp"), "true").setAttribute(BodyQName.create(BOSHConnection.BOSH_URI, "to"), this.connection.getServiceName()).build());
                            this.connection.getSASLAuthentication().authenticated();
                            this.connection.processPacket(new SASLMechanism.Success(parser.nextText()));
                            continue;
                        } else if (parser.getName().equals("features")) {
                            parseFeatures(parser);
                            continue;
                        } else if (parser.getName().equals("failure")) {
                            if ("urn:ietf:params:xml:ns:xmpp-sasl".equals(parser.getNamespace(null))) {
                                SASLMechanism.Failure failure = PacketParserUtils.parseSASLFailure(parser);
                                this.connection.getSASLAuthentication().authenticationFailed();
                                this.connection.processPacket(failure);
                                continue;
                            } else {
                                continue;
                            }
                        } else if (parser.getName().equals("error")) {
                            throw new XMPPException(PacketParserUtils.parseStreamError(parser));
                        }
                    }
                } while (eventType != 1);
            } catch (Exception e) {
                if (this.connection.isConnected()) {
                    this.connection.notifyConnectionError(e);
                }
            }
        }
    }

    private void parseFeatures(XmlPullParser parser) throws Exception {
        boolean done = false;
        while (!done) {
            int eventType = parser.next();
            if (eventType == 2) {
                if (parser.getName().equals("mechanisms")) {
                    this.connection.getSASLAuthentication().setAvailableSASLMethods(PacketParserUtils.parseMechanisms(parser));
                } else if (parser.getName().equals("bind")) {
                    this.connection.getSASLAuthentication().bindingRequired();
                } else if (parser.getName().equals(SessionID.ELEMENT_NAME)) {
                    this.connection.getSASLAuthentication().sessionsSupported();
                } else if (parser.getName().equals("register")) {
                    this.connection.getAccountManager().setSupportsAccountCreation(true);
                }
            } else if (eventType == 3 && parser.getName().equals("features")) {
                done = true;
            }
        }
    }
}
