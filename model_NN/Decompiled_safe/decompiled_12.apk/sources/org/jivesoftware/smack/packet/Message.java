package org.jivesoftware.smack.packet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.jivesoftware.smack.util.StringUtils;

public class Message extends Packet {
    private final Set<Body> bodies = new HashSet();
    private String language;
    private final Set<Subject> subjects = new HashSet();
    private String thread = null;
    private Type type = Type.normal;

    public Message() {
    }

    public Message(String to) {
        setTo(to);
    }

    public Message(String to, Type type2) {
        setTo(to);
        this.type = type2;
    }

    public Type getType() {
        return this.type;
    }

    public void setType(Type type2) {
        if (type2 == null) {
            throw new IllegalArgumentException("Type cannot be null.");
        }
        this.type = type2;
    }

    public String getSubject() {
        return getSubject(null);
    }

    public String getSubject(String language2) {
        Subject subject = getMessageSubject(language2);
        if (subject == null) {
            return null;
        }
        return subject.subject;
    }

    private Subject getMessageSubject(String language2) {
        String language3 = determineLanguage(language2);
        for (Subject subject : this.subjects) {
            if (language3.equals(subject.language)) {
                return subject;
            }
        }
        return null;
    }

    public Collection<Subject> getSubjects() {
        return Collections.unmodifiableCollection(this.subjects);
    }

    public void setSubject(String subject) {
        if (subject == null) {
            removeSubject("");
        } else {
            addSubject(null, subject);
        }
    }

    public Subject addSubject(String language2, String subject) {
        Subject messageSubject = new Subject(determineLanguage(language2), subject, null);
        this.subjects.add(messageSubject);
        return messageSubject;
    }

    public boolean removeSubject(String language2) {
        String language3 = determineLanguage(language2);
        for (Subject subject : this.subjects) {
            if (language3.equals(subject.language)) {
                return this.subjects.remove(subject);
            }
        }
        return false;
    }

    public boolean removeSubject(Subject subject) {
        return this.subjects.remove(subject);
    }

    public Collection<String> getSubjectLanguages() {
        Subject defaultSubject = getMessageSubject(null);
        List<String> languages = new ArrayList<>();
        for (Subject subject : this.subjects) {
            if (!subject.equals(defaultSubject)) {
                languages.add(subject.language);
            }
        }
        return Collections.unmodifiableCollection(languages);
    }

    public String getBody() {
        return getBody(null);
    }

    public String getBody(String language2) {
        Body body = getMessageBody(language2);
        if (body == null) {
            return null;
        }
        return body.message;
    }

    private Body getMessageBody(String language2) {
        String language3 = determineLanguage(language2);
        for (Body body : this.bodies) {
            if (language3.equals(body.language)) {
                return body;
            }
        }
        return null;
    }

    public Collection<Body> getBodies() {
        return Collections.unmodifiableCollection(this.bodies);
    }

    public void setBody(String body) {
        if (body == null) {
            removeBody("");
        } else {
            addBody(null, body);
        }
    }

    public Body addBody(String language2, String body) {
        Body messageBody = new Body(determineLanguage(language2), body, null);
        this.bodies.add(messageBody);
        return messageBody;
    }

    public boolean removeBody(String language2) {
        String language3 = determineLanguage(language2);
        for (Body body : this.bodies) {
            if (language3.equals(body.language)) {
                return this.bodies.remove(body);
            }
        }
        return false;
    }

    public boolean removeBody(Body body) {
        return this.bodies.remove(body);
    }

    public Collection<String> getBodyLanguages() {
        Body defaultBody = getMessageBody(null);
        List<String> languages = new ArrayList<>();
        for (Body body : this.bodies) {
            if (!body.equals(defaultBody)) {
                languages.add(body.language);
            }
        }
        return Collections.unmodifiableCollection(languages);
    }

    public String getThread() {
        return this.thread;
    }

    public void setThread(String thread2) {
        this.thread = thread2;
    }

    public String getLanguage() {
        return this.language;
    }

    public void setLanguage(String language2) {
        this.language = language2;
    }

    private String determineLanguage(String language2) {
        if ("".equals(language2)) {
            language2 = null;
        }
        if (language2 == null && this.language != null) {
            return this.language;
        }
        if (language2 == null) {
            return getDefaultLanguage();
        }
        return language2;
    }

    public String toXML() {
        XMPPError error;
        StringBuilder buf = new StringBuilder();
        buf.append("<message");
        if (getXmlns() != null) {
            buf.append(" xmlns=\"").append(getXmlns()).append("\"");
        }
        if (this.language != null) {
            buf.append(" xml:lang=\"").append(getLanguage()).append("\"");
        }
        if (getPacketID() != null) {
            buf.append(" id=\"").append(getPacketID()).append("\"");
        }
        if (getTo() != null) {
            buf.append(" to=\"").append(StringUtils.escapeForXML(getTo())).append("\"");
        }
        if (getFrom() != null) {
            buf.append(" from=\"").append(StringUtils.escapeForXML(getFrom())).append("\"");
        }
        if (this.type != Type.normal) {
            buf.append(" type=\"").append(this.type).append("\"");
        }
        buf.append(">");
        Subject defaultSubject = getMessageSubject(null);
        if (defaultSubject != null) {
            buf.append("<subject>").append(StringUtils.escapeForXML(defaultSubject.getSubject()));
            buf.append("</subject>");
        }
        for (Subject s : getSubjects()) {
            buf.append("<subject xml:lang=\"" + s.getLanguage() + "\">");
            buf.append(StringUtils.escapeForXML(s.getSubject()));
            buf.append("</subject>");
        }
        Body defaultBody = getMessageBody(null);
        if (defaultBody != null) {
            buf.append("<body>").append(StringUtils.escapeForXML(defaultBody.message)).append("</body>");
        }
        for (Body body : getBodies()) {
            if (!body.equals(defaultBody)) {
                buf.append("<body xml:lang=\"").append(body.getLanguage()).append("\">");
                buf.append(StringUtils.escapeForXML(body.getMessage()));
                buf.append("</body>");
            }
        }
        if (this.thread != null) {
            buf.append("<thread>").append(this.thread).append("</thread>");
        }
        if (this.type == Type.error && (error = getError()) != null) {
            buf.append(error.toXML());
        }
        buf.append(getExtensionsXML());
        buf.append("</message>");
        return buf.toString();
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Message message = (Message) o;
        if (!super.equals(message)) {
            return false;
        }
        if (this.bodies.size() != message.bodies.size() || !this.bodies.containsAll(message.bodies)) {
            return false;
        }
        if (this.language == null ? message.language != null : !this.language.equals(message.language)) {
            return false;
        }
        if (this.subjects.size() != message.subjects.size() || !this.subjects.containsAll(message.subjects)) {
            return false;
        }
        if (this.thread == null ? message.thread != null : !this.thread.equals(message.thread)) {
            return false;
        }
        if (this.type != message.type) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result;
        int i;
        int i2 = 0;
        if (this.type != null) {
            result = this.type.hashCode();
        } else {
            result = 0;
        }
        int hashCode = ((result * 31) + this.subjects.hashCode()) * 31;
        if (this.thread != null) {
            i = this.thread.hashCode();
        } else {
            i = 0;
        }
        int i3 = (hashCode + i) * 31;
        if (this.language != null) {
            i2 = this.language.hashCode();
        }
        return ((i3 + i2) * 31) + this.bodies.hashCode();
    }

    public static class Subject {
        /* access modifiers changed from: private */
        public String language;
        /* access modifiers changed from: private */
        public String subject;

        private Subject(String language2, String subject2) {
            if (language2 == null) {
                throw new NullPointerException("Language cannot be null.");
            } else if (subject2 == null) {
                throw new NullPointerException("Subject cannot be null.");
            } else {
                this.language = language2;
                this.subject = subject2;
            }
        }

        /* synthetic */ Subject(String str, String str2, Subject subject2) {
            this(str, str2);
        }

        public String getLanguage() {
            return this.language;
        }

        public String getSubject() {
            return this.subject;
        }

        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            Subject otherSubject = (Subject) o;
            if (this.language.equals(otherSubject.language)) {
                return this.subject.equals(otherSubject.subject);
            }
            return false;
        }

        public int hashCode() {
            return (this.subject.hashCode() * 31) + this.language.hashCode();
        }
    }

    public static class Body {
        /* access modifiers changed from: private */
        public String language;
        /* access modifiers changed from: private */
        public String message;

        private Body(String language2, String message2) {
            if (language2 == null) {
                throw new NullPointerException("Language cannot be null.");
            } else if (message2 == null) {
                throw new NullPointerException("Message cannot be null.");
            } else {
                this.language = language2;
                this.message = message2;
            }
        }

        /* synthetic */ Body(String str, String str2, Body body) {
            this(str, str2);
        }

        public String getLanguage() {
            return this.language;
        }

        public String getMessage() {
            return this.message;
        }

        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            Body otherBody = (Body) o;
            if (this.language != null) {
                if (!this.language.equals(otherBody.language)) {
                    return false;
                }
            } else if (otherBody.language != null) {
                return false;
            }
            return this.message.equals(otherBody.message);
        }

        public int hashCode() {
            return (this.message.hashCode() * 31) + (this.language != null ? this.language.hashCode() : 0);
        }
    }

    public enum Type {
        normal,
        chat,
        groupchat,
        headline,
        error;

        public static Type fromString(String name) {
            try {
                return valueOf(name);
            } catch (Exception e) {
                return normal;
            }
        }
    }
}
