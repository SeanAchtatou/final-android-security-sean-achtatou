package org.jivesoftware.smack.packet;

public class StreamError {
    private String code;

    public StreamError(String code2) {
        this.code = code2;
    }

    public String getCode() {
        return this.code;
    }

    public String toString() {
        StringBuilder txt = new StringBuilder();
        txt.append("stream:error (").append(this.code).append(")");
        return txt.toString();
    }
}
