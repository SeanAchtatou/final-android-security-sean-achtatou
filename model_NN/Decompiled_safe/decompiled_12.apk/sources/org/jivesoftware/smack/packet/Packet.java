package org.jivesoftware.smack.packet;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.CopyOnWriteArrayList;
import org.jivesoftware.smack.util.StringUtils;

public abstract class Packet {
    protected static final String DEFAULT_LANGUAGE = Locale.getDefault().getLanguage().toLowerCase();
    private static String DEFAULT_XML_NS = null;
    public static final String ID_NOT_AVAILABLE = "ID_NOT_AVAILABLE";
    public static final DateFormat XEP_0082_UTC_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    private static long id = 0;
    private static String prefix = (String.valueOf(StringUtils.randomString(5)) + "-");
    private XMPPError error = null;
    private String from = null;
    private final List<PacketExtension> packetExtensions = new CopyOnWriteArrayList();
    private String packetID = null;
    private final Map<String, Object> properties = new HashMap();
    private String to = null;
    private String xmlns = DEFAULT_XML_NS;

    public abstract String toXML();

    static {
        XEP_0082_UTC_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    public static synchronized String nextID() {
        String sb;
        synchronized (Packet.class) {
            StringBuilder sb2 = new StringBuilder(String.valueOf(prefix));
            long j = id;
            id = 1 + j;
            sb = sb2.append(Long.toString(j)).toString();
        }
        return sb;
    }

    public static void setDefaultXmlns(String defaultXmlns) {
        DEFAULT_XML_NS = defaultXmlns;
    }

    public String getPacketID() {
        if (ID_NOT_AVAILABLE.equals(this.packetID)) {
            return null;
        }
        if (this.packetID == null) {
            this.packetID = nextID();
        }
        return this.packetID;
    }

    public void setPacketID(String packetID2) {
        this.packetID = packetID2;
    }

    public String getTo() {
        return this.to;
    }

    public void setTo(String to2) {
        this.to = to2;
    }

    public String getFrom() {
        return this.from;
    }

    public void setFrom(String from2) {
        this.from = from2;
    }

    public XMPPError getError() {
        return this.error;
    }

    public void setError(XMPPError error2) {
        this.error = error2;
    }

    public synchronized Collection<PacketExtension> getExtensions() {
        List unmodifiableList;
        if (this.packetExtensions == null) {
            unmodifiableList = Collections.emptyList();
        } else {
            unmodifiableList = Collections.unmodifiableList(new ArrayList(this.packetExtensions));
        }
        return unmodifiableList;
    }

    public PacketExtension getExtension(String namespace) {
        return getExtension(null, namespace);
    }

    public PacketExtension getExtension(String elementName, String namespace) {
        if (namespace == null) {
            return null;
        }
        for (PacketExtension ext : this.packetExtensions) {
            if ((elementName == null || elementName.equals(ext.getElementName())) && namespace.equals(ext.getNamespace())) {
                return ext;
            }
        }
        return null;
    }

    public void addExtension(PacketExtension extension) {
        this.packetExtensions.add(extension);
    }

    public void removeExtension(PacketExtension extension) {
        this.packetExtensions.remove(extension);
    }

    public synchronized Object getProperty(String name) {
        Object obj;
        if (this.properties == null) {
            obj = null;
        } else {
            obj = this.properties.get(name);
        }
        return obj;
    }

    public synchronized void setProperty(String name, Object value) {
        if (!(value instanceof Serializable)) {
            throw new IllegalArgumentException("Value must be serialiazble");
        }
        this.properties.put(name, value);
    }

    public synchronized void deleteProperty(String name) {
        if (this.properties != null) {
            this.properties.remove(name);
        }
    }

    public synchronized Collection<String> getPropertyNames() {
        Set unmodifiableSet;
        if (this.properties == null) {
            unmodifiableSet = Collections.emptySet();
        } else {
            unmodifiableSet = Collections.unmodifiableSet(new HashSet(this.properties.keySet()));
        }
        return unmodifiableSet;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0131 A[SYNTHETIC, Splitter:B:59:0x0131] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0136 A[SYNTHETIC, Splitter:B:62:0x0136] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0141 A[SYNTHETIC, Splitter:B:67:0x0141] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0146 A[SYNTHETIC, Splitter:B:70:0x0146] */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x0089 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.lang.String getExtensionsXML() {
        /*
            r13 = this;
            monitor-enter(r13)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x004c }
            r0.<init>()     // Catch:{ all -> 0x004c }
            java.util.Collection r10 = r13.getExtensions()     // Catch:{ all -> 0x004c }
            java.util.Iterator r10 = r10.iterator()     // Catch:{ all -> 0x004c }
        L_0x000e:
            boolean r11 = r10.hasNext()     // Catch:{ all -> 0x004c }
            if (r11 != 0) goto L_0x003e
            java.util.Map<java.lang.String, java.lang.Object> r10 = r13.properties     // Catch:{ all -> 0x004c }
            if (r10 == 0) goto L_0x0038
            java.util.Map<java.lang.String, java.lang.Object> r10 = r13.properties     // Catch:{ all -> 0x004c }
            boolean r10 = r10.isEmpty()     // Catch:{ all -> 0x004c }
            if (r10 != 0) goto L_0x0038
            java.lang.String r10 = "<properties xmlns=\"http://www.jivesoftware.com/xmlns/xmpp/properties\">"
            r0.append(r10)     // Catch:{ all -> 0x004c }
            java.util.Collection r10 = r13.getPropertyNames()     // Catch:{ all -> 0x004c }
            java.util.Iterator r10 = r10.iterator()     // Catch:{ all -> 0x004c }
        L_0x002d:
            boolean r11 = r10.hasNext()     // Catch:{ all -> 0x004c }
            if (r11 != 0) goto L_0x004f
            java.lang.String r10 = "</properties>"
            r0.append(r10)     // Catch:{ all -> 0x004c }
        L_0x0038:
            java.lang.String r10 = r0.toString()     // Catch:{ all -> 0x004c }
            monitor-exit(r13)
            return r10
        L_0x003e:
            java.lang.Object r5 = r10.next()     // Catch:{ all -> 0x004c }
            org.jivesoftware.smack.packet.PacketExtension r5 = (org.jivesoftware.smack.packet.PacketExtension) r5     // Catch:{ all -> 0x004c }
            java.lang.String r11 = r5.toXML()     // Catch:{ all -> 0x004c }
            r0.append(r11)     // Catch:{ all -> 0x004c }
            goto L_0x000e
        L_0x004c:
            r10 = move-exception
            monitor-exit(r13)
            throw r10
        L_0x004f:
            java.lang.Object r6 = r10.next()     // Catch:{ all -> 0x004c }
            java.lang.String r6 = (java.lang.String) r6     // Catch:{ all -> 0x004c }
            java.lang.Object r9 = r13.getProperty(r6)     // Catch:{ all -> 0x004c }
            java.lang.String r11 = "<property>"
            r0.append(r11)     // Catch:{ all -> 0x004c }
            java.lang.String r11 = "<name>"
            java.lang.StringBuilder r11 = r0.append(r11)     // Catch:{ all -> 0x004c }
            java.lang.String r12 = org.jivesoftware.smack.util.StringUtils.escapeForXML(r6)     // Catch:{ all -> 0x004c }
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ all -> 0x004c }
            java.lang.String r12 = "</name>"
            r11.append(r12)     // Catch:{ all -> 0x004c }
            java.lang.String r11 = "<value type=\""
            r0.append(r11)     // Catch:{ all -> 0x004c }
            boolean r11 = r9 instanceof java.lang.Integer     // Catch:{ all -> 0x004c }
            if (r11 == 0) goto L_0x008f
            java.lang.String r11 = "integer\">"
            java.lang.StringBuilder r11 = r0.append(r11)     // Catch:{ all -> 0x004c }
            java.lang.StringBuilder r11 = r11.append(r9)     // Catch:{ all -> 0x004c }
            java.lang.String r12 = "</value>"
            r11.append(r12)     // Catch:{ all -> 0x004c }
        L_0x0089:
            java.lang.String r11 = "</property>"
            r0.append(r11)     // Catch:{ all -> 0x004c }
            goto L_0x002d
        L_0x008f:
            boolean r11 = r9 instanceof java.lang.Long     // Catch:{ all -> 0x004c }
            if (r11 == 0) goto L_0x00a3
            java.lang.String r11 = "long\">"
            java.lang.StringBuilder r11 = r0.append(r11)     // Catch:{ all -> 0x004c }
            java.lang.StringBuilder r11 = r11.append(r9)     // Catch:{ all -> 0x004c }
            java.lang.String r12 = "</value>"
            r11.append(r12)     // Catch:{ all -> 0x004c }
            goto L_0x0089
        L_0x00a3:
            boolean r11 = r9 instanceof java.lang.Float     // Catch:{ all -> 0x004c }
            if (r11 == 0) goto L_0x00b7
            java.lang.String r11 = "float\">"
            java.lang.StringBuilder r11 = r0.append(r11)     // Catch:{ all -> 0x004c }
            java.lang.StringBuilder r11 = r11.append(r9)     // Catch:{ all -> 0x004c }
            java.lang.String r12 = "</value>"
            r11.append(r12)     // Catch:{ all -> 0x004c }
            goto L_0x0089
        L_0x00b7:
            boolean r11 = r9 instanceof java.lang.Double     // Catch:{ all -> 0x004c }
            if (r11 == 0) goto L_0x00cb
            java.lang.String r11 = "double\">"
            java.lang.StringBuilder r11 = r0.append(r11)     // Catch:{ all -> 0x004c }
            java.lang.StringBuilder r11 = r11.append(r9)     // Catch:{ all -> 0x004c }
            java.lang.String r12 = "</value>"
            r11.append(r12)     // Catch:{ all -> 0x004c }
            goto L_0x0089
        L_0x00cb:
            boolean r11 = r9 instanceof java.lang.Boolean     // Catch:{ all -> 0x004c }
            if (r11 == 0) goto L_0x00df
            java.lang.String r11 = "boolean\">"
            java.lang.StringBuilder r11 = r0.append(r11)     // Catch:{ all -> 0x004c }
            java.lang.StringBuilder r11 = r11.append(r9)     // Catch:{ all -> 0x004c }
            java.lang.String r12 = "</value>"
            r11.append(r12)     // Catch:{ all -> 0x004c }
            goto L_0x0089
        L_0x00df:
            boolean r11 = r9 instanceof java.lang.String     // Catch:{ all -> 0x004c }
            if (r11 == 0) goto L_0x00f7
            java.lang.String r11 = "string\">"
            r0.append(r11)     // Catch:{ all -> 0x004c }
            java.lang.String r9 = (java.lang.String) r9     // Catch:{ all -> 0x004c }
            java.lang.String r11 = org.jivesoftware.smack.util.StringUtils.escapeForXML(r9)     // Catch:{ all -> 0x004c }
            r0.append(r11)     // Catch:{ all -> 0x004c }
            java.lang.String r11 = "</value>"
            r0.append(r11)     // Catch:{ all -> 0x004c }
            goto L_0x0089
        L_0x00f7:
            r1 = 0
            r7 = 0
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x012b }
            r2.<init>()     // Catch:{ Exception -> 0x012b }
            java.io.ObjectOutputStream r8 = new java.io.ObjectOutputStream     // Catch:{ Exception -> 0x0159, all -> 0x0152 }
            r8.<init>(r2)     // Catch:{ Exception -> 0x0159, all -> 0x0152 }
            r8.writeObject(r9)     // Catch:{ Exception -> 0x015c, all -> 0x0155 }
            java.lang.String r11 = "java-object\">"
            r0.append(r11)     // Catch:{ Exception -> 0x015c, all -> 0x0155 }
            byte[] r11 = r2.toByteArray()     // Catch:{ Exception -> 0x015c, all -> 0x0155 }
            java.lang.String r4 = org.jivesoftware.smack.util.StringUtils.encodeBase64(r11)     // Catch:{ Exception -> 0x015c, all -> 0x0155 }
            java.lang.StringBuilder r11 = r0.append(r4)     // Catch:{ Exception -> 0x015c, all -> 0x0155 }
            java.lang.String r12 = "</value>"
            r11.append(r12)     // Catch:{ Exception -> 0x015c, all -> 0x0155 }
            if (r8 == 0) goto L_0x0121
            r8.close()     // Catch:{ Exception -> 0x0150 }
        L_0x0121:
            if (r2 == 0) goto L_0x0089
            r2.close()     // Catch:{ Exception -> 0x0128 }
            goto L_0x0089
        L_0x0128:
            r11 = move-exception
            goto L_0x0089
        L_0x012b:
            r3 = move-exception
        L_0x012c:
            r3.printStackTrace()     // Catch:{ all -> 0x013e }
            if (r7 == 0) goto L_0x0134
            r7.close()     // Catch:{ Exception -> 0x014a }
        L_0x0134:
            if (r1 == 0) goto L_0x0089
            r1.close()     // Catch:{ Exception -> 0x013b }
            goto L_0x0089
        L_0x013b:
            r11 = move-exception
            goto L_0x0089
        L_0x013e:
            r10 = move-exception
        L_0x013f:
            if (r7 == 0) goto L_0x0144
            r7.close()     // Catch:{ Exception -> 0x014c }
        L_0x0144:
            if (r1 == 0) goto L_0x0149
            r1.close()     // Catch:{ Exception -> 0x014e }
        L_0x0149:
            throw r10     // Catch:{ all -> 0x004c }
        L_0x014a:
            r11 = move-exception
            goto L_0x0134
        L_0x014c:
            r11 = move-exception
            goto L_0x0144
        L_0x014e:
            r11 = move-exception
            goto L_0x0149
        L_0x0150:
            r11 = move-exception
            goto L_0x0121
        L_0x0152:
            r10 = move-exception
            r1 = r2
            goto L_0x013f
        L_0x0155:
            r10 = move-exception
            r7 = r8
            r1 = r2
            goto L_0x013f
        L_0x0159:
            r3 = move-exception
            r1 = r2
            goto L_0x012c
        L_0x015c:
            r3 = move-exception
            r7 = r8
            r1 = r2
            goto L_0x012c
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smack.packet.Packet.getExtensionsXML():java.lang.String");
    }

    public String getXmlns() {
        return this.xmlns;
    }

    public static String getDefaultLanguage() {
        return DEFAULT_LANGUAGE;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Packet packet = (Packet) o;
        if (this.error != null) {
            if (!this.error.equals(packet.error)) {
                return false;
            }
        } else if (packet.error != null) {
            return false;
        }
        if (this.from != null) {
            if (!this.from.equals(packet.from)) {
                return false;
            }
        } else if (packet.from != null) {
            return false;
        }
        if (!this.packetExtensions.equals(packet.packetExtensions)) {
            return false;
        }
        if (this.packetID != null) {
            if (!this.packetID.equals(packet.packetID)) {
                return false;
            }
        } else if (packet.packetID != null) {
            return false;
        }
        if (this.properties != null) {
            if (!this.properties.equals(packet.properties)) {
                return false;
            }
        } else if (packet.properties != null) {
            return false;
        }
        if (this.to != null) {
            if (!this.to.equals(packet.to)) {
                return false;
            }
        } else if (packet.to != null) {
            return false;
        }
        if (this.xmlns != null) {
            if (!this.xmlns.equals(packet.xmlns)) {
                return false;
            }
        } else if (packet.xmlns != null) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result;
        int i;
        int i2;
        int i3;
        int i4 = 0;
        if (this.xmlns != null) {
            result = this.xmlns.hashCode();
        } else {
            result = 0;
        }
        int i5 = result * 31;
        if (this.packetID != null) {
            i = this.packetID.hashCode();
        } else {
            i = 0;
        }
        int i6 = (i5 + i) * 31;
        if (this.to != null) {
            i2 = this.to.hashCode();
        } else {
            i2 = 0;
        }
        int i7 = (i6 + i2) * 31;
        if (this.from != null) {
            i3 = this.from.hashCode();
        } else {
            i3 = 0;
        }
        int hashCode = (((((i7 + i3) * 31) + this.packetExtensions.hashCode()) * 31) + this.properties.hashCode()) * 31;
        if (this.error != null) {
            i4 = this.error.hashCode();
        }
        return hashCode + i4;
    }
}
