package org.jivesoftware.smack;

import java.io.File;
import javax.net.SocketFactory;
import org.apache.harmony.javax.security.auth.callback.CallbackHandler;
import org.jivesoftware.smack.proxy.ProxyInfo;
import org.jivesoftware.smack.util.DNSUtil;

public class ConnectionConfiguration implements Cloneable {
    private CallbackHandler callbackHandler;
    private String capsNode = null;
    private boolean compressionEnabled = false;
    private boolean debuggerEnabled = Connection.DEBUG_ENABLED;
    private boolean expiredCertificatesCheckEnabled = false;
    private String host;
    private boolean isRosterVersioningAvailable = false;
    private String keystorePath;
    private String keystoreType;
    private boolean notMatchingDomainCheckEnabled = false;
    private String password;
    private String pkcs11Library;
    private int port;
    protected ProxyInfo proxy;
    private boolean reconnectionAllowed = true;
    private String resource;
    private boolean rosterLoadedAtLogin = true;
    private boolean saslAuthenticationEnabled = true;
    private SecurityMode securityMode = SecurityMode.enabled;
    private boolean selfSignedCertificateEnabled = false;
    private boolean sendPresence = true;
    private String serviceName;
    private SocketFactory socketFactory;
    private String truststorePassword;
    private String truststorePath;
    private String truststoreType;
    private String username;
    private boolean verifyChainEnabled = false;
    private boolean verifyRootCAEnabled = false;

    public enum SecurityMode {
        required,
        enabled,
        disabled
    }

    public ConnectionConfiguration(String serviceName2) {
        DNSUtil.HostAddress address = DNSUtil.resolveXMPPDomain(serviceName2);
        init(address.getHost(), address.getPort(), serviceName2, ProxyInfo.forDefaultProxy());
    }

    public ConnectionConfiguration(String serviceName2, ProxyInfo proxy2) {
        DNSUtil.HostAddress address = DNSUtil.resolveXMPPDomain(serviceName2);
        init(address.getHost(), address.getPort(), serviceName2, proxy2);
    }

    public ConnectionConfiguration(String host2, int port2, String serviceName2) {
        init(host2, port2, serviceName2, ProxyInfo.forDefaultProxy());
    }

    public ConnectionConfiguration(String host2, int port2, String serviceName2, ProxyInfo proxy2) {
        init(host2, port2, serviceName2, proxy2);
    }

    public ConnectionConfiguration(String host2, int port2) {
        init(host2, port2, host2, ProxyInfo.forDefaultProxy());
    }

    public ConnectionConfiguration(String host2, int port2, ProxyInfo proxy2) {
        init(host2, port2, host2, proxy2);
    }

    private void init(String host2, int port2, String serviceName2, ProxyInfo proxy2) {
        this.host = host2;
        this.port = port2;
        this.serviceName = serviceName2;
        this.proxy = proxy2;
        String javaHome = System.getProperty("java.home");
        StringBuilder buffer = new StringBuilder();
        buffer.append(javaHome).append(File.separator).append("lib");
        buffer.append(File.separator).append("security");
        buffer.append(File.separator).append("cacerts");
        this.truststorePath = buffer.toString();
        this.truststoreType = "jks";
        this.truststorePassword = "changeit";
        this.keystorePath = System.getProperty("javax.net.ssl.keyStore");
        this.keystoreType = "jks";
        this.pkcs11Library = "pkcs11.config";
        this.socketFactory = proxy2.getSocketFactory();
    }

    public void setServiceName(String serviceName2) {
        this.serviceName = serviceName2;
    }

    public String getServiceName() {
        return this.serviceName;
    }

    public String getHost() {
        return this.host;
    }

    public int getPort() {
        return this.port;
    }

    public SecurityMode getSecurityMode() {
        return this.securityMode;
    }

    public void setSecurityMode(SecurityMode securityMode2) {
        this.securityMode = securityMode2;
    }

    public String getTruststorePath() {
        return this.truststorePath;
    }

    public void setTruststorePath(String truststorePath2) {
        this.truststorePath = truststorePath2;
    }

    public String getTruststoreType() {
        return this.truststoreType;
    }

    public void setTruststoreType(String truststoreType2) {
        this.truststoreType = truststoreType2;
    }

    public String getTruststorePassword() {
        return this.truststorePassword;
    }

    public void setTruststorePassword(String truststorePassword2) {
        this.truststorePassword = truststorePassword2;
    }

    public String getKeystorePath() {
        return this.keystorePath;
    }

    public void setKeystorePath(String keystorePath2) {
        this.keystorePath = keystorePath2;
    }

    public String getKeystoreType() {
        return this.keystoreType;
    }

    public void setKeystoreType(String keystoreType2) {
        this.keystoreType = keystoreType2;
    }

    public String getPKCS11Library() {
        return this.pkcs11Library;
    }

    public void setPKCS11Library(String pkcs11Library2) {
        this.pkcs11Library = pkcs11Library2;
    }

    public boolean isVerifyChainEnabled() {
        return this.verifyChainEnabled;
    }

    public void setVerifyChainEnabled(boolean verifyChainEnabled2) {
        this.verifyChainEnabled = verifyChainEnabled2;
    }

    public boolean isVerifyRootCAEnabled() {
        return this.verifyRootCAEnabled;
    }

    public void setVerifyRootCAEnabled(boolean verifyRootCAEnabled2) {
        this.verifyRootCAEnabled = verifyRootCAEnabled2;
    }

    public boolean isSelfSignedCertificateEnabled() {
        return this.selfSignedCertificateEnabled;
    }

    public void setSelfSignedCertificateEnabled(boolean selfSignedCertificateEnabled2) {
        this.selfSignedCertificateEnabled = selfSignedCertificateEnabled2;
    }

    public boolean isExpiredCertificatesCheckEnabled() {
        return this.expiredCertificatesCheckEnabled;
    }

    public void setExpiredCertificatesCheckEnabled(boolean expiredCertificatesCheckEnabled2) {
        this.expiredCertificatesCheckEnabled = expiredCertificatesCheckEnabled2;
    }

    public boolean isNotMatchingDomainCheckEnabled() {
        return this.notMatchingDomainCheckEnabled;
    }

    public void setNotMatchingDomainCheckEnabled(boolean notMatchingDomainCheckEnabled2) {
        this.notMatchingDomainCheckEnabled = notMatchingDomainCheckEnabled2;
    }

    public boolean isCompressionEnabled() {
        return this.compressionEnabled;
    }

    public void setCompressionEnabled(boolean compressionEnabled2) {
        this.compressionEnabled = compressionEnabled2;
    }

    public boolean isSASLAuthenticationEnabled() {
        return this.saslAuthenticationEnabled;
    }

    public void setSASLAuthenticationEnabled(boolean saslAuthenticationEnabled2) {
        this.saslAuthenticationEnabled = saslAuthenticationEnabled2;
    }

    public boolean isDebuggerEnabled() {
        return this.debuggerEnabled;
    }

    public void setDebuggerEnabled(boolean debuggerEnabled2) {
        this.debuggerEnabled = debuggerEnabled2;
    }

    public void setReconnectionAllowed(boolean isAllowed) {
        this.reconnectionAllowed = isAllowed;
    }

    public boolean isReconnectionAllowed() {
        return this.reconnectionAllowed;
    }

    public void setSocketFactory(SocketFactory socketFactory2) {
        this.socketFactory = socketFactory2;
    }

    public void setSendPresence(boolean sendPresence2) {
        this.sendPresence = sendPresence2;
    }

    public boolean isRosterLoadedAtLogin() {
        return this.rosterLoadedAtLogin;
    }

    public void setRosterLoadedAtLogin(boolean rosterLoadedAtLogin2) {
        this.rosterLoadedAtLogin = rosterLoadedAtLogin2;
    }

    public CallbackHandler getCallbackHandler() {
        return this.callbackHandler;
    }

    public void setCallbackHandler(CallbackHandler callbackHandler2) {
        this.callbackHandler = callbackHandler2;
    }

    public SocketFactory getSocketFactory() {
        return this.socketFactory;
    }

    /* access modifiers changed from: package-private */
    public String getUsername() {
        return this.username;
    }

    /* access modifiers changed from: package-private */
    public String getPassword() {
        return this.password;
    }

    /* access modifiers changed from: package-private */
    public String getResource() {
        return this.resource;
    }

    /* access modifiers changed from: package-private */
    public boolean isRosterVersioningAvailable() {
        return this.isRosterVersioningAvailable;
    }

    /* access modifiers changed from: package-private */
    public void setRosterVersioningAvailable(boolean enabled) {
        this.isRosterVersioningAvailable = enabled;
    }

    /* access modifiers changed from: package-private */
    public void setCapsNode(String node) {
        this.capsNode = node;
    }

    /* access modifiers changed from: package-private */
    public String getCapsNode() {
        return this.capsNode;
    }

    /* access modifiers changed from: package-private */
    public boolean isSendPresence() {
        return this.sendPresence;
    }

    /* access modifiers changed from: package-private */
    public void setLoginInfo(String username2, String password2, String resource2) {
        this.username = username2;
        this.password = password2;
        this.resource = resource2;
    }
}
