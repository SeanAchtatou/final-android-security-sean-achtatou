package org.jivesoftware.smack;

import com.kenai.jbosh.BOSHClient;
import com.kenai.jbosh.BOSHClientConfig;
import com.kenai.jbosh.BOSHClientConnEvent;
import com.kenai.jbosh.BOSHClientConnListener;
import com.kenai.jbosh.BOSHClientRequestListener;
import com.kenai.jbosh.BOSHClientResponseListener;
import com.kenai.jbosh.BOSHException;
import com.kenai.jbosh.BOSHMessageEvent;
import com.kenai.jbosh.BodyQName;
import com.kenai.jbosh.ComposableBody;
import java.io.IOException;
import java.io.PipedReader;
import java.io.PipedWriter;
import java.io.Writer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.XMPPError;
import org.jivesoftware.smack.util.StringUtils;
import org.xbill.DNS.KEYRecord;

public class BOSHConnection extends Connection {
    public static final String BOSH_URI = "http://jabber.org/protocol/httpbind";
    public static final String XMPP_BOSH_NS = "urn:xmpp:xbosh";
    private boolean anonymous;
    protected String authID;
    private boolean authenticated;
    private BOSHClient client;
    /* access modifiers changed from: private */
    public final BOSHConfiguration config;
    /* access modifiers changed from: private */
    public boolean connected;
    /* access modifiers changed from: private */
    public boolean done;
    /* access modifiers changed from: private */
    public boolean isFirstInitialization;
    private ExecutorService listenerExecutor;
    /* access modifiers changed from: private */
    public Thread readerConsumer;
    /* access modifiers changed from: private */
    public PipedWriter readerPipe;
    private Roster roster;
    protected String sessionID;
    private String user;
    /* access modifiers changed from: private */
    public boolean wasAuthenticated;

    public BOSHConnection(boolean https, String host, int port, String filePath, String xmppDomain) {
        super(new BOSHConfiguration(https, host, port, filePath, xmppDomain));
        this.connected = false;
        this.authenticated = false;
        this.anonymous = false;
        this.isFirstInitialization = true;
        this.wasAuthenticated = false;
        this.done = false;
        this.authID = null;
        this.sessionID = null;
        this.user = null;
        this.roster = null;
        this.config = (BOSHConfiguration) getConfiguration();
    }

    public BOSHConnection(BOSHConfiguration config2) {
        super(config2);
        this.connected = false;
        this.authenticated = false;
        this.anonymous = false;
        this.isFirstInitialization = true;
        this.wasAuthenticated = false;
        this.done = false;
        this.authID = null;
        this.sessionID = null;
        this.user = null;
        this.roster = null;
        this.config = config2;
    }

    public void connect() throws XMPPException {
        if (this.connected) {
            throw new IllegalStateException("Already connected to a server.");
        }
        this.done = false;
        try {
            if (this.client != null) {
                this.client.close();
                this.client = null;
            }
            this.saslAuthentication.init();
            this.sessionID = null;
            this.authID = null;
            BOSHClientConfig.Builder cfgBuilder = BOSHClientConfig.Builder.create(this.config.getURI(), this.config.getServiceName());
            if (this.config.isProxyEnabled()) {
                cfgBuilder.setProxy(this.config.getProxyAddress(), this.config.getProxyPort());
            }
            this.client = BOSHClient.create(cfgBuilder.build());
            this.listenerExecutor = Executors.newSingleThreadExecutor(new ThreadFactory() {
                public Thread newThread(Runnable runnable) {
                    Thread thread = new Thread(runnable, "Smack Listener Processor (" + BOSHConnection.this.connectionCounterValue + ")");
                    thread.setDaemon(true);
                    return thread;
                }
            });
            this.client.addBOSHClientConnListener(new BOSHConnectionListener(this));
            this.client.addBOSHClientResponseListener(new BOSHPacketReader(this));
            if (this.config.isDebuggerEnabled()) {
                initDebugger();
                if (this.isFirstInitialization) {
                    if (this.debugger.getReaderListener() != null) {
                        addPacketListener(this.debugger.getReaderListener(), null);
                    }
                    if (this.debugger.getWriterListener() != null) {
                        addPacketSendingListener(this.debugger.getWriterListener(), null);
                    }
                }
            }
            this.client.send(ComposableBody.builder().setNamespaceDefinition("xmpp", XMPP_BOSH_NS).setAttribute(BodyQName.createWithPrefix(XMPP_BOSH_NS, "version", "xmpp"), "1.0").build());
            synchronized (this) {
                long endTime = System.currentTimeMillis() + ((long) (SmackConfiguration.getPacketReplyTimeout() * 6));
                while (!this.connected && System.currentTimeMillis() < endTime) {
                    try {
                        wait(Math.abs(endTime - System.currentTimeMillis()));
                    } catch (InterruptedException e) {
                    }
                }
            }
            if (!this.connected && !this.done) {
                this.done = true;
                String errorMessage = "Timeout reached for the connection to " + getHost() + ":" + getPort() + ".";
                throw new XMPPException(errorMessage, new XMPPError(XMPPError.Condition.remote_server_timeout, errorMessage));
            }
        } catch (Exception e2) {
            throw new XMPPException("Can't connect to " + getServiceName(), e2);
        }
    }

    public String getConnectionID() {
        if (!this.connected) {
            return null;
        }
        if (this.authID != null) {
            return this.authID;
        }
        return this.sessionID;
    }

    public Roster getRoster() {
        if (this.roster == null) {
            return null;
        }
        if (!this.config.isRosterLoadedAtLogin()) {
            this.roster.reload();
        }
        if (!this.roster.rosterInitialized) {
            try {
                synchronized (this.roster) {
                    long waitTime = (long) SmackConfiguration.getPacketReplyTimeout();
                    long start = System.currentTimeMillis();
                    while (!this.roster.rosterInitialized && waitTime > 0) {
                        this.roster.wait(waitTime);
                        long now = System.currentTimeMillis();
                        waitTime -= now - start;
                        start = now;
                    }
                }
            } catch (InterruptedException e) {
            }
        }
        return this.roster;
    }

    public String getUser() {
        return this.user;
    }

    public boolean isAnonymous() {
        return this.anonymous;
    }

    public boolean isAuthenticated() {
        return this.authenticated;
    }

    public boolean isConnected() {
        return this.connected;
    }

    public boolean isSecureConnection() {
        return false;
    }

    public boolean isUsingCompression() {
        return false;
    }

    public void login(String username, String password, String resource) throws XMPPException {
        String response;
        if (!isConnected()) {
            throw new IllegalStateException("Not connected to server.");
        } else if (this.authenticated) {
            throw new IllegalStateException("Already logged in to server.");
        } else {
            String username2 = username.toLowerCase().trim();
            if (!this.config.isSASLAuthenticationEnabled() || !this.saslAuthentication.hasNonAnonymousAuthentication()) {
                response = new NonSASLAuthentication(this).authenticate(username2, password, resource);
            } else if (password != null) {
                response = this.saslAuthentication.authenticate(username2, password, resource);
            } else {
                response = this.saslAuthentication.authenticate(username2, resource, this.config.getCallbackHandler());
            }
            if (response != null) {
                this.user = response;
                this.config.setServiceName(StringUtils.parseServer(response));
            } else {
                this.user = String.valueOf(username2) + "@" + getServiceName();
                if (resource != null) {
                    this.user = String.valueOf(this.user) + "/" + resource;
                }
            }
            if (this.roster == null) {
                if (this.rosterStorage == null) {
                    this.roster = new Roster(this);
                } else {
                    this.roster = new Roster(this, this.rosterStorage);
                }
            }
            if (this.config.isRosterLoadedAtLogin()) {
                this.roster.reload();
            }
            if (this.config.isSendPresence()) {
                sendPacket(new Presence(Presence.Type.available));
            }
            this.authenticated = true;
            this.anonymous = false;
            this.config.setLoginInfo(username2, password, resource);
            if (this.config.isDebuggerEnabled() && this.debugger != null) {
                this.debugger.userHasLogged(this.user);
            }
        }
    }

    public void loginAnonymously() throws XMPPException {
        String response;
        if (!isConnected()) {
            throw new IllegalStateException("Not connected to server.");
        } else if (this.authenticated) {
            throw new IllegalStateException("Already logged in to server.");
        } else {
            if (!this.config.isSASLAuthenticationEnabled() || !this.saslAuthentication.hasAnonymousAuthentication()) {
                response = new NonSASLAuthentication(this).authenticateAnonymously();
            } else {
                response = this.saslAuthentication.authenticateAnonymously();
            }
            this.user = response;
            this.config.setServiceName(StringUtils.parseServer(response));
            this.roster = null;
            if (this.config.isSendPresence()) {
                sendPacket(new Presence(Presence.Type.available));
            }
            this.authenticated = true;
            this.anonymous = true;
            if (this.config.isDebuggerEnabled() && this.debugger != null) {
                this.debugger.userHasLogged(this.user);
            }
        }
    }

    public void sendPacket(Packet packet) {
        if (!isConnected()) {
            throw new IllegalStateException("Not connected to server.");
        } else if (packet == null) {
            throw new NullPointerException("Packet is null.");
        } else if (!this.done) {
            firePacketInterceptors(packet);
            try {
                send(ComposableBody.builder().setPayloadXML(packet.toXML()).build());
                firePacketSendingListeners(packet);
            } catch (BOSHException e) {
                e.printStackTrace();
            }
        }
    }

    public void disconnect(Presence unavailablePresence) {
        if (this.connected) {
            shutdown(unavailablePresence);
            if (this.roster != null) {
                this.roster.cleanup();
                this.roster = null;
            }
            this.sendListeners.clear();
            this.recvListeners.clear();
            this.collectors.clear();
            this.interceptors.clear();
            this.wasAuthenticated = false;
            this.isFirstInitialization = true;
            for (ConnectionListener listener : getConnectionListeners()) {
                try {
                    listener.connectionClosed();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void shutdown(Presence unavailablePresence) {
        setWasAuthenticated(this.authenticated);
        this.authID = null;
        this.sessionID = null;
        this.done = true;
        this.authenticated = false;
        this.connected = false;
        this.isFirstInitialization = false;
        try {
            this.client.disconnect(ComposableBody.builder().setNamespaceDefinition("xmpp", XMPP_BOSH_NS).setPayloadXML(unavailablePresence.toXML()).build());
            Thread.sleep(150);
        } catch (Exception e) {
        }
        if (this.readerPipe != null) {
            try {
                this.readerPipe.close();
            } catch (Throwable th) {
            }
            this.reader = null;
        }
        if (this.reader != null) {
            try {
                this.reader.close();
            } catch (Throwable th2) {
            }
            this.reader = null;
        }
        if (this.writer != null) {
            try {
                this.writer.close();
            } catch (Throwable th3) {
            }
            this.writer = null;
        }
        if (this.listenerExecutor != null) {
            this.listenerExecutor.shutdown();
        }
        this.readerConsumer = null;
    }

    private void setWasAuthenticated(boolean wasAuthenticated2) {
        if (!this.wasAuthenticated) {
            this.wasAuthenticated = wasAuthenticated2;
        }
    }

    /* access modifiers changed from: protected */
    public void send(ComposableBody body) throws BOSHException {
        if (!this.connected) {
            throw new IllegalStateException("Not connected to a server!");
        } else if (body == null) {
            throw new NullPointerException("Body mustn't be null!");
        } else {
            if (this.sessionID != null) {
                body = body.rebuild().setAttribute(BodyQName.create(BOSH_URI, "sid"), this.sessionID).build();
            }
            this.client.send(body);
        }
    }

    /* access modifiers changed from: protected */
    public void processPacket(Packet packet) {
        if (packet != null) {
            for (PacketCollector collector : getPacketCollectors()) {
                collector.processPacket(packet);
            }
            this.listenerExecutor.submit(new ListenerNotification(packet));
        }
    }

    /* access modifiers changed from: protected */
    public void initDebugger() {
        this.writer = new Writer() {
            public void write(char[] cbuf, int off, int len) {
            }

            public void close() {
            }

            public void flush() {
            }
        };
        try {
            this.readerPipe = new PipedWriter();
            this.reader = new PipedReader(this.readerPipe);
        } catch (IOException e) {
        }
        super.initDebugger();
        this.client.addBOSHClientResponseListener(new BOSHClientResponseListener() {
            public void responseReceived(BOSHMessageEvent event) {
                if (event.getBody() != null) {
                    try {
                        BOSHConnection.this.readerPipe.write(event.getBody().toXML());
                        BOSHConnection.this.readerPipe.flush();
                    } catch (Exception e) {
                    }
                }
            }
        });
        this.client.addBOSHClientRequestListener(new BOSHClientRequestListener() {
            public void requestSent(BOSHMessageEvent event) {
                if (event.getBody() != null) {
                    try {
                        BOSHConnection.this.writer.write(event.getBody().toXML());
                    } catch (Exception e) {
                    }
                }
            }
        });
        this.readerConsumer = new Thread() {
            private int bufferLength = KEYRecord.Flags.FLAG5;
            private Thread thread = this;

            public void run() {
                try {
                    char[] cbuf = new char[this.bufferLength];
                    while (BOSHConnection.this.readerConsumer == this.thread && !BOSHConnection.this.done) {
                        BOSHConnection.this.reader.read(cbuf, 0, this.bufferLength);
                    }
                } catch (IOException e) {
                }
            }
        };
        this.readerConsumer.setDaemon(true);
        this.readerConsumer.start();
    }

    /* access modifiers changed from: protected */
    public void notifyConnectionError(Exception e) {
        shutdown(new Presence(Presence.Type.unavailable));
        e.printStackTrace();
        for (ConnectionListener listener : getConnectionListeners()) {
            try {
                listener.connectionClosedOnError(e);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    private class BOSHConnectionListener implements BOSHClientConnListener {
        private final BOSHConnection connection;

        public BOSHConnectionListener(BOSHConnection connection2) {
            this.connection = connection2;
        }

        public void connectionEvent(BOSHClientConnEvent connEvent) {
            try {
                if (connEvent.isConnected()) {
                    BOSHConnection.this.connected = true;
                    if (BOSHConnection.this.isFirstInitialization) {
                        BOSHConnection.this.isFirstInitialization = false;
                        for (ConnectionCreationListener listener : BOSHConnection.getConnectionCreationListeners()) {
                            listener.connectionCreated(this.connection);
                        }
                    } else {
                        try {
                            if (BOSHConnection.this.wasAuthenticated) {
                                this.connection.login(BOSHConnection.this.config.getUsername(), BOSHConnection.this.config.getPassword(), BOSHConnection.this.config.getResource());
                            }
                            for (ConnectionListener listener2 : BOSHConnection.this.getConnectionListeners()) {
                                listener2.reconnectionSuccessful();
                            }
                        } catch (XMPPException e) {
                            for (ConnectionListener listener3 : BOSHConnection.this.getConnectionListeners()) {
                                listener3.reconnectionFailed(e);
                            }
                        }
                    }
                } else {
                    if (connEvent.isError()) {
                        try {
                            connEvent.getCause();
                        } catch (Exception e2) {
                            BOSHConnection.this.notifyConnectionError(e2);
                        }
                    }
                    BOSHConnection.this.connected = false;
                }
                synchronized (this.connection) {
                    this.connection.notifyAll();
                }
            } catch (Throwable th) {
                synchronized (this.connection) {
                    this.connection.notifyAll();
                    throw th;
                }
            }
        }
    }

    private class ListenerNotification implements Runnable {
        private Packet packet;

        public ListenerNotification(Packet packet2) {
            this.packet = packet2;
        }

        public void run() {
            for (Connection.ListenerWrapper listenerWrapper : BOSHConnection.this.recvListeners.values()) {
                listenerWrapper.notifyListener(this.packet);
            }
        }
    }

    public void setRosterStorage(RosterStorage storage) throws IllegalStateException {
        if (this.roster != null) {
            throw new IllegalStateException("Roster is already initialized");
        }
        this.rosterStorage = storage;
    }
}
