package org.jivesoftware.smack;

import java.net.URI;
import java.net.URISyntaxException;
import org.jivesoftware.smack.proxy.ProxyInfo;

public class BOSHConfiguration extends ConnectionConfiguration {
    private String file;
    private boolean ssl;

    public BOSHConfiguration(String xmppDomain) {
        super(xmppDomain, 7070);
        setSASLAuthenticationEnabled(true);
        this.ssl = false;
        this.file = "/http-bind/";
    }

    public BOSHConfiguration(String xmppDomain, int port) {
        super(xmppDomain, port);
        setSASLAuthenticationEnabled(true);
        this.ssl = false;
        this.file = "/http-bind/";
    }

    public BOSHConfiguration(boolean https, String host, int port, String filePath, String xmppDomain) {
        super(host, port, xmppDomain);
        setSASLAuthenticationEnabled(true);
        this.ssl = https;
        this.file = filePath == null ? "/" : filePath;
    }

    public BOSHConfiguration(boolean https, String host, int port, String filePath, ProxyInfo proxy, String xmppDomain) {
        super(host, port, xmppDomain, proxy);
        setSASLAuthenticationEnabled(true);
        this.ssl = https;
        this.file = filePath == null ? "/" : filePath;
    }

    public boolean isProxyEnabled() {
        return (this.proxy == null || this.proxy.getProxyType() == ProxyInfo.ProxyType.NONE) ? false : true;
    }

    public ProxyInfo getProxyInfo() {
        return this.proxy;
    }

    public String getProxyAddress() {
        if (this.proxy != null) {
            return this.proxy.getProxyAddress();
        }
        return null;
    }

    public int getProxyPort() {
        if (this.proxy != null) {
            return this.proxy.getProxyPort();
        }
        return 8080;
    }

    public boolean isUsingSSL() {
        return this.ssl;
    }

    public URI getURI() throws URISyntaxException {
        if (this.file.charAt(0) != '/') {
            this.file = String.valueOf('/') + this.file;
        }
        return new URI(String.valueOf(this.ssl ? "https://" : "http://") + getHost() + ":" + getPort() + this.file);
    }
}
