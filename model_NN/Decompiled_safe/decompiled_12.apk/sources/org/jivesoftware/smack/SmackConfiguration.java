package org.jivesoftware.smack;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Vector;
import org.xmlpull.v1.XmlPullParser;

public final class SmackConfiguration {
    private static final String SMACK_VERSION = "3.1.0";
    private static Vector<String> defaultMechs = new Vector<>();
    private static int keepAliveInterval;
    private static int packetReplyTimeout;

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    static {
        /*
            r12 = 1
            r8 = 0
            r9 = 5000(0x1388, float:7.006E-42)
            org.jivesoftware.smack.SmackConfiguration.packetReplyTimeout = r9
            org.jivesoftware.smack.SmackConfiguration.keepAliveInterval = r8
            java.util.Vector r9 = new java.util.Vector
            r9.<init>()
            org.jivesoftware.smack.SmackConfiguration.defaultMechs = r9
            java.lang.ClassLoader[] r1 = getClassLoaders()     // Catch:{ Exception -> 0x00a1 }
            int r9 = r1.length     // Catch:{ Exception -> 0x00a1 }
        L_0x0014:
            if (r8 < r9) goto L_0x0017
        L_0x0016:
            return
        L_0x0017:
            r0 = r1[r8]     // Catch:{ Exception -> 0x00a1 }
            java.lang.String r10 = "META-INF/smack-config.xml"
            java.util.Enumeration r2 = r0.getResources(r10)     // Catch:{ Exception -> 0x00a1 }
        L_0x001f:
            boolean r10 = r2.hasMoreElements()     // Catch:{ Exception -> 0x00a1 }
            if (r10 != 0) goto L_0x0028
            int r8 = r8 + 1
            goto L_0x0014
        L_0x0028:
            java.lang.Object r7 = r2.nextElement()     // Catch:{ Exception -> 0x00a1 }
            java.net.URL r7 = (java.net.URL) r7     // Catch:{ Exception -> 0x00a1 }
            r6 = 0
            java.io.InputStream r6 = r7.openStream()     // Catch:{ Exception -> 0x007d }
            org.xmlpull.v1.XmlPullParserFactory r10 = org.xmlpull.v1.XmlPullParserFactory.newInstance()     // Catch:{ Exception -> 0x007d }
            org.xmlpull.v1.XmlPullParser r5 = r10.newPullParser()     // Catch:{ Exception -> 0x007d }
            java.lang.String r10 = "http://xmlpull.org/v1/doc/features.html#process-namespaces"
            r11 = 1
            r5.setFeature(r10, r11)     // Catch:{ Exception -> 0x007d }
            java.lang.String r10 = "UTF-8"
            r5.setInput(r6, r10)     // Catch:{ Exception -> 0x007d }
            int r4 = r5.getEventType()     // Catch:{ Exception -> 0x007d }
        L_0x004a:
            r10 = 2
            if (r4 != r10) goto L_0x005c
            java.lang.String r10 = r5.getName()     // Catch:{ Exception -> 0x007d }
            java.lang.String r11 = "className"
            boolean r10 = r10.equals(r11)     // Catch:{ Exception -> 0x007d }
            if (r10 == 0) goto L_0x0068
            parseClassToLoad(r5)     // Catch:{ Exception -> 0x007d }
        L_0x005c:
            int r4 = r5.next()     // Catch:{ Exception -> 0x007d }
            if (r4 != r12) goto L_0x004a
            r6.close()     // Catch:{ Exception -> 0x0066 }
            goto L_0x001f
        L_0x0066:
            r10 = move-exception
            goto L_0x001f
        L_0x0068:
            java.lang.String r10 = r5.getName()     // Catch:{ Exception -> 0x007d }
            java.lang.String r11 = "packetReplyTimeout"
            boolean r10 = r10.equals(r11)     // Catch:{ Exception -> 0x007d }
            if (r10 == 0) goto L_0x0087
            int r10 = org.jivesoftware.smack.SmackConfiguration.packetReplyTimeout     // Catch:{ Exception -> 0x007d }
            int r10 = parseIntProperty(r5, r10)     // Catch:{ Exception -> 0x007d }
            org.jivesoftware.smack.SmackConfiguration.packetReplyTimeout = r10     // Catch:{ Exception -> 0x007d }
            goto L_0x005c
        L_0x007d:
            r3 = move-exception
            r3.printStackTrace()     // Catch:{ all -> 0x009c }
            r6.close()     // Catch:{ Exception -> 0x0085 }
            goto L_0x001f
        L_0x0085:
            r10 = move-exception
            goto L_0x001f
        L_0x0087:
            java.lang.String r10 = r5.getName()     // Catch:{ Exception -> 0x007d }
            java.lang.String r11 = "keepAliveInterval"
            boolean r10 = r10.equals(r11)     // Catch:{ Exception -> 0x007d }
            if (r10 == 0) goto L_0x00a7
            int r10 = org.jivesoftware.smack.SmackConfiguration.keepAliveInterval     // Catch:{ Exception -> 0x007d }
            int r10 = parseIntProperty(r5, r10)     // Catch:{ Exception -> 0x007d }
            org.jivesoftware.smack.SmackConfiguration.keepAliveInterval = r10     // Catch:{ Exception -> 0x007d }
            goto L_0x005c
        L_0x009c:
            r8 = move-exception
            r6.close()     // Catch:{ Exception -> 0x00bd }
        L_0x00a0:
            throw r8     // Catch:{ Exception -> 0x00a1 }
        L_0x00a1:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x0016
        L_0x00a7:
            java.lang.String r10 = r5.getName()     // Catch:{ Exception -> 0x007d }
            java.lang.String r11 = "mechName"
            boolean r10 = r10.equals(r11)     // Catch:{ Exception -> 0x007d }
            if (r10 == 0) goto L_0x005c
            java.util.Vector<java.lang.String> r10 = org.jivesoftware.smack.SmackConfiguration.defaultMechs     // Catch:{ Exception -> 0x007d }
            java.lang.String r11 = r5.nextText()     // Catch:{ Exception -> 0x007d }
            r10.add(r11)     // Catch:{ Exception -> 0x007d }
            goto L_0x005c
        L_0x00bd:
            r9 = move-exception
            goto L_0x00a0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smack.SmackConfiguration.<clinit>():void");
    }

    private SmackConfiguration() {
    }

    public static String getVersion() {
        return SMACK_VERSION;
    }

    public static int getPacketReplyTimeout() {
        if (packetReplyTimeout <= 0) {
            packetReplyTimeout = 5000;
        }
        return packetReplyTimeout;
    }

    public static void setPacketReplyTimeout(int timeout) {
        if (timeout <= 0) {
            throw new IllegalArgumentException();
        }
        packetReplyTimeout = timeout;
    }

    public static int getKeepAliveInterval() {
        return keepAliveInterval;
    }

    public static void setKeepAliveInterval(int interval) {
        keepAliveInterval = interval;
    }

    public static void addSaslMech(String mech) {
        if (!defaultMechs.contains(mech)) {
            defaultMechs.add(mech);
        }
    }

    public static void addSaslMechs(Collection<String> mechs) {
        for (String mech : mechs) {
            addSaslMech(mech);
        }
    }

    public static void removeSaslMech(String mech) {
        if (defaultMechs.contains(mech)) {
            defaultMechs.remove(mech);
        }
    }

    public static void removeSaslMechs(Collection<String> mechs) {
        for (String mech : mechs) {
            removeSaslMech(mech);
        }
    }

    public static List<String> getSaslMechs() {
        return defaultMechs;
    }

    private static void parseClassToLoad(XmlPullParser parser) throws Exception {
        String className = parser.nextText();
        try {
            Class.forName(className);
        } catch (ClassNotFoundException e) {
            System.err.println("Error! A startup class specified in smack-config.xml could not be loaded: " + className);
        }
    }

    private static int parseIntProperty(XmlPullParser parser, int defaultValue) throws Exception {
        try {
            return Integer.parseInt(parser.nextText());
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
            return defaultValue;
        }
    }

    private static ClassLoader[] getClassLoaders() {
        ClassLoader[] classLoaders = {SmackConfiguration.class.getClassLoader(), Thread.currentThread().getContextClassLoader()};
        List<ClassLoader> loaders = new ArrayList<>();
        for (ClassLoader classLoader : classLoaders) {
            if (classLoader != null) {
                loaders.add(classLoader);
            }
        }
        return (ClassLoader[]) loaders.toArray(new ClassLoader[loaders.size()]);
    }
}
