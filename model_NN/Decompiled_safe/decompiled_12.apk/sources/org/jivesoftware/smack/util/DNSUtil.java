package org.jivesoftware.smack.util;

import java.util.Map;
import org.xbill.DNS.Lookup;
import org.xbill.DNS.Record;
import org.xbill.DNS.SRVRecord;
import org.xbill.DNS.TextParseException;

public class DNSUtil {
    private static Map<String, HostAddress> ccache = new Cache(100, 600000);
    private static Map<String, HostAddress> scache = new Cache(100, 600000);

    private static HostAddress resolveSRV(String domain) {
        String bestHost = null;
        int bestPort = -1;
        int bestPriority = Integer.MAX_VALUE;
        int bestWeight = 0;
        try {
            Record[] recs = new Lookup(domain, 33).run();
            if (recs == null) {
                return null;
            }
            for (Record rec : recs) {
                SRVRecord record = (SRVRecord) rec;
                if (!(record == null || record.getTarget() == null)) {
                    int weight = (int) (((double) (record.getWeight() * record.getWeight())) * Math.random());
                    if (record.getPriority() < bestPriority) {
                        bestPriority = record.getPriority();
                        bestWeight = weight;
                        bestHost = record.getTarget().toString();
                        bestPort = record.getPort();
                    } else if (record.getPriority() == bestPriority && weight > bestWeight) {
                        bestPriority = record.getPriority();
                        bestWeight = weight;
                        bestHost = record.getTarget().toString();
                        bestPort = record.getPort();
                    }
                }
            }
            if (bestHost == null) {
                return null;
            }
            if (bestHost.endsWith(".")) {
                bestHost = bestHost.substring(0, bestHost.length() - 1);
            }
            return new HostAddress(bestHost, bestPort, null);
        } catch (NullPointerException | TextParseException e) {
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002b, code lost:
        if (r1 != null) goto L_0x0040;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002d, code lost:
        r1 = resolveSRV("_jabber._tcp." + r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0040, code lost:
        if (r1 != null) goto L_0x004a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0042, code lost:
        r1 = new org.jivesoftware.smack.util.DNSUtil.HostAddress(r4, 5222, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x004a, code lost:
        r3 = org.jivesoftware.smack.util.DNSUtil.ccache;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004c, code lost:
        monitor-enter(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        org.jivesoftware.smack.util.DNSUtil.ccache.put(r4, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0052, code lost:
        monitor-exit(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0018, code lost:
        r1 = resolveSRV("_xmpp-client._tcp." + r4);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static org.jivesoftware.smack.util.DNSUtil.HostAddress resolveXMPPDomain(java.lang.String r4) {
        /*
            java.util.Map<java.lang.String, org.jivesoftware.smack.util.DNSUtil$HostAddress> r3 = org.jivesoftware.smack.util.DNSUtil.ccache
            monitor-enter(r3)
            java.util.Map<java.lang.String, org.jivesoftware.smack.util.DNSUtil$HostAddress> r2 = org.jivesoftware.smack.util.DNSUtil.ccache     // Catch:{ all -> 0x0055 }
            boolean r2 = r2.containsKey(r4)     // Catch:{ all -> 0x0055 }
            if (r2 == 0) goto L_0x0017
            java.util.Map<java.lang.String, org.jivesoftware.smack.util.DNSUtil$HostAddress> r2 = org.jivesoftware.smack.util.DNSUtil.ccache     // Catch:{ all -> 0x0055 }
            java.lang.Object r0 = r2.get(r4)     // Catch:{ all -> 0x0055 }
            org.jivesoftware.smack.util.DNSUtil$HostAddress r0 = (org.jivesoftware.smack.util.DNSUtil.HostAddress) r0     // Catch:{ all -> 0x0055 }
            if (r0 == 0) goto L_0x0017
            monitor-exit(r3)     // Catch:{ all -> 0x0055 }
        L_0x0016:
            return r0
        L_0x0017:
            monitor-exit(r3)     // Catch:{ all -> 0x0055 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "_xmpp-client._tcp."
            r2.<init>(r3)
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            org.jivesoftware.smack.util.DNSUtil$HostAddress r1 = resolveSRV(r2)
            if (r1 != 0) goto L_0x0040
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "_jabber._tcp."
            r2.<init>(r3)
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            org.jivesoftware.smack.util.DNSUtil$HostAddress r1 = resolveSRV(r2)
        L_0x0040:
            if (r1 != 0) goto L_0x004a
            org.jivesoftware.smack.util.DNSUtil$HostAddress r1 = new org.jivesoftware.smack.util.DNSUtil$HostAddress
            r2 = 5222(0x1466, float:7.318E-42)
            r3 = 0
            r1.<init>(r4, r2, r3)
        L_0x004a:
            java.util.Map<java.lang.String, org.jivesoftware.smack.util.DNSUtil$HostAddress> r3 = org.jivesoftware.smack.util.DNSUtil.ccache
            monitor-enter(r3)
            java.util.Map<java.lang.String, org.jivesoftware.smack.util.DNSUtil$HostAddress> r2 = org.jivesoftware.smack.util.DNSUtil.ccache     // Catch:{ all -> 0x0058 }
            r2.put(r4, r1)     // Catch:{ all -> 0x0058 }
            monitor-exit(r3)     // Catch:{ all -> 0x0058 }
            r0 = r1
            goto L_0x0016
        L_0x0055:
            r2 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0055 }
            throw r2
        L_0x0058:
            r2 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0058 }
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smack.util.DNSUtil.resolveXMPPDomain(java.lang.String):org.jivesoftware.smack.util.DNSUtil$HostAddress");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002b, code lost:
        if (r1 != null) goto L_0x0040;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002d, code lost:
        r1 = resolveSRV("_jabber._tcp." + r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0040, code lost:
        if (r1 != null) goto L_0x004a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0042, code lost:
        r1 = new org.jivesoftware.smack.util.DNSUtil.HostAddress(r4, 5269, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x004a, code lost:
        r3 = org.jivesoftware.smack.util.DNSUtil.scache;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004c, code lost:
        monitor-enter(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        org.jivesoftware.smack.util.DNSUtil.scache.put(r4, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0052, code lost:
        monitor-exit(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0018, code lost:
        r1 = resolveSRV("_xmpp-server._tcp." + r4);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static org.jivesoftware.smack.util.DNSUtil.HostAddress resolveXMPPServerDomain(java.lang.String r4) {
        /*
            java.util.Map<java.lang.String, org.jivesoftware.smack.util.DNSUtil$HostAddress> r3 = org.jivesoftware.smack.util.DNSUtil.scache
            monitor-enter(r3)
            java.util.Map<java.lang.String, org.jivesoftware.smack.util.DNSUtil$HostAddress> r2 = org.jivesoftware.smack.util.DNSUtil.scache     // Catch:{ all -> 0x0055 }
            boolean r2 = r2.containsKey(r4)     // Catch:{ all -> 0x0055 }
            if (r2 == 0) goto L_0x0017
            java.util.Map<java.lang.String, org.jivesoftware.smack.util.DNSUtil$HostAddress> r2 = org.jivesoftware.smack.util.DNSUtil.scache     // Catch:{ all -> 0x0055 }
            java.lang.Object r0 = r2.get(r4)     // Catch:{ all -> 0x0055 }
            org.jivesoftware.smack.util.DNSUtil$HostAddress r0 = (org.jivesoftware.smack.util.DNSUtil.HostAddress) r0     // Catch:{ all -> 0x0055 }
            if (r0 == 0) goto L_0x0017
            monitor-exit(r3)     // Catch:{ all -> 0x0055 }
        L_0x0016:
            return r0
        L_0x0017:
            monitor-exit(r3)     // Catch:{ all -> 0x0055 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "_xmpp-server._tcp."
            r2.<init>(r3)
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            org.jivesoftware.smack.util.DNSUtil$HostAddress r1 = resolveSRV(r2)
            if (r1 != 0) goto L_0x0040
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "_jabber._tcp."
            r2.<init>(r3)
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            org.jivesoftware.smack.util.DNSUtil$HostAddress r1 = resolveSRV(r2)
        L_0x0040:
            if (r1 != 0) goto L_0x004a
            org.jivesoftware.smack.util.DNSUtil$HostAddress r1 = new org.jivesoftware.smack.util.DNSUtil$HostAddress
            r2 = 5269(0x1495, float:7.383E-42)
            r3 = 0
            r1.<init>(r4, r2, r3)
        L_0x004a:
            java.util.Map<java.lang.String, org.jivesoftware.smack.util.DNSUtil$HostAddress> r3 = org.jivesoftware.smack.util.DNSUtil.scache
            monitor-enter(r3)
            java.util.Map<java.lang.String, org.jivesoftware.smack.util.DNSUtil$HostAddress> r2 = org.jivesoftware.smack.util.DNSUtil.scache     // Catch:{ all -> 0x0058 }
            r2.put(r4, r1)     // Catch:{ all -> 0x0058 }
            monitor-exit(r3)     // Catch:{ all -> 0x0058 }
            r0 = r1
            goto L_0x0016
        L_0x0055:
            r2 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0055 }
            throw r2
        L_0x0058:
            r2 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0058 }
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smack.util.DNSUtil.resolveXMPPServerDomain(java.lang.String):org.jivesoftware.smack.util.DNSUtil$HostAddress");
    }

    public static class HostAddress {
        private String host;
        private int port;

        private HostAddress(String host2, int port2) {
            this.host = host2;
            this.port = port2;
        }

        /* synthetic */ HostAddress(String str, int i, HostAddress hostAddress) {
            this(str, i);
        }

        public String getHost() {
            return this.host;
        }

        public int getPort() {
            return this.port;
        }

        public String toString() {
            return String.valueOf(this.host) + ":" + this.port;
        }

        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof HostAddress)) {
                return false;
            }
            HostAddress address = (HostAddress) o;
            if (!this.host.equals(address.host)) {
                return false;
            }
            if (this.port != address.port) {
                return false;
            }
            return true;
        }
    }
}
