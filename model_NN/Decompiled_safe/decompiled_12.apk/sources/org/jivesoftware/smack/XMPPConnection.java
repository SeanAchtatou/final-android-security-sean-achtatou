package org.jivesoftware.smack;

import android.util.Log;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Collection;
import org.apache.harmony.javax.security.auth.callback.CallbackHandler;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.XMPPError;
import org.jivesoftware.smack.util.StringUtils;

public class XMPPConnection extends Connection {
    private boolean anonymous = false;
    private boolean authenticated = false;
    private Collection<String> compressionMethods;
    private boolean connected = false;
    String connectionID = null;
    PacketReader packetReader;
    PacketWriter packetWriter;
    Roster roster = null;
    protected Socket socket;
    private String user = null;
    private boolean usingCompression;
    private boolean usingTLS = false;
    private boolean wasAuthenticated = false;

    public XMPPConnection(String serviceName, CallbackHandler callbackHandler) {
        super(new ConnectionConfiguration(serviceName));
        this.config.setCompressionEnabled(false);
        this.config.setSASLAuthenticationEnabled(true);
        this.config.setDebuggerEnabled(DEBUG_ENABLED);
        this.config.setCallbackHandler(callbackHandler);
    }

    public XMPPConnection(String serviceName) {
        super(new ConnectionConfiguration(serviceName));
        this.config.setCompressionEnabled(false);
        this.config.setSASLAuthenticationEnabled(true);
        this.config.setDebuggerEnabled(DEBUG_ENABLED);
    }

    public XMPPConnection(ConnectionConfiguration config) {
        super(config);
        if (config == null) {
            Log.e("config", "is null");
        } else if (config.getUsername() != null) {
            Log.e("config.getusername", config.getUsername());
        } else {
            Log.e("config.getusername", "is null");
        }
    }

    public XMPPConnection(ConnectionConfiguration config, CallbackHandler callbackHandler) {
        super(config);
        config.setCallbackHandler(callbackHandler);
    }

    public String getConnectionID() {
        if (!isConnected()) {
            return null;
        }
        return this.connectionID;
    }

    public String getUser() {
        if (!isAuthenticated()) {
            return null;
        }
        return this.user;
    }

    public synchronized void login(String username, String password, String resource) throws XMPPException {
        String response;
        if (username != null) {
            Log.e("username", username);
        } else {
            Log.e("username", "is null");
        }
        if (!isConnected()) {
            throw new IllegalStateException("Not connected to server.");
        } else if (this.authenticated) {
            throw new IllegalStateException("Already logged in to server.");
        } else {
            String username2 = username.toLowerCase().trim();
            if (!this.config.isSASLAuthenticationEnabled() || !this.saslAuthentication.hasNonAnonymousAuthentication()) {
                response = new NonSASLAuthentication(this).authenticate(username2, password, resource);
            } else if (password != null) {
                response = this.saslAuthentication.authenticate(username2, password, resource);
            } else {
                response = this.saslAuthentication.authenticate(username2, resource, this.config.getCallbackHandler());
            }
            if (response != null) {
                this.user = response;
                this.config.setServiceName(StringUtils.parseServer(response));
            } else {
                this.user = String.valueOf(username2) + "@" + getServiceName();
                if (resource != null) {
                    this.user = String.valueOf(this.user) + "/" + resource;
                }
            }
            if (this.config.isCompressionEnabled()) {
                useCompression();
            }
            if (this.roster == null) {
                if (this.rosterStorage == null) {
                    this.roster = new Roster(this);
                } else {
                    this.roster = new Roster(this, this.rosterStorage);
                }
            }
            if (this.config.isRosterLoadedAtLogin()) {
                this.roster.reload();
            }
            if (this.config.isSendPresence()) {
                this.packetWriter.sendPacket(new Presence(Presence.Type.available));
            }
            this.authenticated = true;
            this.anonymous = false;
            this.config.setLoginInfo(username2, password, resource);
            if (this.config.isDebuggerEnabled() && this.debugger != null) {
                this.debugger.userHasLogged(this.user);
            }
        }
    }

    public synchronized void loginAnonymously() throws XMPPException {
        String response;
        if (!isConnected()) {
            throw new IllegalStateException("Not connected to server.");
        } else if (this.authenticated) {
            throw new IllegalStateException("Already logged in to server.");
        } else {
            if (!this.config.isSASLAuthenticationEnabled() || !this.saslAuthentication.hasAnonymousAuthentication()) {
                response = new NonSASLAuthentication(this).authenticateAnonymously();
            } else {
                response = this.saslAuthentication.authenticateAnonymously();
            }
            this.user = response;
            this.config.setServiceName(StringUtils.parseServer(response));
            if (this.config.isCompressionEnabled()) {
                useCompression();
            }
            this.roster = null;
            this.packetWriter.sendPacket(new Presence(Presence.Type.available));
            this.authenticated = true;
            this.anonymous = true;
            if (this.config.isDebuggerEnabled() && this.debugger != null) {
                this.debugger.userHasLogged(this.user);
            }
        }
    }

    public Roster getRoster() {
        if (this.roster == null) {
            return null;
        }
        if (!this.roster.rosterInitialized) {
            try {
                synchronized (this.roster) {
                    long waitTime = (long) SmackConfiguration.getPacketReplyTimeout();
                    long start = System.currentTimeMillis();
                    while (!this.roster.rosterInitialized && waitTime > 0) {
                        this.roster.wait(waitTime);
                        long now = System.currentTimeMillis();
                        waitTime -= now - start;
                        start = now;
                    }
                }
            } catch (InterruptedException e) {
            }
        }
        return this.roster;
    }

    public boolean isConnected() {
        return this.connected;
    }

    public boolean isSecureConnection() {
        return isUsingTLS();
    }

    public boolean isAuthenticated() {
        return this.authenticated;
    }

    public boolean isAnonymous() {
        return this.anonymous;
    }

    /* access modifiers changed from: protected */
    public void shutdown(Presence unavailablePresence) {
        if (this.packetWriter != null) {
            this.packetWriter.sendPacket(unavailablePresence);
        }
        setWasAuthenticated(this.authenticated);
        this.authenticated = false;
        this.connected = false;
        if (this.packetReader != null) {
            this.packetReader.shutdown();
        }
        if (this.packetWriter != null) {
            this.packetWriter.shutdown();
        }
        try {
            Thread.sleep(150);
        } catch (Exception e) {
        }
        if (this.reader != null) {
            try {
                this.reader.close();
            } catch (Throwable th) {
            }
            this.reader = null;
        }
        if (this.writer != null) {
            try {
                this.writer.close();
            } catch (Throwable th2) {
            }
            this.writer = null;
        }
        try {
            this.socket.close();
        } catch (Exception e2) {
        }
        this.saslAuthentication.init();
    }

    public void disconnect(Presence unavailablePresence) {
        if (this.packetReader != null && this.packetWriter != null) {
            shutdown(unavailablePresence);
            if (this.roster != null) {
                this.roster.cleanup();
                this.roster = null;
            }
            this.wasAuthenticated = false;
            this.packetWriter.cleanup();
            this.packetWriter = null;
            this.packetReader.cleanup();
            this.packetReader = null;
        }
    }

    public void sendPacket(Packet packet) {
        if (!isConnected()) {
            throw new IllegalStateException("Not connected to server.");
        } else if (packet == null) {
            throw new NullPointerException("Packet is null.");
        } else {
            this.packetWriter.sendPacket(packet);
        }
    }

    public void addPacketWriterInterceptor(PacketInterceptor packetInterceptor, PacketFilter packetFilter) {
        addPacketInterceptor(packetInterceptor, packetFilter);
    }

    public void removePacketWriterInterceptor(PacketInterceptor packetInterceptor) {
        removePacketInterceptor(packetInterceptor);
    }

    public void addPacketWriterListener(PacketListener packetListener, PacketFilter packetFilter) {
        addPacketSendingListener(packetListener, packetFilter);
    }

    public void removePacketWriterListener(PacketListener packetListener) {
        removePacketSendingListener(packetListener);
    }

    private void connectUsingConfiguration(ConnectionConfiguration config) throws XMPPException {
        String host = config.getHost();
        int port = config.getPort();
        try {
            if (config.getSocketFactory() == null) {
                this.socket = new Socket(host, port);
            } else {
                this.socket = config.getSocketFactory().createSocket(host, port);
            }
            initConnection();
        } catch (UnknownHostException uhe) {
            String errorMessage = "Could not connect to " + host + ":" + port + ".";
            throw new XMPPException(errorMessage, new XMPPError(XMPPError.Condition.remote_server_timeout, errorMessage), uhe);
        } catch (IOException ioe) {
            String errorMessage2 = "XMPPError connecting to " + host + ":" + port + ".";
            throw new XMPPException(errorMessage2, new XMPPError(XMPPError.Condition.remote_server_error, errorMessage2), ioe);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0085 A[SYNTHETIC, Splitter:B:29:0x0085] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0090 A[SYNTHETIC, Splitter:B:34:0x0090] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x009b A[SYNTHETIC, Splitter:B:39:0x009b] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00a6 A[SYNTHETIC, Splitter:B:44:0x00a6] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void initConnection() throws org.jivesoftware.smack.XMPPException {
        /*
            r7 = this;
            r1 = 1
            r3 = 0
            r6 = 0
            org.jivesoftware.smack.PacketReader r4 = r7.packetReader
            if (r4 == 0) goto L_0x000c
            org.jivesoftware.smack.PacketWriter r4 = r7.packetWriter
            if (r4 == 0) goto L_0x000c
            r1 = r3
        L_0x000c:
            if (r1 != 0) goto L_0x0010
            r7.usingCompression = r3
        L_0x0010:
            r7.initReaderAndWriter()
            if (r1 == 0) goto L_0x006a
            org.jivesoftware.smack.PacketWriter r4 = new org.jivesoftware.smack.PacketWriter     // Catch:{ XMPPException -> 0x0075 }
            r4.<init>(r7)     // Catch:{ XMPPException -> 0x0075 }
            r7.packetWriter = r4     // Catch:{ XMPPException -> 0x0075 }
            org.jivesoftware.smack.PacketReader r4 = new org.jivesoftware.smack.PacketReader     // Catch:{ XMPPException -> 0x0075 }
            r4.<init>(r7)     // Catch:{ XMPPException -> 0x0075 }
            r7.packetReader = r4     // Catch:{ XMPPException -> 0x0075 }
            org.jivesoftware.smack.ConnectionConfiguration r4 = r7.config     // Catch:{ XMPPException -> 0x0075 }
            boolean r4 = r4.isDebuggerEnabled()     // Catch:{ XMPPException -> 0x0075 }
            if (r4 == 0) goto L_0x0047
            org.jivesoftware.smack.debugger.SmackDebugger r4 = r7.debugger     // Catch:{ XMPPException -> 0x0075 }
            org.jivesoftware.smack.PacketListener r4 = r4.getReaderListener()     // Catch:{ XMPPException -> 0x0075 }
            r5 = 0
            r7.addPacketListener(r4, r5)     // Catch:{ XMPPException -> 0x0075 }
            org.jivesoftware.smack.debugger.SmackDebugger r4 = r7.debugger     // Catch:{ XMPPException -> 0x0075 }
            org.jivesoftware.smack.PacketListener r4 = r4.getWriterListener()     // Catch:{ XMPPException -> 0x0075 }
            if (r4 == 0) goto L_0x0047
            org.jivesoftware.smack.debugger.SmackDebugger r4 = r7.debugger     // Catch:{ XMPPException -> 0x0075 }
            org.jivesoftware.smack.PacketListener r4 = r4.getWriterListener()     // Catch:{ XMPPException -> 0x0075 }
            r5 = 0
            r7.addPacketSendingListener(r4, r5)     // Catch:{ XMPPException -> 0x0075 }
        L_0x0047:
            org.jivesoftware.smack.PacketWriter r4 = r7.packetWriter     // Catch:{ XMPPException -> 0x0075 }
            r4.startup()     // Catch:{ XMPPException -> 0x0075 }
            org.jivesoftware.smack.PacketReader r4 = r7.packetReader     // Catch:{ XMPPException -> 0x0075 }
            r4.startup()     // Catch:{ XMPPException -> 0x0075 }
            r4 = 1
            r7.connected = r4     // Catch:{ XMPPException -> 0x0075 }
            org.jivesoftware.smack.PacketWriter r4 = r7.packetWriter     // Catch:{ XMPPException -> 0x0075 }
            r4.startKeepAliveProcess()     // Catch:{ XMPPException -> 0x0075 }
            if (r1 == 0) goto L_0x00c1
            java.util.Collection r4 = getConnectionCreationListeners()     // Catch:{ XMPPException -> 0x0075 }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ XMPPException -> 0x0075 }
        L_0x0063:
            boolean r5 = r4.hasNext()     // Catch:{ XMPPException -> 0x0075 }
            if (r5 != 0) goto L_0x00b7
        L_0x0069:
            return
        L_0x006a:
            org.jivesoftware.smack.PacketWriter r4 = r7.packetWriter     // Catch:{ XMPPException -> 0x0075 }
            r4.init()     // Catch:{ XMPPException -> 0x0075 }
            org.jivesoftware.smack.PacketReader r4 = r7.packetReader     // Catch:{ XMPPException -> 0x0075 }
            r4.init()     // Catch:{ XMPPException -> 0x0075 }
            goto L_0x0047
        L_0x0075:
            r0 = move-exception
            org.jivesoftware.smack.PacketWriter r4 = r7.packetWriter
            if (r4 == 0) goto L_0x0081
            org.jivesoftware.smack.PacketWriter r4 = r7.packetWriter     // Catch:{ Throwable -> 0x00d3 }
            r4.shutdown()     // Catch:{ Throwable -> 0x00d3 }
        L_0x007f:
            r7.packetWriter = r6
        L_0x0081:
            org.jivesoftware.smack.PacketReader r4 = r7.packetReader
            if (r4 == 0) goto L_0x008c
            org.jivesoftware.smack.PacketReader r4 = r7.packetReader     // Catch:{ Throwable -> 0x00d1 }
            r4.shutdown()     // Catch:{ Throwable -> 0x00d1 }
        L_0x008a:
            r7.packetReader = r6
        L_0x008c:
            java.io.Reader r4 = r7.reader
            if (r4 == 0) goto L_0x0097
            java.io.Reader r4 = r7.reader     // Catch:{ Throwable -> 0x00cf }
            r4.close()     // Catch:{ Throwable -> 0x00cf }
        L_0x0095:
            r7.reader = r6
        L_0x0097:
            java.io.Writer r4 = r7.writer
            if (r4 == 0) goto L_0x00a2
            java.io.Writer r4 = r7.writer     // Catch:{ Throwable -> 0x00cd }
            r4.close()     // Catch:{ Throwable -> 0x00cd }
        L_0x00a0:
            r7.writer = r6
        L_0x00a2:
            java.net.Socket r4 = r7.socket
            if (r4 == 0) goto L_0x00ad
            java.net.Socket r4 = r7.socket     // Catch:{ Exception -> 0x00cb }
            r4.close()     // Catch:{ Exception -> 0x00cb }
        L_0x00ab:
            r7.socket = r6
        L_0x00ad:
            boolean r4 = r7.authenticated
            r7.setWasAuthenticated(r4)
            r7.authenticated = r3
            r7.connected = r3
            throw r0
        L_0x00b7:
            java.lang.Object r2 = r4.next()     // Catch:{ XMPPException -> 0x0075 }
            org.jivesoftware.smack.ConnectionCreationListener r2 = (org.jivesoftware.smack.ConnectionCreationListener) r2     // Catch:{ XMPPException -> 0x0075 }
            r2.connectionCreated(r7)     // Catch:{ XMPPException -> 0x0075 }
            goto L_0x0063
        L_0x00c1:
            boolean r4 = r7.wasAuthenticated     // Catch:{ XMPPException -> 0x0075 }
            if (r4 != 0) goto L_0x0069
            org.jivesoftware.smack.PacketReader r4 = r7.packetReader     // Catch:{ XMPPException -> 0x0075 }
            r4.notifyReconnection()     // Catch:{ XMPPException -> 0x0075 }
            goto L_0x0069
        L_0x00cb:
            r4 = move-exception
            goto L_0x00ab
        L_0x00cd:
            r4 = move-exception
            goto L_0x00a0
        L_0x00cf:
            r4 = move-exception
            goto L_0x0095
        L_0x00d1:
            r4 = move-exception
            goto L_0x008a
        L_0x00d3:
            r4 = move-exception
            goto L_0x007f
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smack.XMPPConnection.initConnection():void");
    }

    private void initReaderAndWriter() throws XMPPException {
        try {
            if (!this.usingCompression) {
                this.reader = new BufferedReader(new InputStreamReader(this.socket.getInputStream(), "UTF-8"));
                this.writer = new BufferedWriter(new OutputStreamWriter(this.socket.getOutputStream(), "UTF-8"));
            } else {
                try {
                    Class<?> zoClass = Class.forName("com.jcraft.jzlib.ZOutputStream");
                    Object out = zoClass.getConstructor(OutputStream.class, Integer.TYPE).newInstance(this.socket.getOutputStream(), 9);
                    zoClass.getMethod("setFlushMode", Integer.TYPE).invoke(out, 2);
                    this.writer = new BufferedWriter(new OutputStreamWriter((OutputStream) out, "UTF-8"));
                    Class<?> ziClass = Class.forName("com.jcraft.jzlib.ZInputStream");
                    Object in = ziClass.getConstructor(InputStream.class).newInstance(this.socket.getInputStream());
                    ziClass.getMethod("setFlushMode", Integer.TYPE).invoke(in, 2);
                    this.reader = new BufferedReader(new InputStreamReader((InputStream) in, "UTF-8"));
                } catch (Exception e) {
                    e.printStackTrace();
                    this.reader = new BufferedReader(new InputStreamReader(this.socket.getInputStream(), "UTF-8"));
                    this.writer = new BufferedWriter(new OutputStreamWriter(this.socket.getOutputStream(), "UTF-8"));
                }
            }
            initDebugger();
        } catch (IOException ioe) {
            throw new XMPPException("XMPPError establishing connection with server.", new XMPPError(XMPPError.Condition.remote_server_error, "XMPPError establishing connection with server."), ioe);
        }
    }

    public boolean isUsingTLS() {
        return this.usingTLS;
    }

    /* access modifiers changed from: package-private */
    public void startTLSReceived(boolean required) {
        if (required && this.config.getSecurityMode() == ConnectionConfiguration.SecurityMode.disabled) {
            this.packetReader.notifyConnectionError(new IllegalStateException("TLS required by server but not allowed by connection configuration"));
        } else if (this.config.getSecurityMode() != ConnectionConfiguration.SecurityMode.disabled) {
            try {
                this.writer.write("<starttls xmlns=\"urn:ietf:params:xml:ns:xmpp-tls\"/>");
                this.writer.flush();
            } catch (IOException e) {
                this.packetReader.notifyConnectionError(e);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException}
     arg types: [java.net.Socket, java.lang.String, int, int]
     candidates:
      ClspMth{javax.net.SocketFactory.createSocket(java.lang.String, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException, java.net.UnknownHostException}
      ClspMth{javax.net.SocketFactory.createSocket(java.net.InetAddress, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException}
      ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException} */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x00cc  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x01fe A[SYNTHETIC, Splitter:B:37:0x01fe] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void proceedTLSReceived() throws java.lang.Exception {
        /*
            r23 = this;
            java.lang.String r18 = "TLS"
            javax.net.ssl.SSLContext r7 = javax.net.ssl.SSLContext.getInstance(r18)
            r11 = 0
            r10 = 0
            javax.net.ssl.KeyManager[] r10 = (javax.net.ssl.KeyManager[]) r10
            r14 = 0
            r0 = r23
            org.jivesoftware.smack.ConnectionConfiguration r0 = r0.config
            r18 = r0
            org.apache.harmony.javax.security.auth.callback.CallbackHandler r18 = r18.getCallbackHandler()
            if (r18 != 0) goto L_0x00b0
            r11 = 0
        L_0x0018:
            r18 = 1
            r0 = r18
            javax.net.ssl.TrustManager[] r0 = new javax.net.ssl.TrustManager[r0]
            r18 = r0
            r19 = 0
            org.jivesoftware.smack.ServerTrustManager r20 = new org.jivesoftware.smack.ServerTrustManager
            java.lang.String r21 = r23.getServiceName()
            r0 = r23
            org.jivesoftware.smack.ConnectionConfiguration r0 = r0.config
            r22 = r0
            r20.<init>(r21, r22)
            r18[r19] = r20
            java.security.SecureRandom r19 = new java.security.SecureRandom
            r19.<init>()
            r0 = r18
            r1 = r19
            r7.init(r10, r0, r1)
            r0 = r23
            java.net.Socket r0 = r0.socket
            r17 = r0
            javax.net.ssl.SSLSocketFactory r18 = r7.getSocketFactory()
            java.net.InetAddress r19 = r17.getInetAddress()
            java.lang.String r19 = r19.getHostName()
            int r20 = r17.getPort()
            r21 = 1
            r0 = r18
            r1 = r17
            r2 = r19
            r3 = r20
            r4 = r21
            java.net.Socket r18 = r0.createSocket(r1, r2, r3, r4)
            r0 = r18
            r1 = r23
            r1.socket = r0
            r0 = r23
            java.net.Socket r0 = r0.socket
            r18 = r0
            r19 = 0
            r18.setSoTimeout(r19)
            r0 = r23
            java.net.Socket r0 = r0.socket
            r18 = r0
            r19 = 1
            r18.setKeepAlive(r19)
            r23.initReaderAndWriter()
            r0 = r23
            java.net.Socket r0 = r0.socket
            r18 = r0
            javax.net.ssl.SSLSocket r18 = (javax.net.ssl.SSLSocket) r18
            r18.startHandshake()
            r18 = 1
            r0 = r18
            r1 = r23
            r1.usingTLS = r0
            r0 = r23
            org.jivesoftware.smack.PacketWriter r0 = r0.packetWriter
            r18 = r0
            r0 = r23
            java.io.Writer r0 = r0.writer
            r19 = r0
            r18.setWriter(r19)
            r0 = r23
            org.jivesoftware.smack.PacketWriter r0 = r0.packetWriter
            r18 = r0
            r18.openStream()
            return
        L_0x00b0:
            r0 = r23
            org.jivesoftware.smack.ConnectionConfiguration r0 = r0.config
            r18 = r0
            java.lang.String r18 = r18.getKeystoreType()
            java.lang.String r19 = "NONE"
            boolean r18 = r18.equals(r19)
            if (r18 == 0) goto L_0x00d9
            r11 = 0
            r14 = 0
        L_0x00c4:
            java.lang.String r18 = "SunX509"
            javax.net.ssl.KeyManagerFactory r9 = javax.net.ssl.KeyManagerFactory.getInstance(r18)
            if (r14 != 0) goto L_0x01fe
            r18 = 0
            r0 = r18
            r9.init(r11, r0)     // Catch:{ NullPointerException -> 0x020c }
        L_0x00d3:
            javax.net.ssl.KeyManager[] r10 = r9.getKeyManagers()     // Catch:{ NullPointerException -> 0x020c }
            goto L_0x0018
        L_0x00d9:
            r0 = r23
            org.jivesoftware.smack.ConnectionConfiguration r0 = r0.config
            r18 = r0
            java.lang.String r18 = r18.getKeystoreType()
            java.lang.String r19 = "PKCS11"
            boolean r18 = r18.equals(r19)
            if (r18 == 0) goto L_0x0181
            java.lang.String r18 = "sun.security.pkcs11.SunPKCS11"
            java.lang.Class r18 = java.lang.Class.forName(r18)     // Catch:{ Exception -> 0x017c }
            r19 = 1
            r0 = r19
            java.lang.Class[] r0 = new java.lang.Class[r0]     // Catch:{ Exception -> 0x017c }
            r19 = r0
            r20 = 0
            java.lang.Class<java.io.InputStream> r21 = java.io.InputStream.class
            r19[r20] = r21     // Catch:{ Exception -> 0x017c }
            java.lang.reflect.Constructor r5 = r18.getConstructor(r19)     // Catch:{ Exception -> 0x017c }
            java.lang.StringBuilder r18 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x017c }
            java.lang.String r19 = "name = SmartCard\nlibrary = "
            r18.<init>(r19)     // Catch:{ Exception -> 0x017c }
            r0 = r23
            org.jivesoftware.smack.ConnectionConfiguration r0 = r0.config     // Catch:{ Exception -> 0x017c }
            r19 = r0
            java.lang.String r19 = r19.getPKCS11Library()     // Catch:{ Exception -> 0x017c }
            java.lang.StringBuilder r18 = r18.append(r19)     // Catch:{ Exception -> 0x017c }
            java.lang.String r16 = r18.toString()     // Catch:{ Exception -> 0x017c }
            java.io.ByteArrayInputStream r6 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x017c }
            byte[] r18 = r16.getBytes()     // Catch:{ Exception -> 0x017c }
            r0 = r18
            r6.<init>(r0)     // Catch:{ Exception -> 0x017c }
            r18 = 1
            r0 = r18
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x017c }
            r18 = r0
            r19 = 0
            r18[r19] = r6     // Catch:{ Exception -> 0x017c }
            r0 = r18
            java.lang.Object r13 = r5.newInstance(r0)     // Catch:{ Exception -> 0x017c }
            java.security.Provider r13 = (java.security.Provider) r13     // Catch:{ Exception -> 0x017c }
            java.security.Security.addProvider(r13)     // Catch:{ Exception -> 0x017c }
            java.lang.String r18 = "PKCS11"
            r0 = r18
            java.security.KeyStore r11 = java.security.KeyStore.getInstance(r0, r13)     // Catch:{ Exception -> 0x017c }
            org.apache.harmony.javax.security.auth.callback.PasswordCallback r15 = new org.apache.harmony.javax.security.auth.callback.PasswordCallback     // Catch:{ Exception -> 0x017c }
            java.lang.String r18 = "PKCS11 Password: "
            r19 = 0
            r0 = r18
            r1 = r19
            r15.<init>(r0, r1)     // Catch:{ Exception -> 0x017c }
            r0 = r23
            org.jivesoftware.smack.ConnectionConfiguration r0 = r0.config     // Catch:{ Exception -> 0x0215 }
            r18 = r0
            org.apache.harmony.javax.security.auth.callback.CallbackHandler r18 = r18.getCallbackHandler()     // Catch:{ Exception -> 0x0215 }
            r19 = 1
            r0 = r19
            org.apache.harmony.javax.security.auth.callback.Callback[] r0 = new org.apache.harmony.javax.security.auth.callback.Callback[r0]     // Catch:{ Exception -> 0x0215 }
            r19 = r0
            r20 = 0
            r19[r20] = r15     // Catch:{ Exception -> 0x0215 }
            r18.handle(r19)     // Catch:{ Exception -> 0x0215 }
            r18 = 0
            char[] r19 = r15.getPassword()     // Catch:{ Exception -> 0x0215 }
            r0 = r18
            r1 = r19
            r11.load(r0, r1)     // Catch:{ Exception -> 0x0215 }
            r14 = r15
            goto L_0x00c4
        L_0x017c:
            r8 = move-exception
        L_0x017d:
            r11 = 0
            r14 = 0
            goto L_0x00c4
        L_0x0181:
            r0 = r23
            org.jivesoftware.smack.ConnectionConfiguration r0 = r0.config
            r18 = r0
            java.lang.String r18 = r18.getKeystoreType()
            java.lang.String r19 = "Apple"
            boolean r18 = r18.equals(r19)
            if (r18 == 0) goto L_0x01a8
            java.lang.String r18 = "KeychainStore"
            java.lang.String r19 = "Apple"
            java.security.KeyStore r11 = java.security.KeyStore.getInstance(r18, r19)
            r18 = 0
            r19 = 0
            r0 = r18
            r1 = r19
            r11.load(r0, r1)
            goto L_0x00c4
        L_0x01a8:
            r0 = r23
            org.jivesoftware.smack.ConnectionConfiguration r0 = r0.config
            r18 = r0
            java.lang.String r18 = r18.getKeystoreType()
            java.security.KeyStore r11 = java.security.KeyStore.getInstance(r18)
            org.apache.harmony.javax.security.auth.callback.PasswordCallback r15 = new org.apache.harmony.javax.security.auth.callback.PasswordCallback     // Catch:{ Exception -> 0x01f9 }
            java.lang.String r18 = "Keystore Password: "
            r19 = 0
            r0 = r18
            r1 = r19
            r15.<init>(r0, r1)     // Catch:{ Exception -> 0x01f9 }
            r0 = r23
            org.jivesoftware.smack.ConnectionConfiguration r0 = r0.config     // Catch:{ Exception -> 0x0212 }
            r18 = r0
            org.apache.harmony.javax.security.auth.callback.CallbackHandler r18 = r18.getCallbackHandler()     // Catch:{ Exception -> 0x0212 }
            r19 = 1
            r0 = r19
            org.apache.harmony.javax.security.auth.callback.Callback[] r0 = new org.apache.harmony.javax.security.auth.callback.Callback[r0]     // Catch:{ Exception -> 0x0212 }
            r19 = r0
            r20 = 0
            r19[r20] = r15     // Catch:{ Exception -> 0x0212 }
            r18.handle(r19)     // Catch:{ Exception -> 0x0212 }
            java.io.FileInputStream r18 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0212 }
            r0 = r23
            org.jivesoftware.smack.ConnectionConfiguration r0 = r0.config     // Catch:{ Exception -> 0x0212 }
            r19 = r0
            java.lang.String r19 = r19.getKeystorePath()     // Catch:{ Exception -> 0x0212 }
            r18.<init>(r19)     // Catch:{ Exception -> 0x0212 }
            char[] r19 = r15.getPassword()     // Catch:{ Exception -> 0x0212 }
            r0 = r18
            r1 = r19
            r11.load(r0, r1)     // Catch:{ Exception -> 0x0212 }
            r14 = r15
            goto L_0x00c4
        L_0x01f9:
            r8 = move-exception
        L_0x01fa:
            r11 = 0
            r14 = 0
            goto L_0x00c4
        L_0x01fe:
            char[] r18 = r14.getPassword()     // Catch:{ NullPointerException -> 0x020c }
            r0 = r18
            r9.init(r11, r0)     // Catch:{ NullPointerException -> 0x020c }
            r14.clearPassword()     // Catch:{ NullPointerException -> 0x020c }
            goto L_0x00d3
        L_0x020c:
            r12 = move-exception
            r10 = 0
            javax.net.ssl.KeyManager[] r10 = (javax.net.ssl.KeyManager[]) r10
            goto L_0x0018
        L_0x0212:
            r8 = move-exception
            r14 = r15
            goto L_0x01fa
        L_0x0215:
            r8 = move-exception
            r14 = r15
            goto L_0x017d
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smack.XMPPConnection.proceedTLSReceived():void");
    }

    /* access modifiers changed from: package-private */
    public void setAvailableCompressionMethods(Collection<String> methods) {
        this.compressionMethods = methods;
    }

    private boolean hasAvailableCompressionMethod(String method) {
        return this.compressionMethods != null && this.compressionMethods.contains(method);
    }

    public boolean isUsingCompression() {
        return this.usingCompression;
    }

    private boolean useCompression() {
        if (this.authenticated) {
            throw new IllegalStateException("Compression should be negotiated before authentication.");
        }
        try {
            Class.forName("com.jcraft.jzlib.ZOutputStream");
            if (!hasAvailableCompressionMethod("zlib")) {
                return false;
            }
            requestStreamCompression();
            synchronized (this) {
                try {
                    wait((long) (SmackConfiguration.getPacketReplyTimeout() * 5));
                } catch (InterruptedException e) {
                }
            }
            return this.usingCompression;
        } catch (ClassNotFoundException e2) {
            throw new IllegalStateException("Cannot use compression. Add smackx.jar to the classpath");
        }
    }

    private void requestStreamCompression() {
        try {
            this.writer.write("<compress xmlns='http://jabber.org/protocol/compress'>");
            this.writer.write("<method>zlib</method></compress>");
            this.writer.flush();
        } catch (IOException e) {
            this.packetReader.notifyConnectionError(e);
        }
    }

    /* access modifiers changed from: package-private */
    public void startStreamCompression() throws Exception {
        this.usingCompression = true;
        initReaderAndWriter();
        this.packetWriter.setWriter(this.writer);
        this.packetWriter.openStream();
        synchronized (this) {
            notify();
        }
    }

    /* access modifiers changed from: package-private */
    public void streamCompressionDenied() {
        synchronized (this) {
            notify();
        }
    }

    public void connect() throws XMPPException {
        if (this.config == null) {
            Log.e("config1", "is null");
        } else if (this.config.getUsername() != null) {
            Log.e("config.getusername1", this.config.getUsername());
        } else {
            Log.e("config.getusername1", "is null");
        }
        connectUsingConfiguration(this.config);
        if (this.config == null) {
            Log.e("config2", "is null");
        } else if (this.config.getUsername() != null) {
            Log.e("config.getusername2", this.config.getUsername());
        } else {
            Log.e("config.getusername2", "is null");
        }
        Log.e("isAnonymous()", String.valueOf(isAnonymous()));
        if (this.connected && this.wasAuthenticated) {
            try {
                if (isAnonymous()) {
                    loginAnonymously();
                } else {
                    login(this.config.getUsername(), this.config.getPassword(), this.config.getResource());
                }
                this.packetReader.notifyReconnection();
            } catch (XMPPException e) {
                e.printStackTrace();
            }
        }
    }

    private void setWasAuthenticated(boolean wasAuthenticated2) {
        if (!this.wasAuthenticated) {
            this.wasAuthenticated = wasAuthenticated2;
        }
    }

    public void setRosterStorage(RosterStorage storage) throws IllegalStateException {
        if (this.roster != null) {
            throw new IllegalStateException("Roster is already initialized");
        }
        this.rosterStorage = storage;
    }
}
