package org.jivesoftware.smack;

import org.jivesoftware.smack.packet.StreamError;

public class ReconnectionManager implements ConnectionListener {
    /* access modifiers changed from: private */
    public Connection connection;
    boolean done;

    static {
        Connection.addConnectionCreationListener(new ConnectionCreationListener() {
            public void connectionCreated(Connection connection) {
                connection.addConnectionListener(new ReconnectionManager(connection, null));
            }
        });
    }

    private ReconnectionManager(Connection connection2) {
        this.done = false;
        this.connection = connection2;
    }

    /* synthetic */ ReconnectionManager(Connection connection2, ReconnectionManager reconnectionManager) {
        this(connection2);
    }

    /* access modifiers changed from: private */
    public boolean isReconnectionAllowed() {
        return !this.done && !this.connection.isConnected() && this.connection.isReconnectionAllowed();
    }

    /* access modifiers changed from: protected */
    public void reconnect() {
        if (isReconnectionAllowed()) {
            Thread reconnectionThread = new Thread() {
                private int attempts = 0;

                private int timeDelay() {
                    if (this.attempts > 13) {
                        return 300;
                    }
                    if (this.attempts > 7) {
                        return 60;
                    }
                    return 10;
                }

                public void run() {
                    while (ReconnectionManager.this.isReconnectionAllowed()) {
                        int remainingSeconds = timeDelay();
                        while (ReconnectionManager.this.isReconnectionAllowed() && remainingSeconds > 0) {
                            try {
                                Thread.sleep(1000);
                                remainingSeconds--;
                                ReconnectionManager.this.notifyAttemptToReconnectIn(remainingSeconds);
                            } catch (InterruptedException e1) {
                                e1.printStackTrace();
                                ReconnectionManager.this.notifyReconnectionFailed(e1);
                            }
                        }
                        try {
                            if (ReconnectionManager.this.isReconnectionAllowed()) {
                                ReconnectionManager.this.connection.connect();
                            }
                        } catch (XMPPException e) {
                            ReconnectionManager.this.notifyReconnectionFailed(e);
                        }
                    }
                }
            };
            reconnectionThread.setName("Smack Reconnection Manager");
            reconnectionThread.setDaemon(true);
            reconnectionThread.start();
        }
    }

    /* access modifiers changed from: protected */
    public void notifyReconnectionFailed(Exception exception) {
        if (isReconnectionAllowed()) {
            for (ConnectionListener listener : this.connection.connectionListeners) {
                listener.reconnectionFailed(exception);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void notifyAttemptToReconnectIn(int seconds) {
        if (isReconnectionAllowed()) {
            for (ConnectionListener listener : this.connection.connectionListeners) {
                listener.reconnectingIn(seconds);
            }
        }
    }

    public void connectionClosed() {
        this.done = true;
    }

    public void connectionClosedOnError(Exception e) {
        StreamError error;
        this.done = false;
        if ((!(e instanceof XMPPException) || (error = ((XMPPException) e).getStreamError()) == null || !"conflict".equals(error.getCode())) && isReconnectionAllowed()) {
            reconnect();
        }
    }

    public void reconnectingIn(int seconds) {
    }

    public void reconnectionFailed(Exception e) {
    }

    public void reconnectionSuccessful() {
    }
}
