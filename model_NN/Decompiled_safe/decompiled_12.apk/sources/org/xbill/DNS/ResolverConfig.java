package org.xbill.DNS;

import com.kenfor.client3g.util.Constant;
import com.kenfor.taglib.db.dbPresentTag;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import org.jivesoftware.smackx.packet.IBBExtensions;

public class ResolverConfig {
    private static ResolverConfig currentConfig;
    private Name[] searchlist = null;
    private String[] servers = null;

    static {
        refresh();
    }

    public ResolverConfig() {
        if (findProperty() || findSunJVM()) {
            return;
        }
        if (this.servers == null || this.searchlist == null) {
            String OS = System.getProperty("os.name");
            String vendor = System.getProperty("java.vendor");
            if (OS.indexOf("Windows") != -1) {
                if (OS.indexOf("95") == -1 && OS.indexOf("98") == -1 && OS.indexOf("ME") == -1) {
                    findNT();
                } else {
                    find95();
                }
            } else if (OS.indexOf("NetWare") != -1) {
                findNetware();
            } else if (vendor.indexOf("Android") != -1) {
                findAndroid();
            } else {
                findUnix();
            }
        }
    }

    private void addServer(String server, List list) {
        if (!list.contains(server)) {
            if (Options.check("verbose")) {
                System.out.println("adding server " + server);
            }
            list.add(server);
        }
    }

    private void addSearch(String search, List list) {
        if (Options.check("verbose")) {
            System.out.println("adding search " + search);
        }
        try {
            Name name = Name.fromString(search, Name.root);
            if (!list.contains(name)) {
                list.add(name);
            }
        } catch (TextParseException e) {
        }
    }

    private void configureFromLists(List lserver, List lsearch) {
        if (this.servers == null && lserver.size() > 0) {
            this.servers = (String[]) lserver.toArray(new String[0]);
        }
        if (this.searchlist == null && lsearch.size() > 0) {
            this.searchlist = (Name[]) lsearch.toArray(new Name[0]);
        }
    }

    private boolean findProperty() {
        List lserver = new ArrayList(0);
        List lsearch = new ArrayList(0);
        String prop = System.getProperty("dns.server");
        if (prop != null) {
            StringTokenizer st = new StringTokenizer(prop, dbPresentTag.ROLE_DELIMITER);
            while (st.hasMoreTokens()) {
                addServer(st.nextToken(), lserver);
            }
        }
        String prop2 = System.getProperty("dns.search");
        if (prop2 != null) {
            StringTokenizer st2 = new StringTokenizer(prop2, dbPresentTag.ROLE_DELIMITER);
            while (st2.hasMoreTokens()) {
                addSearch(st2.nextToken(), lsearch);
            }
        }
        configureFromLists(lserver, lsearch);
        if (this.servers == null || this.searchlist == null) {
            return false;
        }
        return true;
    }

    private boolean findSunJVM() {
        List lserver = new ArrayList(0);
        List lsearch = new ArrayList(0);
        try {
            Class[] noClasses = new Class[0];
            Object[] noObjects = new Object[0];
            Class resConfClass = Class.forName("sun.net.dns.ResolverConfiguration");
            Object resConf = resConfClass.getDeclaredMethod(IBBExtensions.Open.ELEMENT_NAME, noClasses).invoke(null, noObjects);
            List<String> lserver_tmp = (List) resConfClass.getMethod("nameservers", noClasses).invoke(resConf, noObjects);
            List<String> lsearch_tmp = (List) resConfClass.getMethod("searchlist", noClasses).invoke(resConf, noObjects);
            if (lserver_tmp.size() == 0) {
                return false;
            }
            if (lserver_tmp.size() > 0) {
                for (String addServer : lserver_tmp) {
                    addServer(addServer, lserver);
                }
            }
            if (lsearch_tmp.size() > 0) {
                for (String addSearch : lsearch_tmp) {
                    addSearch(addSearch, lsearch);
                }
            }
            configureFromLists(lserver, lsearch);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private void findResolvConf(String file) {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            List lserver = new ArrayList(0);
            List lsearch = new ArrayList(0);
            while (true) {
                try {
                    String line = br.readLine();
                    if (line == null) {
                        break;
                    } else if (line.startsWith("nameserver")) {
                        StringTokenizer st = new StringTokenizer(line);
                        st.nextToken();
                        addServer(st.nextToken(), lserver);
                    } else if (line.startsWith(Constant.DOMAIN)) {
                        StringTokenizer st2 = new StringTokenizer(line);
                        st2.nextToken();
                        if (st2.hasMoreTokens() && lsearch.isEmpty()) {
                            addSearch(st2.nextToken(), lsearch);
                        }
                    } else if (line.startsWith("search")) {
                        if (!lsearch.isEmpty()) {
                            lsearch.clear();
                        }
                        StringTokenizer st3 = new StringTokenizer(line);
                        st3.nextToken();
                        while (st3.hasMoreTokens()) {
                            addSearch(st3.nextToken(), lsearch);
                        }
                    }
                } catch (IOException e) {
                }
            }
            br.close();
            configureFromLists(lserver, lsearch);
        } catch (FileNotFoundException e2) {
        }
    }

    private void findUnix() {
        findResolvConf("/etc/resolv.conf");
    }

    private void findNetware() {
        findResolvConf("sys:/etc/resolv.cfg");
    }

    private void findWin(InputStream in) {
        ResourceBundle res = ResourceBundle.getBundle(String.valueOf(ResolverConfig.class.getPackage().getName()) + ".windows.DNSServer");
        String host_name = res.getString("host_name");
        String primary_dns_suffix = res.getString("primary_dns_suffix");
        String dns_suffix = res.getString("dns_suffix");
        String dns_servers = res.getString("dns_servers");
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        try {
            List lserver = new ArrayList();
            List lsearch = new ArrayList();
            boolean readingServers = false;
            boolean readingSearches = false;
            while (true) {
                String line = br.readLine();
                if (line == null) {
                    configureFromLists(lserver, lsearch);
                    try {
                        br.close();
                        return;
                    } catch (IOException e) {
                        return;
                    }
                } else {
                    StringTokenizer stringTokenizer = new StringTokenizer(line);
                    if (!stringTokenizer.hasMoreTokens()) {
                        readingServers = false;
                        readingSearches = false;
                    } else {
                        String s = stringTokenizer.nextToken();
                        if (line.indexOf(":") != -1) {
                            readingServers = false;
                            readingSearches = false;
                        }
                        if (line.indexOf(host_name) != -1) {
                            while (stringTokenizer.hasMoreTokens()) {
                                s = stringTokenizer.nextToken();
                            }
                            try {
                                if (Name.fromString(s, null).labels() != 1) {
                                    addSearch(s, lsearch);
                                }
                            } catch (TextParseException e2) {
                            }
                        } else if (line.indexOf(primary_dns_suffix) != -1) {
                            while (stringTokenizer.hasMoreTokens()) {
                                s = stringTokenizer.nextToken();
                            }
                            if (!s.equals(":")) {
                                addSearch(s, lsearch);
                                readingSearches = true;
                            }
                        } else if (readingSearches || line.indexOf(dns_suffix) != -1) {
                            while (stringTokenizer.hasMoreTokens()) {
                                s = stringTokenizer.nextToken();
                            }
                            if (!s.equals(":")) {
                                addSearch(s, lsearch);
                                readingSearches = true;
                            }
                        } else if (readingServers || line.indexOf(dns_servers) != -1) {
                            while (stringTokenizer.hasMoreTokens()) {
                                s = stringTokenizer.nextToken();
                            }
                            if (!s.equals(":")) {
                                addServer(s, lserver);
                                readingServers = true;
                            }
                        }
                    }
                }
            }
        } catch (IOException e3) {
            try {
                br.close();
            } catch (IOException e4) {
            }
        } catch (Throwable th) {
            try {
                br.close();
            } catch (IOException e5) {
            }
            throw th;
        }
    }

    private void find95() {
        try {
            Runtime.getRuntime().exec("winipcfg /all /batch " + "winipcfg.out").waitFor();
            findWin(new FileInputStream(new File("winipcfg.out")));
            new File("winipcfg.out").delete();
        } catch (Exception e) {
        }
    }

    private void findNT() {
        try {
            Process p = Runtime.getRuntime().exec("ipconfig /all");
            findWin(p.getInputStream());
            p.destroy();
        } catch (Exception e) {
        }
    }

    private void findAndroid() {
        try {
            ArrayList maybe = new ArrayList();
            BufferedReader br = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec("getprop").getInputStream()));
            while (true) {
                String line = br.readLine();
                if (line == null) {
                    configureFromLists(maybe, null);
                    return;
                }
                StringTokenizer t = new StringTokenizer(line, ":");
                if (t.nextToken().indexOf(".dns") > -1) {
                    String v = t.nextToken().replaceAll("[ \\[\\]]", "");
                    if ((v.matches("^\\d+(\\.\\d+){3}$") || v.matches("^[0-9a-f]+(:[0-9a-f]*)+:[0-9a-f]+$")) && !maybe.contains(v)) {
                        maybe.add(v);
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    public String[] servers() {
        return this.servers;
    }

    public String server() {
        if (this.servers == null) {
            return null;
        }
        return this.servers[0];
    }

    public Name[] searchPath() {
        return this.searchlist;
    }

    public static synchronized ResolverConfig getCurrentConfig() {
        ResolverConfig resolverConfig;
        synchronized (ResolverConfig.class) {
            resolverConfig = currentConfig;
        }
        return resolverConfig;
    }

    public static void refresh() {
        ResolverConfig newConfig = new ResolverConfig();
        synchronized (ResolverConfig.class) {
            currentConfig = newConfig;
        }
    }
}
