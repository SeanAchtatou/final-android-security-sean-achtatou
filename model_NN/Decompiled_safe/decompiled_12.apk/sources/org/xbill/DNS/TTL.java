package org.xbill.DNS;

import org.xbill.DNS.WKSRecord;

public final class TTL {
    public static final long MAX_VALUE = 2147483647L;

    private TTL() {
    }

    static void check(long i) {
        if (i < 0 || i > MAX_VALUE) {
            throw new InvalidTTLException(i);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public static long parse(String s, boolean clamp) {
        if (s == null || s.length() == 0 || !Character.isDigit(s.charAt(0))) {
            throw new NumberFormatException();
        }
        long value = 0;
        long ttl = 0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            long oldvalue = value;
            if (Character.isDigit(c)) {
                value = (10 * value) + ((long) Character.getNumericValue(c));
                if (value < oldvalue) {
                    throw new NumberFormatException();
                }
            } else {
                switch (Character.toUpperCase(c)) {
                    case 'D':
                        value *= 24;
                        value *= 60;
                        value *= 60;
                        break;
                    case WKSRecord.Service.NETRJS_2 /*72*/:
                        value *= 60;
                        value *= 60;
                        break;
                    case 'M':
                        value *= 60;
                        break;
                    case 'S':
                        break;
                    case 'W':
                        value *= 7;
                        value *= 24;
                        value *= 60;
                        value *= 60;
                        break;
                    default:
                        throw new NumberFormatException();
                }
                ttl += value;
                value = 0;
                if (ttl > 4294967295L) {
                    throw new NumberFormatException();
                }
            }
        }
        if (ttl == 0) {
            ttl = value;
        }
        if (ttl > 4294967295L) {
            throw new NumberFormatException();
        } else if (ttl <= MAX_VALUE || !clamp) {
            return ttl;
        } else {
            return MAX_VALUE;
        }
    }

    public static long parseTTL(String s) {
        return parse(s, true);
    }

    public static String format(long ttl) {
        check(ttl);
        StringBuffer sb = new StringBuffer();
        long secs = ttl % 60;
        long ttl2 = ttl / 60;
        long mins = ttl2 % 60;
        long ttl3 = ttl2 / 60;
        long hours = ttl3 % 24;
        long ttl4 = ttl3 / 24;
        long days = ttl4 % 7;
        long weeks = ttl4 / 7;
        if (weeks > 0) {
            sb.append(String.valueOf(weeks) + "W");
        }
        if (days > 0) {
            sb.append(String.valueOf(days) + "D");
        }
        if (hours > 0) {
            sb.append(String.valueOf(hours) + "H");
        }
        if (mins > 0) {
            sb.append(String.valueOf(mins) + "M");
        }
        if (secs > 0 || (weeks == 0 && days == 0 && hours == 0 && mins == 0)) {
            sb.append(String.valueOf(secs) + "S");
        }
        return sb.toString();
    }
}
