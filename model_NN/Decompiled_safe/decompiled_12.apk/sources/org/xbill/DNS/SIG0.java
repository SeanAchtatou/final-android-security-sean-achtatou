package org.xbill.DNS;

import java.security.PrivateKey;
import java.util.Date;
import org.xbill.DNS.DNSSEC;

public class SIG0 {
    private static final short VALIDITY = 300;

    private SIG0() {
    }

    public static void signMessage(Message message, KEYRecord key, PrivateKey privkey, SIGRecord previous) throws DNSSEC.DNSSECException {
        int validity = Options.intValue("sig0validity");
        if (validity < 0) {
            validity = 300;
        }
        long now = System.currentTimeMillis();
        message.addRecord(DNSSEC.signMessage(message, previous, key, privkey, new Date(now), new Date(((long) (validity * 1000)) + now)), 3);
    }

    public static void verifyMessage(Message message, byte[] b, KEYRecord key, SIGRecord previous) throws DNSSEC.DNSSECException {
        SIGRecord sig = null;
        Record[] additional = message.getSectionArray(3);
        int i = 0;
        while (true) {
            if (i >= additional.length) {
                break;
            } else if (additional[i].getType() == 24 && ((SIGRecord) additional[i]).getTypeCovered() == 0) {
                sig = (SIGRecord) additional[i];
                break;
            } else {
                i++;
            }
        }
        DNSSEC.verifyMessage(message, b, sig, previous, key);
    }
}
