package org.xbill.DNS;

import java.util.Date;

public class SIGRecord extends SIGBase {
    private static final long serialVersionUID = 4963556060953589058L;

    public /* bridge */ /* synthetic */ int getAlgorithm() {
        return super.getAlgorithm();
    }

    public /* bridge */ /* synthetic */ Date getExpire() {
        return super.getExpire();
    }

    public /* bridge */ /* synthetic */ int getFootprint() {
        return super.getFootprint();
    }

    public /* bridge */ /* synthetic */ int getLabels() {
        return super.getLabels();
    }

    public /* bridge */ /* synthetic */ long getOrigTTL() {
        return super.getOrigTTL();
    }

    public /* bridge */ /* synthetic */ byte[] getSignature() {
        return super.getSignature();
    }

    public /* bridge */ /* synthetic */ Name getSigner() {
        return super.getSigner();
    }

    public /* bridge */ /* synthetic */ Date getTimeSigned() {
        return super.getTimeSigned();
    }

    public /* bridge */ /* synthetic */ int getTypeCovered() {
        return super.getTypeCovered();
    }

    SIGRecord() {
    }

    /* access modifiers changed from: package-private */
    public Record getObject() {
        return new SIGRecord();
    }

    public SIGRecord(Name name, int dclass, long ttl, int covered, int alg, long origttl, Date expire, Date timeSigned, int footprint, Name signer, byte[] signature) {
        super(name, 24, dclass, ttl, covered, alg, origttl, expire, timeSigned, footprint, signer, signature);
    }
}
