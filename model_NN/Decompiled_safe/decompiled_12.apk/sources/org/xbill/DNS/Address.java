package org.xbill.DNS;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

public final class Address {
    public static final int IPv4 = 1;
    public static final int IPv6 = 2;

    private Address() {
    }

    private static byte[] parseV4(String s) {
        int currentOctet;
        byte[] values = new byte[4];
        int length = s.length();
        int currentValue = 0;
        int numDigits = 0;
        int i = 0;
        int currentOctet2 = 0;
        while (i < length) {
            char c = s.charAt(i);
            if (c < '0' || c > '9') {
                if (c != '.') {
                    return null;
                }
                if (currentOctet2 == 3) {
                    return null;
                }
                if (numDigits == 0) {
                    return null;
                }
                currentOctet = currentOctet2 + 1;
                values[currentOctet2] = (byte) currentValue;
                currentValue = 0;
                numDigits = 0;
            } else if (numDigits == 3) {
                return null;
            } else {
                if (numDigits > 0 && currentValue == 0) {
                    return null;
                }
                numDigits++;
                currentValue = (currentValue * 10) + (c - '0');
                if (currentValue > 255) {
                    return null;
                }
                currentOctet = currentOctet2;
            }
            i++;
            currentOctet2 = currentOctet;
        }
        if (currentOctet2 != 3) {
            return null;
        }
        if (numDigits == 0) {
            return null;
        }
        values[currentOctet2] = (byte) currentValue;
        return values;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:85:?, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static byte[] parseV6(java.lang.String r19) {
        /*
            r13 = -1
            r17 = 16
            r0 = r17
            byte[] r4 = new byte[r0]
            java.lang.String r17 = ":"
            r18 = -1
            r0 = r19
            r1 = r17
            r2 = r18
            java.lang.String[] r14 = r0.split(r1, r2)
            r7 = 0
            int r0 = r14.length
            r17 = r0
            int r12 = r17 + -1
            r17 = 0
            r17 = r14[r17]
            int r17 = r17.length()
            if (r17 != 0) goto L_0x0035
            int r17 = r12 - r7
            if (r17 <= 0) goto L_0x005b
            r17 = 1
            r17 = r14[r17]
            int r17 = r17.length()
            if (r17 != 0) goto L_0x005b
            int r7 = r7 + 1
        L_0x0035:
            r17 = r14[r12]
            int r17 = r17.length()
            if (r17 != 0) goto L_0x004d
            int r17 = r12 - r7
            if (r17 <= 0) goto L_0x005d
            int r17 = r12 + -1
            r17 = r14[r17]
            int r17 = r17.length()
            if (r17 != 0) goto L_0x005d
            int r12 = r12 + -1
        L_0x004d:
            int r17 = r12 - r7
            int r17 = r17 + 1
            r18 = 8
            r0 = r17
            r1 = r18
            if (r0 <= r1) goto L_0x005f
            r4 = 0
        L_0x005a:
            return r4
        L_0x005b:
            r4 = 0
            goto L_0x005a
        L_0x005d:
            r4 = 0
            goto L_0x005a
        L_0x005f:
            r8 = r7
            r9 = 0
            r10 = r9
        L_0x0062:
            if (r8 <= r12) goto L_0x006f
            r9 = r10
        L_0x0065:
            r17 = 16
            r0 = r17
            if (r9 >= r0) goto L_0x0110
            if (r13 >= 0) goto L_0x0110
            r4 = 0
            goto L_0x005a
        L_0x006f:
            r17 = r14[r8]
            int r17 = r17.length()
            if (r17 != 0) goto L_0x0081
            if (r13 < 0) goto L_0x007b
            r4 = 0
            goto L_0x005a
        L_0x007b:
            r13 = r10
            r9 = r10
        L_0x007d:
            int r8 = r8 + 1
            r10 = r9
            goto L_0x0062
        L_0x0081:
            r17 = r14[r8]
            r18 = 46
            int r17 = r17.indexOf(r18)
            if (r17 < 0) goto L_0x00b6
            if (r8 >= r12) goto L_0x008f
            r4 = 0
            goto L_0x005a
        L_0x008f:
            r17 = 6
            r0 = r17
            if (r8 <= r0) goto L_0x0097
            r4 = 0
            goto L_0x005a
        L_0x0097:
            r17 = r14[r8]
            r18 = 1
            byte[] r15 = toByteArray(r17, r18)
            if (r15 != 0) goto L_0x00a3
            r4 = 0
            goto L_0x005a
        L_0x00a3:
            r11 = 0
        L_0x00a4:
            r17 = 4
            r0 = r17
            if (r11 < r0) goto L_0x00ac
            r9 = r10
            goto L_0x0065
        L_0x00ac:
            int r9 = r10 + 1
            byte r17 = r15[r11]
            r4[r10] = r17
            int r11 = r11 + 1
            r10 = r9
            goto L_0x00a4
        L_0x00b6:
            r11 = 0
        L_0x00b7:
            r17 = r14[r8]     // Catch:{ NumberFormatException -> 0x010b }
            int r17 = r17.length()     // Catch:{ NumberFormatException -> 0x010b }
            r0 = r17
            if (r11 < r0) goto L_0x00d6
            r17 = r14[r8]     // Catch:{ NumberFormatException -> 0x010b }
            r18 = 16
            int r16 = java.lang.Integer.parseInt(r17, r18)     // Catch:{ NumberFormatException -> 0x010b }
            r17 = 65535(0xffff, float:9.1834E-41)
            r0 = r16
            r1 = r17
            if (r0 > r1) goto L_0x00d4
            if (r16 >= 0) goto L_0x00ee
        L_0x00d4:
            r4 = 0
            goto L_0x005a
        L_0x00d6:
            r17 = r14[r8]     // Catch:{ NumberFormatException -> 0x010b }
            r0 = r17
            char r3 = r0.charAt(r11)     // Catch:{ NumberFormatException -> 0x010b }
            r17 = 16
            r0 = r17
            int r17 = java.lang.Character.digit(r3, r0)     // Catch:{ NumberFormatException -> 0x010b }
            if (r17 >= 0) goto L_0x00eb
            r4 = 0
            goto L_0x005a
        L_0x00eb:
            int r11 = r11 + 1
            goto L_0x00b7
        L_0x00ee:
            int r9 = r10 + 1
            int r17 = r16 >>> 8
            r0 = r17
            byte r0 = (byte) r0
            r17 = r0
            r4[r10] = r17     // Catch:{ NumberFormatException -> 0x012d }
            int r10 = r9 + 1
            r0 = r16
            r0 = r0 & 255(0xff, float:3.57E-43)
            r17 = r0
            r0 = r17
            byte r0 = (byte) r0
            r17 = r0
            r4[r9] = r17     // Catch:{ NumberFormatException -> 0x010b }
            r9 = r10
            goto L_0x007d
        L_0x010b:
            r5 = move-exception
            r9 = r10
        L_0x010d:
            r4 = 0
            goto L_0x005a
        L_0x0110:
            if (r13 < 0) goto L_0x005a
            int r6 = 16 - r9
            int r17 = r13 + r6
            int r18 = r9 - r13
            r0 = r17
            r1 = r18
            java.lang.System.arraycopy(r4, r13, r4, r0, r1)
            r8 = r13
        L_0x0120:
            int r17 = r13 + r6
            r0 = r17
            if (r8 >= r0) goto L_0x005a
            r17 = 0
            r4[r8] = r17
            int r8 = r8 + 1
            goto L_0x0120
        L_0x012d:
            r5 = move-exception
            goto L_0x010d
        */
        throw new UnsupportedOperationException("Method not decompiled: org.xbill.DNS.Address.parseV6(java.lang.String):byte[]");
    }

    public static int[] toArray(String s, int family) {
        byte[] byteArray = toByteArray(s, family);
        if (byteArray == null) {
            return null;
        }
        int[] intArray = new int[byteArray.length];
        for (int i = 0; i < byteArray.length; i++) {
            intArray[i] = byteArray[i] & 255;
        }
        return intArray;
    }

    public static int[] toArray(String s) {
        return toArray(s, 1);
    }

    public static byte[] toByteArray(String s, int family) {
        if (family == 1) {
            return parseV4(s);
        }
        if (family == 2) {
            return parseV6(s);
        }
        throw new IllegalArgumentException("unknown address family");
    }

    public static boolean isDottedQuad(String s) {
        if (toByteArray(s, 1) != null) {
            return true;
        }
        return false;
    }

    public static String toDottedQuad(byte[] addr) {
        return String.valueOf((int) (addr[0] & 255)) + "." + ((int) (addr[1] & 255)) + "." + ((int) (addr[2] & 255)) + "." + ((int) (addr[3] & 255));
    }

    public static String toDottedQuad(int[] addr) {
        return String.valueOf(addr[0]) + "." + addr[1] + "." + addr[2] + "." + addr[3];
    }

    private static Record[] lookupHostName(String name) throws UnknownHostException {
        try {
            Record[] records = new Lookup(name).run();
            if (records != null) {
                return records;
            }
            throw new UnknownHostException("unknown host");
        } catch (TextParseException e) {
            throw new UnknownHostException("invalid name");
        }
    }

    private static InetAddress addrFromRecord(String name, Record r) throws UnknownHostException {
        return InetAddress.getByAddress(name, ((ARecord) r).getAddress().getAddress());
    }

    public static InetAddress getByName(String name) throws UnknownHostException {
        try {
            return getByAddress(name);
        } catch (UnknownHostException e) {
            return addrFromRecord(name, lookupHostName(name)[0]);
        }
    }

    public static InetAddress[] getAllByName(String name) throws UnknownHostException {
        try {
            return new InetAddress[]{getByAddress(name)};
        } catch (UnknownHostException e) {
            Record[] records = lookupHostName(name);
            InetAddress[] addrs = new InetAddress[records.length];
            for (int i = 0; i < records.length; i++) {
                addrs[i] = addrFromRecord(name, records[i]);
            }
            return addrs;
        }
    }

    public static InetAddress getByAddress(String addr) throws UnknownHostException {
        byte[] bytes = toByteArray(addr, 1);
        if (bytes != null) {
            return InetAddress.getByAddress(bytes);
        }
        byte[] bytes2 = toByteArray(addr, 2);
        if (bytes2 != null) {
            return InetAddress.getByAddress(bytes2);
        }
        throw new UnknownHostException("Invalid address: " + addr);
    }

    public static InetAddress getByAddress(String addr, int family) throws UnknownHostException {
        if (family == 1 || family == 2) {
            byte[] bytes = toByteArray(addr, family);
            if (bytes != null) {
                return InetAddress.getByAddress(bytes);
            }
            throw new UnknownHostException("Invalid address: " + addr);
        }
        throw new IllegalArgumentException("unknown address family");
    }

    public static String getHostName(InetAddress addr) throws UnknownHostException {
        Record[] records = new Lookup(ReverseMap.fromAddress(addr), 12).run();
        if (records != null) {
            return ((PTRRecord) records[0]).getTarget().toString();
        }
        throw new UnknownHostException("unknown address");
    }

    public static int familyOf(InetAddress address) {
        if (address instanceof Inet4Address) {
            return 1;
        }
        if (address instanceof Inet6Address) {
            return 2;
        }
        throw new IllegalArgumentException("unknown address family");
    }

    public static int addressLength(int family) {
        if (family == 1) {
            return 4;
        }
        if (family == 2) {
            return 16;
        }
        throw new IllegalArgumentException("unknown address family");
    }
}
