package org.xbill.DNS;

import java.io.IOException;
import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.TreeMap;

public class Zone implements Serializable {
    public static final int PRIMARY = 1;
    public static final int SECONDARY = 2;
    private static final long serialVersionUID = -9220510891189510942L;
    private RRset NS;
    private SOARecord SOA;
    /* access modifiers changed from: private */
    public Map data;
    private int dclass;
    private boolean hasWild;
    /* access modifiers changed from: private */
    public Name origin;
    /* access modifiers changed from: private */
    public Object originNode;

    class ZoneIterator implements Iterator {
        private int count;
        private RRset[] current;
        private boolean wantLastSOA;
        private Iterator zentries;

        ZoneIterator(boolean axfr) {
            synchronized (Zone.this) {
                this.zentries = Zone.this.data.entrySet().iterator();
            }
            this.wantLastSOA = axfr;
            RRset[] sets = Zone.this.allRRsets(Zone.this.originNode);
            this.current = new RRset[sets.length];
            int j = 2;
            for (int i = 0; i < sets.length; i++) {
                int type = sets[i].getType();
                if (type == 6) {
                    this.current[0] = sets[i];
                } else if (type == 2) {
                    this.current[1] = sets[i];
                } else {
                    this.current[j] = sets[i];
                    j++;
                }
            }
        }

        public boolean hasNext() {
            return this.current != null || this.wantLastSOA;
        }

        public Object next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            } else if (this.current == null) {
                this.wantLastSOA = false;
                return Zone.this.oneRRset(Zone.this.originNode, 6);
            } else {
                RRset[] rRsetArr = this.current;
                int i = this.count;
                this.count = i + 1;
                RRset rRset = rRsetArr[i];
                if (this.count != this.current.length) {
                    return rRset;
                }
                this.current = null;
                while (this.zentries.hasNext()) {
                    Map.Entry entry = (Map.Entry) this.zentries.next();
                    if (!entry.getKey().equals(Zone.this.origin)) {
                        RRset[] sets = Zone.this.allRRsets(entry.getValue());
                        if (sets.length != 0) {
                            this.current = sets;
                            this.count = 0;
                            return rRset;
                        }
                    }
                }
                return rRset;
            }
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    private void validate() throws IOException {
        this.originNode = exactName(this.origin);
        if (this.originNode == null) {
            throw new IOException(this.origin + ": no data specified");
        }
        RRset rrset = oneRRset(this.originNode, 6);
        if (rrset == null || rrset.size() != 1) {
            throw new IOException(this.origin + ": exactly 1 SOA must be specified");
        }
        this.SOA = (SOARecord) rrset.rrs().next();
        this.NS = oneRRset(this.originNode, 2);
        if (this.NS == null) {
            throw new IOException(this.origin + ": no NS set specified");
        }
    }

    private final void maybeAddRecord(Record record) throws IOException {
        int rtype = record.getType();
        Name name = record.getName();
        if (rtype == 6 && !name.equals(this.origin)) {
            throw new IOException("SOA owner " + name + " does not match zone origin " + this.origin);
        } else if (name.subdomain(this.origin)) {
            addRecord(record);
        }
    }

    public Zone(Name zone, String file) throws IOException {
        this.dclass = 1;
        this.data = new TreeMap();
        if (zone == null) {
            throw new IllegalArgumentException("no zone name specified");
        }
        Master m = new Master(file, zone);
        this.origin = zone;
        while (true) {
            Record record = m.nextRecord();
            if (record == null) {
                validate();
                return;
            }
            maybeAddRecord(record);
        }
    }

    public Zone(Name zone, Record[] records) throws IOException {
        this.dclass = 1;
        this.data = new TreeMap();
        if (zone == null) {
            throw new IllegalArgumentException("no zone name specified");
        }
        this.origin = zone;
        for (Record maybeAddRecord : records) {
            maybeAddRecord(maybeAddRecord);
        }
        validate();
    }

    private void fromXFR(ZoneTransferIn xfrin) throws IOException, ZoneTransferException {
        this.data = new TreeMap();
        this.origin = xfrin.getName();
        for (Record record : xfrin.run()) {
            maybeAddRecord(record);
        }
        if (!xfrin.isAXFR()) {
            throw new IllegalArgumentException("zones can only be created from AXFRs");
        }
        validate();
    }

    public Zone(ZoneTransferIn xfrin) throws IOException, ZoneTransferException {
        this.dclass = 1;
        fromXFR(xfrin);
    }

    public Zone(Name zone, int dclass2, String remote) throws IOException, ZoneTransferException {
        this.dclass = 1;
        ZoneTransferIn xfrin = ZoneTransferIn.newAXFR(zone, remote, (TSIG) null);
        xfrin.setDClass(dclass2);
        fromXFR(xfrin);
    }

    public Name getOrigin() {
        return this.origin;
    }

    public RRset getNS() {
        return this.NS;
    }

    public SOARecord getSOA() {
        return this.SOA;
    }

    public int getDClass() {
        return this.dclass;
    }

    private synchronized Object exactName(Name name) {
        return this.data.get(name);
    }

    /* access modifiers changed from: private */
    public synchronized RRset[] allRRsets(Object types) {
        RRset[] rRsetArr;
        if (types instanceof List) {
            List typelist = (List) types;
            rRsetArr = (RRset[]) typelist.toArray(new RRset[typelist.size()]);
        } else {
            rRsetArr = new RRset[]{(RRset) types};
        }
        return rRsetArr;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0039, code lost:
        if (r3.getType() == r8) goto L_0x0020;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized org.xbill.DNS.RRset oneRRset(java.lang.Object r7, int r8) {
        /*
            r6 = this;
            monitor-enter(r6)
            r4 = 255(0xff, float:3.57E-43)
            if (r8 != r4) goto L_0x0010
            java.lang.IllegalArgumentException r4 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x000d }
            java.lang.String r5 = "oneRRset(ANY)"
            r4.<init>(r5)     // Catch:{ all -> 0x000d }
            throw r4     // Catch:{ all -> 0x000d }
        L_0x000d:
            r4 = move-exception
            monitor-exit(r6)
            throw r4
        L_0x0010:
            boolean r4 = r7 instanceof java.util.List     // Catch:{ all -> 0x000d }
            if (r4 == 0) goto L_0x0031
            r0 = r7
            java.util.List r0 = (java.util.List) r0     // Catch:{ all -> 0x000d }
            r2 = r0
            r1 = 0
        L_0x0019:
            int r4 = r2.size()     // Catch:{ all -> 0x000d }
            if (r1 < r4) goto L_0x0022
        L_0x001f:
            r3 = 0
        L_0x0020:
            monitor-exit(r6)
            return r3
        L_0x0022:
            java.lang.Object r3 = r2.get(r1)     // Catch:{ all -> 0x000d }
            org.xbill.DNS.RRset r3 = (org.xbill.DNS.RRset) r3     // Catch:{ all -> 0x000d }
            int r4 = r3.getType()     // Catch:{ all -> 0x000d }
            if (r4 == r8) goto L_0x0020
            int r1 = r1 + 1
            goto L_0x0019
        L_0x0031:
            r0 = r7
            org.xbill.DNS.RRset r0 = (org.xbill.DNS.RRset) r0     // Catch:{ all -> 0x000d }
            r3 = r0
            int r4 = r3.getType()     // Catch:{ all -> 0x000d }
            if (r4 != r8) goto L_0x001f
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: org.xbill.DNS.Zone.oneRRset(java.lang.Object, int):org.xbill.DNS.RRset");
    }

    private synchronized RRset findRRset(Name name, int type) {
        RRset oneRRset;
        Object types = exactName(name);
        if (types == null) {
            oneRRset = null;
        } else {
            oneRRset = oneRRset(types, type);
        }
        return oneRRset;
    }

    private synchronized void addRRset(Name name, RRset rrset) {
        if (!this.hasWild && name.isWild()) {
            this.hasWild = true;
        }
        Object types = this.data.get(name);
        if (types == null) {
            this.data.put(name, rrset);
        } else {
            int rtype = rrset.getType();
            if (types instanceof List) {
                List list = (List) types;
                int i = 0;
                while (true) {
                    if (i >= list.size()) {
                        list.add(rrset);
                        break;
                    } else if (((RRset) list.get(i)).getType() == rtype) {
                        list.set(i, rrset);
                        break;
                    } else {
                        i++;
                    }
                }
            } else {
                RRset set = (RRset) types;
                if (set.getType() == rtype) {
                    this.data.put(name, rrset);
                } else {
                    LinkedList list2 = new LinkedList();
                    list2.add(set);
                    list2.add(rrset);
                    this.data.put(name, list2);
                }
            }
        }
    }

    private synchronized void removeRRset(Name name, int type) {
        Object types = this.data.get(name);
        if (types != null) {
            if (types instanceof List) {
                List list = (List) types;
                int i = 0;
                while (true) {
                    if (i >= list.size()) {
                        break;
                    } else if (((RRset) list.get(i)).getType() == type) {
                        list.remove(i);
                        if (list.size() == 0) {
                            this.data.remove(name);
                        }
                    } else {
                        i++;
                    }
                }
            } else if (((RRset) types).getType() == type) {
                this.data.remove(name);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
        r8 = new org.xbill.DNS.SetResponse(6);
        r7 = allRRsets(r11);
        r0 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0074, code lost:
        if (r0 >= r7.length) goto L_0x000e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0076, code lost:
        r8.addRRset(r7[r0]);
        r0 = r0 + 1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized org.xbill.DNS.SetResponse lookup(org.xbill.DNS.Name r14, int r15) {
        /*
            r13 = this;
            monitor-enter(r13)
            org.xbill.DNS.Name r12 = r13.origin     // Catch:{ all -> 0x005f }
            boolean r12 = r14.subdomain(r12)     // Catch:{ all -> 0x005f }
            if (r12 != 0) goto L_0x0010
            r12 = 1
            org.xbill.DNS.SetResponse r8 = org.xbill.DNS.SetResponse.ofType(r12)     // Catch:{ all -> 0x005f }
        L_0x000e:
            monitor-exit(r13)
            return r8
        L_0x0010:
            int r3 = r14.labels()     // Catch:{ all -> 0x005f }
            org.xbill.DNS.Name r12 = r13.origin     // Catch:{ all -> 0x005f }
            int r5 = r12.labels()     // Catch:{ all -> 0x005f }
            r9 = r5
        L_0x001b:
            if (r9 <= r3) goto L_0x002c
            boolean r12 = r13.hasWild     // Catch:{ all -> 0x005f }
            if (r12 == 0) goto L_0x0026
            r0 = 0
        L_0x0022:
            int r12 = r3 - r5
            if (r0 < r12) goto L_0x00b9
        L_0x0026:
            r12 = 1
            org.xbill.DNS.SetResponse r8 = org.xbill.DNS.SetResponse.ofType(r12)     // Catch:{ all -> 0x005f }
            goto L_0x000e
        L_0x002c:
            if (r9 != r5) goto L_0x003f
            r2 = 1
        L_0x002f:
            if (r9 != r3) goto L_0x0041
            r1 = 1
        L_0x0032:
            if (r2 == 0) goto L_0x0043
            org.xbill.DNS.Name r10 = r13.origin     // Catch:{ all -> 0x005f }
        L_0x0036:
            java.lang.Object r11 = r13.exactName(r10)     // Catch:{ all -> 0x005f }
            if (r11 != 0) goto L_0x004f
        L_0x003c:
            int r9 = r9 + 1
            goto L_0x001b
        L_0x003f:
            r2 = 0
            goto L_0x002f
        L_0x0041:
            r1 = 0
            goto L_0x0032
        L_0x0043:
            if (r1 == 0) goto L_0x0047
            r10 = r14
            goto L_0x0036
        L_0x0047:
            org.xbill.DNS.Name r10 = new org.xbill.DNS.Name     // Catch:{ all -> 0x005f }
            int r12 = r3 - r9
            r10.<init>(r14, r12)     // Catch:{ all -> 0x005f }
            goto L_0x0036
        L_0x004f:
            if (r2 != 0) goto L_0x0062
            r12 = 2
            org.xbill.DNS.RRset r4 = r13.oneRRset(r11, r12)     // Catch:{ all -> 0x005f }
            if (r4 == 0) goto L_0x0062
            org.xbill.DNS.SetResponse r8 = new org.xbill.DNS.SetResponse     // Catch:{ all -> 0x005f }
            r12 = 3
            r8.<init>(r12, r4)     // Catch:{ all -> 0x005f }
            goto L_0x000e
        L_0x005f:
            r12 = move-exception
            monitor-exit(r13)
            throw r12
        L_0x0062:
            if (r1 == 0) goto L_0x007e
            r12 = 255(0xff, float:3.57E-43)
            if (r15 != r12) goto L_0x007e
            org.xbill.DNS.SetResponse r8 = new org.xbill.DNS.SetResponse     // Catch:{ all -> 0x005f }
            r12 = 6
            r8.<init>(r12)     // Catch:{ all -> 0x005f }
            org.xbill.DNS.RRset[] r7 = r13.allRRsets(r11)     // Catch:{ all -> 0x005f }
            r0 = 0
        L_0x0073:
            int r12 = r7.length     // Catch:{ all -> 0x005f }
            if (r0 >= r12) goto L_0x000e
            r12 = r7[r0]     // Catch:{ all -> 0x005f }
            r8.addRRset(r12)     // Catch:{ all -> 0x005f }
            int r0 = r0 + 1
            goto L_0x0073
        L_0x007e:
            if (r1 == 0) goto L_0x00a0
            org.xbill.DNS.RRset r6 = r13.oneRRset(r11, r15)     // Catch:{ all -> 0x005f }
            if (r6 == 0) goto L_0x0091
            org.xbill.DNS.SetResponse r8 = new org.xbill.DNS.SetResponse     // Catch:{ all -> 0x005f }
            r12 = 6
            r8.<init>(r12)     // Catch:{ all -> 0x005f }
            r8.addRRset(r6)     // Catch:{ all -> 0x005f }
            goto L_0x000e
        L_0x0091:
            r12 = 5
            org.xbill.DNS.RRset r6 = r13.oneRRset(r11, r12)     // Catch:{ all -> 0x005f }
            if (r6 == 0) goto L_0x00b0
            org.xbill.DNS.SetResponse r8 = new org.xbill.DNS.SetResponse     // Catch:{ all -> 0x005f }
            r12 = 4
            r8.<init>(r12, r6)     // Catch:{ all -> 0x005f }
            goto L_0x000e
        L_0x00a0:
            r12 = 39
            org.xbill.DNS.RRset r6 = r13.oneRRset(r11, r12)     // Catch:{ all -> 0x005f }
            if (r6 == 0) goto L_0x00b0
            org.xbill.DNS.SetResponse r8 = new org.xbill.DNS.SetResponse     // Catch:{ all -> 0x005f }
            r12 = 5
            r8.<init>(r12, r6)     // Catch:{ all -> 0x005f }
            goto L_0x000e
        L_0x00b0:
            if (r1 == 0) goto L_0x003c
            r12 = 2
            org.xbill.DNS.SetResponse r8 = org.xbill.DNS.SetResponse.ofType(r12)     // Catch:{ all -> 0x005f }
            goto L_0x000e
        L_0x00b9:
            int r12 = r0 + 1
            org.xbill.DNS.Name r10 = r14.wild(r12)     // Catch:{ all -> 0x005f }
            java.lang.Object r11 = r13.exactName(r10)     // Catch:{ all -> 0x005f }
            if (r11 != 0) goto L_0x00c9
        L_0x00c5:
            int r0 = r0 + 1
            goto L_0x0022
        L_0x00c9:
            org.xbill.DNS.RRset r6 = r13.oneRRset(r11, r15)     // Catch:{ all -> 0x005f }
            if (r6 == 0) goto L_0x00c5
            org.xbill.DNS.SetResponse r8 = new org.xbill.DNS.SetResponse     // Catch:{ all -> 0x005f }
            r12 = 6
            r8.<init>(r12)     // Catch:{ all -> 0x005f }
            r8.addRRset(r6)     // Catch:{ all -> 0x005f }
            goto L_0x000e
        */
        throw new UnsupportedOperationException("Method not decompiled: org.xbill.DNS.Zone.lookup(org.xbill.DNS.Name, int):org.xbill.DNS.SetResponse");
    }

    public SetResponse findRecords(Name name, int type) {
        return lookup(name, type);
    }

    public RRset findExactMatch(Name name, int type) {
        Object types = exactName(name);
        if (types == null) {
            return null;
        }
        return oneRRset(types, type);
    }

    public void addRRset(RRset rrset) {
        addRRset(rrset.getName(), rrset);
    }

    public void addRecord(Record r) {
        Name name = r.getName();
        int rtype = r.getRRsetType();
        synchronized (this) {
            RRset rrset = findRRset(name, rtype);
            if (rrset == null) {
                addRRset(name, new RRset(r));
            } else {
                rrset.addRR(r);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void removeRecord(org.xbill.DNS.Record r6) {
        /*
            r5 = this;
            org.xbill.DNS.Name r0 = r6.getName()
            int r2 = r6.getRRsetType()
            monitor-enter(r5)
            org.xbill.DNS.RRset r1 = r5.findRRset(r0, r2)     // Catch:{ all -> 0x0027 }
            if (r1 != 0) goto L_0x0011
            monitor-exit(r5)     // Catch:{ all -> 0x0027 }
        L_0x0010:
            return
        L_0x0011:
            int r3 = r1.size()     // Catch:{ all -> 0x0027 }
            r4 = 1
            if (r3 != r4) goto L_0x002a
            org.xbill.DNS.Record r3 = r1.first()     // Catch:{ all -> 0x0027 }
            boolean r3 = r3.equals(r6)     // Catch:{ all -> 0x0027 }
            if (r3 == 0) goto L_0x002a
            r5.removeRRset(r0, r2)     // Catch:{ all -> 0x0027 }
        L_0x0025:
            monitor-exit(r5)     // Catch:{ all -> 0x0027 }
            goto L_0x0010
        L_0x0027:
            r3 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x0027 }
            throw r3
        L_0x002a:
            r1.deleteRR(r6)     // Catch:{ all -> 0x0027 }
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: org.xbill.DNS.Zone.removeRecord(org.xbill.DNS.Record):void");
    }

    public Iterator iterator() {
        return new ZoneIterator(false);
    }

    public Iterator AXFR() {
        return new ZoneIterator(true);
    }

    private void nodeToString(StringBuffer sb, Object node) {
        RRset[] sets = allRRsets(node);
        for (RRset rrset : sets) {
            Iterator it = rrset.rrs();
            while (it.hasNext()) {
                sb.append(it.next() + "\n");
            }
            Iterator it2 = rrset.sigs();
            while (it2.hasNext()) {
                sb.append(it2.next() + "\n");
            }
        }
    }

    public synchronized String toMasterFile() {
        StringBuffer sb;
        sb = new StringBuffer();
        nodeToString(sb, this.originNode);
        for (Map.Entry entry : this.data.entrySet()) {
            if (!this.origin.equals(entry.getKey())) {
                nodeToString(sb, entry.getValue());
            }
        }
        return sb.toString();
    }

    public String toString() {
        return toMasterFile();
    }
}
