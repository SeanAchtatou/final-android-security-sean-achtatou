package org.xbill.DNS;

import java.io.IOException;
import java.security.PublicKey;
import org.xbill.DNS.DNSSEC;
import org.xbill.DNS.utils.base64;

abstract class KEYBase extends Record {
    private static final long serialVersionUID = 3469321722693285454L;
    protected int alg;
    protected int flags;
    protected int footprint = -1;
    protected byte[] key;
    protected int proto;
    protected PublicKey publicKey = null;

    protected KEYBase() {
    }

    public KEYBase(Name name, int type, int dclass, long ttl, int flags2, int proto2, int alg2, byte[] key2) {
        super(name, type, dclass, ttl);
        this.flags = checkU16("flags", flags2);
        this.proto = checkU8("proto", proto2);
        this.alg = checkU8("alg", alg2);
        this.key = key2;
    }

    /* access modifiers changed from: package-private */
    public void rrFromWire(DNSInput in) throws IOException {
        this.flags = in.readU16();
        this.proto = in.readU8();
        this.alg = in.readU8();
        if (in.remaining() > 0) {
            this.key = in.readByteArray();
        }
    }

    /* access modifiers changed from: package-private */
    public String rrToString() {
        StringBuffer sb = new StringBuffer();
        sb.append(this.flags);
        sb.append(" ");
        sb.append(this.proto);
        sb.append(" ");
        sb.append(this.alg);
        if (this.key != null) {
            if (Options.check("multiline")) {
                sb.append(" (\n");
                sb.append(base64.formatString(this.key, 64, "\t", true));
                sb.append(" ; key_tag = ");
                sb.append(getFootprint());
            } else {
                sb.append(" ");
                sb.append(base64.toString(this.key));
            }
        }
        return sb.toString();
    }

    public int getFlags() {
        return this.flags;
    }

    public int getProtocol() {
        return this.proto;
    }

    public int getAlgorithm() {
        return this.alg;
    }

    public byte[] getKey() {
        return this.key;
    }

    public int getFootprint() {
        int foot;
        if (this.footprint >= 0) {
            return this.footprint;
        }
        int foot2 = 0;
        DNSOutput out = new DNSOutput();
        rrToWire(out, null, false);
        byte[] rdata = out.toByteArray();
        if (this.alg == 1) {
            int d1 = rdata[rdata.length - 3] & 255;
            foot = (d1 << 8) + (rdata[rdata.length - 2] & 255);
        } else {
            int i = 0;
            while (i < rdata.length - 1) {
                int d12 = rdata[i] & 255;
                foot2 += (d12 << 8) + (rdata[i + 1] & 255);
                i += 2;
            }
            if (i < rdata.length) {
                foot2 += (rdata[i] & 255) << 8;
            }
            foot = foot2 + ((foot2 >> 16) & Message.MAXLENGTH);
        }
        this.footprint = foot & Message.MAXLENGTH;
        return this.footprint;
    }

    public PublicKey getPublicKey() throws DNSSEC.DNSSECException {
        if (this.publicKey != null) {
            return this.publicKey;
        }
        this.publicKey = DNSSEC.toPublicKey(this);
        return this.publicKey;
    }

    /* access modifiers changed from: package-private */
    public void rrToWire(DNSOutput out, Compression c, boolean canonical) {
        out.writeU16(this.flags);
        out.writeU8(this.proto);
        out.writeU8(this.alg);
        if (this.key != null) {
            out.writeByteArray(this.key);
        }
    }
}
