package org.xbill.DNS;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class Message implements Cloneable {
    public static final int MAXLENGTH = 65535;
    static final int TSIG_FAILED = 4;
    static final int TSIG_INTERMEDIATE = 2;
    static final int TSIG_SIGNED = 3;
    static final int TSIG_UNSIGNED = 0;
    static final int TSIG_VERIFIED = 1;
    private static RRset[] emptyRRsetArray = new RRset[0];
    private static Record[] emptyRecordArray = new Record[0];
    private Header header;
    private TSIGRecord querytsig;
    private List[] sections;
    int sig0start;
    private int size;
    int tsigState;
    private int tsigerror;
    private TSIG tsigkey;
    int tsigstart;

    private Message(Header header2) {
        this.sections = new List[4];
        this.header = header2;
    }

    public Message(int id) {
        this(new Header(id));
    }

    public Message() {
        this(new Header());
    }

    public static Message newQuery(Record r) {
        Message m = new Message();
        m.header.setOpcode(0);
        m.header.setFlag(7);
        m.addRecord(r, 0);
        return m;
    }

    public static Message newUpdate(Name zone) {
        return new Update(zone);
    }

    Message(DNSInput in) throws IOException {
        this(new Header(in));
        boolean isUpdate = this.header.getOpcode() == 5;
        boolean truncated = this.header.getFlag(6);
        int i = 0;
        while (i < 4) {
            try {
                int count = this.header.getCount(i);
                if (count > 0) {
                    this.sections[i] = new ArrayList(count);
                }
                for (int j = 0; j < count; j++) {
                    int pos = in.current();
                    Record rec = Record.fromWire(in, i, isUpdate);
                    this.sections[i].add(rec);
                    if (rec.getType() == 250) {
                        this.tsigstart = pos;
                    }
                    if (rec.getType() == 24 && ((SIGRecord) rec).getTypeCovered() == 0) {
                        this.sig0start = pos;
                    }
                }
                i++;
            } catch (WireParseException e) {
                if (!truncated) {
                    throw e;
                }
            }
        }
        this.size = in.current();
    }

    public Message(byte[] b) throws IOException {
        this(new DNSInput(b));
    }

    public void setHeader(Header h) {
        this.header = h;
    }

    public Header getHeader() {
        return this.header;
    }

    public void addRecord(Record r, int section) {
        if (this.sections[section] == null) {
            this.sections[section] = new LinkedList();
        }
        this.header.incCount(section);
        this.sections[section].add(r);
    }

    public boolean removeRecord(Record r, int section) {
        if (this.sections[section] == null || !this.sections[section].remove(r)) {
            return false;
        }
        this.header.decCount(section);
        return true;
    }

    public void removeAllRecords(int section) {
        this.sections[section] = null;
        this.header.setCount(section, 0);
    }

    public boolean findRecord(Record r, int section) {
        return this.sections[section] != null && this.sections[section].contains(r);
    }

    public boolean findRecord(Record r) {
        for (int i = 1; i <= 3; i++) {
            if (this.sections[i] != null && this.sections[i].contains(r)) {
                return true;
            }
        }
        return false;
    }

    public boolean findRRset(Name name, int type, int section) {
        if (this.sections[section] == null) {
            return false;
        }
        for (int i = 0; i < this.sections[section].size(); i++) {
            Record r = (Record) this.sections[section].get(i);
            if (r.getType() == type && name.equals(r.getName())) {
                return true;
            }
        }
        return false;
    }

    public boolean findRRset(Name name, int type) {
        if (findRRset(name, type, 1) || findRRset(name, type, 2) || findRRset(name, type, 3)) {
            return true;
        }
        return false;
    }

    public Record getQuestion() {
        List l = this.sections[0];
        if (l == null || l.size() == 0) {
            return null;
        }
        return (Record) l.get(0);
    }

    public TSIGRecord getTSIG() {
        int count = this.header.getCount(3);
        if (count == 0) {
            return null;
        }
        Record rec = (Record) this.sections[3].get(count - 1);
        if (rec.type != 250) {
            return null;
        }
        return (TSIGRecord) rec;
    }

    public boolean isSigned() {
        if (this.tsigState == 3 || this.tsigState == 1 || this.tsigState == 4) {
            return true;
        }
        return false;
    }

    public boolean isVerified() {
        return this.tsigState == 1;
    }

    public OPTRecord getOPT() {
        Record[] additional = getSectionArray(3);
        for (int i = 0; i < additional.length; i++) {
            if (additional[i] instanceof OPTRecord) {
                return (OPTRecord) additional[i];
            }
        }
        return null;
    }

    public int getRcode() {
        int rcode = this.header.getRcode();
        OPTRecord opt = getOPT();
        if (opt != null) {
            return rcode + (opt.getExtendedRcode() << 4);
        }
        return rcode;
    }

    public Record[] getSectionArray(int section) {
        if (this.sections[section] == null) {
            return emptyRecordArray;
        }
        List l = this.sections[section];
        return (Record[]) l.toArray(new Record[l.size()]);
    }

    private static boolean sameSet(Record r1, Record r2) {
        return r1.getRRsetType() == r2.getRRsetType() && r1.getDClass() == r2.getDClass() && r1.getName().equals(r2.getName());
    }

    public RRset[] getSectionRRsets(int section) {
        if (this.sections[section] == null) {
            return emptyRRsetArray;
        }
        List sets = new LinkedList();
        Record[] recs = getSectionArray(section);
        Set hash = new HashSet();
        for (int i = 0; i < recs.length; i++) {
            Name name = recs[i].getName();
            boolean newset = true;
            if (hash.contains(name)) {
                int j = sets.size() - 1;
                while (true) {
                    if (j >= 0) {
                        RRset set = (RRset) sets.get(j);
                        if (set.getType() == recs[i].getRRsetType() && set.getDClass() == recs[i].getDClass() && set.getName().equals(name)) {
                            set.addRR(recs[i]);
                            newset = false;
                            break;
                        }
                        j--;
                    } else {
                        break;
                    }
                }
            }
            if (newset) {
                sets.add(new RRset(recs[i]));
                hash.add(name);
            }
        }
        return (RRset[]) sets.toArray(new RRset[sets.size()]);
    }

    /* access modifiers changed from: package-private */
    public void toWire(DNSOutput out) {
        this.header.toWire(out);
        Compression c = new Compression();
        for (int i = 0; i < 4; i++) {
            if (this.sections[i] != null) {
                for (int j = 0; j < this.sections[i].size(); j++) {
                    ((Record) this.sections[i].get(j)).toWire(out, i, c);
                }
            }
        }
    }

    private int sectionToWire(DNSOutput out, int section, Compression c, int maxLength) {
        int n = this.sections[section].size();
        int pos = out.current();
        int rendered = 0;
        Record lastrec = null;
        for (int i = 0; i < n; i++) {
            Record rec = (Record) this.sections[section].get(i);
            if (lastrec != null && !sameSet(rec, lastrec)) {
                pos = out.current();
                rendered = i;
            }
            lastrec = rec;
            rec.toWire(out, section, c);
            if (out.current() > maxLength) {
                out.jump(pos);
                return n - rendered;
            }
        }
        return 0;
    }

    private boolean toWire(DNSOutput out, int maxLength) {
        int skipped;
        if (maxLength < 12) {
            return false;
        }
        Header newheader = null;
        int tempMaxLength = maxLength;
        if (this.tsigkey != null) {
            tempMaxLength -= this.tsigkey.recordLength();
        }
        int startpos = out.current();
        this.header.toWire(out);
        Compression c = new Compression();
        int i = 0;
        while (true) {
            if (i >= 4) {
                break;
            } else if (this.sections[i] == null || (skipped = sectionToWire(out, i, c, tempMaxLength)) == 0) {
                i++;
            } else if (i != 3) {
                if (0 == 0) {
                    newheader = (Header) this.header.clone();
                }
                newheader.setFlag(6);
                newheader.setCount(i, newheader.getCount(i) - skipped);
                for (int j = i + 1; j < 4; j++) {
                    newheader.setCount(j, 0);
                }
                out.save();
                out.jump(startpos);
                newheader.toWire(out);
                out.restore();
            }
        }
        if (this.tsigkey != null) {
            TSIGRecord tsigrec = this.tsigkey.generate(this, out.toByteArray(), this.tsigerror, this.querytsig);
            if (newheader == null) {
                newheader = (Header) this.header.clone();
            }
            tsigrec.toWire(out, 3, c);
            newheader.incCount(3);
            out.save();
            out.jump(startpos);
            newheader.toWire(out);
            out.restore();
        }
        return true;
    }

    public byte[] toWire() {
        DNSOutput out = new DNSOutput();
        toWire(out);
        this.size = out.current();
        return out.toByteArray();
    }

    public byte[] toWire(int maxLength) {
        DNSOutput out = new DNSOutput();
        toWire(out, maxLength);
        this.size = out.current();
        return out.toByteArray();
    }

    public void setTSIG(TSIG key, int error, TSIGRecord querytsig2) {
        this.tsigkey = key;
        this.tsigerror = error;
        this.querytsig = querytsig2;
    }

    public int numBytes() {
        return this.size;
    }

    public String sectionToString(int i) {
        if (i > 3) {
            return null;
        }
        StringBuffer sb = new StringBuffer();
        Record[] records = getSectionArray(i);
        for (Record rec : records) {
            if (i == 0) {
                sb.append(";;\t" + rec.name);
                sb.append(", type = " + Type.string(rec.type));
                sb.append(", class = " + DClass.string(rec.dclass));
            } else {
                sb.append(rec);
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        if (getOPT() != null) {
            sb.append(String.valueOf(this.header.toStringWithRcode(getRcode())) + "\n");
        } else {
            sb.append(this.header + "\n");
        }
        if (isSigned()) {
            sb.append(";; TSIG ");
            if (isVerified()) {
                sb.append("ok");
            } else {
                sb.append("invalid");
            }
            sb.append(10);
        }
        for (int i = 0; i < 4; i++) {
            if (this.header.getOpcode() != 5) {
                sb.append(";; " + Section.longString(i) + ":\n");
            } else {
                sb.append(";; " + Section.updString(i) + ":\n");
            }
            sb.append(String.valueOf(sectionToString(i)) + "\n");
        }
        sb.append(";; Message size: " + numBytes() + " bytes");
        return sb.toString();
    }

    public Object clone() {
        Message m = new Message();
        for (int i = 0; i < this.sections.length; i++) {
            if (this.sections[i] != null) {
                m.sections[i] = new LinkedList(this.sections[i]);
            }
        }
        m.header = (Header) this.header.clone();
        m.size = this.size;
        return m;
    }
}
