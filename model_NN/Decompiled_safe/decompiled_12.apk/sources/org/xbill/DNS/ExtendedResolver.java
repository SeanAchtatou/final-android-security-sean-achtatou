package org.xbill.DNS;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class ExtendedResolver implements Resolver {
    private static final int quantum = 5;
    /* access modifiers changed from: private */
    public int lbStart = 0;
    /* access modifiers changed from: private */
    public boolean loadBalance = false;
    /* access modifiers changed from: private */
    public List resolvers;
    /* access modifiers changed from: private */
    public int retries = 3;

    private static class Resolution implements ResolverListener {
        boolean done;
        Object[] inprogress;
        ResolverListener listener;
        int outstanding;
        Message query;
        Resolver[] resolvers;
        Message response;
        int retries;
        int[] sent;
        Throwable thrown;

        public Resolution(ExtendedResolver eres, Message query2) {
            List l = eres.resolvers;
            this.resolvers = (Resolver[]) l.toArray(new Resolver[l.size()]);
            if (eres.loadBalance) {
                int nresolvers = this.resolvers.length;
                int access$2 = eres.lbStart;
                eres.lbStart = access$2 + 1;
                int start = access$2 % nresolvers;
                if (eres.lbStart > nresolvers) {
                    eres.lbStart = eres.lbStart % nresolvers;
                }
                if (start > 0) {
                    Resolver[] shuffle = new Resolver[nresolvers];
                    for (int i = 0; i < nresolvers; i++) {
                        shuffle[i] = this.resolvers[(i + start) % nresolvers];
                    }
                    this.resolvers = shuffle;
                }
            }
            this.sent = new int[this.resolvers.length];
            this.inprogress = new Object[this.resolvers.length];
            this.retries = eres.retries;
            this.query = query2;
        }

        public void send(int n) {
            int[] iArr = this.sent;
            iArr[n] = iArr[n] + 1;
            this.outstanding++;
            try {
                this.inprogress[n] = this.resolvers[n].sendAsync(this.query, this);
            } catch (Throwable t) {
                synchronized (this) {
                    this.thrown = t;
                    this.done = true;
                    if (this.listener == null) {
                        notifyAll();
                    }
                }
            }
        }

        public Message start() throws IOException {
            try {
                int[] iArr = this.sent;
                iArr[0] = iArr[0] + 1;
                this.outstanding++;
                this.inprogress[0] = new Object();
                return this.resolvers[0].send(this.query);
            } catch (Exception e) {
                handleException(this.inprogress[0], e);
                synchronized (this) {
                    while (!this.done) {
                        try {
                            wait();
                        } catch (InterruptedException e2) {
                        }
                    }
                    if (this.response != null) {
                        return this.response;
                    }
                    if (this.thrown instanceof IOException) {
                        throw ((IOException) this.thrown);
                    } else if (this.thrown instanceof RuntimeException) {
                        throw ((RuntimeException) this.thrown);
                    } else if (this.thrown instanceof Error) {
                        throw ((Error) this.thrown);
                    } else {
                        throw new IllegalStateException("ExtendedResolver failure");
                    }
                }
            }
        }

        public void startAsync(ResolverListener listener2) {
            this.listener = listener2;
            send(0);
        }

        public void receiveMessage(Object id, Message m) {
            if (Options.check("verbose")) {
                System.err.println("ExtendedResolver: received message");
            }
            synchronized (this) {
                if (!this.done) {
                    this.response = m;
                    this.done = true;
                    if (this.listener == null) {
                        notifyAll();
                    } else {
                        this.listener.receiveMessage(this, this.response);
                    }
                }
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:66:0x00a9, code lost:
            if ((r6.thrown instanceof java.lang.Exception) != false) goto L_0x00b8;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:67:0x00ab, code lost:
            r6.thrown = new java.lang.RuntimeException(r6.thrown.getMessage());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:68:0x00b8, code lost:
            r6.listener.handleException(r6, (java.lang.Exception) r6.thrown);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:77:?, code lost:
            return;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void handleException(java.lang.Object r7, java.lang.Exception r8) {
            /*
                r6 = this;
                r5 = 1
                java.lang.String r2 = "verbose"
                boolean r2 = org.xbill.DNS.Options.check(r2)
                if (r2 == 0) goto L_0x001d
                java.io.PrintStream r2 = java.lang.System.err
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                java.lang.String r4 = "ExtendedResolver: got "
                r3.<init>(r4)
                java.lang.StringBuilder r3 = r3.append(r8)
                java.lang.String r3 = r3.toString()
                r2.println(r3)
            L_0x001d:
                monitor-enter(r6)
                int r2 = r6.outstanding     // Catch:{ all -> 0x0037 }
                int r2 = r2 + -1
                r6.outstanding = r2     // Catch:{ all -> 0x0037 }
                boolean r2 = r6.done     // Catch:{ all -> 0x0037 }
                if (r2 == 0) goto L_0x002a
                monitor-exit(r6)     // Catch:{ all -> 0x0037 }
            L_0x0029:
                return
            L_0x002a:
                r0 = 0
            L_0x002b:
                java.lang.Object[] r2 = r6.inprogress     // Catch:{ all -> 0x0037 }
                int r2 = r2.length     // Catch:{ all -> 0x0037 }
                if (r0 < r2) goto L_0x003a
            L_0x0030:
                java.lang.Object[] r2 = r6.inprogress     // Catch:{ all -> 0x0037 }
                int r2 = r2.length     // Catch:{ all -> 0x0037 }
                if (r0 != r2) goto L_0x0043
                monitor-exit(r6)     // Catch:{ all -> 0x0037 }
                goto L_0x0029
            L_0x0037:
                r2 = move-exception
                monitor-exit(r6)     // Catch:{ all -> 0x0037 }
                throw r2
            L_0x003a:
                java.lang.Object[] r2 = r6.inprogress     // Catch:{ all -> 0x0037 }
                r2 = r2[r0]     // Catch:{ all -> 0x0037 }
                if (r2 == r7) goto L_0x0030
                int r0 = r0 + 1
                goto L_0x002b
            L_0x0043:
                r1 = 0
                int[] r2 = r6.sent     // Catch:{ all -> 0x0037 }
                r2 = r2[r0]     // Catch:{ all -> 0x0037 }
                if (r2 != r5) goto L_0x0052
                org.xbill.DNS.Resolver[] r2 = r6.resolvers     // Catch:{ all -> 0x0037 }
                int r2 = r2.length     // Catch:{ all -> 0x0037 }
                int r2 = r2 + -1
                if (r0 >= r2) goto L_0x0052
                r1 = 1
            L_0x0052:
                boolean r2 = r8 instanceof java.io.InterruptedIOException     // Catch:{ all -> 0x0037 }
                if (r2 == 0) goto L_0x006d
                int[] r2 = r6.sent     // Catch:{ all -> 0x0037 }
                r2 = r2[r0]     // Catch:{ all -> 0x0037 }
                int r3 = r6.retries     // Catch:{ all -> 0x0037 }
                if (r2 >= r3) goto L_0x0061
                r6.send(r0)     // Catch:{ all -> 0x0037 }
            L_0x0061:
                java.lang.Throwable r2 = r6.thrown     // Catch:{ all -> 0x0037 }
                if (r2 != 0) goto L_0x0067
                r6.thrown = r8     // Catch:{ all -> 0x0037 }
            L_0x0067:
                boolean r2 = r6.done     // Catch:{ all -> 0x0037 }
                if (r2 == 0) goto L_0x0081
                monitor-exit(r6)     // Catch:{ all -> 0x0037 }
                goto L_0x0029
            L_0x006d:
                boolean r2 = r8 instanceof java.net.SocketException     // Catch:{ all -> 0x0037 }
                if (r2 == 0) goto L_0x007e
                java.lang.Throwable r2 = r6.thrown     // Catch:{ all -> 0x0037 }
                if (r2 == 0) goto L_0x007b
                java.lang.Throwable r2 = r6.thrown     // Catch:{ all -> 0x0037 }
                boolean r2 = r2 instanceof java.io.InterruptedIOException     // Catch:{ all -> 0x0037 }
                if (r2 == 0) goto L_0x0067
            L_0x007b:
                r6.thrown = r8     // Catch:{ all -> 0x0037 }
                goto L_0x0067
            L_0x007e:
                r6.thrown = r8     // Catch:{ all -> 0x0037 }
                goto L_0x0067
            L_0x0081:
                if (r1 == 0) goto L_0x0088
                int r2 = r0 + 1
                r6.send(r2)     // Catch:{ all -> 0x0037 }
            L_0x0088:
                boolean r2 = r6.done     // Catch:{ all -> 0x0037 }
                if (r2 == 0) goto L_0x008e
                monitor-exit(r6)     // Catch:{ all -> 0x0037 }
                goto L_0x0029
            L_0x008e:
                int r2 = r6.outstanding     // Catch:{ all -> 0x0037 }
                if (r2 != 0) goto L_0x009e
                r2 = 1
                r6.done = r2     // Catch:{ all -> 0x0037 }
                org.xbill.DNS.ResolverListener r2 = r6.listener     // Catch:{ all -> 0x0037 }
                if (r2 != 0) goto L_0x009e
                r6.notifyAll()     // Catch:{ all -> 0x0037 }
                monitor-exit(r6)     // Catch:{ all -> 0x0037 }
                goto L_0x0029
            L_0x009e:
                boolean r2 = r6.done     // Catch:{ all -> 0x0037 }
                if (r2 != 0) goto L_0x00a4
                monitor-exit(r6)     // Catch:{ all -> 0x0037 }
                goto L_0x0029
            L_0x00a4:
                monitor-exit(r6)     // Catch:{ all -> 0x0037 }
                java.lang.Throwable r2 = r6.thrown
                boolean r2 = r2 instanceof java.lang.Exception
                if (r2 != 0) goto L_0x00b8
                java.lang.RuntimeException r2 = new java.lang.RuntimeException
                java.lang.Throwable r3 = r6.thrown
                java.lang.String r3 = r3.getMessage()
                r2.<init>(r3)
                r6.thrown = r2
            L_0x00b8:
                org.xbill.DNS.ResolverListener r3 = r6.listener
                java.lang.Throwable r2 = r6.thrown
                java.lang.Exception r2 = (java.lang.Exception) r2
                r3.handleException(r6, r2)
                goto L_0x0029
            */
            throw new UnsupportedOperationException("Method not decompiled: org.xbill.DNS.ExtendedResolver.Resolution.handleException(java.lang.Object, java.lang.Exception):void");
        }
    }

    private void init() {
        this.resolvers = new ArrayList();
    }

    public ExtendedResolver() throws UnknownHostException {
        init();
        String[] servers = ResolverConfig.getCurrentConfig().servers();
        if (servers != null) {
            for (String simpleResolver : servers) {
                Resolver r = new SimpleResolver(simpleResolver);
                r.setTimeout(5);
                this.resolvers.add(r);
            }
            return;
        }
        this.resolvers.add(new SimpleResolver());
    }

    public ExtendedResolver(String[] servers) throws UnknownHostException {
        init();
        for (String simpleResolver : servers) {
            Resolver r = new SimpleResolver(simpleResolver);
            r.setTimeout(5);
            this.resolvers.add(r);
        }
    }

    public ExtendedResolver(Resolver[] res) throws UnknownHostException {
        init();
        for (Resolver add : res) {
            this.resolvers.add(add);
        }
    }

    public void setPort(int port) {
        for (int i = 0; i < this.resolvers.size(); i++) {
            ((Resolver) this.resolvers.get(i)).setPort(port);
        }
    }

    public void setTCP(boolean flag) {
        for (int i = 0; i < this.resolvers.size(); i++) {
            ((Resolver) this.resolvers.get(i)).setTCP(flag);
        }
    }

    public void setIgnoreTruncation(boolean flag) {
        for (int i = 0; i < this.resolvers.size(); i++) {
            ((Resolver) this.resolvers.get(i)).setIgnoreTruncation(flag);
        }
    }

    public void setEDNS(int level) {
        for (int i = 0; i < this.resolvers.size(); i++) {
            ((Resolver) this.resolvers.get(i)).setEDNS(level);
        }
    }

    public void setEDNS(int level, int payloadSize, int flags, List options) {
        for (int i = 0; i < this.resolvers.size(); i++) {
            ((Resolver) this.resolvers.get(i)).setEDNS(level, payloadSize, flags, options);
        }
    }

    public void setTSIGKey(TSIG key) {
        for (int i = 0; i < this.resolvers.size(); i++) {
            ((Resolver) this.resolvers.get(i)).setTSIGKey(key);
        }
    }

    public void setTimeout(int secs, int msecs) {
        for (int i = 0; i < this.resolvers.size(); i++) {
            ((Resolver) this.resolvers.get(i)).setTimeout(secs, msecs);
        }
    }

    public void setTimeout(int secs) {
        setTimeout(secs, 0);
    }

    public Message send(Message query) throws IOException {
        return new Resolution(this, query).start();
    }

    public Object sendAsync(Message query, ResolverListener listener) {
        Resolution res = new Resolution(this, query);
        res.startAsync(listener);
        return res;
    }

    public Resolver getResolver(int n) {
        if (n < this.resolvers.size()) {
            return (Resolver) this.resolvers.get(n);
        }
        return null;
    }

    public Resolver[] getResolvers() {
        return (Resolver[]) this.resolvers.toArray(new Resolver[this.resolvers.size()]);
    }

    public void addResolver(Resolver r) {
        this.resolvers.add(r);
    }

    public void deleteResolver(Resolver r) {
        this.resolvers.remove(r);
    }

    public void setLoadBalance(boolean flag) {
        this.loadBalance = flag;
    }

    public void setRetries(int retries2) {
        this.retries = retries2;
    }
}
