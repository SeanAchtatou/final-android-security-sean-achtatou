package org.xbill.DNS;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Generator {
    private long current;
    public final int dclass;
    public long end;
    public final String namePattern;
    public final Name origin;
    public final String rdataPattern;
    public long start;
    public long step;
    public final long ttl;
    public final int type;

    public static boolean supportedType(int type2) {
        Type.check(type2);
        if (type2 == 12 || type2 == 5 || type2 == 39 || type2 == 1 || type2 == 28 || type2 == 2) {
            return true;
        }
        return false;
    }

    public Generator(long start2, long end2, long step2, String namePattern2, int type2, int dclass2, long ttl2, String rdataPattern2, Name origin2) {
        if (start2 < 0 || end2 < 0 || start2 > end2 || step2 <= 0) {
            throw new IllegalArgumentException("invalid range specification");
        } else if (!supportedType(type2)) {
            throw new IllegalArgumentException("unsupported type");
        } else {
            DClass.check(dclass2);
            this.start = start2;
            this.end = end2;
            this.step = step2;
            this.namePattern = namePattern2;
            this.type = type2;
            this.dclass = dclass2;
            this.ttl = ttl2;
            this.rdataPattern = rdataPattern2;
            this.origin = origin2;
            this.current = start2;
        }
    }

    private String substitute(String spec, long n) throws IOException {
        String number;
        boolean escaped = false;
        byte[] str = spec.getBytes();
        StringBuffer sb = new StringBuffer();
        int i = 0;
        while (i < str.length) {
            char c = (char) (str[i] & 255);
            if (escaped) {
                sb.append(c);
                escaped = false;
            } else if (c == '\\') {
                if (i + 1 == str.length) {
                    throw new TextParseException("invalid escape character");
                }
                escaped = true;
            } else if (c == '$') {
                boolean negative = false;
                long offset = 0;
                long width = 0;
                long base = 10;
                boolean wantUpperCase = false;
                if (i + 1 >= str.length || str[i + 1] != 36) {
                    if (i + 1 < str.length && str[i + 1] == 123) {
                        int i2 = i + 1;
                        if (i2 + 1 < str.length && str[i2 + 1] == 45) {
                            negative = true;
                            i2++;
                        }
                        while (i2 + 1 < str.length) {
                            i2++;
                            c = (char) (str[i2] & 255);
                            if (c == ',' || c == '}') {
                                break;
                            } else if (c < '0' || c > '9') {
                                throw new TextParseException("invalid offset");
                            } else {
                                c = (char) (c - '0');
                                offset = (offset * 10) + ((long) c);
                            }
                        }
                        if (negative) {
                            offset = -offset;
                        }
                        if (c == ',') {
                            while (i2 + 1 < str.length) {
                                i2++;
                                c = (char) (str[i2] & 255);
                                if (c == ',' || c == '}') {
                                    break;
                                } else if (c < '0' || c > '9') {
                                    throw new TextParseException("invalid width");
                                } else {
                                    c = (char) (c - '0');
                                    width = (width * 10) + ((long) c);
                                }
                            }
                        }
                        if (c == ',') {
                            if (i2 + 1 == str.length) {
                                throw new TextParseException("invalid base");
                            }
                            i2++;
                            char c2 = (char) (str[i2] & 255);
                            if (c2 == 'o') {
                                base = 8;
                            } else if (c2 == 'x') {
                                base = 16;
                            } else if (c2 == 'X') {
                                base = 16;
                                wantUpperCase = true;
                            } else if (c2 != 'd') {
                                throw new TextParseException("invalid base");
                            }
                        }
                        if (i2 + 1 == str.length || str[i2 + 1] != 125) {
                            throw new TextParseException("invalid modifiers");
                        }
                        i = i2 + 1;
                    }
                    long v = n + offset;
                    if (v < 0) {
                        throw new TextParseException("invalid offset expansion");
                    }
                    if (base == 8) {
                        number = Long.toOctalString(v);
                    } else if (base == 16) {
                        number = Long.toHexString(v);
                    } else {
                        number = Long.toString(v);
                    }
                    if (wantUpperCase) {
                        number = number.toUpperCase();
                    }
                    if (width != 0 && width > ((long) number.length())) {
                        int zeros = ((int) width) - number.length();
                        while (true) {
                            int zeros2 = zeros;
                            zeros = zeros2 - 1;
                            if (zeros2 <= 0) {
                                break;
                            }
                            sb.append('0');
                        }
                    }
                    sb.append(number);
                } else {
                    i++;
                    sb.append((char) (str[i] & 255));
                }
            } else {
                sb.append(c);
            }
            i++;
        }
        return sb.toString();
    }

    public Record nextRecord() throws IOException {
        if (this.current > this.end) {
            return null;
        }
        Name name = Name.fromString(substitute(this.namePattern, this.current), this.origin);
        String rdata = substitute(this.rdataPattern, this.current);
        this.current += this.step;
        return Record.fromString(name, this.type, this.dclass, this.ttl, rdata, this.origin);
    }

    public Record[] expand() throws IOException {
        List list = new ArrayList();
        long i = this.start;
        while (i < this.end) {
            list.add(Record.fromString(Name.fromString(substitute(this.namePattern, this.current), this.origin), this.type, this.dclass, this.ttl, substitute(this.rdataPattern, this.current), this.origin));
            i += this.step;
        }
        return (Record[]) list.toArray(new Record[list.size()]);
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("$GENERATE ");
        sb.append(String.valueOf(this.start) + "-" + this.end);
        if (this.step > 1) {
            sb.append("/" + this.step);
        }
        sb.append(" ");
        sb.append(String.valueOf(this.namePattern) + " ");
        sb.append(String.valueOf(this.ttl) + " ");
        if (this.dclass != 1 || !Options.check("noPrintIN")) {
            sb.append(String.valueOf(DClass.string(this.dclass)) + " ");
        }
        sb.append(String.valueOf(Type.string(this.type)) + " ");
        sb.append(String.valueOf(this.rdataPattern) + " ");
        return sb.toString();
    }
}
