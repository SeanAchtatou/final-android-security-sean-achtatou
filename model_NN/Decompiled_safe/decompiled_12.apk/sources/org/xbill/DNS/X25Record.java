package org.xbill.DNS;

import java.io.IOException;

public class X25Record extends Record {
    private static final long serialVersionUID = 4267576252335579764L;
    private byte[] address;

    X25Record() {
    }

    /* access modifiers changed from: package-private */
    public Record getObject() {
        return new X25Record();
    }

    private static final byte[] checkAndConvertAddress(String address2) {
        int length = address2.length();
        byte[] out = new byte[length];
        for (int i = 0; i < length; i++) {
            char c = address2.charAt(i);
            if (!Character.isDigit(c)) {
                return null;
            }
            out[i] = (byte) c;
        }
        return out;
    }

    public X25Record(Name name, int dclass, long ttl, String address2) {
        super(name, 19, dclass, ttl);
        this.address = checkAndConvertAddress(address2);
        if (this.address == null) {
            throw new IllegalArgumentException("invalid PSDN address " + address2);
        }
    }

    /* access modifiers changed from: package-private */
    public void rrFromWire(DNSInput in) throws IOException {
        this.address = in.readCountedString();
    }

    /* access modifiers changed from: package-private */
    public void rdataFromString(Tokenizer st, Name origin) throws IOException {
        String addr = st.getString();
        this.address = checkAndConvertAddress(addr);
        if (this.address == null) {
            throw st.exception("invalid PSDN address " + addr);
        }
    }

    public String getAddress() {
        return byteArrayToString(this.address, false);
    }

    /* access modifiers changed from: package-private */
    public void rrToWire(DNSOutput out, Compression c, boolean canonical) {
        out.writeCountedString(this.address);
    }

    /* access modifiers changed from: package-private */
    public String rrToString() {
        return byteArrayToString(this.address, true);
    }
}
