package org.xbill.DNS;

import java.io.IOException;
import java.io.Serializable;
import java.text.DecimalFormat;
import org.kxml2.wap.Wbxml;

public class Name implements Comparable, Serializable {
    private static final int LABEL_COMPRESSION = 192;
    private static final int LABEL_MASK = 192;
    private static final int LABEL_NORMAL = 0;
    private static final int MAXLABEL = 63;
    private static final int MAXLABELS = 128;
    private static final int MAXNAME = 255;
    private static final int MAXOFFSETS = 7;
    private static final DecimalFormat byteFormat = new DecimalFormat();
    public static final Name empty = new Name();
    private static final byte[] emptyLabel = new byte[1];
    private static final byte[] lowercase = new byte[256];
    public static final Name root = new Name();
    private static final long serialVersionUID = -7257019940971525644L;
    private static final Name wild = new Name();
    private static final byte[] wildLabel = {1, 42};
    private int hashcode;
    private byte[] name;
    private long offsets;

    static {
        byteFormat.setMinimumIntegerDigits(3);
        for (int i = 0; i < lowercase.length; i++) {
            if (i < 65 || i > 90) {
                lowercase[i] = (byte) i;
            } else {
                lowercase[i] = (byte) ((i - 65) + 97);
            }
        }
        root.appendSafe(emptyLabel, 0, 1);
        empty.name = new byte[0];
        wild.appendSafe(wildLabel, 0, 1);
    }

    private Name() {
    }

    private final void setoffset(int n, int offset) {
        if (n < 7) {
            int shift = (7 - n) * 8;
            this.offsets &= (255 << shift) ^ -1;
            this.offsets |= ((long) offset) << shift;
        }
    }

    private final int offset(int n) {
        if (n == 0 && getlabels() == 0) {
            return 0;
        }
        if (n < 0 || n >= getlabels()) {
            throw new IllegalArgumentException("label out of range");
        } else if (n < 7) {
            return ((int) (this.offsets >>> ((7 - n) * 8))) & 255;
        } else {
            int pos = offset(6);
            for (int i = 6; i < n; i++) {
                pos += this.name[pos] + 1;
            }
            return pos;
        }
    }

    private final void setlabels(int labels) {
        this.offsets &= -256;
        this.offsets |= (long) labels;
    }

    private final int getlabels() {
        return (int) (this.offsets & 255);
    }

    private static final void copy(Name src, Name dst) {
        if (src.offset(0) == 0) {
            dst.name = src.name;
            dst.offsets = src.offsets;
            return;
        }
        int offset0 = src.offset(0);
        int namelen = src.name.length - offset0;
        int labels = src.labels();
        dst.name = new byte[namelen];
        System.arraycopy(src.name, offset0, dst.name, 0, namelen);
        int i = 0;
        while (i < labels && i < 7) {
            dst.setoffset(i, src.offset(i) - offset0);
            i++;
        }
        dst.setlabels(labels);
    }

    private final void append(byte[] array, int start, int n) throws NameTooLongException {
        int length = this.name == null ? 0 : this.name.length - offset(0);
        int alength = 0;
        int pos = start;
        for (int i = 0; i < n; i++) {
            byte b = array[pos];
            if (b > 63) {
                throw new IllegalStateException("invalid label");
            }
            int len = b + 1;
            pos += len;
            alength += len;
        }
        int newlength = length + alength;
        if (newlength > 255) {
            throw new NameTooLongException();
        }
        int labels = getlabels();
        int newlabels = labels + n;
        if (newlabels > 128) {
            throw new IllegalStateException("too many labels");
        }
        byte[] newname = new byte[newlength];
        if (length != 0) {
            System.arraycopy(this.name, offset(0), newname, 0, length);
        }
        System.arraycopy(array, start, newname, length, alength);
        this.name = newname;
        int pos2 = length;
        for (int i2 = 0; i2 < n; i2++) {
            setoffset(labels + i2, pos2);
            pos2 += newname[pos2] + 1;
        }
        setlabels(newlabels);
    }

    private static TextParseException parseException(String str, String message) {
        return new TextParseException("'" + str + "': " + message);
    }

    private final void appendFromString(String fullName, byte[] array, int start, int n) throws TextParseException {
        try {
            append(array, start, n);
        } catch (NameTooLongException e) {
            throw parseException(fullName, "Name too long");
        }
    }

    private final void appendSafe(byte[] array, int start, int n) {
        try {
            append(array, start, n);
        } catch (NameTooLongException e) {
        }
    }

    public Name(String s, Name origin) throws TextParseException {
        if (s.equals("")) {
            throw parseException(s, "empty name");
        } else if (s.equals("@")) {
            if (origin == null) {
                copy(empty, this);
            } else {
                copy(origin, this);
            }
        } else if (s.equals(".")) {
            copy(root, this);
        } else {
            int labelstart = -1;
            int pos = 1;
            byte[] label = new byte[64];
            boolean escaped = false;
            int digits = 0;
            int intval = 0;
            boolean absolute = false;
            for (int i = 0; i < s.length(); i++) {
                byte b = (byte) s.charAt(i);
                if (escaped) {
                    if (b >= 48 && b <= 57 && digits < 3) {
                        digits++;
                        intval = (intval * 10) + (b - 48);
                        if (intval > 255) {
                            throw parseException(s, "bad escape");
                        } else if (digits < 3) {
                            continue;
                        } else {
                            b = (byte) intval;
                        }
                    } else if (digits > 0 && digits < 3) {
                        throw parseException(s, "bad escape");
                    }
                    if (pos > 63) {
                        throw parseException(s, "label too long");
                    }
                    labelstart = pos;
                    label[pos] = b;
                    escaped = false;
                    pos++;
                } else if (b == 92) {
                    escaped = true;
                    digits = 0;
                    intval = 0;
                } else if (b != 46) {
                    labelstart = labelstart == -1 ? i : labelstart;
                    if (pos > 63) {
                        throw parseException(s, "label too long");
                    }
                    label[pos] = b;
                    pos++;
                } else if (labelstart == -1) {
                    throw parseException(s, "invalid empty label");
                } else {
                    label[0] = (byte) (pos - 1);
                    appendFromString(s, label, 0, 1);
                    labelstart = -1;
                    pos = 1;
                }
            }
            if (digits > 0 && digits < 3) {
                throw parseException(s, "bad escape");
            } else if (escaped) {
                throw parseException(s, "bad escape");
            } else {
                if (labelstart == -1) {
                    appendFromString(s, emptyLabel, 0, 1);
                    absolute = true;
                } else {
                    label[0] = (byte) (pos - 1);
                    appendFromString(s, label, 0, 1);
                }
                if (origin != null && !absolute) {
                    appendFromString(s, origin.name, 0, origin.getlabels());
                }
            }
        }
    }

    public Name(String s) throws TextParseException {
        this(s, (Name) null);
    }

    public static Name fromString(String s, Name origin) throws TextParseException {
        if (s.equals("@") && origin != null) {
            return origin;
        }
        if (s.equals(".")) {
            return root;
        }
        return new Name(s, origin);
    }

    public static Name fromString(String s) throws TextParseException {
        return fromString(s, null);
    }

    public static Name fromConstantString(String s) {
        try {
            return fromString(s, null);
        } catch (TextParseException e) {
            throw new IllegalArgumentException("Invalid name '" + s + "'");
        }
    }

    public Name(DNSInput in) throws WireParseException {
        boolean done = false;
        byte[] label = new byte[64];
        boolean savedState = false;
        while (!done) {
            int len = in.readU8();
            switch (len & Wbxml.EXT_0) {
                case 0:
                    if (getlabels() < 128) {
                        if (len != 0) {
                            label[0] = (byte) len;
                            in.readByteArray(label, 1, len);
                            append(label, 0, 1);
                            break;
                        } else {
                            append(emptyLabel, 0, 1);
                            done = true;
                            break;
                        }
                    } else {
                        throw new WireParseException("too many labels");
                    }
                case Wbxml.EXT_0:
                    int pos = in.readU8() + ((len & -193) << 8);
                    if (Options.check("verbosecompression")) {
                        System.err.println("currently " + in.current() + ", pointer to " + pos);
                    }
                    if (pos < in.current() - 2) {
                        if (!savedState) {
                            in.save();
                            savedState = true;
                        }
                        in.jump(pos);
                        if (!Options.check("verbosecompression")) {
                            break;
                        } else {
                            System.err.println("current name '" + this + "', seeking to " + pos);
                            break;
                        }
                    } else {
                        throw new WireParseException("bad compression");
                    }
                default:
                    throw new WireParseException("bad label type");
            }
        }
        if (savedState) {
            in.restore();
        }
    }

    public Name(byte[] b) throws IOException {
        this(new DNSInput(b));
    }

    public Name(Name src, int n) {
        int slabels = src.labels();
        if (n > slabels) {
            throw new IllegalArgumentException("attempted to remove too many labels");
        }
        this.name = src.name;
        setlabels(slabels - n);
        int i = 0;
        while (i < 7 && i < slabels - n) {
            setoffset(i, src.offset(i + n));
            i++;
        }
    }

    public static Name concatenate(Name prefix, Name suffix) throws NameTooLongException {
        if (prefix.isAbsolute()) {
            return prefix;
        }
        Name newname = new Name();
        copy(prefix, newname);
        newname.append(suffix.name, suffix.offset(0), suffix.getlabels());
        return newname;
    }

    public Name relativize(Name origin) {
        if (origin == null || !subdomain(origin)) {
            return this;
        }
        Name newname = new Name();
        copy(this, newname);
        int length = length() - origin.length();
        newname.setlabels(newname.labels() - origin.labels());
        newname.name = new byte[length];
        System.arraycopy(this.name, offset(0), newname.name, 0, length);
        return newname;
    }

    public Name wild(int n) {
        if (n < 1) {
            throw new IllegalArgumentException("must replace 1 or more labels");
        }
        try {
            Name newname = new Name();
            copy(wild, newname);
            newname.append(this.name, offset(n), getlabels() - n);
            return newname;
        } catch (NameTooLongException e) {
            throw new IllegalStateException("Name.wild: concatenate failed");
        }
    }

    public Name fromDNAME(DNAMERecord dname) throws NameTooLongException {
        Name dnameowner = dname.getName();
        Name dnametarget = dname.getTarget();
        if (!subdomain(dnameowner)) {
            return null;
        }
        int plabels = labels() - dnameowner.labels();
        int plength = length() - dnameowner.length();
        int pstart = offset(0);
        int dlabels = dnametarget.labels();
        int dlength = dnametarget.length();
        if (plength + dlength > 255) {
            throw new NameTooLongException();
        }
        Name newname = new Name();
        newname.setlabels(plabels + dlabels);
        newname.name = new byte[(plength + dlength)];
        System.arraycopy(this.name, pstart, newname.name, 0, plength);
        System.arraycopy(dnametarget.name, 0, newname.name, plength, dlength);
        int i = 0;
        int pos = 0;
        while (i < 7 && i < plabels + dlabels) {
            newname.setoffset(i, pos);
            pos += newname.name[pos] + 1;
            i++;
        }
        return newname;
    }

    public boolean isWild() {
        if (labels() != 0 && this.name[0] == 1 && this.name[1] == 42) {
            return true;
        }
        return false;
    }

    public boolean isAbsolute() {
        if (labels() != 0 && this.name[this.name.length - 1] == 0) {
            return true;
        }
        return false;
    }

    public short length() {
        if (getlabels() == 0) {
            return 0;
        }
        return (short) (this.name.length - offset(0));
    }

    public int labels() {
        return getlabels();
    }

    public boolean subdomain(Name domain) {
        int labels = labels();
        int dlabels = domain.labels();
        if (dlabels > labels) {
            return false;
        }
        if (dlabels == labels) {
            return equals(domain);
        }
        return domain.equals(this.name, offset(labels - dlabels));
    }

    private String byteString(byte[] array, int pos) {
        StringBuffer sb = new StringBuffer();
        int pos2 = pos + 1;
        byte b = array[pos];
        for (int i = pos2; i < pos2 + b; i++) {
            int b2 = array[i] & 255;
            if (b2 <= 32 || b2 >= 127) {
                sb.append('\\');
                sb.append(byteFormat.format((long) b2));
            } else if (b2 == 34 || b2 == 40 || b2 == 41 || b2 == 46 || b2 == 59 || b2 == 92 || b2 == 64 || b2 == 36) {
                sb.append('\\');
                sb.append((char) b2);
            } else {
                sb.append((char) b2);
            }
        }
        return sb.toString();
    }

    public String toString() {
        int labels = labels();
        if (labels == 0) {
            return "@";
        }
        if (labels == 1 && this.name[offset(0)] == 0) {
            return ".";
        }
        StringBuffer sb = new StringBuffer();
        int i = 0;
        int pos = offset(0);
        while (i < labels) {
            byte b = this.name[pos];
            if (b <= 63) {
                if (b == 0) {
                    break;
                }
                sb.append(byteString(this.name, pos));
                sb.append('.');
                pos += b + 1;
                i++;
            } else {
                throw new IllegalStateException("invalid label");
            }
        }
        if (!isAbsolute()) {
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.toString();
    }

    public byte[] getLabel(int n) {
        int pos = offset(n);
        int i = (byte) (this.name[pos] + 1);
        byte[] label = new byte[i];
        System.arraycopy(this.name, pos, label, 0, i);
        return label;
    }

    public String getLabelString(int n) {
        return byteString(this.name, offset(n));
    }

    public void toWire(DNSOutput out, Compression c) {
        Name tname;
        if (!isAbsolute()) {
            throw new IllegalArgumentException("toWire() called on non-absolute name");
        }
        int labels = labels();
        for (int i = 0; i < labels - 1; i++) {
            if (i == 0) {
                tname = this;
            } else {
                tname = new Name(this, i);
            }
            int pos = -1;
            if (c != null) {
                pos = c.get(tname);
            }
            if (pos >= 0) {
                out.writeU16(pos | 49152);
                return;
            }
            if (c != null) {
                c.add(out.current(), tname);
            }
            int off = offset(i);
            out.writeByteArray(this.name, off, this.name[off] + 1);
        }
        out.writeU8(0);
    }

    public byte[] toWire() {
        DNSOutput out = new DNSOutput();
        toWire(out, null);
        return out.toByteArray();
    }

    public void toWireCanonical(DNSOutput out) {
        out.writeByteArray(toWireCanonical());
    }

    public byte[] toWireCanonical() {
        int labels = labels();
        if (labels == 0) {
            return new byte[0];
        }
        byte[] b = new byte[(this.name.length - offset(0))];
        int dpos = 0;
        int spos = offset(0);
        for (int i = 0; i < labels; i++) {
            byte b2 = this.name[spos];
            if (b2 > 63) {
                throw new IllegalStateException("invalid label");
            }
            b[dpos] = this.name[spos];
            int j = 0;
            dpos++;
            spos++;
            while (j < b2) {
                b[dpos] = lowercase[this.name[spos] & 255];
                j++;
                dpos++;
                spos++;
            }
        }
        return b;
    }

    public void toWire(DNSOutput out, Compression c, boolean canonical) {
        if (canonical) {
            toWireCanonical(out);
        } else {
            toWire(out, c);
        }
    }

    private final boolean equals(byte[] b, int bpos) {
        int labels = labels();
        int i = 0;
        int pos = offset(0);
        while (i < labels) {
            if (this.name[pos] != b[bpos]) {
                return false;
            }
            int pos2 = pos + 1;
            byte b2 = this.name[pos];
            int bpos2 = bpos + 1;
            if (b2 > 63) {
                throw new IllegalStateException("invalid label");
            }
            int j = 0;
            pos = pos2;
            int bpos3 = bpos2;
            while (j < b2) {
                int pos3 = pos + 1;
                int bpos4 = bpos3 + 1;
                if (lowercase[this.name[pos] & 255] != lowercase[b[bpos3] & 255]) {
                    return false;
                }
                j++;
                pos = pos3;
                bpos3 = bpos4;
            }
            i++;
            bpos = bpos3;
        }
        return true;
    }

    public boolean equals(Object arg) {
        if (arg == this) {
            return true;
        }
        if (arg == null || !(arg instanceof Name)) {
            return false;
        }
        Name d = (Name) arg;
        if (d.hashcode == 0) {
            d.hashCode();
        }
        if (this.hashcode == 0) {
            hashCode();
        }
        if (d.hashcode == this.hashcode && d.labels() == labels()) {
            return equals(d.name, d.offset(0));
        }
        return false;
    }

    public int hashCode() {
        if (this.hashcode != 0) {
            return this.hashcode;
        }
        int code = 0;
        for (int i = offset(0); i < this.name.length; i++) {
            code += (code << 3) + lowercase[this.name[i] & 255];
        }
        this.hashcode = code;
        return this.hashcode;
    }

    /* JADX WARN: Type inference failed for: r16v0, types: [java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int compareTo(java.lang.Object r16) {
        /*
            r15 = this;
            r2 = r16
            org.xbill.DNS.Name r2 = (org.xbill.DNS.Name) r2
            if (r15 != r2) goto L_0x0008
            r9 = 0
        L_0x0007:
            return r9
        L_0x0008:
            int r7 = r15.labels()
            int r0 = r2.labels()
            if (r7 <= r0) goto L_0x0019
            r4 = r0
        L_0x0013:
            r5 = 1
        L_0x0014:
            if (r5 <= r4) goto L_0x001b
            int r9 = r7 - r0
            goto L_0x0007
        L_0x0019:
            r4 = r7
            goto L_0x0013
        L_0x001b:
            int r11 = r7 - r5
            int r10 = r15.offset(r11)
            int r11 = r0 - r5
            int r3 = r2.offset(r11)
            byte[] r11 = r15.name
            byte r8 = r11[r10]
            byte[] r11 = r2.name
            byte r1 = r11[r3]
            r6 = 0
        L_0x0030:
            if (r6 >= r8) goto L_0x0034
            if (r6 < r1) goto L_0x0039
        L_0x0034:
            if (r8 == r1) goto L_0x005c
            int r9 = r8 - r1
            goto L_0x0007
        L_0x0039:
            byte[] r11 = org.xbill.DNS.Name.lowercase
            byte[] r12 = r15.name
            int r13 = r6 + r10
            int r13 = r13 + 1
            byte r12 = r12[r13]
            r12 = r12 & 255(0xff, float:3.57E-43)
            byte r11 = r11[r12]
            byte[] r12 = org.xbill.DNS.Name.lowercase
            byte[] r13 = r2.name
            int r14 = r6 + r3
            int r14 = r14 + 1
            byte r13 = r13[r14]
            r13 = r13 & 255(0xff, float:3.57E-43)
            byte r12 = r12[r13]
            int r9 = r11 - r12
            if (r9 != 0) goto L_0x0007
            int r6 = r6 + 1
            goto L_0x0030
        L_0x005c:
            int r5 = r5 + 1
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: org.xbill.DNS.Name.compareTo(java.lang.Object):int");
    }
}
