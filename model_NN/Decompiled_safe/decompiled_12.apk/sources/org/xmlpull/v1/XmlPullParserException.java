package org.xmlpull.v1;

public class XmlPullParserException extends Exception {
    protected int column;
    protected Throwable detail;
    protected int row;

    public XmlPullParserException(String s) {
        super(s);
        this.row = -1;
        this.column = -1;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public XmlPullParserException(java.lang.String r5, org.xmlpull.v1.XmlPullParser r6, java.lang.Throwable r7) {
        /*
            r4 = this;
            r3 = -1
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            if (r5 != 0) goto L_0x003a
            java.lang.String r0 = ""
        L_0x000a:
            java.lang.StringBuilder r1 = r1.append(r0)
            if (r6 != 0) goto L_0x004e
            java.lang.String r0 = ""
        L_0x0012:
            java.lang.StringBuilder r1 = r1.append(r0)
            if (r7 != 0) goto L_0x006c
            java.lang.String r0 = ""
        L_0x001a:
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            r4.<init>(r0)
            r4.row = r3
            r4.column = r3
            if (r6 == 0) goto L_0x0037
            int r0 = r6.getLineNumber()
            r4.row = r0
            int r0 = r6.getColumnNumber()
            r4.column = r0
        L_0x0037:
            r4.detail = r7
            return
        L_0x003a:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.StringBuilder r0 = r0.append(r5)
            java.lang.String r2 = " "
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            goto L_0x000a
        L_0x004e:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "(position:"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = r6.getPositionDescription()
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = ") "
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            goto L_0x0012
        L_0x006c:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "caused by: "
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.StringBuilder r0 = r0.append(r7)
            java.lang.String r0 = r0.toString()
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: org.xmlpull.v1.XmlPullParserException.<init>(java.lang.String, org.xmlpull.v1.XmlPullParser, java.lang.Throwable):void");
    }

    public Throwable getDetail() {
        return this.detail;
    }

    public int getLineNumber() {
        return this.row;
    }

    public int getColumnNumber() {
        return this.column;
    }

    public void printStackTrace() {
        if (this.detail == null) {
            super.printStackTrace();
            return;
        }
        synchronized (System.err) {
            System.err.println(super.getMessage() + "; nested exception is:");
            this.detail.printStackTrace();
        }
    }
}
