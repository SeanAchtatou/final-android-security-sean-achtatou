package org.xmlpull.v1;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public class XmlPullParserFactory {
    public static final String PROPERTY_NAME = "org.xmlpull.v1.XmlPullParserFactory";
    private static final String RESOURCE_NAME = "/META-INF/services/org.xmlpull.v1.XmlPullParserFactory";
    static final Class referenceContextClass = new XmlPullParserFactory().getClass();
    protected String classNamesLocation;
    protected Hashtable features = new Hashtable();
    protected Vector parserClasses;
    protected Vector serializerClasses;

    protected XmlPullParserFactory() {
    }

    public void setFeature(String name, boolean state) throws XmlPullParserException {
        this.features.put(name, new Boolean(state));
    }

    public boolean getFeature(String name) {
        Boolean value = (Boolean) this.features.get(name);
        if (value != null) {
            return value.booleanValue();
        }
        return false;
    }

    public void setNamespaceAware(boolean awareness) {
        this.features.put(XmlPullParser.FEATURE_PROCESS_NAMESPACES, new Boolean(awareness));
    }

    public boolean isNamespaceAware() {
        return getFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES);
    }

    public void setValidating(boolean validating) {
        this.features.put(XmlPullParser.FEATURE_VALIDATION, new Boolean(validating));
    }

    public boolean isValidating() {
        return getFeature(XmlPullParser.FEATURE_VALIDATION);
    }

    public XmlPullParser newPullParser() throws XmlPullParserException {
        if (this.parserClasses == null) {
            throw new XmlPullParserException("Factory initialization was incomplete - has not tried " + this.classNamesLocation);
        } else if (this.parserClasses.size() == 0) {
            throw new XmlPullParserException("No valid parser classes found in " + this.classNamesLocation);
        } else {
            StringBuffer issues = new StringBuffer();
            int i = 0;
            while (i < this.parserClasses.size()) {
                Class ppClass = (Class) this.parserClasses.elementAt(i);
                try {
                    XmlPullParser pp = (XmlPullParser) ppClass.newInstance();
                    Enumeration e = this.features.keys();
                    while (e.hasMoreElements()) {
                        String key = (String) e.nextElement();
                        Boolean value = (Boolean) this.features.get(key);
                        if (value != null && value.booleanValue()) {
                            pp.setFeature(key, true);
                        }
                    }
                    return pp;
                } catch (Exception ex) {
                    issues.append(ppClass.getName() + ": " + ex.toString() + "; ");
                    i++;
                }
            }
            throw new XmlPullParserException("could not create parser: " + ((Object) issues));
        }
    }

    public XmlSerializer newSerializer() throws XmlPullParserException {
        if (this.serializerClasses == null) {
            throw new XmlPullParserException("Factory initialization incomplete - has not tried " + this.classNamesLocation);
        } else if (this.serializerClasses.size() == 0) {
            throw new XmlPullParserException("No valid serializer classes found in " + this.classNamesLocation);
        } else {
            StringBuffer issues = new StringBuffer();
            int i = 0;
            while (i < this.serializerClasses.size()) {
                Class ppClass = (Class) this.serializerClasses.elementAt(i);
                try {
                    return (XmlSerializer) ppClass.newInstance();
                } catch (Exception ex) {
                    issues.append(ppClass.getName() + ": " + ex.toString() + "; ");
                    i++;
                }
            }
            throw new XmlPullParserException("could not create serializer: " + ((Object) issues));
        }
    }

    public static XmlPullParserFactory newInstance() throws XmlPullParserException {
        return newInstance(null, null);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static org.xmlpull.v1.XmlPullParserFactory newInstance(java.lang.String r20, java.lang.Class r21) throws org.xmlpull.v1.XmlPullParserException {
        /*
            if (r21 != 0) goto L_0x0004
            java.lang.Class r21 = org.xmlpull.v1.XmlPullParserFactory.referenceContextClass
        L_0x0004:
            r5 = 0
            if (r20 == 0) goto L_0x0019
            int r17 = r20.length()
            if (r17 == 0) goto L_0x0019
            java.lang.String r17 = "DEFAULT"
            r0 = r17
            r1 = r20
            boolean r17 = r0.equals(r1)
            if (r17 == 0) goto L_0x00f7
        L_0x0019:
            java.lang.String r17 = "/META-INF/services/org.xmlpull.v1.XmlPullParserFactory"
            r0 = r21
            r1 = r17
            java.io.InputStream r10 = r0.getResourceAsStream(r1)     // Catch:{ Exception -> 0x002d }
            if (r10 != 0) goto L_0x003e
            org.xmlpull.v1.XmlPullParserException r17 = new org.xmlpull.v1.XmlPullParserException     // Catch:{ Exception -> 0x002d }
            java.lang.String r18 = "resource not found: /META-INF/services/org.xmlpull.v1.XmlPullParserFactory make sure that parser implementing XmlPull API is available"
            r17.<init>(r18)     // Catch:{ Exception -> 0x002d }
            throw r17     // Catch:{ Exception -> 0x002d }
        L_0x002d:
            r7 = move-exception
            org.xmlpull.v1.XmlPullParserException r17 = new org.xmlpull.v1.XmlPullParserException
            r18 = 0
            r19 = 0
            r0 = r17
            r1 = r18
            r2 = r19
            r0.<init>(r1, r2, r7)
            throw r17
        L_0x003e:
            java.lang.StringBuffer r15 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x002d }
            r15.<init>()     // Catch:{ Exception -> 0x002d }
        L_0x0043:
            int r4 = r10.read()     // Catch:{ Exception -> 0x002d }
            if (r4 >= 0) goto L_0x00e7
            r10.close()     // Catch:{ Exception -> 0x002d }
            java.lang.String r20 = r15.toString()     // Catch:{ Exception -> 0x002d }
            java.lang.StringBuilder r17 = new java.lang.StringBuilder
            r17.<init>()
            java.lang.String r18 = "resource /META-INF/services/org.xmlpull.v1.XmlPullParserFactory that contained '"
            java.lang.StringBuilder r17 = r17.append(r18)
            r0 = r17
            r1 = r20
            java.lang.StringBuilder r17 = r0.append(r1)
            java.lang.String r18 = "'"
            java.lang.StringBuilder r17 = r17.append(r18)
            java.lang.String r5 = r17.toString()
        L_0x006d:
            r8 = 0
            java.util.Vector r12 = new java.util.Vector
            r12.<init>()
            java.util.Vector r16 = new java.util.Vector
            r16.<init>()
            r13 = 0
        L_0x0079:
            int r17 = r20.length()
            r0 = r17
            if (r13 >= r0) goto L_0x011a
            r17 = 44
            r0 = r20
            r1 = r17
            int r6 = r0.indexOf(r1, r13)
            r17 = -1
            r0 = r17
            if (r6 != r0) goto L_0x0095
            int r6 = r20.length()
        L_0x0095:
            r0 = r20
            java.lang.String r11 = r0.substring(r13, r6)
            r3 = 0
            r9 = 0
            java.lang.Class r3 = java.lang.Class.forName(r11)     // Catch:{ Exception -> 0x012a }
            java.lang.Object r9 = r3.newInstance()     // Catch:{ Exception -> 0x012a }
        L_0x00a5:
            if (r3 == 0) goto L_0x0116
            r14 = 0
            boolean r0 = r9 instanceof org.xmlpull.v1.XmlPullParser
            r17 = r0
            if (r17 == 0) goto L_0x00b2
            r12.addElement(r3)
            r14 = 1
        L_0x00b2:
            boolean r0 = r9 instanceof org.xmlpull.v1.XmlSerializer
            r17 = r0
            if (r17 == 0) goto L_0x00be
            r0 = r16
            r0.addElement(r3)
            r14 = 1
        L_0x00be:
            boolean r0 = r9 instanceof org.xmlpull.v1.XmlPullParserFactory
            r17 = r0
            if (r17 == 0) goto L_0x00ca
            if (r8 != 0) goto L_0x00c9
            r8 = r9
            org.xmlpull.v1.XmlPullParserFactory r8 = (org.xmlpull.v1.XmlPullParserFactory) r8
        L_0x00c9:
            r14 = 1
        L_0x00ca:
            if (r14 != 0) goto L_0x0116
            org.xmlpull.v1.XmlPullParserException r17 = new org.xmlpull.v1.XmlPullParserException
            java.lang.StringBuilder r18 = new java.lang.StringBuilder
            r18.<init>()
            java.lang.String r19 = "incompatible class: "
            java.lang.StringBuilder r18 = r18.append(r19)
            r0 = r18
            java.lang.StringBuilder r18 = r0.append(r11)
            java.lang.String r18 = r18.toString()
            r17.<init>(r18)
            throw r17
        L_0x00e7:
            r17 = 32
            r0 = r17
            if (r4 <= r0) goto L_0x0043
            char r0 = (char) r4
            r17 = r0
            r0 = r17
            r15.append(r0)     // Catch:{ Exception -> 0x002d }
            goto L_0x0043
        L_0x00f7:
            java.lang.StringBuilder r17 = new java.lang.StringBuilder
            r17.<init>()
            java.lang.String r18 = "parameter classNames to newInstance() that contained '"
            java.lang.StringBuilder r17 = r17.append(r18)
            r0 = r17
            r1 = r20
            java.lang.StringBuilder r17 = r0.append(r1)
            java.lang.String r18 = "'"
            java.lang.StringBuilder r17 = r17.append(r18)
            java.lang.String r5 = r17.toString()
            goto L_0x006d
        L_0x0116:
            int r13 = r6 + 1
            goto L_0x0079
        L_0x011a:
            if (r8 != 0) goto L_0x0121
            org.xmlpull.v1.XmlPullParserFactory r8 = new org.xmlpull.v1.XmlPullParserFactory
            r8.<init>()
        L_0x0121:
            r8.parserClasses = r12
            r0 = r16
            r8.serializerClasses = r0
            r8.classNamesLocation = r5
            return r8
        L_0x012a:
            r17 = move-exception
            goto L_0x00a5
        */
        throw new UnsupportedOperationException("Method not decompiled: org.xmlpull.v1.XmlPullParserFactory.newInstance(java.lang.String, java.lang.Class):org.xmlpull.v1.XmlPullParserFactory");
    }
}
