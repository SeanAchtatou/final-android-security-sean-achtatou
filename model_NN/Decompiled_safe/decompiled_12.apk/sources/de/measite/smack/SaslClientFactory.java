package de.measite.smack;

import com.novell.sasl.client.DigestMD5SaslClient;
import java.util.Map;
import org.apache.harmony.javax.security.auth.callback.CallbackHandler;
import org.apache.harmony.javax.security.sasl.SaslClient;
import org.apache.harmony.javax.security.sasl.SaslException;
import org.apache.qpid.management.common.sasl.Constants;
import org.apache.qpid.management.common.sasl.PlainSaslClient;

public class SaslClientFactory implements org.apache.harmony.javax.security.sasl.SaslClientFactory {
    public SaslClient createSaslClient(String[] mechanisms, String authorizationId, String protocol, String serverName, Map<String, ?> props, CallbackHandler cbh) throws SaslException {
        for (String mech : mechanisms) {
            if (Constants.MECH_PLAIN.equals(mech)) {
                return new PlainSaslClient(authorizationId, cbh);
            }
            if ("DIGEST-MD5".equals(mech)) {
                return DigestMD5SaslClient.getClient(authorizationId, protocol, serverName, props, cbh);
            }
        }
        return null;
    }

    public String[] getMechanismNames(Map<String, ?> map) {
        return new String[]{Constants.MECH_PLAIN, "DIGEST-MD5"};
    }
}
