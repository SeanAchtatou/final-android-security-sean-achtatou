package defpackage;

import android.webkit.WebView;
import com.google.ads.f;
import com.google.ads.util.d;
import java.util.HashMap;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;

/* renamed from: i  reason: default package */
public final class i implements o {
    public final void a(c cVar, HashMap hashMap, WebView webView) {
        d.e("Invalid " + ((String) hashMap.get(TMXConstants.TAG_OBJECT_ATTRIBUTE_TYPE)) + " request error: " + ((String) hashMap.get("errors")));
        f f = cVar.f();
        if (f != null) {
            f.a(f.INVALID_REQUEST);
        }
    }
}
