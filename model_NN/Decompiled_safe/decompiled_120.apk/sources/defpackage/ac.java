package defpackage;

import java.util.LinkedList;

/* renamed from: ac  reason: default package */
final class ac implements Runnable {
    private final c a;
    private final LinkedList b;
    private final int c;
    private /* synthetic */ f d;

    public ac(f fVar, c cVar, LinkedList linkedList, int i) {
        this.d = fVar;
        this.a = cVar;
        this.b = linkedList;
        this.c = i;
    }

    public final void run() {
        this.a.a(this.b);
        this.a.a(this.c);
        this.a.o();
    }
}
