package defpackage;

import com.google.ads.util.d;
import java.lang.ref.WeakReference;

/* renamed from: x  reason: default package */
public final class x implements Runnable {
    private WeakReference a;

    public x(c cVar) {
        this.a = new WeakReference(cVar);
    }

    public final void run() {
        c cVar = (c) this.a.get();
        if (cVar == null) {
            d.a("The ad must be gone, so cancelling the refresh timer.");
        } else {
            cVar.u();
        }
    }
}
