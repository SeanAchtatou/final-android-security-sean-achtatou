package defpackage;

import android.webkit.WebView;
import com.google.ads.f;

/* renamed from: y  reason: default package */
final class y implements Runnable {
    private final c a;
    private final WebView b;
    private final e c;
    private final f d;
    private final boolean e;
    private /* synthetic */ f f;

    public y(f fVar, c cVar, WebView webView, e eVar, f fVar2, boolean z) {
        this.f = fVar;
        this.a = cVar;
        this.b = webView;
        this.c = eVar;
        this.d = fVar2;
        this.e = z;
    }

    public final void run() {
        this.b.stopLoading();
        this.b.destroy();
        this.c.a();
        if (this.e) {
            b h = this.a.h();
            h.stopLoading();
            h.setVisibility(8);
        }
        this.a.a(this.d);
    }
}
