package defpackage;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Handler;
import com.google.ads.AdActivity;
import com.google.ads.AdView;
import com.google.ads.b;
import com.google.ads.d;
import com.google.ads.e;
import com.google.ads.f;
import com.google.ads.g;
import com.google.ads.util.AdUtil;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.LinkedList;

/* renamed from: c  reason: default package */
public final class c {
    private static final Object a = new Object();
    private WeakReference b;
    private e c;
    private b d = null;
    private f e = null;
    private g f = null;
    private d g;
    private a h = new a();
    private String i;
    private b j;
    private n k;
    private Handler l = new Handler();
    private long m;
    private boolean n = false;
    private boolean o = false;
    private SharedPreferences p;
    private long q = 0;
    private x r;
    private boolean s = false;
    private LinkedList t;
    private LinkedList u;
    private int v;

    public c(Activity activity, e eVar, d dVar, String str) {
        this.b = new WeakReference(activity);
        this.c = eVar;
        this.g = dVar;
        this.i = str;
        synchronized (a) {
            this.p = activity.getApplicationContext().getSharedPreferences("GoogleAdMobAdsPrefs", 0);
            this.m = 60000;
        }
        this.r = new x(this);
        this.t = new LinkedList();
        this.u = new LinkedList();
        a();
        AdUtil.g(activity.getApplicationContext());
    }

    private synchronized boolean v() {
        return this.e != null;
    }

    private synchronized void w() {
        Activity activity = (Activity) this.b.get();
        if (activity == null) {
            com.google.ads.util.d.e("activity was null while trying to ping tracking URLs.");
        } else {
            Iterator it = this.t.iterator();
            while (it.hasNext()) {
                new Thread(new p((String) it.next(), activity.getApplicationContext())).start();
            }
        }
    }

    private synchronized void x() {
        Activity activity = (Activity) this.b.get();
        if (activity == null) {
            com.google.ads.util.d.e("activity was null while trying to ping click tracking URLs.");
        } else {
            Iterator it = this.u.iterator();
            while (it.hasNext()) {
                new Thread(new p((String) it.next(), activity.getApplicationContext())).start();
            }
        }
    }

    public final synchronized void a() {
        Activity d2 = d();
        if (d2 == null) {
            com.google.ads.util.d.a("activity was null while trying to create an AdWebView.");
        } else {
            this.j = new b(d2.getApplicationContext(), this.g);
            this.j.setVisibility(8);
            if (this.c instanceof AdView) {
                this.k = new n(this, g.b, true, false);
            } else {
                this.k = new n(this, g.b, true, true);
            }
            this.j.setWebViewClient(this.k);
        }
    }

    public final synchronized void a(float f2) {
        this.q = (long) (1000.0f * f2);
    }

    public final synchronized void a(int i2) {
        this.v = i2;
    }

    public final void a(long j2) {
        synchronized (a) {
            SharedPreferences.Editor edit = this.p.edit();
            edit.putLong("Timeout" + this.i, j2);
            edit.commit();
            if (this.s) {
                this.m = j2;
            }
        }
    }

    public final synchronized void a(b bVar) {
        this.d = bVar;
    }

    public final synchronized void a(f fVar) {
        this.e = null;
        if (this.c instanceof com.google.ads.c) {
            if (fVar == f.NO_FILL) {
                this.h.n();
            } else if (fVar == f.NETWORK_ERROR) {
                this.h.l();
            }
        }
        com.google.ads.util.d.c("onFailedToReceiveAd(" + fVar + ")");
        if (this.d != null) {
            this.d.a(this.c, fVar);
        }
    }

    public final synchronized void a(g gVar) {
        if (v()) {
            com.google.ads.util.d.e("loadAd called while the ad is already loading, so aborting.");
        } else if (AdActivity.c()) {
            com.google.ads.util.d.e("loadAd called while an interstitial or landing page is displayed, so aborting");
        } else {
            Activity d2 = d();
            if (d2 == null) {
                com.google.ads.util.d.e("activity is null while trying to load an ad.");
            } else if (AdUtil.c(d2.getApplicationContext()) && AdUtil.b(d2.getApplicationContext())) {
                this.n = false;
                this.t.clear();
                this.f = gVar;
                this.e = new f(this);
                this.e.a(gVar);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(Runnable runnable) {
        this.l.post(runnable);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str) {
        com.google.ads.util.d.a("Adding a tracking URL: " + str);
        this.t.add(str);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(LinkedList linkedList) {
        Iterator it = linkedList.iterator();
        while (it.hasNext()) {
            com.google.ads.util.d.a("Adding a click tracking URL: " + ((String) it.next()));
        }
        this.u = linkedList;
    }

    public final synchronized void b() {
        if (this.o) {
            com.google.ads.util.d.a("Disabling refreshing.");
            this.l.removeCallbacks(this.r);
            this.o = false;
        } else {
            com.google.ads.util.d.a("Refreshing is already disabled.");
        }
    }

    public final synchronized void c() {
        if (!(this.c instanceof AdView)) {
            com.google.ads.util.d.a("Tried to enable refreshing on something other than an AdView.");
        } else if (!this.o) {
            com.google.ads.util.d.a("Enabling refreshing every " + this.q + " milliseconds.");
            this.l.postDelayed(this.r, this.q);
            this.o = true;
        } else {
            com.google.ads.util.d.a("Refreshing is already enabled.");
        }
    }

    public final Activity d() {
        return (Activity) this.b.get();
    }

    public final e e() {
        return this.c;
    }

    public final synchronized f f() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public final String g() {
        return this.i;
    }

    public final synchronized b h() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public final synchronized n i() {
        return this.k;
    }

    public final d j() {
        return this.g;
    }

    public final a k() {
        return this.h;
    }

    public final synchronized int l() {
        return this.v;
    }

    public final long m() {
        return this.m;
    }

    public final synchronized boolean n() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void o() {
        this.e = null;
        this.n = true;
        this.j.setVisibility(0);
        this.h.c();
        if (this.c instanceof AdView) {
            w();
        }
        com.google.ads.util.d.c("onReceiveAd()");
        if (this.d != null) {
            this.d.a();
        }
    }

    public final synchronized void p() {
        this.h.o();
        com.google.ads.util.d.c("onDismissScreen()");
    }

    public final synchronized void q() {
        com.google.ads.util.d.c("onPresentScreen()");
    }

    public final synchronized void r() {
        com.google.ads.util.d.c("onLeaveApplication()");
    }

    public final void s() {
        this.h.b();
        x();
    }

    /* access modifiers changed from: package-private */
    public final synchronized boolean t() {
        return !this.u.isEmpty();
    }

    public final synchronized void u() {
        if (this.f == null) {
            com.google.ads.util.d.a("Tried to refresh before calling loadAd().");
        } else if (this.c instanceof AdView) {
            if (!((AdView) this.c).isShown() || !AdUtil.d()) {
                com.google.ads.util.d.a("Not refreshing because the ad is not visible.");
            } else {
                com.google.ads.util.d.c("Refreshing ad.");
                a(this.f);
            }
            this.l.postDelayed(this.r, this.q);
        } else {
            com.google.ads.util.d.a("Tried to refresh an ad that wasn't an AdView.");
        }
    }
}
