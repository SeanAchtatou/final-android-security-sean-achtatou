package com.scoreloop.client.android.ui.component.score;

import android.graphics.drawable.Drawable;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.k;
import com.scoreloop.client.android.ui.component.base.m;
import org.anddev.andengine.R;

public class h extends k {
    private boolean a;

    public h(ComponentActivity componentActivity, Score score, boolean z) {
        super(componentActivity, null, m.a(componentActivity, score), m.b(score, componentActivity.x()), score);
        this.a = z;
    }

    /* access modifiers changed from: protected */
    public final String i() {
        User user = ((Score) p()).getUser();
        if (user == null) {
            user = Session.getCurrentSession().getUser();
        }
        return user.getImageUrl();
    }

    /* access modifiers changed from: protected */
    public int h() {
        return R.layout.sl_list_item_score;
    }

    /* access modifiers changed from: protected */
    public final int g() {
        return R.id.sl_list_item_score_result;
    }

    /* access modifiers changed from: protected */
    public final int k() {
        return R.id.sl_list_item_score_title;
    }

    public int a() {
        return 19;
    }

    public final Drawable d() {
        return c().getResources().getDrawable(R.drawable.sl_icon_user);
    }

    public final boolean b() {
        return this.a;
    }
}
