package com.scoreloop.client.android.ui.component.base;

import com.scoreloop.client.android.core.model.Challenge;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.framework.u;

public interface e {
    u a(Game game);

    u a(Game game, Integer num, Integer num2);

    u a(User user);

    u a(User user, int i);

    u a(User user, Boolean bool);

    u b(Challenge challenge);

    u b(User user);

    u c(User user);

    u d();

    u d(User user);

    u e(User user);

    u f();

    u f(User user);

    u g();

    u h();
}
