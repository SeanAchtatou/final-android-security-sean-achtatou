package com.scoreloop.client.android.ui.component.market;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import com.scoreloop.client.android.core.controller.GamesController;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.ui.a.k;
import com.scoreloop.client.android.ui.component.base.ComponentHeaderActivity;
import com.scoreloop.client.android.ui.framework.aj;
import com.scoreloop.client.android.ui.framework.h;
import com.scoreloop.client.android.ui.framework.s;
import java.util.List;
import org.anddev.andengine.R;

public class MarketHeaderActivity extends ComponentHeaderActivity implements View.OnClickListener, aj {
    private GamesController a;

    public void onClick(View view) {
        Game game = (Game) m().a("featuredGame");
        if (game != null) {
            B();
            game.getName();
            a(A().a(game));
        }
    }

    public void onCreate(Bundle bundle) {
        super.a(bundle, (int) R.layout.sl_header_market);
        c(getString(R.string.sl_market));
        b(getString(R.string.sl_market_description));
        a().setImageResource(R.drawable.sl_header_icon_market);
        a("featuredGame", "featuredGameName", "featuredGameImageUrl", "featuredGamePublisher");
        this.a = new GamesController(E());
        this.a.setRangeLength(1);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.a.loadRangeForFeatured();
    }

    public final void a(s sVar, String str, Object obj, Object obj2) {
        if (obj == obj2) {
            return;
        }
        if (str.equals("featuredGameImageUrl")) {
            k.a((String) obj2, getResources().getDrawable(R.drawable.sl_header_icon_market), a());
        } else if (str.equals("featuredGameName")) {
            c((String) obj2);
        } else if (str.equals("featuredGamePublisher")) {
            b((String) obj2);
        }
    }

    public final void a(s sVar, String str) {
        if ("featuredGame".equals(str)) {
            m().a(str, h.NOT_DIRTY, (Object) null);
        } else if ("featuredGameImageUrl".equals(str)) {
            m().a(str, h.NOT_DIRTY, (Object) null);
        } else if ("featuredGameName".equals(str)) {
            m().a(str, h.NOT_DIRTY, (Object) null);
        } else if ("featuredGamePublisher".equals(str)) {
            m().a(str, h.NOT_DIRTY, (Object) null);
        }
    }

    public final void a(RequestController requestController) {
        List games = this.a.getGames();
        if (games.size() > 0) {
            s m = m();
            Game game = (Game) games.get(0);
            m.b("featuredGame", game);
            m.b("featuredGameName", game.getName());
            m.b("featuredGamePublisher", game.getPublisherName());
            m.b("featuredGameImageUrl", game.getImageUrl());
            ((ImageView) findViewById(R.id.sl_control_icon)).setImageResource(R.drawable.sl_button_arrow);
            findViewById(R.id.sl_header_layout).setOnClickListener(this);
        }
    }
}
