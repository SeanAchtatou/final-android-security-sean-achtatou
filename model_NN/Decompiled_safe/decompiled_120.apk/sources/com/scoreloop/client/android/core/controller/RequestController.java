package com.scoreloop.client.android.core.controller;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.Response;
import com.scoreloop.client.android.core.util.Logger;
import com.scoreloop.client.android.core.util.SetterIntent;
import org.anddev.andengine.input.touch.TouchEvent;
import org.json.JSONObject;

public abstract class RequestController {
    static final /* synthetic */ boolean b = (!RequestController.class.desiredAssertionStatus());
    protected RequestControllerObserver a;
    private boolean c = true;
    private final b d;
    private Exception e;
    /* access modifiers changed from: private */
    public Request f;
    private final Session g;
    /* access modifiers changed from: private */
    public f h;

    /* renamed from: com.scoreloop.client.android.core.controller.RequestController$1  reason: invalid class name */
    /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] a = new int[Request.State.values().length];

        static {
            try {
                a[Request.State.COMPLETED.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                a[Request.State.FAILED.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                a[Request.State.CANCELLED.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    class a extends Handler {
        private final Exception b;
        private final boolean c;
        private final RequestControllerObserver d;

        public a(RequestControllerObserver requestControllerObserver, boolean z, Exception exc) {
            this.d = requestControllerObserver;
            this.b = exc;
            this.c = z;
        }

        public void handleMessage(Message message) {
            if (this.c) {
                this.d.requestControllerDidFail(RequestController.this, this.b);
            } else {
                this.d.requestControllerDidReceiveResponse(RequestController.this);
            }
        }
    }

    class b implements RequestCompletionCallback {
        private b() {
        }

        /* synthetic */ b(RequestController requestController, AnonymousClass1 r2) {
            this();
        }

        public void a(Request request) {
            RequestController.this.c((Exception) null);
            switch (AnonymousClass1.a[request.k().ordinal()]) {
                case 1:
                    Logger.a("RequestController", "RequestCallback.onRequestCompleted: request completed: ", request);
                    try {
                        if (RequestController.this.a(request, request.j())) {
                            RequestController.this.c();
                            return;
                        }
                        return;
                    } catch (Exception e) {
                        RequestController.this.d(e);
                        return;
                    }
                case 2:
                    Logger.a("RequestController", "RequestCallback.onRequestCompleted: request failed: ", request);
                    RequestController.this.d(request.g());
                    return;
                case TouchEvent.ACTION_CANCEL /*3*/:
                    Logger.a("RequestController", "RequestCallback.onRequestCompleted: request cancelled: ", request);
                    RequestController.this.d(new RequestCancelledException());
                    return;
                default:
                    throw new IllegalStateException("onRequestCompleted called for not completed request");
            }
        }

        public void b(Request request) {
        }
    }

    class c implements RequestControllerObserver {
        private c() {
        }

        /* synthetic */ c(RequestController requestController, AnonymousClass1 r2) {
            this();
        }

        public void requestControllerDidFail(RequestController requestController, Exception exc) {
            Logger.a("RequestController", "Session authentication failed, failing _request");
            if (RequestController.this.f != null) {
                RequestController.this.f.a(exc);
            }
            f unused = RequestController.this.h = (f) null;
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            f unused = RequestController.this.h = (f) null;
        }
    }

    RequestController(Session session, RequestControllerObserver requestControllerObserver) {
        if (requestControllerObserver == null) {
            throw new IllegalArgumentException("observer parameter cannot be null");
        }
        if (session == null) {
            this.g = Session.getCurrentSession();
        } else {
            this.g = session;
        }
        if (b || this.g != null) {
            this.a = requestControllerObserver;
            this.d = new b(this, null);
            d();
            return;
        }
        throw new AssertionError();
    }

    static Integer a(JSONObject jSONObject) {
        SetterIntent setterIntent = new SetterIntent();
        if (setterIntent.f(jSONObject, "error", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            return setterIntent.a((JSONObject) setterIntent.a(), "code", SetterIntent.KeyMode.USE_NULL_WHEN_NO_KEY, SetterIntent.ValueMode.ALLOWS_NULL_VALUE);
        }
        return null;
    }

    private Request b() {
        return this.f;
    }

    /* access modifiers changed from: private */
    public void c() {
        Logger.a("RequestController.invokeDidReceiveResponse", " observer = ", this.a);
        this.a.requestControllerDidReceiveResponse(this);
    }

    private void c(Request request) {
        this.f = request;
        if (!this.c) {
            request.a(0);
        }
    }

    /* access modifiers changed from: private */
    public void d(Exception exc) {
        Logger.a("onRequestCompleted", "failed with exception: ", exc);
        c(exc);
        this.a.requestControllerDidFail(this, exc);
    }

    /* access modifiers changed from: package-private */
    public void a(Request request) {
        c(request);
        this.g.b().a(request);
    }

    /* access modifiers changed from: package-private */
    public void a(Exception exc) {
        Request b2 = b();
        if (b2 != null) {
            b2.a(exc);
            d(exc);
        }
    }

    /* access modifiers changed from: package-private */
    public abstract boolean a(Request request, Response response);

    /* access modifiers changed from: package-private */
    public void a_() {
        c((Exception) null);
        if (this.f != null) {
            if (!this.f.m()) {
                this.g.b().b(this.f);
            }
            this.f = null;
        }
    }

    /* access modifiers changed from: package-private */
    public void b(Request request) {
        if (Thread.currentThread() != Looper.getMainLooper().getThread()) {
            throw new IllegalStateException("you are not calling from the main thread context");
        }
        e();
        c(request);
        this.g.e();
        this.g.b().a(request);
    }

    /* access modifiers changed from: protected */
    public void b(Exception exc) {
        Logger.a("RequestController.invokeDelayedDidReceiveResponse", " observer = ", this.a);
        new a(this.a, true, exc).obtainMessage().sendToTarget();
    }

    /* access modifiers changed from: protected */
    public void c(Exception exc) {
        this.e = exc;
    }

    /* access modifiers changed from: package-private */
    public void d() {
        if (getGame() == null) {
            throw new IllegalStateException("we do not allow game id to be null at all, please initialize Client with valid game id and secret");
        }
    }

    /* access modifiers changed from: package-private */
    public void e() {
        Session.State c2 = h().c();
        if (c2 != Session.State.AUTHENTICATED && c2 != Session.State.AUTHENTICATING) {
            if (this.h == null) {
                this.h = new f(h(), new c(this, null));
            }
            this.h.b();
        }
    }

    /* access modifiers changed from: package-private */
    public RequestControllerObserver f() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public b g() {
        return this.d;
    }

    public Exception getError() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public Game getGame() {
        return h().getGame();
    }

    /* access modifiers changed from: package-private */
    public final Session h() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public final User i() {
        return h().getUser();
    }

    public boolean isCachedResponseUsed() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public void j() {
        Logger.a("RequestController.invokeDelayedDidReceiveResponse", " observer = ", this.a);
        new a(this.a, false, null).obtainMessage().sendToTarget();
    }

    public void setCachedResponseUsed(boolean z) {
        this.c = z;
    }
}
