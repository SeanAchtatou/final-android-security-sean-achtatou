package com.scoreloop.client.android.ui.a;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import java.io.File;
import java.io.FileNotFoundException;

public final class d {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap a(Uri uri, ContentResolver contentResolver, String str) {
        int i;
        System.gc();
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(contentResolver.openInputStream(uri), null, options);
            if (options.outHeight > 288 || options.outWidth > 288) {
                i = (int) Math.pow(2.0d, (double) ((int) Math.round(Math.log(288.0d / ((double) Math.max(options.outHeight, options.outWidth))) / Math.log(0.5d))));
            } else {
                i = 1;
            }
            BitmapFactory.Options options2 = new BitmapFactory.Options();
            options2.inSampleSize = i;
            Bitmap decodeStream = BitmapFactory.decodeStream(contentResolver.openInputStream(uri), null, options2);
            int width = decodeStream.getWidth();
            int height = decodeStream.getHeight();
            int min = Math.min(width, height);
            int i2 = (width - min) / 2;
            int i3 = (height - min) / 2;
            float f = 144.0f / ((float) min);
            Matrix matrix = new Matrix();
            matrix.postScale(f, f);
            if (str != null && (str.equals("90") || str.equals("180") || str.equals("270"))) {
                matrix.postRotate((float) Integer.valueOf(str).intValue());
            }
            Bitmap createBitmap = Bitmap.createBitmap(decodeStream, i2, i3, width - (i2 * 2), height - (i3 * 2), matrix, true);
            decodeStream.recycle();
            return createBitmap;
        } catch (FileNotFoundException e) {
            throw new RuntimeException("unhandled checked exception", e);
        }
    }

    public static String a(Uri uri, ContentResolver contentResolver, Context context) {
        int i;
        String str = "0";
        try {
            i = Integer.valueOf(Build.VERSION.SDK).intValue();
        } catch (Exception e) {
            i = 3;
        }
        if (i >= 5 && a.a()) {
            File file = null;
            try {
                file = a.a(context, uri.toString(), contentResolver.openInputStream(uri));
                Class<?> cls = Class.forName("android.media.ExifInterface");
                Object newInstance = cls.getConstructor(String.class).newInstance(file.getAbsolutePath());
                String str2 = (String) cls.getMethod("getAttribute", String.class).invoke(newInstance, "Orientation");
                if (str2 != null) {
                    if (str2.equals("1")) {
                        str = "0";
                    } else if (str2.equals("3")) {
                        str = "180";
                    } else if (str2.equals("6")) {
                        str = "90";
                    } else if (str2.equals("8")) {
                        str = "270";
                    }
                }
                if (file != null && file.exists()) {
                    file.delete();
                }
            } catch (Exception e2) {
                if (file != null && file.exists()) {
                    file.delete();
                }
            } catch (Throwable th) {
                if (file != null && file.exists()) {
                    file.delete();
                }
                throw th;
            }
        }
        return str;
    }
}
