package com.scoreloop.client.android.ui.component.base;

import android.app.Dialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.scoreloop.client.android.core.controller.RequestCancelledException;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.framework.BaseActivity;
import com.scoreloop.client.android.ui.framework.s;
import org.anddev.andengine.R;

public abstract class ComponentActivity extends BaseActivity {
    private RequestControllerObserver a;

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        switch (i) {
            case 0:
                Dialog dialog = new Dialog(getParent() != null ? getParent() : this);
                dialog.getWindow().requestFeature(1);
                View inflate = getLayoutInflater().inflate((int) R.layout.sl_dialog_custom, (ViewGroup) null);
                dialog.setContentView(inflate);
                dialog.setCanceledOnTouchOutside(true);
                ((TextView) inflate.findViewById(R.id.message)).setText(getString(R.string.sl_error_message_network));
                dialog.setOnDismissListener(this);
                return dialog;
            default:
                return super.onCreateDialog(i);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.component.base.ComponentActivity.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.scoreloop.client.android.ui.component.base.ComponentActivity.a(com.scoreloop.client.android.core.controller.RequestController, java.lang.Exception):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(android.content.Context, java.lang.String):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(int, com.scoreloop.client.android.ui.framework.y):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.framework.aj.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.component.base.ComponentActivity.a(int, boolean):void */
    /* access modifiers changed from: protected */
    public final void w() {
        a(0, true);
    }

    public static boolean a(String str, String str2, Object obj, Object obj2) {
        return str.equals(str2) && obj2 != null && !obj2.equals(obj);
    }

    public final a x() {
        return (a) m().a("configuration");
    }

    public final s y() {
        return (s) m().a("userValues");
    }

    private s a() {
        return (s) m().a("gameValues");
    }

    public final s z() {
        return (s) m().a("sessionUserValues");
    }

    public final e A() {
        return (e) m().a("factory");
    }

    public final p B() {
        return (p) m().a("tracker");
    }

    public final Game C() {
        if (a() != null) {
            return (Game) a().a("game");
        }
        return null;
    }

    public final b D() {
        return (b) m().a("manager");
    }

    public final int d(int i) {
        Game C = C();
        return (C.hasModes() ? C.getMinMode().intValue() : 0) + i;
    }

    public final String e(int i) {
        if (!C().hasModes()) {
            return "";
        }
        if (x().a() != 0) {
            return getResources().getStringArray(x().a())[f(i)].toString();
        }
        return x().b()[f(i)];
    }

    private int f(int i) {
        Game C = C();
        if (C.hasModes()) {
            return i - C.getMinMode().intValue();
        }
        return -1;
    }

    /* access modifiers changed from: protected */
    public final RequestControllerObserver E() {
        if (this.a == null) {
            this.a = new g(this);
        }
        return this.a;
    }

    public final User F() {
        return (User) y().a("user");
    }

    public final boolean G() {
        User F = F();
        if (F != null) {
            return Session.getCurrentSession().isOwnedByUser(F);
        }
        return false;
    }

    public final void requestControllerDidFail(RequestController requestController, Exception exc) {
        if (!(exc instanceof RequestCancelledException)) {
            a((Object) requestController);
            if (!o()) {
                a(requestController, exc);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.component.base.ComponentActivity.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.scoreloop.client.android.ui.component.base.ComponentActivity.a(com.scoreloop.client.android.core.controller.RequestController, java.lang.Exception):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(android.content.Context, java.lang.String):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(int, com.scoreloop.client.android.ui.framework.y):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.framework.aj.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.component.base.ComponentActivity.a(int, boolean):void */
    /* access modifiers changed from: protected */
    public void a(RequestController requestController, Exception exc) {
        a(0, true);
    }

    public void requestControllerDidReceiveResponse(RequestController requestController) {
        a((Object) requestController);
        if (!o()) {
            a(requestController);
        } else {
            q();
        }
    }

    public void a(RequestController requestController) {
    }

    /* access modifiers changed from: protected */
    public final void a(int i, boolean z) {
        super.a(i, z);
        if (!o()) {
            B();
            String.format("dialog.%s", Integer.valueOf(i));
        }
    }
}
