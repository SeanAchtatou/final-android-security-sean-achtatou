package com.scoreloop.client.android.ui.framework;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.anddev.andengine.R;

public abstract class BaseActivity extends Activity implements DialogInterface.OnDismissListener, aj, f, Runnable {
    private static boolean a = false;
    private View b;
    private s c;
    private Handler d;
    private boolean e;
    private boolean f;
    private Set g = null;
    private int h;
    private s i = null;
    private final Set j = new HashSet();
    private int k;
    private View l;
    private int m = -1;

    public static void a(Context context, String str) {
        a(context, str, 0);
    }

    public static void a(Context context, String str, int i2) {
        View inflate = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.sl_dialog_toast, (ViewGroup) null);
        ((TextView) inflate.findViewById(R.id.message)).setText(str);
        Toast toast = new Toast(context.getApplicationContext());
        toast.setDuration(i2);
        toast.setView(inflate);
        toast.show();
    }

    public void onDismiss(DialogInterface dialogInterface) {
        this.m = -1;
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("bundle_key_visible_dialog_id", this.m);
    }

    public final void a(String... strArr) {
        c();
        if (this.g == null) {
            this.g = new HashSet();
        }
        Collections.addAll(this.g, strArr);
        if (!this.e) {
            b();
        }
    }

    public static void a(u uVar) {
        am.a().a(uVar);
    }

    /* access modifiers changed from: protected */
    public final void i() {
        if (!this.e) {
            am.a().a();
        }
    }

    protected static void j() {
        am.a().b();
    }

    public final s k() {
        return am.a().a(getIntent().getStringExtra("activityIdentifier")).a();
    }

    /* access modifiers changed from: protected */
    public final Handler l() {
        return this.d;
    }

    public final s m() {
        if (this.c == null) {
            this.c = am.a().d().c();
        }
        return this.c;
    }

    private FrameLayout a() {
        for (ViewParent parent = this.b.getParent(); parent != null; parent = parent.getParent()) {
            if (parent instanceof FrameLayout) {
                return (FrameLayout) parent;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final void n() {
        if (this.k == 0) {
            throw new IllegalStateException("spinner not shown you want to hide");
        }
        int i2 = this.k - 1;
        this.k = i2;
        if (i2 == 0) {
            a(false);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj) {
        if (this.j.contains(obj)) {
            this.j.remove(obj);
            n();
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(ah ahVar) {
        return true;
    }

    /* access modifiers changed from: protected */
    public final boolean o() {
        return this.e;
    }

    private void a(String str) {
        if (a) {
            String str2 = "";
            String obj = toString();
            int lastIndexOf = obj.lastIndexOf("@");
            if (lastIndexOf != -1) {
                str2 = obj.substring(lastIndexOf);
            }
            Log.d("ScoreloopUI", "              > " + getClass().getSimpleName() + "(" + str2 + ") " + str);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.aj):void
     arg types: [java.lang.String, com.scoreloop.client.android.ui.framework.BaseActivity]
     candidates:
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, boolean):com.scoreloop.client.android.ui.framework.g
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.j):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, java.lang.Object):java.lang.Object
      com.scoreloop.client.android.ui.framework.s.a(com.scoreloop.client.android.ui.framework.s, java.util.Set):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.aj):void */
    private void b() {
        if (this.g != null) {
            s m2 = m();
            for (String a2 : this.g) {
                m2.a(a2, (aj) this);
            }
        }
    }

    public void onBackPressed() {
        i();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.d = new Handler();
        a("onCreate");
        this.e = true;
        if (bundle != null) {
            this.m = bundle.getInt("bundle_key_visible_dialog_id");
            if (this.m != -1) {
                showDialog(this.m);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        a("onDestroy");
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4 || keyEvent.getRepeatCount() != 0) {
            return super.onKeyDown(i2, keyEvent);
        }
        i();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        a("onPause");
        this.e = true;
        c();
        if (this.g != null) {
            if (this.i == null) {
                this.i = new s();
            }
            this.i.a(this.c, this.g);
        }
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int i2, Dialog dialog) {
        super.onPrepareDialog(i2, dialog);
    }

    public void a(int i2) {
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        a("onRestart");
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        a("onResume");
        this.e = false;
        b();
        this.c = null;
        s m2 = m();
        if (this.g != null) {
            m2.a(this.i, this.g, this);
        }
        p();
    }

    /* access modifiers changed from: protected */
    public void a(boolean z) {
        if (this.b != null) {
            if (z) {
                this.l = getLayoutInflater().inflate((int) R.layout.sl_spinner_view, (ViewGroup) null);
                a().addView(this.l);
                return;
            }
            a().removeView(this.l);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        a("onStart");
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        a("onStop");
    }

    public void a(s sVar, String str, Object obj, Object obj2) {
    }

    public void a(s sVar, String str) {
    }

    /* access modifiers changed from: protected */
    public final void p() {
        if (!this.e && this.f) {
            this.f = false;
            int i2 = this.h;
            this.h = 0;
            a(i2);
        }
    }

    public void run() {
        p();
    }

    /* access modifiers changed from: protected */
    public final void b(int i2) {
        View inflate = getLayoutInflater().inflate(i2, (ViewGroup) null);
        setContentView(inflate);
        this.b = inflate;
    }

    /* access modifiers changed from: protected */
    public final void q() {
        a(0, y.MERGE);
    }

    /* access modifiers changed from: protected */
    public final void a(int i2, y yVar) {
        this.f = true;
        if (yVar == y.MERGE) {
            this.h |= i2;
        } else {
            this.h = i2;
        }
        this.d.post(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.framework.BaseActivity.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.scoreloop.client.android.ui.framework.BaseActivity.a(android.content.Context, java.lang.String):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(int, com.scoreloop.client.android.ui.framework.y):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.framework.aj.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(int, boolean):void */
    /* access modifiers changed from: protected */
    public final void c(int i2) {
        a(i2, false);
    }

    /* access modifiers changed from: protected */
    public void a(int i2, boolean z) {
        if (!this.e) {
            if (z) {
                this.m = i2;
            } else {
                this.m = -1;
            }
            showDialog(i2);
        }
    }

    /* access modifiers changed from: protected */
    public final void r() {
        int i2 = this.k;
        this.k = i2 + 1;
        if (i2 == 0) {
            a(true);
        }
    }

    /* access modifiers changed from: protected */
    public final void b(Object obj) {
        if (!this.j.contains(obj)) {
            this.j.add(obj);
            r();
        }
    }

    public final void d(String str) {
        a(this, str, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.framework.s.b(java.lang.String, com.scoreloop.client.android.ui.framework.aj):void
     arg types: [java.lang.String, com.scoreloop.client.android.ui.framework.BaseActivity]
     candidates:
      com.scoreloop.client.android.ui.framework.s.b(java.lang.String, java.lang.Object):void
      com.scoreloop.client.android.ui.framework.s.b(java.lang.String, com.scoreloop.client.android.ui.framework.aj):void */
    private void c() {
        if (this.g != null) {
            s m2 = m();
            for (String b2 : this.g) {
                m2.b(b2, (aj) this);
            }
        }
    }

    public final boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    public final boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    public final boolean onOptionsItemSelected(MenuItem menuItem) {
        return super.onOptionsItemSelected(menuItem);
    }

    public boolean a(Menu menu) {
        return true;
    }

    public boolean b(Menu menu) {
        return true;
    }

    public boolean a(MenuItem menuItem) {
        return false;
    }
}
