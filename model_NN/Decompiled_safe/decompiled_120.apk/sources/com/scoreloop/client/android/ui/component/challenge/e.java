package com.scoreloop.client.android.ui.component.challenge;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.scoreloop.client.android.core.model.Money;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.m;
import java.util.List;
import org.anddev.andengine.R;

final class e extends q {
    private static List a = Session.getCurrentSession().getChallengeStakes();
    /* access modifiers changed from: private */
    public int b;
    /* access modifiers changed from: private */
    public int c;

    public e(ComponentActivity componentActivity) {
        super(componentActivity, null);
    }

    /* access modifiers changed from: package-private */
    public final Integer g() {
        if (i().C().hasModes()) {
            return Integer.valueOf(i().d(this.b));
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final Money h() {
        Money money = (Money) i().y().a("userBalance");
        if (money == null) {
            return null;
        }
        Money money2 = (Money) a.get(this.c);
        if (money2.compareTo(money) <= 0) {
            return money2;
        }
        return null;
    }

    public final int a() {
        return 9;
    }

    public final View a(View view) {
        View view2;
        if (view == null) {
            view2 = e().inflate((int) R.layout.sl_list_item_challenge_settings_edit, (ViewGroup) null);
        } else {
            view2 = view;
        }
        b(view2);
        return view2;
    }

    public final boolean b() {
        return false;
    }

    /* access modifiers changed from: protected */
    public final void b(View view) {
        a((TextView) view.findViewById(R.id.stake_text));
        Spinner spinner = (Spinner) view.findViewById(R.id.mode_selector);
        if (i().C().hasModes()) {
            spinner.setVisibility(0);
            ArrayAdapter<CharSequence> createFromResource = i().x().a() != 0 ? ArrayAdapter.createFromResource(c(), i().x().a(), R.layout.sl_spinner_item) : new ArrayAdapter<>(c(), (int) R.layout.sl_spinner_item, i().x().b());
            createFromResource.setDropDownViewResource(17367049);
            spinner.setAdapter((SpinnerAdapter) createFromResource);
            spinner.setSelection(this.b);
            spinner.setOnItemSelectedListener(new l(this));
        } else {
            spinner.setVisibility(8);
        }
        SeekBar seekBar = (SeekBar) view.findViewById(R.id.stake_selector);
        seekBar.setMax(a.size() - 1);
        seekBar.setProgress(this.c);
        seekBar.setOnSeekBarChangeListener(new k(this));
        f fVar = new f(this);
        fVar.a = (TextView) view.findViewById(R.id.stake_text);
        seekBar.setTag(fVar);
    }

    /* access modifiers changed from: private */
    public void a(TextView textView) {
        String string;
        Money h = h();
        if (h != null) {
            string = m.a(h, i().x());
        } else {
            string = c().getResources().getString(R.string.sl_balance_too_low);
        }
        textView.setText(string);
    }
}
