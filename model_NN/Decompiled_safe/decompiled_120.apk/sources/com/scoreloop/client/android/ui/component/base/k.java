package com.scoreloop.client.android.ui.component.base;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.scoreloop.client.android.ui.framework.a;
import org.anddev.andengine.R;

public class k extends a {
    private f a;
    private String b;
    private String c;
    private final Object d;

    public k(ComponentActivity componentActivity, Drawable drawable, String str, String str2, Object obj) {
        super(componentActivity, drawable, str);
        this.b = str2;
        this.d = obj;
    }

    public k(ComponentActivity componentActivity, Object obj) {
        super(componentActivity, null, null);
        this.d = obj;
    }

    /* access modifiers changed from: protected */
    public f l() {
        return new f();
    }

    /* access modifiers changed from: protected */
    public void a(View view, f fVar) {
        int j = j();
        if (j != 0) {
            fVar.a = (ImageView) view.findViewById(j);
        }
        fVar.b = (TextView) view.findViewById(k());
        int g = g();
        if (g != 0) {
            fVar.c = (TextView) view.findViewById(g);
        }
        int o = o();
        if (o != 0) {
            fVar.d = (TextView) view.findViewById(o);
        }
    }

    public final ComponentActivity n() {
        return (ComponentActivity) c();
    }

    /* access modifiers changed from: protected */
    public int j() {
        return R.id.sl_icon;
    }

    /* access modifiers changed from: protected */
    public String i() {
        return null;
    }

    /* access modifiers changed from: protected */
    public int h() {
        return R.layout.sl_list_item_icon_title_subtitle;
    }

    public final void b(String str) {
        this.c = str;
    }

    /* access modifiers changed from: protected */
    public int g() {
        return R.id.sl_subtitle;
    }

    /* access modifiers changed from: protected */
    public int o() {
        return 0;
    }

    public final Object p() {
        return this.d;
    }

    /* access modifiers changed from: protected */
    public int k() {
        return R.id.sl_title;
    }

    public int a() {
        return 23;
    }

    public final View a(View view) {
        View view2;
        if (view == null) {
            view2 = e().inflate(h(), (ViewGroup) null);
            this.a = l();
            a(view2, this.a);
            view2.setTag(this.a);
        } else {
            this.a = (f) view.getTag();
            view2 = view;
        }
        a(this.a);
        return view2;
    }

    public boolean b() {
        return true;
    }

    public final void c(String str) {
        this.b = str;
    }

    /* access modifiers changed from: protected */
    public void a(f fVar) {
        String i = i();
        if (i != null) {
            com.scoreloop.client.android.ui.a.k.a(i, c().getResources().getDrawable(R.drawable.sl_icon_games_loading), fVar.a);
        } else {
            Drawable d2 = d();
            if (d2 != null) {
                fVar.a.setImageDrawable(d2);
            }
        }
        fVar.b.setText(f());
        TextView textView = fVar.c;
        if (textView != null) {
            textView.setText(this.b);
        }
        TextView textView2 = fVar.d;
        if (textView2 != null) {
            textView2.setText(this.c);
        }
    }
}
