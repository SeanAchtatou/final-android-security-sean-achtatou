package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Activity;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.anddev.andengine.input.touch.TouchEvent;
import org.json.JSONArray;

public class ActivitiesController extends RequestController {
    private List c;

    /* renamed from: com.scoreloop.client.android.core.controller.ActivitiesController$1  reason: invalid class name */
    /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] a = new int[b.values().length];

        static {
            try {
                a[b.BUDDY.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                a[b.GAME.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                a[b.USER.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                a[b.COMMUNITY.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
        }
    }

    class a extends Request {
        private final Game a;
        private final b b;
        private final User c;

        public a(RequestCompletionCallback requestCompletionCallback, Game game, User user, b bVar) {
            super(requestCompletionCallback);
            this.c = user;
            this.b = bVar;
            this.a = game;
        }

        public String a() {
            switch (AnonymousClass1.a[this.b.ordinal()]) {
                case 1:
                    if (this.c == null) {
                        throw new IllegalStateException("internal error: no _user set");
                    }
                    return String.format("/service/users/%s/buddies/last_activities", this.c.getIdentifier());
                case 2:
                    if (this.a == null) {
                        throw new IllegalStateException("internal error: no _game set");
                    }
                    return String.format("/service/games/%s/activities", this.a.getIdentifier());
                case TouchEvent.ACTION_CANCEL /*3*/:
                    if (this.c == null) {
                        throw new IllegalStateException("internal error: no _user set");
                    }
                    return String.format("/service/users/%s/activities", this.c.getIdentifier());
                case 4:
                    return "/service/activities";
                default:
                    return null;
            }
        }

        public RequestMethod c() {
            return RequestMethod.GET;
        }
    }

    enum b {
        BUDDY,
        COMMUNITY,
        GAME,
        USER
    }

    public ActivitiesController(RequestControllerObserver requestControllerObserver) {
        this(null, requestControllerObserver);
    }

    public ActivitiesController(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
        this.c = Collections.emptyList();
    }

    private void a(List list) {
        this.c = Collections.unmodifiableList(list);
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) {
        if (response.f() == 200) {
            ArrayList arrayList = new ArrayList();
            JSONArray d = response.d();
            Session h = h();
            for (int i = 0; i < d.length(); i++) {
                arrayList.add(new Activity(h, d.getJSONObject(i).getJSONObject(Activity.a)));
            }
            a(arrayList);
            return true;
        }
        throw new IllegalStateException("invalid response status: " + response.f());
    }

    public List getActivities() {
        return this.c;
    }

    public void loadActivitiesForGame(Game game) {
        if (game == null || game.getIdentifier() == null) {
            throw new IllegalArgumentException("invalid game argument");
        }
        a_();
        a aVar = new a(g(), game, null, b.GAME);
        aVar.a(60000);
        b(aVar);
    }

    public void loadActivitiesForUser(User user) {
        if (user == null) {
            throw new IllegalArgumentException("invalid user argument");
        }
        a_();
        a aVar = new a(g(), null, user, b.USER);
        aVar.a(60000);
        b(aVar);
    }

    public void loadBuddyActivities() {
        a_();
        a aVar = new a(g(), null, i(), b.BUDDY);
        aVar.a(60000);
        b(aVar);
    }

    public void loadCommunityActivities() {
        a_();
        a aVar = new a(g(), null, null, b.COMMUNITY);
        aVar.a(60000);
        b(aVar);
    }

    public void loadGameActivities() {
        if (getGame() == null) {
            throw new IllegalArgumentException("using loadGameActivities does not make sense without gameID being set on AcitiviesController instance");
        }
        a_();
        a aVar = new a(g(), getGame(), null, b.GAME);
        aVar.a(60000);
        b(aVar);
    }
}
