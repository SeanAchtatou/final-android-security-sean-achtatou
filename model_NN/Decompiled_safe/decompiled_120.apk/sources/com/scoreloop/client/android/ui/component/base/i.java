package com.scoreloop.client.android.ui.component.base;

import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;

final class i implements RequestControllerObserver {
    private /* synthetic */ a a;

    i(a aVar) {
        this.a = aVar;
    }

    public final void requestControllerDidFail(RequestController requestController, Exception exc) {
    }

    public final void requestControllerDidReceiveResponse(RequestController requestController) {
    }
}
