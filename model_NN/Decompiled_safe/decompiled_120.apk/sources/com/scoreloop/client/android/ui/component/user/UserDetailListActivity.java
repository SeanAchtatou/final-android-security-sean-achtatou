package com.scoreloop.client.android.ui.component.user;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.ListAdapter;
import com.scoreloop.client.android.core.controller.MessageController;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.base.ComponentListActivity;
import com.scoreloop.client.android.ui.component.base.e;
import com.scoreloop.client.android.ui.component.base.k;
import com.scoreloop.client.android.ui.component.base.m;
import com.scoreloop.client.android.ui.component.base.n;
import com.scoreloop.client.android.ui.component.base.o;
import com.scoreloop.client.android.ui.framework.a;
import com.scoreloop.client.android.ui.framework.ag;
import com.scoreloop.client.android.ui.framework.ai;
import com.scoreloop.client.android.ui.framework.h;
import com.scoreloop.client.android.ui.framework.i;
import com.scoreloop.client.android.ui.framework.s;
import com.scoreloop.client.android.ui.framework.w;
import org.anddev.andengine.R;
import org.anddev.andengine.input.touch.TouchEvent;

public class UserDetailListActivity extends ComponentListActivity implements ag {
    private static /* synthetic */ int[] g;
    private c a;
    private c b;
    private a c;
    private d d = d.UNKNOWN;
    private c e;
    private a f;

    private static /* synthetic */ int[] g() {
        int[] iArr = g;
        if (iArr == null) {
            iArr = new int[d.values().length];
            try {
                iArr[d.HIDE.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[d.RECOMMEND.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[d.SHOW.ordinal()] = 3;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[d.UNKNOWN.ordinal()] = 4;
            } catch (NoSuchFieldError e5) {
            }
            g = iArr;
        }
        return iArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.component.base.m.a(android.content.Context, com.scoreloop.client.android.ui.framework.s, boolean):java.lang.String
     arg types: [com.scoreloop.client.android.ui.component.user.UserDetailListActivity, com.scoreloop.client.android.ui.framework.s, int]
     candidates:
      com.scoreloop.client.android.ui.component.base.m.a(android.content.Context, com.scoreloop.client.android.ui.framework.s, com.scoreloop.client.android.ui.component.base.a):java.lang.String
      com.scoreloop.client.android.ui.component.base.m.a(android.content.Context, com.scoreloop.client.android.ui.framework.s, boolean):java.lang.String */
    private c a() {
        if (this.a == null) {
            this.a = new c(this, getResources().getDrawable(R.drawable.sl_icon_achievements), getString(R.string.sl_achievements), m.a((Context) this, y(), false));
        }
        return this.a;
    }

    private c b() {
        if (this.b == null) {
            this.b = new c(this, getResources().getDrawable(R.drawable.sl_icon_friends), getString(R.string.sl_friends), m.a(this, y()));
        }
        return this.b;
    }

    private a c() {
        if (this.c == null) {
            this.c = new k(this, getResources().getDrawable(R.drawable.sl_icon_challenges), getString(R.string.sl_format_challenges_title), getString(R.string.sl_format_challenges_subtitle, new Object[]{F().getDisplayName()}), null);
        }
        return this.c;
    }

    private n d() {
        return new n(this, C().getName());
    }

    private c e() {
        if (this.e == null) {
            this.e = new c(this, getResources().getDrawable(R.drawable.sl_icon_games), getString(R.string.sl_games), m.c(this, y()));
        }
        return this.e;
    }

    private a f() {
        if (C() != null && this.f == null) {
            User F = F();
            Game C = C();
            this.f = new k(this, getResources().getDrawable(R.drawable.sl_icon_recommend), String.format(getString(R.string.sl_format_recommend_title), C.getName()), String.format(getString(R.string.sl_format_recommend_subtitle), F.getDisplayName()), null);
        }
        return this.f;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a((ListAdapter) new i(this));
        a(s.a("userValues", "numberBuddies"), s.a("userValues", "numberGames"), s.a("userValues", "numberAchievements"));
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        q();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.component.base.ComponentActivity.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.scoreloop.client.android.ui.component.user.UserDetailListActivity.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.component.user.UserDetailListActivity.a(com.scoreloop.client.android.ui.framework.w, int):void
      com.scoreloop.client.android.ui.component.base.ComponentActivity.a(com.scoreloop.client.android.core.controller.RequestController, java.lang.Exception):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(android.content.Context, java.lang.String):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(int, com.scoreloop.client.android.ui.framework.y):void
      com.scoreloop.client.android.ui.framework.BaseActivity.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.framework.aj.a(com.scoreloop.client.android.ui.framework.s, java.lang.String):void
      com.scoreloop.client.android.ui.framework.ag.a(com.scoreloop.client.android.ui.framework.w, int):void
      com.scoreloop.client.android.ui.component.base.ComponentActivity.a(int, boolean):void */
    public final void a(a aVar) {
        User F = F();
        e A = A();
        if (aVar == f()) {
            a(11, true);
        } else if (aVar == a()) {
            a(A.a(F));
        } else if (aVar == c()) {
            a(A.b(F));
        } else if (aVar == b()) {
            a(A.f(F));
        } else if (aVar == e()) {
            a(A.a(F, 0));
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        switch (i) {
            case 11:
                ai aiVar = new ai(this);
                aiVar.a((ag) this);
                aiVar.c(getResources().getString(R.string.sl_leave_accept_game_recommendation_ok));
                aiVar.setCancelable(true);
                aiVar.setOnDismissListener(this);
                return aiVar;
            default:
                return super.onCreateDialog(i);
        }
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int i, Dialog dialog) {
        switch (i) {
            case 11:
                ai aiVar = (ai) dialog;
                aiVar.b(getResources().getString(R.string.sl_leave_accept_game_recommendation, C().getName(), F().getDisplayName()));
                return;
            default:
                super.onPrepareDialog(i, dialog);
                return;
        }
    }

    public final void a(w wVar, int i) {
        wVar.dismiss();
        if (i == 0) {
            MessageController messageController = new MessageController(E());
            messageController.setTarget(C());
            messageController.setMessageType(MessageController.TYPE_RECOMMENDATION);
            messageController.addReceiverWithUsers(MessageController.RECEIVER_USER, F());
            if (messageController.isSubmitAllowed()) {
                b(messageController);
                messageController.submitMessage();
            }
        }
    }

    public final void a(int i) {
        if (this.d == d.UNKNOWN) {
            Game C = C();
            if (C != null ? C.equals(Session.getCurrentSession().getGame()) : false) {
                Boolean bool = (Boolean) k().a("userPlaysSessionGame");
                if (bool == null) {
                    this.d = d.UNKNOWN;
                } else if (bool.booleanValue()) {
                    this.d = d.SHOW;
                } else {
                    this.d = d.RECOMMEND;
                }
            } else {
                this.d = d.HIDE;
            }
            i t = t();
            t.clear();
            x();
            switch (g()[this.d.ordinal()]) {
                case 2:
                    t.add(d());
                    t.add(f());
                    break;
                case TouchEvent.ACTION_CANCEL /*3*/:
                    boolean a2 = com.scoreloop.client.android.ui.component.base.a.a(o.ACHIEVEMENT);
                    boolean a3 = com.scoreloop.client.android.ui.component.base.a.a(o.CHALLENGE);
                    if (a2 || a3) {
                        t.add(d());
                        if (a2) {
                            t.add(a());
                        }
                        if (a3) {
                            t.add(c());
                            break;
                        }
                    }
                    break;
            }
            t.add(new n(this, getString(R.string.sl_community)));
            t.add(b());
            t.add(e());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.component.base.m.a(android.content.Context, com.scoreloop.client.android.ui.framework.s, boolean):java.lang.String
     arg types: [com.scoreloop.client.android.ui.component.user.UserDetailListActivity, com.scoreloop.client.android.ui.framework.s, int]
     candidates:
      com.scoreloop.client.android.ui.component.base.m.a(android.content.Context, com.scoreloop.client.android.ui.framework.s, com.scoreloop.client.android.ui.component.base.a):java.lang.String
      com.scoreloop.client.android.ui.component.base.m.a(android.content.Context, com.scoreloop.client.android.ui.framework.s, boolean):java.lang.String */
    public final void a(s sVar, String str, Object obj, Object obj2) {
        if (a(str, "numberBuddies", obj, obj2)) {
            b().c(m.a(this, y()));
            t().notifyDataSetChanged();
        } else if (a(str, "numberGames", obj, obj2)) {
            e().c(m.c(this, y()));
            t().notifyDataSetChanged();
        } else if (a(str, "numberAchievements", obj, obj2)) {
            a().c(m.a((Context) this, y(), false));
            t().notifyDataSetChanged();
        }
    }

    public final void a(s sVar, String str) {
        if ("numberBuddies".equals(str)) {
            y().a(str, h.NOT_DIRTY, (Object) null);
        }
        if ("numberGames".equals(str)) {
            y().a(str, h.NOT_DIRTY, (Object) null);
        }
        x();
        if (com.scoreloop.client.android.ui.component.base.a.a(o.ACHIEVEMENT) && "numberAchievements".equals(str)) {
            y().a(str, h.NOT_DIRTY, (Object) null);
        }
    }

    public final void a(RequestController requestController) {
        d(getString(R.string.sl_recommend_sent));
    }
}
