package com.scoreloop.client.android.ui.component.challenge;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.ui.component.base.ComponentHeaderActivity;
import com.scoreloop.client.android.ui.component.base.m;
import com.scoreloop.client.android.ui.framework.h;
import com.scoreloop.client.android.ui.framework.s;
import org.anddev.andengine.R;

public class ChallengeHeaderActivity extends ComponentHeaderActivity implements View.OnClickListener {
    public void onClick(View view) {
        if (Session.getCurrentSession().isAuthenticated()) {
            a(A().d());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.framework.s.a(java.lang.String, java.lang.Object):java.lang.Object
     arg types: [java.lang.String, int]
     candidates:
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, boolean):com.scoreloop.client.android.ui.framework.g
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.j):void
      com.scoreloop.client.android.ui.framework.s.a(com.scoreloop.client.android.ui.framework.s, java.util.Set):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.aj):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, java.lang.Object):java.lang.Object */
    public void onCreate(Bundle bundle) {
        super.a(bundle, (int) R.layout.sl_header_default);
        a(C().getName());
        if (((Integer) k().a("challengeHeaderMode", (Object) 0)).intValue() == 0) {
            a().setImageDrawable(getResources().getDrawable(R.drawable.sl_header_icon_challenges));
            c(getResources().getString(R.string.sl_challenges));
            ImageView imageView = (ImageView) findViewById(R.id.sl_control_icon);
            imageView.setImageResource(R.drawable.sl_button_add_coins);
            imageView.setEnabled(true);
            imageView.setOnClickListener(this);
        } else {
            a().setImageDrawable(getResources().getDrawable(R.drawable.sl_header_icon_add_coins));
            c(getResources().getString(R.string.sl_add_coins));
        }
        a(s.a("userValues", "userBalance"));
    }

    public void onStart() {
        super.onStart();
        y().b("userBalance");
    }

    public final void a(s sVar, String str, Object obj, Object obj2) {
        b(m.a(this, y(), x()));
    }

    public final void a(s sVar, String str) {
        if ("userBalance".equals(str)) {
            y().a(str, h.NOT_DIRTY, (Object) null);
        }
    }
}
