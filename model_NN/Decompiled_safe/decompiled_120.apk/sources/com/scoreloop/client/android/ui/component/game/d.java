package com.scoreloop.client.android.ui.component.game;

import android.view.View;
import com.scoreloop.client.android.core.model.Game;

final class d implements View.OnClickListener {
    private /* synthetic */ GameDetailHeaderActivity a;
    private final /* synthetic */ Game b;

    d(GameDetailHeaderActivity gameDetailHeaderActivity, Game game) {
        this.a = gameDetailHeaderActivity;
        this.b = game;
    }

    public final void onClick(View view) {
        this.a.B();
        this.b.getName();
        com.scoreloop.client.android.ui.component.base.d.a(this.a, this.b);
    }
}
