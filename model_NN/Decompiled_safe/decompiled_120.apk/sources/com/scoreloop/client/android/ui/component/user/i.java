package com.scoreloop.client.android.ui.component.user;

import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.k;
import org.anddev.andengine.R;

public final class i extends k {
    public i(ComponentActivity componentActivity) {
        super(componentActivity, componentActivity.getResources().getDrawable(R.drawable.sl_icon_recommend), componentActivity.getResources().getString(R.string.sl_find_match), null, null);
    }

    /* access modifiers changed from: protected */
    public final int h() {
        return R.layout.sl_list_item_user;
    }

    public final int a() {
        return 28;
    }
}
