package com.scoreloop.client.android.ui.component.challenge;

import com.scoreloop.client.android.core.model.Challenge;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.k;
import com.scoreloop.client.android.ui.component.base.m;
import org.anddev.andengine.R;

public final class o extends k {
    public o(ComponentActivity componentActivity, Challenge challenge) {
        super(componentActivity, challenge);
        a(componentActivity.getResources().getDrawable(R.drawable.sl_icon_user));
        a(challenge.getContender().getDisplayName());
        c(componentActivity.e(challenge.getMode().intValue()));
        b(m.a(challenge.getStake(), n().x()));
    }

    /* access modifiers changed from: protected */
    public final String i() {
        return ((Challenge) p()).getContender().getImageUrl();
    }

    public final int a() {
        return 6;
    }

    /* access modifiers changed from: protected */
    public final int h() {
        return R.layout.sl_list_item_challenge_open;
    }

    /* access modifiers changed from: protected */
    public final int o() {
        return R.id.sl_subtitle2;
    }

    public final boolean b() {
        return true;
    }
}
