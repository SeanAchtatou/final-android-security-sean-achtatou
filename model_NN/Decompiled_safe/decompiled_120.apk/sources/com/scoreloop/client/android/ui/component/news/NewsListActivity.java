package com.scoreloop.client.android.ui.component.news;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ListAdapter;
import com.scoreloop.client.android.core.addon.RSSItem;
import com.scoreloop.client.android.ui.component.base.ComponentListActivity;
import com.scoreloop.client.android.ui.component.base.h;
import com.scoreloop.client.android.ui.framework.a;
import com.scoreloop.client.android.ui.framework.i;
import com.scoreloop.client.android.ui.framework.s;
import java.util.List;
import org.anddev.andengine.R;

public class NewsListActivity extends ComponentListActivity {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a((ListAdapter) new i(this));
        a(s.a("userValues", "newsFeed"));
    }

    public final void a(a aVar) {
        if (aVar.a() == 15) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse(((a) aVar).g().getLinkUrlString())));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.framework.s.b(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, int]
     candidates:
      com.scoreloop.client.android.ui.framework.s.b(java.lang.String, com.scoreloop.client.android.ui.framework.aj):void
      com.scoreloop.client.android.ui.framework.s.b(java.lang.String, java.lang.Object):void */
    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        List<RSSItem> list = (List) y().a("newsFeed");
        if (list != null) {
            for (RSSItem hasPersistentReadFlag : list) {
                hasPersistentReadFlag.setHasPersistentReadFlag(true);
            }
        }
        y().b("newsNumberUnreadItems", (Object) 0);
    }

    public final void a(s sVar, String str, Object obj, Object obj2) {
        i t = t();
        t.clear();
        List<RSSItem> list = (List) y().a("newsFeed");
        if (list == null || list.size() <= 0) {
            t.add(new h(this, getString(R.string.sl_no_news)));
            return;
        }
        for (RSSItem aVar : list) {
            t.add(new a(this, aVar));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.h, java.lang.Object):boolean
     arg types: [java.lang.String, com.scoreloop.client.android.ui.framework.h, long]
     candidates:
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, java.lang.Object, java.lang.Object):void
      com.scoreloop.client.android.ui.framework.s.a(com.scoreloop.client.android.ui.framework.s, java.util.Set, com.scoreloop.client.android.ui.framework.aj):void
      com.scoreloop.client.android.ui.framework.s.a(java.lang.String, com.scoreloop.client.android.ui.framework.h, java.lang.Object):boolean */
    public final void a(s sVar, String str) {
        if ("newsFeed".equals(str)) {
            y().a(str, com.scoreloop.client.android.ui.framework.h.NOT_OLDER_THAN, (Object) 30000L);
        }
    }
}
