package com.scoreloop.client.android.ui.framework;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import org.anddev.andengine.R;

public final class v extends w {
    public v(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public final int a() {
        return R.layout.sl_dialog_text_button;
    }

    public final void onClick(View view) {
        if (this.a != null && view.getId() == R.id.sl_button_ok) {
            this.a.a(this, 0);
        }
    }

    /* access modifiers changed from: protected */
    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ((Button) findViewById(R.id.sl_button_ok)).setOnClickListener(this);
    }
}
