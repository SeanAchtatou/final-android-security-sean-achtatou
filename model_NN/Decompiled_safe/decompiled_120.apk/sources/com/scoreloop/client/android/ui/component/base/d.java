package com.scoreloop.client.android.ui.component.base;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Session;

public final class d {
    private static final String[] a = {"com.scoreloop.android.slapp"};

    private static void a(Context context, String str) {
        if (str != null) {
            context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
        }
    }

    public static void a(Context context) {
        a(context, Session.getCurrentSession().getScoreloopAppDownloadUrl());
    }

    public static void a(Context context, Game game) {
        a(context, game.getDownloadUrl());
    }

    private static Intent a(Context context, String[] strArr) {
        int i = 0;
        PackageManager packageManager = context.getPackageManager();
        while (i < strArr.length) {
            try {
                if (packageManager.getPackageInfo(strArr[i], 0) != null) {
                    return packageManager.getLaunchIntentForPackage(strArr[i]);
                }
                i++;
            } catch (PackageManager.NameNotFoundException e) {
            }
        }
        return null;
    }

    public static void b(Context context, Game game) {
        Intent a2 = a(context, game.getPackageNames());
        if (a2 != null) {
            context.startActivity(a2);
        }
    }

    public static boolean b(Context context) {
        return a(context, a) != null;
    }

    public static boolean c(Context context, Game game) {
        return a(context, game.getPackageNames()) != null;
    }
}
