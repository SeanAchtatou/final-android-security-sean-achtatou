package com.scoreloop.client.android.ui.component.achievement;

import android.content.Context;
import android.os.Bundle;
import com.scoreloop.client.android.ui.component.base.ComponentHeaderActivity;
import com.scoreloop.client.android.ui.component.base.m;
import com.scoreloop.client.android.ui.framework.h;
import com.scoreloop.client.android.ui.framework.s;
import org.anddev.andengine.R;

public class AchievementHeaderActivity extends ComponentHeaderActivity {
    public void onCreate(Bundle bundle) {
        super.a(bundle, (int) R.layout.sl_header_default);
        a().setImageDrawable(getResources().getDrawable(R.drawable.sl_header_icon_achievements));
        a(C().getName());
        c(getString(R.string.sl_achievements));
        a(s.a("userValues", "numberAchievements"), s.a("userValues", "numberAwards"));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.component.base.m.a(android.content.Context, com.scoreloop.client.android.ui.framework.s, boolean):java.lang.String
     arg types: [com.scoreloop.client.android.ui.component.achievement.AchievementHeaderActivity, com.scoreloop.client.android.ui.framework.s, int]
     candidates:
      com.scoreloop.client.android.ui.component.base.m.a(android.content.Context, com.scoreloop.client.android.ui.framework.s, com.scoreloop.client.android.ui.component.base.a):java.lang.String
      com.scoreloop.client.android.ui.component.base.m.a(android.content.Context, com.scoreloop.client.android.ui.framework.s, boolean):java.lang.String */
    public final void a(s sVar, String str, Object obj, Object obj2) {
        b(m.a((Context) this, y(), true));
    }

    public final void a(s sVar, String str) {
        if (str.equals("numberAchievements")) {
            y().a(str, h.NOT_DIRTY, (Object) null);
        }
        if (str.equals("numberAwards")) {
            y().a(str, h.NOT_DIRTY, (Object) null);
        }
    }
}
