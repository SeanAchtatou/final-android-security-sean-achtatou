package com.scoreloop.client.android.ui.component.base;

import org.anddev.andengine.R;

public final class l extends k {
    public l(ComponentActivity componentActivity) {
        super(componentActivity, componentActivity.getResources().getDrawable(R.drawable.sl_icon_see_more), componentActivity.getResources().getString(R.string.sl_see_more), null, null);
    }

    /* access modifiers changed from: protected */
    public final int h() {
        return R.layout.sl_list_item_icon_title_small;
    }

    public final int a() {
        return 11;
    }
}
